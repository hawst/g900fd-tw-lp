.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;
.super Ljava/lang/Object;
.source "Util.java"


# static fields
.field public static final LAYOUT_TYPE_1LINE:I = 0x1

.field public static final LAYOUT_TYPE_2LINE:I = 0x2

.field public static final LOCATION_SETTING_REQUEST_CODE:I = 0x4b3

.field private static final OTHER_REFRESH_OK:Ljava/lang/String; = "other_refresh"

.field private static final PREF_NAME:Ljava/lang/String; = "updatetime"

.field private static final PREF_UPDATE_TIME:Ljava/lang/String; = "fail_updatetime"

.field private static SHARED_PREF_FIRST:Ljava/lang/String; = null

.field private static final SHARED_PREF_IS_AGREE_USE_LOCATION:Ljava/lang/String; = "SHOW_USE_LOCATION_POPUP"

.field private static final SHARED_PREF_SHOW_CHARGER_POPUP:Ljava/lang/String; = "SHOW_CHARGER_POPUP"

.field public static final hour:J = 0x36ee80L

.field private static isOnTap:Z = false

.field static lang:[Ljava/lang/String; = null

.field private static localeArray:[Ljava/lang/String; = null

.field public static final min:J = 0xea60L

.field static sdf:Ljava/text/SimpleDateFormat;

.field private static toast:Landroid/widget/Toast;

.field private static final weatherEasyModeWidget:[I

.field private static final weatherIconForDetails:[I

.field private static final weatherIconForList:[I

.field private static final weatherIconForWidget:[I

.field private static final weatherIconMore:[I

.field private static final weatherIconSmall:[I

.field private static final weatherMapIcon:[I

.field private static final weatherMapIconNew:[I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/16 v3, 0x14

    .line 1192
    new-array v0, v3, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->weatherIconForWidget:[I

    .line 1224
    new-array v0, v3, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->weatherIconForDetails:[I

    .line 1240
    new-array v0, v3, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->weatherIconMore:[I

    .line 1255
    new-array v0, v3, [I

    fill-array-data v0, :array_3

    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->weatherIconSmall:[I

    .line 1270
    new-array v0, v3, [I

    fill-array-data v0, :array_4

    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->weatherIconForList:[I

    .line 1308
    new-array v0, v3, [I

    fill-array-data v0, :array_5

    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->weatherMapIcon:[I

    .line 1321
    new-array v0, v3, [I

    fill-array-data v0, :array_6

    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->weatherMapIconNew:[I

    .line 1336
    new-array v0, v3, [I

    fill-array-data v0, :array_7

    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->weatherEasyModeWidget:[I

    .line 2426
    const/16 v0, 0x22

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "en#en-US"

    aput-object v1, v0, v4

    const-string v1, "en_US#en-us"

    aput-object v1, v0, v5

    const-string v1, "en_GB#en-gb"

    aput-object v1, v0, v6

    const-string v1, "es#es"

    aput-object v1, v0, v7

    const/4 v1, 0x4

    const-string v2, "fr#fr"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "da#da"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "pt#pt"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "nl#nl"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "nb#no"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "it#it"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "de#de"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "sv#sv"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "fi#fi"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "zh#zh-cn"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "zh_CN#zh-cn"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "zh_TW#zh-tw"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, ""

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, ""

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "sk#sk"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "ro#ro"

    aput-object v2, v0, v1

    const-string v1, "cs#cs"

    aput-object v1, v0, v3

    const/16 v1, 0x15

    const-string v2, "hu#hu"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "pl#pl"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, ""

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "pt_BR#pt-br"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "hi#in"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "ru#ru"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "ar#ar"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "el#el"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "ja#ja"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "ko#ka"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "tr#tr"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "fr_CA#fr-ca"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, "iw#he"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->localeArray:[Ljava/lang/String;

    .line 2632
    const-string v0, "first_update"

    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->SHARED_PREF_FIRST:Ljava/lang/String;

    .line 2888
    const/16 v0, 0x6e

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, ""

    aput-object v1, v0, v4

    const-string v1, "ar"

    aput-object v1, v0, v5

    const-string v1, "ar-dz"

    aput-object v1, v0, v6

    const-string v1, "ar-bh"

    aput-object v1, v0, v7

    const/4 v1, 0x4

    const-string v2, "ar-eg"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "ar-iq"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "ar-jo"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "ar-kw"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "ar-lb"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "ar-ly"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "ar-ma"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "ar-om"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "ar-qa"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "ar-sa"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "ar-sy"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "ar-tn"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "ar-ae"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "ar-ye"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "bn"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "bs"

    aput-object v2, v0, v1

    const-string v1, "bg"

    aput-object v1, v0, v3

    const/16 v1, 0x15

    const-string v2, "ca"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "hr"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "cs"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "zh"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "zh-hk"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "zh-cn"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "zh-sg"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "zh-tw"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "da"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "nl"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "nl-be"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "en"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, "en-au"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, "en-bz"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string v2, "en-ca"

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string v2, "en-ie"

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string v2, "en-nz"

    aput-object v2, v0, v1

    const/16 v1, 0x26

    const-string v2, "en-za"

    aput-object v2, v0, v1

    const/16 v1, 0x27

    const-string v2, "en-tt"

    aput-object v2, v0, v1

    const/16 v1, 0x28

    const-string v2, "en-gb"

    aput-object v2, v0, v1

    const/16 v1, 0x29

    const-string v2, "en-us"

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    const-string v2, "et"

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    const-string v2, "fa"

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    const-string v2, "ph"

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    const-string v2, "fi"

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    const-string v2, "fr"

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    const-string v2, "fr-be"

    aput-object v2, v0, v1

    const/16 v1, 0x30

    const-string v2, "fr-ca"

    aput-object v2, v0, v1

    const/16 v1, 0x31

    const-string v2, "fr-lu"

    aput-object v2, v0, v1

    const/16 v1, 0x32

    const-string v2, "fr-ch"

    aput-object v2, v0, v1

    const/16 v1, 0x33

    const-string v2, "de"

    aput-object v2, v0, v1

    const/16 v1, 0x34

    const-string v2, "de-at"

    aput-object v2, v0, v1

    const/16 v1, 0x35

    const-string v2, "de-li"

    aput-object v2, v0, v1

    const/16 v1, 0x36

    const-string v2, "de-lu"

    aput-object v2, v0, v1

    const/16 v1, 0x37

    const-string v2, "de-ch"

    aput-object v2, v0, v1

    const/16 v1, 0x38

    const-string v2, "el"

    aput-object v2, v0, v1

    const/16 v1, 0x39

    const-string v2, "he"

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    const-string v2, "hi"

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    const-string v2, "hu"

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    const-string v2, "is"

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    const-string v2, "id"

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    const-string v2, "it"

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    const-string v2, "it-ch"

    aput-object v2, v0, v1

    const/16 v1, 0x40

    const-string v2, "ja"

    aput-object v2, v0, v1

    const/16 v1, 0x41

    const-string v2, "kk"

    aput-object v2, v0, v1

    const/16 v1, 0x42

    const-string v2, "ko"

    aput-object v2, v0, v1

    const/16 v1, 0x43

    const-string v2, "lv"

    aput-object v2, v0, v1

    const/16 v1, 0x44

    const-string v2, "lt"

    aput-object v2, v0, v1

    const/16 v1, 0x45

    const-string v2, "mk"

    aput-object v2, v0, v1

    const/16 v1, 0x46

    const-string v2, "ms"

    aput-object v2, v0, v1

    const/16 v1, 0x47

    const-string v2, "sr-me"

    aput-object v2, v0, v1

    const/16 v1, 0x48

    const-string v2, "no"

    aput-object v2, v0, v1

    const/16 v1, 0x49

    const-string v2, "pl"

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    const-string v2, "pt"

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    const-string v2, "pt-br"

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    const-string v2, "ro"

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    const-string v2, "ro-mo"

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    const-string v2, "ru"

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    const-string v2, "ru-mo"

    aput-object v2, v0, v1

    const/16 v1, 0x50

    const-string v2, "sr"

    aput-object v2, v0, v1

    const/16 v1, 0x51

    const-string v2, "sk"

    aput-object v2, v0, v1

    const/16 v1, 0x52

    const-string v2, "sl"

    aput-object v2, v0, v1

    const/16 v1, 0x53

    const-string v2, "es"

    aput-object v2, v0, v1

    const/16 v1, 0x54

    const-string v2, "es-ar"

    aput-object v2, v0, v1

    const/16 v1, 0x55

    const-string v2, "es-bo"

    aput-object v2, v0, v1

    const/16 v1, 0x56

    const-string v2, "es-cl"

    aput-object v2, v0, v1

    const/16 v1, 0x57

    const-string v2, "es-co"

    aput-object v2, v0, v1

    const/16 v1, 0x58

    const-string v2, "es-cr"

    aput-object v2, v0, v1

    const/16 v1, 0x59

    const-string v2, "es-do"

    aput-object v2, v0, v1

    const/16 v1, 0x5a

    const-string v2, "es-ec"

    aput-object v2, v0, v1

    const/16 v1, 0x5b

    const-string v2, "es-sv"

    aput-object v2, v0, v1

    const/16 v1, 0x5c

    const-string v2, "es-gt"

    aput-object v2, v0, v1

    const/16 v1, 0x5d

    const-string v2, "es-hn"

    aput-object v2, v0, v1

    const/16 v1, 0x5e

    const-string v2, "es-mx"

    aput-object v2, v0, v1

    const/16 v1, 0x5f

    const-string v2, "es-ni"

    aput-object v2, v0, v1

    const/16 v1, 0x60

    const-string v2, "es-pa"

    aput-object v2, v0, v1

    const/16 v1, 0x61

    const-string v2, "es-py"

    aput-object v2, v0, v1

    const/16 v1, 0x62

    const-string v2, "es-pu"

    aput-object v2, v0, v1

    const/16 v1, 0x63

    const-string v2, "es-pr"

    aput-object v2, v0, v1

    const/16 v1, 0x64

    const-string v2, "es-uy"

    aput-object v2, v0, v1

    const/16 v1, 0x65

    const-string v2, "es-ve"

    aput-object v2, v0, v1

    const/16 v1, 0x66

    const-string v2, "sw"

    aput-object v2, v0, v1

    const/16 v1, 0x67

    const-string v2, "sv"

    aput-object v2, v0, v1

    const/16 v1, 0x68

    const-string v2, "sv-fi"

    aput-object v2, v0, v1

    const/16 v1, 0x69

    const-string v2, "th"

    aput-object v2, v0, v1

    const/16 v1, 0x6a

    const-string v2, "tr"

    aput-object v2, v0, v1

    const/16 v1, 0x6b

    const-string v2, "uk"

    aput-object v2, v0, v1

    const/16 v1, 0x6c

    const-string v2, "ur"

    aput-object v2, v0, v1

    const/16 v1, 0x6d

    const-string v2, "vi"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->lang:[Ljava/lang/String;

    return-void

    .line 1192
    nop

    :array_0
    .array-data 4
        0x0
        0x7f020101
        0x7f0200f7
        0x7f0200f4
        0x7f0200fb
        0x7f0200f0
        0x7f0200fe
        0x7f0200f9
        0x7f020105
        0x7f0200fa
        0x7f0200ef
        0x7f0200f8
        0x7f0200ff
        0x7f0200fd
        0x7f0200f2
        0x7f0200f1
        0x7f0200ee
        0x7f020106
        0x7f0200ed
        0x7f0200f3
    .end array-data

    .line 1224
    :array_1
    .array-data 4
        0x0
        0x7f020101
        0x7f0200f7
        0x7f0200f4
        0x7f0200fb
        0x7f0200f0
        0x7f0200fe
        0x7f0200f9
        0x7f020105
        0x7f0200fa
        0x7f0200ef
        0x7f0200f8
        0x7f0200ff
        0x7f0200fd
        0x7f0200f2
        0x7f0200f1
        0x7f0200ee
        0x7f020106
        0x7f0200ed
        0x7f0200f3
    .end array-data

    .line 1240
    :array_2
    .array-data 4
        0x0
        0x7f02007a
        0x7f020072
        0x7f020071
        0x7f020076
        0x7f02006d
        0x7f020078
        0x7f020074
        0x7f02007b
        0x7f020075
        0x7f02006c
        0x7f020073
        0x7f020079
        0x7f020077
        0x7f02006f
        0x7f02006e
        0x7f02006b
        0x7f02007c
        0x7f02006a
        0x7f020070
    .end array-data

    .line 1255
    :array_3
    .array-data 4
        0x0
        0x7f020051
        0x7f020049
        0x7f020048
        0x7f02004d
        0x7f020044
        0x7f02004f
        0x7f02004b
        0x7f020052
        0x7f02004c
        0x7f020043
        0x7f02004a
        0x7f020050
        0x7f02004e
        0x7f020046
        0x7f020045
        0x7f020042
        0x7f020053
        0x7f020041
        0x7f020047
    .end array-data

    .line 1270
    :array_4
    .array-data 4
        0x0
        0x7f020101
        0x7f0200f7
        0x7f0200f4
        0x7f0200fb
        0x7f0200f0
        0x7f0200fe
        0x7f0200f9
        0x7f020105
        0x7f0200fa
        0x7f0200ef
        0x7f0200f8
        0x7f0200ff
        0x7f0200fd
        0x7f0200f2
        0x7f0200f1
        0x7f0200ee
        0x7f020106
        0x7f0200ed
        0x7f0200f3
    .end array-data

    .line 1308
    :array_5
    .array-data 4
        0x0
        0x7f020107
        0x7f020108
        0x7f020109
        0x7f02010a
        0x7f02010b
        0x7f02010c
        0x7f02010d
        0x7f02010e
        0x7f02010f
        0x7f020110
        0x7f020111
        0x7f020112
        0x7f020113
        0x7f020114
        0x7f020115
        0x7f020116
        0x7f020117
        0x7f020118
        0x7f020119
    .end array-data

    .line 1321
    :array_6
    .array-data 4
        0x0
        0x7f020066
        0x7f02005e
        0x7f02005d
        0x7f020062
        0x7f020059
        0x7f020064
        0x7f020060
        0x7f020067
        0x7f020061
        0x7f020058
        0x7f02005f
        0x7f020065
        0x7f020063
        0x7f02005b
        0x7f02005a
        0x7f020057
        0x7f020068
        0x7f020056
        0x7f02005c
    .end array-data

    .line 1336
    :array_7
    .array-data 4
        0x0
        0x7f020033
        0x7f02002b
        0x7f02002a
        0x7f02002f
        0x7f020026
        0x7f020031
        0x7f02002d
        0x7f020034
        0x7f02002e
        0x7f020025
        0x7f02002c
        0x7f020032
        0x7f020030
        0x7f020028
        0x7f020027
        0x7f020024
        0x7f020035
        0x7f020023
        0x7f020029
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static addPreferenceWidgetCount(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 6
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "whatWidget"    # Ljava/lang/String;
    .param p2, "idsCount"    # I

    .prologue
    const/4 v5, 0x0

    .line 4085
    const/4 v1, 0x0

    .line 4087
    .local v1, "otherWidgetCnt":I
    const-string v3, "pref_widget_count"

    const/4 v4, 0x4

    invoke-virtual {p0, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 4089
    .local v2, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 4090
    .local v0, "edit":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 4091
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 4094
    const-string v3, "widget_count_surface"

    if-ne p1, v3, :cond_0

    .line 4095
    const-string v3, "widget_count_remote"

    invoke-interface {v2, v3, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 4096
    add-int/2addr v1, p2

    .line 4103
    :goto_0
    invoke-static {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->storeWidgetCountToDataBase(Landroid/content/Context;I)V

    .line 4105
    const-string v3, ""

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "addPWC = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4106
    return-void

    .line 4098
    :cond_0
    const-string v3, "widget_count_surface"

    invoke-interface {v2, v3, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 4099
    add-int/2addr v1, p2

    goto :goto_0
.end method

.method public static addPreferenceWidgetCountForRemoteWidget(Landroid/content/Context;I)V
    .locals 1
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "idsCount"    # I

    .prologue
    .line 4072
    const-string v0, "widget_count_remote"

    invoke-static {p0, v0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->addPreferenceWidgetCount(Landroid/content/Context;Ljava/lang/String;I)V

    .line 4073
    return-void
.end method

.method public static addPreferenceWidgetCountForSurfaceWidget(Landroid/content/Context;)V
    .locals 2
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    .line 4063
    const-string v0, "widget_count_surface"

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->addPreferenceWidgetCount(Landroid/content/Context;Ljava/lang/String;I)V

    .line 4064
    return-void
.end method

.method public static calAlphaVal(I)I
    .locals 4
    .param p0, "val"    # I

    .prologue
    const/high16 v3, 0x437f0000    # 255.0f

    .line 3453
    int-to-float v1, p0

    const v2, 0x3c23d70a    # 0.01f

    mul-float/2addr v1, v2

    mul-float/2addr v1, v3

    sub-float v0, v3, v1

    .line 3454
    .local v0, "alphaVal":F
    float-to-int v1, v0

    return v1
.end method

.method public static cancelToast()V
    .locals 1

    .prologue
    .line 575
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->toast:Landroid/widget/Toast;

    if-eqz v0, :cond_0

    .line 576
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->toast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    .line 578
    :cond_0
    return-void
.end method

.method public static checkAcquireContentProviderClient(Landroid/content/ContentResolver;Landroid/net/Uri;)Z
    .locals 2
    .param p0, "cp"    # Landroid/content/ContentResolver;
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 3458
    const/4 v1, 0x0

    .line 3459
    .local v1, "value":Z
    invoke-virtual {p0, p1}, Landroid/content/ContentResolver;->acquireContentProviderClient(Landroid/net/Uri;)Landroid/content/ContentProviderClient;

    move-result-object v0

    .line 3460
    .local v0, "a":Landroid/content/ContentProviderClient;
    if-eqz v0, :cond_0

    .line 3461
    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->release()Z

    .line 3462
    const/4 v1, 0x1

    .line 3466
    :goto_0
    return v1

    .line 3464
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static checkDayOrNight(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .param p0, "timezone"    # Ljava/lang/String;
    .param p1, "sunrise"    # Ljava/lang/String;
    .param p2, "sunset"    # Ljava/lang/String;

    .prologue
    .line 2207
    const/4 v0, 0x0

    .line 2208
    .local v0, "t":Ljava/util/TimeZone;
    if-eqz p0, :cond_0

    .line 2209
    invoke-static {p0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    .line 2212
    :cond_0
    invoke-static {v0, p1, p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->isDay(Ljava/util/TimeZone;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    return v1
.end method

.method public static checkKeyboard(Landroid/view/inputmethod/InputMethodManager;Landroid/content/Context;)Z
    .locals 12
    .param p0, "inputManager"    # Landroid/view/inputmethod/InputMethodManager;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v7, 0x0

    const/4 v8, 0x1

    .line 3995
    const/4 v0, 0x0

    .line 3996
    .local v0, "isSharingKMS":Z
    const/4 v1, 0x0

    .line 3998
    .local v1, "isSharingPSS":I
    const-string v5, "com.sec.android.sidesync.kms.sink.service.SideSyncServerService"

    .line 3999
    .local v5, "serviceSinkName":Ljava/lang/String;
    const-string v6, "com.sec.android.sidesync.kms.source.service.SideSyncService"

    .line 4000
    .local v6, "serviceSourceName":Ljava/lang/String;
    const-string v9, "activity"

    invoke-virtual {p1, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/ActivityManager;

    .line 4002
    .local v3, "manager":Landroid/app/ActivityManager;
    const v9, 0x7fffffff

    invoke-virtual {v3, v9}, Landroid/app/ActivityManager;->getRunningServices(I)Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_0
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_2

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/ActivityManager$RunningServiceInfo;

    .line 4004
    .local v4, "service":Landroid/app/ActivityManager$RunningServiceInfo;
    iget-object v10, v4, Landroid/app/ActivityManager$RunningServiceInfo;->service:Landroid/content/ComponentName;

    invoke-virtual {v10}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_1

    iget-object v10, v4, Landroid/app/ActivityManager$RunningServiceInfo;->service:Landroid/content/ComponentName;

    .line 4006
    invoke-virtual {v10}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 4008
    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    .line 4013
    .end local v4    # "service":Landroid/app/ActivityManager$RunningServiceInfo;
    :cond_2
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    const-string v10, "sidesync_source_connect"

    invoke-static {v9, v10, v7}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 4014
    const-string v9, ""

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "isSharingKMS = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", isSharingPSS = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4019
    invoke-virtual {p0}, Landroid/view/inputmethod/InputMethodManager;->isAccessoryKeyboardState()I

    move-result v2

    .line 4020
    .local v2, "mKeyboard":I
    and-int/lit8 v9, v2, 0x1

    if-eq v9, v8, :cond_3

    and-int/lit8 v9, v2, 0x2

    const/4 v10, 0x2

    if-eq v9, v10, :cond_3

    and-int/lit8 v9, v2, 0x4

    const/4 v10, 0x4

    if-eq v9, v10, :cond_3

    if-eq v0, v8, :cond_3

    if-ne v1, v8, :cond_4

    :cond_3
    move v7, v8

    .line 4027
    :cond_4
    return v7
.end method

.method public static checkLanguage(Landroid/content/Context;)Ljava/lang/String;
    .locals 7
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    .line 2903
    const-string v1, ""

    .line 2904
    .local v1, "isLang":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget-object v4, v4, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v4}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    .line 2905
    .local v3, "langID":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget-object v4, v4, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v4}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v2

    .line 2906
    .local v2, "langCountry":Ljava/lang/String;
    const-string v4, ""

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "cL lID="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2907
    const-string v4, ""

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "cL lC="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2908
    const-string v4, "zh"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2909
    const-string v4, ""

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 2910
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "-"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2914
    :cond_0
    const-string v4, "in"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2915
    const-string v1, "id"

    .line 2919
    :cond_1
    const-string v4, "iw"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2920
    const-string v1, "he"

    .line 2924
    :cond_2
    const-string v4, "tl"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2925
    const-string v1, "ph"

    .line 2929
    :cond_3
    const-string v4, "uz"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    const-string v4, "ky"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    const-string v4, "tg"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    const-string v4, "tk"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    const-string v4, "az"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    const-string v4, "hy"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 2930
    :cond_4
    const-string v1, "ru"

    .line 2932
    :cond_5
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->lang:[Ljava/lang/String;

    array-length v4, v4

    if-ge v0, v4, :cond_7

    .line 2933
    sget-object v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->lang:[Ljava/lang/String;

    aget-object v4, v4, v0

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 2934
    sget-object v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->lang:[Ljava/lang/String;

    aget-object v1, v4, v0

    .line 2932
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2937
    :cond_7
    if-eqz v1, :cond_8

    const-string v4, ""

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 2938
    :cond_8
    const-string v1, "en"

    .line 2940
    :cond_9
    const-string v4, ""

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "cL iL="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2941
    return-object v1
.end method

.method public static checkLanucherMode(Landroid/content/Context;)Z
    .locals 4
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x1

    .line 2668
    .line 2669
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "easy_mode_switch"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 2670
    .local v0, "easyModeSwitch":I
    if-nez v0, :cond_0

    .line 2673
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static checkNetworkConnected(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 581
    const-string v2, "connectivity"

    .line 582
    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 584
    .local v0, "connManager":Landroid/net/ConnectivityManager;
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v1

    .line 585
    .local v1, "networkInfo":Landroid/net/NetworkInfo;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static checkNewVersion(Landroid/content/Context;I)Z
    .locals 13
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "activity"    # I

    .prologue
    .line 3273
    const/4 v7, 0x1

    .line 3275
    .local v7, "isSamsungAppsInstalled":Z
    const/4 v5, 0x0

    .line 3277
    .local v5, "file":Ljava/io/File;
    :try_start_0
    new-instance v6, Ljava/io/File;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "/go_to_andromeda.test"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v6, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 3278
    .end local v5    # "file":Ljava/io/File;
    .local v6, "file":Ljava/io/File;
    :try_start_1
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_1

    .line 3279
    const-string v10, ""

    const-string v11, "check file : for testing"

    invoke-static {v10, v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3280
    const/4 v10, 0x1

    sput-boolean v10, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/Define;->IS_TEST_PURPOSEED:Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    :goto_0
    move-object v5, v6

    .line 3300
    .end local v6    # "file":Ljava/io/File;
    .restart local v5    # "file":Ljava/io/File;
    :goto_1
    :try_start_2
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v10

    const-string v11, "com.sec.android.app.samsungapps"

    const/4 v12, 0x0

    invoke-virtual {v10, v11, v12}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_1

    .line 3306
    :goto_2
    const-string v10, ""

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "checkNewVersion : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", for testing = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    sget-boolean v12, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/Define;->IS_TEST_PURPOSEED:Z

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3309
    if-eqz v7, :cond_3

    .line 3310
    const-string v10, "config"

    const/4 v11, 0x4

    invoke-virtual {p0, v10, v11}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v10

    const-string v11, "Apps_Date"

    const-string v12, "0"

    invoke-interface {v10, v11, v12}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    .line 3311
    .local v8, "lastTime":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 3312
    .local v0, "currentTime":J
    sub-long v2, v0, v8

    .line 3314
    .local v2, "diff":J
    const/4 v10, 0x1

    if-ne p1, v10, :cond_2

    .line 3315
    const-string v10, ""

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "checkNewVersion : l = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", c = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3316
    const-wide/16 v10, 0x0

    cmp-long v10, v8, v10

    if-eqz v10, :cond_0

    const-wide v10, 0xd84b1800L

    cmp-long v10, v2, v10

    if-lez v10, :cond_3

    .line 3317
    :cond_0
    const/4 v10, 0x1

    invoke-static {p0, v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->updateCheck(Landroid/content/Context;I)V

    .line 3318
    const/4 v10, 0x1

    .line 3325
    .end local v0    # "currentTime":J
    .end local v2    # "diff":J
    .end local v8    # "lastTime":J
    :goto_3
    return v10

    .line 3282
    .end local v5    # "file":Ljava/io/File;
    .restart local v6    # "file":Ljava/io/File;
    :cond_1
    const/4 v10, 0x0

    :try_start_3
    sput-boolean v10, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/Define;->IS_TEST_PURPOSEED:Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_0

    .line 3284
    :catch_0
    move-exception v4

    move-object v5, v6

    .line 3285
    .end local v6    # "file":Ljava/io/File;
    .local v4, "e":Ljava/lang/Exception;
    .restart local v5    # "file":Ljava/io/File;
    :goto_4
    const-string v10, ""

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "check file error msg : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3286
    const/4 v10, 0x0

    sput-boolean v10, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/Define;->IS_TEST_PURPOSEED:Z

    goto/16 :goto_1

    .line 3301
    .end local v4    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v4

    .line 3302
    .local v4, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v7, 0x0

    .line 3303
    const-string v10, ""

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "checkNewVersion : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 3320
    .end local v4    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v0    # "currentTime":J
    .restart local v2    # "diff":J
    .restart local v8    # "lastTime":J
    :cond_2
    const/4 v10, 0x2

    if-ne p1, v10, :cond_3

    .line 3321
    const/4 v10, 0x2

    invoke-static {p0, v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->updateCheck(Landroid/content/Context;I)V

    .line 3322
    const/4 v10, 0x1

    goto :goto_3

    .line 3325
    .end local v0    # "currentTime":J
    .end local v2    # "diff":J
    .end local v8    # "lastTime":J
    :cond_3
    const/4 v10, 0x0

    goto :goto_3

    .line 3284
    :catch_2
    move-exception v4

    goto :goto_4
.end method

.method public static checkPackege(Landroid/content/Context;)Ljava/lang/Boolean;
    .locals 7
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    const/4 v6, 0x1

    const/4 v3, 0x0

    .line 3087
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 3090
    .local v2, "result":Ljava/lang/Boolean;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const-string v4, "com.samsung.helphub"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 3091
    .local v1, "info":Landroid/content/pm/PackageInfo;
    iget v3, v1, Landroid/content/pm/PackageInfo;->versionCode:I

    if-ne v3, v6, :cond_1

    .line 3092
    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 3099
    .end local v1    # "info":Landroid/content/pm/PackageInfo;
    :cond_0
    :goto_0
    return-object v2

    .line 3093
    .restart local v1    # "info":Landroid/content/pm/PackageInfo;
    :cond_1
    iget v3, v1, Landroid/content/pm/PackageInfo;->versionCode:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_0

    .line 3094
    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_0

    .line 3096
    .end local v1    # "info":Landroid/content/pm/PackageInfo;
    :catch_0
    move-exception v0

    .line 3097
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method public static checkRefreshCondition(Ljava/lang/String;)Z
    .locals 9
    .param p0, "time"    # Ljava/lang/String;

    .prologue
    .line 2560
    const-wide/32 v0, 0x493e0

    .line 2562
    .local v0, "FIVE_MIN":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 2563
    .local v2, "currenttime":J
    invoke-static {p0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 2565
    .local v4, "updatetime":J
    const-string v6, ""

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "cRefreshC : c = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", u = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2566
    const-wide/32 v6, 0x493e0

    add-long/2addr v6, v4

    cmp-long v6, v6, v2

    if-gez v6, :cond_0

    .line 2567
    const/4 v6, 0x1

    .line 2569
    :goto_0
    return v6

    :cond_0
    const/4 v6, 0x0

    goto :goto_0
.end method

.method public static checkSunriseOrSunset(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 8
    .param p0, "timezone"    # Ljava/lang/String;
    .param p1, "sunrise"    # Ljava/lang/String;
    .param p2, "sunset"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x1

    const/4 v7, 0x0

    .line 2224
    const/4 v0, 0x0

    .line 2225
    .local v0, "cal":Ljava/util/Calendar;
    if-eqz p0, :cond_2

    .line 2226
    invoke-static {p0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v6

    invoke-static {v6}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v0

    .line 2231
    :goto_0
    const/16 v6, 0xb

    invoke-virtual {v0, v6}, Ljava/util/Calendar;->get(I)I

    move-result v2

    .line 2232
    .local v2, "hour":I
    const/16 v6, 0xc

    invoke-virtual {v0, v6}, Ljava/util/Calendar;->get(I)I

    move-result v3

    .line 2234
    .local v3, "minute":I
    invoke-static {p1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->parseSunrise(Ljava/lang/String;Ljava/util/Calendar;)[I

    move-result-object v1

    .line 2235
    .local v1, "dayTime":[I
    invoke-static {p2, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->parseSunSet(Ljava/lang/String;Ljava/util/Calendar;)[I

    move-result-object v4

    .line 2237
    .local v4, "nightTime":[I
    if-eqz v1, :cond_1

    aget v6, v1, v7

    if-gt v2, v6, :cond_0

    aget v6, v1, v7

    if-ne v2, v6, :cond_1

    aget v6, v1, v5

    if-lt v3, v6, :cond_1

    .line 2238
    :cond_0
    if-nez v4, :cond_3

    .line 2239
    const-string v6, ""

    const-string v7, "nightT is null!!!!"

    invoke-static {v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2251
    :cond_1
    :goto_1
    return v5

    .line 2228
    .end local v1    # "dayTime":[I
    .end local v2    # "hour":I
    .end local v3    # "minute":I
    .end local v4    # "nightTime":[I
    :cond_2
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    goto :goto_0

    .line 2241
    .restart local v1    # "dayTime":[I
    .restart local v2    # "hour":I
    .restart local v3    # "minute":I
    .restart local v4    # "nightTime":[I
    :cond_3
    aget v6, v4, v7

    if-gt v2, v6, :cond_4

    aget v6, v4, v7

    if-ne v2, v6, :cond_5

    aget v5, v4, v5

    if-lt v3, v5, :cond_5

    .line 2243
    :cond_4
    const/4 v5, 0x3

    goto :goto_1

    .line 2246
    :cond_5
    const/4 v5, 0x2

    goto :goto_1
.end method

.method public static checkTodayWeatherIcon(I)Z
    .locals 1
    .param p0, "iconnum"    # I

    .prologue
    .line 1622
    const/4 v0, 0x0

    .line 1624
    .local v0, "value":Z
    packed-switch p0, :pswitch_data_0

    .line 1645
    :pswitch_0
    const/4 v0, 0x0

    .line 1649
    :goto_0
    return v0

    .line 1642
    :pswitch_1
    const/4 v0, 0x1

    .line 1643
    goto :goto_0

    .line 1624
    :pswitch_data_0
    .packed-switch 0xc
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static convertIcon(IZ)I
    .locals 4
    .param p0, "iconNum"    # I
    .param p1, "isDay"    # Z

    .prologue
    const/16 v3, 0x22

    const/16 v1, 0x21

    const/4 v2, 0x3

    const/4 v0, 0x1

    .line 1555
    sparse-switch p0, :sswitch_data_0

    .line 1580
    move v0, p0

    .line 1584
    .local v0, "imageId":I
    :goto_0
    return v0

    .line 1558
    .end local v0    # "imageId":I
    :sswitch_0
    if-ne p1, v0, :cond_0

    .line 1559
    .restart local v0    # "imageId":I
    :goto_1
    goto :goto_0

    .end local v0    # "imageId":I
    :cond_0
    move v0, v1

    .line 1558
    goto :goto_1

    .line 1564
    :sswitch_1
    if-ne p1, v0, :cond_1

    move v0, v2

    .line 1565
    .restart local v0    # "imageId":I
    :goto_2
    goto :goto_0

    .end local v0    # "imageId":I
    :cond_1
    move v0, v3

    .line 1564
    goto :goto_2

    .line 1568
    :sswitch_2
    if-ne p1, v0, :cond_2

    .line 1570
    .restart local v0    # "imageId":I
    :goto_3
    goto :goto_0

    .end local v0    # "imageId":I
    :cond_2
    move v0, v1

    .line 1568
    goto :goto_3

    .line 1576
    :sswitch_3
    if-ne p1, v0, :cond_3

    move v0, v2

    .line 1577
    .restart local v0    # "imageId":I
    :goto_4
    goto :goto_0

    .end local v0    # "imageId":I
    :cond_3
    move v0, v3

    .line 1576
    goto :goto_4

    .line 1555
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_0
        0x3 -> :sswitch_1
        0x4 -> :sswitch_1
        0x5 -> :sswitch_1
        0x21 -> :sswitch_2
        0x22 -> :sswitch_3
        0x23 -> :sswitch_3
        0x24 -> :sswitch_3
        0x25 -> :sswitch_3
    .end sparse-switch
.end method

.method public static convertProvider(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "provider"    # Ljava/lang/String;

    .prologue
    .line 523
    const/4 v0, 0x0

    .line 524
    .local v0, "cp":Ljava/lang/String;
    const-string v1, "network"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 525
    const-string v0, "N"

    .line 531
    :goto_0
    return-object v0

    .line 526
    :cond_0
    const-string v1, "gps"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 527
    const-string v0, "G"

    goto :goto_0

    .line 529
    :cond_1
    const-string v0, "P"

    goto :goto_0
.end method

.method public static convertTemp(IIF)I
    .locals 1
    .param p0, "fromScale"    # I
    .param p1, "toScale"    # I
    .param p2, "value"    # F

    .prologue
    .line 489
    invoke-static {p0, p1, p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTempScale(IIF)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method public static convertTemp(IIFZ)Ljava/lang/String;
    .locals 1
    .param p0, "fromScale"    # I
    .param p1, "toScale"    # I
    .param p2, "value"    # F
    .param p3, "resultFloat"    # Z

    .prologue
    .line 464
    invoke-static {p0, p1, p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTemp(IIF)I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static convertTempScale(IIF)F
    .locals 5
    .param p0, "fromScale"    # I
    .param p1, "toScale"    # I
    .param p2, "value"    # F

    .prologue
    const-wide v3, 0x3ffccccccccccccdL    # 1.8

    .line 496
    const/4 v0, 0x0

    .line 497
    .local v0, "ct":F
    if-ne p0, p1, :cond_2

    .line 498
    move v0, p2

    .line 508
    :cond_0
    :goto_0
    sget-boolean v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isAutoCaptureToolSupportMode:Z

    if-eqz v1, :cond_1

    .line 509
    const/high16 v0, 0x41a00000    # 20.0f

    .line 513
    .end local v0    # "ct":F
    :cond_1
    return v0

    .line 500
    .restart local v0    # "ct":F
    :cond_2
    const/4 v1, 0x1

    if-ne p1, v1, :cond_3

    .line 502
    const/high16 v1, 0x42000000    # 32.0f

    sub-float v1, p2, v1

    float-to-double v1, v1

    div-double/2addr v1, v3

    double-to-float v0, v1

    goto :goto_0

    .line 503
    :cond_3
    if-nez p1, :cond_0

    .line 504
    float-to-double v1, p2

    mul-double/2addr v1, v3

    const-wide/high16 v3, 0x4040000000000000L    # 32.0

    add-double/2addr v1, v3

    double-to-float v0, v1

    goto :goto_0
.end method

.method public static dateFormat(Landroid/content/Context;Ljava/util/Calendar;ZZ)Ljava/lang/String;
    .locals 18
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "cal"    # Ljava/util/Calendar;
    .param p2, "isFullMonth"    # Z
    .param p3, "isFullDay"    # Z

    .prologue
    .line 817
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 818
    .local v1, "dummyCal":Ljava/util/Calendar;
    const/16 v2, 0xd05

    const/16 v3, 0xa

    const/16 v4, 0x16

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v1 .. v7}, Ljava/util/Calendar;->set(IIIIII)V

    .line 819
    const-string v2, ""

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Landroid/text/format/DateFormat;->getDateFormatForSetting(Landroid/content/Context;Ljava/lang/String;)Ljava/text/DateFormat;

    move-result-object v2

    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v15

    .line 820
    .local v15, "regionalDateFormat":Ljava/lang/String;
    const-string v2, "MM-dd-yyyy"

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Landroid/text/format/DateFormat;->getDateFormatForSetting(Landroid/content/Context;Ljava/lang/String;)Ljava/text/DateFormat;

    move-result-object v2

    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v16

    .line 821
    .local v16, "regionalDateFormat2":Ljava/lang/String;
    const-string v14, ""

    .line 822
    .local v14, "month":Ljava/lang/String;
    const-string v9, ""

    .line 824
    .local v9, "date":Ljava/lang/String;
    const/4 v10, 0x0

    .line 825
    .local v10, "dateFormatSwitch":I
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_0
    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v11, v2, :cond_3

    .line 826
    move-object/from16 v0, v16

    invoke-virtual {v0, v11}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x2f

    if-eq v2, v3, :cond_0

    move-object/from16 v0, v16

    invoke-virtual {v0, v11}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x2d

    if-eq v2, v3, :cond_0

    move-object/from16 v0, v16

    invoke-virtual {v0, v11}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x2e

    if-ne v2, v3, :cond_2

    .line 827
    :cond_0
    add-int/lit8 v10, v10, 0x1

    .line 825
    :cond_1
    add-int/lit8 v11, v11, 0x1

    goto :goto_0

    .line 830
    :cond_2
    packed-switch v10, :pswitch_data_0

    .line 838
    :goto_1
    const/4 v2, 0x2

    if-ne v10, v2, :cond_1

    .line 842
    :cond_3
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v17

    .line 843
    .local v17, "res":Landroid/content/res/Resources;
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget-object v2, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v13

    .line 844
    .local v13, "locale":Ljava/lang/String;
    const/4 v8, 0x0

    .line 845
    .local v8, "data":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget-object v2, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-static {v2}, Landroid/text/TextUtils;->getLayoutDirectionFromLocale(Ljava/util/Locale;)I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_7

    const/4 v12, 0x1

    .line 846
    .local v12, "isRTLlanguage":Z
    :goto_2
    sget-boolean v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isLollipopOS:Z

    if-eqz v2, :cond_c

    .line 847
    if-eqz v13, :cond_8

    invoke-virtual {v13}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "ko"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 848
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d010e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 868
    :goto_3
    if-eqz v13, :cond_4

    invoke-virtual {v13}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "sv"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 869
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d00b9

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 875
    :cond_4
    if-eqz v13, :cond_5

    invoke-virtual {v13}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "ko"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_5
    const/4 v2, 0x1

    move/from16 v0, p3

    if-ne v0, v2, :cond_6

    .line 876
    const-string v2, ".*EEE.*"

    invoke-virtual {v8, v2}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_11

    .line 877
    const-string v2, "EEE"

    const-string v3, "EEEE"

    invoke-virtual {v8, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v8

    .line 895
    :cond_6
    :goto_4
    move-object/from16 v0, p1

    invoke-static {v8, v0}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Calendar;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 832
    .end local v8    # "data":Ljava/lang/String;
    .end local v12    # "isRTLlanguage":Z
    .end local v13    # "locale":Ljava/lang/String;
    .end local v17    # "res":Landroid/content/res/Resources;
    :pswitch_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-virtual {v0, v11}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 833
    goto/16 :goto_1

    .line 835
    :pswitch_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-virtual {v0, v11}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    goto/16 :goto_1

    .line 845
    .restart local v8    # "data":Ljava/lang/String;
    .restart local v13    # "locale":Ljava/lang/String;
    .restart local v17    # "res":Landroid/content/res/Resources;
    :cond_7
    const/4 v12, 0x0

    goto/16 :goto_2

    .line 849
    .restart local v12    # "isRTLlanguage":Z
    :cond_8
    invoke-virtual {v15, v14}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v15, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    if-ge v2, v3, :cond_a

    .line 850
    if-eqz v12, :cond_9

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d00b9

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 851
    :goto_5
    goto/16 :goto_3

    :cond_9
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d00b8

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    goto :goto_5

    .line 853
    :cond_a
    if-eqz v12, :cond_b

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d00b8

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 854
    :goto_6
    goto/16 :goto_3

    :cond_b
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d00b9

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    goto :goto_6

    .line 857
    :cond_c
    if-eqz v13, :cond_d

    invoke-virtual {v13}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "ko"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 858
    const-string v2, "full_wday_month_day_no_year"

    const-string v3, "string"

    const-string v4, "android"

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    goto/16 :goto_3

    .line 859
    :cond_d
    invoke-virtual {v15, v14}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v15, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    if-ge v2, v3, :cond_f

    .line 860
    if-eqz v12, :cond_e

    const-string v2, "abbrev_wday_day_abbrev_month_no_year"

    const-string v3, "string"

    const-string v4, "android"

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 861
    :goto_7
    goto/16 :goto_3

    .line 860
    :cond_e
    const-string v2, "abbrev_wday_abbrev_month_day_no_year"

    const-string v3, "string"

    const-string v4, "android"

    .line 861
    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    goto :goto_7

    .line 863
    :cond_f
    if-eqz v12, :cond_10

    const-string v2, "abbrev_wday_abbrev_month_day_no_year"

    const-string v3, "string"

    const-string v4, "android"

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 864
    :goto_8
    goto/16 :goto_3

    .line 863
    :cond_10
    const-string v2, "abbrev_wday_day_abbrev_month_no_year"

    const-string v3, "string"

    const-string v4, "android"

    .line 864
    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    goto :goto_8

    .line 878
    :cond_11
    const-string v2, ".*EE.*"

    invoke-virtual {v8, v2}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_12

    .line 879
    const-string v2, "EE"

    const-string v3, "EEEE"

    invoke-virtual {v8, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v8

    goto/16 :goto_4

    .line 880
    :cond_12
    const-string v2, ".*E.*"

    invoke-virtual {v8, v2}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 881
    const-string v2, "E"

    const-string v3, "EEEE"

    invoke-virtual {v8, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v8

    goto/16 :goto_4

    .line 830
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static debug(Ljava/lang/String;)V
    .locals 1
    .param p0, "msg"    # Ljava/lang/String;

    .prologue
    .line 518
    const-string v0, "WeatherClock"

    invoke-static {v0, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 520
    return-void
.end method

.method public static dpToPx(Landroid/content/res/Resources;I)I
    .locals 3
    .param p0, "res"    # Landroid/content/res/Resources;
    .param p1, "dp"    # I

    .prologue
    .line 3261
    const/4 v0, 0x1

    int-to-float v1, p1

    .line 3262
    invoke-virtual {p0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    .line 3261
    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    return v0
.end method

.method private static findLangString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "loc"    # Ljava/lang/String;

    .prologue
    .line 2454
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->localeArray:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 2455
    sget-object v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->localeArray:[Ljava/lang/String;

    aget-object v2, v2, v0

    const-string v3, "#"

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 2456
    .local v1, "vals":[Ljava/lang/String;
    array-length v2, v1

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    .line 2457
    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2458
    const/4 v2, 0x1

    aget-object v2, v1, v2

    .line 2463
    .end local v1    # "vals":[Ljava/lang/String;
    :goto_1
    return-object v2

    .line 2454
    .restart local v1    # "vals":[Ljava/lang/String;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2463
    .end local v1    # "vals":[Ljava/lang/String;
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public static getBitmapOptions(Landroid/content/Context;I)Landroid/graphics/BitmapFactory$Options;
    .locals 2
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "resId"    # I

    .prologue
    .line 990
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 991
    .local v0, "option":Landroid/graphics/BitmapFactory$Options;
    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 992
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1, p1, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 993
    return-object v0
.end method

.method public static getChangeOrderRowTTS(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "cityName"    # Ljava/lang/String;
    .param p2, "stateName"    # Ljava/lang/String;

    .prologue
    .line 1744
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1746
    .local v0, "sb":Ljava/lang/StringBuilder;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1747
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1748
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0096

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1750
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getCityListTTS(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "scale"    # I
    .param p2, "cityName"    # Ljava/lang/String;
    .param p3, "time"    # Ljava/lang/String;
    .param p4, "date"    # Ljava/lang/String;
    .param p5, "weatherText"    # Ljava/lang/String;
    .param p6, "currentTemp"    # Ljava/lang/String;
    .param p7, "highTemp"    # Ljava/lang/String;
    .param p8, "lowTemp"    # Ljava/lang/String;

    .prologue
    .line 1867
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1869
    .local v0, "sb":Ljava/lang/StringBuilder;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1870
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getOrientation(Landroid/content/Context;)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 1871
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1872
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1873
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p0, p1, p6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getCurrentTempTTS(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1874
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p0, p1, p7, p8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getHighLowTempTTS(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1875
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1884
    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 1877
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1878
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1879
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1880
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p0, p1, p6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getCurrentTempTTS(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1881
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p0, p1, p7, p8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getHighLowTempTTS(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0
.end method

.method public static getConvertTime(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 10
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "time"    # Ljava/lang/String;
    .param p2, "locale"    # Ljava/lang/String;

    .prologue
    .line 2731
    invoke-static {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getHourAndMin(Ljava/lang/String;)[I

    move-result-object v3

    .line 2732
    .local v3, "hourAndMin":[I
    const-string v0, ""

    .line 2734
    .local v0, "AmPm":Ljava/lang/String;
    if-eqz v3, :cond_b

    .line 2735
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 2736
    .local v1, "cal":Ljava/util/Calendar;
    const/16 v7, 0xb

    const/4 v8, 0x0

    aget v8, v3, v8

    invoke-virtual {v1, v7, v8}, Ljava/util/Calendar;->set(II)V

    .line 2737
    const/16 v7, 0xc

    const/4 v8, 0x1

    aget v8, v3, v8

    invoke-virtual {v1, v7, v8}, Ljava/util/Calendar;->set(II)V

    .line 2739
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 2740
    .local v2, "format":Ljava/lang/StringBuffer;
    invoke-static {p0}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 2741
    const/16 v7, 0x68

    invoke-virtual {v2, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 2742
    const/16 v7, 0x68

    invoke-virtual {v2, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 2743
    const-string v7, ":"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 2744
    const/16 v7, 0x6d

    invoke-virtual {v2, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 2745
    const/16 v7, 0x6d

    invoke-virtual {v2, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 2746
    const-string v7, " "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 2755
    :goto_0
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7, v1}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Calendar;)Ljava/lang/CharSequence;

    move-result-object v7

    invoke-interface {v7}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    .line 2757
    .local v5, "timeString":Ljava/lang/String;
    invoke-static {p0}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v7

    if-nez v7, :cond_4

    .line 2758
    const/16 v7, 0x9

    invoke-virtual {v1, v7}, Ljava/util/Calendar;->get(I)I

    move-result v7

    if-nez v7, :cond_3

    .line 2759
    const v7, 0x7f0d00a6

    invoke-virtual {p0, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2764
    :goto_1
    const/4 v7, 0x0

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    if-nez v7, :cond_0

    .line 2765
    const/4 v7, 0x1

    invoke-virtual {v5, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 2771
    :cond_0
    :goto_2
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    .line 2773
    .local v4, "temp":Ljava/lang/StringBuffer;
    if-eqz p2, :cond_5

    invoke-virtual {p2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v7

    const-string v8, "ko"

    invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 2774
    invoke-static {p0}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 2775
    invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 2776
    const-string v7, " "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 2778
    :cond_1
    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 2813
    :goto_3
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    .line 2815
    .end local v1    # "cal":Ljava/util/Calendar;
    .end local v2    # "format":Ljava/lang/StringBuffer;
    .end local v4    # "temp":Ljava/lang/StringBuffer;
    .end local v5    # "timeString":Ljava/lang/String;
    :goto_4
    return-object v7

    .line 2748
    .restart local v1    # "cal":Ljava/util/Calendar;
    .restart local v2    # "format":Ljava/lang/StringBuffer;
    :cond_2
    const/16 v7, 0x6b

    invoke-virtual {v2, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 2749
    const/16 v7, 0x6b

    invoke-virtual {v2, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 2750
    const-string v7, ":"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 2751
    const/16 v7, 0x6d

    invoke-virtual {v2, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 2752
    const/16 v7, 0x6d

    invoke-virtual {v2, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 2761
    .restart local v5    # "timeString":Ljava/lang/String;
    :cond_3
    const v7, 0x7f0d00a7

    invoke-virtual {p0, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 2768
    :cond_4
    const-string v0, ""

    goto :goto_2

    .line 2782
    .restart local v4    # "temp":Ljava/lang/StringBuffer;
    :cond_5
    if-eqz p2, :cond_a

    invoke-virtual {p2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v7

    const-string v8, "ja"

    invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_a

    .line 2784
    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    .line 2785
    .local v6, "timeTemp":Ljava/lang/StringBuffer;
    invoke-virtual {v6, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 2787
    invoke-static {p0}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v7

    if-nez v7, :cond_8

    .line 2788
    const/4 v7, 0x0

    const/4 v8, 0x2

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuffer;->substring(II)Ljava/lang/String;

    move-result-object v7

    const-string v8, "00"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_6

    const/4 v7, 0x0

    const/4 v8, 0x2

    .line 2789
    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuffer;->substring(II)Ljava/lang/String;

    move-result-object v7

    const-string v8, "12"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 2790
    :cond_6
    const/4 v7, 0x0

    const/4 v8, 0x2

    const-string v9, "0"

    invoke-virtual {v6, v7, v8, v9}, Ljava/lang/StringBuffer;->replace(IILjava/lang/String;)Ljava/lang/StringBuffer;

    .line 2798
    :cond_7
    :goto_5
    const/4 v7, 0x0

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    if-nez v7, :cond_9

    .line 2799
    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 2803
    :goto_6
    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 2804
    invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_3

    .line 2793
    :cond_8
    const/4 v7, 0x0

    const/4 v8, 0x2

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuffer;->substring(II)Ljava/lang/String;

    move-result-object v7

    const-string v8, "00"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 2794
    const/4 v7, 0x0

    const/4 v8, 0x2

    const-string v9, "0"

    invoke-virtual {v6, v7, v8, v9}, Ljava/lang/StringBuffer;->replace(IILjava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_5

    .line 2801
    :cond_9
    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_6

    .line 2807
    .end local v6    # "timeTemp":Ljava/lang/StringBuffer;
    :cond_a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 2809
    invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_3

    .line 2815
    .end local v1    # "cal":Ljava/util/Calendar;
    .end local v2    # "format":Ljava/lang/StringBuffer;
    .end local v4    # "temp":Ljava/lang/StringBuffer;
    .end local v5    # "timeString":Ljava/lang/String;
    :cond_b
    const/4 v7, 0x0

    goto/16 :goto_4
.end method

.method public static getConvertTime(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;
    .locals 5
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "time"    # Ljava/lang/String;
    .param p2, "locale"    # Ljava/lang/String;
    .param p3, "forDisplay"    # Z

    .prologue
    .line 2688
    invoke-static {p0, p1, p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getSunRiseAndSunSetByFormat(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2690
    .local v3, "timeTempStr":Ljava/lang/String;
    if-nez v3, :cond_0

    .line 2691
    const/4 v4, 0x0

    .line 2703
    :goto_0
    return-object v4

    .line 2694
    :cond_0
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 2695
    .local v2, "s":Ljava/lang/StringBuffer;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v1, v4, :cond_2

    .line 2696
    invoke-virtual {v3, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 2697
    .local v0, "c":C
    const/16 v4, 0x3a

    if-ne v0, v4, :cond_1

    .line 2698
    const/16 v0, 0x2236

    .line 2700
    :cond_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 2695
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2703
    .end local v0    # "c":C
    :cond_2
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method public static getCurrentTempTTS(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "scale"    # I
    .param p2, "currentTemp"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 1754
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1755
    .local v0, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d009c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1756
    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1766
    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 1767
    .local v1, "temp":I
    if-ne v1, v4, :cond_0

    .line 1768
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d009f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1776
    :goto_0
    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1778
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 1769
    :cond_0
    if-gez v1, :cond_1

    .line 1770
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d00a1

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/Object;

    .line 1771
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    .line 1770
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 1773
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d00a0

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/Object;

    .line 1774
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    .line 1773
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public static getCurrentTime(Landroid/content/Context;Ljava/util/Calendar;Ljava/lang/String;)Ljava/lang/String;
    .locals 11
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "cal"    # Ljava/util/Calendar;
    .param p2, "timeZoneString"    # Ljava/lang/String;

    .prologue
    const/4 v10, 0x1

    const/16 v9, 0x6d

    const/16 v8, 0x9

    const/4 v7, 0x2

    const/4 v6, 0x0

    .line 2820
    invoke-static {p2}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/util/Calendar;->setTimeZone(Ljava/util/TimeZone;)V

    .line 2821
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {p1, v4, v5}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 2822
    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getTimeString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2823
    .local v3, "timeStr":Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 2824
    .local v0, "format":Ljava/lang/StringBuffer;
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget-object v4, v4, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v4}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    .line 2828
    .local v1, "locale":Ljava/lang/String;
    invoke-static {p0}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 2829
    const/16 v4, 0x68

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 2831
    const-string v4, ":"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 2832
    invoke-virtual {v0, v9}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 2833
    invoke-virtual {v0, v9}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 2843
    :goto_0
    invoke-static {p0}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_8

    .line 2844
    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    const-string v5, "ko"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 2845
    invoke-virtual {p1, v8}, Ljava/util/Calendar;->get(I)I

    move-result v4

    if-nez v4, :cond_5

    .line 2846
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const v5, 0x7f0d00a6

    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, p1}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Calendar;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2862
    :cond_0
    :goto_1
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    const-string v5, "ja"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2863
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 2864
    .local v2, "tempTime":Ljava/lang/StringBuffer;
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    .line 2866
    invoke-static {p0}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_9

    .line 2867
    invoke-virtual {v2, v6, v7}, Ljava/lang/StringBuffer;->substring(II)Ljava/lang/String;

    move-result-object v4

    const-string v5, "00"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 2868
    invoke-virtual {v2, v6, v7}, Ljava/lang/StringBuffer;->substring(II)Ljava/lang/String;

    move-result-object v4

    const-string v5, "12"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2869
    :cond_1
    const-string v4, "0"

    invoke-virtual {v2, v6, v7, v4}, Ljava/lang/StringBuffer;->replace(IILjava/lang/String;)Ljava/lang/StringBuffer;

    .line 2877
    :cond_2
    :goto_2
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2878
    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    if-nez v4, :cond_3

    const-string v4, ":"

    .line 2879
    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    if-ne v4, v7, :cond_3

    .line 2880
    invoke-virtual {v3, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 2885
    .end local v2    # "tempTime":Ljava/lang/StringBuffer;
    :cond_3
    return-object v3

    .line 2835
    :cond_4
    const/16 v4, 0x6b

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 2836
    const/16 v4, 0x6b

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 2838
    const-string v4, ":"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 2839
    invoke-virtual {v0, v9}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 2840
    invoke-virtual {v0, v9}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto/16 :goto_0

    .line 2847
    :cond_5
    invoke-virtual {p1, v8}, Ljava/util/Calendar;->get(I)I

    move-result v4

    if-ne v4, v10, :cond_0

    .line 2848
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const v5, 0x7f0d00a7

    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, p1}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Calendar;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_1

    .line 2851
    :cond_6
    invoke-virtual {p1, v8}, Ljava/util/Calendar;->get(I)I

    move-result v4

    if-nez v4, :cond_7

    .line 2852
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, p1}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Calendar;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const v5, 0x7f0d00a6

    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_1

    .line 2853
    :cond_7
    invoke-virtual {p1, v8}, Ljava/util/Calendar;->get(I)I

    move-result v4

    if-ne v4, v10, :cond_0

    .line 2854
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, p1}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Calendar;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const v5, 0x7f0d00a7

    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_1

    .line 2858
    :cond_8
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, p1}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Calendar;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_1

    .line 2872
    .restart local v2    # "tempTime":Ljava/lang/StringBuffer;
    :cond_9
    invoke-virtual {v2, v6, v7}, Ljava/lang/StringBuffer;->substring(II)Ljava/lang/String;

    move-result-object v4

    const-string v5, "00"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2873
    const-string v4, "0"

    invoke-virtual {v2, v6, v7, v4}, Ljava/lang/StringBuffer;->replace(IILjava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_2
.end method

.method public static getCurrent_DATE_FORMAT(Landroid/content/Context;)Ljava/lang/String;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 793
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 794
    .local v5, "result":Ljava/lang/StringBuilder;
    const-string v6, "yyyy"

    .line 795
    .local v6, "year":Ljava/lang/String;
    const-string v4, "MM"

    .line 796
    .local v4, "month":Ljava/lang/String;
    const-string v0, "dd"

    .line 797
    .local v0, "day":Ljava/lang/String;
    const-string v1, "-"

    .line 798
    .local v1, "divider":Ljava/lang/String;
    invoke-static {p0}, Landroid/text/format/DateFormat;->getDateFormatOrder(Landroid/content/Context;)[C

    move-result-object v2

    .line 799
    .local v2, "formatArray":[C
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v7, v2

    if-ge v3, v7, :cond_4

    .line 800
    aget-char v7, v2, v3

    const/16 v8, 0x79

    if-ne v7, v8, :cond_0

    .line 801
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 803
    :cond_0
    aget-char v7, v2, v3

    const/16 v8, 0x4d

    if-ne v7, v8, :cond_1

    .line 804
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 806
    :cond_1
    aget-char v7, v2, v3

    const/16 v8, 0x64

    if-ne v7, v8, :cond_2

    .line 807
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 809
    :cond_2
    array-length v7, v2

    add-int/lit8 v7, v7, -0x1

    if-eq v3, v7, :cond_3

    .line 810
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 799
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 813
    :cond_4
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    return-object v7
.end method

.method public static getDateTimeString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "timestring"    # Ljava/lang/String;

    .prologue
    .line 307
    :try_start_0
    invoke-static {p1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    .line 308
    .local v5, "timestamp":J
    invoke-static {p0}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v1

    .line 315
    .local v1, "dateformat":Ljava/text/DateFormat;
    invoke-static {p0}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v4

    .line 316
    .local v4, "timeformat":Ljava/text/DateFormat;
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, v5, v6}, Ljava/util/Date;-><init>(J)V

    .line 318
    .local v0, "date":Ljava/util/Date;
    invoke-virtual {v4, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    .line 331
    .local v3, "timeTempStr":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    .line 333
    .end local v0    # "date":Ljava/util/Date;
    .end local v1    # "dateformat":Ljava/text/DateFormat;
    .end local v3    # "timeTempStr":Ljava/lang/String;
    .end local v4    # "timeformat":Ljava/text/DateFormat;
    .end local v5    # "timestamp":J
    :goto_0
    return-object v7

    .line 332
    :catch_0
    move-exception v2

    .line 333
    .local v2, "ex":Ljava/lang/Exception;
    const-string v7, ""

    goto :goto_0
.end method

.method public static getDateTimeStringForWidget(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "timestring"    # Ljava/lang/String;

    .prologue
    .line 406
    :try_start_0
    invoke-static {p1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    .line 408
    .local v7, "timestamp":J
    sget-boolean v9, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isAutoCaptureToolSupportMode:Z

    if-eqz v9, :cond_0

    .line 409
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 410
    .local v0, "d":Ljava/util/Date;
    const/16 v9, 0x7de

    invoke-virtual {v0, v9}, Ljava/util/Date;->setYear(I)V

    .line 411
    const/16 v9, 0x18

    invoke-virtual {v0, v9}, Ljava/util/Date;->setDate(I)V

    .line 412
    const/4 v9, 0x1

    invoke-virtual {v0, v9}, Ljava/util/Date;->setMonth(I)V

    .line 413
    const/16 v9, 0xc

    invoke-virtual {v0, v9}, Ljava/util/Date;->setHours(I)V

    .line 414
    const/16 v9, 0x2d

    invoke-virtual {v0, v9}, Ljava/util/Date;->setMinutes(I)V

    .line 416
    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v7

    .line 419
    .end local v0    # "d":Ljava/util/Date;
    :cond_0
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getMonthAndDayFormat(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 423
    .local v2, "dateFormatText":Ljava/lang/String;
    const/4 v3, 0x0

    .line 424
    .local v3, "dateformat":Ljava/text/SimpleDateFormat;
    new-instance v3, Ljava/text/SimpleDateFormat;

    .end local v3    # "dateformat":Ljava/text/SimpleDateFormat;
    invoke-direct {v3, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 426
    .restart local v3    # "dateformat":Ljava/text/SimpleDateFormat;
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1, v7, v8}, Ljava/util/Date;-><init>(J)V

    .line 428
    .local v1, "date":Ljava/util/Date;
    invoke-static {p0}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v6

    .line 429
    .local v6, "timeformat":Ljava/text/DateFormat;
    invoke-virtual {v6, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    .line 442
    .local v5, "timeTempStr":Ljava/lang/String;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v9

    .line 445
    .end local v1    # "date":Ljava/util/Date;
    .end local v2    # "dateFormatText":Ljava/lang/String;
    .end local v3    # "dateformat":Ljava/text/SimpleDateFormat;
    .end local v5    # "timeTempStr":Ljava/lang/String;
    .end local v6    # "timeformat":Ljava/text/DateFormat;
    .end local v7    # "timestamp":J
    :goto_0
    return-object v9

    .line 443
    :catch_0
    move-exception v4

    .line 444
    .local v4, "ex":Ljava/lang/Exception;
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    .line 445
    const-string v9, ""

    goto :goto_0
.end method

.method public static getDateTimeStringTTS(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "timestring"    # Ljava/lang/String;

    .prologue
    .line 339
    const/4 v1, 0x0

    .line 340
    .local v1, "flags":I
    or-int/lit8 v1, v1, 0x4

    .line 341
    or-int/lit8 v1, v1, 0x10

    .line 342
    :try_start_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 344
    .local v2, "str_updated":Ljava/lang/StringBuilder;
    invoke-static {p1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    .line 343
    invoke-static {p0, v3, v4, v1}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 345
    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 346
    invoke-static {p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getTimeString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 347
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 349
    .end local v2    # "str_updated":Ljava/lang/StringBuilder;
    :goto_0
    return-object v3

    .line 348
    :catch_0
    move-exception v0

    .line 349
    .local v0, "ex":Ljava/lang/Exception;
    const-string v3, ""

    goto :goto_0
.end method

.method public static getDayofWeekTTS(Landroid/content/Context;Ljava/util/Calendar;)Ljava/lang/String;
    .locals 3
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "cal"    # Ljava/util/Calendar;

    .prologue
    .line 1701
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1703
    .local v0, "sb":Ljava/lang/StringBuilder;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v2, 0xa

    invoke-static {p0, p1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->makeTodayDate(Landroid/content/Context;Ljava/util/Calendar;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1705
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getDeleteRowTTS(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;
    .locals 4
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "cityName"    # Ljava/lang/String;
    .param p2, "stateName"    # Ljava/lang/String;
    .param p3, "isCheck"    # Z

    .prologue
    .line 1728
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1730
    .local v0, "sb":Ljava/lang/StringBuilder;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1731
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1732
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d0093

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1734
    if-eqz p3, :cond_0

    .line 1735
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0094

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1740
    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 1737
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0095

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public static getEnterOnTap()Z
    .locals 1

    .prologue
    .line 789
    sget-boolean v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->isOnTap:Z

    return v0
.end method

.method public static getFirstLaunch(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 2635
    const-string v2, "config"

    const/4 v3, 0x4

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 2636
    .local v0, "prefs":Landroid/content/SharedPreferences;
    sget-object v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->SHARED_PREF_FIRST:Ljava/lang/String;

    const/4 v3, 0x1

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 2637
    .local v1, "result":Z
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090011

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2638
    const/4 v1, 0x0

    .line 2640
    :cond_0
    return v1
.end method

.method public static getForcastListTTS(Landroid/content/Context;Ljava/util/Calendar;IIIII)Ljava/lang/String;
    .locals 4
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "cal"    # Ljava/util/Calendar;
    .param p2, "iconNum"    # I
    .param p3, "scale"    # I
    .param p4, "highTemp"    # I
    .param p5, "lowTemp"    # I
    .param p6, "probability"    # I

    .prologue
    .line 1954
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1956
    .local v0, "sb":Ljava/lang/StringBuilder;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v2, 0xa

    invoke-static {p0, p1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->makeTodayDate(Landroid/content/Context;Ljava/util/Calendar;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1957
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p0, p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getWeatherText(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1958
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {p5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, p3, v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getHighLowTempTTS(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1959
    const v1, 0x7f0d00ac

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1960
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1962
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getGridCityListTTS(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "scale"    # I
    .param p2, "cityName"    # Ljava/lang/String;
    .param p3, "cityState"    # Ljava/lang/String;
    .param p4, "time"    # Ljava/lang/String;
    .param p5, "currentTemp"    # Ljava/lang/String;
    .param p6, "weatherText"    # Ljava/lang/String;

    .prologue
    .line 1929
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1931
    .local v0, "sb":Ljava/lang/StringBuilder;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1932
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1933
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1934
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p0, p1, p5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getCurrentTempTTS(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1935
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1937
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getGridCityListTTS(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "scale"    # I
    .param p2, "cityName"    # Ljava/lang/String;
    .param p3, "cityState"    # Ljava/lang/String;
    .param p4, "time"    # Ljava/lang/String;
    .param p5, "currentTemp"    # Ljava/lang/String;
    .param p6, "highTemp"    # Ljava/lang/String;
    .param p7, "lowTemp"    # Ljava/lang/String;
    .param p8, "weatherText"    # Ljava/lang/String;

    .prologue
    .line 1903
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1905
    .local v0, "sb":Ljava/lang/StringBuilder;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1906
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1907
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1908
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p0, p1, p5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getCurrentTempTTS(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1909
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p0, p1, p6, p7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getHighLowTempTTS(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1910
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1912
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getHighLowTempTTS(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 11
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "scale"    # I
    .param p2, "highTemp"    # Ljava/lang/String;
    .param p3, "lowTemp"    # Ljava/lang/String;

    .prologue
    const v10, 0x7f0d00a1

    const v9, 0x7f0d00a0

    const v8, 0x7f0d009f

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 1782
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1784
    .local v0, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0d009d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1785
    const-string v3, ", "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1795
    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 1796
    .local v1, "tempHigh":I
    if-ne v1, v6, :cond_0

    .line 1797
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1805
    :goto_0
    const-string v3, ", "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1807
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0d009e

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1808
    const-string v3, ", "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1819
    invoke-static {p3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 1820
    .local v2, "tempLow":I
    if-ne v2, v6, :cond_2

    .line 1821
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1829
    :goto_1
    const-string v3, ", "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1831
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 1798
    .end local v2    # "tempLow":I
    :cond_0
    if-gez v1, :cond_1

    .line 1799
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v6, [Ljava/lang/Object;

    .line 1800
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    .line 1799
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 1802
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v6, [Ljava/lang/Object;

    .line 1803
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    .line 1802
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 1822
    .restart local v2    # "tempLow":I
    :cond_2
    if-gez v2, :cond_3

    .line 1823
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v6, [Ljava/lang/Object;

    .line 1824
    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    .line 1823
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 1826
    :cond_3
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v6, [Ljava/lang/Object;

    .line 1827
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    .line 1826
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method public static getHourAndMin(Ljava/lang/String;)[I
    .locals 8
    .param p0, "time"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2379
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v6

    if-nez v6, :cond_1

    .line 2380
    :cond_0
    const-string v6, ""

    const-string v7, "getHourAndMin : time is null"

    invoke-static {v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2413
    :goto_0
    return-object v5

    .line 2383
    :cond_1
    const-string v6, ":"

    invoke-virtual {p0, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 2384
    .local v0, "colonIndex":I
    const-string v6, " "

    invoke-virtual {p0, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    .line 2385
    .local v4, "spaceIndex":I
    const/4 v2, 0x0

    .line 2386
    .local v2, "hour":I
    const/4 v3, 0x0

    .line 2388
    .local v3, "min":I
    const/4 v6, -0x1

    if-le v0, v6, :cond_5

    .line 2390
    const/4 v6, 0x0

    :try_start_0
    invoke-virtual {p0, v6, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 2391
    const-string v6, "pm"

    invoke-virtual {p0, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    const-string v6, "PM"

    invoke-virtual {p0, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 2392
    :cond_2
    add-int/lit8 v2, v2, 0xc

    .line 2395
    :cond_3
    if-le v4, v0, :cond_4

    .line 2396
    add-int/lit8 v6, v0, 0x1

    invoke-virtual {p0, v6, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    .line 2405
    :goto_1
    const/4 v6, 0x2

    new-array v5, v6, [I

    .line 2406
    .local v5, "timeInt":[I
    aput v2, v5, v7

    .line 2407
    const/4 v6, 0x1

    aput v3, v5, v6

    goto :goto_0

    .line 2398
    .end local v5    # "timeInt":[I
    :cond_4
    add-int/lit8 v6, v0, 0x1

    :try_start_1
    invoke-virtual {p0, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v3

    goto :goto_1

    .line 2400
    :catch_0
    move-exception v1

    .line 2401
    .local v1, "e":Ljava/lang/NumberFormatException;
    invoke-virtual {v1}, Ljava/lang/NumberFormatException;->printStackTrace()V

    goto :goto_0

    .line 2412
    .end local v1    # "e":Ljava/lang/NumberFormatException;
    :cond_5
    const-string v6, ""

    const-string v7, "getHourAndMin : time doesn\'t have a colon"

    invoke-static {v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static getIntentForWidget(Landroid/content/Context;Ljava/lang/String;III)Landroid/content/Intent;
    .locals 6
    .param p0, "mContext"    # Landroid/content/Context;
    .param p1, "location"    # Ljava/lang/String;
    .param p2, "widgetMode"    # I
    .param p3, "AutoRefreshTime"    # I
    .param p4, "size"    # I

    .prologue
    .line 3900
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-static/range {v0 .. v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getIntentForWidget(Landroid/content/Context;Ljava/lang/String;IIIZ)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static getIntentForWidget(Landroid/content/Context;Ljava/lang/String;IIIZ)Landroid/content/Intent;
    .locals 8
    .param p0, "mContext"    # Landroid/content/Context;
    .param p1, "location"    # Ljava/lang/String;
    .param p2, "widgetMode"    # I
    .param p3, "AutoRefreshTime"    # I
    .param p4, "size"    # I
    .param p5, "isShowCityList"    # Z

    .prologue
    .line 3906
    const/4 v1, 0x0

    .line 3908
    .local v1, "intent":Landroid/content/Intent;
    const-string v3, "com.samsung.sec.android.widgetapp.intent.action.MENU_DETAIL_GL"

    .line 3910
    .local v3, "strShowActivityActionName":Ljava/lang/String;
    if-eqz p5, :cond_0

    .line 3911
    const-string v3, "com.samsung.sec.android.widgetapp.intent.action.MENU_CITYLIST_GRID_DELETE"

    .line 3914
    :cond_0
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->isShowChargerPopup(Landroid/content/Context;)Z

    move-result v2

    .line 3916
    .local v2, "isShowCharger":Z
    const-string v4, ""

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getIntentForWidget : check : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 3918
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f090005

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " charger : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " wifi : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 3919
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->isWifiOnly(Landroid/content/Context;)Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " result :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " Activity :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 3916
    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3923
    if-eqz p3, :cond_3

    .line 3924
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090005

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v4

    if-nez v4, :cond_3

    if-eqz v2, :cond_3

    .line 3925
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->isWifiOnly(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_3

    sget-boolean v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isVerizon:Z

    if-nez v4, :cond_3

    .line 3927
    const-string v4, ""

    const-string v5, "startActivity : account dialog"

    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3929
    if-lez p4, :cond_1

    .line 3930
    new-instance v1, Landroid/content/Intent;

    .end local v1    # "intent":Landroid/content/Intent;
    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 3931
    .restart local v1    # "intent":Landroid/content/Intent;
    const-string v4, "flags"

    const/16 v5, -0x7530

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 3943
    :goto_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 3944
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v4, "ACCOUNTING_DIALOG_FROM"

    const-string v5, "FROM_TOUCH_WIDGET"

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 3945
    const-string v4, "AUTOREFRESH_SETTING"

    invoke-virtual {v0, v4, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 3946
    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 3948
    const-string v4, "widget_mode"

    invoke-virtual {v1, v4, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 3949
    const-string v4, "searchlocation"

    invoke-virtual {v1, v4, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3951
    const-class v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;

    invoke-virtual {v1, p0, v4}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 3952
    const/high16 v4, 0x10000000

    invoke-virtual {v1, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .end local v0    # "bundle":Landroid/os/Bundle;
    :goto_1
    move-object v4, v1

    .line 3982
    :goto_2
    return-object v4

    .line 3934
    :cond_1
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->isNetWorkConnected(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 3935
    const v4, 0x7f0d0020

    invoke-static {p0, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->toast(Landroid/content/Context;I)V

    .line 3936
    const/4 v4, 0x0

    goto :goto_2

    .line 3939
    :cond_2
    new-instance v1, Landroid/content/Intent;

    .end local v1    # "intent":Landroid/content/Intent;
    const-string v4, "com.samsung.sec.android.widgetapp.intent.action.MENU_ADD"

    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 3940
    .restart local v1    # "intent":Landroid/content/Intent;
    const-string v4, "flags"

    const/16 v5, 0x7cf

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0

    .line 3955
    :cond_3
    if-nez p3, :cond_4

    if-eqz v2, :cond_4

    .line 3956
    const-string v4, ""

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "GL_TOUCH_WIDGET : r = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", isShow = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3957
    const/4 v4, 0x0

    invoke-static {p0, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->setShowChargerPopup(Landroid/content/Context;Z)V

    .line 3960
    :cond_4
    if-lez p4, :cond_5

    .line 3961
    const-string v4, ""

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "startActivity : details : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3963
    new-instance v1, Landroid/content/Intent;

    .end local v1    # "intent":Landroid/content/Intent;
    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 3964
    .restart local v1    # "intent":Landroid/content/Intent;
    const-string v4, "flags"

    const/16 v5, -0x7530

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 3965
    const-string v4, "widget_mode"

    invoke-virtual {v1, v4, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 3966
    const-string v4, "searchlocation"

    invoke-virtual {v1, v4, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3967
    const-string v4, "where_from_to_details"

    const/16 v5, 0x101

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 3979
    :goto_3
    const v4, 0x10008000

    invoke-virtual {v1, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    goto/16 :goto_1

    .line 3969
    :cond_5
    const-string v4, ""

    const-string v5, "startActivity : add "

    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3970
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090011

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v4

    if-nez v4, :cond_6

    .line 3971
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->isNetWorkConnected(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_6

    .line 3972
    const v4, 0x7f0d0020

    invoke-static {p0, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->toast(Landroid/content/Context;I)V

    .line 3973
    const/4 v4, 0x0

    goto/16 :goto_2

    .line 3976
    :cond_6
    new-instance v1, Landroid/content/Intent;

    .end local v1    # "intent":Landroid/content/Intent;
    const-string v4, "com.samsung.sec.android.widgetapp.intent.action.MENU_ADD"

    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 3977
    .restart local v1    # "intent":Landroid/content/Intent;
    const-string v4, "flags"

    const/16 v5, 0x7cf

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_3
.end method

.method public static getInterval(I)J
    .locals 2
    .param p0, "refreshsetting"    # I

    .prologue
    .line 224
    packed-switch p0, :pswitch_data_0

    .line 236
    const-wide/16 v0, -0x1

    :goto_0
    return-wide v0

    .line 226
    :pswitch_0
    const-wide/32 v0, 0x36ee80

    goto :goto_0

    .line 228
    :pswitch_1
    const-wide/32 v0, 0xa4cb80

    goto :goto_0

    .line 230
    :pswitch_2
    const-wide/32 v0, 0x1499700

    goto :goto_0

    .line 232
    :pswitch_3
    const-wide/32 v0, 0x2932e00

    goto :goto_0

    .line 234
    :pswitch_4
    const-wide/32 v0, 0x5265c00

    goto :goto_0

    .line 224
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static getLanguageString(Landroid/content/Context;)Ljava/lang/String;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 2440
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget-object v2, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v2}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2442
    .local v0, "localeString":Ljava/lang/String;
    invoke-static {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->findLangString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2443
    .local v1, "result":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 2444
    const/4 v2, 0x0

    const/4 v3, 0x2

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->findLangString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2445
    if-nez v1, :cond_0

    .line 2446
    const-string v2, "en-us"

    .line 2450
    :goto_0
    return-object v2

    :cond_0
    move-object v2, v1

    goto :goto_0
.end method

.method public static getLocationCheck(Landroid/content/Context;)I
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 2060
    const/16 v3, 0xbb8

    .line 2065
    .local v3, "rtn":I
    sget-boolean v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isKitKatOS:Z

    if-eqz v4, :cond_5

    .line 2066
    const/4 v2, 0x0

    .line 2068
    .local v2, "locationmode":I
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "location_mode"

    invoke-static {v4, v5}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 2074
    :goto_0
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->isNetWorkConnected(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 2075
    const/16 v3, 0xbb9

    .line 2122
    .end local v2    # "locationmode":I
    :cond_0
    :goto_1
    return v3

    .line 2070
    .restart local v2    # "locationmode":I
    :catch_0
    move-exception v0

    .line 2071
    .local v0, "e":Landroid/provider/Settings$SettingNotFoundException;
    const-string v4, ""

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "location setting not find : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2072
    invoke-virtual {v0}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 2076
    .end local v0    # "e":Landroid/provider/Settings$SettingNotFoundException;
    :cond_1
    sget-boolean v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isVerizon:Z

    if-eqz v4, :cond_2

    .line 2077
    const/4 v4, 0x3

    if-eq v2, v4, :cond_0

    .line 2078
    const/16 v3, 0xbbb

    goto :goto_1

    .line 2081
    :cond_2
    const/4 v4, 0x1

    if-eq v2, v4, :cond_3

    const/4 v4, 0x2

    if-ne v2, v4, :cond_4

    .line 2083
    :cond_3
    const/16 v3, 0xbc0

    goto :goto_1

    .line 2084
    :cond_4
    if-nez v2, :cond_0

    .line 2085
    const/16 v3, 0xbbb

    goto :goto_1

    .line 2093
    .end local v2    # "locationmode":I
    :cond_5
    const-string v4, "location"

    .line 2094
    invoke-virtual {p0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/location/LocationManager;

    .line 2095
    .local v1, "locationManager":Landroid/location/LocationManager;
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->isNetWorkConnected(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_6

    .line 2096
    const/16 v3, 0xbb9

    goto :goto_1

    .line 2097
    :cond_6
    sget-boolean v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isVerizon:Z

    if-eqz v4, :cond_8

    .line 2098
    const-string v4, "network"

    .line 2099
    invoke-virtual {v1, v4}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_7

    const-string v4, "gps"

    .line 2101
    invoke-virtual {v1, v4}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 2103
    :cond_7
    const/16 v3, 0xbbb

    goto :goto_1

    .line 2106
    :cond_8
    const-string v4, "network"

    .line 2107
    invoke-virtual {v1, v4}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_9

    const-string v4, "gps"

    .line 2109
    invoke-virtual {v1, v4}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 2111
    const/16 v3, 0xbc0

    goto :goto_1

    .line 2112
    :cond_9
    const-string v4, "network"

    .line 2113
    invoke-virtual {v1, v4}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "gps"

    .line 2115
    invoke-virtual {v1, v4}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 2117
    const/16 v3, 0xbbb

    goto/16 :goto_1
.end method

.method public static getLocationCheck(Landroid/content/Context;ZZ)I
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "firstUpdate"    # Z
    .param p2, "networkOnly"    # Z

    .prologue
    const/16 v1, 0xbbb

    .line 2150
    const-string v2, "location"

    .line 2151
    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    .line 2152
    .local v0, "locationManager":Landroid/location/LocationManager;
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->isNetWorkConnected(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2153
    const/16 v1, 0xbb9

    .line 2167
    :cond_0
    :goto_0
    return v1

    .line 2154
    :cond_1
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->isFactoryMode(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2155
    const-string v2, ""

    const-string v3, "[UTIL] gLCheck : factory mode on"

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2156
    if-eqz p1, :cond_2

    .line 2157
    const/16 v1, 0xbc1

    goto :goto_0

    .line 2160
    :cond_2
    if-nez p2, :cond_3

    const-string v2, "network"

    invoke-virtual {v0, v2}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string v2, "gps"

    .line 2161
    invoke-virtual {v0, v2}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2163
    :cond_3
    if-eqz p2, :cond_4

    const-string v2, "network"

    .line 2164
    invoke-virtual {v0, v2}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2167
    :cond_4
    const/16 v1, 0xbb8

    goto :goto_0
.end method

.method public static getLocationservice(Landroid/content/Context;)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 2172
    const/16 v1, 0xbbb

    .line 2173
    .local v1, "rtn":I
    const-string v2, "location"

    .line 2174
    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    .line 2176
    .local v0, "locationManager":Landroid/location/LocationManager;
    sget-boolean v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isVerizon:Z

    if-eqz v2, :cond_1

    .line 2177
    const-string v2, "network"

    invoke-virtual {v0, v2}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "gps"

    .line 2178
    invoke-virtual {v0, v2}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2179
    const/16 v1, 0xbbe

    .line 2187
    :cond_0
    :goto_0
    return v1

    .line 2182
    :cond_1
    const-string v2, "network"

    invoke-virtual {v0, v2}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "gps"

    .line 2183
    invoke-virtual {v0, v2}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2184
    :cond_2
    const/16 v1, 0xbbe

    goto :goto_0
.end method

.method public static getMaxTemp([I)I
    .locals 3
    .param p0, "values"    # [I

    .prologue
    .line 3103
    const/4 v1, 0x0

    .line 3104
    .local v1, "result":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p0

    if-ge v0, v2, :cond_1

    .line 3105
    aget v2, p0, v0

    if-ge v1, v2, :cond_0

    .line 3106
    aget v1, p0, v0

    .line 3104
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3110
    :cond_1
    return v1
.end method

.method public static getMinTemp([I)I
    .locals 3
    .param p0, "values"    # [I

    .prologue
    .line 3114
    const/16 v1, 0x63

    .line 3115
    .local v1, "result":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p0

    if-ge v0, v2, :cond_1

    .line 3116
    aget v2, p0, v0

    if-le v1, v2, :cond_0

    .line 3117
    aget v1, p0, v0

    .line 3115
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3120
    :cond_1
    return v1
.end method

.method public static getMonthAndDayFormat(Landroid/content/Context;)Ljava/lang/String;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v7, 0x0

    .line 354
    invoke-static {p0}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v3

    check-cast v3, Ljava/text/SimpleDateFormat;

    .line 355
    .local v3, "systemFormat":Ljava/text/SimpleDateFormat;
    invoke-virtual {v3}, Ljava/text/SimpleDateFormat;->toPattern()Ljava/lang/String;

    move-result-object v4

    .line 361
    .local v4, "systemFormatText":Ljava/lang/String;
    const/16 v6, 0x79

    invoke-virtual {v4, v6}, Ljava/lang/String;->indexOf(I)I

    move-result v5

    .line 362
    .local v5, "yearPos":I
    const/16 v6, 0x4d

    invoke-virtual {v4, v6}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    .line 363
    .local v2, "monthPos":I
    const/16 v6, 0x64

    invoke-virtual {v4, v6}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 370
    .local v1, "dayPos":I
    move-object v0, v4

    .line 371
    .local v0, "dateFormatText":Ljava/lang/String;
    if-ltz v5, :cond_0

    .line 372
    if-nez v5, :cond_4

    .line 373
    if-le v2, v1, :cond_3

    .line 374
    invoke-virtual {v4, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 392
    :cond_0
    :goto_0
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v6

    const-string v7, "fi"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 393
    if-le v2, v1, :cond_1

    .line 394
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v6

    const-string v7, "ddMM"

    invoke-static {v6, v7}, Landroid/text/format/DateFormat;->getBestDateTimePattern(Ljava/util/Locale;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 396
    :cond_1
    const-string v6, "dd"

    const-string v7, "d"

    invoke-virtual {v0, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "MM"

    const-string v8, "M"

    invoke-virtual {v6, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 401
    :cond_2
    return-object v0

    .line 376
    :cond_3
    invoke-virtual {v4, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 378
    :cond_4
    if-le v5, v2, :cond_5

    if-le v5, v1, :cond_5

    .line 379
    add-int/lit8 v6, v5, -0x1

    invoke-virtual {v4, v7, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 381
    :cond_5
    if-le v2, v1, :cond_6

    .line 382
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v7, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 383
    invoke-virtual {v4, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 385
    :cond_6
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v7, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 386
    invoke-virtual {v4, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static getMyCityListRowTTS(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZZ)Ljava/lang/String;
    .locals 4
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "cityName"    # Ljava/lang/String;
    .param p2, "stateName"    # Ljava/lang/String;
    .param p3, "hasCheckBox"    # Z
    .param p4, "isCheck"    # Z

    .prologue
    .line 1710
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1712
    .local v0, "sb":Ljava/lang/StringBuilder;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1713
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1715
    if-eqz p3, :cond_0

    .line 1716
    if-eqz p4, :cond_1

    .line 1717
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d0094

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1723
    :cond_0
    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 1719
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d0093

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public static getNextTime(Landroid/content/Context;ZZ)J
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "forceSet"    # Z
    .param p2, "need_refresh"    # Z

    .prologue
    const/4 v10, 0x1

    .line 240
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getAutoRefreshTime(Landroid/content/Context;)I

    move-result v0

    .line 241
    .local v0, "interval":I
    if-nez v0, :cond_0

    .line 242
    const-wide/16 v3, 0x0

    .line 280
    :goto_0
    return-wide v3

    .line 244
    :cond_0
    new-instance v8, Ljava/text/SimpleDateFormat;

    const-string v9, "yy/MM/dd HH:mm:ss"

    invoke-direct {v8, v9}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    sput-object v8, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->sdf:Ljava/text/SimpleDateFormat;

    .line 245
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getNextRefreshTime(Landroid/content/Context;)J

    move-result-wide v1

    .line 246
    .local v1, "lasttime":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    .line 247
    .local v5, "now":J
    const-wide/16 v3, 0x0

    .line 249
    .local v3, "next":J
    if-eqz p1, :cond_2

    .line 250
    new-instance v7, Landroid/text/format/Time;

    invoke-direct {v7}, Landroid/text/format/Time;-><init>()V

    .line 251
    .local v7, "tmp":Landroid/text/format/Time;
    invoke-virtual {v7, v5, v6}, Landroid/text/format/Time;->set(J)V

    .line 252
    const/4 v8, 0x0

    iput v8, v7, Landroid/text/format/Time;->second:I

    .line 253
    invoke-virtual {v7, v10}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v8

    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getInterval(I)J

    move-result-wide v10

    add-long v3, v8, v10

    .line 274
    .end local v7    # "tmp":Landroid/text/format/Time;
    :cond_1
    :goto_1
    if-eqz p1, :cond_4

    .line 275
    const-string v8, "@@@"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "config "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 255
    :cond_2
    if-ne p2, v10, :cond_3

    .line 256
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getInterval(I)J

    move-result-wide v8

    add-long v3, v5, v8

    goto :goto_1

    .line 258
    :cond_3
    move-wide v3, v1

    .line 260
    :goto_2
    cmp-long v8, v3, v5

    if-gtz v8, :cond_1

    .line 261
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getInterval(I)J

    move-result-wide v8

    add-long/2addr v3, v8

    goto :goto_2

    .line 277
    :cond_4
    const-string v8, "@@@"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "widget "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static getOrientation(Landroid/content/Context;)I
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 977
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v1

    .line 978
    .local v1, "r":Landroid/content/res/Resources;
    const/4 v0, 0x0

    .line 979
    .local v0, "config":Landroid/content/res/Configuration;
    if-eqz v1, :cond_0

    .line 980
    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 983
    :cond_0
    if-eqz v0, :cond_1

    iget v2, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    .line 984
    const/4 v2, 0x0

    .line 986
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public static getOtherRefreshOKFromPref(Landroid/content/Context;)Ljava/lang/Boolean;
    .locals 6
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    .line 3224
    const-string v1, "updatetime"

    const/4 v2, 0x4

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 3225
    .local v0, "prefs":Landroid/content/SharedPreferences;
    const-string v1, ""

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "get OUT pref:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "fail_updatetime"

    const-wide/16 v4, 0x0

    invoke-interface {v0, v3, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3226
    const-string v1, "other_refresh"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    return-object v1
.end method

.method public static getPackagename(Z)Ljava/lang/String;
    .locals 5
    .param p0, "needRefresh"    # Z

    .prologue
    .line 3169
    const-string v2, "com.sec.android.widgetapp.ap.hero.accuweather"

    .line 3171
    .local v2, "mPackagename":Ljava/lang/String;
    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    .line 3172
    .local v1, "end":I
    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-static {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->replaceModelName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 3174
    .local v0, "deviceId":Ljava/lang/String;
    if-eqz p0, :cond_0

    .line 3175
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Auto "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    add-int/lit8 v4, v1, 0x1

    invoke-virtual {v2, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 3179
    :goto_0
    return-object v2

    .line 3177
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Manual "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    add-int/lit8 v4, v1, 0x1

    invoke-virtual {v2, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public static getPixelFromDP(Landroid/content/Context;F)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "dp"    # F

    .prologue
    .line 3249
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v0, v1, Landroid/util/DisplayMetrics;->density:F

    .line 3250
    .local v0, "scale":F
    mul-float v1, p1, v0

    const/high16 v2, 0x3f000000    # 0.5f

    add-float/2addr v1, v2

    float-to-int v1, v1

    return v1
.end method

.method public static getPixelFromDP(Landroid/content/Context;I)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "dp"    # I

    .prologue
    .line 3237
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v0, v1, Landroid/util/DisplayMetrics;->density:F

    .line 3238
    .local v0, "scale":F
    int-to-float v1, p1

    mul-float/2addr v1, v0

    const/high16 v2, 0x3f000000    # 0.5f

    add-float/2addr v1, v2

    float-to-int v1, v1

    return v1
.end method

.method public static getPrecipitaionStringLayout(Landroid/content/Context;Landroid/widget/TextView;)I
    .locals 5
    .param p0, "mCtx"    # Landroid/content/Context;
    .param p1, "tv"    # Landroid/widget/TextView;

    .prologue
    .line 3071
    const/4 v2, 0x1

    .line 3072
    .local v2, "layouttype":I
    invoke-virtual {p1}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v1

    .line 3073
    .local v1, "layout":Landroid/text/Layout;
    if-eqz v1, :cond_0

    .line 3074
    invoke-virtual {v1}, Landroid/text/Layout;->getLineCount()I

    move-result v3

    .line 3075
    .local v3, "lines":I
    if-lez v3, :cond_0

    .line 3076
    add-int/lit8 v4, v3, -0x1

    invoke-virtual {v1, v4}, Landroid/text/Layout;->getEllipsisCount(I)I

    move-result v0

    .line 3077
    .local v0, "ellipsisCount":I
    if-lez v0, :cond_0

    .line 3078
    const/4 v2, 0x2

    .line 3082
    .end local v0    # "ellipsisCount":I
    .end local v3    # "lines":I
    :cond_0
    return v2
.end method

.method public static getPreferenceGPSOnlyDoNotShow(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 2644
    const-string v1, "config"

    const/4 v2, 0x4

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 2645
    .local v0, "prefs":Landroid/content/SharedPreferences;
    const-string v1, "CHECK_GPS_ONLY_DONOT_SHOW"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method

.method public static getPreferenceWidgetCount(Landroid/content/Context;)I
    .locals 6
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x0

    .line 4047
    const/4 v2, 0x0

    .local v2, "surfaceCnt":I
    const/4 v1, 0x0

    .line 4049
    .local v1, "remoteCnt":I
    const-string v3, "pref_widget_count"

    const/4 v4, 0x4

    invoke-virtual {p0, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 4050
    .local v0, "pref":Landroid/content/SharedPreferences;
    const-string v3, "widget_count_surface"

    invoke-interface {v0, v3, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 4051
    const-string v3, "widget_count_remote"

    invoke-interface {v0, v3, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 4053
    const-string v3, ""

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getPWC : surface = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", remote = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4055
    add-int v3, v2, v1

    return v3
.end method

.method public static getProductName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 3987
    :try_start_0
    sget-object v1, Landroid/os/Build;->PRODUCT:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 3990
    .local v0, "e":Ljava/lang/Exception;
    :goto_0
    return-object v1

    .line 3988
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_0
    move-exception v0

    .line 3989
    .restart local v0    # "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 3990
    const-string v1, " "

    goto :goto_0
.end method

.method public static getRealfeelTempTTS(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "scale"    # I
    .param p2, "realfeelTemp"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 1835
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1837
    .local v0, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d00a2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1838
    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1850
    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 1851
    .local v1, "temp":I
    if-ne v1, v4, :cond_0

    .line 1852
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d009f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1860
    :goto_0
    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1862
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 1853
    :cond_0
    if-gez v1, :cond_1

    .line 1854
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d00a1

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/Object;

    .line 1855
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    .line 1854
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 1857
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d00a0

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/Object;

    .line 1858
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    .line 1857
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public static getSettingDataRoaming(Landroid/content/Context;)I
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 3620
    if-nez p0, :cond_0

    .line 3621
    const-string v3, ""

    const-string v4, "gSDR c n"

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3645
    :goto_0
    return v2

    .line 3625
    :cond_0
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->simEnabled(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 3626
    const-string v3, ""

    const-string v4, "gSDR sim not enable"

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 3630
    :cond_1
    const/4 v2, 0x0

    .line 3631
    .local v2, "result":I
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 3633
    .local v0, "cr":Landroid/content/ContentResolver;
    if-eqz v0, :cond_2

    .line 3635
    :try_start_0
    const-string v3, "data_roaming"

    invoke-static {v0, v3}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 3643
    :goto_1
    const-string v3, ""

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "gSDR : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 3636
    :catch_0
    move-exception v1

    .line 3637
    .local v1, "e":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual {v1}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    goto :goto_1

    .line 3640
    .end local v1    # "e":Landroid/provider/Settings$SettingNotFoundException;
    :cond_2
    const-string v3, ""

    const-string v4, "gSDR cr n"

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private static getShadowPercent(I)F
    .locals 2
    .param p0, "Opacity"    # I

    .prologue
    .line 3470
    int-to-float v0, p0

    const v1, 0x3c23d70a    # 0.01f

    mul-float/2addr v0, v1

    return v0
.end method

.method public static getSunRiseAndSunSetByFormat(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "time"    # Ljava/lang/String;
    .param p2, "locale"    # Ljava/lang/String;

    .prologue
    .line 2714
    invoke-static {p0, p1, p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getConvertTime(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2716
    .local v0, "retTime":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 2717
    const-string v0, "--"

    .line 2719
    :cond_0
    return-object v0
.end method

.method public static getSystemResolution(Landroid/content/Context;)I
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 122
    const/4 v0, -0x1

    .line 124
    .local v0, "resolution":I
    :try_start_0
    const-string v2, "window"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/WindowManager;

    .line 125
    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    .line 127
    .local v1, "sysDisplay":Landroid/view/Display;
    if-eqz v1, :cond_0

    .line 128
    invoke-virtual {v1}, Landroid/view/Display;->getHeight()I

    move-result v2

    invoke-virtual {v1}, Landroid/view/Display;->getWidth()I

    move-result v3

    mul-int/2addr v2, v3

    const v3, 0x5dc00

    if-ne v2, v3, :cond_1

    .line 129
    const v0, 0x5dc00

    .line 160
    .end local v1    # "sysDisplay":Landroid/view/Display;
    :cond_0
    :goto_0
    return v0

    .line 130
    .restart local v1    # "sysDisplay":Landroid/view/Display;
    :cond_1
    invoke-virtual {v1}, Landroid/view/Display;->getHeight()I

    move-result v2

    invoke-virtual {v1}, Landroid/view/Display;->getWidth()I

    move-result v3

    mul-int/2addr v2, v3

    const v3, 0x25800

    if-ne v2, v3, :cond_2

    .line 131
    const v0, 0x25800

    goto :goto_0

    .line 132
    :cond_2
    invoke-virtual {v1}, Landroid/view/Display;->getHeight()I

    move-result v2

    invoke-virtual {v1}, Landroid/view/Display;->getWidth()I

    move-result v3

    mul-int/2addr v2, v3

    const v3, 0xfa000

    if-ne v2, v3, :cond_3

    .line 133
    const v0, 0xfa000

    goto :goto_0

    .line 134
    :cond_3
    invoke-virtual {v1}, Landroid/view/Display;->getHeight()I

    move-result v2

    invoke-virtual {v1}, Landroid/view/Display;->getWidth()I

    move-result v3

    mul-int/2addr v2, v3

    const v3, 0x12c00

    if-ne v2, v3, :cond_4

    .line 135
    const v0, 0x12c00

    goto :goto_0

    .line 136
    :cond_4
    invoke-virtual {v1}, Landroid/view/Display;->getHeight()I

    move-result v2

    invoke-virtual {v1}, Landroid/view/Display;->getWidth()I

    move-result v3

    mul-int/2addr v2, v3

    const v3, 0xe1000

    if-ne v2, v3, :cond_5

    .line 137
    const v0, 0xe1000

    goto :goto_0

    .line 138
    :cond_5
    invoke-virtual {v1}, Landroid/view/Display;->getHeight()I

    move-result v2

    invoke-virtual {v1}, Landroid/view/Display;->getWidth()I

    move-result v3

    mul-int/2addr v2, v3

    const v3, 0xd23c0

    if-ne v2, v3, :cond_6

    .line 139
    const v0, 0xd23c0

    goto :goto_0

    .line 140
    :cond_6
    invoke-virtual {v1}, Landroid/view/Display;->getHeight()I

    move-result v2

    invoke-virtual {v1}, Landroid/view/Display;->getWidth()I

    move-result v3

    mul-int/2addr v2, v3

    const v3, 0xd0200

    if-ne v2, v3, :cond_7

    .line 141
    const v0, 0xd0200

    goto :goto_0

    .line 142
    :cond_7
    invoke-virtual {v1}, Landroid/view/Display;->getHeight()I

    move-result v2

    invoke-virtual {v1}, Landroid/view/Display;->getWidth()I

    move-result v3

    mul-int/2addr v2, v3

    const v3, 0x1fa400

    if-ne v2, v3, :cond_8

    .line 143
    const v0, 0x1fa400

    goto :goto_0

    .line 144
    :cond_8
    invoke-virtual {v1}, Landroid/view/Display;->getHeight()I

    move-result v2

    invoke-virtual {v1}, Landroid/view/Display;->getWidth()I

    move-result v3

    mul-int/2addr v2, v3

    const v3, 0x7e900

    if-eq v2, v3, :cond_9

    .line 145
    invoke-virtual {v1}, Landroid/view/Display;->getHeight()I

    move-result v2

    invoke-virtual {v1}, Landroid/view/Display;->getWidth()I

    move-result v3

    mul-int/2addr v2, v3

    const v3, 0x7eb1c

    if-ne v2, v3, :cond_a

    .line 146
    :cond_9
    const v0, 0x7e900

    goto/16 :goto_0

    .line 147
    :cond_a
    invoke-virtual {v1}, Landroid/view/Display;->getHeight()I

    move-result v2

    invoke-virtual {v1}, Landroid/view/Display;->getWidth()I

    move-result v3

    mul-int/2addr v2, v3

    const v3, 0x8a000

    if-ne v2, v3, :cond_b

    .line 148
    const v0, 0x8a000

    goto/16 :goto_0

    .line 149
    :cond_b
    invoke-virtual {v1}, Landroid/view/Display;->getHeight()I

    move-result v2

    invoke-virtual {v1}, Landroid/view/Display;->getWidth()I

    move-result v3

    mul-int/2addr v2, v3

    const v3, 0x8ef80

    if-ne v2, v3, :cond_c

    .line 150
    const v0, 0x8ef80

    goto/16 :goto_0

    .line 151
    :cond_c
    invoke-virtual {v1}, Landroid/view/Display;->getHeight()I

    move-result v2

    invoke-virtual {v1}, Landroid/view/Display;->getWidth()I

    move-result v3

    mul-int/2addr v2, v3

    const v3, 0x96000

    if-ne v2, v3, :cond_d

    .line 152
    const v0, 0x96000

    goto/16 :goto_0

    .line 153
    :cond_d
    invoke-virtual {v1}, Landroid/view/Display;->getHeight()I

    move-result v2

    invoke-virtual {v1}, Landroid/view/Display;->getWidth()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    mul-int/2addr v2, v3

    const v3, 0x384000

    if-ne v2, v3, :cond_0

    .line 154
    const v0, 0x384000

    goto/16 :goto_0

    .line 157
    .end local v1    # "sysDisplay":Landroid/view/Display;
    :catch_0
    move-exception v2

    goto/16 :goto_0
.end method

.method public static getTimeString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "timestring"    # Ljava/lang/String;

    .prologue
    .line 296
    :try_start_0
    invoke-static {p1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 297
    .local v2, "timestamp":J
    invoke-static {p0}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v1

    .line 298
    .local v1, "timeformat":Ljava/text/DateFormat;
    new-instance v4, Ljava/util/Date;

    invoke-direct {v4, v2, v3}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v1, v4}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 301
    .end local v1    # "timeformat":Ljava/text/DateFormat;
    .end local v2    # "timestamp":J
    :goto_0
    return-object v4

    .line 300
    :catch_0
    move-exception v0

    .line 301
    .local v0, "ex":Ljava/lang/Exception;
    const-string v4, ""

    goto :goto_0
.end method

.method public static getTimestamp()Ljava/lang/String;
    .locals 3

    .prologue
    .line 287
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    .line 289
    .local v1, "timeValue":J
    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    .line 291
    .local v0, "timeStamp":Ljava/lang/String;
    return-object v0
.end method

.method public static getTodayBG(Landroid/content/Context;ZII)I
    .locals 11
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "isDay"    # Z
    .param p2, "icon"    # I
    .param p3, "bgType"    # I

    .prologue
    const/16 v10, 0xb

    const/16 v9, 0x8

    const/4 v8, 0x5

    const/4 v7, 0x2

    const/4 v3, -0x1

    .line 1009
    const/4 v2, -0x1

    .line 1010
    .local v2, "resID":I
    const/4 v0, 0x0

    .line 1012
    .local v0, "bgArray":Landroid/content/res/TypedArray;
    const-string v4, ""

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "icon = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", isDay = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", type = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1015
    packed-switch p3, :pswitch_data_0

    .line 1038
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f070007

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1039
    const-string v4, ""

    const-string v5, "bgType default"

    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1047
    :goto_0
    packed-switch p2, :pswitch_data_1

    .line 1181
    :pswitch_0
    invoke-virtual {v0, v7, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    .line 1185
    :goto_1
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1187
    const-string v3, ""

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "resID = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 1189
    :goto_2
    return v3

    .line 1017
    :pswitch_1
    :try_start_1
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f070003

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1018
    goto :goto_0

    .line 1021
    :pswitch_2
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f070005

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1023
    goto :goto_0

    .line 1026
    :pswitch_3
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f070004

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1027
    goto :goto_0

    .line 1030
    :pswitch_4
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f070006

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1031
    goto :goto_0

    .line 1034
    :pswitch_5
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f070007

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;
    :try_end_1
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    .line 1035
    goto :goto_0

    .line 1042
    :catch_0
    move-exception v1

    .line 1043
    .local v1, "e":Landroid/content/res/Resources$NotFoundException;
    invoke-virtual {v1}, Landroid/content/res/Resources$NotFoundException;->printStackTrace()V

    goto :goto_2

    .line 1054
    .end local v1    # "e":Landroid/content/res/Resources$NotFoundException;
    :pswitch_6
    if-eqz p1, :cond_0

    .line 1055
    invoke-virtual {v0, v7, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    goto :goto_1

    .line 1057
    :cond_0
    invoke-virtual {v0, v9, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    .line 1059
    goto :goto_1

    .line 1069
    :pswitch_7
    const/16 v4, 0x105

    if-ne p3, v4, :cond_2

    .line 1070
    if-eqz p1, :cond_1

    .line 1071
    invoke-virtual {v0, v7, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    goto :goto_1

    .line 1073
    :cond_1
    invoke-virtual {v0, v9, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    goto :goto_1

    .line 1076
    :cond_2
    if-eqz p1, :cond_3

    .line 1077
    invoke-virtual {v0, v8, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    goto :goto_1

    .line 1079
    :cond_3
    invoke-virtual {v0, v10, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    .line 1082
    goto/16 :goto_1

    .line 1089
    :pswitch_8
    if-eqz p1, :cond_4

    .line 1090
    const/4 v4, 0x3

    invoke-virtual {v0, v4, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    goto/16 :goto_1

    .line 1092
    :cond_4
    const/16 v4, 0x9

    invoke-virtual {v0, v4, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    .line 1094
    goto/16 :goto_1

    .line 1098
    :pswitch_9
    if-eqz p1, :cond_5

    .line 1099
    invoke-virtual {v0, v8, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    goto/16 :goto_1

    .line 1101
    :cond_5
    invoke-virtual {v0, v10, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    .line 1103
    goto/16 :goto_1

    .line 1107
    :pswitch_a
    if-eqz p1, :cond_6

    .line 1108
    const/16 v4, 0xc

    invoke-virtual {v0, v4, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    goto/16 :goto_1

    .line 1110
    :cond_6
    const/16 v4, 0xd

    invoke-virtual {v0, v4, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    .line 1112
    goto/16 :goto_1

    .line 1126
    :pswitch_b
    if-eqz p1, :cond_7

    .line 1127
    const/4 v4, 0x0

    invoke-virtual {v0, v4, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    goto/16 :goto_1

    .line 1129
    :cond_7
    const/4 v4, 0x6

    invoke-virtual {v0, v4, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    .line 1131
    goto/16 :goto_1

    .line 1141
    :pswitch_c
    if-eqz p1, :cond_8

    .line 1142
    const/4 v4, 0x4

    invoke-virtual {v0, v4, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    goto/16 :goto_1

    .line 1144
    :cond_8
    const/16 v4, 0xa

    invoke-virtual {v0, v4, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    .line 1146
    goto/16 :goto_1

    .line 1163
    :pswitch_d
    if-eqz p1, :cond_9

    .line 1164
    const/4 v4, 0x1

    invoke-virtual {v0, v4, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    goto/16 :goto_1

    .line 1166
    :cond_9
    const/4 v4, 0x7

    invoke-virtual {v0, v4, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    .line 1168
    goto/16 :goto_1

    .line 1172
    :pswitch_e
    const/16 v4, 0xe

    invoke-virtual {v0, v4, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    .line 1173
    goto/16 :goto_1

    .line 1177
    :pswitch_f
    const/16 v4, 0xf

    invoke-virtual {v0, v4, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    .line 1178
    goto/16 :goto_1

    .line 1015
    :pswitch_data_0
    .packed-switch 0x101
        :pswitch_1
        :pswitch_4
        :pswitch_2
        :pswitch_3
        :pswitch_5
    .end packed-switch

    .line 1047
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_6
        :pswitch_6
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_a
        :pswitch_b
        :pswitch_b
        :pswitch_b
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_b
        :pswitch_d
        :pswitch_d
        :pswitch_d
        :pswitch_d
        :pswitch_d
        :pswitch_d
        :pswitch_d
        :pswitch_d
        :pswitch_0
        :pswitch_0
        :pswitch_b
        :pswitch_e
        :pswitch_f
        :pswitch_9
        :pswitch_6
        :pswitch_6
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_8
        :pswitch_b
        :pswitch_b
        :pswitch_c
        :pswitch_c
        :pswitch_d
        :pswitch_d
    .end packed-switch
.end method

.method public static getURL(Landroid/content/Context;I)Ljava/net/URL;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "urlType"    # I

    .prologue
    .line 3599
    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;-><init>(Landroid/content/Context;)V

    .line 3600
    .local v1, "urlManager":Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkLanguage(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 3601
    .local v0, "language":Ljava/lang/String;
    const/4 v2, 0x0

    .line 3603
    .local v2, "zoomURL":Ljava/net/URL;
    packed-switch p1, :pswitch_data_0

    .line 3615
    :goto_0
    return-object v2

    .line 3605
    :pswitch_0
    invoke-virtual {v1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;->makeUrlForMapZoom5(Ljava/lang/String;)Ljava/net/URL;

    move-result-object v2

    .line 3606
    goto :goto_0

    .line 3608
    :pswitch_1
    invoke-virtual {v1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;->makeUrlForMapZoom6(Ljava/lang/String;)Ljava/net/URL;

    move-result-object v2

    .line 3609
    goto :goto_0

    .line 3611
    :pswitch_2
    invoke-virtual {v1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;->makeUrlForMapZoom7(Ljava/lang/String;)Ljava/net/URL;

    move-result-object v2

    goto :goto_0

    .line 3603
    nop

    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static getUVIndexText(Landroid/content/Context;)Ljava/lang/String;
    .locals 4
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    .line 4031
    const/4 v0, 0x0

    .line 4032
    .local v0, "UVIndex":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget-object v2, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    .line 4034
    .local v1, "locale":Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "en"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 4035
    const-string v0, "UV Index"

    .line 4039
    :goto_0
    return-object v0

    .line 4037
    :cond_0
    const v2, 0x7f0d00b5

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static getUpdateTimeFromPref(Landroid/content/Context;)J
    .locals 7
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    const-wide/16 v5, 0x0

    .line 3211
    const-string v1, "updatetime"

    const/4 v2, 0x4

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 3212
    .local v0, "prefs":Landroid/content/SharedPreferences;
    const-string v1, ""

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "get UT pref:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "fail_updatetime"

    invoke-interface {v0, v3, v5, v6}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3213
    const-string v1, "fail_updatetime"

    invoke-interface {v0, v1, v5, v6}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    return-wide v1
.end method

.method public static getWIdgetMode(Landroid/content/Context;)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 2660
    const-string v1, "config"

    const/4 v2, 0x4

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 2661
    .local v0, "prefs":Landroid/content/SharedPreferences;
    const-string v1, "widget_mode"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    return v1
.end method

.method public static getWeatherInfoIcon(IZ)I
    .locals 8
    .param p0, "weatherIconNum"    # I
    .param p1, "isDay"    # Z

    .prologue
    const/16 v4, 0x13

    const/16 v2, 0x12

    const/4 v3, 0x2

    const/4 v1, 0x1

    .line 3758
    const/4 v0, 0x0

    .line 3759
    .local v0, "weatherInfoIcon":I
    const-string v5, ""

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "weatherIconNum=="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", isDay:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3760
    packed-switch p0, :pswitch_data_0

    .line 3859
    :pswitch_0
    const-string v1, ""

    const-string v2, "weatherIconNum is E"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3860
    const/4 v0, 0x1

    .line 3863
    :goto_0
    return v0

    .line 3763
    :pswitch_1
    if-ne p1, v1, :cond_0

    move v0, v1

    .line 3764
    :goto_1
    goto :goto_0

    :cond_0
    move v0, v2

    .line 3763
    goto :goto_1

    .line 3769
    :pswitch_2
    if-ne p1, v1, :cond_1

    move v0, v3

    .line 3770
    :goto_2
    goto :goto_0

    :cond_1
    move v0, v4

    .line 3769
    goto :goto_2

    .line 3776
    :pswitch_3
    const/4 v0, 0x3

    .line 3777
    goto :goto_0

    .line 3780
    :pswitch_4
    const/4 v0, 0x5

    .line 3781
    goto :goto_0

    .line 3787
    :pswitch_5
    const/4 v0, 0x6

    .line 3788
    goto :goto_0

    .line 3791
    :pswitch_6
    if-ne p1, v1, :cond_2

    const/4 v0, 0x7

    .line 3792
    :goto_3
    goto :goto_0

    .line 3791
    :cond_2
    const/4 v0, 0x6

    goto :goto_3

    .line 3798
    :pswitch_7
    const/16 v0, 0x8

    .line 3799
    goto :goto_0

    .line 3802
    :pswitch_8
    if-ne p1, v1, :cond_3

    const/16 v0, 0x9

    .line 3803
    :goto_4
    goto :goto_0

    .line 3802
    :cond_3
    const/16 v0, 0x8

    goto :goto_4

    .line 3806
    :pswitch_9
    const/4 v0, 0x4

    .line 3807
    goto :goto_0

    .line 3812
    :pswitch_a
    const/16 v0, 0xa

    .line 3813
    goto :goto_0

    .line 3816
    :pswitch_b
    if-ne p1, v1, :cond_4

    const/16 v0, 0xb

    .line 3817
    :goto_5
    goto :goto_0

    .line 3816
    :cond_4
    const/16 v0, 0xa

    goto :goto_5

    .line 3822
    :pswitch_c
    const/16 v0, 0xc

    .line 3823
    goto :goto_0

    .line 3828
    :pswitch_d
    const/16 v0, 0xe

    .line 3829
    goto :goto_0

    .line 3832
    :pswitch_e
    const/16 v0, 0xd

    .line 3833
    goto :goto_0

    .line 3836
    :pswitch_f
    const/16 v0, 0xf

    .line 3837
    goto :goto_0

    .line 3840
    :pswitch_10
    const/16 v0, 0x10

    .line 3841
    goto :goto_0

    .line 3844
    :pswitch_11
    const/16 v0, 0x11

    .line 3845
    goto :goto_0

    .line 3849
    :pswitch_12
    if-ne p1, v1, :cond_5

    move v0, v1

    .line 3850
    :goto_6
    goto :goto_0

    :cond_5
    move v0, v2

    .line 3849
    goto :goto_6

    .line 3855
    :pswitch_13
    if-ne p1, v1, :cond_6

    move v0, v3

    .line 3856
    :goto_7
    goto :goto_0

    :cond_6
    move v0, v4

    .line 3855
    goto :goto_7

    .line 3760
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_c
        :pswitch_d
        :pswitch_d
        :pswitch_d
        :pswitch_0
        :pswitch_0
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_12
        :pswitch_13
        :pswitch_13
        :pswitch_13
        :pswitch_3
        :pswitch_5
        :pswitch_5
        :pswitch_7
        :pswitch_7
        :pswitch_a
        :pswitch_c
    .end packed-switch
.end method

.method public static getWeatherInfoTTS(Landroid/content/Context;IILcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;)Ljava/lang/String;
    .locals 6
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "fromScale"    # I
    .param p2, "toScale"    # I
    .param p3, "specificDay"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    .prologue
    const/4 v5, 0x0

    .line 1687
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 1688
    .local v2, "sb":Ljava/lang/StringBuilder;
    const/4 v0, 0x0

    .local v0, "highTemp":Ljava/lang/String;
    const/4 v1, 0x0

    .line 1690
    .local v1, "lowTemp":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getIconNum()I

    move-result v4

    invoke-static {p0, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getWeatherText(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1692
    invoke-virtual {p3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getHighTemp()F

    move-result v3

    invoke-static {p1, p2, v3, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTemp(IIFZ)Ljava/lang/String;

    move-result-object v0

    .line 1693
    invoke-virtual {p3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getLowTemp()F

    move-result v3

    invoke-static {p1, p2, v3, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTemp(IIFZ)Ljava/lang/String;

    move-result-object v1

    .line 1695
    invoke-static {p0, p2, v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getHighLowTempTTS(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1697
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method public static getWeatherMainInfoTTS(Landroid/content/Context;Ljava/util/Calendar;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "cal"    # Ljava/util/Calendar;
    .param p2, "fromScale"    # I
    .param p3, "toScale"    # I
    .param p4, "cityName"    # Ljava/lang/String;
    .param p5, "stateName"    # Ljava/lang/String;
    .param p6, "sunrisesunset"    # Ljava/lang/String;
    .param p7, "sensibleTemp"    # Ljava/lang/String;
    .param p8, "probability"    # Ljava/lang/String;
    .param p9, "specificDay"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    .param p10, "weatherText"    # Ljava/lang/String;

    .prologue
    .line 1656
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 1657
    .local v4, "sb":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .local v1, "currentTemp":Ljava/lang/String;
    const/4 v2, 0x0

    .local v2, "highTemp":Ljava/lang/String;
    const/4 v3, 0x0

    .line 1659
    .local v3, "lowTemp":Ljava/lang/String;
    if-eqz p4, :cond_0

    .line 1660
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1662
    :cond_0
    if-eqz p5, :cond_1

    .line 1663
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1666
    :cond_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v6, 0xa

    invoke-static {p0, p1, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->makeTodayDate(Landroid/content/Context;Ljava/util/Calendar;I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1668
    invoke-virtual/range {p9 .. p9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getHighTemp()F

    move-result v5

    const/4 v6, 0x0

    invoke-static {p2, p3, v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTemp(IIFZ)Ljava/lang/String;

    move-result-object v2

    .line 1669
    invoke-virtual/range {p9 .. p9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getLowTemp()F

    move-result v5

    const/4 v6, 0x0

    invoke-static {p2, p3, v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTemp(IIFZ)Ljava/lang/String;

    move-result-object v3

    .line 1671
    move-object/from16 v0, p9

    instance-of v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    if-eqz v5, :cond_2

    .line 1672
    check-cast p9, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    .line 1673
    .end local p9    # "specificDay":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    invoke-virtual/range {p9 .. p9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getCurrentTemp()F

    move-result v5

    const/4 v6, 0x0

    .line 1672
    invoke-static {p2, p3, v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTemp(IIFZ)Ljava/lang/String;

    move-result-object v1

    .line 1674
    invoke-static {p0, p3, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getCurrentTempTTS(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1676
    :cond_2
    invoke-static {p0, p3, v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getHighLowTempTTS(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1677
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p10

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1678
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1679
    invoke-static {p7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {p0, p3, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getRealfeelTempTTS(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1680
    const-string v5, "%"

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0d00a3

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p8

    invoke-virtual {v0, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1682
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5
.end method

.method public static getWeatherText(Landroid/content/Context;I)Ljava/lang/String;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "weatherIconNum"    # I

    .prologue
    const v6, 0x7f0d004d

    const v5, 0x7f0d004a

    const v4, 0x7f0d0045

    const v2, 0x7f0d0043

    const v3, 0x7f0d0040

    .line 648
    const-string v0, ""

    .line 649
    .local v0, "weatherText":Ljava/lang/String;
    packed-switch p1, :pswitch_data_0

    .line 777
    :pswitch_0
    const-string v1, ""

    const-string v2, "gWT is E"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 778
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 781
    :goto_0
    return-object v0

    .line 651
    :pswitch_1
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 652
    goto :goto_0

    .line 654
    :pswitch_2
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0041

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 655
    goto :goto_0

    .line 657
    :pswitch_3
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0042

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 658
    goto :goto_0

    .line 660
    :pswitch_4
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 661
    goto :goto_0

    .line 663
    :pswitch_5
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0044

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 664
    goto :goto_0

    .line 666
    :pswitch_6
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 667
    goto :goto_0

    .line 669
    :pswitch_7
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0046

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 670
    goto :goto_0

    .line 672
    :pswitch_8
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0047

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 673
    goto :goto_0

    .line 675
    :pswitch_9
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0048

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 676
    goto :goto_0

    .line 678
    :pswitch_a
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0049

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 679
    goto :goto_0

    .line 681
    :pswitch_b
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 682
    goto :goto_0

    .line 684
    :pswitch_c
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d004b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 685
    goto/16 :goto_0

    .line 687
    :pswitch_d
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d004c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 688
    goto/16 :goto_0

    .line 690
    :pswitch_e
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 692
    goto/16 :goto_0

    .line 694
    :pswitch_f
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d004e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 696
    goto/16 :goto_0

    .line 698
    :pswitch_10
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d004f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 699
    goto/16 :goto_0

    .line 701
    :pswitch_11
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0050

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 702
    goto/16 :goto_0

    .line 704
    :pswitch_12
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0051

    .line 705
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 706
    goto/16 :goto_0

    .line 708
    :pswitch_13
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0052

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 709
    goto/16 :goto_0

    .line 711
    :pswitch_14
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0053

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 712
    goto/16 :goto_0

    .line 714
    :pswitch_15
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0054

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 715
    goto/16 :goto_0

    .line 717
    :pswitch_16
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0055

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 718
    goto/16 :goto_0

    .line 720
    :pswitch_17
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0056

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 721
    goto/16 :goto_0

    .line 723
    :pswitch_18
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0057

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 724
    goto/16 :goto_0

    .line 726
    :pswitch_19
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0058

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 727
    goto/16 :goto_0

    .line 729
    :pswitch_1a
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0059

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 730
    goto/16 :goto_0

    .line 732
    :pswitch_1b
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d005a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 733
    goto/16 :goto_0

    .line 735
    :pswitch_1c
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d005b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 736
    goto/16 :goto_0

    .line 738
    :pswitch_1d
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d005c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 739
    goto/16 :goto_0

    .line 741
    :pswitch_1e
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d005d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 742
    goto/16 :goto_0

    .line 744
    :pswitch_1f
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d005e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 745
    goto/16 :goto_0

    .line 747
    :pswitch_20
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 748
    goto/16 :goto_0

    .line 750
    :pswitch_21
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d005f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 751
    goto/16 :goto_0

    .line 753
    :pswitch_22
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 754
    goto/16 :goto_0

    .line 756
    :pswitch_23
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0061

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 757
    goto/16 :goto_0

    .line 759
    :pswitch_24
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 760
    goto/16 :goto_0

    .line 762
    :pswitch_25
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0060

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 764
    goto/16 :goto_0

    .line 766
    :pswitch_26
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 768
    goto/16 :goto_0

    .line 770
    :pswitch_27
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0051

    .line 771
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 772
    goto/16 :goto_0

    .line 774
    :pswitch_28
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0054

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 775
    goto/16 :goto_0

    .line 649
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_0
        :pswitch_0
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_26
        :pswitch_27
        :pswitch_28
    .end packed-switch
.end method

.method public static getWeekHighTemp([IILcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;)Ljava/lang/Boolean;
    .locals 4
    .param p0, "high_values"    # [I
    .param p1, "todayTempScale"    # I
    .param p2, "info"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    .prologue
    const/4 v3, 0x0

    .line 3125
    if-eqz p2, :cond_1

    .line 3126
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getForcastSize()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 3128
    invoke-virtual {p2, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getForecastInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getHighTemp()F

    move-result v2

    .line 3127
    invoke-static {p1, v3, v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTemp(IIFZ)Ljava/lang/String;

    move-result-object v1

    .line 3129
    .local v1, "temp":Ljava/lang/String;
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    aput v2, p0, v0

    .line 3126
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3132
    .end local v1    # "temp":Ljava/lang/String;
    :cond_0
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 3134
    .end local v0    # "i":I
    :goto_1
    return-object v2

    :cond_1
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto :goto_1
.end method

.method public static getWeekLowTemp([IILcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;)Ljava/lang/Boolean;
    .locals 4
    .param p0, "low_values"    # [I
    .param p1, "todayTempScale"    # I
    .param p2, "info"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    .prologue
    const/4 v3, 0x0

    .line 3139
    if-eqz p2, :cond_1

    .line 3140
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getForcastSize()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 3142
    invoke-virtual {p2, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getForecastInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getLowTemp()F

    move-result v2

    .line 3141
    invoke-static {p1, v3, v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTemp(IIFZ)Ljava/lang/String;

    move-result-object v1

    .line 3143
    .local v1, "temp":Ljava/lang/String;
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    aput v2, p0, v0

    .line 3140
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3145
    .end local v1    # "temp":Ljava/lang/String;
    :cond_0
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 3147
    .end local v0    # "i":I
    :goto_1
    return-object v2

    :cond_1
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto :goto_1
.end method

.method public static getWindowDisplay(Landroid/content/Context;)Landroid/view/Display;
    .locals 3
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    .line 642
    const-string v2, "window"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    .line 643
    .local v1, "wm":Landroid/view/WindowManager;
    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 644
    .local v0, "display":Landroid/view/Display;
    return-object v0
.end method

.method public static handleConnectivityUpdate(Landroid/content/Context;)Z
    .locals 5
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 3649
    const-string v2, "phone"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    .line 3651
    .local v1, "tm":Landroid/telephony/TelephonyManager;
    if-nez v1, :cond_0

    .line 3657
    :goto_0
    return v0

    .line 3652
    :cond_0
    invoke-static {}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->isIgnoreNationalRoaming()Z

    move-result v2

    if-nez v2, :cond_1

    .line 3653
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->isNetworkRoaming()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 3654
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getDataState()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    const/4 v0, 0x1

    .line 3656
    .local v0, "res":Z
    :cond_1
    const-string v2, ""

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "handleConnectivityUpdate() = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static insertHighlight(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/CharSequence;
    .locals 10
    .param p0, "searchWord"    # Ljava/lang/String;
    .param p1, "originalText"    # Ljava/lang/String;

    .prologue
    .line 3868
    if-eqz p0, :cond_0

    if-nez p1, :cond_2

    .line 3869
    :cond_0
    const-string v6, ""

    const-string v7, "iH arg n"

    invoke-static {v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3870
    const/4 p1, 0x0

    .line 3893
    .end local p1    # "originalText":Ljava/lang/String;
    :cond_1
    :goto_0
    return-object p1

    .line 3873
    .restart local p1    # "originalText":Ljava/lang/String;
    :cond_2
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    .line 3874
    .local v1, "originalTextLowerCase":Ljava/lang/String;
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    .line 3876
    .local v2, "searchWordLowerCase":Ljava/lang/String;
    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    .line 3877
    .local v5, "start":I
    if-ltz v5, :cond_1

    .line 3881
    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, p1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 3882
    .local v0, "highlightedStr":Landroid/text/Spannable;
    if-ltz v5, :cond_3

    .line 3883
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v6

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 3884
    .local v4, "spanStart":I
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v6, v5

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v7

    invoke-static {v6, v7}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 3886
    .local v3, "spanEnd":I
    new-instance v6, Landroid/text/style/ForegroundColorSpan;

    const/4 v7, 0x0

    const/16 v8, 0x84

    const/16 v9, 0xa5

    invoke-static {v7, v8, v9}, Landroid/graphics/Color;->rgb(III)I

    move-result v7

    invoke-direct {v6, v7}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/16 v7, 0x21

    invoke-interface {v0, v6, v4, v3, v7}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 3888
    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v5

    .end local v3    # "spanEnd":I
    .end local v4    # "spanStart":I
    :cond_3
    move-object p1, v0

    .line 3893
    goto :goto_0
.end method

.method public static isAgreeUseLocation(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 198
    const/4 v0, 0x0

    .line 199
    .local v0, "ret":Z
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getShowUseLocationPopup(Landroid/content/Context;)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 200
    const/4 v0, 0x1

    .line 202
    :cond_0
    return v0
.end method

.method public static isChangeIcon(IZ)Z
    .locals 4
    .param p0, "iconNum"    # I
    .param p1, "isDay"    # Z

    .prologue
    .line 1593
    const/4 v0, 0x0

    .line 1595
    .local v0, "ret":Z
    const-string v1, ""

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "iCI : iconNum = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", isDay = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1597
    if-eqz p1, :cond_0

    .line 1598
    packed-switch p0, :pswitch_data_0

    .line 1617
    :goto_0
    return v0

    .line 1604
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 1607
    :cond_0
    packed-switch p0, :pswitch_data_1

    goto :goto_0

    .line 1613
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 1598
    nop

    :pswitch_data_0
    .packed-switch 0x21
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 1607
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public static isDay(Ljava/util/TimeZone;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 8
    .param p0, "timeZone"    # Ljava/util/TimeZone;
    .param p1, "sunrise"    # Ljava/lang/String;
    .param p2, "sunset"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 2264
    const/4 v0, 0x0

    .line 2265
    .local v0, "cal":Ljava/util/Calendar;
    if-eqz p0, :cond_2

    .line 2266
    invoke-static {p0}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v0

    .line 2271
    :goto_0
    const/16 v7, 0xb

    invoke-virtual {v0, v7}, Ljava/util/Calendar;->get(I)I

    move-result v2

    .line 2272
    .local v2, "hour":I
    const/16 v7, 0xc

    invoke-virtual {v0, v7}, Ljava/util/Calendar;->get(I)I

    move-result v3

    .line 2274
    .local v3, "minute":I
    invoke-static {p1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->parseSunrise(Ljava/lang/String;Ljava/util/Calendar;)[I

    move-result-object v1

    .line 2275
    .local v1, "dayTime":[I
    invoke-static {p2, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->parseSunSet(Ljava/lang/String;Ljava/util/Calendar;)[I

    move-result-object v4

    .line 2277
    .local v4, "nightTime":[I
    if-eqz v1, :cond_1

    if-eqz v4, :cond_1

    .line 2285
    aget v7, v1, v5

    if-gt v2, v7, :cond_0

    aget v7, v1, v5

    if-ne v2, v7, :cond_1

    aget v7, v1, v6

    if-lt v3, v7, :cond_1

    .line 2286
    :cond_0
    aget v7, v4, v5

    if-gt v2, v7, :cond_1

    aget v7, v4, v5

    if-ne v2, v7, :cond_3

    aget v7, v4, v6

    if-lt v3, v7, :cond_3

    .line 2299
    :cond_1
    :goto_1
    return v5

    .line 2268
    .end local v1    # "dayTime":[I
    .end local v2    # "hour":I
    .end local v3    # "minute":I
    .end local v4    # "nightTime":[I
    :cond_2
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    goto :goto_0

    .restart local v1    # "dayTime":[I
    .restart local v2    # "hour":I
    .restart local v3    # "minute":I
    .restart local v4    # "nightTime":[I
    :cond_3
    move v5, v6

    .line 2289
    goto :goto_1
.end method

.method public static isFactoryMode(Landroid/content/Context;)Z
    .locals 8
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x1

    .line 2130
    const-string v5, "phone"

    .line 2131
    invoke-virtual {p0, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/telephony/TelephonyManager;

    .line 2132
    .local v3, "tm":Landroid/telephony/TelephonyManager;
    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getSubscriberId()Ljava/lang/String;

    move-result-object v1

    .line 2133
    .local v1, "imsi":Ljava/lang/String;
    if-eqz v1, :cond_1

    const-string v5, "999999999999999"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 2146
    :cond_0
    :goto_0
    return v4

    .line 2137
    :cond_1
    const/4 v2, 0x0

    .line 2139
    .local v2, "mode":Ljava/lang/String;
    :try_start_0
    new-instance v5, Ljava/io/File;

    const-string v6, "/efs/FactoryApp/factorymode"

    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/16 v6, 0x20

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Landroid/os/FileUtils;->readTextFile(Ljava/io/File;ILjava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 2143
    :goto_1
    if-eqz v2, :cond_2

    const-string v5, "OFF"

    invoke-virtual {v2, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 2146
    :cond_2
    const/4 v4, 0x0

    goto :goto_0

    .line 2140
    :catch_0
    move-exception v0

    .line 2141
    .local v0, "e":Ljava/io/IOException;
    const-string v2, "OFF"

    goto :goto_1
.end method

.method public static isHardkeyboard(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x1

    .line 164
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 165
    .local v0, "config":Landroid/content/res/Configuration;
    iget v2, v0, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    if-ne v2, v1, :cond_0

    .line 168
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isIgnoreNationalRoaming()Z
    .locals 3

    .prologue
    .line 3662
    const-string v1, "ro.csc.sales_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 3664
    .local v0, "salesCode":Ljava/lang/String;
    const-string v1, "XEO"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "PRT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    invoke-static {}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->isNationalRoaming()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 3665
    const-string v1, ""

    const-string v2, "isIgnoreNationalRoaming() = true"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3666
    const/4 v1, 0x1

    .line 3669
    :goto_0
    return v1

    .line 3668
    :cond_1
    const-string v1, ""

    const-string v2, "isIgnoreNationalRoaming() = false"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3669
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isInstalledApplication(Ljava/lang/String;Landroid/content/Context;)Z
    .locals 5
    .param p0, "packageName"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 3158
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 3160
    .local v1, "pm":Landroid/content/pm/PackageManager;
    const/16 v2, 0x80

    :try_start_0
    invoke-virtual {v1, p0, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3165
    const/4 v2, 0x1

    :goto_0
    return v2

    .line 3161
    :catch_0
    move-exception v0

    .line 3162
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v2, ""

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " not found!!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3163
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static isLanguageHindiorBengali(Landroid/content/Context;)Z
    .locals 3
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    .line 2945
    const/4 v1, 0x0

    .line 2947
    .local v1, "result":Z
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget-object v2, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    .line 2949
    .local v0, "langID":Ljava/lang/String;
    const-string v2, "hi"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "bn"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2950
    :cond_0
    const/4 v1, 0x1

    .line 2953
    :cond_1
    return v1
.end method

.method public static isLocationAvailable(Landroid/content/Context;Z)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "networkOnly"    # Z

    .prologue
    const/4 v0, 0x1

    .line 589
    if-eqz p1, :cond_1

    .line 590
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "network"

    invoke-static {v1, v2}, Landroid/provider/Settings$Secure;->isLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 611
    :cond_0
    :goto_0
    return v0

    .line 595
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "network"

    invoke-static {v1, v2}, Landroid/provider/Settings$Secure;->isLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 597
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "gps"

    invoke-static {v1, v2}, Landroid/provider/Settings$Secure;->isLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 602
    :cond_2
    const-string v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "iLA : N only = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", N = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 606
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "network"

    invoke-static {v2, v3}, Landroid/provider/Settings$Secure;->isLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", G = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 609
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "gps"

    invoke-static {v2, v3}, Landroid/provider/Settings$Secure;->isLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 602
    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 611
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isNationalRoaming()Z
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v4, 0x0

    .line 3673
    const-string v5, "gsm.sim.operator.numeric"

    const-string v6, ""

    invoke-static {v5, v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 3674
    .local v3, "simNumeric":Ljava/lang/String;
    const-string v5, "gsm.operator.numeric"

    const-string v6, ""

    invoke-static {v5, v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 3676
    .local v1, "plmnNumeric":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_1

    .line 3677
    invoke-virtual {v3, v4, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 3678
    .local v2, "simMCC":Ljava/lang/String;
    invoke-virtual {v1, v4, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 3680
    .local v0, "plmnMCC":Ljava/lang/String;
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 3681
    const-string v4, ""

    const-string v5, "isNationalRoaming() = true"

    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3682
    const/4 v4, 0x1

    .line 3689
    .end local v0    # "plmnMCC":Ljava/lang/String;
    .end local v2    # "simMCC":Ljava/lang/String;
    :goto_0
    return v4

    .line 3684
    .restart local v0    # "plmnMCC":Ljava/lang/String;
    .restart local v2    # "simMCC":Ljava/lang/String;
    :cond_0
    const-string v5, ""

    const-string v6, "isNationalRoaming() = false : not equal"

    invoke-static {v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 3688
    .end local v0    # "plmnMCC":Ljava/lang/String;
    .end local v2    # "simMCC":Ljava/lang/String;
    :cond_1
    const-string v5, ""

    const-string v6, "isNationalRoaming() = false"

    invoke-static {v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static isNeedAutoRefresh(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 2477
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 2478
    .local v2, "now":J
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getNextRefreshTime(Landroid/content/Context;)J

    move-result-wide v0

    .line 2479
    .local v0, "lasttime":J
    cmp-long v4, v0, v2

    if-gtz v4, :cond_0

    const/4 v4, 0x1

    :goto_0
    return v4

    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public static isShowChargerPopup(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 184
    const-string v2, "config"

    const/4 v3, 0x4

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 185
    .local v0, "pref":Landroid/content/SharedPreferences;
    const-string v2, "SHOW_CHARGER_POPUP"

    const/4 v3, 0x1

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 186
    .local v1, "ret":Z
    return v1
.end method

.method public static isSymbol(Ljava/lang/String;)Z
    .locals 4
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    .line 2191
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 2192
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Ljava/lang/Character;->getType(C)I

    move-result v2

    const/4 v3, 0x2

    if-eq v2, v3, :cond_1

    .line 2193
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Ljava/lang/Character;->getType(C)I

    move-result v2

    if-eq v2, v1, :cond_1

    .line 2194
    const/4 v1, 0x0

    .line 2197
    :cond_0
    return v1

    .line 2191
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static isWifiConnecting(Landroid/content/Context;)Z
    .locals 9
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 616
    :try_start_0
    const-string v7, "connectivity"

    .line 617
    invoke-virtual {p0, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 619
    .local v0, "connManager":Landroid/net/ConnectivityManager;
    if-nez v0, :cond_1

    .line 638
    .end local v0    # "connManager":Landroid/net/ConnectivityManager;
    :cond_0
    :goto_0
    return v5

    .line 623
    .restart local v0    # "connManager":Landroid/net/ConnectivityManager;
    :cond_1
    const/4 v7, 0x1

    invoke-virtual {v0, v7}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v3

    .line 624
    .local v3, "networkInfo":Landroid/net/NetworkInfo;
    if-eqz v3, :cond_0

    .line 625
    invoke-virtual {v3}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    move-result-object v1

    .line 626
    .local v1, "ds":Landroid/net/NetworkInfo$DetailedState;
    invoke-virtual {v3}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v4

    .line 628
    .local v4, "st":Landroid/net/NetworkInfo$State;
    sget-object v7, Landroid/net/NetworkInfo$State;->UNKNOWN:Landroid/net/NetworkInfo$State;

    if-eq v4, v7, :cond_0

    .line 631
    sget-object v7, Landroid/net/NetworkInfo$DetailedState;->IDLE:Landroid/net/NetworkInfo$DetailedState;

    if-eq v1, v7, :cond_2

    sget-object v7, Landroid/net/NetworkInfo$DetailedState;->SCANNING:Landroid/net/NetworkInfo$DetailedState;

    if-eq v1, v7, :cond_2

    sget-object v7, Landroid/net/NetworkInfo$State;->CONNECTING:Landroid/net/NetworkInfo$State;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-ne v4, v7, :cond_0

    :cond_2
    move v5, v6

    goto :goto_0

    .line 634
    .end local v0    # "connManager":Landroid/net/ConnectivityManager;
    .end local v1    # "ds":Landroid/net/NetworkInfo$DetailedState;
    .end local v3    # "networkInfo":Landroid/net/NetworkInfo;
    .end local v4    # "st":Landroid/net/NetworkInfo$State;
    :catch_0
    move-exception v2

    .line 635
    .local v2, "e":Ljava/lang/Exception;
    const-string v6, ""

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Util.isWifiConnecting() "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static isWifiOnly(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 3152
    const-string v2, "connectivity"

    .line 3153
    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 3154
    .local v0, "cm":Landroid/net/ConnectivityManager;
    invoke-virtual {v0, v1}, Landroid/net/ConnectivityManager;->isNetworkSupported(I)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method public static makeQuickViewUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 6
    .param p0, "lang"    # Ljava/lang/String;
    .param p1, "location"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2467
    const-string v0, "http://www.accuweather.com"

    .line 2468
    .local v0, "PROVIDER_HOME_URL":Ljava/lang/String;
    if-nez p1, :cond_0

    .line 2469
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/?p=samand&lang=%s"

    new-array v3, v5, [Ljava/lang/Object;

    aput-object p0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 2472
    :goto_0
    return-object v1

    .line 2471
    :cond_0
    invoke-static {p1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 2472
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/m/Quick-Look.aspx?p=samand&lang=%s&loc=%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p0, v3, v4

    aput-object p1, v3, v5

    .line 2473
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2472
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    goto :goto_0
.end method

.method public static makeTimeText(Landroid/content/Context;Ljava/util/Calendar;)Ljava/lang/String;
    .locals 10
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "cal"    # Ljava/util/Calendar;

    .prologue
    const/16 v9, 0x6b

    const/16 v5, 0x68

    const/4 v8, 0x1

    const/4 v7, 0x2

    const/4 v6, 0x0

    .line 3693
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 3694
    .local v0, "format":Ljava/lang/StringBuffer;
    invoke-static {p0}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 3695
    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 3696
    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 3701
    :goto_0
    const-string v4, ":"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 3702
    const/16 v4, 0x6d

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 3703
    const/16 v4, 0x6d

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 3705
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, p1}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Calendar;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    .line 3721
    .local v3, "time":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget-object v4, v4, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v4}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    .line 3723
    .local v1, "locale":Ljava/lang/String;
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    const-string v5, "ja"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 3724
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 3725
    .local v2, "tempTime":Ljava/lang/StringBuffer;
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    .line 3727
    invoke-static {p0}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 3728
    invoke-virtual {v2, v6, v7}, Ljava/lang/StringBuffer;->substring(II)Ljava/lang/String;

    move-result-object v4

    const-string v5, "00"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 3729
    invoke-virtual {v2, v6, v7}, Ljava/lang/StringBuffer;->substring(II)Ljava/lang/String;

    move-result-object v4

    const-string v5, "12"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 3730
    :cond_0
    const-string v4, "0"

    invoke-virtual {v2, v6, v7, v4}, Ljava/lang/StringBuffer;->replace(IILjava/lang/String;)Ljava/lang/StringBuffer;

    .line 3738
    :cond_1
    :goto_1
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    .line 3739
    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    if-nez v4, :cond_2

    const-string v4, ":"

    .line 3740
    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    if-ne v4, v7, :cond_2

    .line 3741
    invoke-virtual {v3, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 3746
    .end local v2    # "tempTime":Ljava/lang/StringBuffer;
    :cond_2
    invoke-static {p0}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 3747
    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    if-nez v4, :cond_3

    const-string v4, ":"

    .line 3748
    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    if-ne v4, v7, :cond_3

    .line 3749
    invoke-virtual {v3, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 3753
    :cond_3
    return-object v3

    .line 3698
    .end local v1    # "locale":Ljava/lang/String;
    .end local v3    # "time":Ljava/lang/String;
    :cond_4
    invoke-virtual {v0, v9}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 3699
    invoke-virtual {v0, v9}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto/16 :goto_0

    .line 3733
    .restart local v1    # "locale":Ljava/lang/String;
    .restart local v2    # "tempTime":Ljava/lang/StringBuffer;
    .restart local v3    # "time":Ljava/lang/String;
    :cond_5
    invoke-virtual {v2, v6, v7}, Ljava/lang/StringBuffer;->substring(II)Ljava/lang/String;

    move-result-object v4

    const-string v5, "00"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 3734
    const-string v4, "0"

    invoke-virtual {v2, v6, v7, v4}, Ljava/lang/StringBuffer;->replace(IILjava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1
.end method

.method public static makeTodayDate(Landroid/content/Context;Ljava/util/Calendar;I)Ljava/lang/String;
    .locals 11
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "cal"    # Ljava/util/Calendar;
    .param p2, "dateLength"    # I

    .prologue
    .line 899
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v8, "date_format"

    invoke-static {v7, v8}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 902
    .local v1, "format":Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v7

    if-nez v7, :cond_1

    .line 903
    :cond_0
    const-string v1, "mm-dd-yyyy"

    .line 906
    :cond_1
    const/4 v7, 0x2

    invoke-virtual {p1, v7}, Ljava/util/Calendar;->get(I)I

    move-result v7

    invoke-static {v7, p2}, Landroid/text/format/DateUtils;->getMonthString(II)Ljava/lang/String;

    move-result-object v3

    .line 907
    .local v3, "monthName":Ljava/lang/String;
    const/4 v7, 0x7

    invoke-virtual {p1, v7}, Ljava/util/Calendar;->get(I)I

    move-result v7

    invoke-static {v7, p2}, Landroid/text/format/DateUtils;->getDayOfWeekString(II)Ljava/lang/String;

    move-result-object v5

    .line 908
    .local v5, "weekName":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0d00da

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 909
    .local v0, "dayText":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0d00db

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 910
    .local v6, "yearText":Ljava/lang/String;
    const-string v4, ""

    .line 911
    .local v4, "todayDate":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v7

    iget-object v7, v7, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v7}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    .line 912
    .local v2, "locale":Ljava/lang/String;
    if-eqz v2, :cond_2

    .line 913
    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v7

    const-string v8, "ja"

    invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 914
    const-string v7, "dd-MM-yyyy"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 915
    const-string v7, "%te%s %s %s%s%s"

    const/4 v8, 0x6

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object p1, v8, v9

    const/4 v9, 0x1

    aput-object v0, v8, v9

    const/4 v9, 0x2

    aput-object v3, v8, v9

    const/4 v9, 0x3

    const-string v10, "("

    aput-object v10, v8, v9

    const/4 v9, 0x4

    .line 916
    invoke-virtual {v5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x5

    const-string v10, ")"

    aput-object v10, v8, v9

    .line 915
    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 960
    :cond_2
    :goto_0
    return-object v4

    .line 917
    :cond_3
    const-string v7, "MM-dd-yyyy"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 918
    const-string v7, "%s %te%s %s%s%s"

    const/4 v8, 0x6

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v3, v8, v9

    const/4 v9, 0x1

    aput-object p1, v8, v9

    const/4 v9, 0x2

    aput-object v0, v8, v9

    const/4 v9, 0x3

    const-string v10, "("

    aput-object v10, v8, v9

    const/4 v9, 0x4

    .line 919
    invoke-virtual {v5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x5

    const-string v10, ")"

    aput-object v10, v8, v9

    .line 918
    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 922
    :cond_4
    const-string v7, "%s %te%s %s%s%s"

    const/4 v8, 0x6

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v3, v8, v9

    const/4 v9, 0x1

    aput-object p1, v8, v9

    const/4 v9, 0x2

    aput-object v0, v8, v9

    const/4 v9, 0x3

    const-string v10, "("

    aput-object v10, v8, v9

    const/4 v9, 0x4

    .line 923
    invoke-virtual {v5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x5

    const-string v10, ")"

    aput-object v10, v8, v9

    .line 922
    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 925
    :cond_5
    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v7

    const-string v8, "ko"

    invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 926
    const-string v7, "%s %te%s %s%s%s"

    const/4 v8, 0x6

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v3, v8, v9

    const/4 v9, 0x1

    aput-object p1, v8, v9

    const/4 v9, 0x2

    aput-object v0, v8, v9

    const/4 v9, 0x3

    const-string v10, "("

    aput-object v10, v8, v9

    const/4 v9, 0x4

    .line 927
    invoke-virtual {v5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x5

    const-string v10, ")"

    aput-object v10, v8, v9

    .line 926
    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 928
    :cond_6
    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v7

    const-string v8, "zh"

    invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_a

    .line 929
    const-string v7, "dd-MM-yyyy"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 930
    const-string v7, "%s %te%s %s"

    const/4 v8, 0x4

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-virtual {v5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    aput-object p1, v8, v9

    const/4 v9, 0x2

    aput-object v0, v8, v9

    const/4 v9, 0x3

    aput-object v3, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_0

    .line 932
    :cond_7
    const-string v7, "MM-dd-yyyy"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 933
    const-string v7, "%s %s %te%s"

    const/4 v8, 0x4

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-virtual {v5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    aput-object v3, v8, v9

    const/4 v9, 0x2

    aput-object p1, v8, v9

    const/4 v9, 0x3

    aput-object v0, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_0

    .line 935
    :cond_8
    const-string v7, "yyyy-MM-dd"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 937
    const-string v7, "%s %te%s %s"

    const/4 v8, 0x4

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v3, v8, v9

    const/4 v9, 0x1

    aput-object p1, v8, v9

    const/4 v9, 0x2

    aput-object v0, v8, v9

    const/4 v9, 0x3

    .line 938
    invoke-virtual {v5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    .line 937
    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_0

    .line 940
    :cond_9
    const-string v7, "%s %tY%s %s %te%s"

    const/4 v8, 0x6

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-virtual {v5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    aput-object p1, v8, v9

    const/4 v9, 0x2

    aput-object v6, v8, v9

    const/4 v9, 0x3

    aput-object v3, v8, v9

    const/4 v9, 0x4

    aput-object p1, v8, v9

    const/4 v9, 0x5

    aput-object v0, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_0

    .line 944
    :cond_a
    const-string v7, "dd-MM-yyyy"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_b

    .line 945
    const-string v7, "%s %te%s %s"

    const/4 v8, 0x4

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-virtual {v5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    aput-object p1, v8, v9

    const/4 v9, 0x2

    aput-object v0, v8, v9

    const/4 v9, 0x3

    aput-object v3, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_0

    .line 947
    :cond_b
    const-string v7, "MM-dd-yyyy"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_c

    .line 948
    const-string v7, "%s %s %te%s"

    const/4 v8, 0x4

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-virtual {v5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    aput-object v3, v8, v9

    const/4 v9, 0x2

    aput-object p1, v8, v9

    const/4 v9, 0x3

    aput-object v0, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_0

    .line 950
    :cond_c
    const-string v7, "yyyy-MM-dd"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_d

    .line 952
    const-string v7, "%s %s %te%s"

    const/4 v8, 0x4

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-virtual {v5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    aput-object v3, v8, v9

    const/4 v9, 0x2

    aput-object p1, v8, v9

    const/4 v9, 0x3

    aput-object v0, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_0

    .line 955
    :cond_d
    const-string v7, "%s %tY%s %s %te%s"

    const/4 v8, 0x6

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-virtual {v5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    aput-object p1, v8, v9

    const/4 v9, 0x2

    aput-object v6, v8, v9

    const/4 v9, 0x3

    aput-object v3, v8, v9

    const/4 v9, 0x4

    aput-object p1, v8, v9

    const/4 v9, 0x5

    aput-object v0, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_0
.end method

.method public static menuItemGrayByTitle(Landroid/content/Context;Landroid/view/Menu;IZ)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "menus"    # Landroid/view/Menu;
    .param p2, "stringId"    # I
    .param p3, "visiability"    # Z

    .prologue
    .line 2038
    const/4 v2, 0x0

    .line 2039
    .local v2, "menuSize":I
    const/4 v1, 0x0

    .line 2040
    .local v1, "menuItem":Landroid/view/MenuItem;
    const/4 v3, 0x0

    .line 2042
    .local v3, "title":Ljava/lang/CharSequence;
    if-nez p1, :cond_1

    .line 2057
    :cond_0
    :goto_0
    return-void

    .line 2046
    :cond_1
    invoke-interface {p1}, Landroid/view/Menu;->size()I

    move-result v2

    .line 2048
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v2, :cond_0

    .line 2049
    invoke-interface {p1, v0}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 2050
    invoke-interface {v1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v3

    .line 2052
    if-eqz v3, :cond_2

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2053
    invoke-interface {v1, p3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 2048
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public static parseSunSet(Ljava/lang/String;Ljava/util/Calendar;)[I
    .locals 8
    .param p0, "sunset"    # Ljava/lang/String;
    .param p1, "cal"    # Ljava/util/Calendar;

    .prologue
    const/16 v7, 0x15

    const/4 v4, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 2344
    invoke-virtual {p1, v4}, Ljava/util/Calendar;->get(I)I

    move-result v2

    add-int/lit8 v0, v2, 0x1

    .line 2345
    .local v0, "month":I
    const/4 v1, 0x0

    .line 2347
    .local v1, "nightTime":[I
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getHourAndMin(Ljava/lang/String;)[I

    move-result-object v1

    .line 2348
    if-eqz v1, :cond_2

    .line 2350
    aget v2, v1, v5

    if-ge v2, v7, :cond_0

    aget v2, v1, v5

    const/4 v3, 0x4

    if-ge v2, v3, :cond_1

    .line 2351
    :cond_0
    aput v7, v1, v5

    .line 2352
    aput v5, v1, v6

    .line 2353
    const-string v2, ""

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SS h:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget v4, v1, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", m:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget v4, v1, v6

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2369
    :cond_1
    :goto_0
    return-object v1

    .line 2359
    :cond_2
    const-string v2, ""

    const-string v3, "getSunSet() ::: nightTime is null "

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2360
    new-array v1, v4, [I

    .line 2361
    const/4 v2, 0x3

    if-lt v0, v2, :cond_3

    const/16 v2, 0xa

    if-ge v0, v2, :cond_3

    .line 2362
    const/16 v2, 0x13

    aput v2, v1, v5

    .line 2366
    :goto_1
    aput v5, v1, v6

    goto :goto_0

    .line 2364
    :cond_3
    const/16 v2, 0x12

    aput v2, v1, v5

    goto :goto_1
.end method

.method public static parseSunrise(Ljava/lang/String;Ljava/util/Calendar;)[I
    .locals 8
    .param p0, "sunrise"    # Ljava/lang/String;
    .param p1, "cal"    # Ljava/util/Calendar;

    .prologue
    const/4 v7, 0x4

    const/4 v4, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 2309
    invoke-virtual {p1, v4}, Ljava/util/Calendar;->get(I)I

    move-result v2

    add-int/lit8 v1, v2, 0x1

    .line 2310
    .local v1, "month":I
    const/4 v0, 0x0

    .line 2312
    .local v0, "dayTime":[I
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getHourAndMin(Ljava/lang/String;)[I

    move-result-object v0

    .line 2313
    if-eqz v0, :cond_2

    .line 2315
    aget v2, v0, v5

    if-lt v2, v7, :cond_0

    aget v2, v0, v5

    const/16 v3, 0x15

    if-lt v2, v3, :cond_1

    .line 2316
    :cond_0
    aput v7, v0, v5

    .line 2317
    aput v5, v0, v6

    .line 2318
    const-string v2, ""

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SR h:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget v4, v0, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", m:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget v4, v0, v6

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2334
    :cond_1
    :goto_0
    return-object v0

    .line 2324
    :cond_2
    const-string v2, ""

    const-string v3, "getSunrise() ::: dayTime is null "

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2325
    new-array v0, v4, [I

    .line 2326
    const/4 v2, 0x3

    if-lt v1, v2, :cond_3

    const/16 v2, 0xa

    if-ge v1, v2, :cond_3

    .line 2327
    const/4 v2, 0x6

    aput v2, v0, v5

    .line 2331
    :goto_1
    aput v5, v0, v6

    goto :goto_0

    .line 2329
    :cond_3
    const/4 v2, 0x7

    aput v2, v0, v5

    goto :goto_1
.end method

.method public static performClickGpsBtn(Landroid/app/Activity;Landroid/view/View;Landroid/app/Dialog;Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/LocationGetter;Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;Landroid/content/DialogInterface$OnDismissListener;Z)Landroid/app/Dialog;
    .locals 5
    .param p0, "context"    # Landroid/app/Activity;
    .param p1, "v"    # Landroid/view/View;
    .param p2, "dialog"    # Landroid/app/Dialog;
    .param p3, "lgetter"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/LocationGetter;
    .param p4, "confirmGpsDialogInteraction"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;
    .param p5, "confirmGpsDismissListener"    # Landroid/content/DialogInterface$OnDismissListener;
    .param p6, "COLLAB_MODE"    # Z

    .prologue
    const/16 v4, 0xbc0

    .line 3410
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getLocationCheck(Landroid/content/Context;)I

    move-result v0

    .line 3411
    .local v0, "locationSetting":I
    const-string v1, ""

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[pCGB] loSe = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3412
    const/16 v1, 0xbb9

    if-ne v0, v1, :cond_1

    if-nez p6, :cond_1

    .line 3413
    const v1, 0x7f0d0020

    invoke-static {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->toast(Landroid/content/Context;I)V

    .line 3449
    :cond_0
    :goto_0
    return-object p2

    .line 3414
    :cond_1
    const/16 v1, 0xbba

    if-ne v0, v1, :cond_2

    if-nez p6, :cond_2

    .line 3415
    const/16 v1, 0x3f5

    invoke-static {p0, v1, p4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->showDialog(Landroid/content/Context;ILcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;)Landroid/app/Dialog;

    move-result-object p2

    .line 3418
    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util$4;

    invoke-direct {v1, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util$4;-><init>(Landroid/view/View;)V

    invoke-virtual {p2, v1}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    goto :goto_0

    .line 3423
    :cond_2
    if-ne v0, v4, :cond_4

    .line 3424
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getPreferenceGPSOnlyDoNotShow(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_4

    if-nez p6, :cond_4

    .line 3426
    invoke-static {p0, p3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->showGPSonlyDialog(Landroid/app/Activity;Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/LocationGetter;)Landroid/app/Dialog;

    move-result-object p2

    .line 3428
    if-nez p5, :cond_3

    .line 3429
    new-instance p5, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util$5;

    .end local p5    # "confirmGpsDismissListener":Landroid/content/DialogInterface$OnDismissListener;
    invoke-direct {p5, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util$5;-><init>(Landroid/view/View;)V

    .line 3435
    .restart local p5    # "confirmGpsDismissListener":Landroid/content/DialogInterface$OnDismissListener;
    :cond_3
    invoke-virtual {p2, p5}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    goto :goto_0

    .line 3436
    :cond_4
    if-ne v0, v4, :cond_5

    .line 3437
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getPreferenceGPSOnlyDoNotShow(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_5

    if-nez p6, :cond_5

    .line 3438
    invoke-interface {p3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/LocationGetter;->getLocation()V

    goto :goto_0

    .line 3439
    :cond_5
    const/16 v1, 0xbbb

    if-ne v0, v1, :cond_6

    if-nez p6, :cond_6

    .line 3440
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->showGPSoffDialog(Landroid/app/Activity;)Landroid/app/Dialog;

    move-result-object p2

    .line 3441
    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util$6;

    invoke-direct {v1, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util$6;-><init>(Landroid/view/View;)V

    invoke-virtual {p2, v1}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    goto :goto_0

    .line 3446
    :cond_6
    const/16 v1, 0xbb8

    if-ne v0, v1, :cond_0

    .line 3447
    invoke-interface {p3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/LocationGetter;->getLocation()V

    goto :goto_0
.end method

.method private static registerNotiTime(Landroid/content/Context;IILjava/lang/Boolean;)V
    .locals 13
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "hour"    # I
    .param p2, "min"    # I
    .param p3, "value"    # Ljava/lang/Boolean;

    .prologue
    .line 2594
    const-string v2, "alarm"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/app/AlarmManager;

    .line 2595
    .local v8, "manager":Landroid/app/AlarmManager;
    new-instance v7, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;

    invoke-direct {v7, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2596
    .local v7, "intent":Landroid/content/Intent;
    const-string v2, "com.sec.android.widgetapp.ap.hero.accuweather.action.ALERT_NOTIFICATION"

    invoke-virtual {v7, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 2597
    const/4 v2, 0x1

    const/4 v4, 0x0

    invoke-static {p0, v2, v7, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v10

    .line 2599
    .local v10, "sender":Landroid/app/PendingIntent;
    new-instance v12, Ljava/util/GregorianCalendar;

    invoke-direct {v12}, Ljava/util/GregorianCalendar;-><init>()V

    .line 2600
    .local v12, "today":Ljava/util/GregorianCalendar;
    const/4 v2, 0x1

    invoke-virtual {v12, v2}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v1

    .line 2601
    .local v1, "year":I
    const/4 v2, 0x2

    invoke-virtual {v12, v2}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v2

    add-int/lit8 v9, v2, 0x1

    .line 2602
    .local v9, "month":I
    const/4 v2, 0x5

    invoke-virtual {v12, v2}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v3

    .line 2604
    .local v3, "day":I
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 2605
    .local v0, "calendar":Ljava/util/Calendar;
    invoke-virtual/range {p3 .. p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2606
    add-int/lit8 v2, v9, -0x1

    add-int/lit8 v3, v3, 0x1

    const/4 v6, 0x0

    move v4, p1

    move v5, p2

    invoke-virtual/range {v0 .. v6}, Ljava/util/Calendar;->set(IIIIII)V

    .line 2619
    .end local v3    # "day":I
    :goto_0
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    .line 2621
    .local v11, "setTime":Ljava/lang/Long;
    const/4 v2, 0x0

    invoke-virtual {v11}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v8, v2, v4, v5, v10}, Landroid/app/AlarmManager;->setExact(IJLandroid/app/PendingIntent;)V

    .line 2622
    return-void

    .line 2608
    .end local v11    # "setTime":Ljava/lang/Long;
    .restart local v3    # "day":I
    :cond_0
    add-int/lit8 v2, v9, -0x1

    const/4 v6, 0x0

    move v4, p1

    move v5, p2

    invoke-virtual/range {v0 .. v6}, Ljava/util/Calendar;->set(IIIIII)V

    goto :goto_0
.end method

.method public static removeDuplicatedItem(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 3551
    .local p0, "listLeft":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;>;"
    .local p1, "listRight":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;>;"
    if-eqz p0, :cond_0

    if-nez p1, :cond_2

    .line 3552
    :cond_0
    const-string v9, ""

    const-string v10, "rSI n"

    invoke-static {v9, v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3590
    :cond_1
    return-void

    .line 3556
    :cond_2
    const/4 v1, 0x0

    .line 3557
    .local v1, "cityInfoLeft":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;
    const/4 v2, 0x0

    .line 3559
    .local v2, "cityInfoRight":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 3560
    .local v6, "listLeftSize":I
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v7

    .line 3561
    .local v7, "listRightSize":I
    const/4 v3, 0x0

    .line 3563
    .local v3, "count":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-ge v4, v6, :cond_5

    .line 3564
    invoke-virtual {p0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "cityInfoLeft":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;
    check-cast v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;

    .line 3566
    .restart local v1    # "cityInfoLeft":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;
    const/4 v5, 0x0

    .local v5, "j":I
    :goto_1
    if-ge v5, v7, :cond_3

    .line 3567
    invoke-virtual {p1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "cityInfoRight":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;
    check-cast v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;

    .line 3568
    .restart local v2    # "cityInfoRight":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;
    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getLocation()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getLocation()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 3569
    const-string v9, "true"

    invoke-virtual {v2, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->setIsDuplicateItem(Ljava/lang/String;)V

    .line 3570
    add-int/lit8 v3, v3, 0x1

    .line 3563
    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 3566
    :cond_4
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 3576
    .end local v5    # "j":I
    :cond_5
    const/4 v0, 0x0

    .line 3578
    .local v0, "cityInfo":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;
    :cond_6
    :goto_2
    if-lez v3, :cond_1

    .line 3579
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v8

    .line 3580
    .local v8, "listSize":I
    const/4 v4, 0x0

    :goto_3
    if-ge v4, v8, :cond_6

    .line 3581
    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "cityInfo":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;
    check-cast v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;

    .line 3582
    .restart local v0    # "cityInfo":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;
    if-eqz v0, :cond_7

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getIsDuplicateItem()Ljava/lang/String;

    move-result-object v9

    const-string v10, "true"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 3583
    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 3584
    add-int/lit8 v3, v3, -0x1

    .line 3585
    goto :goto_2

    .line 3580
    :cond_7
    add-int/lit8 v4, v4, 0x1

    goto :goto_3
.end method

.method private static removePreferenceWidgetCount(Landroid/content/Context;Ljava/lang/String;)V
    .locals 9
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "whatWidget"    # Ljava/lang/String;

    .prologue
    .line 4133
    const/4 v5, 0x0

    .local v5, "widgetCount":I
    const/4 v4, 0x0

    .local v4, "surfaceWidgetCnt":I
    const/4 v3, 0x0

    .line 4134
    .local v3, "remoteViewWidgetCnt":I
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getPreferenceWidgetCount(Landroid/content/Context;)I

    move-result v2

    .line 4136
    .local v2, "prevCount":I
    const-string v6, "pref_widget_count"

    const/4 v7, 0x4

    invoke-virtual {p0, v6, v7}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 4138
    .local v1, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 4141
    .local v0, "edit":Landroid/content/SharedPreferences$Editor;
    const-string v6, "widget_count_surface"

    if-ne p1, v6, :cond_1

    .line 4142
    const/4 v6, 0x0

    invoke-interface {v0, p1, v6}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 4147
    :cond_0
    :goto_0
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 4150
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getPreferenceWidgetCount(Landroid/content/Context;)I

    move-result v5

    .line 4152
    invoke-static {p0, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->storeWidgetCountToDataBase(Landroid/content/Context;I)V

    .line 4154
    const-string v6, ""

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "remove PWC = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4155
    return-void

    .line 4143
    :cond_1
    const-string v6, "widget_count_remote"

    if-ne p1, v6, :cond_0

    .line 4144
    add-int/lit8 v6, v2, -0x1

    invoke-interface {v0, p1, v6}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    goto :goto_0
.end method

.method public static removePreferenceWidgetCountForRemoteWidget(Landroid/content/Context;)V
    .locals 1
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    .line 4121
    const-string v0, "widget_count_remote"

    invoke-static {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->removePreferenceWidgetCount(Landroid/content/Context;Ljava/lang/String;)V

    .line 4122
    return-void
.end method

.method public static removePreferenceWidgetCountForSurfaceWidget(Landroid/content/Context;)V
    .locals 1
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    .line 4113
    const-string v0, "widget_count_surface"

    invoke-static {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->removePreferenceWidgetCount(Landroid/content/Context;Ljava/lang/String;)V

    .line 4114
    return-void
.end method

.method public static replaceModelName(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "src"    # Ljava/lang/String;

    .prologue
    .line 2418
    const-string v0, ""

    .line 2419
    .local v0, "des":Ljava/lang/String;
    if-eqz p0, :cond_0

    .line 2420
    invoke-virtual {p0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, "SAMSUNG-"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 2423
    :cond_0
    return-object v0
.end method

.method public static setEnterOnTap(Z)V
    .locals 0
    .param p0, "is"    # Z

    .prologue
    .line 785
    sput-boolean p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->isOnTap:Z

    .line 786
    return-void
.end method

.method public static setFirstLaunch(Landroid/content/Context;Z)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "isFirst"    # Z

    .prologue
    .line 2649
    const-string v1, "config"

    const/4 v2, 0x4

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 2650
    .local v0, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    sget-object v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->SHARED_PREF_FIRST:Ljava/lang/String;

    invoke-interface {v1, v2, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v1

    return v1
.end method

.method public static setIsAgreeUseLocation(Landroid/content/Context;Z)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "isShow"    # Z

    .prologue
    .line 206
    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->setIsAgreeUseLocation(Landroid/content/Context;ZZ)V

    .line 207
    return-void
.end method

.method public static setIsAgreeUseLocation(Landroid/content/Context;ZZ)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "isShow"    # Z
    .param p2, "notifyToDaemon"    # Z

    .prologue
    .line 213
    const/4 v0, 0x0

    .line 214
    .local v0, "useLocationValue":I
    const/4 v1, 0x1

    if-ne p1, v1, :cond_0

    .line 215
    const/4 v0, 0x1

    .line 217
    :cond_0
    invoke-static {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateShowUseLocationPopup(Landroid/content/Context;I)I

    .line 219
    if-eqz p2, :cond_1

    .line 220
    invoke-static {p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DaemonInterface;->notifyAgreementToDaemon(Landroid/content/Context;Z)V

    .line 221
    :cond_1
    return-void
.end method

.method public static setNotiTime(Landroid/content/Context;IILjava/lang/Boolean;)V
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "hour"    # I
    .param p2, "min"    # I
    .param p3, "value"    # Ljava/lang/Boolean;

    .prologue
    const/4 v6, 0x0

    .line 2573
    const-string v2, ""

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "sNT : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2574
    new-instance v8, Ljava/util/GregorianCalendar;

    invoke-direct {v8}, Ljava/util/GregorianCalendar;-><init>()V

    .line 2575
    .local v8, "today":Ljava/util/GregorianCalendar;
    const/4 v2, 0x1

    invoke-virtual {v8, v2}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v1

    .line 2576
    .local v1, "year":I
    const/4 v2, 0x2

    invoke-virtual {v8, v2}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v2

    add-int/lit8 v7, v2, 0x1

    .line 2577
    .local v7, "month":I
    const/4 v2, 0x5

    invoke-virtual {v8, v2}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v3

    .line 2579
    .local v3, "day":I
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 2581
    .local v0, "today_cal":Ljava/util/Calendar;
    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2582
    add-int/lit8 v2, v7, -0x1

    add-int/lit8 v3, v3, 0x1

    move v4, p1

    move v5, p2

    invoke-virtual/range {v0 .. v6}, Ljava/util/Calendar;->set(IIIIII)V

    .line 2587
    .end local v3    # "day":I
    :goto_0
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->unregisterNotiTime(Landroid/content/Context;)V

    .line 2588
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    invoke-static {p0, v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateNotificationTimeSettings(Landroid/content/Context;J)I

    .line 2589
    invoke-static {p0, p1, p2, p3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->registerNotiTime(Landroid/content/Context;IILjava/lang/Boolean;)V

    .line 2590
    return-void

    .line 2584
    .restart local v3    # "day":I
    :cond_0
    add-int/lit8 v2, v7, -0x1

    move v4, p1

    move v5, p2

    invoke-virtual/range {v0 .. v6}, Ljava/util/Calendar;->set(IIIIII)V

    goto :goto_0
.end method

.method public static setOtherRefreshOKToPref(Landroid/content/Context;Z)V
    .locals 4
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "otherRefreshOK"    # Z

    .prologue
    .line 3217
    const-string v2, "updatetime"

    const/4 v3, 0x4

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 3218
    .local v1, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 3219
    .local v0, "edit":Landroid/content/SharedPreferences$Editor;
    const-string v2, "other_refresh"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 3220
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 3221
    return-void
.end method

.method public static setOutterGlowNOutterShadowForTransWidget(Landroid/widget/RemoteViews;ILandroid/content/Context;IZ)V
    .locals 11
    .param p0, "views"    # Landroid/widget/RemoteViews;
    .param p1, "targetId"    # I
    .param p2, "con"    # Landroid/content/Context;
    .param p3, "shadowOpacity"    # I
    .param p4, "isEffectOn"    # Z

    .prologue
    .line 3519
    const/high16 v4, 0x40000000    # 2.0f

    .line 3520
    .local v4, "shadowSoftness":F
    const/high16 v3, 0x40000000    # 2.0f

    .line 3522
    .local v3, "shadowOffset":F
    invoke-static {p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getSystemResolution(Landroid/content/Context;)I

    move-result v7

    .line 3524
    .local v7, "resolution":I
    const v0, 0x1fa400

    if-ne v7, v0, :cond_2

    .line 3525
    const/high16 v3, 0x40000000    # 2.0f

    .line 3534
    :cond_0
    :goto_0
    const/high16 v10, 0x3f800000    # 1.0f

    .line 3535
    .local v10, "strokeSize":F
    const/high16 v8, -0x1000000

    .line 3536
    .local v8, "strokeColor":I
    const v9, 0x3ee66666    # 0.45f

    .line 3538
    .local v9, "strokeOffset":F
    invoke-virtual {p0, p1}, Landroid/widget/RemoteViews;->clearAllTextEffect(I)V

    .line 3539
    if-eqz p2, :cond_1

    .line 3540
    const/high16 v2, 0x42b40000    # 90.0f

    const/high16 v5, -0x1000000

    invoke-static {p3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getShadowPercent(I)F

    move-result v6

    move-object v0, p0

    move v1, p1

    invoke-virtual/range {v0 .. v6}, Landroid/widget/RemoteViews;->addOuterShadowTextEffect(IFFFIF)V

    .line 3541
    invoke-virtual {p0, p1, v10, v8, v9}, Landroid/widget/RemoteViews;->addStrokeTextEffect(IFIF)V

    .line 3543
    :cond_1
    return-void

    .line 3526
    .end local v8    # "strokeColor":I
    .end local v9    # "strokeOffset":F
    .end local v10    # "strokeSize":F
    :cond_2
    const v0, 0xe1000

    if-ne v7, v0, :cond_3

    .line 3527
    const/high16 v3, 0x3f800000    # 1.0f

    goto :goto_0

    .line 3528
    :cond_3
    const v0, 0x7e900

    if-ne v7, v0, :cond_4

    .line 3529
    const/high16 v3, 0x3f800000    # 1.0f

    goto :goto_0

    .line 3530
    :cond_4
    const v0, 0x384000

    if-ne v7, v0, :cond_0

    .line 3531
    const/high16 v3, 0x40400000    # 3.0f

    goto :goto_0
.end method

.method public static setOutterGlowNOutterShadowForWidget(Landroid/widget/RemoteViews;ILandroid/content/Context;IZ)V
    .locals 11
    .param p0, "views"    # Landroid/widget/RemoteViews;
    .param p1, "targetId"    # I
    .param p2, "con"    # Landroid/content/Context;
    .param p3, "shadowOpacity"    # I
    .param p4, "isEffectOn"    # Z

    .prologue
    .line 3484
    invoke-static {p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getSystemResolution(Landroid/content/Context;)I

    move-result v7

    .line 3486
    .local v7, "resolution":I
    const/high16 v4, 0x40800000    # 4.0f

    .line 3487
    .local v4, "shadowSoftness":F
    const/high16 v3, 0x3f800000    # 1.0f

    .line 3488
    .local v3, "shadowOffset":F
    const/high16 v10, 0x3f800000    # 1.0f

    .line 3489
    .local v10, "strokeSize":F
    const/high16 v8, -0x1000000

    .line 3490
    .local v8, "strokeColor":I
    const v9, 0x3ee66666    # 0.45f

    .line 3492
    .local v9, "strokeOffset":F
    const v0, 0x1fa400

    if-ne v7, v0, :cond_2

    .line 3493
    const/high16 v3, 0x40000000    # 2.0f

    .line 3501
    :cond_0
    :goto_0
    invoke-virtual {p0, p1}, Landroid/widget/RemoteViews;->clearAllTextEffect(I)V

    .line 3502
    if-eqz p2, :cond_1

    .line 3503
    const/high16 v2, 0x42b40000    # 90.0f

    const/high16 v5, -0x1000000

    invoke-static {p3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getShadowPercent(I)F

    move-result v6

    move-object v0, p0

    move v1, p1

    invoke-virtual/range {v0 .. v6}, Landroid/widget/RemoteViews;->addOuterShadowTextEffect(IFFFIF)V

    .line 3504
    invoke-virtual {p0, p1, v10, v8, v9}, Landroid/widget/RemoteViews;->addStrokeTextEffect(IFIF)V

    .line 3507
    :cond_1
    return-void

    .line 3494
    :cond_2
    const v0, 0xe1000

    if-ne v7, v0, :cond_3

    .line 3495
    const/high16 v3, 0x3f800000    # 1.0f

    goto :goto_0

    .line 3496
    :cond_3
    const v0, 0x7e900

    if-ne v7, v0, :cond_4

    .line 3497
    const/high16 v3, 0x3f800000    # 1.0f

    goto :goto_0

    .line 3498
    :cond_4
    const v0, 0x384000

    if-ne v7, v0, :cond_0

    .line 3499
    const/high16 v3, 0x40400000    # 3.0f

    goto :goto_0
.end method

.method public static setPrecipitaionString(Landroid/content/Context;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;Z)Ljava/lang/String;
    .locals 8
    .param p0, "mCtx"    # Landroid/content/Context;
    .param p1, "today_Info"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;
    .param p2, "search_Info"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;
    .param p3, "isDay"    # Z

    .prologue
    .line 2957
    const/4 v4, 0x0

    .local v4, "notiRain":I
    const/4 v5, 0x0

    .local v5, "notiSnow":I
    const/4 v2, 0x0

    .local v2, "notiHail":I
    const/4 v3, 0x0

    .line 2958
    .local v3, "notiPrecipitation":I
    const-string v1, ""

    .line 2960
    .local v1, "mPrecipitation":Ljava/lang/String;
    if-eqz p1, :cond_8

    .line 2961
    if-eqz p3, :cond_7

    .line 2962
    invoke-virtual {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getDayRainProbability()I

    move-result v4

    .line 2963
    invoke-virtual {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getDaySnowProbability()I

    move-result v5

    .line 2964
    invoke-virtual {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getDayHailProbability()I

    move-result v2

    .line 2965
    invoke-virtual {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getDayPrecipitationProbability()I

    move-result v3

    .line 2986
    :cond_0
    :goto_0
    if-nez p1, :cond_1

    if-eqz p2, :cond_6

    .line 2987
    :cond_1
    const/4 v0, 0x0

    .line 2988
    .local v0, "flag":I
    if-eqz v4, :cond_2

    .line 2989
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0d00ac

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2990
    add-int/lit8 v0, v0, 0x1

    .line 2992
    :cond_2
    if-eqz v5, :cond_3

    .line 2993
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0d00ad

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2994
    add-int/lit8 v0, v0, 0x1

    .line 2997
    :cond_3
    if-eqz v2, :cond_4

    .line 2998
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0d00ae

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2999
    add-int/lit8 v0, v0, 0x1

    .line 3002
    :cond_4
    if-eqz v0, :cond_5

    const/4 v6, 0x1

    if-le v0, v6, :cond_6

    .line 3004
    :cond_5
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0d00af

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 3008
    .end local v0    # "flag":I
    :cond_6
    return-object v1

    .line 2967
    :cond_7
    invoke-virtual {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getNightRainProbability()I

    move-result v4

    .line 2968
    invoke-virtual {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getNightSnowProbability()I

    move-result v5

    .line 2969
    invoke-virtual {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getNightHailProbability()I

    move-result v2

    .line 2970
    invoke-virtual {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getNightPrecipitationProbability()I

    move-result v3

    goto :goto_0

    .line 2973
    :cond_8
    if-eqz p2, :cond_0

    .line 2974
    if-eqz p3, :cond_9

    .line 2975
    invoke-virtual {p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getDayRainProbability()I

    move-result v4

    .line 2976
    invoke-virtual {p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getDaySnowProbability()I

    move-result v5

    .line 2977
    invoke-virtual {p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getDayHailProbability()I

    move-result v2

    .line 2978
    invoke-virtual {p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getDayPrecipitationProbability()I

    move-result v3

    goto :goto_0

    .line 2980
    :cond_9
    invoke-virtual {p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getNightRainProbability()I

    move-result v4

    .line 2981
    invoke-virtual {p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getNightSnowProbability()I

    move-result v5

    .line 2982
    invoke-virtual {p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getNightHailProbability()I

    move-result v2

    .line 2983
    invoke-virtual {p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getNightPrecipitationProbability()I

    move-result v3

    goto :goto_0
.end method

.method public static setPrecipitaionValue(Landroid/content/Context;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;Z)I
    .locals 7
    .param p0, "mCtx"    # Landroid/content/Context;
    .param p1, "today_Info"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;
    .param p2, "search_Info"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;
    .param p3, "isDay"    # Z

    .prologue
    .line 3012
    const/4 v4, 0x0

    .local v4, "notiRain":I
    const/4 v5, 0x0

    .local v5, "notiSnow":I
    const/4 v2, 0x0

    .local v2, "notiHail":I
    const/4 v3, 0x0

    .line 3013
    .local v3, "notiPrecipitation":I
    const/4 v1, 0x0

    .line 3015
    .local v1, "mPrecipitationValue":I
    if-eqz p1, :cond_8

    .line 3016
    if-eqz p3, :cond_7

    .line 3017
    invoke-virtual {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getDayRainProbability()I

    move-result v4

    .line 3018
    invoke-virtual {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getDaySnowProbability()I

    move-result v5

    .line 3019
    invoke-virtual {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getDayHailProbability()I

    move-result v2

    .line 3020
    invoke-virtual {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getDayPrecipitationProbability()I

    move-result v3

    .line 3042
    :cond_0
    :goto_0
    if-nez p1, :cond_1

    if-eqz p2, :cond_6

    .line 3043
    :cond_1
    const/4 v0, 0x0

    .line 3044
    .local v0, "flag":I
    if-eqz v4, :cond_2

    .line 3045
    move v1, v4

    .line 3046
    add-int/lit8 v0, v0, 0x1

    .line 3049
    :cond_2
    if-eqz v5, :cond_3

    .line 3050
    move v1, v5

    .line 3051
    add-int/lit8 v0, v0, 0x1

    .line 3054
    :cond_3
    if-eqz v2, :cond_4

    .line 3055
    move v1, v2

    .line 3056
    add-int/lit8 v0, v0, 0x1

    .line 3059
    :cond_4
    if-eqz v0, :cond_5

    const/4 v6, 0x1

    if-le v0, v6, :cond_6

    .line 3061
    :cond_5
    move v1, v3

    .line 3065
    .end local v0    # "flag":I
    :cond_6
    return v1

    .line 3022
    :cond_7
    invoke-virtual {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getNightRainProbability()I

    move-result v4

    .line 3023
    invoke-virtual {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getNightSnowProbability()I

    move-result v5

    .line 3024
    invoke-virtual {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getNightHailProbability()I

    move-result v2

    .line 3025
    invoke-virtual {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getNightPrecipitationProbability()I

    move-result v3

    goto :goto_0

    .line 3028
    :cond_8
    if-eqz p2, :cond_0

    .line 3029
    if-eqz p3, :cond_9

    .line 3030
    invoke-virtual {p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getDayRainProbability()I

    move-result v4

    .line 3031
    invoke-virtual {p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getDaySnowProbability()I

    move-result v5

    .line 3032
    invoke-virtual {p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getDayHailProbability()I

    move-result v2

    .line 3033
    invoke-virtual {p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getDayPrecipitationProbability()I

    move-result v3

    goto :goto_0

    .line 3035
    :cond_9
    invoke-virtual {p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getNightRainProbability()I

    move-result v4

    .line 3036
    invoke-virtual {p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getNightSnowProbability()I

    move-result v5

    .line 3037
    invoke-virtual {p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getNightHailProbability()I

    move-result v2

    .line 3038
    invoke-virtual {p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getNightPrecipitationProbability()I

    move-result v3

    goto :goto_0
.end method

.method public static setShowChargerPopup(Landroid/content/Context;Z)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "isShow"    # Z

    .prologue
    .line 190
    const-string v1, "config"

    const/4 v2, 0x4

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 191
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "SHOW_CHARGER_POPUP"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 192
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 193
    return-void
.end method

.method public static setTodayWeatherIcon(IIZ)I
    .locals 7
    .param p0, "iconnum"    # I
    .param p1, "iconType"    # I
    .param p2, "isDay"    # Z

    .prologue
    const/16 v6, 0x13

    const/16 v5, 0x12

    const/4 v4, 0x5

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 1354
    const/4 v0, 0x0

    .line 1356
    .local v0, "iconList":[I
    packed-switch p1, :pswitch_data_0

    .line 1401
    :pswitch_0
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->weatherIconForDetails:[I

    .line 1405
    :goto_0
    packed-switch p0, :pswitch_data_1

    .line 1541
    :pswitch_1
    aget v1, v0, v2

    .line 1545
    .local v1, "imageId":I
    :goto_1
    return v1

    .line 1358
    .end local v1    # "imageId":I
    :pswitch_2
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->weatherIconForWidget:[I

    .line 1359
    goto :goto_0

    .line 1362
    :pswitch_3
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->weatherIconForDetails:[I

    .line 1363
    goto :goto_0

    .line 1366
    :pswitch_4
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->weatherIconSmall:[I

    .line 1367
    goto :goto_0

    .line 1370
    :pswitch_5
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->weatherIconForList:[I

    .line 1371
    goto :goto_0

    .line 1378
    :pswitch_6
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->weatherMapIcon:[I

    .line 1379
    goto :goto_0

    .line 1382
    :pswitch_7
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->weatherMapIconNew:[I

    .line 1383
    goto :goto_0

    .line 1387
    :pswitch_8
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->weatherEasyModeWidget:[I

    .line 1388
    goto :goto_0

    .line 1391
    :pswitch_9
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->weatherIconMore:[I

    .line 1392
    goto :goto_0

    .line 1394
    :pswitch_a
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/remoteview/RvUtils;->weatherSubWidgetIcon_4x2:[I

    .line 1395
    goto :goto_0

    .line 1398
    :pswitch_b
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/remoteview/RvUtils;->weatherSubWidgetIcon_4x1:[I

    .line 1399
    goto :goto_0

    .line 1408
    :pswitch_c
    if-eq p1, v3, :cond_1

    if-eq p1, v4, :cond_1

    .line 1410
    if-ne p2, v2, :cond_0

    aget v1, v0, v2

    .restart local v1    # "imageId":I
    :goto_2
    goto :goto_1

    .end local v1    # "imageId":I
    :cond_0
    aget v1, v0, v5

    goto :goto_2

    .line 1413
    :cond_1
    aget v1, v0, v2

    .line 1416
    .restart local v1    # "imageId":I
    goto :goto_1

    .line 1421
    .end local v1    # "imageId":I
    :pswitch_d
    if-eq p1, v3, :cond_3

    if-eq p1, v4, :cond_3

    .line 1423
    if-ne p2, v2, :cond_2

    aget v1, v0, v3

    .restart local v1    # "imageId":I
    :goto_3
    goto :goto_1

    .end local v1    # "imageId":I
    :cond_2
    aget v1, v0, v6

    goto :goto_3

    .line 1425
    :cond_3
    aget v1, v0, v3

    .line 1427
    .restart local v1    # "imageId":I
    goto :goto_1

    .line 1433
    .end local v1    # "imageId":I
    :pswitch_e
    const/4 v2, 0x3

    aget v1, v0, v2

    .line 1434
    .restart local v1    # "imageId":I
    goto :goto_1

    .line 1437
    .end local v1    # "imageId":I
    :pswitch_f
    aget v1, v0, v4

    .line 1438
    .restart local v1    # "imageId":I
    goto :goto_1

    .line 1444
    .end local v1    # "imageId":I
    :pswitch_10
    const/4 v2, 0x6

    aget v1, v0, v2

    .line 1445
    .restart local v1    # "imageId":I
    goto :goto_1

    .line 1448
    .end local v1    # "imageId":I
    :pswitch_11
    if-eq p1, v3, :cond_5

    if-eq p1, v4, :cond_5

    .line 1450
    if-ne p2, v2, :cond_4

    const/4 v2, 0x7

    aget v1, v0, v2

    .restart local v1    # "imageId":I
    :goto_4
    goto :goto_1

    .end local v1    # "imageId":I
    :cond_4
    const/4 v2, 0x6

    aget v1, v0, v2

    goto :goto_4

    .line 1452
    :cond_5
    const/4 v2, 0x7

    aget v1, v0, v2

    .line 1454
    .restart local v1    # "imageId":I
    goto :goto_1

    .line 1460
    .end local v1    # "imageId":I
    :pswitch_12
    const/16 v2, 0x8

    aget v1, v0, v2

    .line 1461
    .restart local v1    # "imageId":I
    goto :goto_1

    .line 1464
    .end local v1    # "imageId":I
    :pswitch_13
    if-eq p1, v3, :cond_7

    if-eq p1, v4, :cond_7

    .line 1466
    if-ne p2, v2, :cond_6

    const/16 v2, 0x9

    aget v1, v0, v2

    .restart local v1    # "imageId":I
    :goto_5
    goto :goto_1

    .end local v1    # "imageId":I
    :cond_6
    const/16 v2, 0x8

    aget v1, v0, v2

    goto :goto_5

    .line 1468
    :cond_7
    const/16 v2, 0x9

    aget v1, v0, v2

    .line 1470
    .restart local v1    # "imageId":I
    goto :goto_1

    .line 1473
    .end local v1    # "imageId":I
    :pswitch_14
    const/4 v2, 0x4

    aget v1, v0, v2

    .line 1474
    .restart local v1    # "imageId":I
    goto :goto_1

    .line 1479
    .end local v1    # "imageId":I
    :pswitch_15
    const/16 v2, 0xa

    aget v1, v0, v2

    .line 1480
    .restart local v1    # "imageId":I
    goto :goto_1

    .line 1483
    .end local v1    # "imageId":I
    :pswitch_16
    if-eq p1, v3, :cond_9

    if-eq p1, v4, :cond_9

    .line 1485
    if-ne p2, v2, :cond_8

    const/16 v2, 0xb

    aget v1, v0, v2

    .restart local v1    # "imageId":I
    :goto_6
    goto/16 :goto_1

    .end local v1    # "imageId":I
    :cond_8
    const/16 v2, 0xa

    aget v1, v0, v2

    goto :goto_6

    .line 1487
    :cond_9
    const/16 v2, 0xb

    aget v1, v0, v2

    .line 1489
    .restart local v1    # "imageId":I
    goto/16 :goto_1

    .line 1494
    .end local v1    # "imageId":I
    :pswitch_17
    const/16 v2, 0xc

    aget v1, v0, v2

    .line 1495
    .restart local v1    # "imageId":I
    goto/16 :goto_1

    .line 1498
    .end local v1    # "imageId":I
    :pswitch_18
    const/16 v2, 0xd

    aget v1, v0, v2

    .line 1499
    .restart local v1    # "imageId":I
    goto/16 :goto_1

    .line 1504
    .end local v1    # "imageId":I
    :pswitch_19
    const/16 v2, 0xe

    aget v1, v0, v2

    .line 1505
    .restart local v1    # "imageId":I
    goto/16 :goto_1

    .line 1508
    .end local v1    # "imageId":I
    :pswitch_1a
    const/16 v2, 0xf

    aget v1, v0, v2

    .line 1509
    .restart local v1    # "imageId":I
    goto/16 :goto_1

    .line 1512
    .end local v1    # "imageId":I
    :pswitch_1b
    const/16 v2, 0x10

    aget v1, v0, v2

    .line 1513
    .restart local v1    # "imageId":I
    goto/16 :goto_1

    .line 1516
    .end local v1    # "imageId":I
    :pswitch_1c
    const/16 v2, 0x11

    aget v1, v0, v2

    .line 1517
    .restart local v1    # "imageId":I
    goto/16 :goto_1

    .line 1521
    .end local v1    # "imageId":I
    :pswitch_1d
    if-eq p1, v3, :cond_b

    if-eq p1, v4, :cond_b

    .line 1523
    if-ne p2, v2, :cond_a

    aget v1, v0, v2

    .restart local v1    # "imageId":I
    :goto_7
    goto/16 :goto_1

    .end local v1    # "imageId":I
    :cond_a
    aget v1, v0, v5

    goto :goto_7

    .line 1525
    :cond_b
    aget v1, v0, v5

    .line 1527
    .restart local v1    # "imageId":I
    goto/16 :goto_1

    .line 1532
    .end local v1    # "imageId":I
    :pswitch_1e
    if-eq p1, v3, :cond_d

    if-eq p1, v4, :cond_d

    .line 1534
    if-ne p2, v2, :cond_c

    aget v1, v0, v3

    .restart local v1    # "imageId":I
    :goto_8
    goto/16 :goto_1

    .end local v1    # "imageId":I
    :cond_c
    aget v1, v0, v6

    goto :goto_8

    .line 1536
    :cond_d
    aget v1, v0, v6

    .line 1538
    .restart local v1    # "imageId":I
    goto/16 :goto_1

    .line 1356
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_6
        :pswitch_8
        :pswitch_9
        :pswitch_7
        :pswitch_0
        :pswitch_a
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_b
    .end packed-switch

    .line 1405
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_c
        :pswitch_c
        :pswitch_d
        :pswitch_d
        :pswitch_d
        :pswitch_e
        :pswitch_e
        :pswitch_e
        :pswitch_1
        :pswitch_1
        :pswitch_f
        :pswitch_10
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_17
        :pswitch_19
        :pswitch_19
        :pswitch_19
        :pswitch_1
        :pswitch_1
        :pswitch_18
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1d
        :pswitch_1e
        :pswitch_1e
        :pswitch_1e
        :pswitch_e
        :pswitch_10
        :pswitch_10
        :pswitch_12
        :pswitch_12
        :pswitch_15
        :pswitch_17
    .end packed-switch
.end method

.method public static setUpdateTimeToPref(Landroid/content/Context;Z)V
    .locals 9
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "reset"    # Z

    .prologue
    .line 3198
    const-wide/32 v0, 0x1b7740

    .line 3199
    .local v0, "THIRTY_MIN":J
    const-string v4, "updatetime"

    const/4 v5, 0x4

    invoke-virtual {p0, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 3200
    .local v3, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 3201
    .local v2, "edit":Landroid/content/SharedPreferences$Editor;
    if-eqz p1, :cond_0

    .line 3202
    const-string v4, "fail_updatetime"

    const-wide/16 v5, 0x0

    invoke-interface {v2, v4, v5, v6}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 3206
    :goto_0
    const-string v4, ""

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "set UT pref:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3207
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 3208
    return-void

    .line 3204
    :cond_0
    const-string v4, "fail_updatetime"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    const-wide/32 v7, 0x1b7740

    add-long/2addr v5, v7

    invoke-interface {v2, v4, v5, v6}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    goto :goto_0
.end method

.method public static setUpdateTimeToPref(Landroid/content/Context;ZZ)V
    .locals 9
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "isFristRetry"    # Z
    .param p2, "reset"    # Z

    .prologue
    .line 3183
    const-wide/32 v0, 0x1b7740

    .line 3184
    .local v0, "THIRTY_MIN":J
    const-string v4, "updatetime"

    const/4 v5, 0x4

    invoke-virtual {p0, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 3185
    .local v3, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 3186
    .local v2, "edit":Landroid/content/SharedPreferences$Editor;
    if-eqz p2, :cond_0

    .line 3187
    const-string v4, "fail_updatetime"

    const-wide/16 v5, 0x0

    invoke-interface {v2, v4, v5, v6}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 3193
    :goto_0
    const-string v4, ""

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "set UT pref:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3194
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 3195
    return-void

    .line 3188
    :cond_0
    if-eqz p1, :cond_1

    .line 3189
    const-string v4, "fail_updatetime"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-interface {v2, v4, v5, v6}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    .line 3191
    :cond_1
    const-string v4, "fail_updatetime"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    const-wide/32 v7, 0x1b7740

    add-long/2addr v5, v7

    invoke-interface {v2, v4, v5, v6}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    goto :goto_0
.end method

.method public static setWidgetMode(Landroid/content/Context;I)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "mode"    # I

    .prologue
    .line 2655
    const-string v1, "config"

    const/4 v2, 0x4

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 2656
    .local v0, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "widget_mode"

    invoke-interface {v1, v2, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v1

    return v1
.end method

.method public static showGPSoffDialog(Landroid/app/Activity;)Landroid/app/Dialog;
    .locals 2
    .param p0, "context"    # Landroid/app/Activity;

    .prologue
    .line 3363
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util$1;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util$1;-><init>(Landroid/app/Activity;)V

    .line 3371
    .local v0, "dialogInteraction":Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;
    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->showGPSoffDialog(Landroid/app/Activity;Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/Dialog;

    move-result-object v1

    return-object v1
.end method

.method public static showGPSoffDialog(Landroid/app/Activity;Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/Dialog;
    .locals 2
    .param p0, "context"    # Landroid/app/Activity;
    .param p1, "lDismissListener"    # Landroid/content/DialogInterface$OnDismissListener;

    .prologue
    .line 3375
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util$2;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util$2;-><init>(Landroid/app/Activity;)V

    .line 3383
    .local v0, "dialogInteraction":Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;
    invoke-static {p0, v0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->showGPSoffDialog(Landroid/app/Activity;Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/Dialog;

    move-result-object v1

    return-object v1
.end method

.method public static showGPSoffDialog(Landroid/app/Activity;Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;)Landroid/app/Dialog;
    .locals 1
    .param p0, "context"    # Landroid/app/Activity;
    .param p1, "dialogInteraction"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;

    .prologue
    .line 3359
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->showGPSoffDialog(Landroid/app/Activity;Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method

.method public static showGPSoffDialog(Landroid/app/Activity;Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/Dialog;
    .locals 2
    .param p0, "context"    # Landroid/app/Activity;
    .param p1, "dialogInteraction"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;
    .param p2, "lDismissListener"    # Landroid/content/DialogInterface$OnDismissListener;

    .prologue
    .line 3348
    const/16 v1, 0x3f6

    invoke-static {p0, v1, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->showDialog(Landroid/content/Context;ILcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;)Landroid/app/Dialog;

    move-result-object v0

    .line 3351
    .local v0, "dialog":Landroid/app/Dialog;
    if-eqz p2, :cond_0

    .line 3352
    invoke-virtual {v0, p2}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 3354
    :cond_0
    return-object v0
.end method

.method public static showGPSonlyDialog(Landroid/app/Activity;Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/LocationGetter;)Landroid/app/Dialog;
    .locals 3
    .param p0, "context"    # Landroid/app/Activity;
    .param p1, "lgetter"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/LocationGetter;

    .prologue
    .line 3387
    const/16 v1, 0x400

    new-instance v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util$3;

    invoke-direct {v2, p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util$3;-><init>(Landroid/app/Activity;Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/LocationGetter;)V

    invoke-static {p0, v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->showDialog(Landroid/content/Context;ILcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;)Landroid/app/Dialog;

    move-result-object v0

    .line 3404
    .local v0, "dialog":Landroid/app/Dialog;
    return-object v0
.end method

.method public static showUseCurrentLocation(Landroid/content/Context;Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;)Landroid/app/Dialog;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "dialogInteraction"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;

    .prologue
    .line 3341
    const/16 v1, 0x402

    invoke-static {p0, v1, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->showDialog(Landroid/content/Context;ILcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;)Landroid/app/Dialog;

    move-result-object v0

    .line 3343
    .local v0, "dialog":Landroid/app/Dialog;
    return-object v0
.end method

.method public static simEnabled(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 172
    const-string v1, "phone"

    .line 173
    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 174
    .local v0, "tm":Landroid/telephony/TelephonyManager;
    const/4 v1, 0x5

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 175
    const/4 v1, 0x1

    .line 177
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static startLocationSettingActivity(Landroid/app/Activity;)V
    .locals 4
    .param p0, "context"    # Landroid/app/Activity;

    .prologue
    .line 2024
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.LOCATION_SOURCE_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2027
    .local v0, "locationConsetIntent":Landroid/content/Intent;
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xe

    if-ge v1, v2, :cond_0

    .line 2030
    invoke-virtual {p0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "network"

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$Secure;->setLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;Z)V

    .line 2033
    :cond_0
    const/16 v1, 0x4b3

    invoke-virtual {p0, v0, v1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 2034
    return-void
.end method

.method private static storeWidgetCountToDataBase(Landroid/content/Context;I)V
    .locals 6
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "widgetCount"    # I

    .prologue
    .line 4164
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 4166
    .local v0, "cp":Landroid/content/ContentResolver;
    if-eqz v0, :cond_0

    .line 4167
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 4168
    .local v1, "data":Landroid/content/ContentValues;
    const-string v3, "WIDGET_COUNT"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4170
    :try_start_0
    sget-object v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_SETTING_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v0, v3, v1, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 4172
    const-string v3, ""

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Store PWC = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getPreferenceWidgetCount(Landroid/content/Context;)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 4178
    .end local v1    # "data":Landroid/content/ContentValues;
    :cond_0
    :goto_0
    return-void

    .line 4174
    .restart local v1    # "data":Landroid/content/ContentValues;
    :catch_0
    move-exception v2

    .line 4175
    .local v2, "e":Ljava/lang/Exception;
    const-string v3, ""

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Store PWC Exception "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static toHexString([B)Ljava/lang/String;
    .locals 6
    .param p0, "bytes"    # [B

    .prologue
    const/16 v5, 0x10

    .line 964
    if-nez p0, :cond_0

    .line 965
    const/4 v2, 0x0

    .line 973
    :goto_0
    return-object v2

    .line 968
    :cond_0
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 969
    .local v1, "result":Ljava/lang/StringBuffer;
    array-length v3, p0

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v3, :cond_1

    aget-byte v0, p0, v2

    .line 970
    .local v0, "b":B
    and-int/lit16 v4, v0, 0xf0

    shr-int/lit8 v4, v4, 0x4

    invoke-static {v4, v5}, Ljava/lang/Integer;->toString(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 971
    and-int/lit8 v4, v0, 0xf

    invoke-static {v4, v5}, Ljava/lang/Integer;->toString(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 969
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 973
    .end local v0    # "b":B
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public static toast(Landroid/content/Context;I)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "stringResource"    # I

    .prologue
    .line 535
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->toast:Landroid/widget/Toast;

    if-nez v0, :cond_0

    .line 536
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->toast:Landroid/widget/Toast;

    .line 538
    :cond_0
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->toast:Landroid/widget/Toast;

    invoke-virtual {v0, p1}, Landroid/widget/Toast;->setText(I)V

    .line 539
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->toast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 540
    return-void
.end method

.method public static toast(Landroid/content/Context;ILandroid/os/Handler;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "stringResource"    # I
    .param p2, "handler"    # Landroid/os/Handler;

    .prologue
    const/4 v2, 0x0

    .line 551
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->toast:Landroid/widget/Toast;

    if-nez v0, :cond_0

    .line 552
    invoke-static {p0, p1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->toast:Landroid/widget/Toast;

    .line 554
    :cond_0
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->toast:Landroid/widget/Toast;

    invoke-virtual {v0, p1}, Landroid/widget/Toast;->setText(I)V

    .line 555
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->toast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 557
    if-eqz p2, :cond_1

    .line 558
    const-wide/16 v0, 0x7d0

    invoke-virtual {p2, v2, v0, v1}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 560
    :cond_1
    return-void
.end method

.method public static toast(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "stringResource"    # Ljava/lang/String;

    .prologue
    .line 543
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->toast:Landroid/widget/Toast;

    if-nez v0, :cond_0

    .line 544
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->toast:Landroid/widget/Toast;

    .line 546
    :cond_0
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->toast:Landroid/widget/Toast;

    invoke-virtual {v0, p1}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    .line 547
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->toast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 548
    return-void
.end method

.method public static toast(Landroid/content/Context;Ljava/lang/String;Landroid/os/Handler;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "stringResource"    # Ljava/lang/String;
    .param p2, "handler"    # Landroid/os/Handler;

    .prologue
    const/4 v2, 0x0

    .line 563
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->toast:Landroid/widget/Toast;

    if-nez v0, :cond_0

    .line 564
    invoke-static {p0, p1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->toast:Landroid/widget/Toast;

    .line 566
    :cond_0
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->toast:Landroid/widget/Toast;

    invoke-virtual {v0, p1}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    .line 567
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->toast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 569
    if-eqz p2, :cond_1

    .line 570
    const-wide/16 v0, 0x7d0

    invoke-virtual {p2, v2, v0, v1}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 572
    :cond_1
    return-void
.end method

.method public static toastHint(Landroid/content/Context;Landroid/view/View;Ljava/lang/String;)Landroid/widget/Toast;
    .locals 19
    .param p0, "mCtx"    # Landroid/content/Context;
    .param p1, "v"    # Landroid/view/View;
    .param p2, "stringResource"    # Ljava/lang/String;

    .prologue
    .line 1966
    const/4 v13, 0x2

    new-array v10, v13, [I

    .line 1967
    .local v10, "screenPos":[I
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    .line 1968
    .local v4, "displayFrame":Landroid/graphics/Rect;
    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 1969
    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 1971
    move-object/from16 v2, p0

    .line 1972
    .local v2, "context":Landroid/content/Context;
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getWidth()I

    move-result v12

    .line 1973
    .local v12, "width":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getHeight()I

    move-result v5

    .line 1974
    .local v5, "height":I
    const/4 v13, 0x1

    aget v13, v10, v13

    div-int/lit8 v14, v5, 0x2

    add-int v7, v13, v14

    .line 1975
    .local v7, "midy":I
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    invoke-virtual {v13}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v13

    iget v11, v13, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 1978
    .local v11, "screenWidth":I
    const/4 v13, 0x0

    aget v14, v10, v13

    const/4 v15, 0x5

    move-object/from16 v0, p0

    invoke-static {v0, v15}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getPixelFromDP(Landroid/content/Context;I)I

    move-result v15

    add-int/2addr v14, v15

    aput v14, v10, v13

    .line 1979
    const/4 v13, 0x0

    move-object/from16 v0, p2

    invoke-static {v2, v0, v13}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    .line 1982
    .local v1, "cheatSheet":Landroid/widget/Toast;
    const/4 v3, 0x0

    .line 1985
    .local v3, "custom_v":Landroid/view/View;
    const-string v13, "layout_inflater"

    .line 1986
    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/LayoutInflater;

    .line 1988
    .local v6, "mInflater":Landroid/view/LayoutInflater;
    const v13, 0x7f030025

    const/4 v14, 0x0

    invoke-virtual {v6, v13, v14}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 1989
    invoke-virtual {v1, v3}, Landroid/widget/Toast;->setView(Landroid/view/View;)V

    .line 1991
    const v13, 0x7f080063

    invoke-virtual {v3, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 1993
    .local v8, "myToastText":Landroid/widget/TextView;
    move-object/from16 v0, p2

    invoke-virtual {v8, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1996
    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v13

    if-ge v7, v13, :cond_4

    .line 1998
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    invoke-virtual {v13}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v13

    iget v9, v13, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 1999
    .local v9, "screenHeight":I
    const/16 v13, 0xf0

    if-ne v11, v13, :cond_0

    const/16 v13, 0x140

    if-eq v9, v13, :cond_1

    :cond_0
    const/16 v13, 0x140

    if-ne v11, v13, :cond_2

    const/16 v13, 0xf0

    if-ne v9, v13, :cond_2

    .line 2001
    :cond_1
    const/16 v13, 0x35

    const/4 v14, 0x0

    aget v14, v10, v14

    sub-int v14, v11, v14

    invoke-virtual {v1, v13, v14, v5}, Landroid/widget/Toast;->setGravity(III)V

    .line 2016
    .end local v9    # "screenHeight":I
    :goto_0
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 2018
    return-object v1

    .line 2003
    .restart local v9    # "screenHeight":I
    :cond_2
    sget-boolean v13, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isLollipopOS:Z

    if-eqz v13, :cond_3

    .line 2006
    const/16 v13, 0x35

    const/4 v14, 0x0

    aget v14, v10, v14

    sub-int v14, v11, v14

    div-int/lit8 v15, v12, 0x2

    sub-int/2addr v14, v15

    int-to-double v15, v5

    const-wide v17, 0x3fe3333333333333L    # 0.6

    mul-double v15, v15, v17

    double-to-int v15, v15

    invoke-virtual {v1, v13, v14, v15}, Landroid/widget/Toast;->setGravity(III)V

    goto :goto_0

    .line 2009
    :cond_3
    const/16 v13, 0x35

    const/4 v14, 0x0

    aget v14, v10, v14

    sub-int v14, v11, v14

    div-int/lit8 v15, v12, 0x2

    sub-int/2addr v14, v15

    invoke-virtual {v1, v13, v14, v5}, Landroid/widget/Toast;->setGravity(III)V

    goto :goto_0

    .line 2014
    .end local v9    # "screenHeight":I
    :cond_4
    const/16 v13, 0x51

    const/4 v14, 0x0

    invoke-virtual {v1, v13, v14, v5}, Landroid/widget/Toast;->setGravity(III)V

    goto :goto_0
.end method

.method public static unregisterNotiTime(Landroid/content/Context;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 2625
    const-string v3, "alarm"

    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/AlarmManager;

    .line 2626
    .local v1, "manager":Landroid/app/AlarmManager;
    new-instance v0, Landroid/content/Intent;

    const-class v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;

    invoke-direct {v0, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2627
    .local v0, "intent":Landroid/content/Intent;
    const-string v3, "com.sec.android.widgetapp.ap.hero.accuweather.action.ALERT_NOTIFICATION"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 2628
    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-static {p0, v3, v0, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 2629
    .local v2, "sender":Landroid/app/PendingIntent;
    invoke-virtual {v1, v2}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 2630
    return-void
.end method

.method private static updateCheck(Landroid/content/Context;I)V
    .locals 1
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "activity"    # I

    .prologue
    .line 3334
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo;-><init>(Landroid/content/Context;)V

    .line 3335
    .local v0, "updateCheckInfo":Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo;
    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo;->responseUpdateCheckVersion()V

    .line 3336
    return-void
.end method
