.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;
.super Ljava/lang/Object;
.source "CityListItem.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem$MyCreator;
    }
.end annotation


# instance fields
.field private mCityName:Ljava/lang/String;

.field private mLatitude:Ljava/lang/String;

.field private mLocation:Ljava/lang/String;

.field private mLongitude:Ljava/lang/String;

.field mMainDefault:[Z

.field mRealLocation:Ljava/lang/String;

.field private mState:Ljava/lang/String;

.field private mSummerTime:Ljava/lang/String;

.field private mSunRiseTime:Ljava/lang/String;

.field private mSunSetTime:Ljava/lang/String;

.field private mTempScale:I

.field private mTemperature:Ljava/lang/String;

.field private mTimeZone:Ljava/lang/String;

.field provider:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    const/4 v0, 0x1

    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->mMainDefault:[Z

    .line 25
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->mSunRiseTime:Ljava/lang/String;

    .line 27
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->mSunSetTime:Ljava/lang/String;

    .line 29
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->mTemperature:Ljava/lang/String;

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    const/4 v1, 0x0

    .line 159
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    const/4 v0, 0x1

    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->mMainDefault:[Z

    .line 25
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->mSunRiseTime:Ljava/lang/String;

    .line 27
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->mSunSetTime:Ljava/lang/String;

    .line 29
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->mTemperature:Ljava/lang/String;

    .line 160
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->mCityName:Ljava/lang/String;

    .line 161
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->mState:Ljava/lang/String;

    .line 162
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->mLocation:Ljava/lang/String;

    .line 163
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->mLatitude:Ljava/lang/String;

    .line 164
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->mLongitude:Ljava/lang/String;

    .line 165
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->mMainDefault:[Z

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readBooleanArray([Z)V

    .line 166
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->mSummerTime:Ljava/lang/String;

    .line 167
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->provider:I

    .line 168
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->mTemperature:Ljava/lang/String;

    .line 169
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->mTimeZone:Ljava/lang/String;

    .line 170
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->mTempScale:I

    .line 171
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "cityName"    # Ljava/lang/String;
    .param p2, "state"    # Ljava/lang/String;
    .param p3, "location"    # Ljava/lang/String;
    .param p4, "isSummerTime"    # Ljava/lang/String;
    .param p5, "latitude"    # Ljava/lang/String;
    .param p6, "longitude"    # Ljava/lang/String;
    .param p7, "realLocation"    # Ljava/lang/String;
    .param p8, "timeZone"    # Ljava/lang/String;
    .param p9, "SunRiseTime"    # Ljava/lang/String;
    .param p10, "SunSetTime"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    new-array v0, v2, [Z

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->mMainDefault:[Z

    .line 25
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->mSunRiseTime:Ljava/lang/String;

    .line 27
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->mSunSetTime:Ljava/lang/String;

    .line 29
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->mTemperature:Ljava/lang/String;

    .line 38
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->mCityName:Ljava/lang/String;

    .line 39
    iput-object p2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->mState:Ljava/lang/String;

    .line 40
    iput-object p3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->mLocation:Ljava/lang/String;

    .line 41
    iput-object p5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->mLatitude:Ljava/lang/String;

    .line 42
    iput-object p6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->mLongitude:Ljava/lang/String;

    .line 43
    iput-object p4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->mSummerTime:Ljava/lang/String;

    .line 44
    iput-object p7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->mRealLocation:Ljava/lang/String;

    .line 45
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->mTemperature:Ljava/lang/String;

    .line 46
    iput v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->mTempScale:I

    .line 47
    iput-object p8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->mTimeZone:Ljava/lang/String;

    .line 48
    iput-object p9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->mSunRiseTime:Ljava/lang/String;

    .line 49
    iput-object p10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->mSunSetTime:Ljava/lang/String;

    .line 50
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 129
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public getCityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->mCityName:Ljava/lang/String;

    return-object v0
.end method

.method public getLatitude()Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->mLatitude:Ljava/lang/String;

    return-object v0
.end method

.method public getLocation()Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->mLocation:Ljava/lang/String;

    return-object v0
.end method

.method public getLongitude()Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->mLongitude:Ljava/lang/String;

    return-object v0
.end method

.method public getRealLocation()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->mRealLocation:Ljava/lang/String;

    return-object v0
.end method

.method public getState()Ljava/lang/String;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->mState:Ljava/lang/String;

    return-object v0
.end method

.method public getSummerTime()Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->mSummerTime:Ljava/lang/String;

    return-object v0
.end method

.method public getSunRiseTime()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->mSunRiseTime:Ljava/lang/String;

    return-object v0
.end method

.method public getSunSetTime()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->mSunSetTime:Ljava/lang/String;

    return-object v0
.end method

.method public getTempScale()I
    .locals 1

    .prologue
    .line 117
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->mTempScale:I

    return v0
.end method

.method public getTemperature()Ljava/lang/String;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->mTemperature:Ljava/lang/String;

    return-object v0
.end method

.method public getTimeZone()Ljava/lang/String;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->mTimeZone:Ljava/lang/String;

    return-object v0
.end method

.method public setRealLocation(Ljava/lang/String;)V
    .locals 0
    .param p1, "realLocation"    # Ljava/lang/String;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->mRealLocation:Ljava/lang/String;

    .line 74
    return-void
.end method

.method public setSummerTime(Ljava/lang/String;)V
    .locals 0
    .param p1, "state"    # Ljava/lang/String;

    .prologue
    .line 97
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->mSummerTime:Ljava/lang/String;

    .line 98
    return-void
.end method

.method public setSunRiseTime(Ljava/lang/String;)V
    .locals 0
    .param p1, "time"    # Ljava/lang/String;

    .prologue
    .line 57
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->mSunRiseTime:Ljava/lang/String;

    .line 58
    return-void
.end method

.method public setSunSetTime(Ljava/lang/String;)V
    .locals 0
    .param p1, "time"    # Ljava/lang/String;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->mSunSetTime:Ljava/lang/String;

    .line 66
    return-void
.end method

.method public setTempScale(I)V
    .locals 0
    .param p1, "tempScale"    # I

    .prologue
    .line 113
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->mTempScale:I

    .line 114
    return-void
.end method

.method public setTemperature(Ljava/lang/String;)V
    .locals 0
    .param p1, "temp"    # Ljava/lang/String;

    .prologue
    .line 105
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->mTemperature:Ljava/lang/String;

    .line 106
    return-void
.end method

.method public setTimeZone(Ljava/lang/String;)V
    .locals 0
    .param p1, "timeZone"    # Ljava/lang/String;

    .prologue
    .line 121
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->mTimeZone:Ljava/lang/String;

    .line 122
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 147
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 148
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string v1, "CityListItem >> \n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 149
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CityListItem.getCity() = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->mCityName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 150
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CityListItem.getLocation() = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->mLocation:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 151
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CityListItem.getLatitude() = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->mLatitude:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 152
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CityListItem.getLongitude() = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->mLongitude:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 153
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CityListItem.getProvider() = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->provider:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 154
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CityListItem.getState() = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->mState:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 155
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CityListItem.getRealLocation() ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->mRealLocation:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 156
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 133
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->mCityName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 134
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->mState:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 135
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->mLocation:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 136
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->mLatitude:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 137
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->mLongitude:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 138
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->mMainDefault:[Z

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBooleanArray([Z)V

    .line 139
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->mSummerTime:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 140
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->provider:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 141
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->mTemperature:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 142
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->mTimeZone:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 143
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->mTempScale:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 144
    return-void
.end method
