.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;
.super Landroid/app/Activity;
.source "AccountingDialogActivity.java"


# instance fields
.field mActivityFlags:I

.field mAutoRefreshSetting:I

.field mConfirm:Landroid/app/Dialog;

.field mDialogFrom:Ljava/lang/String;

.field mLocation:Ljava/lang/String;

.field mSelectedIndex_easy:Ljava/lang/String;

.field mStartActivity:Ljava/lang/String;

.field mWidgetMode:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 21
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 22
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;->mAutoRefreshSetting:I

    .line 24
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;->mDialogFrom:Ljava/lang/String;

    .line 26
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;->mSelectedIndex_easy:Ljava/lang/String;

    .line 28
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;->mConfirm:Landroid/app/Dialog;

    .line 30
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;->mStartActivity:Ljava/lang/String;

    .line 32
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;->mActivityFlags:I

    .line 34
    const/16 v0, 0xfa0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;->mWidgetMode:I

    .line 36
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;->mLocation:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;Landroid/app/Activity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;
    .param p1, "x1"    # Landroid/app/Activity;

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;->activityfinish(Landroid/app/Activity;)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;Landroid/app/Activity;Landroid/content/Context;Landroid/content/Intent;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;
    .param p1, "x1"    # Landroid/app/Activity;
    .param p2, "x2"    # Landroid/content/Context;
    .param p3, "x3"    # Landroid/content/Intent;

    .prologue
    .line 21
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;->showUseCurrentLocationPopup(Landroid/app/Activity;Landroid/content/Context;Landroid/content/Intent;)V

    return-void
.end method

.method private activityfinish(Landroid/app/Activity;)V
    .locals 0
    .param p1, "acitivity"    # Landroid/app/Activity;

    .prologue
    .line 39
    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    .line 40
    return-void
.end method

.method private showUseCurrentLocationPopup(Landroid/app/Activity;Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "mActivity"    # Landroid/app/Activity;
    .param p2, "ctx"    # Landroid/content/Context;
    .param p3, "intent"    # Landroid/content/Intent;

    .prologue
    .line 135
    invoke-static {p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->isAgreeUseLocation(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 136
    const-string v3, "easy"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {p3, v3, v4}, Landroid/content/Intent;->getExtra(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 137
    .local v1, "easy":I
    const/16 v3, 0x3e7

    if-ne v1, v3, :cond_0

    .line 138
    new-instance v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity$2;

    invoke-direct {v3, p0, p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity$2;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;Landroid/content/Context;)V

    invoke-static {p2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->showUseCurrentLocation(Landroid/content/Context;Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;)Landroid/app/Dialog;

    move-result-object v0

    .line 148
    .local v0, "d":Landroid/app/Dialog;
    new-instance v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity$3;

    invoke-direct {v3, p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity$3;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;Landroid/app/Activity;)V

    invoke-virtual {v0, v3}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 164
    .end local v0    # "d":Landroid/app/Dialog;
    .end local v1    # "easy":I
    :cond_0
    :goto_0
    return-void

    .line 155
    :cond_1
    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.sec.android.widgetapp.ap.hero.accuweather.easy.widget.action.ACTIVITY"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 156
    .local v2, "sintent":Landroid/content/Intent;
    const-string v3, "cls"

    const-string v4, "com.samsung.sec.android.widgetapp.intent.action.MENU_DETAIL_GL"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 158
    const-string v3, "flags"

    const/16 v4, -0x7530

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 159
    const-string v3, "searchlocation"

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;->mSelectedIndex_easy:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 160
    invoke-virtual {p0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 161
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;->activityfinish(Landroid/app/Activity;)V

    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v8, 0x0

    .line 43
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 44
    move-object v1, p0

    .line 45
    .local v1, "ctx":Landroid/content/Context;
    move-object v4, p0

    .line 46
    .local v4, "mActivity":Landroid/app/Activity;
    const-string v5, "FROM_NO"

    iput-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;->mDialogFrom:Ljava/lang/String;

    .line 48
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    .line 50
    .local v3, "intent":Landroid/content/Intent;
    const/4 v5, 0x1

    invoke-virtual {p0, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;->requestWindowFeature(I)Z

    .line 55
    const/high16 v5, 0x7f030000

    invoke-virtual {p0, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;->setContentView(I)V

    .line 57
    if-eqz v3, :cond_2

    .line 58
    invoke-virtual {v3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 59
    .local v0, "bundle":Landroid/os/Bundle;
    if-eqz v0, :cond_1

    .line 60
    const-string v5, "ACCOUNTING_DIALOG_FROM"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 61
    .local v2, "from":Ljava/lang/String;
    if-eqz v2, :cond_0

    const-string v5, ""

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 62
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;->mDialogFrom:Ljava/lang/String;

    .line 64
    :cond_0
    const-string v5, "SELECTED_INDEX_EASY"

    invoke-virtual {v0, v5, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;->mSelectedIndex_easy:Ljava/lang/String;

    .line 69
    const-string v5, "AUTOREFRESH_SETTING"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    iput v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;->mAutoRefreshSetting:I

    .line 72
    .end local v2    # "from":Ljava/lang/String;
    :cond_1
    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;->mDialogFrom:Ljava/lang/String;

    const-string v6, "FROM_TOUCH_WIDGET"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 73
    invoke-virtual {v3}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;->mStartActivity:Ljava/lang/String;

    .line 74
    const-string v5, "flags"

    const/4 v6, -0x1

    invoke-virtual {v3, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    iput v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;->mActivityFlags:I

    .line 75
    invoke-virtual {v3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "widget_mode"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    iput v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;->mWidgetMode:I

    .line 76
    invoke-virtual {v3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "searchlocation"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;->mLocation:Ljava/lang/String;

    .line 80
    .end local v0    # "bundle":Landroid/os/Bundle;
    :cond_2
    const-string v5, ""

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onCreate : from : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;->mDialogFrom:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", location = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;->mLocation:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", idx_easy = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;->mSelectedIndex_easy:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", setting = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;->mAutoRefreshSetting:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->isShowChargerPopup(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_3

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;->mAutoRefreshSetting:I

    if-eqz v5, :cond_3

    .line 84
    const/16 v5, 0x401

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;->mAutoRefreshSetting:I

    invoke-static {v1, v5, v8, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->showDialog(Landroid/content/Context;ILcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;I)Landroid/app/Dialog;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;->mConfirm:Landroid/app/Dialog;

    .line 88
    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;->mConfirm:Landroid/app/Dialog;

    new-instance v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity$1;

    invoke-direct {v6, p0, v4, v1, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity$1;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;Landroid/app/Activity;Landroid/content/Context;Landroid/content/Intent;)V

    invoke-virtual {v5, v6}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 132
    :goto_0
    return-void

    .line 130
    :cond_3
    invoke-direct {p0, v4, v1, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;->showUseCurrentLocationPopup(Landroid/app/Activity;Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_0
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 166
    const-string v0, "FROM_NO"

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;->mDialogFrom:Ljava/lang/String;

    .line 167
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;->mConfirm:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    .line 168
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;->mConfirm:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 169
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;->mConfirm:Landroid/app/Dialog;

    .line 171
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 172
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;->finish()V

    .line 174
    :cond_1
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 175
    return-void
.end method
