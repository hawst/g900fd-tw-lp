.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;
.super Ljava/lang/Object;
.source "SettingsInfoForMain.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain$MyCreator;
    }
.end annotation


# instance fields
.field private mAutoRefreshTime:I

.field private mAutoScroll:I

.field private mCurrentLoccation:I

.field private mLastSelectLocation:Ljava/lang/String;

.field private mNextTime:J

.field private mNotification:I

.field private mNotificationSetTime:J

.field private mRefreshEntering:I

.field private mRefreshRoaming:I

.field private mSelectDefaultLocation:Ljava/lang/String;

.field mTempScale:Ljava/lang/String;

.field private mTempScaleSetting:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    return-void
.end method

.method public constructor <init>(IIJIIIIIJLjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "mTempScaleSetting"    # I
    .param p2, "mAutoRefreshTime"    # I
    .param p3, "nextTime"    # J
    .param p5, "mAutoScroll"    # I
    .param p6, "mRefreshEntering"    # I
    .param p7, "mRefreshRoaming"    # I
    .param p8, "mCurrentLoccation"    # I
    .param p9, "notification"    # I
    .param p10, "mmNotificationSetTime"    # J
    .param p12, "lastLocation"    # Ljava/lang/String;
    .param p13, "defaultLocation"    # Ljava/lang/String;

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->mTempScaleSetting:I

    .line 34
    iput p2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->mAutoRefreshTime:I

    .line 35
    iput p5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->mAutoScroll:I

    .line 36
    iput p6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->mRefreshEntering:I

    .line 37
    iput p7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->mRefreshRoaming:I

    .line 38
    iput-wide p3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->mNextTime:J

    .line 39
    iput p8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->mCurrentLoccation:I

    .line 40
    iput p9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->mNotification:I

    .line 41
    iput-wide p10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->mNotificationSetTime:J

    .line 42
    iput-object p12, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->mLastSelectLocation:Ljava/lang/String;

    .line 43
    iput-object p13, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->mSelectDefaultLocation:Ljava/lang/String;

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 165
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 166
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->mTempScale:Ljava/lang/String;

    .line 167
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->mAutoRefreshTime:I

    .line 168
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->mCurrentLoccation:I

    .line 169
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 156
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public getAutoRefreshTime()I
    .locals 1

    .prologue
    .line 85
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->mAutoRefreshTime:I

    return v0
.end method

.method public getAutoScroll()I
    .locals 1

    .prologue
    .line 101
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->mAutoScroll:I

    return v0
.end method

.method public getAutoUpdateTime()I
    .locals 1

    .prologue
    .line 109
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->mAutoRefreshTime:I

    return v0
.end method

.method public getCurrentLoccation()I
    .locals 1

    .prologue
    .line 47
    sget-boolean v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isAutoCaptureToolSupportMode:Z

    if-eqz v0, :cond_0

    .line 48
    const/4 v0, 0x0

    .line 50
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->mCurrentLoccation:I

    goto :goto_0
.end method

.method public getLastSelectLocation()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->mLastSelectLocation:Ljava/lang/String;

    return-object v0
.end method

.method public getNextTime()J
    .locals 2

    .prologue
    .line 93
    iget-wide v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->mNextTime:J

    return-wide v0
.end method

.method public getNotificatioSetTime()J
    .locals 2

    .prologue
    .line 129
    iget-wide v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->mNotificationSetTime:J

    return-wide v0
.end method

.method public getNotification()I
    .locals 1

    .prologue
    .line 121
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->mNotification:I

    return v0
.end method

.method public getRefreshEntering()I
    .locals 1

    .prologue
    .line 133
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->mRefreshEntering:I

    return v0
.end method

.method public getRefreshRoaming()I
    .locals 1

    .prologue
    .line 141
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->mRefreshRoaming:I

    return v0
.end method

.method public getSelectDefaultLocation()Ljava/lang/String;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->mSelectDefaultLocation:Ljava/lang/String;

    return-object v0
.end method

.method public getTempScale()Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->mTempScale:Ljava/lang/String;

    return-object v0
.end method

.method public getTempScaleSetting()I
    .locals 1

    .prologue
    .line 66
    sget-boolean v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isAutoCaptureToolSupportMode:Z

    if-eqz v0, :cond_0

    .line 67
    const/4 v0, 0x1

    .line 69
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->mTempScaleSetting:I

    goto :goto_0
.end method

.method public setAutoRefreshTime(I)V
    .locals 0
    .param p1, "mAutoRefreshTime"    # I

    .prologue
    .line 97
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->mAutoRefreshTime:I

    .line 98
    return-void
.end method

.method public setAutoScroll(I)V
    .locals 0
    .param p1, "mAutoScroll"    # I

    .prologue
    .line 105
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->mAutoScroll:I

    .line 106
    return-void
.end method

.method public setAutoUpdateTIme(I)V
    .locals 0
    .param p1, "time"    # I

    .prologue
    .line 113
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->mAutoRefreshTime:I

    .line 114
    return-void
.end method

.method public setCurrentLoccation(I)V
    .locals 0
    .param p1, "mCurrentLoccation"    # I

    .prologue
    .line 54
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->mCurrentLoccation:I

    .line 55
    return-void
.end method

.method public setLastSelectLocation(Ljava/lang/String;)V
    .locals 0
    .param p1, "lastSelectLocation"    # Ljava/lang/String;

    .prologue
    .line 62
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->mLastSelectLocation:Ljava/lang/String;

    .line 63
    return-void
.end method

.method public setNextTime(J)V
    .locals 0
    .param p1, "nexttime"    # J

    .prologue
    .line 89
    iput-wide p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->mNextTime:J

    .line 90
    return-void
.end method

.method public setNotificatioSetTime(J)V
    .locals 0
    .param p1, "mNotificationSetTime"    # J

    .prologue
    .line 125
    iput-wide p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->mNotificationSetTime:J

    .line 126
    return-void
.end method

.method public setNotification(I)V
    .locals 0
    .param p1, "mNotification"    # I

    .prologue
    .line 117
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->mNotification:I

    .line 118
    return-void
.end method

.method public setRefreshEntering(I)V
    .locals 0
    .param p1, "mRefreshEntering"    # I

    .prologue
    .line 137
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->mRefreshEntering:I

    .line 138
    return-void
.end method

.method public setRefreshRoaming(I)V
    .locals 0
    .param p1, "mRefreshRoaming"    # I

    .prologue
    .line 144
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->mRefreshRoaming:I

    .line 145
    return-void
.end method

.method public setTempScale(Ljava/lang/String;)V
    .locals 0
    .param p1, "mTempScale"    # Ljava/lang/String;

    .prologue
    .line 81
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->mTempScale:Ljava/lang/String;

    .line 82
    return-void
.end method

.method public setTempScaleSetting(I)V
    .locals 0
    .param p1, "mTempScaleSetting"    # I

    .prologue
    .line 73
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->mTempScaleSetting:I

    .line 74
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 160
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->mTempScale:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 161
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->mAutoRefreshTime:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 162
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->mCurrentLoccation:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 163
    return-void
.end method
