.class Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34$4;
.super Ljava/lang/Object;
.source "MenuAdd.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->onReceive(IILjava/lang/String;Ljava/lang/String;J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;)V
    .locals 0
    .param p1, "this$1"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;

    .prologue
    .line 1859
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34$4;->this$1:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 1861
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34$4;->this$1:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;

    iget-object v3, v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->hideLoadingDialog()V
    invoke-static {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$2100(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)V

    .line 1863
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34$4;->this$1:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;

    iget-object v3, v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    const/4 v4, -0x1

    invoke-virtual {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->setResult(I)V

    .line 1865
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34$4;->this$1:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;

    iget-object v3, v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->ctx:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$2400(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->isMaxCityListAdded(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1866
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34$4;->this$1:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;

    iget-object v3, v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34$4;->this$1:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;

    iget-object v4, v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    .line 1867
    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0d0023

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/16 v6, 0xa

    .line 1869
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    .line 1866
    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34$4$1;

    invoke-direct {v5, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34$4$1;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34$4;)V

    invoke-static {v3, v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->toast(Landroid/content/Context;Ljava/lang/String;Landroid/os/Handler;)V

    .line 1904
    :goto_0
    return-void

    .line 1875
    :cond_0
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34$4;->this$1:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;

    iget-object v3, v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->ctx:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$2400(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DaemonInterface;->getSettingDaemonCityId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34$4;->this$1:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;

    iget-object v3, v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    .line 1876
    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->ctx:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$2400(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DaemonInterface;->getSettingDaemonCityId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34$4;->this$1:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;

    iget-object v4, v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->val$location:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    :cond_1
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34$4;->this$1:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;

    iget-object v3, v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->val$cityInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    .line 1878
    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->getLocation()Ljava/lang/String;

    move-result-object v3

    const-string v4, "cityId:current"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1880
    :cond_2
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34$4;->this$1:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;

    iget-object v3, v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    .line 1881
    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->ctx:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$2400(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DaemonInterface;->checkSameDaemonCityIdAtDate(Landroid/content/Context;)V

    .line 1883
    :cond_3
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34$4;->this$1:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;

    iget-object v3, v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "flags"

    const/16 v5, -0x3e7

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    const/16 v4, 0x6977

    if-ne v3, v4, :cond_5

    .line 1885
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34$4;->this$1:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;

    iget-object v3, v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    .line 1886
    invoke-static {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getAllCityList(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1887
    .local v0, "cityList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;>;"
    const/4 v2, 0x0

    .line 1888
    .local v2, "location":Ljava/lang/String;
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_4

    .line 1889
    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->getLocation()Ljava/lang/String;

    move-result-object v2

    .line 1891
    :cond_4
    new-instance v1, Landroid/content/Intent;

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34$4;->this$1:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;

    iget-object v3, v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    const-class v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    invoke-direct {v1, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1893
    .local v1, "citylistintent":Landroid/content/Intent;
    const/high16 v3, 0x24000000

    .line 1894
    invoke-virtual {v1, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1896
    const-string v3, "searchlocation"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1898
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34$4;->this$1:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;

    iget-object v3, v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    invoke-virtual {v3, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->startActivity(Landroid/content/Intent;)V

    .line 1899
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34$4;->this$1:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;

    iget-object v3, v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->finish()V

    goto/16 :goto_0

    .line 1901
    .end local v0    # "cityList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;>;"
    .end local v1    # "citylistintent":Landroid/content/Intent;
    .end local v2    # "location":Ljava/lang/String;
    :cond_5
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34$4;->this$1:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;

    iget-object v3, v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->finish()V

    goto/16 :goto_0
.end method
