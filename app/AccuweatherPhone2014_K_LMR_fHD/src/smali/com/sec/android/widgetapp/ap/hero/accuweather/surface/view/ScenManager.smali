.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;
.super Ljava/lang/Object;
.source "ScenManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager$ImageGetter;,
        Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager$DrawListener;
    }
.end annotation


# static fields
.field public static final FLICK_DOWN:I = 0x2

.field public static final FLICK_NONE:I = 0x0

.field public static final FLICK_UP:I = 0x1

.field public static final TOUCH_GAP:F = 30.0f

.field private static mCityIndex:I

.field private static mCurCityBGView:Landroid/view/ViewGroup;

.field private static mCurCityInfoView:Landroid/view/ViewGroup;

.field private static mNextCityBGView:Landroid/view/ViewGroup;

.field private static mNextCityInfoView:Landroid/view/ViewGroup;

.field private static mTouchDownY:F

.field private static mTz:Ljava/util/TimeZone;


# instance fields
.field private audio:Landroid/media/AudioManager;

.field private cal_b:Ljava/util/Calendar;

.field private mAnimationManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/AnimationManager;

.field private mBGTransparencyVal:I

.field private mCityDayOrNight:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private mCityList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mCurrentBGLayout:Landroid/view/ViewGroup;

.field private mCurrentCityInfoLayout:Landroid/view/ViewGroup;

.field private mDetailWeatherInfo:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mDrawListener:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager$DrawListener;

.field private mFrameShadow:Landroid/widget/RelativeLayout;

.field private mIconHandler:Landroid/os/Handler;

.field private mIconManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;

.field private mInstance:I

.field private mIsShownDst:Z

.field private mIsStartActivity:Z

.field private mIsStartScrollAnimation:Z

.field private mNextBGLayout:Landroid/view/ViewGroup;

.field private mNextCityInfoLayout:Landroid/view/ViewGroup;

.field private mRefreshListener:Landroid/view/View$OnClickListener;

.field private mResizeStep:I

.field private mRootView:Landroid/view/ViewGroup;

.field private mSelectedLocation:Ljava/lang/String;

.field private mSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

.field private mSettingsViEffect:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

.field private mWeatherRenderer:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;

.field private msgId:I

.field private shadowOffset:F

.field private shadowSoftness:F

.field private startRefresh:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 67
    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mNextCityInfoView:Landroid/view/ViewGroup;

    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCurCityInfoView:Landroid/view/ViewGroup;

    .line 69
    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mNextCityBGView:Landroid/view/ViewGroup;

    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCurCityBGView:Landroid/view/ViewGroup;

    .line 73
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityIndex:I

    .line 109
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v0

    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mTz:Ljava/util/TimeZone;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager$DrawListener;Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;)V
    .locals 3
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "instance"    # I
    .param p3, "listener"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager$DrawListener;
    .param p4, "r"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 128
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mContext:Landroid/content/Context;

    .line 63
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCurrentCityInfoLayout:Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCurrentBGLayout:Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mRootView:Landroid/view/ViewGroup;

    .line 65
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mNextCityInfoLayout:Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mNextBGLayout:Landroid/view/ViewGroup;

    .line 71
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mFrameShadow:Landroid/widget/RelativeLayout;

    .line 75
    iput-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->startRefresh:Z

    .line 83
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mSelectedLocation:Ljava/lang/String;

    .line 85
    iput v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->msgId:I

    .line 87
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mRefreshListener:Landroid/view/View$OnClickListener;

    .line 89
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mSettingsViEffect:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    .line 91
    iput-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mIsStartActivity:Z

    .line 93
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mResizeStep:I

    .line 101
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mDrawListener:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager$DrawListener;

    .line 102
    iput v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mInstance:I

    .line 104
    iput-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mIsStartScrollAnimation:Z

    .line 105
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mIconManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;

    .line 106
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mWeatherRenderer:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;

    .line 111
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityDayOrNight:Ljava/util/HashMap;

    .line 112
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mDetailWeatherInfo:Ljava/util/HashMap;

    .line 114
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mIconHandler:Landroid/os/Handler;

    .line 116
    iput v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mBGTransparencyVal:I

    .line 118
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->audio:Landroid/media/AudioManager;

    .line 120
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mIsShownDst:Z

    .line 841
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->shadowOffset:F

    .line 842
    const/high16 v0, 0x40800000    # 4.0f

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->shadowSoftness:F

    .line 129
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mContext:Landroid/content/Context;

    .line 130
    invoke-direct {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->setTimeZone(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;)V

    .line 131
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/AnimationManager;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/AnimationManager;-><init>(Landroid/content/Context;Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mAnimationManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/AnimationManager;

    .line 132
    iput-object p3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mDrawListener:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager$DrawListener;

    .line 133
    iput p2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mInstance:I

    .line 135
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mContext:Landroid/content/Context;

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->audio:Landroid/media/AudioManager;

    .line 137
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mIconManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;

    .line 138
    iput-object p4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mWeatherRenderer:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;

    .line 140
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090009

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mIsShownDst:Z

    .line 142
    sget-boolean v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isVerizon:Z

    if-eqz v0, :cond_0

    .line 143
    iput-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mIsShownDst:Z

    .line 145
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->setFontEffectAttr()V

    .line 146
    return-void
.end method

.method static synthetic access$000()Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCurCityInfoView:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic access$100()Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mNextCityInfoView:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic access$200()Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mNextCityBGView:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;

    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mIsStartScrollAnimation:Z

    return v0
.end method

.method static synthetic access$302(Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;
    .param p1, "x1"    # Z

    .prologue
    .line 50
    iput-boolean p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mIsStartScrollAnimation:Z

    return p1
.end method

.method static synthetic access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$500()F
    .locals 1

    .prologue
    .line 50
    sget v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mTouchDownY:F

    return v0
.end method

.method static synthetic access$502(F)F
    .locals 0
    .param p0, "x0"    # F

    .prologue
    .line 50
    sput p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mTouchDownY:F

    return p0
.end method

.method static synthetic access$600(Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;
    .param p1, "x1"    # I

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->moveCity(I)V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;)Landroid/media/AudioManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->audio:Landroid/media/AudioManager;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;)Landroid/view/ViewGroup;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCurrentCityInfoLayout:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;)Landroid/view/ViewGroup;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mNextCityInfoLayout:Landroid/view/ViewGroup;

    return-object v0
.end method

.method private adjustTimeZoneLayout(Landroid/widget/LinearLayout;I)V
    .locals 3
    .param p1, "weatherTimeZoneLayout"    # Landroid/widget/LinearLayout;
    .param p2, "leftMarginDp"    # I

    .prologue
    .line 1491
    if-eqz p1, :cond_0

    .line 1493
    invoke-virtual {p1}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 1494
    .local v0, "weatherTimeZoneLayoutParam":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mContext:Landroid/content/Context;

    invoke-static {v1, p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getPixelFromDP(Landroid/content/Context;I)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 1495
    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1499
    .end local v0    # "weatherTimeZoneLayoutParam":Landroid/widget/LinearLayout$LayoutParams;
    :goto_0
    return-void

    .line 1497
    :cond_0
    const-string v1, ""

    const-string v2, "aTZ wTZL n"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private getLastSelectCityIndex()I
    .locals 5

    .prologue
    .line 385
    const/4 v2, 0x0

    .line 389
    .local v2, "lastLoc":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    if-nez v3, :cond_0

    .line 390
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getSettingsForMain(Landroid/content/Context;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    .line 393
    :cond_0
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    if-eqz v3, :cond_1

    .line 394
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->getLastSelectLocation()Ljava/lang/String;

    move-result-object v2

    .line 397
    :cond_1
    if-nez v2, :cond_3

    .line 398
    const/4 v1, 0x0

    .line 416
    :cond_2
    :goto_0
    return v1

    .line 400
    :cond_3
    const/4 v1, 0x0

    .line 402
    .local v1, "index":I
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityList:Ljava/util/ArrayList;

    if-eqz v3, :cond_2

    .line 403
    const/4 v1, 0x0

    :goto_1
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_2

    .line 404
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityList:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getLocation()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v3

    if-nez v3, :cond_2

    .line 403
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 409
    :catch_0
    move-exception v0

    .line 410
    .local v0, "e":Ljava/lang/IndexOutOfBoundsException;
    invoke-virtual {v0}, Ljava/lang/IndexOutOfBoundsException;->printStackTrace()V

    .line 411
    const-string v3, ""

    const-string v4, "gLSC IOOBE"

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 412
    .end local v0    # "e":Ljava/lang/IndexOutOfBoundsException;
    :catch_1
    move-exception v0

    .line 413
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    .line 414
    const-string v3, ""

    const-string v4, "gLSC nul"

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private isTimeStartWithOne(Ljava/lang/String;)Z
    .locals 3
    .param p1, "timeStr"    # Ljava/lang/String;

    .prologue
    .line 1477
    const/4 v0, 0x0

    .line 1479
    .local v0, "result":Z
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_1

    .line 1480
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    const/16 v2, 0x31

    if-ne v1, v2, :cond_0

    .line 1481
    const/4 v0, 0x1

    .line 1487
    :cond_0
    :goto_0
    return v0

    .line 1484
    :cond_1
    const-string v1, ""

    const-string v2, "iTSWO tS n"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private makeDateText(Z)Ljava/lang/String;
    .locals 6
    .param p1, "isFullDay"    # Z

    .prologue
    const/4 v5, 0x1

    .line 1520
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 1521
    .local v0, "curtime":J
    sget-boolean v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isAutoCaptureToolSupportMode:Z

    if-eqz v3, :cond_0

    .line 1522
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    .line 1523
    .local v2, "d":Ljava/util/Date;
    const/16 v3, 0x7dd

    invoke-virtual {v2, v3}, Ljava/util/Date;->setYear(I)V

    .line 1524
    const/16 v3, 0x18

    invoke-virtual {v2, v3}, Ljava/util/Date;->setDate(I)V

    .line 1525
    invoke-virtual {v2, v5}, Ljava/util/Date;->setMonth(I)V

    .line 1526
    const/16 v3, 0xc

    invoke-virtual {v2, v3}, Ljava/util/Date;->setHours(I)V

    .line 1527
    const/16 v3, 0x2d

    invoke-virtual {v2, v3}, Ljava/util/Date;->setMinutes(I)V

    .line 1529
    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    .line 1530
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->cal_b:Ljava/util/Calendar;

    .line 1531
    const-string v3, "Asia/Seoul"

    invoke-static {v3}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v3

    sput-object v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mTz:Ljava/util/TimeZone;

    .line 1532
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->cal_b:Ljava/util/Calendar;

    sget-object v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mTz:Ljava/util/TimeZone;

    invoke-virtual {v3, v4}, Ljava/util/Calendar;->setTimeZone(Ljava/util/TimeZone;)V

    .line 1533
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->cal_b:Ljava/util/Calendar;

    invoke-virtual {v3, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1534
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->cal_b:Ljava/util/Calendar;

    invoke-virtual {v3, v5}, Ljava/util/Calendar;->setFirstDayOfWeek(I)V

    .line 1541
    .end local v2    # "d":Ljava/util/Date;
    :goto_0
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->cal_b:Ljava/util/Calendar;

    invoke-static {v3, v4, v5, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->dateFormat(Landroid/content/Context;Ljava/util/Calendar;ZZ)Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 1536
    :cond_0
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->cal_b:Ljava/util/Calendar;

    if-nez v3, :cond_1

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->cal_b:Ljava/util/Calendar;

    .line 1537
    :cond_1
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->cal_b:Ljava/util/Calendar;

    sget-object v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mTz:Ljava/util/TimeZone;

    invoke-virtual {v3, v4}, Ljava/util/Calendar;->setTimeZone(Ljava/util/TimeZone;)V

    .line 1538
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->cal_b:Ljava/util/Calendar;

    invoke-virtual {v3, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    goto :goto_0
.end method

.method private makeTimeText()Ljava/lang/String;
    .locals 5

    .prologue
    .line 1502
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 1503
    .local v0, "curtime":J
    sget-boolean v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isAutoCaptureToolSupportMode:Z

    if-eqz v3, :cond_0

    .line 1504
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    .line 1505
    .local v2, "d":Ljava/util/Date;
    const/16 v3, 0x7de

    invoke-virtual {v2, v3}, Ljava/util/Date;->setYear(I)V

    .line 1506
    const/16 v3, 0x18

    invoke-virtual {v2, v3}, Ljava/util/Date;->setDate(I)V

    .line 1507
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/Date;->setMonth(I)V

    .line 1508
    const/16 v3, 0xc

    invoke-virtual {v2, v3}, Ljava/util/Date;->setHours(I)V

    .line 1509
    const/16 v3, 0x2d

    invoke-virtual {v2, v3}, Ljava/util/Date;->setMinutes(I)V

    .line 1510
    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    .line 1512
    .end local v2    # "d":Ljava/util/Date;
    :cond_0
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->cal_b:Ljava/util/Calendar;

    if-nez v3, :cond_1

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->cal_b:Ljava/util/Calendar;

    .line 1513
    :cond_1
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->cal_b:Ljava/util/Calendar;

    sget-object v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mTz:Ljava/util/TimeZone;

    invoke-virtual {v3, v4}, Ljava/util/Calendar;->setTimeZone(Ljava/util/TimeZone;)V

    .line 1514
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->cal_b:Ljava/util/Calendar;

    invoke-virtual {v3, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1516
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->cal_b:Ljava/util/Calendar;

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->makeTimeText(Landroid/content/Context;Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method private moveCity(I)V
    .locals 8
    .param p1, "direction"    # I

    .prologue
    const/4 v1, 0x1

    .line 572
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mIsStartScrollAnimation:Z

    if-eq v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->startRefresh:Z

    if-ne v0, v1, :cond_1

    .line 574
    :cond_0
    const-string v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "moveCity : no scroll : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mIsStartScrollAnimation:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " sr : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->startRefresh:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 658
    :goto_0
    return-void

    .line 580
    :cond_1
    :try_start_0
    const-string v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "moveCity : direction = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mCityIndex = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityIndex:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", size = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityList:Ljava/util/ArrayList;

    .line 581
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 580
    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 582
    packed-switch p1, :pswitch_data_0

    .line 603
    :goto_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityList:Ljava/util/ArrayList;

    sget v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityIndex:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getLocation()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mSelectedLocation:Ljava/lang/String;

    .line 604
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mSelectedLocation:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateLastSelectedLocation(Landroid/content/Context;Ljava/lang/String;)I

    .line 605
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityList:Ljava/util/ArrayList;

    sget v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityIndex:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    invoke-direct {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->setTimeZone(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;)V

    .line 607
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mSettingsViEffect:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->getWidgetCityChangeAnimation()I

    move-result v0

    if-nez v0, :cond_4

    .line 608
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCurCityInfoView:Landroid/view/ViewGroup;

    sget-object v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCurCityBGView:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityList:Ljava/util/ArrayList;

    sget v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityIndex:I

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    invoke-direct {p0, v1, v2, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->setLayoutContentNotEmpty(Landroid/view/View;Landroid/view/View;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;)V

    .line 609
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCurCityInfoView:Landroid/view/ViewGroup;

    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->startIconAnimation(Landroid/view/ViewGroup;)V
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 651
    :catch_0
    move-exception v7

    .line 652
    .local v7, "e":Ljava/lang/IndexOutOfBoundsException;
    invoke-virtual {v7}, Ljava/lang/IndexOutOfBoundsException;->printStackTrace()V

    .line 653
    const-string v0, ""

    const-string v1, "mC IOOBE"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 584
    .end local v7    # "e":Ljava/lang/IndexOutOfBoundsException;
    :pswitch_0
    :try_start_1
    sget v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityIndex:I

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    .line 585
    sget v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityIndex:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityIndex:I
    :try_end_1
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 654
    :catch_1
    move-exception v7

    .line 655
    .local v7, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v7}, Ljava/lang/NullPointerException;->printStackTrace()V

    .line 656
    const-string v0, ""

    const-string v1, "mC nul"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 587
    .end local v7    # "e":Ljava/lang/NullPointerException;
    :cond_2
    const/4 v0, 0x0

    :try_start_2
    sput v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityIndex:I

    goto :goto_1

    .line 592
    :pswitch_1
    sget v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityIndex:I

    if-lez v0, :cond_3

    .line 593
    sget v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityIndex:I

    add-int/lit8 v0, v0, -0x1

    sput v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityIndex:I

    goto :goto_1

    .line 595
    :cond_3
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    sput v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityIndex:I

    goto/16 :goto_1

    .line 611
    :cond_4
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mNextCityInfoView:Landroid/view/ViewGroup;

    sget-object v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mNextCityBGView:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityList:Ljava/util/ArrayList;

    sget v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityIndex:I

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    invoke-direct {p0, v1, v2, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->setLayoutContentNotEmpty(Landroid/view/View;Landroid/view/View;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;)V

    .line 613
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mAnimationManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/AnimationManager;

    sget-object v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCurCityInfoView:Landroid/view/ViewGroup;

    sget-object v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mNextCityInfoView:Landroid/view/ViewGroup;

    sget-object v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCurCityBGView:Landroid/view/ViewGroup;

    sget-object v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mNextCityBGView:Landroid/view/ViewGroup;

    new-instance v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager$3;

    invoke-direct {v6, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager$3;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;)V

    move v1, p1

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/AnimationManager;->startScrollAnimation(ILandroid/view/View;Landroid/view/View;Landroid/view/View;Landroid/view/View;Landroid/animation/Animator$AnimatorListener;)V
    :try_end_2
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_0

    .line 582
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private setEventListener(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 752
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCurCityInfoView:Landroid/view/ViewGroup;

    const v2, 0x7f080028

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 753
    .local v0, "refreshbtn":Landroid/widget/LinearLayout;
    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mRefreshListener:Landroid/view/View$OnClickListener;

    if-eqz v1, :cond_1

    .line 754
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mRefreshListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 761
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mRootView:Landroid/view/ViewGroup;

    if-eqz v1, :cond_2

    .line 762
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mRootView:Landroid/view/ViewGroup;

    new-instance v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager$4;

    invoke-direct {v2, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager$4;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;)V

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 810
    :goto_1
    return-void

    .line 756
    :cond_1
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mRefreshListener:Landroid/view/View$OnClickListener;

    if-nez v1, :cond_0

    .line 757
    const-string v1, ""

    const-string v2, "Refresh listener is null"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 808
    :cond_2
    const-string v1, ""

    const-string v2, "root view is null"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private setFontEffectAttr()V
    .locals 4

    .prologue
    const/high16 v3, 0x40a00000    # 5.0f

    const/high16 v2, 0x3f800000    # 1.0f

    .line 845
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getSystemResolution(Landroid/content/Context;)I

    move-result v0

    .line 849
    .local v0, "resolution":I
    const v1, 0x1fa400

    if-ne v0, v1, :cond_1

    .line 850
    const/high16 v1, 0x40000000    # 2.0f

    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->shadowOffset:F

    .line 851
    iput v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->shadowSoftness:F

    .line 860
    :cond_0
    :goto_0
    return-void

    .line 852
    :cond_1
    const v1, 0xe1000

    if-ne v0, v1, :cond_2

    .line 853
    iput v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->shadowOffset:F

    goto :goto_0

    .line 854
    :cond_2
    const v1, 0x7e900

    if-ne v0, v1, :cond_3

    .line 855
    iput v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->shadowOffset:F

    goto :goto_0

    .line 856
    :cond_3
    const v1, 0x384000

    if-ne v0, v1, :cond_0

    .line 857
    const/high16 v1, 0x40400000    # 3.0f

    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->shadowOffset:F

    .line 858
    iput v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->shadowSoftness:F

    goto :goto_0
.end method

.method private setLayoutContentEmptyMsg(Landroid/view/View;Landroid/view/View;I)V
    .locals 11
    .param p1, "v_info"    # Landroid/view/View;
    .param p2, "v_bg"    # Landroid/view/View;
    .param p3, "id"    # I

    .prologue
    .line 1044
    const-string v7, ""

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "setLayoutContentEmptyMsg = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1049
    const v7, 0x7f080016

    :try_start_0
    invoke-virtual {p2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 1050
    .local v1, "bgImage":Landroid/widget/ImageView;
    const v7, 0x7f080017

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 1051
    .local v5, "errorText":Landroid/widget/TextView;
    const v7, 0x7f080024

    .line 1052
    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 1054
    .local v0, "anotherInfoGroup":Landroid/widget/RelativeLayout;
    const/4 v7, 0x4

    invoke-virtual {v0, v7}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1055
    const/4 v7, 0x0

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1056
    invoke-virtual {v5, p3}, Landroid/widget/TextView;->setText(I)V

    .line 1058
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mContext:Landroid/content/Context;

    const/4 v8, 0x1

    const/16 v9, 0x63

    const/16 v10, 0x105

    invoke-static {v7, v8, v9, v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getTodayBG(Landroid/content/Context;ZII)I

    move-result v6

    .line 1059
    .local v6, "imageID":I
    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1060
    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mBGTransparencyVal:I

    invoke-static {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->calAlphaVal(I)I

    move-result v7

    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setImageAlpha(I)V

    .line 1063
    const v7, 0x7f080020

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 1064
    .local v2, "dstIcon":Landroid/widget/ImageView;
    const v7, 0x7f08001b

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 1065
    .local v3, "dstIconKr":Landroid/widget/ImageView;
    const/16 v7, 0x8

    invoke-virtual {v3, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1066
    const/16 v7, 0x8

    invoke-virtual {v2, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1068
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->setEventListener(Landroid/view/View;)V

    .line 1069
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->setShadowVisibility()V

    .line 1070
    invoke-virtual {p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->setClockLayoutContent(Landroid/view/View;)V

    .line 1072
    const-string v7, ""

    const-string v8, "complete setLayoutContentEmptyMsg Content"

    invoke-static {v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1076
    .end local v0    # "anotherInfoGroup":Landroid/widget/RelativeLayout;
    .end local v1    # "bgImage":Landroid/widget/ImageView;
    .end local v2    # "dstIcon":Landroid/widget/ImageView;
    .end local v3    # "dstIconKr":Landroid/widget/ImageView;
    .end local v5    # "errorText":Landroid/widget/TextView;
    .end local v6    # "imageID":I
    :goto_0
    return-void

    .line 1073
    :catch_0
    move-exception v4

    .line 1074
    .local v4, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v4}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method private setLayoutContentNotEmpty(Landroid/view/View;Landroid/view/View;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;)V
    .locals 36
    .param p1, "v_info"    # Landroid/view/View;
    .param p2, "v_bg"    # Landroid/view/View;
    .param p3, "cityInfo"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    .prologue
    .line 869
    :try_start_0
    invoke-virtual/range {p3 .. p3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getLocation()Ljava/lang/String;

    move-result-object v32

    const-string v33, "cityId:current"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_4

    .line 870
    const-string v32, ""

    new-instance v33, Ljava/lang/StringBuilder;

    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    const-string v34, "setLayoutContentNotEmpty rL ="

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual/range {p3 .. p3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getRealLocation()Ljava/lang/String;

    move-result-object v34

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v33

    invoke-static/range {v32 .. v33}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 877
    :goto_0
    const v32, 0x7f08002a

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v28

    check-cast v28, Landroid/widget/TextView;

    .line 878
    .local v28, "temperatureText":Landroid/widget/TextView;
    const v32, 0x7f08002c

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/TextView;

    .line 879
    .local v17, "highTemperatureText":Landroid/widget/TextView;
    const v32, 0x7f08002d

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v26

    check-cast v26, Landroid/widget/TextView;

    .line 881
    .local v26, "lowTemperatureText":Landroid/widget/TextView;
    const v32, 0x7f08002b

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v27

    check-cast v27, Landroid/widget/ImageView;

    .line 882
    .local v27, "temperatureIcon":Landroid/widget/ImageView;
    const v32, 0x7f08002f

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v29

    check-cast v29, Landroid/widget/ImageView;

    .line 883
    .local v29, "weatherIcon":Landroid/widget/ImageView;
    const v32, 0x7f080030

    .line 884
    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v30

    check-cast v30, Landroid/widget/RelativeLayout;

    .line 885
    .local v30, "weatherIconLayout":Landroid/widget/RelativeLayout;
    const v32, 0x7f080027

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 886
    .local v7, "city":Landroid/widget/TextView;
    const v32, 0x7f080031

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v22

    check-cast v22, Landroid/widget/TextView;

    .line 887
    .local v22, "lastUpdate":Landroid/widget/TextView;
    const v32, 0x7f080016

    move-object/from16 v0, p2

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    .line 889
    .local v6, "bgImage":Landroid/widget/ImageView;
    const v32, 0x7f080017

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/TextView;

    .line 890
    .local v14, "errorText":Landroid/widget/TextView;
    const v32, 0x7f080024

    .line 891
    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/RelativeLayout;

    .line 893
    .local v5, "anotherInfoGroup":Landroid/widget/RelativeLayout;
    const/16 v32, 0x0

    move/from16 v0, v32

    invoke-virtual {v5, v0}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 894
    const/16 v32, 0x4

    move/from16 v0, v32

    invoke-virtual {v14, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 897
    invoke-virtual/range {p3 .. p3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getTodayWeatherInfo()Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getTempScale()I

    move-result v32

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    move-object/from16 v33, v0

    .line 898
    invoke-virtual/range {v33 .. v33}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->getTempScaleSetting()I

    move-result v33

    invoke-virtual/range {p3 .. p3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getTodayWeatherInfo()Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getCurrentTemp()F

    move-result v34

    .line 897
    invoke-static/range {v32 .. v34}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTemp(IIF)I

    move-result v9

    .line 901
    .local v9, "currentTemp":I
    invoke-virtual/range {p3 .. p3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getTodayWeatherInfo()Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getTempScale()I

    move-result v32

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    move-object/from16 v33, v0

    .line 902
    invoke-virtual/range {v33 .. v33}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->getTempScaleSetting()I

    move-result v33

    invoke-virtual/range {p3 .. p3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getTodayWeatherInfo()Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getHighTemp()F

    move-result v34

    .line 901
    invoke-static/range {v32 .. v34}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTemp(IIF)I

    move-result v16

    .line 905
    .local v16, "highTemp":I
    invoke-virtual/range {p3 .. p3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getTodayWeatherInfo()Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getTempScale()I

    move-result v32

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    move-object/from16 v33, v0

    .line 906
    invoke-virtual/range {v33 .. v33}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->getTempScaleSetting()I

    move-result v33

    invoke-virtual/range {p3 .. p3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getTodayWeatherInfo()Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getLowTemp()F

    move-result v34

    .line 905
    invoke-static/range {v32 .. v34}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTemp(IIF)I

    move-result v25

    .line 908
    .local v25, "lowTemp":I
    sget-object v32, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->SamsungNumber3L:Landroid/graphics/Typeface;

    if-eqz v32, :cond_0

    .line 909
    sget-object v32, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->SamsungNumber3L:Landroid/graphics/Typeface;

    move-object/from16 v0, v28

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 912
    :cond_0
    const-string v32, "%d"

    const/16 v33, 0x1

    move/from16 v0, v33

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v33, v0

    const/16 v34, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v35

    aput-object v35, v33, v34

    invoke-static/range {v32 .. v33}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, v28

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 913
    const-string v32, "%d"

    const/16 v33, 0x1

    move/from16 v0, v33

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v33, v0

    const/16 v34, 0x0

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v35

    aput-object v35, v33, v34

    invoke-static/range {v32 .. v33}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, v17

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 914
    const-string v32, "%d"

    const/16 v33, 0x1

    move/from16 v0, v33

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v33, v0

    const/16 v34, 0x0

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v35

    aput-object v35, v33, v34

    invoke-static/range {v32 .. v33}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, v26

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 916
    move-object/from16 v0, p0

    move-object/from16 v1, v28

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->setStrokeAndShadow(Landroid/widget/TextView;)V

    .line 917
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->setStrokeAndShadow(Landroid/widget/TextView;)V

    .line 918
    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->setStrokeAndShadow(Landroid/widget/TextView;)V

    .line 920
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    move-object/from16 v32, v0

    .line 921
    invoke-virtual/range {v32 .. v32}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->getTempScaleSetting()I

    move-result v32

    const/16 v33, 0x1

    move/from16 v0, v32

    move/from16 v1, v33

    if-ne v0, v1, :cond_5

    const v32, 0x7f020103

    :goto_1
    move-object/from16 v0, v27

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 923
    sget-object v32, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mTz:Ljava/util/TimeZone;

    invoke-virtual/range {p3 .. p3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getTodayWeatherInfo()Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getSunRiseTime()Ljava/lang/String;

    move-result-object v33

    .line 924
    invoke-virtual/range {p3 .. p3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getTodayWeatherInfo()Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getSunSetTime()Ljava/lang/String;

    move-result-object v34

    .line 923
    invoke-static/range {v32 .. v34}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->isDay(Ljava/util/TimeZone;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v20

    .line 926
    .local v20, "isDay":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityDayOrNight:Ljava/util/HashMap;

    move-object/from16 v32, v0

    invoke-virtual/range {p3 .. p3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getLocation()Ljava/lang/String;

    move-result-object v33

    invoke-static/range {v20 .. v20}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v34

    invoke-virtual/range {v32 .. v34}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 927
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mDetailWeatherInfo:Ljava/util/HashMap;

    move-object/from16 v32, v0

    invoke-virtual/range {p3 .. p3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getLocation()Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v32

    move-object/from16 v1, v33

    move-object/from16 v2, p3

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 929
    invoke-virtual/range {p3 .. p3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getTodayWeatherInfo()Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getIconNum()I

    move-result v18

    .line 930
    .local v18, "iconNum":I
    const/16 v32, 0x0

    move-object/from16 v0, v30

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 932
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mSettingsViEffect:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->getWidgetIconAnimation()I

    move-result v32

    if-nez v32, :cond_6

    .line 933
    const/16 v32, 0x0

    move-object/from16 v0, v29

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 935
    invoke-virtual/range {v30 .. v30}, Landroid/widget/RelativeLayout;->getChildCount()I

    move-result v32

    if-lez v32, :cond_1

    .line 936
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mIconManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;->cancelIconAnimation(Landroid/view/ViewGroup;)V

    .line 937
    invoke-virtual/range {v30 .. v30}, Landroid/widget/RelativeLayout;->removeAllViews()V

    .line 938
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mIconHandler:Landroid/os/Handler;

    move-object/from16 v32, v0

    if-eqz v32, :cond_1

    .line 939
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mIconHandler:Landroid/os/Handler;

    move-object/from16 v32, v0

    const/16 v33, 0x0

    invoke-virtual/range {v32 .. v33}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 942
    :cond_1
    const/16 v32, 0x4

    move-object/from16 v0, v30

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 944
    const/16 v32, 0x0

    move/from16 v0, v18

    move/from16 v1, v32

    move/from16 v2, v20

    invoke-static {v0, v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->setTodayWeatherIcon(IIZ)I

    move-result v32

    move-object/from16 v0, v29

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 970
    :goto_2
    invoke-virtual/range {p3 .. p3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getCityName()Ljava/lang/String;

    move-result-object v8

    .line 971
    .local v8, "cityNameString":Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getLocation()Ljava/lang/String;

    move-result-object v32

    const-string v33, "cityId:current"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_9

    .line 972
    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    const-string v33, "<img src=\"icon\"/>"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, v32

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 973
    new-instance v32, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager$ImageGetter;

    move-object/from16 v0, v32

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager$ImageGetter;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;)V

    const/16 v33, 0x0

    move-object/from16 v0, v32

    move-object/from16 v1, v33

    invoke-static {v8, v0, v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;Landroid/text/Html$ImageGetter;Landroid/text/Html$TagHandler;)Landroid/text/Spanned;

    move-result-object v32

    move-object/from16 v0, v32

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 977
    :goto_3
    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->setStrokeAndShadow(Landroid/widget/TextView;)V

    .line 979
    const-string v32, ""

    new-instance v33, Ljava/lang/StringBuilder;

    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    const-string v34, "lastU = "

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual/range {p3 .. p3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getTodayWeatherInfo()Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getUpdateDate()Ljava/lang/String;

    move-result-object v34

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v33

    invoke-static/range {v32 .. v33}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 980
    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mContext:Landroid/content/Context;

    move-object/from16 v33, v0

    const v34, 0x7f0d003b

    invoke-virtual/range {v33 .. v34}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v33

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string v33, " "

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mContext:Landroid/content/Context;

    move-object/from16 v33, v0

    .line 981
    invoke-virtual/range {p3 .. p3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getTodayWeatherInfo()Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getUpdateDate()Ljava/lang/String;

    move-result-object v34

    .line 980
    invoke-static/range {v33 .. v34}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getDateTimeStringForWidget(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v33

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    .line 982
    .local v23, "lastUpdateString":Ljava/lang/String;
    invoke-virtual/range {v22 .. v23}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 983
    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->setStrokeAndShadow(Landroid/widget/TextView;)V

    .line 985
    const/16 v19, -0x1

    .line 986
    .local v19, "imageID":I
    sget-boolean v32, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isAutoCaptureToolSupportMode:Z

    if-eqz v32, :cond_a

    .line 987
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mContext:Landroid/content/Context;

    move-object/from16 v32, v0

    const/16 v33, 0x1

    const/16 v34, 0x105

    move-object/from16 v0, v32

    move/from16 v1, v33

    move/from16 v2, v18

    move/from16 v3, v34

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getTodayBG(Landroid/content/Context;ZII)I

    move-result v19

    .line 991
    :goto_4
    move/from16 v0, v19

    invoke-virtual {v6, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 992
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mBGTransparencyVal:I

    move/from16 v32, v0

    invoke-static/range {v32 .. v32}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->calAlphaVal(I)I

    move-result v32

    move/from16 v0, v32

    invoke-virtual {v6, v0}, Landroid/widget/ImageView;->setImageAlpha(I)V

    .line 993
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->setShadowVisibility()V

    .line 996
    invoke-direct/range {p0 .. p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->setEventListener(Landroid/view/View;)V

    .line 999
    const v32, 0x7f080020

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/ImageView;

    .line 1000
    .local v10, "dstIcon":Landroid/widget/ImageView;
    const v32, 0x7f08001b

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/ImageView;

    .line 1002
    .local v11, "dstIconKr":Landroid/widget/ImageView;
    invoke-virtual/range {p3 .. p3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getTodayWeatherInfo()Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getSummerTime()Ljava/lang/String;

    move-result-object v32

    invoke-static/range {v32 .. v32}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/Integer;->intValue()I

    move-result v12

    .line 1003
    .local v12, "dstTime":I
    const/16 v21, 0x0

    .line 1004
    .local v21, "isKor":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mContext:Landroid/content/Context;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v32

    move-object/from16 v0, v32

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v24

    .line 1005
    .local v24, "locale":Ljava/lang/String;
    if-eqz v24, :cond_2

    invoke-virtual/range {v24 .. v24}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v32

    const-string v33, "ko"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v32

    if-eqz v32, :cond_2

    .line 1006
    const/16 v21, 0x1

    .line 1009
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mContext:Landroid/content/Context;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v32

    move-object/from16 v0, v32

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    move-object/from16 v32, v0

    invoke-static/range {v32 .. v32}, Landroid/text/TextUtils;->getLayoutDirectionFromLocale(Ljava/util/Locale;)I

    move-result v32

    const/16 v33, 0x1

    move/from16 v0, v32

    move/from16 v1, v33

    if-ne v0, v1, :cond_3

    .line 1010
    invoke-virtual/range {v24 .. v24}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v32

    const-string v33, "ur"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v32

    if-nez v32, :cond_3

    .line 1011
    const/16 v21, 0x1

    .line 1014
    :cond_3
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mIsShownDst:Z

    move/from16 v32, v0

    if-eqz v32, :cond_c

    if-lez v12, :cond_c

    .line 1015
    if-eqz v21, :cond_b

    .line 1016
    const/16 v32, 0x0

    move/from16 v0, v32

    invoke-virtual {v11, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1017
    const/16 v32, 0x8

    move/from16 v0, v32

    invoke-virtual {v10, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1028
    :goto_5
    invoke-virtual/range {p0 .. p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->setClockLayoutContent(Landroid/view/View;)V

    .line 1030
    const-string v32, ""

    const-string v33, "complete setLayoutContentNotEmpty Content"

    invoke-static/range {v32 .. v33}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1035
    .end local v5    # "anotherInfoGroup":Landroid/widget/RelativeLayout;
    .end local v6    # "bgImage":Landroid/widget/ImageView;
    .end local v7    # "city":Landroid/widget/TextView;
    .end local v8    # "cityNameString":Ljava/lang/String;
    .end local v9    # "currentTemp":I
    .end local v10    # "dstIcon":Landroid/widget/ImageView;
    .end local v11    # "dstIconKr":Landroid/widget/ImageView;
    .end local v12    # "dstTime":I
    .end local v14    # "errorText":Landroid/widget/TextView;
    .end local v16    # "highTemp":I
    .end local v17    # "highTemperatureText":Landroid/widget/TextView;
    .end local v18    # "iconNum":I
    .end local v19    # "imageID":I
    .end local v20    # "isDay":Z
    .end local v21    # "isKor":Z
    .end local v22    # "lastUpdate":Landroid/widget/TextView;
    .end local v23    # "lastUpdateString":Ljava/lang/String;
    .end local v24    # "locale":Ljava/lang/String;
    .end local v25    # "lowTemp":I
    .end local v26    # "lowTemperatureText":Landroid/widget/TextView;
    .end local v27    # "temperatureIcon":Landroid/widget/ImageView;
    .end local v28    # "temperatureText":Landroid/widget/TextView;
    .end local v29    # "weatherIcon":Landroid/widget/ImageView;
    .end local v30    # "weatherIconLayout":Landroid/widget/RelativeLayout;
    :goto_6
    return-void

    .line 872
    :cond_4
    const-string v32, ""

    new-instance v33, Ljava/lang/StringBuilder;

    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    const-string v34, "setLayoutContentNotEmpty l ="

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual/range {p3 .. p3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getLocation()Ljava/lang/String;

    move-result-object v34

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v33

    invoke-static/range {v32 .. v33}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 1032
    :catch_0
    move-exception v13

    .line 1033
    .local v13, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v13}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_6

    .line 921
    .end local v13    # "e":Ljava/lang/NullPointerException;
    .restart local v5    # "anotherInfoGroup":Landroid/widget/RelativeLayout;
    .restart local v6    # "bgImage":Landroid/widget/ImageView;
    .restart local v7    # "city":Landroid/widget/TextView;
    .restart local v9    # "currentTemp":I
    .restart local v14    # "errorText":Landroid/widget/TextView;
    .restart local v16    # "highTemp":I
    .restart local v17    # "highTemperatureText":Landroid/widget/TextView;
    .restart local v22    # "lastUpdate":Landroid/widget/TextView;
    .restart local v25    # "lowTemp":I
    .restart local v26    # "lowTemperatureText":Landroid/widget/TextView;
    .restart local v27    # "temperatureIcon":Landroid/widget/ImageView;
    .restart local v28    # "temperatureText":Landroid/widget/TextView;
    .restart local v29    # "weatherIcon":Landroid/widget/ImageView;
    .restart local v30    # "weatherIconLayout":Landroid/widget/RelativeLayout;
    :cond_5
    const v32, 0x7f020104

    goto/16 :goto_1

    .line 947
    .restart local v18    # "iconNum":I
    .restart local v20    # "isDay":Z
    :cond_6
    const/16 v32, 0x4

    :try_start_1
    move-object/from16 v0, v29

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 948
    const/16 v32, 0x0

    move-object/from16 v0, v30

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 950
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mContext:Landroid/content/Context;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v32

    const v33, 0x7f0b000b

    invoke-virtual/range {v32 .. v33}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v32

    invoke-static/range {v32 .. v32}, Ljava/lang/Math;->round(F)I

    move-result v31

    .line 952
    .local v31, "width":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mContext:Landroid/content/Context;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v32

    const v33, 0x7f0b000c

    invoke-virtual/range {v32 .. v33}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v32

    invoke-static/range {v32 .. v32}, Ljava/lang/Math;->round(F)I

    move-result v15

    .line 955
    .local v15, "height":I
    const/4 v4, 0x0

    .line 956
    .local v4, "ani":Landroid/view/View;
    sget-boolean v32, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isAutoCaptureToolSupportMode:Z

    if-eqz v32, :cond_8

    .line 957
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mIconManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;

    move-object/from16 v32, v0

    const/16 v33, 0x1

    move-object/from16 v0, v32

    move/from16 v1, v18

    move/from16 v2, v33

    move/from16 v3, v31

    invoke-virtual {v0, v1, v2, v3, v15}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;->getIconAnimation(IZII)Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;

    move-result-object v4

    .end local v4    # "ani":Landroid/view/View;
    check-cast v4, Landroid/view/View;

    .line 962
    .restart local v4    # "ani":Landroid/view/View;
    :goto_7
    invoke-virtual/range {v30 .. v30}, Landroid/widget/RelativeLayout;->getChildCount()I

    move-result v32

    if-lez v32, :cond_7

    .line 963
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mIconManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;->cancelIconAnimation(Landroid/view/ViewGroup;)V

    .line 965
    invoke-virtual/range {v30 .. v30}, Landroid/widget/RelativeLayout;->removeAllViews()V

    .line 967
    :cond_7
    move-object/from16 v0, v30

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_2

    .line 959
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mIconManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    move/from16 v1, v18

    move/from16 v2, v20

    move/from16 v3, v31

    invoke-virtual {v0, v1, v2, v3, v15}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;->getIconAnimation(IZII)Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;

    move-result-object v4

    .end local v4    # "ani":Landroid/view/View;
    check-cast v4, Landroid/view/View;

    .restart local v4    # "ani":Landroid/view/View;
    goto :goto_7

    .line 975
    .end local v4    # "ani":Landroid/view/View;
    .end local v15    # "height":I
    .end local v31    # "width":I
    .restart local v8    # "cityNameString":Ljava/lang/String;
    :cond_9
    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 989
    .restart local v19    # "imageID":I
    .restart local v23    # "lastUpdateString":Ljava/lang/String;
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mContext:Landroid/content/Context;

    move-object/from16 v32, v0

    const/16 v33, 0x105

    move-object/from16 v0, v32

    move/from16 v1, v20

    move/from16 v2, v18

    move/from16 v3, v33

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getTodayBG(Landroid/content/Context;ZII)I

    move-result v19

    goto/16 :goto_4

    .line 1019
    .restart local v10    # "dstIcon":Landroid/widget/ImageView;
    .restart local v11    # "dstIconKr":Landroid/widget/ImageView;
    .restart local v12    # "dstTime":I
    .restart local v21    # "isKor":Z
    .restart local v24    # "locale":Ljava/lang/String;
    :cond_b
    const/16 v32, 0x8

    move/from16 v0, v32

    invoke-virtual {v11, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1020
    const/16 v32, 0x0

    move/from16 v0, v32

    invoke-virtual {v10, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_5

    .line 1023
    :cond_c
    const/16 v32, 0x8

    move/from16 v0, v32

    invoke-virtual {v11, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1024
    const/16 v32, 0x8

    move/from16 v0, v32

    invoke-virtual {v10, v0}, Landroid/widget/ImageView;->setVisibility(I)V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_5
.end method

.method private setStrokeAndShadow(Landroid/widget/TextView;)V
    .locals 8
    .param p1, "textview"    # Landroid/widget/TextView;

    .prologue
    const/high16 v4, -0x1000000

    .line 825
    if-nez p1, :cond_0

    .line 826
    const-string v0, ""

    const-string v2, "setFE tv is null!!"

    invoke-static {v0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 839
    :goto_0
    return-void

    .line 829
    :cond_0
    invoke-virtual {p1}, Landroid/widget/TextView;->clearAllTextEffect()V

    .line 831
    const/high16 v7, 0x3f800000    # 1.0f

    .line 832
    .local v7, "strokeSize":F
    const v6, 0x3ee66666    # 0.45f

    .line 834
    .local v6, "strokeOffset":F
    const/high16 v1, 0x42b40000    # 90.0f

    .line 835
    .local v1, "shadowAngle":F
    const v5, 0x3f4ccccd    # 0.8f

    .line 837
    .local v5, "shadowOpacity":F
    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->shadowOffset:F

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->shadowSoftness:F

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/widget/TextView;->addOuterShadowTextEffect(FFFIF)I

    .line 838
    invoke-virtual {p1, v7, v4, v6}, Landroid/widget/TextView;->addStrokeTextEffect(FIF)I

    goto :goto_0
.end method

.method private setTimeZone(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;)V
    .locals 2
    .param p1, "cityInfo"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    .prologue
    .line 666
    if-eqz p1, :cond_3

    .line 667
    invoke-virtual {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getTodayWeatherInfo()Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 668
    invoke-virtual {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getTodayWeatherInfo()Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getTimeZone()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 669
    invoke-virtual {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getTodayWeatherInfo()Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getTimeZone()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mTz:Ljava/util/TimeZone;

    .line 683
    :goto_0
    sget-boolean v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isAutoCaptureToolSupportMode:Z

    if-eqz v0, :cond_0

    .line 684
    const-string v0, "Asia/Seoul"

    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mTz:Ljava/util/TimeZone;

    .line 686
    :cond_0
    return-void

    .line 671
    :cond_1
    const-string v0, ""

    const-string v1, "[sCCTZ] tZ is null"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 672
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v0

    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mTz:Ljava/util/TimeZone;

    goto :goto_0

    .line 675
    :cond_2
    const-string v0, ""

    const-string v1, "[sCCTZ] todayInfo is null"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 676
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v0

    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mTz:Ljava/util/TimeZone;

    goto :goto_0

    .line 679
    :cond_3
    const-string v0, ""

    const-string v1, "[sCCTZ] set default tZ"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 680
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v0

    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mTz:Ljava/util/TimeZone;

    goto :goto_0
.end method

.method public static setViews(Landroid/view/ViewGroup;Landroid/view/ViewGroup;Landroid/view/ViewGroup;Landroid/view/ViewGroup;)V
    .locals 1
    .param p0, "curCityInfoView"    # Landroid/view/ViewGroup;
    .param p1, "nextCityInfoView"    # Landroid/view/ViewGroup;
    .param p2, "curBGView"    # Landroid/view/ViewGroup;
    .param p3, "nextBGView"    # Landroid/view/ViewGroup;

    .prologue
    .line 361
    sput-object p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCurCityInfoView:Landroid/view/ViewGroup;

    .line 362
    sput-object p1, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mNextCityInfoView:Landroid/view/ViewGroup;

    .line 363
    sput-object p2, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCurCityBGView:Landroid/view/ViewGroup;

    .line 364
    sput-object p3, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mNextCityBGView:Landroid/view/ViewGroup;

    .line 366
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mNextCityInfoView:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->bringToFront()V

    .line 367
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCurCityInfoView:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->bringToFront()V

    .line 368
    return-void
.end method

.method private unbindDrawables(Landroid/view/View;)V
    .locals 7
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 1589
    if-nez p1, :cond_0

    .line 1627
    :goto_0
    return-void

    .line 1594
    :cond_0
    :try_start_0
    invoke-virtual {p1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 1595
    invoke-virtual {p1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 1598
    :cond_1
    instance-of v5, p1, Landroid/widget/ImageView;

    if-eqz v5, :cond_2

    .line 1600
    const/4 p1, 0x0

    .line 1604
    :cond_2
    instance-of v5, p1, Landroid/widget/TextView;

    if-eqz v5, :cond_3

    .line 1606
    const/4 p1, 0x0

    .line 1609
    :cond_3
    instance-of v5, p1, Landroid/view/ViewGroup;

    if-eqz v5, :cond_5

    .line 1610
    move-object v0, p1

    check-cast v0, Landroid/view/ViewGroup;

    move-object v3, v0

    .line 1611
    .local v3, "group":Landroid/view/ViewGroup;
    invoke-virtual {v3}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    .line 1612
    .local v1, "count":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    if-ge v4, v1, :cond_4

    .line 1613
    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->unbindDrawables(Landroid/view/View;)V

    .line 1612
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 1616
    :cond_4
    instance-of v5, p1, Landroid/widget/AdapterView;

    if-nez v5, :cond_5

    .line 1617
    invoke-virtual {v3}, Landroid/view/ViewGroup;->removeAllViewsInLayout()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1626
    .end local v1    # "count":I
    .end local v3    # "group":Landroid/view/ViewGroup;
    .end local v4    # "i":I
    :cond_5
    :goto_2
    const/4 p1, 0x0

    .line 1627
    goto :goto_0

    .line 1622
    :catch_0
    move-exception v2

    .line 1623
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2
.end method


# virtual methods
.method public cancelIconAnimation(Landroid/view/ViewGroup;)V
    .locals 4
    .param p1, "root"    # Landroid/view/ViewGroup;

    .prologue
    .line 1257
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mIconManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;

    if-nez v2, :cond_1

    .line 1281
    :cond_0
    :goto_0
    return-void

    .line 1260
    :cond_1
    if-nez p1, :cond_2

    .line 1261
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCurrentCityInfoLayout:Landroid/view/ViewGroup;

    if-eqz v2, :cond_3

    .line 1262
    iget-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCurrentCityInfoLayout:Landroid/view/ViewGroup;

    .line 1268
    :cond_2
    if-eqz p1, :cond_0

    .line 1269
    const v2, 0x7f080030

    invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 1270
    .local v1, "parent":Landroid/view/ViewGroup;
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mIconManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;

    invoke-virtual {v2, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;->isRunningIconAnimation(Landroid/view/ViewGroup;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1271
    const-string v2, ""

    const-string v3, "cancleIA!!"

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1272
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mIconManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;

    invoke-virtual {v2, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;->cancelIconAnimation(Landroid/view/ViewGroup;)V

    .line 1273
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mIconHandler:Landroid/os/Handler;

    if-eqz v2, :cond_0

    .line 1274
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mIconHandler:Landroid/os/Handler;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1278
    .end local v1    # "parent":Landroid/view/ViewGroup;
    :catch_0
    move-exception v0

    .line 1279
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 1264
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_3
    :try_start_1
    const-string v2, ""

    const-string v3, "CCILv null!"

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public clearRootView()V
    .locals 2

    .prologue
    .line 560
    const-string v0, ""

    const-string v1, "clear rootview "

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 561
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mRootView:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 562
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mRootView:Landroid/view/ViewGroup;

    invoke-direct {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->unbindDrawables(Landroid/view/View;)V

    .line 564
    :cond_0
    return-void
.end method

.method protected doChangeDayOrNight()Z
    .locals 10

    .prologue
    const/4 v7, 0x0

    .line 1290
    :try_start_0
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityList:Ljava/util/ArrayList;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-gtz v6, :cond_1

    :cond_0
    move v6, v7

    .line 1347
    :goto_0
    return v6

    .line 1294
    :cond_1
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mDetailWeatherInfo:Ljava/util/HashMap;

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityDayOrNight:Ljava/util/HashMap;

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mDetailWeatherInfo:Ljava/util/HashMap;

    .line 1295
    invoke-virtual {v6}, Ljava/util/HashMap;->size()I

    move-result v6

    if-lez v6, :cond_2

    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityDayOrNight:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->size()I

    move-result v6

    if-gtz v6, :cond_3

    :cond_2
    move v6, v7

    .line 1296
    goto :goto_0

    .line 1299
    :cond_3
    const/4 v4, 0x0

    .line 1300
    .local v4, "today":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mSelectedLocation:Ljava/lang/String;

    if-nez v6, :cond_4

    .line 1301
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getLastSelectedLocation(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mSelectedLocation:Ljava/lang/String;

    .line 1304
    :cond_4
    const-string v6, ""

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "doChangeDayOrNight : mCL.size = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityList:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", mSL = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mSelectedLocation:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1307
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mDetailWeatherInfo:Ljava/util/HashMap;

    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mSelectedLocation:Ljava/lang/String;

    invoke-virtual {v6, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "today":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    check-cast v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    .line 1308
    .restart local v4    # "today":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    if-nez v4, :cond_5

    .line 1309
    const-string v6, ""

    const-string v8, "today is null!!"

    invoke-static {v6, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v6, v7

    .line 1310
    goto :goto_0

    .line 1312
    :cond_5
    sget-object v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCurCityInfoView:Landroid/view/ViewGroup;

    if-nez v6, :cond_6

    .line 1313
    const-string v6, ""

    const-string v8, "mCurView is null!!"

    invoke-static {v6, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v6, v7

    .line 1314
    goto :goto_0

    .line 1316
    :cond_6
    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getTodayWeatherInfo()Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getTimeZone()Ljava/lang/String;

    move-result-object v6

    .line 1317
    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getTodayWeatherInfo()Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getSunRiseTime()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getTodayWeatherInfo()Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    move-result-object v9

    .line 1318
    invoke-virtual {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getSunSetTime()Ljava/lang/String;

    move-result-object v9

    .line 1316
    invoke-static {v6, v8, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkDayOrNight(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    .line 1319
    .local v3, "isDay":Z
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityDayOrNight:Ljava/util/HashMap;

    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mSelectedLocation:Ljava/lang/String;

    invoke-virtual {v6, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-eq v6, v3, :cond_8

    .line 1320
    sget-object v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCurCityInfoView:Landroid/view/ViewGroup;

    sget-object v8, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCurCityBGView:Landroid/view/ViewGroup;

    invoke-direct {p0, v6, v8, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->setLayoutContentNotEmpty(Landroid/view/View;Landroid/view/View;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;)V

    .line 1321
    sget-object v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCurCityInfoView:Landroid/view/ViewGroup;

    invoke-virtual {p0, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->startIconAnimation(Landroid/view/ViewGroup;)V
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_2

    .line 1323
    :try_start_1
    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getLocation()Ljava/lang/String;

    move-result-object v6

    const-string v8, "cityId:current"

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 1325
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getDaemonWeatherIconNum(Landroid/content/Context;)I

    move-result v5

    .line 1326
    .local v5, "weatherIcon":I
    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getTodayWeatherInfo()Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getIconNum()I

    move-result v6

    invoke-static {v6, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getWeatherInfoIcon(IZ)I

    move-result v6

    if-eq v5, v6, :cond_7

    .line 1327
    const-string v6, ""

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "weatherIcon : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1328
    const-string v6, ""

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "convertIcon : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getTodayWeatherInfo()Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getIconNum()I

    move-result v9

    invoke-static {v9, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getWeatherInfoIcon(IZ)I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1329
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getTodayWeatherInfo()Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getIconNum()I

    move-result v8

    invoke-static {v8, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getWeatherInfoIcon(IZ)I

    move-result v8

    invoke-static {v6, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateDaemonWeatherIconNum(Landroid/content/Context;I)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_2

    .line 1335
    .end local v5    # "weatherIcon":I
    :cond_7
    :goto_1
    const/4 v6, 0x1

    goto/16 :goto_0

    .line 1332
    :catch_0
    move-exception v0

    .line 1333
    .local v0, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v6, ""

    const-string v8, "CI exception"

    invoke-static {v6, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_1

    .line 1337
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v3    # "isDay":Z
    .end local v4    # "today":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    :catch_1
    move-exception v1

    .line 1338
    .local v1, "e1":Ljava/lang/IndexOutOfBoundsException;
    const-string v6, ""

    const-string v8, "doChangeDayOrNight : IndexOutOfBoundsException"

    invoke-static {v6, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1339
    invoke-virtual {v1}, Ljava/lang/IndexOutOfBoundsException;->printStackTrace()V

    move v6, v7

    .line 1340
    goto/16 :goto_0

    .line 1341
    .end local v1    # "e1":Ljava/lang/IndexOutOfBoundsException;
    :catch_2
    move-exception v2

    .line 1342
    .local v2, "e2":Ljava/lang/NullPointerException;
    const-string v6, ""

    const-string v8, "doChangeDayOrNight : NullPointerException"

    invoke-static {v6, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1343
    invoke-virtual {v2}, Ljava/lang/NullPointerException;->printStackTrace()V

    move v6, v7

    .line 1344
    goto/16 :goto_0

    .end local v2    # "e2":Ljava/lang/NullPointerException;
    .restart local v3    # "isDay":Z
    .restart local v4    # "today":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    :cond_8
    move v6, v7

    .line 1347
    goto/16 :goto_0
.end method

.method public drawLayoutAfterResize()V
    .locals 5

    .prologue
    .line 178
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityList:Ljava/util/ArrayList;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_3

    .line 179
    sget v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityIndex:I

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-le v1, v2, :cond_0

    .line 180
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    sput v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityIndex:I

    .line 182
    :cond_0
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityList:Ljava/util/ArrayList;

    sget v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityIndex:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    invoke-direct {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->setTimeZone(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;)V

    .line 183
    sget-object v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCurCityInfoView:Landroid/view/ViewGroup;

    sget-object v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCurCityBGView:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityList:Ljava/util/ArrayList;

    sget v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityIndex:I

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    invoke-direct {p0, v2, v3, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->setLayoutContentNotEmpty(Landroid/view/View;Landroid/view/View;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;)V

    .line 184
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mIconHandler:Landroid/os/Handler;

    if-eqz v1, :cond_1

    .line 185
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mIconHandler:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 186
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mIconHandler:Landroid/os/Handler;

    new-instance v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager$1;

    invoke-direct {v2, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager$1;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;)V

    const-wide/16 v3, 0x3e8

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 202
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->isRefreshing()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 203
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->startRefreshIcon()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 208
    :cond_2
    :goto_1
    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->setResizeStep(I)V

    .line 210
    return-void

    .line 194
    :cond_3
    const/4 v1, 0x0

    :try_start_1
    invoke-direct {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->setTimeZone(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;)V

    .line 196
    sget v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityIndex:I

    if-lez v1, :cond_4

    .line 197
    const/4 v1, 0x0

    sput v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityIndex:I

    .line 199
    :cond_4
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCurCityInfoView:Landroid/view/ViewGroup;

    sget-object v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCurCityBGView:Landroid/view/ViewGroup;

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->msgId:I

    invoke-direct {p0, v1, v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->setLayoutContentEmptyMsg(Landroid/view/View;Landroid/view/View;I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 205
    :catch_0
    move-exception v0

    .line 206
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public getAnimationManager()Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/AnimationManager;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mAnimationManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/AnimationManager;

    return-object v0
.end method

.method public getCityIndex()I
    .locals 1

    .prologue
    .line 376
    sget v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityIndex:I

    return v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 9

    .prologue
    .line 1545
    const-string v1, ""

    .line 1548
    .local v1, "desc":Ljava/lang/String;
    :try_start_0
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityList:Ljava/util/ArrayList;

    if-eqz v6, :cond_0

    .line 1549
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    sget v7, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityIndex:I

    if-ge v6, v7, :cond_0

    .line 1550
    const/4 v6, 0x0

    sput v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityIndex:I

    .line 1551
    const-string v6, ""

    const-string v7, "getDescription : index error"

    invoke-static {v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1556
    :cond_0
    new-instance v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TTSinfo;

    invoke-direct {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TTSinfo;-><init>()V

    .line 1557
    .local v5, "ttsInfo":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TTSinfo;
    sget v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TTSinfo;->DESCRIPTION_TYPE_LONG:I

    .line 1559
    .local v2, "descType":I
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityList:Ljava/util/ArrayList;

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    const/4 v7, 0x1

    if-ge v6, v7, :cond_2

    .line 1560
    :cond_1
    sget v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TTSinfo;->DESCRIPTION_TYPE_EMPTY_CITY:I

    or-int/2addr v2, v6

    .line 1574
    :goto_0
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mContext:Landroid/content/Context;

    invoke-virtual {v5, v6, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TTSinfo;->getDescription(Landroid/content/Context;I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    .line 1584
    .end local v2    # "descType":I
    .end local v5    # "ttsInfo":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TTSinfo;
    :goto_1
    const-string v6, ""

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "des : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1585
    return-object v1

    .line 1562
    .restart local v2    # "descType":I
    .restart local v5    # "ttsInfo":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TTSinfo;
    :cond_2
    :try_start_1
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityList:Ljava/util/ArrayList;

    sget v7, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityIndex:I

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    .line 1563
    .local v0, "cw":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getCityName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TTSinfo;->setmTTScityname(Ljava/lang/String;)V

    .line 1564
    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getTodayWeatherInfo()Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getTempScale()I

    move-result v6

    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    .line 1565
    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->getTempScaleSetting()I

    move-result v7

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getTodayWeatherInfo()Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getHighTemp()F

    move-result v8

    .line 1564
    invoke-static {v6, v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTemp(IIF)I

    move-result v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TTSinfo;->setmTTSHighTemp(Ljava/lang/String;)V

    .line 1566
    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getTodayWeatherInfo()Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getTempScale()I

    move-result v6

    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    .line 1567
    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->getTempScaleSetting()I

    move-result v7

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getTodayWeatherInfo()Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getLowTemp()F

    move-result v8

    .line 1566
    invoke-static {v6, v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTemp(IIF)I

    move-result v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TTSinfo;->setmTTSLowTemp(Ljava/lang/String;)V

    .line 1568
    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getTodayWeatherInfo()Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getTempScale()I

    move-result v6

    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    .line 1569
    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->getTempScaleSetting()I

    move-result v7

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getTodayWeatherInfo()Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getCurrentTemp()F

    move-result v8

    .line 1568
    invoke-static {v6, v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTemp(IIF)I

    move-result v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TTSinfo;->setmTTStemp(Ljava/lang/String;)V

    .line 1570
    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getTodayWeatherInfo()Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getWeatherText()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TTSinfo;->setmTTSweatherText(Ljava/lang/String;)V

    .line 1571
    sget-object v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mTz:Ljava/util/TimeZone;

    invoke-virtual {v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TTSinfo;->setTimeZone(Ljava/util/TimeZone;)V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    .line 1576
    .end local v0    # "cw":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    .end local v2    # "descType":I
    .end local v5    # "ttsInfo":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TTSinfo;
    :catch_0
    move-exception v3

    .line 1577
    .local v3, "e":Ljava/lang/NullPointerException;
    const-string v6, ""

    const-string v7, "getDesc : NullPointerException"

    invoke-static {v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1578
    invoke-virtual {v3}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto/16 :goto_1

    .line 1579
    .end local v3    # "e":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v4

    .line 1580
    .local v4, "e1":Ljava/lang/IndexOutOfBoundsException;
    const-string v6, ""

    const-string v7, "getDesc : IndexOutOfBoundsException"

    invoke-static {v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1581
    invoke-virtual {v4}, Ljava/lang/IndexOutOfBoundsException;->printStackTrace()V

    goto/16 :goto_1
.end method

.method public getResizeStep()I
    .locals 3

    .prologue
    .line 167
    const-string v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Rs Step = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mResizeStep:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 168
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mResizeStep:I

    return v0
.end method

.method public isRefreshing()Z
    .locals 1

    .prologue
    .line 1129
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->startRefresh:Z

    return v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 550
    const-string v0, ""

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 551
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mAnimationManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/AnimationManager;

    if-eqz v0, :cond_0

    .line 552
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mAnimationManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/AnimationManager;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/AnimationManager;->destroyAllAnimation()V

    .line 554
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mIconHandler:Landroid/os/Handler;

    if-eqz v0, :cond_1

    .line 555
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mIconHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 557
    :cond_1
    return-void
.end method

.method public onInflateView(ILandroid/view/View;)V
    .locals 8
    .param p1, "instance"    # I
    .param p2, "rootView"    # Landroid/view/View;

    .prologue
    const/4 v7, 0x2

    .line 237
    :try_start_0
    const-string v3, ""

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onInflateView start rsStep = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mResizeStep:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 238
    move-object v0, p2

    check-cast v0, Landroid/view/ViewGroup;

    move-object v3, v0

    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mRootView:Landroid/view/ViewGroup;

    .line 239
    const v3, 0x7f080128

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCurrentCityInfoLayout:Landroid/view/ViewGroup;

    .line 240
    const v3, 0x7f080126

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCurrentBGLayout:Landroid/view/ViewGroup;

    .line 241
    const v3, 0x7f080129

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mNextCityInfoLayout:Landroid/view/ViewGroup;

    .line 242
    const v3, 0x7f080127

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mNextBGLayout:Landroid/view/ViewGroup;

    .line 244
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCurrentCityInfoLayout:Landroid/view/ViewGroup;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 245
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCurrentBGLayout:Landroid/view/ViewGroup;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 247
    const v3, 0x7f080123

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mFrameShadow:Landroid/widget/RelativeLayout;

    .line 249
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mNextCityInfoLayout:Landroid/view/ViewGroup;

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 250
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mNextBGLayout:Landroid/view/ViewGroup;

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 252
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCurrentCityInfoLayout:Landroid/view/ViewGroup;

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mNextCityInfoLayout:Landroid/view/ViewGroup;

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCurrentBGLayout:Landroid/view/ViewGroup;

    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mNextBGLayout:Landroid/view/ViewGroup;

    invoke-static {v3, v4, v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->setViews(Landroid/view/ViewGroup;Landroid/view/ViewGroup;Landroid/view/ViewGroup;Landroid/view/ViewGroup;)V

    .line 253
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    if-eqz v3, :cond_0

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mResizeStep:I

    if-ge v3, v7, :cond_0

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mDrawListener:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager$DrawListener;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mDrawListener:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager$DrawListener;

    .line 255
    invoke-interface {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager$DrawListener;->getFirstContentRequestEvt()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 256
    :cond_0
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getSettingsForMain(Landroid/content/Context;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    .line 257
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getAllDetailWeatherInfo(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityList:Ljava/util/ArrayList;

    .line 258
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getViEffectSettings(Landroid/content/Context;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mSettingsViEffect:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    .line 262
    :cond_1
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    if-nez v3, :cond_4

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityList:Ljava/util/ArrayList;

    if-nez v3, :cond_4

    .line 263
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 264
    .local v1, "cp":Landroid/content/ContentResolver;
    if-eqz v1, :cond_2

    sget-object v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_SETTING_URI:Landroid/net/Uri;

    .line 265
    invoke-static {v1, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkAcquireContentProviderClient(Landroid/content/ContentResolver;Landroid/net/Uri;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 267
    :cond_2
    const-string v3, ""

    const-string v4, "Could not get the info in DB !!!!"

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 268
    sget-object v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCurCityInfoView:Landroid/view/ViewGroup;

    invoke-virtual {p0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->setClockLayoutContent(Landroid/view/View;)V

    .line 269
    const/4 v3, 0x2

    invoke-virtual {p0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->setResizeStep(I)V

    .line 352
    .end local v1    # "cp":Landroid/content/ContentResolver;
    :cond_3
    :goto_0
    return-void

    .line 274
    :cond_4
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->getLastSelectLocation()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mSelectedLocation:Ljava/lang/String;

    .line 276
    const-string v3, ""

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onInflateView mSL = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mSelectedLocation:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 278
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mSettingsViEffect:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->getWidgetBGOnOff()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_5

    .line 279
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mSettingsViEffect:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->getWidgetBGTransparencyVal()I

    move-result v3

    iput v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mBGTransparencyVal:I

    .line 280
    const-string v3, ""

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "bg transparency val = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mBGTransparencyVal:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 283
    :cond_5
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityList:Ljava/util/ArrayList;

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_7

    .line 284
    const-string v3, ""

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onInflateView mCL.size = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 285
    sget v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityIndex:I

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-le v3, v4, :cond_6

    .line 286
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    sput v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityIndex:I

    .line 288
    :cond_6
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityList:Ljava/util/ArrayList;

    sget v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityIndex:I

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    invoke-direct {p0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->setTimeZone(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;)V

    .line 289
    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mResizeStep:I

    if-lt v3, v7, :cond_3

    .line 290
    sget-object v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCurCityInfoView:Landroid/view/ViewGroup;

    sget-object v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCurCityBGView:Landroid/view/ViewGroup;

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityList:Ljava/util/ArrayList;

    sget v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityIndex:I

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    invoke-direct {p0, v4, v5, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->setLayoutContentNotEmpty(Landroid/view/View;Landroid/view/View;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;)V

    .line 291
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mIconHandler:Landroid/os/Handler;

    if-eqz v3, :cond_3

    .line 292
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mIconHandler:Landroid/os/Handler;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 293
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mIconHandler:Landroid/os/Handler;

    new-instance v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager$2;

    invoke-direct {v4, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager$2;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;)V

    const-wide/16 v5, 0x3e8

    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    goto/16 :goto_0

    .line 345
    :catch_0
    move-exception v2

    .line 346
    .local v2, "e":Ljava/lang/IndexOutOfBoundsException;
    invoke-virtual {v2}, Ljava/lang/IndexOutOfBoundsException;->printStackTrace()V

    .line 347
    const-string v3, ""

    const-string v4, "oIfV IOOBE"

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 327
    .end local v2    # "e":Ljava/lang/IndexOutOfBoundsException;
    :cond_7
    const/4 v3, 0x0

    :try_start_1
    invoke-direct {p0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->setTimeZone(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;)V

    .line 329
    sget v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityIndex:I

    if-lez v3, :cond_8

    .line 330
    const/4 v3, 0x0

    sput v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityIndex:I

    .line 332
    :cond_8
    const v3, 0x7f0d007a

    iput v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->msgId:I

    .line 333
    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mResizeStep:I

    if-lt v3, v7, :cond_3

    .line 334
    sget-object v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCurCityInfoView:Landroid/view/ViewGroup;

    sget-object v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCurCityBGView:Landroid/view/ViewGroup;

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->msgId:I

    invoke-direct {p0, v3, v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->setLayoutContentEmptyMsg(Landroid/view/View;Landroid/view/View;I)V
    :try_end_1
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    .line 348
    :catch_1
    move-exception v2

    .line 349
    .local v2, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v2}, Ljava/lang/NullPointerException;->printStackTrace()V

    .line 350
    const-string v3, ""

    const-string v4, "oIfV nul"

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public onInflateView(ILandroid/view/View;Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1, "instance"    # I
    .param p2, "rootView"    # Landroid/view/View;
    .param p3, "refreshListener"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 160
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mIconHandler:Landroid/os/Handler;

    .line 162
    invoke-virtual {p0, p3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->setRefreshListener(Landroid/view/View$OnClickListener;)V

    .line 163
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->onInflateView(ILandroid/view/View;)V

    .line 164
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 538
    const-string v0, ""

    const-string v1, "onPause"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 539
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCurCityInfoView:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 540
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCurCityInfoView:Landroid/view/ViewGroup;

    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->cancelIconAnimation(Landroid/view/ViewGroup;)V

    .line 541
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->isRefreshing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 542
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->stopRefreshIcon()V

    .line 544
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    .line 523
    const-string v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onResume : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mIsStartActivity:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", rsStep = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mResizeStep:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 524
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mIsStartActivity:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mResizeStep:I

    if-ge v0, v4, :cond_1

    .line 526
    :cond_0
    invoke-virtual {p0, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->setResizeStep(I)V

    .line 527
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->updateView(Z)V

    .line 528
    iput-boolean v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mIsStartActivity:Z

    .line 532
    :goto_0
    return-void

    .line 530
    :cond_1
    invoke-virtual {p0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->updateView(Z)V

    goto :goto_0
.end method

.method protected setClockLayoutContent(Landroid/view/View;)V
    .locals 12
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1402
    :try_start_0
    const-string v10, ""

    const-string v11, "setClockLayoutContent"

    invoke-static {v10, v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1403
    const v10, 0x7f08001e

    invoke-virtual {p1, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 1404
    .local v8, "weatherTime":Landroid/widget/TextView;
    const v10, 0x7f080021

    invoke-virtual {p1, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 1405
    .local v5, "weatherAmPm":Landroid/widget/TextView;
    const v10, 0x7f08001c

    invoke-virtual {p1, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 1406
    .local v6, "weatherAmPmKr":Landroid/widget/TextView;
    const v10, 0x7f080023

    invoke-virtual {p1, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 1408
    .local v7, "weatherDate":Landroid/widget/TextView;
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->makeTimeText()Ljava/lang/String;

    move-result-object v4

    .line 1409
    .local v4, "tempTime":Ljava/lang/String;
    const/4 v10, 0x0

    invoke-direct {p0, v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->makeDateText(Z)Ljava/lang/String;

    move-result-object v3

    .line 1411
    .local v3, "tempDate":Ljava/lang/String;
    sget-object v10, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->SamsungNumber3T:Landroid/graphics/Typeface;

    if-eqz v10, :cond_0

    .line 1412
    sget-object v10, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->SamsungNumber3T:Landroid/graphics/Typeface;

    invoke-virtual {v8, v10}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 1416
    :cond_0
    const v10, 0x7f08001d

    invoke-virtual {p1, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/LinearLayout;

    .line 1417
    .local v9, "weatherTimeZoneLayout":Landroid/widget/LinearLayout;
    const v10, 0x7f08001b

    invoke-virtual {p1, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 1419
    .local v0, "dstIconKr":Landroid/widget/ImageView;
    invoke-virtual {v6}, Landroid/widget/TextView;->getVisibility()I

    move-result v10

    if-eqz v10, :cond_1

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v10

    if-nez v10, :cond_4

    .line 1420
    :cond_1
    invoke-direct {p0, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->isTimeStartWithOne(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 1421
    const/4 v10, -0x6

    invoke-direct {p0, v9, v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->adjustTimeZoneLayout(Landroid/widget/LinearLayout;I)V

    .line 1434
    :goto_0
    invoke-virtual {v8, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1435
    invoke-virtual {v7, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1437
    iget-object v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mContext:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v10

    iget-object v10, v10, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v10}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    .line 1438
    .local v2, "locale":Ljava/lang/String;
    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v10

    const-string v11, "ko"

    invoke-virtual {v10, v11}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 1439
    const/4 v10, 0x4

    invoke-virtual {v5, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1440
    const/4 v10, 0x0

    invoke-virtual {v6, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1450
    :goto_1
    iget-object v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mContext:Landroid/content/Context;

    invoke-static {v10}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v10

    if-nez v10, :cond_9

    .line 1451
    iget-object v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->cal_b:Ljava/util/Calendar;

    const/16 v11, 0x9

    invoke-virtual {v10, v11}, Ljava/util/Calendar;->get(I)I

    move-result v10

    if-nez v10, :cond_8

    .line 1452
    iget-object v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mContext:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0d00a6

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v5, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1453
    iget-object v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mContext:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0d00a6

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1463
    :goto_2
    iget-object v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mDrawListener:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager$DrawListener;

    if-eqz v10, :cond_2

    .line 1464
    iget-object v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mDrawListener:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager$DrawListener;

    iget v11, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mInstance:I

    invoke-interface {v10, v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager$DrawListener;->onChangeLayout(I)V

    .line 1474
    .end local v0    # "dstIconKr":Landroid/widget/ImageView;
    .end local v2    # "locale":Ljava/lang/String;
    .end local v3    # "tempDate":Ljava/lang/String;
    .end local v4    # "tempTime":Ljava/lang/String;
    .end local v5    # "weatherAmPm":Landroid/widget/TextView;
    .end local v6    # "weatherAmPmKr":Landroid/widget/TextView;
    .end local v7    # "weatherDate":Landroid/widget/TextView;
    .end local v8    # "weatherTime":Landroid/widget/TextView;
    .end local v9    # "weatherTimeZoneLayout":Landroid/widget/LinearLayout;
    :cond_2
    :goto_3
    return-void

    .line 1423
    .restart local v0    # "dstIconKr":Landroid/widget/ImageView;
    .restart local v3    # "tempDate":Ljava/lang/String;
    .restart local v4    # "tempTime":Ljava/lang/String;
    .restart local v5    # "weatherAmPm":Landroid/widget/TextView;
    .restart local v6    # "weatherAmPmKr":Landroid/widget/TextView;
    .restart local v7    # "weatherDate":Landroid/widget/TextView;
    .restart local v8    # "weatherTime":Landroid/widget/TextView;
    .restart local v9    # "weatherTimeZoneLayout":Landroid/widget/LinearLayout;
    :cond_3
    const/4 v10, 0x0

    invoke-direct {p0, v9, v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->adjustTimeZoneLayout(Landroid/widget/LinearLayout;I)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1470
    .end local v0    # "dstIconKr":Landroid/widget/ImageView;
    .end local v3    # "tempDate":Ljava/lang/String;
    .end local v4    # "tempTime":Ljava/lang/String;
    .end local v5    # "weatherAmPm":Landroid/widget/TextView;
    .end local v6    # "weatherAmPmKr":Landroid/widget/TextView;
    .end local v7    # "weatherDate":Landroid/widget/TextView;
    .end local v8    # "weatherTime":Landroid/widget/TextView;
    .end local v9    # "weatherTimeZoneLayout":Landroid/widget/LinearLayout;
    :catch_0
    move-exception v1

    .line 1471
    .local v1, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v1}, Ljava/lang/NullPointerException;->printStackTrace()V

    .line 1472
    const-string v10, ""

    const-string v11, "sCLC null "

    invoke-static {v10, v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 1426
    .end local v1    # "e":Ljava/lang/NullPointerException;
    .restart local v0    # "dstIconKr":Landroid/widget/ImageView;
    .restart local v3    # "tempDate":Ljava/lang/String;
    .restart local v4    # "tempTime":Ljava/lang/String;
    .restart local v5    # "weatherAmPm":Landroid/widget/TextView;
    .restart local v6    # "weatherAmPmKr":Landroid/widget/TextView;
    .restart local v7    # "weatherDate":Landroid/widget/TextView;
    .restart local v8    # "weatherTime":Landroid/widget/TextView;
    .restart local v9    # "weatherTimeZoneLayout":Landroid/widget/LinearLayout;
    :cond_4
    :try_start_1
    invoke-direct {p0, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->isTimeStartWithOne(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 1427
    const/16 v10, -0xa

    invoke-direct {p0, v9, v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->adjustTimeZoneLayout(Landroid/widget/LinearLayout;I)V

    goto/16 :goto_0

    .line 1429
    :cond_5
    const/4 v10, -0x6

    invoke-direct {p0, v9, v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->adjustTimeZoneLayout(Landroid/widget/LinearLayout;I)V

    goto/16 :goto_0

    .line 1441
    .restart local v2    # "locale":Ljava/lang/String;
    :cond_6
    iget-object v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mContext:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v10

    iget-object v10, v10, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-static {v10}, Landroid/text/TextUtils;->getLayoutDirectionFromLocale(Ljava/util/Locale;)I

    move-result v10

    const/4 v11, 0x1

    if-ne v10, v11, :cond_7

    .line 1442
    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v10

    const-string v11, "ur"

    invoke-virtual {v10, v11}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_7

    .line 1443
    const/4 v10, 0x4

    invoke-virtual {v5, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1444
    const/4 v10, 0x0

    invoke-virtual {v6, v10}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1

    .line 1446
    :cond_7
    const/4 v10, 0x0

    invoke-virtual {v5, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1447
    const/16 v10, 0x8

    invoke-virtual {v6, v10}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1

    .line 1455
    :cond_8
    iget-object v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mContext:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0d00a7

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v5, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1456
    iget-object v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mContext:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0d00a7

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 1459
    :cond_9
    const/4 v10, 0x4

    invoke-virtual {v5, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1460
    const/16 v10, 0x8

    invoke-virtual {v6, v10}, Landroid/widget/TextView;->setVisibility(I)V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_2
.end method

.method public setIsStartScrollAnimation(Z)V
    .locals 0
    .param p1, "isStartScrollAnimation"    # Z

    .prologue
    .line 213
    iput-boolean p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mIsStartScrollAnimation:Z

    .line 214
    return-void
.end method

.method protected setLayoutContentNotEmptyMsg(Landroid/view/View;Landroid/view/View;ILcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;)V
    .locals 10
    .param p1, "v_info"    # Landroid/view/View;
    .param p2, "v_bg"    # Landroid/view/View;
    .param p3, "id"    # I
    .param p4, "cityInfo"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    .prologue
    .line 1087
    :try_start_0
    invoke-virtual {p4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getLocation()Ljava/lang/String;

    move-result-object v7

    const-string v8, "cityId:current"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1088
    const-string v7, ""

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "setLayoutContentNotEmptyMsg = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", rL ="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getRealLocation()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1095
    :goto_0
    const v7, 0x7f080016

    invoke-virtual {p2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 1096
    .local v1, "bgImage":Landroid/widget/ImageView;
    const v7, 0x7f080017

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 1097
    .local v3, "errorText":Landroid/widget/TextView;
    const v7, 0x7f080024

    .line 1098
    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 1100
    .local v0, "anotherInfoGroup":Landroid/widget/RelativeLayout;
    const/4 v7, 0x0

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1101
    invoke-virtual {v3, p3}, Landroid/widget/TextView;->setText(I)V

    .line 1102
    const-string v7, ""

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "error text = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1103
    const/4 v7, 0x4

    invoke-virtual {v0, v7}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1105
    sget-object v7, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mTz:Ljava/util/TimeZone;

    invoke-virtual {p4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getTodayWeatherInfo()Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getSunRiseTime()Ljava/lang/String;

    move-result-object v8

    .line 1106
    invoke-virtual {p4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getTodayWeatherInfo()Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getSunSetTime()Ljava/lang/String;

    move-result-object v9

    .line 1105
    invoke-static {v7, v8, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->isDay(Ljava/util/TimeZone;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v6

    .line 1108
    .local v6, "isDay":Z
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityDayOrNight:Ljava/util/HashMap;

    invoke-virtual {p4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getLocation()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1109
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mDetailWeatherInfo:Ljava/util/HashMap;

    invoke-virtual {p4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getLocation()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8, p4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1111
    invoke-virtual {p4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getTodayWeatherInfo()Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getIconNum()I

    move-result v4

    .line 1112
    .local v4, "iconNum":I
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mContext:Landroid/content/Context;

    const/16 v8, 0x105

    invoke-static {v7, v6, v4, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getTodayBG(Landroid/content/Context;ZII)I

    move-result v5

    .line 1113
    .local v5, "imageID":I
    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1115
    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mBGTransparencyVal:I

    invoke-static {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->calAlphaVal(I)I

    move-result v7

    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setImageAlpha(I)V

    .line 1118
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->setEventListener(Landroid/view/View;)V

    .line 1119
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->setShadowVisibility()V

    .line 1120
    invoke-virtual {p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->setClockLayoutContent(Landroid/view/View;)V

    .line 1122
    const-string v7, ""

    const-string v8, "complete setLayoutContentNotEmptyMsg Content"

    invoke-static {v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1126
    .end local v0    # "anotherInfoGroup":Landroid/widget/RelativeLayout;
    .end local v1    # "bgImage":Landroid/widget/ImageView;
    .end local v3    # "errorText":Landroid/widget/TextView;
    .end local v4    # "iconNum":I
    .end local v5    # "imageID":I
    .end local v6    # "isDay":Z
    :goto_1
    return-void

    .line 1090
    :cond_0
    const-string v7, ""

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "setLayoutContentNotEmptyMsg = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", l ="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getLocation()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 1123
    :catch_0
    move-exception v2

    .line 1124
    .local v2, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v2}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_1
.end method

.method public setRefreshListener(Landroid/view/View$OnClickListener;)V
    .locals 2
    .param p1, "l"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 739
    if-eqz p1, :cond_0

    .line 740
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mRefreshListener:Landroid/view/View$OnClickListener;

    .line 744
    :goto_0
    return-void

    .line 742
    :cond_0
    const-string v0, ""

    const-string v1, "refresh listener is null"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setResizeStep(I)V
    .locals 3
    .param p1, "resizeStep"    # I

    .prologue
    .line 172
    const-string v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Rs Step = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 173
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mResizeStep:I

    .line 174
    return-void
.end method

.method public setShadowVisibility()V
    .locals 2

    .prologue
    .line 218
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mBGTransparencyVal:I

    const/16 v1, 0x64

    if-ne v0, v1, :cond_1

    .line 219
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mFrameShadow:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 220
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mFrameShadow:Landroid/widget/RelativeLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 227
    :cond_0
    :goto_0
    return-void

    .line 223
    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mFrameShadow:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 224
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mFrameShadow:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method public showMessage(I)V
    .locals 6
    .param p1, "id"    # I

    .prologue
    .line 486
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCurCityInfoView:Landroid/view/ViewGroup;

    if-nez v1, :cond_0

    .line 487
    const-string v1, ""

    const-string v2, "mCurView is null!!"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 517
    :goto_0
    return-void

    .line 490
    :cond_0
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityList:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 491
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 496
    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getAllDetailWeatherInfo(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityList:Ljava/util/ArrayList;

    .line 497
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->msgId:I

    .line 499
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityList:Ljava/util/ArrayList;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_3

    .line 500
    sget v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityIndex:I

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-le v1, v2, :cond_2

    .line 501
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    sput v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityIndex:I

    .line 503
    :cond_2
    sget-object v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCurCityInfoView:Landroid/view/ViewGroup;

    sget-object v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCurCityBGView:Landroid/view/ViewGroup;

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->msgId:I

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityList:Ljava/util/ArrayList;

    sget v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityIndex:I

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    invoke-virtual {p0, v2, v3, v4, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->setLayoutContentNotEmptyMsg(Landroid/view/View;Landroid/view/View;ILcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;)V
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 510
    :catch_0
    move-exception v0

    .line 511
    .local v0, "e":Ljava/lang/IndexOutOfBoundsException;
    invoke-virtual {v0}, Ljava/lang/IndexOutOfBoundsException;->printStackTrace()V

    .line 512
    const-string v1, ""

    const-string v2, "sM IOOBE"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 505
    .end local v0    # "e":Ljava/lang/IndexOutOfBoundsException;
    :cond_3
    :try_start_1
    sget v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityIndex:I

    if-lez v1, :cond_4

    .line 506
    const/4 v1, 0x0

    sput v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityIndex:I

    .line 508
    :cond_4
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCurCityInfoView:Landroid/view/ViewGroup;

    sget-object v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCurCityBGView:Landroid/view/ViewGroup;

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->msgId:I

    invoke-direct {p0, v1, v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->setLayoutContentEmptyMsg(Landroid/view/View;Landroid/view/View;I)V
    :try_end_1
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 513
    :catch_1
    move-exception v0

    .line 514
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    .line 515
    const-string v1, ""

    const-string v2, "sM nul"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public startActivity(I)V
    .locals 6
    .param p1, "index"    # I

    .prologue
    .line 707
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->getAutoRefreshTime()I

    move-result v1

    .line 709
    .local v1, "result":I
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityList:Ljava/util/ArrayList;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_2

    .line 710
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt p1, v3, :cond_1

    .line 711
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityList:Ljava/util/ArrayList;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getLocation()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mSelectedLocation:Ljava/lang/String;

    .line 719
    :goto_0
    const/4 v2, 0x0

    .line 720
    .local v2, "size":I
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityList:Ljava/util/ArrayList;

    if-eqz v3, :cond_0

    .line 721
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 723
    :cond_0
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mSelectedLocation:Ljava/lang/String;

    const/16 v5, 0xfa0

    invoke-static {v3, v4, v5, v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getIntentForWidget(Landroid/content/Context;Ljava/lang/String;III)Landroid/content/Intent;

    move-result-object v0

    .line 725
    .local v0, "intent":Landroid/content/Intent;
    if-nez v0, :cond_3

    .line 726
    const-string v3, ""

    const-string v4, "intent is n"

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 731
    :goto_1
    return-void

    .line 713
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v2    # "size":I
    :cond_1
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityList:Ljava/util/ArrayList;

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getLocation()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mSelectedLocation:Ljava/lang/String;

    goto :goto_0

    .line 716
    :cond_2
    const-string v3, " "

    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mSelectedLocation:Ljava/lang/String;

    goto :goto_0

    .line 729
    .restart local v0    # "intent":Landroid/content/Intent;
    .restart local v2    # "size":I
    :cond_3
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mIsStartActivity:Z

    .line 730
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_1
.end method

.method public startIconAnimation(Landroid/view/ViewGroup;)V
    .locals 6
    .param p1, "root"    # Landroid/view/ViewGroup;

    .prologue
    .line 1179
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mSettingsViEffect:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mIconManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;

    if-nez v2, :cond_1

    .line 1217
    :cond_0
    :goto_0
    return-void

    .line 1183
    :cond_1
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mWeatherRenderer:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mWeatherRenderer:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;->isPaused()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1184
    const-string v2, ""

    const-string v3, "sIA render paused "

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1188
    :cond_2
    if-nez p1, :cond_3

    .line 1189
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCurrentCityInfoLayout:Landroid/view/ViewGroup;

    if-eqz v2, :cond_4

    .line 1190
    iget-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCurrentCityInfoLayout:Landroid/view/ViewGroup;

    .line 1196
    :cond_3
    const-string v2, ""

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "startIconAnimation : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mSettingsViEffect:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->getWidgetIconAnimation()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1197
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mSettingsViEffect:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->getWidgetIconAnimation()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    if-eqz p1, :cond_0

    .line 1198
    const v2, 0x7f080030

    invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 1199
    .local v1, "parent":Landroid/view/ViewGroup;
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mIconManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;

    invoke-virtual {v2, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;->startIconAnimation(Landroid/view/ViewGroup;)V

    .line 1200
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mIconHandler:Landroid/os/Handler;

    if-eqz v2, :cond_0

    .line 1201
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mIconHandler:Landroid/os/Handler;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1202
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mIconHandler:Landroid/os/Handler;

    new-instance v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager$5;

    invoke-direct {v3, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager$5;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;)V

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mIconManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mSettingsViEffect:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    .line 1211
    invoke-virtual {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->getIconDurationVal()I

    move-result v5

    invoke-virtual {v4, v1, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;->calcAnimationDuration(Landroid/view/ViewGroup;I)J

    move-result-wide v4

    .line 1202
    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1214
    .end local v1    # "parent":Landroid/view/ViewGroup;
    :catch_0
    move-exception v0

    .line 1215
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 1192
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_4
    :try_start_1
    const-string v2, ""

    const-string v3, "CCILv null!"

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method public startRefreshIcon()V
    .locals 5

    .prologue
    .line 1137
    :try_start_0
    sget-object v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCurCityInfoView:Landroid/view/ViewGroup;

    const v4, 0x7f080035

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 1138
    .local v1, "refreshBtn":Landroid/widget/ImageView;
    sget-object v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCurCityInfoView:Landroid/view/ViewGroup;

    const v4, 0x7f080034

    .line 1139
    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ProgressBar;

    .line 1141
    .local v2, "refreshflipper":Landroid/widget/ProgressBar;
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->startRefresh:Z

    .line 1143
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1144
    const/4 v3, 0x4

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1145
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1146
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/ProgressBar;->setFocusable(Z)V

    .line 1147
    invoke-virtual {v2}, Landroid/widget/ProgressBar;->requestFocus()Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1151
    .end local v1    # "refreshBtn":Landroid/widget/ImageView;
    .end local v2    # "refreshflipper":Landroid/widget/ProgressBar;
    :goto_0
    return-void

    .line 1148
    :catch_0
    move-exception v0

    .line 1149
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method public stopIconAnimation(Landroid/view/ViewGroup;)V
    .locals 4
    .param p1, "root"    # Landroid/view/ViewGroup;

    .prologue
    .line 1225
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mIconManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;

    if-nez v2, :cond_1

    .line 1249
    :cond_0
    :goto_0
    return-void

    .line 1228
    :cond_1
    if-nez p1, :cond_2

    .line 1229
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCurrentCityInfoLayout:Landroid/view/ViewGroup;

    if-eqz v2, :cond_3

    .line 1230
    iget-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCurrentCityInfoLayout:Landroid/view/ViewGroup;

    .line 1236
    :cond_2
    if-eqz p1, :cond_0

    .line 1237
    const v2, 0x7f080030

    invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 1238
    .local v1, "parent":Landroid/view/ViewGroup;
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mIconManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;

    invoke-virtual {v2, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;->isRunningIconAnimation(Landroid/view/ViewGroup;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1239
    const-string v2, ""

    const-string v3, "stopIconAnimation"

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1240
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mIconManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;

    invoke-virtual {v2, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;->stopIconAnimation(Landroid/view/ViewGroup;)V

    .line 1241
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mIconHandler:Landroid/os/Handler;

    if-eqz v2, :cond_0

    .line 1242
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mIconHandler:Landroid/os/Handler;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1246
    .end local v1    # "parent":Landroid/view/ViewGroup;
    :catch_0
    move-exception v0

    .line 1247
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 1232
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_3
    :try_start_1
    const-string v2, ""

    const-string v3, "CCILv null!"

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public stopRefreshIcon()V
    .locals 5

    .prologue
    .line 1158
    :try_start_0
    sget-object v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCurCityInfoView:Landroid/view/ViewGroup;

    const v4, 0x7f080035

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 1159
    .local v1, "refreshBtn":Landroid/widget/ImageView;
    sget-object v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCurCityInfoView:Landroid/view/ViewGroup;

    const v4, 0x7f080034

    .line 1160
    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ProgressBar;

    .line 1162
    .local v2, "refreshflipper":Landroid/widget/ProgressBar;
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->startRefresh:Z

    .line 1164
    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1165
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1166
    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1167
    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 1168
    invoke-virtual {v1}, Landroid/widget/ImageView;->requestFocus()Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1172
    .end local v1    # "refreshBtn":Landroid/widget/ImageView;
    .end local v2    # "refreshflipper":Landroid/widget/ProgressBar;
    :goto_0
    return-void

    .line 1169
    :catch_0
    move-exception v0

    .line 1170
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method public swapView()V
    .locals 3

    .prologue
    .line 693
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mNextCityInfoView:Landroid/view/ViewGroup;

    .line 694
    .local v1, "temp_info":Landroid/view/ViewGroup;
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mNextCityBGView:Landroid/view/ViewGroup;

    .line 695
    .local v0, "temp_bg":Landroid/view/ViewGroup;
    sget-object v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCurCityInfoView:Landroid/view/ViewGroup;

    sput-object v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mNextCityInfoView:Landroid/view/ViewGroup;

    .line 696
    sget-object v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCurCityBGView:Landroid/view/ViewGroup;

    sput-object v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mNextCityBGView:Landroid/view/ViewGroup;

    .line 697
    sput-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCurCityInfoView:Landroid/view/ViewGroup;

    .line 698
    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCurCityBGView:Landroid/view/ViewGroup;

    .line 699
    return-void
.end method

.method protected updateClock(Z)V
    .locals 6
    .param p1, "isChangeTimeZone"    # Z

    .prologue
    .line 1355
    sget-object v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCurCityInfoView:Landroid/view/ViewGroup;

    if-nez v3, :cond_0

    .line 1356
    const-string v3, ""

    const-string v4, "mCurView is null!!"

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1394
    :goto_0
    return-void

    .line 1377
    :cond_0
    if-eqz p1, :cond_2

    .line 1378
    const/4 v1, 0x0

    .line 1379
    .local v1, "cityInfo":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityList:Ljava/util/ArrayList;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_1

    .line 1381
    :try_start_0
    sget v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityIndex:I

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v3, v4, :cond_3

    .line 1382
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityList:Ljava/util/ArrayList;

    sget v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityIndex:I

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    move-object v1, v0
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1390
    :cond_1
    :goto_1
    invoke-direct {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->setTimeZone(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;)V

    .line 1392
    .end local v1    # "cityInfo":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    :cond_2
    sget-object v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCurCityInfoView:Landroid/view/ViewGroup;

    invoke-virtual {p0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->setClockLayoutContent(Landroid/view/View;)V

    goto :goto_0

    .line 1384
    .restart local v1    # "cityInfo":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    :cond_3
    :try_start_1
    const-string v3, ""

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[updateClock] s = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", i ="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityIndex:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 1386
    :catch_0
    move-exception v2

    .line 1387
    .local v2, "e":Ljava/lang/IndexOutOfBoundsException;
    invoke-virtual {v2}, Ljava/lang/IndexOutOfBoundsException;->printStackTrace()V

    goto :goto_1
.end method

.method public updateView(Z)V
    .locals 5
    .param p1, "isStartAnim"    # Z

    .prologue
    .line 424
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getSettingsForMain(Landroid/content/Context;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    .line 425
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getViEffectSettings(Landroid/content/Context;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mSettingsViEffect:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    .line 427
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityList:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 428
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 431
    :cond_0
    if-eqz p1, :cond_1

    .line 432
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityDayOrNight:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    .line 433
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mDetailWeatherInfo:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    .line 436
    :cond_1
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getAllDetailWeatherInfo(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityList:Ljava/util/ArrayList;

    .line 438
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->getLastSelectCityIndex()I

    move-result v1

    sput v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityIndex:I

    .line 440
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->getLastSelectLocation()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mSelectedLocation:Ljava/lang/String;

    .line 442
    const-string v1, ""

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "updateView iSA = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mCI = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityIndex:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mSL = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mSelectedLocation:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " , cls = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    .line 443
    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->getCurrentLoccation()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " , cls = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->getTempScaleSetting()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 442
    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 445
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mSettingsViEffect:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->getWidgetBGOnOff()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    .line 446
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mSettingsViEffect:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->getWidgetBGTransparencyVal()I

    move-result v1

    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mBGTransparencyVal:I

    .line 447
    const-string v1, ""

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "bg transparency val = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mBGTransparencyVal:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 456
    :cond_2
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityList:Ljava/util/ArrayList;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_5

    .line 457
    const-string v1, ""

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "updateView mCL.size = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 458
    sget v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityIndex:I

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-le v1, v2, :cond_3

    .line 459
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    sput v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityIndex:I

    .line 461
    :cond_3
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityList:Ljava/util/ArrayList;

    sget v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityIndex:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    invoke-direct {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->setTimeZone(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;)V

    .line 462
    sget-object v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCurCityInfoView:Landroid/view/ViewGroup;

    sget-object v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCurCityBGView:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityList:Ljava/util/ArrayList;

    sget v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityIndex:I

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    invoke-direct {p0, v2, v3, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->setLayoutContentNotEmpty(Landroid/view/View;Landroid/view/View;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;)V

    .line 463
    if-eqz p1, :cond_4

    .line 464
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCurCityInfoView:Landroid/view/ViewGroup;

    invoke-virtual {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->startIconAnimation(Landroid/view/ViewGroup;)V

    .line 478
    :cond_4
    :goto_0
    return-void

    .line 468
    :cond_5
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->setTimeZone(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;)V

    .line 470
    sget v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityIndex:I

    if-lez v1, :cond_6

    .line 471
    const/4 v1, 0x0

    sput v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCityIndex:I

    .line 473
    :cond_6
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCurCityInfoView:Landroid/view/ViewGroup;

    sget-object v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCurCityBGView:Landroid/view/ViewGroup;

    const v3, 0x7f0d007a

    invoke-direct {p0, v1, v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->setLayoutContentEmptyMsg(Landroid/view/View;Landroid/view/View;I)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 475
    :catch_0
    move-exception v0

    .line 476
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method
