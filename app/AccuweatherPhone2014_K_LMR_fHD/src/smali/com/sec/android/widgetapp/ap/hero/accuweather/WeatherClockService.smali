.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockService;
.super Landroid/app/Service;
.source "WeatherClockService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockService$DateFormatObserver;
    }
.end annotation


# static fields
.field static final DATE_FORMAT_CHANGED:Ljava/lang/String; = "clock.date_format_changed"

.field static final NETWORK_SET_TIMEZONE:Ljava/lang/String; = "android.intent.action.NETWORK_SET_TIMEZONE"

.field private static final TAG:Ljava/lang/String; = "WeatherClockService"


# instance fields
.field private mContactObserver:Landroid/database/ContentObserver;

.field private mDateFormatObserver:Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockService$DateFormatObserver;

.field protected mIntent:Landroid/content/Intent;

.field private mReceiver:Landroid/content/BroadcastReceiver;

.field private mSettingsObserver:Landroid/database/ContentObserver;

.field protected value:Ljava/lang/String;

.field protected what:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 26
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 34
    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockService;->value:Ljava/lang/String;

    .line 36
    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockService;->mIntent:Landroid/content/Intent;

    .line 122
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockService$1;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockService$1;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockService;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockService;->mContactObserver:Landroid/database/ContentObserver;

    .line 128
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockService$2;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockService$2;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockService;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockService;->mSettingsObserver:Landroid/database/ContentObserver;

    .line 135
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockService$3;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockService$3;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockService;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockService;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 174
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "arg0"    # Landroid/content/Intent;

    .prologue
    .line 119
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 6

    .prologue
    .line 39
    const-string v2, ""

    const-string v3, "onCreate()"

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 40
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 41
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v2, "android.intent.action.TIMEZONE_CHANGED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 42
    const-string v2, "android.intent.action.TIME_SET"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 43
    const-string v2, "android.intent.action.DATE_CHANGED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 44
    const-string v2, "android.intent.action.TIME_TICK"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 45
    const-string v2, "android.intent.action.CONFIGURATION_CHANGED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 46
    const-string v2, "android.intent.action.NETWORK_SET_TIMEZONE"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 48
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockService;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v2, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 50
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 51
    .local v1, "filter1":Landroid/content/IntentFilter;
    const-string v2, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 53
    new-instance v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockService$DateFormatObserver;

    invoke-direct {v2, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockService$DateFormatObserver;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockService;)V

    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockService;->mDateFormatObserver:Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockService$DateFormatObserver;

    .line 56
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "content://settings/system/date_format"

    .line 57
    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockService;->mDateFormatObserver:Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockService$DateFormatObserver;

    .line 56
    invoke-virtual {v2, v3, v4, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 62
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockService;->mContactObserver:Landroid/database/ContentObserver;

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 82
    return-void
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 90
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockService;->mReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 91
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockService;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 94
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockService;->mDateFormatObserver:Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockService$DateFormatObserver;

    if-eqz v0, :cond_1

    .line 95
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockService;->mDateFormatObserver:Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockService$DateFormatObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 102
    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockService;->mContactObserver:Landroid/database/ContentObserver;

    if-eqz v0, :cond_2

    .line 103
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockService;->mContactObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 106
    :cond_2
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockService;->mSettingsObserver:Landroid/database/ContentObserver;

    if-eqz v0, :cond_3

    .line 107
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockService;->mSettingsObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 110
    :cond_3
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockService;->mIntent:Landroid/content/Intent;

    .line 111
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockService;->value:Ljava/lang/String;

    .line 113
    const-string v0, ""

    const-string v1, "onDestroy : End"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 116
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 85
    const/4 v0, 0x1

    return v0
.end method
