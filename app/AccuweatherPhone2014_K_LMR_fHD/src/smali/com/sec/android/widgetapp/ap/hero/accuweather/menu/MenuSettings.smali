.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;
.super Landroid/preference/PreferenceActivity;
.source "MenuSettings.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Landroid/preference/Preference$OnPreferenceClickListener;
.implements Lcom/sec/android/widgetapp/ap/hero/accuweather/model/IActivityVisibleState;


# static fields
.field private static final KEY_AUTOREFRESH:Ljava/lang/String; = "autorefresh"

.field private static final KEY_BACKGROUND_VIDEO:Ljava/lang/String; = "background_video"

.field private static final KEY_BACKGROUND_VIDEO_REPEAT:Ljava/lang/String; = "background_video_repeat"

.field private static final KEY_BOTTOM_ICON_ANIMATION:Ljava/lang/String; = "bottom_icon_animation"

.field private static final KEY_CHANGE_CITY_SCROLL:Ljava/lang/String; = "change_city_scroll"

.field private static final KEY_CITY_SELECT_ANIMATION:Ljava/lang/String; = "city_select_animation"

.field private static final KEY_DURATION_TIME:Ljava/lang/String; = "duration_time"

.field private static final KEY_ICON_ANIMATION:Ljava/lang/String; = "icon_animation"

.field private static final KEY_ICON_ANIMATION_REPEAT:Ljava/lang/String; = "icon_animation_repeat"

.field private static final KEY_INFOR_ANIMATION:Ljava/lang/String; = "infor_animation"

.field private static final KEY_REFRESH_ENTERING:Ljava/lang/String; = "refreshentering"

.field private static final KEY_REFRESH_ROAMING:Ljava/lang/String; = "refreshroaming"

.field private static final KEY_SET_CURRENT_CITY:Ljava/lang/String; = "setcurrentcity"

.field private static final KEY_SET_NOTIFICATIONS:Ljava/lang/String; = "setnotifications"

.field private static final KEY_SET_NOTIFICATIONS_TIME:Ljava/lang/String; = "setnotificationtime"

.field private static final KEY_SOFTWARE_UPDATE:Ljava/lang/String; = "software_update"

.field private static final KEY_UNIT:Ljava/lang/String; = "unit"

.field private static final KEY_WEATHER_EFFECT:Ljava/lang/String; = "weather_effect"

.field private static final KEY_WIDGET_BACKGROUND_ONOFF:Ljava/lang/String; = "widgetBGOnOff"

.field private static final KEY_WIDGET_BACKGROUND_TRANSPARENCY_CONTROL:Ljava/lang/String; = "widgetBGTransparencyControl"

.field private static final KEY_WIDGET_CITY_CHANGE_ANIMATION:Ljava/lang/String; = "widget_city_change_animation"

.field private static final KEY_WIDGET_ICON_ANIMATION:Ljava/lang/String; = "widget_icon_animation"

.field private static len:I

.field static notiRequest:I

.field private static sdf:Ljava/text/SimpleDateFormat;


# instance fields
.field private CheckType:I

.field private actionbar:Landroid/app/ActionBar;

.field private actionbarBack:Landroid/widget/LinearLayout;

.field private actionbarLayout:Landroid/widget/RelativeLayout;

.field private actionbarTitle:Landroid/widget/TextView;

.field private autorefresh_preferences:Landroid/preference/ListPreference;

.field private background_video_preferences:Landroid/preference/CheckBoxPreference;

.field private background_video_repeat_preferences:Landroid/preference/CheckBoxPreference;

.field private bottom_icon_animation_preferences:Landroid/preference/CheckBoxPreference;

.field ch_location:I

.field private changecity_scroll_preferences:Landroid/preference/CheckBoxPreference;

.field private city_select_animation_preferences:Landroid/preference/CheckBoxPreference;

.field creating:Z

.field devSetting:Z

.field private durationDefalut:Ljava/lang/String;

.field private duration_time_preferences:Landroid/preference/Preference;

.field private getDataHandler:Landroid/os/Handler;

.field getLocDataHandler:Landroid/os/Handler;

.field private icon_animation_preferences:Landroid/preference/CheckBoxPreference;

.field private icon_animation_repeat_preferences:Landroid/preference/CheckBoxPreference;

.field private infor_animation_preferences:Landroid/preference/CheckBoxPreference;

.field isFirstGps:Z

.field location:I

.field private mCtx:Landroid/content/Context;

.field mHour:I

.field public mHttpThreads:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Thread;",
            ">;"
        }
    .end annotation
.end field

.field mIsVisible:Z

.field public mLoadingDialog:Landroid/app/Dialog;

.field mMinute:I

.field private mMoveHandler:Landroid/os/Handler;

.field public mNetworkErrorDialog:Landroid/app/Dialog;

.field public mPopupDialog:Landroid/app/Dialog;

.field private mcl:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

.field private refreshItems:[Ljava/lang/String;

.field private refreshItemsValue:[Ljava/lang/String;

.field private refreshTimes:[I

.field private refreshentering_preferences:Landroid/preference/CheckBoxPreference;

.field private refreshroaming_preferences:Landroid/preference/CheckBoxPreference;

.field private set_current_city_preferences:Landroid/preference/CheckBoxPreference;

.field private set_notifications_preferences:Landroid/preference/CheckBoxPreference;

.field private set_notificationsettime_preferences:Landroid/preference/Preference;

.field private timeListener:Landroid/app/TimePickerDialog$OnTimeSetListener;

.field timePickerDialog:Landroid/app/TimePickerDialog;

.field private unit_preferences:Landroid/preference/ListPreference;

.field private useCurrentCity_preferences:Landroid/preference/Preference;

.field private weather_effect_preferences:Landroid/preference/CheckBoxPreference;

.field private widgetBGOnOff_preferences:Landroid/preference/CheckBoxPreference;

.field private widgetBGTransparencyControl_preferences:Landroid/preference/PreferenceScreen;

.field private widget_city_change_animation_preferences:Landroid/preference/CheckBoxPreference;

.field private widget_icon_animation_preferences:Landroid/preference/CheckBoxPreference;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1784
    const/4 v0, 0x1

    sput v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->notiRequest:I

    return-void
.end method

.method public constructor <init>()V
    .locals 6

    .prologue
    const/4 v5, 0x6

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 68
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    .line 167
    new-array v0, v5, [Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->refreshItems:[Ljava/lang/String;

    .line 169
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "0"

    aput-object v1, v0, v3

    const/4 v1, 0x1

    const-string v2, "1"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "2"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "3"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "4"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "5"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->refreshItemsValue:[Ljava/lang/String;

    .line 173
    new-array v0, v5, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->refreshTimes:[I

    .line 185
    const/16 v0, 0x63

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->ch_location:I

    .line 193
    iput-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->actionbarTitle:Landroid/widget/TextView;

    .line 197
    iput v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->CheckType:I

    .line 200
    iput-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mLoadingDialog:Landroid/app/Dialog;

    .line 202
    iput-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mPopupDialog:Landroid/app/Dialog;

    .line 204
    iput-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mNetworkErrorDialog:Landroid/app/Dialog;

    .line 209
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mHttpThreads:Ljava/util/ArrayList;

    .line 211
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->getDataHandler:Landroid/os/Handler;

    .line 213
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mMoveHandler:Landroid/os/Handler;

    .line 258
    iput-boolean v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->creating:Z

    .line 260
    iput-boolean v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->devSetting:Z

    .line 262
    iput-boolean v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->isFirstGps:Z

    .line 264
    const-string v0, "5"

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->durationDefalut:Ljava/lang/String;

    .line 1148
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$4;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$4;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->getLocDataHandler:Landroid/os/Handler;

    .line 1780
    iput v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mMinute:I

    .line 1875
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$18;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$18;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->timeListener:Landroid/app/TimePickerDialog$OnTimeSetListener;

    return-void

    .line 173
    :array_0
    .array-data 4
        0x0
        0x1
        0x3
        0x6
        0xc
        0x18
    .end array-data
.end method

.method public static SaveTemperaturesData(Landroid/content/Context;II)V
    .locals 12
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "result"    # I
    .param p2, "tempScale"    # I

    .prologue
    .line 2015
    const-string v9, "cityId:current"

    invoke-static {p0, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->isRegisteredToCityList(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    .line 2016
    .local v0, "currentDBTable":Z
    const-string v9, ""

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "curDTb :"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2017
    if-nez v0, :cond_1

    .line 2072
    :cond_0
    :goto_0
    return-void

    .line 2021
    :cond_1
    const-string v9, "cityId:current"

    invoke-static {p0, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getDaemonDetailWeatherInfo(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    move-result-object v3

    .line 2022
    .local v3, "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    if-eqz v3, :cond_0

    .line 2023
    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getTodayWeatherInfo()Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    move-result-object v7

    .line 2024
    .local v7, "today":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;
    if-eqz v7, :cond_2

    .line 2025
    invoke-virtual {v7, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setTempScale(I)V

    .line 2027
    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getCurrentTemp()F

    move-result v9

    .line 2026
    invoke-static {p2, p1, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTempScale(IIF)F

    move-result v9

    invoke-virtual {v7, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setCurrentTemp(F)V

    .line 2028
    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getHighTemp()F

    move-result v9

    invoke-static {p2, p1, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTempScale(IIF)F

    move-result v9

    invoke-virtual {v7, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setHighTemp(F)V

    .line 2029
    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getLowTemp()F

    move-result v9

    invoke-static {p2, p1, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTempScale(IIF)F

    move-result v9

    invoke-virtual {v7, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setLowTemp(F)V

    .line 2030
    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getRealFeel()F

    move-result v9

    invoke-static {p2, p1, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTempScale(IIF)F

    move-result v9

    invoke-virtual {v7, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setRealFeel(F)V

    .line 2033
    :cond_2
    const/4 v9, 0x0

    invoke-virtual {v3, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getForecastInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    move-result-object v4

    .line 2034
    .local v4, "oneday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    if-eqz v4, :cond_3

    .line 2035
    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getHighTemp()F

    move-result v9

    invoke-static {p2, p1, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTempScale(IIF)F

    move-result v9

    invoke-virtual {v4, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setHighTemp(F)V

    .line 2036
    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getLowTemp()F

    move-result v9

    invoke-static {p2, p1, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTempScale(IIF)F

    move-result v9

    invoke-virtual {v4, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setLowTemp(F)V

    .line 2039
    :cond_3
    const/4 v9, 0x1

    invoke-virtual {v3, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getForecastInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    move-result-object v8

    .line 2040
    .local v8, "twoday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    if-eqz v8, :cond_4

    .line 2041
    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getHighTemp()F

    move-result v9

    invoke-static {p2, p1, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTempScale(IIF)F

    move-result v9

    invoke-virtual {v8, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setHighTemp(F)V

    .line 2042
    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getLowTemp()F

    move-result v9

    invoke-static {p2, p1, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTempScale(IIF)F

    move-result v9

    invoke-virtual {v8, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setLowTemp(F)V

    .line 2045
    :cond_4
    const/4 v9, 0x2

    invoke-virtual {v3, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getForecastInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    move-result-object v6

    .line 2046
    .local v6, "threeday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    if-eqz v6, :cond_5

    .line 2048
    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getHighTemp()F

    move-result v9

    .line 2047
    invoke-static {p2, p1, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTempScale(IIF)F

    move-result v9

    invoke-virtual {v6, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setHighTemp(F)V

    .line 2049
    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getLowTemp()F

    move-result v9

    invoke-static {p2, p1, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTempScale(IIF)F

    move-result v9

    invoke-virtual {v6, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setLowTemp(F)V

    .line 2052
    :cond_5
    const/4 v9, 0x3

    invoke-virtual {v3, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getForecastInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    move-result-object v2

    .line 2053
    .local v2, "fourday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    if-eqz v2, :cond_6

    .line 2054
    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getHighTemp()F

    move-result v9

    invoke-static {p2, p1, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTempScale(IIF)F

    move-result v9

    invoke-virtual {v2, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setHighTemp(F)V

    .line 2055
    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getLowTemp()F

    move-result v9

    invoke-static {p2, p1, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTempScale(IIF)F

    move-result v9

    invoke-virtual {v2, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setLowTemp(F)V

    .line 2058
    :cond_6
    const/4 v9, 0x4

    invoke-virtual {v3, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getForecastInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    move-result-object v1

    .line 2059
    .local v1, "fiveday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    if-eqz v1, :cond_7

    .line 2060
    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getHighTemp()F

    move-result v9

    invoke-static {p2, p1, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTempScale(IIF)F

    move-result v9

    invoke-virtual {v1, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setHighTemp(F)V

    .line 2061
    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getLowTemp()F

    move-result v9

    invoke-static {p2, p1, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTempScale(IIF)F

    move-result v9

    invoke-virtual {v1, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setLowTemp(F)V

    .line 2064
    :cond_7
    const/4 v9, 0x5

    invoke-virtual {v3, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getForecastInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    move-result-object v5

    .line 2065
    .local v5, "sixday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    if-eqz v5, :cond_8

    .line 2066
    invoke-virtual {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getHighTemp()F

    move-result v9

    invoke-static {p2, p1, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTempScale(IIF)F

    move-result v9

    invoke-virtual {v5, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setHighTemp(F)V

    .line 2067
    invoke-virtual {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getLowTemp()F

    move-result v9

    invoke-static {p2, p1, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTempScale(IIF)F

    move-result v9

    invoke-virtual {v5, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setLowTemp(F)V

    .line 2070
    :cond_8
    const-string v9, "cityId:current"

    invoke-static {p0, v9, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateAllTempScaleCity(Landroid/content/Context;Ljava/lang/String;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;)I

    goto/16 :goto_0
.end method

.method static synthetic access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;)Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mcl:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->stopHttpThread()V

    return-void
.end method

.method static synthetic access$1000(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->getCurrentLocation()V

    return-void
.end method

.method static synthetic access$1100(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->durationDefalut:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1202(Ljava/text/SimpleDateFormat;)Ljava/text/SimpleDateFormat;
    .locals 0
    .param p0, "x0"    # Ljava/text/SimpleDateFormat;

    .prologue
    .line 68
    sput-object p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->sdf:Ljava/text/SimpleDateFormat;

    return-object p0
.end method

.method static synthetic access$1302(I)I
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 68
    sput p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->len:I

    return p0
.end method

.method static synthetic access$200(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->hideLoadingDialog()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->hideAllDialog()V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->getLocationInfo()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;
    .param p1, "x1"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;
    .param p2, "x2"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    .param p3, "x3"    # Ljava/lang/String;

    .prologue
    .line 68
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->saveData(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mCtx:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;)Landroid/preference/CheckBoxPreference;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->set_notifications_preferences:Landroid/preference/CheckBoxPreference;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->init()V

    return-void
.end method

.method static synthetic access$900(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->setValues()V

    return-void
.end method

.method private checkResultCode(I)I
    .locals 2
    .param p1, "resultCode"    # I

    .prologue
    .line 1351
    const/4 v0, 0x0

    .line 1353
    .local v0, "result":I
    const/4 v1, -0x1

    if-ne v1, p1, :cond_1

    .line 1354
    const/16 v0, 0x3e7

    .line 1359
    :cond_0
    :goto_0
    return v0

    .line 1355
    :cond_1
    if-nez p1, :cond_0

    .line 1356
    const/16 v0, 0x3e7

    goto :goto_0
.end method

.method private getCurrentLocation()V
    .locals 1

    .prologue
    .line 1067
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkNetworkConnected(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1068
    const v0, 0x7f0d0020

    invoke-static {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->toast(Landroid/content/Context;I)V

    .line 1072
    :goto_0
    return-void

    .line 1071
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->getLocationInfo()V

    goto :goto_0
.end method

.method private getLocationInfo()V
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1076
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->hideAllDialog()V

    .line 1078
    const-string v0, "location"

    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/location/LocationManager;

    .line 1080
    .local v7, "locationManager":Landroid/location/LocationManager;
    const-string v0, "gps"

    invoke-virtual {v7, v0}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "network"

    .line 1081
    invoke-virtual {v7, v0}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1082
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->showLocatingDialog()V

    .line 1085
    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mcl:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->getLocDataHandler:Landroid/os/Handler;

    move v4, v2

    move v5, v3

    move v6, v2

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->performGetCurrentLocation(Landroid/os/Handler;IZZZZ)V

    .line 1086
    return-void
.end method

.method private hideAllDialog()V
    .locals 1

    .prologue
    .line 1128
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->hideLoadingDialog()V

    .line 1130
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mNetworkErrorDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    .line 1132
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mNetworkErrorDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 1138
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mPopupDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_1

    .line 1140
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mPopupDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 1145
    :cond_1
    :goto_1
    return-void

    .line 1141
    :catch_0
    move-exception v0

    goto :goto_1

    .line 1133
    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method private hideLoadingDialog()V
    .locals 3

    .prologue
    .line 1115
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mLoadingDialog:Landroid/app/Dialog;

    if-eqz v1, :cond_0

    .line 1117
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mLoadingDialog:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1123
    :goto_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mLoadingDialog:Landroid/app/Dialog;

    .line 1125
    :cond_0
    return-void

    .line 1118
    :catch_0
    move-exception v0

    .line 1120
    .local v0, "ex":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MenuAdd.hideLoadingDialog() "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->debug(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private init()V
    .locals 0

    .prologue
    .line 526
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->setView()V

    .line 527
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->setEntries()V

    .line 528
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->setValues()V

    .line 529
    return-void
.end method

.method private saveData(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;Ljava/lang/String;)V
    .locals 11
    .param p1, "cityInfo"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;
    .param p2, "detailInfo"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    .param p3, "xmlinfo"    # Ljava/lang/String;

    .prologue
    const/4 v9, 0x0

    const/4 v10, 0x1

    .line 1255
    if-eqz p1, :cond_3

    if-eqz p2, :cond_3

    .line 1256
    const/4 v5, 0x1

    .line 1257
    .local v5, "result":I
    const/4 v6, 0x0

    .line 1259
    .local v6, "resultCode":I
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mCtx:Landroid/content/Context;

    invoke-static {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getRealLocation(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 1263
    .local v3, "oldRealLocation":Ljava/lang/String;
    const-string v0, "cityId:current"

    .line 1264
    .local v0, "curLocation":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->getLocation()Ljava/lang/String;

    move-result-object v4

    .line 1265
    .local v4, "realLocation":Ljava/lang/String;
    invoke-virtual {p1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->setLocation(Ljava/lang/String;)V

    .line 1266
    invoke-virtual {p1, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->setRealLocation(Ljava/lang/String;)V

    .line 1268
    invoke-static {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->isRegisteredToCityList(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 1269
    sget-boolean v7, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isCurCityAtTopInCityList:Z

    if-nez v7, :cond_0

    sget-boolean v7, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isCurCityAtTopInCityList:Z

    if-nez v7, :cond_4

    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mCtx:Landroid/content/Context;

    .line 1271
    invoke-static {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getCheckCurrentCityLocation(Landroid/content/Context;)I

    move-result v7

    if-ne v7, v10, :cond_4

    .line 1272
    :cond_0
    invoke-static {p0, v0, p1, p2, p3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateCity(Landroid/content/Context;Ljava/lang/String;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;Ljava/lang/String;)I

    move-result v5

    .line 1296
    :cond_1
    :goto_0
    if-eqz v3, :cond_8

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v7

    if-lez v7, :cond_8

    .line 1297
    invoke-virtual {p2, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getPhotosInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;->getCityId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 1298
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mCtx:Landroid/content/Context;

    invoke-static {v7, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getExistPhotos(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1299
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mCtx:Landroid/content/Context;

    invoke-static {v7, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->deletePhotosInfo(Landroid/content/Context;Ljava/lang/String;)I

    .line 1317
    :cond_2
    :goto_1
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mCtx:Landroid/content/Context;

    const-string v8, "cityId:current"

    invoke-static {v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->isRegisteredToDetailHourInfo(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 1318
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mCtx:Landroid/content/Context;

    const-string v8, "cityId:current"

    invoke-static {v7, v8, p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateDetailHourInfo(Landroid/content/Context;Ljava/lang/String;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;)I

    .line 1323
    :goto_2
    invoke-direct {p0, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->checkResultCode(I)I

    move-result v6

    .line 1325
    const/16 v7, 0x3e7

    if-ne v7, v6, :cond_a

    .line 1326
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->getDataHandler:Landroid/os/Handler;

    new-instance v8, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$5;

    invoke-direct {v8, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$5;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;)V

    invoke-virtual {v7, v8}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1347
    .end local v0    # "curLocation":Ljava/lang/String;
    .end local v3    # "oldRealLocation":Ljava/lang/String;
    .end local v4    # "realLocation":Ljava/lang/String;
    .end local v5    # "result":I
    .end local v6    # "resultCode":I
    :cond_3
    :goto_3
    return-void

    .line 1275
    .restart local v0    # "curLocation":Ljava/lang/String;
    .restart local v3    # "oldRealLocation":Ljava/lang/String;
    .restart local v4    # "realLocation":Ljava/lang/String;
    .restart local v5    # "result":I
    .restart local v6    # "resultCode":I
    :cond_4
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1276
    .local v1, "deleteList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1277
    invoke-static {p0, v1, v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->deleteCitys(Landroid/content/Context;Ljava/util/ArrayList;Z)I

    move-result v5

    .line 1279
    if-ne v5, v10, :cond_1

    .line 1280
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mCtx:Landroid/content/Context;

    invoke-static {v7, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->deletePhotosInfo(Landroid/content/Context;Ljava/util/ArrayList;)I

    .line 1282
    invoke-static {p0, p1, p2, p3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->insertCityCurrentLocation(Landroid/content/Context;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;Ljava/lang/String;)I

    move-result v5

    .line 1285
    if-ne v5, v10, :cond_1

    .line 1286
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mCtx:Landroid/content/Context;

    invoke-static {v7, v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateCheckCurrentCityLocation(Landroid/content/Context;I)I

    goto :goto_0

    .line 1292
    .end local v1    # "deleteList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_5
    invoke-static {p0, p1, p2, p3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->insertCityCurrentLocation(Landroid/content/Context;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;Ljava/lang/String;)I

    move-result v5

    goto :goto_0

    .line 1302
    :cond_6
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mCtx:Landroid/content/Context;

    invoke-static {v7, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->isRegisteredToCityList(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_7

    .line 1303
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mCtx:Landroid/content/Context;

    invoke-static {v7, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getExistPhotos(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 1304
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mCtx:Landroid/content/Context;

    invoke-static {v7, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->deletePhotosInfo(Landroid/content/Context;Ljava/lang/String;)I

    .line 1307
    :cond_7
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mCtx:Landroid/content/Context;

    invoke-virtual {p2, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getPhotosInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;->getCityId()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getExistPhotos(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1308
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mCtx:Landroid/content/Context;

    invoke-virtual {p2, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getPhotosInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;->getCityId()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->deletePhotosInfo(Landroid/content/Context;Ljava/lang/String;)I

    goto :goto_1

    .line 1312
    :cond_8
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mCtx:Landroid/content/Context;

    invoke-virtual {p2, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getPhotosInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;->getCityId()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getExistPhotos(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1313
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mCtx:Landroid/content/Context;

    invoke-virtual {p2, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getPhotosInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;->getCityId()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->deletePhotosInfo(Landroid/content/Context;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 1320
    :cond_9
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mCtx:Landroid/content/Context;

    const-string v8, "cityId:current"

    invoke-static {v7, v8, p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->insertDetailHourInfo(Landroid/content/Context;Ljava/lang/String;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;)I

    goto/16 :goto_2

    .line 1335
    :cond_a
    const-string v7, ""

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "LC : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " -> "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1336
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateLastSelectedLocation(Landroid/content/Context;Ljava/lang/String;)I

    .line 1337
    if-ne v5, v10, :cond_3

    .line 1339
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mCtx:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f090013

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v7

    if-eqz v7, :cond_b

    .line 1340
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getSelectedDefaultLocation(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 1344
    .local v2, "mSelectedLocation":Ljava/lang/String;
    :goto_4
    invoke-static {p0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateSelectDefaultdLocation(Landroid/content/Context;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 1342
    .end local v2    # "mSelectedLocation":Ljava/lang/String;
    :cond_b
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getLastSelectedLocation(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "mSelectedLocation":Ljava/lang/String;
    goto :goto_4
.end method

.method public static saveSwitchState(Landroid/content/Context;Landroid/preference/CheckBoxPreference;Z)V
    .locals 14
    .param p0, "mContext"    # Landroid/content/Context;
    .param p1, "notiPre"    # Landroid/preference/CheckBoxPreference;
    .param p2, "switched"    # Z

    .prologue
    .line 1913
    if-eqz p2, :cond_1

    .line 1914
    const/4 v10, 0x1

    .line 1919
    .local v10, "value":I
    :goto_0
    invoke-static {p0, v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateNotificationSettings(Landroid/content/Context;I)I

    .line 1921
    const/4 v11, 0x1

    if-ne v10, v11, :cond_4

    .line 1925
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getNotificationTimeSettings(Landroid/content/Context;)Ljava/lang/Long;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    .line 1926
    .local v8, "setTime":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 1928
    .local v6, "now_time":J
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 1929
    .local v0, "db_Cal":Ljava/util/Calendar;
    invoke-virtual {v0, v8, v9}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1930
    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v11

    invoke-virtual {v11}, Ljava/util/Date;->getHours()I

    move-result v1

    .line 1931
    .local v1, "db_mHour":I
    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v11

    invoke-virtual {v11}, Ljava/util/Date;->getMinutes()I

    move-result v2

    .line 1933
    .local v2, "db_mMinute":I
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    .line 1934
    .local v3, "now_Cal":Ljava/util/Calendar;
    invoke-virtual {v3, v6, v7}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1935
    invoke-virtual {v3}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v11

    invoke-virtual {v11}, Ljava/util/Date;->getHours()I

    move-result v4

    .line 1936
    .local v4, "now_mHour":I
    invoke-virtual {v3}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v11

    invoke-virtual {v11}, Ljava/util/Date;->getMinutes()I

    move-result v5

    .line 1938
    .local v5, "now_mMinute":I
    new-instance v11, Ljava/text/SimpleDateFormat;

    const-string v12, "yy/MM/dd HH:mm:ss"

    invoke-direct {v11, v12}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    sput-object v11, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->sdf:Ljava/text/SimpleDateFormat;

    .line 1939
    const-string v11, "yy/MM/dd HH:mm:ss"

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    sput v11, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->len:I

    .line 1948
    if-lt v1, v4, :cond_0

    if-ne v1, v4, :cond_3

    if-gt v2, v5, :cond_3

    .line 1949
    :cond_0
    const-wide/16 v11, 0x0

    cmp-long v11, v8, v11

    if-nez v11, :cond_2

    .line 1950
    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x1

    invoke-static {v13}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v13

    invoke-static {p0, v11, v12, v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->setNotiTime(Landroid/content/Context;IILjava/lang/Boolean;)V

    .line 1962
    .end local v0    # "db_Cal":Ljava/util/Calendar;
    .end local v1    # "db_mHour":I
    .end local v2    # "db_mMinute":I
    .end local v3    # "now_Cal":Ljava/util/Calendar;
    .end local v4    # "now_mHour":I
    .end local v5    # "now_mMinute":I
    .end local v6    # "now_time":J
    .end local v8    # "setTime":J
    :goto_1
    return-void

    .line 1916
    .end local v10    # "value":I
    :cond_1
    const/4 v10, 0x0

    .restart local v10    # "value":I
    goto :goto_0

    .line 1952
    .restart local v0    # "db_Cal":Ljava/util/Calendar;
    .restart local v1    # "db_mHour":I
    .restart local v2    # "db_mMinute":I
    .restart local v3    # "now_Cal":Ljava/util/Calendar;
    .restart local v4    # "now_mHour":I
    .restart local v5    # "now_mMinute":I
    .restart local v6    # "now_time":J
    .restart local v8    # "setTime":J
    :cond_2
    const/4 v11, 0x1

    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    invoke-static {p0, v1, v2, v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->setNotiTime(Landroid/content/Context;IILjava/lang/Boolean;)V

    goto :goto_1

    .line 1955
    :cond_3
    const/4 v11, 0x0

    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    invoke-static {p0, v1, v2, v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->setNotiTime(Landroid/content/Context;IILjava/lang/Boolean;)V

    goto :goto_1

    .line 1958
    .end local v0    # "db_Cal":Ljava/util/Calendar;
    .end local v1    # "db_mHour":I
    .end local v2    # "db_mMinute":I
    .end local v3    # "now_Cal":Ljava/util/Calendar;
    .end local v4    # "now_mHour":I
    .end local v5    # "now_mMinute":I
    .end local v6    # "now_time":J
    .end local v8    # "setTime":J
    :cond_4
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->unregisterNotiTime(Landroid/content/Context;)V

    goto :goto_1
.end method

.method private setCurrentLocationSetting()V
    .locals 4

    .prologue
    .line 573
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v2

    .line 574
    .local v2, "mPreferenceScreen":Landroid/preference/PreferenceScreen;
    if-eqz v2, :cond_0

    .line 575
    const-string v3, "setcurrentcity"

    invoke-virtual {p0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 576
    .local v0, "mPreference":Landroid/preference/Preference;
    if-nez v0, :cond_0

    .line 577
    new-instance v1, Landroid/preference/Preference;

    invoke-direct {v1, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 578
    .local v1, "mPreference2":Landroid/preference/Preference;
    const-string v3, "setcurrentcity"

    invoke-virtual {v1, v3}, Landroid/preference/Preference;->setKey(Ljava/lang/String;)V

    .line 579
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/preference/Preference;->setPersistent(Z)V

    .line 580
    const v3, 0x7f0d0004

    invoke-virtual {v1, v3}, Landroid/preference/Preference;->setTitle(I)V

    .line 581
    invoke-virtual {v1, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 583
    invoke-virtual {v2, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 587
    .end local v0    # "mPreference":Landroid/preference/Preference;
    .end local v1    # "mPreference2":Landroid/preference/Preference;
    :cond_0
    return-void
.end method

.method private setDailyNotiTimePreference(Z)V
    .locals 6
    .param p1, "value"    # Z

    .prologue
    const/4 v4, 0x1

    .line 532
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    .line 534
    .local v1, "mPreferenceScreen":Landroid/preference/PreferenceScreen;
    if-eqz v1, :cond_1

    .line 535
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getWIdgetMode(Landroid/content/Context;)I

    move-result v2

    const/16 v3, 0xfa1

    if-eq v2, v3, :cond_2

    .line 536
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getCheckCurrentCityLocation(Landroid/content/Context;)I

    move-result v2

    if-ne v2, v4, :cond_2

    .line 537
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getNotificationSettings(Landroid/content/Context;)I

    move-result v2

    if-ne v2, v4, :cond_2

    .line 538
    const-string v2, "setnotificationtime"

    invoke-virtual {p0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    if-nez v2, :cond_1

    .line 539
    const/4 v0, 0x0

    .line 540
    .local v0, "mPreference":Landroid/preference/Preference;
    iget-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->isFirstGps:Z

    if-nez v2, :cond_0

    .line 541
    const-string v2, "setcurrentcity"

    invoke-virtual {p0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 542
    if-eqz v0, :cond_0

    .line 543
    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 547
    :cond_0
    new-instance v0, Landroid/preference/Preference;

    .end local v0    # "mPreference":Landroid/preference/Preference;
    invoke-direct {v0, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 548
    .restart local v0    # "mPreference":Landroid/preference/Preference;
    const-string v2, "setnotificationtime"

    invoke-virtual {v0, v2}, Landroid/preference/Preference;->setKey(Ljava/lang/String;)V

    .line 549
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/preference/Preference;->setPersistent(Z)V

    .line 550
    const v2, 0x7f0d00ab

    invoke-virtual {v0, v2}, Landroid/preference/Preference;->setTitle(I)V

    .line 552
    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 554
    if-ne p1, v4, :cond_1

    .line 555
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mMoveHandler:Landroid/os/Handler;

    new-instance v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$2;

    invoke-direct {v3, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$2;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;)V

    const-wide/16 v4, 0x64

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 571
    .end local v0    # "mPreference":Landroid/preference/Preference;
    :cond_1
    :goto_0
    return-void

    .line 565
    :cond_2
    const-string v2, "setnotificationtime"

    invoke-virtual {p0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 566
    .restart local v0    # "mPreference":Landroid/preference/Preference;
    if-eqz v0, :cond_1

    .line 567
    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_0
.end method

.method private setEntries()V
    .locals 7

    .prologue
    const v3, 0x7f0d0015

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 745
    const-string v1, ""

    const-string v2, "setEntries"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 746
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->refreshTimes:[I

    if-eqz v1, :cond_0

    .line 747
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->refreshTimes:[I

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 748
    packed-switch v0, :pswitch_data_0

    .line 768
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->refreshItems:[Ljava/lang/String;

    invoke-virtual {p0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 747
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 750
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->refreshItems:[Ljava/lang/String;

    invoke-virtual {p0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_1

    .line 753
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->refreshItems:[Ljava/lang/String;

    const v2, 0x7f0d0016

    invoke-virtual {p0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_1

    .line 756
    :pswitch_2
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->refreshItems:[Ljava/lang/String;

    const v2, 0x7f0d0017

    invoke-virtual {p0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_1

    .line 759
    :pswitch_3
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->refreshItems:[Ljava/lang/String;

    const v2, 0x7f0d0018

    invoke-virtual {p0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_1

    .line 762
    :pswitch_4
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->refreshItems:[Ljava/lang/String;

    const v2, 0x7f0d0019

    invoke-virtual {p0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_1

    .line 765
    :pswitch_5
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->refreshItems:[Ljava/lang/String;

    const v2, 0x7f0d001a

    invoke-virtual {p0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_1

    .line 781
    .end local v0    # "i":I
    :cond_0
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->autorefresh_preferences:Landroid/preference/ListPreference;

    if-eqz v1, :cond_1

    .line 782
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->autorefresh_preferences:Landroid/preference/ListPreference;

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->refreshItems:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/preference/ListPreference;->setEntries([Ljava/lang/CharSequence;)V

    .line 783
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->autorefresh_preferences:Landroid/preference/ListPreference;

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->refreshItemsValue:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/preference/ListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    .line 796
    :cond_1
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->unit_preferences:Landroid/preference/ListPreference;

    if-eqz v1, :cond_2

    .line 797
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->unit_preferences:Landroid/preference/ListPreference;

    new-array v2, v6, [Ljava/lang/String;

    const v3, 0x7f0d0081

    .line 798
    invoke-virtual {p0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    const v3, 0x7f0d0080

    invoke-virtual {p0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    .line 797
    invoke-virtual {v1, v2}, Landroid/preference/ListPreference;->setEntries([Ljava/lang/CharSequence;)V

    .line 800
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->unit_preferences:Landroid/preference/ListPreference;

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "0"

    aput-object v3, v2, v4

    const-string v3, "1"

    aput-object v3, v2, v5

    invoke-virtual {v1, v2}, Landroid/preference/ListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    .line 813
    :cond_2
    return-void

    .line 748
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private setValues()V
    .locals 17

    .prologue
    .line 816
    const-string v13, ""

    const-string v14, "setValues"

    invoke-static {v13, v14}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 817
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mCtx:Landroid/content/Context;

    invoke-static {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getSettingsForMain(Landroid/content/Context;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    move-result-object v7

    .line 818
    .local v7, "settingsInfo":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mCtx:Landroid/content/Context;

    invoke-static {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getViEffectSettings(Landroid/content/Context;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    move-result-object v12

    .line 819
    .local v12, "viEffectsettingsInfo":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;
    if-nez v7, :cond_1

    .line 820
    const-string v13, ""

    const-string v14, "sV N"

    invoke-static {v13, v14}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1063
    :cond_0
    :goto_0
    return-void

    .line 823
    :cond_1
    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->getTempScaleSetting()I

    move-result v10

    .line 824
    .local v10, "unit":I
    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->getCurrentLoccation()I

    move-result v13

    move-object/from16 v0, p0

    iput v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->location:I

    .line 825
    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->getAutoScroll()I

    move-result v3

    .line 826
    .local v3, "autoscroll":I
    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->getAutoRefreshTime()I

    move-result v1

    .line 828
    .local v1, "autorefresh":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->unit_preferences:Landroid/preference/ListPreference;

    if-eqz v13, :cond_2

    .line 829
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->unit_preferences:Landroid/preference/ListPreference;

    const/4 v13, 0x1

    if-ne v10, v13, :cond_1a

    const v13, 0x7f0d0081

    .line 830
    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 829
    :goto_1
    invoke-virtual {v14, v13}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 831
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->unit_preferences:Landroid/preference/ListPreference;

    const/4 v13, 0x1

    if-ne v10, v13, :cond_1b

    const/4 v13, 0x0

    :goto_2
    invoke-virtual {v14, v13}, Landroid/preference/ListPreference;->setValueIndex(I)V

    .line 835
    :cond_2
    packed-switch v1, :pswitch_data_0

    .line 855
    const-string v13, ""

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "RT NULL err af = "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " !!!!!!"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 856
    const v13, 0x7f0d0015

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 874
    .local v2, "autorefresh_time":Ljava/lang/String;
    :goto_3
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->autorefresh_preferences:Landroid/preference/ListPreference;

    if-eqz v13, :cond_3

    .line 875
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->autorefresh_preferences:Landroid/preference/ListPreference;

    invoke-virtual {v13, v2}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 876
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->autorefresh_preferences:Landroid/preference/ListPreference;

    invoke-virtual {v13, v1}, Landroid/preference/ListPreference;->setValueIndex(I)V

    .line 878
    :cond_3
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->refreshentering_preferences:Landroid/preference/CheckBoxPreference;

    if-eqz v13, :cond_4

    .line 879
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->refreshentering_preferences:Landroid/preference/CheckBoxPreference;

    .line 880
    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->getRefreshEntering()I

    move-result v13

    const/4 v15, 0x1

    if-ne v13, v15, :cond_1c

    const/4 v13, 0x1

    :goto_4
    invoke-virtual {v14, v13}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 882
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->refreshentering_preferences:Landroid/preference/CheckBoxPreference;

    const v14, 0x7f0d007f

    invoke-virtual {v13, v14}, Landroid/preference/CheckBoxPreference;->setSummary(I)V

    .line 885
    :cond_4
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->refreshroaming_preferences:Landroid/preference/CheckBoxPreference;

    if-eqz v13, :cond_5

    .line 886
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->refreshroaming_preferences:Landroid/preference/CheckBoxPreference;

    .line 887
    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->getRefreshRoaming()I

    move-result v13

    const/4 v15, 0x1

    if-ne v13, v15, :cond_1d

    const/4 v13, 0x1

    :goto_5
    invoke-virtual {v14, v13}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 893
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->refreshroaming_preferences:Landroid/preference/CheckBoxPreference;

    const/4 v14, 0x1

    invoke-virtual {v13, v14}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 898
    :cond_5
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->widgetBGOnOff_preferences:Landroid/preference/CheckBoxPreference;

    if-eqz v13, :cond_6

    .line 899
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->widgetBGOnOff_preferences:Landroid/preference/CheckBoxPreference;

    .line 900
    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->getWidgetBGOnOff()I

    move-result v13

    const/4 v15, 0x1

    if-ne v13, v15, :cond_1e

    const/4 v13, 0x1

    :goto_6
    invoke-virtual {v14, v13}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 904
    :cond_6
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->widgetBGTransparencyControl_preferences:Landroid/preference/PreferenceScreen;

    if-eqz v13, :cond_7

    .line 905
    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->getWidgetBGTransparencyVal()I

    move-result v11

    .line 906
    .local v11, "value":I
    const-string v13, "%d%%"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-static {v13, v14}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 907
    .local v8, "str":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->widgetBGTransparencyControl_preferences:Landroid/preference/PreferenceScreen;

    .line 908
    invoke-virtual {v13, v8}, Landroid/preference/PreferenceScreen;->setSummary(Ljava/lang/CharSequence;)V

    .line 911
    .end local v8    # "str":Ljava/lang/String;
    .end local v11    # "value":I
    :cond_7
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->useCurrentCity_preferences:Landroid/preference/Preference;

    if-eqz v13, :cond_8

    .line 912
    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->getWidgetBGTransparencyVal()I

    move-result v11

    .line 913
    .restart local v11    # "value":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mCtx:Landroid/content/Context;

    invoke-static {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->isAgreeUseLocation(Landroid/content/Context;)Z

    move-result v13

    if-eqz v13, :cond_1f

    .line 914
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->useCurrentCity_preferences:Landroid/preference/Preference;

    const v14, 0x7f0d00e5

    invoke-virtual {v13, v14}, Landroid/preference/Preference;->setSummary(I)V

    .line 920
    .end local v11    # "value":I
    :cond_8
    :goto_7
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->set_current_city_preferences:Landroid/preference/CheckBoxPreference;

    if-eqz v13, :cond_9

    .line 921
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->set_current_city_preferences:Landroid/preference/CheckBoxPreference;

    .line 922
    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->getCurrentLoccation()I

    move-result v13

    const/4 v15, 0x1

    if-ne v13, v15, :cond_20

    const/4 v13, 0x1

    :goto_8
    invoke-virtual {v14, v13}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 925
    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->getCurrentLoccation()I

    move-result v13

    const/4 v14, 0x1

    if-ne v13, v14, :cond_21

    .line 926
    invoke-static/range {p0 .. p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getCurrentCityinfo(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    .line 927
    .local v4, "curCity":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->set_current_city_preferences:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v13, v4}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 934
    .end local v4    # "curCity":Ljava/lang/String;
    :cond_9
    :goto_9
    invoke-static/range {p0 .. p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getWIdgetMode(Landroid/content/Context;)I

    move-result v13

    const/16 v14, 0xfa1

    if-eq v13, v14, :cond_d

    .line 944
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->set_notifications_preferences:Landroid/preference/CheckBoxPreference;

    if-eqz v13, :cond_a

    .line 945
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->set_notifications_preferences:Landroid/preference/CheckBoxPreference;

    .line 946
    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->getNotification()I

    move-result v13

    const/4 v15, 0x1

    if-ne v13, v15, :cond_22

    const/4 v13, 0x1

    :goto_a
    invoke-virtual {v14, v13}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 950
    :cond_a
    invoke-static/range {p0 .. p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getNotificationTimeSettings(Landroid/content/Context;)Ljava/lang/Long;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    .line 952
    .local v5, "setTime":J
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->set_notificationsettime_preferences:Landroid/preference/Preference;

    if-eqz v13, :cond_b

    .line 953
    const-wide/16 v13, 0x0

    cmp-long v13, v5, v13

    if-nez v13, :cond_23

    .line 954
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->set_notificationsettime_preferences:Landroid/preference/Preference;

    const v14, 0x7f0d00c2

    .line 955
    invoke-virtual {v13, v14}, Landroid/preference/Preference;->setSummary(I)V

    .line 961
    :cond_b
    :goto_b
    invoke-static/range {p0 .. p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getCheckCurrentCityLocation(Landroid/content/Context;)I

    move-result v13

    if-nez v13, :cond_24

    .line 962
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->set_notifications_preferences:Landroid/preference/CheckBoxPreference;

    if-eqz v13, :cond_c

    .line 963
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->set_notifications_preferences:Landroid/preference/CheckBoxPreference;

    const v14, 0x7f0d00aa

    .line 964
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 963
    invoke-virtual {v13, v14}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 965
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->set_notifications_preferences:Landroid/preference/CheckBoxPreference;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 967
    :cond_c
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->set_notificationsettime_preferences:Landroid/preference/Preference;

    if-eqz v13, :cond_d

    .line 968
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->set_notificationsettime_preferences:Landroid/preference/Preference;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 987
    .end local v5    # "setTime":J
    :cond_d
    :goto_c
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->devSetting:Z

    if-eqz v13, :cond_0

    .line 988
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->background_video_preferences:Landroid/preference/CheckBoxPreference;

    if-eqz v13, :cond_e

    .line 989
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->background_video_preferences:Landroid/preference/CheckBoxPreference;

    .line 990
    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->getBackgroundVideo()I

    move-result v13

    const/4 v15, 0x1

    if-ne v13, v15, :cond_27

    const/4 v13, 0x1

    :goto_d
    invoke-virtual {v14, v13}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 993
    :cond_e
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->background_video_repeat_preferences:Landroid/preference/CheckBoxPreference;

    if-eqz v13, :cond_f

    .line 994
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->background_video_repeat_preferences:Landroid/preference/CheckBoxPreference;

    .line 995
    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->getBackgroundVideoRepeat()I

    move-result v13

    const/4 v15, 0x1

    if-ne v13, v15, :cond_28

    const/4 v13, 0x1

    .line 994
    :goto_e
    invoke-virtual {v14, v13}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 997
    :cond_f
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->icon_animation_preferences:Landroid/preference/CheckBoxPreference;

    if-eqz v13, :cond_10

    .line 998
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->icon_animation_preferences:Landroid/preference/CheckBoxPreference;

    .line 999
    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->getIconAnimation()I

    move-result v13

    const/4 v15, 0x1

    if-ne v13, v15, :cond_29

    const/4 v13, 0x1

    :goto_f
    invoke-virtual {v14, v13}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 1002
    :cond_10
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->icon_animation_repeat_preferences:Landroid/preference/CheckBoxPreference;

    if-eqz v13, :cond_11

    .line 1003
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->icon_animation_repeat_preferences:Landroid/preference/CheckBoxPreference;

    .line 1004
    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->getIconAnimationRepeat()I

    move-result v13

    const/4 v15, 0x1

    if-ne v13, v15, :cond_2a

    const/4 v13, 0x1

    .line 1003
    :goto_10
    invoke-virtual {v14, v13}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 1010
    :cond_11
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->changecity_scroll_preferences:Landroid/preference/CheckBoxPreference;

    if-eqz v13, :cond_12

    .line 1016
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->changecity_scroll_preferences:Landroid/preference/CheckBoxPreference;

    .line 1017
    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->getCityChangeScrollAnimation()I

    move-result v13

    const/4 v15, 0x1

    if-ne v13, v15, :cond_2b

    const/4 v13, 0x1

    .line 1016
    :goto_11
    invoke-virtual {v14, v13}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 1019
    :cond_12
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->weather_effect_preferences:Landroid/preference/CheckBoxPreference;

    if-eqz v13, :cond_13

    .line 1020
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->weather_effect_preferences:Landroid/preference/CheckBoxPreference;

    .line 1021
    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->getWeatherEffect()I

    move-result v13

    const/4 v15, 0x1

    if-ne v13, v15, :cond_2c

    const/4 v13, 0x1

    :goto_12
    invoke-virtual {v14, v13}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 1024
    :cond_13
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->infor_animation_preferences:Landroid/preference/CheckBoxPreference;

    if-eqz v13, :cond_14

    .line 1025
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->infor_animation_preferences:Landroid/preference/CheckBoxPreference;

    .line 1026
    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->getInforAnimation()I

    move-result v13

    const/4 v15, 0x1

    if-ne v13, v15, :cond_2d

    const/4 v13, 0x1

    :goto_13
    invoke-virtual {v14, v13}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 1029
    :cond_14
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->bottom_icon_animation_preferences:Landroid/preference/CheckBoxPreference;

    if-eqz v13, :cond_15

    .line 1030
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->bottom_icon_animation_preferences:Landroid/preference/CheckBoxPreference;

    .line 1031
    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->getBottomIconAnimtion()I

    move-result v13

    const/4 v15, 0x1

    if-ne v13, v15, :cond_2e

    const/4 v13, 0x1

    .line 1030
    :goto_14
    invoke-virtual {v14, v13}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 1033
    :cond_15
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->widget_icon_animation_preferences:Landroid/preference/CheckBoxPreference;

    if-eqz v13, :cond_16

    .line 1034
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->widget_icon_animation_preferences:Landroid/preference/CheckBoxPreference;

    .line 1035
    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->getWidgetIconAnimation()I

    move-result v13

    const/4 v15, 0x1

    if-ne v13, v15, :cond_2f

    const/4 v13, 0x1

    :goto_15
    invoke-virtual {v14, v13}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 1038
    :cond_16
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->widget_city_change_animation_preferences:Landroid/preference/CheckBoxPreference;

    if-eqz v13, :cond_17

    .line 1039
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->widget_city_change_animation_preferences:Landroid/preference/CheckBoxPreference;

    .line 1040
    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->getWidgetCityChangeAnimation()I

    move-result v13

    const/4 v15, 0x1

    if-ne v13, v15, :cond_30

    const/4 v13, 0x1

    :goto_16
    invoke-virtual {v14, v13}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 1044
    :cond_17
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->city_select_animation_preferences:Landroid/preference/CheckBoxPreference;

    if-eqz v13, :cond_18

    .line 1045
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->city_select_animation_preferences:Landroid/preference/CheckBoxPreference;

    .line 1046
    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->getCitySelectAnimation()I

    move-result v13

    const/4 v15, 0x1

    if-ne v13, v15, :cond_31

    const/4 v13, 0x1

    :goto_17
    invoke-virtual {v14, v13}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 1050
    :cond_18
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->widgetBGOnOff_preferences:Landroid/preference/CheckBoxPreference;

    if-eqz v13, :cond_19

    .line 1051
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->widgetBGOnOff_preferences:Landroid/preference/CheckBoxPreference;

    .line 1052
    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->getWidgetBGOnOff()I

    move-result v13

    const/4 v15, 0x1

    if-ne v13, v15, :cond_32

    const/4 v13, 0x1

    :goto_18
    invoke-virtual {v14, v13}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 1056
    :cond_19
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->duration_time_preferences:Landroid/preference/Preference;

    if-eqz v13, :cond_0

    .line 1057
    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->getIconDurationVal()I

    move-result v13

    const/4 v14, 0x5

    if-gt v13, v14, :cond_33

    const/4 v9, 0x5

    .line 1058
    .local v9, "temp":I
    :goto_19
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->duration_time_preferences:Landroid/preference/Preference;

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 830
    .end local v2    # "autorefresh_time":Ljava/lang/String;
    .end local v9    # "temp":I
    :cond_1a
    const v13, 0x7f0d0080

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->getString(I)Ljava/lang/String;

    move-result-object v13

    goto/16 :goto_1

    .line 831
    :cond_1b
    const/4 v13, 0x1

    goto/16 :goto_2

    .line 837
    :pswitch_0
    const v13, 0x7f0d0015

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 838
    .restart local v2    # "autorefresh_time":Ljava/lang/String;
    goto/16 :goto_3

    .line 840
    .end local v2    # "autorefresh_time":Ljava/lang/String;
    :pswitch_1
    const v13, 0x7f0d0016

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 841
    .restart local v2    # "autorefresh_time":Ljava/lang/String;
    goto/16 :goto_3

    .line 843
    .end local v2    # "autorefresh_time":Ljava/lang/String;
    :pswitch_2
    const v13, 0x7f0d0017

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 844
    .restart local v2    # "autorefresh_time":Ljava/lang/String;
    goto/16 :goto_3

    .line 846
    .end local v2    # "autorefresh_time":Ljava/lang/String;
    :pswitch_3
    const v13, 0x7f0d0018

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 847
    .restart local v2    # "autorefresh_time":Ljava/lang/String;
    goto/16 :goto_3

    .line 849
    .end local v2    # "autorefresh_time":Ljava/lang/String;
    :pswitch_4
    const v13, 0x7f0d0019

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 850
    .restart local v2    # "autorefresh_time":Ljava/lang/String;
    goto/16 :goto_3

    .line 852
    .end local v2    # "autorefresh_time":Ljava/lang/String;
    :pswitch_5
    const v13, 0x7f0d001a

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 853
    .restart local v2    # "autorefresh_time":Ljava/lang/String;
    goto/16 :goto_3

    .line 880
    :cond_1c
    const/4 v13, 0x0

    goto/16 :goto_4

    .line 887
    :cond_1d
    const/4 v13, 0x0

    goto/16 :goto_5

    .line 900
    :cond_1e
    const/4 v13, 0x0

    goto/16 :goto_6

    .line 916
    .restart local v11    # "value":I
    :cond_1f
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->useCurrentCity_preferences:Landroid/preference/Preference;

    const v14, 0x7f0d00e6

    invoke-virtual {v13, v14}, Landroid/preference/Preference;->setSummary(I)V

    goto/16 :goto_7

    .line 922
    .end local v11    # "value":I
    :cond_20
    const/4 v13, 0x0

    goto/16 :goto_8

    .line 929
    :cond_21
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->set_current_city_preferences:Landroid/preference/CheckBoxPreference;

    const v14, 0x7f0d0076

    invoke-virtual {v13, v14}, Landroid/preference/CheckBoxPreference;->setSummary(I)V

    goto/16 :goto_9

    .line 946
    :cond_22
    const/4 v13, 0x0

    goto/16 :goto_a

    .line 957
    .restart local v5    # "setTime":J
    :cond_23
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->set_notificationsettime_preferences:Landroid/preference/Preference;

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->checkDateFormat(J)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto/16 :goto_b

    .line 971
    :cond_24
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->set_notifications_preferences:Landroid/preference/CheckBoxPreference;

    if-eqz v13, :cond_25

    .line 972
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->set_notifications_preferences:Landroid/preference/CheckBoxPreference;

    const/4 v14, 0x1

    invoke-virtual {v13, v14}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 973
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->set_notifications_preferences:Landroid/preference/CheckBoxPreference;

    const v14, 0x7f0d00aa

    .line 974
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 973
    invoke-virtual {v13, v14}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 976
    :cond_25
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->set_notificationsettime_preferences:Landroid/preference/Preference;

    if-eqz v13, :cond_d

    .line 977
    invoke-static/range {p0 .. p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getNotificationSettings(Landroid/content/Context;)I

    move-result v13

    const/4 v14, 0x1

    if-ne v13, v14, :cond_26

    .line 978
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->set_notificationsettime_preferences:Landroid/preference/Preference;

    const/4 v14, 0x1

    invoke-virtual {v13, v14}, Landroid/preference/Preference;->setEnabled(Z)V

    goto/16 :goto_c

    .line 980
    :cond_26
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->set_notificationsettime_preferences:Landroid/preference/Preference;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/preference/Preference;->setEnabled(Z)V

    goto/16 :goto_c

    .line 990
    .end local v5    # "setTime":J
    :cond_27
    const/4 v13, 0x0

    goto/16 :goto_d

    .line 995
    :cond_28
    const/4 v13, 0x0

    goto/16 :goto_e

    .line 999
    :cond_29
    const/4 v13, 0x0

    goto/16 :goto_f

    .line 1004
    :cond_2a
    const/4 v13, 0x0

    goto/16 :goto_10

    .line 1017
    :cond_2b
    const/4 v13, 0x0

    goto/16 :goto_11

    .line 1021
    :cond_2c
    const/4 v13, 0x0

    goto/16 :goto_12

    .line 1026
    :cond_2d
    const/4 v13, 0x0

    goto/16 :goto_13

    .line 1031
    :cond_2e
    const/4 v13, 0x0

    goto/16 :goto_14

    .line 1035
    :cond_2f
    const/4 v13, 0x0

    goto/16 :goto_15

    .line 1040
    :cond_30
    const/4 v13, 0x0

    goto/16 :goto_16

    .line 1046
    :cond_31
    const/4 v13, 0x0

    goto/16 :goto_17

    .line 1052
    :cond_32
    const/4 v13, 0x0

    goto/16 :goto_18

    .line 1057
    :cond_33
    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->getIconDurationVal()I

    move-result v9

    goto/16 :goto_19

    .line 835
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private setView()V
    .locals 3

    .prologue
    .line 591
    :try_start_0
    const-string v1, "unit"

    invoke-virtual {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/ListPreference;

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->unit_preferences:Landroid/preference/ListPreference;

    .line 592
    const-string v1, "autorefresh"

    invoke-virtual {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/ListPreference;

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->autorefresh_preferences:Landroid/preference/ListPreference;

    .line 593
    const-string v1, "refreshentering"

    invoke-virtual {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->refreshentering_preferences:Landroid/preference/CheckBoxPreference;

    .line 594
    const-string v1, "refreshroaming"

    invoke-virtual {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->refreshroaming_preferences:Landroid/preference/CheckBoxPreference;

    .line 596
    const-string v1, "widgetBGOnOff"

    invoke-virtual {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->widgetBGOnOff_preferences:Landroid/preference/CheckBoxPreference;

    .line 598
    const-string v1, "widgetBGTransparencyControl"

    invoke-virtual {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/PreferenceScreen;

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->widgetBGTransparencyControl_preferences:Landroid/preference/PreferenceScreen;

    .line 600
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->unit_preferences:Landroid/preference/ListPreference;

    if-eqz v1, :cond_0

    .line 601
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->unit_preferences:Landroid/preference/ListPreference;

    invoke-virtual {v1, p0}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 604
    :cond_0
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->autorefresh_preferences:Landroid/preference/ListPreference;

    if-eqz v1, :cond_1

    .line 605
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->autorefresh_preferences:Landroid/preference/ListPreference;

    invoke-virtual {v1, p0}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 608
    :cond_1
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->refreshentering_preferences:Landroid/preference/CheckBoxPreference;

    if-eqz v1, :cond_2

    .line 609
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->refreshentering_preferences:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 612
    :cond_2
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->refreshroaming_preferences:Landroid/preference/CheckBoxPreference;

    if-eqz v1, :cond_3

    .line 613
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->refreshroaming_preferences:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 616
    :cond_3
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->widgetBGOnOff_preferences:Landroid/preference/CheckBoxPreference;

    if-eqz v1, :cond_4

    .line 617
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->widgetBGOnOff_preferences:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 620
    :cond_4
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->widgetBGTransparencyControl_preferences:Landroid/preference/PreferenceScreen;

    if-eqz v1, :cond_5

    .line 621
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->widgetBGTransparencyControl_preferences:Landroid/preference/PreferenceScreen;

    invoke-virtual {v1, p0}, Landroid/preference/PreferenceScreen;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 633
    :cond_5
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getWIdgetMode(Landroid/content/Context;)I

    move-result v1

    const/16 v2, 0xfa1

    if-eq v1, v2, :cond_7

    .line 642
    const-string v1, "setnotifications"

    invoke-virtual {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->set_notifications_preferences:Landroid/preference/CheckBoxPreference;

    .line 644
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->set_notifications_preferences:Landroid/preference/CheckBoxPreference;

    if-eqz v1, :cond_6

    .line 645
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->set_notifications_preferences:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 648
    :cond_6
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->setDailyNotiTimePreference(Z)V

    .line 650
    const-string v1, "setnotificationtime"

    invoke-virtual {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->set_notificationsettime_preferences:Landroid/preference/Preference;

    .line 651
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->set_notificationsettime_preferences:Landroid/preference/Preference;

    if-eqz v1, :cond_7

    .line 652
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->set_notificationsettime_preferences:Landroid/preference/Preference;

    invoke-virtual {v1, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 658
    :cond_7
    iget-boolean v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->isFirstGps:Z

    if-nez v1, :cond_15

    .line 659
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->setCurrentLocationSetting()V

    .line 660
    const-string v1, "setcurrentcity"

    invoke-virtual {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->useCurrentCity_preferences:Landroid/preference/Preference;

    .line 661
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->useCurrentCity_preferences:Landroid/preference/Preference;

    if-eqz v1, :cond_8

    .line 662
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->useCurrentCity_preferences:Landroid/preference/Preference;

    invoke-virtual {v1, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 672
    :cond_8
    :goto_0
    iget-boolean v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->devSetting:Z

    if-eqz v1, :cond_14

    .line 673
    const-string v1, "background_video"

    invoke-virtual {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->background_video_preferences:Landroid/preference/CheckBoxPreference;

    .line 674
    const-string v1, "background_video_repeat"

    invoke-virtual {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->background_video_repeat_preferences:Landroid/preference/CheckBoxPreference;

    .line 675
    const-string v1, "icon_animation"

    invoke-virtual {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->icon_animation_preferences:Landroid/preference/CheckBoxPreference;

    .line 676
    const-string v1, "icon_animation_repeat"

    invoke-virtual {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->icon_animation_repeat_preferences:Landroid/preference/CheckBoxPreference;

    .line 677
    const-string v1, "change_city_scroll"

    invoke-virtual {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->changecity_scroll_preferences:Landroid/preference/CheckBoxPreference;

    .line 678
    const-string v1, "weather_effect"

    invoke-virtual {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->weather_effect_preferences:Landroid/preference/CheckBoxPreference;

    .line 679
    const-string v1, "infor_animation"

    invoke-virtual {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->infor_animation_preferences:Landroid/preference/CheckBoxPreference;

    .line 680
    const-string v1, "bottom_icon_animation"

    invoke-virtual {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->bottom_icon_animation_preferences:Landroid/preference/CheckBoxPreference;

    .line 681
    const-string v1, "widget_icon_animation"

    invoke-virtual {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->widget_icon_animation_preferences:Landroid/preference/CheckBoxPreference;

    .line 682
    const-string v1, "widget_city_change_animation"

    invoke-virtual {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->widget_city_change_animation_preferences:Landroid/preference/CheckBoxPreference;

    .line 683
    const-string v1, "city_select_animation"

    invoke-virtual {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->city_select_animation_preferences:Landroid/preference/CheckBoxPreference;

    .line 684
    const-string v1, "duration_time"

    invoke-virtual {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->duration_time_preferences:Landroid/preference/Preference;

    .line 686
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->background_video_preferences:Landroid/preference/CheckBoxPreference;

    if-eqz v1, :cond_9

    .line 687
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->background_video_preferences:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 690
    :cond_9
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->background_video_repeat_preferences:Landroid/preference/CheckBoxPreference;

    if-eqz v1, :cond_a

    .line 691
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->background_video_repeat_preferences:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 694
    :cond_a
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->icon_animation_preferences:Landroid/preference/CheckBoxPreference;

    if-eqz v1, :cond_b

    .line 695
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->icon_animation_preferences:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 698
    :cond_b
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->icon_animation_repeat_preferences:Landroid/preference/CheckBoxPreference;

    if-eqz v1, :cond_c

    .line 699
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->icon_animation_repeat_preferences:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 706
    :cond_c
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->changecity_scroll_preferences:Landroid/preference/CheckBoxPreference;

    if-eqz v1, :cond_d

    .line 707
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->changecity_scroll_preferences:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 710
    :cond_d
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->weather_effect_preferences:Landroid/preference/CheckBoxPreference;

    if-eqz v1, :cond_e

    .line 711
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->weather_effect_preferences:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 714
    :cond_e
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->infor_animation_preferences:Landroid/preference/CheckBoxPreference;

    if-eqz v1, :cond_f

    .line 715
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->infor_animation_preferences:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 718
    :cond_f
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->bottom_icon_animation_preferences:Landroid/preference/CheckBoxPreference;

    if-eqz v1, :cond_10

    .line 719
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->bottom_icon_animation_preferences:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 722
    :cond_10
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->widget_icon_animation_preferences:Landroid/preference/CheckBoxPreference;

    if-eqz v1, :cond_11

    .line 723
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->widget_icon_animation_preferences:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 726
    :cond_11
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->widget_city_change_animation_preferences:Landroid/preference/CheckBoxPreference;

    if-eqz v1, :cond_12

    .line 727
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->widget_city_change_animation_preferences:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 730
    :cond_12
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->city_select_animation_preferences:Landroid/preference/CheckBoxPreference;

    if-eqz v1, :cond_13

    .line 731
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->city_select_animation_preferences:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 734
    :cond_13
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->duration_time_preferences:Landroid/preference/Preference;

    if-eqz v1, :cond_14

    .line 735
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->duration_time_preferences:Landroid/preference/Preference;

    invoke-virtual {v1, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 742
    :cond_14
    :goto_1
    return-void

    .line 665
    :cond_15
    const-string v1, "setcurrentcity"

    invoke-virtual {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->set_current_city_preferences:Landroid/preference/CheckBoxPreference;

    .line 667
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->set_current_city_preferences:Landroid/preference/CheckBoxPreference;

    if-eqz v1, :cond_8

    .line 668
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->set_current_city_preferences:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 738
    :catch_0
    move-exception v0

    .line 739
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    .line 740
    const-string v1, ""

    const-string v2, "setView : preference is null"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private setupActionBar()V
    .locals 8

    .prologue
    const v7, 0x7f0d0006

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 336
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->actionbar:Landroid/app/ActionBar;

    if-nez v2, :cond_0

    .line 337
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->actionbar:Landroid/app/ActionBar;

    .line 339
    :cond_0
    sget-boolean v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isLollipopOS:Z

    if-nez v2, :cond_3

    .line 340
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->actionbar:Landroid/app/ActionBar;

    if-eqz v2, :cond_1

    .line 341
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->actionbar:Landroid/app/ActionBar;

    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f02008f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 343
    const v2, 0x7f030005

    const/4 v3, 0x0

    invoke-static {p0, v2, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->actionbarLayout:Landroid/widget/RelativeLayout;

    .line 345
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->actionbar:Landroid/app/ActionBar;

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->actionbarLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v3}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    .line 346
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->actionbarLayout:Landroid/widget/RelativeLayout;

    const v3, 0x7f080008

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->actionbarTitle:Landroid/widget/TextView;

    .line 347
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->actionbarLayout:Landroid/widget/RelativeLayout;

    const v3, 0x7f080006

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->actionbarBack:Landroid/widget/LinearLayout;

    .line 349
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->actionbarBack:Landroid/widget/LinearLayout;

    const v3, 0x7f080009

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 350
    .local v1, "v":Landroid/widget/ImageView;
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0200aa

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 351
    .local v0, "backIcon":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v0, v6}, Landroid/graphics/drawable/Drawable;->setAutoMirrored(Z)V

    .line 352
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 354
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->actionbar:Landroid/app/ActionBar;

    invoke-virtual {v2, v5}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 355
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->actionbar:Landroid/app/ActionBar;

    invoke-virtual {v2, v5}, Landroid/app/ActionBar;->setDisplayUseLogoEnabled(Z)V

    .line 356
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->actionbar:Landroid/app/ActionBar;

    invoke-virtual {v2, v5}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 357
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->actionbar:Landroid/app/ActionBar;

    invoke-virtual {v2, v6}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 359
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->actionbarTitle:Landroid/widget/TextView;

    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setText(I)V

    .line 361
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->actionbarBack:Landroid/widget/LinearLayout;

    new-instance v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$1;

    invoke-direct {v3, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$1;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;)V

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 368
    .end local v0    # "backIcon":Landroid/graphics/drawable/Drawable;
    .end local v1    # "v":Landroid/widget/ImageView;
    :cond_1
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->actionbarBack:Landroid/widget/LinearLayout;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mCtx:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0d008b

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mCtx:Landroid/content/Context;

    .line 370
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0d0097

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 368
    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 382
    :cond_2
    :goto_0
    return-void

    .line 373
    :cond_3
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->actionbar:Landroid/app/ActionBar;

    if-eqz v2, :cond_2

    .line 374
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->actionbar:Landroid/app/ActionBar;

    invoke-virtual {v2, v5}, Landroid/app/ActionBar;->setDisplayUseLogoEnabled(Z)V

    .line 375
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->actionbar:Landroid/app/ActionBar;

    invoke-virtual {v2, v6}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 376
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->actionbar:Landroid/app/ActionBar;

    invoke-virtual {v2, v5}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 378
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->actionbar:Landroid/app/ActionBar;

    invoke-virtual {v2, v7}, Landroid/app/ActionBar;->setTitle(I)V

    .line 379
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->actionbar:Landroid/app/ActionBar;

    invoke-virtual {v2, v6}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    goto :goto_0
.end method

.method private stopHttpThread()V
    .locals 3

    .prologue
    .line 1364
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mHttpThreads:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    .line 1365
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mHttpThreads:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 1366
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mHttpThreads:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Thread;

    .line 1367
    .local v0, "t":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    .line 1370
    .end local v0    # "t":Ljava/lang/Thread;
    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mHttpThreads:Ljava/util/ArrayList;

    .line 1372
    :cond_1
    return-void
.end method

.method private updateCheckboxPreference(Landroid/preference/Preference;Ljava/lang/String;)V
    .locals 4
    .param p1, "preference"    # Landroid/preference/Preference;
    .param p2, "column"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    .line 1756
    instance-of v2, p1, Landroid/preference/CheckBoxPreference;

    if-eqz v2, :cond_0

    move-object v2, p1

    .line 1757
    check-cast v2, Landroid/preference/CheckBoxPreference;

    invoke-virtual {v2}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1758
    const/4 v1, 0x0

    .line 1763
    .local v1, "value":I
    :goto_0
    const/4 v0, 0x0

    .line 1764
    .local v0, "result":I
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mCtx:Landroid/content/Context;

    invoke-static {v2, p2, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateViEffectSettings(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v0

    .line 1765
    const/4 v2, -0x1

    if-ne v0, v2, :cond_2

    .line 1766
    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->setResult(I)V

    .line 1767
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->finish()V

    .line 1776
    .end local v0    # "result":I
    .end local v1    # "value":I
    .end local p1    # "preference":Landroid/preference/Preference;
    :cond_0
    :goto_1
    return-void

    .line 1760
    .restart local p1    # "preference":Landroid/preference/Preference;
    :cond_1
    const/4 v1, 0x1

    .restart local v1    # "value":I
    goto :goto_0

    .line 1769
    .restart local v0    # "result":I
    :cond_2
    if-ne v1, v3, :cond_3

    .line 1770
    check-cast p1, Landroid/preference/CheckBoxPreference;

    .end local p1    # "preference":Landroid/preference/Preference;
    invoke-virtual {p1, v3}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto :goto_1

    .line 1772
    .restart local p1    # "preference":Landroid/preference/Preference;
    :cond_3
    check-cast p1, Landroid/preference/CheckBoxPreference;

    .end local p1    # "preference":Landroid/preference/Preference;
    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto :goto_1
.end method


# virtual methods
.method public checkDateFormat(J)Ljava/lang/String;
    .locals 3
    .param p1, "setTime"    # J

    .prologue
    .line 1974
    invoke-static {p0}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v2

    .line 1975
    .local v2, "timeformat":Ljava/text/DateFormat;
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, p1, p2}, Ljava/util/Date;-><init>(J)V

    .line 1977
    .local v0, "date":Ljava/util/Date;
    invoke-virtual {v2, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    .line 1989
    .local v1, "timeTempStr":Ljava/lang/String;
    return-object v1
.end method

.method public cleanResource()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 403
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->unit_preferences:Landroid/preference/ListPreference;

    .line 405
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->autorefresh_preferences:Landroid/preference/ListPreference;

    .line 406
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->refreshentering_preferences:Landroid/preference/CheckBoxPreference;

    .line 407
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->widgetBGOnOff_preferences:Landroid/preference/CheckBoxPreference;

    .line 408
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->widgetBGTransparencyControl_preferences:Landroid/preference/PreferenceScreen;

    .line 409
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->useCurrentCity_preferences:Landroid/preference/Preference;

    .line 410
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->set_notifications_preferences:Landroid/preference/CheckBoxPreference;

    .line 411
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->set_notificationsettime_preferences:Landroid/preference/Preference;

    .line 419
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->refreshItems:[Ljava/lang/String;

    .line 420
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->refreshItemsValue:[Ljava/lang/String;

    .line 421
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->refreshTimes:[I

    .line 423
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->actionbar:Landroid/app/ActionBar;

    .line 424
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->actionbarLayout:Landroid/widget/RelativeLayout;

    .line 425
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->actionbarBack:Landroid/widget/LinearLayout;

    .line 426
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->actionbarTitle:Landroid/widget/TextView;

    .line 429
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->stopHttpThread()V

    .line 431
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->getDataHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 432
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->getDataHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 433
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->getDataHandler:Landroid/os/Handler;

    .line 436
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->getLocDataHandler:Landroid/os/Handler;

    if-eqz v0, :cond_1

    .line 437
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->getLocDataHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 438
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->getLocDataHandler:Landroid/os/Handler;

    .line 441
    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mMoveHandler:Landroid/os/Handler;

    if-eqz v0, :cond_2

    .line 442
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mMoveHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 443
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mMoveHandler:Landroid/os/Handler;

    .line 446
    :cond_2
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mLoadingDialog:Landroid/app/Dialog;

    .line 447
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mPopupDialog:Landroid/app/Dialog;

    .line 448
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mNetworkErrorDialog:Landroid/app/Dialog;

    .line 451
    return-void
.end method

.method public getButtenTypeCheck()I
    .locals 1

    .prologue
    .line 1969
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->CheckType:I

    return v0
.end method

.method public isActivityVisible()Z
    .locals 1

    .prologue
    .line 470
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mIsVisible:Z

    return v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 491
    invoke-super {p0, p1, p2, p3}, Landroid/preference/PreferenceActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 492
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 493
    invoke-virtual {p0, p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->setResult(I)V

    .line 494
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->finish()V

    .line 497
    :cond_0
    if-nez p1, :cond_1

    .line 498
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->init()V

    .line 502
    :cond_1
    const/16 v0, 0x4b3

    if-ne v0, p1, :cond_2

    .line 503
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getLocationCheck(Landroid/content/Context;)I

    move-result v0

    const/16 v1, 0xbbb

    if-ne v0, v1, :cond_3

    .line 523
    :cond_2
    :goto_0
    return-void

    .line 505
    :cond_3
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getCheckCurrentCityLocation(Landroid/content/Context;)I

    move-result v0

    if-nez v0, :cond_2

    .line 506
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->getCurrentLocation()V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 464
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 465
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->setupActionBar()V

    .line 466
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->init()V

    .line 467
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 267
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 268
    iput-object p0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mCtx:Landroid/content/Context;

    .line 269
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setLayoutDirection(I)V

    .line 271
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mcl:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    .line 284
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->devSetting:Z

    if-nez v0, :cond_0

    .line 285
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09000c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 286
    iput-boolean v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->devSetting:Z

    .line 292
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090011

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->isFirstGps:Z

    .line 294
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->devSetting:Z

    if-eqz v0, :cond_3

    .line 295
    const v0, 0x7f03001c

    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->addPreferencesFromResource(I)V

    .line 316
    :goto_1
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->setupActionBar()V

    .line 318
    invoke-virtual {p0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->setActivityVisibleState(Z)V

    .line 320
    invoke-direct {p0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->setDailyNotiTimePreference(Z)V

    .line 321
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->isFirstGps:Z

    if-nez v0, :cond_1

    .line 322
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->setCurrentLocationSetting()V

    .line 324
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->init()V

    .line 325
    iput-boolean v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->creating:Z

    .line 333
    return-void

    .line 288
    :cond_2
    iput-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->devSetting:Z

    goto :goto_0

    .line 296
    :cond_3
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getWIdgetMode(Landroid/content/Context;)I

    move-result v0

    const/16 v1, 0xfa1

    if-ne v0, v1, :cond_5

    .line 297
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->isFirstGps:Z

    if-nez v0, :cond_4

    .line 298
    const v0, 0x7f03001e

    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->addPreferencesFromResource(I)V

    goto :goto_1

    .line 300
    :cond_4
    const v0, 0x7f03001d

    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->addPreferencesFromResource(I)V

    goto :goto_1

    .line 302
    :cond_5
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getWIdgetMode(Landroid/content/Context;)I

    move-result v0

    const/16 v1, 0xfa2

    if-ne v0, v1, :cond_7

    .line 303
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->isFirstGps:Z

    if-nez v0, :cond_6

    .line 304
    const v0, 0x7f030020

    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->addPreferencesFromResource(I)V

    goto :goto_1

    .line 306
    :cond_6
    const v0, 0x7f03001f

    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->addPreferencesFromResource(I)V

    goto :goto_1

    .line 309
    :cond_7
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->isFirstGps:Z

    if-nez v0, :cond_8

    .line 310
    const v0, 0x7f030021

    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->addPreferencesFromResource(I)V

    goto :goto_1

    .line 312
    :cond_8
    const v0, 0x7f03001b

    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->addPreferencesFromResource(I)V

    goto :goto_1
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 385
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onDestroy()V

    .line 387
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->cleanResource()V

    .line 388
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 454
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    .line 455
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 460
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 457
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->finish()V

    .line 458
    const/4 v0, 0x1

    goto :goto_0

    .line 455
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 478
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onPause()V

    .line 479
    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->ch_location:I

    const/16 v2, 0x63

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->location:I

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->ch_location:I

    if-eq v1, v2, :cond_0

    .line 482
    const-string v1, ""

    const-string v2, "location setting changed in menusettings"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 483
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 484
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "flags"

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->ch_location:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 485
    const/16 v1, 0x4b00

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->setResult(ILandroid/content/Intent;)V

    .line 487
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->setActivityVisibleState(Z)V

    .line 488
    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 12
    .param p1, "preference"    # Landroid/preference/Preference;
    .param p2, "newValue"    # Ljava/lang/Object;

    .prologue
    .line 1375
    const-string v9, ""

    const-string v10, "onPreferenceChange"

    invoke-static {v9, v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1376
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v9

    const-string v10, "unit"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 1377
    instance-of v9, p1, Landroid/preference/ListPreference;

    if-eqz v9, :cond_1

    .line 1378
    const-string v10, ""

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "unit setting = "

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object v9, p1

    check-cast v9, Landroid/preference/ListPreference;

    invoke-virtual {v9}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v11, " -> "

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 1379
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 1378
    invoke-static {v10, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v9, p1

    .line 1380
    check-cast v9, Landroid/preference/ListPreference;

    invoke-virtual {v9}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 1381
    const/4 v9, 0x0

    .line 1751
    .end local p1    # "preference":Landroid/preference/Preference;
    :goto_0
    return v9

    .line 1384
    .restart local p1    # "preference":Landroid/preference/Preference;
    :cond_0
    const/4 v2, 0x0

    .line 1385
    .local v2, "fromScale":I
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getTempScaleSetting(Landroid/content/Context;)I

    move-result v2

    .line 1387
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    if-nez v9, :cond_2

    const/4 v9, 0x1

    .line 1386
    :goto_1
    invoke-static {p0, v9, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->SaveTemperaturesData(Landroid/content/Context;II)V

    .line 1389
    iget-object v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mCtx:Landroid/content/Context;

    .line 1390
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v9

    .line 1389
    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    if-nez v9, :cond_3

    const/4 v9, 0x1

    :goto_2
    invoke-static {v10, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateTempScale(Landroid/content/Context;I)I

    move-result v7

    .line 1392
    .local v7, "result":I
    const-string v9, ""

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "onPrefChange= "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1393
    const/4 v9, -0x1

    if-ne v7, v9, :cond_4

    .line 1394
    invoke-virtual {p0, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->setResult(I)V

    .line 1395
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->finish()V

    .line 1751
    .end local v2    # "fromScale":I
    .end local v7    # "result":I
    .end local p1    # "preference":Landroid/preference/Preference;
    :cond_1
    :goto_3
    const/4 v9, 0x0

    goto :goto_0

    .line 1387
    .restart local v2    # "fromScale":I
    .restart local p1    # "preference":Landroid/preference/Preference;
    :cond_2
    const/4 v9, 0x0

    goto :goto_1

    .line 1389
    :cond_3
    const/4 v9, 0x0

    goto :goto_2

    .restart local v7    # "result":I
    :cond_4
    move-object v9, p1

    .line 1397
    check-cast v9, Landroid/preference/ListPreference;

    .line 1398
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v10

    .line 1397
    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v10

    invoke-virtual {v9, v10}, Landroid/preference/ListPreference;->setValueIndex(I)V

    .line 1399
    const-string v9, ""

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "onPC= "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1400
    iget-object v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mCtx:Landroid/content/Context;

    invoke-static {v9, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DaemonInterface;->checkSameDaemonCityIdAtStting(Landroid/content/Context;Landroid/preference/Preference;)V

    .line 1401
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->setValues()V

    .line 1402
    new-instance v9, Landroid/content/Intent;

    const-string v10, "com.sec.android.widgetapp.ap.hero.accuweather.action.CHANGE_SETTING"

    invoke-direct {v9, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_3

    .line 1431
    .end local v2    # "fromScale":I
    .end local v7    # "result":I
    :cond_5
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v9

    const-string v10, "autorefresh"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_b

    .line 1432
    instance-of v9, p1, Landroid/preference/ListPreference;

    if-eqz v9, :cond_1

    move-object v9, p1

    .line 1433
    check-cast v9, Landroid/preference/ListPreference;

    invoke-virtual {v9}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 1437
    :cond_6
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f090005

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v9

    if-nez v9, :cond_7

    .line 1438
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->simEnabled(Landroid/content/Context;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 1443
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    if-eqz v9, :cond_7

    const-string v9, "config"

    const/4 v10, 0x4

    .line 1444
    invoke-virtual {p0, v9, v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v9

    const-string v10, "CONFIRM_AUTOREFRESH"

    const/4 v11, 0x1

    invoke-interface {v9, v10, v11}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 1446
    const/16 v9, 0x3f4

    new-instance v10, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$6;

    invoke-direct {v10, p0, p2, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$6;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;Ljava/lang/Object;Landroid/preference/Preference;)V

    invoke-static {p0, v9, v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->showDialog(Landroid/content/Context;ILcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;)Landroid/app/Dialog;

    move-result-object v1

    .line 1474
    .local v1, "confirm":Landroid/app/Dialog;
    new-instance v9, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$7;

    invoke-direct {v9, p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$7;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;Landroid/app/Dialog;)V

    invoke-virtual {v1, v9}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    goto/16 :goto_3

    .line 1486
    .end local v1    # "confirm":Landroid/app/Dialog;
    :cond_7
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    if-nez v9, :cond_9

    .line 1487
    iget-object v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mCtx:Landroid/content/Context;

    .line 1488
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v10

    .line 1487
    invoke-static {v9, v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateAutoRefreshTime(Landroid/content/Context;I)I

    move-result v7

    .line 1489
    .restart local v7    # "result":I
    const/4 v9, -0x1

    if-ne v7, v9, :cond_8

    .line 1490
    invoke-virtual {p0, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->setResult(I)V

    .line 1491
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->finish()V

    goto/16 :goto_3

    .line 1493
    :cond_8
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    const/4 v10, 0x1

    const/4 v11, 0x0

    invoke-static {v9, v10, v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getNextTime(Landroid/content/Context;ZZ)J

    move-result-wide v5

    .line 1494
    .local v5, "nextTime":J
    iget-object v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mCtx:Landroid/content/Context;

    invoke-static {v9, v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateNextTime(Landroid/content/Context;J)I

    .line 1495
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->init()V

    move-object v9, p1

    .line 1497
    check-cast v9, Landroid/preference/ListPreference;

    .line 1498
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v10

    .line 1497
    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v10

    invoke-virtual {v9, v10}, Landroid/preference/ListPreference;->setValueIndex(I)V

    .line 1499
    iget-object v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mCtx:Landroid/content/Context;

    invoke-static {v9, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DaemonInterface;->checkSameDaemonCityIdAtStting(Landroid/content/Context;Landroid/preference/Preference;)V

    .line 1500
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->setValues()V

    goto/16 :goto_3

    .line 1503
    .end local v5    # "nextTime":J
    .end local v7    # "result":I
    :cond_9
    iget-object v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mCtx:Landroid/content/Context;

    .line 1504
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v10

    .line 1503
    invoke-static {v9, v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateAutoRefreshTime(Landroid/content/Context;I)I

    move-result v7

    .line 1506
    .restart local v7    # "result":I
    const/4 v9, -0x1

    if-ne v7, v9, :cond_a

    .line 1507
    invoke-virtual {p0, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->setResult(I)V

    .line 1508
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->finish()V

    goto/16 :goto_3

    .line 1510
    :cond_a
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    const/4 v10, 0x1

    const/4 v11, 0x0

    invoke-static {v9, v10, v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getNextTime(Landroid/content/Context;ZZ)J

    move-result-wide v5

    .line 1511
    .restart local v5    # "nextTime":J
    iget-object v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mCtx:Landroid/content/Context;

    invoke-static {v9, v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateNextTime(Landroid/content/Context;J)I

    .line 1512
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->init()V

    move-object v9, p1

    .line 1514
    check-cast v9, Landroid/preference/ListPreference;

    .line 1515
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v10

    .line 1514
    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v10

    invoke-virtual {v9, v10}, Landroid/preference/ListPreference;->setValueIndex(I)V

    .line 1516
    iget-object v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mCtx:Landroid/content/Context;

    invoke-static {v9, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DaemonInterface;->checkSameDaemonCityIdAtStting(Landroid/content/Context;Landroid/preference/Preference;)V

    .line 1517
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->setValues()V

    goto/16 :goto_3

    .line 1522
    .end local v5    # "nextTime":J
    .end local v7    # "result":I
    :cond_b
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v9

    const-string v10, "refreshentering"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_f

    .line 1524
    instance-of v9, p1, Landroid/preference/CheckBoxPreference;

    if-eqz v9, :cond_1

    move-object v9, p1

    .line 1525
    check-cast v9, Landroid/preference/CheckBoxPreference;

    invoke-virtual {v9}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v9

    if-eqz v9, :cond_c

    .line 1526
    const/4 v8, 0x0

    .line 1531
    .local v8, "value":I
    :goto_4
    iget-object v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mCtx:Landroid/content/Context;

    invoke-static {v9, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateRefreshEnteringSettings(Landroid/content/Context;I)I

    move-result v7

    .line 1532
    .restart local v7    # "result":I
    const/4 v9, -0x1

    if-ne v7, v9, :cond_d

    .line 1533
    invoke-virtual {p0, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->setResult(I)V

    .line 1534
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->finish()V

    goto/16 :goto_3

    .line 1528
    .end local v7    # "result":I
    .end local v8    # "value":I
    :cond_c
    const/4 v8, 0x1

    .restart local v8    # "value":I
    goto :goto_4

    .line 1536
    .restart local v7    # "result":I
    :cond_d
    const/4 v9, 0x1

    if-ne v8, v9, :cond_e

    .line 1537
    check-cast p1, Landroid/preference/CheckBoxPreference;

    .end local p1    # "preference":Landroid/preference/Preference;
    const/4 v9, 0x1

    invoke-virtual {p1, v9}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto/16 :goto_3

    .line 1539
    .restart local p1    # "preference":Landroid/preference/Preference;
    :cond_e
    check-cast p1, Landroid/preference/CheckBoxPreference;

    .end local p1    # "preference":Landroid/preference/Preference;
    const/4 v9, 0x0

    invoke-virtual {p1, v9}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto/16 :goto_3

    .line 1543
    .end local v7    # "result":I
    .end local v8    # "value":I
    .restart local p1    # "preference":Landroid/preference/Preference;
    :cond_f
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v9

    const-string v10, "refreshroaming"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_13

    .line 1545
    instance-of v9, p1, Landroid/preference/CheckBoxPreference;

    if-eqz v9, :cond_1

    move-object v9, p1

    .line 1551
    check-cast v9, Landroid/preference/CheckBoxPreference;

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    move-object v9, p1

    .line 1553
    check-cast v9, Landroid/preference/CheckBoxPreference;

    invoke-virtual {v9}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v9

    if-eqz v9, :cond_10

    .line 1554
    const/4 v8, 0x0

    .line 1559
    .restart local v8    # "value":I
    :goto_5
    iget-object v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mCtx:Landroid/content/Context;

    invoke-static {v9, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateRefreshRoamingSettings(Landroid/content/Context;I)I

    move-result v7

    .line 1560
    .restart local v7    # "result":I
    const/4 v9, -0x1

    if-ne v7, v9, :cond_11

    .line 1561
    invoke-virtual {p0, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->setResult(I)V

    .line 1562
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->finish()V

    goto/16 :goto_3

    .line 1556
    .end local v7    # "result":I
    .end local v8    # "value":I
    :cond_10
    const/4 v8, 0x1

    .restart local v8    # "value":I
    goto :goto_5

    .line 1564
    .restart local v7    # "result":I
    :cond_11
    const/4 v9, 0x1

    if-ne v8, v9, :cond_12

    .line 1565
    check-cast p1, Landroid/preference/CheckBoxPreference;

    .end local p1    # "preference":Landroid/preference/Preference;
    const/4 v9, 0x1

    invoke-virtual {p1, v9}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto/16 :goto_3

    .line 1567
    .restart local p1    # "preference":Landroid/preference/Preference;
    :cond_12
    check-cast p1, Landroid/preference/CheckBoxPreference;

    .end local p1    # "preference":Landroid/preference/Preference;
    const/4 v9, 0x0

    invoke-virtual {p1, v9}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto/16 :goto_3

    .line 1574
    .end local v7    # "result":I
    .end local v8    # "value":I
    .restart local p1    # "preference":Landroid/preference/Preference;
    :cond_13
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v9

    const-string v10, "widgetBGOnOff"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_14

    .line 1575
    const-string v9, "EFFECT_14"

    invoke-direct {p0, p1, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->updateCheckboxPreference(Landroid/preference/Preference;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 1576
    :cond_14
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v9

    const-string v10, "setcurrentcity"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1e

    .line 1580
    instance-of v9, p1, Landroid/preference/CheckBoxPreference;

    if-eqz v9, :cond_1

    move-object v9, p1

    .line 1582
    check-cast v9, Landroid/preference/CheckBoxPreference;

    invoke-virtual {v9}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v9

    if-eqz v9, :cond_16

    .line 1583
    const/4 v8, 0x0

    .line 1588
    .restart local v8    # "value":I
    :goto_6
    const/4 v9, 0x1

    if-ne v8, v9, :cond_1b

    .line 1589
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getLocationCheck(Landroid/content/Context;)I

    move-result v4

    .line 1590
    .local v4, "locationSetting":I
    const/16 v9, 0xbba

    if-ne v4, v9, :cond_17

    .line 1591
    const/16 v9, 0x3f5

    new-instance v10, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$8;

    invoke-direct {v10, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$8;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;)V

    invoke-static {p0, v9, v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->showDialog(Landroid/content/Context;ILcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;)Landroid/app/Dialog;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mPopupDialog:Landroid/app/Dialog;

    .line 1653
    :cond_15
    :goto_7
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->init()V

    goto/16 :goto_3

    .line 1585
    .end local v4    # "locationSetting":I
    .end local v8    # "value":I
    :cond_16
    const/4 v8, 0x1

    .restart local v8    # "value":I
    goto :goto_6

    .line 1617
    .restart local v4    # "locationSetting":I
    :cond_17
    const/16 v9, 0xbbb

    if-ne v4, v9, :cond_18

    .line 1618
    new-instance v9, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$9;

    invoke-direct {v9, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$9;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;)V

    invoke-static {p0, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->showGPSoffDialog(Landroid/app/Activity;Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;)Landroid/app/Dialog;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mPopupDialog:Landroid/app/Dialog;

    .line 1629
    iget-object v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mPopupDialog:Landroid/app/Dialog;

    new-instance v10, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$10;

    invoke-direct {v10, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$10;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;)V

    invoke-virtual {v9, v10}, Landroid/app/Dialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    goto :goto_7

    .line 1634
    :cond_18
    const/16 v9, 0xbc0

    if-ne v4, v9, :cond_19

    .line 1635
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getPreferenceGPSOnlyDoNotShow(Landroid/content/Context;)Z

    move-result v9

    if-nez v9, :cond_19

    .line 1636
    new-instance v9, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$11;

    invoke-direct {v9, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$11;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;)V

    invoke-static {p0, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->showGPSonlyDialog(Landroid/app/Activity;Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/LocationGetter;)Landroid/app/Dialog;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mPopupDialog:Landroid/app/Dialog;

    .line 1642
    iget-object v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mPopupDialog:Landroid/app/Dialog;

    new-instance v10, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$12;

    invoke-direct {v10, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$12;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;)V

    invoke-virtual {v9, v10}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    goto :goto_7

    .line 1647
    :cond_19
    const/16 v9, 0xbc0

    if-ne v4, v9, :cond_1a

    .line 1648
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getPreferenceGPSOnlyDoNotShow(Landroid/content/Context;)Z

    move-result v9

    if-eqz v9, :cond_1a

    .line 1649
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->getLocationInfo()V

    goto :goto_7

    .line 1650
    :cond_1a
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getCheckCurrentCityLocation(Landroid/content/Context;)I

    move-result v9

    if-nez v9, :cond_15

    .line 1651
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->getCurrentLocation()V

    goto :goto_7

    .line 1655
    .end local v4    # "locationSetting":I
    :cond_1b
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getRegisteredCityCount(Landroid/content/Context;)I

    move-result v0

    .line 1657
    .local v0, "cityListCnt":I
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1658
    .local v3, "location":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v9, "cityId:current"

    invoke-virtual {v3, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1659
    const/4 v9, 0x1

    if-ne v0, v9, :cond_1c

    .line 1660
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getCheckCurrentCityLocation(Landroid/content/Context;)I

    move-result v9

    const/4 v10, 0x1

    if-ne v9, v10, :cond_1c

    .line 1661
    const/16 v9, 0x3fc

    new-instance v10, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$13;

    invoke-direct {v10, p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$13;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;Landroid/preference/Preference;)V

    invoke-static {p0, v9, v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->showDialog(Landroid/content/Context;ILcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;)Landroid/app/Dialog;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mPopupDialog:Landroid/app/Dialog;

    goto/16 :goto_3

    .line 1685
    :cond_1c
    const/4 v9, 0x0

    invoke-static {p0, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateCheckCurrentCityLocation(Landroid/content/Context;I)I

    .line 1687
    check-cast p1, Landroid/preference/CheckBoxPreference;

    .end local p1    # "preference":Landroid/preference/Preference;
    const/4 v9, 0x0

    invoke-virtual {p1, v9}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 1688
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getNotificationSettings(Landroid/content/Context;)I

    move-result v9

    const/4 v10, 0x1

    if-ne v9, v10, :cond_1d

    .line 1689
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->unregisterNotiTime(Landroid/content/Context;)V

    .line 1691
    :cond_1d
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->init()V

    .line 1693
    new-instance v9, Landroid/content/Intent;

    const-string v10, "com.sec.android.widgetapp.ap.hero.accuweather.action.CHANGE_SETTING"

    invoke-direct {v9, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_3

    .line 1700
    .end local v0    # "cityListCnt":I
    .end local v3    # "location":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v8    # "value":I
    .restart local p1    # "preference":Landroid/preference/Preference;
    :cond_1e
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v9

    const-string v10, "setnotifications"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_22

    .line 1703
    instance-of v9, p1, Landroid/preference/CheckBoxPreference;

    if-eqz v9, :cond_1

    .line 1705
    check-cast p1, Landroid/preference/CheckBoxPreference;

    .end local p1    # "preference":Landroid/preference/Preference;
    invoke-virtual {p1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v9

    if-eqz v9, :cond_1f

    .line 1706
    const/4 v8, 0x0

    .line 1711
    .restart local v8    # "value":I
    :goto_8
    iget-object v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mCtx:Landroid/content/Context;

    invoke-static {v9, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateNotificationSettings(Landroid/content/Context;I)I

    move-result v7

    .line 1712
    .restart local v7    # "result":I
    const/4 v9, -0x1

    if-ne v7, v9, :cond_20

    .line 1713
    invoke-virtual {p0, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->setResult(I)V

    .line 1714
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->finish()V

    .line 1725
    :goto_9
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->init()V

    goto/16 :goto_3

    .line 1708
    .end local v7    # "result":I
    .end local v8    # "value":I
    :cond_1f
    const/4 v8, 0x1

    .restart local v8    # "value":I
    goto :goto_8

    .line 1716
    .restart local v7    # "result":I
    :cond_20
    const/4 v9, 0x1

    if-ne v8, v9, :cond_21

    .line 1718
    iget-object v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->set_notifications_preferences:Landroid/preference/CheckBoxPreference;

    const/4 v10, 0x1

    invoke-static {p0, v9, v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->saveSwitchState(Landroid/content/Context;Landroid/preference/CheckBoxPreference;Z)V

    goto :goto_9

    .line 1721
    :cond_21
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->unregisterNotiTime(Landroid/content/Context;)V

    .line 1722
    iget-object v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->set_notifications_preferences:Landroid/preference/CheckBoxPreference;

    const/4 v10, 0x0

    invoke-static {p0, v9, v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->saveSwitchState(Landroid/content/Context;Landroid/preference/CheckBoxPreference;Z)V

    goto :goto_9

    .line 1727
    .end local v7    # "result":I
    .end local v8    # "value":I
    .restart local p1    # "preference":Landroid/preference/Preference;
    :cond_22
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v9

    const-string v10, "background_video"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_23

    .line 1728
    const-string v9, "EFFECT_1"

    invoke-direct {p0, p1, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->updateCheckboxPreference(Landroid/preference/Preference;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 1729
    :cond_23
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v9

    const-string v10, "background_video_repeat"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_24

    .line 1730
    const-string v9, "EFFECT_2"

    invoke-direct {p0, p1, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->updateCheckboxPreference(Landroid/preference/Preference;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 1731
    :cond_24
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v9

    const-string v10, "icon_animation"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_25

    .line 1732
    const-string v9, "EFFECT_3"

    invoke-direct {p0, p1, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->updateCheckboxPreference(Landroid/preference/Preference;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 1733
    :cond_25
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v9

    const-string v10, "icon_animation_repeat"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_26

    .line 1734
    const-string v9, "EFFECT_4"

    invoke-direct {p0, p1, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->updateCheckboxPreference(Landroid/preference/Preference;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 1735
    :cond_26
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v9

    const-string v10, "change_city_scroll"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_27

    .line 1736
    const-string v9, "EFFECT_6"

    invoke-direct {p0, p1, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->updateCheckboxPreference(Landroid/preference/Preference;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 1737
    :cond_27
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v9

    const-string v10, "weather_effect"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_28

    .line 1738
    const-string v9, "EFFECT_7"

    invoke-direct {p0, p1, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->updateCheckboxPreference(Landroid/preference/Preference;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 1739
    :cond_28
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v9

    const-string v10, "infor_animation"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_29

    .line 1740
    const-string v9, "EFFECT_8"

    invoke-direct {p0, p1, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->updateCheckboxPreference(Landroid/preference/Preference;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 1741
    :cond_29
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v9

    const-string v10, "bottom_icon_animation"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2a

    .line 1742
    const-string v9, "EFFECT_9"

    invoke-direct {p0, p1, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->updateCheckboxPreference(Landroid/preference/Preference;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 1743
    :cond_2a
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v9

    const-string v10, "widget_icon_animation"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2b

    .line 1744
    const-string v9, "EFFECT_10"

    invoke-direct {p0, p1, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->updateCheckboxPreference(Landroid/preference/Preference;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 1745
    :cond_2b
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v9

    const-string v10, "widget_city_change_animation"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2c

    .line 1746
    const-string v9, "EFFECT_11"

    invoke-direct {p0, p1, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->updateCheckboxPreference(Landroid/preference/Preference;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 1747
    :cond_2c
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v9

    const-string v10, "city_select_animation"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 1748
    const-string v9, "EFFECT_12"

    invoke-direct {p0, p1, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->updateCheckboxPreference(Landroid/preference/Preference;Ljava/lang/String;)V

    goto/16 :goto_3
.end method

.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 12
    .param p1, "preference"    # Landroid/preference/Preference;

    .prologue
    const/4 v11, 0x0

    .line 1788
    const-string v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onPreferenceClick : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1789
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string v1, "setnotificationtime"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1790
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v7

    .line 1791
    .local v7, "calendar":Ljava/util/Calendar;
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->isActivityVisible()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1871
    .end local v7    # "calendar":Ljava/util/Calendar;
    :cond_0
    :goto_0
    return v11

    .line 1793
    .restart local v7    # "calendar":Ljava/util/Calendar;
    :cond_1
    invoke-virtual {p0, v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->setActivityVisibleState(Z)V

    .line 1794
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getNotificationTimeSettings(Landroid/content/Context;)Ljava/lang/Long;

    move-result-object v10

    .line 1796
    .local v10, "time":Ljava/lang/Long;
    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_2

    .line 1797
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-virtual {v7, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1802
    :goto_1
    new-instance v0, Landroid/app/TimePickerDialog;

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->timeListener:Landroid/app/TimePickerDialog$OnTimeSetListener;

    const/16 v1, 0xb

    .line 1803
    invoke-virtual {v7, v1}, Ljava/util/Calendar;->get(I)I

    move-result v3

    const/16 v1, 0xc

    invoke-virtual {v7, v1}, Ljava/util/Calendar;->get(I)I

    move-result v4

    .line 1804
    invoke-static {p0}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v5

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Landroid/app/TimePickerDialog;-><init>(Landroid/content/Context;Landroid/app/TimePickerDialog$OnTimeSetListener;IIZ)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->timePickerDialog:Landroid/app/TimePickerDialog;

    .line 1805
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->timePickerDialog:Landroid/app/TimePickerDialog;

    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$14;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$14;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;)V

    invoke-virtual {v0, v1}, Landroid/app/TimePickerDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 1810
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->timePickerDialog:Landroid/app/TimePickerDialog;

    invoke-virtual {v0}, Landroid/app/TimePickerDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1811
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->timePickerDialog:Landroid/app/TimePickerDialog;

    invoke-virtual {v0}, Landroid/app/TimePickerDialog;->show()V

    goto :goto_0

    .line 1799
    :cond_2
    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {v7, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    goto :goto_1

    .line 1817
    .end local v7    # "calendar":Ljava/util/Calendar;
    .end local v10    # "time":Ljava/lang/Long;
    :cond_3
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string v1, "duration_time"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1818
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->isActivityVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1821
    invoke-virtual {p0, v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->setActivityVisibleState(Z)V

    .line 1823
    new-instance v6, Landroid/app/AlertDialog$Builder;

    invoke-direct {v6, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1824
    .local v6, "builder":Landroid/app/AlertDialog$Builder;
    const v0, 0x7f0d00fe

    invoke-virtual {v6, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 1826
    new-instance v8, Landroid/widget/EditText;

    invoke-direct {v8, p0}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 1827
    .local v8, "input":Landroid/widget/EditText;
    const/4 v0, 0x2

    invoke-virtual {v8, v0}, Landroid/widget/EditText;->setInputType(I)V

    .line 1828
    invoke-virtual {v6, v8}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 1830
    const-string v0, "OK"

    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$15;

    invoke-direct {v1, p0, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$15;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;Landroid/widget/EditText;)V

    invoke-virtual {v6, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1847
    const-string v0, "Cancel"

    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$16;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$16;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;)V

    invoke-virtual {v6, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1854
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$17;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$17;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;)V

    invoke-virtual {v6, v0}, Landroid/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    .line 1861
    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    .line 1862
    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto/16 :goto_0

    .line 1863
    .end local v6    # "builder":Landroid/app/AlertDialog$Builder;
    .end local v8    # "input":Landroid/widget/EditText;
    :cond_4
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string v1, "widgetBGTransparencyControl"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1864
    new-instance v9, Landroid/content/Intent;

    const-class v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;

    invoke-direct {v9, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1865
    .local v9, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 1866
    .end local v9    # "intent":Landroid/content/Intent;
    :cond_5
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string v1, "setcurrentcity"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1867
    new-instance v9, Landroid/content/Intent;

    const-class v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;

    invoke-direct {v9, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1868
    .restart local v9    # "intent":Landroid/content/Intent;
    invoke-virtual {p0, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 391
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onResume()V

    .line 393
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->creating:Z

    if-nez v0, :cond_0

    .line 394
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->init()V

    .line 396
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->creating:Z

    .line 397
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->setActivityVisibleState(Z)V

    .line 398
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->setContext(Landroid/content/Context;)V

    .line 400
    return-void
.end method

.method public setActivityVisibleState(Z)V
    .locals 0
    .param p1, "onoff"    # Z

    .prologue
    .line 474
    iput-boolean p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mIsVisible:Z

    .line 475
    return-void
.end method

.method public setButtenTypeCheck(I)V
    .locals 0
    .param p1, "Check"    # I

    .prologue
    .line 1965
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->CheckType:I

    .line 1966
    return-void
.end method

.method public showLocatingDialog()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1090
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 1092
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mLoadingDialog:Landroid/app/Dialog;

    if-nez v0, :cond_0

    .line 1093
    const/4 v0, 0x0

    const v1, 0x7f0d0038

    .line 1094
    invoke-virtual {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1093
    invoke-static {p0, v0, v1, v2, v2}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZ)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mLoadingDialog:Landroid/app/Dialog;

    .line 1096
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mLoadingDialog:Landroid/app/Dialog;

    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$3;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$3;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 1109
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mLoadingDialog:Landroid/app/Dialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 1110
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mLoadingDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 1111
    return-void
.end method
