.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo;
.super Ljava/lang/Object;
.source "UpdateCheckInfo.java"


# instance fields
.field private ctx:Landroid/content/Context;

.field httpClient:Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;

.field private mHttpThread:Ljava/lang/Thread;

.field public mPopupDialog:Landroid/app/Dialog;

.field private response:Ljava/lang/String;

.field private th:Ljava/lang/Thread;

.field private urlManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "ctx"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo;->mHttpThread:Ljava/lang/Thread;

    .line 48
    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo;->httpClient:Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;

    .line 52
    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo;->response:Ljava/lang/String;

    .line 54
    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo;->mPopupDialog:Landroid/app/Dialog;

    .line 61
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo;->ctx:Landroid/content/Context;

    .line 62
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;

    invoke-direct {v0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo;->urlManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;

    .line 63
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;

    invoke-direct {v0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo;->httpClient:Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;

    .line 64
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo;)Ljava/lang/Thread;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo;->mHttpThread:Ljava/lang/Thread;

    return-object v0
.end method

.method static synthetic access$002(Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo;Ljava/lang/Thread;)Ljava/lang/Thread;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo;
    .param p1, "x1"    # Ljava/lang/Thread;

    .prologue
    .line 41
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo;->mHttpThread:Ljava/lang/Thread;

    return-object p1
.end method

.method static synthetic access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo;->ctx:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo;->response:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 41
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo;->response:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$300(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;,
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 41
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo;->parseUpdateVersionInfo(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getCSC()Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 367
    const-string v0, ""

    .line 368
    .local v0, "cscVersion":Ljava/lang/String;
    const/4 v1, 0x0

    .line 371
    .local v1, "value":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo;->isCSCExistFile()Z

    move-result v2

    if-eq v2, v3, :cond_0

    .line 390
    :goto_0
    return-object v0

    .line 375
    :cond_0
    invoke-static {}, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo;->getCSCVersion()Ljava/lang/String;

    move-result-object v1

    .line 376
    if-nez v1, :cond_1

    .line 377
    const-string v2, ""

    const-string v3, "getCSC::getCSCVersion::value is null"

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 381
    :cond_1
    const-string v2, "FAIL"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-ne v2, v3, :cond_2

    .line 382
    const-string v2, ""

    const-string v3, "getCSC::getCSCVersion::Fail to read CSC Version"

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 386
    :cond_2
    const/4 v2, 0x0

    const/4 v3, 0x3

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static getCSCVersion()Ljava/lang/String;
    .locals 8

    .prologue
    .line 398
    const/4 v5, 0x0

    .line 399
    .local v5, "s":Ljava/lang/String;
    new-instance v4, Ljava/io/File;

    const-string v7, "/system/csc/sales_code.dat"

    invoke-direct {v4, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 400
    .local v4, "mFile":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->isFile()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 401
    const/16 v7, 0x14

    new-array v0, v7, [B

    .line 403
    .local v0, "buffer":[B
    const/4 v2, 0x0

    .line 406
    .local v2, "in":Ljava/io/InputStream;
    :try_start_0
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, v4}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 408
    .end local v2    # "in":Ljava/io/InputStream;
    .local v3, "in":Ljava/io/InputStream;
    :try_start_1
    invoke-virtual {v3, v0}, Ljava/io/InputStream;->read([B)I

    move-result v7

    if-eqz v7, :cond_1

    .line 409
    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v0}, Ljava/lang/String;-><init>([B)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .end local v5    # "s":Ljava/lang/String;
    .local v6, "s":Ljava/lang/String;
    move-object v5, v6

    .line 420
    .end local v6    # "s":Ljava/lang/String;
    .restart local v5    # "s":Ljava/lang/String;
    :goto_0
    if-eqz v3, :cond_0

    .line 422
    :try_start_2
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 426
    :goto_1
    const/4 v2, 0x0

    .line 432
    .end local v0    # "buffer":[B
    .end local v3    # "in":Ljava/io/InputStream;
    :cond_0
    :goto_2
    return-object v5

    .line 411
    .restart local v0    # "buffer":[B
    .restart local v3    # "in":Ljava/io/InputStream;
    :cond_1
    :try_start_3
    new-instance v6, Ljava/lang/String;

    const-string v7, "FAIL"

    invoke-direct {v6, v7}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_7
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_6
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .end local v5    # "s":Ljava/lang/String;
    .restart local v6    # "s":Ljava/lang/String;
    move-object v5, v6

    .end local v6    # "s":Ljava/lang/String;
    .restart local v5    # "s":Ljava/lang/String;
    goto :goto_0

    .line 423
    :catch_0
    move-exception v1

    .line 424
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 414
    .end local v1    # "e":Ljava/io/IOException;
    .end local v3    # "in":Ljava/io/InputStream;
    .restart local v2    # "in":Ljava/io/InputStream;
    :catch_1
    move-exception v1

    .line 415
    .local v1, "e":Ljava/io/FileNotFoundException;
    :goto_3
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 420
    if-eqz v2, :cond_0

    .line 422
    :try_start_5
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    .line 426
    .end local v1    # "e":Ljava/io/FileNotFoundException;
    :goto_4
    const/4 v2, 0x0

    goto :goto_2

    .line 423
    .restart local v1    # "e":Ljava/io/FileNotFoundException;
    :catch_2
    move-exception v1

    .line 424
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 416
    .end local v1    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v1

    .line 417
    .restart local v1    # "e":Ljava/io/IOException;
    :goto_5
    :try_start_6
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 420
    if-eqz v2, :cond_0

    .line 422
    :try_start_7
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    .line 426
    :goto_6
    const/4 v2, 0x0

    goto :goto_2

    .line 423
    :catch_4
    move-exception v1

    .line 424
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 420
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v7

    :goto_7
    if-eqz v2, :cond_2

    .line 422
    :try_start_8
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    .line 426
    :goto_8
    const/4 v2, 0x0

    :cond_2
    throw v7

    .line 423
    :catch_5
    move-exception v1

    .line 424
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_8

    .line 420
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "in":Ljava/io/InputStream;
    .restart local v3    # "in":Ljava/io/InputStream;
    :catchall_1
    move-exception v7

    move-object v2, v3

    .end local v3    # "in":Ljava/io/InputStream;
    .restart local v2    # "in":Ljava/io/InputStream;
    goto :goto_7

    .line 416
    .end local v2    # "in":Ljava/io/InputStream;
    .restart local v3    # "in":Ljava/io/InputStream;
    :catch_6
    move-exception v1

    move-object v2, v3

    .end local v3    # "in":Ljava/io/InputStream;
    .restart local v2    # "in":Ljava/io/InputStream;
    goto :goto_5

    .line 414
    .end local v2    # "in":Ljava/io/InputStream;
    .restart local v3    # "in":Ljava/io/InputStream;
    :catch_7
    move-exception v1

    move-object v2, v3

    .end local v3    # "in":Ljava/io/InputStream;
    .restart local v2    # "in":Ljava/io/InputStream;
    goto :goto_3
.end method

.method private getContent(Ljava/lang/String;Landroid/os/Handler;)V
    .locals 8
    .param p1, "server_url"    # Ljava/lang/String;
    .param p2, "handler"    # Landroid/os/Handler;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/MalformedURLException;
        }
    .end annotation

    .prologue
    .line 137
    new-instance v0, Ljava/net/URL;

    const-string v1, "http"

    const-string v2, "vas.samsungapps.com"

    const/4 v3, -0x1

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p1, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Ljava/net/URL;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/net/URLStreamHandler;)V

    .line 139
    .local v0, "url":Ljava/net/URL;
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo;->urlManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;->makeHeader()Lorg/apache/http/message/HeaderGroup;

    move-result-object v6

    .line 141
    .local v6, "headerGroup":Lorg/apache/http/message/HeaderGroup;
    new-instance v7, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo$2;

    invoke-direct {v7, p0, v0, v6, p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo$2;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo;Ljava/net/URL;Lorg/apache/http/message/HeaderGroup;Landroid/os/Handler;)V

    .line 184
    .local v7, "t":Ljava/lang/Thread;
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo;->th:Ljava/lang/Thread;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo;->th:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->isAlive()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 185
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo;->th:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    .line 188
    :cond_0
    iput-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo;->th:Ljava/lang/Thread;

    .line 189
    const/4 v1, 0x1

    invoke-virtual {v7, v1}, Ljava/lang/Thread;->setDaemon(Z)V

    .line 190
    invoke-virtual {v7}, Ljava/lang/Thread;->start()V

    .line 191
    return-void
.end method

.method private getMCC()Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x0

    .line 309
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo;->ctx:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "phone"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/telephony/TelephonyManager;

    .line 311
    .local v3, "telMgr":Landroid/telephony/TelephonyManager;
    const-string v0, ""

    .line 313
    .local v0, "mcc":Ljava/lang/String;
    if-nez v3, :cond_0

    move-object v1, v0

    .line 337
    .end local v0    # "mcc":Ljava/lang/String;
    .local v1, "mcc":Ljava/lang/String;
    :goto_0
    return-object v1

    .line 317
    .end local v1    # "mcc":Ljava/lang/String;
    .restart local v0    # "mcc":Ljava/lang/String;
    :cond_0
    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v2

    .line 319
    .local v2, "networkOperator":Ljava/lang/String;
    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 329
    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v2

    .line 330
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_2

    .line 331
    invoke-virtual {v2, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    :goto_1
    move-object v1, v0

    .line 337
    .end local v0    # "mcc":Ljava/lang/String;
    .restart local v1    # "mcc":Ljava/lang/String;
    goto :goto_0

    .line 321
    .end local v1    # "mcc":Ljava/lang/String;
    .restart local v0    # "mcc":Ljava/lang/String;
    :pswitch_0
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_1

    .line 322
    invoke-virtual {v2, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 323
    goto :goto_1

    .line 325
    :cond_1
    const-string v0, ""

    .line 326
    goto :goto_1

    .line 333
    :cond_2
    const-string v0, ""

    goto :goto_1

    .line 319
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method private getMNC()Ljava/lang/String;
    .locals 5

    .prologue
    .line 345
    const-string v0, "00"

    .line 347
    .local v0, "mnc":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo;->ctx:Landroid/content/Context;

    const-string v4, "phone"

    .line 348
    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telephony/TelephonyManager;

    .line 350
    .local v2, "telMgr":Landroid/telephony/TelephonyManager;
    if-nez v2, :cond_1

    .line 359
    .end local v0    # "mnc":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v0

    .line 355
    .restart local v0    # "mnc":Ljava/lang/String;
    :cond_1
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v1

    .line 356
    .local v1, "networkOperator":Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    .line 357
    const/4 v3, 0x3

    invoke-virtual {v1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static getPD()Ljava/lang/String;
    .locals 2

    .prologue
    .line 479
    const-string v0, ""

    .line 481
    .local v0, "retStr":Ljava/lang/String;
    sget-boolean v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/Define;->IS_TEST_PURPOSEED:Z

    if-eqz v1, :cond_0

    .line 482
    const-string v0, "1"

    .line 485
    :cond_0
    return-object v0
.end method

.method private static getResultUpdateCheck(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p0, "packageName"    # Ljava/lang/String;
    .param p1, "code"    # Ljava/lang/String;

    .prologue
    .line 299
    const-string v0, "com.sec.android.widgetapp.ap.hero.accuweather"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "2"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 302
    :cond_0
    return-void
.end method

.method private static isCSCExistFile()Z
    .locals 6

    .prologue
    .line 440
    const/4 v2, 0x0

    .line 442
    .local v2, "result":Z
    new-instance v1, Ljava/io/File;

    const-string v3, "/system/csc/sales_code.dat"

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 445
    .local v1, "file":Ljava/io/File;
    :try_start_0
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    .line 446
    const/4 v3, 0x1

    if-eq v2, v3, :cond_0

    .line 447
    const-string v3, ""

    const-string v4, "CSC is not exist"

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 453
    :cond_0
    :goto_0
    return v2

    .line 449
    :catch_0
    move-exception v0

    .line 450
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, ""

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "isCSCExistFile::"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private static parseUpdateVersionInfo(Ljava/lang/String;)Ljava/lang/String;
    .locals 11
    .param p0, "xml"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;,
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v10, 0x4

    .line 216
    if-nez p0, :cond_1

    .line 288
    :cond_0
    :goto_0
    return-object v5

    .line 220
    :cond_1
    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-virtual {p0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v8

    const-string v9, "UTF-8"

    invoke-virtual {v8, v9}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v8

    invoke-direct {v1, v8}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 222
    .local v1, "is":Ljava/io/InputStream;
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v3

    .line 223
    .local v3, "parserCreator":Lorg/xmlpull/v1/XmlPullParserFactory;
    invoke-virtual {v3}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v2

    .line 224
    .local v2, "parser":Lorg/xmlpull/v1/XmlPullParser;
    invoke-interface {v2, v1, v5}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 226
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v4

    .line 230
    .local v4, "parserEvent":I
    const-string v0, ""

    .local v0, "id":Ljava/lang/String;
    const-string v5, ""

    .line 232
    .local v5, "result":Ljava/lang/String;
    :goto_1
    const/4 v8, 0x1

    if-eq v4, v8, :cond_5

    .line 234
    const/4 v8, 0x2

    if-ne v4, v8, :cond_3

    .line 236
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v6

    .line 238
    .local v6, "tag":Ljava/lang/String;
    const-string v8, "appId"

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 240
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v7

    .line 242
    .local v7, "type":I
    if-ne v7, v10, :cond_2

    .line 244
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v0

    .line 250
    .end local v7    # "type":I
    :cond_2
    const-string v8, "resultCode"

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 252
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v7

    .line 254
    .restart local v7    # "type":I
    if-ne v7, v10, :cond_3

    .line 256
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v5

    .line 264
    .end local v6    # "tag":Ljava/lang/String;
    .end local v7    # "type":I
    :cond_3
    const/4 v8, 0x3

    if-ne v4, v8, :cond_4

    .line 266
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v6

    .line 268
    .restart local v6    # "tag":Ljava/lang/String;
    const-string v8, "appInfo"

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 270
    invoke-static {v0, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo;->getResultUpdateCheck(Ljava/lang/String;Ljava/lang/String;)V

    .line 272
    const-string v0, ""

    .line 280
    .end local v6    # "tag":Ljava/lang/String;
    :cond_4
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v4

    goto :goto_1

    .line 284
    :cond_5
    if-eqz v1, :cond_0

    .line 285
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    goto :goto_0
.end method

.method private static replaceModelName(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "src"    # Ljava/lang/String;

    .prologue
    .line 494
    const-string v0, ""

    .line 495
    .local v0, "des":Ljava/lang/String;
    if-eqz p0, :cond_0

    .line 496
    invoke-virtual {p0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, "SAMSUNG-"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 499
    :cond_0
    return-object v0
.end method

.method public static startSamsungApps(Landroid/content/Context;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x1

    .line 545
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 546
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "com.sec.android.app.samsungapps"

    const-string v3, "com.sec.android.app.samsungapps.Main"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 548
    const-string v2, "directcall"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 549
    const-string v2, "CallerType"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 550
    const-string v2, "GUID"

    const-string v3, "com.sec.android.widgetapp.ap.hero.accuweather"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 555
    const/high16 v2, 0x14000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 558
    :try_start_0
    invoke-virtual {p0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 563
    :goto_0
    return-void

    .line 559
    :catch_0
    move-exception v0

    .line 560
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public responseUpdateCheckVersion()V
    .locals 9

    .prologue
    .line 76
    const-string v4, "stub/stubUpdateCheck.as?"

    .line 77
    .local v4, "serverUrl":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "appId=com.sec.android.widgetapp.ap.hero.accuweather&versionCode="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 80
    :try_start_0
    sget-boolean v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/Define;->IS_TEST_PURPOSEED:Z

    if-eqz v5, :cond_0

    .line 81
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "1"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 91
    :goto_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "&deviceId="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-static {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo;->replaceModelName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 93
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "&mcc="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo;->getMCC()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 95
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "&mnc="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo;->getMNC()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 97
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "&csc="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo;->getCSC()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 99
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "&sdkVer="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget v6, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 101
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "&pd="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo;->getPD()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 106
    const-string v5, "&"

    invoke-virtual {v4, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    .line 107
    .local v3, "idx":I
    const/4 v5, -0x1

    if-le v3, v5, :cond_1

    .line 108
    invoke-virtual {v4, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 109
    .local v2, "forLogStr":Ljava/lang/String;
    const/4 v5, 0x1

    const-string v6, ""

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "[rUCV] "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(ZLjava/lang/String;Ljava/lang/String;)I

    .line 115
    .end local v2    # "forLogStr":Ljava/lang/String;
    :goto_1
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo$1;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo$1;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo;)V

    .line 124
    .local v0, "dialogHandler":Landroid/os/Handler;
    :try_start_1
    invoke-direct {p0, v4, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo;->getContent(Ljava/lang/String;Landroid/os/Handler;)V
    :try_end_1
    .catch Ljava/net/MalformedURLException; {:try_start_1 .. :try_end_1} :catch_1

    .line 128
    :goto_2
    return-void

    .line 83
    .end local v0    # "dialogHandler":Landroid/os/Handler;
    .end local v3    # "idx":I
    :cond_0
    :try_start_2
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo;->ctx:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    const-string v7, "com.sec.android.widgetapp.ap.hero.accuweather"

    const/16 v8, 0x80

    .line 84
    invoke-virtual {v6, v7, v8}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v6

    iget v6, v6, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v4

    goto/16 :goto_0

    .line 87
    :catch_0
    move-exception v1

    .line 88
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto/16 :goto_0

    .line 111
    .end local v1    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v3    # "idx":I
    :cond_1
    const-string v5, ""

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[rUCV] "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 125
    .restart local v0    # "dialogHandler":Landroid/os/Handler;
    :catch_1
    move-exception v1

    .line 126
    .local v1, "e":Ljava/net/MalformedURLException;
    invoke-virtual {v1}, Ljava/net/MalformedURLException;->printStackTrace()V

    goto :goto_2
.end method

.method public startUserDialog()V
    .locals 3

    .prologue
    .line 506
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo;->ctx:Landroid/content/Context;

    const/16 v1, 0x421

    new-instance v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo$3;

    invoke-direct {v2, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo$3;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo;)V

    invoke-static {v0, v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->showDialog(Landroid/content/Context;ILcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;)Landroid/app/Dialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo;->mPopupDialog:Landroid/app/Dialog;

    .line 522
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo;->mPopupDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    .line 523
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo;->mPopupDialog:Landroid/app/Dialog;

    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo$4;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo$4;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 538
    :cond_0
    return-void
.end method

.method public stopHttpThread()V
    .locals 1

    .prologue
    .line 197
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo;->mHttpThread:Ljava/lang/Thread;

    if-eqz v0, :cond_1

    .line 198
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo;->mHttpThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 199
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo;->mHttpThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 201
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo;->mHttpThread:Ljava/lang/Thread;

    .line 203
    :cond_1
    return-void
.end method
