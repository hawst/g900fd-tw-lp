.class Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$8;
.super Ljava/lang/Object;
.source "MyCurrentLocation.java"

# interfaces
.implements Landroid/location/LocationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    .prologue
    .line 702
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$8;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLocationChanged(Landroid/location/Location;)V
    .locals 2
    .param p1, "location"    # Landroid/location/Location;

    .prologue
    .line 705
    const-string v0, ""

    const-string v1, "-- MCL -- oLC ____Passive"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 706
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$8;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->updateWithNewLocation(Landroid/location/Location;)V
    invoke-static {v0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->access$600(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;Landroid/location/Location;)V

    .line 707
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$8;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->currentLocationHandler:Landroid/os/Handler;

    const/16 v1, 0xca

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 709
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$8;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->passiveLocationManager:Landroid/location/LocationManager;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->access$700(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;)Landroid/location/LocationManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 710
    const-string v0, ""

    const-string v1, "mcl rm___Passive "

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 711
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$8;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->passiveLocationManager:Landroid/location/LocationManager;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->access$700(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;)Landroid/location/LocationManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$8;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->passiveLocationListener:Landroid/location/LocationListener;
    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->access$800(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;)Landroid/location/LocationListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 713
    :cond_0
    return-void
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .locals 5
    .param p1, "provider"    # Ljava/lang/String;

    .prologue
    .line 716
    const-string v0, ""

    const-string v1, "-- DEBUG GPS MCL -- oPvD : %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 717
    invoke-static {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertProvider(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 716
    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 718
    return-void
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .locals 5
    .param p1, "provider"    # Ljava/lang/String;

    .prologue
    .line 721
    const-string v0, ""

    const-string v1, "-- DEBUG GPS MCL -- oPvE : %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 722
    invoke-static {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertProvider(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 721
    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 723
    return-void
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 5
    .param p1, "provider"    # Ljava/lang/String;
    .param p2, "status"    # I
    .param p3, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 726
    const-string v0, ""

    const-string v1, "-- DEBUG GPS MCL -- oSC : %s / %d / %s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 728
    invoke-static {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertProvider(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-virtual {p3}, Landroid/os/Bundle;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 727
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 726
    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 729
    return-void
.end method
