.class Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather$2;
.super Ljava/lang/Object;
.source "SurfaceWidgetWeather.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;

    .prologue
    .line 127
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather$2;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 129
    const-string v1, ""

    const-string v2, "REFRESH : start manual refresh"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 131
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather$2;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;
    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->access$700(Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;)Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-result-object v1

    if-nez v1, :cond_0

    .line 132
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather$2;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;

    new-instance v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather$2;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;-><init>(Landroid/content/Context;)V

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;
    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->access$702(Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    .line 134
    :cond_0
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather$2;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->startRefreshIcon()V
    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->access$800(Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;)V

    .line 135
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather$2;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;
    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->access$700(Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;)Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather$2;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mRefreshUIHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->access$900(Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;)Landroid/os/Handler;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->doManualRefresh(ILandroid/os/Handler;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 139
    :goto_0
    return-void

    .line 136
    :catch_0
    move-exception v0

    .line 137
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
