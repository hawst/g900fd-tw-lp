.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;
.super Landroid/view/View;
.source "FogIconView.java"

# interfaces
.implements Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;


# instance fields
.field private TranslateAni:Landroid/animation/ValueAnimator;

.field private TranslateAni2:Landroid/animation/ValueAnimator;

.field private UnderLineAni1:Landroid/animation/ValueAnimator;

.field private UnderLineAni2:Landroid/animation/ValueAnimator;

.field private UnderLineAni3:Landroid/animation/ValueAnimator;

.field private UnderLineAni4:Landroid/animation/ValueAnimator;

.field a1:I

.field a2:I

.field a3:I

.field a4:I

.field cloudyX:F

.field dx:F

.field isStop:Z

.field l:Landroid/animation/Animator$AnimatorListener;

.field private mCloudPaint1:Landroid/graphics/Paint;

.field private mCloudPath_left:Landroid/graphics/Path;

.field private mCloudPath_right:Landroid/graphics/Path;

.field private mCloudPath_top:Landroid/graphics/Path;

.field private mIsActiveAnimationThread:Z

.field private mPaintColor:I

.field private mScale:F

.field private mUnderLine1:Landroid/graphics/Path;

.field private mUnderLine2:Landroid/graphics/Path;

.field private mUnderLine3:Landroid/graphics/Path;

.field private mUnderLine4:Landroid/graphics/Path;

.field s1:F

.field s2:F

.field s3:F

.field t1:F

.field t2:F

.field t3:F

.field t4:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 25
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attributeset"    # Landroid/util/AttributeSet;

    .prologue
    .line 29
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attributeset"    # Landroid/util/AttributeSet;
    .param p3, "i"    # I

    .prologue
    const/4 v4, 0x0

    const/16 v3, 0xff

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 33
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 39
    iput v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    .line 55
    iput-boolean v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mIsActiveAnimationThread:Z

    .line 57
    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    .line 59
    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->TranslateAni2:Landroid/animation/ValueAnimator;

    .line 61
    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->UnderLineAni1:Landroid/animation/ValueAnimator;

    .line 63
    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->UnderLineAni2:Landroid/animation/ValueAnimator;

    .line 65
    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->UnderLineAni3:Landroid/animation/ValueAnimator;

    .line 67
    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->UnderLineAni4:Landroid/animation/ValueAnimator;

    .line 69
    const v0, -0xa0a0b

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mPaintColor:I

    .line 241
    iput-boolean v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->isStop:Z

    .line 246
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView$1;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->l:Landroid/animation/Animator$AnimatorListener;

    .line 428
    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->dx:F

    .line 429
    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->cloudyX:F

    .line 431
    iput v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->a1:I

    .line 432
    iput v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->a2:I

    .line 433
    iput v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->a3:I

    .line 434
    iput v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->a4:I

    .line 436
    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->t1:F

    .line 437
    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->t2:F

    .line 438
    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->t3:F

    .line 439
    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->t4:F

    .line 441
    iput v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->s1:F

    .line 442
    iput v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->s2:F

    .line 443
    iput v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->s3:F

    .line 34
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->init()V

    .line 35
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;)Landroid/animation/ValueAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->UnderLineAni1:Landroid/animation/ValueAnimator;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;)Landroid/animation/ValueAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->UnderLineAni2:Landroid/animation/ValueAnimator;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;)Landroid/animation/ValueAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->UnderLineAni3:Landroid/animation/ValueAnimator;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;)Landroid/animation/ValueAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->UnderLineAni4:Landroid/animation/ValueAnimator;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;)Landroid/animation/ValueAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;)Landroid/animation/ValueAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->TranslateAni2:Landroid/animation/ValueAnimator;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;

    .prologue
    .line 23
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mIsActiveAnimationThread:Z

    return v0
.end method

.method private init()V
    .locals 0

    .prologue
    .line 76
    invoke-virtual {p0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 77
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->initPaint()V

    .line 78
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->initPath()V

    .line 79
    return-void
.end method

.method private initPaint()V
    .locals 3

    .prologue
    .line 235
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mCloudPaint1:Landroid/graphics/Paint;

    .line 236
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mCloudPaint1:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 237
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mCloudPaint1:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 238
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mCloudPaint1:Landroid/graphics/Paint;

    const/high16 v1, 0x41400000    # 12.0f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 239
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mCloudPaint1:Landroid/graphics/Paint;

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mPaintColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 240
    return-void
.end method

.method private initPath()V
    .locals 13

    .prologue
    const/high16 v12, 0x42340000    # 45.0f

    const/high16 v11, 0x432e0000    # 174.0f

    const/high16 v10, 0x43090000    # 137.0f

    const/high16 v9, 0x430d0000    # 141.0f

    const/high16 v8, 0x43050000    # 133.0f

    .line 82
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mCloudPath_left:Landroid/graphics/Path;

    .line 83
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mCloudPath_left:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 84
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mCloudPath_left:Landroid/graphics/Path;

    const v1, 0x428227f0

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42935f3b

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 85
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mCloudPath_left:Landroid/graphics/Path;

    const v1, 0x427dee98

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4292b646

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42776a7f    # 61.854f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42925f3b

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4270cccd    # 60.2f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42925f3b

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 87
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mCloudPath_left:Landroid/graphics/Path;

    const v1, 0x423be76d

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42925f3b

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4210e042

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42a7e354    # 83.944f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4210e042

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42c25581    # 97.167f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 89
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mCloudPath_left:Landroid/graphics/Path;

    const v1, 0x4210e042

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42c2cdd3    # 97.402f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4210df3b

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42c33852    # 97.61f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4210e560    # 36.224f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42c3b0a4

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 91
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mCloudPath_left:Landroid/graphics/Path;

    const v1, 0x41ddc49c    # 27.721f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42c720c5

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v2, v3

    const/high16 v3, 0x41a80000    # 21.0f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42d7f021

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v4, v5

    const/high16 v5, 0x41a80000    # 21.0f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42eac106

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 93
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mCloudPath_left:Landroid/graphics/Path;

    const/high16 v1, 0x41a80000    # 21.0f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42f2c000    # 121.375f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 94
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mCloudPath_left:Landroid/graphics/Path;

    const v1, 0x42ff66e9

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v1, v2

    const/high16 v2, 0x42f20000    # 121.0f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 95
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mCloudPath_left:Landroid/graphics/Path;

    const v1, 0x4301e8b4

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v1, v2

    const/high16 v2, 0x42f20000    # 121.0f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4303b333    # 131.7f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42eed3f8

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4303b375

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42ea68f6

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 97
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mCloudPath_left:Landroid/graphics/Path;

    const v1, 0x4303b3b6

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42e5fdf4

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4301e937

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42e29d2f    # 113.307f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42ff676d

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42e29d2f    # 113.307f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 99
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mCloudPath_left:Landroid/graphics/Path;

    const v1, 0x41ec4dd3    # 29.538f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42e2a7f0

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 100
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mCloudPath_left:Landroid/graphics/Path;

    const v1, 0x41f8df3b    # 31.109f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42d9cfdf

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x420c3b64

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42d2d78d

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x421d71aa    # 39.361f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42d2d78d

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 102
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mCloudPath_left:Landroid/graphics/Path;

    const v1, 0x421f5c29    # 39.84f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42d2d78d

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4220b22d    # 40.174f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42d2d47b    # 105.415f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4223872b    # 40.882f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42d2e979

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 104
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mCloudPath_left:Landroid/graphics/Path;

    const v1, 0x42375604    # 45.834f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42d422d1    # 106.068f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 105
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mCloudPath_left:Landroid/graphics/Path;

    const v1, 0x42327ae1    # 44.62f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42c9753f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 106
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mCloudPath_left:Landroid/graphics/Path;

    const v1, 0x4231676d

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42c71a1d

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4230dc29    # 44.215f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42c55062

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4230de35    # 44.217f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42c25581    # 97.167f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 108
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mCloudPath_left:Landroid/graphics/Path;

    const v1, 0x4230ea7f    # 44.229f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42b0b53f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x424d8b44

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42a25f3b

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4270cac1

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42a25f3b

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 110
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mCloudPath_left:Landroid/graphics/Path;

    const/high16 v1, 0x42750000    # 61.25f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42a25f3b

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x427923d7    # 62.285f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42a294fe    # 81.291f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x427d3333    # 63.3f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42a2fb64

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 112
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mCloudPath_left:Landroid/graphics/Path;

    const v1, 0x427e8000    # 63.625f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42a31062

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4288e042

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42a535c3

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4289cac1

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x429c8b44

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 114
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mCloudPath_left:Landroid/graphics/Path;

    const v1, 0x428aa042

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4294a5e3

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x428227f0

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42935f3b

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x428227f0

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42935f3b

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 117
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mCloudPath_right:Landroid/graphics/Path;

    .line 118
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mCloudPath_right:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 119
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mCloudPath_right:Landroid/graphics/Path;

    const v1, 0x43263646

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x428a8bc7

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 120
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mCloudPath_right:Landroid/graphics/Path;

    const v1, 0x431b54fe    # 155.332f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x428a8bc7

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x430ec7f0

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42931e35

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x430650e5

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42adc937

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 122
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mCloudPath_right:Landroid/graphics/Path;

    const v1, 0x430521cb

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42b18419

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4305af5c

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42b69604    # 91.293f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x43078c8b

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42b8f439

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 124
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mCloudPath_right:Landroid/graphics/Path;

    const v1, 0x430836c9

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42b9cc4a

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4308f47b

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42ba4419

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4309b062

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42ba4419

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 126
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mCloudPath_right:Landroid/graphics/Path;

    const v1, 0x430b0354    # 139.013f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42ba4419

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x430c4ed9

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42b8fbe7

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x430d126f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42b694fe    # 91.291f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 128
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mCloudPath_right:Landroid/graphics/Path;

    const v1, 0x43113e77

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42a96e98

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x431851ec    # 152.32f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x429a8dd3    # 77.277f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4326a354    # 166.638f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x429a8dd3    # 77.277f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 130
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mCloudPath_right:Landroid/graphics/Path;

    const v1, 0x4333d1ec    # 179.82f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x429a8dd3    # 77.277f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x433eee56    # 190.931f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42b10189

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x433eee56    # 190.931f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42cbfae1    # 101.99f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 132
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mCloudPath_right:Landroid/graphics/Path;

    const v1, 0x433eee56    # 190.931f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42d54bc7

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x433da3d7    # 189.64f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42e01917

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x433bb7cf

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42e6dc29    # 115.43f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 134
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mCloudPath_right:Landroid/graphics/Path;

    const v1, 0x433aa042

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42eab333    # 117.35f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x433b4c4a

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42ef94fe    # 119.791f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x433d3852    # 189.22f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42f1c419

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 136
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mCloudPath_right:Landroid/graphics/Path;

    const v1, 0x433dd7cf

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42f279db

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x433e85e3

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42f2d062

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x433f3168

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42f2d062

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 138
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mCloudPath_right:Landroid/graphics/Path;

    const v1, 0x43409581    # 192.584f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42f2d062

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4341efdf

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42f15ba6    # 120.679f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4342ac4a

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42eec49c

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 140
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mCloudPath_right:Landroid/graphics/Path;

    const v1, 0x43454c8b

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42e58831

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4346ee56    # 198.931f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42d833b6

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4346ee56    # 198.931f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42cbfc6a

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 142
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mCloudPath_right:Landroid/graphics/Path;

    const v1, 0x4346ee98

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42a7e76d

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x433840c5

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x428a8bc7

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x43263646

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x428a8bc7

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 145
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mCloudPath_top:Landroid/graphics/Path;

    .line 146
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mCloudPath_top:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 147
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mCloudPath_top:Landroid/graphics/Path;

    const v1, 0x4322d810

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x428ab4bc

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 148
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mCloudPath_top:Landroid/graphics/Path;

    const v1, 0x431ac28f    # 154.76f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4248ad0e

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4307e354    # 135.888f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42160625    # 37.506f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42e5b333    # 114.85f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42160625    # 37.506f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 150
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mCloudPath_top:Landroid/graphics/Path;

    const v1, 0x42e40b44

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42160625    # 37.506f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42e26666    # 113.2f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42161fbe

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42e0c28f    # 112.38f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x421646a8    # 37.569f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 152
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mCloudPath_top:Landroid/graphics/Path;

    const v1, 0x42dfe666    # 111.95f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42165a1d

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42df0d50    # 111.526f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x421678d5    # 37.618f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42de3333    # 111.1f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42169581    # 37.646f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 154
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mCloudPath_top:Landroid/graphics/Path;

    const v1, 0x42dda76d

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4216a9fc    # 37.666f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42dd1b23

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4216bc6a

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42dc90e5

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4216d4fe    # 37.708f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 156
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mCloudPath_top:Landroid/graphics/Path;

    const v1, 0x42c1872b    # 96.764f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x421b3333    # 38.8f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42aa3f7d    # 85.124f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4232e24e    # 44.721f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42995375

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42584ed9    # 54.077f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 158
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mCloudPath_top:Landroid/graphics/Path;

    const v1, 0x428f1eb8    # 71.56f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x426e4fdf    # 59.578f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x428738d5    # 67.611f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42847646

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42827958    # 65.237f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42936e14

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 160
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mCloudPath_top:Landroid/graphics/Path;

    const v1, 0x42825eb8

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42936873

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x427d1ba6    # 63.277f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42a34419

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x427d1ba6    # 63.277f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42a34419

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 162
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mCloudPath_top:Landroid/graphics/Path;

    const v1, 0x4279cac1

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42abc49c

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4277fdf4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42b4b127    # 90.346f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4277fdf4

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42bdfd71

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 164
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mCloudPath_top:Landroid/graphics/Path;

    const v1, 0x4277fdf4

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42c26873

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x427f27f0

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42c5fd71

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4283fefa

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42c5fd71

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 166
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mCloudPath_top:Landroid/graphics/Path;

    const v1, 0x428869fc    # 68.207f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42c5fd71

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x428bfefa    # 69.998f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42c26873

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x428bfefa    # 69.998f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42bdfd71

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 168
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mCloudPath_top:Landroid/graphics/Path;

    const v1, 0x428bfefa    # 69.998f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x429bfbe7

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42994312

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x427e0937

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42aee4dd

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x425af0a4    # 54.735f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 170
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mCloudPath_top:Landroid/graphics/Path;

    const v1, 0x42bc020c

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42462f1b

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42cb6354    # 101.694f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v4, v12

    const v5, 0x42e5b127    # 114.846f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v5, v6

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v6, v12

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 172
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mCloudPath_top:Landroid/graphics/Path;

    const v1, 0x42e5b74c

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v2, v12

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 173
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mCloudPath_top:Landroid/graphics/Path;

    const v1, 0x4304170a    # 132.09f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v2, v12

    const v3, 0x4313b646

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x425e48b4    # 55.571f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x431ae560

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x428de042

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 175
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mCloudPath_top:Landroid/graphics/Path;

    const v1, 0x431ae560

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x428de042

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x431ce20c

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42940e56    # 74.028f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x431ff021

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42931581    # 73.542f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 177
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mCloudPath_top:Landroid/graphics/Path;

    const v1, 0x4323cc08

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4291daa0

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4322d810

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x428ab4bc

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4322d810

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x428ab4bc

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 180
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mUnderLine1:Landroid/graphics/Path;

    .line 181
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mUnderLine2:Landroid/graphics/Path;

    .line 182
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mUnderLine3:Landroid/graphics/Path;

    .line 183
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mUnderLine4:Landroid/graphics/Path;

    .line 185
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mUnderLine1:Landroid/graphics/Path;

    const v1, 0x429c9b23

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v2, v10

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 186
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mUnderLine1:Landroid/graphics/Path;

    const v1, 0x429c9b23

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4306ca7f    # 134.791f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42990625

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v4, v8

    const v5, 0x42949b23

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v5, v6

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v6, v8

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 188
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mUnderLine1:Landroid/graphics/Path;

    const v1, 0x41dfa9fc    # 27.958f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v2, v8

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 189
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mUnderLine1:Landroid/graphics/Path;

    const v1, 0x41cdfdf4    # 25.749f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v2, v8

    const v3, 0x41bfa9fc    # 23.958f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4306ca7f    # 134.791f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x41bfa9fc    # 23.958f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v5, v6

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v6, v10

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 191
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mUnderLine1:Landroid/graphics/Path;

    const v1, 0x41bfa9fc    # 23.958f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x430b3581    # 139.209f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x41cdfdf4    # 25.749f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v4, v9

    const v5, 0x41dfa9fc    # 27.958f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v5, v6

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v6, v9

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 193
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mUnderLine1:Landroid/graphics/Path;

    const v1, 0x42949b23

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v2, v9

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 194
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mUnderLine1:Landroid/graphics/Path;

    const v1, 0x42990625

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v2, v9

    const v3, 0x429c9b23

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x430b3581    # 139.209f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x429c9b23

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v5, v6

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v6, v10

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 197
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mUnderLine2:Landroid/graphics/Path;

    const v1, 0x4340a560

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v2, v8

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 198
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mUnderLine2:Landroid/graphics/Path;

    const v1, 0x42bfa354    # 95.819f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v2, v8

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 199
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mUnderLine2:Landroid/graphics/Path;

    const v1, 0x42bb3852    # 93.61f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v2, v8

    const v3, 0x42b7a354    # 91.819f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4306ca7f    # 134.791f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42b7a354    # 91.819f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v5, v6

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v6, v10

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 201
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mUnderLine2:Landroid/graphics/Path;

    const v1, 0x42b7a354    # 91.819f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x430b3581    # 139.209f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42bb3852    # 93.61f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v4, v9

    const v5, 0x42bfa354    # 95.819f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v5, v6

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v6, v9

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 203
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mUnderLine2:Landroid/graphics/Path;

    const v1, 0x4340a5a2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v2, v9

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 204
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mUnderLine2:Landroid/graphics/Path;

    const v1, 0x4342db23

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v2, v9

    const v3, 0x4344a5a2

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x430b3581    # 139.209f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4344a5a2

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v5, v6

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v6, v10

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 206
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mUnderLine2:Landroid/graphics/Path;

    const v1, 0x4344a5a2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4306ca7f    # 134.791f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4342dae1

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v4, v8

    const v5, 0x4340a560

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v5, v6

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v6, v8

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 209
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mUnderLine3:Landroid/graphics/Path;

    const v1, 0x4317f53f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v1, v2

    const/high16 v2, 0x431e0000    # 158.0f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 210
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mUnderLine3:Landroid/graphics/Path;

    const v1, 0x4317f53f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x431bca7f    # 155.791f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x43162ac1

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v3, v4

    const/high16 v4, 0x431a0000    # 154.0f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4313f53f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v5, v6

    const/high16 v6, 0x431a0000    # 154.0f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 212
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mUnderLine3:Landroid/graphics/Path;

    const v1, 0x42670106    # 57.751f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v1, v2

    const/high16 v2, 0x431a0000    # 154.0f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 213
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mUnderLine3:Landroid/graphics/Path;

    const v1, 0x425e2b02    # 55.542f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v1, v2

    const/high16 v2, 0x431a0000    # 154.0f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42570106    # 53.751f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x431bca7f    # 155.791f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42570106    # 53.751f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v5, v6

    const/high16 v6, 0x431e0000    # 158.0f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 215
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mUnderLine3:Landroid/graphics/Path;

    const v1, 0x42570106    # 53.751f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43203581    # 160.209f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x425e2b02    # 55.542f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v3, v4

    const/high16 v4, 0x43220000    # 162.0f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42670106    # 57.751f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v5, v6

    const/high16 v6, 0x43220000    # 162.0f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 217
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mUnderLine3:Landroid/graphics/Path;

    const v1, 0x4313f53f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v1, v2

    const/high16 v2, 0x43220000    # 162.0f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 218
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mUnderLine3:Landroid/graphics/Path;

    const v1, 0x43162ac1

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v1, v2

    const/high16 v2, 0x43220000    # 162.0f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4317f53f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x43203581    # 160.209f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4317f53f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v5, v6

    const/high16 v6, 0x431e0000    # 158.0f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 221
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mUnderLine4:Landroid/graphics/Path;

    const v1, 0x433efdf4

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v2, v11

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 222
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mUnderLine4:Landroid/graphics/Path;

    const v1, 0x4310a560

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v2, v11

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 223
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mUnderLine4:Landroid/graphics/Path;

    const v1, 0x430e6fdf

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v2, v11

    const v3, 0x430ca560

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x432fca7f    # 175.791f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x430ca560

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v5, v6

    const/high16 v6, 0x43320000    # 178.0f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 225
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mUnderLine4:Landroid/graphics/Path;

    const v1, 0x430ca560

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43343581    # 180.209f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x430e6fdf

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v3, v4

    const/high16 v4, 0x43360000    # 182.0f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4310a560

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v5, v6

    const/high16 v6, 0x43360000    # 182.0f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 227
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mUnderLine4:Landroid/graphics/Path;

    const v1, 0x433efdf4

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v1, v2

    const/high16 v2, 0x43360000    # 182.0f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 228
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mUnderLine4:Landroid/graphics/Path;

    const v1, 0x43413375

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v1, v2

    const/high16 v2, 0x43360000    # 182.0f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4342fdf4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x43343581    # 180.209f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4342fdf4

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v5, v6

    const/high16 v6, 0x43320000    # 178.0f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 230
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mUnderLine4:Landroid/graphics/Path;

    const v1, 0x4342fdf4

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x432fca7f    # 175.791f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x43413375

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v4, v11

    const v5, 0x433efdf4

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v5, v6

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float/2addr v6, v11

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 232
    return-void
.end method


# virtual methods
.method public cancelAnimation()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    const/16 v1, 0xff

    const/4 v0, 0x0

    .line 526
    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->dx:F

    .line 527
    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->cloudyX:F

    .line 529
    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->a1:I

    .line 530
    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->a2:I

    .line 531
    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->a3:I

    .line 532
    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->a4:I

    .line 534
    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->t1:F

    .line 535
    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->t2:F

    .line 536
    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->t3:F

    .line 537
    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->t4:F

    .line 539
    iput v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->s1:F

    .line 540
    iput v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->s2:F

    .line 541
    iput v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->s3:F

    .line 543
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 544
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 546
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->TranslateAni2:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->TranslateAni2:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 547
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->TranslateAni2:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 550
    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->UnderLineAni1:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->UnderLineAni1:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 551
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->UnderLineAni1:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 553
    :cond_2
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->UnderLineAni2:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->UnderLineAni2:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 554
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->UnderLineAni2:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 556
    :cond_3
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->UnderLineAni3:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->UnderLineAni3:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 557
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->UnderLineAni3:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 559
    :cond_4
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->UnderLineAni4:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->UnderLineAni4:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 560
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->UnderLineAni4:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 563
    :cond_5
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mIsActiveAnimationThread:Z

    .line 564
    return-void
.end method

.method public drawCludy(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 446
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 447
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->dx:F

    invoke-virtual {p1, v0, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 448
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mCloudPath_left:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mCloudPaint1:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 449
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 451
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 452
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->dx:F

    invoke-virtual {p1, v2, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 453
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mCloudPath_top:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mCloudPaint1:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 454
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 456
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 457
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->dx:F

    neg-float v0, v0

    invoke-virtual {p1, v0, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 458
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mCloudPath_right:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mCloudPaint1:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 459
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 461
    return-void
.end method

.method public drawUnderLine(Landroid/graphics/Canvas;Landroid/graphics/Path;FIFFF)V
    .locals 1
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "path"    # Landroid/graphics/Path;
    .param p3, "trans"    # F
    .param p4, "alpha"    # I
    .param p5, "scale"    # F
    .param p6, "px"    # F
    .param p7, "py"    # F
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 464
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 465
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mCloudPaint1:Landroid/graphics/Paint;

    invoke-virtual {v0, p4}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 466
    const/4 v0, 0x0

    invoke-virtual {p1, p3, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 467
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p1, p5, v0, p6, p7}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 468
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mCloudPaint1:Landroid/graphics/Paint;

    invoke-virtual {p1, p2, v0}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 469
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 470
    return-void
.end method

.method public getIsPalyAnimation()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 414
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->UnderLineAni1:Landroid/animation/ValueAnimator;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->UnderLineAni2:Landroid/animation/ValueAnimator;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->UnderLineAni3:Landroid/animation/ValueAnimator;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->UnderLineAni4:Landroid/animation/ValueAnimator;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->TranslateAni2:Landroid/animation/ValueAnimator;

    if-nez v1, :cond_1

    .line 425
    :cond_0
    :goto_0
    return v0

    .line 421
    :cond_1
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->UnderLineAni1:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->UnderLineAni2:Landroid/animation/ValueAnimator;

    .line 422
    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->UnderLineAni3:Landroid/animation/ValueAnimator;

    .line 423
    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->UnderLineAni4:Landroid/animation/ValueAnimator;

    .line 424
    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->TranslateAni2:Landroid/animation/ValueAnimator;

    .line 425
    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getScale()F
    .locals 1

    .prologue
    .line 576
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    return v0
.end method

.method public getView()Landroid/view/View;
    .locals 0

    .prologue
    .line 522
    return-object p0
.end method

.method public isRunning()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 567
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->getIsPalyAnimation()Z

    move-result v0

    return v0
.end method

.method public makeAnimation()V
    .locals 22

    .prologue
    .line 282
    const/16 v17, 0x3

    move/from16 v0, v17

    new-array v0, v0, [F

    move-object/from16 v17, v0

    const/16 v18, 0x0

    const/16 v19, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    aput v19, v17, v18

    const/16 v18, 0x1

    const/high16 v19, -0x40000000    # -2.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    aput v19, v17, v18

    const/16 v18, 0x2

    const/16 v19, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    aput v19, v17, v18

    invoke-static/range {v17 .. v17}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    .line 283
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    move-object/from16 v17, v0

    const/16 v18, -0x1

    invoke-virtual/range {v17 .. v18}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 284
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    move-object/from16 v17, v0

    const-wide/16 v18, 0x5dc

    invoke-virtual/range {v17 .. v19}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 285
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/animation/ValueAnimator;->start()V

    .line 286
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->l:Landroid/animation/Animator$AnimatorListener;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 288
    const/16 v17, 0x3

    move/from16 v0, v17

    new-array v0, v0, [F

    move-object/from16 v17, v0

    const/16 v18, 0x0

    const/16 v19, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    aput v19, v17, v18

    const/16 v18, 0x1

    const/high16 v19, -0x3f200000    # -7.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    aput v19, v17, v18

    const/16 v18, 0x2

    const/16 v19, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    aput v19, v17, v18

    invoke-static/range {v17 .. v17}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->TranslateAni2:Landroid/animation/ValueAnimator;

    .line 289
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->TranslateAni2:Landroid/animation/ValueAnimator;

    move-object/from16 v17, v0

    const/16 v18, -0x1

    invoke-virtual/range {v17 .. v18}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 290
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->TranslateAni2:Landroid/animation/ValueAnimator;

    move-object/from16 v17, v0

    const-wide/16 v18, 0xbb8

    invoke-virtual/range {v17 .. v19}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 291
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->TranslateAni2:Landroid/animation/ValueAnimator;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/animation/ValueAnimator;->start()V

    .line 292
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->TranslateAni2:Landroid/animation/ValueAnimator;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->l:Landroid/animation/Animator$AnimatorListener;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 294
    const/16 v17, 0x0

    const/16 v18, 0x0

    invoke-static/range {v17 .. v18}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v2

    .line 295
    .local v2, "key1":Landroid/animation/Keyframe;
    const v17, 0x3e4ccccd    # 0.2f

    const/high16 v18, 0x437f0000    # 255.0f

    invoke-static/range {v17 .. v18}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v3

    .line 296
    .local v3, "key2":Landroid/animation/Keyframe;
    const v17, 0x3ecccccd    # 0.4f

    const/high16 v18, 0x437f0000    # 255.0f

    invoke-static/range {v17 .. v18}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v4

    .line 297
    .local v4, "key3":Landroid/animation/Keyframe;
    const v17, 0x3f4ccccd    # 0.8f

    const/high16 v18, 0x437f0000    # 255.0f

    invoke-static/range {v17 .. v18}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v5

    .line 298
    .local v5, "key4":Landroid/animation/Keyframe;
    const/high16 v17, 0x3f800000    # 1.0f

    const/high16 v18, 0x437f0000    # 255.0f

    invoke-static/range {v17 .. v18}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v6

    .line 299
    .local v6, "key5":Landroid/animation/Keyframe;
    const/16 v17, 0x0

    const/high16 v18, -0x3dc40000    # -47.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    invoke-static/range {v17 .. v18}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v12

    .line 300
    .local v12, "t_key1":Landroid/animation/Keyframe;
    const v17, 0x3e4ccccd    # 0.2f

    const/16 v18, 0x0

    invoke-static/range {v17 .. v18}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v13

    .line 301
    .local v13, "t_key2":Landroid/animation/Keyframe;
    const v17, 0x3ecccccd    # 0.4f

    const/16 v18, 0x0

    invoke-static/range {v17 .. v18}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v14

    .line 302
    .local v14, "t_key3":Landroid/animation/Keyframe;
    const v17, 0x3f4ccccd    # 0.8f

    const/16 v18, 0x0

    invoke-static/range {v17 .. v18}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v15

    .line 303
    .local v15, "t_key4":Landroid/animation/Keyframe;
    const/high16 v17, 0x3f800000    # 1.0f

    const/16 v18, 0x0

    invoke-static/range {v17 .. v18}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v16

    .line 304
    .local v16, "t_key5":Landroid/animation/Keyframe;
    const/16 v17, 0x0

    const v18, 0x3fe66666    # 1.8f

    invoke-static/range {v17 .. v18}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v7

    .line 305
    .local v7, "s_key1":Landroid/animation/Keyframe;
    const v17, 0x3e4ccccd    # 0.2f

    const/high16 v18, 0x3f800000    # 1.0f

    invoke-static/range {v17 .. v18}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v8

    .line 306
    .local v8, "s_key2":Landroid/animation/Keyframe;
    const v17, 0x3ecccccd    # 0.4f

    const/high16 v18, 0x3f800000    # 1.0f

    invoke-static/range {v17 .. v18}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v9

    .line 307
    .local v9, "s_key3":Landroid/animation/Keyframe;
    const v17, 0x3f4ccccd    # 0.8f

    const/high16 v18, 0x3f800000    # 1.0f

    invoke-static/range {v17 .. v18}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v10

    .line 308
    .local v10, "s_key4":Landroid/animation/Keyframe;
    const/high16 v17, 0x3f800000    # 1.0f

    const/high16 v18, 0x3f800000    # 1.0f

    invoke-static/range {v17 .. v18}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v11

    .line 310
    .local v11, "s_key5":Landroid/animation/Keyframe;
    const/16 v17, 0x3

    move/from16 v0, v17

    new-array v0, v0, [Landroid/animation/PropertyValuesHolder;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    const-string v19, "alpha"

    const/16 v20, 0x5

    move/from16 v0, v20

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    aput-object v2, v20, v21

    const/16 v21, 0x1

    aput-object v3, v20, v21

    const/16 v21, 0x2

    aput-object v4, v20, v21

    const/16 v21, 0x3

    aput-object v5, v20, v21

    const/16 v21, 0x4

    aput-object v6, v20, v21

    .line 311
    invoke-static/range {v19 .. v20}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v19

    aput-object v19, v17, v18

    const/16 v18, 0x1

    const-string v19, "trans"

    const/16 v20, 0x5

    move/from16 v0, v20

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    aput-object v12, v20, v21

    const/16 v21, 0x1

    aput-object v13, v20, v21

    const/16 v21, 0x2

    aput-object v14, v20, v21

    const/16 v21, 0x3

    aput-object v15, v20, v21

    const/16 v21, 0x4

    aput-object v16, v20, v21

    .line 312
    invoke-static/range {v19 .. v20}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v19

    aput-object v19, v17, v18

    const/16 v18, 0x2

    const-string v19, "scale"

    const/16 v20, 0x5

    move/from16 v0, v20

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    aput-object v7, v20, v21

    const/16 v21, 0x1

    aput-object v8, v20, v21

    const/16 v21, 0x2

    aput-object v9, v20, v21

    const/16 v21, 0x3

    aput-object v10, v20, v21

    const/16 v21, 0x4

    aput-object v11, v20, v21

    .line 313
    invoke-static/range {v19 .. v20}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v19

    aput-object v19, v17, v18

    .line 310
    invoke-static/range {v17 .. v17}, Landroid/animation/ValueAnimator;->ofPropertyValuesHolder([Landroid/animation/PropertyValuesHolder;)Landroid/animation/ValueAnimator;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->UnderLineAni1:Landroid/animation/ValueAnimator;

    .line 314
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->UnderLineAni1:Landroid/animation/ValueAnimator;

    move-object/from16 v17, v0

    const/16 v18, -0x1

    invoke-virtual/range {v17 .. v18}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 315
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->UnderLineAni1:Landroid/animation/ValueAnimator;

    move-object/from16 v17, v0

    const/16 v18, 0x2

    invoke-virtual/range {v17 .. v18}, Landroid/animation/ValueAnimator;->setRepeatMode(I)V

    .line 316
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->UnderLineAni1:Landroid/animation/ValueAnimator;

    move-object/from16 v17, v0

    const-wide/16 v18, 0x1388

    invoke-virtual/range {v17 .. v19}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 317
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->UnderLineAni1:Landroid/animation/ValueAnimator;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/animation/ValueAnimator;->start()V

    .line 318
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->UnderLineAni1:Landroid/animation/ValueAnimator;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->l:Landroid/animation/Animator$AnimatorListener;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 320
    const/16 v17, 0x0

    const/16 v18, 0x0

    invoke-static/range {v17 .. v18}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v2

    .line 321
    const v17, 0x3dcccccd    # 0.1f

    const/16 v18, 0x0

    invoke-static/range {v17 .. v18}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v3

    .line 322
    const v17, 0x3e99999a    # 0.3f

    const/high16 v18, 0x437f0000    # 255.0f

    invoke-static/range {v17 .. v18}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v4

    .line 323
    const v17, 0x3f4ccccd    # 0.8f

    const/high16 v18, 0x437f0000    # 255.0f

    invoke-static/range {v17 .. v18}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v5

    .line 324
    const/high16 v17, 0x3f800000    # 1.0f

    const/high16 v18, 0x437f0000    # 255.0f

    invoke-static/range {v17 .. v18}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v6

    .line 325
    const/16 v17, 0x0

    const/high16 v18, 0x42640000    # 57.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    invoke-static/range {v17 .. v18}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v12

    .line 326
    const v17, 0x3dcccccd    # 0.1f

    const/high16 v18, 0x42640000    # 57.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    invoke-static/range {v17 .. v18}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v13

    .line 327
    const v17, 0x3e99999a    # 0.3f

    const/16 v18, 0x0

    invoke-static/range {v17 .. v18}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v14

    .line 328
    const v17, 0x3f4ccccd    # 0.8f

    const/16 v18, 0x0

    invoke-static/range {v17 .. v18}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v15

    .line 329
    const/high16 v17, 0x3f800000    # 1.0f

    const/16 v18, 0x0

    invoke-static/range {v17 .. v18}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v16

    .line 330
    const/16 v17, 0x0

    const v18, 0x3f4ccccd    # 0.8f

    invoke-static/range {v17 .. v18}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v7

    .line 331
    const v17, 0x3dcccccd    # 0.1f

    const v18, 0x3f4ccccd    # 0.8f

    invoke-static/range {v17 .. v18}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v8

    .line 332
    const v17, 0x3e99999a    # 0.3f

    const/high16 v18, 0x3f800000    # 1.0f

    invoke-static/range {v17 .. v18}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v9

    .line 333
    const v17, 0x3f4ccccd    # 0.8f

    const/high16 v18, 0x3f800000    # 1.0f

    invoke-static/range {v17 .. v18}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v10

    .line 334
    const/high16 v17, 0x3f800000    # 1.0f

    const/high16 v18, 0x3f800000    # 1.0f

    invoke-static/range {v17 .. v18}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v11

    .line 335
    const/16 v17, 0x3

    move/from16 v0, v17

    new-array v0, v0, [Landroid/animation/PropertyValuesHolder;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    const-string v19, "alpha"

    const/16 v20, 0x5

    move/from16 v0, v20

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    aput-object v2, v20, v21

    const/16 v21, 0x1

    aput-object v3, v20, v21

    const/16 v21, 0x2

    aput-object v4, v20, v21

    const/16 v21, 0x3

    aput-object v5, v20, v21

    const/16 v21, 0x4

    aput-object v6, v20, v21

    .line 336
    invoke-static/range {v19 .. v20}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v19

    aput-object v19, v17, v18

    const/16 v18, 0x1

    const-string v19, "trans"

    const/16 v20, 0x5

    move/from16 v0, v20

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    aput-object v12, v20, v21

    const/16 v21, 0x1

    aput-object v13, v20, v21

    const/16 v21, 0x2

    aput-object v14, v20, v21

    const/16 v21, 0x3

    aput-object v15, v20, v21

    const/16 v21, 0x4

    aput-object v16, v20, v21

    .line 337
    invoke-static/range {v19 .. v20}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v19

    aput-object v19, v17, v18

    const/16 v18, 0x2

    const-string v19, "scale"

    const/16 v20, 0x5

    move/from16 v0, v20

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    aput-object v7, v20, v21

    const/16 v21, 0x1

    aput-object v8, v20, v21

    const/16 v21, 0x2

    aput-object v9, v20, v21

    const/16 v21, 0x3

    aput-object v10, v20, v21

    const/16 v21, 0x4

    aput-object v11, v20, v21

    .line 338
    invoke-static/range {v19 .. v20}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v19

    aput-object v19, v17, v18

    .line 335
    invoke-static/range {v17 .. v17}, Landroid/animation/ValueAnimator;->ofPropertyValuesHolder([Landroid/animation/PropertyValuesHolder;)Landroid/animation/ValueAnimator;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->UnderLineAni2:Landroid/animation/ValueAnimator;

    .line 339
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->UnderLineAni2:Landroid/animation/ValueAnimator;

    move-object/from16 v17, v0

    const/16 v18, -0x1

    invoke-virtual/range {v17 .. v18}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 340
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->UnderLineAni2:Landroid/animation/ValueAnimator;

    move-object/from16 v17, v0

    const/16 v18, 0x2

    invoke-virtual/range {v17 .. v18}, Landroid/animation/ValueAnimator;->setRepeatMode(I)V

    .line 341
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->UnderLineAni2:Landroid/animation/ValueAnimator;

    move-object/from16 v17, v0

    const-wide/16 v18, 0x1388

    invoke-virtual/range {v17 .. v19}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 342
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->UnderLineAni2:Landroid/animation/ValueAnimator;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/animation/ValueAnimator;->start()V

    .line 343
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->UnderLineAni2:Landroid/animation/ValueAnimator;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->l:Landroid/animation/Animator$AnimatorListener;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 345
    const/16 v17, 0x0

    const/16 v18, 0x0

    invoke-static/range {v17 .. v18}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v2

    .line 346
    const v17, 0x3e99999a    # 0.3f

    const/16 v18, 0x0

    invoke-static/range {v17 .. v18}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v3

    .line 347
    const/high16 v17, 0x3f000000    # 0.5f

    const/high16 v18, 0x437f0000    # 255.0f

    invoke-static/range {v17 .. v18}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v4

    .line 348
    const v17, 0x3f4ccccd    # 0.8f

    const/high16 v18, 0x437f0000    # 255.0f

    invoke-static/range {v17 .. v18}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v5

    .line 349
    const/high16 v17, 0x3f800000    # 1.0f

    const/high16 v18, 0x437f0000    # 255.0f

    invoke-static/range {v17 .. v18}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v6

    .line 350
    const/16 v17, 0x0

    const/high16 v18, -0x3dc40000    # -47.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    invoke-static/range {v17 .. v18}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v12

    .line 351
    const v17, 0x3e99999a    # 0.3f

    const/high16 v18, -0x3dc40000    # -47.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    invoke-static/range {v17 .. v18}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v13

    .line 352
    const/high16 v17, 0x3f000000    # 0.5f

    const/16 v18, 0x0

    invoke-static/range {v17 .. v18}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v14

    .line 353
    const v17, 0x3f4ccccd    # 0.8f

    const/16 v18, 0x0

    invoke-static/range {v17 .. v18}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v15

    .line 354
    const/high16 v17, 0x3f800000    # 1.0f

    const/16 v18, 0x0

    invoke-static/range {v17 .. v18}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v16

    .line 355
    const/16 v17, 0x0

    const/high16 v18, 0x3fc00000    # 1.5f

    invoke-static/range {v17 .. v18}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v7

    .line 356
    const v17, 0x3e99999a    # 0.3f

    const/high16 v18, 0x3fc00000    # 1.5f

    invoke-static/range {v17 .. v18}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v8

    .line 357
    const/high16 v17, 0x3f000000    # 0.5f

    const/high16 v18, 0x3f800000    # 1.0f

    invoke-static/range {v17 .. v18}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v9

    .line 358
    const v17, 0x3f4ccccd    # 0.8f

    const/high16 v18, 0x3f800000    # 1.0f

    invoke-static/range {v17 .. v18}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v10

    .line 359
    const/high16 v17, 0x3f800000    # 1.0f

    const/high16 v18, 0x3f800000    # 1.0f

    invoke-static/range {v17 .. v18}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v11

    .line 360
    const/16 v17, 0x3

    move/from16 v0, v17

    new-array v0, v0, [Landroid/animation/PropertyValuesHolder;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    const-string v19, "alpha"

    const/16 v20, 0x5

    move/from16 v0, v20

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    aput-object v2, v20, v21

    const/16 v21, 0x1

    aput-object v3, v20, v21

    const/16 v21, 0x2

    aput-object v4, v20, v21

    const/16 v21, 0x3

    aput-object v5, v20, v21

    const/16 v21, 0x4

    aput-object v6, v20, v21

    .line 361
    invoke-static/range {v19 .. v20}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v19

    aput-object v19, v17, v18

    const/16 v18, 0x1

    const-string v19, "trans"

    const/16 v20, 0x5

    move/from16 v0, v20

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    aput-object v12, v20, v21

    const/16 v21, 0x1

    aput-object v13, v20, v21

    const/16 v21, 0x2

    aput-object v14, v20, v21

    const/16 v21, 0x3

    aput-object v15, v20, v21

    const/16 v21, 0x4

    aput-object v16, v20, v21

    .line 362
    invoke-static/range {v19 .. v20}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v19

    aput-object v19, v17, v18

    const/16 v18, 0x2

    const-string v19, "scale"

    const/16 v20, 0x5

    move/from16 v0, v20

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    aput-object v7, v20, v21

    const/16 v21, 0x1

    aput-object v8, v20, v21

    const/16 v21, 0x2

    aput-object v9, v20, v21

    const/16 v21, 0x3

    aput-object v10, v20, v21

    const/16 v21, 0x4

    aput-object v11, v20, v21

    .line 363
    invoke-static/range {v19 .. v20}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v19

    aput-object v19, v17, v18

    .line 360
    invoke-static/range {v17 .. v17}, Landroid/animation/ValueAnimator;->ofPropertyValuesHolder([Landroid/animation/PropertyValuesHolder;)Landroid/animation/ValueAnimator;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->UnderLineAni3:Landroid/animation/ValueAnimator;

    .line 364
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->UnderLineAni3:Landroid/animation/ValueAnimator;

    move-object/from16 v17, v0

    const/16 v18, -0x1

    invoke-virtual/range {v17 .. v18}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 365
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->UnderLineAni3:Landroid/animation/ValueAnimator;

    move-object/from16 v17, v0

    const/16 v18, 0x2

    invoke-virtual/range {v17 .. v18}, Landroid/animation/ValueAnimator;->setRepeatMode(I)V

    .line 366
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->UnderLineAni3:Landroid/animation/ValueAnimator;

    move-object/from16 v17, v0

    const-wide/16 v18, 0x1388

    invoke-virtual/range {v17 .. v19}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 367
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->UnderLineAni3:Landroid/animation/ValueAnimator;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/animation/ValueAnimator;->start()V

    .line 368
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->UnderLineAni3:Landroid/animation/ValueAnimator;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->l:Landroid/animation/Animator$AnimatorListener;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 370
    const/16 v17, 0x0

    const/16 v18, 0x0

    invoke-static/range {v17 .. v18}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v2

    .line 371
    const v17, 0x3e99999a    # 0.3f

    const/16 v18, 0x0

    invoke-static/range {v17 .. v18}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v3

    .line 372
    const/high16 v17, 0x3f000000    # 0.5f

    const/16 v18, 0x0

    invoke-static/range {v17 .. v18}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v4

    .line 373
    const v17, 0x3f333333    # 0.7f

    const/high16 v18, 0x437f0000    # 255.0f

    invoke-static/range {v17 .. v18}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v5

    .line 374
    const/high16 v17, 0x3f800000    # 1.0f

    const/high16 v18, 0x437f0000    # 255.0f

    invoke-static/range {v17 .. v18}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v6

    .line 376
    const/16 v17, 0x0

    const/high16 v18, 0x41f00000    # 30.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    invoke-static/range {v17 .. v18}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v12

    .line 377
    const v17, 0x3e99999a    # 0.3f

    const/high16 v18, 0x41f00000    # 30.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    invoke-static/range {v17 .. v18}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v13

    .line 378
    const/high16 v17, 0x3f000000    # 0.5f

    const/high16 v18, 0x41f00000    # 30.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    invoke-static/range {v17 .. v18}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v14

    .line 379
    const v17, 0x3f333333    # 0.7f

    const/16 v18, 0x0

    invoke-static/range {v17 .. v18}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v15

    .line 380
    const/high16 v17, 0x3f800000    # 1.0f

    const/16 v18, 0x0

    invoke-static/range {v17 .. v18}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v16

    .line 381
    const/16 v17, 0x2

    move/from16 v0, v17

    new-array v0, v0, [Landroid/animation/PropertyValuesHolder;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    const-string v19, "alpha"

    const/16 v20, 0x5

    move/from16 v0, v20

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    aput-object v2, v20, v21

    const/16 v21, 0x1

    aput-object v3, v20, v21

    const/16 v21, 0x2

    aput-object v4, v20, v21

    const/16 v21, 0x3

    aput-object v5, v20, v21

    const/16 v21, 0x4

    aput-object v6, v20, v21

    .line 382
    invoke-static/range {v19 .. v20}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v19

    aput-object v19, v17, v18

    const/16 v18, 0x1

    const-string v19, "trans"

    const/16 v20, 0x5

    move/from16 v0, v20

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    aput-object v12, v20, v21

    const/16 v21, 0x1

    aput-object v13, v20, v21

    const/16 v21, 0x2

    aput-object v14, v20, v21

    const/16 v21, 0x3

    aput-object v15, v20, v21

    const/16 v21, 0x4

    aput-object v16, v20, v21

    .line 383
    invoke-static/range {v19 .. v20}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v19

    aput-object v19, v17, v18

    .line 381
    invoke-static/range {v17 .. v17}, Landroid/animation/ValueAnimator;->ofPropertyValuesHolder([Landroid/animation/PropertyValuesHolder;)Landroid/animation/ValueAnimator;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->UnderLineAni4:Landroid/animation/ValueAnimator;

    .line 384
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->UnderLineAni4:Landroid/animation/ValueAnimator;

    move-object/from16 v17, v0

    const/16 v18, -0x1

    invoke-virtual/range {v17 .. v18}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 385
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->UnderLineAni4:Landroid/animation/ValueAnimator;

    move-object/from16 v17, v0

    const/16 v18, 0x2

    invoke-virtual/range {v17 .. v18}, Landroid/animation/ValueAnimator;->setRepeatMode(I)V

    .line 386
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->UnderLineAni4:Landroid/animation/ValueAnimator;

    move-object/from16 v17, v0

    const-wide/16 v18, 0x1388

    invoke-virtual/range {v17 .. v19}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 387
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->UnderLineAni4:Landroid/animation/ValueAnimator;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/animation/ValueAnimator;->start()V

    .line 388
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->UnderLineAni4:Landroid/animation/ValueAnimator;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->l:Landroid/animation/Animator$AnimatorListener;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 389
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 9
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v1, 0x0

    .line 474
    :try_start_0
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_1

    .line 475
    const-string v0, ""

    const-string v1, "scale is less then 0"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 519
    :cond_0
    :goto_0
    return-void

    .line 479
    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mIsActiveAnimationThread:Z

    if-eqz v0, :cond_2

    .line 480
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->dx:F

    .line 481
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->TranslateAni2:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->cloudyX:F

    .line 483
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->UnderLineAni1:Landroid/animation/ValueAnimator;

    const-string v1, "alpha"

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->intValue()I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->a1:I

    .line 484
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->UnderLineAni2:Landroid/animation/ValueAnimator;

    const-string v1, "alpha"

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->intValue()I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->a2:I

    .line 485
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->UnderLineAni3:Landroid/animation/ValueAnimator;

    const-string v1, "alpha"

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->intValue()I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->a3:I

    .line 486
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->UnderLineAni4:Landroid/animation/ValueAnimator;

    const-string v1, "alpha"

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->intValue()I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->a4:I

    .line 488
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->UnderLineAni1:Landroid/animation/ValueAnimator;

    const-string v1, "trans"

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->t1:F

    .line 489
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->UnderLineAni2:Landroid/animation/ValueAnimator;

    const-string v1, "trans"

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->t2:F

    .line 490
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->UnderLineAni3:Landroid/animation/ValueAnimator;

    const-string v1, "trans"

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->t3:F

    .line 491
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->UnderLineAni4:Landroid/animation/ValueAnimator;

    const-string v1, "trans"

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->t4:F

    .line 493
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->UnderLineAni1:Landroid/animation/ValueAnimator;

    const-string v1, "scale"

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->s1:F

    .line 494
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->UnderLineAni2:Landroid/animation/ValueAnimator;

    const-string v1, "scale"

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->s2:F

    .line 495
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->UnderLineAni3:Landroid/animation/ValueAnimator;

    const-string v1, "scale"

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->s3:F

    .line 498
    :cond_2
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 499
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->cloudyX:F

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 501
    invoke-virtual {p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->drawCludy(Landroid/graphics/Canvas;)V

    .line 502
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mUnderLine1:Landroid/graphics/Path;

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->t1:F

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->a1:I

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->s1:F

    const v0, 0x429c9b23

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float v6, v0, v1

    const/high16 v0, 0x43090000    # 137.0f

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    mul-float v7, v0, v1

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->drawUnderLine(Landroid/graphics/Canvas;Landroid/graphics/Path;FIFFF)V

    .line 503
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mUnderLine2:Landroid/graphics/Path;

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->t2:F

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->a2:I

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->s2:F

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->drawUnderLine(Landroid/graphics/Canvas;Landroid/graphics/Path;FIFFF)V

    .line 504
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mUnderLine3:Landroid/graphics/Path;

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->t3:F

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->a3:I

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->s3:F

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->drawUnderLine(Landroid/graphics/Canvas;Landroid/graphics/Path;FIFFF)V

    .line 505
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mUnderLine4:Landroid/graphics/Path;

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->t4:F

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->a4:I

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->drawUnderLine(Landroid/graphics/Canvas;Landroid/graphics/Path;FIFFF)V

    .line 507
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mCloudPaint1:Landroid/graphics/Paint;

    const/16 v1, 0xff

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 508
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 510
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->getIsPalyAnimation()Z

    move-result v0

    if-nez v0, :cond_0

    .line 511
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->cancelAnimation()V

    .line 512
    const-string v0, ""

    const-string v1, "cancelA"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 515
    :catch_0
    move-exception v8

    .line 516
    .local v8, "e":Ljava/lang/Exception;
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0
.end method

.method public onViewAttachedToWindow(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 629
    return-void
.end method

.method public onViewDetachedFromWindow(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x0

    .line 584
    const-string v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onViewDetachedFromWindow : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 585
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mCloudPaint1:Landroid/graphics/Paint;

    .line 586
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mCloudPath_left:Landroid/graphics/Path;

    .line 587
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mCloudPath_right:Landroid/graphics/Path;

    .line 588
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mCloudPath_top:Landroid/graphics/Path;

    .line 589
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mUnderLine1:Landroid/graphics/Path;

    .line 590
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mUnderLine2:Landroid/graphics/Path;

    .line 591
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mUnderLine3:Landroid/graphics/Path;

    .line 592
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mUnderLine4:Landroid/graphics/Path;

    .line 594
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    .line 595
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 596
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 598
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->TranslateAni2:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_1

    .line 599
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->TranslateAni2:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 600
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->TranslateAni2:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 603
    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->UnderLineAni1:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_2

    .line 604
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->UnderLineAni1:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 605
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->UnderLineAni1:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 607
    :cond_2
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->UnderLineAni2:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_3

    .line 608
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->UnderLineAni2:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 609
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->UnderLineAni2:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 611
    :cond_3
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->UnderLineAni3:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_4

    .line 612
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->UnderLineAni3:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 613
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->UnderLineAni3:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 615
    :cond_4
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->UnderLineAni4:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_5

    .line 616
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->UnderLineAni4:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 617
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->UnderLineAni4:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 620
    :cond_5
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    .line 621
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->TranslateAni2:Landroid/animation/ValueAnimator;

    .line 622
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->UnderLineAni1:Landroid/animation/ValueAnimator;

    .line 623
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->UnderLineAni2:Landroid/animation/ValueAnimator;

    .line 624
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->UnderLineAni3:Landroid/animation/ValueAnimator;

    .line 625
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->UnderLineAni4:Landroid/animation/ValueAnimator;

    .line 626
    return-void
.end method

.method public setPaintColor(I)V
    .locals 0
    .param p1, "color"    # I

    .prologue
    .line 72
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mPaintColor:I

    .line 73
    return-void
.end method

.method public setScale(F)V
    .locals 0
    .param p1, "scale"    # F

    .prologue
    .line 571
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mScale:F

    .line 572
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->init()V

    .line 573
    return-void
.end method

.method public setWidthAndHeight(II)V
    .locals 1
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 580
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, p1, p2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 581
    .local v0, "l":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 582
    return-void
.end method

.method public startAnimation()V
    .locals 2

    .prologue
    .line 392
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->isStop:Z

    .line 393
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->makeAnimation()V

    .line 394
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->mIsActiveAnimationThread:Z

    .line 395
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView$2;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView$2;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;)V

    .line 409
    .local v0, "t":Ljava/lang/Thread;
    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setPriority(I)V

    .line 410
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 411
    return-void
.end method

.method public stopAnimation()V
    .locals 1

    .prologue
    .line 243
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;->isStop:Z

    .line 244
    return-void
.end method
