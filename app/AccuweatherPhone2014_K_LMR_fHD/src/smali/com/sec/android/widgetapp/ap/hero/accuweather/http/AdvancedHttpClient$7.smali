.class Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient$7;
.super Ljava/lang/Thread;
.source "AdvancedHttpClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->post(ILandroid/content/Context;Ljava/net/URL;Ljava/lang/String;Lorg/apache/http/message/HeaderGroup;Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;)Ljava/lang/Thread;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;

.field final synthetic val$body:Ljava/lang/String;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$handler:Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;

.field final synthetic val$headers:Lorg/apache/http/message/HeaderGroup;

.field final synthetic val$position:I

.field final synthetic val$url:Ljava/net/URL;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;Landroid/content/Context;Ljava/net/URL;Ljava/lang/String;Lorg/apache/http/message/HeaderGroup;Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;I)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;

    .prologue
    .line 296
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient$7;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;

    iput-object p2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient$7;->val$context:Landroid/content/Context;

    iput-object p3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient$7;->val$url:Ljava/net/URL;

    iput-object p4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient$7;->val$body:Ljava/lang/String;

    iput-object p5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient$7;->val$headers:Lorg/apache/http/message/HeaderGroup;

    iput-object p6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient$7;->val$handler:Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;

    iput p7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient$7;->val$position:I

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 298
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient$7;->val$context:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient$7;->val$url:Ljava/net/URL;

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient$7;->val$body:Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient$7;->val$headers:Lorg/apache/http/message/HeaderGroup;

    invoke-static {v2, v3, v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpClientThread;->post(Landroid/content/Context;Ljava/net/URL;Ljava/lang/String;Lorg/apache/http/message/HeaderGroup;)Lorg/apache/http/HttpResponse;

    move-result-object v1

    .line 300
    .local v1, "response":Lorg/apache/http/HttpResponse;
    const-wide/16 v2, 0x1

    :try_start_0
    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient$7;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 304
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient$7;->val$handler:Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;

    invoke-virtual {v2, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;->setResponse(Lorg/apache/http/HttpResponse;)V

    .line 306
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient$7;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;

    const/4 v3, 0x0

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient$7;->val$position:I

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient$7;->val$handler:Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;

    invoke-static {v3, v4, v6, v6, v5}, Landroid/os/Message;->obtain(Landroid/os/Handler;IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->processResult(Landroid/os/Message;)V
    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;Landroid/os/Message;)V

    .line 307
    :goto_0
    return-void

    .line 301
    :catch_0
    move-exception v0

    .line 302
    .local v0, "e":Ljava/lang/InterruptedException;
    goto :goto_0
.end method
