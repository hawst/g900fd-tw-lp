.class Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$3;
.super Landroid/content/BroadcastReceiver;
.source "WeatherClockDetail.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .prologue
    .line 446
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 14
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 450
    :try_start_0
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 452
    .local v0, "action":Ljava/lang/String;
    const-string v11, ""

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "onReceive : action = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 453
    const-string v11, "android.intent.action.TIME_TICK"

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_0

    const-string v11, "android.intent.action.TIME_SET"

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 454
    :cond_0
    const/4 v9, 0x0

    .line 456
    .local v9, "value":I
    iget-object v11, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCityList:Ljava/util/ArrayList;
    invoke-static {v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$1000(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Ljava/util/ArrayList;

    move-result-object v11

    if-eqz v11, :cond_5

    iget-object v11, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCityList:Ljava/util/ArrayList;
    invoke-static {v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$1000(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Ljava/util/ArrayList;

    move-result-object v11

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v11

    if-lez v11, :cond_5

    .line 457
    const/4 v2, 0x0

    .line 458
    .local v2, "checkValue":Z
    iget-object v11, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCityList:Ljava/util/ArrayList;
    invoke-static {v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$1000(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Ljava/util/ArrayList;

    move-result-object v11

    invoke-virtual {v11}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_1
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_2

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;

    .line 459
    .local v6, "item":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;
    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->getLocation()Ljava/lang/String;

    move-result-object v12

    iget-object v13, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->selLocation:Ljava/lang/String;
    invoke-static {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$1100(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 460
    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->getTimeZone()Ljava/lang/String;

    move-result-object v11

    .line 461
    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->getSunRiseTime()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->getSunSetTime()Ljava/lang/String;

    move-result-object v13

    .line 460
    invoke-static {v11, v12, v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkDayOrNight(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    .line 462
    .local v5, "isDay":Z
    const/4 v11, 0x1

    if-ne v5, v11, :cond_6

    const/4 v9, 0x0

    .line 464
    :goto_0
    iget-object v11, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    iget v11, v11, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mIsDay:I

    if-eq v11, v9, :cond_2

    .line 465
    const-string v11, ""

    const-string v12, "change isday !!!"

    invoke-static {v11, v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 466
    const/4 v2, 0x1

    .line 472
    .end local v5    # "isDay":Z
    .end local v6    # "item":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;
    :cond_2
    if-eqz v2, :cond_5

    .line 475
    iget-object v11, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    iget-object v12, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .line 476
    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->selLocation:Ljava/lang/String;
    invoke-static {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$1100(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Ljava/lang/String;

    move-result-object v12

    .line 475
    invoke-static {v11, v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getDetailWeatherInfo(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    move-result-object v4

    .line 477
    .local v4, "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getTodayWeatherInfo()Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    move-result-object v8

    .line 480
    .local v8, "today":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;
    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getTimeZone()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v11

    .line 479
    invoke-static {v11}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v11

    invoke-static {v11}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v1

    .line 482
    .local v1, "cal":Ljava/util/Calendar;
    if-nez v1, :cond_3

    .line 483
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 484
    const-string v11, ""

    const-string v12, " cal nul set default"

    invoke-static {v11, v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 486
    :cond_3
    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getDate()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Long;->longValue()J

    move-result-wide v11

    invoke-virtual {v1, v11, v12}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 488
    iget-object v11, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    invoke-virtual {v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->makeFormatDate()Ljava/text/SimpleDateFormat;

    move-result-object v7

    .line 490
    .local v7, "sdf":Ljava/text/SimpleDateFormat;
    invoke-virtual {v7, v1}, Ljava/text/SimpleDateFormat;->setCalendar(Ljava/util/Calendar;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 493
    :try_start_1
    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getLocation()Ljava/lang/String;

    move-result-object v11

    const-string v12, "cityId:current"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 494
    iget-object v11, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    iget-object v11, v11, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->detailContext:Landroid/app/Activity;

    invoke-static {v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getDaemonWeatherIconNum(Landroid/content/Context;)I

    move-result v10

    .line 495
    .local v10, "weatherIcon":I
    const/4 v5, 0x0

    .line 496
    .restart local v5    # "isDay":Z
    if-nez v9, :cond_7

    .line 497
    const/4 v5, 0x1

    .line 501
    :goto_1
    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getTodayWeatherInfo()Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    move-result-object v11

    invoke-virtual {v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getIconNum()I

    move-result v11

    invoke-static {v11, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getWeatherInfoIcon(IZ)I

    move-result v11

    if-eq v10, v11, :cond_4

    .line 502
    const-string v11, ""

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "weatherIcon : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 503
    const-string v11, ""

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "convertIcon : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getTodayWeatherInfo()Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    move-result-object v13

    invoke-virtual {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getIconNum()I

    move-result v13

    invoke-static {v13, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getWeatherInfoIcon(IZ)I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 504
    iget-object v11, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    iget-object v11, v11, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->detailContext:Landroid/app/Activity;

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getTodayWeatherInfo()Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    move-result-object v12

    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getIconNum()I

    move-result v12

    invoke-static {v12, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getWeatherInfoIcon(IZ)I

    move-result v12

    invoke-static {v11, v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateDaemonWeatherIconNum(Landroid/content/Context;I)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 511
    .end local v5    # "isDay":Z
    .end local v10    # "weatherIcon":I
    :cond_4
    :goto_2
    if-nez v9, :cond_8

    .line 512
    :try_start_2
    const-string v11, ""

    const-string v12, "ISDAYTYPE_SUNRISE !!!"

    invoke-static {v11, v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 513
    iget-object v11, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    const/4 v12, 0x1

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->setIsDay(Ljava/util/Calendar;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;Z)V
    invoke-static {v11, v1, v4, v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$1200(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Ljava/util/Calendar;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;Z)V

    .line 524
    .end local v0    # "action":Ljava/lang/String;
    .end local v1    # "cal":Ljava/util/Calendar;
    .end local v2    # "checkValue":Z
    .end local v4    # "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    .end local v7    # "sdf":Ljava/text/SimpleDateFormat;
    .end local v8    # "today":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;
    .end local v9    # "value":I
    :cond_5
    :goto_3
    return-void

    .line 462
    .restart local v0    # "action":Ljava/lang/String;
    .restart local v2    # "checkValue":Z
    .restart local v5    # "isDay":Z
    .restart local v6    # "item":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;
    .restart local v9    # "value":I
    :cond_6
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 499
    .end local v6    # "item":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;
    .restart local v1    # "cal":Ljava/util/Calendar;
    .restart local v4    # "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    .restart local v7    # "sdf":Ljava/text/SimpleDateFormat;
    .restart local v8    # "today":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;
    .restart local v10    # "weatherIcon":I
    :cond_7
    const/4 v5, 0x0

    goto :goto_1

    .line 507
    .end local v5    # "isDay":Z
    .end local v10    # "weatherIcon":I
    :catch_0
    move-exception v3

    .line 508
    .local v3, "e":Ljava/lang/Exception;
    const-string v11, ""

    const-string v12, "CI exception"

    invoke-static {v11, v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_2

    .line 521
    .end local v0    # "action":Ljava/lang/String;
    .end local v1    # "cal":Ljava/util/Calendar;
    .end local v2    # "checkValue":Z
    .end local v3    # "e":Ljava/lang/Exception;
    .end local v4    # "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    .end local v7    # "sdf":Ljava/text/SimpleDateFormat;
    .end local v8    # "today":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;
    .end local v9    # "value":I
    :catch_1
    move-exception v3

    .line 522
    .restart local v3    # "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3

    .line 515
    .end local v3    # "e":Ljava/lang/Exception;
    .restart local v0    # "action":Ljava/lang/String;
    .restart local v1    # "cal":Ljava/util/Calendar;
    .restart local v2    # "checkValue":Z
    .restart local v4    # "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    .restart local v7    # "sdf":Ljava/text/SimpleDateFormat;
    .restart local v8    # "today":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;
    .restart local v9    # "value":I
    :cond_8
    :try_start_3
    const-string v11, ""

    const-string v12, "ISDAYTYPE_SUNSET !!!"

    invoke-static {v11, v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 516
    iget-object v11, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    const/4 v12, 0x0

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->setIsDay(Ljava/util/Calendar;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;Z)V
    invoke-static {v11, v1, v4, v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$1200(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Ljava/util/Calendar;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;Z)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_3
.end method
