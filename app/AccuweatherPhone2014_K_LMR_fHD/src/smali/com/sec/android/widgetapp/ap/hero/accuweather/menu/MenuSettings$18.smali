.class Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$18;
.super Ljava/lang/Object;
.source "MenuSettings.java"

# interfaces
.implements Landroid/app/TimePickerDialog$OnTimeSetListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    .prologue
    .line 1875
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$18;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTimeSet(Landroid/widget/TimePicker;II)V
    .locals 12
    .param p1, "arg0"    # Landroid/widget/TimePicker;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I

    .prologue
    const/4 v11, 0x1

    .line 1877
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$18;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    iput p2, v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mHour:I

    .line 1878
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$18;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    iput p3, v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mMinute:I

    .line 1880
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    .line 1882
    .local v7, "nowTime":J
    new-instance v10, Ljava/util/GregorianCalendar;

    invoke-direct {v10}, Ljava/util/GregorianCalendar;-><init>()V

    .line 1883
    .local v10, "today":Ljava/util/GregorianCalendar;
    invoke-virtual {v10, v11}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v1

    .line 1884
    .local v1, "year":I
    const/4 v2, 0x2

    invoke-virtual {v10, v2}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v2

    add-int/lit8 v6, v2, 0x1

    .line 1885
    .local v6, "month":I
    const/4 v2, 0x5

    invoke-virtual {v10, v2}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v3

    .line 1887
    .local v3, "day":I
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 1888
    .local v0, "calendar":Ljava/util/Calendar;
    add-int/lit8 v2, v6, -0x1

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$18;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    iget v4, v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mHour:I

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$18;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    iget v5, v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mMinute:I

    invoke-virtual/range {v0 .. v5}, Ljava/util/Calendar;->set(IIIII)V

    .line 1889
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    .line 1891
    .local v9, "setTime":Ljava/lang/Long;
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v4, "yy/MM/dd HH:mm:ss"

    invoke-direct {v2, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->sdf:Ljava/text/SimpleDateFormat;
    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->access$1202(Ljava/text/SimpleDateFormat;)Ljava/text/SimpleDateFormat;

    .line 1892
    const-string v2, "yy/MM/dd HH:mm:ss"

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->len:I
    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->access$1302(I)I

    .line 1894
    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v2, v4, v7

    if-ltz v2, :cond_0

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v2, v4, v7

    if-nez v2, :cond_1

    .line 1895
    :cond_0
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$18;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$18;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    iget v4, v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mHour:I

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$18;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    iget v5, v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mMinute:I

    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    invoke-static {v2, v4, v5, v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->setNotiTime(Landroid/content/Context;IILjava/lang/Boolean;)V

    .line 1900
    :goto_0
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$18;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->init()V
    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->access$800(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;)V

    .line 1901
    return-void

    .line 1897
    :cond_1
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$18;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$18;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    iget v4, v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mHour:I

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$18;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    iget v5, v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mMinute:I

    const/4 v11, 0x0

    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    invoke-static {v2, v4, v5, v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->setNotiTime(Landroid/content/Context;IILjava/lang/Boolean;)V

    goto :goto_0
.end method
