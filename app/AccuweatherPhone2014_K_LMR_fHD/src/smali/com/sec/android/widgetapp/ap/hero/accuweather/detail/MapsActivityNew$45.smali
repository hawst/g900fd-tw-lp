.class Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$45;
.super Landroid/os/Handler;
.source "MapsActivityNew.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field resultArrived:Z

.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V
    .locals 1
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    .prologue
    .line 1631
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$45;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 1632
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$45;->resultArrived:Z

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 21
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 1635
    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v17, v0

    const v18, -0x13d30

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_1

    .line 1636
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$45;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->detailHandler:Landroid/os/Handler;

    move-object/from16 v17, v0

    if-eqz v17, :cond_0

    .line 1637
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$45;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->detailHandler:Landroid/os/Handler;

    move-object/from16 v17, v0

    const v18, -0x140b4

    const-wide/16 v19, 0x7530

    invoke-virtual/range {v17 .. v20}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 1762
    :cond_0
    :goto_0
    return-void

    .line 1639
    :cond_1
    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v17, v0

    const v18, -0x140b4

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_2

    .line 1640
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$45;->resultArrived:Z

    move/from16 v17, v0

    if-nez v17, :cond_0

    .line 1641
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$45;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    move-object/from16 v17, v0

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->hideLoadingDialog()V
    invoke-static/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$2300(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V

    .line 1642
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$45;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    move-object/from16 v17, v0

    const v18, 0x7f0d001f

    invoke-static/range {v17 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->toast(Landroid/content/Context;I)V

    goto :goto_0

    .line 1645
    :cond_2
    const/16 v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$45;->resultArrived:Z

    .line 1646
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v17

    const-string v18, "RESPONSE_BODY"

    invoke-virtual/range {v17 .. v18}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1647
    .local v3, "body":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v17

    const-string v18, "RESPONSE_CODE"

    invoke-virtual/range {v17 .. v18}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v13

    .line 1649
    .local v13, "result":I
    if-nez v3, :cond_3

    .line 1650
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$45;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    move-object/from16 v17, v0

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->hideLoadingDialog()V
    invoke-static/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$2300(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V

    goto :goto_0

    .line 1654
    :cond_3
    const/16 v17, 0xc8

    move/from16 v0, v17

    if-ne v13, v0, :cond_7

    .line 1655
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$45;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mParser:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$3800(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;

    move-result-object v17

    if-eqz v17, :cond_0

    .line 1656
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$45;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mParser:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$3800(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$45;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    move-object/from16 v18, v0

    .line 1657
    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v0, v3, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;->parseDetailWeatherLocationCity(Ljava/lang/String;Landroid/content/Context;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    move-result-object v4

    .line 1658
    .local v4, "cityInfo":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$45;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mParser:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$3800(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$45;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mTempScale:I
    invoke-static/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$4100(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)I

    move-result v18

    .line 1659
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v19

    invoke-static/range {v19 .. v20}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v19

    .line 1658
    move-object/from16 v0, v17

    move/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v3, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;->parseDetailWeather(ILjava/lang/String;Ljava/lang/String;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    move-result-object v10

    .line 1660
    .local v10, "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    if-eqz v10, :cond_0

    .line 1661
    new-instance v5, Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$45;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    move-object/from16 v17, v0

    const-class v18, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-direct {v5, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1664
    .local v5, "detailIntent":Landroid/content/Intent;
    invoke-virtual {v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getTodayWeatherInfo()Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    move-result-object v16

    .line 1665
    .local v16, "twi":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;
    new-instance v14, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    invoke-direct {v14}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;-><init>()V

    .line 1666
    .local v14, "sinfo":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;
    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getCurrentTemp()F

    move-result v17

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->setCurrentTemp(F)V

    .line 1667
    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getDate()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->setDate(Ljava/lang/String;)V

    .line 1668
    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getHighTemp()F

    move-result v17

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->setHighTemp(F)V

    .line 1669
    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getIconNum()I

    move-result v17

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->setIconNum(I)V

    .line 1670
    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getLowTemp()F

    move-result v17

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->setLowTemp(F)V

    .line 1671
    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getTempScale()I

    move-result v17

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->setTempScale(I)V

    .line 1672
    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getTimeZone()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->setTimeZone(Ljava/lang/String;)V

    .line 1673
    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getUpdateDate()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->setUpdateDate(Ljava/lang/String;)V

    .line 1674
    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getUrl()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->setUrl(Ljava/lang/String;)V

    .line 1675
    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getSunRiseTime()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->setSunRiseTime(Ljava/lang/String;)V

    .line 1676
    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getSunSetTime()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->setSunSetTime(Ljava/lang/String;)V

    .line 1677
    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getWeatherText()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->setWeatherText(Ljava/lang/String;)V

    .line 1678
    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getRealFeel()F

    move-result v17

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->setRealFeel(F)V

    .line 1679
    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getDayRainProbability()I

    move-result v17

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->setDayRainProbability(I)V

    .line 1680
    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getDaySnowProbability()I

    move-result v17

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->setDaySnowProbability(I)V

    .line 1681
    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getDayHailProbability()I

    move-result v17

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->setDayHailProbability(I)V

    .line 1683
    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getDayPrecipitationProbability()I

    move-result v17

    .line 1682
    move/from16 v0, v17

    invoke-virtual {v14, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->setDayPrecipitationProbability(I)V

    .line 1684
    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getNightRainProbability()I

    move-result v17

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->setNightRainProbability(I)V

    .line 1685
    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getNightSnowProbability()I

    move-result v17

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->setNightSnowProbability(I)V

    .line 1686
    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getNightHailProbability()I

    move-result v17

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->setNightHailProbability(I)V

    .line 1688
    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getNightPrecipitationProbability()I

    move-result v17

    .line 1687
    move/from16 v0, v17

    invoke-virtual {v14, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->setNightPrecipitationProbability(I)V

    .line 1690
    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getRelativeHumidity()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->setRelativeHumidity(Ljava/lang/String;)V

    .line 1691
    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getUVIndex()I

    move-result v17

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->setUVIndex(I)V

    .line 1692
    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getUVIndexText()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->setUVIndexText(Ljava/lang/String;)V

    .line 1694
    const-string v17, "todayInfo"

    move-object/from16 v0, v17

    invoke-virtual {v5, v0, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1695
    new-instance v9, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    invoke-direct {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;-><init>()V

    .line 1696
    .local v9, "imsi":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$45;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->detailitem:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$4500(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getCity()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v9, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->setCity(Ljava/lang/String;)V

    .line 1697
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$45;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->detailitem:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$4500(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getLocation()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v9, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->setLocation(Ljava/lang/String;)V

    .line 1698
    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->getLatitude()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v9, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->setLatitude(Ljava/lang/String;)V

    .line 1699
    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->getLongitude()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v9, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->setLongitude(Ljava/lang/String;)V

    .line 1700
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$45;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->detailitem:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$4500(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getProvider()I

    move-result v17

    move/from16 v0, v17

    invoke-virtual {v9, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->setProvider(I)V

    .line 1701
    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->getState()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v9, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->setState(Ljava/lang/String;)V

    .line 1702
    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getSummerTime()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v9, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->setSummerTime(Ljava/lang/String;)V

    .line 1703
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$45;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mZoomlevel:I
    invoke-static/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$1600(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)I

    move-result v17

    move/from16 v0, v17

    invoke-virtual {v9, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->setZoomlevel(I)V

    .line 1704
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$45;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->detailitem:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$4500(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getLocation()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v9, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->setRealLocation(Ljava/lang/String;)V

    .line 1705
    const-string v17, "cityInfo"

    move-object/from16 v0, v17

    invoke-virtual {v5, v0, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1706
    const-string v17, "flags"

    const v18, -0x9bdc

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v5, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1708
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 1710
    .local v6, "forcastList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;>;"
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_1
    invoke-virtual {v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getForcastSize()I

    move-result v17

    move/from16 v0, v17

    if-ge v8, v0, :cond_4

    .line 1711
    invoke-virtual {v10, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getForecastInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    move-result-object v15

    .line 1712
    .local v15, "tempGeneralWeatherInfo":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    invoke-virtual {v6, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1710
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 1715
    .end local v15    # "tempGeneralWeatherInfo":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    :cond_4
    const-string v17, "forcastList"

    move-object/from16 v0, v17

    invoke-virtual {v5, v0, v6}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 1717
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 1718
    .local v7, "hourlyForcast":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;>;"
    if-eqz v10, :cond_5

    .line 1719
    const/4 v8, 0x0

    :goto_2
    invoke-virtual {v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getHourSize()I

    move-result v17

    move/from16 v0, v17

    if-ge v8, v0, :cond_5

    .line 1720
    invoke-virtual {v10, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getHourInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1719
    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    .line 1723
    :cond_5
    const-string v17, "hourlyweather"

    move-object/from16 v0, v17

    invoke-virtual {v5, v0, v7}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 1724
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 1725
    .local v11, "list2":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$45;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->detailitem:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$4500(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$45;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mZoomlevel:I
    invoke-static/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$1600(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)I

    move-result v18

    invoke-virtual/range {v17 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->setZoomlevel(I)V

    .line 1726
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$45;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->detailitem:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$4500(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1728
    const-string v17, "mapinfo"

    move-object/from16 v0, v17

    invoke-virtual {v5, v0, v11}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 1729
    const-string v17, "mapweatherzoom5"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$45;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mListZoom5:Ljava/util/ArrayList;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$1200(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v5, v0, v1}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 1730
    const-string v17, "mapweatherzoom6"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$45;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mListZoom6:Ljava/util/ArrayList;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$1300(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v5, v0, v1}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 1731
    const-string v17, "mapweatherzoom7"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$45;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mListZoom7:Ljava/util/ArrayList;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$1400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v5, v0, v1}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 1732
    const-string v17, "latitude"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$45;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mLatitude:I
    invoke-static/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$4600(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)I

    move-result v18

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v5, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1733
    const-string v17, "longitude"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$45;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mLongitude:I
    invoke-static/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$4700(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)I

    move-result v18

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v5, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1734
    const-string v17, "pre_lat"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$45;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mPrelatitude:D
    invoke-static/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$4800(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)D

    move-result-wide v18

    move-object/from16 v0, v17

    move-wide/from16 v1, v18

    invoke-virtual {v5, v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;D)Landroid/content/Intent;

    .line 1735
    const-string v17, "pre_lon"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$45;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mPreLongitutde:D
    invoke-static/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$4900(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)D

    move-result-wide v18

    move-object/from16 v0, v17

    move-wide/from16 v1, v18

    invoke-virtual {v5, v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;D)Landroid/content/Intent;

    .line 1736
    const-string v17, "tempscale"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$45;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mTempScale:I
    invoke-static/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$4100(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)I

    move-result v18

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v5, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1737
    const-string v17, "launcher"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$45;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mLauncher:I
    invoke-static/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$3300(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)I

    move-result v18

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v5, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1739
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 1740
    .local v12, "photosInfo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;>;"
    const/4 v8, 0x0

    :goto_3
    invoke-virtual {v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getPhotosInfoSize()I

    move-result v17

    move/from16 v0, v17

    if-ge v8, v0, :cond_6

    .line 1741
    invoke-virtual {v10, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getPhotosInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1740
    add-int/lit8 v8, v8, 0x1

    goto :goto_3

    .line 1743
    :cond_6
    const-string v17, "photosInfo"

    move-object/from16 v0, v17

    invoke-virtual {v5, v0, v12}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 1747
    const-string v17, "flags"

    const v18, 0xf22f

    .line 1748
    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v5, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1750
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$45;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    move-object/from16 v17, v0

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->hideLoadingDialog()V
    invoke-static/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$2300(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V

    .line 1751
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$45;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    move-object/from16 v17, v0

    const v18, 0xf22f

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v0, v5, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 1759
    .end local v4    # "cityInfo":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;
    .end local v5    # "detailIntent":Landroid/content/Intent;
    .end local v6    # "forcastList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;>;"
    .end local v7    # "hourlyForcast":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;>;"
    .end local v8    # "i":I
    .end local v9    # "imsi":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;
    .end local v10    # "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    .end local v11    # "list2":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;>;"
    .end local v12    # "photosInfo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;>;"
    .end local v14    # "sinfo":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;
    .end local v16    # "twi":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$45;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    move-object/from16 v17, v0

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->hideLoadingDialog()V
    invoke-static/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$2300(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V

    .line 1760
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$45;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    move-object/from16 v17, v0

    const v18, 0x7f0d001f

    invoke-static/range {v17 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->toast(Landroid/content/Context;I)V

    goto/16 :goto_0
.end method
