.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;
.super Ljava/lang/Object;
.source "MyCurrentLocation.java"


# instance fields
.field private bGPSProvider:Z

.field private bNetworkProvider:Z

.field private cancelFlag:I

.field private ctx:Landroid/content/Context;

.field currentLocationHandler:Landroid/os/Handler;

.field private gpsLocationManager:Landroid/location/LocationManager;

.field private httpClient:Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;

.field private isLocation:Z

.field private isNewLocationApplied:Z

.field isRetry:Z

.field private latitude:D

.field private final locationListener:Landroid/location/LocationListener;

.field private longitude:D

.field private mHttpThread:Ljava/lang/Thread;

.field private mIsRefresh:Z

.field private mProvider:Ljava/lang/String;

.field private mclsProvider:Ljava/lang/String;

.field private networkLocationManager:Landroid/location/LocationManager;

.field networkOnly:Z

.field private final passiveLocationListener:Landroid/location/LocationListener;

.field private passiveLocationManager:Landroid/location/LocationManager;

.field returnHandler:Landroid/os/Handler;

.field th:Ljava/lang/Thread;

.field timeoutHandler:Landroid/os/Handler;

.field private timer:Ljava/util/Timer;

.field private urlManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "ctx"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->bGPSProvider:Z

    iput-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->bNetworkProvider:Z

    .line 41
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->networkLocationManager:Landroid/location/LocationManager;

    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->gpsLocationManager:Landroid/location/LocationManager;

    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->passiveLocationManager:Landroid/location/LocationManager;

    .line 46
    iput-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->isLocation:Z

    .line 48
    iput v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->cancelFlag:I

    .line 54
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->isNewLocationApplied:Z

    .line 215
    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$1;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$1;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;)V

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->returnHandler:Landroid/os/Handler;

    .line 230
    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$2;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$2;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;)V

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->timeoutHandler:Landroid/os/Handler;

    .line 257
    iput-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->networkOnly:Z

    .line 259
    iput-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->isRetry:Z

    .line 297
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->mHttpThread:Ljava/lang/Thread;

    .line 680
    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$7;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$7;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;)V

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->locationListener:Landroid/location/LocationListener;

    .line 702
    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$8;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$8;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;)V

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->passiveLocationListener:Landroid/location/LocationListener;

    .line 110
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->ctx:Landroid/content/Context;

    .line 111
    const-string v0, "location"

    .line 112
    .local v0, "context":Ljava/lang/String;
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/location/LocationManager;

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->networkLocationManager:Landroid/location/LocationManager;

    .line 113
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/location/LocationManager;

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->gpsLocationManager:Landroid/location/LocationManager;

    .line 114
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/location/LocationManager;

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->passiveLocationManager:Landroid/location/LocationManager;

    .line 115
    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;

    invoke-direct {v1, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->urlManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;

    .line 117
    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;

    invoke-direct {v1, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->httpClient:Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;

    .line 118
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;)D
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    .prologue
    .line 31
    iget-wide v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->latitude:D

    return-wide v0
.end method

.method static synthetic access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;)D
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    .prologue
    .line 31
    iget-wide v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->longitude:D

    return-wide v0
.end method

.method static synthetic access$200(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;)Ljava/lang/Thread;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->mHttpThread:Ljava/lang/Thread;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;Ljava/lang/Thread;)Ljava/lang/Thread;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;
    .param p1, "x1"    # Ljava/lang/Thread;

    .prologue
    .line 31
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->mHttpThread:Ljava/lang/Thread;

    return-object p1
.end method

.method static synthetic access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->ctx:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->mProvider:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;)Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->httpClient:Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;Landroid/location/Location;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;
    .param p1, "x1"    # Landroid/location/Location;

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->updateWithNewLocation(Landroid/location/Location;)V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;)Landroid/location/LocationManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->passiveLocationManager:Landroid/location/LocationManager;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;)Landroid/location/LocationListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->passiveLocationListener:Landroid/location/LocationListener;

    return-object v0
.end method

.method private getCurrentProvider(Z)I
    .locals 8
    .param p1, "isRefresh"    # Z

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 121
    const-string v4, ""

    const-string v5, "mcl getCP"

    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    iget-boolean v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->networkOnly:Z

    if-nez v4, :cond_4

    .line 124
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->gpsLocationManager:Landroid/location/LocationManager;

    const-string v5, "gps"

    invoke-virtual {v4, v5}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    .line 125
    .local v0, "bGPS":Z
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->networkLocationManager:Landroid/location/LocationManager;

    const-string v5, "network"

    .line 126
    invoke-virtual {v4, v5}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v1

    .line 128
    .local v1, "bNetwork":Z
    sget-boolean v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isVerizon:Z

    if-eqz v4, :cond_1

    if-nez p1, :cond_1

    .line 129
    const-string v4, ""

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mcl getCP FW : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 130
    if-eqz v0, :cond_0

    if-nez v1, :cond_2

    .line 169
    .end local v0    # "bGPS":Z
    :cond_0
    :goto_0
    return v2

    .line 134
    .restart local v0    # "bGPS":Z
    :cond_1
    if-nez v0, :cond_2

    if-eqz v1, :cond_0

    .line 139
    :cond_2
    iput-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->bGPSProvider:Z

    .line 140
    iput-boolean v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->bNetworkProvider:Z

    .line 143
    iget-boolean v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->bNetworkProvider:Z

    if-eqz v4, :cond_3

    .line 144
    const-string v4, "network"

    iput-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->mclsProvider:Ljava/lang/String;

    .line 149
    :goto_1
    const-string v4, ""

    const-string v5, "-- MCL -- p : %s"

    new-array v6, v3, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->mclsProvider:Ljava/lang/String;

    invoke-static {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertProvider(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v2

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v3

    .line 150
    goto :goto_0

    .line 146
    :cond_3
    const-string v4, "gps"

    iput-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->mclsProvider:Ljava/lang/String;

    goto :goto_1

    .line 152
    .end local v0    # "bGPS":Z
    .end local v1    # "bNetwork":Z
    :cond_4
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->networkLocationManager:Landroid/location/LocationManager;

    const-string v5, "network"

    .line 153
    invoke-virtual {v4, v5}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v1

    .line 155
    .restart local v1    # "bNetwork":Z
    if-eqz v1, :cond_0

    .line 159
    iput-boolean v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->bNetworkProvider:Z

    .line 162
    const-string v4, "network"

    iput-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->mclsProvider:Ljava/lang/String;

    .line 166
    const-string v4, ""

    const-string v5, "-- MCL -- getCurrentProvider provider : %s"

    new-array v6, v3, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->mclsProvider:Ljava/lang/String;

    .line 168
    invoke-static {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertProvider(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v2

    .line 167
    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 166
    invoke-static {v4, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v3

    .line 169
    goto :goto_0
.end method

.method private setIsLocation(Z)V
    .locals 0
    .param p1, "isLocation"    # Z

    .prologue
    .line 82
    iput-boolean p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->isLocation:Z

    .line 83
    return-void
.end method

.method private updateWithNewLocation(Landroid/location/Location;)V
    .locals 3
    .param p1, "location"    # Landroid/location/Location;

    .prologue
    const/4 v2, 0x0

    .line 642
    const-string v0, ""

    const-string v1, "mcl uWNL"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 644
    if-eqz p1, :cond_0

    .line 645
    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->setLatitude(D)V

    .line 646
    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->setLongitude(D)V

    .line 647
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->setIsLocation(Z)V

    .line 648
    invoke-virtual {p0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->cancelGetLastLocation(I)V

    .line 656
    iget-wide v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->latitude:D

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v0

    iget-wide v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->longitude:D

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->currentLocationHandler:Landroid/os/Handler;

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->addCityWeatherFromProvider(Ljava/lang/String;Ljava/lang/String;Landroid/os/Handler;)V

    .line 661
    :goto_0
    return-void

    .line 659
    :cond_0
    invoke-direct {p0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->setIsLocation(Z)V

    goto :goto_0
.end method


# virtual methods
.method public addCityWeatherFromDetail(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;Landroid/os/Handler;)V
    .locals 8
    .param p1, "info"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;
    .param p2, "locationHandler"    # Landroid/os/Handler;

    .prologue
    .line 411
    const-string v1, ""

    const-string v6, "MCL adCtWthrFrmDetail@"

    invoke-static {v1, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 412
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->urlManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;

    iget-boolean v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->mIsRefresh:Z

    invoke-static {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getPackagename(Z)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;->makeHeader(Ljava/lang/String;)Lorg/apache/http/message/HeaderGroup;

    move-result-object v3

    .line 413
    .local v3, "headerGroup":Lorg/apache/http/message/HeaderGroup;
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->ctx:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getTempScaleSetting(Landroid/content/Context;)I

    move-result v4

    .line 414
    .local v4, "tempScaleSetting":I
    const-string v1, ""

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " MCL adCtWthrFrmDetail tempSS = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v1, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 415
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->urlManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;

    invoke-virtual {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->getLocation()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->ctx:Landroid/content/Context;

    .line 416
    invoke-static {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkLanguage(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    .line 415
    invoke-virtual {v1, v6, v4, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;->makeUrlForGetDetailData(Ljava/lang/String;ILjava/lang/String;)Ljava/net/URL;

    move-result-object v2

    .line 417
    .local v2, "url":Ljava/net/URL;
    invoke-static {}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getTimestamp()Ljava/lang/String;

    move-result-object v5

    .line 419
    .local v5, "updateDate":Ljava/lang/String;
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$4;

    move-object v1, p0

    move-object v6, p1

    move-object v7, p2

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$4;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;Ljava/net/URL;Lorg/apache/http/message/HeaderGroup;ILjava/lang/String;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;Landroid/os/Handler;)V

    .line 485
    .local v0, "t":Ljava/lang/Thread;
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->th:Ljava/lang/Thread;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->th:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->isAlive()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 486
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->th:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    .line 489
    :cond_0
    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->th:Ljava/lang/Thread;

    .line 490
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setDaemon(Z)V

    .line 491
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 492
    return-void
.end method

.method public addCityWeatherFromProvider(Ljava/lang/String;Ljava/lang/String;Landroid/os/Handler;)V
    .locals 8
    .param p1, "latitude"    # Ljava/lang/String;
    .param p2, "longitude"    # Ljava/lang/String;
    .param p3, "locationHandler"    # Landroid/os/Handler;

    .prologue
    const-wide/16 v6, 0x0

    .line 326
    const-string v1, ""

    const-string v4, "MCL adCtWthrFrmPrvdr@"

    invoke-static {v1, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 327
    invoke-static {p1}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    cmpl-double v1, v4, v6

    if-nez v1, :cond_0

    invoke-static {p2}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    cmpl-double v1, v4, v6

    if-nez v1, :cond_0

    .line 328
    const-string v1, ""

    const-string v4, "mcl adCtWthrFrmPrvdr@ : invalid la,lo"

    invoke-static {v1, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 329
    const/16 v1, 0x3f2

    invoke-virtual {p3, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 408
    :goto_0
    return-void

    .line 334
    :cond_0
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->urlManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;

    iget-boolean v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->mIsRefresh:Z

    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getPackagename(Z)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;->makeHeader(Ljava/lang/String;)Lorg/apache/http/message/HeaderGroup;

    move-result-object v3

    .line 336
    .local v3, "headerGroup":Lorg/apache/http/message/HeaderGroup;
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->urlManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 337
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    .line 336
    invoke-virtual {v1, v4, v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;->makeUrlForLocGetData(Ljava/lang/String;Ljava/lang/String;Z)Ljava/net/URL;

    move-result-object v2

    .line 339
    .local v2, "url":Ljava/net/URL;
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$3;

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$3;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;Ljava/net/URL;Lorg/apache/http/message/HeaderGroup;Ljava/lang/String;Ljava/lang/String;Landroid/os/Handler;)V

    .line 401
    .local v0, "t":Ljava/lang/Thread;
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->th:Ljava/lang/Thread;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->th:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->isAlive()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 402
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->th:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    .line 405
    :cond_1
    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->th:Ljava/lang/Thread;

    .line 406
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setDaemon(Z)V

    .line 407
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method public addCityWeatherFromProvider_Deutsch(Ljava/lang/String;Ljava/lang/String;Landroid/os/Handler;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;)V
    .locals 7
    .param p1, "latitude"    # Ljava/lang/String;
    .param p2, "longitude"    # Ljava/lang/String;
    .param p3, "locationHandler"    # Landroid/os/Handler;
    .param p4, "temp_cityinfo"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    .prologue
    const/4 v6, 0x1

    .line 496
    const-string v1, ""

    const-string v4, "MCL adCtWthrFrmPrvdr_Deutsch@"

    invoke-static {v1, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 497
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->urlManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;

    iget-boolean v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->mIsRefresh:Z

    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getPackagename(Z)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;->makeHeader(Ljava/lang/String;)Lorg/apache/http/message/HeaderGroup;

    move-result-object v3

    .line 498
    .local v3, "headerGroup":Lorg/apache/http/message/HeaderGroup;
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->urlManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 499
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 498
    invoke-virtual {v1, v4, v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;->makeUrlForLocGetData(Ljava/lang/String;Ljava/lang/String;Z)Ljava/net/URL;

    move-result-object v2

    .line 501
    .local v2, "url":Ljava/net/URL;
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$5;

    move-object v1, p0

    move-object v4, p4

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$5;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;Ljava/net/URL;Lorg/apache/http/message/HeaderGroup;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;Landroid/os/Handler;)V

    .line 551
    .local v0, "t":Ljava/lang/Thread;
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->th:Ljava/lang/Thread;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->th:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->isAlive()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 552
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->th:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    .line 555
    :cond_0
    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->th:Ljava/lang/Thread;

    .line 556
    invoke-virtual {v0, v6}, Ljava/lang/Thread;->setDaemon(Z)V

    .line 557
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 558
    return-void
.end method

.method public cancelGetLastLocation(I)V
    .locals 2
    .param p1, "cancelFlag"    # I

    .prologue
    .line 622
    const-string v0, ""

    const-string v1, "mcl cGLL"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 624
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->removeLocationListener()V

    .line 625
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->removePassiveLocationListener()V

    .line 627
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->timer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 628
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->timer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 631
    :cond_0
    invoke-virtual {p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->setCancelFlag(I)V

    .line 633
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->th:Ljava/lang/Thread;

    if-eqz v0, :cond_1

    .line 634
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->th:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 635
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->th:Ljava/lang/Thread;

    .line 638
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->stopHttpThread()V

    .line 639
    return-void
.end method

.method public getCencelFlag()I
    .locals 1

    .prologue
    .line 66
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->cancelFlag:I

    return v0
.end method

.method public getCurrentLocation()V
    .locals 6

    .prologue
    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    .line 173
    const-string v0, ""

    const-string v1, "mcl getCL"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->bGPSProvider:Z

    if-eqz v0, :cond_0

    .line 176
    const-string v0, ""

    const-string v1, "-- MCL -- TryviaG"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 177
    const-string v0, "G"

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->mProvider:Ljava/lang/String;

    .line 178
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->gpsLocationManager:Landroid/location/LocationManager;

    const-string v1, "gps"

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->locationListener:Landroid/location/LocationListener;

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    .line 182
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->bNetworkProvider:Z

    if-eqz v0, :cond_1

    .line 183
    const-string v0, ""

    const-string v1, "-- MCL -- TryviaN"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->mProvider:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->mProvider:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_2

    .line 185
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->mProvider:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "+N"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->mProvider:Ljava/lang/String;

    .line 189
    :goto_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->networkLocationManager:Landroid/location/LocationManager;

    const-string v1, "network"

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->locationListener:Landroid/location/LocationListener;

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    .line 192
    :cond_1
    return-void

    .line 187
    :cond_2
    const-string v0, "N"

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->mProvider:Ljava/lang/String;

    goto :goto_0
.end method

.method public getIsNewLocationApplied()Z
    .locals 1

    .prologue
    .line 74
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->isNewLocationApplied:Z

    return v0
.end method

.method public getLastLocation(Landroid/os/Handler;IZZ)V
    .locals 6
    .param p1, "resultHandler"    # Landroid/os/Handler;
    .param p2, "condition"    # I
    .param p3, "isDetectingTimeLimit"    # Z
    .param p4, "isRefresh"    # Z

    .prologue
    .line 565
    const-string v2, ""

    const-string v3, "mcl gLL"

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 566
    const/4 v1, 0x0

    .line 567
    .local v1, "timeout":I
    iget-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->isRetry:Z

    if-eqz v2, :cond_1

    .line 568
    const/16 v1, 0xa

    .line 572
    :goto_0
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->currentLocationHandler:Landroid/os/Handler;

    .line 573
    invoke-direct {p0, p4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->getCurrentProvider(Z)I

    move-result v0

    .line 575
    .local v0, "resultProvider":I
    const/4 v2, 0x1

    if-ne v2, v0, :cond_2

    .line 576
    if-nez p2, :cond_0

    .line 577
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->getCurrentLocation()V

    .line 580
    :cond_0
    new-instance v2, Ljava/util/Timer;

    invoke-direct {v2}, Ljava/util/Timer;-><init>()V

    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->timer:Ljava/util/Timer;

    .line 581
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->timer:Ljava/util/Timer;

    new-instance v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$6;

    invoke-direct {v3, p0, p3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$6;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;Z)V

    mul-int/lit16 v4, v1, 0x3e8

    int-to-long v4, v4

    invoke-virtual {v2, v3, v4, v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 619
    :goto_1
    return-void

    .line 570
    .end local v0    # "resultProvider":I
    :cond_1
    const/16 v1, 0x1e

    goto :goto_0

    .line 616
    .restart local v0    # "resultProvider":I
    :cond_2
    const-string v2, ""

    const-string v3, "p err"

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 617
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->currentLocationHandler:Landroid/os/Handler;

    const/16 v3, 0xc9

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1
.end method

.method public getLatitude()D
    .locals 2

    .prologue
    .line 86
    iget-wide v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->latitude:D

    return-wide v0
.end method

.method public getLongitude()D
    .locals 2

    .prologue
    .line 94
    iget-wide v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->longitude:D

    return-wide v0
.end method

.method public getPassiveLocation()V
    .locals 8

    .prologue
    .line 195
    const-string v0, ""

    const-string v1, "mcl getPL"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->passiveLocationManager:Landroid/location/LocationManager;

    const-string v1, "passive"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 198
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->mProvider:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "+P"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->mProvider:Ljava/lang/String;

    .line 199
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 200
    .local v6, "data":Landroid/os/Bundle;
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v7

    .line 201
    .local v7, "msg2":Landroid/os/Message;
    invoke-virtual {v7, v6}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 202
    const/16 v0, 0xca

    iput v0, v7, Landroid/os/Message;->what:I

    .line 203
    const-string v0, "provider"

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->mProvider:Ljava/lang/String;

    invoke-virtual {v6, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    const-string v0, "isFristRetry"

    const/4 v1, 0x1

    invoke-virtual {v6, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 205
    const-string v0, "isAutoRefresh"

    iget-boolean v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->mIsRefresh:Z

    invoke-virtual {v6, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 206
    invoke-virtual {v7, v6}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 207
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->currentLocationHandler:Landroid/os/Handler;

    const-wide/16 v1, 0x2710

    invoke-virtual {v0, v7, v1, v2}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 209
    const-string v0, ""

    const-string v1, "-- MCL -- TryviaP"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 210
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->passiveLocationManager:Landroid/location/LocationManager;

    const-string v1, "passive"

    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->passiveLocationListener:Landroid/location/LocationListener;

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    .line 213
    .end local v6    # "data":Landroid/os/Bundle;
    .end local v7    # "msg2":Landroid/os/Message;
    :cond_0
    return-void
.end method

.method public getUseProvider()Ljava/lang/String;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->mclsProvider:Ljava/lang/String;

    return-object v0
.end method

.method public isGetLocation()Z
    .locals 1

    .prologue
    .line 78
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->isLocation:Z

    return v0
.end method

.method public performCurrentLocation(Landroid/os/Handler;)V
    .locals 6
    .param p1, "locationHandler"    # Landroid/os/Handler;

    .prologue
    const/4 v5, 0x0

    .line 300
    const-string v1, ""

    const-string v2, "mcl pCL"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 301
    invoke-virtual {p0, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->setCancelFlag(I)V

    .line 302
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->urlManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;

    iget-wide v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->latitude:D

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v2

    iget-wide v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->longitude:D

    .line 303
    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v3

    .line 302
    invoke-virtual {v1, v2, v3, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;->makeUrlForLocGetData(Ljava/lang/String;Ljava/lang/String;Z)Ljava/net/URL;

    move-result-object v0

    .line 305
    .local v0, "url":Ljava/net/URL;
    if-nez v0, :cond_0

    .line 311
    :goto_0
    return-void

    .line 309
    :cond_0
    iget-wide v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->latitude:D

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v1

    iget-wide v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->longitude:D

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->addCityWeatherFromProvider(Ljava/lang/String;Ljava/lang/String;Landroid/os/Handler;)V

    goto :goto_0
.end method

.method public performGetCurrentLocation(Landroid/os/Handler;IZZZZ)V
    .locals 8
    .param p1, "locationHandler"    # Landroid/os/Handler;
    .param p2, "condition"    # I
    .param p3, "isDetectingTimeLimit"    # Z
    .param p4, "networkOnly"    # Z
    .param p5, "isRefresh"    # Z
    .param p6, "isNeedRefresh"    # Z

    .prologue
    .line 264
    const-string v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mcl pGCL :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 266
    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->performGetCurrentLocation(Landroid/os/Handler;IZZZZZ)V

    .line 268
    return-void
.end method

.method public performGetCurrentLocation(Landroid/os/Handler;IZZZZZ)V
    .locals 3
    .param p1, "locationHandler"    # Landroid/os/Handler;
    .param p2, "condition"    # I
    .param p3, "isDetectingTimeLimit"    # Z
    .param p4, "networkOnly"    # Z
    .param p5, "isRefresh"    # Z
    .param p6, "isNeedRefresh"    # Z
    .param p7, "isRetry"    # Z

    .prologue
    .line 273
    iput-boolean p4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->networkOnly:Z

    .line 274
    iput-boolean p7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->isRetry:Z

    .line 275
    const-string v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mcl pGCL retry :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 276
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->currentLocationHandler:Landroid/os/Handler;

    .line 280
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->th:Ljava/lang/Thread;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->th:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->cancelFlag:I

    if-nez v0, :cond_1

    .line 295
    :cond_0
    :goto_0
    return-void

    .line 283
    :cond_1
    if-eqz p1, :cond_0

    .line 287
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 289
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->setCancelFlag(I)V

    .line 290
    iput-boolean p6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->mIsRefresh:Z

    .line 294
    invoke-virtual {p0, p1, p2, p3, p5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->getLastLocation(Landroid/os/Handler;IZZ)V

    goto :goto_0
.end method

.method public removeLocationListener()V
    .locals 2

    .prologue
    .line 664
    const-string v0, ""

    const-string v1, "mcl rm LL"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 666
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->networkLocationManager:Landroid/location/LocationManager;

    if-eqz v0, :cond_0

    .line 667
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->networkLocationManager:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->locationListener:Landroid/location/LocationListener;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 669
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->gpsLocationManager:Landroid/location/LocationManager;

    if-eqz v0, :cond_1

    .line 670
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->gpsLocationManager:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->locationListener:Landroid/location/LocationListener;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 672
    :cond_1
    return-void
.end method

.method public removePassiveLocationListener()V
    .locals 2

    .prologue
    .line 675
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->passiveLocationManager:Landroid/location/LocationManager;

    if-eqz v0, :cond_0

    .line 676
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->passiveLocationManager:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->passiveLocationListener:Landroid/location/LocationListener;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 678
    :cond_0
    return-void
.end method

.method public setCancelFlag(I)V
    .locals 0
    .param p1, "cancelFlag"    # I

    .prologue
    .line 62
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->cancelFlag:I

    .line 63
    return-void
.end method

.method public setIsNewLocationApplied(Z)V
    .locals 0
    .param p1, "isNewLocationApplied"    # Z

    .prologue
    .line 70
    iput-boolean p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->isNewLocationApplied:Z

    .line 71
    return-void
.end method

.method public setLatitude(D)V
    .locals 0
    .param p1, "latitude"    # D

    .prologue
    .line 90
    iput-wide p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->latitude:D

    .line 91
    return-void
.end method

.method public setLongitude(D)V
    .locals 0
    .param p1, "longitude"    # D

    .prologue
    .line 98
    iput-wide p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->longitude:D

    .line 99
    return-void
.end method

.method public setUseProvider(Ljava/lang/String;)V
    .locals 0
    .param p1, "provider"    # Ljava/lang/String;

    .prologue
    .line 106
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->mclsProvider:Ljava/lang/String;

    .line 107
    return-void
.end method

.method public stopHttpThread()V
    .locals 2

    .prologue
    .line 314
    const-string v0, ""

    const-string v1, "mcl sHT"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 316
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->mHttpThread:Ljava/lang/Thread;

    if-eqz v0, :cond_1

    .line 317
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->mHttpThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 318
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->mHttpThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 320
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->mHttpThread:Ljava/lang/Thread;

    .line 322
    :cond_1
    return-void
.end method
