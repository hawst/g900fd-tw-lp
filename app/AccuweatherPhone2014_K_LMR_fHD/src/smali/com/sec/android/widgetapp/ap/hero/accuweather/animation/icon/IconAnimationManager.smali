.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;
.super Ljava/lang/Object;
.source "IconAnimationManager.java"


# static fields
.field public static final DEFAULT_PAINT_COLOR:I = -0xa0a0b

.field public static final ICON_ANI_DURATION:J = 0xbb8L


# instance fields
.field private mContext:Landroid/content/Context;

.field private mPaintColor:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "ctx"    # Landroid/content/Context;

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;->mContext:Landroid/content/Context;

    .line 39
    const v0, -0xa0a0b

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;->mPaintColor:I

    .line 42
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;->mContext:Landroid/content/Context;

    .line 43
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0066

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;->mPaintColor:I

    .line 44
    return-void
.end method


# virtual methods
.method public calcAnimationDuration(Landroid/view/ViewGroup;I)J
    .locals 5
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "sec"    # I

    .prologue
    .line 357
    const-wide/16 v0, 0x7d0

    .line 360
    .local v0, "endDuraton":J
    if-eqz p1, :cond_1

    .line 361
    const/4 v3, 0x0

    invoke-virtual {p1, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 362
    .local v2, "v":Landroid/view/View;
    if-eqz v2, :cond_1

    instance-of v3, v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;

    if-eqz v3, :cond_1

    .line 363
    instance-of v3, v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;

    if-nez v3, :cond_0

    instance-of v3, v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;

    if-nez v3, :cond_0

    instance-of v3, v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;

    if-eqz v3, :cond_1

    .line 366
    :cond_0
    const-wide/16 v0, 0x0

    .line 371
    .end local v2    # "v":Landroid/view/View;
    :cond_1
    mul-int/lit16 v3, p2, 0x3e8

    int-to-long v3, v3

    sub-long/2addr v3, v0

    return-wide v3
.end method

.method public cancelIconAnimation(Landroid/view/ViewGroup;)V
    .locals 5
    .param p1, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 294
    if-eqz p1, :cond_1

    .line 295
    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 296
    .local v1, "v":Landroid/view/View;
    if-eqz v1, :cond_0

    instance-of v2, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;

    if-eqz v2, :cond_0

    .line 297
    check-cast v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;

    .end local v1    # "v":Landroid/view/View;
    invoke-interface {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;->cancelAnimation()V

    .line 305
    :cond_0
    :goto_0
    return-void

    .line 300
    :cond_1
    const-string v2, ""

    const-string v3, "stopIconAnimation : parent layout is null"

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 302
    :catch_0
    move-exception v0

    .line 303
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, ""

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getIconAnimation(IZII)Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;
    .locals 4
    .param p1, "iconNum"    # I
    .param p2, "isDay"    # Z
    .param p3, "width"    # I
    .param p4, "height"    # I

    .prologue
    .line 56
    const-string v1, ""

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getIconAnimation : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", w/h = ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 59
    const/4 v0, 0x0

    .line 60
    .local v0, "icon":Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;
    packed-switch p1, :pswitch_data_0

    .line 178
    :pswitch_0
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;

    .end local v0    # "icon":Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;-><init>(Landroid/content/Context;)V

    .line 182
    .restart local v0    # "icon":Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;
    :goto_0
    invoke-virtual {p0, v0, p3, p4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;->setWidthAndHeight(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;II)V

    .line 184
    return-object v0

    .line 63
    :pswitch_1
    if-eqz p2, :cond_0

    .line 64
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;

    .end local v0    # "icon":Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;-><init>(Landroid/content/Context;)V

    .restart local v0    # "icon":Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;
    goto :goto_0

    .line 66
    :cond_0
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;

    .end local v0    # "icon":Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;-><init>(Landroid/content/Context;)V

    .line 68
    .restart local v0    # "icon":Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;
    goto :goto_0

    .line 72
    :pswitch_2
    if-eqz p2, :cond_1

    .line 73
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;

    .end local v0    # "icon":Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;-><init>(Landroid/content/Context;)V

    .restart local v0    # "icon":Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;
    goto :goto_0

    .line 75
    :cond_1
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;

    .end local v0    # "icon":Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;-><init>(Landroid/content/Context;)V

    .line 77
    .restart local v0    # "icon":Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;
    goto :goto_0

    .line 83
    :pswitch_3
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;

    .end local v0    # "icon":Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;-><init>(Landroid/content/Context;)V

    .line 84
    .restart local v0    # "icon":Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;
    goto :goto_0

    .line 86
    :pswitch_4
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;

    .end local v0    # "icon":Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FogIconView;-><init>(Landroid/content/Context;)V

    .line 87
    .restart local v0    # "icon":Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;
    goto :goto_0

    .line 92
    :pswitch_5
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;

    .end local v0    # "icon":Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;-><init>(Landroid/content/Context;)V

    .line 93
    .restart local v0    # "icon":Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;
    goto :goto_0

    .line 96
    :pswitch_6
    if-eqz p2, :cond_2

    .line 97
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyShowerIconView;

    .end local v0    # "icon":Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyShowerIconView;-><init>(Landroid/content/Context;)V

    .restart local v0    # "icon":Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;
    goto :goto_0

    .line 99
    :cond_2
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;

    .end local v0    # "icon":Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;-><init>(Landroid/content/Context;)V

    .line 101
    .restart local v0    # "icon":Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;
    goto :goto_0

    .line 107
    :pswitch_7
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;

    .end local v0    # "icon":Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;-><init>(Landroid/content/Context;)V

    .line 108
    .restart local v0    # "icon":Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;
    goto :goto_0

    .line 111
    :pswitch_8
    if-eqz p2, :cond_3

    .line 112
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;

    .end local v0    # "icon":Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;-><init>(Landroid/content/Context;)V

    .restart local v0    # "icon":Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;
    goto :goto_0

    .line 114
    :cond_3
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;

    .end local v0    # "icon":Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;-><init>(Landroid/content/Context;)V

    .line 116
    .restart local v0    # "icon":Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;
    goto :goto_0

    .line 119
    :pswitch_9
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;

    .end local v0    # "icon":Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;-><init>(Landroid/content/Context;)V

    .line 120
    .restart local v0    # "icon":Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;
    goto :goto_0

    .line 124
    :pswitch_a
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;

    .end local v0    # "icon":Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;-><init>(Landroid/content/Context;)V

    .line 125
    .restart local v0    # "icon":Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;
    goto :goto_0

    .line 128
    :pswitch_b
    if-eqz p2, :cond_4

    .line 129
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;

    .end local v0    # "icon":Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;-><init>(Landroid/content/Context;)V

    .restart local v0    # "icon":Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;
    goto/16 :goto_0

    .line 131
    :cond_4
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;

    .end local v0    # "icon":Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;-><init>(Landroid/content/Context;)V

    .line 133
    .restart local v0    # "icon":Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;
    goto/16 :goto_0

    .line 138
    :pswitch_c
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;

    .end local v0    # "icon":Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;-><init>(Landroid/content/Context;)V

    .line 139
    .restart local v0    # "icon":Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;
    goto/16 :goto_0

    .line 141
    :pswitch_d
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;

    .end local v0    # "icon":Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;-><init>(Landroid/content/Context;)V

    .line 142
    .restart local v0    # "icon":Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;
    goto/16 :goto_0

    .line 146
    :pswitch_e
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;

    .end local v0    # "icon":Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;-><init>(Landroid/content/Context;)V

    .line 147
    .restart local v0    # "icon":Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;
    goto/16 :goto_0

    .line 150
    :pswitch_f
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;

    .end local v0    # "icon":Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;-><init>(Landroid/content/Context;)V

    .line 151
    .restart local v0    # "icon":Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;
    goto/16 :goto_0

    .line 153
    :pswitch_10
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;

    .end local v0    # "icon":Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;-><init>(Landroid/content/Context;)V

    .line 154
    .restart local v0    # "icon":Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;
    goto/16 :goto_0

    .line 156
    :pswitch_11
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;

    .end local v0    # "icon":Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;-><init>(Landroid/content/Context;)V

    .line 157
    .restart local v0    # "icon":Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;
    goto/16 :goto_0

    .line 161
    :pswitch_12
    if-eqz p2, :cond_5

    .line 162
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;

    .end local v0    # "icon":Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;-><init>(Landroid/content/Context;)V

    .restart local v0    # "icon":Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;
    goto/16 :goto_0

    .line 164
    :cond_5
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;

    .end local v0    # "icon":Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;-><init>(Landroid/content/Context;)V

    .line 166
    .restart local v0    # "icon":Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;
    goto/16 :goto_0

    .line 170
    :pswitch_13
    if-eqz p2, :cond_6

    .line 171
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;

    .end local v0    # "icon":Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;-><init>(Landroid/content/Context;)V

    .restart local v0    # "icon":Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;
    goto/16 :goto_0

    .line 173
    :cond_6
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;

    .end local v0    # "icon":Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;-><init>(Landroid/content/Context;)V

    .line 175
    .restart local v0    # "icon":Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;
    goto/16 :goto_0

    .line 60
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_c
        :pswitch_e
        :pswitch_e
        :pswitch_e
        :pswitch_0
        :pswitch_0
        :pswitch_d
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_12
        :pswitch_13
        :pswitch_13
        :pswitch_13
        :pswitch_3
        :pswitch_5
        :pswitch_5
        :pswitch_7
        :pswitch_7
        :pswitch_a
        :pswitch_c
    .end packed-switch
.end method

.method public isRunningIconAnimation(Landroid/view/ViewGroup;)Z
    .locals 7
    .param p1, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v3, 0x0

    .line 315
    const/4 v1, 0x0

    .line 316
    .local v1, "ret":Z
    if-eqz p1, :cond_0

    .line 317
    const/4 v4, 0x0

    :try_start_0
    invoke-virtual {p1, v4}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 318
    .local v2, "v":Landroid/view/View;
    if-eqz v2, :cond_0

    instance-of v4, v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;

    if-eqz v4, :cond_0

    .line 319
    check-cast v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;

    .end local v2    # "v":Landroid/view/View;
    invoke-interface {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;->isRunning()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 325
    .end local v1    # "ret":Z
    :cond_0
    :goto_0
    return v1

    .line 323
    .restart local v1    # "ret":Z
    :catch_0
    move-exception v0

    .line 324
    .local v0, "e":Ljava/lang/Exception;
    const-string v4, ""

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Exception : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v3

    .line 325
    goto :goto_0
.end method

.method public isRunningIconAnimation(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;)Z
    .locals 5
    .param p1, "animation"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;

    .prologue
    .line 337
    const/4 v1, 0x0

    .line 339
    .local v1, "ret":Z
    if-eqz p1, :cond_0

    .line 340
    :try_start_0
    invoke-interface {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;->isRunning()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 346
    .end local v1    # "ret":Z
    :cond_0
    :goto_0
    return v1

    .line 344
    .restart local v1    # "ret":Z
    :catch_0
    move-exception v0

    .line 345
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, ""

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 346
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setWidthAndHeight(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;II)V
    .locals 4
    .param p1, "animation"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    const/high16 v3, 0x435c0000    # 220.0f

    .line 196
    invoke-interface {p1, p2, p3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;->setWidthAndHeight(II)V

    .line 198
    const/high16 v1, 0x3f800000    # 1.0f

    .line 200
    .local v1, "ratio":F
    if-gt p2, p3, :cond_0

    .line 201
    int-to-float v2, p2

    div-float v1, v2, v3

    .line 206
    :goto_0
    new-instance v0, Ljava/math/BigDecimal;

    float-to-double v2, v1

    invoke-direct {v0, v2, v3}, Ljava/math/BigDecimal;-><init>(D)V

    .line 207
    .local v0, "b":Ljava/math/BigDecimal;
    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;->mPaintColor:I

    invoke-interface {p1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;->setPaintColor(I)V

    .line 208
    const/4 v2, 0x5

    const/4 v3, 0x4

    invoke-virtual {v0, v2, v3}, Ljava/math/BigDecimal;->setScale(II)Ljava/math/BigDecimal;

    move-result-object v2

    invoke-virtual {v2}, Ljava/math/BigDecimal;->floatValue()F

    move-result v2

    invoke-interface {p1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;->setScale(F)V

    .line 209
    return-void

    .line 203
    .end local v0    # "b":Ljava/math/BigDecimal;
    :cond_0
    int-to-float v2, p3

    div-float v1, v2, v3

    goto :goto_0
.end method

.method public startIconAnimation(Landroid/view/ViewGroup;)V
    .locals 6
    .param p1, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 275
    if-eqz p1, :cond_1

    .line 276
    const/4 v3, 0x0

    :try_start_0
    invoke-virtual {p1, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 277
    .local v2, "v":Landroid/view/View;
    if-eqz v2, :cond_0

    instance-of v3, v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;

    if-eqz v3, :cond_0

    .line 278
    move-object v0, v2

    check-cast v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;

    move-object v3, v0

    invoke-interface {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;->isRunning()Z

    move-result v3

    if-nez v3, :cond_0

    .line 279
    check-cast v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;

    .end local v2    # "v":Landroid/view/View;
    invoke-interface {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;->startAnimation()V

    .line 288
    :cond_0
    :goto_0
    return-void

    .line 283
    :cond_1
    const-string v3, ""

    const-string v4, "startIconAnimation : parent layout is null"

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 285
    :catch_0
    move-exception v1

    .line 286
    .local v1, "e":Ljava/lang/Exception;
    const-string v3, ""

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Exception : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public startIconAnimation(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;)V
    .locals 4
    .param p1, "animation"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;

    .prologue
    .line 256
    if-eqz p1, :cond_0

    .line 257
    :try_start_0
    invoke-interface {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;->startAnimation()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 263
    :cond_0
    :goto_0
    return-void

    .line 260
    :catch_0
    move-exception v0

    .line 261
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, ""

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public stopIconAnimation(Landroid/view/ViewGroup;)V
    .locals 5
    .param p1, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 218
    if-eqz p1, :cond_1

    .line 219
    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 220
    .local v1, "v":Landroid/view/View;
    if-eqz v1, :cond_0

    instance-of v2, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;

    if-eqz v2, :cond_0

    .line 221
    check-cast v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;

    .end local v1    # "v":Landroid/view/View;
    invoke-interface {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;->stopAnimation()V

    .line 230
    :cond_0
    :goto_0
    return-void

    .line 224
    :cond_1
    const-string v2, ""

    const-string v3, "stopIconAnimation : parent layout is null"

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 227
    :catch_0
    move-exception v0

    .line 228
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, ""

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public stopIconAnimation(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;)V
    .locals 4
    .param p1, "animation"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;

    .prologue
    .line 239
    if-eqz p1, :cond_0

    :try_start_0
    invoke-interface {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;->isRunning()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 240
    invoke-interface {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;->stopAnimation()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 246
    :cond_0
    :goto_0
    return-void

    .line 243
    :catch_0
    move-exception v0

    .line 244
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, ""

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
