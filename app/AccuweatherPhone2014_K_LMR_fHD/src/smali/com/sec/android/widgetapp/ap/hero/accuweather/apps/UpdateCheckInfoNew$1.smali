.class Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfoNew$1;
.super Landroid/os/Handler;
.source "UpdateCheckInfoNew.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfoNew;->responseUpdateCheckVersion()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfoNew;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfoNew;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfoNew;

    .prologue
    .line 112
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfoNew$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfoNew;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v3, 0x1

    .line 114
    const-string v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "dialogHandler : msg.what = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(ZLjava/lang/String;Ljava/lang/String;)I

    .line 115
    iget v0, p1, Landroid/os/Message;->what:I

    if-nez v0, :cond_1

    .line 116
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfoNew$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfoNew;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfoNew;->startUserDialog()V

    .line 124
    :cond_0
    :goto_0
    return-void

    .line 117
    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    if-ne v0, v3, :cond_2

    .line 119
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfoNew$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfoNew;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfoNew;->newVersionSoftwareDialog()V

    goto :goto_0

    .line 120
    :cond_2
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 122
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfoNew$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfoNew;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfoNew;->noNewSoftwareDialog()V

    goto :goto_0
.end method
