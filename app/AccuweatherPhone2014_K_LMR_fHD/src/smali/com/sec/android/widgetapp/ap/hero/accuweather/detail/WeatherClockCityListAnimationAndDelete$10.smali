.class Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete$10;
.super Ljava/lang/Object;
.source "WeatherClockCityListAnimationAndDelete.java"

# interfaces
.implements Landroid/view/View$OnHoverListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;

    .prologue
    .line 700
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete$10;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "view"    # Landroid/view/View;
    .param p2, "motionevent"    # Landroid/view/MotionEvent;

    .prologue
    .line 703
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    const/16 v4, 0x9

    if-ne v3, v4, :cond_1

    .line 704
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 706
    .local v1, "sb":Ljava/lang/StringBuffer;
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete$ViewHolder;

    .line 707
    .local v2, "viewHolder":Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete$ViewHolder;
    iget-object v3, v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete$ViewHolder;->tts:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 709
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete$10;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;->mIsDeleteMode:Z
    invoke-static {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;->access$900(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 710
    const v3, 0x7f080073

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultCheckBox;

    .line 711
    .local v0, "checkbox":Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultCheckBox;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete$10;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0d0093

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 713
    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultCheckBox;->isChecked()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 714
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete$10;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0d0094

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 720
    .end local v0    # "checkbox":Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultCheckBox;
    :cond_0
    :goto_0
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 722
    const/4 v3, 0x0

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->length()I

    move-result v4

    invoke-virtual {v1, v3, v4}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    .line 725
    .end local v1    # "sb":Ljava/lang/StringBuffer;
    .end local v2    # "viewHolder":Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete$ViewHolder;
    :cond_1
    const/4 v3, 0x1

    return v3

    .line 716
    .restart local v0    # "checkbox":Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultCheckBox;
    .restart local v1    # "sb":Ljava/lang/StringBuffer;
    .restart local v2    # "viewHolder":Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete$ViewHolder;
    :cond_2
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete$10;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0d0095

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0
.end method
