.class Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$2;
.super Ljava/lang/Object;
.source "MediaEditGridItemLayout.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;


# direct methods
.method constructor <init>(Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$2;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;

    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 11
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    const v10, 0x7f020062

    const/16 v9, 0x64

    const/4 v7, 0x0

    .line 70
    sget-object v6, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v6}, Lcom/sec/android/app/storycam/AppDataManager;->getOriginalTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v4

    .line 71
    .local v4, "origElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    iget-object v6, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$2;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;

    # getter for: Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mMetaData:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;
    invoke-static {v6}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->access$1(Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;)Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;

    move-result-object v6

    # getter for: Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;->index:I
    invoke-static {v6}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;->access$1(Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;)I

    move-result v6

    invoke-virtual {v4, v6}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElement(I)Lcom/samsung/app/video/editor/external/Element;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/app/video/editor/external/Element;->getEditList()Ljava/util/List;

    move-result-object v5

    .line 72
    .local v5, "origTransEditList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Edit;>;"
    iget-object v6, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$2;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;

    # getter for: Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mPlayIconResId:I
    invoke-static {v6}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->access$2(Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;)I

    move-result v6

    if-ne v6, v10, :cond_2

    const/4 v3, 0x1

    .line 73
    .local v3, "isVolumeOnState":Z
    :goto_0
    if-eqz v3, :cond_4

    .line 74
    iget-object v6, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$2;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;

    # getter for: Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mPlayView:Landroid/widget/ImageView;
    invoke-static {v6}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->access$3(Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;)Landroid/widget/ImageView;

    move-result-object v6

    const v8, 0x7f07026e

    invoke-static {v8}, Lcom/sec/android/app/storycam/VEAppSpecific;->getStringValue(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 75
    iget-object v6, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$2;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;

    const v8, 0x7f02005e

    invoke-static {v6, v8}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->access$4(Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;I)V

    .line 76
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v6

    if-lt v2, v6, :cond_3

    .line 85
    :cond_0
    sget-object v6, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v6}, Lcom/sec/android/app/storycam/AppDataManager;->getCurrentTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v1

    .line 86
    .local v1, "currElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    if-eqz v1, :cond_1

    iget-object v6, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$2;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;

    # getter for: Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mMetaData:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;
    invoke-static {v6}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->access$1(Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;)Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;

    move-result-object v6

    # getter for: Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;->index:I
    invoke-static {v6}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;->access$1(Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;)I

    move-result v6

    invoke-virtual {v1, v6}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElement(I)Lcom/samsung/app/video/editor/external/Element;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 87
    sget-object v6, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v6}, Lcom/sec/android/app/storycam/AppDataManager;->getCurrentTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v6

    iget-object v8, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$2;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;

    # getter for: Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mMetaData:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;
    invoke-static {v8}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->access$1(Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;)Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;

    move-result-object v8

    # getter for: Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;->index:I
    invoke-static {v8}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;->access$1(Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;)I

    move-result v8

    invoke-virtual {v6, v8}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElement(I)Lcom/samsung/app/video/editor/external/Element;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/app/video/editor/external/Element;->getEditList()Ljava/util/List;

    move-result-object v0

    .line 88
    .local v0, "curTransEditList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Edit;>;"
    if-eqz v3, :cond_6

    .line 89
    const/4 v2, 0x0

    :goto_2
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v6

    if-lt v2, v6, :cond_5

    .line 96
    .end local v0    # "curTransEditList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Edit;>;"
    :cond_1
    iget-object v6, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$2;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;

    # getter for: Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mPlayView:Landroid/widget/ImageView;
    invoke-static {v6}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->access$3(Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;)Landroid/widget/ImageView;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$2;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;

    # getter for: Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mPlayIconResId:I
    invoke-static {v7}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->access$2(Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;)I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 97
    # getter for: Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;
    invoke-static {}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->access$5()Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    move-result-object v6

    invoke-interface {v6}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;->requestForOptionMenuRefresh()V

    .line 98
    return-void

    .end local v1    # "currElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    .end local v2    # "i":I
    .end local v3    # "isVolumeOnState":Z
    :cond_2
    move v3, v7

    .line 72
    goto :goto_0

    .line 77
    .restart local v2    # "i":I
    .restart local v3    # "isVolumeOnState":Z
    :cond_3
    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/app/video/editor/external/Edit;

    invoke-virtual {v6, v7}, Lcom/samsung/app/video/editor/external/Edit;->setVolumeLevel(I)V

    .line 76
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 79
    .end local v2    # "i":I
    :cond_4
    iget-object v6, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$2;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;

    # getter for: Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mPlayView:Landroid/widget/ImageView;
    invoke-static {v6}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->access$3(Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;)Landroid/widget/ImageView;

    move-result-object v6

    const v8, 0x7f07026d

    invoke-static {v8}, Lcom/sec/android/app/storycam/VEAppSpecific;->getStringValue(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 80
    iget-object v6, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$2;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;

    invoke-static {v6, v10}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->access$4(Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;I)V

    .line 81
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_3
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v6

    if-ge v2, v6, :cond_0

    .line 82
    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/app/video/editor/external/Edit;

    invoke-virtual {v6, v9}, Lcom/samsung/app/video/editor/external/Edit;->setVolumeLevel(I)V

    .line 81
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 90
    .restart local v0    # "curTransEditList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Edit;>;"
    .restart local v1    # "currElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    :cond_5
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/app/video/editor/external/Edit;

    invoke-virtual {v6, v7}, Lcom/samsung/app/video/editor/external/Edit;->setVolumeLevel(I)V

    .line 89
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 92
    :cond_6
    const/4 v2, 0x0

    :goto_4
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v6

    if-ge v2, v6, :cond_1

    .line 93
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/app/video/editor/external/Edit;

    invoke-virtual {v6, v9}, Lcom/samsung/app/video/editor/external/Edit;->setVolumeLevel(I)V

    .line 92
    add-int/lit8 v2, v2, 0x1

    goto :goto_4
.end method
