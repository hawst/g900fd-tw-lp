.class Lcom/sec/android/app/storycam/view/PreviewViewGroup$7;
.super Ljava/lang/Object;
.source "PreviewViewGroup.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/storycam/view/PreviewViewGroup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$7;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    .line 694
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 6
    .param p1, "view"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/16 v3, 0x16

    const/16 v2, 0x15

    const/4 v5, 0x2

    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 698
    if-eq p2, v3, :cond_0

    if-ne p2, v2, :cond_2

    .line 699
    :cond_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_2

    .line 700
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$7;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    # getter for: Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mPreview:Lcom/sec/android/app/ve/PreviewPlayerInterface;
    invoke-static {v0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$0(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)Lcom/sec/android/app/ve/PreviewPlayerInterface;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$7;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    # getter for: Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mPreview:Lcom/sec/android/app/ve/PreviewPlayerInterface;
    invoke-static {v0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$0(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)Lcom/sec/android/app/ve/PreviewPlayerInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/ve/PreviewPlayerInterface;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 701
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$7;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    invoke-virtual {v0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->pause_player()V

    .line 702
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$7;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    invoke-static {v0, v1}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$3(Lcom/sec/android/app/storycam/view/PreviewViewGroup;Z)V

    .line 717
    :cond_1
    :goto_0
    return v4

    .line 705
    :cond_2
    if-eq p2, v3, :cond_3

    if-ne p2, v2, :cond_1

    .line 706
    :cond_3
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-ne v0, v1, :cond_1

    .line 707
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$7;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    # getter for: Lcom/sec/android/app/storycam/view/PreviewViewGroup;->wasPlayingBeforeProgressTouch:Z
    invoke-static {v0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$8(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$7;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    # getter for: Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mProgressBar:Landroid/widget/SeekBar;
    invoke-static {v0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$2(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)Landroid/widget/SeekBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$7;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    # getter for: Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mProgressBar:Landroid/widget/SeekBar;
    invoke-static {v1}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$2(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)Landroid/widget/SeekBar;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getMax()I

    move-result v1

    if-eq v0, v1, :cond_4

    .line 708
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$7;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    # getter for: Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$5(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 709
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$7;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    # getter for: Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$5(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)Landroid/os/Handler;

    move-result-object v0

    const-wide/16 v2, 0x32

    invoke-virtual {v0, v5, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 710
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$7;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    # getter for: Lcom/sec/android/app/storycam/view/PreviewViewGroup;->wasPlayingBeforeProgressTouch:Z
    invoke-static {v0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$8(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 712
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$7;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    invoke-static {v0, v4}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$3(Lcom/sec/android/app/storycam/view/PreviewViewGroup;Z)V

    .line 713
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$7;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    invoke-virtual {v0, v4}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->setPlayButtonVisibility(I)V

    goto :goto_0
.end method
