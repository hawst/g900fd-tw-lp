.class public Lcom/sec/android/app/storycam/projectsharing/ProjectPluginManager$AudioEffectPluginImpl;
.super Ljava/lang/Object;
.source "ProjectPluginManager.java"

# interfaces
.implements Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework$AudioEffectPlugin;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/storycam/projectsharing/ProjectPluginManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AudioEffectPluginImpl"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 341
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public mapEffectFromGeneric(I)I
    .locals 1
    .param p1, "genericAudioEffect"    # I

    .prologue
    .line 366
    const/4 v0, 0x0

    .line 367
    .local v0, "internalEffect":I
    packed-switch p1, :pswitch_data_0

    .line 382
    :goto_0
    return v0

    .line 369
    :pswitch_0
    const/16 v0, 0x3b

    .line 370
    goto :goto_0

    .line 372
    :pswitch_1
    const/16 v0, 0x3f

    .line 373
    goto :goto_0

    .line 375
    :pswitch_2
    const/16 v0, 0x42

    .line 376
    goto :goto_0

    .line 378
    :pswitch_3
    const/16 v0, 0x43

    .line 379
    goto :goto_0

    .line 367
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public mapEffectToGeneric(I)I
    .locals 1
    .param p1, "internalAudioEffect"    # I

    .prologue
    .line 345
    const/4 v0, 0x0

    .line 346
    .local v0, "genericEffect":I
    packed-switch p1, :pswitch_data_0

    .line 361
    :goto_0
    :pswitch_0
    return v0

    .line 348
    :pswitch_1
    const/4 v0, 0x1

    .line 349
    goto :goto_0

    .line 351
    :pswitch_2
    const/4 v0, 0x2

    .line 352
    goto :goto_0

    .line 354
    :pswitch_3
    const/4 v0, 0x3

    .line 355
    goto :goto_0

    .line 357
    :pswitch_4
    const/4 v0, 0x4

    .line 358
    goto :goto_0

    .line 346
    nop

    :pswitch_data_0
    .packed-switch 0x3b
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
