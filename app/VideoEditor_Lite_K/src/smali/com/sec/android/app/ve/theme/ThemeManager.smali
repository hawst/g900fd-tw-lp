.class public Lcom/sec/android/app/ve/theme/ThemeManager;
.super Ljava/lang/Object;
.source "ThemeManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/ve/theme/ThemeManager$Adapter;
    }
.end annotation


# instance fields
.field private mAdapter:Lcom/sec/android/app/ve/theme/ThemeManager$Adapter;

.field private final mThemeList:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/sec/android/app/ve/theme/Theme;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/ve/theme/ThemeManager;->mThemeList:Ljava/util/LinkedHashMap;

    .line 16
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/ve/theme/ThemeManager;->mAdapter:Lcom/sec/android/app/ve/theme/ThemeManager$Adapter;

    .line 14
    return-void
.end method


# virtual methods
.method public addTheme(Lcom/sec/android/app/ve/theme/Theme;)V
    .locals 2
    .param p1, "theme"    # Lcom/sec/android/app/ve/theme/Theme;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/android/app/ve/theme/ThemeManager;->mThemeList:Ljava/util/LinkedHashMap;

    invoke-virtual {p1}, Lcom/sec/android/app/ve/theme/Theme;->getThemeID()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 20
    return-void
.end method

.method public getThemeAssetManager(I)Landroid/content/res/AssetManager;
    .locals 3
    .param p1, "themeID"    # I

    .prologue
    .line 143
    iget-object v1, p0, Lcom/sec/android/app/ve/theme/ThemeManager;->mThemeList:Ljava/util/LinkedHashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/ve/theme/Theme;

    .line 144
    .local v0, "theme":Lcom/sec/android/app/ve/theme/Theme;
    if-nez v0, :cond_0

    .line 145
    const/4 v1, 0x0

    .line 147
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/app/ve/theme/Theme;->getThemeAssetManager()Landroid/content/res/AssetManager;

    move-result-object v1

    goto :goto_0
.end method

.method public getThemeCount()I
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/ve/theme/ThemeManager;->mThemeList:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->size()I

    move-result v0

    return v0
.end method

.method public getThemeDataPath(I)Ljava/lang/String;
    .locals 3
    .param p1, "themeID"    # I

    .prologue
    .line 65
    iget-object v1, p0, Lcom/sec/android/app/ve/theme/ThemeManager;->mThemeList:Ljava/util/LinkedHashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/ve/theme/Theme;

    .line 66
    .local v0, "theme":Lcom/sec/android/app/ve/theme/Theme;
    if-nez v0, :cond_0

    .line 67
    const/4 v1, 0x0

    .line 69
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/app/ve/theme/Theme;->getThemeDataPath()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getThemeDescription(I)Ljava/lang/String;
    .locals 3
    .param p1, "themeID"    # I

    .prologue
    .line 112
    iget-object v1, p0, Lcom/sec/android/app/ve/theme/ThemeManager;->mThemeList:Ljava/util/LinkedHashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/ve/theme/Theme;

    .line 113
    .local v0, "theme":Lcom/sec/android/app/ve/theme/Theme;
    if-nez v0, :cond_0

    .line 114
    const/4 v1, 0x0

    .line 116
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/app/ve/theme/Theme;->getThemeDescription()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getThemeDisplayName(I)Ljava/lang/String;
    .locals 3
    .param p1, "themeID"    # I

    .prologue
    .line 48
    iget-object v1, p0, Lcom/sec/android/app/ve/theme/ThemeManager;->mThemeList:Ljava/util/LinkedHashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/ve/theme/Theme;

    .line 49
    .local v0, "theme":Lcom/sec/android/app/ve/theme/Theme;
    if-nez v0, :cond_0

    .line 50
    const/4 v1, 0x0

    .line 52
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/app/ve/theme/Theme;->getThemeDisplayName()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getThemeDownloadAlertMessage(I)Ljava/lang/String;
    .locals 3
    .param p1, "themeID"    # I

    .prologue
    .line 134
    iget-object v1, p0, Lcom/sec/android/app/ve/theme/ThemeManager;->mThemeList:Ljava/util/LinkedHashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/ve/theme/Theme;

    .line 135
    .local v0, "theme":Lcom/sec/android/app/ve/theme/Theme;
    if-nez v0, :cond_0

    .line 136
    const/4 v1, 0x0

    .line 138
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/app/ve/theme/Theme;->getThemeDownloadAlertMessage()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getThemeDrawable(IZ)Landroid/graphics/drawable/Drawable;
    .locals 3
    .param p1, "themeID"    # I
    .param p2, "createIfRequired"    # Z

    .prologue
    .line 85
    iget-object v1, p0, Lcom/sec/android/app/ve/theme/ThemeManager;->mThemeList:Ljava/util/LinkedHashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/ve/theme/Theme;

    .line 86
    .local v0, "theme":Lcom/sec/android/app/ve/theme/Theme;
    if-nez v0, :cond_0

    .line 87
    const/4 v1, 0x0

    .line 89
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0, p2}, Lcom/sec/android/app/ve/theme/Theme;->getThemeDrawable(Z)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    goto :goto_0
.end method

.method public getThemeDuration(I)J
    .locals 4
    .param p1, "themeID"    # I

    .prologue
    .line 42
    iget-object v1, p0, Lcom/sec/android/app/ve/theme/ThemeManager;->mThemeList:Ljava/util/LinkedHashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/ve/theme/Theme;

    .line 43
    .local v0, "theme":Lcom/sec/android/app/ve/theme/Theme;
    if-nez v0, :cond_0

    .line 44
    const-wide/16 v2, 0x0

    .line 45
    :goto_0
    return-wide v2

    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/app/ve/theme/Theme;->getThemeDuration()J

    move-result-wide v2

    goto :goto_0
.end method

.method public getThemeJSONFilePath(I)Ljava/lang/String;
    .locals 3
    .param p1, "themeID"    # I

    .prologue
    .line 94
    iget-object v1, p0, Lcom/sec/android/app/ve/theme/ThemeManager;->mThemeList:Ljava/util/LinkedHashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/ve/theme/Theme;

    .line 95
    .local v0, "theme":Lcom/sec/android/app/ve/theme/Theme;
    if-nez v0, :cond_0

    .line 96
    const/4 v1, 0x0

    .line 98
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/app/ve/theme/Theme;->getThemeJSONFilePath()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getThemeName(I)Ljava/lang/String;
    .locals 3
    .param p1, "themeID"    # I

    .prologue
    .line 34
    iget-object v1, p0, Lcom/sec/android/app/ve/theme/ThemeManager;->mThemeList:Ljava/util/LinkedHashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/ve/theme/Theme;

    .line 35
    .local v0, "theme":Lcom/sec/android/app/ve/theme/Theme;
    if-nez v0, :cond_0

    .line 36
    const/4 v1, 0x0

    .line 38
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/app/ve/theme/Theme;->getThemeName()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getThemePackage(I)Ljava/lang/String;
    .locals 3
    .param p1, "themeID"    # I

    .prologue
    .line 56
    iget-object v1, p0, Lcom/sec/android/app/ve/theme/ThemeManager;->mThemeList:Ljava/util/LinkedHashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/ve/theme/Theme;

    .line 57
    .local v0, "theme":Lcom/sec/android/app/ve/theme/Theme;
    if-nez v0, :cond_0

    .line 58
    const/4 v1, 0x0

    .line 60
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/app/ve/theme/Theme;->getThemePackage()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getThemePreviewfromRaw(I)Landroid/net/Uri;
    .locals 3
    .param p1, "themeID"    # I

    .prologue
    .line 76
    iget-object v1, p0, Lcom/sec/android/app/ve/theme/ThemeManager;->mThemeList:Ljava/util/LinkedHashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/ve/theme/Theme;

    .line 77
    .local v0, "theme":Lcom/sec/android/app/ve/theme/Theme;
    if-nez v0, :cond_0

    .line 78
    const/4 v1, 0x0

    .line 79
    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "android.resource://"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/android/app/ve/theme/Theme;->getThemePathFromRaw()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    goto :goto_0
.end method

.method public init()V
    .locals 4

    .prologue
    .line 24
    iget-object v2, p0, Lcom/sec/android/app/ve/theme/ThemeManager;->mThemeList:Ljava/util/LinkedHashMap;

    invoke-virtual {v2}, Ljava/util/LinkedHashMap;->size()I

    move-result v0

    .line 25
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v0, :cond_0

    .line 27
    return-void

    .line 26
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/ve/theme/ThemeManager;->mThemeList:Ljava/util/LinkedHashMap;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/ve/theme/Theme;

    invoke-virtual {v2}, Lcom/sec/android/app/ve/theme/Theme;->initializeTheme()Z

    .line 25
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public isSamsungAppsAvailable()Z
    .locals 4

    .prologue
    .line 151
    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getAppPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 153
    .local v1, "packageManager":Landroid/content/pm/PackageManager;
    :try_start_0
    const-string v2, "com.sec.android.app.samsungapps"

    const/16 v3, 0x80

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 158
    const/4 v2, 0x1

    :goto_0
    return v2

    .line 155
    :catch_0
    move-exception v0

    .line 156
    .local v0, "exception":Ljava/lang/Exception;
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public isThemeDownloaded(I)Z
    .locals 3
    .param p1, "themeID"    # I

    .prologue
    .line 103
    iget-object v1, p0, Lcom/sec/android/app/ve/theme/ThemeManager;->mThemeList:Ljava/util/LinkedHashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/ve/theme/Theme;

    .line 104
    .local v0, "theme":Lcom/sec/android/app/ve/theme/Theme;
    if-nez v0, :cond_0

    .line 105
    const/4 v1, 0x0

    .line 107
    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/app/ve/theme/Theme;->isThemeDownloaded()Z

    move-result v1

    goto :goto_0
.end method

.method public setAdapter(Lcom/sec/android/app/ve/theme/ThemeManager$Adapter;)V
    .locals 0
    .param p1, "adap"    # Lcom/sec/android/app/ve/theme/ThemeManager$Adapter;

    .prologue
    .line 120
    iput-object p1, p0, Lcom/sec/android/app/ve/theme/ThemeManager;->mAdapter:Lcom/sec/android/app/ve/theme/ThemeManager$Adapter;

    .line 121
    return-void
.end method

.method public startDownload(Landroid/app/Activity;I)V
    .locals 3
    .param p1, "actvt"    # Landroid/app/Activity;
    .param p2, "themeID"    # I

    .prologue
    .line 125
    iget-object v1, p0, Lcom/sec/android/app/ve/theme/ThemeManager;->mThemeList:Ljava/util/LinkedHashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/ve/theme/Theme;

    .line 126
    .local v0, "theme":Lcom/sec/android/app/ve/theme/Theme;
    if-nez v0, :cond_0

    .line 130
    :goto_0
    return-void

    .line 129
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/ve/theme/ThemeManager;->mThemeList:Ljava/util/LinkedHashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/ve/theme/Theme;

    iget-object v2, p0, Lcom/sec/android/app/ve/theme/ThemeManager;->mAdapter:Lcom/sec/android/app/ve/theme/ThemeManager$Adapter;

    invoke-virtual {v1, v2, p1, p2}, Lcom/sec/android/app/ve/theme/Theme;->startDownload(Lcom/sec/android/app/ve/theme/ThemeManager$Adapter;Landroid/app/Activity;I)V

    goto :goto_0
.end method
