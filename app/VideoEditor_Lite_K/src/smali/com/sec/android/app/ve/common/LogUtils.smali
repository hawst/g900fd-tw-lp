.class public Lcom/sec/android/app/ve/common/LogUtils;
.super Ljava/lang/Object;
.source "LogUtils.java"


# static fields
.field private static DEBUG:Z = false

.field public static final TAG:Ljava/lang/String; = "VideoEditor_AutomationTest"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/ve/common/LogUtils;->DEBUG:Z

    .line 13
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static checkDebugOptions(Landroid/app/Activity;)V
    .locals 2
    .param p0, "instance"    # Landroid/app/Activity;

    .prologue
    .line 32
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/app/video/editor/external/NativeInterface;->_currentBinaryType(I)V

    .line 33
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/ve/common/LogUtils;->DEBUG:Z

    .line 34
    return-void
.end method

.method public static criticalLog(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "logMessage"    # Ljava/lang/String;

    .prologue
    .line 28
    invoke-static {p0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 29
    return-void
.end method

.method public static log(Ljava/lang/String;)V
    .locals 1
    .param p0, "logMessage"    # Ljava/lang/String;

    .prologue
    .line 16
    sget-boolean v0, Lcom/sec/android/app/ve/common/LogUtils;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 17
    const-string v0, "VideoEditorAppTab"

    invoke-static {v0, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 19
    :cond_0
    return-void
.end method

.method public static log(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "logMessage"    # Ljava/lang/String;

    .prologue
    .line 22
    sget-boolean v0, Lcom/sec/android/app/ve/common/LogUtils;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 23
    invoke-static {p0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 25
    :cond_0
    return-void
.end method
