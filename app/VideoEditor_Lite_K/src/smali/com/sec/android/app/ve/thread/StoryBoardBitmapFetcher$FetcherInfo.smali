.class public Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher$FetcherInfo;
.super Ljava/lang/Object;
.source "StoryBoardBitmapFetcher.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FetcherInfo"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field callback:Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher$FetcherCallback;

.field filePath:Ljava/lang/String;

.field filename_pos:Ljava/lang/String;

.field isAssetResource:Z

.field mAssetManager:Landroid/content/res/AssetManager;

.field mMediaEmpty:Z

.field time:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 147
    const-class v0, Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher$FetcherInfo;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;JLcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher$FetcherCallback;Ljava/lang/String;Landroid/content/res/AssetManager;ZZ)V
    .locals 0
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "time"    # J
    .param p4, "callback"    # Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher$FetcherCallback;
    .param p5, "filename_pos"    # Ljava/lang/String;
    .param p6, "am"    # Landroid/content/res/AssetManager;
    .param p7, "isAssetResource"    # Z
    .param p8, "mMediaEmpty"    # Z

    .prologue
    .line 155
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 156
    iput-object p1, p0, Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher$FetcherInfo;->filePath:Ljava/lang/String;

    .line 157
    iput-wide p2, p0, Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher$FetcherInfo;->time:J

    .line 158
    iput-object p4, p0, Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher$FetcherInfo;->callback:Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher$FetcherCallback;

    .line 159
    iput-object p5, p0, Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher$FetcherInfo;->filename_pos:Ljava/lang/String;

    .line 160
    iput-boolean p7, p0, Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher$FetcherInfo;->isAssetResource:Z

    .line 161
    iput-object p6, p0, Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher$FetcherInfo;->mAssetManager:Landroid/content/res/AssetManager;

    .line 162
    iput-boolean p8, p0, Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher$FetcherInfo;->mMediaEmpty:Z

    .line 163
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 172
    instance-of v1, p1, Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher$FetcherInfo;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 173
    check-cast v0, Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher$FetcherInfo;

    .line 175
    .local v0, "finfo":Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher$FetcherInfo;
    iget-object v1, p0, Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher$FetcherInfo;->filePath:Ljava/lang/String;

    iget-object v2, v0, Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher$FetcherInfo;->filePath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 176
    iget-wide v2, p0, Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher$FetcherInfo;->time:J

    iget-wide v4, v0, Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher$FetcherInfo;->time:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 177
    iget-object v1, p0, Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher$FetcherInfo;->callback:Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher$FetcherCallback;

    iget-object v2, v0, Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher$FetcherInfo;->callback:Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher$FetcherCallback;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 178
    const/4 v1, 0x1

    .line 182
    .end local v0    # "finfo":Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher$FetcherInfo;
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 166
    sget-boolean v0, Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher$FetcherInfo;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "hashCode not designed"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 167
    :cond_0
    const/4 v0, 0x0

    return v0
.end method
