.class public Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;
.super Ljava/lang/Object;
.source "MediaEditGridItemLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MetaData"
.end annotation


# instance fields
.field private bSupportedMedia:Z

.field private callback:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MediaGridItemInterface;

.field private index:I

.field private mMediaType:I

.field private mThumbProvider:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$ThumbnailDataProvider;


# direct methods
.method public constructor <init>(ILcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MediaGridItemInterface;Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$ThumbnailDataProvider;IZ)V
    .locals 0
    .param p1, "n"    # I
    .param p2, "cbk"    # Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MediaGridItemInterface;
    .param p3, "thumbProvider"    # Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$ThumbnailDataProvider;
    .param p4, "mediaType"    # I
    .param p5, "supportedMedia"    # Z

    .prologue
    .line 332
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 333
    iput p1, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;->index:I

    .line 334
    iput-object p2, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;->callback:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MediaGridItemInterface;

    .line 335
    iput-object p3, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;->mThumbProvider:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$ThumbnailDataProvider;

    .line 336
    iput p4, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;->mMediaType:I

    .line 337
    iput-boolean p5, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;->bSupportedMedia:Z

    .line 338
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;)Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MediaGridItemInterface;
    .locals 1

    .prologue
    .line 327
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;->callback:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MediaGridItemInterface;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;)I
    .locals 1

    .prologue
    .line 326
    iget v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;->index:I

    return v0
.end method

.method static synthetic access$2(Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;)Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$ThumbnailDataProvider;
    .locals 1

    .prologue
    .line 328
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;->mThumbProvider:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$ThumbnailDataProvider;

    return-object v0
.end method

.method static synthetic access$3(Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;)I
    .locals 1

    .prologue
    .line 329
    iget v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;->mMediaType:I

    return v0
.end method

.method static synthetic access$4(Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;)Z
    .locals 1

    .prologue
    .line 330
    iget-boolean v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;->bSupportedMedia:Z

    return v0
.end method

.method static synthetic access$5(Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MediaGridItemInterface;)V
    .locals 0

    .prologue
    .line 327
    iput-object p1, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;->callback:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MediaGridItemInterface;

    return-void
.end method

.method static synthetic access$6(Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;I)V
    .locals 0

    .prologue
    .line 326
    iput p1, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;->index:I

    return-void
.end method

.method static synthetic access$7(Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$ThumbnailDataProvider;)V
    .locals 0

    .prologue
    .line 328
    iput-object p1, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;->mThumbProvider:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$ThumbnailDataProvider;

    return-void
.end method

.method static synthetic access$8(Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;I)V
    .locals 0

    .prologue
    .line 329
    iput p1, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;->mMediaType:I

    return-void
.end method

.method static synthetic access$9(Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;Z)V
    .locals 0

    .prologue
    .line 330
    iput-boolean p1, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;->bSupportedMedia:Z

    return-void
.end method
