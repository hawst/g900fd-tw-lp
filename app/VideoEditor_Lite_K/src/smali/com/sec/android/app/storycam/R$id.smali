.class public final Lcom/sec/android/app/storycam/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/storycam/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final VideoPreview:I = 0x7f0d0040

.field public static final action_bar_bgm_edit:I = 0x7f0d0008

.field public static final add_bgm_icon:I = 0x7f0d0019

.field public static final add_bgm_icon_layout:I = 0x7f0d0018

.field public static final background_image_view:I = 0x7f0d004f

.field public static final background_image_view_end:I = 0x7f0d0055

.field public static final bgmGridView:I = 0x7f0d0014

.field public static final bgmLayout:I = 0x7f0d000e

.field public static final bgmLayoutParent:I = 0x7f0d000d

.field public static final bgm_bottom_text:I = 0x7f0d0016

.field public static final bgm_duration_list:I = 0x7f0d000c

.field public static final bgm_icon_text:I = 0x7f0d001a

.field public static final bgm_layout:I = 0x7f0d0011

.field public static final bgm_layout_cover:I = 0x7f0d0017

.field public static final bgm_left_text:I = 0x7f0d0013

.field public static final bgm_message:I = 0x7f0d000b

.field public static final bgm_modes:I = 0x7f0d000a

.field public static final bgm_right_text:I = 0x7f0d0015

.field public static final bgm_thumbnail:I = 0x7f0d001c

.field public static final bgm_thumbnail_border:I = 0x7f0d001e

.field public static final bgm_thumbnail_layout:I = 0x7f0d001b

.field public static final bgm_thumbnail_selected:I = 0x7f0d001d

.field public static final bgm_top_text:I = 0x7f0d0012

.field public static final cancel_text:I = 0x7f0d0006

.field public static final cancelitem:I = 0x7f0d005f

.field public static final captionEditText:I = 0x7f0d0005

.field public static final caption_container:I = 0x7f0d0004

.field public static final checkbox:I = 0x7f0d0020

.field public static final current_storyboard_time:I = 0x7f0d0044

.field public static final custom_radio_button:I = 0x7f0d0023

.field public static final done_text:I = 0x7f0d0009

.field public static final dummy_layout:I = 0x7f0d0007

.field public static final editMediaLayout:I = 0x7f0d0027

.field public static final editMediaUnsupported:I = 0x7f0d002e

.field public static final editMedia_play:I = 0x7f0d0032

.field public static final editMedia_thumbnail:I = 0x7f0d002d

.field public static final editMedia_thumbnail_border:I = 0x7f0d002c

.field public static final edit_cover_layout:I = 0x7f0d003c

.field public static final element_uhd:I = 0x7f0d0030

.field public static final element_wqhd:I = 0x7f0d0031

.field public static final export_done_text:I = 0x7f0d0038

.field public static final export_filename:I = 0x7f0d0036

.field public static final filename:I = 0x7f0d0049

.field public static final full_storyboard_time:I = 0x7f0d0046

.field public static final gesture_overlay_help:I = 0x7f0d003e

.field public static final horizontal_scroll_view:I = 0x7f0d002a

.field public static final hover_preview:I = 0x7f0d0025

.field public static final hover_preview_unsupported_icon:I = 0x7f0d0026

.field public static final img_contentView:I = 0x7f0d0024

.field public static final layoutNeedingRepositioning:I = 0x7f0d0002

.field public static final lite_bgm_menuitem:I = 0x7f0d005c

.field public static final lite_defaultview:I = 0x7f0d0001

.field public static final lite_edit_custom:I = 0x7f0d005e

.field public static final lite_edit_menuitem:I = 0x7f0d005d

.field public static final lite_rootview:I = 0x7f0d0000

.field public static final lite_seekbar_parent:I = 0x7f0d0042

.field public static final mediaEditGridView:I = 0x7f0d0029

.field public static final mediaEditGridView1:I = 0x7f0d002b

.field public static final media_duration:I = 0x7f0d0033

.field public static final media_time_text:I = 0x7f0d0034

.field public static final music_title:I = 0x7f0d000f

.field public static final notification_icon_layout:I = 0x7f0d003a

.field public static final overlay_help_button:I = 0x7f0d003d

.field public static final overlay_help_content:I = 0x7f0d003f

.field public static final percent_progress:I = 0x7f0d0039

.field public static final preview_bottom_divider:I = 0x7f0d0047

.field public static final preview_parent:I = 0x7f0d0003

.field public static final previewlayout:I = 0x7f0d0041

.field public static final previewplay:I = 0x7f0d0043

.field public static final progress:I = 0x7f0d004a

.field public static final progressBar:I = 0x7f0d004b

.field public static final progress_bar:I = 0x7f0d0045

.field public static final resolution_text:I = 0x7f0d0021

.field public static final resolution_text_desc:I = 0x7f0d0022

.field public static final rightDividerView:I = 0x7f0d0010

.field public static final saveitem:I = 0x7f0d0060

.field public static final scrollView:I = 0x7f0d004d

.field public static final status_icon:I = 0x7f0d0035

.field public static final status_progress:I = 0x7f0d003b

.field public static final status_text:I = 0x7f0d0037

.field public static final textview:I = 0x7f0d001f

.field public static final themeSelectorGrid:I = 0x7f0d004e

.field public static final theme_name:I = 0x7f0d0053

.field public static final theme_thumbnail:I = 0x7f0d0050

.field public static final theme_thumbnail_border:I = 0x7f0d0051

.field public static final theme_thumbnail_coverUHD:I = 0x7f0d0054

.field public static final theme_thumbnail_selected:I = 0x7f0d0052

.field public static final theme_video_length:I = 0x7f0d0048

.field public static final themelist_layout:I = 0x7f0d004c

.field public static final unsupported_icon:I = 0x7f0d002f

.field public static final vertical_scroll_view:I = 0x7f0d0028

.field public static final videoPreview:I = 0x7f0d0057

.field public static final videoPreview_unsupported_icon:I = 0x7f0d0058

.field public static final videoView:I = 0x7f0d0059

.field public static final video_contentView:I = 0x7f0d0056

.field public static final wait_progress_text:I = 0x7f0d005b

.field public static final wait_progress_view:I = 0x7f0d005a


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1365
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
