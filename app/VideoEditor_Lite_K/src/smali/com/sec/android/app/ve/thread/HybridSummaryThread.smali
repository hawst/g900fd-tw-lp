.class public Lcom/sec/android/app/ve/thread/HybridSummaryThread;
.super Ljava/lang/Thread;
.source "HybridSummaryThread.java"


# static fields
.field public static final MSG_DELAY_SUMMARY_WAKELOCK:I = 0x7d

.field public static final MSG_SUMMARY_FAILURE:I = 0x7a

.field public static final MSG_SUMMARY_FILE_ERROR:I = 0x7e

.field public static final MSG_SUMMARY_STOPPED:I = 0x7c

.field public static final MSG_SUMMARY_SUCCESS:I = 0x7b

.field public static final SUMMARY_ANALYSIS_SUCCESS:I = 0x1

.field static final SUMMARY_ERROR:I = 0x1

.field static final SUMMARY_SUCCESS:I = 0x0

.field public static final VIDEO_SUMMARY:I = 0x3eb


# instance fields
.field private analysisRequired:Z

.field private handler:Landroid/os/Handler;

.field private mContext:Landroid/content/Context;

.field private mCurrentTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

.field private mForceStop:Z

.field mHandler:Landroid/os/Handler;

.field mSummaryDuration:J

.field private mode:I

.field private path:[Ljava/lang/String;

.field posAfterSummPlay:J

.field previewPlayafterSum:Z

.field private summaryTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

.field wakelock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method public constructor <init>(Landroid/os/Handler;ILandroid/content/Context;Lcom/samsung/app/video/editor/external/TranscodeElement;JLandroid/os/Bundle;ILcom/samsung/app/video/editor/external/TranscodeElement;ZZJ)V
    .locals 2
    .param p1, "handler"    # Landroid/os/Handler;
    .param p2, "operation"    # I
    .param p3, "context"    # Landroid/content/Context;
    .param p4, "transcodeElement"    # Lcom/samsung/app/video/editor/external/TranscodeElement;
    .param p5, "videoSummaryTime"    # J
    .param p7, "aBundle"    # Landroid/os/Bundle;
    .param p8, "mode"    # I
    .param p9, "output"    # Lcom/samsung/app/video/editor/external/TranscodeElement;
    .param p10, "analysis"    # Z
    .param p11, "previewPlay"    # Z
    .param p12, "pos"    # J

    .prologue
    const/4 v1, 0x1

    .line 46
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 28
    new-array v0, v1, [Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/ve/thread/HybridSummaryThread;->path:[Ljava/lang/String;

    .line 36
    iput-boolean v1, p0, Lcom/sec/android/app/ve/thread/HybridSummaryThread;->analysisRequired:Z

    .line 226
    new-instance v0, Lcom/sec/android/app/ve/thread/HybridSummaryThread$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/ve/thread/HybridSummaryThread$1;-><init>(Lcom/sec/android/app/ve/thread/HybridSummaryThread;)V

    iput-object v0, p0, Lcom/sec/android/app/ve/thread/HybridSummaryThread;->handler:Landroid/os/Handler;

    .line 49
    iput-object p1, p0, Lcom/sec/android/app/ve/thread/HybridSummaryThread;->mHandler:Landroid/os/Handler;

    .line 50
    iput-object p3, p0, Lcom/sec/android/app/ve/thread/HybridSummaryThread;->mContext:Landroid/content/Context;

    .line 51
    iput-object p4, p0, Lcom/sec/android/app/ve/thread/HybridSummaryThread;->mCurrentTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    .line 52
    iput-wide p5, p0, Lcom/sec/android/app/ve/thread/HybridSummaryThread;->mSummaryDuration:J

    .line 53
    iput-boolean p11, p0, Lcom/sec/android/app/ve/thread/HybridSummaryThread;->previewPlayafterSum:Z

    .line 54
    iput-wide p12, p0, Lcom/sec/android/app/ve/thread/HybridSummaryThread;->posAfterSummPlay:J

    .line 55
    iput p8, p0, Lcom/sec/android/app/ve/thread/HybridSummaryThread;->mode:I

    .line 56
    iput-object p9, p0, Lcom/sec/android/app/ve/thread/HybridSummaryThread;->summaryTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    .line 57
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/ve/thread/HybridSummaryThread;->mForceStop:Z

    .line 58
    iput-boolean p10, p0, Lcom/sec/android/app/ve/thread/HybridSummaryThread;->analysisRequired:Z

    .line 59
    return-void
.end method

.method private createThemeSummaryProject()Z
    .locals 20

    .prologue
    .line 132
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/ve/thread/HybridSummaryThread;->mCurrentTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    .line 133
    .local v4, "oldTranscodeElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    const/4 v11, 0x0

    .line 134
    .local v11, "elem":Lcom/samsung/app/video/editor/external/Element;
    const/4 v10, 0x0

    .line 135
    .local v10, "edit":Lcom/samsung/app/video/editor/external/Edit;
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v3

    .line 136
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/ve/thread/HybridSummaryThread;->summaryTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    .line 137
    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/sec/android/app/ve/thread/HybridSummaryThread;->mSummaryDuration:J

    long-to-double v6, v6

    move-object/from16 v0, p0

    iget v8, v0, Lcom/sec/android/app/ve/thread/HybridSummaryThread;->mode:I

    .line 136
    invoke-virtual/range {v3 .. v8}, Lcom/samsung/app/video/editor/external/NativeInterface;->createSummaryFromEngine(Lcom/samsung/app/video/editor/external/TranscodeElement;Lcom/samsung/app/video/editor/external/TranscodeElement;DI)I

    move-result v15

    .line 139
    .local v15, "outProject":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/ve/thread/HybridSummaryThread;->summaryTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Lcom/samsung/app/video/editor/external/TranscodeElement;->setSummaryProject(Z)V

    .line 141
    if-nez v15, :cond_0

    .line 142
    const/4 v3, 0x0

    .line 192
    :goto_0
    return v3

    .line 145
    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/ve/thread/HybridSummaryThread;->summaryTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v2

    .line 146
    .local v2, "count":I
    new-instance v16, Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/ve/thread/HybridSummaryThread;->summaryTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-object/from16 v0, v16

    invoke-direct {v0, v3}, Lcom/samsung/app/video/editor/external/TranscodeElement;-><init>(Lcom/samsung/app/video/editor/external/TranscodeElement;)V

    .line 148
    .local v16, "output":Lcom/samsung/app/video/editor/external/TranscodeElement;
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_1
    if-lt v12, v2, :cond_1

    .line 151
    const/4 v13, 0x0

    .line 154
    .local v13, "k":I
    :goto_2
    new-instance v11, Lcom/samsung/app/video/editor/external/Element;

    .end local v11    # "elem":Lcom/samsung/app/video/editor/external/Element;
    rem-int v3, v13, v2

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElement(I)Lcom/samsung/app/video/editor/external/Element;

    move-result-object v3

    invoke-direct {v11, v3}, Lcom/samsung/app/video/editor/external/Element;-><init>(Lcom/samsung/app/video/editor/external/Element;)V

    .line 155
    .restart local v11    # "elem":Lcom/samsung/app/video/editor/external/Element;
    const/4 v3, 0x0

    iput-object v3, v11, Lcom/samsung/app/video/editor/external/Element;->filePath:Ljava/lang/String;

    .line 157
    new-instance v17, Lcom/samsung/app/video/editor/external/Edit;

    invoke-direct/range {v17 .. v17}, Lcom/samsung/app/video/editor/external/Edit;-><init>()V

    .line 158
    .local v17, "volumeEdit":Lcom/samsung/app/video/editor/external/Edit;
    const/4 v3, 0x1

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Lcom/samsung/app/video/editor/external/Edit;->setType(I)V

    .line 159
    const/4 v3, -0x1

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Lcom/samsung/app/video/editor/external/Edit;->setVolumeLevel(I)V

    .line 160
    move-object/from16 v0, v17

    invoke-virtual {v11, v0}, Lcom/samsung/app/video/editor/external/Element;->addEdit(Lcom/samsung/app/video/editor/external/Edit;)V

    .line 162
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v3

    invoke-virtual {v3, v11, v15, v13}, Lcom/samsung/app/video/editor/external/NativeInterface;->_getSummaryElementInElementListAt(Lcom/samsung/app/video/editor/external/Element;II)I

    .line 164
    iget-object v3, v11, Lcom/samsung/app/video/editor/external/Element;->filePath:Ljava/lang/String;

    if-nez v3, :cond_2

    .line 189
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v3

    invoke-virtual {v3, v15}, Lcom/samsung/app/video/editor/external/NativeInterface;->_freeSummaryOutput(I)I

    .line 190
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/ve/thread/HybridSummaryThread;->summaryTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4}, Lcom/sec/android/app/ve/thread/HybridSummaryThread;->updateTimingInfoForGroup(Lcom/samsung/app/video/editor/external/TranscodeElement;Lcom/samsung/app/video/editor/external/TranscodeElement;)V

    .line 191
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/ve/thread/HybridSummaryThread;->summaryTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/TranscodeElement;->overlapTransitionsHandling()V

    .line 192
    const/4 v3, 0x1

    goto :goto_0

    .line 149
    .end local v13    # "k":I
    .end local v17    # "volumeEdit":Lcom/samsung/app/video/editor/external/Edit;
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/ve/thread/HybridSummaryThread;->summaryTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/ve/thread/HybridSummaryThread;->summaryTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElement(I)Lcom/samsung/app/video/editor/external/Element;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/samsung/app/video/editor/external/TranscodeElement;->removeElement(Lcom/samsung/app/video/editor/external/Element;)Ljava/util/List;

    .line 148
    add-int/lit8 v12, v12, 0x1

    goto :goto_1

    .line 168
    .restart local v13    # "k":I
    .restart local v17    # "volumeEdit":Lcom/samsung/app/video/editor/external/Edit;
    :cond_2
    invoke-virtual {v11}, Lcom/samsung/app/video/editor/external/Element;->getEndTime()J

    move-result-wide v6

    invoke-virtual {v11}, Lcom/samsung/app/video/editor/external/Element;->getStartTime()J

    move-result-wide v18

    cmp-long v3, v6, v18

    if-gtz v3, :cond_3

    .line 169
    add-int/lit8 v13, v13, 0x1

    .line 170
    goto :goto_2

    .line 173
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {v11, v3}, Lcom/samsung/app/video/editor/external/Element;->setAutoEdited(Z)V

    .line 175
    invoke-virtual {v11}, Lcom/samsung/app/video/editor/external/Element;->getGroupID()I

    move-result v3

    invoke-virtual {v4, v3}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementWithID(I)Lcom/samsung/app/video/editor/external/Element;

    move-result-object v14

    .line 176
    .local v14, "originalEle":Lcom/samsung/app/video/editor/external/Element;
    if-eqz v14, :cond_4

    .line 178
    const/4 v3, 0x1

    invoke-virtual {v14, v3}, Lcom/samsung/app/video/editor/external/Element;->getEdit(I)Lcom/samsung/app/video/editor/external/Edit;

    move-result-object v9

    .line 179
    .local v9, "curEdit":Lcom/samsung/app/video/editor/external/Edit;
    if-eqz v9, :cond_4

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/app/video/editor/external/Edit;->getVolumeLevel()I

    move-result v3

    const/4 v5, -0x1

    if-ne v3, v5, :cond_4

    .line 180
    new-instance v10, Lcom/samsung/app/video/editor/external/Edit;

    .end local v10    # "edit":Lcom/samsung/app/video/editor/external/Edit;
    const/4 v3, 0x1

    invoke-virtual {v14, v3}, Lcom/samsung/app/video/editor/external/Element;->getEdit(I)Lcom/samsung/app/video/editor/external/Edit;

    move-result-object v3

    invoke-direct {v10, v3}, Lcom/samsung/app/video/editor/external/Edit;-><init>(Lcom/samsung/app/video/editor/external/Edit;)V

    .line 181
    .restart local v10    # "edit":Lcom/samsung/app/video/editor/external/Edit;
    invoke-virtual {v11, v10}, Lcom/samsung/app/video/editor/external/Element;->addEdit(Lcom/samsung/app/video/editor/external/Edit;)V

    .line 184
    .end local v9    # "curEdit":Lcom/samsung/app/video/editor/external/Edit;
    :cond_4
    invoke-virtual {v11}, Lcom/samsung/app/video/editor/external/Element;->getEndTime()J

    move-result-wide v6

    invoke-virtual {v11}, Lcom/samsung/app/video/editor/external/Element;->getStartTime()J

    move-result-wide v18

    sub-long v6, v6, v18

    long-to-int v3, v6

    const/4 v5, 0x4

    invoke-virtual {v11, v3, v5}, Lcom/samsung/app/video/editor/external/Element;->setEffectEndTime(II)V

    .line 186
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/ve/thread/HybridSummaryThread;->summaryTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    const/4 v5, 0x0

    invoke-virtual {v3, v11, v5}, Lcom/samsung/app/video/editor/external/TranscodeElement;->addElement(Lcom/samsung/app/video/editor/external/Element;Z)V

    .line 187
    add-int/lit8 v13, v13, 0x1

    .line 152
    goto/16 :goto_2
.end method

.method private updateTimingInfoForGroup(Lcom/samsung/app/video/editor/external/TranscodeElement;Lcom/samsung/app/video/editor/external/TranscodeElement;)V
    .locals 20
    .param p1, "summaryProject"    # Lcom/samsung/app/video/editor/external/TranscodeElement;
    .param p2, "origProject"    # Lcom/samsung/app/video/editor/external/TranscodeElement;

    .prologue
    .line 196
    const/4 v3, 0x0

    .line 197
    .local v3, "element":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementList()Ljava/util/List;

    move-result-object v10

    .line 198
    .local v10, "list":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getMaxGroupID()I

    move-result v2

    .line 199
    .local v2, "count":I
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    if-le v8, v2, :cond_0

    .line 224
    return-void

    .line 200
    :cond_0
    new-instance v6, Ljava/util/Vector;

    invoke-direct {v6}, Ljava/util/Vector;-><init>()V

    .line 201
    .local v6, "groupList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v11

    .line 202
    .local v11, "listSize":I
    const/4 v9, 0x0

    .local v9, "j":I
    :goto_1
    if-lt v9, v11, :cond_1

    .line 208
    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getSummaryGroupTotalTime(Ljava/util/List;)J

    move-result-wide v14

    .line 209
    .local v14, "totalDuration":J
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v7

    .line 210
    .local v7, "groupSize":I
    const/4 v9, 0x0

    :goto_2
    if-lt v9, v7, :cond_3

    .line 199
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 203
    .end local v7    # "groupSize":I
    .end local v14    # "totalDuration":J
    :cond_1
    invoke-interface {v10, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "element":Lcom/samsung/app/video/editor/external/Element;
    check-cast v3, Lcom/samsung/app/video/editor/external/Element;

    .line 204
    .restart local v3    # "element":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/Element;->getGroupID()I

    move-result v13

    if-ne v8, v13, :cond_2

    .line 205
    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 202
    :cond_2
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 211
    .restart local v7    # "groupSize":I
    .restart local v14    # "totalDuration":J
    :cond_3
    invoke-interface {v6, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "element":Lcom/samsung/app/video/editor/external/Element;
    check-cast v3, Lcom/samsung/app/video/editor/external/Element;

    .line 212
    .restart local v3    # "element":Lcom/samsung/app/video/editor/external/Element;
    const/4 v13, 0x0

    invoke-interface {v6, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/app/video/editor/external/Element;

    .line 213
    .local v4, "groupFirstEle":Lcom/samsung/app/video/editor/external/Element;
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v13

    add-int/lit8 v13, v13, -0x1

    invoke-interface {v6, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/app/video/editor/external/Element;

    .line 214
    .local v5, "groupLastEle":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/Element;->getStartTime()J

    move-result-wide v16

    move-wide/from16 v0, v16

    invoke-virtual {v3, v0, v1}, Lcom/samsung/app/video/editor/external/Element;->setGroupStartTime(J)V

    .line 215
    invoke-virtual {v3, v14, v15}, Lcom/samsung/app/video/editor/external/Element;->setGroupDuration(J)V

    .line 216
    if-eqz p2, :cond_4

    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/Element;->getGroupID()I

    move-result v13

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementWithID(I)Lcom/samsung/app/video/editor/external/Element;

    move-result-object v13

    if-eqz v13, :cond_4

    .line 217
    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/Element;->getGroupID()I

    move-result v13

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementWithID(I)Lcom/samsung/app/video/editor/external/Element;

    move-result-object v13

    invoke-virtual {v13}, Lcom/samsung/app/video/editor/external/Element;->getSplitTime()J

    move-result-wide v16

    move-wide/from16 v0, v16

    invoke-virtual {v3, v0, v1}, Lcom/samsung/app/video/editor/external/Element;->setSplitTime(J)V

    .line 218
    :cond_4
    invoke-virtual {v5}, Lcom/samsung/app/video/editor/external/Element;->getEndTime()J

    move-result-wide v16

    move-wide/from16 v0, v16

    invoke-virtual {v3, v0, v1}, Lcom/samsung/app/video/editor/external/Element;->setGroupEndTime(J)V

    .line 219
    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/Element;->getEndTime()J

    move-result-wide v16

    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/Element;->getStartTime()J

    move-result-wide v18

    sub-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-float v13, v0

    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/Element;->getGroupDuration()J

    move-result-wide v16

    move-wide/from16 v0, v16

    long-to-float v0, v0

    move/from16 v16, v0

    div-float v12, v13, v16

    .line 220
    .local v12, "ratio":F
    invoke-virtual {v3, v12}, Lcom/samsung/app/video/editor/external/Element;->setElementRatioInGroup(F)V

    .line 210
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_2
.end method


# virtual methods
.method public getFinalPath()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/sec/android/app/ve/thread/HybridSummaryThread;->path:[Ljava/lang/String;

    return-object v0
.end method

.method public getSummaryTranscode()Lcom/samsung/app/video/editor/external/TranscodeElement;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/android/app/ve/thread/HybridSummaryThread;->summaryTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    return-object v0
.end method

.method public releaseWakelock()V
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/sec/android/app/ve/thread/HybridSummaryThread;->wakelock:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/ve/thread/HybridSummaryThread;->wakelock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 125
    iget-object v0, p0, Lcom/sec/android/app/ve/thread/HybridSummaryThread;->wakelock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 126
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/ve/thread/HybridSummaryThread;->wakelock:Landroid/os/PowerManager$WakeLock;

    .line 128
    :cond_0
    return-void
.end method

.method public run()V
    .locals 12

    .prologue
    const-wide/16 v10, 0xa

    const/16 v6, 0x7d

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 62
    iget-object v3, p0, Lcom/sec/android/app/ve/thread/HybridSummaryThread;->mContext:Landroid/content/Context;

    if-eqz v3, :cond_2

    .line 64
    iget-object v3, p0, Lcom/sec/android/app/ve/thread/HybridSummaryThread;->mContext:Landroid/content/Context;

    .line 65
    const-string v4, "power"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/PowerManager;

    .line 66
    const/16 v4, 0xa

    const-string v5, "VE Export Thread"

    .line 65
    invoke-virtual {v3, v4, v5}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v3

    .line 64
    iput-object v3, p0, Lcom/sec/android/app/ve/thread/HybridSummaryThread;->wakelock:Landroid/os/PowerManager$WakeLock;

    .line 67
    iget-object v3, p0, Lcom/sec/android/app/ve/thread/HybridSummaryThread;->wakelock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v3}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 69
    const/4 v0, 0x0

    .line 70
    .local v0, "isSummarySuccess":Z
    const/4 v2, 0x1

    .line 71
    .local v2, "result":I
    iget-boolean v3, p0, Lcom/sec/android/app/ve/thread/HybridSummaryThread;->analysisRequired:Z

    if-eqz v3, :cond_0

    .line 72
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/ve/thread/HybridSummaryThread;->mCurrentTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    .line 73
    sget v5, Lcom/samsung/app/video/editor/external/Constants;->AUTOEDIT_MODE_AUTO:I

    .line 72
    invoke-virtual {v3, v4, v5, v8}, Lcom/samsung/app/video/editor/external/NativeInterface;->createSummaryInstance(Lcom/samsung/app/video/editor/external/TranscodeElement;II)I

    move-result v2

    .line 74
    :cond_0
    if-ne v2, v8, :cond_1

    .line 75
    invoke-direct {p0}, Lcom/sec/android/app/ve/thread/HybridSummaryThread;->createThemeSummaryProject()Z

    move-result v0

    .line 77
    :cond_1
    if-nez v0, :cond_3

    .line 78
    invoke-virtual {p0}, Lcom/sec/android/app/ve/thread/HybridSummaryThread;->releaseWakelock()V

    .line 85
    :goto_0
    if-nez v0, :cond_6

    .line 86
    iget-boolean v3, p0, Lcom/sec/android/app/ve/thread/HybridSummaryThread;->mForceStop:Z

    if-eqz v3, :cond_4

    const/16 v1, 0x7c

    .line 87
    .local v1, "msg":I
    :goto_1
    const-string v3, "LIBRARY"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "RESULT RETURN BY ENGINE "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    if-ge v2, v8, :cond_5

    .line 92
    iget-object v3, p0, Lcom/sec/android/app/ve/thread/HybridSummaryThread;->mHandler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/sec/android/app/ve/thread/HybridSummaryThread;->mHandler:Landroid/os/Handler;

    const/16 v5, 0x7e

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v6

    invoke-static {v4, v1, v5, v6}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {v3, v4, v10, v11}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 96
    :goto_2
    iput-boolean v9, p0, Lcom/sec/android/app/ve/thread/HybridSummaryThread;->mForceStop:Z

    .line 108
    .end local v0    # "isSummarySuccess":Z
    .end local v1    # "msg":I
    .end local v2    # "result":I
    :cond_2
    :goto_3
    return-void

    .line 80
    .restart local v0    # "isSummarySuccess":Z
    .restart local v2    # "result":I
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/ve/thread/HybridSummaryThread;->handler:Landroid/os/Handler;

    invoke-virtual {v3, v6}, Landroid/os/Handler;->removeMessages(I)V

    .line 81
    iget-object v3, p0, Lcom/sec/android/app/ve/thread/HybridSummaryThread;->handler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/sec/android/app/ve/thread/HybridSummaryThread;->handler:Landroid/os/Handler;

    invoke-static {v4, v6}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v4

    .line 82
    const-wide/16 v6, 0xbb8

    .line 81
    invoke-virtual {v3, v4, v6, v7}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    .line 86
    :cond_4
    const/16 v1, 0x7a

    goto :goto_1

    .line 95
    .restart local v1    # "msg":I
    :cond_5
    iget-object v3, p0, Lcom/sec/android/app/ve/thread/HybridSummaryThread;->mHandler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/sec/android/app/ve/thread/HybridSummaryThread;->mHandler:Landroid/os/Handler;

    invoke-static {v4, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {v3, v4, v10, v11}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_2

    .line 99
    .end local v1    # "msg":I
    :cond_6
    iget-object v3, p0, Lcom/sec/android/app/ve/thread/HybridSummaryThread;->mContext:Landroid/content/Context;

    if-nez v3, :cond_7

    .line 100
    const-string v3, "context is null"

    invoke-static {v3}, Lcom/sec/android/app/ve/common/LogUtils;->log(Ljava/lang/String;)V

    .line 103
    :cond_7
    iget-object v3, p0, Lcom/sec/android/app/ve/thread/HybridSummaryThread;->mHandler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/sec/android/app/ve/thread/HybridSummaryThread;->mHandler:Landroid/os/Handler;

    .line 104
    const/16 v5, 0x7b

    .line 105
    iget-wide v6, p0, Lcom/sec/android/app/ve/thread/HybridSummaryThread;->posAfterSummPlay:J

    long-to-int v6, v6

    iget-boolean v7, p0, Lcom/sec/android/app/ve/thread/HybridSummaryThread;->previewPlayafterSum:Z

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    .line 103
    invoke-static {v4, v5, v6, v9, v7}, Landroid/os/Message;->obtain(Landroid/os/Handler;IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v4

    .line 105
    const-wide/16 v6, 0x3e8

    .line 103
    invoke-virtual {v3, v4, v6, v7}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_3
.end method

.method public stopSummary()V
    .locals 1

    .prologue
    .line 111
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/ve/thread/HybridSummaryThread;->mForceStop:Z

    .line 112
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/app/video/editor/external/NativeInterface;->stopEngineSummary()V

    .line 113
    return-void
.end method
