.class public Lcom/sec/android/app/ve/common/ConfigUtils;
.super Ljava/lang/Object;
.source "ConfigUtils.java"


# static fields
.field public static EXPORT_PATH:Ljava/lang/String; = null

.field public static FIXED_PREVIEW_HEIGHT:I = 0x0

.field public static FIXED_PREVIEW_WIDTH:I = 0x0

.field public static final GLOBAL_THUMBNAIL_HEIGHT:I = 0x168

.field public static final GLOBAL_THUMBNAIL_WIDTH:I = 0x280

.field public static KENBURN_HEIGHT:I = 0x0

.field public static KENBURN_WIDTH:I = 0x0

.field public static final LAUNCHER_THUMB_HEIGHT:I

.field public static final LAUNCHER_THUMB_WIDTH:I

.field public static final MAX_NUMBER_OF_ELEMENTS:I = 0x20

.field public static final MAX_SUPPORTED_IMAGE_FILE_SIZE:J = 0x3200000L

.field public static MAX_SUPPORTED_VIDEO_FILE_SIZE:J = 0x0L

.field public static final MIN_RESOLUTION_FOR_THUMBNAILS:I = 0x11170

.field public static PREVIEW_HEIGHT:I = 0x0

.field public static PREVIEW_WIDTH:I = 0x0

.field public static final PROJECT_PREVIEW_MAX_TIME:I = 0x3a98

.field public static STORYBOARD_TIME_LIMIT:I = 0x0

.field public static final TRANSITION_DURATION:I = 0x3e8

.field public static isHDMovieLockRequired:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 17
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "/Video Editor/Export/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/ve/common/ConfigUtils;->EXPORT_PATH:Ljava/lang/String;

    .line 19
    const v0, 0x249f00

    sput v0, Lcom/sec/android/app/ve/common/ConfigUtils;->STORYBOARD_TIME_LIMIT:I

    .line 20
    const-wide v0, 0x80000000L

    sput-wide v0, Lcom/sec/android/app/ve/common/ConfigUtils;->MAX_SUPPORTED_VIDEO_FILE_SIZE:J

    .line 22
    sget v0, Lcom/sec/android/app/ve/R$dimen;->launcher_unselitem_width:I

    invoke-static {v0}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v0

    sput v0, Lcom/sec/android/app/ve/common/ConfigUtils;->LAUNCHER_THUMB_WIDTH:I

    .line 23
    sget v0, Lcom/sec/android/app/ve/R$dimen;->launcher_unselitem_height:I

    invoke-static {v0}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v0

    sput v0, Lcom/sec/android/app/ve/common/ConfigUtils;->LAUNCHER_THUMB_HEIGHT:I

    .line 24
    const/16 v0, 0x1f1

    sput v0, Lcom/sec/android/app/ve/common/ConfigUtils;->PREVIEW_WIDTH:I

    .line 25
    const/16 v0, 0x110

    sput v0, Lcom/sec/android/app/ve/common/ConfigUtils;->PREVIEW_HEIGHT:I

    .line 26
    const/16 v0, 0x500

    sput v0, Lcom/sec/android/app/ve/common/ConfigUtils;->FIXED_PREVIEW_WIDTH:I

    .line 27
    const/16 v0, 0x2d0

    sput v0, Lcom/sec/android/app/ve/common/ConfigUtils;->FIXED_PREVIEW_HEIGHT:I

    .line 28
    sget v0, Lcom/sec/android/app/ve/R$dimen;->PreviewGroup_width:I

    invoke-static {v0}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v0

    sput v0, Lcom/sec/android/app/ve/common/ConfigUtils;->KENBURN_WIDTH:I

    .line 29
    sget v0, Lcom/sec/android/app/ve/R$dimen;->PreviewGroup_height:I

    invoke-static {v0}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v0

    sput v0, Lcom/sec/android/app/ve/common/ConfigUtils;->KENBURN_HEIGHT:I

    .line 36
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/ve/common/ConfigUtils;->isHDMovieLockRequired:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static isHoverSettingDisabled(Landroid/content/Context;Landroid/view/MotionEvent;)Z
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 100
    const/4 v4, 0x1

    .line 101
    .local v4, "retValue":Z
    if-eqz p0, :cond_0

    .line 102
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v8, "pen_hovering"

    invoke-static {v7, v8, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v7

    if-ne v7, v5, :cond_1

    move v2, v5

    .line 103
    .local v2, "PEN_HOVERING":Z
    :goto_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v8, "finger_air_view"

    invoke-static {v7, v8, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v7

    if-ne v7, v5, :cond_2

    move v0, v5

    .line 104
    .local v0, "FINGER_AIR_VIEW":Z
    :goto_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v8, "pen_hovering_information_preview"

    invoke-static {v7, v8, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v7

    if-ne v7, v5, :cond_3

    move v3, v5

    .line 105
    .local v3, "PEN_HOVERING_INFORMATION_PREVIEW":Z
    :goto_2
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v8, "finger_air_view_information_preview"

    invoke-static {v7, v8, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v7

    if-ne v7, v5, :cond_4

    move v1, v5

    .line 106
    .local v1, "FINGER_AIR_VIEW_INFORMATION_PREVIEW":Z
    :goto_3
    invoke-virtual {p1, v6}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v7

    const/4 v8, 0x2

    if-ne v7, v8, :cond_5

    .line 107
    if-eqz v2, :cond_0

    if-eqz v3, :cond_0

    .line 108
    const/4 v4, 0x0

    .line 115
    .end local v0    # "FINGER_AIR_VIEW":Z
    .end local v1    # "FINGER_AIR_VIEW_INFORMATION_PREVIEW":Z
    .end local v2    # "PEN_HOVERING":Z
    .end local v3    # "PEN_HOVERING_INFORMATION_PREVIEW":Z
    :cond_0
    :goto_4
    return v4

    :cond_1
    move v2, v6

    .line 102
    goto :goto_0

    .restart local v2    # "PEN_HOVERING":Z
    :cond_2
    move v0, v6

    .line 103
    goto :goto_1

    .restart local v0    # "FINGER_AIR_VIEW":Z
    :cond_3
    move v3, v6

    .line 104
    goto :goto_2

    .restart local v3    # "PEN_HOVERING_INFORMATION_PREVIEW":Z
    :cond_4
    move v1, v6

    .line 105
    goto :goto_3

    .line 110
    .restart local v1    # "FINGER_AIR_VIEW_INFORMATION_PREVIEW":Z
    :cond_5
    invoke-virtual {p1, v6}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v6

    if-ne v6, v5, :cond_0

    .line 111
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 112
    const/4 v4, 0x0

    goto :goto_4
.end method

.method public static isHoveringUI(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 88
    if-nez p0, :cond_0

    .line 90
    const/4 v0, 0x0

    .line 95
    :goto_0
    return v0

    .line 94
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v2, "com.sec.feature.hovering_ui"

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    .line 95
    .local v0, "bReturn":Z
    goto :goto_0
.end method

.method public static isTalkBackModeOn(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 119
    const/4 v0, 0x0

    .line 120
    .local v0, "enabled":Z
    const-string v2, "accessibility"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/accessibility/AccessibilityManager;

    .line 121
    .local v1, "mgr":Landroid/view/accessibility/AccessibilityManager;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x1

    .line 122
    :goto_0
    return v0

    .line 121
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static setHovering(Landroid/content/Context;Landroid/view/View;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v1, 0x1

    .line 79
    invoke-static {p0}, Lcom/sec/android/app/ve/common/ConfigUtils;->isHoveringUI(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 81
    invoke-virtual {p1, v1}, Landroid/view/View;->setHovered(Z)V

    .line 82
    invoke-virtual {p1, v1}, Landroid/view/View;->setHoverPopupType(I)V

    .line 84
    :cond_0
    return-void
.end method

.method public static setHoveringPointerIcon(I)V
    .locals 2
    .param p0, "iconType"    # I

    .prologue
    .line 70
    const/4 v1, -0x1

    :try_start_0
    invoke-static {p0, v1}, Landroid/view/PointerIcon;->setHoveringSpenIcon(II)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 75
    :goto_0
    return-void

    .line 71
    :catch_0
    move-exception v0

    .line 73
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static setLockForHDMoviePlay(Landroid/content/Context;Z)V
    .locals 0
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "isAquire"    # Z

    .prologue
    .line 65
    return-void
.end method
