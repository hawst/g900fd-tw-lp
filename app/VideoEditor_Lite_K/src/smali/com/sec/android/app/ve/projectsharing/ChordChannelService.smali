.class public Lcom/sec/android/app/ve/projectsharing/ChordChannelService;
.super Landroid/app/Service;
.source "ChordChannelService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordServiceBinder;,
        Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;,
        Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ProjectApprovalMessage;,
        Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ServiceHandler;
    }
.end annotation


# static fields
.field public static final BROADCAST_CHORD_SERVICE_FINISHED:Ljava/lang/String; = "BROADCAST_CHORD_SERVICE_FINISHED"

.field public static final BROADCAST_CHORD_SERVICE_STARTED:Ljava/lang/String; = "BROADCAST_CHORD_SERVICE_STARTED"

.field public static final BROADCAST_EXTRA_PEER_CHORD_ID:Ljava/lang/String; = "BROADCAST_EXTRA_PEER_CHORD_ID"

.field public static final BROADCAST_FINISH_CHORD_SERVICE:Ljava/lang/String; = "BROADCAST_FINISH_CHORD_SERVICE"

.field public static final BROADCAST_NEW_PEER_AVAILABLE:Ljava/lang/String; = "BROADCAST_NEW_PEER_AVAILABLE"

.field public static final BROADCAST_PEER_LEFT:Ljava/lang/String; = "BROADCAST_PEER_LEFT"

.field public static final BROADCAST_PROJECT_IMPORTED:Ljava/lang/String; = "BROADCAST_PROJECT_IMPORTED"

.field public static final BROADCAST_START_PRJ_SHARING:Ljava/lang/String; = "BROADCAST_START_CHORD_SESSION"

.field private static final CHORD_CHUNK_SEND_RETRIES:I = 0x2

.field private static final CHORD_CHUNK_TIMEOUT:I = 0x7530

.field private static final CHORD_FILE_CHUNK_SIZE:I = 0x4b000

.field public static final CHORD_PRJ_SHARING_CHANNEL:Ljava/lang/String; = "CHORD_PRJ_SHARING_CHANNEL"

.field public static final INTENT_EXTRA_FROM_BROADCASTRECEIVER:Ljava/lang/String; = "INTENT_EXTRA_FROM_BROADCASTRECEIVER"

.field private static final MSG_ACK_PRJ_SHARE:Ljava/lang/String; = "MSG_ACK_PRJ_SHARE"

.field private static final MSG_NOTIFY_PRJ_SHARE:Ljava/lang/String; = "MSG_NOTIFY_PRJ_SHARE"

.field private static final MSG_REQUEST_DEVICE_NAME:Ljava/lang/String; = "MSG_REQUEST_DEVICE_NAME"

.field private static final MSG_RESPONSE_DEVICE_NAME:Ljava/lang/String; = "MSG_RESPONSE_DEVICE_NAME"

.field private static final NOTIFICATION_ID_NOTIFY_PRJ_SHARE:I = 0xc

.field private static final NOTIFICATION_ID_SHARE_PRJ_ONGOING:I = 0xb

.field private static final SHARE_FILE_TIMEOUT_MILISECONDS:I = 0x36ee80

.field private static final SHARE_PRJ_PENDING_TIMEOUT:I = 0x7530

.field private static final THREAD_CMD_APPROVE_PRJ_SHARE:I = 0x6b

.field private static final THREAD_CMD_CANCEL_PENDING_PRJ_SHARE:I = 0x6a

.field private static final THREAD_CMD_CLOSE_CHORD:I = 0x65

.field private static final THREAD_CMD_EXIT_THREAD:I = 0x66

.field private static final THREAD_CMD_INIT_CHORD:I = 0x64

.field private static final THREAD_CMD_NOTIFY_PRJ_SHARE:I = 0x69

.field private static final THREAD_CMD_START_CHORD:I = 0x68

.field private static final THREAD_NAME:Ljava/lang/String; = "ProjectSharingThread"

.field private static final UI_MSG_SHOW_TOAST:I = 0x3e8

.field private static final VE_PRJ_CONTENTS:Ljava/lang/String; = "VE_PRJ_CONTENTS"


# instance fields
.field private final mBinder:Landroid/os/IBinder;

.field private mChannelListener:Lcom/samsung/chord/IChordChannelListener;

.field private mChordManager:Lcom/samsung/chord/ChordManager;

.field private volatile mDeviceMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mHandler:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ServiceHandler;

.field private mHandlerThread:Landroid/os/HandlerThread;

.field private volatile mIsChordStarted:Z

.field private mLocalBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mLooper:Landroid/os/Looper;

.field private mManagerListener:Lcom/samsung/chord/IChordManagerListener;

.field private mNetworkListener:Lcom/samsung/chord/ChordManager$INetworkListener;

.field private volatile mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

.field private final mUIThreadHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 49
    new-instance v0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordServiceBinder;

    invoke-direct {v0, p0}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordServiceBinder;-><init>(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;)V

    iput-object v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mBinder:Landroid/os/IBinder;

    .line 51
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mIsChordStarted:Z

    .line 95
    new-instance v0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$1;-><init>(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;)V

    iput-object v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mLocalBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 135
    new-instance v0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$2;-><init>(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;)V

    iput-object v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mManagerListener:Lcom/samsung/chord/IChordManagerListener;

    .line 182
    new-instance v0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$3;-><init>(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;)V

    iput-object v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mChannelListener:Lcom/samsung/chord/IChordChannelListener;

    .line 403
    new-instance v0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$4;-><init>(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;)V

    iput-object v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mNetworkListener:Lcom/samsung/chord/ChordManager$INetworkListener;

    .line 418
    new-instance v0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$5;-><init>(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;)V

    iput-object v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mUIThreadHandler:Landroid/os/Handler;

    .line 43
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;)Lcom/samsung/chord/ChordManager;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mChordManager:Lcom/samsung/chord/ChordManager;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;)Z
    .locals 1

    .prologue
    .line 772
    invoke-direct {p0}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->isWiFiDirectInterfaceAvailable()Z

    move-result v0

    return v0
.end method

.method static synthetic access$10(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;JZZ)Landroid/app/Notification;
    .locals 1

    .prologue
    .line 700
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->updateReceiverProgressNotification(JZZ)Landroid/app/Notification;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$11(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;JZZ)Landroid/app/Notification;
    .locals 1

    .prologue
    .line 726
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->updateSenderProgressNotification(JZZ)Landroid/app/Notification;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$12(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 611
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->saveTempFile(Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

.method static synthetic access$13(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;I)V
    .locals 0

    .prologue
    .line 685
    invoke-direct {p0, p1}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->sendToastMessageToUIHandler(I)V

    return-void
.end method

.method static synthetic access$14(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;)V
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    return-void
.end method

.method static synthetic access$15(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;Ljava/lang/String;[[B)V
    .locals 0

    .prologue
    .line 806
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->handleDeviceNameRequestMsg(Ljava/lang/String;[[B)V

    return-void
.end method

.method static synthetic access$16(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;Ljava/lang/String;[[B)V
    .locals 0

    .prologue
    .line 815
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->handleDeviceNameResponseMsg(Ljava/lang/String;[[B)V

    return-void
.end method

.method static synthetic access$17(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;Ljava/lang/String;[[B)V
    .locals 0

    .prologue
    .line 820
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->handleReceiveProjectNotificationMsg(Ljava/lang/String;[[B)V

    return-void
.end method

.method static synthetic access$18(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ProjectApprovalMessage;)V
    .locals 0

    .prologue
    .line 532
    invoke-direct {p0, p1}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->approveProjectShare(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ProjectApprovalMessage;)V

    return-void
.end method

.method static synthetic access$19(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;Ljava/lang/String;[[B)V
    .locals 0

    .prologue
    .line 847
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->handleProjectShareAckMsg(Ljava/lang/String;[[B)V

    return-void
.end method

.method static synthetic access$2(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;)Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ServiceHandler;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mHandler:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ServiceHandler;

    return-object v0
.end method

.method static synthetic access$20(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 760
    invoke-direct {p0, p1}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->getInterfaceName(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$21(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;I)V
    .locals 0

    .prologue
    .line 750
    invoke-direct {p0, p1}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->startChord(I)V

    return-void
.end method

.method static synthetic access$22(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;Z)V
    .locals 0

    .prologue
    .line 475
    invoke-direct {p0, p1}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->initChord(Z)V

    return-void
.end method

.method static synthetic access$23(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;)V
    .locals 0

    .prologue
    .line 591
    invoke-direct {p0}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->notifyProjectTransferToDestination()V

    return-void
.end method

.method static synthetic access$24(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;)V
    .locals 0

    .prologue
    .line 500
    invoke-direct {p0}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->closeChord()V

    return-void
.end method

.method static synthetic access$25(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;)V
    .locals 0

    .prologue
    .line 514
    invoke-direct {p0}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->exitThread()V

    return-void
.end method

.method static synthetic access$26(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;)V
    .locals 0

    .prologue
    .line 521
    invoke-direct {p0}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->cancelProjectShareSession()V

    return-void
.end method

.method static synthetic access$3(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;)Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    return-object v0
.end method

.method static synthetic access$4(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;Z)V
    .locals 0

    .prologue
    .line 51
    iput-boolean p1, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mIsChordStarted:Z

    return-void
.end method

.method static synthetic access$5(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;)Lcom/samsung/chord/IChordChannelListener;
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mChannelListener:Lcom/samsung/chord/IChordChannelListener;

    return-object v0
.end method

.method static synthetic access$6(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;)Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mDeviceMap:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$7(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 781
    invoke-direct {p0}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->getMyDeviceName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$8(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 796
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->sendLocalBroadcast(Ljava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method static synthetic access$9(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;J)Z
    .locals 1

    .prologue
    .line 667
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->checkAvailableMemory(J)Z

    move-result v0

    return v0
.end method

.method private approveProjectShare(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ProjectApprovalMessage;)V
    .locals 6
    .param p1, "approveObj"    # Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ProjectApprovalMessage;

    .prologue
    const/4 v5, 0x0

    .line 533
    iget-object v2, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    if-eqz v2, :cond_0

    .line 534
    iget-object v2, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mChordManager:Lcom/samsung/chord/ChordManager;

    const-string v3, "CHORD_PRJ_SHARING_CHANNEL"

    iget-object v4, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mChannelListener:Lcom/samsung/chord/IChordChannelListener;

    invoke-virtual {v2, v3, v4}, Lcom/samsung/chord/ChordManager;->joinChannel(Ljava/lang/String;Lcom/samsung/chord/IChordChannelListener;)Lcom/samsung/chord/IChordChannel;

    move-result-object v0

    .line 535
    .local v0, "channel":Lcom/samsung/chord/IChordChannel;
    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ProjectApprovalMessage;->approve:Z
    invoke-static {p1}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ProjectApprovalMessage;->access$0(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ProjectApprovalMessage;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 536
    const/4 v2, 0x1

    new-array v1, v2, [[B

    .line 537
    .local v1, "msgPayLoad":[[B
    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mProjectMetaData:[B
    invoke-static {v3}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$10(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;)[B

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mReceiverFileMapping:Ljava/util/HashMap;
    invoke-static {v4}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$9(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;)Ljava/util/HashMap;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper;->prepareProjectResponseAtReceiver([BLjava/util/HashMap;)Lorg/json/JSONObject;

    move-result-object v3

    invoke-virtual {v3}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    aput-object v3, v1, v2

    .line 538
    iget-object v2, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mSourceNode:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$11(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "MSG_ACK_PRJ_SHARE"

    invoke-interface {v0, v2, v3, v1}, Lcom/samsung/chord/IChordChannel;->sendData(Ljava/lang/String;Ljava/lang/String;[[B)Z

    .line 539
    iget-object v2, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ProjectApprovalMessage;->overwrite:Z
    invoke-static {p1}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ProjectApprovalMessage;->access$1(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ProjectApprovalMessage;)Z

    move-result v3

    invoke-static {v2, v3}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$12(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;Z)V

    .line 549
    .end local v0    # "channel":Lcom/samsung/chord/IChordChannel;
    .end local v1    # "msgPayLoad":[[B
    :cond_0
    :goto_0
    return-void

    .line 542
    .restart local v0    # "channel":Lcom/samsung/chord/IChordChannel;
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mSourceNode:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$11(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "MSG_ACK_PRJ_SHARE"

    invoke-interface {v0, v2, v3, v5}, Lcom/samsung/chord/IChordChannel;->sendData(Ljava/lang/String;Ljava/lang/String;[[B)Z

    .line 543
    iput-object v5, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    goto :goto_0
.end method

.method private beginProjectSharingFromChordThread()V
    .locals 15

    .prologue
    const/4 v12, 0x0

    .line 553
    iget-object v10, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mFilesBeingTransferred:Ljava/util/ArrayList;
    invoke-static {v10}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$7(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;)Ljava/util/ArrayList;

    move-result-object v10

    if-eqz v10, :cond_1

    .line 554
    iget-object v10, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mCurrentTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;
    invoke-static {v10}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$13(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;)Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v10

    invoke-static {v10}, Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper;->createProjectJSONFile(Lcom/samsung/app/video/editor/external/TranscodeElement;)Ljava/lang/String;

    move-result-object v5

    .line 555
    .local v5, "jsonFilePath":Ljava/lang/String;
    if-eqz v5, :cond_0

    .line 556
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 557
    .local v4, "jsonFile":Ljava/io/File;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v11, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ".flist"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 558
    .local v7, "tmpFilePath":Ljava/lang/String;
    iget-object v10, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mFilesBeingTransferred:Ljava/util/ArrayList;
    invoke-static {v10}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$7(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;)Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 559
    iget-object v10, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mFilesBeingTransferred:Ljava/util/ArrayList;
    invoke-static {v10}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$7(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;)Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10, v12, v7}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 560
    iget-object v10, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mFilesBeingTransferred:Ljava/util/ArrayList;
    invoke-static {v10}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$7(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;)Ljava/util/ArrayList;

    move-result-object v10

    invoke-static {v5, v10, v7}, Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper;->createTempFilesList(Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 563
    .end local v4    # "jsonFile":Ljava/io/File;
    .end local v7    # "tmpFilePath":Ljava/lang/String;
    :cond_0
    iget-object v10, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mChordManager:Lcom/samsung/chord/ChordManager;

    const-string v11, "CHORD_PRJ_SHARING_CHANNEL"

    invoke-virtual {v10, v11}, Lcom/samsung/chord/ChordManager;->getJoinedChannel(Ljava/lang/String;)Lcom/samsung/chord/IChordChannel;

    move-result-object v0

    .line 564
    .local v0, "channel":Lcom/samsung/chord/IChordChannel;
    if-eqz v0, :cond_1

    .line 566
    :try_start_0
    iget-object v10, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    const-wide/16 v12, 0x0

    invoke-static {v10, v12, v13}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$2(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;J)V

    .line 567
    iget-object v10, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    const-wide/16 v12, 0x0

    invoke-static {v10, v12, v13}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$3(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;J)V

    .line 568
    iget-object v10, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    iget-object v11, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mFilesBeingTransferred:Ljava/util/ArrayList;
    invoke-static {v11}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$7(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;)Ljava/util/ArrayList;

    move-result-object v11

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v11

    new-array v11, v11, [J

    invoke-static {v10, v11}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$14(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;[J)V

    .line 569
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v10, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mFilesBeingTransferred:Ljava/util/ArrayList;
    invoke-static {v10}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$7(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;)Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-lt v3, v10, :cond_2

    .line 577
    const-string v10, "power"

    invoke-virtual {p0, v10}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/os/PowerManager;

    .line 578
    .local v6, "pm":Landroid/os/PowerManager;
    iget-object v10, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    const/4 v11, 0x1

    const-string v12, "ProjectSharingThread"

    invoke-virtual {v6, v11, v12}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$0(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;Landroid/os/PowerManager$WakeLock;)V

    .line 579
    iget-object v10, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mPartialWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v10}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$1(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;)Landroid/os/PowerManager$WakeLock;

    move-result-object v10

    invoke-virtual {v10}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 580
    iget-object v10, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    new-instance v11, Landroid/app/Notification$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->getApplicationContext()Landroid/content/Context;

    move-result-object v12

    invoke-direct {v11, v12}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    invoke-static {v10, v11}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$4(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;Landroid/app/Notification$Builder;)V

    .line 581
    iget-object v10, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mDestinationNode:Ljava/lang/String;
    invoke-static {v10}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$16(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;)Ljava/lang/String;

    move-result-object v10

    const-string v11, "VE_PRJ_CONTENTS"

    iget-object v12, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mFilesBeingTransferred:Ljava/util/ArrayList;
    invoke-static {v12}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$7(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;)Ljava/util/ArrayList;

    move-result-object v12

    const v13, 0x36ee80

    invoke-interface {v0, v10, v11, v12, v13}, Lcom/samsung/chord/IChordChannel;->sendMultiFiles(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;I)Ljava/lang/String;

    .line 582
    const/16 v10, 0xb

    const-wide/16 v12, 0x0

    const/4 v11, 0x0

    const/4 v14, 0x1

    invoke-direct {p0, v12, v13, v11, v14}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->updateSenderProgressNotification(JZZ)Landroid/app/Notification;

    move-result-object v11

    invoke-virtual {p0, v10, v11}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->startForeground(ILandroid/app/Notification;)V

    .line 588
    .end local v0    # "channel":Lcom/samsung/chord/IChordChannel;
    .end local v3    # "i":I
    .end local v5    # "jsonFilePath":Ljava/lang/String;
    .end local v6    # "pm":Landroid/os/PowerManager;
    :cond_1
    :goto_1
    return-void

    .line 570
    .restart local v0    # "channel":Lcom/samsung/chord/IChordChannel;
    .restart local v3    # "i":I
    .restart local v5    # "jsonFilePath":Ljava/lang/String;
    :cond_2
    new-instance v2, Ljava/io/File;

    iget-object v10, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mFilesBeingTransferred:Ljava/util/ArrayList;
    invoke-static {v10}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$7(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;)Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    invoke-direct {v2, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 571
    .local v2, "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v8

    .line 572
    .local v8, "size":J
    iget-object v10, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mBytesToTransfer:J
    invoke-static {v10}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$15(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;)J

    move-result-wide v12

    add-long/2addr v12, v8

    invoke-static {v10, v12, v13}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$2(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;J)V

    .line 573
    iget-object v10, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mFileSizes:[J
    invoke-static {v10}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$6(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;)[J

    move-result-object v10

    aput-wide v8, v10, v3
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 569
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    .line 583
    .end local v2    # "file":Ljava/io/File;
    .end local v3    # "i":I
    .end local v8    # "size":J
    :catch_0
    move-exception v1

    .line 584
    .local v1, "e":Ljava/io/FileNotFoundException;
    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_1
.end method

.method private cancelProjectShareSession()V
    .locals 1

    .prologue
    .line 522
    iget-object v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    if-eqz v0, :cond_0

    .line 523
    iget-object v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mIsSender:Z
    invoke-static {v0}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$8(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 524
    sget v0, Lcom/sec/android/app/ve/R$string;->prj_sharing_failed_sender:I

    invoke-direct {p0, v0}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->sendToastMessageToUIHandler(I)V

    .line 526
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    .line 529
    return-void
.end method

.method private checkAvailableMemory(J)Z
    .locals 9
    .param p1, "fileSize"    # J

    .prologue
    .line 668
    new-instance v5, Ljava/io/File;

    sget-object v8, Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper;->PATH_FOR_SHARED_PRJ_CONTENTS:Ljava/lang/String;

    invoke-direct {v5, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 669
    .local v5, "targetdir":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v8

    if-nez v8, :cond_0

    .line 670
    invoke-virtual {v5}, Ljava/io/File;->mkdirs()Z

    .line 673
    :cond_0
    new-instance v4, Landroid/os/StatFs;

    sget-object v8, Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper;->PATH_FOR_SHARED_PRJ_CONTENTS:Ljava/lang/String;

    invoke-direct {v4, v8}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 674
    .local v4, "stat":Landroid/os/StatFs;
    invoke-virtual {v4}, Landroid/os/StatFs;->getBlockSize()I

    move-result v8

    int-to-long v2, v8

    .line 675
    .local v2, "blockSize":J
    invoke-virtual {v4}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v8

    int-to-long v6, v8

    .line 676
    .local v6, "totalBlocks":J
    mul-long v0, v2, v6

    .line 678
    .local v0, "availableMemory":J
    cmp-long v8, v0, p1

    if-gez v8, :cond_1

    .line 679
    const/4 v8, 0x0

    .line 682
    :goto_0
    return v8

    :cond_1
    const/4 v8, 0x1

    goto :goto_0
.end method

.method private closeChord()V
    .locals 2

    .prologue
    .line 501
    iget-object v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mPartialWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v0}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$1(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mPartialWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v0}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$1(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 502
    iget-object v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mPartialWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v0}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$1(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 504
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mChordManager:Lcom/samsung/chord/ChordManager;

    if-eqz v0, :cond_1

    .line 505
    iget-object v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mChordManager:Lcom/samsung/chord/ChordManager;

    const-string v1, "CHORD_PRJ_SHARING_CHANNEL"

    invoke-virtual {v0, v1}, Lcom/samsung/chord/ChordManager;->leaveChannel(Ljava/lang/String;)V

    .line 506
    iget-object v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mChordManager:Lcom/samsung/chord/ChordManager;

    invoke-virtual {v0}, Lcom/samsung/chord/ChordManager;->stop()V

    .line 507
    iget-object v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mChordManager:Lcom/samsung/chord/ChordManager;

    invoke-virtual {v0}, Lcom/samsung/chord/ChordManager;->close()V

    .line 508
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mChordManager:Lcom/samsung/chord/ChordManager;

    .line 509
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mIsChordStarted:Z

    .line 510
    const-string v0, "ProjectSharing"

    const-string v1, "Closed Chord Manager"

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 512
    :cond_1
    return-void
.end method

.method private exitThread()V
    .locals 2

    .prologue
    .line 515
    iget-object v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mLooper:Landroid/os/Looper;

    if-eqz v0, :cond_0

    .line 516
    iget-object v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mLooper:Landroid/os/Looper;

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 517
    const-string v0, "ProjectSharing"

    const-string v1, "Quit Looper"

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 519
    :cond_0
    return-void
.end method

.method private getCurrentProgressPercentage(J)I
    .locals 7
    .param p1, "offset"    # J

    .prologue
    .line 691
    const/4 v0, 0x0

    .line 692
    .local v0, "intPercentage":I
    iget-object v2, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    if-eqz v2, :cond_0

    .line 693
    iget-object v2, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mBytesTransferred:J
    invoke-static {v2}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$5(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;)J

    move-result-wide v2

    add-long/2addr v2, p1

    long-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mBytesToTransfer:J
    invoke-static {v3}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$15(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;)J

    move-result-wide v4

    long-to-float v3, v4

    div-float/2addr v2, v3

    const/high16 v3, 0x42c80000    # 100.0f

    mul-float v1, v2, v3

    .line 694
    .local v1, "percentage":F
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 697
    .end local v1    # "percentage":F
    :cond_0
    return v0
.end method

.method private getInterfaceName(I)Ljava/lang/String;
    .locals 1
    .param p1, "interfaceType"    # I

    .prologue
    .line 761
    if-nez p1, :cond_0

    .line 762
    const-string v0, "Wi-Fi"

    .line 769
    :goto_0
    return-object v0

    .line 763
    :cond_0
    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    .line 764
    const-string v0, "Mobile AP"

    goto :goto_0

    .line 765
    :cond_1
    const/4 v0, 0x1

    if-ne p1, v0, :cond_2

    .line 766
    const-string v0, "Wi-Fi Direct"

    goto :goto_0

    .line 769
    :cond_2
    const-string v0, "UNKNOWN"

    goto :goto_0
.end method

.method private getMyDeviceName()Ljava/lang/String;
    .locals 4

    .prologue
    .line 782
    invoke-virtual {p0}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "device_name"

    invoke-static {v1, v2}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 783
    .local v0, "deviceName":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 784
    const-string v1, "ProjectSharing"

    const-string v2, "Device is null 1"

    invoke-static {v1, v2}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 785
    invoke-virtual {p0}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "wifi_p2p_device_name"

    invoke-static {v1, v2}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 787
    if-nez v0, :cond_0

    .line 788
    invoke-virtual {p0}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "device_name"

    invoke-static {v1, v2}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 789
    const-string v1, "ProjectSharing"

    const-string v2, "Device is null 2"

    invoke-static {v1, v2}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 792
    :cond_0
    const-string v1, "ProjectSharing"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Returning deviceName"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 793
    return-object v0
.end method

.method private handleDeviceNameRequestMsg(Ljava/lang/String;[[B)V
    .locals 6
    .param p1, "fromNode"    # Ljava/lang/String;
    .param p2, "payload"    # [[B

    .prologue
    const/4 v5, 0x0

    .line 807
    iget-object v2, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mDeviceMap:Ljava/util/HashMap;

    new-instance v3, Ljava/lang/String;

    aget-object v4, p2, v5

    invoke-direct {v3, v4}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v2, p1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 809
    const/4 v2, 0x1

    new-array v1, v2, [[B

    .line 810
    .local v1, "myName":[[B
    invoke-direct {p0}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->getMyDeviceName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    aput-object v2, v1, v5

    .line 811
    iget-object v2, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mChordManager:Lcom/samsung/chord/ChordManager;

    const-string v3, "CHORD_PRJ_SHARING_CHANNEL"

    iget-object v4, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mChannelListener:Lcom/samsung/chord/IChordChannelListener;

    invoke-virtual {v2, v3, v4}, Lcom/samsung/chord/ChordManager;->joinChannel(Ljava/lang/String;Lcom/samsung/chord/IChordChannelListener;)Lcom/samsung/chord/IChordChannel;

    move-result-object v0

    .line 812
    .local v0, "channel":Lcom/samsung/chord/IChordChannel;
    const-string v2, "MSG_RESPONSE_DEVICE_NAME"

    invoke-interface {v0, p1, v2, v1}, Lcom/samsung/chord/IChordChannel;->sendData(Ljava/lang/String;Ljava/lang/String;[[B)Z

    .line 813
    return-void
.end method

.method private handleDeviceNameResponseMsg(Ljava/lang/String;[[B)V
    .locals 3
    .param p1, "fromNode"    # Ljava/lang/String;
    .param p2, "payload"    # [[B

    .prologue
    .line 816
    iget-object v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mDeviceMap:Ljava/util/HashMap;

    new-instance v1, Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v2, p2, v2

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 817
    return-void
.end method

.method private handleProjectShareAckMsg(Ljava/lang/String;[[B)V
    .locals 2
    .param p1, "fromNode"    # Ljava/lang/String;
    .param p2, "payload"    # [[B

    .prologue
    .line 848
    if-eqz p2, :cond_0

    const/4 v0, 0x0

    aget-object v0, p2, v0

    array-length v0, v0

    if-lez v0, :cond_0

    .line 850
    iget-object v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    invoke-static {p2}, Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper;->prepareFinalListOfFilesToTransferAtSender([[B)Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$17(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;Ljava/util/ArrayList;)V

    .line 852
    invoke-direct {p0}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->beginProjectSharingFromChordThread()V

    .line 859
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mHandler:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ServiceHandler;

    const/16 v1, 0x6a

    invoke-virtual {v0, v1}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ServiceHandler;->removeMessages(I)V

    .line 862
    return-void

    .line 855
    :cond_0
    sget v0, Lcom/sec/android/app/ve/R$string;->prj_sharing_error:I

    invoke-direct {p0, v0}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->sendToastMessageToUIHandler(I)V

    .line 856
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    goto :goto_0
.end method

.method private handleReceiveProjectNotificationMsg(Ljava/lang/String;[[B)V
    .locals 2
    .param p1, "fromNode"    # Ljava/lang/String;
    .param p2, "payload"    # [[B

    .prologue
    .line 821
    new-instance v0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;-><init>(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;)V

    iput-object v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    .line 822
    iget-object v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$22(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;Ljava/util/HashMap;)V

    .line 823
    iget-object v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    invoke-static {v0, p1}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$23(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;Ljava/lang/String;)V

    .line 824
    iget-object v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    const/4 v1, 0x0

    aget-object v1, p2, v1

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$24(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;[B)V

    .line 844
    return-void
.end method

.method private inflateFileList(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 7
    .param p1, "listFile"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 640
    const/4 v4, 0x0

    .line 641
    .local v4, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v2, 0x0

    .line 643
    .local v2, "inputStream":Ljava/io/ObjectInputStream;
    :try_start_0
    new-instance v3, Ljava/io/ObjectInputStream;

    new-instance v5, Ljava/io/FileInputStream;

    new-instance v6, Ljava/io/File;

    invoke-direct {v6, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {v5, v6}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v3, v5}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/StreamCorruptedException; {:try_start_0 .. :try_end_0} :catch_b
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 645
    .end local v2    # "inputStream":Ljava/io/ObjectInputStream;
    .local v3, "inputStream":Ljava/io/ObjectInputStream;
    :try_start_1
    invoke-virtual {v3}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v5

    move-object v0, v5

    check-cast v0, Ljava/util/ArrayList;

    move-object v4, v0
    :try_end_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/StreamCorruptedException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_a
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_9
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 657
    :goto_0
    if-eqz v3, :cond_2

    .line 659
    :try_start_2
    invoke-virtual {v3}, Ljava/io/ObjectInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_8

    move-object v2, v3

    .line 664
    .end local v3    # "inputStream":Ljava/io/ObjectInputStream;
    .restart local v2    # "inputStream":Ljava/io/ObjectInputStream;
    :cond_0
    :goto_1
    return-object v4

    .line 646
    .end local v2    # "inputStream":Ljava/io/ObjectInputStream;
    .restart local v3    # "inputStream":Ljava/io/ObjectInputStream;
    :catch_0
    move-exception v1

    .line 647
    .local v1, "e":Ljava/lang/ClassNotFoundException;
    :try_start_3
    invoke-virtual {v1}, Ljava/lang/ClassNotFoundException;->printStackTrace()V
    :try_end_3
    .catch Ljava/io/StreamCorruptedException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_a
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_9
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    .line 649
    .end local v1    # "e":Ljava/lang/ClassNotFoundException;
    :catch_1
    move-exception v1

    move-object v2, v3

    .line 650
    .end local v3    # "inputStream":Ljava/io/ObjectInputStream;
    .local v1, "e":Ljava/io/StreamCorruptedException;
    .restart local v2    # "inputStream":Ljava/io/ObjectInputStream;
    :goto_2
    :try_start_4
    invoke-virtual {v1}, Ljava/io/StreamCorruptedException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 657
    if-eqz v2, :cond_0

    .line 659
    :try_start_5
    invoke-virtual {v2}, Ljava/io/ObjectInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_1

    .line 660
    :catch_2
    move-exception v1

    .line 661
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 651
    .end local v1    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v1

    .line 652
    .local v1, "e":Ljava/io/FileNotFoundException;
    :goto_3
    :try_start_6
    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 657
    if-eqz v2, :cond_0

    .line 659
    :try_start_7
    invoke-virtual {v2}, Ljava/io/ObjectInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    goto :goto_1

    .line 660
    :catch_4
    move-exception v1

    .line 661
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 653
    .end local v1    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v1

    .line 654
    .restart local v1    # "e":Ljava/io/IOException;
    :goto_4
    :try_start_8
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 657
    if-eqz v2, :cond_0

    .line 659
    :try_start_9
    invoke-virtual {v2}, Ljava/io/ObjectInputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_6

    goto :goto_1

    .line 660
    :catch_6
    move-exception v1

    .line 661
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 656
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v5

    .line 657
    :goto_5
    if-eqz v2, :cond_1

    .line 659
    :try_start_a
    invoke-virtual {v2}, Ljava/io/ObjectInputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_7

    .line 663
    :cond_1
    :goto_6
    throw v5

    .line 660
    :catch_7
    move-exception v1

    .line 661
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 660
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "inputStream":Ljava/io/ObjectInputStream;
    .restart local v3    # "inputStream":Ljava/io/ObjectInputStream;
    :catch_8
    move-exception v1

    .line 661
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .end local v1    # "e":Ljava/io/IOException;
    :cond_2
    move-object v2, v3

    .end local v3    # "inputStream":Ljava/io/ObjectInputStream;
    .restart local v2    # "inputStream":Ljava/io/ObjectInputStream;
    goto :goto_1

    .line 656
    .end local v2    # "inputStream":Ljava/io/ObjectInputStream;
    .restart local v3    # "inputStream":Ljava/io/ObjectInputStream;
    :catchall_1
    move-exception v5

    move-object v2, v3

    .end local v3    # "inputStream":Ljava/io/ObjectInputStream;
    .restart local v2    # "inputStream":Ljava/io/ObjectInputStream;
    goto :goto_5

    .line 653
    .end local v2    # "inputStream":Ljava/io/ObjectInputStream;
    .restart local v3    # "inputStream":Ljava/io/ObjectInputStream;
    :catch_9
    move-exception v1

    move-object v2, v3

    .end local v3    # "inputStream":Ljava/io/ObjectInputStream;
    .restart local v2    # "inputStream":Ljava/io/ObjectInputStream;
    goto :goto_4

    .line 651
    .end local v2    # "inputStream":Ljava/io/ObjectInputStream;
    .restart local v3    # "inputStream":Ljava/io/ObjectInputStream;
    :catch_a
    move-exception v1

    move-object v2, v3

    .end local v3    # "inputStream":Ljava/io/ObjectInputStream;
    .restart local v2    # "inputStream":Ljava/io/ObjectInputStream;
    goto :goto_3

    .line 649
    :catch_b
    move-exception v1

    goto :goto_2
.end method

.method private initChord(Z)V
    .locals 4
    .param p1, "isFromBR"    # Z

    .prologue
    .line 476
    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getInitialApplication()Landroid/app/Application;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/chord/ChordManager;->getInstance(Landroid/content/Context;)Lcom/samsung/chord/ChordManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mChordManager:Lcom/samsung/chord/ChordManager;

    .line 477
    iget-object v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mChordManager:Lcom/samsung/chord/ChordManager;

    iget-object v1, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mLooper:Landroid/os/Looper;

    invoke-virtual {v0, v1}, Lcom/samsung/chord/ChordManager;->setHandleEventLooper(Landroid/os/Looper;)V

    .line 478
    iget-object v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mChordManager:Lcom/samsung/chord/ChordManager;

    sget-object v1, Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper;->PATH_FOR_SHARED_PRJ_CONTENTS:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/chord/ChordManager;->setTempDirectory(Ljava/lang/String;)V

    .line 479
    iget-object v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mChordManager:Lcom/samsung/chord/ChordManager;

    iget-object v1, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mNetworkListener:Lcom/samsung/chord/ChordManager$INetworkListener;

    invoke-virtual {v0, v1}, Lcom/samsung/chord/ChordManager;->setNetworkListener(Lcom/samsung/chord/ChordManager$INetworkListener;)Z

    .line 480
    iget-object v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mChordManager:Lcom/samsung/chord/ChordManager;

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v2, v3}, Lcom/samsung/chord/ChordManager;->setNodeKeepAliveTimeout(J)V

    .line 481
    iget-object v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mChordManager:Lcom/samsung/chord/ChordManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/chord/ChordManager;->setSmartDiscoveryEnabled(Z)V

    .line 484
    invoke-direct {p0}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->isWiFiDirectInterfaceAvailable()Z

    move-result v0

    if-nez v0, :cond_1

    .line 485
    if-eqz p1, :cond_0

    .line 486
    const-string v0, "ProjectSharing"

    const-string v1, "interface list is empty, CANNOT START CHORD, will be started from inside onConnected"

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 498
    :goto_0
    return-void

    .line 491
    :cond_0
    const-string v0, "ProjectSharing"

    const-string v1, "interface list is empty, Stopping Chord service"

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 492
    invoke-virtual {p0}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->stopSelf()V

    goto :goto_0

    .line 496
    :cond_1
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->startChord(I)V

    goto :goto_0
.end method

.method private isWiFiDirectInterfaceAvailable()Z
    .locals 3

    .prologue
    .line 773
    const/4 v0, 0x0

    .line 774
    .local v0, "available":Z
    iget-object v2, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mChordManager:Lcom/samsung/chord/ChordManager;

    invoke-virtual {v2}, Lcom/samsung/chord/ChordManager;->getAvailableInterfaceTypes()Ljava/util/List;

    move-result-object v1

    .line 775
    .local v1, "infList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 776
    const/4 v0, 0x1

    .line 778
    :cond_0
    return v0
.end method

.method private notifyProjectTransferToDestination()V
    .locals 6

    .prologue
    .line 592
    const/4 v0, 0x0

    .line 593
    .local v0, "channel":Lcom/samsung/chord/IChordChannel;
    iget-object v2, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mChordManager:Lcom/samsung/chord/ChordManager;

    const-string v3, "CHORD_PRJ_SHARING_CHANNEL"

    iget-object v4, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mChannelListener:Lcom/samsung/chord/IChordChannelListener;

    invoke-virtual {v2, v3, v4}, Lcom/samsung/chord/ChordManager;->joinChannel(Ljava/lang/String;Lcom/samsung/chord/IChordChannelListener;)Lcom/samsung/chord/IChordChannel;

    move-result-object v0

    .line 594
    if-eqz v0, :cond_0

    .line 595
    const/4 v2, 0x1

    new-array v1, v2, [[B

    .line 596
    .local v1, "payload":[[B
    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mCurrentTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;
    invoke-static {v3}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$13(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;)Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper;->prepareProjectMetaDataAtSender(Lcom/samsung/app/video/editor/external/TranscodeElement;)Lorg/json/JSONObject;

    move-result-object v3

    invoke-virtual {v3}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    aput-object v3, v1, v2

    .line 597
    iget-object v2, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mDestinationNode:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$16(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "MSG_NOTIFY_PRJ_SHARE"

    invoke-interface {v0, v2, v3, v1}, Lcom/samsung/chord/IChordChannel;->sendData(Ljava/lang/String;Ljava/lang/String;[[B)Z

    .line 606
    iget-object v2, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mHandler:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ServiceHandler;

    const/16 v3, 0x6a

    const-wide/16 v4, 0x7530

    invoke-virtual {v2, v3, v4, v5}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ServiceHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 608
    .end local v1    # "payload":[[B
    :cond_0
    return-void
.end method

.method private saveTempFile(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 10
    .param p1, "tmpFilename"    # Ljava/lang/String;
    .param p2, "originalFilename"    # Ljava/lang/String;
    .param p3, "index"    # I

    .prologue
    .line 612
    new-instance v6, Ljava/io/File;

    invoke-direct {v6, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 613
    .local v6, "tmpFile":Ljava/io/File;
    new-instance v5, Ljava/io/File;

    sget-object v7, Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper;->PATH_FOR_SHARED_PRJ_CONTENTS:Ljava/lang/String;

    invoke-direct {v5, v7, p2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 615
    .local v5, "targetFile":Ljava/io/File;
    const-string v7, "."

    invoke-virtual {p2, v7}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v3

    .line 616
    .local v3, "i":I
    const/4 v7, 0x0

    invoke-virtual {p2, v7, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 617
    .local v2, "fileName":Ljava/lang/String;
    invoke-virtual {p2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 618
    .local v1, "ext":Ljava/lang/String;
    move-object v4, p2

    .line 620
    .local v4, "savedName":Ljava/lang/String;
    const/4 v0, 0x0

    .line 621
    .local v0, "counter":I
    :goto_0
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_2

    .line 628
    invoke-virtual {v6, v5}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 630
    const-string v7, ".flist"

    invoke-virtual {p2, v7}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 631
    iget-object v7, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v8}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->inflateFileList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$17(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;Ljava/util/ArrayList;)V

    .line 634
    :cond_0
    iget-object v7, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mFilesBeingTransferred:Ljava/util/ArrayList;
    invoke-static {v7}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$7(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;)Ljava/util/ArrayList;

    move-result-object v7

    if-eqz v7, :cond_1

    .line 635
    iget-object v7, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mReceiverFileMapping:Ljava/util/HashMap;
    invoke-static {v7}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$9(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;)Ljava/util/HashMap;

    move-result-object v8

    iget-object v7, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mFilesBeingTransferred:Ljava/util/ArrayList;
    invoke-static {v7}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$7(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;)Ljava/util/ArrayList;

    move-result-object v7

    add-int/lit8 v9, p3, -0x1

    invoke-virtual {v7, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v7, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 636
    :cond_1
    return-void

    .line 622
    :cond_2
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, "_"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 623
    new-instance v5, Ljava/io/File;

    .end local v5    # "targetFile":Ljava/io/File;
    sget-object v7, Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper;->PATH_FOR_SHARED_PRJ_CONTENTS:Ljava/lang/String;

    invoke-direct {v5, v7, v4}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 624
    .restart local v5    # "targetFile":Ljava/io/File;
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private sendLocalBroadcast(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "msg"    # Ljava/lang/String;
    .param p2, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 797
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 798
    .local v0, "broadcastIntent":Landroid/content/Intent;
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 800
    if-eqz p2, :cond_0

    .line 801
    invoke-virtual {v0, p2}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 803
    :cond_0
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 804
    return-void
.end method

.method private sendToastMessageToUIHandler(I)V
    .locals 4
    .param p1, "stringID"    # I

    .prologue
    .line 686
    invoke-static {p1}, Lcom/sec/android/app/ve/VEApp;->getStringValue(I)Ljava/lang/String;

    move-result-object v0

    .line 687
    .local v0, "messageText":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mUIThreadHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mUIThreadHandler:Landroid/os/Handler;

    const/16 v3, 0x3e8

    invoke-static {v2, v3, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 688
    return-void
.end method

.method private startChord(I)V
    .locals 3
    .param p1, "networkInterface"    # I

    .prologue
    .line 751
    iget-boolean v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mIsChordStarted:Z

    if-nez v0, :cond_0

    .line 752
    iget-object v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mChordManager:Lcom/samsung/chord/ChordManager;

    iget-object v1, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mManagerListener:Lcom/samsung/chord/IChordManagerListener;

    invoke-virtual {v0, p1, v1}, Lcom/samsung/chord/ChordManager;->start(ILcom/samsung/chord/IChordManagerListener;)I

    move-result v0

    if-nez v0, :cond_1

    .line 753
    const-string v0, "ProjectSharing"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Successfully started CHORD MANAGER, interface is "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->getInterfaceName(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 758
    :cond_0
    :goto_0
    return-void

    .line 756
    :cond_1
    const-string v0, "ProjectSharing"

    const-string v1, "FAILED TO START CHORD MANAGER"

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private updateReceiverProgressNotification(JZZ)Landroid/app/Notification;
    .locals 11
    .param p1, "fileOffset"    # J
    .param p3, "notify"    # Z
    .param p4, "addTicker"    # Z

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 701
    const/4 v2, 0x0

    .line 702
    .local v2, "notification":Landroid/app/Notification;
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->getCurrentProgressPercentage(J)I

    move-result v3

    .line 703
    .local v3, "percentage":I
    iget-object v4, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mPercentageCompleted:I
    invoke-static {v4}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$18(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;)I

    move-result v4

    if-eq v3, v4, :cond_2

    .line 704
    sget v4, Lcom/sec/android/app/ve/R$string;->prj_sharing_receiver_progress_msg:I

    invoke-static {v4}, Lcom/sec/android/app/ve/VEApp;->getStringValue(I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v9, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mDeviceMap:Ljava/util/HashMap;

    iget-object v7, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mSourceNode:Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$11(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 705
    .local v0, "contextText":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mNotificationBuilder:Landroid/app/Notification$Builder;
    invoke-static {v4}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$19(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;)Landroid/app/Notification$Builder;

    move-result-object v4

    sget-object v5, Lcom/sec/android/app/ve/VEApp;->gAdaper:Lcom/sec/android/app/ve/VEAdapter;

    invoke-interface {v5}, Lcom/sec/android/app/ve/VEAdapter;->getAppIconResource()I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v4

    .line 706
    sget v5, Lcom/sec/android/app/ve/R$string;->prj_sharing_receiver_progress_title:I

    invoke-static {v5}, Lcom/sec/android/app/ve/VEApp;->getStringValue(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v4

    .line 707
    invoke-virtual {v4, v0}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v4

    .line 708
    const/16 v5, 0x64

    invoke-virtual {v4, v5, v3, v8}, Landroid/app/Notification$Builder;->setProgress(IIZ)Landroid/app/Notification$Builder;

    move-result-object v4

    .line 709
    invoke-virtual {v4, v9}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    .line 711
    if-eqz p4, :cond_0

    .line 712
    iget-object v4, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mNotificationBuilder:Landroid/app/Notification$Builder;
    invoke-static {v4}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$19(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;)Landroid/app/Notification$Builder;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 714
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mNotificationBuilder:Landroid/app/Notification$Builder;
    invoke-static {v4}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$19(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;)Landroid/app/Notification$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v2

    .line 716
    if-eqz p3, :cond_1

    .line 717
    const-string v4, "notification"

    invoke-virtual {p0, v4}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/NotificationManager;

    .line 718
    .local v1, "nm":Landroid/app/NotificationManager;
    const/16 v4, 0xb

    invoke-virtual {v1, v4, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 721
    .end local v1    # "nm":Landroid/app/NotificationManager;
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    invoke-static {v4, v3}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$20(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;I)V

    .line 723
    .end local v0    # "contextText":Ljava/lang/String;
    :cond_2
    return-object v2
.end method

.method private updateSenderProgressNotification(JZZ)Landroid/app/Notification;
    .locals 11
    .param p1, "fileOffset"    # J
    .param p3, "notify"    # Z
    .param p4, "addTicker"    # Z

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 727
    const/4 v2, 0x0

    .line 728
    .local v2, "notification":Landroid/app/Notification;
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->getCurrentProgressPercentage(J)I

    move-result v3

    .line 729
    .local v3, "percentage":I
    iget-object v4, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mPercentageCompleted:I
    invoke-static {v4}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$18(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;)I

    move-result v4

    if-eq v3, v4, :cond_2

    .line 730
    sget v4, Lcom/sec/android/app/ve/R$string;->prj_sharing_sender_progress_msg:I

    invoke-static {v4}, Lcom/sec/android/app/ve/VEApp;->getStringValue(I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v9, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mDeviceMap:Ljava/util/HashMap;

    iget-object v7, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mDestinationNode:Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$16(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 731
    .local v0, "contentText":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mNotificationBuilder:Landroid/app/Notification$Builder;
    invoke-static {v4}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$19(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;)Landroid/app/Notification$Builder;

    move-result-object v4

    sget v5, Lcom/sec/android/app/ve/R$string;->prj_sharing_receiver_progress_title:I

    invoke-static {v5}, Lcom/sec/android/app/ve/VEApp;->getStringValue(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v4

    .line 732
    sget-object v5, Lcom/sec/android/app/ve/VEApp;->gAdaper:Lcom/sec/android/app/ve/VEAdapter;

    invoke-interface {v5}, Lcom/sec/android/app/ve/VEAdapter;->getAppIconResource()I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v4

    .line 733
    invoke-virtual {v4, v0}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v4

    .line 734
    const/16 v5, 0x64

    invoke-virtual {v4, v5, v3, v8}, Landroid/app/Notification$Builder;->setProgress(IIZ)Landroid/app/Notification$Builder;

    move-result-object v4

    .line 735
    invoke-virtual {v4, v9}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    .line 736
    if-eqz p4, :cond_0

    .line 737
    iget-object v4, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mNotificationBuilder:Landroid/app/Notification$Builder;
    invoke-static {v4}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$19(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;)Landroid/app/Notification$Builder;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 739
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mNotificationBuilder:Landroid/app/Notification$Builder;
    invoke-static {v4}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$19(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;)Landroid/app/Notification$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v2

    .line 740
    if-eqz p3, :cond_1

    .line 741
    const-string v4, "notification"

    invoke-virtual {p0, v4}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/NotificationManager;

    .line 742
    .local v1, "nm":Landroid/app/NotificationManager;
    const/16 v4, 0xb

    invoke-virtual {v1, v4, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 745
    .end local v1    # "nm":Landroid/app/NotificationManager;
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    invoke-static {v4, v3}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$20(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;I)V

    .line 747
    .end local v0    # "contentText":Ljava/lang/String;
    :cond_2
    return-object v2
.end method


# virtual methods
.method public approveProjectShareRequest(ZZ)V
    .locals 4
    .param p1, "approve"    # Z
    .param p2, "overwriteExisting"    # Z

    .prologue
    .line 914
    new-instance v1, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ProjectApprovalMessage;

    invoke-direct {v1, p1, p2}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ProjectApprovalMessage;-><init>(ZZ)V

    .line 915
    .local v1, "obj":Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ProjectApprovalMessage;
    iget-object v2, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mHandler:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ServiceHandler;

    const/16 v3, 0x6b

    invoke-static {v2, v3, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 916
    .local v0, "msg":Landroid/os/Message;
    iget-object v2, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mHandler:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ServiceHandler;

    invoke-virtual {v2, v0}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ServiceHandler;->sendMessage(Landroid/os/Message;)Z

    .line 917
    return-void
.end method

.method public getChordDeviceNameList()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 865
    const/4 v1, 0x0

    .line 866
    .local v1, "deviceList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v3, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mChordManager:Lcom/samsung/chord/ChordManager;

    if-eqz v3, :cond_1

    .line 867
    iget-object v3, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mChordManager:Lcom/samsung/chord/ChordManager;

    const-string v4, "CHORD_PRJ_SHARING_CHANNEL"

    invoke-virtual {v3, v4}, Lcom/samsung/chord/ChordManager;->getJoinedChannel(Ljava/lang/String;)Lcom/samsung/chord/IChordChannel;

    move-result-object v0

    .line 868
    .local v0, "channel":Lcom/samsung/chord/IChordChannel;
    if-nez v0, :cond_0

    move-object v2, v1

    .line 873
    .end local v0    # "channel":Lcom/samsung/chord/IChordChannel;
    .end local v1    # "deviceList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local v2, "deviceList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :goto_0
    return-object v2

    .line 871
    .end local v2    # "deviceList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v0    # "channel":Lcom/samsung/chord/IChordChannel;
    .restart local v1    # "deviceList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_0
    invoke-interface {v0}, Lcom/samsung/chord/IChordChannel;->getJoinedNodeList()Ljava/util/List;

    move-result-object v1

    .end local v0    # "channel":Lcom/samsung/chord/IChordChannel;
    :cond_1
    move-object v2, v1

    .line 873
    .end local v1    # "deviceList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v2    # "deviceList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    goto :goto_0
.end method

.method public getDisplayDeviceNameList()Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 877
    const/4 v3, 0x0

    .line 878
    .local v3, "displayList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v2, 0x0

    .line 879
    .local v2, "deviceList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v6, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mChordManager:Lcom/samsung/chord/ChordManager;

    if-eqz v6, :cond_1

    .line 880
    iget-object v6, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mChordManager:Lcom/samsung/chord/ChordManager;

    const-string v7, "CHORD_PRJ_SHARING_CHANNEL"

    invoke-virtual {v6, v7}, Lcom/samsung/chord/ChordManager;->getJoinedChannel(Ljava/lang/String;)Lcom/samsung/chord/IChordChannel;

    move-result-object v0

    .line 881
    .local v0, "channel":Lcom/samsung/chord/IChordChannel;
    if-nez v0, :cond_0

    move-object v6, v2

    .line 894
    .end local v0    # "channel":Lcom/samsung/chord/IChordChannel;
    :goto_0
    return-object v6

    .line 884
    .restart local v0    # "channel":Lcom/samsung/chord/IChordChannel;
    :cond_0
    invoke-interface {v0}, Lcom/samsung/chord/IChordChannel;->getJoinedNodeList()Ljava/util/List;

    move-result-object v2

    .line 885
    if-eqz v2, :cond_1

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_1

    .line 886
    new-instance v3, Ljava/util/ArrayList;

    .end local v3    # "displayList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 887
    .restart local v3    # "displayList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_1
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v6

    if-lt v5, v6, :cond_2

    .end local v0    # "channel":Lcom/samsung/chord/IChordChannel;
    .end local v5    # "i":I
    :cond_1
    move-object v6, v3

    .line 894
    goto :goto_0

    .line 888
    .restart local v0    # "channel":Lcom/samsung/chord/IChordChannel;
    .restart local v5    # "i":I
    :cond_2
    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 889
    .local v1, "device":Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mDeviceMap:Ljava/util/HashMap;

    invoke-virtual {v6, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 890
    .local v4, "displayName":Ljava/lang/String;
    if-eqz v4, :cond_3

    .end local v4    # "displayName":Ljava/lang/String;
    :goto_2
    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 887
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .restart local v4    # "displayName":Ljava/lang/String;
    :cond_3
    move-object v4, v1

    .line 890
    goto :goto_2
.end method

.method public getProjectMetaData()[B
    .locals 1

    .prologue
    .line 920
    iget-object v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    if-eqz v0, :cond_0

    .line 921
    iget-object v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mProjectMetaData:[B
    invoke-static {v0}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$10(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;)[B

    move-result-object v0

    .line 923
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSourceDevicename()Ljava/lang/String;
    .locals 2

    .prologue
    .line 927
    iget-object v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mDeviceMap:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    .line 928
    iget-object v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mDeviceMap:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mSourceNode:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$11(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 930
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isBusy()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 936
    const-string v3, "ProjectSharing"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v0, "Chord Service is busy->"

    invoke-direct {v4, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 937
    iget-object v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    if-eqz v0, :cond_1

    :goto_1
    return v1

    :cond_0
    move v0, v2

    .line 936
    goto :goto_0

    :cond_1
    move v1, v2

    .line 937
    goto :goto_1
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "arg0"    # Landroid/content/Intent;

    .prologue
    .line 432
    iget-object v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mBinder:Landroid/os/IBinder;

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 437
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 438
    new-instance v1, Landroid/os/HandlerThread;

    const-string v2, "ProjectSharingThread"

    invoke-direct {v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mHandlerThread:Landroid/os/HandlerThread;

    .line 439
    iget-object v1, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V

    .line 440
    iget-object v1, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mLooper:Landroid/os/Looper;

    .line 441
    new-instance v1, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ServiceHandler;

    iget-object v2, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mLooper:Landroid/os/Looper;

    invoke-direct {v1, p0, v2}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ServiceHandler;-><init>(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mHandler:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ServiceHandler;

    .line 442
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mDeviceMap:Ljava/util/HashMap;

    .line 444
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 445
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    const-string v1, "BROADCAST_FINISH_CHORD_SERVICE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 446
    const-string v1, "BROADCAST_START_CHORD_SESSION"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 447
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mLocalBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 448
    const-string v1, "ProjectSharing"

    const-string v2, "inside ChordChannelService.onCreate"

    invoke-static {v1, v2}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 449
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 465
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mLocalBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 466
    iget-object v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mHandler:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ServiceHandler;

    const/16 v1, 0x65

    invoke-virtual {v0, v1}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ServiceHandler;->sendEmptyMessage(I)Z

    .line 467
    iget-object v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mHandler:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ServiceHandler;

    const/16 v1, 0x66

    invoke-virtual {v0, v1}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ServiceHandler;->sendEmptyMessage(I)Z

    .line 468
    const-string v0, "BROADCAST_CHORD_SERVICE_FINISHED"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->sendLocalBroadcast(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 471
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 472
    const-string v0, "ProjectSharing"

    const-string v1, "inside ChordChannelService.onDestroy"

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 473
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 5
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 453
    const-string v2, "ProjectSharing"

    const-string v3, "inside ChordChannelService.onStartCommand"

    invoke-static {v2, v3}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 454
    const/4 v0, 0x0

    .line 455
    .local v0, "fromBR":Z
    if-eqz p1, :cond_0

    .line 456
    const-string v2, "INTENT_EXTRA_FROM_BROADCASTRECEIVER"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 457
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mHandler:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ServiceHandler;

    const/16 v3, 0x64

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-static {v2, v3, v4}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 458
    .local v1, "msg":Landroid/os/Message;
    iget-object v2, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mHandler:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ServiceHandler;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ServiceHandler;->sendMessage(Landroid/os/Message;)Z

    .line 459
    const-string v2, "BROADCAST_CHORD_SERVICE_STARTED"

    const/4 v3, 0x0

    invoke-direct {p0, v2, v3}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->sendLocalBroadcast(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 460
    const/4 v2, 0x1

    return v2
.end method

.method public startProjectSharingSession(Ljava/lang/String;Lcom/samsung/app/video/editor/external/TranscodeElement;)V
    .locals 3
    .param p1, "destination"    # Ljava/lang/String;
    .param p2, "shareThisTE"    # Lcom/samsung/app/video/editor/external/TranscodeElement;

    .prologue
    .line 898
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    if-nez v1, :cond_0

    .line 899
    new-instance v1, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;-><init>(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;)V

    iput-object v1, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    .line 900
    iget-object v1, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$25(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;Z)V

    .line 901
    iget-object v1, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    invoke-static {v1, p1}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$26(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;Ljava/lang/String;)V

    .line 902
    iget-object v1, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    invoke-static {v1, p2}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$27(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;Lcom/samsung/app/video/editor/external/TranscodeElement;)V

    .line 903
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 904
    .local v0, "msg":Landroid/os/Message;
    const/16 v1, 0x69

    iput v1, v0, Landroid/os/Message;->what:I

    .line 905
    iget-object v1, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mHandler:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ServiceHandler;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ServiceHandler;->sendMessage(Landroid/os/Message;)Z

    .line 910
    .end local v0    # "msg":Landroid/os/Message;
    :goto_0
    return-void

    .line 908
    :cond_0
    sget v1, Lcom/sec/android/app/ve/R$string;->prj_sharing_busy:I

    invoke-direct {p0, v1}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->sendToastMessageToUIHandler(I)V

    goto :goto_0
.end method
