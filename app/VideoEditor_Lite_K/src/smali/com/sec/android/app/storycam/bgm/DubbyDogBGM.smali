.class public Lcom/sec/android/app/storycam/bgm/DubbyDogBGM;
.super Lcom/sec/android/app/ve/bgm/BGM;
.source "DubbyDogBGM.java"


# static fields
.field private static synthetic $SWITCH_TABLE$com$sec$android$app$ve$bgm$BGM$BGMModes:[I


# instance fields
.field private fastModeBGM:[I

.field private mediumModeBGM:[I

.field private slowModeBGM:[I

.field private totalDurationBGM:I


# direct methods
.method static synthetic $SWITCH_TABLE$com$sec$android$app$ve$bgm$BGM$BGMModes()[I
    .locals 3

    .prologue
    .line 6
    sget-object v0, Lcom/sec/android/app/storycam/bgm/DubbyDogBGM;->$SWITCH_TABLE$com$sec$android$app$ve$bgm$BGM$BGMModes:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/sec/android/app/ve/bgm/BGM$BGMModes;->values()[Lcom/sec/android/app/ve/bgm/BGM$BGMModes;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/sec/android/app/ve/bgm/BGM$BGMModes;->FAST:Lcom/sec/android/app/ve/bgm/BGM$BGMModes;

    invoke-virtual {v1}, Lcom/sec/android/app/ve/bgm/BGM$BGMModes;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    :try_start_1
    sget-object v1, Lcom/sec/android/app/ve/bgm/BGM$BGMModes;->MEDIUM:Lcom/sec/android/app/ve/bgm/BGM$BGMModes;

    invoke-virtual {v1}, Lcom/sec/android/app/ve/bgm/BGM$BGMModes;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    :try_start_2
    sget-object v1, Lcom/sec/android/app/ve/bgm/BGM$BGMModes;->SLOW:Lcom/sec/android/app/ve/bgm/BGM$BGMModes;

    invoke-virtual {v1}, Lcom/sec/android/app/ve/bgm/BGM$BGMModes;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_3
    sput-object v0, Lcom/sec/android/app/storycam/bgm/DubbyDogBGM;->$SWITCH_TABLE$com$sec$android$app$ve$bgm$BGM$BGMModes:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_1
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 6
    invoke-direct {p0}, Lcom/sec/android/app/ve/bgm/BGM;-><init>()V

    .line 8
    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    .line 13
    iput-object v0, p0, Lcom/sec/android/app/storycam/bgm/DubbyDogBGM;->slowModeBGM:[I

    .line 16
    const/16 v0, 0x9

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    .line 24
    iput-object v0, p0, Lcom/sec/android/app/storycam/bgm/DubbyDogBGM;->mediumModeBGM:[I

    .line 27
    const/16 v0, 0xc

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    .line 38
    iput-object v0, p0, Lcom/sec/android/app/storycam/bgm/DubbyDogBGM;->fastModeBGM:[I

    .line 41
    const v0, 0x9218

    iput v0, p0, Lcom/sec/android/app/storycam/bgm/DubbyDogBGM;->totalDurationBGM:I

    .line 6
    return-void

    .line 8
    nop

    :array_0
    .array-data 4
        0x125c
        0x24b8
        0x3714
        0x4a38
        0x5dc0
        0x7468
    .end array-data

    .line 16
    :array_1
    .array-data 4
        0x8fc
        0x17d4
        0x2774
        0x3714
        0x46b4
        0x5654
        0x65f4
        0x7850
        0x8854
    .end array-data

    .line 27
    :array_2
    .array-data 4
        0x834
        0x13ec
        0x1fa4
        0x2b5c
        0x3714
        0x4394
        0x4fb0
        0x5c94
        0x684c
        0x7468
        0x8020
        0x8bd8
    .end array-data
.end method


# virtual methods
.method protected getBGMFilepath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    const-string v0, "themedata/mirror/bgm/B1_Dubby.ogg"

    return-object v0
.end method

.method protected getBGMName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    const-string v0, "Dubby"

    return-object v0
.end method

.method protected getTotalDurationBGM()I
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Lcom/sec/android/app/storycam/bgm/DubbyDogBGM;->totalDurationBGM:I

    return v0
.end method

.method protected getTransitionTimeStamps(Lcom/sec/android/app/ve/bgm/BGM$BGMModes;)[I
    .locals 2
    .param p1, "mode"    # Lcom/sec/android/app/ve/bgm/BGM$BGMModes;

    .prologue
    .line 45
    invoke-static {}, Lcom/sec/android/app/storycam/bgm/DubbyDogBGM;->$SWITCH_TABLE$com$sec$android$app$ve$bgm$BGM$BGMModes()[I

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/android/app/ve/bgm/BGM$BGMModes;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 52
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 47
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/storycam/bgm/DubbyDogBGM;->slowModeBGM:[I

    goto :goto_0

    .line 49
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/storycam/bgm/DubbyDogBGM;->mediumModeBGM:[I

    goto :goto_0

    .line 51
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/storycam/bgm/DubbyDogBGM;->fastModeBGM:[I

    goto :goto_0

    .line 45
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
