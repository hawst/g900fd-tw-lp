.class public Lcom/sec/android/app/ve/util/ArtistCacheUtil;
.super Ljava/lang/Object;
.source "ArtistCacheUtil.java"


# static fields
.field public static artistList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/ve/data/VEAlbum;",
            ">;"
        }
    .end annotation
.end field

.field private static artistThread:Ljava/lang/Thread;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0()Ljava/util/List;
    .locals 1

    .prologue
    .line 40
    invoke-static {}, Lcom/sec/android/app/ve/util/ArtistCacheUtil;->readArtistList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static clear()V
    .locals 1

    .prologue
    .line 62
    sget-object v0, Lcom/sec/android/app/ve/util/ArtistCacheUtil;->artistList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 63
    return-void
.end method

.method public static defaultAudioWhereClause()Ljava/lang/String;
    .locals 2

    .prologue
    .line 146
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 147
    .local v0, "where":Ljava/lang/StringBuilder;
    const-string v1, " (is_music = 1) "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 148
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static durationWhereClause()Ljava/lang/String;
    .locals 2

    .prologue
    .line 159
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 160
    .local v0, "where":Ljava/lang/StringBuilder;
    const-string v1, " (duration > 0) "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 162
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getAllArtists()Landroid/database/Cursor;
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 68
    const/4 v0, 0x5

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v0

    const/4 v0, 0x1

    .line 69
    const-string v4, "artist"

    aput-object v4, v2, v0

    const/4 v0, 0x2

    .line 70
    const-string v4, "artist_key"

    aput-object v4, v2, v0

    const/4 v0, 0x3

    .line 71
    const-string v4, "number_of_albums"

    aput-object v4, v2, v0

    const/4 v0, 0x4

    .line 72
    const-string v4, "number_of_tracks"

    aput-object v4, v2, v0

    .line 73
    .local v2, "col":[Ljava/lang/String;
    sget-object v1, Landroid/provider/MediaStore$Audio$Artists;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 75
    .local v1, "lUri":Landroid/net/Uri;
    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 77
    const-string v5, "artist_key"

    move-object v4, v3

    .line 76
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 78
    .local v6, "lCur":Landroid/database/Cursor;
    return-object v6
.end method

.method public static getArtistList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/ve/data/VEAlbum;",
            ">;"
        }
    .end annotation

    .prologue
    .line 58
    sget-object v0, Lcom/sec/android/app/ve/util/ArtistCacheUtil;->artistList:Ljava/util/List;

    return-object v0
.end method

.method public static getArtistSongs(I)Landroid/database/Cursor;
    .locals 8
    .param p0, "aAristId"    # I

    .prologue
    .line 123
    const/16 v0, 0x8

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v0

    const/4 v0, 0x1

    .line 124
    const-string v4, "album_id"

    aput-object v4, v2, v0

    const/4 v0, 0x2

    const-string v4, "artist"

    aput-object v4, v2, v0

    const/4 v0, 0x3

    .line 125
    const-string v4, "album"

    aput-object v4, v2, v0

    const/4 v0, 0x4

    const-string v4, "title"

    aput-object v4, v2, v0

    const/4 v0, 0x5

    .line 126
    const-string v4, "_data"

    aput-object v4, v2, v0

    const/4 v0, 0x6

    const-string v4, "title_key"

    aput-object v4, v2, v0

    const/4 v0, 0x7

    .line 127
    const-string v4, "duration"

    aput-object v4, v2, v0

    .line 128
    .local v2, "col":[Ljava/lang/String;
    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 129
    .local v1, "lUri":Landroid/net/Uri;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 131
    .local v6, "builder":Ljava/lang/StringBuilder;
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "artist_id = "

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 132
    const-string v0, " AND "

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 133
    invoke-static {}, Lcom/sec/android/app/ve/util/ArtistCacheUtil;->defaultAudioWhereClause()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 134
    const-string v0, " AND "

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 135
    invoke-static {}, Lcom/sec/android/app/ve/util/ArtistCacheUtil;->mp3WhereClause()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 136
    const-string v0, " AND "

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 137
    invoke-static {}, Lcom/sec/android/app/ve/util/ArtistCacheUtil;->durationWhereClause()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 139
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 140
    .local v3, "selection":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 141
    const/4 v4, 0x0

    const-string v5, "title"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 142
    .local v7, "lCur":Landroid/database/Cursor;
    return-object v7
.end method

.method public static loadArtistList()V
    .locals 2

    .prologue
    .line 18
    sget-object v0, Lcom/sec/android/app/ve/util/ArtistCacheUtil;->artistThread:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/app/ve/util/ArtistCacheUtil;->artistThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 38
    :goto_0
    return-void

    .line 21
    :cond_0
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/app/ve/util/ArtistCacheUtil$1;

    invoke-direct {v1}, Lcom/sec/android/app/ve/util/ArtistCacheUtil$1;-><init>()V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    sput-object v0, Lcom/sec/android/app/ve/util/ArtistCacheUtil;->artistThread:Ljava/lang/Thread;

    .line 36
    sget-object v0, Lcom/sec/android/app/ve/util/ArtistCacheUtil;->artistThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 37
    sget-object v0, Lcom/sec/android/app/ve/util/ArtistCacheUtil;->artistThread:Ljava/lang/Thread;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setPriority(I)V

    goto :goto_0
.end method

.method public static mp3WhereClause()Ljava/lang/String;
    .locals 2

    .prologue
    .line 152
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 153
    .local v0, "where":Ljava/lang/StringBuilder;
    const-string v1, " (_data LIKE \'%.mp3\' OR _data LIKE  \'%.3gpp\' OR _data LIKE  \'%.MP3\' OR _data LIKE  \'%.3GPP\'OR _data LIKE  \'%.3ga\' OR _data LIKE  \'%.3GA\' OR _data LIKE  \'%.m4a\' OR _data LIKE  \'%.M4A\' OR _data LIKE  \'%.3GP\' OR _data LIKE  \'%.3gp\') "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 155
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private static parseArtistsCursor(Landroid/database/Cursor;)Ljava/util/List;
    .locals 6
    .param p0, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/ve/data/VEAlbum;",
            ">;"
        }
    .end annotation

    .prologue
    .line 83
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 87
    .local v2, "lVEAlbumList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/ve/data/VEAlbum;>;"
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    .line 88
    .local v4, "valid":Z
    :goto_0
    if-nez v4, :cond_0

    .line 100
    return-object v2

    .line 90
    :cond_0
    const-string v5, "_id"

    invoke-interface {p0, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    .line 89
    invoke-interface {p0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 92
    .local v0, "id":Ljava/lang/String;
    const-string v5, "artist"

    invoke-interface {p0, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    .line 91
    invoke-interface {p0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 94
    .local v3, "name":Ljava/lang/String;
    new-instance v1, Lcom/sec/android/app/ve/data/VEAlbum;

    invoke-direct {v1}, Lcom/sec/android/app/ve/data/VEAlbum;-><init>()V

    .line 95
    .local v1, "lVEAlbum":Lcom/sec/android/app/ve/data/VEAlbum;
    invoke-virtual {v1, v0}, Lcom/sec/android/app/ve/data/VEAlbum;->setMusicCategoryId(Ljava/lang/String;)V

    .line 96
    invoke-virtual {v1, v3}, Lcom/sec/android/app/ve/data/VEAlbum;->setMusicCategoryName(Ljava/lang/String;)V

    .line 97
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 98
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    goto :goto_0
.end method

.method private static readArtistList()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/ve/data/VEAlbum;",
            ">;"
        }
    .end annotation

    .prologue
    .line 42
    const/4 v2, 0x0

    .line 45
    .local v2, "lVEAlbumList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/ve/data/VEAlbum;>;"
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/ve/util/ArtistCacheUtil;->getAllArtists()Landroid/database/Cursor;

    move-result-object v0

    .line 46
    .local v0, "cursor":Landroid/database/Cursor;
    if-eqz v0, :cond_0

    .line 47
    invoke-static {v0}, Lcom/sec/android/app/ve/util/ArtistCacheUtil;->parseArtistsCursor(Landroid/database/Cursor;)Ljava/util/List;

    move-result-object v2

    .line 48
    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 54
    .end local v0    # "cursor":Landroid/database/Cursor;
    :cond_0
    :goto_0
    return-object v2

    .line 50
    :catch_0
    move-exception v1

    .line 51
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static readSongsCountOfArtist(Ljava/lang/String;)I
    .locals 4
    .param p0, "artistId"    # Ljava/lang/String;

    .prologue
    .line 106
    const/4 v0, 0x0

    .line 108
    .local v0, "count":I
    :try_start_0
    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 109
    .local v2, "id":I
    invoke-static {v2}, Lcom/sec/android/app/ve/util/ArtistCacheUtil;->getArtistSongs(I)Landroid/database/Cursor;

    move-result-object v1

    .line 110
    .local v1, "cursor":Landroid/database/Cursor;
    if-eqz v1, :cond_0

    .line 111
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    .line 112
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 117
    .end local v1    # "cursor":Landroid/database/Cursor;
    .end local v2    # "id":I
    :cond_0
    :goto_0
    return v0

    .line 114
    :catch_0
    move-exception v3

    .line 115
    .local v3, "pe":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
