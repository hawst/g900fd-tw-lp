.class Lcom/sec/android/app/storycam/view/PreviewViewGroup$8;
.super Ljava/lang/Object;
.source "PreviewViewGroup.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/storycam/view/PreviewViewGroup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$8;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    .line 721
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 4
    .param p1, "bar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromUser"    # Z

    .prologue
    .line 725
    int-to-long v2, p2

    sget-object v1, Lcom/sec/android/app/ve/util/CommonUtils$TimeInTextUnits;->MMSS:Lcom/sec/android/app/ve/util/CommonUtils$TimeInTextUnits;

    invoke-static {v2, v3, v1}, Lcom/sec/android/app/ve/util/CommonUtils;->getTimeInText(JLcom/sec/android/app/ve/util/CommonUtils$TimeInTextUnits;)Ljava/lang/String;

    move-result-object v0

    .line 726
    .local v0, "storytime":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$8;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    # getter for: Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mStoryBoardCurrTime:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$7(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 727
    if-eqz p3, :cond_0

    .line 728
    iget-object v1, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$8;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    # invokes: Lcom/sec/android/app/storycam/view/PreviewViewGroup;->refreshPreviewForCurrentSeekBarPosition(I)V
    invoke-static {v1, p2}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$6(Lcom/sec/android/app/storycam/view/PreviewViewGroup;I)V

    .line 729
    iget-object v1, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$8;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    invoke-virtual {v1}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->updateCurrentTimeContentDesc()V

    .line 731
    :cond_0
    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 2
    .param p1, "arg0"    # Landroid/widget/SeekBar;

    .prologue
    .line 735
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$8;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    # getter for: Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mPreview:Lcom/sec/android/app/ve/PreviewPlayerInterface;
    invoke-static {v0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$0(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)Lcom/sec/android/app/ve/PreviewPlayerInterface;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$8;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    # getter for: Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mPreview:Lcom/sec/android/app/ve/PreviewPlayerInterface;
    invoke-static {v0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$0(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)Lcom/sec/android/app/ve/PreviewPlayerInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/ve/PreviewPlayerInterface;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 736
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$8;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    invoke-virtual {v0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->pause_player()V

    .line 737
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$8;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$3(Lcom/sec/android/app/storycam/view/PreviewViewGroup;Z)V

    .line 739
    :cond_0
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 5
    .param p1, "arg0"    # Landroid/widget/SeekBar;

    .prologue
    const/4 v4, 0x2

    const/4 v2, 0x0

    .line 743
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$8;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    # getter for: Lcom/sec/android/app/storycam/view/PreviewViewGroup;->wasPlayingBeforeProgressTouch:Z
    invoke-static {v0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$8(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    invoke-virtual {p1}, Landroid/widget/SeekBar;->getMax()I

    move-result v1

    if-eq v0, v1, :cond_1

    .line 744
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$8;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    # getter for: Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$5(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 745
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$8;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    # getter for: Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$5(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)Landroid/os/Handler;

    move-result-object v0

    const-wide/16 v2, 0x32

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 751
    :cond_0
    :goto_0
    return-void

    .line 746
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$8;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    # getter for: Lcom/sec/android/app/storycam/view/PreviewViewGroup;->wasPlayingBeforeProgressTouch:Z
    invoke-static {v0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$8(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 748
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$8;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    invoke-static {v0, v2}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$3(Lcom/sec/android/app/storycam/view/PreviewViewGroup;Z)V

    .line 749
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$8;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->setPlayButtonVisibility(I)V

    goto :goto_0
.end method
