.class Lcom/sec/android/app/ve/pm/ProjectManager$3;
.super Ljava/lang/Thread;
.source "ProjectManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/ve/pm/ProjectManager;->undoTheDeletedproject()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/ve/pm/ProjectManager;


# direct methods
.method constructor <init>(Lcom/sec/android/app/ve/pm/ProjectManager;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/ve/pm/ProjectManager$3;->this$0:Lcom/sec/android/app/ve/pm/ProjectManager;

    .line 451
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 454
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/ve/pm/ProjectManager$3;->this$0:Lcom/sec/android/app/ve/pm/ProjectManager;

    # getter for: Lcom/sec/android/app/ve/pm/ProjectManager;->mDeleteProjectList:Ljava/util/List;
    invoke-static {v1}, Lcom/sec/android/app/ve/pm/ProjectManager;->access$0(Lcom/sec/android/app/ve/pm/ProjectManager;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 457
    iget-object v1, p0, Lcom/sec/android/app/ve/pm/ProjectManager$3;->this$0:Lcom/sec/android/app/ve/pm/ProjectManager;

    const/4 v2, 0x0

    # invokes: Lcom/sec/android/app/ve/pm/ProjectManager;->clearDeletedProjectList(Z)V
    invoke-static {v1, v2}, Lcom/sec/android/app/ve/pm/ProjectManager;->access$2(Lcom/sec/android/app/ve/pm/ProjectManager;Z)V

    .line 458
    return-void

    .line 455
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/ve/pm/ProjectManager$3;->this$0:Lcom/sec/android/app/ve/pm/ProjectManager;

    iget-object v1, p0, Lcom/sec/android/app/ve/pm/ProjectManager$3;->this$0:Lcom/sec/android/app/ve/pm/ProjectManager;

    # getter for: Lcom/sec/android/app/ve/pm/ProjectManager;->mDeleteProjectList:Ljava/util/List;
    invoke-static {v1}, Lcom/sec/android/app/ve/pm/ProjectManager;->access$0(Lcom/sec/android/app/ve/pm/ProjectManager;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/app/video/editor/external/TranscodeElement;

    # invokes: Lcom/sec/android/app/ve/pm/ProjectManager;->saveDeletedProject(Lcom/samsung/app/video/editor/external/TranscodeElement;)V
    invoke-static {v2, v1}, Lcom/sec/android/app/ve/pm/ProjectManager;->access$1(Lcom/sec/android/app/ve/pm/ProjectManager;Lcom/samsung/app/video/editor/external/TranscodeElement;)V

    .line 454
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
