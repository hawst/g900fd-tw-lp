.class public Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcher;
.super Ljava/lang/Object;
.source "ThumbnailFetcher.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcher$Adapter;
    }
.end annotation


# static fields
.field public static IMAGE_THUMBNAIL_HEIGHT:I = 0x0

.field public static IMAGE_THUMBNAIL_WIDTH:I = 0x0

.field public static final VIDEO_THUMBNAIL_HEIGHT:I = 0x180

.field public static final VIDEO_THUMBNAIL_WIDTH:I = 0x200

.field private static mCache:Lcom/sec/android/app/ve/thumbcache/DiskLruCache;


# instance fields
.field private mTaskPool:Lcom/sec/android/app/ve/thread/AsyncTaskPool;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const/16 v0, 0x200

    sput v0, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcher;->IMAGE_THUMBNAIL_WIDTH:I

    .line 16
    const/16 v0, 0x180

    sput v0, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcher;->IMAGE_THUMBNAIL_HEIGHT:I

    .line 18
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    invoke-static {}, Lcom/sec/android/app/ve/thumbcache/DiskLruCache;->getInstance()Lcom/sec/android/app/ve/thumbcache/DiskLruCache;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcher;->mCache:Lcom/sec/android/app/ve/thumbcache/DiskLruCache;

    .line 23
    new-instance v0, Lcom/sec/android/app/ve/thread/AsyncTaskPool;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, Lcom/sec/android/app/ve/thread/AsyncTaskPool;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcher;->mTaskPool:Lcom/sec/android/app/ve/thread/AsyncTaskPool;

    .line 24
    return-void
.end method


# virtual methods
.method public clearLocalCache()V
    .locals 1

    .prologue
    .line 71
    sget-object v0, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcher;->mCache:Lcom/sec/android/app/ve/thumbcache/DiskLruCache;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/thumbcache/DiskLruCache;->clearLocalCache()V

    .line 72
    iget-object v0, p0, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcher;->mTaskPool:Lcom/sec/android/app/ve/thread/AsyncTaskPool;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/thread/AsyncTaskPool;->clearOrCancelTasks()V

    .line 73
    return-void
.end method

.method public clearOrCancelTasks()V
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcher;->mTaskPool:Lcom/sec/android/app/ve/thread/AsyncTaskPool;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/thread/AsyncTaskPool;->clearOrCancelTasks()V

    .line 91
    return-void
.end method

.method public getCacheSize()I
    .locals 1

    .prologue
    .line 94
    sget-object v0, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcher;->mCache:Lcom/sec/android/app/ve/thumbcache/DiskLruCache;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/thumbcache/DiskLruCache;->size()I

    move-result v0

    return v0
.end method

.method public declared-synchronized getImageThumbnail(Ljava/lang/String;ILcom/sec/android/app/ve/thumbcache/ThumbnailFetcher$Adapter;Ljava/lang/Object;)Landroid/graphics/Bitmap;
    .locals 12
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "thumbPosition"    # I
    .param p3, "callBack"    # Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcher$Adapter;
    .param p4, "tag"    # Ljava/lang/Object;

    .prologue
    .line 27
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcher;->mCache:Lcom/sec/android/app/ve/thumbcache/DiskLruCache;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/ve/thumbcache/DiskLruCache;->getBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v10

    .line 28
    .local v10, "thumbnail":Landroid/graphics/Bitmap;
    if-nez v10, :cond_0

    .line 29
    iget-object v11, p0, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcher;->mTaskPool:Lcom/sec/android/app/ve/thread/AsyncTaskPool;

    new-instance v0, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcherTask;

    iget-object v3, p0, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcher;->mTaskPool:Lcom/sec/android/app/ve/thread/AsyncTaskPool;

    sget-object v4, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcher;->mCache:Lcom/sec/android/app/ve/thumbcache/DiskLruCache;

    sget v6, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcher;->IMAGE_THUMBNAIL_WIDTH:I

    sget v7, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcher;->IMAGE_THUMBNAIL_HEIGHT:I

    const/4 v9, 0x2

    move-object v1, p1

    move v2, p2

    move-object v5, p3

    move-object/from16 v8, p4

    invoke-direct/range {v0 .. v9}, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcherTask;-><init>(Ljava/lang/String;ILcom/sec/android/app/ve/thread/AsyncTaskPool;Lcom/sec/android/app/ve/thumbcache/DiskLruCache;Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcher$Adapter;IILjava/lang/Object;I)V

    invoke-virtual {v11, v0}, Lcom/sec/android/app/ve/thread/AsyncTaskPool;->addUniqueTaskToTop(Lcom/sec/android/app/ve/thread/SimpleTask;)Lcom/sec/android/app/ve/thread/SimpleTask;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 32
    :cond_0
    monitor-exit p0

    return-object v10

    .line 27
    .end local v10    # "thumbnail":Landroid/graphics/Bitmap;
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getImageThumbnail2(Ljava/lang/String;ILcom/sec/android/app/ve/thumbcache/ThumbnailFetcher$Adapter;Ljava/lang/Object;)Landroid/graphics/Bitmap;
    .locals 12
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "thumbPosition"    # I
    .param p3, "callBack"    # Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcher$Adapter;
    .param p4, "tag"    # Ljava/lang/Object;

    .prologue
    .line 39
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcher;->mCache:Lcom/sec/android/app/ve/thumbcache/DiskLruCache;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/ve/thumbcache/DiskLruCache;->getBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v10

    .line 40
    .local v10, "thumbnail":Landroid/graphics/Bitmap;
    if-nez v10, :cond_0

    .line 41
    iget-object v11, p0, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcher;->mTaskPool:Lcom/sec/android/app/ve/thread/AsyncTaskPool;

    new-instance v0, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcherTask2;

    iget-object v3, p0, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcher;->mTaskPool:Lcom/sec/android/app/ve/thread/AsyncTaskPool;

    sget-object v4, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcher;->mCache:Lcom/sec/android/app/ve/thumbcache/DiskLruCache;

    sget v6, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcher;->IMAGE_THUMBNAIL_WIDTH:I

    sget v7, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcher;->IMAGE_THUMBNAIL_HEIGHT:I

    const/4 v9, 0x2

    move-object v1, p1

    move v2, p2

    move-object v5, p3

    move-object/from16 v8, p4

    invoke-direct/range {v0 .. v9}, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcherTask2;-><init>(Ljava/lang/String;ILcom/sec/android/app/ve/thread/AsyncTaskPool;Lcom/sec/android/app/ve/thumbcache/DiskLruCache;Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcher$Adapter;IILjava/lang/Object;I)V

    invoke-virtual {v11, v0}, Lcom/sec/android/app/ve/thread/AsyncTaskPool;->addUniqueTaskToTop(Lcom/sec/android/app/ve/thread/SimpleTask;)Lcom/sec/android/app/ve/thread/SimpleTask;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 44
    :cond_0
    monitor-exit p0

    return-object v10

    .line 39
    .end local v10    # "thumbnail":Landroid/graphics/Bitmap;
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getImageThumbnailForTimeline(Ljava/lang/String;Ljava/lang/String;Z)Landroid/graphics/Bitmap;
    .locals 2
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "time"    # Ljava/lang/String;
    .param p3, "readFromPersistentCache"    # Z

    .prologue
    .line 48
    monitor-enter p0

    :try_start_0
    sget-object v1, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcher;->mCache:Lcom/sec/android/app/ve/thumbcache/DiskLruCache;

    invoke-virtual {v1, p1, p2, p3}, Lcom/sec/android/app/ve/thumbcache/DiskLruCache;->getTimelineBitmap(Ljava/lang/String;Ljava/lang/String;Z)Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 49
    .local v0, "thumbnail":Landroid/graphics/Bitmap;
    monitor-exit p0

    return-object v0

    .line 48
    .end local v0    # "thumbnail":Landroid/graphics/Bitmap;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized getPhotoAlbumThumbnail(Ljava/util/ArrayList;ILcom/sec/android/app/ve/thumbcache/ThumbnailFetcher$Adapter;)Landroid/graphics/Bitmap;
    .locals 8
    .param p2, "thumbPosition"    # I
    .param p3, "callBack"    # Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcher$Adapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;I",
            "Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcher$Adapter;",
            ")",
            "Landroid/graphics/Bitmap;"
        }
    .end annotation

    .prologue
    .line 76
    .local p1, "filePaths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    monitor-enter p0

    :try_start_0
    iget-object v7, p0, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcher;->mTaskPool:Lcom/sec/android/app/ve/thread/AsyncTaskPool;

    new-instance v0, Lcom/sec/android/app/ve/thumbcache/FolderThumbnailFetcherTask;

    iget-object v3, p0, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcher;->mTaskPool:Lcom/sec/android/app/ve/thread/AsyncTaskPool;

    sget-object v4, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcher;->mCache:Lcom/sec/android/app/ve/thumbcache/DiskLruCache;

    const/4 v6, 0x2

    move-object v1, p1

    move v2, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/ve/thumbcache/FolderThumbnailFetcherTask;-><init>(Ljava/util/ArrayList;ILcom/sec/android/app/ve/thread/AsyncTaskPool;Lcom/sec/android/app/ve/thumbcache/DiskLruCache;Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcher$Adapter;I)V

    invoke-virtual {v7, v0}, Lcom/sec/android/app/ve/thread/AsyncTaskPool;->addUniqueTaskToTop(Lcom/sec/android/app/ve/thread/SimpleTask;)Lcom/sec/android/app/ve/thread/SimpleTask;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 77
    const/4 v0, 0x0

    monitor-exit p0

    return-object v0

    .line 76
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getVibrantColorOfMediaThumbnail(Ljava/lang/String;)I
    .locals 1
    .param p1, "filepath"    # Ljava/lang/String;

    .prologue
    .line 111
    sget-object v0, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcher;->mCache:Lcom/sec/android/app/ve/thumbcache/DiskLruCache;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/ve/thumbcache/DiskLruCache;->getVibrantColorOfMediaThumbnail(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public declared-synchronized getVideoThumbnail(Ljava/lang/String;ILcom/sec/android/app/ve/thumbcache/ThumbnailFetcher$Adapter;Ljava/lang/Object;)Landroid/graphics/Bitmap;
    .locals 12
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "thumbPosition"    # I
    .param p3, "callBack"    # Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcher$Adapter;
    .param p4, "tag"    # Ljava/lang/Object;

    .prologue
    .line 53
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcher;->mCache:Lcom/sec/android/app/ve/thumbcache/DiskLruCache;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/ve/thumbcache/DiskLruCache;->getBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v10

    .line 54
    .local v10, "thumbnail":Landroid/graphics/Bitmap;
    if-nez v10, :cond_0

    .line 55
    iget-object v11, p0, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcher;->mTaskPool:Lcom/sec/android/app/ve/thread/AsyncTaskPool;

    new-instance v0, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcherTask;

    iget-object v3, p0, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcher;->mTaskPool:Lcom/sec/android/app/ve/thread/AsyncTaskPool;

    sget-object v4, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcher;->mCache:Lcom/sec/android/app/ve/thumbcache/DiskLruCache;

    const/16 v6, 0x200

    const/16 v7, 0x180

    const/4 v9, 0x1

    move-object v1, p1

    move v2, p2

    move-object v5, p3

    move-object/from16 v8, p4

    invoke-direct/range {v0 .. v9}, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcherTask;-><init>(Ljava/lang/String;ILcom/sec/android/app/ve/thread/AsyncTaskPool;Lcom/sec/android/app/ve/thumbcache/DiskLruCache;Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcher$Adapter;IILjava/lang/Object;I)V

    invoke-virtual {v11, v0}, Lcom/sec/android/app/ve/thread/AsyncTaskPool;->addUniqueTaskToTop(Lcom/sec/android/app/ve/thread/SimpleTask;)Lcom/sec/android/app/ve/thread/SimpleTask;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 58
    :cond_0
    monitor-exit p0

    return-object v10

    .line 53
    .end local v10    # "thumbnail":Landroid/graphics/Bitmap;
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getVideoThumbnailForTimeline(Ljava/lang/String;Ljava/lang/String;Z)Landroid/graphics/Bitmap;
    .locals 2
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "time"    # Ljava/lang/String;
    .param p3, "readFromPersistentCache"    # Z

    .prologue
    .line 66
    monitor-enter p0

    :try_start_0
    sget-object v1, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcher;->mCache:Lcom/sec/android/app/ve/thumbcache/DiskLruCache;

    invoke-virtual {v1, p1, p2, p3}, Lcom/sec/android/app/ve/thumbcache/DiskLruCache;->getTimelineBitmap(Ljava/lang/String;Ljava/lang/String;Z)Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 67
    .local v0, "thumbnail":Landroid/graphics/Bitmap;
    monitor-exit p0

    return-object v0

    .line 66
    .end local v0    # "thumbnail":Landroid/graphics/Bitmap;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public loadCacheFromDisk()V
    .locals 1

    .prologue
    .line 98
    invoke-static {}, Lcom/sec/android/app/ve/thumbcache/DiskLruCache;->loadFromDisk()Z

    .line 99
    invoke-static {}, Lcom/sec/android/app/ve/thumbcache/DiskLruCache;->getInstance()Lcom/sec/android/app/ve/thumbcache/DiskLruCache;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcher;->mCache:Lcom/sec/android/app/ve/thumbcache/DiskLruCache;

    .line 100
    return-void
.end method

.method public putTimelineBitmapToCache(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "time"    # Ljava/lang/String;
    .param p3, "bmp"    # Landroid/graphics/Bitmap;

    .prologue
    .line 62
    sget-object v0, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcher;->mCache:Lcom/sec/android/app/ve/thumbcache/DiskLruCache;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/android/app/ve/thumbcache/DiskLruCache;->putTimelineBitmap(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 63
    return-void
.end method

.method public recheckAfterMediaScanned()V
    .locals 12

    .prologue
    const/4 v3, 0x0

    .line 103
    sget-object v2, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcher;->mCache:Lcom/sec/android/app/ve/thumbcache/DiskLruCache;

    invoke-virtual {v2}, Lcom/sec/android/app/ve/thumbcache/DiskLruCache;->recheckAfterMediaScanned()Ljava/util/ArrayList;

    move-result-object v10

    .line 104
    .local v10, "notFoundFiles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 108
    return-void

    .line 104
    :cond_0
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 105
    .local v1, "filePath":Ljava/lang/String;
    new-instance v0, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcherTask;

    const/4 v2, -0x1

    sget v6, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcher;->IMAGE_THUMBNAIL_WIDTH:I

    sget v7, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcher;->IMAGE_THUMBNAIL_HEIGHT:I

    const/4 v9, 0x2

    move-object v4, v3

    move-object v5, v3

    move-object v8, v3

    invoke-direct/range {v0 .. v9}, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcherTask;-><init>(Ljava/lang/String;ILcom/sec/android/app/ve/thread/AsyncTaskPool;Lcom/sec/android/app/ve/thumbcache/DiskLruCache;Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcher$Adapter;IILjava/lang/Object;I)V

    .line 106
    .local v0, "dummyTask":Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcherTask;
    iget-object v2, p0, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcher;->mTaskPool:Lcom/sec/android/app/ve/thread/AsyncTaskPool;

    invoke-virtual {v2, v0}, Lcom/sec/android/app/ve/thread/AsyncTaskPool;->removeTaskIfPending(Lcom/sec/android/app/ve/thread/SimpleTask;)Z

    goto :goto_0
.end method

.method public writeCacheToDisk()V
    .locals 1

    .prologue
    .line 85
    sget-object v0, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcher;->mCache:Lcom/sec/android/app/ve/thumbcache/DiskLruCache;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/thumbcache/DiskLruCache;->saveToDisk()V

    .line 86
    return-void
.end method
