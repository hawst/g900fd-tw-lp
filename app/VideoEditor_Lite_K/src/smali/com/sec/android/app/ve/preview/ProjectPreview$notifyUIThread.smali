.class Lcom/sec/android/app/ve/preview/ProjectPreview$notifyUIThread;
.super Ljava/util/TimerTask;
.source "ProjectPreview.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/ve/preview/ProjectPreview;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "notifyUIThread"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/ve/preview/ProjectPreview;


# direct methods
.method constructor <init>(Lcom/sec/android/app/ve/preview/ProjectPreview;)V
    .locals 0

    .prologue
    .line 732
    iput-object p1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview$notifyUIThread;->this$0:Lcom/sec/android/app/ve/preview/ProjectPreview;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 734
    iget-object v1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview$notifyUIThread;->this$0:Lcom/sec/android/app/ve/preview/ProjectPreview;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/sec/android/app/ve/preview/ProjectPreview;->access$17(Lcom/sec/android/app/ve/preview/ProjectPreview;Z)V

    .line 735
    const-string v1, "PLAYER_STATE"

    const-string v2, "Timer expired and run executed"

    invoke-static {v1, v2}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 737
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview$notifyUIThread;->this$0:Lcom/sec/android/app/ve/preview/ProjectPreview;

    # getter for: Lcom/sec/android/app/ve/preview/ProjectPreview;->syncToken:Ljava/lang/Object;
    invoke-static {v1}, Lcom/sec/android/app/ve/preview/ProjectPreview;->access$12(Lcom/sec/android/app/ve/preview/ProjectPreview;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 738
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview$notifyUIThread;->this$0:Lcom/sec/android/app/ve/preview/ProjectPreview;

    # getter for: Lcom/sec/android/app/ve/preview/ProjectPreview;->syncToken:Ljava/lang/Object;
    invoke-static {v1}, Lcom/sec/android/app/ve/preview/ProjectPreview;->access$12(Lcom/sec/android/app/ve/preview/ProjectPreview;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->notify()V

    .line 737
    monitor-exit v2

    .line 743
    :goto_0
    return-void

    .line 737
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 740
    :catch_0
    move-exception v0

    .line 741
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
