.class public Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$VideoPreviewController;
.super Ljava/lang/Object;
.source "MediaPreviewHoverWindow.java"

# interfaces
.implements Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$Controller;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "VideoPreviewController"
.end annotation


# instance fields
.field private mDialog:Landroid/app/Dialog;

.field private mVideoPreview:Lcom/sec/android/app/ve/preview/VideoFilePreview;

.field private final mVideoPreviewAdapter:Lcom/sec/android/app/ve/PreviewPlayerInterface$Adapter;


# direct methods
.method public constructor <init>(Landroid/app/Dialog;)V
    .locals 1
    .param p1, "dialog"    # Landroid/app/Dialog;

    .prologue
    .line 269
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 308
    new-instance v0, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$VideoPreviewController$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$VideoPreviewController$1;-><init>(Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$VideoPreviewController;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$VideoPreviewController;->mVideoPreviewAdapter:Lcom/sec/android/app/ve/PreviewPlayerInterface$Adapter;

    .line 270
    iput-object p1, p0, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$VideoPreviewController;->mDialog:Landroid/app/Dialog;

    .line 271
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$VideoPreviewController;)Landroid/app/Dialog;
    .locals 1

    .prologue
    .line 266
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$VideoPreviewController;->mDialog:Landroid/app/Dialog;

    return-object v0
.end method


# virtual methods
.method public doLayout(Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;Landroid/view/View$OnHoverListener;)Landroid/view/View;
    .locals 11
    .param p1, "params"    # Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;
    .param p2, "hoverListener"    # Landroid/view/View$OnHoverListener;

    .prologue
    const/4 v10, 0x0

    .line 275
    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getInflater()Landroid/view/LayoutInflater;

    move-result-object v7

    const v8, 0x7f030016

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    .line 276
    .local v1, "layout":Landroid/widget/RelativeLayout;
    const v7, 0x7f0d0056

    invoke-virtual {v1, v7}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 277
    .local v0, "contentView":Landroid/widget/RelativeLayout;
    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 278
    .local v2, "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    # getter for: Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;->mLeft:I
    invoke-static {p1}, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;->access$0(Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;)I

    move-result v7

    iput v7, v2, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 279
    # getter for: Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;->mTop:I
    invoke-static {p1}, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;->access$1(Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;)I

    move-result v7

    iput v7, v2, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 280
    # getter for: Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;->mWidth:I
    invoke-static {p1}, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;->access$2(Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;)I

    move-result v7

    iput v7, v2, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 281
    # getter for: Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;->mHeight:I
    invoke-static {p1}, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;->access$3(Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;)I

    move-result v7

    iput v7, v2, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 282
    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 284
    const v7, 0x7f0d0057

    invoke-virtual {v1, v7}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/sec/android/app/ve/preview/VideoFilePreview;

    iput-object v7, p0, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$VideoPreviewController;->mVideoPreview:Lcom/sec/android/app/ve/preview/VideoFilePreview;

    .line 285
    # getter for: Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;->mWidth:I
    invoke-static {p1}, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;->access$2(Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;)I

    move-result v7

    const v8, 0x7f0601d0

    invoke-static {v8}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v8

    add-int v6, v7, v8

    .line 286
    .local v6, "videoRight":I
    # getter for: Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;->mHeight:I
    invoke-static {p1}, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;->access$3(Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;)I

    move-result v7

    const v8, 0x7f0601cf

    invoke-static {v8}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v8

    add-int v5, v7, v8

    .line 287
    .local v5, "videoBottom":I
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3, v10, v10, v6, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 288
    .local v3, "surfaceRect":Landroid/graphics/Rect;
    iget-object v7, p0, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$VideoPreviewController;->mVideoPreview:Lcom/sec/android/app/ve/preview/VideoFilePreview;

    iget-object v8, p0, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$VideoPreviewController;->mVideoPreviewAdapter:Lcom/sec/android/app/ve/PreviewPlayerInterface$Adapter;

    invoke-virtual {v7, v8}, Lcom/sec/android/app/ve/preview/VideoFilePreview;->setAdapter(Lcom/sec/android/app/ve/PreviewPlayerInterface$Adapter;)V

    .line 289
    iget-object v7, p0, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$VideoPreviewController;->mVideoPreview:Lcom/sec/android/app/ve/preview/VideoFilePreview;

    # getter for: Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;->mFilePath:Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;->access$6(Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/sec/android/app/ve/preview/VideoFilePreview;->setDataSource(Ljava/lang/String;)V

    .line 290
    iget-object v7, p0, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$VideoPreviewController;->mVideoPreview:Lcom/sec/android/app/ve/preview/VideoFilePreview;

    invoke-virtual {v7, v3}, Lcom/sec/android/app/ve/preview/VideoFilePreview;->setFixedSurfaceRect(Landroid/graphics/Rect;)V

    .line 291
    iget-object v7, p0, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$VideoPreviewController;->mVideoPreview:Lcom/sec/android/app/ve/preview/VideoFilePreview;

    const-wide/16 v8, 0x0

    invoke-virtual {v7, v8, v9}, Lcom/sec/android/app/ve/preview/VideoFilePreview;->play(J)V

    .line 293
    const v7, 0x7f0d0058

    invoke-virtual {v1, v7}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    .line 294
    .local v4, "unsupImageView":Landroid/widget/ImageView;
    # getter for: Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;->mIsSupported:Z
    invoke-static {p1}, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;->access$5(Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 295
    invoke-virtual {v4, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 296
    const v7, 0x7f02005f

    invoke-virtual {v4, v7}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 299
    :goto_0
    return-object v1

    .line 298
    :cond_0
    const/16 v7, 0x8

    invoke-virtual {v4, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public refreshThumbnails(Ljava/util/LinkedHashMap;I)V
    .locals 0
    .param p2, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/graphics/Bitmap;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 330
    .local p1, "thumbList":Ljava/util/LinkedHashMap;, "Ljava/util/LinkedHashMap<Ljava/lang/Integer;Landroid/graphics/Bitmap;>;"
    return-void
.end method

.method public release()V
    .locals 1

    .prologue
    .line 304
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$VideoPreviewController;->mVideoPreview:Lcom/sec/android/app/ve/preview/VideoFilePreview;

    if-eqz v0, :cond_0

    .line 305
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$VideoPreviewController;->mVideoPreview:Lcom/sec/android/app/ve/preview/VideoFilePreview;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/preview/VideoFilePreview;->stop()V

    .line 306
    :cond_0
    return-void
.end method
