.class Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$10;
.super Ljava/lang/Object;
.source "VideoEditorLiteActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->onResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$10;->this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;

    .line 552
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 555
    iget-object v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$10;->this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;

    # getter for: Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->isOkToResumeExport:Z
    invoke-static {v0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->access$16(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/app/ve/VEApp;->gExport:Lcom/sec/android/app/ve/export/Export;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/export/Export;->isDefaultPause()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 556
    iget-object v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$10;->this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->access$17(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;Z)V

    .line 557
    sget-object v0, Lcom/sec/android/app/ve/VEApp;->gExport:Lcom/sec/android/app/ve/export/Export;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/export/Export;->resumeExport()V

    .line 559
    :cond_0
    return-void
.end method
