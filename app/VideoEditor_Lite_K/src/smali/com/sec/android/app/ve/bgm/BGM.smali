.class public abstract Lcom/sec/android/app/ve/bgm/BGM;
.super Ljava/lang/Object;
.source "BGM.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/ve/bgm/BGM$BGMModes;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method protected abstract getBGMFilepath()Ljava/lang/String;
.end method

.method protected abstract getBGMName()Ljava/lang/String;
.end method

.method protected abstract getTotalDurationBGM()I
.end method

.method protected abstract getTransitionTimeStamps(Lcom/sec/android/app/ve/bgm/BGM$BGMModes;)[I
.end method
