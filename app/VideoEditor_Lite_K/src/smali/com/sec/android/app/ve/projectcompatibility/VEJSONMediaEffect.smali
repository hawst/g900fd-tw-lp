.class public Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaEffect;
.super Lorg/json/JSONObject;
.source "VEJSONMediaEffect.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lorg/json/JSONObject;-><init>()V

    .line 22
    return-void
.end method

.method protected constructor <init>(Lorg/json/JSONObject;)V
    .locals 1
    .param p1, "source"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 25
    invoke-virtual {p1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 26
    return-void
.end method


# virtual methods
.method public getEffectEndTime()I
    .locals 3

    .prologue
    .line 102
    const/4 v0, 0x0

    .line 104
    .local v0, "endTime":I
    :try_start_0
    const-string v2, "EFFECT_END_TIME"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaEffect;->getInt(Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 109
    :goto_0
    return v0

    .line 106
    :catch_0
    move-exception v1

    .line 107
    .local v1, "jException":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getEffectResourcePath()Ljava/lang/String;
    .locals 3

    .prologue
    .line 410
    const/4 v1, 0x0

    .line 412
    .local v1, "resPath":Ljava/lang/String;
    :try_start_0
    const-string v2, "EFFECT_RESOURCE_PATH"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaEffect;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 416
    :goto_0
    return-object v1

    .line 413
    :catch_0
    move-exception v0

    .line 414
    .local v0, "jException":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getEffectStartAlpha()D
    .locals 4

    .prologue
    .line 367
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    .line 369
    .local v2, "startAlpha":D
    :try_start_0
    const-string v1, "ALPHA_START"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaEffect;->getDouble(Ljava/lang/String;)D
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 374
    :goto_0
    return-wide v2

    .line 371
    :catch_0
    move-exception v0

    .line 372
    .local v0, "jException":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getEffectStartTime()I
    .locals 3

    .prologue
    .line 74
    const/4 v1, 0x0

    .line 76
    .local v1, "startTime":I
    :try_start_0
    const-string v2, "EFFECT_START_TIME"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaEffect;->getInt(Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 81
    :goto_0
    return v1

    .line 78
    :catch_0
    move-exception v0

    .line 79
    .local v0, "jException":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getEffectType()I
    .locals 3

    .prologue
    .line 46
    const/4 v0, 0x0

    .line 48
    .local v0, "effectType":I
    :try_start_0
    const-string v2, "EFFECT_TYPE"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaEffect;->getInt(Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 53
    :goto_0
    return v0

    .line 50
    :catch_0
    move-exception v1

    .line 51
    .local v1, "jException":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getEndAlpha()D
    .locals 4

    .prologue
    .line 382
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    .line 384
    .local v0, "endAlpha":D
    :try_start_0
    const-string v3, "ALPHA_END"

    invoke-virtual {p0, v3}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaEffect;->getDouble(Ljava/lang/String;)D
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 389
    :goto_0
    return-wide v0

    .line 386
    :catch_0
    move-exception v2

    .line 387
    .local v2, "jException":Lorg/json/JSONException;
    invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getScaleEndX()D
    .locals 4

    .prologue
    .line 204
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    .line 206
    .local v2, "scaleEndX":D
    :try_start_0
    const-string v1, "SCALE_END_X"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaEffect;->getDouble(Ljava/lang/String;)D
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 211
    :goto_0
    return-wide v2

    .line 208
    :catch_0
    move-exception v0

    .line 209
    .local v0, "jException":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getScaleEndY()D
    .locals 4

    .prologue
    .line 219
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    .line 221
    .local v2, "scaleEndY":D
    :try_start_0
    const-string v1, "SCALE_END_Y"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaEffect;->getDouble(Ljava/lang/String;)D
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 226
    :goto_0
    return-wide v2

    .line 223
    :catch_0
    move-exception v0

    .line 224
    .local v0, "jException":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getScaleStartX()D
    .locals 4

    .prologue
    .line 145
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    .line 147
    .local v2, "scaleStartX":D
    :try_start_0
    const-string v1, "SCALE_START_X"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaEffect;->getDouble(Ljava/lang/String;)D
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 153
    :goto_0
    return-wide v2

    .line 149
    :catch_0
    move-exception v0

    .line 150
    .local v0, "jException":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getScaleStartY()D
    .locals 4

    .prologue
    .line 161
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    .line 163
    .local v2, "scaleStartY":D
    :try_start_0
    const-string v1, "SCALE_START_Y"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaEffect;->getDouble(Ljava/lang/String;)D
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 168
    :goto_0
    return-wide v2

    .line 165
    :catch_0
    move-exception v0

    .line 166
    .local v0, "jException":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getTranslateEndX()D
    .locals 4

    .prologue
    .line 321
    const-wide/16 v2, 0x0

    .line 323
    .local v2, "translateEndX":D
    :try_start_0
    const-string v1, "TRANSLATION_TO_X"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaEffect;->getDouble(Ljava/lang/String;)D
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 328
    :goto_0
    return-wide v2

    .line 325
    :catch_0
    move-exception v0

    .line 326
    .local v0, "jException":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getTranslateEndY()D
    .locals 4

    .prologue
    .line 336
    const-wide/16 v2, 0x0

    .line 338
    .local v2, "translateEndY":D
    :try_start_0
    const-string v1, "TRANSLATION_TO_Y"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaEffect;->getDouble(Ljava/lang/String;)D
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 343
    :goto_0
    return-wide v2

    .line 340
    :catch_0
    move-exception v0

    .line 341
    .local v0, "jException":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getTranslateStartX()D
    .locals 4

    .prologue
    .line 262
    const-wide/16 v2, 0x0

    .line 264
    .local v2, "translateStartX":D
    :try_start_0
    const-string v1, "TRANSLATION_FROM_X"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaEffect;->getDouble(Ljava/lang/String;)D
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 269
    :goto_0
    return-wide v2

    .line 266
    :catch_0
    move-exception v0

    .line 267
    .local v0, "jException":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getTranslateStartY()D
    .locals 4

    .prologue
    .line 277
    const-wide/16 v2, 0x0

    .line 279
    .local v2, "translateStartY":D
    :try_start_0
    const-string v1, "TRANSLATION_FROM_Y"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaEffect;->getDouble(Ljava/lang/String;)D
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 284
    :goto_0
    return-wide v2

    .line 281
    :catch_0
    move-exception v0

    .line 282
    .local v0, "jException":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public setEffectAlpha(DD)V
    .locals 3
    .param p1, "startAlpha"    # D
    .param p3, "endAlpha"    # D

    .prologue
    .line 354
    :try_start_0
    const-string v1, "ALPHA_START"

    invoke-virtual {p0, v1, p1, p2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaEffect;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 355
    const-string v1, "ALPHA_END"

    invoke-virtual {p0, v1, p3, p4}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaEffect;->put(Ljava/lang/String;D)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 360
    :goto_0
    return-void

    .line 357
    :catch_0
    move-exception v0

    .line 358
    .local v0, "jException":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public setEffectEndTime(I)V
    .locals 2
    .param p1, "endTime"    # I

    .prologue
    .line 90
    :try_start_0
    const-string v1, "EFFECT_END_TIME"

    invoke-virtual {p0, v1, p1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaEffect;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 95
    :goto_0
    return-void

    .line 92
    :catch_0
    move-exception v0

    .line 93
    .local v0, "jException":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public setEffectResourcePath(Ljava/lang/String;)V
    .locals 2
    .param p1, "resPath"    # Ljava/lang/String;

    .prologue
    .line 398
    :try_start_0
    const-string v1, "EFFECT_RESOURCE_PATH"

    invoke-virtual {p0, v1, p1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaEffect;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 403
    :goto_0
    return-void

    .line 400
    :catch_0
    move-exception v0

    .line 401
    .local v0, "jException":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public setEffectStartTime(I)V
    .locals 2
    .param p1, "startTime"    # I

    .prologue
    .line 62
    :try_start_0
    const-string v1, "EFFECT_START_TIME"

    invoke-virtual {p0, v1, p1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaEffect;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 67
    :goto_0
    return-void

    .line 64
    :catch_0
    move-exception v0

    .line 65
    .local v0, "jException":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public setEffectType(I)V
    .locals 2
    .param p1, "type"    # I

    .prologue
    .line 34
    :try_start_0
    const-string v1, "EFFECT_TYPE"

    invoke-virtual {p0, v1, p1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaEffect;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 39
    :goto_0
    return-void

    .line 36
    :catch_0
    move-exception v0

    .line 37
    .local v0, "jException":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public setScaleEndValues(DD)V
    .locals 5
    .param p1, "scaleEndX"    # D
    .param p3, "scaleEndY"    # D
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 178
    const/4 v1, 0x0

    .line 180
    .local v1, "type":I
    :try_start_0
    const-string v2, "EFFECT_TYPE"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaEffect;->getInt(Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 185
    :goto_0
    const/16 v2, 0x8

    if-ne v1, v2, :cond_0

    .line 187
    :try_start_1
    const-string v2, "SCALE_END_X"

    invoke-virtual {p0, v2, p1, p2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaEffect;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 188
    const-string v2, "SCALE_END_Y"

    invoke-virtual {p0, v2, p3, p4}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaEffect;->put(Ljava/lang/String;D)Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 197
    :goto_1
    return-void

    .line 182
    :catch_0
    move-exception v0

    .line 183
    .local v0, "jException":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0

    .line 190
    .end local v0    # "jException":Lorg/json/JSONException;
    :catch_1
    move-exception v0

    .line 191
    .restart local v0    # "jException":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_1

    .line 195
    .end local v0    # "jException":Lorg/json/JSONException;
    :cond_0
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "This method is supported for only Kenburns effect. Please set type to VEMediaEffects.VEMEDIAEFFECTS_KENBURNS before calling this method"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public setScaleStartValues(DD)V
    .locals 5
    .param p1, "scaleStartX"    # D
    .param p3, "scaleStartY"    # D
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 119
    const/4 v1, 0x0

    .line 121
    .local v1, "type":I
    :try_start_0
    const-string v2, "EFFECT_TYPE"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaEffect;->getInt(Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 126
    :goto_0
    const/16 v2, 0x8

    if-ne v1, v2, :cond_0

    .line 128
    :try_start_1
    const-string v2, "SCALE_START_X"

    invoke-virtual {p0, v2, p1, p2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaEffect;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 129
    const-string v2, "SCALE_START_Y"

    invoke-virtual {p0, v2, p3, p4}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaEffect;->put(Ljava/lang/String;D)Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 138
    :goto_1
    return-void

    .line 123
    :catch_0
    move-exception v0

    .line 124
    .local v0, "jException":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0

    .line 131
    .end local v0    # "jException":Lorg/json/JSONException;
    :catch_1
    move-exception v0

    .line 132
    .restart local v0    # "jException":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_1

    .line 136
    .end local v0    # "jException":Lorg/json/JSONException;
    :cond_0
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "This method is supported for only Kenburns effect. Please set type to VEMediaEffects.VEMEDIAEFFECTS_KENBURNS before calling this method"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public setTranslateEndValues(DD)V
    .locals 5
    .param p1, "translateEndX"    # D
    .param p3, "translateEndY"    # D
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 294
    const/4 v1, 0x0

    .line 296
    .local v1, "type":I
    :try_start_0
    const-string v2, "EFFECT_TYPE"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaEffect;->getInt(Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 301
    :goto_0
    const/16 v2, 0x8

    if-ne v1, v2, :cond_0

    .line 303
    :try_start_1
    const-string v2, "TRANSLATION_TO_X"

    invoke-virtual {p0, v2, p1, p2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaEffect;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 304
    const-string v2, "TRANSLATION_TO_Y"

    invoke-virtual {p0, v2, p3, p4}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaEffect;->put(Ljava/lang/String;D)Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 314
    :goto_1
    return-void

    .line 298
    :catch_0
    move-exception v0

    .line 299
    .local v0, "jException":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0

    .line 306
    .end local v0    # "jException":Lorg/json/JSONException;
    :catch_1
    move-exception v0

    .line 307
    .restart local v0    # "jException":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_1

    .line 311
    .end local v0    # "jException":Lorg/json/JSONException;
    :cond_0
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "This method is supported for only Kenburns effect. Please set type to VEMediaEffects.VEMEDIAEFFECTS_KENBURNS before calling this method"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public setTranslateStartValues(DD)V
    .locals 5
    .param p1, "translateStartX"    # D
    .param p3, "translateStartY"    # D
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 236
    const/4 v1, 0x0

    .line 238
    .local v1, "type":I
    :try_start_0
    const-string v2, "EFFECT_TYPE"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaEffect;->getInt(Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 243
    :goto_0
    const/16 v2, 0x8

    if-ne v1, v2, :cond_0

    .line 245
    :try_start_1
    const-string v2, "TRANSLATION_FROM_X"

    invoke-virtual {p0, v2, p1, p2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaEffect;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 246
    const-string v2, "TRANSLATION_FROM_Y"

    invoke-virtual {p0, v2, p3, p4}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaEffect;->put(Ljava/lang/String;D)Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 255
    :goto_1
    return-void

    .line 240
    :catch_0
    move-exception v0

    .line 241
    .local v0, "jException":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0

    .line 248
    .end local v0    # "jException":Lorg/json/JSONException;
    :catch_1
    move-exception v0

    .line 249
    .restart local v0    # "jException":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_1

    .line 253
    .end local v0    # "jException":Lorg/json/JSONException;
    :cond_0
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "This method is supported for only Kenburns effect. Please set type to VEMediaEffects.VEMEDIAEFFECTS_KENBURNS before calling this method"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2
.end method
