.class Lcom/sec/android/app/ve/preview/ProjectPreview$2;
.super Ljava/lang/Object;
.source "ProjectPreview.java"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/ve/preview/ProjectPreview;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/ve/preview/ProjectPreview;


# direct methods
.method constructor <init>(Lcom/sec/android/app/ve/preview/ProjectPreview;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview$2;->this$0:Lcom/sec/android/app/ve/preview/ProjectPreview;

    .line 507
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 3
    .param p1, "arg0"    # Landroid/view/SurfaceHolder;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I
    .param p4, "arg3"    # I

    .prologue
    .line 555
    const-string v0, "PLAYER_STATE"

    const-string v1, "PVG::surface surfaceChanged"

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 556
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview$2;->this$0:Lcom/sec/android/app/ve/preview/ProjectPreview;

    invoke-static {v0, p1}, Lcom/sec/android/app/ve/preview/ProjectPreview;->access$3(Lcom/sec/android/app/ve/preview/ProjectPreview;Landroid/view/SurfaceHolder;)V

    .line 557
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview$2;->this$0:Lcom/sec/android/app/ve/preview/ProjectPreview;

    # getter for: Lcom/sec/android/app/ve/preview/ProjectPreview;->mSurfaceHolder:Landroid/view/SurfaceHolder;
    invoke-static {v0}, Lcom/sec/android/app/ve/preview/ProjectPreview;->access$4(Lcom/sec/android/app/ve/preview/ProjectPreview;)Landroid/view/SurfaceHolder;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 558
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview$2;->this$0:Lcom/sec/android/app/ve/preview/ProjectPreview;

    # getter for: Lcom/sec/android/app/ve/preview/ProjectPreview;->mSurfaceHolder:Landroid/view/SurfaceHolder;
    invoke-static {v0}, Lcom/sec/android/app/ve/preview/ProjectPreview;->access$4(Lcom/sec/android/app/ve/preview/ProjectPreview;)Landroid/view/SurfaceHolder;

    move-result-object v0

    sget v1, Lcom/sec/android/app/ve/common/ConfigUtils;->FIXED_PREVIEW_WIDTH:I

    sget v2, Lcom/sec/android/app/ve/common/ConfigUtils;->FIXED_PREVIEW_HEIGHT:I

    invoke-interface {v0, v1, v2}, Landroid/view/SurfaceHolder;->setFixedSize(II)V

    .line 560
    sget-object v0, Lcom/sec/android/app/ve/VEApp;->gExport:Lcom/sec/android/app/ve/export/Export;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/export/Export;->isExportRunning()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview$2;->this$0:Lcom/sec/android/app/ve/preview/ProjectPreview;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/preview/ProjectPreview;->isPlaying()Z

    move-result v0

    if-nez v0, :cond_1

    .line 561
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview$2;->this$0:Lcom/sec/android/app/ve/preview/ProjectPreview;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/preview/ProjectPreview;->setEngineSurface()V

    .line 564
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/app/video/editor/external/NativeInterface;->makeSurfaceBlack()V

    .line 569
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview$2;->this$0:Lcom/sec/android/app/ve/preview/ProjectPreview;

    iget-object v0, v0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mAdapter:Lcom/sec/android/app/ve/PreviewPlayerInterface$Adapter;

    if-eqz v0, :cond_0

    .line 570
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview$2;->this$0:Lcom/sec/android/app/ve/preview/ProjectPreview;

    iget-object v0, v0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mAdapter:Lcom/sec/android/app/ve/PreviewPlayerInterface$Adapter;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/PreviewPlayerInterface$Adapter;->onSurfaceChanged()V

    .line 572
    :cond_0
    return-void

    .line 566
    :cond_1
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v0

    sget v1, Lcom/sec/android/app/ve/common/ConfigUtils;->FIXED_PREVIEW_WIDTH:I

    sget v2, Lcom/sec/android/app/ve/common/ConfigUtils;->FIXED_PREVIEW_HEIGHT:I

    invoke-virtual {v0, v1, v2}, Lcom/samsung/app/video/editor/external/NativeInterface;->setDisplayAspectRatio(II)V

    .line 567
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview$2;->this$0:Lcom/sec/android/app/ve/preview/ProjectPreview;

    # getter for: Lcom/sec/android/app/ve/preview/ProjectPreview;->mSurfaceHolder:Landroid/view/SurfaceHolder;
    invoke-static {v1}, Lcom/sec/android/app/ve/preview/ProjectPreview;->access$4(Lcom/sec/android/app/ve/preview/ProjectPreview;)Landroid/view/SurfaceHolder;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/app/video/editor/external/NativeInterface;->setPreviewDisplay(Landroid/view/SurfaceHolder;)V

    goto :goto_0
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 4
    .param p1, "arg0"    # Landroid/view/SurfaceHolder;

    .prologue
    const/4 v3, 0x1

    .line 526
    const-string v0, "PLAYER_STATE"

    const-string v1, "PVG::surface surfaceCreated"

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 527
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview$2;->this$0:Lcom/sec/android/app/ve/preview/ProjectPreview;

    invoke-static {v0, p1}, Lcom/sec/android/app/ve/preview/ProjectPreview;->access$3(Lcom/sec/android/app/ve/preview/ProjectPreview;Landroid/view/SurfaceHolder;)V

    .line 528
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview$2;->this$0:Lcom/sec/android/app/ve/preview/ProjectPreview;

    # getter for: Lcom/sec/android/app/ve/preview/ProjectPreview;->mSurfaceHolder:Landroid/view/SurfaceHolder;
    invoke-static {v0}, Lcom/sec/android/app/ve/preview/ProjectPreview;->access$4(Lcom/sec/android/app/ve/preview/ProjectPreview;)Landroid/view/SurfaceHolder;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 529
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview$2;->this$0:Lcom/sec/android/app/ve/preview/ProjectPreview;

    # getter for: Lcom/sec/android/app/ve/preview/ProjectPreview;->mSurfaceHolder:Landroid/view/SurfaceHolder;
    invoke-static {v0}, Lcom/sec/android/app/ve/preview/ProjectPreview;->access$4(Lcom/sec/android/app/ve/preview/ProjectPreview;)Landroid/view/SurfaceHolder;

    move-result-object v0

    sget v1, Lcom/sec/android/app/ve/common/ConfigUtils;->FIXED_PREVIEW_WIDTH:I

    sget v2, Lcom/sec/android/app/ve/common/ConfigUtils;->FIXED_PREVIEW_HEIGHT:I

    invoke-interface {v0, v1, v2}, Landroid/view/SurfaceHolder;->setFixedSize(II)V

    .line 531
    sget-object v0, Lcom/sec/android/app/ve/VEApp;->gExport:Lcom/sec/android/app/ve/export/Export;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/export/Export;->isExportRunning()Z

    move-result v0

    if-nez v0, :cond_3

    .line 532
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview$2;->this$0:Lcom/sec/android/app/ve/preview/ProjectPreview;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/preview/ProjectPreview;->setEngineSurface()V

    .line 538
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview$2;->this$0:Lcom/sec/android/app/ve/preview/ProjectPreview;

    # getter for: Lcom/sec/android/app/ve/preview/ProjectPreview;->mFrameThread:Lcom/sec/android/app/ve/preview/PreviewFrameThread;
    invoke-static {v0}, Lcom/sec/android/app/ve/preview/ProjectPreview;->access$1(Lcom/sec/android/app/ve/preview/ProjectPreview;)Lcom/sec/android/app/ve/preview/PreviewFrameThread;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 539
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview$2;->this$0:Lcom/sec/android/app/ve/preview/ProjectPreview;

    # getter for: Lcom/sec/android/app/ve/preview/ProjectPreview;->mFrameThread:Lcom/sec/android/app/ve/preview/PreviewFrameThread;
    invoke-static {v0}, Lcom/sec/android/app/ve/preview/ProjectPreview;->access$1(Lcom/sec/android/app/ve/preview/ProjectPreview;)Lcom/sec/android/app/ve/preview/PreviewFrameThread;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/sec/android/app/ve/preview/PreviewFrameThread;->setSurfaceAvailable(Z)V

    .line 540
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview$2;->this$0:Lcom/sec/android/app/ve/preview/ProjectPreview;

    invoke-static {v0, v3}, Lcom/sec/android/app/ve/preview/ProjectPreview;->access$0(Lcom/sec/android/app/ve/preview/ProjectPreview;Z)V

    .line 542
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview$2;->this$0:Lcom/sec/android/app/ve/preview/ProjectPreview;

    # getter for: Lcom/sec/android/app/ve/preview/ProjectPreview;->waitForSurfaceCreation:Z
    invoke-static {v0}, Lcom/sec/android/app/ve/preview/ProjectPreview;->access$5(Lcom/sec/android/app/ve/preview/ProjectPreview;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 544
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview$2;->this$0:Lcom/sec/android/app/ve/preview/ProjectPreview;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/preview/ProjectPreview;->access$6(Lcom/sec/android/app/ve/preview/ProjectPreview;Z)V

    .line 545
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview$2;->this$0:Lcom/sec/android/app/ve/preview/ProjectPreview;

    iget-object v1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview$2;->this$0:Lcom/sec/android/app/ve/preview/ProjectPreview;

    # getter for: Lcom/sec/android/app/ve/preview/ProjectPreview;->mPlayPos:J
    invoke-static {v1}, Lcom/sec/android/app/ve/preview/ProjectPreview;->access$7(Lcom/sec/android/app/ve/preview/ProjectPreview;)J

    move-result-wide v2

    # invokes: Lcom/sec/android/app/ve/preview/ProjectPreview;->_play(J)V
    invoke-static {v0, v2, v3}, Lcom/sec/android/app/ve/preview/ProjectPreview;->access$8(Lcom/sec/android/app/ve/preview/ProjectPreview;J)V

    .line 547
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview$2;->this$0:Lcom/sec/android/app/ve/preview/ProjectPreview;

    iget-object v0, v0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mAdapter:Lcom/sec/android/app/ve/PreviewPlayerInterface$Adapter;

    if-eqz v0, :cond_2

    .line 548
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview$2;->this$0:Lcom/sec/android/app/ve/preview/ProjectPreview;

    iget-object v0, v0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mAdapter:Lcom/sec/android/app/ve/PreviewPlayerInterface$Adapter;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/PreviewPlayerInterface$Adapter;->onSurfaceCreated()V

    .line 551
    :cond_2
    return-void

    .line 534
    :cond_3
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v0

    sget v1, Lcom/sec/android/app/ve/common/ConfigUtils;->FIXED_PREVIEW_WIDTH:I

    sget v2, Lcom/sec/android/app/ve/common/ConfigUtils;->FIXED_PREVIEW_HEIGHT:I

    invoke-virtual {v0, v1, v2}, Lcom/samsung/app/video/editor/external/NativeInterface;->setDisplayAspectRatio(II)V

    .line 535
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview$2;->this$0:Lcom/sec/android/app/ve/preview/ProjectPreview;

    # getter for: Lcom/sec/android/app/ve/preview/ProjectPreview;->mSurfaceHolder:Landroid/view/SurfaceHolder;
    invoke-static {v1}, Lcom/sec/android/app/ve/preview/ProjectPreview;->access$4(Lcom/sec/android/app/ve/preview/ProjectPreview;)Landroid/view/SurfaceHolder;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/app/video/editor/external/NativeInterface;->setPreviewDisplay(Landroid/view/SurfaceHolder;)V

    goto :goto_0
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 3
    .param p1, "arg0"    # Landroid/view/SurfaceHolder;

    .prologue
    const/4 v2, 0x0

    .line 511
    const-string v0, "PLAYER_STATE"

    const-string v1, "PVG::surface destroyed"

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 512
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/samsung/app/video/editor/external/NativeInterface;->_surfaceDestroyed(Landroid/view/SurfaceHolder;)V

    .line 514
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview$2;->this$0:Lcom/sec/android/app/ve/preview/ProjectPreview;

    invoke-static {v0, v2}, Lcom/sec/android/app/ve/preview/ProjectPreview;->access$0(Lcom/sec/android/app/ve/preview/ProjectPreview;Z)V

    .line 515
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview$2;->this$0:Lcom/sec/android/app/ve/preview/ProjectPreview;

    # getter for: Lcom/sec/android/app/ve/preview/ProjectPreview;->mFrameThread:Lcom/sec/android/app/ve/preview/PreviewFrameThread;
    invoke-static {v0}, Lcom/sec/android/app/ve/preview/ProjectPreview;->access$1(Lcom/sec/android/app/ve/preview/ProjectPreview;)Lcom/sec/android/app/ve/preview/PreviewFrameThread;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 516
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview$2;->this$0:Lcom/sec/android/app/ve/preview/ProjectPreview;

    # getter for: Lcom/sec/android/app/ve/preview/ProjectPreview;->mFrameThread:Lcom/sec/android/app/ve/preview/PreviewFrameThread;
    invoke-static {v0}, Lcom/sec/android/app/ve/preview/ProjectPreview;->access$1(Lcom/sec/android/app/ve/preview/ProjectPreview;)Lcom/sec/android/app/ve/preview/PreviewFrameThread;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/ve/preview/PreviewFrameThread;->setSurfaceAvailable(Z)V

    .line 518
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview$2;->this$0:Lcom/sec/android/app/ve/preview/ProjectPreview;

    # invokes: Lcom/sec/android/app/ve/preview/ProjectPreview;->_stop(Z)V
    invoke-static {v0, v2}, Lcom/sec/android/app/ve/preview/ProjectPreview;->access$2(Lcom/sec/android/app/ve/preview/ProjectPreview;Z)V

    .line 520
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview$2;->this$0:Lcom/sec/android/app/ve/preview/ProjectPreview;

    iget-object v0, v0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mAdapter:Lcom/sec/android/app/ve/PreviewPlayerInterface$Adapter;

    if-eqz v0, :cond_1

    .line 521
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview$2;->this$0:Lcom/sec/android/app/ve/preview/ProjectPreview;

    iget-object v0, v0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mAdapter:Lcom/sec/android/app/ve/PreviewPlayerInterface$Adapter;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/PreviewPlayerInterface$Adapter;->onStopped()V

    .line 522
    :cond_1
    return-void
.end method
