.class public Lcom/sec/android/app/ve/common/AndroidUtils$DlgFragment;
.super Landroid/app/DialogFragment;
.source "AndroidUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/ve/common/AndroidUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DlgFragment"
.end annotation


# instance fields
.field private mEditTextIfAny:Landroid/widget/EditText;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/app/ve/common/AndroidUtils$DlgFragment;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/android/app/ve/common/AndroidUtils$DlgFragment;->mEditTextIfAny:Landroid/widget/EditText;

    return-object v0
.end method


# virtual methods
.method public dismiss()V
    .locals 1

    .prologue
    .line 120
    :try_start_0
    invoke-super {p0}, Landroid/app/DialogFragment;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 124
    :goto_0
    return-void

    .line 121
    :catch_0
    move-exception v0

    .line 122
    .local v0, "e":Ljava/lang/Throwable;
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 96
    invoke-super {p0}, Landroid/app/DialogFragment;->onPause()V

    .line 97
    iget-object v0, p0, Lcom/sec/android/app/ve/common/AndroidUtils$DlgFragment;->mEditTextIfAny:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    .line 98
    iget-object v0, p0, Lcom/sec/android/app/ve/common/AndroidUtils$DlgFragment;->mEditTextIfAny:Landroid/widget/EditText;

    invoke-static {v0}, Lcom/sec/android/app/ve/common/AndroidUtils;->hideSoftKeyboard(Landroid/view/View;)V

    .line 100
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    .line 83
    invoke-super {p0}, Landroid/app/DialogFragment;->onResume()V

    .line 84
    iget-object v0, p0, Lcom/sec/android/app/ve/common/AndroidUtils$DlgFragment;->mEditTextIfAny:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Lcom/sec/android/app/ve/common/AndroidUtils$DlgFragment;->mEditTextIfAny:Landroid/widget/EditText;

    new-instance v1, Lcom/sec/android/app/ve/common/AndroidUtils$DlgFragment$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/ve/common/AndroidUtils$DlgFragment$1;-><init>(Lcom/sec/android/app/ve/common/AndroidUtils$DlgFragment;)V

    .line 90
    const-wide/16 v2, 0x64

    .line 85
    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/EditText;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 92
    :cond_0
    return-void
.end method

.method public setShowSoftKeyBoardFor(Landroid/widget/EditText;)V
    .locals 0
    .param p1, "editText"    # Landroid/widget/EditText;

    .prologue
    .line 103
    iput-object p1, p0, Lcom/sec/android/app/ve/common/AndroidUtils$DlgFragment;->mEditTextIfAny:Landroid/widget/EditText;

    .line 104
    return-void
.end method

.method public show(Landroid/app/FragmentManager;Ljava/lang/String;)V
    .locals 1
    .param p1, "fm"    # Landroid/app/FragmentManager;
    .param p2, "tag"    # Ljava/lang/String;

    .prologue
    .line 110
    :try_start_0
    invoke-super {p0, p1, p2}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 114
    :goto_0
    return-void

    .line 111
    :catch_0
    move-exception v0

    .line 112
    .local v0, "e":Ljava/lang/Throwable;
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method
