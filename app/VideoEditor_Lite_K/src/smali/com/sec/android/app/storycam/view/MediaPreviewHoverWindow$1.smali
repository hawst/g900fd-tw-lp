.class Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$1;
.super Ljava/lang/Object;
.source "MediaPreviewHoverWindow.java"

# interfaces
.implements Landroid/view/View$OnHoverListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$1;->this$0:Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 53
    invoke-virtual {p2, v0}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v2

    if-eq v2, v1, :cond_0

    invoke-virtual {p2, v0}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v2

    const/4 v3, 0x2

    if-eq v2, v3, :cond_0

    .line 83
    :goto_0
    return v0

    .line 56
    :cond_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_1
    :goto_1
    :pswitch_0
    move v0, v1

    .line 83
    goto :goto_0

    .line 58
    :pswitch_1
    # getter for: Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;->mHandler:Landroid/os/Handler;
    invoke-static {}, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;->access$0()Landroid/os/Handler;

    move-result-object v2

    if-eqz v2, :cond_2

    # getter for: Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;->mHandler:Landroid/os/Handler;
    invoke-static {}, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;->access$0()Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 59
    # getter for: Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;->mHandler:Landroid/os/Handler;
    invoke-static {}, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;->access$0()Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/os/Handler;->removeMessages(I)V

    .line 60
    :cond_2
    const/16 v0, 0xa

    invoke-static {v0}, Lcom/sec/android/app/ve/common/ConfigUtils;->setHoveringPointerIcon(I)V

    goto :goto_1

    .line 64
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$1;->this$0:Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;

    # getter for: Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;->mViewRect:Landroid/graphics/RectF;
    invoke-static {v0}, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;->access$1(Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;)Landroid/graphics/RectF;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$1;->this$0:Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;

    # getter for: Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;->mDialogRect:Landroid/graphics/RectF;
    invoke-static {v0}, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;->access$2(Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;)Landroid/graphics/RectF;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 67
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$1;->this$0:Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;

    # getter for: Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;->mViewRect:Landroid/graphics/RectF;
    invoke-static {v0}, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;->access$1(Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;)Landroid/graphics/RectF;

    move-result-object v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$1;->this$0:Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;

    # getter for: Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;->mDialogRect:Landroid/graphics/RectF;
    invoke-static {v0}, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;->access$2(Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;)Landroid/graphics/RectF;

    move-result-object v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v0

    if-nez v0, :cond_3

    .line 68
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$1;->this$0:Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;

    invoke-virtual {v0}, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;->dismiss()V

    goto :goto_1

    .line 71
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$1;->this$0:Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;

    invoke-virtual {v0}, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;->keepOnScreen()V

    goto :goto_1

    .line 75
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$1;->this$0:Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;

    invoke-virtual {v0}, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;->dismiss()V

    .line 76
    invoke-static {v1}, Lcom/sec/android/app/ve/common/ConfigUtils;->setHoveringPointerIcon(I)V

    goto :goto_1

    .line 56
    nop

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method
