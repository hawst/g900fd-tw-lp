.class public Lcom/sec/android/app/ve/projectcompatibility/VEJSONAnimation;
.super Lorg/json/JSONObject;
.source "VEJSONAnimation.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lorg/json/JSONObject;-><init>()V

    .line 27
    return-void
.end method

.method protected constructor <init>(Lorg/json/JSONObject;)V
    .locals 1
    .param p1, "source"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 30
    invoke-virtual {p1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 31
    return-void
.end method


# virtual methods
.method public getAnimationEndFrame()I
    .locals 3

    .prologue
    .line 111
    const/4 v1, 0x0

    .line 113
    .local v1, "endFrame":I
    :try_start_0
    const-string v2, "ANIM_END"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAnimation;->getInt(Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 118
    :goto_0
    return v1

    .line 115
    :catch_0
    move-exception v0

    .line 116
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getAnimationInterpolation()I
    .locals 3

    .prologue
    .line 140
    const/4 v1, 0x0

    .line 142
    .local v1, "interpolation":I
    :try_start_0
    const-string v2, "ANIM_INTERPOLATION"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAnimation;->getInt(Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 147
    :goto_0
    return v1

    .line 144
    :catch_0
    move-exception v0

    .line 145
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getAnimationStartFrame()I
    .locals 3

    .prologue
    .line 83
    const/4 v1, 0x0

    .line 85
    .local v1, "startFrame":I
    :try_start_0
    const-string v2, "ANIM_START"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAnimation;->getInt(Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 90
    :goto_0
    return v1

    .line 87
    :catch_0
    move-exception v0

    .line 88
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getAnimationType()I
    .locals 3

    .prologue
    .line 52
    const/4 v0, 0x0

    .line 54
    .local v0, "animType":I
    :try_start_0
    const-string v2, "ANIM_TYPE"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAnimation;->getInt(Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 59
    :goto_0
    return v0

    .line 56
    :catch_0
    move-exception v1

    .line 57
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getEndAlpha()F
    .locals 4

    .prologue
    .line 188
    const/high16 v1, 0x3f800000    # 1.0f

    .line 190
    .local v1, "endAlpha":F
    :try_start_0
    const-string v2, "ALPHA_END"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAnimation;->getDouble(Ljava/lang/String;)D
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    double-to-float v1, v2

    .line 195
    :goto_0
    return v1

    .line 192
    :catch_0
    move-exception v0

    .line 193
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getRotationEnd()F
    .locals 4

    .prologue
    .line 403
    const/4 v1, 0x0

    .line 405
    .local v1, "rotationEnd":F
    :try_start_0
    const-string v2, "ROTATION_END"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAnimation;->getDouble(Ljava/lang/String;)D
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    double-to-float v1, v2

    .line 410
    :goto_0
    return v1

    .line 407
    :catch_0
    move-exception v0

    .line 408
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getRotationStart()F
    .locals 4

    .prologue
    .line 388
    const/4 v1, 0x0

    .line 390
    .local v1, "rotationStart":F
    :try_start_0
    const-string v2, "ROTATION_START"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAnimation;->getDouble(Ljava/lang/String;)D
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    double-to-float v1, v2

    .line 395
    :goto_0
    return v1

    .line 392
    :catch_0
    move-exception v0

    .line 393
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getScaleEndX()F
    .locals 4

    .prologue
    .line 256
    const/high16 v1, 0x3f800000    # 1.0f

    .line 258
    .local v1, "scaleEndX":F
    :try_start_0
    const-string v2, "SCALE_END_X"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAnimation;->getDouble(Ljava/lang/String;)D
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    double-to-float v1, v2

    .line 263
    :goto_0
    return v1

    .line 260
    :catch_0
    move-exception v0

    .line 261
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getScaleEndY()F
    .locals 4

    .prologue
    .line 272
    const/high16 v1, 0x3f800000    # 1.0f

    .line 274
    .local v1, "scaleEndY":F
    :try_start_0
    const-string v2, "SCALE_END_Y"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAnimation;->getDouble(Ljava/lang/String;)D
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    double-to-float v1, v2

    .line 279
    :goto_0
    return v1

    .line 276
    :catch_0
    move-exception v0

    .line 277
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getScaleStartX()F
    .locals 4

    .prologue
    .line 224
    const/high16 v1, 0x3f800000    # 1.0f

    .line 226
    .local v1, "scaleStartX":F
    :try_start_0
    const-string v2, "SCALE_START_X"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAnimation;->getDouble(Ljava/lang/String;)D
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    double-to-float v1, v2

    .line 231
    :goto_0
    return v1

    .line 228
    :catch_0
    move-exception v0

    .line 229
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getScaleStartY()F
    .locals 4

    .prologue
    .line 240
    const/high16 v1, 0x3f800000    # 1.0f

    .line 242
    .local v1, "scaleStartY":F
    :try_start_0
    const-string v2, "SCALE_START_Y"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAnimation;->getDouble(Ljava/lang/String;)D
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    double-to-float v1, v2

    .line 247
    :goto_0
    return v1

    .line 244
    :catch_0
    move-exception v0

    .line 245
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getStartAlpha()F
    .locals 4

    .prologue
    .line 172
    const/high16 v1, 0x3f800000    # 1.0f

    .line 174
    .local v1, "startAlpha":F
    :try_start_0
    const-string v2, "ALPHA_START"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAnimation;->getDouble(Ljava/lang/String;)D
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    double-to-float v1, v2

    .line 179
    :goto_0
    return v1

    .line 176
    :catch_0
    move-exception v0

    .line 177
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getTranslateEndX()F
    .locals 4

    .prologue
    .line 340
    const/4 v1, 0x0

    .line 342
    .local v1, "translateEndX":F
    :try_start_0
    const-string v2, "TRANSLATION_TO_X"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAnimation;->getDouble(Ljava/lang/String;)D
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    double-to-float v1, v2

    .line 347
    :goto_0
    return v1

    .line 344
    :catch_0
    move-exception v0

    .line 345
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getTranslateEndY()F
    .locals 4

    .prologue
    .line 356
    const/4 v1, 0x0

    .line 358
    .local v1, "translateEndY":F
    :try_start_0
    const-string v2, "TRANSLATION_TO_Y"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAnimation;->getDouble(Ljava/lang/String;)D
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    double-to-float v1, v2

    .line 363
    :goto_0
    return v1

    .line 360
    :catch_0
    move-exception v0

    .line 361
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getTranslateStartX()F
    .locals 4

    .prologue
    .line 308
    const/4 v1, 0x0

    .line 310
    .local v1, "translateStartX":F
    :try_start_0
    const-string v2, "TRANSLATION_FROM_X"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAnimation;->getDouble(Ljava/lang/String;)D
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    double-to-float v1, v2

    .line 315
    :goto_0
    return v1

    .line 312
    :catch_0
    move-exception v0

    .line 313
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getTranslateStartY()F
    .locals 4

    .prologue
    .line 324
    const/4 v1, 0x0

    .line 326
    .local v1, "translateStartY":F
    :try_start_0
    const-string v2, "TRANSLATION_FROM_Y"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAnimation;->getDouble(Ljava/lang/String;)D
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    double-to-float v1, v2

    .line 331
    :goto_0
    return v1

    .line 328
    :catch_0
    move-exception v0

    .line 329
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public setAlpha(FF)V
    .locals 4
    .param p1, "startAlpha"    # F
    .param p2, "endAlpha"    # F

    .prologue
    .line 158
    :try_start_0
    const-string v1, "ALPHA_START"

    float-to-double v2, p1

    invoke-virtual {p0, v1, v2, v3}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAnimation;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 159
    const-string v1, "ALPHA_END"

    float-to-double v2, p2

    invoke-virtual {p0, v1, v2, v3}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAnimation;->put(Ljava/lang/String;D)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 164
    :goto_0
    return-void

    .line 161
    :catch_0
    move-exception v0

    .line 162
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public setAnimationEndFrame(I)V
    .locals 2
    .param p1, "animEndFrame"    # I

    .prologue
    .line 99
    :try_start_0
    const-string v1, "ANIM_END"

    invoke-virtual {p0, v1, p1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAnimation;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 104
    :goto_0
    return-void

    .line 101
    :catch_0
    move-exception v0

    .line 102
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public setAnimationInterpolation(I)V
    .locals 2
    .param p1, "interpolation"    # I

    .prologue
    .line 128
    :try_start_0
    const-string v1, "ANIM_INTERPOLATION"

    invoke-virtual {p0, v1, p1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAnimation;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 133
    :goto_0
    return-void

    .line 130
    :catch_0
    move-exception v0

    .line 131
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public setAnimationStartFrame(I)V
    .locals 2
    .param p1, "animStartFrame"    # I

    .prologue
    .line 71
    :try_start_0
    const-string v1, "ANIM_START"

    invoke-virtual {p0, v1, p1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAnimation;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 76
    :goto_0
    return-void

    .line 73
    :catch_0
    move-exception v0

    .line 74
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public setAnimationType(I)V
    .locals 2
    .param p1, "type"    # I

    .prologue
    .line 40
    :try_start_0
    const-string v1, "ANIM_TYPE"

    invoke-virtual {p0, v1, p1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAnimation;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 45
    :goto_0
    return-void

    .line 42
    :catch_0
    move-exception v0

    .line 43
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public setRotation(FF)V
    .locals 4
    .param p1, "rotationStart"    # F
    .param p2, "rotationEnd"    # F

    .prologue
    .line 374
    :try_start_0
    const-string v1, "ROTATION_START"

    float-to-double v2, p1

    invoke-virtual {p0, v1, v2, v3}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAnimation;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 375
    const-string v1, "ROTATION_END"

    float-to-double v2, p2

    invoke-virtual {p0, v1, v2, v3}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAnimation;->put(Ljava/lang/String;D)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 380
    :goto_0
    return-void

    .line 377
    :catch_0
    move-exception v0

    .line 378
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public setScale(FFFF)V
    .locals 4
    .param p1, "startScaleX"    # F
    .param p2, "endScaleX"    # F
    .param p3, "startScaleY"    # F
    .param p4, "endScaleY"    # F

    .prologue
    .line 208
    :try_start_0
    const-string v1, "SCALE_START_X"

    float-to-double v2, p1

    invoke-virtual {p0, v1, v2, v3}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAnimation;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 209
    const-string v1, "SCALE_END_X"

    float-to-double v2, p2

    invoke-virtual {p0, v1, v2, v3}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAnimation;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 210
    const-string v1, "SCALE_START_Y"

    float-to-double v2, p3

    invoke-virtual {p0, v1, v2, v3}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAnimation;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 211
    const-string v1, "SCALE_END_Y"

    float-to-double v2, p4

    invoke-virtual {p0, v1, v2, v3}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAnimation;->put(Ljava/lang/String;D)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 216
    :goto_0
    return-void

    .line 213
    :catch_0
    move-exception v0

    .line 214
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public setTranslations(FFFF)V
    .locals 4
    .param p1, "translateStartX"    # F
    .param p2, "translateEndX"    # F
    .param p3, "translateStartY"    # F
    .param p4, "translateEndY"    # F

    .prologue
    .line 292
    :try_start_0
    const-string v1, "TRANSLATION_FROM_X"

    float-to-double v2, p1

    invoke-virtual {p0, v1, v2, v3}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAnimation;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 293
    const-string v1, "TRANSLATION_TO_X"

    float-to-double v2, p2

    invoke-virtual {p0, v1, v2, v3}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAnimation;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 294
    const-string v1, "TRANSLATION_FROM_Y"

    float-to-double v2, p3

    invoke-virtual {p0, v1, v2, v3}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAnimation;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 295
    const-string v1, "TRANSLATION_TO_Y"

    float-to-double v2, p4

    invoke-virtual {p0, v1, v2, v3}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAnimation;->put(Ljava/lang/String;D)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 300
    :goto_0
    return-void

    .line 297
    :catch_0
    move-exception v0

    .line 298
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method
