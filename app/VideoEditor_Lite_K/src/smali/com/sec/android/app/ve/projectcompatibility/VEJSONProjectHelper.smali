.class public Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectHelper;
.super Ljava/lang/Object;
.source "VEJSONProjectHelper.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static convertStreamToString(Ljava/io/InputStream;)Ljava/lang/String;
    .locals 6
    .param p0, "is"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 71
    if-eqz p0, :cond_1

    .line 72
    new-instance v3, Ljava/io/StringWriter;

    invoke-direct {v3}, Ljava/io/StringWriter;-><init>()V

    .line 74
    .local v3, "writer":Ljava/io/Writer;
    const/16 v4, 0x400

    new-array v0, v4, [C

    .line 76
    .local v0, "buffer":[C
    :try_start_0
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/InputStreamReader;

    const-string v5, "UTF-8"

    invoke-direct {v4, p0, v5}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-direct {v2, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 77
    .local v2, "reader":Ljava/io/Reader;
    invoke-virtual {v2, v0}, Ljava/io/Reader;->read([C)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    .line 78
    .local v1, "n":I
    :goto_0
    const/4 v4, -0x1

    if-ne v1, v4, :cond_0

    .line 83
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V

    .line 85
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 87
    .end local v0    # "buffer":[C
    .end local v1    # "n":I
    .end local v2    # "reader":Ljava/io/Reader;
    .end local v3    # "writer":Ljava/io/Writer;
    :goto_1
    return-object v4

    .line 79
    .restart local v0    # "buffer":[C
    .restart local v1    # "n":I
    .restart local v2    # "reader":Ljava/io/Reader;
    .restart local v3    # "writer":Ljava/io/Writer;
    :cond_0
    const/4 v4, 0x0

    :try_start_1
    invoke-virtual {v3, v0, v4, v1}, Ljava/io/Writer;->write([CII)V

    .line 80
    invoke-virtual {v2, v0}, Ljava/io/Reader;->read([C)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    goto :goto_0

    .line 82
    .end local v1    # "n":I
    .end local v2    # "reader":Ljava/io/Reader;
    :catchall_0
    move-exception v4

    .line 83
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V

    .line 84
    throw v4

    .line 87
    .end local v0    # "buffer":[C
    .end local v3    # "writer":Ljava/io/Writer;
    :cond_1
    const-string v4, ""

    goto :goto_1
.end method

.method public static openSavedProject(Ljava/lang/String;)Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;
    .locals 8
    .param p0, "jsonFilePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/io/FileNotFoundException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 41
    if-nez p0, :cond_0

    .line 42
    new-instance v6, Ljava/lang/IllegalArgumentException;

    const-string v7, "Project File does not exist or path is Invalid"

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 45
    :cond_0
    const/4 v1, 0x0

    .line 46
    .local v1, "projectRoot":Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;
    const/4 v4, 0x0

    .line 47
    .local v4, "temp":Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;
    const/4 v2, 0x0

    .line 50
    .local v2, "stream":Ljava/io/FileInputStream;
    :try_start_0
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, p0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 51
    .end local v2    # "stream":Ljava/io/FileInputStream;
    .local v3, "stream":Ljava/io/FileInputStream;
    :try_start_1
    invoke-static {v3}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectHelper;->convertStreamToString(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v0

    .line 52
    .local v0, "jSonString":Ljava/lang/String;
    new-instance v5, Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;

    invoke-direct {v5, v0}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 53
    .end local v4    # "temp":Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;
    .local v5, "temp":Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;
    :try_start_2
    new-instance v1, Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;

    .end local v1    # "projectRoot":Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;
    invoke-direct {v1, v5}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;-><init>(Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 56
    .restart local v1    # "projectRoot":Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;
    if-eqz v3, :cond_1

    .line 57
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V

    .line 60
    :cond_1
    return-object v1

    .line 55
    .end local v0    # "jSonString":Ljava/lang/String;
    .end local v3    # "stream":Ljava/io/FileInputStream;
    .end local v5    # "temp":Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;
    .restart local v2    # "stream":Ljava/io/FileInputStream;
    .restart local v4    # "temp":Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;
    :catchall_0
    move-exception v6

    .line 56
    .end local v1    # "projectRoot":Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;
    :goto_0
    if-eqz v2, :cond_2

    .line 57
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V

    .line 58
    :cond_2
    throw v6

    .line 55
    .end local v2    # "stream":Ljava/io/FileInputStream;
    .restart local v1    # "projectRoot":Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;
    .restart local v3    # "stream":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v6

    move-object v2, v3

    .end local v3    # "stream":Ljava/io/FileInputStream;
    .restart local v2    # "stream":Ljava/io/FileInputStream;
    goto :goto_0

    .end local v1    # "projectRoot":Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;
    .end local v2    # "stream":Ljava/io/FileInputStream;
    .end local v4    # "temp":Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;
    .restart local v0    # "jSonString":Ljava/lang/String;
    .restart local v3    # "stream":Ljava/io/FileInputStream;
    .restart local v5    # "temp":Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;
    :catchall_2
    move-exception v6

    move-object v2, v3

    .end local v3    # "stream":Ljava/io/FileInputStream;
    .restart local v2    # "stream":Ljava/io/FileInputStream;
    move-object v4, v5

    .end local v5    # "temp":Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;
    .restart local v4    # "temp":Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;
    goto :goto_0
.end method

.method public static saveProject(Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;Ljava/lang/String;)V
    .locals 5
    .param p0, "projectRoot"    # Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;
    .param p1, "absFilePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 100
    const/4 v0, 0x0

    .line 102
    .local v0, "fos":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 103
    .local v2, "prjFile":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_0

    .line 104
    invoke-virtual {v2}, Ljava/io/File;->createNewFile()Z

    .line 106
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;->toString()Ljava/lang/String;

    move-result-object v3

    .line 107
    .local v3, "project":Ljava/lang/String;
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 108
    .end local v0    # "fos":Ljava/io/FileOutputStream;
    .local v1, "fos":Ljava/io/FileOutputStream;
    :try_start_1
    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 111
    if-eqz v1, :cond_1

    .line 112
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    .line 114
    :cond_1
    return-void

    .line 110
    .end local v1    # "fos":Ljava/io/FileOutputStream;
    .end local v2    # "prjFile":Ljava/io/File;
    .end local v3    # "project":Ljava/lang/String;
    .restart local v0    # "fos":Ljava/io/FileOutputStream;
    :catchall_0
    move-exception v4

    .line 111
    :goto_0
    if-eqz v0, :cond_2

    .line 112
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V

    .line 113
    :cond_2
    throw v4

    .line 110
    .end local v0    # "fos":Ljava/io/FileOutputStream;
    .restart local v1    # "fos":Ljava/io/FileOutputStream;
    .restart local v2    # "prjFile":Ljava/io/File;
    .restart local v3    # "project":Ljava/lang/String;
    :catchall_1
    move-exception v4

    move-object v0, v1

    .end local v1    # "fos":Ljava/io/FileOutputStream;
    .restart local v0    # "fos":Ljava/io/FileOutputStream;
    goto :goto_0
.end method
