.class public Lcom/sec/android/app/ve/projectcompatibility/VEJSONBitmapBlendAnimation;
.super Lorg/json/JSONObject;
.source "VEJSONBitmapBlendAnimation.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lorg/json/JSONObject;-><init>()V

    .line 20
    return-void
.end method

.method protected constructor <init>(Lorg/json/JSONObject;)V
    .locals 1
    .param p1, "source"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 23
    invoke-virtual {p1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 24
    return-void
.end method


# virtual methods
.method public getCurrentEndFrameWithinElement()I
    .locals 3

    .prologue
    .line 95
    const/4 v0, 0x0

    .line 97
    .local v0, "currentEndFrame":I
    :try_start_0
    const-string v2, "BLENDING_FILES_CURRENT_END_FRAME"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONBitmapBlendAnimation;->getInt(Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 102
    :goto_0
    return v0

    .line 99
    :catch_0
    move-exception v1

    .line 100
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getCurrentStartFrameWithinElement()I
    .locals 3

    .prologue
    .line 48
    const/4 v0, 0x0

    .line 50
    .local v0, "currentStartFrame":I
    :try_start_0
    const-string v2, "BLENDING_FILES_CURRENT_START_FRAME"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONBitmapBlendAnimation;->getInt(Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 55
    :goto_0
    return v0

    .line 52
    :catch_0
    move-exception v1

    .line 53
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getEndIndexInTemplate()I
    .locals 3

    .prologue
    .line 197
    const/4 v1, 0x0

    .line 199
    .local v1, "endIndex":I
    :try_start_0
    const-string v2, "BLENDING_FILES_END_INDEX"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONBitmapBlendAnimation;->getInt(Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 204
    :goto_0
    return v1

    .line 201
    :catch_0
    move-exception v0

    .line 202
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getFilenameTemplateForBlendingBitmap()Ljava/lang/String;
    .locals 3

    .prologue
    .line 139
    const/4 v1, 0x0

    .line 141
    .local v1, "template":Ljava/lang/String;
    :try_start_0
    const-string v2, "BLENDING_FILES_PNG_FILE_NAME"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONBitmapBlendAnimation;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 146
    :goto_0
    return-object v1

    .line 143
    :catch_0
    move-exception v0

    .line 144
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getOriginalEndFrameWithinElement()I
    .locals 3

    .prologue
    .line 110
    const/4 v1, 0x0

    .line 112
    .local v1, "originalEndFrame":I
    :try_start_0
    const-string v2, "BLENDING_FILES_ORIG_END_FRAME"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONBitmapBlendAnimation;->getInt(Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 117
    :goto_0
    return v1

    .line 114
    :catch_0
    move-exception v0

    .line 115
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getOriginalStartFrameWithinElement()I
    .locals 3

    .prologue
    .line 63
    const/4 v1, 0x0

    .line 65
    .local v1, "originalStartFrame":I
    :try_start_0
    const-string v2, "BLENDING_FILES_ORIG_START_FRAME"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONBitmapBlendAnimation;->getInt(Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 70
    :goto_0
    return v1

    .line 67
    :catch_0
    move-exception v0

    .line 68
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getStartIndexInTemplate()I
    .locals 3

    .prologue
    .line 168
    const/4 v1, 0x0

    .line 170
    .local v1, "startIndex":I
    :try_start_0
    const-string v2, "BLENDING_FILES_START_INDEX"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONBitmapBlendAnimation;->getInt(Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 175
    :goto_0
    return v1

    .line 172
    :catch_0
    move-exception v0

    .line 173
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public setEndFrameWithinElement(II)V
    .locals 2
    .param p1, "currentEndFrame"    # I
    .param p2, "originalEndFrame"    # I

    .prologue
    .line 82
    :try_start_0
    const-string v1, "BLENDING_FILES_CURRENT_END_FRAME"

    invoke-virtual {p0, v1, p1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONBitmapBlendAnimation;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 83
    const-string v1, "BLENDING_FILES_ORIG_END_FRAME"

    invoke-virtual {p0, v1, p2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONBitmapBlendAnimation;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 88
    :goto_0
    return-void

    .line 85
    :catch_0
    move-exception v0

    .line 86
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public setEndIndexInTemplate(I)V
    .locals 2
    .param p1, "endIndexInTemplate"    # I

    .prologue
    .line 185
    :try_start_0
    const-string v1, "BLENDING_FILES_END_INDEX"

    invoke-virtual {p0, v1, p1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONBitmapBlendAnimation;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 190
    :goto_0
    return-void

    .line 187
    :catch_0
    move-exception v0

    .line 188
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public setFilenameTemplateForBlendingBitmap(Ljava/lang/String;)V
    .locals 2
    .param p1, "pngTemplateFileName"    # Ljava/lang/String;

    .prologue
    .line 127
    :try_start_0
    const-string v1, "BLENDING_FILES_PNG_FILE_NAME"

    invoke-virtual {p0, v1, p1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONBitmapBlendAnimation;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 132
    :goto_0
    return-void

    .line 129
    :catch_0
    move-exception v0

    .line 130
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public setStartFrameWithinElement(II)V
    .locals 2
    .param p1, "currentStartFrame"    # I
    .param p2, "originalStartFrame"    # I

    .prologue
    .line 35
    :try_start_0
    const-string v1, "BLENDING_FILES_CURRENT_START_FRAME"

    invoke-virtual {p0, v1, p1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONBitmapBlendAnimation;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 36
    const-string v1, "BLENDING_FILES_ORIG_START_FRAME"

    invoke-virtual {p0, v1, p2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONBitmapBlendAnimation;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 41
    :goto_0
    return-void

    .line 38
    :catch_0
    move-exception v0

    .line 39
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public setStartIndexInTemplate(I)V
    .locals 2
    .param p1, "startIndexInTemplate"    # I

    .prologue
    .line 156
    :try_start_0
    const-string v1, "BLENDING_FILES_START_INDEX"

    invoke-virtual {p0, v1, p1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONBitmapBlendAnimation;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 161
    :goto_0
    return-void

    .line 158
    :catch_0
    move-exception v0

    .line 159
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method
