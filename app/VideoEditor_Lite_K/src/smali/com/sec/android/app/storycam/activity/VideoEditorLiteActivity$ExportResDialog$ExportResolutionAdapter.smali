.class Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog$ExportResolutionAdapter;
.super Landroid/widget/BaseAdapter;
.source "VideoEditorLiteActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ExportResolutionAdapter"
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog;)V
    .locals 0

    .prologue
    .line 1136
    iput-object p1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog$ExportResolutionAdapter;->this$1:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog;Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog$ExportResolutionAdapter;)V
    .locals 0

    .prologue
    .line 1136
    invoke-direct {p0, p1}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog$ExportResolutionAdapter;-><init>(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog;)V

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 1140
    iget-object v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog$ExportResolutionAdapter;->this$1:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog;

    # getter for: Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog;->exportResOptionsText:[Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog;->access$0(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog;)[Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1141
    const/4 v0, 0x0

    .line 1144
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog$ExportResolutionAdapter;->this$1:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog;

    # getter for: Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog;->exportResOptionsText:[Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog;->access$0(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog;)[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "arg0"    # I

    .prologue
    .line 1149
    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "arg0"    # I

    .prologue
    .line 1154
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "container"    # Landroid/view/ViewGroup;

    .prologue
    .line 1159
    if-nez p2, :cond_0

    .line 1160
    iget-object v3, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog$ExportResolutionAdapter;->this$1:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog;

    invoke-virtual {v3}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const v4, 0x7f030006

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    :cond_0
    move-object v0, p2

    .line 1163
    check-cast v0, Lcom/sec/android/app/storycam/view/ExportResolutionItemView;

    .line 1164
    .local v0, "itemView":Lcom/sec/android/app/storycam/view/ExportResolutionItemView;
    const v3, 0x7f0d0021

    invoke-virtual {v0, v3}, Lcom/sec/android/app/storycam/view/ExportResolutionItemView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 1165
    .local v1, "reslnText":Landroid/widget/TextView;
    const v3, 0x7f0d0022

    invoke-virtual {v0, v3}, Lcom/sec/android/app/storycam/view/ExportResolutionItemView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 1166
    .local v2, "reslnTextDesc":Landroid/widget/TextView;
    iget-object v3, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog$ExportResolutionAdapter;->this$1:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog;

    # getter for: Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog;->exportResOptionsText:[Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog;->access$0(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog;)[Ljava/lang/String;

    move-result-object v3

    aget-object v3, v3, p1

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1167
    iget-object v3, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog$ExportResolutionAdapter;->this$1:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog;

    # getter for: Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog;->exportResOptionsTextDesc:[Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog;->access$1(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog;)[Ljava/lang/String;

    move-result-object v3

    aget-object v3, v3, p1

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1168
    return-object v0
.end method
