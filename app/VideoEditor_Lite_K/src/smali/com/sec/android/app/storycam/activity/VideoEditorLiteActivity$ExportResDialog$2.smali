.class Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog$2;
.super Ljava/lang/Object;
.source "VideoEditorLiteActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog;


# direct methods
.method constructor <init>(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog$2;->this$1:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog;

    .line 1113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "arg1"    # I

    .prologue
    .line 1117
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 1119
    iget-object v1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog$2;->this$1:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog;

    invoke-virtual {v1}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;

    # getter for: Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mRootView:Landroid/widget/RelativeLayout;
    invoke-static {v1}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->access$0(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;)Landroid/widget/RelativeLayout;

    move-result-object v1

    const v2, 0x7f0d0003

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    .line 1121
    .local v0, "pvg":Lcom/sec/android/app/storycam/view/PreviewViewGroup;
    if-eqz v0, :cond_0

    .line 1122
    invoke-virtual {v0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->pause_player()V

    .line 1123
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog$2;->this$1:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog;

    invoke-virtual {v1}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;

    iget-object v2, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog$2;->this$1:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog;

    # getter for: Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog;->selectedResIndex:I
    invoke-static {v2}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog;->access$3(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog;)I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog$2;->this$1:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog;

    # getter for: Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog;->m2Model:I
    invoke-static {v3}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog;->access$4(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog;)I

    move-result v3

    add-int/2addr v2, v3

    invoke-virtual {v1, v2}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->beginexport(I)V

    .line 1125
    return-void
.end method
