.class Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;
.super Ljava/lang/Object;
.source "MediaEditGridView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/storycam/view/MediaEditGridView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DragMovementListener"
.end annotation


# static fields
.field public static final LEFT_DRAGGING:I = 0x1

.field public static final MODE_LANDSCAPE:I = 0x1

.field public static final MODE_PORTRAIT:I = 0x0

.field public static final NOT_DRAGGING:I = 0x0

.field public static final RIGHT_DRAGGING:I = 0x2


# instance fields
.field private mLastDragX:F

.field private mLastDragY:F

.field public movement:I

.field final synthetic this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/storycam/view/MediaEditGridView;)V
    .locals 2

    .prologue
    const/high16 v1, -0x40800000    # -1.0f

    .line 208
    iput-object p1, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 212
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;->movement:I

    .line 213
    iput v1, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;->mLastDragX:F

    .line 214
    iput v1, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;->mLastDragY:F

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/storycam/view/MediaEditGridView;Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;)V
    .locals 0

    .prologue
    .line 208
    invoke-direct {p0, p1}, Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;-><init>(Lcom/sec/android/app/storycam/view/MediaEditGridView;)V

    return-void
.end method


# virtual methods
.method public getDragDirection()I
    .locals 1

    .prologue
    .line 242
    iget v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;->movement:I

    return v0
.end method

.method public listenToDrag(Landroid/view/DragEvent;I)V
    .locals 7
    .param p1, "event"    # Landroid/view/DragEvent;
    .param p2, "mode"    # I

    .prologue
    const/4 v3, 0x2

    const/4 v4, 0x0

    const/high16 v6, -0x40800000    # -1.0f

    const/4 v2, 0x1

    .line 219
    if-ne p2, v2, :cond_4

    .line 220
    invoke-virtual {p1}, Landroid/view/DragEvent;->getX()F

    move-result v0

    .line 221
    .local v0, "x":F
    iget v5, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;->mLastDragX:F

    cmpl-float v5, v5, v6

    if-nez v5, :cond_1

    .line 222
    iput v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;->mLastDragX:F

    .line 239
    .end local v0    # "x":F
    :cond_0
    :goto_0
    return-void

    .line 225
    .restart local v0    # "x":F
    :cond_1
    iget v5, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;->mLastDragX:F

    cmpg-float v5, v0, v5

    if-gez v5, :cond_2

    :goto_1
    iput v2, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;->movement:I

    .line 226
    iput v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;->mLastDragX:F

    goto :goto_0

    .line 225
    :cond_2
    iget v2, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;->mLastDragX:F

    cmpl-float v2, v0, v2

    if-lez v2, :cond_3

    move v2, v3

    goto :goto_1

    :cond_3
    move v2, v4

    goto :goto_1

    .line 229
    .end local v0    # "x":F
    :cond_4
    if-nez p2, :cond_0

    .line 230
    invoke-virtual {p1}, Landroid/view/DragEvent;->getY()F

    move-result v1

    .line 231
    .local v1, "y":F
    iget v5, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;->mLastDragY:F

    cmpl-float v5, v5, v6

    if-nez v5, :cond_5

    .line 232
    iput v1, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;->mLastDragY:F

    goto :goto_0

    .line 235
    :cond_5
    iget v5, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;->mLastDragY:F

    cmpg-float v5, v1, v5

    if-gez v5, :cond_6

    :goto_2
    iput v2, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;->movement:I

    .line 236
    iput v1, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;->mLastDragY:F

    goto :goto_0

    .line 235
    :cond_6
    iget v2, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;->mLastDragY:F

    cmpl-float v2, v1, v2

    if-lez v2, :cond_7

    move v2, v3

    goto :goto_2

    :cond_7
    move v2, v4

    goto :goto_2
.end method

.method public reset()V
    .locals 1

    .prologue
    const/high16 v0, -0x40800000    # -1.0f

    .line 246
    iput v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;->mLastDragX:F

    .line 247
    iput v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;->mLastDragY:F

    .line 248
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;->movement:I

    .line 249
    return-void
.end method
