.class public Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;
.super Ljava/lang/Object;
.source "ExportWrapper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExpAdapter;,
        Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment;,
        Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;
    }
.end annotation


# static fields
.field public static final EXPORT_RES:[Ljava/lang/Integer;


# instance fields
.field private exportFolder:Ljava/lang/String;

.field private mActivity:Landroid/app/Activity;

.field public mAdapter:Lcom/sec/android/app/ve/export/Export$Adapter;

.field private resolutionSupported:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 47
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Integer;

    const/4 v1, 0x0

    .line 48
    const/4 v2, 0x5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    .line 49
    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    .line 50
    const/4 v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 47
    sput-object v0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;->EXPORT_RES:[Ljava/lang/Integer;

    .line 50
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-object v1, p0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;->resolutionSupported:Ljava/util/List;

    .line 53
    iput-object v1, p0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;->mActivity:Landroid/app/Activity;

    .line 54
    const-string v0, "Studio"

    iput-object v0, p0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;->exportFolder:Ljava/lang/String;

    .line 55
    iput-object v1, p0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;->mAdapter:Lcom/sec/android/app/ve/export/Export$Adapter;

    .line 37
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;->mActivity:Landroid/app/Activity;

    return-object v0
.end method


# virtual methods
.method public getActivity()Landroid/app/Activity;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method public getExportFolderName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 409
    iget-object v0, p0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;->exportFolder:Ljava/lang/String;

    return-object v0
.end method

.method public getSupportedResolutionArray()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;->resolutionSupported:Ljava/util/List;

    return-object v0
.end method

.method public getSupportedResolutionCount()I
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;->resolutionSupported:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;->resolutionSupported:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 59
    iget-object v0, p0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;->resolutionSupported:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 60
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isExportRunning()Z
    .locals 1

    .prologue
    .line 81
    sget-object v0, Lcom/sec/android/app/ve/VEApp;->gExport:Lcom/sec/android/app/ve/export/Export;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/export/Export;->isExportRunning()Z

    move-result v0

    return v0
.end method

.method public onNotificationClick()V
    .locals 1

    .prologue
    .line 76
    sget-object v0, Lcom/sec/android/app/ve/VEApp;->gExport:Lcom/sec/android/app/ve/export/Export;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/export/Export;->onNotificationClick()V

    .line 77
    return-void
.end method

.method public setExportFolderName(Ljava/lang/String;)V
    .locals 0
    .param p1, "newName"    # Ljava/lang/String;

    .prologue
    .line 406
    iput-object p1, p0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;->exportFolder:Ljava/lang/String;

    .line 407
    return-void
.end method

.method public startExport(Landroid/app/Activity;Lcom/samsung/app/video/editor/external/TranscodeElement;Lcom/sec/android/app/ve/export/Export$Adapter;I)Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;
    .locals 12
    .param p1, "actvt"    # Landroid/app/Activity;
    .param p2, "trans"    # Lcom/samsung/app/video/editor/external/TranscodeElement;
    .param p3, "adp"    # Lcom/sec/android/app/ve/export/Export$Adapter;
    .param p4, "expResIndex"    # I

    .prologue
    .line 86
    sget-object v3, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;->EXPORT_START_SUCCESS:Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;

    .line 88
    .local v3, "res":Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;
    sget-object v7, Lcom/sec/android/app/ve/VEApp;->gExport:Lcom/sec/android/app/ve/export/Export;

    invoke-virtual {v7}, Lcom/sec/android/app/ve/export/Export;->isExportRunning()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 89
    sget-object v7, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;->EXPORT_ALREADY_RUNNING:Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;

    .line 135
    :goto_0
    return-object v7

    .line 92
    :cond_0
    invoke-virtual {p2}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getTotalDuration()J

    move-result-wide v4

    .line 94
    .local v4, "totalDuration":J
    sget-object v7, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;->EXPORT_RES:[Ljava/lang/Integer;

    aget-object v7, v7, p4

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-static {v7}, Lcom/samsung/app/video/editor/external/Constants;->getVideoBitRate(I)I

    move-result v7

    add-int/lit8 v7, v7, 0x40

    int-to-long v8, v7

    mul-long/2addr v8, v4

    .line 96
    const-wide/16 v10, 0x400

    .line 93
    mul-long/2addr v8, v10

    long-to-float v7, v8

    .line 98
    const/high16 v8, 0x45fa0000    # 8000.0f

    .line 93
    div-float v0, v7, v8

    .line 100
    .local v0, "expectedOutputSizeBytes":F
    new-instance v6, Ljava/io/File;

    sget-object v7, Lcom/sec/android/app/ve/common/ConfigUtils;->EXPORT_PATH:Ljava/lang/String;

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 101
    .local v6, "veDir":Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->isDirectory()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_2

    .line 102
    :cond_1
    invoke-virtual {v6}, Ljava/io/File;->mkdirs()Z

    move-result v7

    if-nez v7, :cond_2

    .line 103
    sget-object v7, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;->DIRECTORY_CREATION_FAILED:Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;

    goto :goto_0

    .line 106
    :cond_2
    sget-object v7, Lcom/sec/android/app/ve/common/ConfigUtils;->EXPORT_PATH:Ljava/lang/String;

    float-to-long v8, v0

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/ve/util/CommonUtils;->isStorageSizeAvailable(Ljava/lang/String;J)Z

    move-result v7

    if-nez v7, :cond_3

    .line 107
    sget-object v7, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;->EXPORT_NO_MEMORY:Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;

    goto :goto_0

    .line 111
    :cond_3
    sget-object v7, Lcom/sec/android/app/ve/VEApp;->gExport:Lcom/sec/android/app/ve/export/Export;

    invoke-virtual {v7, p2}, Lcom/sec/android/app/ve/export/Export;->setTranscodeElement(Lcom/samsung/app/video/editor/external/TranscodeElement;)V

    .line 112
    sget-object v7, Lcom/sec/android/app/ve/VEApp;->gExport:Lcom/sec/android/app/ve/export/Export;

    invoke-virtual {v7, p1}, Lcom/sec/android/app/ve/export/Export;->setActivity(Landroid/app/Activity;)V

    .line 113
    sget-object v7, Lcom/sec/android/app/ve/VEApp;->gExport:Lcom/sec/android/app/ve/export/Export;

    new-instance v8, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExpAdapter;

    const/4 v9, 0x0

    invoke-direct {v8, p0, v9}, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExpAdapter;-><init>(Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExpAdapter;)V

    invoke-virtual {v7, v8}, Lcom/sec/android/app/ve/export/Export;->setAdapter(Lcom/sec/android/app/ve/export/Export$Adapter;)V

    .line 114
    sget-object v7, Lcom/sec/android/app/ve/VEApp;->gExport:Lcom/sec/android/app/ve/export/Export;

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Lcom/sec/android/app/ve/export/Export;->setIsBackgroundExport(Z)V

    .line 116
    iput-object p3, p0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;->mAdapter:Lcom/sec/android/app/ve/export/Export$Adapter;

    .line 117
    iput-object p1, p0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;->mActivity:Landroid/app/Activity;

    .line 119
    sget-object v3, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;->FILENAME_NULL:Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;

    .line 121
    const v7, 0x7f070103

    invoke-static {v7}, Lcom/sec/android/app/ve/VEApp;->getStringValue(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/app/storycam/Utils;->getNextAvailableDeafultTitle(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 122
    .local v1, "exportFilename":Ljava/lang/String;
    new-instance v2, Ljava/io/File;

    new-instance v7, Ljava/lang/StringBuilder;

    sget-object v8, Lcom/sec/android/app/ve/common/ConfigUtils;->EXPORT_PATH:Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ".mp4"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v2, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 123
    .local v2, "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 124
    sget-object v7, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;->FILENAME_EXISTS:Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;

    goto/16 :goto_0

    .line 129
    :cond_4
    iget-object v7, p0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;->mActivity:Landroid/app/Activity;

    invoke-virtual {v7}, Landroid/app/Activity;->isFinishing()Z

    move-result v7

    if-nez v7, :cond_5

    .line 130
    sget-object v3, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;->EXPORT_START_SUCCESS:Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;

    .line 131
    sget-object v7, Lcom/sec/android/app/ve/VEApp;->gExport:Lcom/sec/android/app/ve/export/Export;

    .line 132
    sget-object v8, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;->EXPORT_RES:[Ljava/lang/Integer;

    aget-object v8, v8, p4

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    const/4 v9, 0x0

    const-string v10, "com.sec.android.app.storycam"

    .line 131
    invoke-virtual {v7, v1, v8, v9, v10}, Lcom/sec/android/app/ve/export/Export;->start(Ljava/lang/String;IZLjava/lang/String;)V

    :cond_5
    move-object v7, v3

    .line 135
    goto/16 :goto_0
.end method

.method public stopExport()V
    .locals 1

    .prologue
    .line 70
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/ve/common/AndroidUtils;->dismissDialog(Landroid/app/Activity;)V

    .line 71
    sget-object v0, Lcom/sec/android/app/ve/VEApp;->gExport:Lcom/sec/android/app/ve/export/Export;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/export/Export;->onExportProgressCancelled()V

    .line 72
    return-void
.end method
