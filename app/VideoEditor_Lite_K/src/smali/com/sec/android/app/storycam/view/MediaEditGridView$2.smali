.class Lcom/sec/android/app/storycam/view/MediaEditGridView$2;
.super Ljava/lang/Object;
.source "MediaEditGridView.java"

# interfaces
.implements Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MediaGridItemInterface;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/storycam/view/MediaEditGridView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/storycam/view/MediaEditGridView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView$2;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDeleteButtonClicked(I)V
    .locals 3
    .param p1, "index"    # I

    .prologue
    .line 78
    sget-object v2, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v2}, Lcom/sec/android/app/storycam/AppDataManager;->getOriginalTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v1

    .line 79
    .local v1, "trans":Lcom/samsung/app/video/editor/external/TranscodeElement;
    if-eqz v1, :cond_0

    .line 80
    invoke-virtual {v1, p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElement(I)Lcom/samsung/app/video/editor/external/Element;

    move-result-object v0

    .line 81
    .local v0, "deleteElement":Lcom/samsung/app/video/editor/external/Element;
    if-eqz v0, :cond_0

    .line 83
    sget-object v2, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v2, v0}, Lcom/sec/android/app/storycam/AppDataManager;->doLibDeinitForElement(Lcom/samsung/app/video/editor/external/Element;)V

    .line 84
    invoke-virtual {v1, v0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->removeElement(Lcom/samsung/app/video/editor/external/Element;)Ljava/util/List;

    .line 85
    iget-object v2, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView$2;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    invoke-virtual {v2, p1}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->removeViewAt(I)V

    .line 86
    # getter for: Lcom/sec/android/app/storycam/view/MediaEditGridView;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;
    invoke-static {}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->access$1()Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;->requestForOptionMenuRefresh()V

    .line 87
    const/4 v2, 0x1

    invoke-static {v2}, Lcom/sec/android/app/storycam/summary/Summary;->setAnalysisRequired(Z)V

    .line 88
    iget-object v2, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView$2;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    # invokes: Lcom/sec/android/app/storycam/view/MediaEditGridView;->refreshAllViews()V
    invoke-static {v2}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->access$0(Lcom/sec/android/app/storycam/view/MediaEditGridView;)V

    .line 89
    iget-object v2, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView$2;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    invoke-virtual {v2, p1}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->requestFocus()Z

    .line 92
    .end local v0    # "deleteElement":Lcom/samsung/app/video/editor/external/Element;
    :cond_0
    return-void
.end method
