.class public Lcom/sec/android/app/ve/theme/Download;
.super Ljava/lang/Object;
.source "Download.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/ve/theme/Download$Adapter;
    }
.end annotation


# instance fields
.field private mActivity:Landroid/app/Activity;

.field private mAdapter:Lcom/sec/android/app/ve/theme/Download$Adapter;

.field private final mBTReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    new-instance v0, Lcom/sec/android/app/ve/theme/Download$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/ve/theme/Download$1;-><init>(Lcom/sec/android/app/ve/theme/Download;)V

    iput-object v0, p0, Lcom/sec/android/app/ve/theme/Download;->mBTReceiver:Landroid/content/BroadcastReceiver;

    .line 12
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/app/ve/theme/Download;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/sec/android/app/ve/theme/Download;->getThemePackage()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/app/ve/theme/Download;)Lcom/sec/android/app/ve/theme/Download$Adapter;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/sec/android/app/ve/theme/Download;->mAdapter:Lcom/sec/android/app/ve/theme/Download$Adapter;

    return-object v0
.end method

.method private getThemePackage()Ljava/lang/String;
    .locals 2

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/ve/theme/Download;->mAdapter:Lcom/sec/android/app/ve/theme/Download$Adapter;

    if-nez v0, :cond_0

    .line 64
    const-string v0, ""

    .line 66
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/sec/android/app/ve/VEApp;->gThemeMgr:Lcom/sec/android/app/ve/theme/ThemeManager;

    iget-object v1, p0, Lcom/sec/android/app/ve/theme/Download;->mAdapter:Lcom/sec/android/app/ve/theme/Download$Adapter;

    invoke-virtual {v1}, Lcom/sec/android/app/ve/theme/Download$Adapter;->getThemePosition()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/ve/theme/ThemeManager;->getThemePackage(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private registerForBroadcast(Landroid/content/BroadcastReceiver;)V
    .locals 2
    .param p1, "btrec"    # Landroid/content/BroadcastReceiver;

    .prologue
    .line 51
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 52
    .local v0, "intFilt":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 53
    const-string v1, "android.intent.action.PACKAGE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 54
    const-string v1, "android.intent.action.PACKAGE_REPLACED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 55
    const-string v1, "package"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 57
    iget-object v1, p0, Lcom/sec/android/app/ve/theme/Download;->mActivity:Landroid/app/Activity;

    if-eqz v1, :cond_0

    .line 58
    iget-object v1, p0, Lcom/sec/android/app/ve/theme/Download;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1, p1, v0}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 59
    :cond_0
    return-void
.end method


# virtual methods
.method public setAdapter(Lcom/sec/android/app/ve/theme/Download$Adapter;)V
    .locals 0
    .param p1, "adap"    # Lcom/sec/android/app/ve/theme/Download$Adapter;

    .prologue
    .line 46
    iput-object p1, p0, Lcom/sec/android/app/ve/theme/Download;->mAdapter:Lcom/sec/android/app/ve/theme/Download$Adapter;

    .line 47
    return-void
.end method

.method public startDownload()V
    .locals 4

    .prologue
    .line 20
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 21
    .local v1, "intent":Landroid/content/Intent;
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "samsungapps://ProductDetail/"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/sec/android/app/ve/theme/Download;->getThemePackage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 22
    const v2, 0x14000020

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 23
    invoke-static {v1}, Lcom/sec/android/app/ve/VEApp;->startActivity(Landroid/content/Intent;)V

    .line 25
    iget-object v2, p0, Lcom/sec/android/app/ve/theme/Download;->mActivity:Landroid/app/Activity;

    if-eqz v2, :cond_0

    .line 27
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/ve/theme/Download;->mActivity:Landroid/app/Activity;

    iget-object v3, p0, Lcom/sec/android/app/ve/theme/Download;->mBTReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 33
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/ve/theme/Download;->mAdapter:Lcom/sec/android/app/ve/theme/Download$Adapter;

    if-nez v2, :cond_1

    .line 41
    :goto_1
    return-void

    .line 28
    :catch_0
    move-exception v0

    .line 29
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 36
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/ve/theme/Download;->mAdapter:Lcom/sec/android/app/ve/theme/Download$Adapter;

    invoke-virtual {v2}, Lcom/sec/android/app/ve/theme/Download$Adapter;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/ve/theme/Download;->mActivity:Landroid/app/Activity;

    .line 37
    iget-object v2, p0, Lcom/sec/android/app/ve/theme/Download;->mBTReceiver:Landroid/content/BroadcastReceiver;

    invoke-direct {p0, v2}, Lcom/sec/android/app/ve/theme/Download;->registerForBroadcast(Landroid/content/BroadcastReceiver;)V

    goto :goto_1
.end method
