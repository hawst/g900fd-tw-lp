.class public Lcom/sec/android/app/ve/data/VolumesDAO;
.super Ljava/lang/Object;
.source "VolumesDAO.java"


# instance fields
.field private backgroundMusicProgress:I

.field private recordedAudioProgress:I

.field private videoProgress:I


# direct methods
.method public constructor <init>(III)V
    .locals 0
    .param p1, "videoProgress"    # I
    .param p2, "recordedAudioProgress"    # I
    .param p3, "backgroundMusicProgress"    # I

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput p1, p0, Lcom/sec/android/app/ve/data/VolumesDAO;->videoProgress:I

    .line 13
    iput p2, p0, Lcom/sec/android/app/ve/data/VolumesDAO;->recordedAudioProgress:I

    .line 14
    iput p3, p0, Lcom/sec/android/app/ve/data/VolumesDAO;->backgroundMusicProgress:I

    .line 15
    return-void
.end method


# virtual methods
.method public getBackgroundMusicProgress()I
    .locals 1

    .prologue
    .line 30
    iget v0, p0, Lcom/sec/android/app/ve/data/VolumesDAO;->backgroundMusicProgress:I

    return v0
.end method

.method public getRecordedAudioProgress()I
    .locals 1

    .prologue
    .line 24
    iget v0, p0, Lcom/sec/android/app/ve/data/VolumesDAO;->recordedAudioProgress:I

    return v0
.end method

.method public getVideoProgress()I
    .locals 1

    .prologue
    .line 18
    iget v0, p0, Lcom/sec/android/app/ve/data/VolumesDAO;->videoProgress:I

    return v0
.end method

.method public setBackgroundMusicProgress(I)V
    .locals 0
    .param p1, "backgroundMusicProgress"    # I

    .prologue
    .line 33
    iput p1, p0, Lcom/sec/android/app/ve/data/VolumesDAO;->backgroundMusicProgress:I

    .line 34
    return-void
.end method

.method public setRecordedAudioProgress(I)V
    .locals 0
    .param p1, "recordedAudioProgress"    # I

    .prologue
    .line 27
    iput p1, p0, Lcom/sec/android/app/ve/data/VolumesDAO;->recordedAudioProgress:I

    .line 28
    return-void
.end method

.method public setVideoProgress(I)V
    .locals 0
    .param p1, "videoProgress"    # I

    .prologue
    .line 21
    iput p1, p0, Lcom/sec/android/app/ve/data/VolumesDAO;->videoProgress:I

    .line 22
    return-void
.end method
