.class public Lcom/sec/android/app/storycam/AppDataManager;
.super Ljava/lang/Object;
.source "AppDataManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;,
        Lcom/sec/android/app/storycam/AppDataManager$MediaFile;,
        Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;,
        Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheUpdateListener;,
        Lcom/sec/android/app/storycam/AppDataManager$TranscodeElementChangeListener;
    }
.end annotation


# instance fields
.field private isUHDContentInTranscode:Z

.field private mInitialTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

.field private mLibraryInitDeinitFacade:Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade;

.field private mOriginalTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

.field private mOutputDuration:J

.field private mSummarizedTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

.field private mTaskPool:Lcom/sec/android/app/ve/thread/AsyncTaskPool;

.field private mThumbnailCache:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;",
            ">;"
        }
    .end annotation
.end field

.field private final mThumbnailCacheLock:Ljava/lang/Object;

.field private mThumbnailCacheUpdateListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheUpdateListener;",
            ">;"
        }
    .end annotation
.end field

.field private mThumbnailCbk:Lcom/sec/android/app/ve/thread/VideoImageThumbFetcher$VideoImageThumbnaiFetcherCallbak;

.field private mTranscodeElementChangedListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/storycam/AppDataManager$TranscodeElementChangeListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object v2, p0, Lcom/sec/android/app/storycam/AppDataManager;->mSummarizedTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    .line 44
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/storycam/AppDataManager;->mThumbnailCacheLock:Ljava/lang/Object;

    .line 47
    const-wide/16 v0, 0x7530

    iput-wide v0, p0, Lcom/sec/android/app/storycam/AppDataManager;->mOutputDuration:J

    .line 49
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/storycam/AppDataManager;->isUHDContentInTranscode:Z

    .line 54
    iput-object v2, p0, Lcom/sec/android/app/storycam/AppDataManager;->mInitialTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    .line 58
    new-instance v0, Lcom/sec/android/app/storycam/AppDataManager$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/AppDataManager$1;-><init>(Lcom/sec/android/app/storycam/AppDataManager;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/AppDataManager;->mThumbnailCbk:Lcom/sec/android/app/ve/thread/VideoImageThumbFetcher$VideoImageThumbnaiFetcherCallbak;

    .line 111
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/app/storycam/AppDataManager;)Ljava/util/LinkedHashMap;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/app/storycam/AppDataManager;->mThumbnailCache:Ljava/util/LinkedHashMap;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/app/storycam/AppDataManager;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/storycam/AppDataManager;->mThumbnailCacheLock:Ljava/lang/Object;

    return-object v0
.end method

.method private getFilePathsFromUri(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 281
    .local p1, "mediaUriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 282
    .local v6, "mediaFileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v3, 0x0

    .line 284
    .local v3, "lEditPath":Ljava/lang/String;
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 285
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v0, :cond_0

    .line 330
    return-object v6

    .line 287
    :cond_0
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/net/Uri;

    .line 289
    .local v4, "lEditUri":Landroid/net/Uri;
    if-eqz v4, :cond_4

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    if-lez v7, :cond_4

    .line 290
    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    .line 293
    .local v5, "lFileName":Ljava/lang/String;
    sget-object v7, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 294
    invoke-virtual {v7}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    .line 293
    invoke-virtual {v5, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    .line 294
    if-nez v7, :cond_1

    .line 296
    sget-object v7, Landroid/provider/MediaStore$Video$Media;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 297
    invoke-virtual {v7}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    .line 296
    invoke-virtual {v5, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    .line 297
    if-eqz v7, :cond_5

    .line 298
    :cond_1
    invoke-static {v4}, Lcom/sec/android/app/ve/common/MediaUtils;->getVideoFileInfoByUri(Landroid/net/Uri;)Landroid/database/Cursor;

    move-result-object v2

    .line 300
    .local v2, "lCursor":Landroid/database/Cursor;
    if-eqz v2, :cond_3

    .line 302
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v7

    if-lez v7, :cond_2

    .line 304
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    .line 306
    const-string v7, "_data"

    invoke-interface {v2, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    .line 305
    invoke-interface {v2, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 308
    :cond_2
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 324
    .end local v2    # "lCursor":Landroid/database/Cursor;
    :cond_3
    :goto_1
    if-eqz v3, :cond_4

    .line 325
    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 285
    .end local v5    # "lFileName":Ljava/lang/String;
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 311
    .restart local v5    # "lFileName":Ljava/lang/String;
    :cond_5
    sget-object v7, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 312
    invoke-virtual {v7}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    .line 311
    invoke-virtual {v5, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    .line 312
    if-nez v7, :cond_6

    .line 314
    sget-object v7, Landroid/provider/MediaStore$Images$Media;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 315
    invoke-virtual {v7}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    .line 314
    invoke-virtual {v5, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    .line 315
    if-eqz v7, :cond_7

    .line 316
    :cond_6
    invoke-static {v4}, Lcom/sec/android/app/ve/common/MediaUtils;->getImageFilePathByUri(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v3

    .line 317
    goto :goto_1

    :cond_7
    const-string v7, "content://mms/part"

    invoke-virtual {v5, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 318
    invoke-static {v4}, Lcom/sec/android/app/ve/pm/ProjectManager;->getMMSImagePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v3

    .line 319
    goto :goto_1

    :cond_8
    const-string v7, "file://"

    invoke-virtual {v5, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 320
    invoke-virtual {v4}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    .line 321
    goto :goto_1

    .line 322
    :cond_9
    move-object v3, v5

    goto :goto_1
.end method

.method private validateVideoEditIntent(Landroid/content/Intent;Ljava/util/ArrayList;Ljava/lang/String;)Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;
    .locals 14
    .param p1, "aIntent"    # Landroid/content/Intent;
    .param p3, "reuestString"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/storycam/AppDataManager$MediaFile;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;"
        }
    .end annotation

    .prologue
    .line 200
    .local p2, "mediaFiles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/storycam/AppDataManager$MediaFile;>;"
    if-nez p1, :cond_1

    .line 201
    sget-object v3, Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;->NONE_SUPPORTED:Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;

    .line 277
    :cond_0
    :goto_0
    return-object v3

    .line 205
    :cond_1
    move-object/from16 v0, p3

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v7

    .line 207
    .local v7, "mediaList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    if-nez v7, :cond_2

    .line 209
    new-instance v7, Ljava/util/ArrayList;

    .end local v7    # "mediaList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 210
    .restart local v7    # "mediaList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v8

    .line 211
    .local v8, "mediaUri":Landroid/net/Uri;
    if-eqz v8, :cond_2

    .line 212
    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 216
    .end local v8    # "mediaUri":Landroid/net/Uri;
    :cond_2
    invoke-direct {p0, v7}, Lcom/sec/android/app/storycam/AppDataManager;->getFilePathsFromUri(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v6

    .line 217
    .local v6, "mediaFilesPath":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v2, 0x1

    .line 218
    .local v2, "allMediaSupported":Z
    const/4 v1, 0x1

    .line 220
    .local v1, "allFilesUnsupported":Z
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-nez v10, :cond_3

    .line 221
    sget-object v3, Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;->NONE_SUPPORTED:Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;

    goto :goto_0

    .line 226
    :cond_3
    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-nez v11, :cond_4

    .line 266
    sget-object v3, Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;->ALL_FILES_SUPPORTED:Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;

    .line 267
    .local v3, "error":Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;
    if-eqz v1, :cond_8

    .line 269
    sget-object v3, Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;->NONE_SUPPORTED:Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;

    .line 270
    goto :goto_0

    .line 226
    .end local v3    # "error":Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;
    :cond_4
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 239
    .local v4, "lFile":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v11

    invoke-virtual {v11, v4}, Lcom/samsung/app/video/editor/external/NativeInterface;->isJPG(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 240
    new-instance v5, Lcom/sec/android/app/storycam/AppDataManager$MediaFile;

    const/4 v11, 0x2

    const/4 v12, 0x1

    const/4 v13, 0x0

    invoke-direct {v5, v4, v11, v12, v13}, Lcom/sec/android/app/storycam/AppDataManager$MediaFile;-><init>(Ljava/lang/String;IZLcom/sec/android/app/storycam/AppDataManager$MediaFile;)V

    .line 241
    .local v5, "mediaFile":Lcom/sec/android/app/storycam/AppDataManager$MediaFile;
    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 242
    const/4 v1, 0x0

    .line 243
    goto :goto_1

    .line 245
    .end local v5    # "mediaFile":Lcom/sec/android/app/storycam/AppDataManager$MediaFile;
    :cond_5
    const/4 v11, 0x0

    invoke-static {v4, v11}, Lcom/sec/android/app/ve/common/MediaUtils;->checkIfMediaSupported(Ljava/lang/String;Z)I

    move-result v9

    .line 246
    .local v9, "returnVal":I
    sget v11, Lcom/sec/android/app/ve/common/MediaUtils$FILE_CHECK_STATUS;->VALID_FILE:I

    if-ne v9, v11, :cond_6

    .line 248
    const-string v11, "Video is supported for Editing by VideoMaker"

    invoke-static {v11}, Lcom/sec/android/app/ve/common/LogUtils;->log(Ljava/lang/String;)V

    .line 249
    new-instance v5, Lcom/sec/android/app/storycam/AppDataManager$MediaFile;

    const/4 v11, 0x1

    const/4 v12, 0x1

    const/4 v13, 0x0

    invoke-direct {v5, v4, v11, v12, v13}, Lcom/sec/android/app/storycam/AppDataManager$MediaFile;-><init>(Ljava/lang/String;IZLcom/sec/android/app/storycam/AppDataManager$MediaFile;)V

    .line 250
    .restart local v5    # "mediaFile":Lcom/sec/android/app/storycam/AppDataManager$MediaFile;
    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 251
    const/4 v1, 0x0

    .line 252
    goto :goto_1

    .line 254
    .end local v5    # "mediaFile":Lcom/sec/android/app/storycam/AppDataManager$MediaFile;
    :cond_6
    invoke-static {v4}, Lcom/sec/android/app/storycam/Utils;->isImage(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 255
    new-instance v5, Lcom/sec/android/app/storycam/AppDataManager$MediaFile;

    const/4 v11, 0x2

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-direct {v5, v4, v11, v12, v13}, Lcom/sec/android/app/storycam/AppDataManager$MediaFile;-><init>(Ljava/lang/String;IZLcom/sec/android/app/storycam/AppDataManager$MediaFile;)V

    .line 258
    .restart local v5    # "mediaFile":Lcom/sec/android/app/storycam/AppDataManager$MediaFile;
    :goto_2
    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 260
    const/4 v2, 0x0

    goto :goto_1

    .line 257
    .end local v5    # "mediaFile":Lcom/sec/android/app/storycam/AppDataManager$MediaFile;
    :cond_7
    new-instance v5, Lcom/sec/android/app/storycam/AppDataManager$MediaFile;

    const/4 v11, 0x1

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-direct {v5, v4, v11, v12, v13}, Lcom/sec/android/app/storycam/AppDataManager$MediaFile;-><init>(Ljava/lang/String;IZLcom/sec/android/app/storycam/AppDataManager$MediaFile;)V

    .restart local v5    # "mediaFile":Lcom/sec/android/app/storycam/AppDataManager$MediaFile;
    goto :goto_2

    .line 272
    .end local v4    # "lFile":Ljava/lang/String;
    .end local v5    # "mediaFile":Lcom/sec/android/app/storycam/AppDataManager$MediaFile;
    .end local v9    # "returnVal":I
    .restart local v3    # "error":Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;
    :cond_8
    if-nez v2, :cond_0

    .line 274
    sget-object v3, Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;->SOME_FILES_SUPPORTED:Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;

    goto/16 :goto_0
.end method


# virtual methods
.method public addThumbnailCacheUpdateListener(Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheUpdateListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheUpdateListener;

    .prologue
    .line 570
    iget-object v0, p0, Lcom/sec/android/app/storycam/AppDataManager;->mThumbnailCacheUpdateListeners:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 571
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/storycam/AppDataManager;->mThumbnailCacheUpdateListeners:Ljava/util/ArrayList;

    .line 573
    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/storycam/AppDataManager;->mThumbnailCacheUpdateListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 574
    iget-object v0, p0, Lcom/sec/android/app/storycam/AppDataManager;->mThumbnailCacheUpdateListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 575
    :cond_1
    return-void
.end method

.method public addTranscodeElementChangeListener(Lcom/sec/android/app/storycam/AppDataManager$TranscodeElementChangeListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/sec/android/app/storycam/AppDataManager$TranscodeElementChangeListener;

    .prologue
    .line 375
    iget-object v0, p0, Lcom/sec/android/app/storycam/AppDataManager;->mTranscodeElementChangedListeners:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 376
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/storycam/AppDataManager;->mTranscodeElementChangedListeners:Ljava/util/ArrayList;

    .line 378
    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/storycam/AppDataManager;->mTranscodeElementChangedListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 379
    iget-object v0, p0, Lcom/sec/android/app/storycam/AppDataManager;->mTranscodeElementChangedListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 380
    :cond_1
    return-void
.end method

.method public are2UHDFilesInTransElement(Lcom/samsung/app/video/editor/external/TranscodeElement;)Z
    .locals 7
    .param p1, "tElement"    # Lcom/samsung/app/video/editor/external/TranscodeElement;

    .prologue
    .line 422
    const/4 v1, 0x0

    .line 423
    .local v1, "are2UHDContentInTranscode":Z
    const/4 v0, 0x0

    .line 424
    .local v0, "UHDCount":I
    const/4 v3, 0x0

    .line 425
    .local v3, "element":Lcom/samsung/app/video/editor/external/Element;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v6

    if-lt v4, v6, :cond_1

    .line 441
    const/4 v6, 0x2

    if-lt v0, v6, :cond_0

    .line 442
    const/4 v1, 0x1

    .line 444
    :cond_0
    return v1

    .line 426
    :cond_1
    new-instance v5, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v5}, Landroid/media/MediaMetadataRetriever;-><init>()V

    .line 427
    .local v5, "retriever":Landroid/media/MediaMetadataRetriever;
    invoke-virtual {p1, v4}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElement(I)Lcom/samsung/app/video/editor/external/Element;

    move-result-object v3

    .line 429
    :try_start_0
    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/Element;->getFilePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/lang/String;)V

    .line 430
    invoke-static {v5}, Lcom/sec/android/app/ve/util/CommonUtils;->isUHD(Landroid/media/MediaMetadataRetriever;)Z

    move-result v6

    if-nez v6, :cond_2

    invoke-static {v5}, Lcom/sec/android/app/ve/util/CommonUtils;->isWQHD(Landroid/media/MediaMetadataRetriever;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v6

    if-eqz v6, :cond_3

    .line 431
    :cond_2
    add-int/lit8 v0, v0, 0x1

    .line 438
    :cond_3
    :goto_1
    if-eqz v5, :cond_4

    .line 439
    invoke-virtual {v5}, Landroid/media/MediaMetadataRetriever;->release()V

    .line 425
    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 434
    :catch_0
    move-exception v2

    .line 436
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public clearAllImageCachesInEngine()V
    .locals 1

    .prologue
    .line 478
    iget-object v0, p0, Lcom/sec/android/app/storycam/AppDataManager;->mLibraryInitDeinitFacade:Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade;

    if-eqz v0, :cond_0

    .line 479
    iget-object v0, p0, Lcom/sec/android/app/storycam/AppDataManager;->mLibraryInitDeinitFacade:Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade;->destroy()V

    .line 481
    :cond_0
    return-void
.end method

.method public doLibDeinitForElement(Lcom/samsung/app/video/editor/external/Element;)V
    .locals 2
    .param p1, "element"    # Lcom/samsung/app/video/editor/external/Element;

    .prologue
    .line 473
    iget-object v0, p0, Lcom/sec/android/app/storycam/AppDataManager;->mLibraryInitDeinitFacade:Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade;

    if-eqz v0, :cond_0

    .line 474
    iget-object v0, p0, Lcom/sec/android/app/storycam/AppDataManager;->mLibraryInitDeinitFacade:Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade;

    iget-object v1, p0, Lcom/sec/android/app/storycam/AppDataManager;->mOriginalTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade;->elementDeInit(Lcom/samsung/app/video/editor/external/Element;Lcom/samsung/app/video/editor/external/TranscodeElement;)V

    .line 476
    :cond_0
    return-void
.end method

.method public doLibInitForElement(Lcom/samsung/app/video/editor/external/Element;)V
    .locals 1
    .param p1, "element"    # Lcom/samsung/app/video/editor/external/Element;

    .prologue
    .line 466
    iget-object v0, p0, Lcom/sec/android/app/storycam/AppDataManager;->mLibraryInitDeinitFacade:Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade;

    if-nez v0, :cond_0

    .line 467
    new-instance v0, Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade;

    invoke-direct {v0}, Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/storycam/AppDataManager;->mLibraryInitDeinitFacade:Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade;

    .line 469
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/storycam/AppDataManager;->mLibraryInitDeinitFacade:Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade;->elementInit(Lcom/samsung/app/video/editor/external/Element;)V

    .line 470
    return-void
.end method

.method public getCurrentTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;
    .locals 1

    .prologue
    .line 335
    iget-object v0, p0, Lcom/sec/android/app/storycam/AppDataManager;->mSummarizedTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    return-object v0
.end method

.method public getInitialTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;
    .locals 1

    .prologue
    .line 338
    iget-object v0, p0, Lcom/sec/android/app/storycam/AppDataManager;->mInitialTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    return-object v0
.end method

.method public getOriginalTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;
    .locals 1

    .prologue
    .line 362
    iget-object v0, p0, Lcom/sec/android/app/storycam/AppDataManager;->mOriginalTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    return-object v0
.end method

.method public getOutputDuration()J
    .locals 2

    .prologue
    .line 629
    iget-wide v0, p0, Lcom/sec/android/app/storycam/AppDataManager;->mOutputDuration:J

    return-wide v0
.end method

.method public getThumbnail(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 4
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 592
    const/4 v0, 0x0

    .line 593
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    iget-object v2, p0, Lcom/sec/android/app/storycam/AppDataManager;->mThumbnailCache:Ljava/util/LinkedHashMap;

    if-eqz v2, :cond_1

    .line 594
    iget-object v3, p0, Lcom/sec/android/app/storycam/AppDataManager;->mThumbnailCacheLock:Ljava/lang/Object;

    monitor-enter v3

    .line 595
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/storycam/AppDataManager;->mThumbnailCache:Ljava/util/LinkedHashMap;

    invoke-virtual {v2, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;

    .line 596
    .local v1, "item":Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;
    if-eqz v1, :cond_0

    .line 597
    # getter for: Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;->mBitmap:Landroid/graphics/Bitmap;
    invoke-static {v1}, Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;->access$1(Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 594
    :cond_0
    monitor-exit v3

    .line 600
    .end local v1    # "item":Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;
    :cond_1
    return-object v0

    .line 594
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public getUHDCheckFlag()Z
    .locals 1

    .prologue
    .line 448
    iget-boolean v0, p0, Lcom/sec/android/app/storycam/AppDataManager;->isUHDContentInTranscode:Z

    return v0
.end method

.method public notifyThumbnailCacheUpdated()V
    .locals 3

    .prologue
    .line 584
    iget-object v1, p0, Lcom/sec/android/app/storycam/AppDataManager;->mThumbnailCacheUpdateListeners:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 585
    iget-object v1, p0, Lcom/sec/android/app/storycam/AppDataManager;->mThumbnailCacheUpdateListeners:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 589
    :cond_0
    return-void

    .line 585
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheUpdateListener;

    .line 586
    .local v0, "callback":Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheUpdateListener;
    invoke-interface {v0}, Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheUpdateListener;->onThumbnailCacheUpdated()V

    goto :goto_0
.end method

.method public notifyTranscodeElementChanges()V
    .locals 3

    .prologue
    .line 389
    iget-object v1, p0, Lcom/sec/android/app/storycam/AppDataManager;->mTranscodeElementChangedListeners:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 390
    iget-object v1, p0, Lcom/sec/android/app/storycam/AppDataManager;->mTranscodeElementChangedListeners:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 394
    :cond_0
    return-void

    .line 390
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/storycam/AppDataManager$TranscodeElementChangeListener;

    .line 391
    .local v0, "callback":Lcom/sec/android/app/storycam/AppDataManager$TranscodeElementChangeListener;
    iget-object v2, p0, Lcom/sec/android/app/storycam/AppDataManager;->mSummarizedTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    invoke-interface {v0, v2}, Lcom/sec/android/app/storycam/AppDataManager$TranscodeElementChangeListener;->onTranscodeElementChanged(Lcom/samsung/app/video/editor/external/TranscodeElement;)V

    goto :goto_0
.end method

.method public prepareEngineForPlayback()V
    .locals 6

    .prologue
    .line 451
    iget-object v4, p0, Lcom/sec/android/app/storycam/AppDataManager;->mOriginalTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    if-eqz v4, :cond_0

    .line 452
    iget-object v4, p0, Lcom/sec/android/app/storycam/AppDataManager;->mOriginalTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementList()Ljava/util/List;

    move-result-object v1

    .line 453
    .local v1, "eleList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    if-eqz v1, :cond_0

    .line 454
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    .line 455
    .local v0, "count":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-lt v3, v0, :cond_1

    .line 463
    .end local v0    # "count":I
    .end local v1    # "eleList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    .end local v3    # "i":I
    :cond_0
    return-void

    .line 456
    .restart local v0    # "count":I
    .restart local v1    # "eleList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    .restart local v3    # "i":I
    :cond_1
    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/app/video/editor/external/Element;

    .line 457
    .local v2, "element":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/Element;->getType()I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_2

    .line 458
    invoke-virtual {p0, v2}, Lcom/sec/android/app/storycam/AppDataManager;->doLibInitForElement(Lcom/samsung/app/video/editor/external/Element;)V

    .line 455
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public recheckFilesInTranscodeElement(Lcom/samsung/app/video/editor/external/TranscodeElement;Ljava/lang/String;)Z
    .locals 7
    .param p1, "tElement"    # Lcom/samsung/app/video/editor/external/TranscodeElement;
    .param p2, "fPath"    # Ljava/lang/String;

    .prologue
    .line 604
    const/4 v4, 0x0

    .line 605
    .local v4, "elmntsRemoved":Z
    if-eqz p1, :cond_0

    .line 606
    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementList()Ljava/util/List;

    move-result-object v3

    .line 607
    .local v3, "elementList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    .line 609
    .local v0, "count":I
    add-int/lit8 v5, v0, -0x1

    .local v5, "i":I
    :goto_0
    if-gez v5, :cond_1

    .line 621
    .end local v0    # "count":I
    .end local v3    # "elementList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    .end local v5    # "i":I
    :cond_0
    return v4

    .line 610
    .restart local v0    # "count":I
    .restart local v3    # "elementList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    .restart local v5    # "i":I
    :cond_1
    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/app/video/editor/external/Element;

    .line 611
    .local v2, "element":Lcom/samsung/app/video/editor/external/Element;
    if-eqz v2, :cond_3

    .line 612
    new-instance v1, Ljava/io/File;

    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/Element;->getFilePath()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v1, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 613
    .local v1, "eleFile":Ljava/io/File;
    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/Element;->isAssetResource()Z

    move-result v6

    if-nez v6, :cond_3

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {v1}, Ljava/io/File;->isFile()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/Element;->getFilePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 614
    :cond_2
    invoke-virtual {p1, v2}, Lcom/samsung/app/video/editor/external/TranscodeElement;->removeElement(Lcom/samsung/app/video/editor/external/Element;)Ljava/util/List;

    .line 615
    const/4 v4, 0x1

    .line 609
    .end local v1    # "eleFile":Ljava/io/File;
    :cond_3
    add-int/lit8 v5, v5, -0x1

    goto :goto_0
.end method

.method public recycle(Z)V
    .locals 10
    .param p1, "clearListenersAndExit"    # Z

    .prologue
    const/4 v6, 0x0

    .line 519
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v7

    if-eqz v7, :cond_0

    if-eqz p1, :cond_0

    .line 520
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/app/video/editor/external/NativeInterface;->EditActivityExit()V

    .line 522
    :cond_0
    iget-object v7, p0, Lcom/sec/android/app/storycam/AppDataManager;->mTaskPool:Lcom/sec/android/app/ve/thread/AsyncTaskPool;

    if-eqz v7, :cond_1

    .line 523
    iget-object v7, p0, Lcom/sec/android/app/storycam/AppDataManager;->mTaskPool:Lcom/sec/android/app/ve/thread/AsyncTaskPool;

    invoke-virtual {v7}, Lcom/sec/android/app/ve/thread/AsyncTaskPool;->clearOrCancelTasks()V

    .line 524
    :cond_1
    iget-object v7, p0, Lcom/sec/android/app/storycam/AppDataManager;->mThumbnailCache:Ljava/util/LinkedHashMap;

    if-eqz v7, :cond_2

    .line 525
    iget-object v7, p0, Lcom/sec/android/app/storycam/AppDataManager;->mThumbnailCacheLock:Ljava/lang/Object;

    monitor-enter v7

    .line 526
    :try_start_0
    iget-object v8, p0, Lcom/sec/android/app/storycam/AppDataManager;->mThumbnailCache:Ljava/util/LinkedHashMap;

    invoke-virtual {v8}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v4

    .line 527
    .local v4, "lKeySet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v4}, Ljava/util/Set;->size()I

    move-result v8

    new-array v5, v8, [Ljava/lang/String;

    .line 528
    .local v5, "lKeys":[Ljava/lang/String;
    invoke-interface {v4, v5}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 529
    iget-object v8, p0, Lcom/sec/android/app/storycam/AppDataManager;->mThumbnailCache:Ljava/util/LinkedHashMap;

    invoke-virtual {v8}, Ljava/util/LinkedHashMap;->size()I

    move-result v1

    .line 530
    .local v1, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-lt v2, v1, :cond_8

    .line 537
    iget-object v8, p0, Lcom/sec/android/app/storycam/AppDataManager;->mThumbnailCache:Ljava/util/LinkedHashMap;

    invoke-virtual {v8}, Ljava/util/LinkedHashMap;->clear()V

    .line 538
    const/4 v8, 0x0

    iput-object v8, p0, Lcom/sec/android/app/storycam/AppDataManager;->mThumbnailCache:Ljava/util/LinkedHashMap;

    .line 525
    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 541
    .end local v1    # "count":I
    .end local v2    # "i":I
    .end local v4    # "lKeySet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v5    # "lKeys":[Ljava/lang/String;
    :cond_2
    iget-object v7, p0, Lcom/sec/android/app/storycam/AppDataManager;->mSummarizedTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    if-eqz v7, :cond_3

    .line 542
    iget-object v7, p0, Lcom/sec/android/app/storycam/AppDataManager;->mSummarizedTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    invoke-virtual {v7}, Lcom/samsung/app/video/editor/external/TranscodeElement;->deleteAllClipArts()V

    .line 543
    :cond_3
    iput-object v6, p0, Lcom/sec/android/app/storycam/AppDataManager;->mSummarizedTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    .line 545
    iget-object v7, p0, Lcom/sec/android/app/storycam/AppDataManager;->mOriginalTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    if-eqz v7, :cond_4

    .line 546
    iget-object v7, p0, Lcom/sec/android/app/storycam/AppDataManager;->mOriginalTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    invoke-virtual {v7}, Lcom/samsung/app/video/editor/external/TranscodeElement;->deleteAllClipArts()V

    .line 547
    :cond_4
    iput-object v6, p0, Lcom/sec/android/app/storycam/AppDataManager;->mOriginalTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    .line 549
    iget-object v7, p0, Lcom/sec/android/app/storycam/AppDataManager;->mInitialTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    if-eqz v7, :cond_5

    .line 550
    iget-object v7, p0, Lcom/sec/android/app/storycam/AppDataManager;->mInitialTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    invoke-virtual {v7}, Lcom/samsung/app/video/editor/external/TranscodeElement;->deleteAllClipArts()V

    .line 551
    :cond_5
    iput-object v6, p0, Lcom/sec/android/app/storycam/AppDataManager;->mInitialTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    .line 553
    iput-object v6, p0, Lcom/sec/android/app/storycam/AppDataManager;->mTaskPool:Lcom/sec/android/app/ve/thread/AsyncTaskPool;

    .line 555
    if-eqz p1, :cond_7

    .line 556
    iget-object v7, p0, Lcom/sec/android/app/storycam/AppDataManager;->mTranscodeElementChangedListeners:Ljava/util/ArrayList;

    if-eqz v7, :cond_6

    .line 557
    iget-object v7, p0, Lcom/sec/android/app/storycam/AppDataManager;->mTranscodeElementChangedListeners:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->clear()V

    .line 558
    iput-object v6, p0, Lcom/sec/android/app/storycam/AppDataManager;->mTranscodeElementChangedListeners:Ljava/util/ArrayList;

    .line 561
    :cond_6
    iget-object v7, p0, Lcom/sec/android/app/storycam/AppDataManager;->mThumbnailCacheUpdateListeners:Ljava/util/ArrayList;

    if-eqz v7, :cond_7

    .line 562
    iget-object v7, p0, Lcom/sec/android/app/storycam/AppDataManager;->mThumbnailCacheUpdateListeners:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->clear()V

    .line 563
    iput-object v6, p0, Lcom/sec/android/app/storycam/AppDataManager;->mThumbnailCacheUpdateListeners:Ljava/util/ArrayList;

    .line 566
    :cond_7
    const-wide/16 v6, 0x7530

    iput-wide v6, p0, Lcom/sec/android/app/storycam/AppDataManager;->mOutputDuration:J

    .line 567
    return-void

    .line 532
    .restart local v1    # "count":I
    .restart local v2    # "i":I
    .restart local v4    # "lKeySet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .restart local v5    # "lKeys":[Ljava/lang/String;
    :cond_8
    :try_start_1
    iget-object v8, p0, Lcom/sec/android/app/storycam/AppDataManager;->mThumbnailCache:Ljava/util/LinkedHashMap;

    aget-object v9, v5, v2

    invoke-virtual {v8, v9}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;

    .line 533
    .local v3, "item":Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;
    if-eqz v3, :cond_a

    # getter for: Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;->mBitmap:Landroid/graphics/Bitmap;
    invoke-static {v3}, Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;->access$1(Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 534
    .local v0, "bmp":Landroid/graphics/Bitmap;
    :goto_1
    if-eqz v0, :cond_9

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v8

    if-nez v8, :cond_9

    .line 535
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 530
    :cond_9
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .end local v0    # "bmp":Landroid/graphics/Bitmap;
    :cond_a
    move-object v0, v6

    .line 533
    goto :goto_1

    .line 525
    .end local v1    # "count":I
    .end local v2    # "i":I
    .end local v3    # "item":Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;
    .end local v4    # "lKeySet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v5    # "lKeys":[Ljava/lang/String;
    :catchall_0
    move-exception v6

    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v6
.end method

.method public removeThumbnailCacheUpdateListener(Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheUpdateListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheUpdateListener;

    .prologue
    .line 578
    iget-object v0, p0, Lcom/sec/android/app/storycam/AppDataManager;->mThumbnailCacheUpdateListeners:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 579
    iget-object v0, p0, Lcom/sec/android/app/storycam/AppDataManager;->mThumbnailCacheUpdateListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 581
    :cond_0
    return-void
.end method

.method public removeTranscodeElementChangeListener(Lcom/sec/android/app/storycam/AppDataManager$TranscodeElementChangeListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/sec/android/app/storycam/AppDataManager$TranscodeElementChangeListener;

    .prologue
    .line 383
    iget-object v0, p0, Lcom/sec/android/app/storycam/AppDataManager;->mTranscodeElementChangedListeners:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 384
    iget-object v0, p0, Lcom/sec/android/app/storycam/AppDataManager;->mTranscodeElementChangedListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 386
    :cond_0
    return-void
.end method

.method public setCurrentTranscodeElement(Lcom/samsung/app/video/editor/external/TranscodeElement;)V
    .locals 1
    .param p1, "newTrans"    # Lcom/samsung/app/video/editor/external/TranscodeElement;

    .prologue
    .line 352
    if-eqz p1, :cond_1

    .line 353
    iget-object v0, p0, Lcom/sec/android/app/storycam/AppDataManager;->mSummarizedTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    if-eqz v0, :cond_0

    .line 354
    iget-object v0, p0, Lcom/sec/android/app/storycam/AppDataManager;->mSummarizedTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    invoke-virtual {v0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->deleteAllClipArts()V

    .line 356
    :cond_0
    new-instance v0, Lcom/samsung/app/video/editor/external/TranscodeElement;

    invoke-direct {v0, p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;-><init>(Lcom/samsung/app/video/editor/external/TranscodeElement;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/AppDataManager;->mSummarizedTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    .line 357
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/AppDataManager;->notifyTranscodeElementChanges()V

    .line 359
    :cond_1
    return-void
.end method

.method public setInitialTransocdeElement(Lcom/samsung/app/video/editor/external/TranscodeElement;)V
    .locals 1
    .param p1, "initialTrans"    # Lcom/samsung/app/video/editor/external/TranscodeElement;

    .prologue
    .line 344
    if-eqz p1, :cond_1

    .line 345
    iget-object v0, p0, Lcom/sec/android/app/storycam/AppDataManager;->mInitialTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    if-eqz v0, :cond_0

    .line 346
    iget-object v0, p0, Lcom/sec/android/app/storycam/AppDataManager;->mInitialTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    invoke-virtual {v0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->deleteAllClipArts()V

    .line 347
    :cond_0
    new-instance v0, Lcom/samsung/app/video/editor/external/TranscodeElement;

    invoke-direct {v0, p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;-><init>(Lcom/samsung/app/video/editor/external/TranscodeElement;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/AppDataManager;->mInitialTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    .line 349
    :cond_1
    return-void
.end method

.method public setIsUHDFileInTranscodeElement(Lcom/samsung/app/video/editor/external/TranscodeElement;)V
    .locals 6
    .param p1, "tElement"    # Lcom/samsung/app/video/editor/external/TranscodeElement;

    .prologue
    .line 397
    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/sec/android/app/storycam/AppDataManager;->isUHDContentInTranscode:Z

    .line 398
    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v0

    .line 399
    .local v0, "count":I
    const/4 v2, 0x0

    .line 400
    .local v2, "element":Lcom/samsung/app/video/editor/external/Element;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-lt v3, v0, :cond_1

    .line 419
    :cond_0
    :goto_1
    return-void

    .line 401
    :cond_1
    new-instance v4, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v4}, Landroid/media/MediaMetadataRetriever;-><init>()V

    .line 402
    .local v4, "retriever":Landroid/media/MediaMetadataRetriever;
    invoke-virtual {p1, v3}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElement(I)Lcom/samsung/app/video/editor/external/Element;

    move-result-object v2

    .line 404
    :try_start_0
    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/Element;->getFilePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/lang/String;)V

    .line 405
    invoke-static {v4}, Lcom/sec/android/app/ve/util/CommonUtils;->isUHD(Landroid/media/MediaMetadataRetriever;)Z

    move-result v5

    if-nez v5, :cond_2

    invoke-static {v4}, Lcom/sec/android/app/ve/util/CommonUtils;->isWQHD(Landroid/media/MediaMetadataRetriever;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 406
    :cond_2
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/sec/android/app/storycam/AppDataManager;->isUHDContentInTranscode:Z

    .line 407
    if-eqz v4, :cond_0

    .line 408
    invoke-virtual {v4}, Landroid/media/MediaMetadataRetriever;->release()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 412
    :catch_0
    move-exception v1

    .line 414
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 416
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_3
    if-eqz v4, :cond_4

    .line 417
    invoke-virtual {v4}, Landroid/media/MediaMetadataRetriever;->release()V

    .line 400
    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public setOriginalTranscodeElement(Lcom/samsung/app/video/editor/external/TranscodeElement;)V
    .locals 1
    .param p1, "origTrans"    # Lcom/samsung/app/video/editor/external/TranscodeElement;

    .prologue
    .line 366
    if-eqz p1, :cond_1

    .line 367
    iget-object v0, p0, Lcom/sec/android/app/storycam/AppDataManager;->mOriginalTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    if-eqz v0, :cond_0

    .line 368
    iget-object v0, p0, Lcom/sec/android/app/storycam/AppDataManager;->mOriginalTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    invoke-virtual {v0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->deleteAllClipArts()V

    .line 369
    :cond_0
    new-instance v0, Lcom/samsung/app/video/editor/external/TranscodeElement;

    invoke-direct {v0, p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;-><init>(Lcom/samsung/app/video/editor/external/TranscodeElement;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/AppDataManager;->mOriginalTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    .line 371
    :cond_1
    return-void
.end method

.method public setOutputDuration(J)V
    .locals 1
    .param p1, "duration"    # J

    .prologue
    .line 625
    iput-wide p1, p0, Lcom/sec/android/app/storycam/AppDataManager;->mOutputDuration:J

    .line 626
    return-void
.end method

.method public startFetchingThumbnails()V
    .locals 17

    .prologue
    .line 484
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/storycam/AppDataManager;->mThumbnailCache:Ljava/util/LinkedHashMap;

    if-nez v3, :cond_0

    .line 485
    new-instance v3, Ljava/util/LinkedHashMap;

    invoke-direct {v3}, Ljava/util/LinkedHashMap;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/android/app/storycam/AppDataManager;->mThumbnailCache:Ljava/util/LinkedHashMap;

    .line 487
    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/storycam/AppDataManager;->mTaskPool:Lcom/sec/android/app/ve/thread/AsyncTaskPool;

    if-nez v3, :cond_1

    .line 488
    new-instance v3, Lcom/sec/android/app/ve/thread/AsyncTaskPool;

    const/16 v4, 0xa

    invoke-direct {v3, v4}, Lcom/sec/android/app/ve/thread/AsyncTaskPool;-><init>(I)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/android/app/storycam/AppDataManager;->mTaskPool:Lcom/sec/android/app/ve/thread/AsyncTaskPool;

    .line 490
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/storycam/AppDataManager;->mOriginalTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    if-eqz v3, :cond_2

    .line 491
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/storycam/AppDataManager;->mOriginalTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementList()Ljava/util/List;

    move-result-object v12

    .line 492
    .local v12, "eleList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    if-eqz v12, :cond_2

    .line 493
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/storycam/AppDataManager;->mThumbnailCacheLock:Ljava/lang/Object;

    monitor-enter v15

    .line 494
    :try_start_0
    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v11

    .line 495
    .local v11, "count":I
    const/4 v14, 0x0

    .local v14, "i":I
    :goto_0
    if-lt v14, v11, :cond_3

    .line 493
    monitor-exit v15

    .line 516
    .end local v11    # "count":I
    .end local v12    # "eleList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    .end local v14    # "i":I
    :cond_2
    return-void

    .line 496
    .restart local v11    # "count":I
    .restart local v12    # "eleList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    .restart local v14    # "i":I
    :cond_3
    invoke-interface {v12, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/samsung/app/video/editor/external/Element;

    .line 499
    .local v13, "element":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {v13}, Lcom/samsung/app/video/editor/external/Element;->getFilePath()Ljava/lang/String;

    move-result-object v2

    .line 500
    .local v2, "filePath":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/storycam/AppDataManager;->mThumbnailCache:Ljava/util/LinkedHashMap;

    invoke-virtual {v3, v2}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/storycam/AppDataManager;->mThumbnailCache:Ljava/util/LinkedHashMap;

    invoke-virtual {v3, v2}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_5

    .line 501
    :cond_4
    new-instance v1, Lcom/sec/android/app/ve/thread/ImageThumbnailFetcherTask;

    .line 502
    const/4 v3, 0x0

    .line 503
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/storycam/AppDataManager;->mTaskPool:Lcom/sec/android/app/ve/thread/AsyncTaskPool;

    .line 504
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/storycam/AppDataManager;->mThumbnailCbk:Lcom/sec/android/app/ve/thread/VideoImageThumbFetcher$VideoImageThumbnaiFetcherCallbak;

    .line 505
    const/16 v6, 0x280

    .line 506
    const/16 v7, 0x168

    .line 507
    const/4 v8, 0x0

    .line 508
    invoke-virtual {v13}, Lcom/samsung/app/video/editor/external/Element;->getType()I

    move-result v9

    .line 509
    invoke-virtual {v13}, Lcom/samsung/app/video/editor/external/Element;->getID()I

    move-result v10

    const/16 v16, -0x1

    move/from16 v0, v16

    if-eq v10, v0, :cond_6

    const/4 v10, 0x1

    .line 501
    :goto_1
    invoke-direct/range {v1 .. v10}, Lcom/sec/android/app/ve/thread/ImageThumbnailFetcherTask;-><init>(Ljava/lang/String;ILcom/sec/android/app/ve/thread/AsyncTaskPool;Lcom/sec/android/app/ve/thread/VideoImageThumbFetcher$VideoImageThumbnaiFetcherCallbak;IILjava/lang/Object;IZ)V

    .line 510
    .local v1, "task":Lcom/sec/android/app/ve/thread/ImageThumbnailFetcherTask;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/storycam/AppDataManager;->mTaskPool:Lcom/sec/android/app/ve/thread/AsyncTaskPool;

    invoke-virtual {v3, v1}, Lcom/sec/android/app/ve/thread/AsyncTaskPool;->addUniqueTask(Lcom/sec/android/app/ve/thread/SimpleTask;)Lcom/sec/android/app/ve/thread/SimpleTask;

    .line 495
    .end local v1    # "task":Lcom/sec/android/app/ve/thread/ImageThumbnailFetcherTask;
    :cond_5
    add-int/lit8 v14, v14, 0x1

    goto :goto_0

    .line 509
    :cond_6
    const/4 v10, 0x0

    goto :goto_1

    .line 493
    .end local v2    # "filePath":Ljava/lang/String;
    .end local v11    # "count":I
    .end local v13    # "element":Lcom/samsung/app/video/editor/external/Element;
    .end local v14    # "i":I
    :catchall_0
    move-exception v3

    monitor-exit v15
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method public updateDefaultKenburns(Lcom/samsung/app/video/editor/external/TranscodeElement;)V
    .locals 8
    .param p1, "newTranscodeElement"    # Lcom/samsung/app/video/editor/external/TranscodeElement;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 710
    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v0

    .line 711
    .local v0, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-lt v2, v0, :cond_0

    .line 736
    return-void

    .line 712
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/app/video/editor/external/Element;

    .line 715
    .local v1, "element":Lcom/samsung/app/video/editor/external/Element;
    const/4 v3, 0x0

    .line 716
    .local v3, "item":Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;
    iget-object v4, p0, Lcom/sec/android/app/storycam/AppDataManager;->mThumbnailCache:Ljava/util/LinkedHashMap;

    iget-object v5, v1, Lcom/samsung/app/video/editor/external/Element;->filePath:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "item":Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;
    check-cast v3, Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;

    .line 717
    .restart local v3    # "item":Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;
    if-eqz v3, :cond_1

    # getter for: Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;->mStartRect:Landroid/graphics/RectF;
    invoke-static {v3}, Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;->access$2(Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;)Landroid/graphics/RectF;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 719
    rem-int/lit8 v4, v2, 0x2

    if-nez v4, :cond_2

    .line 720
    # getter for: Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;->mStartRect:Landroid/graphics/RectF;
    invoke-static {v3}, Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;->access$2(Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;)Landroid/graphics/RectF;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/samsung/app/video/editor/external/Element;->setStartRect(Landroid/graphics/RectF;)V

    .line 721
    # getter for: Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;->mEndRect:Landroid/graphics/RectF;
    invoke-static {v3}, Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;->access$3(Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;)Landroid/graphics/RectF;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/samsung/app/video/editor/external/Element;->setEndRect(Landroid/graphics/RectF;)V

    .line 722
    # getter for: Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;->mStartMatrix:Landroid/graphics/Matrix;
    invoke-static {v3}, Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;->access$4(Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;)Landroid/graphics/Matrix;

    move-result-object v4

    invoke-virtual {v1, v4, v7}, Lcom/samsung/app/video/editor/external/Element;->setMatrix(Landroid/graphics/Matrix;Z)V

    .line 723
    # getter for: Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;->mEndMatrix:Landroid/graphics/Matrix;
    invoke-static {v3}, Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;->access$5(Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;)Landroid/graphics/Matrix;

    move-result-object v4

    invoke-virtual {v1, v4, v6}, Lcom/samsung/app/video/editor/external/Element;->setMatrix(Landroid/graphics/Matrix;Z)V

    .line 732
    :goto_1
    # getter for: Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;->mRefRect:Landroid/graphics/RectF;
    invoke-static {v3}, Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;->access$6(Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;)Landroid/graphics/RectF;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/samsung/app/video/editor/external/Element;->setRefRect(Landroid/graphics/RectF;)V

    .line 711
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 725
    :cond_2
    # getter for: Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;->mEndRect:Landroid/graphics/RectF;
    invoke-static {v3}, Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;->access$3(Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;)Landroid/graphics/RectF;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/samsung/app/video/editor/external/Element;->setStartRect(Landroid/graphics/RectF;)V

    .line 726
    # getter for: Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;->mStartRect:Landroid/graphics/RectF;
    invoke-static {v3}, Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;->access$2(Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;)Landroid/graphics/RectF;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/samsung/app/video/editor/external/Element;->setEndRect(Landroid/graphics/RectF;)V

    .line 727
    # getter for: Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;->mStartMatrix:Landroid/graphics/Matrix;
    invoke-static {v3}, Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;->access$4(Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;)Landroid/graphics/Matrix;

    move-result-object v4

    invoke-virtual {v1, v4, v6}, Lcom/samsung/app/video/editor/external/Element;->setMatrix(Landroid/graphics/Matrix;Z)V

    .line 728
    # getter for: Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;->mEndMatrix:Landroid/graphics/Matrix;
    invoke-static {v3}, Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;->access$5(Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;)Landroid/graphics/Matrix;

    move-result-object v4

    invoke-virtual {v1, v4, v7}, Lcom/samsung/app/video/editor/external/Element;->setMatrix(Landroid/graphics/Matrix;Z)V

    goto :goto_1
.end method

.method public validateIntentAndAddToTranscodeElement(Landroid/content/Intent;)Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;
    .locals 14
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v13, -0x1

    .line 115
    sget-object v6, Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;->NONE_SUPPORTED:Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;

    .line 116
    .local v6, "result":Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;
    if-eqz p1, :cond_1

    .line 118
    sget-object v6, Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;->ALL_FILES_SUPPORTED:Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;

    .line 119
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 120
    .local v5, "mediaFiles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/storycam/AppDataManager$MediaFile;>;"
    const-string v9, "selectedItems"

    invoke-direct {p0, p1, v5, v9}, Lcom/sec/android/app/storycam/AppDataManager;->validateVideoEditIntent(Landroid/content/Intent;Ljava/util/ArrayList;Ljava/lang/String;)Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;

    move-result-object v6

    .line 121
    const/4 v8, 0x0

    .line 122
    .local v8, "uniqueID":I
    const/4 v7, 0x0

    .line 123
    .local v7, "uniqueGID":I
    iget-object v9, p0, Lcom/sec/android/app/storycam/AppDataManager;->mOriginalTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    invoke-virtual {v9}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementList()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_0
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-nez v10, :cond_2

    .line 131
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 133
    .local v0, "countOfElements":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    if-lt v3, v0, :cond_4

    .line 150
    iget-object v9, p0, Lcom/sec/android/app/storycam/AppDataManager;->mOriginalTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    invoke-virtual {v9}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v9

    if-nez v9, :cond_7

    .line 151
    sget-object v6, Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;->NONE_SUPPORTED:Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;

    .line 157
    .end local v0    # "countOfElements":I
    .end local v3    # "i":I
    .end local v5    # "mediaFiles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/storycam/AppDataManager$MediaFile;>;"
    .end local v7    # "uniqueGID":I
    .end local v8    # "uniqueID":I
    :cond_1
    :goto_2
    return-object v6

    .line 123
    .restart local v5    # "mediaFiles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/storycam/AppDataManager$MediaFile;>;"
    .restart local v7    # "uniqueGID":I
    .restart local v8    # "uniqueID":I
    :cond_2
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/app/video/editor/external/Element;

    .line 124
    .local v1, "ele":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/Element;->getID()I

    move-result v10

    if-le v10, v8, :cond_3

    .line 125
    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/Element;->getID()I

    move-result v8

    .line 127
    :cond_3
    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/Element;->getGroupID()I

    move-result v10

    if-le v10, v7, :cond_0

    .line 128
    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/Element;->getGroupID()I

    move-result v7

    goto :goto_0

    .line 134
    .end local v1    # "ele":Lcom/samsung/app/video/editor/external/Element;
    .restart local v0    # "countOfElements":I
    .restart local v3    # "i":I
    :cond_4
    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/storycam/AppDataManager$MediaFile;

    .line 135
    .local v4, "mediaFile":Lcom/sec/android/app/storycam/AppDataManager$MediaFile;
    iget-object v9, p0, Lcom/sec/android/app/storycam/AppDataManager;->mOriginalTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    invoke-virtual {v4}, Lcom/sec/android/app/storycam/AppDataManager$MediaFile;->getPath()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4}, Lcom/sec/android/app/storycam/AppDataManager$MediaFile;->getType()I

    move-result v11

    const/4 v12, 0x0

    invoke-virtual {v9, v10, v11, v12}, Lcom/samsung/app/video/editor/external/TranscodeElement;->createElement(Ljava/lang/String;IZ)Lcom/samsung/app/video/editor/external/Element;

    move-result-object v2

    .line 136
    .local v2, "element":Lcom/samsung/app/video/editor/external/Element;
    if-eqz v2, :cond_5

    .line 137
    invoke-virtual {v4}, Lcom/sec/android/app/storycam/AppDataManager$MediaFile;->isValid()Z

    move-result v9

    if-eqz v9, :cond_6

    .line 138
    add-int/lit8 v8, v8, 0x1

    invoke-virtual {v2, v8}, Lcom/samsung/app/video/editor/external/Element;->setID(I)V

    .line 139
    add-int/lit8 v7, v7, 0x1

    invoke-virtual {v2, v7}, Lcom/samsung/app/video/editor/external/Element;->setGroupID(I)V

    .line 145
    :goto_3
    iget-object v9, p0, Lcom/sec/android/app/storycam/AppDataManager;->mOriginalTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    invoke-virtual {v9, v2}, Lcom/samsung/app/video/editor/external/TranscodeElement;->addElement(Lcom/samsung/app/video/editor/external/Element;)V

    .line 133
    :cond_5
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 142
    :cond_6
    invoke-virtual {v2, v13}, Lcom/samsung/app/video/editor/external/Element;->setID(I)V

    .line 143
    invoke-virtual {v2, v13}, Lcom/samsung/app/video/editor/external/Element;->setGroupID(I)V

    goto :goto_3

    .line 152
    .end local v2    # "element":Lcom/samsung/app/video/editor/external/Element;
    .end local v4    # "mediaFile":Lcom/sec/android/app/storycam/AppDataManager$MediaFile;
    :cond_7
    iget-object v9, p0, Lcom/sec/android/app/storycam/AppDataManager;->mOriginalTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    invoke-virtual {v9}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v9

    const/16 v10, 0xf

    if-le v9, v10, :cond_1

    .line 153
    sget-object v6, Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;->COUNT_EXCEEDS_LIMIT:Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;

    goto :goto_2
.end method

.method public validateIntentAndCreateTranscodeElement(Landroid/content/Intent;)Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;
    .locals 11
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 163
    sget-object v6, Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;->NONE_SUPPORTED:Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;

    .line 164
    .local v6, "result":Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;
    if-eqz p1, :cond_0

    .line 165
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    .line 166
    .local v3, "lAction":Ljava/lang/String;
    const-string v7, "android.intent.action.EDIT"

    invoke-virtual {v7, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 167
    sget-object v6, Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;->ALL_FILES_SUPPORTED:Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;

    .line 168
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 169
    .local v5, "mediaFiles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/storycam/AppDataManager$MediaFile;>;"
    const-string v7, "android.intent.extra.STREAM"

    invoke-direct {p0, p1, v5, v7}, Lcom/sec/android/app/storycam/AppDataManager;->validateVideoEditIntent(Landroid/content/Intent;Ljava/util/ArrayList;Ljava/lang/String;)Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;

    move-result-object v6

    .line 171
    new-instance v7, Lcom/samsung/app/video/editor/external/TranscodeElement;

    invoke-direct {v7}, Lcom/samsung/app/video/editor/external/TranscodeElement;-><init>()V

    iput-object v7, p0, Lcom/sec/android/app/storycam/AppDataManager;->mOriginalTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    .line 172
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 174
    .local v0, "countOfElements":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-lt v2, v0, :cond_1

    .line 187
    iget-object v7, p0, Lcom/sec/android/app/storycam/AppDataManager;->mOriginalTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    invoke-virtual {v7}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v7

    if-nez v7, :cond_4

    .line 188
    sget-object v6, Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;->NONE_SUPPORTED:Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;

    .line 196
    .end local v0    # "countOfElements":I
    .end local v2    # "i":I
    .end local v3    # "lAction":Ljava/lang/String;
    .end local v5    # "mediaFiles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/storycam/AppDataManager$MediaFile;>;"
    :cond_0
    :goto_1
    return-object v6

    .line 175
    .restart local v0    # "countOfElements":I
    .restart local v2    # "i":I
    .restart local v3    # "lAction":Ljava/lang/String;
    .restart local v5    # "mediaFiles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/storycam/AppDataManager$MediaFile;>;"
    :cond_1
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/storycam/AppDataManager$MediaFile;

    .line 176
    .local v4, "mediaFile":Lcom/sec/android/app/storycam/AppDataManager$MediaFile;
    iget-object v7, p0, Lcom/sec/android/app/storycam/AppDataManager;->mOriginalTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    invoke-virtual {v4}, Lcom/sec/android/app/storycam/AppDataManager$MediaFile;->getPath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4}, Lcom/sec/android/app/storycam/AppDataManager$MediaFile;->getType()I

    move-result v9

    const/4 v10, 0x0

    invoke-virtual {v7, v8, v9, v10}, Lcom/samsung/app/video/editor/external/TranscodeElement;->createElement(Ljava/lang/String;IZ)Lcom/samsung/app/video/editor/external/Element;

    move-result-object v1

    .line 177
    .local v1, "element":Lcom/samsung/app/video/editor/external/Element;
    if-eqz v1, :cond_2

    .line 178
    invoke-virtual {v4}, Lcom/sec/android/app/storycam/AppDataManager$MediaFile;->isValid()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 179
    invoke-virtual {v1, v2}, Lcom/samsung/app/video/editor/external/Element;->setID(I)V

    .line 182
    :goto_2
    iget-object v7, p0, Lcom/sec/android/app/storycam/AppDataManager;->mOriginalTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    invoke-virtual {v7, v1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->addElement(Lcom/samsung/app/video/editor/external/Element;)V

    .line 174
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 181
    :cond_3
    const/4 v7, -0x1

    invoke-virtual {v1, v7}, Lcom/samsung/app/video/editor/external/Element;->setID(I)V

    goto :goto_2

    .line 190
    .end local v1    # "element":Lcom/samsung/app/video/editor/external/Element;
    .end local v4    # "mediaFile":Lcom/sec/android/app/storycam/AppDataManager$MediaFile;
    :cond_4
    iget-object v7, p0, Lcom/sec/android/app/storycam/AppDataManager;->mOriginalTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    invoke-virtual {v7}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v7

    const/16 v8, 0xf

    if-le v7, v8, :cond_0

    .line 191
    sget-object v6, Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;->COUNT_EXCEEDS_LIMIT:Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;

    goto :goto_1
.end method
