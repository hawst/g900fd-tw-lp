.class Lcom/sec/android/app/ve/projectsharing/ChordChannelService$2;
.super Ljava/lang/Object;
.source "ChordChannelService.java"

# interfaces
.implements Lcom/samsung/chord/IChordManagerListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/ve/projectsharing/ChordChannelService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$2;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    .line 135
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(I)V
    .locals 2
    .param p1, "arg0"    # I

    .prologue
    .line 139
    const-string v0, "ProjectSharing"

    const-string v1, "inside IChordManagerListener.onError"

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    return-void
.end method

.method public onNetworkDisconnected()V
    .locals 2

    .prologue
    .line 144
    const-string v0, "ProjectSharing"

    const-string v1, "inside IChordManagerListener.onNetworkDisconnected"

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    return-void
.end method

.method public onStarted(Ljava/lang/String;I)V
    .locals 9
    .param p1, "nodeName"    # Ljava/lang/String;
    .param p2, "reason"    # I

    .prologue
    const/4 v8, 0x1

    .line 149
    if-nez p2, :cond_0

    .line 150
    iget-object v5, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$2;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    invoke-static {v5, v8}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->access$4(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;Z)V

    .line 151
    const/4 v0, 0x0

    .line 152
    .local v0, "channel":Lcom/samsung/chord/IChordChannel;
    iget-object v5, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$2;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mChordManager:Lcom/samsung/chord/ChordManager;
    invoke-static {v5}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->access$0(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;)Lcom/samsung/chord/ChordManager;

    move-result-object v5

    const-string v6, "CHORD_PRJ_SHARING_CHANNEL"

    iget-object v7, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$2;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mChannelListener:Lcom/samsung/chord/IChordChannelListener;
    invoke-static {v7}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->access$5(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;)Lcom/samsung/chord/IChordChannelListener;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lcom/samsung/chord/ChordManager;->joinChannel(Ljava/lang/String;Lcom/samsung/chord/IChordChannelListener;)Lcom/samsung/chord/IChordChannel;

    move-result-object v0

    .line 154
    if-nez v0, :cond_1

    .line 155
    const-string v5, "ProjectSharing"

    const-string v6, "Failed to join the channel"

    invoke-static {v5, v6}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    .end local v0    # "channel":Lcom/samsung/chord/IChordChannel;
    :cond_0
    :goto_0
    return-void

    .line 158
    .restart local v0    # "channel":Lcom/samsung/chord/IChordChannel;
    :cond_1
    const/4 v2, 0x0

    .line 159
    .local v2, "deviceList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v0}, Lcom/samsung/chord/IChordChannel;->getJoinedNodeList()Ljava/util/List;

    move-result-object v2

    .line 160
    if-eqz v2, :cond_2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_2

    .line 161
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v5

    if-lt v3, v5, :cond_3

    .line 170
    .end local v3    # "i":I
    :cond_2
    const-string v5, "ProjectSharing"

    const-string v6, "Successfully joined the chord channel"

    invoke-static {v5, v6}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 162
    .restart local v3    # "i":I
    :cond_3
    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 163
    .local v1, "device":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$2;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mDeviceMap:Ljava/util/HashMap;
    invoke-static {v5}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->access$6(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;)Ljava/util/HashMap;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    if-nez v5, :cond_4

    .line 164
    new-array v4, v8, [[B

    .line 165
    .local v4, "payload":[[B
    const/4 v5, 0x0

    iget-object v6, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$2;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    # invokes: Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->getMyDeviceName()Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->access$7(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->getBytes()[B

    move-result-object v6

    aput-object v6, v4, v5

    .line 166
    const-string v5, "MSG_REQUEST_DEVICE_NAME"

    invoke-interface {v0, v1, v5, v4}, Lcom/samsung/chord/IChordChannel;->sendData(Ljava/lang/String;Ljava/lang/String;[[B)Z

    .line 161
    .end local v4    # "payload":[[B
    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method public onStopped(I)V
    .locals 3
    .param p1, "reason"    # I

    .prologue
    .line 177
    iget-object v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$2;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->access$4(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;Z)V

    .line 178
    const-string v0, "ProjectSharing"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "inside IChordManagerListener.onStopped, reason = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    return-void
.end method
