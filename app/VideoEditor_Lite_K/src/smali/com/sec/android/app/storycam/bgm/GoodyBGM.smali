.class public Lcom/sec/android/app/storycam/bgm/GoodyBGM;
.super Lcom/sec/android/app/ve/bgm/BGM;
.source "GoodyBGM.java"


# static fields
.field private static synthetic $SWITCH_TABLE$com$sec$android$app$ve$bgm$BGM$BGMModes:[I


# instance fields
.field private fastModeBGM:[I

.field private mediumModeBGM:[I

.field private slowModeBGM:[I

.field private totalDurationBGM:I


# direct methods
.method static synthetic $SWITCH_TABLE$com$sec$android$app$ve$bgm$BGM$BGMModes()[I
    .locals 3

    .prologue
    .line 5
    sget-object v0, Lcom/sec/android/app/storycam/bgm/GoodyBGM;->$SWITCH_TABLE$com$sec$android$app$ve$bgm$BGM$BGMModes:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/sec/android/app/ve/bgm/BGM$BGMModes;->values()[Lcom/sec/android/app/ve/bgm/BGM$BGMModes;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/sec/android/app/ve/bgm/BGM$BGMModes;->FAST:Lcom/sec/android/app/ve/bgm/BGM$BGMModes;

    invoke-virtual {v1}, Lcom/sec/android/app/ve/bgm/BGM$BGMModes;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    :try_start_1
    sget-object v1, Lcom/sec/android/app/ve/bgm/BGM$BGMModes;->MEDIUM:Lcom/sec/android/app/ve/bgm/BGM$BGMModes;

    invoke-virtual {v1}, Lcom/sec/android/app/ve/bgm/BGM$BGMModes;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    :try_start_2
    sget-object v1, Lcom/sec/android/app/ve/bgm/BGM$BGMModes;->SLOW:Lcom/sec/android/app/ve/bgm/BGM$BGMModes;

    invoke-virtual {v1}, Lcom/sec/android/app/ve/bgm/BGM$BGMModes;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_3
    sput-object v0, Lcom/sec/android/app/storycam/bgm/GoodyBGM;->$SWITCH_TABLE$com$sec$android$app$ve$bgm$BGM$BGMModes:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_1
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/sec/android/app/ve/bgm/BGM;-><init>()V

    .line 8
    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    .line 14
    iput-object v0, p0, Lcom/sec/android/app/storycam/bgm/GoodyBGM;->slowModeBGM:[I

    .line 17
    const/16 v0, 0xa

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    .line 26
    iput-object v0, p0, Lcom/sec/android/app/storycam/bgm/GoodyBGM;->mediumModeBGM:[I

    .line 29
    const/16 v0, 0xf

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    .line 43
    iput-object v0, p0, Lcom/sec/android/app/storycam/bgm/GoodyBGM;->fastModeBGM:[I

    .line 46
    const v0, 0xb8ce

    iput v0, p0, Lcom/sec/android/app/storycam/bgm/GoodyBGM;->totalDurationBGM:I

    .line 5
    return-void

    .line 8
    nop

    :array_0
    .array-data 4
        0x16a8
        0x2e18
        0x4588
        0x5cf8
        0x7468
        0x8bd8
        0xa348
    .end array-data

    .line 17
    :array_1
    .array-data 4
        0xaf0
        0x1af4
        0x2a94
        0x39d0
        0x4a38
        0x5cf8
        0x6c98
        0x7d00
        0x8ca0
        0xa348
    .end array-data

    .line 29
    :array_2
    .array-data 4
        0xaf0
        0x16a8
        0x2260
        0x2e18
        0x39d0
        0x4588
        0x5140
        0x5cf8
        0x68b0
        0x7468
        0x8020
        0x8bd8
        0x9790
        0xa348
        0xaf00
    .end array-data
.end method


# virtual methods
.method protected getBGMFilepath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    const-string v0, "themedata/shining/bgm/E3_Goody.ogg"

    return-object v0
.end method

.method protected getBGMName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    const-string v0, "Goody"

    return-object v0
.end method

.method protected getTotalDurationBGM()I
    .locals 1

    .prologue
    .line 64
    iget v0, p0, Lcom/sec/android/app/storycam/bgm/GoodyBGM;->totalDurationBGM:I

    return v0
.end method

.method protected getTransitionTimeStamps(Lcom/sec/android/app/ve/bgm/BGM$BGMModes;)[I
    .locals 2
    .param p1, "mode"    # Lcom/sec/android/app/ve/bgm/BGM$BGMModes;

    .prologue
    .line 50
    invoke-static {}, Lcom/sec/android/app/storycam/bgm/GoodyBGM;->$SWITCH_TABLE$com$sec$android$app$ve$bgm$BGM$BGMModes()[I

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/android/app/ve/bgm/BGM$BGMModes;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 58
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 52
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/storycam/bgm/GoodyBGM;->slowModeBGM:[I

    goto :goto_0

    .line 54
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/storycam/bgm/GoodyBGM;->mediumModeBGM:[I

    goto :goto_0

    .line 56
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/storycam/bgm/GoodyBGM;->fastModeBGM:[I

    goto :goto_0

    .line 50
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
