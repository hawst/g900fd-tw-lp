.class public abstract Lcom/sec/android/app/ve/export/ExportInterface$Adapter;
.super Ljava/lang/Object;
.source "ExportInterface.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/ve/export/ExportInterface;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Adapter"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 575
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract getCurrentTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;
.end method

.method public onError()V
    .locals 0

    .prologue
    .line 587
    return-void
.end method

.method public onExportCompleted()V
    .locals 0

    .prologue
    .line 579
    return-void
.end method

.method public onExportPaused()V
    .locals 0

    .prologue
    .line 585
    return-void
.end method

.method public onExportResumed()V
    .locals 0

    .prologue
    .line 583
    return-void
.end method

.method public onExportStarted()V
    .locals 0

    .prologue
    .line 577
    return-void
.end method

.method public onExportStopped()V
    .locals 0

    .prologue
    .line 581
    return-void
.end method
