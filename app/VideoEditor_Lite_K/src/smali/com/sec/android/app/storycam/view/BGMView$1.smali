.class Lcom/sec/android/app/storycam/view/BGMView$1;
.super Ljava/lang/Object;
.source "BGMView.java"

# interfaces
.implements Landroid/media/AudioManager$OnAudioFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/storycam/view/BGMView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/storycam/view/BGMView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/storycam/view/BGMView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/storycam/view/BGMView$1;->this$0:Lcom/sec/android/app/storycam/view/BGMView;

    .line 128
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAudioFocusChange(I)V
    .locals 1
    .param p1, "focusChange"    # I

    .prologue
    .line 131
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView$1;->this$0:Lcom/sec/android/app/storycam/view/BGMView;

    # getter for: Lcom/sec/android/app/storycam/view/BGMView;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v0}, Lcom/sec/android/app/storycam/view/BGMView;->access$0(Lcom/sec/android/app/storycam/view/BGMView;)Landroid/media/MediaPlayer;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView$1;->this$0:Lcom/sec/android/app/storycam/view/BGMView;

    # getter for: Lcom/sec/android/app/storycam/view/BGMView;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v0}, Lcom/sec/android/app/storycam/view/BGMView;->access$0(Lcom/sec/android/app/storycam/view/BGMView;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 132
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView$1;->this$0:Lcom/sec/android/app/storycam/view/BGMView;

    # getter for: Lcom/sec/android/app/storycam/view/BGMView;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v0}, Lcom/sec/android/app/storycam/view/BGMView;->access$0(Lcom/sec/android/app/storycam/view/BGMView;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    .line 133
    :cond_0
    return-void
.end method
