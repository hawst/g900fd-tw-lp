.class Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$4;
.super Landroid/content/BroadcastReceiver;
.source "VideoEditorLiteActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$4;->this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;

    .line 229
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 233
    if-eqz p2, :cond_c

    .line 234
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 235
    .local v0, "action":Ljava/lang/String;
    const-string v4, "VE_LITE"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "onReceive "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    if-eqz v0, :cond_0

    .line 237
    const-string v4, "android.intent.action.BATTERY_LOW"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 238
    iget-object v4, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$4;->this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;

    const v5, 0x7f070059

    invoke-static {v4, v5, v8}, Lcom/sec/android/app/ve/VEApp;->showToast(Landroid/content/Context;II)Landroid/widget/Toast;

    .line 346
    .end local v0    # "action":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 240
    .restart local v0    # "action":Ljava/lang/String;
    :cond_1
    const-string v4, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 241
    const-string v4, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 242
    const-string v4, "android.intent.action.MEDIA_REMOVED"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 243
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$4;->this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;

    # getter for: Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mPreviewPlayer:Lcom/sec/android/app/ve/PreviewPlayerInterface;
    invoke-static {v4}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->access$1(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;)Lcom/sec/android/app/ve/PreviewPlayerInterface;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 244
    iget-object v4, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$4;->this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;

    # getter for: Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mPreviewPlayer:Lcom/sec/android/app/ve/PreviewPlayerInterface;
    invoke-static {v4}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->access$1(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;)Lcom/sec/android/app/ve/PreviewPlayerInterface;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/ve/PreviewPlayerInterface;->stop()V

    .line 245
    :cond_3
    iget-object v4, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$4;->this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;

    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v5

    # invokes: Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->recheckFiles(Ljava/lang/String;Z)Z
    invoke-static {v4, v5, v7}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->access$2(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;Ljava/lang/String;Z)Z

    goto :goto_0

    .line 249
    :cond_4
    const-string v4, "android.intent.action.MEDIA_SCANNER_FINISHED"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 250
    iget-object v4, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$4;->this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;

    iget-object v5, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$4;->this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;

    # getter for: Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->spaceString:Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->access$3(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;)Ljava/lang/String;

    move-result-object v5

    # invokes: Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->recheckFiles(Ljava/lang/String;Z)Z
    invoke-static {v4, v5, v7}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->access$2(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$4;->this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;

    # getter for: Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mPreviewPlayer:Lcom/sec/android/app/ve/PreviewPlayerInterface;
    invoke-static {v4}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->access$1(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;)Lcom/sec/android/app/ve/PreviewPlayerInterface;

    move-result-object v4

    if-eqz v4, :cond_5

    .line 251
    iget-object v4, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$4;->this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;

    # getter for: Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mPreviewPlayer:Lcom/sec/android/app/ve/PreviewPlayerInterface;
    invoke-static {v4}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->access$1(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;)Lcom/sec/android/app/ve/PreviewPlayerInterface;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/ve/PreviewPlayerInterface;->stop()V

    .line 252
    :cond_5
    sget-object v4, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v4}, Lcom/sec/android/app/storycam/AppDataManager;->getOriginalTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v2

    .line 253
    .local v2, "originalTEle":Lcom/samsung/app/video/editor/external/TranscodeElement;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v4

    if-gtz v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$4;->this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;

    # getter for: Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mPreviewPlayer:Lcom/sec/android/app/ve/PreviewPlayerInterface;
    invoke-static {v4}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->access$1(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;)Lcom/sec/android/app/ve/PreviewPlayerInterface;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 254
    iget-object v4, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$4;->this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;

    const v5, 0x7f070301

    invoke-static {v4, v5, v7}, Lcom/sec/android/app/ve/VEApp;->showToast(Landroid/content/Context;II)Landroid/widget/Toast;

    .line 255
    iget-object v4, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$4;->this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;

    invoke-virtual {v4}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->finish()V

    goto/16 :goto_0

    .line 259
    .end local v2    # "originalTEle":Lcom/samsung/app/video/editor/external/TranscodeElement;
    :cond_6
    const-string v4, "com.samsung.sec.android.clockpackage.alarm.ALARM_ALERT"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 260
    iget-object v4, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$4;->this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;

    # getter for: Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mPreviewPlayer:Lcom/sec/android/app/ve/PreviewPlayerInterface;
    invoke-static {v4}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->access$1(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;)Lcom/sec/android/app/ve/PreviewPlayerInterface;

    move-result-object v4

    if-eqz v4, :cond_7

    .line 261
    iget-object v4, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$4;->this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;

    # getter for: Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mPreviewPlayer:Lcom/sec/android/app/ve/PreviewPlayerInterface;
    invoke-static {v4}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->access$1(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;)Lcom/sec/android/app/ve/PreviewPlayerInterface;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/ve/PreviewPlayerInterface;->stop()V

    .line 262
    :cond_7
    iget-object v4, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$4;->this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;

    # getter for: Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mBgmInstance:Lcom/sec/android/app/storycam/view/BGMView;
    invoke-static {v4}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->access$4(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;)Lcom/sec/android/app/storycam/view/BGMView;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$4;->this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;

    # getter for: Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mBgmInstance:Lcom/sec/android/app/storycam/view/BGMView;
    invoke-static {v4}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->access$4(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;)Lcom/sec/android/app/storycam/view/BGMView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/storycam/view/BGMView;->isMediaplayerPlaying()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 263
    iget-object v4, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$4;->this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;

    # getter for: Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mBgmInstance:Lcom/sec/android/app/storycam/view/BGMView;
    invoke-static {v4}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->access$4(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;)Lcom/sec/android/app/storycam/view/BGMView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/storycam/view/BGMView;->stopMediaPlayer()V

    goto/16 :goto_0

    .line 265
    :cond_8
    const-string v4, "android.intent.action.PHONE_STATE"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 266
    const-string v4, "state"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 267
    .local v3, "state":Ljava/lang/String;
    sget-object v4, Landroid/telephony/TelephonyManager;->EXTRA_STATE_RINGING:Ljava/lang/String;

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 268
    iget-object v4, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$4;->this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;

    # getter for: Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mPreviewPlayer:Lcom/sec/android/app/ve/PreviewPlayerInterface;
    invoke-static {v4}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->access$1(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;)Lcom/sec/android/app/ve/PreviewPlayerInterface;

    move-result-object v4

    if-eqz v4, :cond_9

    .line 269
    iget-object v4, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$4;->this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;

    # getter for: Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mPreviewPlayer:Lcom/sec/android/app/ve/PreviewPlayerInterface;
    invoke-static {v4}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->access$1(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;)Lcom/sec/android/app/ve/PreviewPlayerInterface;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/ve/PreviewPlayerInterface;->stop()V

    .line 270
    :cond_9
    iget-object v4, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$4;->this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;

    # getter for: Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mBgmInstance:Lcom/sec/android/app/storycam/view/BGMView;
    invoke-static {v4}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->access$4(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;)Lcom/sec/android/app/storycam/view/BGMView;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$4;->this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;

    # getter for: Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mBgmInstance:Lcom/sec/android/app/storycam/view/BGMView;
    invoke-static {v4}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->access$4(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;)Lcom/sec/android/app/storycam/view/BGMView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/storycam/view/BGMView;->isMediaplayerPlaying()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 271
    iget-object v4, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$4;->this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;

    # getter for: Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mBgmInstance:Lcom/sec/android/app/storycam/view/BGMView;
    invoke-static {v4}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->access$4(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;)Lcom/sec/android/app/storycam/view/BGMView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/storycam/view/BGMView;->stopMediaPlayer()V

    goto/16 :goto_0

    .line 274
    .end local v3    # "state":Ljava/lang/String;
    :cond_a
    const-string v4, "com.samsung.cover.OPEN"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 276
    const-string v4, "coverOpen"

    invoke-virtual {p2, v4, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 277
    .local v1, "isCoverOpen":Z
    const-string v4, "VE_LITE"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "onReceive cover open intent isCoverOpen = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    if-nez v1, :cond_b

    iget-object v4, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$4;->this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;

    # getter for: Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mPreviewPlayer:Lcom/sec/android/app/ve/PreviewPlayerInterface;
    invoke-static {v4}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->access$1(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;)Lcom/sec/android/app/ve/PreviewPlayerInterface;

    move-result-object v4

    if-eqz v4, :cond_b

    .line 279
    iget-object v4, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$4;->this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;

    # getter for: Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mPreviewPlayer:Lcom/sec/android/app/ve/PreviewPlayerInterface;
    invoke-static {v4}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->access$1(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;)Lcom/sec/android/app/ve/PreviewPlayerInterface;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/ve/PreviewPlayerInterface;->stop()V

    .line 281
    :cond_b
    iget-object v4, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$4;->this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;

    # getter for: Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mBgmInstance:Lcom/sec/android/app/storycam/view/BGMView;
    invoke-static {v4}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->access$4(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;)Lcom/sec/android/app/storycam/view/BGMView;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$4;->this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;

    # getter for: Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mBgmInstance:Lcom/sec/android/app/storycam/view/BGMView;
    invoke-static {v4}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->access$4(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;)Lcom/sec/android/app/storycam/view/BGMView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/storycam/view/BGMView;->isMediaplayerPlaying()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 282
    iget-object v4, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$4;->this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;

    # getter for: Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mBgmInstance:Lcom/sec/android/app/storycam/view/BGMView;
    invoke-static {v4}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->access$4(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;)Lcom/sec/android/app/storycam/view/BGMView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/storycam/view/BGMView;->stopMediaPlayer()V

    goto/16 :goto_0

    .line 344
    .end local v0    # "action":Ljava/lang/String;
    .end local v1    # "isCoverOpen":Z
    :cond_c
    const-string v4, "VE_LITE"

    const-string v5, "onReceive Intent is null"

    invoke-static {v4, v5}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
