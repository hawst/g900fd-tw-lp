.class Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$3;
.super Ljava/lang/Object;
.source "MediaEditGridItemLayout.java"

# interfaces
.implements Landroid/view/View$OnHoverListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;


# direct methods
.method constructor <init>(Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$3;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;

    .line 101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 104
    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->checkAccessibillityEnabled()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 135
    :cond_0
    :goto_0
    return v1

    .line 107
    :cond_1
    invoke-virtual {p2, v1}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v3

    if-eq v3, v2, :cond_2

    invoke-virtual {p2, v1}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_0

    .line 110
    :cond_2
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    :goto_1
    :pswitch_0
    move v1, v2

    .line 135
    goto :goto_0

    .line 112
    :pswitch_1
    iget-object v3, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$3;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;

    # getter for: Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mHoverWindow:Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;
    invoke-static {v3}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->access$6(Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;)Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 113
    iget-object v3, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$3;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;

    # getter for: Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mHoverWindow:Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;
    invoke-static {v3}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->access$6(Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;)Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;->isShowing()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 114
    iget-object v1, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$3;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;

    # getter for: Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mHoverWindow:Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;
    invoke-static {v1}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->access$6(Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;)Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;->keepOnScreen()V

    goto :goto_1

    .line 119
    :cond_3
    # getter for: Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mHandler:Landroid/os/Handler;
    invoke-static {}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->access$7()Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 120
    # getter for: Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mHandler:Landroid/os/Handler;
    invoke-static {}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->access$7()Landroid/os/Handler;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$3;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;

    invoke-static {v3, v1, v4}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 121
    .local v0, "msg":Landroid/os/Message;
    # getter for: Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mHandler:Landroid/os/Handler;
    invoke-static {}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->access$7()Landroid/os/Handler;

    move-result-object v1

    const-wide/16 v4, 0x190

    invoke-virtual {v1, v0, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_1

    .line 128
    .end local v0    # "msg":Landroid/os/Message;
    :pswitch_2
    iget-object v1, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$3;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;

    # invokes: Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->dismissHoverWindowDelayed()V
    invoke-static {v1}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->access$8(Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;)V

    goto :goto_1

    .line 110
    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
