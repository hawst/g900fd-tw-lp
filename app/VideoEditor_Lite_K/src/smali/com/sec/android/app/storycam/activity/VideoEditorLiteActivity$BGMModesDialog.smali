.class public Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$BGMModesDialog;
.super Lcom/sec/android/app/ve/common/AndroidUtils$DlgFragment;
.source "VideoEditorLiteActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "BGMModesDialog"
.end annotation


# static fields
.field private static BGMmode:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2347
    const/4 v0, 0x1

    sput v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$BGMModesDialog;->BGMmode:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2346
    invoke-direct {p0}, Lcom/sec/android/app/ve/common/AndroidUtils$DlgFragment;-><init>()V

    return-void
.end method

.method static synthetic access$0(I)V
    .locals 0

    .prologue
    .line 2347
    sput p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$BGMModesDialog;->BGMmode:I

    return-void
.end method

.method static synthetic access$1()I
    .locals 1

    .prologue
    .line 2347
    sget v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$BGMModesDialog;->BGMmode:I

    return v0
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 2426
    invoke-super {p0, p1}, Lcom/sec/android/app/ve/common/AndroidUtils$DlgFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 2427
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$BGMModesDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$BGMModesDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p1, Landroid/content/res/Configuration;->fontScale:F

    # getter for: Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->fontScale_value:F
    invoke-static {}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->access$7()F

    move-result v1

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    .line 2428
    iget v0, p1, Landroid/content/res/Configuration;->fontScale:F

    invoke-static {v0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->access$8(F)V

    .line 2429
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$BGMModesDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/ve/common/AndroidUtils;->dismissDialog(Landroid/app/Activity;)V

    .line 2432
    :cond_0
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 10
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v9, 0x1

    .line 2351
    # invokes: Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->updateBGMDurationList()I
    invoke-static {}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->access$10()I

    move-result v4

    .line 2356
    .local v4, "selectedIndex":I
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$BGMModesDialog;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-direct {v1, v6}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 2357
    .local v1, "builder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$BGMModesDialog;->getActivity()Landroid/app/Activity;

    move-result-object v6

    const-string v7, "layout_inflater"

    invoke-virtual {v6, v7}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 2358
    .local v2, "inflater":Landroid/view/LayoutInflater;
    const v6, 0x7f030002

    const/4 v7, 0x0

    invoke-virtual {v2, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 2359
    .local v5, "view":Landroid/view/View;
    const v6, 0x7f0d000c

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ListView;

    .line 2360
    .local v3, "listView":Landroid/widget/ListView;
    invoke-virtual {v3, v9}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 2361
    new-instance v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$BGMModesDialog$1;

    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$BGMModesDialog;->getActivity()Landroid/app/Activity;

    move-result-object v6

    const v7, 0x109000f

    # getter for: Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mBGMDurationList:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->access$11()Ljava/util/ArrayList;

    move-result-object v8

    invoke-direct {v0, p0, v6, v7, v8}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$BGMModesDialog$1;-><init>(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$BGMModesDialog;Landroid/content/Context;ILjava/util/List;)V

    .line 2373
    .local v0, "adapter":Landroid/widget/ListAdapter;
    invoke-virtual {v3, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2374
    invoke-virtual {v1, v5}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 2375
    invoke-virtual {v3, v4, v9}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 2376
    const v6, 0x7f07026f

    invoke-virtual {v1, v6}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 2377
    const v6, 0x7f07005e

    new-instance v7, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$BGMModesDialog$2;

    invoke-direct {v7, p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$BGMModesDialog$2;-><init>(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$BGMModesDialog;)V

    invoke-virtual {v1, v6, v7}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 2387
    new-instance v6, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$BGMModesDialog$3;

    invoke-direct {v6, p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$BGMModesDialog$3;-><init>(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$BGMModesDialog;)V

    invoke-virtual {v3, v6}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 2403
    const v6, 0x7f0700ab

    new-instance v7, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$BGMModesDialog$4;

    invoke-direct {v7, p0, v3}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$BGMModesDialog$4;-><init>(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$BGMModesDialog;Landroid/widget/ListView;)V

    invoke-virtual {v1, v6, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 2420
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v6

    return-object v6
.end method
