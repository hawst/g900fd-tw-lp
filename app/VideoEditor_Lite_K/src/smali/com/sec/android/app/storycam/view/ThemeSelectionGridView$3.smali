.class Lcom/sec/android/app/storycam/view/ThemeSelectionGridView$3;
.super Ljava/lang/Object;
.source "ThemeSelectionGridView.java"

# interfaces
.implements Lcom/sec/android/app/storycam/AppDataManager$TranscodeElementChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView$3;->this$0:Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;

    .line 319
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTranscodeElementChanged(Lcom/samsung/app/video/editor/external/TranscodeElement;)V
    .locals 2
    .param p1, "transcodeElement"    # Lcom/samsung/app/video/editor/external/TranscodeElement;

    .prologue
    .line 323
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView$3;->this$0:Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;

    iget-object v1, p0, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView$3;->this$0:Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;

    # getter for: Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;->mItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;
    invoke-static {v1}, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;->access$1(Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;)Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 324
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView$3;->this$0:Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;

    iget-object v1, p0, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView$3;->this$0:Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;

    # getter for: Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;->isM2Model:Z
    invoke-static {v1}, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;->access$2(Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    # invokes: Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;->isM2ModelAdjustLayout(Ljava/lang/Boolean;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;->access$3(Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;Ljava/lang/Boolean;)V

    .line 325
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView$3;->this$0:Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;

    # getter for: Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;->mAdapter:Lcom/sec/android/app/storycam/view/ThemeSelectionGridView$ThemeSelectionAdapter;
    invoke-static {v0}, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;->access$4(Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;)Lcom/sec/android/app/storycam/view/ThemeSelectionGridView$ThemeSelectionAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView$ThemeSelectionAdapter;->notifyDataSetChanged()V

    .line 327
    return-void
.end method
