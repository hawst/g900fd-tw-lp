.class public Lcom/sec/android/app/ve/util/WaveformUtil;
.super Ljava/lang/Object;
.source "WaveformUtil.java"


# static fields
.field private static final FILE_EXT:Ljava/lang/String; = ".wfd"

.field private static final _instance:Lcom/sec/android/app/ve/util/WaveformUtil;


# instance fields
.field private allWaveforms:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/app/ve/data/WaveformDAO;",
            ">;"
        }
    .end annotation
.end field

.field private clientsCallback:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/sec/android/app/ve/thread/WaveformDataFetcher$WaveformDataFetcherCallback;",
            ">;"
        }
    .end annotation
.end field

.field private fetcherThread:Lcom/sec/android/app/ve/thread/WaveformDataFetcher;

.field private utilCallback:Lcom/sec/android/app/ve/thread/WaveformDataFetcher$WaveformDataFetcherCallback;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    new-instance v0, Lcom/sec/android/app/ve/util/WaveformUtil;

    invoke-direct {v0}, Lcom/sec/android/app/ve/util/WaveformUtil;-><init>()V

    sput-object v0, Lcom/sec/android/app/ve/util/WaveformUtil;->_instance:Lcom/sec/android/app/ve/util/WaveformUtil;

    .line 47
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    new-instance v0, Lcom/sec/android/app/ve/util/WaveformUtil$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/ve/util/WaveformUtil$1;-><init>(Lcom/sec/android/app/ve/util/WaveformUtil;)V

    iput-object v0, p0, Lcom/sec/android/app/ve/util/WaveformUtil;->utilCallback:Lcom/sec/android/app/ve/thread/WaveformDataFetcher$WaveformDataFetcherCallback;

    .line 88
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/app/ve/util/WaveformUtil;)Ljava/util/concurrent/CopyOnWriteArrayList;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/ve/util/WaveformUtil;->clientsCallback:Ljava/util/concurrent/CopyOnWriteArrayList;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/app/ve/util/WaveformUtil;Lcom/sec/android/app/ve/data/WaveformDAO;)Z
    .locals 1

    .prologue
    .line 205
    invoke-direct {p0, p1}, Lcom/sec/android/app/ve/util/WaveformUtil;->addWaveform(Lcom/sec/android/app/ve/data/WaveformDAO;)Z

    move-result v0

    return v0
.end method

.method private addWaveform(Lcom/sec/android/app/ve/data/WaveformDAO;)Z
    .locals 2
    .param p1, "waveform"    # Lcom/sec/android/app/ve/data/WaveformDAO;

    .prologue
    .line 206
    iget-object v0, p0, Lcom/sec/android/app/ve/util/WaveformUtil;->allWaveforms:Ljava/util/HashMap;

    if-nez v0, :cond_0

    .line 207
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/ve/util/WaveformUtil;->allWaveforms:Ljava/util/HashMap;

    .line 210
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/ve/util/WaveformUtil;->allWaveforms:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/sec/android/app/ve/data/WaveformDAO;->getMediaSourcePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 212
    const/4 v0, 0x0

    .line 216
    :goto_0
    return v0

    .line 215
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/ve/util/WaveformUtil;->allWaveforms:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/sec/android/app/ve/data/WaveformDAO;->getMediaSourcePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 216
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static generateBufferFilePath(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "sourcePath"    # Ljava/lang/String;

    .prologue
    .line 240
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 241
    .local v0, "sourceFile":Ljava/io/File;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getFilesDirectory()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 242
    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".wfd"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 241
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getInstance()Lcom/sec/android/app/ve/util/WaveformUtil;
    .locals 1

    .prologue
    .line 91
    sget-object v0, Lcom/sec/android/app/ve/util/WaveformUtil;->_instance:Lcom/sec/android/app/ve/util/WaveformUtil;

    return-object v0
.end method

.method private startDataLoadingFromFile(Lcom/sec/android/app/ve/data/WaveformDAO;)V
    .locals 8
    .param p1, "waveformDAO"    # Lcom/sec/android/app/ve/data/WaveformDAO;

    .prologue
    .line 154
    new-instance v1, Ljava/io/File;

    invoke-virtual {p1}, Lcom/sec/android/app/ve/data/WaveformDAO;->getDataFileName()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v1, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 155
    .local v1, "bufferFile":Ljava/io/File;
    const/4 v3, 0x0

    .line 156
    .local v3, "fis":Ljava/io/FileInputStream;
    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v6

    long-to-int v5, v6

    invoke-static {v5}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 158
    .local v0, "buffer":Ljava/nio/ByteBuffer;
    :try_start_0
    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 159
    .end local v3    # "fis":Ljava/io/FileInputStream;
    .local v4, "fis":Ljava/io/FileInputStream;
    :try_start_1
    monitor-enter v0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 160
    if-eqz v0, :cond_0

    .line 161
    :try_start_2
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v7

    invoke-virtual {v4, v5, v6, v7}, Ljava/io/FileInputStream;->read([BII)I

    .line 159
    :cond_0
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 163
    :try_start_3
    invoke-virtual {p1, v0}, Lcom/sec/android/app/ve/data/WaveformDAO;->setWaveformData(Ljava/nio/ByteBuffer;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 167
    if-eqz v4, :cond_3

    .line 169
    :try_start_4
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    move-object v3, v4

    .line 175
    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    :cond_1
    :goto_0
    return-void

    .line 159
    .end local v3    # "fis":Ljava/io/FileInputStream;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    :catchall_0
    move-exception v5

    :try_start_5
    monitor-exit v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    throw v5
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 164
    :catch_0
    move-exception v2

    move-object v3, v4

    .line 165
    .end local v4    # "fis":Ljava/io/FileInputStream;
    .local v2, "e":Ljava/io/IOException;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    :goto_1
    :try_start_7
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 167
    if-eqz v3, :cond_1

    .line 169
    :try_start_8
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1

    goto :goto_0

    .line 170
    :catch_1
    move-exception v2

    .line 171
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 166
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_1
    move-exception v5

    .line 167
    :goto_2
    if-eqz v3, :cond_2

    .line 169
    :try_start_9
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_2

    .line 174
    :cond_2
    :goto_3
    throw v5

    .line 170
    :catch_2
    move-exception v2

    .line 171
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 170
    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "fis":Ljava/io/FileInputStream;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    :catch_3
    move-exception v2

    .line 171
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    .end local v2    # "e":Ljava/io/IOException;
    :cond_3
    move-object v3, v4

    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    goto :goto_0

    .line 166
    .end local v3    # "fis":Ljava/io/FileInputStream;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    :catchall_2
    move-exception v5

    move-object v3, v4

    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    goto :goto_2

    .line 164
    :catch_4
    move-exception v2

    goto :goto_1
.end method

.method private startWaveformLoading(Ljava/lang/String;Lcom/sec/android/app/ve/thread/WaveformDataFetcher$WaveformDataFetcherCallback;)V
    .locals 5
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "callback"    # Lcom/sec/android/app/ve/thread/WaveformDataFetcher$WaveformDataFetcherCallback;

    .prologue
    .line 121
    invoke-static {p1}, Lcom/sec/android/app/ve/util/WaveformUtil;->generateBufferFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 122
    .local v1, "dataFileName":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 123
    .local v0, "bufferFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 125
    new-instance v2, Lcom/sec/android/app/ve/data/WaveformDAO;

    invoke-direct {v2}, Lcom/sec/android/app/ve/data/WaveformDAO;-><init>()V

    .line 126
    .local v2, "waveformDAO":Lcom/sec/android/app/ve/data/WaveformDAO;
    invoke-virtual {v2, p1}, Lcom/sec/android/app/ve/data/WaveformDAO;->setMediaSourcePath(Ljava/lang/String;)V

    .line 127
    invoke-virtual {v2, v1}, Lcom/sec/android/app/ve/data/WaveformDAO;->setDataFileName(Ljava/lang/String;)V

    .line 128
    invoke-direct {p0, v2}, Lcom/sec/android/app/ve/util/WaveformUtil;->startDataLoadingFromFile(Lcom/sec/android/app/ve/data/WaveformDAO;)V

    .line 129
    invoke-virtual {v2}, Lcom/sec/android/app/ve/data/WaveformDAO;->getWaveformData()Ljava/nio/ByteBuffer;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 130
    invoke-direct {p0, v2}, Lcom/sec/android/app/ve/util/WaveformUtil;->addWaveform(Lcom/sec/android/app/ve/data/WaveformDAO;)Z

    .line 132
    :cond_0
    invoke-interface {p2, v2}, Lcom/sec/android/app/ve/thread/WaveformDataFetcher$WaveformDataFetcherCallback;->fetchingComplete(Lcom/sec/android/app/ve/data/WaveformDAO;)Z

    .line 146
    .end local v2    # "waveformDAO":Lcom/sec/android/app/ve/data/WaveformDAO;
    :goto_0
    return-void

    .line 136
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/ve/util/WaveformUtil;->fetcherThread:Lcom/sec/android/app/ve/thread/WaveformDataFetcher;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/sec/android/app/ve/util/WaveformUtil;->fetcherThread:Lcom/sec/android/app/ve/thread/WaveformDataFetcher;

    invoke-virtual {v3}, Lcom/sec/android/app/ve/thread/WaveformDataFetcher;->isAlive()Z

    move-result v3

    if-nez v3, :cond_3

    .line 137
    :cond_2
    new-instance v3, Lcom/sec/android/app/ve/thread/WaveformDataFetcher;

    invoke-direct {v3}, Lcom/sec/android/app/ve/thread/WaveformDataFetcher;-><init>()V

    iput-object v3, p0, Lcom/sec/android/app/ve/util/WaveformUtil;->fetcherThread:Lcom/sec/android/app/ve/thread/WaveformDataFetcher;

    .line 138
    iget-object v3, p0, Lcom/sec/android/app/ve/util/WaveformUtil;->fetcherThread:Lcom/sec/android/app/ve/thread/WaveformDataFetcher;

    iget-object v4, p0, Lcom/sec/android/app/ve/util/WaveformUtil;->utilCallback:Lcom/sec/android/app/ve/thread/WaveformDataFetcher$WaveformDataFetcherCallback;

    invoke-virtual {v3, v4}, Lcom/sec/android/app/ve/thread/WaveformDataFetcher;->setWaveformUtilCallback(Lcom/sec/android/app/ve/thread/WaveformDataFetcher$WaveformDataFetcherCallback;)V

    .line 139
    iget-object v3, p0, Lcom/sec/android/app/ve/util/WaveformUtil;->fetcherThread:Lcom/sec/android/app/ve/thread/WaveformDataFetcher;

    invoke-virtual {v3}, Lcom/sec/android/app/ve/thread/WaveformDataFetcher;->start()V

    .line 142
    :cond_3
    invoke-virtual {p0, p2}, Lcom/sec/android/app/ve/util/WaveformUtil;->registerWaveformFetcherCallback(Lcom/sec/android/app/ve/thread/WaveformDataFetcher$WaveformDataFetcherCallback;)V

    .line 143
    iget-object v3, p0, Lcom/sec/android/app/ve/util/WaveformUtil;->fetcherThread:Lcom/sec/android/app/ve/thread/WaveformDataFetcher;

    invoke-virtual {v3, p1}, Lcom/sec/android/app/ve/thread/WaveformDataFetcher;->addOperation(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public clearAllWaveforms()V
    .locals 1

    .prologue
    .line 222
    iget-object v0, p0, Lcom/sec/android/app/ve/util/WaveformUtil;->allWaveforms:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    .line 223
    iget-object v0, p0, Lcom/sec/android/app/ve/util/WaveformUtil;->allWaveforms:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 224
    :cond_0
    return-void
.end method

.method public deRegisterWaveformFetcherCallback(Lcom/sec/android/app/ve/thread/WaveformDataFetcher$WaveformDataFetcherCallback;)V
    .locals 1
    .param p1, "callback"    # Lcom/sec/android/app/ve/thread/WaveformDataFetcher$WaveformDataFetcherCallback;

    .prologue
    .line 194
    iget-object v0, p0, Lcom/sec/android/app/ve/util/WaveformUtil;->clientsCallback:Ljava/util/concurrent/CopyOnWriteArrayList;

    if-eqz v0, :cond_0

    .line 195
    iget-object v0, p0, Lcom/sec/android/app/ve/util/WaveformUtil;->clientsCallback:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 197
    :cond_0
    return-void
.end method

.method public genWaveformForFile(Ljava/lang/String;Lcom/sec/android/app/ve/thread/WaveformDataFetcher$WaveformDataFetcherCallback;)V
    .locals 2
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "callback"    # Lcom/sec/android/app/ve/thread/WaveformDataFetcher$WaveformDataFetcherCallback;

    .prologue
    .line 101
    const/4 v0, 0x0

    .line 102
    .local v0, "waveformDAO":Lcom/sec/android/app/ve/data/WaveformDAO;
    iget-object v1, p0, Lcom/sec/android/app/ve/util/WaveformUtil;->allWaveforms:Ljava/util/HashMap;

    if-eqz v1, :cond_0

    .line 103
    iget-object v1, p0, Lcom/sec/android/app/ve/util/WaveformUtil;->allWaveforms:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "waveformDAO":Lcom/sec/android/app/ve/data/WaveformDAO;
    check-cast v0, Lcom/sec/android/app/ve/data/WaveformDAO;

    .line 105
    .restart local v0    # "waveformDAO":Lcom/sec/android/app/ve/data/WaveformDAO;
    :cond_0
    if-eqz v0, :cond_1

    .line 106
    invoke-interface {p2, v0}, Lcom/sec/android/app/ve/thread/WaveformDataFetcher$WaveformDataFetcherCallback;->fetchingComplete(Lcom/sec/android/app/ve/data/WaveformDAO;)Z

    .line 111
    :goto_0
    return-void

    .line 109
    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/ve/util/WaveformUtil;->startWaveformLoading(Ljava/lang/String;Lcom/sec/android/app/ve/thread/WaveformDataFetcher$WaveformDataFetcherCallback;)V

    goto :goto_0
.end method

.method public registerWaveformFetcherCallback(Lcom/sec/android/app/ve/thread/WaveformDataFetcher$WaveformDataFetcherCallback;)V
    .locals 1
    .param p1, "callback"    # Lcom/sec/android/app/ve/thread/WaveformDataFetcher$WaveformDataFetcherCallback;

    .prologue
    .line 182
    iget-object v0, p0, Lcom/sec/android/app/ve/util/WaveformUtil;->clientsCallback:Ljava/util/concurrent/CopyOnWriteArrayList;

    if-nez v0, :cond_0

    .line 183
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/ve/util/WaveformUtil;->clientsCallback:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 185
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/ve/util/WaveformUtil;->clientsCallback:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 186
    iget-object v0, p0, Lcom/sec/android/app/ve/util/WaveformUtil;->clientsCallback:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 187
    :cond_1
    return-void
.end method

.method public terminate()V
    .locals 1

    .prologue
    .line 228
    invoke-virtual {p0}, Lcom/sec/android/app/ve/util/WaveformUtil;->clearAllWaveforms()V

    .line 229
    iget-object v0, p0, Lcom/sec/android/app/ve/util/WaveformUtil;->fetcherThread:Lcom/sec/android/app/ve/thread/WaveformDataFetcher;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/ve/util/WaveformUtil;->fetcherThread:Lcom/sec/android/app/ve/thread/WaveformDataFetcher;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/thread/WaveformDataFetcher;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 230
    iget-object v0, p0, Lcom/sec/android/app/ve/util/WaveformUtil;->fetcherThread:Lcom/sec/android/app/ve/thread/WaveformDataFetcher;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/thread/WaveformDataFetcher;->terminate()V

    .line 231
    :cond_0
    return-void
.end method
