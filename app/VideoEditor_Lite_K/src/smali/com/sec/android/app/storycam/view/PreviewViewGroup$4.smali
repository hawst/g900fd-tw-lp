.class Lcom/sec/android/app/storycam/view/PreviewViewGroup$4;
.super Ljava/lang/Object;
.source "PreviewViewGroup.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/storycam/view/PreviewViewGroup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$4;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    .line 623
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 628
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$4;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    invoke-virtual {v0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->isPreviewPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 629
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$4;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    invoke-virtual {v0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->pause_player()V

    .line 636
    :goto_0
    return-void

    .line 631
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$4;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    # getter for: Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;
    invoke-static {v0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$4(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 632
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$4;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    # getter for: Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;
    invoke-static {v0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$4(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;->saveCaption()V

    .line 633
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$4;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    invoke-virtual {v0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->start_play()I

    goto :goto_0
.end method
