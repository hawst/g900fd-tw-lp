.class public Lcom/sec/android/app/ve/thread/LibInitDeinitForElementTask;
.super Lcom/sec/android/app/ve/thread/SimpleTask;
.source "LibInitDeinitForElementTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/ve/thread/LibInitDeinitForElementTask$LibInitForElementTaskListener;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private mEle:Lcom/samsung/app/video/editor/external/Element;

.field private mIsInitCall:Z

.field private mListener:Lcom/sec/android/app/ve/thread/LibInitDeinitForElementTask$LibInitForElementTaskListener;

.field private mTaskPool:Lcom/sec/android/app/ve/thread/AsyncTaskPool;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10
    const-class v0, Lcom/sec/android/app/ve/thread/LibInitDeinitForElementTask;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/app/ve/thread/LibInitDeinitForElementTask;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/sec/android/app/ve/thread/AsyncTaskPool;Lcom/samsung/app/video/editor/external/Element;Lcom/sec/android/app/ve/thread/LibInitDeinitForElementTask$LibInitForElementTaskListener;Z)V
    .locals 0
    .param p1, "pool"    # Lcom/sec/android/app/ve/thread/AsyncTaskPool;
    .param p2, "ele"    # Lcom/samsung/app/video/editor/external/Element;
    .param p3, "listener"    # Lcom/sec/android/app/ve/thread/LibInitDeinitForElementTask$LibInitForElementTaskListener;
    .param p4, "isInitCall"    # Z

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/sec/android/app/ve/thread/SimpleTask;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/sec/android/app/ve/thread/LibInitDeinitForElementTask;->mTaskPool:Lcom/sec/android/app/ve/thread/AsyncTaskPool;

    .line 18
    iput-object p2, p0, Lcom/sec/android/app/ve/thread/LibInitDeinitForElementTask;->mEle:Lcom/samsung/app/video/editor/external/Element;

    .line 19
    iput-object p3, p0, Lcom/sec/android/app/ve/thread/LibInitDeinitForElementTask;->mListener:Lcom/sec/android/app/ve/thread/LibInitDeinitForElementTask$LibInitForElementTaskListener;

    .line 20
    iput-boolean p4, p0, Lcom/sec/android/app/ve/thread/LibInitDeinitForElementTask;->mIsInitCall:Z

    .line 21
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 70
    if-ne p0, p1, :cond_1

    .line 81
    :cond_0
    :goto_0
    return v1

    .line 73
    :cond_1
    instance-of v3, p1, Lcom/sec/android/app/ve/thread/LibInitDeinitForElementTask;

    if-nez v3, :cond_2

    move v1, v2

    .line 74
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 76
    check-cast v0, Lcom/sec/android/app/ve/thread/LibInitDeinitForElementTask;

    .line 77
    .local v0, "task":Lcom/sec/android/app/ve/thread/LibInitDeinitForElementTask;
    iget-object v3, v0, Lcom/sec/android/app/ve/thread/LibInitDeinitForElementTask;->mEle:Lcom/samsung/app/video/editor/external/Element;

    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/Element;->getFilePath()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/ve/thread/LibInitDeinitForElementTask;->mEle:Lcom/samsung/app/video/editor/external/Element;

    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/Element;->getFilePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-eqz v3, :cond_0

    move v1, v2

    .line 81
    goto :goto_0
.end method

.method protected getParam()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 25
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getPool()Lcom/sec/android/app/ve/thread/AsyncTaskPool;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/ve/thread/LibInitDeinitForElementTask;->mTaskPool:Lcom/sec/android/app/ve/thread/AsyncTaskPool;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 63
    sget-boolean v0, Lcom/sec/android/app/ve/thread/LibInitDeinitForElementTask;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "hashCode not designed"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 64
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method protected onBackgroundTask(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "param"    # Ljava/lang/Object;

    .prologue
    .line 30
    iget-boolean v0, p0, Lcom/sec/android/app/ve/thread/LibInitDeinitForElementTask;->mIsInitCall:Z

    if-eqz v0, :cond_0

    .line 31
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/ve/thread/LibInitDeinitForElementTask;->mEle:Lcom/samsung/app/video/editor/external/Element;

    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/Element;->getFilePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/app/video/editor/external/NativeInterface;->EngineInitImage(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 32
    iget-object v0, p0, Lcom/sec/android/app/ve/thread/LibInitDeinitForElementTask;->mEle:Lcom/samsung/app/video/editor/external/Element;

    invoke-virtual {v0}, Lcom/samsung/app/video/editor/external/Element;->getFilePath()Ljava/lang/String;

    move-result-object v0

    .line 39
    :goto_0
    return-object v0

    .line 36
    :cond_0
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/ve/thread/LibInitDeinitForElementTask;->mEle:Lcom/samsung/app/video/editor/external/Element;

    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/Element;->getFilePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/app/video/editor/external/NativeInterface;->EngineDeinitImage(Ljava/lang/String;)V

    .line 37
    iget-object v0, p0, Lcom/sec/android/app/ve/thread/LibInitDeinitForElementTask;->mEle:Lcom/samsung/app/video/editor/external/Element;

    invoke-virtual {v0}, Lcom/samsung/app/video/editor/external/Element;->getFilePath()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 39
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onUIThreadFinally(Ljava/lang/Object;)V
    .locals 4
    .param p1, "result"    # Ljava/lang/Object;

    .prologue
    .line 44
    iget-boolean v0, p0, Lcom/sec/android/app/ve/thread/LibInitDeinitForElementTask;->mIsInitCall:Z

    if-eqz v0, :cond_1

    .line 45
    if-eqz p1, :cond_0

    .line 46
    iget-object v0, p0, Lcom/sec/android/app/ve/thread/LibInitDeinitForElementTask;->mListener:Lcom/sec/android/app/ve/thread/LibInitDeinitForElementTask$LibInitForElementTaskListener;

    check-cast p1, Ljava/lang/String;

    .end local p1    # "result":Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/android/app/ve/thread/LibInitDeinitForElementTask;->mEle:Lcom/samsung/app/video/editor/external/Element;

    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/Element;->getStartTime()J

    move-result-wide v2

    invoke-interface {v0, p1, v2, v3}, Lcom/sec/android/app/ve/thread/LibInitDeinitForElementTask$LibInitForElementTaskListener;->onMediaInitialized(Ljava/lang/String;J)V

    .line 50
    :cond_0
    :goto_0
    return-void

    .line 49
    .restart local p1    # "result":Ljava/lang/Object;
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/ve/thread/LibInitDeinitForElementTask;->mListener:Lcom/sec/android/app/ve/thread/LibInitDeinitForElementTask$LibInitForElementTaskListener;

    check-cast p1, Ljava/lang/String;

    .end local p1    # "result":Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/android/app/ve/thread/LibInitDeinitForElementTask;->mEle:Lcom/samsung/app/video/editor/external/Element;

    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/Element;->getStartTime()J

    move-result-wide v2

    invoke-interface {v0, p1, v2, v3}, Lcom/sec/android/app/ve/thread/LibInitDeinitForElementTask$LibInitForElementTaskListener;->onMediaDeinitialized(Ljava/lang/String;J)V

    goto :goto_0
.end method
