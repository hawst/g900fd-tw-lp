.class public abstract Lcom/sec/android/app/ve/PreviewPlayerInterface;
.super Landroid/widget/RelativeLayout;
.source "PreviewPlayerInterface.java"

# interfaces
.implements Lcom/sec/android/app/ve/PlayerInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/ve/PreviewPlayerInterface$Adapter;
    }
.end annotation


# instance fields
.field protected isSurfaceAvailable:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 26
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 47
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/ve/PreviewPlayerInterface;->isSurfaceAvailable:Z

    .line 27
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 47
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/ve/PreviewPlayerInterface;->isSurfaceAvailable:Z

    .line 23
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 18
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 47
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/ve/PreviewPlayerInterface;->isSurfaceAvailable:Z

    .line 19
    return-void
.end method


# virtual methods
.method public clearNativeJobBeforeExport()Z
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x1

    return v0
.end method

.method public abstract clearPendingPreviewRequest()V
.end method

.method public abstract getSurfaceView()Landroid/view/SurfaceView;
.end method

.method public isSurfaceAvailable()Z
    .locals 1

    .prologue
    .line 49
    iget-boolean v0, p0, Lcom/sec/android/app/ve/PreviewPlayerInterface;->isSurfaceAvailable:Z

    return v0
.end method

.method public abstract setAdapter(Lcom/sec/android/app/ve/PreviewPlayerInterface$Adapter;)V
.end method

.method public setEngineSurface()V
    .locals 0

    .prologue
    .line 41
    return-void
.end method

.method public abstract setFixedSurfaceRect(Landroid/graphics/Rect;)V
.end method

.method public setUIThreadIsLoaded(Z)V
    .locals 0
    .param p1, "loaded"    # Z

    .prologue
    .line 45
    return-void
.end method

.method public abstract update(Lcom/samsung/app/video/editor/external/Element;Lcom/samsung/app/video/editor/external/Element;JIFLjava/util/List;Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/app/video/editor/external/Element;",
            "Lcom/samsung/app/video/editor/external/Element;",
            "JIF",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/ClipartParams;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/ClipartParams;",
            ">;)V"
        }
    .end annotation
.end method
