.class public Lcom/sec/android/app/ve/thumbcache/FolderThumbnailFetcherTask;
.super Lcom/sec/android/app/ve/thread/SimpleTask;
.source "FolderThumbnailFetcherTask.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final CANVAS_HEIGHT:I

.field private static final CANVAS_WIDTH:I

.field private static final RADIUS:I

.field private static final ROT_THUMB_HEIGHT:I

.field private static final ROT_THUMB_WIDTH:I

.field private static final STROKE_THICKNESS:I = 0x2

.field private static final THUMB_HEIGHT:I

.field private static final THUMB_WIDTH:I

.field private static final mRotAngles:[F


# instance fields
.field private mCallBack:Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcher$Adapter;

.field private mElementType:I

.field private final mPaint:Landroid/graphics/Paint;

.field private mPos:I

.field private mTaskPool:Lcom/sec/android/app/ve/thread/AsyncTaskPool;

.field private mThumbnailPaths:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lcom/sec/android/app/ve/thumbcache/FolderThumbnailFetcherTask;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/app/ve/thumbcache/FolderThumbnailFetcherTask;->$assertionsDisabled:Z

    .line 26
    const/4 v0, 0x3

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/android/app/ve/thumbcache/FolderThumbnailFetcherTask;->mRotAngles:[F

    .line 27
    sget v0, Lcom/sec/android/app/ve/R$dimen;->image_picker_folder_canvas_width:I

    invoke-static {v0}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v0

    sput v0, Lcom/sec/android/app/ve/thumbcache/FolderThumbnailFetcherTask;->CANVAS_WIDTH:I

    .line 28
    sget v0, Lcom/sec/android/app/ve/R$dimen;->image_picker_folder_canvas_height:I

    invoke-static {v0}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v0

    sput v0, Lcom/sec/android/app/ve/thumbcache/FolderThumbnailFetcherTask;->CANVAS_HEIGHT:I

    .line 29
    sget v0, Lcom/sec/android/app/ve/R$dimen;->image_picker_folder_thumb_width:I

    invoke-static {v0}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v0

    sput v0, Lcom/sec/android/app/ve/thumbcache/FolderThumbnailFetcherTask;->THUMB_WIDTH:I

    .line 30
    sget v0, Lcom/sec/android/app/ve/R$dimen;->image_picker_folder_thumb_height:I

    invoke-static {v0}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v0

    sput v0, Lcom/sec/android/app/ve/thumbcache/FolderThumbnailFetcherTask;->THUMB_HEIGHT:I

    .line 31
    sget v0, Lcom/sec/android/app/ve/R$dimen;->image_picker_folder_rot_thumb_width:I

    invoke-static {v0}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v0

    sput v0, Lcom/sec/android/app/ve/thumbcache/FolderThumbnailFetcherTask;->ROT_THUMB_WIDTH:I

    .line 32
    sget v0, Lcom/sec/android/app/ve/R$dimen;->image_picker_folder_rot_thumb_height:I

    invoke-static {v0}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v0

    sput v0, Lcom/sec/android/app/ve/thumbcache/FolderThumbnailFetcherTask;->ROT_THUMB_HEIGHT:I

    .line 33
    sget v0, Lcom/sec/android/app/ve/R$dimen;->addmedia_imagevideo_corner_radius:I

    invoke-static {v0}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v0

    sput v0, Lcom/sec/android/app/ve/thumbcache/FolderThumbnailFetcherTask;->RADIUS:I

    .line 34
    return-void

    .line 25
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 26
    :array_0
    .array-data 4
        0x0
        -0x3f800000    # -4.0f
        0x40400000    # 3.0f
    .end array-data
.end method

.method public constructor <init>(Ljava/util/ArrayList;ILcom/sec/android/app/ve/thread/AsyncTaskPool;Lcom/sec/android/app/ve/thumbcache/DiskLruCache;Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcher$Adapter;I)V
    .locals 2
    .param p2, "pos"    # I
    .param p3, "pool"    # Lcom/sec/android/app/ve/thread/AsyncTaskPool;
    .param p4, "cache"    # Lcom/sec/android/app/ve/thumbcache/DiskLruCache;
    .param p5, "callBack"    # Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcher$Adapter;
    .param p6, "elementType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;I",
            "Lcom/sec/android/app/ve/thread/AsyncTaskPool;",
            "Lcom/sec/android/app/ve/thumbcache/DiskLruCache;",
            "Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcher$Adapter;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 43
    .local p1, "filePaths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct {p0}, Lcom/sec/android/app/ve/thread/SimpleTask;-><init>()V

    .line 41
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x7

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/ve/thumbcache/FolderThumbnailFetcherTask;->mPaint:Landroid/graphics/Paint;

    .line 44
    iput p2, p0, Lcom/sec/android/app/ve/thumbcache/FolderThumbnailFetcherTask;->mPos:I

    .line 45
    iput-object p1, p0, Lcom/sec/android/app/ve/thumbcache/FolderThumbnailFetcherTask;->mThumbnailPaths:Ljava/util/ArrayList;

    .line 46
    iput-object p3, p0, Lcom/sec/android/app/ve/thumbcache/FolderThumbnailFetcherTask;->mTaskPool:Lcom/sec/android/app/ve/thread/AsyncTaskPool;

    .line 47
    iput-object p5, p0, Lcom/sec/android/app/ve/thumbcache/FolderThumbnailFetcherTask;->mCallBack:Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcher$Adapter;

    .line 48
    iput p6, p0, Lcom/sec/android/app/ve/thumbcache/FolderThumbnailFetcherTask;->mElementType:I

    .line 49
    iget-object v0, p0, Lcom/sec/android/app/ve/thumbcache/FolderThumbnailFetcherTask;->mPaint:Landroid/graphics/Paint;

    sget v1, Lcom/sec/android/app/ve/R$color;->folderview_thumb_frameColor:I

    invoke-static {v1}, Lcom/sec/android/app/ve/VEApp;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 50
    return-void
.end method


# virtual methods
.method createFolderViewBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    .locals 25
    .param p1, "src"    # Landroid/graphics/Bitmap;
    .param p2, "index"    # I

    .prologue
    .line 141
    if-nez p1, :cond_0

    .line 142
    const/4 v4, 0x0

    .line 215
    :goto_0
    return-object v4

    .line 144
    :cond_0
    if-nez p2, :cond_3

    sget v18, Lcom/sec/android/app/ve/thumbcache/FolderThumbnailFetcherTask;->THUMB_WIDTH:I

    .line 145
    .local v18, "dstWidth":I
    :goto_1
    if-nez p2, :cond_4

    sget v17, Lcom/sec/android/app/ve/thumbcache/FolderThumbnailFetcherTask;->THUMB_HEIGHT:I

    .line 147
    .local v17, "dstHeight":I
    :goto_2
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    int-to-float v4, v4

    move/from16 v0, v18

    int-to-float v11, v0

    div-float v24, v4, v11

    .line 148
    .local v24, "widRatio":F
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    int-to-float v4, v4

    move/from16 v0, v17

    int-to-float v11, v0

    div-float v19, v4, v11

    .line 150
    .local v19, "heiRatio":F
    new-instance v9, Landroid/graphics/Matrix;

    invoke-direct {v9}, Landroid/graphics/Matrix;-><init>()V

    .line 151
    .local v9, "mat":Landroid/graphics/Matrix;
    invoke-virtual {v9}, Landroid/graphics/Matrix;->reset()V

    .line 153
    const/4 v5, 0x0

    .line 154
    .local v5, "x":I
    const/4 v6, 0x0

    .line 155
    .local v6, "y":I
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    .line 156
    .local v7, "width":I
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    .line 158
    .local v8, "height":I
    cmpg-float v4, v24, v19

    if-gez v4, :cond_5

    .line 160
    move/from16 v0, v17

    int-to-float v4, v0

    mul-float v4, v4, v24

    float-to-int v8, v4

    .line 161
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    sub-int/2addr v4, v8

    div-int/lit8 v6, v4, 0x2

    .line 162
    const/high16 v4, 0x3f800000    # 1.0f

    div-float v4, v4, v24

    const/high16 v11, 0x3f800000    # 1.0f

    div-float v11, v11, v24

    invoke-virtual {v9, v4, v11}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 171
    :goto_3
    const/4 v10, 0x1

    move-object/from16 v4, p1

    invoke-static/range {v4 .. v10}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v23

    .line 172
    .local v23, "temp":Landroid/graphics/Bitmap;
    const/4 v10, 0x0

    .line 174
    .local v10, "rndBmp":Landroid/graphics/Bitmap;
    if-eqz v23, :cond_1

    .line 176
    invoke-virtual/range {v23 .. v23}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    invoke-virtual/range {v23 .. v23}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v11

    sget-object v12, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v4, v11, v12}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v10

    .line 177
    new-instance v22, Landroid/graphics/Canvas;

    move-object/from16 v0, v22

    invoke-direct {v0, v10}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 178
    .local v22, "rndCnvs":Landroid/graphics/Canvas;
    new-instance v21, Landroid/graphics/RectF;

    const/4 v4, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {v23 .. v23}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v12

    int-to-float v12, v12

    invoke-virtual/range {v23 .. v23}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v13

    int-to-float v13, v13

    move-object/from16 v0, v21

    invoke-direct {v0, v4, v11, v12, v13}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 180
    .local v21, "rect":Landroid/graphics/RectF;
    new-instance v20, Landroid/graphics/Paint;

    invoke-direct/range {v20 .. v20}, Landroid/graphics/Paint;-><init>()V

    .line 181
    .local v20, "paint":Landroid/graphics/Paint;
    const/4 v4, 0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 182
    const/high16 v4, -0x1000000

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 183
    const/4 v4, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v0, v4, v11, v12, v13}, Landroid/graphics/Canvas;->drawARGB(IIII)V

    .line 184
    sget v4, Lcom/sec/android/app/ve/thumbcache/FolderThumbnailFetcherTask;->RADIUS:I

    int-to-float v4, v4

    sget v11, Lcom/sec/android/app/ve/thumbcache/FolderThumbnailFetcherTask;->RADIUS:I

    int-to-float v11, v11

    move-object/from16 v0, v22

    move-object/from16 v1, v21

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v4, v11, v2}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 186
    new-instance v20, Landroid/graphics/Paint;

    .end local v20    # "paint":Landroid/graphics/Paint;
    invoke-direct/range {v20 .. v20}, Landroid/graphics/Paint;-><init>()V

    .line 187
    .restart local v20    # "paint":Landroid/graphics/Paint;
    const/4 v4, 0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 188
    new-instance v4, Landroid/graphics/PorterDuffXfermode;

    sget-object v11, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v4, v11}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 189
    invoke-virtual/range {v22 .. v22}, Landroid/graphics/Canvas;->getClipBounds()Landroid/graphics/Rect;

    move-result-object v4

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    move-object/from16 v2, v21

    move-object/from16 v3, v20

    invoke-virtual {v0, v1, v4, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 191
    new-instance v20, Landroid/graphics/Paint;

    .end local v20    # "paint":Landroid/graphics/Paint;
    invoke-direct/range {v20 .. v20}, Landroid/graphics/Paint;-><init>()V

    .line 192
    .restart local v20    # "paint":Landroid/graphics/Paint;
    const/4 v4, 0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 193
    const/high16 v4, -0x1000000

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 194
    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 195
    const/high16 v4, 0x40000000    # 2.0f

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 196
    sget v4, Lcom/sec/android/app/ve/thumbcache/FolderThumbnailFetcherTask;->RADIUS:I

    int-to-float v4, v4

    sget v11, Lcom/sec/android/app/ve/thumbcache/FolderThumbnailFetcherTask;->RADIUS:I

    int-to-float v11, v11

    move-object/from16 v0, v22

    move-object/from16 v1, v21

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v4, v11, v2}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 198
    invoke-virtual/range {v23 .. v23}, Landroid/graphics/Bitmap;->recycle()V

    .line 199
    const/16 v23, 0x0

    .line 202
    .end local v20    # "paint":Landroid/graphics/Paint;
    .end local v21    # "rect":Landroid/graphics/RectF;
    .end local v22    # "rndCnvs":Landroid/graphics/Canvas;
    :cond_1
    if-eqz v10, :cond_2

    .line 204
    invoke-virtual {v9}, Landroid/graphics/Matrix;->reset()V

    .line 205
    sget-object v4, Lcom/sec/android/app/ve/thumbcache/FolderThumbnailFetcherTask;->mRotAngles:[F

    aget v4, v4, p2

    invoke-virtual {v9, v4}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 207
    const/4 v11, 0x0

    const/4 v12, 0x0

    .line 208
    invoke-virtual {v10}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v13

    invoke-virtual {v10}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v14

    const/16 v16, 0x1

    move-object v15, v9

    .line 207
    invoke-static/range {v10 .. v16}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object p1

    .line 210
    invoke-virtual {v10}, Landroid/graphics/Bitmap;->recycle()V

    .line 211
    const/4 v10, 0x0

    .line 214
    :cond_2
    invoke-static {}, Ljava/lang/System;->gc()V

    move-object/from16 v4, p1

    .line 215
    goto/16 :goto_0

    .line 144
    .end local v5    # "x":I
    .end local v6    # "y":I
    .end local v7    # "width":I
    .end local v8    # "height":I
    .end local v9    # "mat":Landroid/graphics/Matrix;
    .end local v10    # "rndBmp":Landroid/graphics/Bitmap;
    .end local v17    # "dstHeight":I
    .end local v18    # "dstWidth":I
    .end local v19    # "heiRatio":F
    .end local v23    # "temp":Landroid/graphics/Bitmap;
    .end local v24    # "widRatio":F
    :cond_3
    sget v18, Lcom/sec/android/app/ve/thumbcache/FolderThumbnailFetcherTask;->ROT_THUMB_WIDTH:I

    goto/16 :goto_1

    .line 145
    .restart local v18    # "dstWidth":I
    :cond_4
    sget v17, Lcom/sec/android/app/ve/thumbcache/FolderThumbnailFetcherTask;->ROT_THUMB_HEIGHT:I

    goto/16 :goto_2

    .line 166
    .restart local v5    # "x":I
    .restart local v6    # "y":I
    .restart local v7    # "width":I
    .restart local v8    # "height":I
    .restart local v9    # "mat":Landroid/graphics/Matrix;
    .restart local v17    # "dstHeight":I
    .restart local v19    # "heiRatio":F
    .restart local v24    # "widRatio":F
    :cond_5
    move/from16 v0, v18

    int-to-float v4, v0

    mul-float v4, v4, v19

    float-to-int v7, v4

    .line 167
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    sub-int/2addr v4, v7

    div-int/lit8 v5, v4, 0x2

    .line 168
    const/high16 v4, 0x3f800000    # 1.0f

    div-float v4, v4, v19

    const/high16 v11, 0x3f800000    # 1.0f

    div-float v11, v11, v19

    invoke-virtual {v9, v4, v11}, Landroid/graphics/Matrix;->postScale(FF)Z

    goto/16 :goto_3
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "taskObj"    # Ljava/lang/Object;

    .prologue
    .line 225
    instance-of v1, p1, Lcom/sec/android/app/ve/thumbcache/FolderThumbnailFetcherTask;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 226
    check-cast v0, Lcom/sec/android/app/ve/thumbcache/FolderThumbnailFetcherTask;

    .line 227
    .local v0, "task":Lcom/sec/android/app/ve/thumbcache/FolderThumbnailFetcherTask;
    iget v1, v0, Lcom/sec/android/app/ve/thumbcache/FolderThumbnailFetcherTask;->mPos:I

    iget v2, p0, Lcom/sec/android/app/ve/thumbcache/FolderThumbnailFetcherTask;->mPos:I

    if-ne v1, v2, :cond_0

    iget-object v1, v0, Lcom/sec/android/app/ve/thumbcache/FolderThumbnailFetcherTask;->mThumbnailPaths:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/ve/thumbcache/FolderThumbnailFetcherTask;->mThumbnailPaths:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/sec/android/app/ve/thumbcache/FolderThumbnailFetcherTask;->mThumbnailPaths:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/ve/thumbcache/FolderThumbnailFetcherTask;->mThumbnailPaths:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 228
    iget-object v1, p0, Lcom/sec/android/app/ve/thumbcache/FolderThumbnailFetcherTask;->mThumbnailPaths:Ljava/util/ArrayList;

    iget-object v2, v0, Lcom/sec/android/app/ve/thumbcache/FolderThumbnailFetcherTask;->mThumbnailPaths:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->containsAll(Ljava/util/Collection;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 229
    const/4 v1, 0x1

    .line 232
    .end local v0    # "task":Lcom/sec/android/app/ve/thumbcache/FolderThumbnailFetcherTask;
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected getParam()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 125
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getPool()Lcom/sec/android/app/ve/thread/AsyncTaskPool;
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/app/ve/thumbcache/FolderThumbnailFetcherTask;->mTaskPool:Lcom/sec/android/app/ve/thread/AsyncTaskPool;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 219
    sget-boolean v0, Lcom/sec/android/app/ve/thumbcache/FolderThumbnailFetcherTask;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "hashCode not designed"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 220
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method protected onBackgroundTask(Ljava/lang/Object;)Landroid/graphics/Bitmap;
    .locals 17
    .param p1, "param"    # Ljava/lang/Object;

    .prologue
    .line 54
    const/4 v1, 0x0

    .line 55
    .local v1, "compThumb":Landroid/graphics/Bitmap;
    new-instance v9, Landroid/graphics/Matrix;

    invoke-direct {v9}, Landroid/graphics/Matrix;-><init>()V

    .line 57
    .local v9, "mat":Landroid/graphics/Matrix;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/ve/thumbcache/FolderThumbnailFetcherTask;->mThumbnailPaths:Ljava/util/ArrayList;

    if-eqz v13, :cond_0

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/ve/thumbcache/FolderThumbnailFetcherTask;->mThumbnailPaths:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v13

    if-nez v13, :cond_0

    .line 59
    sget v13, Lcom/sec/android/app/ve/thumbcache/FolderThumbnailFetcherTask;->CANVAS_WIDTH:I

    sget v14, Lcom/sec/android/app/ve/thumbcache/FolderThumbnailFetcherTask;->CANVAS_HEIGHT:I

    sget-object v15, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v13, v14, v15}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 60
    .local v4, "finalBitmap":Landroid/graphics/Bitmap;
    new-instance v5, Landroid/graphics/Canvas;

    invoke-direct {v5, v4}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 61
    .local v5, "finalCanvas":Landroid/graphics/Canvas;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/ve/thumbcache/FolderThumbnailFetcherTask;->mThumbnailPaths:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v13

    add-int/lit8 v6, v13, -0x1

    .local v6, "i":I
    :goto_0
    if-gez v6, :cond_1

    .line 113
    move-object v1, v4

    .line 120
    .end local v4    # "finalBitmap":Landroid/graphics/Bitmap;
    .end local v5    # "finalCanvas":Landroid/graphics/Canvas;
    .end local v6    # "i":I
    :cond_0
    :goto_1
    return-object v1

    .line 63
    .restart local v4    # "finalBitmap":Landroid/graphics/Bitmap;
    .restart local v5    # "finalCanvas":Landroid/graphics/Canvas;
    .restart local v6    # "i":I
    :cond_1
    invoke-virtual {v5}, Landroid/graphics/Canvas;->save()I

    .line 65
    const/4 v11, 0x0

    .line 66
    .local v11, "tempBitmap":Landroid/graphics/Bitmap;
    const/4 v7, 0x0

    .line 68
    .local v7, "imgBitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v9}, Landroid/graphics/Matrix;->reset()V

    .line 69
    sget-object v13, Lcom/sec/android/app/ve/thumbcache/FolderThumbnailFetcherTask;->mRotAngles:[F

    aget v13, v13, v6

    invoke-virtual {v9, v13}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 71
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/ve/thumbcache/FolderThumbnailFetcherTask;->mThumbnailPaths:Ljava/util/ArrayList;

    invoke-virtual {v13, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    sget v14, Lcom/sec/android/app/ve/thumbcache/FolderThumbnailFetcherTask;->THUMB_WIDTH:I

    sget v15, Lcom/sec/android/app/ve/thumbcache/FolderThumbnailFetcherTask;->THUMB_HEIGHT:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/ve/thumbcache/FolderThumbnailFetcherTask;->mElementType:I

    move/from16 v16, v0

    invoke-static/range {v13 .. v16}, Lcom/sec/android/app/ve/common/MediaUtils;->getThumbnail(Ljava/lang/String;III)Landroid/graphics/Bitmap;

    move-result-object v11

    .line 73
    if-eqz v11, :cond_2

    .line 75
    move-object/from16 v0, p0

    invoke-virtual {v0, v11, v6}, Lcom/sec/android/app/ve/thumbcache/FolderThumbnailFetcherTask;->createFolderViewBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 76
    invoke-virtual {v11}, Landroid/graphics/Bitmap;->recycle()V

    .line 77
    const/4 v11, 0x0

    .line 80
    :cond_2
    if-eqz v7, :cond_3

    .line 82
    invoke-virtual {v9}, Landroid/graphics/Matrix;->reset()V

    .line 84
    new-instance v10, Landroid/graphics/Rect;

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v15

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v16

    move/from16 v0, v16

    invoke-direct {v10, v13, v14, v15, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 86
    .local v10, "srcRect":Landroid/graphics/Rect;
    const/4 v8, 0x0

    .line 87
    .local v8, "left":I
    const/4 v12, 0x0

    .line 89
    .local v12, "top":I
    if-nez v6, :cond_4

    .line 91
    sget v13, Lcom/sec/android/app/ve/thumbcache/FolderThumbnailFetcherTask;->CANVAS_WIDTH:I

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v14

    sub-int/2addr v13, v14

    div-int/lit8 v8, v13, 0x3

    .line 92
    sget v13, Lcom/sec/android/app/ve/thumbcache/FolderThumbnailFetcherTask;->CANVAS_HEIGHT:I

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v14

    sub-int/2addr v13, v14

    div-int/lit8 v12, v13, 0x2

    .line 105
    :goto_2
    new-instance v2, Landroid/graphics/Rect;

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v13

    add-int/2addr v13, v8

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v14

    add-int/2addr v14, v12

    invoke-direct {v2, v8, v12, v13, v14}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 107
    .local v2, "dstRect":Landroid/graphics/Rect;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/ve/thumbcache/FolderThumbnailFetcherTask;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v5, v7, v10, v2, v13}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 108
    invoke-virtual {v5}, Landroid/graphics/Canvas;->restore()V

    .line 61
    .end local v2    # "dstRect":Landroid/graphics/Rect;
    .end local v8    # "left":I
    .end local v10    # "srcRect":Landroid/graphics/Rect;
    .end local v12    # "top":I
    :cond_3
    add-int/lit8 v6, v6, -0x1

    goto :goto_0

    .line 94
    .restart local v8    # "left":I
    .restart local v10    # "srcRect":Landroid/graphics/Rect;
    .restart local v12    # "top":I
    :cond_4
    const/4 v13, 0x1

    if-ne v13, v6, :cond_5

    .line 96
    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v13

    sget v14, Lcom/sec/android/app/ve/thumbcache/FolderThumbnailFetcherTask;->CANVAS_WIDTH:I

    sub-int/2addr v13, v14

    mul-int/lit8 v8, v13, 0x8

    .line 97
    sget v13, Lcom/sec/android/app/ve/thumbcache/FolderThumbnailFetcherTask;->CANVAS_HEIGHT:I

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v14

    sub-int v12, v13, v14

    .line 98
    goto :goto_2

    .line 101
    :cond_5
    sget v13, Lcom/sec/android/app/ve/thumbcache/FolderThumbnailFetcherTask;->CANVAS_WIDTH:I

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v14

    sub-int/2addr v13, v14

    div-int/lit8 v8, v13, 0x3

    .line 102
    sget v13, Lcom/sec/android/app/ve/thumbcache/FolderThumbnailFetcherTask;->CANVAS_HEIGHT:I

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v14

    sub-int/2addr v13, v14

    div-int/lit8 v12, v13, 0x2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 116
    .end local v4    # "finalBitmap":Landroid/graphics/Bitmap;
    .end local v5    # "finalCanvas":Landroid/graphics/Canvas;
    .end local v6    # "i":I
    .end local v7    # "imgBitmap":Landroid/graphics/Bitmap;
    .end local v8    # "left":I
    .end local v10    # "srcRect":Landroid/graphics/Rect;
    .end local v11    # "tempBitmap":Landroid/graphics/Bitmap;
    .end local v12    # "top":I
    :catch_0
    move-exception v3

    .line 117
    .local v3, "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_1
.end method

.method protected bridge synthetic onBackgroundTask(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lcom/sec/android/app/ve/thumbcache/FolderThumbnailFetcherTask;->onBackgroundTask(Ljava/lang/Object;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected onUIThreadFinally(Ljava/lang/Object;)V
    .locals 3
    .param p1, "result"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x0

    .line 130
    iget-object v0, p0, Lcom/sec/android/app/ve/thumbcache/FolderThumbnailFetcherTask;->mCallBack:Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcher$Adapter;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 131
    iget-object v0, p0, Lcom/sec/android/app/ve/thumbcache/FolderThumbnailFetcherTask;->mCallBack:Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcher$Adapter;

    iget v1, p0, Lcom/sec/android/app/ve/thumbcache/FolderThumbnailFetcherTask;->mPos:I

    check-cast p1, Landroid/graphics/Bitmap;

    .end local p1    # "result":Ljava/lang/Object;
    invoke-interface {v0, v1, v2, p1, v2}, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcher$Adapter;->onBitmapCreated(ILjava/lang/String;Landroid/graphics/Bitmap;Ljava/lang/Object;)V

    .line 132
    :cond_0
    return-void
.end method
