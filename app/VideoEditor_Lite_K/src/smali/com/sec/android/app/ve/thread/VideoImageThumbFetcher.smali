.class public abstract Lcom/sec/android/app/ve/thread/VideoImageThumbFetcher;
.super Ljava/lang/Thread;
.source "VideoImageThumbFetcher.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/ve/thread/VideoImageThumbFetcher$FetcherInfo;,
        Lcom/sec/android/app/ve/thread/VideoImageThumbFetcher$VideoImageThumbnaiFetcherCallbak;
    }
.end annotation


# instance fields
.field protected mJobList:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Lcom/sec/android/app/ve/thread/VideoImageThumbFetcher$FetcherInfo;",
            ">;"
        }
    .end annotation
.end field

.field protected mTerminate:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public addOperation(Ljava/lang/String;IJLcom/sec/android/app/ve/thread/VideoImageThumbFetcher$VideoImageThumbnaiFetcherCallbak;)V
    .locals 7
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "aPosition"    # I
    .param p3, "aId"    # J
    .param p5, "callback"    # Lcom/sec/android/app/ve/thread/VideoImageThumbFetcher$VideoImageThumbnaiFetcherCallbak;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/android/app/ve/thread/VideoImageThumbFetcher;->mJobList:Ljava/util/Vector;

    new-instance v1, Lcom/sec/android/app/ve/thread/VideoImageThumbFetcher$FetcherInfo;

    move-object v2, p1

    move v3, p2

    move-wide v4, p3

    move-object v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/sec/android/app/ve/thread/VideoImageThumbFetcher$FetcherInfo;-><init>(Ljava/lang/String;IJLcom/sec/android/app/ve/thread/VideoImageThumbFetcher$VideoImageThumbnaiFetcherCallbak;)V

    invoke-virtual {v0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 69
    return-void
.end method

.method protected checkJobExists(Ljava/lang/String;)Z
    .locals 5
    .param p1, "aFilepath"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 24
    iget-object v4, p0, Lcom/sec/android/app/ve/thread/VideoImageThumbFetcher;->mJobList:Ljava/util/Vector;

    if-nez v4, :cond_1

    .line 40
    :cond_0
    :goto_0
    return v3

    .line 29
    :cond_1
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/app/ve/thread/VideoImageThumbFetcher;->mJobList:Ljava/util/Vector;

    invoke-virtual {v4}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 30
    .local v2, "lIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/app/ve/thread/VideoImageThumbFetcher$FetcherInfo;>;"
    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 31
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/ve/thread/VideoImageThumbFetcher$FetcherInfo;

    .line 32
    .local v1, "fetcherInfo":Lcom/sec/android/app/ve/thread/VideoImageThumbFetcher$FetcherInfo;
    iget-object v4, v1, Lcom/sec/android/app/ve/thread/VideoImageThumbFetcher$FetcherInfo;->filePath:Ljava/lang/String;

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    if-eqz v4, :cond_2

    .line 33
    const/4 v3, 0x1

    goto :goto_0

    .line 36
    .end local v1    # "fetcherInfo":Lcom/sec/android/app/ve/thread/VideoImageThumbFetcher$FetcherInfo;
    .end local v2    # "lIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/app/ve/thread/VideoImageThumbFetcher$FetcherInfo;>;"
    :catch_0
    move-exception v0

    .line 38
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public clearPendingOperations()V
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/ve/thread/VideoImageThumbFetcher;->mJobList:Ljava/util/Vector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/ve/thread/VideoImageThumbFetcher;->mJobList:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 74
    iget-object v0, p0, Lcom/sec/android/app/ve/thread/VideoImageThumbFetcher;->mJobList:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clear()V

    .line 75
    :cond_0
    return-void
.end method

.method public init()V
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/ve/thread/VideoImageThumbFetcher;->mJobList:Ljava/util/Vector;

    if-nez v0, :cond_0

    .line 48
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/ve/thread/VideoImageThumbFetcher;->mJobList:Ljava/util/Vector;

    .line 50
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/ve/thread/VideoImageThumbFetcher;->isAlive()Z

    move-result v0

    if-nez v0, :cond_1

    .line 52
    invoke-virtual {p0}, Lcom/sec/android/app/ve/thread/VideoImageThumbFetcher;->start()V

    .line 54
    :cond_1
    return-void
.end method

.method public abstract run()V
.end method

.method public terminate()V
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/android/app/ve/thread/VideoImageThumbFetcher;->mJobList:Ljava/util/Vector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/ve/thread/VideoImageThumbFetcher;->mJobList:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 61
    iget-object v0, p0, Lcom/sec/android/app/ve/thread/VideoImageThumbFetcher;->mJobList:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clear()V

    .line 62
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/ve/thread/VideoImageThumbFetcher;->mTerminate:Z

    .line 63
    return-void
.end method
