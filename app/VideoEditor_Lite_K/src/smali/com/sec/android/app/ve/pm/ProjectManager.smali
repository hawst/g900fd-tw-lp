.class public Lcom/sec/android/app/ve/pm/ProjectManager;
.super Ljava/lang/Object;
.source "ProjectManager.java"


# static fields
.field private static final CREATE_TIME_ORDER:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/samsung/app/video/editor/external/TranscodeElement;",
            ">;"
        }
    .end annotation
.end field

.field public static final PROJECT_EXT:Ljava/lang/String; = ".vep"

.field private static _instance:Lcom/sec/android/app/ve/pm/ProjectManager;

.field static cloneTE:Lcom/samsung/app/video/editor/external/TranscodeElement;

.field public static mTempMMSImage:Ljava/lang/String;


# instance fields
.field private isFileCreated:Z

.field private mDeleteProjectList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/TranscodeElement;",
            ">;"
        }
    .end annotation
.end field

.field private mDrawingClipartsMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/samsung/app/video/editor/external/TranscodeElement;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/app/video/editor/external/ClipartParams;",
            ">;>;"
        }
    .end annotation
.end field

.field private mProjectList:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Lcom/samsung/app/video/editor/external/TranscodeElement;",
            ">;"
        }
    .end annotation
.end field

.field private mTextClipartsMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/samsung/app/video/editor/external/TranscodeElement;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/app/video/editor/external/ClipartParams;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 48
    new-instance v0, Lcom/sec/android/app/ve/pm/ProjectManager;

    invoke-direct {v0}, Lcom/sec/android/app/ve/pm/ProjectManager;-><init>()V

    sput-object v0, Lcom/sec/android/app/ve/pm/ProjectManager;->_instance:Lcom/sec/android/app/ve/pm/ProjectManager;

    .line 49
    sput-object v1, Lcom/sec/android/app/ve/pm/ProjectManager;->cloneTE:Lcom/samsung/app/video/editor/external/TranscodeElement;

    .line 50
    sput-object v1, Lcom/sec/android/app/ve/pm/ProjectManager;->mTempMMSImage:Ljava/lang/String;

    .line 52
    new-instance v0, Lcom/sec/android/app/ve/pm/ProjectManager$1;

    invoke-direct {v0}, Lcom/sec/android/app/ve/pm/ProjectManager$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/ve/pm/ProjectManager;->CREATE_TIME_ORDER:Ljava/util/Comparator;

    .line 61
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    new-instance v0, Lcom/samsung/app/video/editor/external/VEVector;

    invoke-direct {v0}, Lcom/samsung/app/video/editor/external/VEVector;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/ve/pm/ProjectManager;->mDeleteProjectList:Ljava/util/List;

    .line 65
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/ve/pm/ProjectManager;->mTextClipartsMap:Ljava/util/HashMap;

    .line 66
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/ve/pm/ProjectManager;->mDrawingClipartsMap:Ljava/util/HashMap;

    .line 69
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/app/ve/pm/ProjectManager;)Ljava/util/List;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/ve/pm/ProjectManager;->mDeleteProjectList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/app/ve/pm/ProjectManager;Lcom/samsung/app/video/editor/external/TranscodeElement;)V
    .locals 0

    .prologue
    .line 471
    invoke-direct {p0, p1}, Lcom/sec/android/app/ve/pm/ProjectManager;->saveDeletedProject(Lcom/samsung/app/video/editor/external/TranscodeElement;)V

    return-void
.end method

.method static synthetic access$2(Lcom/sec/android/app/ve/pm/ProjectManager;Z)V
    .locals 0

    .prologue
    .line 501
    invoke-direct {p0, p1}, Lcom/sec/android/app/ve/pm/ProjectManager;->clearDeletedProjectList(Z)V

    return-void
.end method

.method private addProject(Lcom/samsung/app/video/editor/external/TranscodeElement;Ljava/lang/String;)Lcom/samsung/app/video/editor/external/TranscodeElement;
    .locals 4
    .param p1, "transcodeElement"    # Lcom/samsung/app/video/editor/external/TranscodeElement;
    .param p2, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 365
    const/4 v1, 0x0

    sput-object v1, Lcom/sec/android/app/ve/pm/ProjectManager;->cloneTE:Lcom/samsung/app/video/editor/external/TranscodeElement;

    .line 367
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getFilesDirectory()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".vep"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 368
    .local v0, "path":Ljava/lang/String;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {p1, v2, v3}, Lcom/samsung/app/video/editor/external/TranscodeElement;->setProjCreationTime(J)V

    .line 370
    invoke-virtual {p1, p2}, Lcom/samsung/app/video/editor/external/TranscodeElement;->setProjectName(Ljava/lang/String;)V

    .line 371
    invoke-virtual {p1, v0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->setProjectFileName(Ljava/lang/String;)V

    .line 374
    invoke-virtual {p0}, Lcom/sec/android/app/ve/pm/ProjectManager;->getProjectList()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 375
    iget-object v1, p0, Lcom/sec/android/app/ve/pm/ProjectManager;->mProjectList:Ljava/util/Vector;

    sget-object v2, Lcom/sec/android/app/ve/pm/ProjectManager;->CREATE_TIME_ORDER:Ljava/util/Comparator;

    invoke-static {v1, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 376
    return-object p1
.end method

.method private clearDeletedProjectList(Z)V
    .locals 2
    .param p1, "alsoDeleteMedia"    # Z

    .prologue
    .line 502
    iget-object v0, p0, Lcom/sec/android/app/ve/pm/ProjectManager;->mTextClipartsMap:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    .line 503
    iget-object v0, p0, Lcom/sec/android/app/ve/pm/ProjectManager;->mTextClipartsMap:Ljava/util/HashMap;

    invoke-direct {p0, v0}, Lcom/sec/android/app/ve/pm/ProjectManager;->forceDeleteCaptionFiles(Ljava/util/HashMap;)V

    .line 504
    iget-object v0, p0, Lcom/sec/android/app/ve/pm/ProjectManager;->mTextClipartsMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 506
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/ve/pm/ProjectManager;->mDrawingClipartsMap:Ljava/util/HashMap;

    if-eqz v0, :cond_1

    .line 507
    iget-object v0, p0, Lcom/sec/android/app/ve/pm/ProjectManager;->mDrawingClipartsMap:Ljava/util/HashMap;

    invoke-direct {p0, v0}, Lcom/sec/android/app/ve/pm/ProjectManager;->forceDeleteCaptionFiles(Ljava/util/HashMap;)V

    .line 508
    iget-object v0, p0, Lcom/sec/android/app/ve/pm/ProjectManager;->mDrawingClipartsMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 510
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/ve/pm/ProjectManager;->mDeleteProjectList:Ljava/util/List;

    if-eqz v0, :cond_3

    .line 511
    if-eqz p1, :cond_2

    .line 512
    iget-object v0, p0, Lcom/sec/android/app/ve/pm/ProjectManager;->mDeleteProjectList:Ljava/util/List;

    invoke-virtual {p0}, Lcom/sec/android/app/ve/pm/ProjectManager;->getProjectList()Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper;->deleteMediaFromProjects(Ljava/util/List;Ljava/util/List;)V

    .line 513
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/ve/pm/ProjectManager;->mDeleteProjectList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 515
    :cond_3
    return-void
.end method

.method public static clearTempMMSFileName()V
    .locals 1

    .prologue
    .line 441
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/ve/pm/ProjectManager;->mTempMMSImage:Ljava/lang/String;

    .line 442
    return-void
.end method

.method private forceDeleteCaptionFiles(Ljava/util/HashMap;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Lcom/samsung/app/video/editor/external/TranscodeElement;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/app/video/editor/external/ClipartParams;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 519
    .local p1, "ClipartMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Lcom/samsung/app/video/editor/external/TranscodeElement;Ljava/util/ArrayList<Lcom/samsung/app/video/editor/external/ClipartParams;>;>;"
    invoke-virtual {p1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 526
    invoke-virtual {p1}, Ljava/util/HashMap;->clear()V

    .line 527
    return-void

    .line 519
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 520
    .local v0, "ClipartList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    if-eqz v0, :cond_0

    .line 521
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/app/video/editor/external/ClipartParams;

    .line 522
    .local v1, "clipart":Lcom/samsung/app/video/editor/external/ClipartParams;
    const/4 v4, 0x1

    invoke-virtual {v1, v4}, Lcom/samsung/app/video/editor/external/ClipartParams;->remove(Z)V

    goto :goto_0
.end method

.method public static getInstance()Lcom/sec/android/app/ve/pm/ProjectManager;
    .locals 1

    .prologue
    .line 77
    sget-object v0, Lcom/sec/android/app/ve/pm/ProjectManager;->_instance:Lcom/sec/android/app/ve/pm/ProjectManager;

    return-object v0
.end method

.method public static getMMSImagePath(Landroid/net/Uri;)Ljava/lang/String;
    .locals 9
    .param p0, "lEditUri"    # Landroid/net/Uri;

    .prologue
    const/4 v2, 0x0

    .line 417
    const/4 v7, 0x0

    .line 419
    .local v7, "name":Ljava/lang/String;
    const-string v0, "content://mms/part"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 420
    .local v1, "uri":Landroid/net/Uri;
    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 422
    .local v6, "cPart":Landroid/database/Cursor;
    if-eqz v6, :cond_3

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 424
    :cond_0
    const-string v0, "ct"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 425
    .local v8, "type":Ljava/lang/String;
    const-string v0, "cl"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 427
    const-string v0, "image/jpeg"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "image/bmp"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 428
    const-string v0, "image/gif"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "image/jpg"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 429
    const-string v0, "image/png"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 430
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v2, Lcom/sec/android/app/ve/common/ConfigUtils;->EXPORT_PATH:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/ve/pm/ProjectManager;->mTempMMSImage:Ljava/lang/String;

    .line 432
    :cond_2
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 435
    .end local v8    # "type":Ljava/lang/String;
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v2, Lcom/sec/android/app/ve/common/ConfigUtils;->EXPORT_PATH:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "/Videoeditor/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private overWriteTranscodeFileData(Lcom/samsung/app/video/editor/external/TranscodeElement;)V
    .locals 6
    .param p1, "transcodeElement"    # Lcom/samsung/app/video/editor/external/TranscodeElement;

    .prologue
    .line 151
    new-instance v1, Ljava/io/File;

    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getProjectFileName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 152
    .local v1, "new_proj":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 155
    :try_start_0
    new-instance v2, Ljava/io/ObjectOutputStream;

    new-instance v3, Ljava/io/BufferedOutputStream;

    .line 156
    new-instance v4, Ljava/io/FileOutputStream;

    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getProjectFileName()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v3, v4}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 155
    invoke-direct {v2, v3}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 157
    .local v2, "obj_out_strm":Ljava/io/ObjectOutputStream;
    invoke-virtual {v2, p1}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 158
    invoke-virtual {v2}, Ljava/io/ObjectOutputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 165
    .end local v2    # "obj_out_strm":Ljava/io/ObjectOutputStream;
    :cond_0
    :goto_0
    return-void

    .line 159
    :catch_0
    move-exception v0

    .line 160
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private restoreDeletedRawFiles()V
    .locals 7

    .prologue
    .line 564
    iget-object v4, p0, Lcom/sec/android/app/ve/pm/ProjectManager;->mDeleteProjectList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_2

    .line 567
    iget-object v4, p0, Lcom/sec/android/app/ve/pm/ProjectManager;->mDeleteProjectList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_3

    .line 579
    iget-object v4, p0, Lcom/sec/android/app/ve/pm/ProjectManager;->mDeleteProjectList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_5

    .line 590
    return-void

    .line 564
    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/app/video/editor/external/TranscodeElement;

    .line 565
    .local v3, "transcodeElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/TranscodeElement;->deleteAllClipArts()V

    goto :goto_0

    .line 567
    .end local v3    # "transcodeElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    :cond_3
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/app/video/editor/external/TranscodeElement;

    .line 568
    .restart local v3    # "transcodeElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    iget-object v5, p0, Lcom/sec/android/app/ve/pm/ProjectManager;->mTextClipartsMap:Ljava/util/HashMap;

    invoke-virtual {v5, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    .line 569
    .local v2, "textClipArtList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    if-eqz v2, :cond_0

    .line 570
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_4

    .line 574
    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    goto :goto_1

    .line 570
    :cond_4
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/app/video/editor/external/ClipartParams;

    .line 571
    .local v1, "params":Lcom/samsung/app/video/editor/external/ClipartParams;
    new-instance v6, Lcom/samsung/app/video/editor/external/ClipartParams;

    invoke-direct {v6, v1}, Lcom/samsung/app/video/editor/external/ClipartParams;-><init>(Lcom/samsung/app/video/editor/external/ClipartParams;)V

    invoke-virtual {v3, v6}, Lcom/samsung/app/video/editor/external/TranscodeElement;->addTextEleList(Lcom/samsung/app/video/editor/external/ClipartParams;)V

    .line 572
    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/ClipartParams;->remove()V

    goto :goto_3

    .line 579
    .end local v1    # "params":Lcom/samsung/app/video/editor/external/ClipartParams;
    .end local v2    # "textClipArtList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    .end local v3    # "transcodeElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    :cond_5
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/app/video/editor/external/TranscodeElement;

    .line 580
    .restart local v3    # "transcodeElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    iget-object v5, p0, Lcom/sec/android/app/ve/pm/ProjectManager;->mDrawingClipartsMap:Ljava/util/HashMap;

    invoke-virtual {v5, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 581
    .local v0, "drawingClipArtList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    if-eqz v0, :cond_1

    .line 582
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_6

    .line 586
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto :goto_2

    .line 582
    :cond_6
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/app/video/editor/external/ClipartParams;

    .line 583
    .restart local v1    # "params":Lcom/samsung/app/video/editor/external/ClipartParams;
    new-instance v6, Lcom/samsung/app/video/editor/external/ClipartParams;

    invoke-direct {v6, v1}, Lcom/samsung/app/video/editor/external/ClipartParams;-><init>(Lcom/samsung/app/video/editor/external/ClipartParams;)V

    invoke-virtual {v3, v6}, Lcom/samsung/app/video/editor/external/TranscodeElement;->addDrawingEleList(Lcom/samsung/app/video/editor/external/ClipartParams;)V

    .line 584
    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/ClipartParams;->remove()V

    goto :goto_4
.end method

.method private saveDeletedProject(Lcom/samsung/app/video/editor/external/TranscodeElement;)V
    .locals 5
    .param p1, "transcodeElement"    # Lcom/samsung/app/video/editor/external/TranscodeElement;

    .prologue
    .line 472
    invoke-static {}, Lcom/sec/android/app/ve/pm/ProjectManager;->getInstance()Lcom/sec/android/app/ve/pm/ProjectManager;

    move-result-object v2

    .line 473
    invoke-virtual {v2}, Lcom/sec/android/app/ve/pm/ProjectManager;->getProjectList()Ljava/util/List;

    move-result-object v1

    .line 475
    .local v1, "tList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/TranscodeElement;>;"
    if-eqz v1, :cond_0

    .line 476
    new-instance v2, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getFilesDirectory()Ljava/io/File;

    move-result-object v4

    .line 477
    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 478
    sget-object v4, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getProjectName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".vep"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 476
    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 478
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    .line 481
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/ve/pm/ProjectManager;->getInstance()Lcom/sec/android/app/ve/pm/ProjectManager;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/sec/android/app/ve/pm/ProjectManager;->saveProject(Lcom/samsung/app/video/editor/external/TranscodeElement;)Z

    .line 482
    invoke-static {}, Lcom/sec/android/app/ve/pm/ProjectManager;->getInstance()Lcom/sec/android/app/ve/pm/ProjectManager;

    move-result-object v2

    new-instance v3, Lcom/samsung/app/video/editor/external/TranscodeElement;

    invoke-direct {v3, p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;-><init>(Lcom/samsung/app/video/editor/external/TranscodeElement;)V

    invoke-virtual {v2, v3}, Lcom/sec/android/app/ve/pm/ProjectManager;->setCloneTE(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 489
    :cond_0
    :goto_0
    return-void

    .line 483
    :catch_0
    move-exception v0

    .line 484
    .local v0, "e":Ljava/io/FileNotFoundException;
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 485
    .end local v0    # "e":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v0

    .line 486
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method private setProjectDetails(Lcom/samsung/app/video/editor/external/TranscodeElement;Ljava/lang/String;)V
    .locals 4
    .param p1, "transcodeElement"    # Lcom/samsung/app/video/editor/external/TranscodeElement;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 384
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getFilesDirectory()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".vep"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 385
    .local v0, "path":Ljava/lang/String;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {p1, v2, v3}, Lcom/samsung/app/video/editor/external/TranscodeElement;->setProjCreationTime(J)V

    .line 386
    invoke-virtual {p1, p2}, Lcom/samsung/app/video/editor/external/TranscodeElement;->setProjectName(Ljava/lang/String;)V

    .line 387
    invoke-virtual {p1, v0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->setProjectFileName(Ljava/lang/String;)V

    .line 388
    return-void
.end method

.method private storeTheRawFiles(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/TranscodeElement;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 540
    .local p1, "deleteprojList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/TranscodeElement;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_1

    .line 560
    return-void

    .line 540
    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/app/video/editor/external/TranscodeElement;

    .line 541
    .local v3, "transcode":Lcom/samsung/app/video/editor/external/TranscodeElement;
    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getTextEleList()Ljava/util/List;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 542
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 543
    .local v1, "lTextClipartList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getTextEleList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_3

    .line 546
    iget-object v5, p0, Lcom/sec/android/app/ve/pm/ProjectManager;->mTextClipartsMap:Ljava/util/HashMap;

    invoke-virtual {v5, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 549
    .end local v1    # "lTextClipartList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    :cond_2
    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getDrawEleList()Ljava/util/List;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 551
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 552
    .local v0, "lDrawingClipartList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getDrawEleList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_4

    .line 555
    iget-object v5, p0, Lcom/sec/android/app/ve/pm/ProjectManager;->mDrawingClipartsMap:Ljava/util/HashMap;

    invoke-virtual {v5, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 543
    .end local v0    # "lDrawingClipartList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    .restart local v1    # "lTextClipartList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    :cond_3
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/app/video/editor/external/ClipartParams;

    .line 544
    .local v2, "params":Lcom/samsung/app/video/editor/external/ClipartParams;
    invoke-static {v2}, Lcom/samsung/app/video/editor/external/ClipartParams;->getDeepCopyOf(Lcom/samsung/app/video/editor/external/ClipartParams;)Lcom/samsung/app/video/editor/external/ClipartParams;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 552
    .end local v1    # "lTextClipartList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    .end local v2    # "params":Lcom/samsung/app/video/editor/external/ClipartParams;
    .restart local v0    # "lDrawingClipartList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    :cond_4
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/app/video/editor/external/ClipartParams;

    .line 553
    .restart local v2    # "params":Lcom/samsung/app/video/editor/external/ClipartParams;
    invoke-static {v2}, Lcom/samsung/app/video/editor/external/ClipartParams;->getDeepCopyOf(Lcom/samsung/app/video/editor/external/ClipartParams;)Lcom/samsung/app/video/editor/external/ClipartParams;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2
.end method


# virtual methods
.method public addDeletedProject(Lcom/samsung/app/video/editor/external/TranscodeElement;)V
    .locals 2
    .param p1, "transcodeElement"    # Lcom/samsung/app/video/editor/external/TranscodeElement;

    .prologue
    .line 445
    invoke-virtual {p0}, Lcom/sec/android/app/ve/pm/ProjectManager;->getProjectList()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 446
    iget-object v0, p0, Lcom/sec/android/app/ve/pm/ProjectManager;->mProjectList:Ljava/util/Vector;

    sget-object v1, Lcom/sec/android/app/ve/pm/ProjectManager;->CREATE_TIME_ORDER:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 447
    return-void
.end method

.method public clearProjects()V
    .locals 1

    .prologue
    .line 493
    iget-object v0, p0, Lcom/sec/android/app/ve/pm/ProjectManager;->mProjectList:Ljava/util/Vector;

    if-eqz v0, :cond_0

    .line 494
    iget-object v0, p0, Lcom/sec/android/app/ve/pm/ProjectManager;->mProjectList:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clear()V

    .line 495
    :cond_0
    return-void
.end method

.method public cleardeletelist()V
    .locals 1

    .prologue
    .line 498
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/app/ve/pm/ProjectManager;->clearDeletedProjectList(Z)V

    .line 499
    return-void
.end method

.method public deleteProject(Lcom/samsung/app/video/editor/external/TranscodeElement;)Z
    .locals 5
    .param p1, "transcodeElement"    # Lcom/samsung/app/video/editor/external/TranscodeElement;

    .prologue
    .line 391
    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getProjectFileName()Ljava/lang/String;

    move-result-object v1

    .line 393
    .local v1, "fileName":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 394
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v2

    .line 396
    .local v2, "isFileDeleted":Z
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 397
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "File Stilll Exists::PMFile Deleted = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/ve/common/LogUtils;->log(Ljava/lang/String;)V

    .line 401
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/ve/pm/ProjectManager;->mProjectList:Ljava/util/Vector;

    if-eqz v3, :cond_0

    .line 403
    iget-object v3, p0, Lcom/sec/android/app/ve/pm/ProjectManager;->mProjectList:Ljava/util/Vector;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/ve/pm/ProjectManager;->getTranscodeElement(Ljava/lang/String;)Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/Vector;->remove(Ljava/lang/Object;)Z

    .line 405
    :cond_0
    const/4 v3, 0x1

    return v3

    .line 399
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "File Deleted::PMFile Deleted = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/ve/common/LogUtils;->log(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getCloneTE()Lcom/samsung/app/video/editor/external/TranscodeElement;
    .locals 1

    .prologue
    .line 229
    sget-object v0, Lcom/sec/android/app/ve/pm/ProjectManager;->cloneTE:Lcom/samsung/app/video/editor/external/TranscodeElement;

    return-object v0
.end method

.method public getIfFileCreatedNow()Z
    .locals 1

    .prologue
    .line 409
    iget-boolean v0, p0, Lcom/sec/android/app/ve/pm/ProjectManager;->isFileCreated:Z

    return v0
.end method

.method public getProjectFileList()[Ljava/io/File;
    .locals 4

    .prologue
    .line 82
    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getFilesDirectory()Ljava/io/File;

    move-result-object v0

    .line 86
    .local v0, "base_dir":Ljava/io/File;
    :try_start_0
    new-instance v3, Lcom/sec/android/app/ve/pm/ProjectManager$2;

    invoke-direct {v3, p0}, Lcom/sec/android/app/ve/pm/ProjectManager$2;-><init>(Lcom/sec/android/app/ve/pm/ProjectManager;)V

    .line 96
    .local v3, "filter":Ljava/io/FilenameFilter;
    invoke-virtual {v0, v3}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 103
    .end local v3    # "filter":Ljava/io/FilenameFilter;
    :goto_0
    return-object v2

    .line 99
    :catch_0
    move-exception v1

    .line 100
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 103
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getProjectFileNameAvailableNext(Ljava/lang/String;)Ljava/lang/String;
    .locals 12
    .param p1, "referenceName"    # Ljava/lang/String;

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 281
    const/4 v1, 0x1

    .line 282
    .local v1, "index":I
    const/4 v2, 0x0

    .line 283
    .local v2, "mMatched":Z
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v6

    if-nez v6, :cond_2

    .line 284
    :cond_0
    new-instance v6, Ljava/lang/StringBuilder;

    sget v7, Lcom/sec/android/app/ve/R$string;->project_name:I

    invoke-static {v7}, Lcom/sec/android/app/ve/VEApp;->getStringValue(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, "%d"

    new-array v8, v11, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 289
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/ve/pm/ProjectManager;->getProjectList()Ljava/util/List;

    move-result-object v3

    .line 290
    .local v3, "mTranscodeProjectList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/TranscodeElement;>;"
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    .line 292
    .local v4, "projectlistszie":I
    if-eqz v3, :cond_1

    .line 294
    :goto_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_2
    if-lt v0, v4, :cond_3

    .line 302
    :goto_3
    if-nez v2, :cond_5

    .line 310
    .end local v0    # "i":I
    :cond_1
    return-object p1

    .line 287
    .end local v3    # "mTranscodeProjectList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/TranscodeElement;>;"
    .end local v4    # "projectlistszie":I
    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    .line 295
    .restart local v0    # "i":I
    .restart local v3    # "mTranscodeProjectList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/TranscodeElement;>;"
    .restart local v4    # "projectlistszie":I
    :cond_3
    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/app/video/editor/external/TranscodeElement;

    .line 296
    .local v5, "tElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    if-eqz v5, :cond_4

    invoke-virtual {v5}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getProjectName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 297
    add-int/lit8 v1, v1, 0x1

    .line 298
    const/4 v2, 0x1

    .line 299
    goto :goto_3

    .line 294
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 305
    .end local v5    # "tElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    :cond_5
    const/4 v2, 0x0

    .line 306
    new-instance v6, Ljava/lang/StringBuilder;

    sget v7, Lcom/sec/android/app/ve/R$string;->project_name:I

    invoke-static {v7}, Lcom/sec/android/app/ve/VEApp;->getStringValue(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, "%d"

    new-array v8, v11, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 307
    goto :goto_1
.end method

.method public getProjectList()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/TranscodeElement;",
            ">;"
        }
    .end annotation

    .prologue
    .line 241
    iget-object v1, p0, Lcom/sec/android/app/ve/pm/ProjectManager;->mProjectList:Ljava/util/Vector;

    if-nez v1, :cond_0

    .line 243
    invoke-virtual {p0}, Lcom/sec/android/app/ve/pm/ProjectManager;->getProjectFileList()[Ljava/io/File;

    move-result-object v0

    .line 244
    .local v0, "files":[Ljava/io/File;
    if-nez v0, :cond_1

    .line 245
    new-instance v1, Ljava/util/Vector;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Ljava/util/Vector;-><init>(I)V

    iput-object v1, p0, Lcom/sec/android/app/ve/pm/ProjectManager;->mProjectList:Ljava/util/Vector;

    .line 250
    .end local v0    # "files":[Ljava/io/File;
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/ve/pm/ProjectManager;->mProjectList:Ljava/util/Vector;

    return-object v1

    .line 247
    .restart local v0    # "files":[Ljava/io/File;
    :cond_1
    new-instance v1, Ljava/util/Vector;

    array-length v2, v0

    invoke-direct {v1, v2}, Ljava/util/Vector;-><init>(I)V

    iput-object v1, p0, Lcom/sec/android/app/ve/pm/ProjectManager;->mProjectList:Ljava/util/Vector;

    goto :goto_0
.end method

.method public getTranscodeElement(Ljava/lang/String;)Lcom/samsung/app/video/editor/external/TranscodeElement;
    .locals 4
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 259
    iget-object v3, p0, Lcom/sec/android/app/ve/pm/ProjectManager;->mProjectList:Ljava/util/Vector;

    if-nez v3, :cond_0

    move-object v1, v2

    .line 277
    :goto_0
    return-object v1

    .line 262
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/ve/pm/ProjectManager;->mProjectList:Ljava/util/Vector;

    invoke-virtual {v3}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 264
    .local v0, "itr":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/app/video/editor/external/TranscodeElement;>;"
    :cond_1
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_2

    move-object v1, v2

    .line 277
    goto :goto_0

    .line 265
    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/app/video/editor/external/TranscodeElement;

    .line 267
    .local v1, "temp":Lcom/samsung/app/video/editor/external/TranscodeElement;
    if-eqz v1, :cond_3

    .line 269
    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getProjectFileName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    goto :goto_0

    .line 273
    :cond_3
    const-string v3, "PM:::::temp  null"

    invoke-static {v3}, Lcom/sec/android/app/ve/common/LogUtils;->log(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public getUndoDeleteList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/TranscodeElement;",
            ">;"
        }
    .end annotation

    .prologue
    .line 593
    iget-object v0, p0, Lcom/sec/android/app/ve/pm/ProjectManager;->mDeleteProjectList:Ljava/util/List;

    return-object v0
.end method

.method public init()V
    .locals 12

    .prologue
    const/4 v8, 0x0

    .line 113
    iget-object v9, p0, Lcom/sec/android/app/ve/pm/ProjectManager;->mProjectList:Ljava/util/Vector;

    if-eqz v9, :cond_1

    .line 148
    :cond_0
    :goto_0
    return-void

    .line 120
    :cond_1
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/ve/pm/ProjectManager;->getProjectFileList()[Ljava/io/File;

    move-result-object v4

    .line 122
    .local v4, "files":[Ljava/io/File;
    if-nez v4, :cond_2

    .line 123
    new-instance v8, Lcom/samsung/app/video/editor/external/VEVector;

    const/4 v9, 0x0

    invoke-direct {v8, v9}, Lcom/samsung/app/video/editor/external/VEVector;-><init>(I)V

    iput-object v8, p0, Lcom/sec/android/app/ve/pm/ProjectManager;->mProjectList:Ljava/util/Vector;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 145
    .end local v4    # "files":[Ljava/io/File;
    :catch_0
    move-exception v2

    .line 146
    .local v2, "exception":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 125
    .end local v2    # "exception":Ljava/lang/Exception;
    .restart local v4    # "files":[Ljava/io/File;
    :cond_2
    :try_start_1
    new-instance v9, Lcom/samsung/app/video/editor/external/VEVector;

    array-length v10, v4

    invoke-direct {v9, v10}, Lcom/samsung/app/video/editor/external/VEVector;-><init>(I)V

    iput-object v9, p0, Lcom/sec/android/app/ve/pm/ProjectManager;->mProjectList:Ljava/util/Vector;

    .line 126
    if-eqz v4, :cond_0

    .line 127
    array-length v9, v4

    :goto_1
    if-ge v8, v9, :cond_0

    aget-object v3, v4, v8
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 129
    .local v3, "fil":Ljava/io/File;
    :try_start_2
    new-instance v5, Ljava/io/FileInputStream;

    invoke-direct {v5, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 130
    .local v5, "fis":Ljava/io/FileInputStream;
    new-instance v0, Ljava/io/BufferedInputStream;

    invoke-direct {v0, v5}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    .line 131
    .local v0, "bis":Ljava/io/BufferedInputStream;
    new-instance v6, Ljava/io/ObjectInputStream;

    invoke-direct {v6, v0}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V

    .line 132
    .local v6, "obj_in_stream":Ljava/io/ObjectInputStream;
    invoke-virtual {v6}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/app/video/editor/external/TranscodeElement;

    .line 134
    .local v7, "tElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    sget-object v10, Lcom/sec/android/app/ve/VEApp;->gThemeMgr:Lcom/sec/android/app/ve/theme/ThemeManager;

    invoke-virtual {v7}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getThemeName()I

    move-result v11

    invoke-virtual {v10, v11}, Lcom/sec/android/app/ve/theme/ThemeManager;->getThemeAssetManager(I)Landroid/content/res/AssetManager;

    move-result-object v10

    invoke-virtual {v7, v10}, Lcom/samsung/app/video/editor/external/TranscodeElement;->setAssetManager(Landroid/content/res/AssetManager;)V

    .line 135
    iget-object v10, p0, Lcom/sec/android/app/ve/pm/ProjectManager;->mProjectList:Ljava/util/Vector;

    invoke-virtual {v10, v7}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 136
    invoke-virtual {v6}, Ljava/io/ObjectInputStream;->close()V
    :try_end_2
    .catch Ljava/io/EOFException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 127
    .end local v0    # "bis":Ljava/io/BufferedInputStream;
    .end local v5    # "fis":Ljava/io/FileInputStream;
    .end local v6    # "obj_in_stream":Ljava/io/ObjectInputStream;
    .end local v7    # "tElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    :goto_2
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 137
    :catch_1
    move-exception v1

    .line 138
    .local v1, "e":Ljava/io/EOFException;
    :try_start_3
    invoke-virtual {v1}, Ljava/io/EOFException;->printStackTrace()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_2
.end method

.method public recheckProjects(Landroid/content/Context;)V
    .locals 24
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 597
    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/ve/util/CommonUtils;->isPrivateModeON(Landroid/content/Context;)Z

    move-result v12

    .line 598
    .local v12, "isPrivateMode":Z
    new-instance v16, Ljava/util/HashMap;

    invoke-direct/range {v16 .. v16}, Ljava/util/HashMap;-><init>()V

    .line 600
    .local v16, "projectThumbMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Landroid/graphics/Bitmap;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/ve/pm/ProjectManager;->mProjectList:Ljava/util/Vector;

    move-object/from16 v19, v0

    if-eqz v19, :cond_3

    .line 601
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/ve/pm/ProjectManager;->mProjectList:Ljava/util/Vector;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/util/Vector;->size()I

    move-result v19

    move/from16 v0, v19

    if-lt v10, v0, :cond_1

    .line 607
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/ve/pm/ProjectManager;->mProjectList:Ljava/util/Vector;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/util/Vector;->clear()V

    .line 612
    .end local v10    # "i":I
    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/ve/pm/ProjectManager;->getProjectFileList()[Ljava/io/File;

    move-result-object v8

    .line 613
    .local v8, "files":[Ljava/io/File;
    if-eqz v8, :cond_0

    .line 614
    array-length v0, v8

    move/from16 v21, v0

    const/16 v19, 0x0

    move/from16 v20, v19

    :goto_2
    move/from16 v0, v20

    move/from16 v1, v21

    if-lt v0, v1, :cond_4

    .line 660
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/ve/pm/ProjectManager;->sortList()V

    .line 661
    return-void

    .line 602
    .end local v8    # "files":[Ljava/io/File;
    .restart local v10    # "i":I
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/ve/pm/ProjectManager;->mProjectList:Ljava/util/Vector;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v10}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/samsung/app/video/editor/external/TranscodeElement;

    .line 603
    .local v15, "project":Lcom/samsung/app/video/editor/external/TranscodeElement;
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v15, v0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getThumbnail(Z)Landroid/graphics/Bitmap;

    move-result-object v18

    .line 604
    .local v18, "thumb":Landroid/graphics/Bitmap;
    if-eqz v18, :cond_2

    .line 605
    invoke-virtual {v15}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getProjectName()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 601
    :cond_2
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 610
    .end local v10    # "i":I
    .end local v15    # "project":Lcom/samsung/app/video/editor/external/TranscodeElement;
    .end local v18    # "thumb":Landroid/graphics/Bitmap;
    :cond_3
    new-instance v19, Lcom/samsung/app/video/editor/external/VEVector;

    invoke-direct/range {v19 .. v19}, Lcom/samsung/app/video/editor/external/VEVector;-><init>()V

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/ve/pm/ProjectManager;->mProjectList:Ljava/util/Vector;

    goto :goto_1

    .line 614
    .restart local v8    # "files":[Ljava/io/File;
    :cond_4
    aget-object v7, v8, v20

    .line 616
    .local v7, "fil":Ljava/io/File;
    const/4 v13, 0x0

    .line 618
    .local v13, "obj_in_stream":Ljava/io/ObjectInputStream;
    :try_start_0
    new-instance v9, Ljava/io/FileInputStream;

    invoke-direct {v9, v7}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 619
    .local v9, "fis":Ljava/io/FileInputStream;
    new-instance v5, Ljava/io/BufferedInputStream;

    invoke-direct {v5, v9}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    .line 620
    .local v5, "bis":Ljava/io/BufferedInputStream;
    new-instance v14, Ljava/io/ObjectInputStream;

    invoke-direct {v14, v5}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 621
    .end local v13    # "obj_in_stream":Ljava/io/ObjectInputStream;
    .local v14, "obj_in_stream":Ljava/io/ObjectInputStream;
    :try_start_1
    invoke-virtual {v14}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/samsung/app/video/editor/external/TranscodeElement;

    .line 623
    .local v17, "tElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/util/CommonUtils;->containsPrivateModeContents(Lcom/samsung/app/video/editor/external/TranscodeElement;Landroid/content/Context;)Z

    move-result v19

    if-eqz v19, :cond_6

    if-nez v12, :cond_6

    .line 624
    const-string v19, "Ignored a project because it contains private mode contents and currently private mode is OFF!"

    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/ve/common/LogUtils;->log(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 649
    :goto_3
    if-eqz v14, :cond_b

    .line 651
    :try_start_2
    invoke-virtual {v14}, Ljava/io/ObjectInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    move-object v13, v14

    .line 614
    .end local v5    # "bis":Ljava/io/BufferedInputStream;
    .end local v9    # "fis":Ljava/io/FileInputStream;
    .end local v14    # "obj_in_stream":Ljava/io/ObjectInputStream;
    .end local v17    # "tElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    .restart local v13    # "obj_in_stream":Ljava/io/ObjectInputStream;
    :cond_5
    :goto_4
    add-int/lit8 v19, v20, 0x1

    move/from16 v20, v19

    goto :goto_2

    .line 627
    .end local v13    # "obj_in_stream":Ljava/io/ObjectInputStream;
    .restart local v5    # "bis":Ljava/io/BufferedInputStream;
    .restart local v9    # "fis":Ljava/io/FileInputStream;
    .restart local v14    # "obj_in_stream":Ljava/io/ObjectInputStream;
    .restart local v17    # "tElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    :cond_6
    :try_start_3
    sget-object v19, Lcom/sec/android/app/ve/VEApp;->gThemeMgr:Lcom/sec/android/app/ve/theme/ThemeManager;

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getThemeName()I

    move-result v22

    move-object/from16 v0, v19

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/app/ve/theme/ThemeManager;->getThemeAssetManager(I)Landroid/content/res/AssetManager;

    move-result-object v19

    if-eqz v19, :cond_7

    .line 628
    sget-object v19, Lcom/sec/android/app/ve/VEApp;->gThemeMgr:Lcom/sec/android/app/ve/theme/ThemeManager;

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getThemeName()I

    move-result v22

    move-object/from16 v0, v19

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/app/ve/theme/ThemeManager;->getThemeAssetManager(I)Landroid/content/res/AssetManager;

    move-result-object v19

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->setAssetManager(Landroid/content/res/AssetManager;)V

    .line 629
    :cond_7
    invoke-virtual/range {v17 .. v17}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementList()Ljava/util/List;

    move-result-object v19

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getAssetManager()Landroid/content/res/AssetManager;

    move-result-object v22

    const/16 v23, 0x1

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    move-object/from16 v2, v19

    move-object/from16 v3, v22

    move/from16 v4, v23

    invoke-static {v0, v1, v2, v3, v4}, Lcom/sec/android/app/ve/util/CommonUtils;->removeNonExistentElements(Lcom/samsung/app/video/editor/external/TranscodeElement;Landroid/content/Context;Ljava/util/List;Landroid/content/res/AssetManager;Z)Z

    move-result v11

    .line 630
    .local v11, "isMediaDeleted":Z
    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/util/CommonUtils;->removeNonExistentElements(Lcom/samsung/app/video/editor/external/TranscodeElement;Landroid/content/Context;)V

    .line 631
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/ve/pm/ProjectManager;->mProjectList:Ljava/util/Vector;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 633
    if-eqz v11, :cond_a

    .line 635
    invoke-virtual/range {v17 .. v17}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v19

    if-nez v19, :cond_8

    .line 636
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/sec/android/app/ve/pm/ProjectManager;->deleteProject(Lcom/samsung/app/video/editor/external/TranscodeElement;)Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_3

    .line 645
    .end local v11    # "isMediaDeleted":Z
    .end local v17    # "tElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    :catch_0
    move-exception v6

    move-object v13, v14

    .line 646
    .end local v5    # "bis":Ljava/io/BufferedInputStream;
    .end local v9    # "fis":Ljava/io/FileInputStream;
    .end local v14    # "obj_in_stream":Ljava/io/ObjectInputStream;
    .local v6, "e":Ljava/lang/Exception;
    .restart local v13    # "obj_in_stream":Ljava/io/ObjectInputStream;
    :goto_5
    :try_start_4
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 649
    if-eqz v13, :cond_5

    .line 651
    :try_start_5
    invoke-virtual {v13}, Ljava/io/ObjectInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_4

    .line 652
    :catch_1
    move-exception v6

    .line 654
    .local v6, "e":Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 638
    .end local v6    # "e":Ljava/io/IOException;
    .end local v13    # "obj_in_stream":Ljava/io/ObjectInputStream;
    .restart local v5    # "bis":Ljava/io/BufferedInputStream;
    .restart local v9    # "fis":Ljava/io/FileInputStream;
    .restart local v11    # "isMediaDeleted":Z
    .restart local v14    # "obj_in_stream":Ljava/io/ObjectInputStream;
    .restart local v17    # "tElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    :cond_8
    :try_start_6
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/sec/android/app/ve/pm/ProjectManager;->saveProject(Lcom/samsung/app/video/editor/external/TranscodeElement;)Z
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_3

    .line 648
    .end local v11    # "isMediaDeleted":Z
    .end local v17    # "tElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    :catchall_0
    move-exception v19

    move-object v13, v14

    .line 649
    .end local v5    # "bis":Ljava/io/BufferedInputStream;
    .end local v9    # "fis":Ljava/io/FileInputStream;
    .end local v14    # "obj_in_stream":Ljava/io/ObjectInputStream;
    .restart local v13    # "obj_in_stream":Ljava/io/ObjectInputStream;
    :goto_6
    if-eqz v13, :cond_9

    .line 651
    :try_start_7
    invoke-virtual {v13}, Ljava/io/ObjectInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2

    .line 657
    :cond_9
    :goto_7
    throw v19

    .line 641
    .end local v13    # "obj_in_stream":Ljava/io/ObjectInputStream;
    .restart local v5    # "bis":Ljava/io/BufferedInputStream;
    .restart local v9    # "fis":Ljava/io/FileInputStream;
    .restart local v11    # "isMediaDeleted":Z
    .restart local v14    # "obj_in_stream":Ljava/io/ObjectInputStream;
    .restart local v17    # "tElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    :cond_a
    :try_start_8
    invoke-virtual/range {v17 .. v17}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getProjectName()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Landroid/graphics/Bitmap;

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->setThumbnail(Landroid/graphics/Bitmap;)V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_0
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto/16 :goto_3

    .line 652
    .end local v5    # "bis":Ljava/io/BufferedInputStream;
    .end local v9    # "fis":Ljava/io/FileInputStream;
    .end local v11    # "isMediaDeleted":Z
    .end local v14    # "obj_in_stream":Ljava/io/ObjectInputStream;
    .end local v17    # "tElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    .restart local v13    # "obj_in_stream":Ljava/io/ObjectInputStream;
    :catch_2
    move-exception v6

    .line 654
    .restart local v6    # "e":Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_7

    .line 652
    .end local v6    # "e":Ljava/io/IOException;
    .end local v13    # "obj_in_stream":Ljava/io/ObjectInputStream;
    .restart local v5    # "bis":Ljava/io/BufferedInputStream;
    .restart local v9    # "fis":Ljava/io/FileInputStream;
    .restart local v14    # "obj_in_stream":Ljava/io/ObjectInputStream;
    .restart local v17    # "tElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    :catch_3
    move-exception v6

    .line 654
    .restart local v6    # "e":Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    .end local v6    # "e":Ljava/io/IOException;
    :cond_b
    move-object v13, v14

    .end local v14    # "obj_in_stream":Ljava/io/ObjectInputStream;
    .restart local v13    # "obj_in_stream":Ljava/io/ObjectInputStream;
    goto/16 :goto_4

    .line 648
    .end local v5    # "bis":Ljava/io/BufferedInputStream;
    .end local v9    # "fis":Ljava/io/FileInputStream;
    .end local v17    # "tElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    :catchall_1
    move-exception v19

    goto :goto_6

    .line 645
    :catch_4
    move-exception v6

    goto :goto_5
.end method

.method public renameProject(Lcom/samsung/app/video/editor/external/TranscodeElement;Lcom/samsung/app/video/editor/external/TranscodeElement;)V
    .locals 5
    .param p1, "oldElement"    # Lcom/samsung/app/video/editor/external/TranscodeElement;
    .param p2, "newElement"    # Lcom/samsung/app/video/editor/external/TranscodeElement;

    .prologue
    .line 316
    const/4 v2, 0x0

    .line 317
    .local v2, "oldFile":Ljava/io/File;
    :try_start_0
    invoke-virtual {p2}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getProjectFileName()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 318
    new-instance v3, Ljava/io/File;

    invoke-virtual {p2}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getProjectFileName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .end local v2    # "oldFile":Ljava/io/File;
    .local v3, "oldFile":Ljava/io/File;
    move-object v2, v3

    .line 319
    .end local v3    # "oldFile":Ljava/io/File;
    .restart local v2    # "oldFile":Ljava/io/File;
    :cond_0
    const/4 v1, 0x0

    .line 320
    .local v1, "newFile":Ljava/io/File;
    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getProjectFileName()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 321
    new-instance v1, Ljava/io/File;

    .end local v1    # "newFile":Ljava/io/File;
    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getProjectFileName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 323
    .restart local v1    # "newFile":Ljava/io/File;
    :cond_1
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 324
    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getProjectName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v4}, Lcom/samsung/app/video/editor/external/TranscodeElement;->setProjectName(Ljava/lang/String;)V

    .line 325
    invoke-static {}, Lcom/sec/android/app/ve/pm/ProjectManager;->getInstance()Lcom/sec/android/app/ve/pm/ProjectManager;

    move-result-object v4

    invoke-virtual {v4, p1}, Lcom/sec/android/app/ve/pm/ProjectManager;->deleteProject(Lcom/samsung/app/video/editor/external/TranscodeElement;)Z

    .line 326
    invoke-virtual {p0, p2}, Lcom/sec/android/app/ve/pm/ProjectManager;->saveProject(Lcom/samsung/app/video/editor/external/TranscodeElement;)Z

    .line 336
    .end local v1    # "newFile":Ljava/io/File;
    :cond_2
    :goto_0
    return-void

    .line 327
    .restart local v1    # "newFile":Ljava/io/File;
    :cond_3
    if-eqz v1, :cond_2

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 328
    invoke-virtual {v2, v1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 329
    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getProjectFileName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v4}, Lcom/samsung/app/video/editor/external/TranscodeElement;->setProjectFileName(Ljava/lang/String;)V

    .line 330
    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getProjectName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v4}, Lcom/samsung/app/video/editor/external/TranscodeElement;->setProjectName(Ljava/lang/String;)V

    .line 331
    invoke-direct {p0, p2}, Lcom/sec/android/app/ve/pm/ProjectManager;->overWriteTranscodeFileData(Lcom/samsung/app/video/editor/external/TranscodeElement;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 333
    .end local v1    # "newFile":Ljava/io/File;
    :catch_0
    move-exception v0

    .line 334
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public renameProject(Lcom/samsung/app/video/editor/external/TranscodeElement;Ljava/lang/String;)V
    .locals 7
    .param p1, "project"    # Lcom/samsung/app/video/editor/external/TranscodeElement;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 340
    const/4 v2, 0x0

    .line 341
    .local v2, "oldFile":Ljava/io/File;
    :try_start_0
    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getProjectFileName()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 342
    new-instance v3, Ljava/io/File;

    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getProjectFileName()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .end local v2    # "oldFile":Ljava/io/File;
    .local v3, "oldFile":Ljava/io/File;
    move-object v2, v3

    .line 343
    .end local v3    # "oldFile":Ljava/io/File;
    .restart local v2    # "oldFile":Ljava/io/File;
    :cond_0
    new-instance v1, Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getFilesDirectory()Ljava/io/File;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ".vep"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v1, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 345
    .local v1, "newFile":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 346
    invoke-virtual {p1, p2}, Lcom/samsung/app/video/editor/external/TranscodeElement;->setProjectName(Ljava/lang/String;)V

    .line 347
    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getProjectFileName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/sec/android/app/ve/pm/ProjectManager;->getTranscodeElement(Ljava/lang/String;)Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v4

    .line 348
    .local v4, "tElem":Lcom/samsung/app/video/editor/external/TranscodeElement;
    if-eqz v4, :cond_1

    .line 349
    invoke-virtual {p0, v4}, Lcom/sec/android/app/ve/pm/ProjectManager;->deleteProject(Lcom/samsung/app/video/editor/external/TranscodeElement;)Z

    .line 350
    :cond_1
    invoke-virtual {p0, p1}, Lcom/sec/android/app/ve/pm/ProjectManager;->saveProject(Lcom/samsung/app/video/editor/external/TranscodeElement;)Z

    .line 360
    .end local v1    # "newFile":Ljava/io/File;
    .end local v4    # "tElem":Lcom/samsung/app/video/editor/external/TranscodeElement;
    :cond_2
    :goto_0
    return-void

    .line 351
    .restart local v1    # "newFile":Ljava/io/File;
    :cond_3
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 352
    invoke-virtual {v2, v1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 353
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getFilesDirectory()Ljava/io/File;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ".vep"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Lcom/samsung/app/video/editor/external/TranscodeElement;->setProjectFileName(Ljava/lang/String;)V

    .line 354
    invoke-virtual {p1, p2}, Lcom/samsung/app/video/editor/external/TranscodeElement;->setProjectName(Ljava/lang/String;)V

    .line 355
    invoke-direct {p0, p1}, Lcom/sec/android/app/ve/pm/ProjectManager;->overWriteTranscodeFileData(Lcom/samsung/app/video/editor/external/TranscodeElement;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 357
    .end local v1    # "newFile":Ljava/io/File;
    :catch_0
    move-exception v0

    .line 358
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public saveProject(Lcom/samsung/app/video/editor/external/TranscodeElement;)Z
    .locals 12
    .param p1, "transcodeElement"    # Lcom/samsung/app/video/editor/external/TranscodeElement;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 169
    invoke-static {}, Lcom/sec/android/app/ve/pm/ProjectManager;->clearTempMMSFileName()V

    .line 175
    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getProjectFileName()Ljava/lang/String;

    move-result-object v8

    if-nez v8, :cond_3

    .line 176
    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getProjectName()Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, p1, v8}, Lcom/sec/android/app/ve/pm/ProjectManager;->setProjectDetails(Lcom/samsung/app/video/editor/external/TranscodeElement;Ljava/lang/String;)V

    .line 177
    new-instance v6, Lcom/samsung/app/video/editor/external/TranscodeElement;

    invoke-direct {v6, p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;-><init>(Lcom/samsung/app/video/editor/external/TranscodeElement;)V

    .line 179
    .local v6, "saveProject":Lcom/samsung/app/video/editor/external/TranscodeElement;
    invoke-virtual {v6}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getProjectName()Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v6, v8}, Lcom/sec/android/app/ve/pm/ProjectManager;->addProject(Lcom/samsung/app/video/editor/external/TranscodeElement;Ljava/lang/String;)Lcom/samsung/app/video/editor/external/TranscodeElement;

    .line 191
    :cond_0
    :goto_0
    const-wide/16 v8, 0x3a98

    invoke-static {v6, v8, v9}, Lcom/sec/android/app/ve/util/CommonUtils;->getBitmapFromEngine(Lcom/samsung/app/video/editor/external/TranscodeElement;J)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 192
    .local v0, "bmp":Landroid/graphics/Bitmap;
    invoke-virtual {v6, v0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->setThumbnail(Landroid/graphics/Bitmap;)V

    .line 197
    :try_start_0
    invoke-virtual {v6}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getProjectFileName()Ljava/lang/String;

    move-result-object v5

    .line 199
    .local v5, "project_file":Ljava/lang/String;
    if-nez v5, :cond_4

    .line 200
    new-instance v3, Ljava/io/File;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getFilesDirectory()Ljava/io/File;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/VEFile."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ".vep"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v3, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 205
    .local v3, "new_proj":Ljava/io/File;
    :goto_1
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v8

    if-nez v8, :cond_1

    .line 206
    invoke-virtual {v3}, Ljava/io/File;->createNewFile()Z

    .line 210
    :cond_1
    new-instance v4, Ljava/io/ObjectOutputStream;

    new-instance v8, Ljava/io/BufferedOutputStream;

    .line 211
    new-instance v9, Ljava/io/FileOutputStream;

    invoke-direct {v9, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v8, v9}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 210
    invoke-direct {v4, v8}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 212
    .local v4, "obj_out_strm":Ljava/io/ObjectOutputStream;
    invoke-virtual {v4, v6}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 213
    invoke-virtual {v4}, Ljava/io/ObjectOutputStream;->close()V

    .line 215
    sget-object v8, Lcom/sec/android/app/ve/pm/ProjectManager;->cloneTE:Lcom/samsung/app/video/editor/external/TranscodeElement;

    if-eqz v8, :cond_2

    .line 216
    sget-object v8, Lcom/sec/android/app/ve/pm/ProjectManager;->cloneTE:Lcom/samsung/app/video/editor/external/TranscodeElement;

    invoke-virtual {v8}, Lcom/samsung/app/video/editor/external/TranscodeElement;->deleteAllClipArts()V

    .line 219
    :cond_2
    new-instance v8, Lcom/samsung/app/video/editor/external/TranscodeElement;

    invoke-direct {v8, v6}, Lcom/samsung/app/video/editor/external/TranscodeElement;-><init>(Lcom/samsung/app/video/editor/external/TranscodeElement;)V

    sput-object v8, Lcom/sec/android/app/ve/pm/ProjectManager;->cloneTE:Lcom/samsung/app/video/editor/external/TranscodeElement;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 220
    const/4 v8, 0x1

    .line 225
    .end local v3    # "new_proj":Ljava/io/File;
    .end local v4    # "obj_out_strm":Ljava/io/ObjectOutputStream;
    .end local v5    # "project_file":Ljava/lang/String;
    :goto_2
    return v8

    .line 181
    .end local v0    # "bmp":Landroid/graphics/Bitmap;
    .end local v6    # "saveProject":Lcom/samsung/app/video/editor/external/TranscodeElement;
    :cond_3
    new-instance v6, Lcom/samsung/app/video/editor/external/TranscodeElement;

    invoke-direct {v6, p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;-><init>(Lcom/samsung/app/video/editor/external/TranscodeElement;)V

    .line 182
    .restart local v6    # "saveProject":Lcom/samsung/app/video/editor/external/TranscodeElement;
    iget-object v8, p0, Lcom/sec/android/app/ve/pm/ProjectManager;->mProjectList:Ljava/util/Vector;

    if-eqz v8, :cond_0

    .line 183
    invoke-virtual {v6}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getProjectFileName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, Lcom/sec/android/app/ve/pm/ProjectManager;->getTranscodeElement(Ljava/lang/String;)Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v7

    .line 184
    .local v7, "temp":Lcom/samsung/app/video/editor/external/TranscodeElement;
    if-eqz v7, :cond_0

    iget-object v8, p0, Lcom/sec/android/app/ve/pm/ProjectManager;->mProjectList:Ljava/util/Vector;

    invoke-virtual {v8, v7}, Ljava/util/Vector;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 185
    invoke-virtual {p0}, Lcom/sec/android/app/ve/pm/ProjectManager;->getProjectList()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8, v7}, Ljava/util/List;->lastIndexOf(Ljava/lang/Object;)I

    move-result v2

    .line 186
    .local v2, "index":I
    iget-object v8, p0, Lcom/sec/android/app/ve/pm/ProjectManager;->mProjectList:Ljava/util/Vector;

    invoke-virtual {v8, v2, v6}, Ljava/util/Vector;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 202
    .end local v2    # "index":I
    .end local v7    # "temp":Lcom/samsung/app/video/editor/external/TranscodeElement;
    .restart local v0    # "bmp":Landroid/graphics/Bitmap;
    .restart local v5    # "project_file":Ljava/lang/String;
    :cond_4
    :try_start_1
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .restart local v3    # "new_proj":Ljava/io/File;
    goto :goto_1

    .line 221
    .end local v3    # "new_proj":Ljava/io/File;
    .end local v5    # "project_file":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 222
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 225
    const/4 v8, 0x0

    goto :goto_2
.end method

.method public setCloneTE(Ljava/lang/Object;)V
    .locals 0
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 233
    check-cast p1, Lcom/samsung/app/video/editor/external/TranscodeElement;

    .end local p1    # "obj":Ljava/lang/Object;
    sput-object p1, Lcom/sec/android/app/ve/pm/ProjectManager;->cloneTE:Lcom/samsung/app/video/editor/external/TranscodeElement;

    .line 234
    return-void
.end method

.method public setUndoDeleteList(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/TranscodeElement;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 530
    .local p1, "deleteprojList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/TranscodeElement;>;"
    invoke-virtual {p0}, Lcom/sec/android/app/ve/pm/ProjectManager;->cleardeletelist()V

    .line 531
    iget-object v0, p0, Lcom/sec/android/app/ve/pm/ProjectManager;->mDeleteProjectList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 532
    iget-object v0, p0, Lcom/sec/android/app/ve/pm/ProjectManager;->mDeleteProjectList:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/sec/android/app/ve/pm/ProjectManager;->storeTheRawFiles(Ljava/util/List;)V

    .line 535
    return-void
.end method

.method public sortList()V
    .locals 2

    .prologue
    .line 413
    iget-object v0, p0, Lcom/sec/android/app/ve/pm/ProjectManager;->mProjectList:Ljava/util/Vector;

    sget-object v1, Lcom/sec/android/app/ve/pm/ProjectManager;->CREATE_TIME_ORDER:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 414
    return-void
.end method

.method public undoTheDeletedproject()V
    .locals 3

    .prologue
    .line 450
    iget-object v2, p0, Lcom/sec/android/app/ve/pm/ProjectManager;->mDeleteProjectList:Ljava/util/List;

    if-eqz v2, :cond_1

    .line 451
    new-instance v1, Lcom/sec/android/app/ve/pm/ProjectManager$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/ve/pm/ProjectManager$3;-><init>(Lcom/sec/android/app/ve/pm/ProjectManager;)V

    .line 459
    .local v1, "thread":Ljava/lang/Thread;
    iget-object v2, p0, Lcom/sec/android/app/ve/pm/ProjectManager;->mDeleteProjectList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-eqz v2, :cond_0

    .line 460
    invoke-direct {p0}, Lcom/sec/android/app/ve/pm/ProjectManager;->restoreDeletedRawFiles()V

    .line 461
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/ve/pm/ProjectManager;->mDeleteProjectList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lt v0, v2, :cond_2

    .line 465
    .end local v0    # "i":I
    :cond_0
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 468
    .end local v1    # "thread":Ljava/lang/Thread;
    :cond_1
    return-void

    .line 462
    .restart local v0    # "i":I
    .restart local v1    # "thread":Ljava/lang/Thread;
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/ve/pm/ProjectManager;->mDeleteProjectList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/app/video/editor/external/TranscodeElement;

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/pm/ProjectManager;->addDeletedProject(Lcom/samsung/app/video/editor/external/TranscodeElement;)V

    .line 461
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
