.class public Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogHelpPage;
.super Lcom/sec/android/app/ve/common/AndroidUtils$DlgFragment;
.source "VideoEditorLiteActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DialogHelpPage"
.end annotation


# instance fields
.field activity:Landroid/app/Activity;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 855
    invoke-direct {p0}, Lcom/sec/android/app/ve/common/AndroidUtils$DlgFragment;-><init>()V

    .line 856
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogHelpPage;->activity:Landroid/app/Activity;

    .line 855
    return-void
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 860
    invoke-super {p0, p1}, Lcom/sec/android/app/ve/common/AndroidUtils$DlgFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 861
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogHelpPage;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogHelpPage;->activity:Landroid/app/Activity;

    .line 863
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 900
    invoke-super {p0, p1}, Lcom/sec/android/app/ve/common/AndroidUtils$DlgFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 902
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogHelpPage;->getDialog()Landroid/app/Dialog;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogHelpPage;->getDialog()Landroid/app/Dialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_1

    iget v1, p1, Landroid/content/res/Configuration;->fontScale:F

    # getter for: Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->fontScale_value:F
    invoke-static {}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->access$7()F

    move-result v2

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_1

    .line 903
    iget v1, p1, Landroid/content/res/Configuration;->fontScale:F

    invoke-static {v1}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->access$8(F)V

    .line 904
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogHelpPage;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/ve/common/AndroidUtils;->dismissDialog(Landroid/app/Activity;)V

    .line 920
    :cond_0
    :goto_0
    return-void

    .line 908
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogHelpPage;->getDialog()Landroid/app/Dialog;

    move-result-object v1

    const v2, 0x7f030010

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->setContentView(I)V

    .line 909
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogHelpPage;->getDialog()Landroid/app/Dialog;

    move-result-object v1

    const v2, 0x7f0d003d

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 910
    .local v0, "img":Landroid/widget/Button;
    if-eqz v0, :cond_0

    .line 911
    new-instance v1, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogHelpPage$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogHelpPage$3;-><init>(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogHelpPage;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 867
    new-instance v0, Landroid/app/Dialog;

    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogHelpPage;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 868
    .local v0, "dialog":Landroid/app/Dialog;
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogHelpPage;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/16 v3, 0x400

    const/high16 v4, 0x20000

    invoke-virtual {v2, v3, v4}, Landroid/view/Window;->setFlags(II)V

    .line 869
    invoke-virtual {v0, v6}, Landroid/app/Dialog;->requestWindowFeature(I)Z

    .line 870
    invoke-virtual {v0, v5}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 871
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    const v3, 0x7f0603af

    invoke-static {v3}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v3

    const v4, 0x7f0603b0

    invoke-static {v4}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/view/Window;->setLayout(II)V

    .line 872
    invoke-virtual {p0, v6}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogHelpPage;->setCancelable(Z)V

    .line 873
    const v2, 0x7f030010

    invoke-virtual {v0, v2}, Landroid/app/Dialog;->setContentView(I)V

    .line 874
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    new-instance v3, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v3, v5}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v2, v3}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 876
    new-instance v2, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogHelpPage$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogHelpPage$1;-><init>(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogHelpPage;)V

    invoke-virtual {v0, v2}, Landroid/app/Dialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 886
    const v2, 0x7f0d003d

    invoke-virtual {v0, v2}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 887
    .local v1, "img":Landroid/widget/Button;
    if-eqz v1, :cond_0

    .line 888
    new-instance v2, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogHelpPage$2;

    invoke-direct {v2, p0, v0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogHelpPage$2;-><init>(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogHelpPage;Landroid/app/Dialog;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 896
    :cond_0
    return-object v0
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 923
    iget-object v2, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogHelpPage;->activity:Landroid/app/Activity;

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 924
    .local v1, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 925
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "OVERLAY_HELP_SHOWN"

    const/4 v3, 0x1

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 926
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 927
    invoke-super {p0, p1}, Lcom/sec/android/app/ve/common/AndroidUtils$DlgFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 928
    return-void
.end method
