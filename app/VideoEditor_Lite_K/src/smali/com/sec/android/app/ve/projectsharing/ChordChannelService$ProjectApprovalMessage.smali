.class Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ProjectApprovalMessage;
.super Ljava/lang/Object;
.source "ChordChannelService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/ve/projectsharing/ChordChannelService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ProjectApprovalMessage"
.end annotation


# instance fields
.field private approve:Z

.field private overwrite:Z


# direct methods
.method public constructor <init>(ZZ)V
    .locals 0
    .param p1, "approve"    # Z
    .param p2, "overwrite"    # Z

    .prologue
    .line 1009
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1010
    iput-boolean p1, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ProjectApprovalMessage;->approve:Z

    .line 1011
    iput-boolean p2, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ProjectApprovalMessage;->overwrite:Z

    .line 1012
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ProjectApprovalMessage;)Z
    .locals 1

    .prologue
    .line 1006
    iget-boolean v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ProjectApprovalMessage;->approve:Z

    return v0
.end method

.method static synthetic access$1(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ProjectApprovalMessage;)Z
    .locals 1

    .prologue
    .line 1007
    iget-boolean v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ProjectApprovalMessage;->overwrite:Z

    return v0
.end method
