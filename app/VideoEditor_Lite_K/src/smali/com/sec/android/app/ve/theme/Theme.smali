.class public abstract Lcom/sec/android/app/ve/theme/Theme;
.super Ljava/lang/Object;
.source "Theme.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/ve/theme/Theme$DownloadAdapter;
    }
.end annotation


# static fields
.field public static final THEME_DATA:Ljava/lang/String; = "themedata"


# instance fields
.field private mActivity:Landroid/app/Activity;

.field private final mDownloader:Lcom/sec/android/app/ve/theme/Download;

.field private mThemeAdapter:Lcom/sec/android/app/ve/theme/ThemeManager$Adapter;

.field protected mThemeDrawableResId:I

.field protected mThumb:Ljava/lang/ref/SoftReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/SoftReference",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->createDownload()Lcom/sec/android/app/ve/theme/Download;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/ve/theme/Theme;->mDownloader:Lcom/sec/android/app/ve/theme/Download;

    .line 21
    iput-object v1, p0, Lcom/sec/android/app/ve/theme/Theme;->mThemeAdapter:Lcom/sec/android/app/ve/theme/ThemeManager$Adapter;

    .line 22
    iput-object v1, p0, Lcom/sec/android/app/ve/theme/Theme;->mActivity:Landroid/app/Activity;

    .line 60
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/ve/theme/Theme;->mThemeDrawableResId:I

    .line 16
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/app/ve/theme/Theme;)Lcom/sec/android/app/ve/theme/ThemeManager$Adapter;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/android/app/ve/theme/Theme;->mThemeAdapter:Lcom/sec/android/app/ve/theme/ThemeManager$Adapter;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/app/ve/theme/Theme;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/app/ve/theme/Theme;->mActivity:Landroid/app/Activity;

    return-object v0
.end method


# virtual methods
.method protected abstract getPreviewPathInAssets()Ljava/lang/String;
.end method

.method protected getThemeAssetManager()Landroid/content/res/AssetManager;
    .locals 5

    .prologue
    .line 98
    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getAppPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 99
    .local v2, "pm":Landroid/content/pm/PackageManager;
    const/4 v0, 0x0

    .line 101
    .local v0, "assetManager":Landroid/content/res/AssetManager;
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/ve/theme/Theme;->getThemePackage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v3

    .line 102
    .local v3, "res":Landroid/content/res/Resources;
    invoke-virtual {v3}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 109
    .end local v3    # "res":Landroid/content/res/Resources;
    :goto_0
    return-object v0

    .line 104
    :catch_0
    move-exception v1

    .line 105
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method protected getThemeDataPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    const-string v0, "themedata"

    return-object v0
.end method

.method protected abstract getThemeDescription()Ljava/lang/String;
.end method

.method protected abstract getThemeDisplayName()Ljava/lang/String;
.end method

.method public abstract getThemeDownloadAlertMessage()Ljava/lang/String;
.end method

.method public getThemeDrawable(Z)Landroid/graphics/drawable/Drawable;
    .locals 3
    .param p1, "createIfRequired"    # Z

    .prologue
    .line 40
    iget v1, p0, Lcom/sec/android/app/ve/theme/Theme;->mThemeDrawableResId:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    .line 41
    const/4 v0, 0x0

    .line 50
    :cond_0
    :goto_0
    return-object v0

    .line 43
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/ve/theme/Theme;->mThumb:Ljava/lang/ref/SoftReference;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/ve/theme/Theme;->mThumb:Ljava/lang/ref/SoftReference;

    invoke-virtual {v1}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_3

    :cond_2
    if-eqz p1, :cond_3

    .line 44
    new-instance v1, Ljava/lang/ref/SoftReference;

    iget v2, p0, Lcom/sec/android/app/ve/theme/Theme;->mThemeDrawableResId:I

    invoke-static {v2}, Lcom/sec/android/app/ve/VEApp;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/sec/android/app/ve/theme/Theme;->mThumb:Ljava/lang/ref/SoftReference;

    .line 46
    :cond_3
    const/4 v0, 0x0

    .line 47
    .local v0, "draw":Landroid/graphics/drawable/Drawable;
    iget-object v1, p0, Lcom/sec/android/app/ve/theme/Theme;->mThumb:Ljava/lang/ref/SoftReference;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/ve/theme/Theme;->mThumb:Ljava/lang/ref/SoftReference;

    invoke-virtual {v1}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 48
    iget-object v1, p0, Lcom/sec/android/app/ve/theme/Theme;->mThumb:Ljava/lang/ref/SoftReference;

    invoke-virtual {v1}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "draw":Landroid/graphics/drawable/Drawable;
    check-cast v0, Landroid/graphics/drawable/Drawable;

    .restart local v0    # "draw":Landroid/graphics/drawable/Drawable;
    goto :goto_0
.end method

.method protected getThemeDuration()J
    .locals 2

    .prologue
    .line 29
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method protected abstract getThemeID()I
.end method

.method protected abstract getThemeJSONFileName()Ljava/lang/String;
.end method

.method protected getThemeJSONFilePath()Ljava/lang/String;
    .locals 2

    .prologue
    .line 77
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/sec/android/app/ve/theme/Theme;->getThemeDataPath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 78
    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 79
    invoke-virtual {p0}, Lcom/sec/android/app/ve/theme/Theme;->getThemeJSONFileName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 77
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected abstract getThemeName()Ljava/lang/String;
.end method

.method protected abstract getThemePackage()Ljava/lang/String;
.end method

.method protected abstract getThemePathFromRaw()I
.end method

.method protected initializeTheme()Z
    .locals 1

    .prologue
    .line 92
    const/4 v0, 0x1

    return v0
.end method

.method protected isThemeDownloaded()Z
    .locals 4

    .prologue
    .line 64
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getAppPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 65
    .local v1, "pm":Landroid/content/pm/PackageManager;
    invoke-virtual {p0}, Lcom/sec/android/app/ve/theme/Theme;->getThemePackage()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x80

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 69
    const/4 v2, 0x1

    .end local v1    # "pm":Landroid/content/pm/PackageManager;
    :goto_0
    return v2

    .line 66
    :catch_0
    move-exception v0

    .line 67
    .local v0, "e":Ljava/lang/Exception;
    const/4 v2, 0x0

    goto :goto_0
.end method

.method protected startDownload(Lcom/sec/android/app/ve/theme/ThemeManager$Adapter;Landroid/app/Activity;I)V
    .locals 2
    .param p1, "mThemeAdapter"    # Lcom/sec/android/app/ve/theme/ThemeManager$Adapter;
    .param p2, "actvt"    # Landroid/app/Activity;
    .param p3, "pos"    # I

    .prologue
    .line 84
    iput-object p1, p0, Lcom/sec/android/app/ve/theme/Theme;->mThemeAdapter:Lcom/sec/android/app/ve/theme/ThemeManager$Adapter;

    .line 85
    iput-object p2, p0, Lcom/sec/android/app/ve/theme/Theme;->mActivity:Landroid/app/Activity;

    .line 86
    iget-object v0, p0, Lcom/sec/android/app/ve/theme/Theme;->mDownloader:Lcom/sec/android/app/ve/theme/Download;

    new-instance v1, Lcom/sec/android/app/ve/theme/Theme$DownloadAdapter;

    invoke-direct {v1, p0, p3}, Lcom/sec/android/app/ve/theme/Theme$DownloadAdapter;-><init>(Lcom/sec/android/app/ve/theme/Theme;I)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/ve/theme/Download;->setAdapter(Lcom/sec/android/app/ve/theme/Download$Adapter;)V

    .line 87
    iget-object v0, p0, Lcom/sec/android/app/ve/theme/Theme;->mDownloader:Lcom/sec/android/app/ve/theme/Download;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/theme/Download;->startDownload()V

    .line 88
    return-void
.end method
