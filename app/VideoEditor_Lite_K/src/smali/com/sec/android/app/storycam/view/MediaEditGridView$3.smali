.class Lcom/sec/android/app/storycam/view/MediaEditGridView$3;
.super Ljava/lang/Object;
.source "MediaEditGridView.java"

# interfaces
.implements Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$ThumbnailDataProvider;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/storycam/view/MediaEditGridView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/storycam/view/MediaEditGridView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView$3;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getBitmapForIndex(I)Landroid/graphics/Bitmap;
    .locals 6
    .param p1, "index"    # I

    .prologue
    .line 98
    sget-object v4, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v4}, Lcom/sec/android/app/storycam/AppDataManager;->getOriginalTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v3

    .line 99
    .local v3, "trans":Lcom/samsung/app/video/editor/external/TranscodeElement;
    const/4 v0, 0x0

    .line 100
    .local v0, "bmp":Landroid/graphics/Bitmap;
    const/4 v2, 0x0

    .line 101
    .local v2, "filepath":Ljava/lang/String;
    invoke-virtual {v3, p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElement(I)Lcom/samsung/app/video/editor/external/Element;

    move-result-object v1

    .line 102
    .local v1, "element":Lcom/samsung/app/video/editor/external/Element;
    if-eqz v1, :cond_2

    .line 103
    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/Element;->getFilePath()Ljava/lang/String;

    move-result-object v2

    .line 104
    sget-object v4, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v4, v2}, Lcom/sec/android/app/storycam/AppDataManager;->getThumbnail(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 105
    if-nez v0, :cond_0

    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/Element;->getID()I

    move-result v4

    const/4 v5, -0x1

    if-ne v4, v5, :cond_0

    .line 106
    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/Element;->getType()I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_1

    .line 107
    iget-object v4, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView$3;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    # getter for: Lcom/sec/android/app/storycam/view/MediaEditGridView;->mImageUnsupported:Landroid/graphics/Bitmap;
    invoke-static {v4}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->access$2(Lcom/sec/android/app/storycam/view/MediaEditGridView;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 115
    :cond_0
    :goto_0
    return-object v0

    .line 109
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView$3;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    # getter for: Lcom/sec/android/app/storycam/view/MediaEditGridView;->mVideoUnsupported:Landroid/graphics/Bitmap;
    invoke-static {v4}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->access$3(Lcom/sec/android/app/storycam/view/MediaEditGridView;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 111
    goto :goto_0

    .line 112
    :cond_2
    const/4 v2, 0x0

    .line 113
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getFilePathForIndex(I)Ljava/lang/String;
    .locals 3
    .param p1, "index"    # I

    .prologue
    .line 120
    sget-object v2, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v2}, Lcom/sec/android/app/storycam/AppDataManager;->getOriginalTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v1

    .line 121
    .local v1, "trans":Lcom/samsung/app/video/editor/external/TranscodeElement;
    invoke-virtual {v1, p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElement(I)Lcom/samsung/app/video/editor/external/Element;

    move-result-object v0

    .line 122
    .local v0, "element":Lcom/samsung/app/video/editor/external/Element;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/samsung/app/video/editor/external/Element;->getFilePath()Ljava/lang/String;

    move-result-object v2

    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method
