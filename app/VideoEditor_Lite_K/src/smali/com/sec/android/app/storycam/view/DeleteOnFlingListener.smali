.class public Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;
.super Ljava/lang/Object;
.source "DeleteOnFlingListener.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/storycam/view/DeleteOnFlingListener$DeleteCallbacks;
    }
.end annotation


# instance fields
.field private mAnimTime:J

.field private mCallbacks:Lcom/sec/android/app/storycam/view/DeleteOnFlingListener$DeleteCallbacks;

.field private mFlinging:Z

.field private mFlingingSlop:I

.field private mHeight:I

.field private mSlop:I

.field private mTransX:F

.field private mTransY:F

.field private mVelocityTracker:Landroid/view/VelocityTracker;

.field private mView:Landroid/view/View;

.field private mWidth:I

.field private mXCord:F

.field private mYCord:F


# direct methods
.method public constructor <init>(Landroid/view/View;Lcom/sec/android/app/storycam/view/DeleteOnFlingListener$DeleteCallbacks;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "callbacks"    # Lcom/sec/android/app/storycam/view/DeleteOnFlingListener$DeleteCallbacks;

    .prologue
    const/4 v1, 0x1

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput v1, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mWidth:I

    .line 18
    iput v1, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mHeight:I

    .line 39
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 40
    .local v0, "vc":Landroid/view/ViewConfiguration;
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mSlop:I

    .line 41
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x10e0000

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-long v2, v1

    iput-wide v2, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mAnimTime:J

    .line 42
    iput-object p1, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mView:Landroid/view/View;

    .line 43
    iput-object p2, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mCallbacks:Lcom/sec/android/app/storycam/view/DeleteOnFlingListener$DeleteCallbacks;

    .line 44
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;)Lcom/sec/android/app/storycam/view/DeleteOnFlingListener$DeleteCallbacks;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mCallbacks:Lcom/sec/android/app/storycam/view/DeleteOnFlingListener$DeleteCallbacks;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;)Landroid/view/View;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mView:Landroid/view/View;

    return-object v0
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 11
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/high16 v10, 0x40000000    # 2.0f

    const/4 v5, 0x0

    const/high16 v9, 0x3f800000    # 1.0f

    const/4 v4, 0x1

    const/4 v8, 0x0

    .line 48
    iget-object v6, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mCallbacks:Lcom/sec/android/app/storycam/view/DeleteOnFlingListener$DeleteCallbacks;

    invoke-interface {v6}, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener$DeleteCallbacks;->getConfig()Landroid/content/res/Configuration;

    move-result-object v6

    if-eqz v6, :cond_a

    iget-object v6, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mCallbacks:Lcom/sec/android/app/storycam/view/DeleteOnFlingListener$DeleteCallbacks;

    invoke-interface {v6}, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener$DeleteCallbacks;->getConfig()Landroid/content/res/Configuration;

    move-result-object v6

    iget v6, v6, Landroid/content/res/Configuration;->orientation:I

    const/4 v7, 0x2

    if-ne v6, v7, :cond_a

    .line 50
    iget v6, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mTransY:F

    invoke-virtual {p2, v6, v8}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 51
    iget v6, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mHeight:I

    const/4 v7, 0x2

    if-ge v6, v7, :cond_0

    .line 52
    iget-object v6, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mView:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getHeight()I

    move-result v6

    iput v6, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mHeight:I

    .line 54
    :cond_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    :cond_1
    :goto_0
    move v4, v5

    .line 243
    :cond_2
    :goto_1
    return v4

    .line 56
    :pswitch_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v4

    iput v4, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mXCord:F

    .line 57
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v4

    iput v4, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mYCord:F

    .line 58
    iget-object v4, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mCallbacks:Lcom/sec/android/app/storycam/view/DeleteOnFlingListener$DeleteCallbacks;

    invoke-interface {v4}, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener$DeleteCallbacks;->canDelete()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 59
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 60
    iget-object v4, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-eqz v4, :cond_1

    .line 63
    iget-object v4, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v4, p2}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    :cond_3
    move v4, v5

    .line 65
    goto :goto_1

    .line 69
    :pswitch_1
    iget-object v6, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-eqz v6, :cond_1

    .line 72
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v6

    iget v7, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mYCord:F

    sub-float v3, v6, v7

    .line 73
    .local v3, "deltaY":F
    iget-object v6, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v6, p2}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 74
    iget-object v6, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mVelocityTracker:Landroid/view/VelocityTracker;

    const/16 v7, 0x3e8

    invoke-virtual {v6, v7}, Landroid/view/VelocityTracker;->computeCurrentVelocity(I)V

    .line 75
    const/4 v0, 0x0

    .line 76
    .local v0, "delete":Z
    const/4 v1, 0x0

    .line 77
    .local v1, "deleteRight":Z
    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v6

    iget v7, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mHeight:I

    int-to-float v7, v7

    div-float/2addr v7, v10

    cmpl-float v6, v6, v7

    if-lez v6, :cond_4

    .line 78
    const/4 v0, 0x1

    .line 79
    cmpl-float v6, v3, v8

    if-lez v6, :cond_5

    move v1, v4

    .line 81
    :cond_4
    :goto_2
    if-eqz v0, :cond_7

    .line 82
    iget-object v5, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mView:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v6

    .line 83
    if-eqz v1, :cond_6

    iget v5, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mHeight:I

    :goto_3
    int-to-float v5, v5

    invoke-virtual {v6, v5}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    .line 84
    invoke-virtual {v5, v8}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    .line 85
    iget-wide v6, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mAnimTime:J

    invoke-virtual {v5, v6, v7}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    .line 86
    new-instance v6, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener$1;

    invoke-direct {v6, p0}, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener$1;-><init>(Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;)V

    invoke-virtual {v5, v6}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    goto :goto_1

    :cond_5
    move v1, v5

    .line 79
    goto :goto_2

    .line 83
    :cond_6
    iget v5, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mHeight:I

    neg-int v5, v5

    goto :goto_3

    .line 92
    :cond_7
    iget-boolean v5, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mFlinging:Z

    if-eqz v5, :cond_2

    .line 93
    iget-object v5, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mView:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    .line 94
    invoke-virtual {v5, v8}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    .line 95
    invoke-virtual {v5, v9}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    .line 96
    iget-wide v6, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mAnimTime:J

    invoke-virtual {v5, v6, v7}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    .line 97
    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    goto/16 :goto_1

    .line 103
    .end local v0    # "delete":Z
    .end local v1    # "deleteRight":Z
    .end local v3    # "deltaY":F
    :pswitch_2
    iget-object v4, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-eqz v4, :cond_1

    .line 106
    iget-object v4, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    .line 107
    invoke-virtual {v4, v8}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    .line 108
    invoke-virtual {v4, v9}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    .line 109
    iget-wide v6, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mAnimTime:J

    invoke-virtual {v4, v6, v7}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    .line 110
    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 111
    iget-object v4, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v4}, Landroid/view/VelocityTracker;->recycle()V

    .line 112
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 113
    iput v8, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mTransY:F

    .line 114
    iput v8, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mXCord:F

    .line 115
    iput v8, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mYCord:F

    .line 116
    iput-boolean v5, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mFlinging:Z

    goto/16 :goto_0

    .line 121
    :pswitch_3
    iget-object v6, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-eqz v6, :cond_1

    .line 124
    iget-object v6, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v6, p2}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 125
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v6

    iget v7, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mXCord:F

    sub-float v2, v6, v7

    .line 126
    .local v2, "deltaX":F
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v6

    iget v7, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mYCord:F

    sub-float v3, v6, v7

    .line 127
    .restart local v3    # "deltaY":F
    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v6

    iget v7, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mSlop:I

    int-to-float v7, v7

    cmpl-float v6, v6, v7

    if-lez v6, :cond_8

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v6

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v7

    div-float/2addr v7, v10

    cmpg-float v6, v6, v7

    if-gez v6, :cond_8

    .line 128
    iput-boolean v4, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mFlinging:Z

    .line 129
    cmpl-float v6, v3, v8

    if-lez v6, :cond_9

    iget v6, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mSlop:I

    :goto_4
    iput v6, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mFlingingSlop:I

    .line 130
    iget-object v6, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mView:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v6

    invoke-interface {v6, v4}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 132
    :cond_8
    iget-boolean v6, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mFlinging:Z

    if-eqz v6, :cond_1

    .line 133
    iput v3, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mTransY:F

    .line 134
    iget-object v5, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mView:Landroid/view/View;

    iget v6, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mFlingingSlop:I

    int-to-float v6, v6

    sub-float v6, v3, v6

    invoke-virtual {v5, v6}, Landroid/view/View;->setTranslationY(F)V

    .line 136
    iget-object v5, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mView:Landroid/view/View;

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v6

    mul-float/2addr v6, v10

    iget v7, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mHeight:I

    int-to-float v7, v7

    div-float/2addr v6, v7

    sub-float v6, v9, v6

    invoke-static {v9, v6}, Ljava/lang/Math;->min(FF)F

    move-result v6

    invoke-static {v8, v6}, Ljava/lang/Math;->max(FF)F

    move-result v6

    invoke-virtual {v5, v6}, Landroid/view/View;->setAlpha(F)V

    goto/16 :goto_1

    .line 129
    :cond_9
    iget v6, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mSlop:I

    neg-int v6, v6

    goto :goto_4

    .line 146
    .end local v2    # "deltaX":F
    .end local v3    # "deltaY":F
    :cond_a
    iget v6, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mTransX:F

    invoke-virtual {p2, v6, v8}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 147
    iget v6, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mWidth:I

    const/4 v7, 0x2

    if-ge v6, v7, :cond_b

    .line 148
    iget-object v6, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mView:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getWidth()I

    move-result v6

    iput v6, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mWidth:I

    .line 150
    :cond_b
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v6

    packed-switch v6, :pswitch_data_1

    goto/16 :goto_0

    .line 152
    :pswitch_4
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v4

    iput v4, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mXCord:F

    .line 153
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v4

    iput v4, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mYCord:F

    .line 154
    iget-object v4, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mCallbacks:Lcom/sec/android/app/storycam/view/DeleteOnFlingListener$DeleteCallbacks;

    invoke-interface {v4}, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener$DeleteCallbacks;->canDelete()Z

    move-result v4

    if-eqz v4, :cond_c

    .line 155
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 156
    iget-object v4, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-eqz v4, :cond_1

    .line 159
    iget-object v4, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v4, p2}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    :cond_c
    move v4, v5

    .line 161
    goto/16 :goto_1

    .line 165
    :pswitch_5
    iget-object v6, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-eqz v6, :cond_1

    .line 168
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v6

    iget v7, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mXCord:F

    sub-float v2, v6, v7

    .line 169
    .restart local v2    # "deltaX":F
    iget-object v6, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v6, p2}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 170
    iget-object v6, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mVelocityTracker:Landroid/view/VelocityTracker;

    const/16 v7, 0x3e8

    invoke-virtual {v6, v7}, Landroid/view/VelocityTracker;->computeCurrentVelocity(I)V

    .line 171
    const/4 v0, 0x0

    .line 172
    .restart local v0    # "delete":Z
    const/4 v1, 0x0

    .line 173
    .restart local v1    # "deleteRight":Z
    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v6

    iget v7, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mWidth:I

    int-to-float v7, v7

    div-float/2addr v7, v10

    cmpl-float v6, v6, v7

    if-lez v6, :cond_d

    .line 174
    const/4 v0, 0x1

    .line 175
    cmpl-float v6, v2, v8

    if-lez v6, :cond_e

    move v1, v4

    .line 177
    :cond_d
    :goto_5
    if-eqz v0, :cond_10

    .line 178
    iget-object v5, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mView:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v6

    .line 179
    if-eqz v1, :cond_f

    iget v5, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mWidth:I

    :goto_6
    int-to-float v5, v5

    invoke-virtual {v6, v5}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    .line 180
    invoke-virtual {v5, v8}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    .line 181
    iget-wide v6, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mAnimTime:J

    invoke-virtual {v5, v6, v7}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    .line 182
    new-instance v6, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener$2;

    invoke-direct {v6, p0}, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener$2;-><init>(Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;)V

    invoke-virtual {v5, v6}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    goto/16 :goto_1

    :cond_e
    move v1, v5

    .line 175
    goto :goto_5

    .line 179
    :cond_f
    iget v5, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mWidth:I

    neg-int v5, v5

    goto :goto_6

    .line 188
    :cond_10
    iget-boolean v5, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mFlinging:Z

    if-eqz v5, :cond_2

    .line 189
    iget-object v5, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mView:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    .line 190
    invoke-virtual {v5, v8}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    .line 191
    invoke-virtual {v5, v9}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    .line 192
    iget-wide v6, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mAnimTime:J

    invoke-virtual {v5, v6, v7}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    .line 193
    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    goto/16 :goto_1

    .line 199
    .end local v0    # "delete":Z
    .end local v1    # "deleteRight":Z
    .end local v2    # "deltaX":F
    :pswitch_6
    iget-object v4, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-eqz v4, :cond_1

    .line 203
    iget-object v4, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    .line 204
    invoke-virtual {v4, v8}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    .line 205
    invoke-virtual {v4, v9}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    .line 206
    iget-wide v6, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mAnimTime:J

    invoke-virtual {v4, v6, v7}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    .line 207
    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 208
    iget-object v4, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v4}, Landroid/view/VelocityTracker;->recycle()V

    .line 209
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 210
    iput v8, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mTransX:F

    .line 211
    iput v8, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mXCord:F

    .line 212
    iput v8, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mYCord:F

    .line 213
    iput-boolean v5, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mFlinging:Z

    goto/16 :goto_0

    .line 218
    :pswitch_7
    iget-object v6, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-eqz v6, :cond_1

    .line 221
    iget-object v6, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v6, p2}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 222
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v6

    iget v7, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mXCord:F

    sub-float v2, v6, v7

    .line 223
    .restart local v2    # "deltaX":F
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v6

    iget v7, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mYCord:F

    sub-float v3, v6, v7

    .line 224
    .restart local v3    # "deltaY":F
    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v6

    iget v7, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mSlop:I

    int-to-float v7, v7

    cmpl-float v6, v6, v7

    if-lez v6, :cond_11

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v6

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v7

    div-float/2addr v7, v10

    cmpg-float v6, v6, v7

    if-gez v6, :cond_11

    .line 225
    iput-boolean v4, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mFlinging:Z

    .line 226
    cmpl-float v6, v2, v8

    if-lez v6, :cond_12

    iget v6, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mSlop:I

    :goto_7
    iput v6, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mFlingingSlop:I

    .line 227
    iget-object v6, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mView:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v6

    invoke-interface {v6, v4}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 230
    :cond_11
    iget-boolean v6, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mFlinging:Z

    if-eqz v6, :cond_1

    .line 231
    iput v2, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mTransX:F

    .line 232
    iget-object v5, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mView:Landroid/view/View;

    iget v6, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mFlingingSlop:I

    int-to-float v6, v6

    sub-float v6, v2, v6

    invoke-virtual {v5, v6}, Landroid/view/View;->setTranslationX(F)V

    .line 234
    iget-object v5, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mView:Landroid/view/View;

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v6

    mul-float/2addr v6, v10

    iget v7, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mWidth:I

    int-to-float v7, v7

    div-float/2addr v6, v7

    sub-float v6, v9, v6

    invoke-static {v9, v6}, Ljava/lang/Math;->min(FF)F

    move-result v6

    invoke-static {v8, v6}, Ljava/lang/Math;->max(FF)F

    move-result v6

    invoke-virtual {v5, v6}, Landroid/view/View;->setAlpha(F)V

    goto/16 :goto_1

    .line 226
    :cond_12
    iget v6, p0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;->mSlop:I

    neg-int v6, v6

    goto :goto_7

    .line 54
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
    .end packed-switch

    .line 150
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
        :pswitch_7
        :pswitch_6
    .end packed-switch
.end method
