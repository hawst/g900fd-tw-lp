.class public Lcom/sec/android/app/ve/util/PlaylistsCacheUtil;
.super Ljava/lang/Object;
.source "PlaylistsCacheUtil.java"


# static fields
.field public static playlistList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/ve/data/VEAlbum;",
            ">;"
        }
    .end annotation
.end field

.field private static playlistThread:Ljava/lang/Thread;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static clear()V
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lcom/sec/android/app/ve/util/PlaylistsCacheUtil;->playlistList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 46
    return-void
.end method

.method public static defaultAudioWhereClause()Ljava/lang/String;
    .locals 2

    .prologue
    .line 143
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 144
    .local v0, "where":Ljava/lang/StringBuilder;
    const-string v1, " (is_music = 1) "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 145
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static durationWhereClause()Ljava/lang/String;
    .locals 2

    .prologue
    .line 156
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 157
    .local v0, "where":Ljava/lang/StringBuilder;
    const-string v1, " (duration > 0) "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 159
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getAllPlayLists()Landroid/database/Cursor;
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 67
    const/4 v0, 0x3

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v0

    const/4 v0, 0x1

    .line 68
    const-string v4, "name"

    aput-object v4, v2, v0

    const/4 v0, 0x2

    .line 69
    const-string v4, "_data"

    aput-object v4, v2, v0

    .line 70
    .local v2, "cols":[Ljava/lang/String;
    sget-object v1, Landroid/provider/MediaStore$Audio$Playlists;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 71
    .local v1, "playlist_uri":Landroid/net/Uri;
    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 73
    const-string v5, "name"

    move-object v4, v3

    .line 72
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 74
    .local v6, "cursor":Landroid/database/Cursor;
    return-object v6
.end method

.method public static getPlayListSongs(I)Landroid/database/Cursor;
    .locals 8
    .param p0, "aPlayListId"    # I

    .prologue
    .line 116
    const-string v0, "external"

    int-to-long v4, p0

    .line 115
    invoke-static {v0, v4, v5}, Landroid/provider/MediaStore$Audio$Playlists$Members;->getContentUri(Ljava/lang/String;J)Landroid/net/Uri;

    move-result-object v1

    .line 118
    .local v1, "lPlayUri":Landroid/net/Uri;
    const/16 v0, 0xc

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v0

    const/4 v0, 0x1

    .line 119
    const-string v4, "_id"

    aput-object v4, v2, v0

    const/4 v0, 0x2

    .line 120
    const-string v4, "title"

    aput-object v4, v2, v0

    const/4 v0, 0x3

    const-string v4, "_data"

    aput-object v4, v2, v0

    const/4 v0, 0x4

    .line 121
    const-string v4, "album"

    aput-object v4, v2, v0

    const/4 v0, 0x5

    const-string v4, "album_id"

    aput-object v4, v2, v0

    const/4 v0, 0x6

    .line 122
    const-string v4, "artist"

    aput-object v4, v2, v0

    const/4 v0, 0x7

    .line 123
    const-string v4, "artist_id"

    aput-object v4, v2, v0

    const/16 v0, 0x8

    .line 124
    const-string v4, "duration"

    aput-object v4, v2, v0

    const/16 v0, 0x9

    .line 125
    const-string v4, "play_order"

    aput-object v4, v2, v0

    const/16 v0, 0xa

    .line 126
    const-string v4, "audio_id"

    aput-object v4, v2, v0

    const/16 v0, 0xb

    .line 127
    const-string v4, "is_music"

    aput-object v4, v2, v0

    .line 128
    .local v2, "lCols":[Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 129
    .local v6, "builder":Ljava/lang/StringBuilder;
    invoke-static {}, Lcom/sec/android/app/ve/util/PlaylistsCacheUtil;->durationWhereClause()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 130
    const-string v0, " AND "

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 131
    invoke-static {}, Lcom/sec/android/app/ve/util/PlaylistsCacheUtil;->mp3WhereClause()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 132
    const-string v0, " AND "

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 133
    invoke-static {}, Lcom/sec/android/app/ve/util/PlaylistsCacheUtil;->defaultAudioWhereClause()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 134
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 136
    .local v3, "selection":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 137
    const/4 v4, 0x0

    .line 138
    const-string v5, "title"

    .line 137
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 139
    .local v7, "lPlayListCursor":Landroid/database/Cursor;
    return-object v7
.end method

.method public static getPlaylistList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/ve/data/VEAlbum;",
            ">;"
        }
    .end annotation

    .prologue
    .line 41
    sget-object v0, Lcom/sec/android/app/ve/util/PlaylistsCacheUtil;->playlistList:Ljava/util/List;

    return-object v0
.end method

.method public static loadPlaylistList()V
    .locals 2

    .prologue
    .line 18
    sget-object v0, Lcom/sec/android/app/ve/util/PlaylistsCacheUtil;->playlistThread:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/app/ve/util/PlaylistsCacheUtil;->playlistThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 38
    :goto_0
    return-void

    .line 21
    :cond_0
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/app/ve/util/PlaylistsCacheUtil$1;

    invoke-direct {v1}, Lcom/sec/android/app/ve/util/PlaylistsCacheUtil$1;-><init>()V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    sput-object v0, Lcom/sec/android/app/ve/util/PlaylistsCacheUtil;->playlistThread:Ljava/lang/Thread;

    .line 36
    sget-object v0, Lcom/sec/android/app/ve/util/PlaylistsCacheUtil;->playlistThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 37
    sget-object v0, Lcom/sec/android/app/ve/util/PlaylistsCacheUtil;->playlistThread:Ljava/lang/Thread;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setPriority(I)V

    goto :goto_0
.end method

.method public static mp3WhereClause()Ljava/lang/String;
    .locals 2

    .prologue
    .line 149
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 150
    .local v0, "where":Ljava/lang/StringBuilder;
    const-string v1, " (_data LIKE \'%.mp3\' OR _data LIKE  \'%.3gpp\' OR _data LIKE  \'%.MP3\' OR _data LIKE  \'%.3GPP\'OR _data LIKE  \'%.3ga\' OR _data LIKE  \'%.3GA\' OR _data LIKE  \'%.m4a\' OR _data LIKE  \'%.M4A\' OR _data LIKE  \'%.3GP\' OR _data LIKE  \'%.3gp\') "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 152
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private static parsePlaylists(Landroid/database/Cursor;)Ljava/util/List;
    .locals 6
    .param p0, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/ve/data/VEAlbum;",
            ">;"
        }
    .end annotation

    .prologue
    .line 79
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 82
    .local v2, "musicSubgroupDAOList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/ve/data/VEAlbum;>;"
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    .line 83
    .local v4, "valid":Z
    :goto_0
    if-nez v4, :cond_0

    .line 93
    return-object v2

    .line 84
    :cond_0
    const/4 v5, 0x0

    invoke-interface {p0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 85
    .local v0, "id":Ljava/lang/String;
    const/4 v5, 0x1

    invoke-interface {p0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 86
    .local v3, "name":Ljava/lang/String;
    new-instance v1, Lcom/sec/android/app/ve/data/VEAlbum;

    invoke-direct {v1}, Lcom/sec/android/app/ve/data/VEAlbum;-><init>()V

    .line 88
    .local v1, "lVEAlbum":Lcom/sec/android/app/ve/data/VEAlbum;
    invoke-virtual {v1, v0}, Lcom/sec/android/app/ve/data/VEAlbum;->setMusicCategoryId(Ljava/lang/String;)V

    .line 89
    invoke-virtual {v1, v3}, Lcom/sec/android/app/ve/data/VEAlbum;->setMusicCategoryName(Ljava/lang/String;)V

    .line 90
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 91
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    goto :goto_0
.end method

.method public static readPlaylistsFromDB()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/ve/data/VEAlbum;",
            ">;"
        }
    .end annotation

    .prologue
    .line 50
    const/4 v2, 0x0

    .line 52
    .local v2, "lVEAlbumList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/ve/data/VEAlbum;>;"
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/ve/util/PlaylistsCacheUtil;->getAllPlayLists()Landroid/database/Cursor;

    move-result-object v0

    .line 53
    .local v0, "cursor":Landroid/database/Cursor;
    if-eqz v0, :cond_0

    .line 54
    invoke-static {v0}, Lcom/sec/android/app/ve/util/PlaylistsCacheUtil;->parsePlaylists(Landroid/database/Cursor;)Ljava/util/List;

    move-result-object v2

    .line 56
    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    move-object v3, v2

    .line 62
    .end local v0    # "cursor":Landroid/database/Cursor;
    :goto_0
    return-object v3

    .line 58
    :catch_0
    move-exception v1

    .line 59
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 60
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method public static readSongsCountOfPlaylist(Ljava/lang/String;)I
    .locals 4
    .param p0, "playlistId"    # Ljava/lang/String;

    .prologue
    .line 99
    const/4 v0, 0x0

    .line 101
    .local v0, "count":I
    :try_start_0
    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 102
    .local v2, "id":I
    invoke-static {v2}, Lcom/sec/android/app/ve/util/PlaylistsCacheUtil;->getPlayListSongs(I)Landroid/database/Cursor;

    move-result-object v1

    .line 103
    .local v1, "cursor":Landroid/database/Cursor;
    if-eqz v1, :cond_0

    .line 104
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    .line 105
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 110
    .end local v1    # "cursor":Landroid/database/Cursor;
    .end local v2    # "id":I
    :cond_0
    :goto_0
    return v0

    .line 107
    :catch_0
    move-exception v3

    .line 108
    .local v3, "pe":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
