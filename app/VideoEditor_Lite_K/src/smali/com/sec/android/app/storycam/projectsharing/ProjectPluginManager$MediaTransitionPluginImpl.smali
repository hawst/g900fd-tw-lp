.class public Lcom/sec/android/app/storycam/projectsharing/ProjectPluginManager$MediaTransitionPluginImpl;
.super Ljava/lang/Object;
.source "ProjectPluginManager.java"

# interfaces
.implements Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework$MediaTransitionPlugin;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/storycam/projectsharing/ProjectPluginManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MediaTransitionPluginImpl"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 251
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public mapTransitionFromGeneric(I)I
    .locals 1
    .param p1, "genericTransitionType"    # I

    .prologue
    .line 305
    const/4 v0, 0x0

    .line 306
    .local v0, "internalTransitiontype":I
    packed-switch p1, :pswitch_data_0

    .line 336
    :goto_0
    :pswitch_0
    return v0

    .line 308
    :pswitch_1
    const/4 v0, 0x1

    .line 309
    goto :goto_0

    .line 311
    :pswitch_2
    const/4 v0, 0x3

    .line 312
    goto :goto_0

    .line 314
    :pswitch_3
    const/16 v0, 0x29

    .line 315
    goto :goto_0

    .line 317
    :pswitch_4
    const/4 v0, 0x2

    .line 318
    goto :goto_0

    .line 320
    :pswitch_5
    const/16 v0, 0x2a

    .line 321
    goto :goto_0

    .line 323
    :pswitch_6
    const/16 v0, 0x28

    .line 324
    goto :goto_0

    .line 326
    :pswitch_7
    const/16 v0, 0x39

    .line 327
    goto :goto_0

    .line 329
    :pswitch_8
    const/16 v0, 0x9

    .line 330
    goto :goto_0

    .line 332
    :pswitch_9
    const/4 v0, 0x5

    .line 333
    goto :goto_0

    .line 306
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method public mapTransitionToGeneric(I)I
    .locals 1
    .param p1, "internalTransitionType"    # I

    .prologue
    .line 255
    const/4 v0, 0x0

    .line 257
    .local v0, "genericTransitionType":I
    packed-switch p1, :pswitch_data_0

    .line 300
    :goto_0
    :pswitch_0
    return v0

    .line 259
    :pswitch_1
    const/16 v0, 0xb

    .line 260
    goto :goto_0

    .line 262
    :pswitch_2
    const/16 v0, 0xc

    .line 263
    goto :goto_0

    .line 265
    :pswitch_3
    const/16 v0, 0xd

    .line 266
    goto :goto_0

    .line 268
    :pswitch_4
    const/16 v0, 0xe

    .line 269
    goto :goto_0

    .line 271
    :pswitch_5
    const/16 v0, 0xf

    .line 272
    goto :goto_0

    .line 274
    :pswitch_6
    const/16 v0, 0x10

    .line 275
    goto :goto_0

    .line 277
    :pswitch_7
    const/16 v0, 0x11

    .line 278
    goto :goto_0

    .line 280
    :pswitch_8
    const/16 v0, 0x12

    .line 281
    goto :goto_0

    .line 283
    :pswitch_9
    const/16 v0, 0x13

    .line 284
    goto :goto_0

    .line 286
    :pswitch_a
    const/16 v0, 0x14

    .line 287
    goto :goto_0

    .line 289
    :pswitch_b
    const/16 v0, 0x15

    .line 290
    goto :goto_0

    .line 292
    :pswitch_c
    const/16 v0, 0x16

    .line 293
    goto :goto_0

    .line 295
    :pswitch_d
    const/16 v0, 0x17

    .line 296
    goto :goto_0

    .line 257
    :pswitch_data_0
    .packed-switch 0x52
        :pswitch_a
        :pswitch_0
        :pswitch_c
        :pswitch_0
        :pswitch_0
        :pswitch_9
        :pswitch_b
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_1
        :pswitch_d
        :pswitch_7
        :pswitch_6
        :pswitch_3
        :pswitch_2
        :pswitch_8
    .end packed-switch
.end method
