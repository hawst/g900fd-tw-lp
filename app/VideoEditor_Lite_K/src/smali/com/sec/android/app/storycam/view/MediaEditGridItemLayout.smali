.class public Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;
.super Landroid/widget/RelativeLayout;
.source "MediaEditGridItemLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MediaGridItemInterface;,
        Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;,
        Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$ThumbnailDataProvider;
    }
.end annotation


# static fields
.field private static final MSG_SHOW_HOVER_POPUP:I

.field private static final mHandler:Landroid/os/Handler;

.field private static mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;


# instance fields
.field private mConfig:Landroid/content/res/Configuration;

.field private mGridRect:Landroid/graphics/Rect;

.field private final mHoverListener:Landroid/view/View$OnHoverListener;

.field private mHoverWindow:Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;

.field private mHoverWindowDismissListener:Landroid/content/DialogInterface$OnDismissListener;

.field private mMediaDurationTextView:Landroid/widget/TextView;

.field private mMediaDurationView:Landroid/widget/RelativeLayout;

.field private mMetaData:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;

.field private mPlayClickListener:Landroid/view/View$OnClickListener;

.field private mPlayIconResId:I

.field private mPlayView:Landroid/widget/ImageView;

.field private mSwipeListener:Landroid/view/View$OnTouchListener;

.field private mThumbnailView:Landroid/widget/ImageView;

.field private thumbnailBorder:Landroid/widget/ImageView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 139
    new-instance v0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$4;

    invoke-direct {v0}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$4;-><init>()V

    sput-object v0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mHandler:Landroid/os/Handler;

    .line 150
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 164
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 42
    const v0, 0x7f020062

    iput v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mPlayIconResId:I

    .line 49
    new-instance v0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;

    new-instance v1, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$1;-><init>(Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;)V

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;-><init>(Landroid/view/View;Lcom/sec/android/app/storycam/view/DeleteOnFlingListener$DeleteCallbacks;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mSwipeListener:Landroid/view/View$OnTouchListener;

    .line 67
    new-instance v0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$2;-><init>(Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mPlayClickListener:Landroid/view/View$OnClickListener;

    .line 101
    new-instance v0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$3;-><init>(Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mHoverListener:Landroid/view/View$OnHoverListener;

    .line 152
    new-instance v0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$5;-><init>(Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mHoverWindowDismissListener:Landroid/content/DialogInterface$OnDismissListener;

    .line 165
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 160
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 42
    const v0, 0x7f020062

    iput v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mPlayIconResId:I

    .line 49
    new-instance v0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;

    new-instance v1, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$1;-><init>(Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;)V

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;-><init>(Landroid/view/View;Lcom/sec/android/app/storycam/view/DeleteOnFlingListener$DeleteCallbacks;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mSwipeListener:Landroid/view/View$OnTouchListener;

    .line 67
    new-instance v0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$2;-><init>(Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mPlayClickListener:Landroid/view/View$OnClickListener;

    .line 101
    new-instance v0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$3;-><init>(Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mHoverListener:Landroid/view/View$OnHoverListener;

    .line 152
    new-instance v0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$5;-><init>(Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mHoverWindowDismissListener:Landroid/content/DialogInterface$OnDismissListener;

    .line 161
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 168
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 42
    const v0, 0x7f020062

    iput v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mPlayIconResId:I

    .line 49
    new-instance v0, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;

    new-instance v1, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$1;-><init>(Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;)V

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;-><init>(Landroid/view/View;Lcom/sec/android/app/storycam/view/DeleteOnFlingListener$DeleteCallbacks;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mSwipeListener:Landroid/view/View$OnTouchListener;

    .line 67
    new-instance v0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$2;-><init>(Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mPlayClickListener:Landroid/view/View$OnClickListener;

    .line 101
    new-instance v0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$3;-><init>(Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mHoverListener:Landroid/view/View$OnHoverListener;

    .line 152
    new-instance v0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$5;-><init>(Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mHoverWindowDismissListener:Landroid/content/DialogInterface$OnDismissListener;

    .line 169
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;)Landroid/content/res/Configuration;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mConfig:Landroid/content/res/Configuration;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;)Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mMetaData:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;

    return-object v0
.end method

.method static synthetic access$10(Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;)V
    .locals 0

    .prologue
    .line 37
    iput-object p1, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mHoverWindow:Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;

    return-void
.end method

.method static synthetic access$2(Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;)I
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mPlayIconResId:I

    return v0
.end method

.method static synthetic access$3(Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mPlayView:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$4(Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;I)V
    .locals 0

    .prologue
    .line 42
    iput p1, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mPlayIconResId:I

    return-void
.end method

.method static synthetic access$5()Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    return-object v0
.end method

.method static synthetic access$6(Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;)Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mHoverWindow:Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;

    return-object v0
.end method

.method static synthetic access$7()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 139
    sget-object v0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$8(Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;)V
    .locals 0

    .prologue
    .line 205
    invoke-direct {p0}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->dismissHoverWindowDelayed()V

    return-void
.end method

.method static synthetic access$9(Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;)V
    .locals 0

    .prologue
    .line 211
    invoke-direct {p0}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->createAndShowHoverWindow()V

    return-void
.end method

.method private createAndShowHoverWindow()V
    .locals 18

    .prologue
    .line 212
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mMetaData:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;

    if-eqz v10, :cond_0

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mMetaData:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;

    # getter for: Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;->mThumbProvider:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$ThumbnailDataProvider;
    invoke-static {v10}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;->access$2(Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;)Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$ThumbnailDataProvider;

    move-result-object v10

    if-eqz v10, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v10

    if-eqz v10, :cond_0

    .line 213
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mMetaData:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;

    # getter for: Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;->mThumbProvider:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$ThumbnailDataProvider;
    invoke-static {v10}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;->access$2(Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;)Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$ThumbnailDataProvider;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mMetaData:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;

    # getter for: Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;->index:I
    invoke-static {v11}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;->access$1(Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;)I

    move-result v11

    invoke-interface {v10, v11}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$ThumbnailDataProvider;->getBitmapForIndex(I)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 214
    .local v7, "bmp":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mMetaData:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;

    # getter for: Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;->mThumbProvider:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$ThumbnailDataProvider;
    invoke-static {v10}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;->access$2(Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;)Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$ThumbnailDataProvider;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mMetaData:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;

    # getter for: Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;->index:I
    invoke-static {v11}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;->access$1(Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;)I

    move-result v11

    invoke-interface {v10, v11}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$ThumbnailDataProvider;->getFilePathForIndex(I)Ljava/lang/String;

    move-result-object v8

    .line 216
    .local v8, "filePath":Ljava/lang/String;
    if-nez v7, :cond_1

    .line 217
    sget-object v10, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mHandler:Landroid/os/Handler;

    const/4 v11, 0x0

    move-object/from16 v0, p0

    invoke-static {v10, v11, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v13

    .line 218
    .local v13, "msg":Landroid/os/Message;
    sget-object v10, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mHandler:Landroid/os/Handler;

    const-wide/16 v16, 0x190

    move-wide/from16 v0, v16

    invoke-virtual {v10, v13, v0, v1}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 266
    .end local v7    # "bmp":Landroid/graphics/Bitmap;
    .end local v8    # "filePath":Ljava/lang/String;
    .end local v13    # "msg":Landroid/os/Message;
    :cond_0
    :goto_0
    return-void

    .line 222
    .restart local v7    # "bmp":Landroid/graphics/Bitmap;
    .restart local v8    # "filePath":Ljava/lang/String;
    :cond_1
    new-instance v15, Landroid/graphics/Rect;

    invoke-direct {v15}, Landroid/graphics/Rect;-><init>()V

    .line 228
    .local v15, "r":Landroid/graphics/Rect;
    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v11

    if-le v10, v11, :cond_a

    .line 229
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f06033d

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 230
    .local v5, "width":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f06033d

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v11

    mul-int/2addr v10, v11

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v11

    div-int v6, v10, v11

    .line 236
    .local v6, "height":I
    :goto_1
    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 237
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mGridRect:Landroid/graphics/Rect;

    if-nez v10, :cond_2

    .line 238
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v14

    check-cast v14, Landroid/view/View;

    .line 239
    .local v14, "parent":Landroid/view/View;
    new-instance v10, Landroid/graphics/Rect;

    invoke-direct {v10}, Landroid/graphics/Rect;-><init>()V

    move-object/from16 v0, p0

    iput-object v10, v0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mGridRect:Landroid/graphics/Rect;

    .line 240
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mGridRect:Landroid/graphics/Rect;

    invoke-virtual {v14, v10}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 241
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mGridRect:Landroid/graphics/Rect;

    iget v11, v10, Landroid/graphics/Rect;->left:I

    invoke-virtual {v14}, Landroid/view/View;->getPaddingLeft()I

    move-result v12

    add-int/2addr v11, v12

    iput v11, v10, Landroid/graphics/Rect;->left:I

    .line 242
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mGridRect:Landroid/graphics/Rect;

    iget v11, v10, Landroid/graphics/Rect;->right:I

    invoke-virtual {v14}, Landroid/view/View;->getPaddingRight()I

    move-result v12

    sub-int/2addr v11, v12

    iput v11, v10, Landroid/graphics/Rect;->right:I

    .line 245
    .end local v14    # "parent":Landroid/view/View;
    :cond_2
    iget v10, v15, Landroid/graphics/Rect;->left:I

    invoke-virtual {v15}, Landroid/graphics/Rect;->width()I

    move-result v11

    sub-int v11, v5, v11

    div-int/lit8 v11, v11, 0x2

    sub-int v3, v10, v11

    .line 246
    .local v3, "left":I
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mGridRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->left:I

    if-ge v3, v10, :cond_3

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mGridRect:Landroid/graphics/Rect;

    iget v3, v10, Landroid/graphics/Rect;->left:I

    .line 247
    :cond_3
    iget v10, v15, Landroid/graphics/Rect;->bottom:I

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f06033e

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    sub-int/2addr v10, v11

    sub-int v4, v10, v6

    .line 248
    .local v4, "top":I
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mGridRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->top:I

    if-ge v4, v10, :cond_4

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mGridRect:Landroid/graphics/Rect;

    iget v4, v10, Landroid/graphics/Rect;->top:I

    .line 250
    :cond_4
    add-int v10, v3, v5

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mGridRect:Landroid/graphics/Rect;

    iget v11, v11, Landroid/graphics/Rect;->right:I

    if-le v10, v11, :cond_5

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mGridRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->right:I

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f06033e

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    sub-int/2addr v10, v11

    sub-int v3, v10, v5

    .line 251
    :cond_5
    add-int v10, v4, v6

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mGridRect:Landroid/graphics/Rect;

    iget v11, v11, Landroid/graphics/Rect;->bottom:I

    if-le v10, v11, :cond_6

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mGridRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->bottom:I

    sub-int v4, v10, v6

    .line 253
    :cond_6
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mGridRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->left:I

    if-ne v3, v10, :cond_7

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f06033e

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    add-int/2addr v3, v10

    .line 254
    :cond_7
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mGridRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->top:I

    if-ne v4, v10, :cond_8

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f06033e

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    add-int/2addr v4, v10

    .line 256
    :cond_8
    new-instance v10, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mContext:Landroid/content/Context;

    invoke-direct {v10, v11}, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v10, v0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mHoverWindow:Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;

    .line 257
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mHoverWindow:Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mHoverWindowDismissListener:Landroid/content/DialogInterface$OnDismissListener;

    invoke-virtual {v10, v11}, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 258
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mMetaData:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;

    # getter for: Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;->mMediaType:I
    invoke-static {v10}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;->access$3(Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;)I

    move-result v10

    const/4 v11, 0x2

    if-ne v10, v11, :cond_b

    sget-object v9, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowType;->IMAGE_HOVER_WINDOW:Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowType;

    .line 259
    .local v9, "windowType":Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowType;
    :goto_2
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mMetaData:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;

    # getter for: Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;->bSupportedMedia:Z
    invoke-static {v10}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;->access$4(Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;)Z

    move-result v10

    if-nez v10, :cond_9

    .line 260
    sget-object v9, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowType;->IMAGE_HOVER_WINDOW:Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowType;

    .line 263
    :cond_9
    new-instance v2, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;

    new-instance v10, Landroid/graphics/RectF;

    invoke-direct {v10, v15}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    const/4 v11, 0x1

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mMetaData:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;

    # getter for: Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;->bSupportedMedia:Z
    invoke-static {v12}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;->access$4(Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;)Z

    move-result v12

    invoke-direct/range {v2 .. v12}, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;-><init>(IIIILandroid/graphics/Bitmap;Ljava/lang/String;Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowType;Landroid/graphics/RectF;IZ)V

    .line 264
    .local v2, "params":Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mHoverWindow:Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;

    invoke-virtual {v10, v2}, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;->show(Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;)V

    goto/16 :goto_0

    .line 232
    .end local v2    # "params":Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;
    .end local v3    # "left":I
    .end local v4    # "top":I
    .end local v5    # "width":I
    .end local v6    # "height":I
    .end local v9    # "windowType":Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowType;
    :cond_a
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f06033d

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 233
    .restart local v6    # "height":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f06033d

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v11

    mul-int/2addr v10, v11

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v11

    div-int v5, v10, v11

    .restart local v5    # "width":I
    goto/16 :goto_1

    .line 258
    .restart local v3    # "left":I
    .restart local v4    # "top":I
    :cond_b
    sget-object v9, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowType;->VIDEO_HOVER_WINDOW:Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowType;

    goto :goto_2
.end method

.method private dismissHoverWindowDelayed()V
    .locals 2

    .prologue
    .line 206
    sget-object v0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 207
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mHoverWindow:Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;

    if-eqz v0, :cond_0

    .line 208
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mHoverWindow:Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;

    invoke-virtual {v0}, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;->dismissDelayed()V

    .line 209
    :cond_0
    return-void
.end method

.method private init()V
    .locals 3

    .prologue
    const v2, 0x7f0d0032

    .line 188
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    sput-object v0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    .line 189
    invoke-virtual {p0, v2}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mPlayView:Landroid/widget/ImageView;

    .line 190
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mPlayView:Landroid/widget/ImageView;

    iget v1, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mPlayIconResId:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 191
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mPlayView:Landroid/widget/ImageView;

    const v1, 0x7f07026d

    invoke-static {v1}, Lcom/sec/android/app/storycam/VEAppSpecific;->getStringValue(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 192
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mPlayView:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mPlayClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 193
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mSwipeListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 194
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 195
    const v0, 0x7f0d002d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mThumbnailView:Landroid/widget/ImageView;

    .line 196
    const v0, 0x7f0d0033

    invoke-virtual {p0, v0}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mMediaDurationView:Landroid/widget/RelativeLayout;

    .line 197
    const v0, 0x7f0d0034

    invoke-virtual {p0, v0}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mMediaDurationTextView:Landroid/widget/TextView;

    .line 198
    const v0, 0x7f0d002c

    invoke-virtual {p0, v0}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->thumbnailBorder:Landroid/widget/ImageView;

    .line 199
    sget-object v0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    invoke-interface {v0}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;->getCurrentConfig()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 200
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->thumbnailBorder:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setNextFocusRightId(I)V

    .line 203
    :goto_0
    return-void

    .line 202
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->thumbnailBorder:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setNextFocusDownId(I)V

    goto :goto_0
.end method


# virtual methods
.method public getIndex()I
    .locals 1

    .prologue
    .line 318
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mMetaData:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;

    # getter for: Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;->index:I
    invoke-static {v0}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;->access$1(Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;)I

    move-result v0

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 179
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onDetachedFromWindow()V

    .line 180
    sget-object v0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 181
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mHoverWindow:Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;

    if-eqz v0, :cond_0

    .line 182
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mHoverWindow:Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 183
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mHoverWindow:Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;

    invoke-virtual {v0}, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;->dismiss()V

    .line 185
    :cond_0
    return-void
.end method

.method protected onFinishInflate()V
    .locals 0

    .prologue
    .line 173
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 174
    invoke-direct {p0}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->init()V

    .line 175
    return-void
.end method

.method public refreshView(Landroid/graphics/Bitmap;Z)V
    .locals 7
    .param p1, "bmp"    # Landroid/graphics/Bitmap;
    .param p2, "committed"    # Z

    .prologue
    const/4 v6, 0x0

    const/16 v5, 0x8

    .line 284
    if-eqz p2, :cond_3

    .line 285
    iget-object v3, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mThumbnailView:Landroid/widget/ImageView;

    invoke-virtual {v3, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 286
    new-instance v0, Ljava/io/File;

    sget-object v3, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v3}, Lcom/sec/android/app/storycam/AppDataManager;->getOriginalTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mMetaData:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;

    # getter for: Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;->index:I
    invoke-static {v4}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;->access$1(Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElement(I)Lcom/samsung/app/video/editor/external/Element;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/Element;->getFilePath()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 287
    .local v0, "fileName":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    .line 288
    .local v1, "titleName":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mThumbnailView:Landroid/widget/ImageView;

    invoke-virtual {v3, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 289
    iget-object v3, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->thumbnailBorder:Landroid/widget/ImageView;

    invoke-virtual {v3, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 290
    iget-object v3, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mThumbnailView:Landroid/widget/ImageView;

    const v4, 0x7f050001

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 291
    iget-object v3, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mMetaData:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;

    # getter for: Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;->mMediaType:I
    invoke-static {v3}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;->access$3(Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;)I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_2

    .line 292
    iget-object v3, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mMetaData:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;

    # getter for: Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;->bSupportedMedia:Z
    invoke-static {v3}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;->access$4(Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 293
    iget-object v3, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mPlayView:Landroid/widget/ImageView;

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 294
    sget-object v3, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v3}, Lcom/sec/android/app/storycam/AppDataManager;->getOriginalTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mMetaData:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;

    # getter for: Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;->index:I
    invoke-static {v4}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;->access$1(Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElement(I)Lcom/samsung/app/video/editor/external/Element;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/Element;->getVolumeEdit()Lcom/samsung/app/video/editor/external/Edit;

    move-result-object v2

    .line 295
    .local v2, "volumeEdit":Lcom/samsung/app/video/editor/external/Edit;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/Edit;->getVolumeLevel()I

    move-result v3

    if-gtz v3, :cond_0

    .line 296
    const v3, 0x7f02005e

    iput v3, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mPlayIconResId:I

    .line 300
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mPlayView:Landroid/widget/ImageView;

    iget v4, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mPlayIconResId:I

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 301
    iget-object v3, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mMediaDurationView:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v6}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 315
    .end local v0    # "fileName":Ljava/io/File;
    .end local v1    # "titleName":Ljava/lang/String;
    .end local v2    # "volumeEdit":Lcom/samsung/app/video/editor/external/Edit;
    :goto_1
    return-void

    .line 298
    .restart local v0    # "fileName":Ljava/io/File;
    .restart local v1    # "titleName":Ljava/lang/String;
    .restart local v2    # "volumeEdit":Lcom/samsung/app/video/editor/external/Edit;
    :cond_0
    const v3, 0x7f020062

    iput v3, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mPlayIconResId:I

    goto :goto_0

    .line 303
    .end local v2    # "volumeEdit":Lcom/samsung/app/video/editor/external/Edit;
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mPlayView:Landroid/widget/ImageView;

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 304
    iget-object v3, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mMediaDurationView:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_1

    .line 307
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mPlayView:Landroid/widget/ImageView;

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 308
    iget-object v3, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mMediaDurationView:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_1

    .line 312
    .end local v0    # "fileName":Ljava/io/File;
    .end local v1    # "titleName":Ljava/lang/String;
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mThumbnailView:Landroid/widget/ImageView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 313
    iget-object v3, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mThumbnailView:Landroid/widget/ImageView;

    const v4, 0x7f02000d

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_1
.end method

.method public setConfig(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "config"    # Landroid/content/res/Configuration;

    .prologue
    .line 280
    iput-object p1, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mConfig:Landroid/content/res/Configuration;

    .line 281
    return-void
.end method

.method public setMediaDuration(Ljava/lang/String;)V
    .locals 6
    .param p1, "timeInText"    # Ljava/lang/String;

    .prologue
    const/16 v5, 0x3a

    .line 347
    invoke-virtual {p1, v5}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {p1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 348
    .local v1, "minuSecsStr":Ljava/lang/String;
    const/4 v3, 0x0

    invoke-virtual {v1, v5}, Ljava/lang/String;->indexOf(I)I

    move-result v4

    invoke-virtual {v1, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 349
    .local v0, "minsStr":Ljava/lang/String;
    invoke-virtual {p1, v5}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v1, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 350
    .local v2, "secsStr":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mMediaDurationTextView:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 351
    iget-object v3, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mMediaDurationTextView:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const v5, 0x7f0701fd

    invoke-static {v5}, Lcom/sec/android/app/storycam/VEAppSpecific;->getStringValue(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const v5, 0x7f0701fe

    invoke-static {v5}, Lcom/sec/android/app/storycam/VEAppSpecific;->getStringValue(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 352
    return-void
.end method

.method public setMetaData(ILcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MediaGridItemInterface;Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$ThumbnailDataProvider;IZ)V
    .locals 6
    .param p1, "index"    # I
    .param p2, "listener"    # Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MediaGridItemInterface;
    .param p3, "thumbProvider"    # Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$ThumbnailDataProvider;
    .param p4, "mediaType"    # I
    .param p5, "supportedMedia"    # Z

    .prologue
    .line 269
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mMetaData:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;

    if-nez v0, :cond_0

    .line 270
    new-instance v0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;-><init>(ILcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MediaGridItemInterface;Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$ThumbnailDataProvider;IZ)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mMetaData:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;

    .line 278
    :goto_0
    return-void

    .line 272
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mMetaData:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;

    invoke-static {v0, p2}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;->access$5(Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MediaGridItemInterface;)V

    .line 273
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mMetaData:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;

    invoke-static {v0, p1}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;->access$6(Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;I)V

    .line 274
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mMetaData:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;

    invoke-static {v0, p3}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;->access$7(Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$ThumbnailDataProvider;)V

    .line 275
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mMetaData:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;

    invoke-static {v0, p4}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;->access$8(Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;I)V

    .line 276
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mMetaData:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;

    invoke-static {v0, p5}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;->access$9(Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;Z)V

    goto :goto_0
.end method
