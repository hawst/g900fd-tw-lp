.class public Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher;
.super Ljava/lang/Thread;
.source "StoryBoardBitmapFetcher.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher$FetcherCallback;,
        Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher$FetcherInfo;
    }
.end annotation


# instance fields
.field private mCurrFileUri:Ljava/lang/String;

.field private mJobList:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher$FetcherInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mRetriever:Landroid/media/MediaMetadataRetriever;

.field private mTerminate:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 26
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 28
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher;->mJobList:Ljava/util/Vector;

    .line 29
    iput-object v1, p0, Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher;->mRetriever:Landroid/media/MediaMetadataRetriever;

    .line 30
    iput-object v1, p0, Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher;->mCurrFileUri:Ljava/lang/String;

    .line 26
    return-void
.end method


# virtual methods
.method public addOperation(Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher$FetcherInfo;)I
    .locals 1
    .param p1, "fetcherInfo"    # Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher$FetcherInfo;

    .prologue
    .line 33
    if-eqz p1, :cond_1

    .line 34
    iget-object v0, p0, Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher;->mJobList:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 35
    iget-object v0, p0, Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher;->mJobList:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 37
    monitor-enter p0

    .line 38
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V

    .line 37
    monitor-exit p0

    .line 41
    const/4 v0, 0x1

    .line 46
    :goto_0
    return v0

    .line 37
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 44
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 46
    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public clearJobs()V
    .locals 2

    .prologue
    .line 71
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher;->mJobList:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->clear()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 75
    :goto_0
    return-void

    .line 72
    :catch_0
    move-exception v0

    .line 73
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public run()V
    .locals 9

    .prologue
    .line 81
    :goto_0
    :try_start_0
    iget-boolean v1, p0, Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher;->mTerminate:Z

    if-eqz v1, :cond_4

    .line 140
    :goto_1
    return-void

    .line 85
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher;->mJobList:Ljava/util/Vector;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher$FetcherInfo;

    .line 87
    .local v8, "info":Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher$FetcherInfo;
    iget-object v1, v8, Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher$FetcherInfo;->callback:Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher$FetcherCallback;

    iget-wide v2, v8, Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher$FetcherInfo;->time:J

    invoke-interface {v1, v2, v3}, Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher$FetcherCallback;->isTargetViewExist(J)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 88
    const/4 v4, 0x0

    .line 90
    .local v4, "bitmap":Landroid/graphics/Bitmap;
    iget-object v1, p0, Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher;->mCurrFileUri:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher;->mCurrFileUri:Ljava/lang/String;

    iget-object v2, v8, Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher$FetcherInfo;->filePath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_1

    .line 92
    iget-object v1, p0, Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher;->mRetriever:Landroid/media/MediaMetadataRetriever;

    invoke-static {v1}, Lcom/sec/android/app/ve/util/CommonUtils;->releaseRetriever(Landroid/media/MediaMetadataRetriever;)V

    .line 93
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher;->mRetriever:Landroid/media/MediaMetadataRetriever;

    .line 94
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher;->mCurrFileUri:Ljava/lang/String;

    .line 97
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher;->mRetriever:Landroid/media/MediaMetadataRetriever;

    if-nez v1, :cond_2

    .line 99
    iget-object v1, v8, Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher$FetcherInfo;->mAssetManager:Landroid/content/res/AssetManager;

    iget-object v2, v8, Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher$FetcherInfo;->filePath:Ljava/lang/String;

    iget-boolean v3, v8, Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher$FetcherInfo;->isAssetResource:Z

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/ve/util/CommonUtils;->getRetrieverForSource(Landroid/content/res/AssetManager;Ljava/lang/String;Z)Landroid/media/MediaMetadataRetriever;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher;->mRetriever:Landroid/media/MediaMetadataRetriever;

    .line 100
    iget-object v1, v8, Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher$FetcherInfo;->filePath:Ljava/lang/String;

    iput-object v1, p0, Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher;->mCurrFileUri:Ljava/lang/String;

    .line 103
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher;->mRetriever:Landroid/media/MediaMetadataRetriever;

    if-eqz v1, :cond_3

    .line 104
    iget-boolean v1, v8, Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher$FetcherInfo;->mMediaEmpty:Z

    if-nez v1, :cond_7

    .line 105
    iget-object v1, p0, Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher;->mRetriever:Landroid/media/MediaMetadataRetriever;

    iget-wide v2, v8, Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher$FetcherInfo;->time:J

    long-to-float v2, v2

    invoke-static {v1, v2}, Lcom/sec/android/app/ve/common/MediaUtils;->getVideoFrame(Landroid/media/MediaMetadataRetriever;F)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 106
    iget-object v1, v8, Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher$FetcherInfo;->callback:Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher$FetcherCallback;

    iget-wide v2, v8, Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher$FetcherInfo;->time:J

    iget-object v5, v8, Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher$FetcherInfo;->filePath:Ljava/lang/String;

    iget-object v6, v8, Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher$FetcherInfo;->filename_pos:Ljava/lang/String;

    const/4 v7, 0x1

    invoke-interface/range {v1 .. v7}, Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher$FetcherCallback;->bitmapCreated(JLandroid/graphics/Bitmap;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 117
    .end local v4    # "bitmap":Landroid/graphics/Bitmap;
    :cond_3
    :goto_2
    iget-object v1, p0, Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher;->mJobList:Ljava/util/Vector;

    invoke-virtual {v1, v8}, Ljava/util/Vector;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher;->mJobList:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    if-lez v1, :cond_4

    .line 118
    iget-object v1, p0, Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher;->mJobList:Ljava/util/Vector;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/Vector;->remove(I)Ljava/lang/Object;

    .line 82
    .end local v8    # "info":Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher$FetcherInfo;
    :cond_4
    iget-boolean v1, p0, Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher;->mTerminate:Z

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher;->mJobList:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 122
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher;->mRetriever:Landroid/media/MediaMetadataRetriever;

    invoke-static {v1}, Lcom/sec/android/app/ve/util/CommonUtils;->releaseRetriever(Landroid/media/MediaMetadataRetriever;)V

    .line 123
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher;->mRetriever:Landroid/media/MediaMetadataRetriever;

    .line 124
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher;->mCurrFileUri:Ljava/lang/String;

    .line 126
    monitor-enter p0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 128
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher;->mJobList:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 129
    const-wide/16 v2, 0x7d0

    invoke-virtual {p0, v2, v3}, Ljava/lang/Object;->wait(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 126
    :cond_6
    :goto_3
    :try_start_2
    monitor-exit p0

    goto/16 :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v1
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    .line 137
    :catch_0
    move-exception v0

    .line 138
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_1

    .line 109
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v4    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v8    # "info":Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher$FetcherInfo;
    :cond_7
    :try_start_4
    sget v1, Lcom/samsung/app/video/editor/external/Constants;->THUMBNAIL_IMAGE_SIZE:I

    sget v2, Lcom/samsung/app/video/editor/external/Constants;->THUMBNAIL_IMAGE_HEIGHT:I

    sget-object v3, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 110
    const/4 v1, 0x0

    invoke-virtual {v4, v1}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 111
    iget-object v1, v8, Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher$FetcherInfo;->callback:Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher$FetcherCallback;

    iget-wide v2, v8, Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher$FetcherInfo;->time:J

    iget-object v5, v8, Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher$FetcherInfo;->filePath:Ljava/lang/String;

    iget-object v6, v8, Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher$FetcherInfo;->filename_pos:Ljava/lang/String;

    const/4 v7, 0x1

    invoke-interface/range {v1 .. v7}, Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher$FetcherCallback;->bitmapCreated(JLandroid/graphics/Bitmap;Ljava/lang/String;Ljava/lang/String;Z)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_2

    .line 131
    .end local v4    # "bitmap":Landroid/graphics/Bitmap;
    .end local v8    # "info":Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher$FetcherInfo;
    :catch_1
    move-exception v0

    .line 132
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_5
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_3
.end method

.method public terminate()V
    .locals 2

    .prologue
    .line 54
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher;->mJobList:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->clear()V

    .line 55
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/ve/thread/StoryBoardBitmapFetcher;->mTerminate:Z

    .line 57
    monitor-enter p0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 58
    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V

    .line 57
    monitor-exit p0

    .line 64
    :goto_0
    return-void

    .line 57
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 61
    :catch_0
    move-exception v0

    .line 62
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
