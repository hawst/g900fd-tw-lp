.class public Lcom/sec/android/app/ve/thread/SummaryThread;
.super Ljava/lang/Thread;
.source "SummaryThread.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/ve/thread/SummaryThread$SummaryBit;
    }
.end annotation


# static fields
.field public static final MSG_DELAY_SUMMARY_WAKELOCK:I = 0x79

.field public static final MSG_REMOVE_SUMMARY_DIALOG:I = 0x7b

.field public static final MSG_SUMMARY_FAILURE:I = 0x7a

.field public static final MSG_SUMMARY_STOPPED:I = 0x7c

.field public static final SUMMARY_ANALYSIS_SUCCESS:I = 0x1

.field static final SUMMARY_ERROR:I = 0x1

.field static final SUMMARY_SUCCESS:I = 0x0

.field public static final VIDEO_SUMMARY:I = 0x3eb


# instance fields
.field private mContext:Landroid/content/Context;

.field private mCurrentTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

.field private mForceStop:Z

.field mHandler:Landroid/os/Handler;

.field mOperation:I

.field mSummaryDuration:J

.field private mode:I

.field private path:[Ljava/lang/String;

.field private summaryTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

.field wakelock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method public constructor <init>(Landroid/os/Handler;ILandroid/content/Context;Lcom/samsung/app/video/editor/external/TranscodeElement;JLandroid/os/Bundle;ILcom/samsung/app/video/editor/external/TranscodeElement;)V
    .locals 1
    .param p1, "handler"    # Landroid/os/Handler;
    .param p2, "operation"    # I
    .param p3, "context"    # Landroid/content/Context;
    .param p4, "transcodeElement"    # Lcom/samsung/app/video/editor/external/TranscodeElement;
    .param p5, "videoSummaryTime"    # J
    .param p7, "aBundle"    # Landroid/os/Bundle;
    .param p8, "mode"    # I
    .param p9, "output"    # Lcom/samsung/app/video/editor/external/TranscodeElement;

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 31
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/ve/thread/SummaryThread;->path:[Ljava/lang/String;

    .line 48
    iput-object p1, p0, Lcom/sec/android/app/ve/thread/SummaryThread;->mHandler:Landroid/os/Handler;

    .line 49
    iput p2, p0, Lcom/sec/android/app/ve/thread/SummaryThread;->mOperation:I

    .line 50
    iput-object p3, p0, Lcom/sec/android/app/ve/thread/SummaryThread;->mContext:Landroid/content/Context;

    .line 51
    iput-object p4, p0, Lcom/sec/android/app/ve/thread/SummaryThread;->mCurrentTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    .line 52
    iput-wide p5, p0, Lcom/sec/android/app/ve/thread/SummaryThread;->mSummaryDuration:J

    .line 53
    iput p8, p0, Lcom/sec/android/app/ve/thread/SummaryThread;->mode:I

    .line 54
    iput-object p9, p0, Lcom/sec/android/app/ve/thread/SummaryThread;->summaryTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    .line 55
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/ve/thread/SummaryThread;->mForceStop:Z

    .line 56
    return-void
.end method

.method private addFadeEffects(Lcom/samsung/app/video/editor/external/TranscodeElement;Lcom/samsung/app/video/editor/external/TranscodeElement;)V
    .locals 5
    .param p1, "newTE"    # Lcom/samsung/app/video/editor/external/TranscodeElement;
    .param p2, "oldTE"    # Lcom/samsung/app/video/editor/external/TranscodeElement;

    .prologue
    .line 436
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v4

    if-lez v4, :cond_0

    .line 437
    const/4 v4, 0x0

    invoke-virtual {p2, v4}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElement(I)Lcom/samsung/app/video/editor/external/Element;

    move-result-object v1

    .line 438
    .local v1, "oldFirstElement":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/Element;->getEditList()Ljava/util/List;

    move-result-object v0

    .line 439
    .local v0, "oldElemEditList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Edit;>;"
    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/Element;->getGroupID()I

    move-result v4

    invoke-direct {p0, v4, v0, p1}, Lcom/sec/android/app/ve/thread/SummaryThread;->copyFadeEditFromOldElement(ILjava/util/List;Lcom/samsung/app/video/editor/external/TranscodeElement;)V

    .line 441
    invoke-virtual {p2}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {p2, v4}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElement(I)Lcom/samsung/app/video/editor/external/Element;

    move-result-object v2

    .line 442
    .local v2, "oldLastElement":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/Element;->getEditList()Ljava/util/List;

    move-result-object v3

    .line 443
    .local v3, "oldLastElementEditList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Edit;>;"
    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/Element;->getGroupID()I

    move-result v4

    invoke-direct {p0, v4, v3, p1}, Lcom/sec/android/app/ve/thread/SummaryThread;->copyFadeEditFromOldElement(ILjava/util/List;Lcom/samsung/app/video/editor/external/TranscodeElement;)V

    .line 446
    .end local v0    # "oldElemEditList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Edit;>;"
    .end local v1    # "oldFirstElement":Lcom/samsung/app/video/editor/external/Element;
    .end local v2    # "oldLastElement":Lcom/samsung/app/video/editor/external/Element;
    .end local v3    # "oldLastElementEditList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Edit;>;"
    :cond_0
    return-void
.end method

.method private addRecordingListToSummary(Lcom/samsung/app/video/editor/external/TranscodeElement;Lcom/samsung/app/video/editor/external/TranscodeElement;Lcom/sec/android/app/ve/thread/SummaryThread$SummaryBit;)V
    .locals 28
    .param p1, "oldProject"    # Lcom/samsung/app/video/editor/external/TranscodeElement;
    .param p2, "summaryTranscode"    # Lcom/samsung/app/video/editor/external/TranscodeElement;
    .param p3, "currBit"    # Lcom/sec/android/app/ve/thread/SummaryThread$SummaryBit;

    .prologue
    .line 272
    const/16 v17, 0x0

    .line 275
    .local v17, "recordingsSize":I
    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/sec/android/app/ve/thread/SummaryThread$SummaryBit;->summaryElement:Lcom/samsung/app/video/editor/external/Element;

    move-object/from16 v22, v0

    move-object/from16 v0, p2

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getStoryTimeTillElement(Lcom/samsung/app/video/editor/external/Element;)J

    move-result-wide v20

    .line 276
    .local v20, "summaryStartTime":J
    move-object/from16 v0, p3

    iget-wide v4, v0, Lcom/sec/android/app/ve/thread/SummaryThread$SummaryBit;->actualOccurance:J

    .line 278
    .local v4, "actualSummaryStartTime":J
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getRecordListCount()I

    move-result v14

    .line 280
    .local v14, "newRecordListCnt":I
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getAdditionlRecordEleList()Ljava/util/List;

    move-result-object v22

    .line 281
    move-object/from16 v0, p3

    iget-wide v0, v0, Lcom/sec/android/app/ve/thread/SummaryThread$SummaryBit;->actualOccurance:J

    move-wide/from16 v24, v0

    move-wide/from16 v0, v24

    long-to-float v0, v0

    move/from16 v23, v0

    move-object/from16 v0, p3

    iget-wide v0, v0, Lcom/sec/android/app/ve/thread/SummaryThread$SummaryBit;->actualOccurance:J

    move-wide/from16 v24, v0

    move-object/from16 v0, p3

    iget-wide v0, v0, Lcom/sec/android/app/ve/thread/SummaryThread$SummaryBit;->endTime:J

    move-wide/from16 v26, v0

    add-long v24, v24, v26

    move-object/from16 v0, p3

    iget-wide v0, v0, Lcom/sec/android/app/ve/thread/SummaryThread$SummaryBit;->startTime:J

    move-wide/from16 v26, v0

    sub-long v24, v24, v26

    move-wide/from16 v0, v24

    long-to-float v0, v0

    move/from16 v24, v0

    .line 280
    move-object/from16 v0, p1

    move-object/from16 v1, v22

    move/from16 v2, v23

    move/from16 v3, v24

    invoke-virtual {v0, v14, v1, v2, v3}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementsBetween(ILjava/util/List;FF)Ljava/util/Vector;

    move-result-object v16

    .line 283
    .local v16, "recordings":Ljava/util/Vector;, "Ljava/util/Vector<Lcom/samsung/app/video/editor/external/Element;>;"
    if-eqz v16, :cond_0

    .line 284
    invoke-virtual/range {v16 .. v16}, Ljava/util/Vector;->size()I

    move-result v17

    .line 286
    :cond_0
    const/4 v12, 0x0

    .local v12, "j":I
    :goto_0
    move/from16 v0, v17

    if-lt v12, v0, :cond_1

    .line 313
    return-void

    .line 287
    :cond_1
    move-object/from16 v0, v16

    invoke-virtual {v0, v12}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/samsung/app/video/editor/external/Element;

    .line 288
    .local v15, "origRecordEle":Lcom/samsung/app/video/editor/external/Element;
    if-eqz v15, :cond_3

    .line 289
    new-instance v13, Lcom/samsung/app/video/editor/external/Element;

    invoke-direct {v13, v15}, Lcom/samsung/app/video/editor/external/Element;-><init>(Lcom/samsung/app/video/editor/external/Element;)V

    .line 290
    .local v13, "newRecordElement":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {v15}, Lcom/samsung/app/video/editor/external/Element;->getStoryBoardStartTime()J

    move-result-wide v18

    .line 291
    .local v18, "startTime":J
    invoke-virtual {v15}, Lcom/samsung/app/video/editor/external/Element;->getStoryBoardEndTime()J

    move-result-wide v10

    .line 293
    .local v10, "endTime":J
    sub-long v8, v10, v18

    .line 294
    .local v8, "duration":J
    add-long v22, v20, v18

    sub-long v6, v22, v4

    .line 295
    .local v6, "currentRecEleStartTime":J
    const-wide/16 v22, 0x3e8

    sub-long v22, v20, v22

    cmp-long v22, v6, v22

    if-gez v22, :cond_4

    .line 296
    sub-long v22, v20, v6

    move-wide/from16 v0, v22

    invoke-virtual {v13, v0, v1}, Lcom/samsung/app/video/editor/external/Element;->setStartTime(J)V

    .line 301
    :goto_1
    const-wide/16 v22, 0x3e8

    sub-long v22, v20, v22

    cmp-long v22, v6, v22

    if-gez v22, :cond_2

    move-wide/from16 v6, v20

    .line 303
    :cond_2
    add-long v22, v6, v8

    move-wide/from16 v0, v22

    invoke-virtual {v13, v0, v1}, Lcom/samsung/app/video/editor/external/Element;->setDuration(J)V

    .line 306
    invoke-virtual {v13, v6, v7}, Lcom/samsung/app/video/editor/external/Element;->setStoryBoardStartTime(J)V

    .line 307
    add-long v22, v6, v8

    move-wide/from16 v0, v22

    invoke-virtual {v13, v0, v1}, Lcom/samsung/app/video/editor/external/Element;->setStoryBoardEndTime(J)V

    .line 309
    invoke-virtual {v13}, Lcom/samsung/app/video/editor/external/Element;->getDuration()J

    move-result-wide v22

    const-wide/16 v24, 0x3e8

    cmp-long v22, v22, v24

    if-ltz v22, :cond_3

    .line 310
    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Lcom/samsung/app/video/editor/external/TranscodeElement;->addRecordEleList(Lcom/samsung/app/video/editor/external/Element;)V

    .line 286
    .end local v6    # "currentRecEleStartTime":J
    .end local v8    # "duration":J
    .end local v10    # "endTime":J
    .end local v13    # "newRecordElement":Lcom/samsung/app/video/editor/external/Element;
    .end local v18    # "startTime":J
    :cond_3
    add-int/lit8 v12, v12, 0x1

    goto :goto_0

    .line 298
    .restart local v6    # "currentRecEleStartTime":J
    .restart local v8    # "duration":J
    .restart local v10    # "endTime":J
    .restart local v13    # "newRecordElement":Lcom/samsung/app/video/editor/external/Element;
    .restart local v18    # "startTime":J
    :cond_4
    const-wide/16 v22, 0x0

    move-wide/from16 v0, v22

    invoke-virtual {v13, v0, v1}, Lcom/samsung/app/video/editor/external/Element;->setStartTime(J)V

    goto :goto_1
.end method

.method private addSoundEffectList(Lcom/samsung/app/video/editor/external/TranscodeElement;Ljava/util/Vector;Lcom/sec/android/app/ve/thread/SummaryThread$SummaryBit;)V
    .locals 30
    .param p1, "newTranscode"    # Lcom/samsung/app/video/editor/external/TranscodeElement;
    .param p3, "currBit"    # Lcom/sec/android/app/ve/thread/SummaryThread$SummaryBit;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/app/video/editor/external/TranscodeElement;",
            "Ljava/util/Vector",
            "<",
            "Lcom/sec/android/app/ve/thread/SummaryThread$SummaryBit;",
            ">;",
            "Lcom/sec/android/app/ve/thread/SummaryThread$SummaryBit;",
            ")V"
        }
    .end annotation

    .prologue
    .line 502
    .local p2, "summaryBits":Ljava/util/Vector;, "Ljava/util/Vector<Lcom/sec/android/app/ve/thread/SummaryThread$SummaryBit;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/ve/thread/SummaryThread;->mCurrentTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/ve/thread/SummaryThread;->mCurrentTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getAdditionlSoundEleList()Ljava/util/List;

    move-result-object v24

    .line 503
    move-object/from16 v0, p3

    iget-wide v0, v0, Lcom/sec/android/app/ve/thread/SummaryThread$SummaryBit;->actualOccurance:J

    move-wide/from16 v26, v0

    move-wide/from16 v0, v26

    long-to-float v0, v0

    move/from16 v25, v0

    .line 504
    move-object/from16 v0, p3

    iget-wide v0, v0, Lcom/sec/android/app/ve/thread/SummaryThread$SummaryBit;->actualOccurance:J

    move-wide/from16 v26, v0

    move-object/from16 v0, p3

    iget-wide v0, v0, Lcom/sec/android/app/ve/thread/SummaryThread$SummaryBit;->endTime:J

    move-wide/from16 v28, v0

    add-long v26, v26, v28

    .line 505
    move-object/from16 v0, p3

    iget-wide v0, v0, Lcom/sec/android/app/ve/thread/SummaryThread$SummaryBit;->startTime:J

    move-wide/from16 v28, v0

    .line 504
    sub-long v26, v26, v28

    move-wide/from16 v0, v26

    long-to-float v0, v0

    move/from16 v26, v0

    .line 502
    move-object/from16 v0, v19

    move-object/from16 v1, v24

    move/from16 v2, v25

    move/from16 v3, v26

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementsBetween(Ljava/util/List;FF)Ljava/util/Vector;

    move-result-object v17

    .line 507
    .local v17, "soundEffectList":Ljava/util/Vector;, "Ljava/util/Vector<Lcom/samsung/app/video/editor/external/Element;>;"
    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/sec/android/app/ve/thread/SummaryThread$SummaryBit;->summaryElement:Lcom/samsung/app/video/editor/external/Element;

    move-object/from16 v19, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getStoryTimeTillElement(Lcom/samsung/app/video/editor/external/Element;)J

    move-result-wide v22

    .line 508
    .local v22, "summaryStartTime":J
    move-object/from16 v0, p3

    iget-wide v0, v0, Lcom/sec/android/app/ve/thread/SummaryThread$SummaryBit;->endTime:J

    move-wide/from16 v24, v0

    add-long v24, v24, v22

    move-object/from16 v0, p3

    iget-wide v0, v0, Lcom/sec/android/app/ve/thread/SummaryThread$SummaryBit;->startTime:J

    move-wide/from16 v26, v0

    sub-long v20, v24, v26

    .line 509
    .local v20, "summaryEndTime":J
    move-object/from16 v0, p3

    iget-wide v10, v0, Lcom/sec/android/app/ve/thread/SummaryThread$SummaryBit;->actualOccurance:J

    .line 510
    .local v10, "actualSummaryStartTime":J
    invoke-virtual/range {v17 .. v17}, Ljava/util/Vector;->size()I

    move-result v18

    .line 512
    .local v18, "soundEffectListSize":I
    const/16 v16, 0x0

    .line 514
    .local v16, "newSoundEffectElement":Lcom/samsung/app/video/editor/external/Element;
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    move/from16 v0, v18

    if-lt v5, v0, :cond_0

    .line 557
    return-void

    .line 515
    :cond_0
    move-object/from16 v0, v17

    invoke-virtual {v0, v5}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/app/video/editor/external/Element;

    .line 517
    .local v4, "actualSoundEffectElement":Lcom/samsung/app/video/editor/external/Element;
    if-eqz v4, :cond_3

    .line 518
    new-instance v16, Lcom/samsung/app/video/editor/external/Element;

    .end local v16    # "newSoundEffectElement":Lcom/samsung/app/video/editor/external/Element;
    move-object/from16 v0, v16

    invoke-direct {v0, v4}, Lcom/samsung/app/video/editor/external/Element;-><init>(Lcom/samsung/app/video/editor/external/Element;)V

    .line 520
    .restart local v16    # "newSoundEffectElement":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/Element;->getStoryBoardStartTime()J

    move-result-wide v8

    .line 521
    .local v8, "actualSoundEffectElementStartTime":J
    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/Element;->getStoryBoardEndTime()J

    move-result-wide v6

    .line 523
    .local v6, "actualSoundEffectElementEndTime":J
    add-long v24, v22, v8

    sub-long v14, v24, v10

    .line 526
    .local v14, "currentSoundEffectElementStartTime":J
    add-long v24, v14, v6

    sub-long v12, v24, v8

    .line 530
    .local v12, "currentSoundEffectElementEndTime":J
    cmp-long v19, v14, v22

    if-gez v19, :cond_4

    .line 532
    sub-long v24, v22, v14

    .line 531
    move-object/from16 v0, v16

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Lcom/samsung/app/video/editor/external/Element;->setStartTime(J)V

    .line 537
    :goto_1
    cmp-long v19, v14, v22

    if-gez v19, :cond_1

    move-wide/from16 v14, v22

    .line 539
    :cond_1
    cmp-long v19, v12, v20

    if-lez v19, :cond_2

    move-wide/from16 v12, v20

    .line 542
    :cond_2
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/app/video/editor/external/Element;->getStartTime()J

    move-result-wide v24

    .line 543
    add-long v24, v24, v12

    .line 544
    sub-long v24, v24, v14

    .line 542
    move-object/from16 v0, v16

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Lcom/samsung/app/video/editor/external/Element;->setEndTime(J)V

    .line 546
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/app/video/editor/external/Element;->getEndTime()J

    move-result-wide v24

    .line 547
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/app/video/editor/external/Element;->getStartTime()J

    move-result-wide v26

    sub-long v24, v24, v26

    .line 546
    move-object/from16 v0, v16

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Lcom/samsung/app/video/editor/external/Element;->setDuration(J)V

    .line 549
    move-object/from16 v0, v16

    invoke-virtual {v0, v14, v15}, Lcom/samsung/app/video/editor/external/Element;->setStoryBoardStartTime(J)V

    .line 550
    move-object/from16 v0, v16

    invoke-virtual {v0, v12, v13}, Lcom/samsung/app/video/editor/external/Element;->setStoryBoardEndTime(J)V

    .line 552
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/app/video/editor/external/Element;->getDuration()J

    move-result-wide v24

    const-wide/16 v26, 0x3e8

    cmp-long v19, v24, v26

    if-ltz v19, :cond_3

    .line 553
    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->addSoundEleList(Lcom/samsung/app/video/editor/external/Element;)V

    .line 514
    .end local v6    # "actualSoundEffectElementEndTime":J
    .end local v8    # "actualSoundEffectElementStartTime":J
    .end local v12    # "currentSoundEffectElementEndTime":J
    .end local v14    # "currentSoundEffectElementStartTime":J
    :cond_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 534
    .restart local v6    # "actualSoundEffectElementEndTime":J
    .restart local v8    # "actualSoundEffectElementStartTime":J
    .restart local v12    # "currentSoundEffectElementEndTime":J
    .restart local v14    # "currentSoundEffectElementStartTime":J
    :cond_4
    const-wide/16 v24, 0x0

    move-object/from16 v0, v16

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Lcom/samsung/app/video/editor/external/Element;->setStartTime(J)V

    goto :goto_1
.end method

.method private copyFadeEditFromOldElement(ILjava/util/List;Lcom/samsung/app/video/editor/external/TranscodeElement;)V
    .locals 8
    .param p1, "groupID"    # I
    .param p3, "newTE"    # Lcom/samsung/app/video/editor/external/TranscodeElement;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/Edit;",
            ">;",
            "Lcom/samsung/app/video/editor/external/TranscodeElement;",
            ")V"
        }
    .end annotation

    .prologue
    .local p2, "oldEditList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Edit;>;"
    const/4 v7, 0x0

    .line 449
    if-eqz p2, :cond_0

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_0

    .line 450
    const/4 v0, 0x0

    .local v0, "n":I
    :goto_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v5

    if-lt v0, v5, :cond_1

    .line 466
    .end local v0    # "n":I
    :cond_0
    return-void

    .line 451
    .restart local v0    # "n":I
    :cond_1
    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/app/video/editor/external/Edit;

    .line 452
    .local v4, "originalEdit":Lcom/samsung/app/video/editor/external/Edit;
    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/Edit;->getType()I

    move-result v5

    const/4 v6, 0x4

    if-ne v5, v6, :cond_2

    .line 453
    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/Edit;->getSubType()I

    move-result v5

    const/16 v6, 0x33

    if-ne v5, v6, :cond_3

    .line 454
    invoke-virtual {p3, p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getFirstElementInGroup(I)Lcom/samsung/app/video/editor/external/Element;

    move-result-object v2

    .line 455
    .local v2, "newFirstElement":Lcom/samsung/app/video/editor/external/Element;
    new-instance v1, Lcom/samsung/app/video/editor/external/Edit;

    invoke-direct {v1, v4}, Lcom/samsung/app/video/editor/external/Edit;-><init>(Lcom/samsung/app/video/editor/external/Edit;)V

    .line 456
    .local v1, "newEdit":Lcom/samsung/app/video/editor/external/Edit;
    invoke-virtual {v2, v1, v7}, Lcom/samsung/app/video/editor/external/Element;->addEdit(Lcom/samsung/app/video/editor/external/Edit;Z)V

    .line 450
    .end local v1    # "newEdit":Lcom/samsung/app/video/editor/external/Edit;
    .end local v2    # "newFirstElement":Lcom/samsung/app/video/editor/external/Element;
    :cond_2
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 458
    :cond_3
    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/Edit;->getSubType()I

    move-result v5

    const/16 v6, 0x35

    if-ne v5, v6, :cond_2

    .line 459
    invoke-virtual {p3, p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getLastElementInGroup(I)Lcom/samsung/app/video/editor/external/Element;

    move-result-object v3

    .line 460
    .local v3, "newLastElement":Lcom/samsung/app/video/editor/external/Element;
    new-instance v1, Lcom/samsung/app/video/editor/external/Edit;

    invoke-direct {v1, v4}, Lcom/samsung/app/video/editor/external/Edit;-><init>(Lcom/samsung/app/video/editor/external/Edit;)V

    .line 461
    .restart local v1    # "newEdit":Lcom/samsung/app/video/editor/external/Edit;
    invoke-virtual {v3, v1, v7}, Lcom/samsung/app/video/editor/external/Element;->addEdit(Lcom/samsung/app/video/editor/external/Edit;Z)V

    goto :goto_1
.end method

.method private copyKenburns(Lcom/samsung/app/video/editor/external/Element;Lcom/samsung/app/video/editor/external/Element;)V
    .locals 3
    .param p1, "destEle"    # Lcom/samsung/app/video/editor/external/Element;
    .param p2, "srcEle"    # Lcom/samsung/app/video/editor/external/Element;

    .prologue
    .line 262
    invoke-virtual {p2}, Lcom/samsung/app/video/editor/external/Element;->getStartRect()Landroid/graphics/RectF;

    move-result-object v2

    .line 263
    .local v2, "startRect":Landroid/graphics/RectF;
    invoke-virtual {p2}, Lcom/samsung/app/video/editor/external/Element;->getEndRect()Landroid/graphics/RectF;

    move-result-object v0

    .line 264
    .local v0, "endRect":Landroid/graphics/RectF;
    invoke-virtual {p2}, Lcom/samsung/app/video/editor/external/Element;->getRefRect()Landroid/graphics/RectF;

    move-result-object v1

    .line 266
    .local v1, "refRect":Landroid/graphics/RectF;
    invoke-virtual {p1, v2}, Lcom/samsung/app/video/editor/external/Element;->setStartRect(Landroid/graphics/RectF;)V

    .line 267
    invoke-virtual {p1, v0}, Lcom/samsung/app/video/editor/external/Element;->setEndRect(Landroid/graphics/RectF;)V

    .line 268
    invoke-virtual {p1, v1}, Lcom/samsung/app/video/editor/external/Element;->setRefRect(Landroid/graphics/RectF;)V

    .line 269
    return-void
.end method

.method private createNoneThemeSummaryProject()Z
    .locals 30

    .prologue
    .line 122
    new-instance v25, Ljava/util/Vector;

    invoke-direct/range {v25 .. v25}, Ljava/util/Vector;-><init>()V

    .line 124
    .local v25, "summaryBits":Ljava/util/Vector;, "Ljava/util/Vector<Lcom/sec/android/app/ve/thread/SummaryThread$SummaryBit;>;"
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/ve/thread/SummaryThread;->mCurrentTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    .line 125
    .local v4, "oldTranscodeElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    const/4 v13, 0x0

    .line 126
    .local v13, "elem":Lcom/samsung/app/video/editor/external/Element;
    const/4 v11, 0x0

    .line 127
    .local v11, "edit":Lcom/samsung/app/video/editor/external/Edit;
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v3

    .line 128
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/ve/thread/SummaryThread;->summaryTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    .line 129
    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/sec/android/app/ve/thread/SummaryThread;->mSummaryDuration:J

    long-to-double v6, v6

    move-object/from16 v0, p0

    iget v8, v0, Lcom/sec/android/app/ve/thread/SummaryThread;->mode:I

    .line 128
    invoke-virtual/range {v3 .. v8}, Lcom/samsung/app/video/editor/external/NativeInterface;->createSummaryFromEngine(Lcom/samsung/app/video/editor/external/TranscodeElement;Lcom/samsung/app/video/editor/external/TranscodeElement;DI)I

    move-result v24

    .line 130
    .local v24, "outProject":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/ve/thread/SummaryThread;->summaryTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    const/4 v5, 0x1

    invoke-virtual {v3, v5}, Lcom/samsung/app/video/editor/external/TranscodeElement;->setSummaryProject(Z)V

    .line 132
    if-nez v24, :cond_0

    .line 133
    const/4 v3, 0x0

    .line 258
    :goto_0
    return v3

    .line 136
    :cond_0
    const/4 v14, 0x0

    .line 140
    .local v14, "i":I
    :goto_1
    new-instance v13, Lcom/samsung/app/video/editor/external/Element;

    .end local v13    # "elem":Lcom/samsung/app/video/editor/external/Element;
    invoke-direct {v13}, Lcom/samsung/app/video/editor/external/Element;-><init>()V

    .line 141
    .restart local v13    # "elem":Lcom/samsung/app/video/editor/external/Element;
    const/4 v3, 0x0

    iput-object v3, v13, Lcom/samsung/app/video/editor/external/Element;->filePath:Ljava/lang/String;

    .line 143
    new-instance v26, Lcom/samsung/app/video/editor/external/Edit;

    invoke-direct/range {v26 .. v26}, Lcom/samsung/app/video/editor/external/Edit;-><init>()V

    .line 144
    .local v26, "volumeEdit":Lcom/samsung/app/video/editor/external/Edit;
    const/4 v3, 0x1

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Lcom/samsung/app/video/editor/external/Edit;->setType(I)V

    .line 145
    const/4 v3, -0x1

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Lcom/samsung/app/video/editor/external/Edit;->setVolumeLevel(I)V

    .line 146
    move-object/from16 v0, v26

    invoke-virtual {v13, v0}, Lcom/samsung/app/video/editor/external/Element;->addEdit(Lcom/samsung/app/video/editor/external/Edit;)V

    .line 149
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v3

    move/from16 v0, v24

    invoke-virtual {v3, v13, v0, v14}, Lcom/samsung/app/video/editor/external/NativeInterface;->_getSummaryElementInElementListAt(Lcom/samsung/app/video/editor/external/Element;II)I

    .line 151
    iget-object v3, v13, Lcom/samsung/app/video/editor/external/Element;->filePath:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 211
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v3

    move/from16 v0, v24

    invoke-virtual {v3, v0}, Lcom/samsung/app/video/editor/external/NativeInterface;->_freeSummaryOutput(I)I

    .line 214
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/ve/thread/SummaryThread;->summaryTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementList()Ljava/util/List;

    move-result-object v16

    .line 216
    .local v16, "list":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    if-eqz v16, :cond_1

    invoke-interface/range {v16 .. v16}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_1

    .line 218
    invoke-interface/range {v16 .. v16}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/samsung/app/video/editor/external/Element;

    .line 219
    .local v12, "ele":Lcom/samsung/app/video/editor/external/Element;
    if-eqz v12, :cond_1

    .line 220
    invoke-virtual {v12}, Lcom/samsung/app/video/editor/external/Element;->removetransitionEdit()V

    .line 224
    .end local v12    # "ele":Lcom/samsung/app/video/editor/external/Element;
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/ve/thread/SummaryThread;->summaryTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getThemeName()I

    move-result v5

    invoke-virtual {v3, v5}, Lcom/samsung/app/video/editor/external/TranscodeElement;->setThemeName(I)V

    .line 228
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/ve/thread/SummaryThread;->summaryTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/app/ve/thread/SummaryThread;->keepOriginalTranscodeTransition(Lcom/samsung/app/video/editor/external/TranscodeElement;Lcom/samsung/app/video/editor/external/TranscodeElement;)V

    .line 230
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/ve/thread/SummaryThread;->summaryTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/TranscodeElement;->overlapTransitionsHandling()V

    .line 232
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/ve/thread/SummaryThread;->summaryTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4}, Lcom/sec/android/app/ve/thread/SummaryThread;->updateTimingInfoForGroup(Lcom/samsung/app/video/editor/external/TranscodeElement;Lcom/samsung/app/video/editor/external/TranscodeElement;)V

    .line 235
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/ve/thread/SummaryThread;->summaryTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4}, Lcom/sec/android/app/ve/thread/SummaryThread;->addFadeEffects(Lcom/samsung/app/video/editor/external/TranscodeElement;Lcom/samsung/app/video/editor/external/TranscodeElement;)V

    .line 237
    invoke-virtual/range {v25 .. v25}, Ljava/util/Vector;->size()I

    move-result v2

    .line 238
    .local v2, "count":I
    const/4 v15, 0x0

    .local v15, "k":I
    :goto_2
    if-lt v15, v2, :cond_d

    .line 248
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/ve/thread/SummaryThread;->summaryTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v3}, Lcom/sec/android/app/ve/thread/SummaryThread;->updateCaption(Lcom/samsung/app/video/editor/external/TranscodeElement;Lcom/samsung/app/video/editor/external/TranscodeElement;)V

    .line 249
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/ve/thread/SummaryThread;->summaryTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v3}, Lcom/sec/android/app/ve/thread/SummaryThread;->updateDrawing(Lcom/samsung/app/video/editor/external/TranscodeElement;Lcom/samsung/app/video/editor/external/TranscodeElement;)V

    .line 250
    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getAudioListCount()I

    move-result v3

    if-lez v3, :cond_2

    .line 252
    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getAdditionlAudioEleList()Ljava/util/List;

    move-result-object v3

    const/4 v5, 0x0

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/samsung/app/video/editor/external/Element;

    .line 253
    .local v17, "musicElement":Lcom/samsung/app/video/editor/external/Element;
    new-instance v20, Lcom/samsung/app/video/editor/external/Element;

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/samsung/app/video/editor/external/Element;-><init>(Lcom/samsung/app/video/editor/external/Element;)V

    .line 254
    .local v20, "newMusicElement":Lcom/samsung/app/video/editor/external/Element;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/ve/thread/SummaryThread;->summaryTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getTotalDuration()J

    move-result-wide v6

    move-object/from16 v0, v20

    invoke-virtual {v0, v6, v7}, Lcom/samsung/app/video/editor/external/Element;->setStoryBoardEndTime(J)V

    .line 255
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/ve/thread/SummaryThread;->summaryTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-object/from16 v0, v20

    invoke-virtual {v3, v0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->addMusicEleList(Lcom/samsung/app/video/editor/external/Element;)V

    .line 258
    .end local v17    # "musicElement":Lcom/samsung/app/video/editor/external/Element;
    .end local v20    # "newMusicElement":Lcom/samsung/app/video/editor/external/Element;
    :cond_2
    const/4 v3, 0x1

    goto/16 :goto_0

    .line 154
    .end local v2    # "count":I
    .end local v15    # "k":I
    .end local v16    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    :cond_3
    add-int/lit8 v14, v14, 0x1

    .line 155
    new-instance v3, Lcom/sec/android/app/ve/thread/SummaryThread$SummaryBit;

    invoke-direct {v3, v13}, Lcom/sec/android/app/ve/thread/SummaryThread$SummaryBit;-><init>(Lcom/samsung/app/video/editor/external/Element;)V

    move-object/from16 v0, v25

    invoke-virtual {v0, v3}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 158
    invoke-virtual {v13}, Lcom/samsung/app/video/editor/external/Element;->getEndTime()J

    move-result-wide v6

    invoke-virtual {v13}, Lcom/samsung/app/video/editor/external/Element;->getStartTime()J

    move-result-wide v28

    cmp-long v3, v6, v28

    if-gtz v3, :cond_4

    .line 159
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/ve/thread/SummaryThread;->summaryTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v13}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 163
    :cond_4
    invoke-virtual {v13}, Lcom/samsung/app/video/editor/external/Element;->getEndTime()J

    move-result-wide v6

    invoke-virtual {v13}, Lcom/samsung/app/video/editor/external/Element;->getStartTime()J

    move-result-wide v28

    sub-long v6, v6, v28

    invoke-virtual {v13, v6, v7}, Lcom/samsung/app/video/editor/external/Element;->setDuration(J)V

    .line 164
    const/4 v3, 0x1

    invoke-virtual {v13, v3}, Lcom/samsung/app/video/editor/external/Element;->setAutoEdited(Z)V

    .line 165
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/ve/thread/SummaryThread;->summaryTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    invoke-virtual {v3, v13}, Lcom/samsung/app/video/editor/external/TranscodeElement;->addElement(Lcom/samsung/app/video/editor/external/Element;)V

    .line 167
    invoke-virtual {v13}, Lcom/samsung/app/video/editor/external/Element;->getGroupID()I

    move-result v3

    invoke-virtual {v4, v3}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementWithID(I)Lcom/samsung/app/video/editor/external/Element;

    move-result-object v23

    .line 168
    .local v23, "originalEle":Lcom/samsung/app/video/editor/external/Element;
    if-eqz v23, :cond_c

    .line 170
    const/4 v3, 0x1

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Lcom/samsung/app/video/editor/external/Element;->getEdit(I)Lcom/samsung/app/video/editor/external/Edit;

    move-result-object v9

    .line 171
    .local v9, "curEdit":Lcom/samsung/app/video/editor/external/Edit;
    if-eqz v9, :cond_5

    invoke-virtual/range {v26 .. v26}, Lcom/samsung/app/video/editor/external/Edit;->getVolumeLevel()I

    move-result v3

    const/4 v5, -0x1

    if-ne v3, v5, :cond_5

    .line 172
    new-instance v19, Lcom/samsung/app/video/editor/external/Edit;

    const/4 v3, 0x1

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Lcom/samsung/app/video/editor/external/Element;->getEdit(I)Lcom/samsung/app/video/editor/external/Edit;

    move-result-object v3

    move-object/from16 v0, v19

    invoke-direct {v0, v3}, Lcom/samsung/app/video/editor/external/Edit;-><init>(Lcom/samsung/app/video/editor/external/Edit;)V

    .line 173
    .local v19, "newEdit":Lcom/samsung/app/video/editor/external/Edit;
    move-object/from16 v0, v19

    invoke-virtual {v13, v0}, Lcom/samsung/app/video/editor/external/Element;->addEdit(Lcom/samsung/app/video/editor/external/Edit;)V

    .line 176
    .end local v19    # "newEdit":Lcom/samsung/app/video/editor/external/Edit;
    :cond_5
    invoke-virtual/range {v23 .. v23}, Lcom/samsung/app/video/editor/external/Element;->getEditList()Ljava/util/List;

    move-result-object v22

    .line 177
    .local v22, "originalEditList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Edit;>;"
    if-eqz v22, :cond_6

    invoke-interface/range {v22 .. v22}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_6

    .line 178
    const/16 v18, 0x0

    .local v18, "n":I
    :goto_3
    invoke-interface/range {v22 .. v22}, Ljava/util/List;->size()I

    move-result v3

    move/from16 v0, v18

    if-lt v0, v3, :cond_8

    .line 194
    .end local v18    # "n":I
    :cond_6
    invoke-virtual/range {v23 .. v23}, Lcom/samsung/app/video/editor/external/Element;->getAudioEffectEdit()Lcom/samsung/app/video/editor/external/Edit;

    move-result-object v9

    .line 195
    if-eqz v9, :cond_7

    .line 196
    new-instance v19, Lcom/samsung/app/video/editor/external/Edit;

    move-object/from16 v0, v19

    invoke-direct {v0, v9}, Lcom/samsung/app/video/editor/external/Edit;-><init>(Lcom/samsung/app/video/editor/external/Edit;)V

    .line 197
    .restart local v19    # "newEdit":Lcom/samsung/app/video/editor/external/Edit;
    move-object/from16 v0, v19

    invoke-virtual {v13, v0}, Lcom/samsung/app/video/editor/external/Element;->addEdit(Lcom/samsung/app/video/editor/external/Edit;)V

    .line 205
    .end local v9    # "curEdit":Lcom/samsung/app/video/editor/external/Edit;
    .end local v19    # "newEdit":Lcom/samsung/app/video/editor/external/Edit;
    .end local v22    # "originalEditList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Edit;>;"
    :cond_7
    :goto_4
    new-instance v11, Lcom/samsung/app/video/editor/external/Edit;

    .end local v11    # "edit":Lcom/samsung/app/video/editor/external/Edit;
    invoke-direct {v11}, Lcom/samsung/app/video/editor/external/Edit;-><init>()V

    .line 206
    .restart local v11    # "edit":Lcom/samsung/app/video/editor/external/Edit;
    const/4 v3, 0x6

    invoke-virtual {v11, v3}, Lcom/samsung/app/video/editor/external/Edit;->setType(I)V

    .line 207
    const/4 v3, 0x0

    invoke-virtual {v11, v3}, Lcom/samsung/app/video/editor/external/Edit;->setTrans_duration(I)V

    .line 208
    const/4 v3, 0x0

    invoke-virtual {v11, v3}, Lcom/samsung/app/video/editor/external/Edit;->setSubType(I)V

    .line 209
    invoke-virtual {v13, v11}, Lcom/samsung/app/video/editor/external/Element;->addEdit(Lcom/samsung/app/video/editor/external/Edit;)V

    goto/16 :goto_1

    .line 179
    .restart local v9    # "curEdit":Lcom/samsung/app/video/editor/external/Edit;
    .restart local v18    # "n":I
    .restart local v22    # "originalEditList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Edit;>;"
    :cond_8
    move-object/from16 v0, v22

    move/from16 v1, v18

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/samsung/app/video/editor/external/Edit;

    .line 180
    .local v21, "originalEdit":Lcom/samsung/app/video/editor/external/Edit;
    invoke-virtual/range {v21 .. v21}, Lcom/samsung/app/video/editor/external/Edit;->getType()I

    move-result v3

    const/4 v5, 0x4

    if-ne v3, v5, :cond_a

    .line 181
    invoke-virtual/range {v21 .. v21}, Lcom/samsung/app/video/editor/external/Edit;->getSubType()I

    move-result v3

    const/16 v5, 0x27

    if-ne v3, v5, :cond_b

    .line 182
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-direct {v0, v13, v1}, Lcom/sec/android/app/ve/thread/SummaryThread;->copyKenburns(Lcom/samsung/app/video/editor/external/Element;Lcom/samsung/app/video/editor/external/Element;)V

    .line 188
    :cond_9
    new-instance v19, Lcom/samsung/app/video/editor/external/Edit;

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/samsung/app/video/editor/external/Edit;-><init>(Lcom/samsung/app/video/editor/external/Edit;)V

    .line 189
    .restart local v19    # "newEdit":Lcom/samsung/app/video/editor/external/Edit;
    const/4 v3, 0x0

    move-object/from16 v0, v19

    invoke-virtual {v13, v0, v3}, Lcom/samsung/app/video/editor/external/Element;->addEdit(Lcom/samsung/app/video/editor/external/Edit;Z)V

    .line 178
    .end local v19    # "newEdit":Lcom/samsung/app/video/editor/external/Edit;
    :cond_a
    :goto_5
    add-int/lit8 v18, v18, 0x1

    goto :goto_3

    .line 184
    :cond_b
    invoke-virtual/range {v21 .. v21}, Lcom/samsung/app/video/editor/external/Edit;->getSubType()I

    move-result v3

    const/16 v5, 0x33

    if-eq v3, v5, :cond_a

    .line 185
    invoke-virtual/range {v21 .. v21}, Lcom/samsung/app/video/editor/external/Edit;->getSubType()I

    move-result v3

    const/16 v5, 0x35

    if-ne v3, v5, :cond_9

    goto :goto_5

    .line 201
    .end local v9    # "curEdit":Lcom/samsung/app/video/editor/external/Edit;
    .end local v18    # "n":I
    .end local v21    # "originalEdit":Lcom/samsung/app/video/editor/external/Edit;
    .end local v22    # "originalEditList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Edit;>;"
    :cond_c
    new-instance v11, Lcom/samsung/app/video/editor/external/Edit;

    .end local v11    # "edit":Lcom/samsung/app/video/editor/external/Edit;
    invoke-direct {v11}, Lcom/samsung/app/video/editor/external/Edit;-><init>()V

    .line 202
    .restart local v11    # "edit":Lcom/samsung/app/video/editor/external/Edit;
    const/4 v3, 0x1

    invoke-virtual {v11, v3}, Lcom/samsung/app/video/editor/external/Edit;->setType(I)V

    .line 203
    invoke-virtual {v13, v11}, Lcom/samsung/app/video/editor/external/Element;->addEdit(Lcom/samsung/app/video/editor/external/Edit;)V

    goto :goto_4

    .line 240
    .end local v23    # "originalEle":Lcom/samsung/app/video/editor/external/Element;
    .restart local v2    # "count":I
    .restart local v15    # "k":I
    .restart local v16    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    :cond_d
    move-object/from16 v0, v25

    invoke-virtual {v0, v15}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/android/app/ve/thread/SummaryThread$SummaryBit;

    .line 241
    .local v10, "currBit":Lcom/sec/android/app/ve/thread/SummaryThread$SummaryBit;
    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v4}, Lcom/sec/android/app/ve/thread/SummaryThread;->computeOccurance(Lcom/sec/android/app/ve/thread/SummaryThread$SummaryBit;Lcom/samsung/app/video/editor/external/TranscodeElement;)V

    .line 242
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/ve/thread/SummaryThread;->summaryTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v3, v10}, Lcom/sec/android/app/ve/thread/SummaryThread;->addRecordingListToSummary(Lcom/samsung/app/video/editor/external/TranscodeElement;Lcom/samsung/app/video/editor/external/TranscodeElement;Lcom/sec/android/app/ve/thread/SummaryThread$SummaryBit;)V

    .line 245
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/ve/thread/SummaryThread;->summaryTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-direct {v0, v3, v1, v10}, Lcom/sec/android/app/ve/thread/SummaryThread;->addSoundEffectList(Lcom/samsung/app/video/editor/external/TranscodeElement;Ljava/util/Vector;Lcom/sec/android/app/ve/thread/SummaryThread$SummaryBit;)V

    .line 238
    add-int/lit8 v15, v15, 0x1

    goto/16 :goto_2
.end method

.method private updateCaption(Lcom/samsung/app/video/editor/external/TranscodeElement;Lcom/samsung/app/video/editor/external/TranscodeElement;)V
    .locals 18
    .param p1, "oldTranscodeElement"    # Lcom/samsung/app/video/editor/external/TranscodeElement;
    .param p2, "newTranscodeElement"    # Lcom/samsung/app/video/editor/external/TranscodeElement;

    .prologue
    .line 317
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getTextEleListCount()I

    move-result v13

    if-eqz v13, :cond_1

    .line 318
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementList()Ljava/util/List;

    move-result-object v8

    .line 319
    .local v8, "oldEleList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    const/4 v5, 0x0

    .line 320
    .local v5, "groupID":I
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :cond_0
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-nez v14, :cond_2

    .line 349
    .end local v5    # "groupID":I
    .end local v8    # "oldEleList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    :cond_1
    return-void

    .line 320
    .restart local v5    # "groupID":I
    .restart local v8    # "oldEleList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    :cond_2
    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/app/video/editor/external/Element;

    .line 321
    .local v7, "oldEle":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {v7}, Lcom/samsung/app/video/editor/external/Element;->getGroupID()I

    move-result v5

    .line 322
    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getFirstElementInGroup(I)Lcom/samsung/app/video/editor/external/Element;

    move-result-object v4

    .line 323
    .local v4, "groupFirstEle":Lcom/samsung/app/video/editor/external/Element;
    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getTextClipartParamsListForElement(Lcom/samsung/app/video/editor/external/Element;)Ljava/util/List;

    move-result-object v12

    .line 324
    .local v12, "textListForEle":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    if-eqz v12, :cond_0

    .line 326
    invoke-interface {v12}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :cond_3
    :goto_0
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_0

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/app/video/editor/external/ClipartParams;

    .line 327
    .local v9, "textClipart":Lcom/samsung/app/video/editor/external/ClipartParams;
    new-instance v6, Lcom/samsung/app/video/editor/external/ClipartParams;

    invoke-direct {v6, v9}, Lcom/samsung/app/video/editor/external/ClipartParams;-><init>(Lcom/samsung/app/video/editor/external/ClipartParams;)V

    .line 330
    .local v6, "newTextClipart":Lcom/samsung/app/video/editor/external/ClipartParams;
    invoke-virtual {v6, v5}, Lcom/samsung/app/video/editor/external/ClipartParams;->setElementID(I)V

    .line 331
    if-eqz v4, :cond_3

    .line 332
    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getStoryTimeTillElement(Lcom/samsung/app/video/editor/external/Element;)J

    move-result-wide v10

    .line 333
    .local v10, "startTime":J
    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/Element;->getGroupDuration()J

    move-result-wide v16

    add-long v2, v10, v16

    .line 335
    .local v2, "endTime":J
    long-to-float v15, v10

    const/high16 v16, 0x447a0000    # 1000.0f

    div-float v15, v15, v16

    const/high16 v16, 0x41f00000    # 30.0f

    mul-float v15, v15, v16

    invoke-static {v15}, Ljava/lang/Math;->round(F)I

    move-result v15

    invoke-virtual {v6, v15}, Lcom/samsung/app/video/editor/external/ClipartParams;->setStoryboardStartFrame(I)V

    .line 338
    long-to-float v15, v2

    const/high16 v16, 0x447a0000    # 1000.0f

    div-float v15, v15, v16

    const/high16 v16, 0x41f00000    # 30.0f

    mul-float v15, v15, v16

    invoke-static {v15}, Ljava/lang/Math;->round(F)I

    move-result v15

    invoke-virtual {v6, v15}, Lcom/samsung/app/video/editor/external/ClipartParams;->setStoryboardEndFrame(I)V

    .line 341
    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Lcom/samsung/app/video/editor/external/TranscodeElement;->addTextEleList(Lcom/samsung/app/video/editor/external/ClipartParams;)V

    goto :goto_0
.end method

.method private updateDrawing(Lcom/samsung/app/video/editor/external/TranscodeElement;Lcom/samsung/app/video/editor/external/TranscodeElement;)V
    .locals 18
    .param p1, "oldTranscodeElement"    # Lcom/samsung/app/video/editor/external/TranscodeElement;
    .param p2, "newTranscodeElement"    # Lcom/samsung/app/video/editor/external/TranscodeElement;

    .prologue
    .line 351
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getDrawingEleListCount()I

    move-result v11

    if-eqz v11, :cond_1

    .line 352
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementList()Ljava/util/List;

    move-result-object v10

    .line 353
    .local v10, "oldEleList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    const/4 v7, 0x0

    .line 354
    .local v7, "groupID":I
    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-nez v14, :cond_2

    .line 376
    .end local v7    # "groupID":I
    .end local v10    # "oldEleList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    :cond_1
    return-void

    .line 354
    .restart local v7    # "groupID":I
    .restart local v10    # "oldEleList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    :cond_2
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/app/video/editor/external/Element;

    .line 355
    .local v9, "oldEle":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {v9}, Lcom/samsung/app/video/editor/external/Element;->getGroupID()I

    move-result v7

    .line 356
    move-object/from16 v0, p2

    invoke-virtual {v0, v7}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getFirstElementInGroup(I)Lcom/samsung/app/video/editor/external/Element;

    move-result-object v6

    .line 357
    .local v6, "groupFirstEle":Lcom/samsung/app/video/editor/external/Element;
    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getDrawClipartParamsListForElement(Lcom/samsung/app/video/editor/external/Element;)Ljava/util/List;

    move-result-object v3

    .line 358
    .local v3, "drawListForEle":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    if-eqz v3, :cond_0

    if-eqz v6, :cond_0

    .line 360
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :goto_0
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_0

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/app/video/editor/external/ClipartParams;

    .line 361
    .local v2, "drawClipart":Lcom/samsung/app/video/editor/external/ClipartParams;
    new-instance v8, Lcom/samsung/app/video/editor/external/ClipartParams;

    invoke-direct {v8, v2}, Lcom/samsung/app/video/editor/external/ClipartParams;-><init>(Lcom/samsung/app/video/editor/external/ClipartParams;)V

    .line 363
    .local v8, "newDrawClipart":Lcom/samsung/app/video/editor/external/ClipartParams;
    invoke-virtual {v8, v7}, Lcom/samsung/app/video/editor/external/ClipartParams;->setElementID(I)V

    .line 364
    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getStoryTimeTillElement(Lcom/samsung/app/video/editor/external/Element;)J

    move-result-wide v12

    .line 365
    .local v12, "startTime":J
    invoke-virtual {v6}, Lcom/samsung/app/video/editor/external/Element;->getGroupDuration()J

    move-result-wide v16

    add-long v4, v12, v16

    .line 367
    .local v4, "endTime":J
    long-to-float v15, v12

    const/high16 v16, 0x447a0000    # 1000.0f

    div-float v15, v15, v16

    const/high16 v16, 0x41f00000    # 30.0f

    mul-float v15, v15, v16

    invoke-static {v15}, Ljava/lang/Math;->round(F)I

    move-result v15

    invoke-virtual {v8, v15}, Lcom/samsung/app/video/editor/external/ClipartParams;->setStoryboardStartFrame(I)V

    .line 369
    long-to-float v15, v4

    const/high16 v16, 0x447a0000    # 1000.0f

    div-float v15, v15, v16

    const/high16 v16, 0x41f00000    # 30.0f

    mul-float v15, v15, v16

    invoke-static {v15}, Ljava/lang/Math;->round(F)I

    move-result v15

    invoke-virtual {v8, v15}, Lcom/samsung/app/video/editor/external/ClipartParams;->setStoryboardEndFrame(I)V

    .line 371
    move-object/from16 v0, p2

    invoke-virtual {v0, v8}, Lcom/samsung/app/video/editor/external/TranscodeElement;->addDrawingEleList(Lcom/samsung/app/video/editor/external/ClipartParams;)V

    goto :goto_0
.end method

.method private updateTimingInfoForGroup(Lcom/samsung/app/video/editor/external/TranscodeElement;Lcom/samsung/app/video/editor/external/TranscodeElement;)V
    .locals 20
    .param p1, "summaryProject"    # Lcom/samsung/app/video/editor/external/TranscodeElement;
    .param p2, "origProject"    # Lcom/samsung/app/video/editor/external/TranscodeElement;

    .prologue
    .line 378
    const/4 v4, 0x0

    .line 379
    .local v4, "element":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementList()Ljava/util/List;

    move-result-object v11

    .line 380
    .local v11, "list":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getMaxGroupID()I

    move-result v2

    .line 381
    .local v2, "count":I
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_0
    if-le v9, v2, :cond_0

    .line 407
    return-void

    .line 382
    :cond_0
    new-instance v7, Ljava/util/Vector;

    invoke-direct {v7}, Ljava/util/Vector;-><init>()V

    .line 383
    .local v7, "groupList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v12

    .line 384
    .local v12, "listSize":I
    const/4 v10, 0x0

    .local v10, "j":I
    :goto_1
    if-lt v10, v12, :cond_1

    .line 390
    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getSummaryGroupTotalTime(Ljava/util/List;)J

    move-result-wide v14

    .line 391
    .local v14, "totalDuration":J
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v8

    .line 392
    .local v8, "groupSize":I
    const/4 v10, 0x0

    :goto_2
    if-lt v10, v8, :cond_3

    .line 381
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 385
    .end local v8    # "groupSize":I
    .end local v14    # "totalDuration":J
    :cond_1
    invoke-interface {v11, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "element":Lcom/samsung/app/video/editor/external/Element;
    check-cast v4, Lcom/samsung/app/video/editor/external/Element;

    .line 386
    .restart local v4    # "element":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/Element;->getGroupID()I

    move-result v16

    move/from16 v0, v16

    if-ne v9, v0, :cond_2

    .line 387
    invoke-interface {v7, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 384
    :cond_2
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    .line 393
    .restart local v8    # "groupSize":I
    .restart local v14    # "totalDuration":J
    :cond_3
    invoke-interface {v7, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "element":Lcom/samsung/app/video/editor/external/Element;
    check-cast v4, Lcom/samsung/app/video/editor/external/Element;

    .line 394
    .restart local v4    # "element":Lcom/samsung/app/video/editor/external/Element;
    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-interface {v7, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/app/video/editor/external/Element;

    .line 395
    .local v5, "groupFirstEle":Lcom/samsung/app/video/editor/external/Element;
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v16

    add-int/lit8 v16, v16, -0x1

    move/from16 v0, v16

    invoke-interface {v7, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/app/video/editor/external/Element;

    .line 396
    .local v6, "groupLastEle":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {v5}, Lcom/samsung/app/video/editor/external/Element;->getStartTime()J

    move-result-wide v16

    move-wide/from16 v0, v16

    invoke-virtual {v4, v0, v1}, Lcom/samsung/app/video/editor/external/Element;->setGroupStartTime(J)V

    .line 397
    invoke-virtual {v4, v14, v15}, Lcom/samsung/app/video/editor/external/Element;->setGroupDuration(J)V

    .line 398
    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/Element;->getGroupID()I

    move-result v16

    move-object/from16 v0, p2

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementWithID(I)Lcom/samsung/app/video/editor/external/Element;

    move-result-object v3

    .line 399
    .local v3, "elem":Lcom/samsung/app/video/editor/external/Element;
    if-eqz v3, :cond_4

    .line 400
    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/Element;->getSplitTime()J

    move-result-wide v16

    move-wide/from16 v0, v16

    invoke-virtual {v4, v0, v1}, Lcom/samsung/app/video/editor/external/Element;->setSplitTime(J)V

    .line 401
    :cond_4
    invoke-virtual {v6}, Lcom/samsung/app/video/editor/external/Element;->getEndTime()J

    move-result-wide v16

    move-wide/from16 v0, v16

    invoke-virtual {v4, v0, v1}, Lcom/samsung/app/video/editor/external/Element;->setGroupEndTime(J)V

    .line 402
    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/Element;->getEndTime()J

    move-result-wide v16

    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/Element;->getStartTime()J

    move-result-wide v18

    sub-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-float v0, v0

    move/from16 v16, v0

    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/Element;->getGroupDuration()J

    move-result-wide v18

    move-wide/from16 v0, v18

    long-to-float v0, v0

    move/from16 v17, v0

    div-float v13, v16, v17

    .line 403
    .local v13, "ratio":F
    invoke-virtual {v4, v13}, Lcom/samsung/app/video/editor/external/Element;->setElementRatioInGroup(F)V

    .line 392
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_2
.end method


# virtual methods
.method public computeOccurance(Lcom/sec/android/app/ve/thread/SummaryThread$SummaryBit;Lcom/samsung/app/video/editor/external/TranscodeElement;)V
    .locals 10
    .param p1, "summary"    # Lcom/sec/android/app/ve/thread/SummaryThread$SummaryBit;
    .param p2, "oldTransElement"    # Lcom/samsung/app/video/editor/external/TranscodeElement;

    .prologue
    .line 486
    invoke-virtual {p2}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementList()Ljava/util/List;

    move-result-object v4

    .line 487
    .local v4, "list":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    if-eqz v4, :cond_0

    .line 488
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    .line 489
    .local v5, "listSize":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v5, :cond_1

    .line 498
    .end local v1    # "i":I
    .end local v5    # "listSize":I
    :cond_0
    return-void

    .line 490
    .restart local v1    # "i":I
    .restart local v5    # "listSize":I
    :cond_1
    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/app/video/editor/external/Element;

    .line 491
    .local v0, "element":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {v0}, Lcom/samsung/app/video/editor/external/Element;->getGroupID()I

    move-result v6

    iget-object v7, p1, Lcom/sec/android/app/ve/thread/SummaryThread$SummaryBit;->summaryElement:Lcom/samsung/app/video/editor/external/Element;

    invoke-virtual {v7}, Lcom/samsung/app/video/editor/external/Element;->getGroupID()I

    move-result v7

    if-ne v6, v7, :cond_2

    .line 492
    invoke-virtual {p2, v0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getStoryTimeTillElement(Lcom/samsung/app/video/editor/external/Element;)J

    move-result-wide v2

    .line 493
    .local v2, "elementStartTime":J
    iget-wide v6, p1, Lcom/sec/android/app/ve/thread/SummaryThread$SummaryBit;->startTime:J

    add-long/2addr v6, v2

    invoke-virtual {v0}, Lcom/samsung/app/video/editor/external/Element;->getStartTime()J

    move-result-wide v8

    sub-long/2addr v6, v8

    iput-wide v6, p1, Lcom/sec/android/app/ve/thread/SummaryThread$SummaryBit;->actualOccurance:J

    .line 489
    .end local v2    # "elementStartTime":J
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public getFinalPath()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/sec/android/app/ve/thread/SummaryThread;->path:[Ljava/lang/String;

    return-object v0
.end method

.method public keepOriginalTranscodeTransition(Lcom/samsung/app/video/editor/external/TranscodeElement;Lcom/samsung/app/video/editor/external/TranscodeElement;)V
    .locals 10
    .param p1, "newTranscode"    # Lcom/samsung/app/video/editor/external/TranscodeElement;
    .param p2, "oldTranscode"    # Lcom/samsung/app/video/editor/external/TranscodeElement;

    .prologue
    .line 411
    const/4 v3, 0x0

    .line 413
    .local v3, "newElement":Lcom/samsung/app/video/editor/external/Element;
    const/4 v2, 0x0

    .line 414
    .local v2, "newEdit":Lcom/samsung/app/video/editor/external/Edit;
    invoke-virtual {p2}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v8

    add-int/lit8 v0, v8, -0x1

    .line 415
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v0, :cond_0

    .line 430
    return-void

    .line 416
    :cond_0
    invoke-virtual {p2}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementList()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/app/video/editor/external/Element;

    .line 417
    .local v7, "oldElement":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {p2}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementList()Ljava/util/List;

    move-result-object v8

    add-int/lit8 v9, v1, 0x1

    invoke-interface {v8, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/app/video/editor/external/Element;

    .line 418
    .local v5, "nextToOldElement":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {v7}, Lcom/samsung/app/video/editor/external/Element;->getGroupID()I

    move-result v8

    invoke-virtual {p1, v8}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getLastElementInGroup(I)Lcom/samsung/app/video/editor/external/Element;

    move-result-object v3

    .line 419
    invoke-virtual {v5}, Lcom/samsung/app/video/editor/external/Element;->getGroupID()I

    move-result v8

    invoke-virtual {p1, v8}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getFirstElementInGroup(I)Lcom/samsung/app/video/editor/external/Element;

    move-result-object v4

    .line 420
    .local v4, "nextToNewElement":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {v7}, Lcom/samsung/app/video/editor/external/Element;->hasNonTimeLossTransitionAtEnd()Z

    move-result v8

    invoke-virtual {v3, v8}, Lcom/samsung/app/video/editor/external/Element;->setNonTimeLossTransitionAtEnd(Z)V

    .line 421
    invoke-virtual {v5}, Lcom/samsung/app/video/editor/external/Element;->hasNonTimeLossTransitionAtStart()Z

    move-result v8

    invoke-virtual {v4, v8}, Lcom/samsung/app/video/editor/external/Element;->setNonTimeLossTransitionAtStart(Z)V

    .line 423
    invoke-virtual {v7}, Lcom/samsung/app/video/editor/external/Element;->getTransitionEdit()Lcom/samsung/app/video/editor/external/Edit;

    move-result-object v6

    .line 424
    .local v6, "oldEdit":Lcom/samsung/app/video/editor/external/Edit;
    if-eqz v3, :cond_1

    if-eqz v6, :cond_1

    .line 426
    new-instance v2, Lcom/samsung/app/video/editor/external/Edit;

    .end local v2    # "newEdit":Lcom/samsung/app/video/editor/external/Edit;
    invoke-direct {v2, v6}, Lcom/samsung/app/video/editor/external/Edit;-><init>(Lcom/samsung/app/video/editor/external/Edit;)V

    .line 427
    .restart local v2    # "newEdit":Lcom/samsung/app/video/editor/external/Edit;
    invoke-virtual {v3, v2}, Lcom/samsung/app/video/editor/external/Element;->addEdit(Lcom/samsung/app/video/editor/external/Edit;)V

    .line 415
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public releaseWakelock()V
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/sec/android/app/ve/thread/SummaryThread;->wakelock:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/ve/thread/SummaryThread;->wakelock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 116
    iget-object v0, p0, Lcom/sec/android/app/ve/thread/SummaryThread;->wakelock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 117
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/ve/thread/SummaryThread;->wakelock:Landroid/os/PowerManager$WakeLock;

    .line 119
    :cond_0
    return-void
.end method

.method public run()V
    .locals 12

    .prologue
    const-wide/16 v10, 0xa

    const/4 v8, 0x0

    const/16 v6, 0x79

    .line 59
    const-string v3, "Inside SummaryThread run method"

    invoke-static {v3}, Lcom/sec/android/app/ve/common/LogUtils;->log(Ljava/lang/String;)V

    .line 61
    iget v3, p0, Lcom/sec/android/app/ve/thread/SummaryThread;->mOperation:I

    const/16 v4, 0x3eb

    if-ne v3, v4, :cond_1

    .line 62
    iget-object v3, p0, Lcom/sec/android/app/ve/thread/SummaryThread;->mContext:Landroid/content/Context;

    if-eqz v3, :cond_1

    .line 64
    iget-object v3, p0, Lcom/sec/android/app/ve/thread/SummaryThread;->mHandler:Landroid/os/Handler;

    invoke-virtual {v3, v6}, Landroid/os/Handler;->removeMessages(I)V

    .line 65
    iget-object v3, p0, Lcom/sec/android/app/ve/thread/SummaryThread;->mContext:Landroid/content/Context;

    .line 66
    const-string v4, "power"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/PowerManager;

    .line 67
    const/16 v4, 0xa

    const-string v5, "VE Export Thread"

    .line 66
    invoke-virtual {v3, v4, v5}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v3

    .line 65
    iput-object v3, p0, Lcom/sec/android/app/ve/thread/SummaryThread;->wakelock:Landroid/os/PowerManager$WakeLock;

    .line 68
    iget-object v3, p0, Lcom/sec/android/app/ve/thread/SummaryThread;->wakelock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v3}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 69
    const/4 v2, 0x1

    .line 70
    .local v2, "result":I
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/ve/thread/SummaryThread;->mCurrentTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    sget v5, Lcom/samsung/app/video/editor/external/Constants;->AUTOEDIT_MODE_AUTO:I

    invoke-virtual {v3, v4, v5, v8}, Lcom/samsung/app/video/editor/external/NativeInterface;->createSummaryInstance(Lcom/samsung/app/video/editor/external/TranscodeElement;II)I

    move-result v2

    .line 72
    const/4 v0, 0x0

    .line 74
    .local v0, "isSummarySuccess":Z
    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 75
    invoke-direct {p0}, Lcom/sec/android/app/ve/thread/SummaryThread;->createNoneThemeSummaryProject()Z

    move-result v0

    .line 77
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/ve/thread/SummaryThread;->wakelock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v3}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v3

    if-eqz v3, :cond_2

    if-nez v0, :cond_2

    .line 78
    iget-object v3, p0, Lcom/sec/android/app/ve/thread/SummaryThread;->wakelock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v3}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 85
    :goto_0
    if-nez v0, :cond_4

    .line 86
    iget-boolean v3, p0, Lcom/sec/android/app/ve/thread/SummaryThread;->mForceStop:Z

    if-eqz v3, :cond_3

    const/16 v1, 0x7c

    .line 87
    .local v1, "msg":I
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/ve/thread/SummaryThread;->mHandler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/sec/android/app/ve/thread/SummaryThread;->mHandler:Landroid/os/Handler;

    invoke-static {v4, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {v3, v4, v10, v11}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 88
    iput-boolean v8, p0, Lcom/sec/android/app/ve/thread/SummaryThread;->mForceStop:Z

    .line 99
    .end local v1    # "msg":I
    :goto_2
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/NativeInterface;->clearSummaryInstance()V

    .line 103
    .end local v0    # "isSummarySuccess":Z
    .end local v2    # "result":I
    :cond_1
    return-void

    .line 80
    .restart local v0    # "isSummarySuccess":Z
    .restart local v2    # "result":I
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/ve/thread/SummaryThread;->mHandler:Landroid/os/Handler;

    invoke-virtual {v3, v6}, Landroid/os/Handler;->removeMessages(I)V

    .line 81
    iget-object v3, p0, Lcom/sec/android/app/ve/thread/SummaryThread;->mHandler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/sec/android/app/ve/thread/SummaryThread;->mHandler:Landroid/os/Handler;

    invoke-static {v4, v6}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v4

    .line 82
    const-wide/16 v6, 0xbb8

    .line 81
    invoke-virtual {v3, v4, v6, v7}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    .line 86
    :cond_3
    const/16 v1, 0x7a

    goto :goto_1

    .line 91
    :cond_4
    iget-object v3, p0, Lcom/sec/android/app/ve/thread/SummaryThread;->mContext:Landroid/content/Context;

    if-nez v3, :cond_5

    .line 92
    const-string v3, "context is null"

    invoke-static {v3}, Lcom/sec/android/app/ve/common/LogUtils;->log(Ljava/lang/String;)V

    .line 95
    :cond_5
    iget-object v3, p0, Lcom/sec/android/app/ve/thread/SummaryThread;->mHandler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/sec/android/app/ve/thread/SummaryThread;->mHandler:Landroid/os/Handler;

    .line 96
    const/16 v5, 0x7b

    .line 97
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    .line 95
    invoke-static {v4, v5, v6}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {v3, v4, v10, v11}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_2
.end method

.method public stopSummary()V
    .locals 1

    .prologue
    .line 106
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/ve/thread/SummaryThread;->mForceStop:Z

    .line 107
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/app/video/editor/external/NativeInterface;->stopEngineSummary()V

    .line 108
    return-void
.end method
