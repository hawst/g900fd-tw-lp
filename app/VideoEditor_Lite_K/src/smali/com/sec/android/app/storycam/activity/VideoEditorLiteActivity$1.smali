.class Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$1;
.super Ljava/lang/Object;
.source "VideoEditorLiteActivity.java"

# interfaces
.implements Lcom/sec/android/app/storycam/VEMediaSession$VEMediaSessionCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$1;->this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;

    .line 174
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPause()V
    .locals 3

    .prologue
    .line 198
    iget-object v1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$1;->this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;

    # getter for: Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mRootView:Landroid/widget/RelativeLayout;
    invoke-static {v1}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->access$0(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;)Landroid/widget/RelativeLayout;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 199
    iget-object v1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$1;->this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;

    # getter for: Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mRootView:Landroid/widget/RelativeLayout;
    invoke-static {v1}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->access$0(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;)Landroid/widget/RelativeLayout;

    move-result-object v1

    const v2, 0x7f0d0003

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    .line 200
    .local v0, "pvg":Lcom/sec/android/app/storycam/view/PreviewViewGroup;
    if-eqz v0, :cond_0

    .line 201
    invoke-virtual {v0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->pause_player()V

    .line 204
    .end local v0    # "pvg":Lcom/sec/android/app/storycam/view/PreviewViewGroup;
    :cond_0
    return-void
.end method

.method public onPlay()V
    .locals 3

    .prologue
    .line 188
    iget-object v1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$1;->this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;

    # getter for: Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mRootView:Landroid/widget/RelativeLayout;
    invoke-static {v1}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->access$0(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;)Landroid/widget/RelativeLayout;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 189
    iget-object v1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$1;->this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;

    # getter for: Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mRootView:Landroid/widget/RelativeLayout;
    invoke-static {v1}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->access$0(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;)Landroid/widget/RelativeLayout;

    move-result-object v1

    const v2, 0x7f0d0003

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    .line 190
    .local v0, "pvg":Lcom/sec/android/app/storycam/view/PreviewViewGroup;
    if-eqz v0, :cond_0

    .line 191
    invoke-virtual {v0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->start_play()I

    .line 194
    .end local v0    # "pvg":Lcom/sec/android/app/storycam/view/PreviewViewGroup;
    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 3

    .prologue
    .line 178
    iget-object v1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$1;->this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;

    # getter for: Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mRootView:Landroid/widget/RelativeLayout;
    invoke-static {v1}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->access$0(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;)Landroid/widget/RelativeLayout;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 179
    iget-object v1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$1;->this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;

    # getter for: Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mRootView:Landroid/widget/RelativeLayout;
    invoke-static {v1}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->access$0(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;)Landroid/widget/RelativeLayout;

    move-result-object v1

    const v2, 0x7f0d0003

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    .line 180
    .local v0, "pvg":Lcom/sec/android/app/storycam/view/PreviewViewGroup;
    if-eqz v0, :cond_0

    .line 181
    invoke-virtual {v0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->pause_player()V

    .line 184
    .end local v0    # "pvg":Lcom/sec/android/app/storycam/view/PreviewViewGroup;
    :cond_0
    return-void
.end method
