.class public Lcom/sec/android/app/storycam/view/ThemeSelectionGridView$ThemeSelectionAdapter;
.super Landroid/widget/BaseAdapter;
.source "ThemeSelectionGridView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ThemeSelectionAdapter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;)V
    .locals 0

    .prologue
    .line 190
    iput-object p1, p0, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView$ThemeSelectionAdapter;->this$0:Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/app/storycam/view/ThemeSelectionGridView$ThemeSelectionAdapter;)Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView$ThemeSelectionAdapter;->this$0:Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;

    return-object v0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 194
    sget-object v0, Lcom/sec/android/app/ve/VEApp;->gThemeMgr:Lcom/sec/android/app/ve/theme/ThemeManager;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/theme/ThemeManager;->getThemeCount()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "arg0"    # I

    .prologue
    .line 199
    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "arg0"    # I

    .prologue
    .line 204
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 16
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 209
    const/4 v4, 0x0

    .line 211
    .local v4, "layout":Landroid/widget/RelativeLayout;
    if-nez p2, :cond_2

    .line 212
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView$ThemeSelectionAdapter;->this$0:Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;

    invoke-virtual {v14}, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;->getContext()Landroid/content/Context;

    move-result-object v14

    invoke-static {v14}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    .line 213
    .local v3, "inflater":Landroid/view/LayoutInflater;
    const v14, 0x7f030015

    const/4 v15, 0x0

    invoke-virtual {v3, v14, v15}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .end local v4    # "layout":Landroid/widget/RelativeLayout;
    check-cast v4, Landroid/widget/RelativeLayout;

    .line 220
    .end local v3    # "inflater":Landroid/view/LayoutInflater;
    .restart local v4    # "layout":Landroid/widget/RelativeLayout;
    :goto_0
    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/storycam/Utils;->mapThemeIndexToID(I)I

    move-result v7

    .line 222
    .local v7, "themeId":I
    const v14, 0x7f0d0050

    invoke-virtual {v4, v14}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/ImageView;

    .line 223
    .local v10, "themeThumbnail":Landroid/widget/ImageView;
    const v14, 0x7f0d0051

    invoke-virtual {v4, v14}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/ImageView;

    .line 224
    .local v11, "themeThumbnailBorder":Landroid/widget/ImageView;
    const v14, 0x7f0d0052

    invoke-virtual {v4, v14}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/ImageView;

    .line 225
    .local v12, "themeThumbnailSelected":Landroid/widget/ImageView;
    const v14, 0x7f0d0053

    invoke-virtual {v4, v14}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    .line 226
    .local v9, "themeName":Landroid/widget/TextView;
    const v14, 0x7f0d004f

    invoke-virtual {v4, v14}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 227
    .local v1, "backgroundImage":Landroid/widget/ImageView;
    const v14, 0x7f0d0054

    invoke-virtual {v4, v14}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    .line 228
    .local v6, "themeCoverUHD":Landroid/widget/ImageView;
    const v14, 0x7f0d0055

    invoke-virtual {v4, v14}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    .line 229
    .local v5, "themeBackgroundEndL":Landroid/widget/ImageView;
    sget-object v14, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v14}, Lcom/sec/android/app/storycam/AppDataManager;->getCurrentTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v13

    .line 231
    .local v13, "transcodeElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView$ThemeSelectionAdapter;->this$0:Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;

    # getter for: Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;->clickListener:Landroid/view/View$OnClickListener;
    invoke-static {v14}, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;->access$5(Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;)Landroid/view/View$OnClickListener;

    move-result-object v14

    invoke-virtual {v11, v14}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 232
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v11, v14}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 233
    new-instance v14, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView$ThemeSelectionAdapter$1;

    move-object/from16 v0, p0

    invoke-direct {v14, v0}, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView$ThemeSelectionAdapter$1;-><init>(Lcom/sec/android/app/storycam/view/ThemeSelectionGridView$ThemeSelectionAdapter;)V

    invoke-virtual {v11, v14}, Landroid/widget/ImageView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 243
    invoke-virtual {v4}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v8

    check-cast v8, Landroid/widget/AbsListView$LayoutParams;

    .line 244
    .local v8, "themeItemLayoutParams":Landroid/widget/AbsListView$LayoutParams;
    if-nez v8, :cond_0

    .line 245
    new-instance v8, Landroid/widget/AbsListView$LayoutParams;

    .end local v8    # "themeItemLayoutParams":Landroid/widget/AbsListView$LayoutParams;
    const/4 v14, -0x2

    const/4 v15, -0x2

    invoke-direct {v8, v14, v15}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    .line 248
    .restart local v8    # "themeItemLayoutParams":Landroid/widget/AbsListView$LayoutParams;
    :cond_0
    invoke-virtual {v1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 249
    .local v2, "bgdIamgeLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    if-nez v2, :cond_1

    .line 250
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    .end local v2    # "bgdIamgeLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v14, -0x2

    const/4 v15, -0x2

    invoke-direct {v2, v14, v15}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 253
    .restart local v2    # "bgdIamgeLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView$ThemeSelectionAdapter;->this$0:Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;

    invoke-virtual {v14}, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    invoke-virtual {v14}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v14

    iget v14, v14, Landroid/content/res/Configuration;->orientation:I

    const/4 v15, 0x1

    if-ne v14, v15, :cond_4

    .line 254
    sget-object v14, Lcom/sec/android/app/ve/VEApp;->gThemeMgr:Lcom/sec/android/app/ve/theme/ThemeManager;

    invoke-virtual {v14}, Lcom/sec/android/app/ve/theme/ThemeManager;->getThemeCount()I

    move-result v14

    add-int/lit8 v14, v14, -0x1

    move/from16 v0, p1

    if-ne v0, v14, :cond_3

    .line 256
    const/4 v14, 0x0

    invoke-virtual {v5, v14}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 257
    const v14, 0x7f0603d7

    invoke-static {v14}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v14

    iput v14, v8, Landroid/widget/AbsListView$LayoutParams;->width:I

    .line 258
    const v14, 0x7f0603d7

    invoke-static {v14}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v14

    iput v14, v2, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 266
    :goto_1
    const/4 v14, 0x1

    invoke-virtual {v11, v14}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 267
    const/4 v14, 0x1

    invoke-virtual {v11, v14}, Landroid/widget/ImageView;->setClickable(Z)V

    .line 282
    :goto_2
    invoke-virtual {v4, v8}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 283
    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 285
    sget-object v14, Lcom/sec/android/app/ve/VEApp;->gThemeMgr:Lcom/sec/android/app/ve/theme/ThemeManager;

    const/4 v15, 0x1

    invoke-virtual {v14, v7, v15}, Lcom/sec/android/app/ve/theme/ThemeManager;->getThemeDrawable(IZ)Landroid/graphics/drawable/Drawable;

    move-result-object v14

    invoke-virtual {v10, v14}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 287
    sget-object v14, Lcom/sec/android/app/ve/VEApp;->gThemeMgr:Lcom/sec/android/app/ve/theme/ThemeManager;

    invoke-virtual {v14, v7}, Lcom/sec/android/app/ve/theme/ThemeManager;->getThemeDisplayName(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v11, v14}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 288
    sget-object v14, Lcom/sec/android/app/ve/VEApp;->gThemeMgr:Lcom/sec/android/app/ve/theme/ThemeManager;

    invoke-virtual {v14, v7}, Lcom/sec/android/app/ve/theme/ThemeManager;->getThemeDisplayName(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v9, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 289
    const/4 v14, 0x1

    invoke-virtual {v9, v14}, Landroid/widget/TextView;->setSelected(Z)V

    .line 292
    if-eqz v13, :cond_6

    invoke-virtual {v13}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getThemeName()I

    move-result v14

    if-ne v14, v7, :cond_6

    .line 293
    const/4 v14, 0x0

    invoke-virtual {v12, v14}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 294
    const v14, 0x7f02005c

    invoke-virtual {v12, v14}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 295
    const/4 v14, 0x1

    invoke-virtual {v11, v14}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 306
    :goto_3
    sget-object v14, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v14}, Lcom/sec/android/app/storycam/AppDataManager;->getUHDCheckFlag()Z

    move-result v14

    if-eqz v14, :cond_7

    if-eqz p1, :cond_7

    .line 307
    const/4 v14, 0x0

    invoke-virtual {v6, v14}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 315
    :goto_4
    return-object v4

    .end local v1    # "backgroundImage":Landroid/widget/ImageView;
    .end local v2    # "bgdIamgeLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v5    # "themeBackgroundEndL":Landroid/widget/ImageView;
    .end local v6    # "themeCoverUHD":Landroid/widget/ImageView;
    .end local v7    # "themeId":I
    .end local v8    # "themeItemLayoutParams":Landroid/widget/AbsListView$LayoutParams;
    .end local v9    # "themeName":Landroid/widget/TextView;
    .end local v10    # "themeThumbnail":Landroid/widget/ImageView;
    .end local v11    # "themeThumbnailBorder":Landroid/widget/ImageView;
    .end local v12    # "themeThumbnailSelected":Landroid/widget/ImageView;
    .end local v13    # "transcodeElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    :cond_2
    move-object/from16 v4, p2

    .line 216
    check-cast v4, Landroid/widget/RelativeLayout;

    goto/16 :goto_0

    .line 261
    .restart local v1    # "backgroundImage":Landroid/widget/ImageView;
    .restart local v2    # "bgdIamgeLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v5    # "themeBackgroundEndL":Landroid/widget/ImageView;
    .restart local v6    # "themeCoverUHD":Landroid/widget/ImageView;
    .restart local v7    # "themeId":I
    .restart local v8    # "themeItemLayoutParams":Landroid/widget/AbsListView$LayoutParams;
    .restart local v9    # "themeName":Landroid/widget/TextView;
    .restart local v10    # "themeThumbnail":Landroid/widget/ImageView;
    .restart local v11    # "themeThumbnailBorder":Landroid/widget/ImageView;
    .restart local v12    # "themeThumbnailSelected":Landroid/widget/ImageView;
    .restart local v13    # "transcodeElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    :cond_3
    const v14, 0x7f0603d6

    invoke-static {v14}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v14

    iput v14, v8, Landroid/widget/AbsListView$LayoutParams;->width:I

    .line 262
    const v14, 0x7f0603d6

    invoke-static {v14}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v14

    iput v14, v2, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 263
    const/4 v14, 0x4

    invoke-virtual {v5, v14}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    .line 271
    :cond_4
    sget-object v14, Lcom/sec/android/app/ve/VEApp;->gThemeMgr:Lcom/sec/android/app/ve/theme/ThemeManager;

    invoke-virtual {v14}, Lcom/sec/android/app/ve/theme/ThemeManager;->getThemeCount()I

    move-result v14

    add-int/lit8 v14, v14, -0x1

    move/from16 v0, p1

    if-ne v0, v14, :cond_5

    .line 272
    const v14, 0x7f0603d7

    invoke-static {v14}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v14

    iput v14, v8, Landroid/widget/AbsListView$LayoutParams;->height:I

    .line 273
    const/4 v14, 0x0

    invoke-virtual {v5, v14}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 279
    :goto_5
    const/4 v14, 0x0

    invoke-virtual {v11, v14}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 280
    const/4 v14, 0x0

    invoke-virtual {v11, v14}, Landroid/widget/ImageView;->setClickable(Z)V

    goto/16 :goto_2

    .line 276
    :cond_5
    const v14, 0x7f0603d6

    invoke-static {v14}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v14

    iput v14, v8, Landroid/widget/AbsListView$LayoutParams;->height:I

    .line 277
    const/4 v14, 0x4

    invoke-virtual {v5, v14}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_5

    .line 299
    :cond_6
    const/4 v14, 0x4

    invoke-virtual {v12, v14}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 300
    const/4 v14, 0x0

    invoke-virtual {v11, v14}, Landroid/widget/ImageView;->setSelected(Z)V

    goto :goto_3

    .line 312
    :cond_7
    const/4 v14, 0x4

    invoke-virtual {v6, v14}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_4
.end method
