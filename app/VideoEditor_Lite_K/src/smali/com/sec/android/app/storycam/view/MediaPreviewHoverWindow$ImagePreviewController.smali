.class public Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$ImagePreviewController;
.super Ljava/lang/Object;
.source "MediaPreviewHoverWindow.java"

# interfaces
.implements Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$Controller;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ImagePreviewController"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 227
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public doLayout(Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;Landroid/view/View$OnHoverListener;)Landroid/view/View;
    .locals 11
    .param p1, "params"    # Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;
    .param p2, "hoverListener"    # Landroid/view/View$OnHoverListener;

    .prologue
    const v10, 0x7f0d0025

    const/4 v9, 0x0

    .line 231
    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getInflater()Landroid/view/LayoutInflater;

    move-result-object v6

    const v7, 0x7f030007

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    .line 232
    .local v3, "layout":Landroid/widget/RelativeLayout;
    const v6, 0x7f0d0024

    invoke-virtual {v3, v6}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 233
    .local v0, "contentView":Landroid/widget/RelativeLayout;
    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout$LayoutParams;

    .line 234
    .local v4, "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    # getter for: Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;->mLeft:I
    invoke-static {p1}, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;->access$0(Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;)I

    move-result v6

    iput v6, v4, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 235
    # getter for: Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;->mTop:I
    invoke-static {p1}, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;->access$1(Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;)I

    move-result v6

    iput v6, v4, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 236
    # getter for: Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;->mWidth:I
    invoke-static {p1}, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;->access$2(Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;)I

    move-result v6

    iput v6, v4, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 237
    # getter for: Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;->mHeight:I
    invoke-static {p1}, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;->access$3(Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;)I

    move-result v6

    iput v6, v4, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 238
    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 240
    invoke-virtual {v3, v10}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 241
    .local v2, "imageView":Landroid/widget/ImageView;
    invoke-virtual {v3, v10}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 242
    .local v1, "imageParams":Landroid/widget/RelativeLayout$LayoutParams;
    # getter for: Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;->mWidth:I
    invoke-static {p1}, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;->access$2(Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;)I

    move-result v6

    iput v6, v1, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 243
    # getter for: Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;->mHeight:I
    invoke-static {p1}, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;->access$3(Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;)I

    move-result v6

    iput v6, v1, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 244
    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 245
    # getter for: Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;->mBitmap:Landroid/graphics/Bitmap;
    invoke-static {p1}, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;->access$4(Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;)Landroid/graphics/Bitmap;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 247
    const v6, 0x7f0d0026

    invoke-virtual {v3, v6}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    .line 248
    .local v5, "unsupImageView":Landroid/widget/ImageView;
    # getter for: Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;->mIsSupported:Z
    invoke-static {p1}, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;->access$5(Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 249
    invoke-virtual {v5, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 250
    const v6, 0x7f02005f

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 253
    :goto_0
    return-object v3

    .line 252
    :cond_0
    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public refreshThumbnails(Ljava/util/LinkedHashMap;I)V
    .locals 0
    .param p2, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/graphics/Bitmap;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 262
    .local p1, "thumbList":Ljava/util/LinkedHashMap;, "Ljava/util/LinkedHashMap<Ljava/lang/Integer;Landroid/graphics/Bitmap;>;"
    return-void
.end method

.method public release()V
    .locals 0

    .prologue
    .line 258
    return-void
.end method
