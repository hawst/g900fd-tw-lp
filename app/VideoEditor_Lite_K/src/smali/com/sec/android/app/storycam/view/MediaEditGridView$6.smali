.class Lcom/sec/android/app/storycam/view/MediaEditGridView$6;
.super Ljava/lang/Object;
.source "MediaEditGridView.java"

# interfaces
.implements Landroid/view/View$OnDragListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/storycam/view/MediaEditGridView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/storycam/view/MediaEditGridView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView$6;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    .line 253
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDrag(Landroid/view/View;Landroid/view/DragEvent;)Z
    .locals 16
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/DragEvent;

    .prologue
    .line 257
    invoke-virtual/range {p2 .. p2}, Landroid/view/DragEvent;->getLocalState()Ljava/lang/Object;

    move-result-object v12

    instance-of v12, v12, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;

    if-nez v12, :cond_0

    .line 258
    const/4 v12, 0x0

    .line 380
    :goto_0
    return v12

    .line 259
    :cond_0
    const/4 v6, -0x1

    .line 260
    .local v6, "droppedLocation":I
    const/4 v12, 0x5

    invoke-virtual/range {p2 .. p2}, Landroid/view/DragEvent;->getAction()I

    move-result v13

    if-ne v12, v13, :cond_3

    .line 261
    invoke-virtual/range {p2 .. p2}, Landroid/view/DragEvent;->getLocalState()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;

    .line 262
    .local v5, "draggingView":Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/storycam/view/MediaEditGridView$6;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    # getter for: Lcom/sec/android/app/storycam/view/MediaEditGridView;->mDragMovementListener:Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;
    invoke-static {v12}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->access$10(Lcom/sec/android/app/storycam/view/MediaEditGridView;)Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;

    move-result-object v12

    invoke-virtual {v12}, Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;->reset()V

    .line 263
    move-object/from16 v0, p1

    instance-of v12, v0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;

    if-eqz v12, :cond_2

    .line 264
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/storycam/view/MediaEditGridView$6;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    # invokes: Lcom/sec/android/app/storycam/view/MediaEditGridView;->resetDragState()V
    invoke-static {v12}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->access$11(Lcom/sec/android/app/storycam/view/MediaEditGridView;)V

    .line 265
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/storycam/view/MediaEditGridView$6;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    invoke-static {v12, v5}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->access$12(Lcom/sec/android/app/storycam/view/MediaEditGridView;Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;)V

    .line 266
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/storycam/view/MediaEditGridView$6;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/storycam/view/MediaEditGridView$6;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    move-object/from16 v0, p1

    invoke-virtual {v13, v0}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->indexOfChild(Landroid/view/View;)I

    move-result v13

    invoke-static {v12, v13}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->access$13(Lcom/sec/android/app/storycam/view/MediaEditGridView;I)V

    .line 267
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/storycam/view/MediaEditGridView$6;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    # getter for: Lcom/sec/android/app/storycam/view/MediaEditGridView;->mHandler:Landroid/os/Handler;
    invoke-static {v12}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->access$9(Lcom/sec/android/app/storycam/view/MediaEditGridView;)Landroid/os/Handler;

    move-result-object v12

    const/4 v13, 0x1

    const-wide/16 v14, 0x64

    invoke-virtual {v12, v13, v14, v15}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 380
    .end local v5    # "draggingView":Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;
    :cond_1
    :goto_1
    const/4 v12, 0x1

    goto :goto_0

    .line 270
    .restart local v5    # "draggingView":Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;
    :cond_2
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/storycam/view/MediaEditGridView$6;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    # invokes: Lcom/sec/android/app/storycam/view/MediaEditGridView;->resetDragState()V
    invoke-static {v12}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->access$11(Lcom/sec/android/app/storycam/view/MediaEditGridView;)V

    .line 271
    sget-object v12, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v12}, Lcom/sec/android/app/storycam/AppDataManager;->getOriginalTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v11

    .line 272
    .local v11, "trans":Lcom/samsung/app/video/editor/external/TranscodeElement;
    invoke-virtual {v11}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v12

    add-int/lit8 v6, v12, -0x1

    .line 273
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/storycam/view/MediaEditGridView$6;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    invoke-static {v12, v6}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->access$13(Lcom/sec/android/app/storycam/view/MediaEditGridView;I)V

    .line 274
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/storycam/view/MediaEditGridView$6;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    invoke-static {v12, v5}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->access$12(Lcom/sec/android/app/storycam/view/MediaEditGridView;Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;)V

    goto :goto_1

    .line 277
    .end local v5    # "draggingView":Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;
    .end local v11    # "trans":Lcom/samsung/app/video/editor/external/TranscodeElement;
    :cond_3
    const/4 v12, 0x2

    invoke-virtual/range {p2 .. p2}, Landroid/view/DragEvent;->getAction()I

    move-result v13

    if-ne v12, v13, :cond_9

    move-object/from16 v0, p1

    instance-of v12, v0, Lcom/sec/android/app/storycam/view/MediaEditGridView;

    if-eqz v12, :cond_9

    .line 278
    # getter for: Lcom/sec/android/app/storycam/view/MediaEditGridView;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;
    invoke-static {}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->access$1()Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    move-result-object v12

    invoke-interface {v12}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;->getCurrentConfig()Landroid/content/res/Configuration;

    move-result-object v12

    iget v12, v12, Landroid/content/res/Configuration;->orientation:I

    const/4 v13, 0x1

    if-ne v12, v13, :cond_6

    .line 279
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/storycam/view/MediaEditGridView$6;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    invoke-virtual {v12}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->getParent()Landroid/view/ViewParent;

    move-result-object v12

    check-cast v12, Lcom/sec/android/app/storycam/view/AutoScrollableVerticalScrollView;

    invoke-virtual {v12}, Lcom/sec/android/app/storycam/view/AutoScrollableVerticalScrollView;->getViewStartPosition()I

    move-result v7

    .line 280
    .local v7, "origin":I
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/storycam/view/MediaEditGridView$6;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    # getter for: Lcom/sec/android/app/storycam/view/MediaEditGridView;->mDragMovementListener:Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;
    invoke-static {v12}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->access$10(Lcom/sec/android/app/storycam/view/MediaEditGridView;)Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;

    move-result-object v12

    const/4 v13, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v12, v0, v13}, Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;->listenToDrag(Landroid/view/DragEvent;I)V

    .line 281
    invoke-virtual/range {p2 .. p2}, Landroid/view/DragEvent;->getY()F

    move-result v12

    int-to-float v13, v7

    sub-float/2addr v12, v13

    const v13, 0x7f060391

    invoke-static {v13}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v13

    int-to-float v13, v13

    cmpl-float v12, v12, v13

    if-lez v12, :cond_4

    .line 282
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/storycam/view/MediaEditGridView$6;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    # getter for: Lcom/sec/android/app/storycam/view/MediaEditGridView;->mDragMovementListener:Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;
    invoke-static {v12}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->access$10(Lcom/sec/android/app/storycam/view/MediaEditGridView;)Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;

    move-result-object v12

    invoke-virtual {v12}, Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;->getDragDirection()I

    move-result v12

    const/4 v13, 0x2

    if-ne v12, v13, :cond_1

    .line 283
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/storycam/view/MediaEditGridView$6;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    const/4 v13, 0x3

    invoke-virtual {v12, v13}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->startAutoScroll(I)V

    goto/16 :goto_1

    .line 284
    :cond_4
    invoke-virtual/range {p2 .. p2}, Landroid/view/DragEvent;->getY()F

    move-result v12

    int-to-float v13, v7

    sub-float/2addr v12, v13

    const v13, 0x7f060392

    invoke-static {v13}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v13

    int-to-float v13, v13

    cmpg-float v12, v12, v13

    if-gez v12, :cond_5

    .line 285
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/storycam/view/MediaEditGridView$6;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    # getter for: Lcom/sec/android/app/storycam/view/MediaEditGridView;->mDragMovementListener:Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;
    invoke-static {v12}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->access$10(Lcom/sec/android/app/storycam/view/MediaEditGridView;)Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;

    move-result-object v12

    invoke-virtual {v12}, Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;->getDragDirection()I

    move-result v12

    const/4 v13, 0x1

    if-ne v12, v13, :cond_1

    .line 286
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/storycam/view/MediaEditGridView$6;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    const/4 v13, 0x2

    invoke-virtual {v12, v13}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->startAutoScroll(I)V

    goto/16 :goto_1

    .line 288
    :cond_5
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/storycam/view/MediaEditGridView$6;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    move-object/from16 v0, p2

    invoke-virtual {v12, v0}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->stopAutoScoll(Landroid/view/DragEvent;)V

    goto/16 :goto_1

    .line 290
    .end local v7    # "origin":I
    :cond_6
    # getter for: Lcom/sec/android/app/storycam/view/MediaEditGridView;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;
    invoke-static {}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->access$1()Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    move-result-object v12

    invoke-interface {v12}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;->getCurrentConfig()Landroid/content/res/Configuration;

    move-result-object v12

    iget v12, v12, Landroid/content/res/Configuration;->orientation:I

    const/4 v13, 0x2

    if-ne v12, v13, :cond_1

    .line 291
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/storycam/view/MediaEditGridView$6;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    invoke-virtual {v12}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->getParent()Landroid/view/ViewParent;

    move-result-object v12

    check-cast v12, Lcom/sec/android/app/storycam/view/AutoScrollableHorizontalScrollView;

    invoke-virtual {v12}, Lcom/sec/android/app/storycam/view/AutoScrollableHorizontalScrollView;->getViewStartPosition()I

    move-result v7

    .line 292
    .restart local v7    # "origin":I
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/storycam/view/MediaEditGridView$6;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    # getter for: Lcom/sec/android/app/storycam/view/MediaEditGridView;->mDragMovementListener:Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;
    invoke-static {v12}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->access$10(Lcom/sec/android/app/storycam/view/MediaEditGridView;)Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;

    move-result-object v12

    const/4 v13, 0x1

    move-object/from16 v0, p2

    invoke-virtual {v12, v0, v13}, Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;->listenToDrag(Landroid/view/DragEvent;I)V

    .line 293
    invoke-virtual/range {p2 .. p2}, Landroid/view/DragEvent;->getX()F

    move-result v12

    int-to-float v13, v7

    sub-float/2addr v12, v13

    const v13, 0x7f060393

    invoke-static {v13}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v13

    int-to-float v13, v13

    cmpl-float v12, v12, v13

    if-lez v12, :cond_7

    .line 294
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/storycam/view/MediaEditGridView$6;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    # getter for: Lcom/sec/android/app/storycam/view/MediaEditGridView;->mDragMovementListener:Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;
    invoke-static {v12}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->access$10(Lcom/sec/android/app/storycam/view/MediaEditGridView;)Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;

    move-result-object v12

    invoke-virtual {v12}, Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;->getDragDirection()I

    move-result v12

    const/4 v13, 0x2

    if-ne v12, v13, :cond_1

    .line 295
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/storycam/view/MediaEditGridView$6;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    const/4 v13, 0x3

    invoke-virtual {v12, v13}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->startAutoScroll(I)V

    goto/16 :goto_1

    .line 296
    :cond_7
    invoke-virtual/range {p2 .. p2}, Landroid/view/DragEvent;->getX()F

    move-result v12

    int-to-float v13, v7

    sub-float/2addr v12, v13

    const v13, 0x7f060394

    invoke-static {v13}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v13

    int-to-float v13, v13

    cmpg-float v12, v12, v13

    if-gez v12, :cond_8

    .line 297
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/storycam/view/MediaEditGridView$6;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    # getter for: Lcom/sec/android/app/storycam/view/MediaEditGridView;->mDragMovementListener:Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;
    invoke-static {v12}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->access$10(Lcom/sec/android/app/storycam/view/MediaEditGridView;)Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;

    move-result-object v12

    invoke-virtual {v12}, Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;->getDragDirection()I

    move-result v12

    const/4 v13, 0x1

    if-ne v12, v13, :cond_1

    .line 298
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/storycam/view/MediaEditGridView$6;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    const/4 v13, 0x2

    invoke-virtual {v12, v13}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->startAutoScroll(I)V

    goto/16 :goto_1

    .line 300
    :cond_8
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/storycam/view/MediaEditGridView$6;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    move-object/from16 v0, p2

    invoke-virtual {v12, v0}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->stopAutoScoll(Landroid/view/DragEvent;)V

    goto/16 :goto_1

    .line 304
    .end local v7    # "origin":I
    :cond_9
    const/4 v12, 0x2

    invoke-virtual/range {p2 .. p2}, Landroid/view/DragEvent;->getAction()I

    move-result v13

    if-ne v12, v13, :cond_11

    move-object/from16 v0, p1

    instance-of v12, v0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;

    if-eqz v12, :cond_11

    .line 305
    # getter for: Lcom/sec/android/app/storycam/view/MediaEditGridView;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;
    invoke-static {}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->access$1()Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    move-result-object v12

    invoke-interface {v12}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;->getCurrentConfig()Landroid/content/res/Configuration;

    move-result-object v12

    iget v12, v12, Landroid/content/res/Configuration;->orientation:I

    const/4 v13, 0x1

    if-ne v12, v13, :cond_d

    .line 306
    const/4 v4, 0x0

    .line 307
    .local v4, "dragY":I
    new-instance v10, Landroid/graphics/Rect;

    invoke-direct {v10}, Landroid/graphics/Rect;-><init>()V

    .line 308
    .local v10, "rect":Landroid/graphics/Rect;
    new-instance v9, Landroid/graphics/Point;

    invoke-direct {v9}, Landroid/graphics/Point;-><init>()V

    .line 309
    .local v9, "point":Landroid/graphics/Point;
    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v9}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;Landroid/graphics/Point;)Z

    move-result v12

    if-eqz v12, :cond_a

    .line 310
    iget v12, v10, Landroid/graphics/Rect;->top:I

    invoke-virtual/range {p2 .. p2}, Landroid/view/DragEvent;->getY()F

    move-result v13

    float-to-int v13, v13

    add-int v4, v12, v13

    .line 311
    iget v12, v9, Landroid/graphics/Point;->y:I

    if-gez v12, :cond_a

    .line 312
    iget v12, v9, Landroid/graphics/Point;->y:I

    invoke-static {v12}, Ljava/lang/Math;->abs(I)I

    move-result v12

    sub-int/2addr v4, v12

    .line 315
    :cond_a
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/storycam/view/MediaEditGridView$6;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    # getter for: Lcom/sec/android/app/storycam/view/MediaEditGridView;->mDragMovementListener:Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;
    invoke-static {v12}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->access$10(Lcom/sec/android/app/storycam/view/MediaEditGridView;)Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;

    move-result-object v12

    const/4 v13, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v12, v0, v13}, Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;->listenToDrag(Landroid/view/DragEvent;I)V

    .line 316
    const v12, 0x7f060391

    invoke-static {v12}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v12

    if-le v4, v12, :cond_b

    .line 317
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/storycam/view/MediaEditGridView$6;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    # getter for: Lcom/sec/android/app/storycam/view/MediaEditGridView;->mDragMovementListener:Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;
    invoke-static {v12}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->access$10(Lcom/sec/android/app/storycam/view/MediaEditGridView;)Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;

    move-result-object v12

    invoke-virtual {v12}, Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;->getDragDirection()I

    move-result v12

    const/4 v13, 0x2

    if-ne v12, v13, :cond_1

    .line 318
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/storycam/view/MediaEditGridView$6;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    const/4 v13, 0x3

    invoke-virtual {v12, v13}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->startAutoScroll(I)V

    goto/16 :goto_1

    .line 319
    :cond_b
    const v12, 0x7f060392

    invoke-static {v12}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v12

    if-ge v4, v12, :cond_c

    .line 320
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/storycam/view/MediaEditGridView$6;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    # getter for: Lcom/sec/android/app/storycam/view/MediaEditGridView;->mDragMovementListener:Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;
    invoke-static {v12}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->access$10(Lcom/sec/android/app/storycam/view/MediaEditGridView;)Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;

    move-result-object v12

    invoke-virtual {v12}, Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;->getDragDirection()I

    move-result v12

    const/4 v13, 0x1

    if-ne v12, v13, :cond_1

    .line 321
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/storycam/view/MediaEditGridView$6;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    const/4 v13, 0x2

    invoke-virtual {v12, v13}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->startAutoScroll(I)V

    goto/16 :goto_1

    .line 323
    :cond_c
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/storycam/view/MediaEditGridView$6;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    move-object/from16 v0, p2

    invoke-virtual {v12, v0}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->stopAutoScoll(Landroid/view/DragEvent;)V

    goto/16 :goto_1

    .line 325
    .end local v4    # "dragY":I
    .end local v9    # "point":Landroid/graphics/Point;
    .end local v10    # "rect":Landroid/graphics/Rect;
    :cond_d
    # getter for: Lcom/sec/android/app/storycam/view/MediaEditGridView;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;
    invoke-static {}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->access$1()Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    move-result-object v12

    invoke-interface {v12}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;->getCurrentConfig()Landroid/content/res/Configuration;

    move-result-object v12

    iget v12, v12, Landroid/content/res/Configuration;->orientation:I

    const/4 v13, 0x2

    if-ne v12, v13, :cond_1

    .line 326
    const/4 v3, 0x0

    .line 327
    .local v3, "dragX":I
    new-instance v10, Landroid/graphics/Rect;

    invoke-direct {v10}, Landroid/graphics/Rect;-><init>()V

    .line 328
    .restart local v10    # "rect":Landroid/graphics/Rect;
    new-instance v9, Landroid/graphics/Point;

    invoke-direct {v9}, Landroid/graphics/Point;-><init>()V

    .line 329
    .restart local v9    # "point":Landroid/graphics/Point;
    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v9}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;Landroid/graphics/Point;)Z

    move-result v12

    if-eqz v12, :cond_e

    .line 330
    iget v12, v10, Landroid/graphics/Rect;->left:I

    invoke-virtual/range {p2 .. p2}, Landroid/view/DragEvent;->getX()F

    move-result v13

    float-to-int v13, v13

    add-int v3, v12, v13

    .line 331
    iget v12, v9, Landroid/graphics/Point;->x:I

    if-gez v12, :cond_e

    .line 332
    iget v12, v9, Landroid/graphics/Point;->x:I

    invoke-static {v12}, Ljava/lang/Math;->abs(I)I

    move-result v12

    sub-int/2addr v3, v12

    .line 335
    :cond_e
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/storycam/view/MediaEditGridView$6;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    # getter for: Lcom/sec/android/app/storycam/view/MediaEditGridView;->mDragMovementListener:Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;
    invoke-static {v12}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->access$10(Lcom/sec/android/app/storycam/view/MediaEditGridView;)Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;

    move-result-object v12

    const/4 v13, 0x1

    move-object/from16 v0, p2

    invoke-virtual {v12, v0, v13}, Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;->listenToDrag(Landroid/view/DragEvent;I)V

    .line 336
    const v12, 0x7f060393

    invoke-static {v12}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v12

    if-le v3, v12, :cond_f

    .line 337
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/storycam/view/MediaEditGridView$6;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    # getter for: Lcom/sec/android/app/storycam/view/MediaEditGridView;->mDragMovementListener:Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;
    invoke-static {v12}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->access$10(Lcom/sec/android/app/storycam/view/MediaEditGridView;)Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;

    move-result-object v12

    invoke-virtual {v12}, Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;->getDragDirection()I

    move-result v12

    const/4 v13, 0x2

    if-ne v12, v13, :cond_1

    .line 338
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/storycam/view/MediaEditGridView$6;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    const/4 v13, 0x3

    invoke-virtual {v12, v13}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->startAutoScroll(I)V

    goto/16 :goto_1

    .line 339
    :cond_f
    const v12, 0x7f060394

    invoke-static {v12}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v12

    if-ge v3, v12, :cond_10

    .line 340
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/storycam/view/MediaEditGridView$6;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    # getter for: Lcom/sec/android/app/storycam/view/MediaEditGridView;->mDragMovementListener:Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;
    invoke-static {v12}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->access$10(Lcom/sec/android/app/storycam/view/MediaEditGridView;)Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;

    move-result-object v12

    invoke-virtual {v12}, Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;->getDragDirection()I

    move-result v12

    const/4 v13, 0x1

    if-ne v12, v13, :cond_1

    .line 341
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/storycam/view/MediaEditGridView$6;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    const/4 v13, 0x2

    invoke-virtual {v12, v13}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->startAutoScroll(I)V

    goto/16 :goto_1

    .line 343
    :cond_10
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/storycam/view/MediaEditGridView$6;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    move-object/from16 v0, p2

    invoke-virtual {v12, v0}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->stopAutoScoll(Landroid/view/DragEvent;)V

    goto/16 :goto_1

    .line 347
    .end local v3    # "dragX":I
    .end local v9    # "point":Landroid/graphics/Point;
    .end local v10    # "rect":Landroid/graphics/Rect;
    :cond_11
    const/4 v12, 0x3

    invoke-virtual/range {p2 .. p2}, Landroid/view/DragEvent;->getAction()I

    move-result v13

    if-ne v12, v13, :cond_13

    .line 348
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/storycam/view/MediaEditGridView$6;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    # invokes: Lcom/sec/android/app/storycam/view/MediaEditGridView;->resetDragState()V
    invoke-static {v12}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->access$11(Lcom/sec/android/app/storycam/view/MediaEditGridView;)V

    .line 349
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/storycam/view/MediaEditGridView$6;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    # getter for: Lcom/sec/android/app/storycam/view/MediaEditGridView;->mDragMovementListener:Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;
    invoke-static {v12}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->access$10(Lcom/sec/android/app/storycam/view/MediaEditGridView;)Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;

    move-result-object v12

    invoke-virtual {v12}, Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;->reset()V

    .line 350
    invoke-virtual/range {p2 .. p2}, Landroid/view/DragEvent;->getLocalState()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;

    .line 351
    .restart local v5    # "draggingView":Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/storycam/view/MediaEditGridView$6;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    invoke-virtual {v12, v5}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->indexOfChild(Landroid/view/View;)I

    move-result v2

    .line 352
    .local v2, "currentIndex":I
    move-object/from16 v0, p1

    instance-of v12, v0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;

    if-eqz v12, :cond_12

    .line 353
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/storycam/view/MediaEditGridView$6;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    invoke-virtual {v5}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->getIndex()I

    move-result v13

    # invokes: Lcom/sec/android/app/storycam/view/MediaEditGridView;->changeElementPosition(II)V
    invoke-static {v12, v13, v2}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->access$14(Lcom/sec/android/app/storycam/view/MediaEditGridView;II)V

    .line 354
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/storycam/view/MediaEditGridView$6;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    invoke-virtual {v12, v5}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->removeView(Landroid/view/View;)V

    .line 355
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/storycam/view/MediaEditGridView$6;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    invoke-virtual {v12, v5, v2}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->addView(Landroid/view/View;I)V

    .line 363
    :goto_2
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/storycam/view/MediaEditGridView$6;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    # invokes: Lcom/sec/android/app/storycam/view/MediaEditGridView;->refreshAllViews()V
    invoke-static {v12}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->access$0(Lcom/sec/android/app/storycam/view/MediaEditGridView;)V

    goto/16 :goto_1

    .line 357
    :cond_12
    sget-object v12, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v12}, Lcom/sec/android/app/storycam/AppDataManager;->getOriginalTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v11

    .line 358
    .restart local v11    # "trans":Lcom/samsung/app/video/editor/external/TranscodeElement;
    invoke-virtual {v11}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v12

    add-int/lit8 v6, v12, -0x1

    .line 359
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/storycam/view/MediaEditGridView$6;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    invoke-virtual {v5}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->getIndex()I

    move-result v13

    # invokes: Lcom/sec/android/app/storycam/view/MediaEditGridView;->changeElementPosition(II)V
    invoke-static {v12, v13, v6}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->access$14(Lcom/sec/android/app/storycam/view/MediaEditGridView;II)V

    .line 360
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/storycam/view/MediaEditGridView$6;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    invoke-virtual {v12, v5}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->removeView(Landroid/view/View;)V

    .line 361
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/storycam/view/MediaEditGridView$6;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    invoke-virtual {v12, v5, v6}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->addView(Landroid/view/View;I)V

    goto :goto_2

    .line 365
    .end local v2    # "currentIndex":I
    .end local v5    # "draggingView":Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;
    .end local v11    # "trans":Lcom/samsung/app/video/editor/external/TranscodeElement;
    :cond_13
    const/4 v12, 0x4

    invoke-virtual/range {p2 .. p2}, Landroid/view/DragEvent;->getAction()I

    move-result v13

    if-ne v12, v13, :cond_1

    .line 366
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/storycam/view/MediaEditGridView$6;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    # invokes: Lcom/sec/android/app/storycam/view/MediaEditGridView;->resetDragState()V
    invoke-static {v12}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->access$11(Lcom/sec/android/app/storycam/view/MediaEditGridView;)V

    .line 367
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/storycam/view/MediaEditGridView$6;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    # getter for: Lcom/sec/android/app/storycam/view/MediaEditGridView;->mDragMovementListener:Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;
    invoke-static {v12}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->access$10(Lcom/sec/android/app/storycam/view/MediaEditGridView;)Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;

    move-result-object v12

    invoke-virtual {v12}, Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;->reset()V

    .line 368
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/storycam/view/MediaEditGridView$6;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    move-object/from16 v0, p2

    invoke-virtual {v12, v0}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->stopAutoScoll(Landroid/view/DragEvent;)V

    .line 369
    invoke-virtual/range {p2 .. p2}, Landroid/view/DragEvent;->getResult()Z

    move-result v12

    if-nez v12, :cond_14

    .line 371
    invoke-virtual/range {p2 .. p2}, Landroid/view/DragEvent;->getLocalState()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;

    .line 372
    .restart local v5    # "draggingView":Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;
    invoke-virtual {v5}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->getIndex()I

    move-result v8

    .line 373
    .local v8, "originalIndex":I
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/storycam/view/MediaEditGridView$6;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    invoke-virtual {v12, v5}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->indexOfChild(Landroid/view/View;)I

    move-result v12

    if-eq v8, v12, :cond_14

    .line 374
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/storycam/view/MediaEditGridView$6;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    invoke-virtual {v12, v5}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->removeView(Landroid/view/View;)V

    .line 375
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/storycam/view/MediaEditGridView$6;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    invoke-virtual {v12, v5, v8}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->addView(Landroid/view/View;I)V

    .line 378
    .end local v5    # "draggingView":Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;
    .end local v8    # "originalIndex":I
    :cond_14
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/storycam/view/MediaEditGridView$6;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    # invokes: Lcom/sec/android/app/storycam/view/MediaEditGridView;->refreshAllViews()V
    invoke-static {v12}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->access$0(Lcom/sec/android/app/storycam/view/MediaEditGridView;)V

    goto/16 :goto_1
.end method
