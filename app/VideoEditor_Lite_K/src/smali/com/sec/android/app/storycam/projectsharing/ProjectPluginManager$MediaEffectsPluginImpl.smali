.class public Lcom/sec/android/app/storycam/projectsharing/ProjectPluginManager$MediaEffectsPluginImpl;
.super Ljava/lang/Object;
.source "ProjectPluginManager.java"

# interfaces
.implements Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework$MediaEffectsPlugin;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/storycam/projectsharing/ProjectPluginManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MediaEffectsPluginImpl"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 124
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public mapEffectTypeFromGeneric(I)I
    .locals 1
    .param p1, "genericEffectType"    # I

    .prologue
    .line 217
    const/16 v0, 0x16

    .line 219
    .local v0, "internalEffectType":I
    packed-switch p1, :pswitch_data_0

    .line 246
    :goto_0
    return v0

    .line 221
    :pswitch_0
    const/16 v0, 0x17

    .line 222
    goto :goto_0

    .line 224
    :pswitch_1
    const/16 v0, 0x1b

    .line 225
    goto :goto_0

    .line 227
    :pswitch_2
    const/16 v0, 0x1f

    .line 228
    goto :goto_0

    .line 230
    :pswitch_3
    const/16 v0, 0x18

    .line 231
    goto :goto_0

    .line 233
    :pswitch_4
    const/16 v0, 0x1c

    .line 234
    goto :goto_0

    .line 236
    :pswitch_5
    const/16 v0, 0x2b

    .line 237
    goto :goto_0

    .line 239
    :pswitch_6
    const/16 v0, 0x2c

    .line 240
    goto :goto_0

    .line 242
    :pswitch_7
    const/16 v0, 0x27

    .line 243
    goto :goto_0

    .line 219
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_4
        :pswitch_2
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public mapEffectTypeToGeneric(I)I
    .locals 1
    .param p1, "internalEffectType"    # I

    .prologue
    .line 128
    const/4 v0, 0x0

    .line 130
    .local v0, "genericEffectType":I
    sparse-switch p1, :sswitch_data_0

    .line 212
    :goto_0
    return v0

    .line 132
    :sswitch_0
    const/4 v0, 0x1

    .line 133
    goto :goto_0

    .line 135
    :sswitch_1
    const/4 v0, 0x3

    .line 136
    goto :goto_0

    .line 138
    :sswitch_2
    const/4 v0, 0x5

    .line 139
    goto :goto_0

    .line 141
    :sswitch_3
    const/4 v0, 0x2

    .line 142
    goto :goto_0

    .line 144
    :sswitch_4
    const/4 v0, 0x4

    .line 145
    goto :goto_0

    .line 147
    :sswitch_5
    const/4 v0, 0x6

    .line 148
    goto :goto_0

    .line 150
    :sswitch_6
    const/4 v0, 0x7

    .line 151
    goto :goto_0

    .line 153
    :sswitch_7
    const/16 v0, 0x8

    .line 154
    goto :goto_0

    .line 158
    :sswitch_8
    const/16 v0, 0x9

    .line 159
    goto :goto_0

    .line 161
    :sswitch_9
    const/16 v0, 0xa

    .line 162
    goto :goto_0

    .line 164
    :sswitch_a
    const/16 v0, 0xb

    .line 165
    goto :goto_0

    .line 167
    :sswitch_b
    const/16 v0, 0xc

    .line 168
    goto :goto_0

    .line 170
    :sswitch_c
    const/16 v0, 0xd

    .line 171
    goto :goto_0

    .line 173
    :sswitch_d
    const/16 v0, 0xe

    .line 174
    goto :goto_0

    .line 176
    :sswitch_e
    const/16 v0, 0xf

    .line 177
    goto :goto_0

    .line 179
    :sswitch_f
    const/16 v0, 0x10

    .line 180
    goto :goto_0

    .line 182
    :sswitch_10
    const/16 v0, 0x11

    .line 183
    goto :goto_0

    .line 185
    :sswitch_11
    const/16 v0, 0x12

    .line 186
    goto :goto_0

    .line 188
    :sswitch_12
    const/16 v0, 0x13

    .line 189
    goto :goto_0

    .line 191
    :sswitch_13
    const/16 v0, 0x14

    .line 192
    goto :goto_0

    .line 194
    :sswitch_14
    const/16 v0, 0x15

    .line 195
    goto :goto_0

    .line 197
    :sswitch_15
    const/16 v0, 0x16

    .line 198
    goto :goto_0

    .line 200
    :sswitch_16
    const/16 v0, 0x17

    .line 201
    goto :goto_0

    .line 203
    :sswitch_17
    const/16 v0, 0x18

    .line 204
    goto :goto_0

    .line 206
    :sswitch_18
    const/16 v0, 0x19

    .line 207
    goto :goto_0

    .line 130
    nop

    :sswitch_data_0
    .sparse-switch
        0x17 -> :sswitch_0
        0x18 -> :sswitch_3
        0x1b -> :sswitch_1
        0x1c -> :sswitch_4
        0x1f -> :sswitch_2
        0x27 -> :sswitch_7
        0x2b -> :sswitch_5
        0x2c -> :sswitch_6
        0x5b -> :sswitch_18
        0x5d -> :sswitch_8
        0x5e -> :sswitch_12
        0x5f -> :sswitch_9
        0x60 -> :sswitch_16
        0x61 -> :sswitch_a
        0x62 -> :sswitch_c
        0x63 -> :sswitch_b
        0x6c -> :sswitch_17
        0x6d -> :sswitch_10
        0x6e -> :sswitch_f
        0x6f -> :sswitch_14
        0x70 -> :sswitch_11
        0x71 -> :sswitch_13
        0x72 -> :sswitch_e
        0x73 -> :sswitch_d
        0x74 -> :sswitch_15
    .end sparse-switch
.end method
