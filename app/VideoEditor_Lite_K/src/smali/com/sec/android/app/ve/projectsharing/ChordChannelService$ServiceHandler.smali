.class Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ServiceHandler;
.super Landroid/os/Handler;
.source "ChordChannelService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/ve/projectsharing/ChordChannelService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ServiceHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .prologue
    .line 942
    iput-object p1, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ServiceHandler;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    .line 943
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 944
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 948
    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    .line 978
    :goto_0
    :pswitch_0
    return-void

    .line 950
    :pswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    .line 951
    .local v0, "fromBR":Ljava/lang/Boolean;
    iget-object v2, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ServiceHandler;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    # invokes: Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->initChord(Z)V
    invoke-static {v2, v3}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->access$22(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;Z)V

    goto :goto_0

    .line 957
    .end local v0    # "fromBR":Ljava/lang/Boolean;
    :pswitch_2
    iget-object v2, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ServiceHandler;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    # invokes: Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->notifyProjectTransferToDestination()V
    invoke-static {v2}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->access$23(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;)V

    goto :goto_0

    .line 960
    :pswitch_3
    iget-object v2, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ServiceHandler;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    # invokes: Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->closeChord()V
    invoke-static {v2}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->access$24(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;)V

    goto :goto_0

    .line 963
    :pswitch_4
    iget-object v2, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ServiceHandler;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    # invokes: Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->exitThread()V
    invoke-static {v2}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->access$25(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;)V

    goto :goto_0

    .line 966
    :pswitch_5
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 967
    .local v1, "networkInterface":I
    iget-object v2, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ServiceHandler;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    # invokes: Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->startChord(I)V
    invoke-static {v2, v1}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->access$21(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;I)V

    goto :goto_0

    .line 970
    .end local v1    # "networkInterface":I
    :pswitch_6
    iget-object v2, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ServiceHandler;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    # invokes: Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->cancelProjectShareSession()V
    invoke-static {v2}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->access$26(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;)V

    goto :goto_0

    .line 973
    :pswitch_7
    iget-object v3, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ServiceHandler;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ProjectApprovalMessage;

    # invokes: Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->approveProjectShare(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ProjectApprovalMessage;)V
    invoke-static {v3, v2}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->access$18(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ProjectApprovalMessage;)V

    goto :goto_0

    .line 948
    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_2
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method
