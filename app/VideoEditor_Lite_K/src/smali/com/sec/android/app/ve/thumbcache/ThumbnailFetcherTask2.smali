.class public Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcherTask2;
.super Lcom/sec/android/app/ve/thread/SimpleTask;
.source "ThumbnailFetcherTask2.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private mCache:Lcom/sec/android/app/ve/thumbcache/DiskLruCache;

.field private mCallBack:Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcher$Adapter;

.field private mElementType:I

.field private mKey:Ljava/lang/String;

.field private mOptionalTag:Ljava/lang/Object;

.field private mPos:I

.field private mTaskPool:Lcom/sec/android/app/ve/thread/AsyncTaskPool;

.field private mThumbHeight:I

.field private mThumbWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcherTask2;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcherTask2;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;ILcom/sec/android/app/ve/thread/AsyncTaskPool;Lcom/sec/android/app/ve/thumbcache/DiskLruCache;Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcher$Adapter;IILjava/lang/Object;I)V
    .locals 1
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "position"    # I
    .param p3, "pool"    # Lcom/sec/android/app/ve/thread/AsyncTaskPool;
    .param p4, "cache"    # Lcom/sec/android/app/ve/thumbcache/DiskLruCache;
    .param p5, "callBack"    # Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcher$Adapter;
    .param p6, "thumbnailWidth"    # I
    .param p7, "thumbnailHeight"    # I
    .param p8, "tag"    # Ljava/lang/Object;
    .param p9, "elementType"    # I

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/sec/android/app/ve/thread/SimpleTask;-><init>()V

    .line 31
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcherTask2;->mPos:I

    .line 36
    iput-object p1, p0, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcherTask2;->mKey:Ljava/lang/String;

    .line 37
    iput p2, p0, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcherTask2;->mPos:I

    .line 38
    iput p6, p0, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcherTask2;->mThumbWidth:I

    .line 39
    iput p7, p0, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcherTask2;->mThumbHeight:I

    .line 40
    iput-object p5, p0, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcherTask2;->mCallBack:Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcher$Adapter;

    .line 41
    iput-object p4, p0, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcherTask2;->mCache:Lcom/sec/android/app/ve/thumbcache/DiskLruCache;

    .line 42
    iput p9, p0, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcherTask2;->mElementType:I

    .line 43
    iput-object p3, p0, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcherTask2;->mTaskPool:Lcom/sec/android/app/ve/thread/AsyncTaskPool;

    .line 44
    iput-object p8, p0, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcherTask2;->mOptionalTag:Ljava/lang/Object;

    .line 45
    return-void
.end method

.method private prepareEmptyBitmap(II)Landroid/graphics/Bitmap;
    .locals 3
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 102
    sget-object v2, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {p1, p2, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 103
    .local v0, "bmp":Landroid/graphics/Bitmap;
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 104
    .local v1, "cnvs":Landroid/graphics/Canvas;
    const/high16 v2, -0x1000000

    invoke-virtual {v1, v2}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 106
    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "taskObj"    # Ljava/lang/Object;

    .prologue
    .line 95
    instance-of v0, p1, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcherTask2;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcherTask2;->mKey:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 96
    iget-object v0, p0, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcherTask2;->mKey:Ljava/lang/String;

    check-cast p1, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcherTask2;

    .end local p1    # "taskObj":Ljava/lang/Object;
    invoke-virtual {p1}, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcherTask2;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 98
    :goto_0
    return v0

    .restart local p1    # "taskObj":Ljava/lang/Object;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcherTask2;->mKey:Ljava/lang/String;

    return-object v0
.end method

.method protected getParam()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 49
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getPool()Lcom/sec/android/app/ve/thread/AsyncTaskPool;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcherTask2;->mTaskPool:Lcom/sec/android/app/ve/thread/AsyncTaskPool;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 89
    sget-boolean v0, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcherTask2;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "hashCode not designed"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 90
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method protected onBackgroundTask(Ljava/lang/Object;)Landroid/graphics/Bitmap;
    .locals 7
    .param p1, "param"    # Ljava/lang/Object;

    .prologue
    .line 54
    const/4 v6, 0x0

    .line 55
    .local v6, "bmp":Landroid/graphics/Bitmap;
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcherTask2;->mKey:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 56
    iget v0, p0, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcherTask2;->mElementType:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 57
    iget-object v0, p0, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcherTask2;->mKey:Ljava/lang/String;

    iget v1, p0, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcherTask2;->mThumbWidth:I

    iget v2, p0, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcherTask2;->mThumbHeight:I

    iget v3, p0, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcherTask2;->mElementType:I

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/app/ve/common/MediaUtils;->getThumbnail(Ljava/lang/String;III)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 61
    :goto_0
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcherTask2;->mKey:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 62
    if-nez v6, :cond_0

    .line 63
    iget v0, p0, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcherTask2;->mThumbWidth:I

    iget v1, p0, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcherTask2;->mThumbHeight:I

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcherTask2;->prepareEmptyBitmap(II)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 65
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcherTask2;->mKey:Ljava/lang/String;

    if-eqz v0, :cond_1

    if-eqz v6, :cond_1

    .line 66
    iget-object v0, p0, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcherTask2;->mCache:Lcom/sec/android/app/ve/thumbcache/DiskLruCache;

    iget-object v1, p0, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcherTask2;->mKey:Ljava/lang/String;

    invoke-virtual {v0, v1, v6}, Lcom/sec/android/app/ve/thumbcache/DiskLruCache;->putBitmap(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 70
    :cond_1
    return-object v6

    .line 59
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcherTask2;->mKey:Ljava/lang/String;

    iget v1, p0, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcherTask2;->mThumbWidth:I

    iget v2, p0, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcherTask2;->mThumbHeight:I

    const/high16 v3, -0x1000000

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/ve/common/MediaUtils;->getFitCenterBitmap(Ljava/lang/String;IIILandroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v6

    goto :goto_0
.end method

.method protected bridge synthetic onBackgroundTask(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcherTask2;->onBackgroundTask(Ljava/lang/Object;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected onUIThreadFinally(Ljava/lang/Object;)V
    .locals 4
    .param p1, "result"    # Ljava/lang/Object;

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcherTask2;->mCallBack:Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcher$Adapter;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    move-object v0, p1

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 76
    iget-object v0, p0, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcherTask2;->mCallBack:Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcher$Adapter;

    iget v1, p0, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcherTask2;->mPos:I

    iget-object v2, p0, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcherTask2;->mKey:Ljava/lang/String;

    check-cast p1, Landroid/graphics/Bitmap;

    .end local p1    # "result":Ljava/lang/Object;
    iget-object v3, p0, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcherTask2;->mOptionalTag:Ljava/lang/Object;

    invoke-interface {v0, v1, v2, p1, v3}, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcher$Adapter;->onBitmapCreated(ILjava/lang/String;Landroid/graphics/Bitmap;Ljava/lang/Object;)V

    .line 77
    :cond_0
    return-void
.end method
