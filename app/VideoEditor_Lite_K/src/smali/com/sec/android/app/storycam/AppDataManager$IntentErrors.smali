.class public final enum Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;
.super Ljava/lang/Enum;
.source "AppDataManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/storycam/AppDataManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "IntentErrors"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum ALL_FILES_SUPPORTED:Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;

.field public static final enum COUNT_EXCEEDS_LIMIT:Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;

.field public static final enum DURATION_EXCEEDS_LIMIT:Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;

.field private static final synthetic ENUM$VALUES:[Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;

.field public static final enum MIN_DURATION_NOT_REACHED:Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;

.field public static final enum NONE_SUPPORTED:Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;

.field public static final enum SOME_FILES_SUPPORTED:Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 689
    new-instance v0, Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;

    const-string v1, "ALL_FILES_SUPPORTED"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;->ALL_FILES_SUPPORTED:Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;

    .line 690
    new-instance v0, Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;

    const-string v1, "SOME_FILES_SUPPORTED"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;->SOME_FILES_SUPPORTED:Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;

    .line 691
    new-instance v0, Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;

    const-string v1, "NONE_SUPPORTED"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;->NONE_SUPPORTED:Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;

    .line 692
    new-instance v0, Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;

    const-string v1, "COUNT_EXCEEDS_LIMIT"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;->COUNT_EXCEEDS_LIMIT:Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;

    .line 693
    new-instance v0, Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;

    const-string v1, "MIN_DURATION_NOT_REACHED"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;->MIN_DURATION_NOT_REACHED:Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;

    .line 694
    new-instance v0, Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;

    const-string v1, "DURATION_EXCEEDS_LIMIT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;->DURATION_EXCEEDS_LIMIT:Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;

    .line 688
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;

    sget-object v1, Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;->ALL_FILES_SUPPORTED:Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;->SOME_FILES_SUPPORTED:Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;->NONE_SUPPORTED:Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;->COUNT_EXCEEDS_LIMIT:Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;->MIN_DURATION_NOT_REACHED:Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;->DURATION_EXCEEDS_LIMIT:Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;->ENUM$VALUES:[Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 688
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;->ENUM$VALUES:[Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;

    array-length v1, v0

    new-array v2, v1, [Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
