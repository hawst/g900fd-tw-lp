.class public Lcom/sec/android/app/ve/common/MediaUtils;
.super Ljava/lang/Object;
.source "MediaUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/ve/common/MediaUtils$FILE_CHECK_STATUS;
    }
.end annotation


# static fields
.field private static final IMAGE_RESOLUTION_HORIZONTAL_WIDE:I = 0xc

.field private static final IMAGE_RESOLUTION_SQUARE:I = 0xd

.field private static final IMAGE_RESOLUTION_VERTICAL_LONG:I = 0xb

.field private static final LANDSCAPE_PORTRAIT_RATIO:F = 0.5625f

.field private static final LANDSCAPE_PORTRAIT_RATIO_180:F = 1.777f

.field private static final MAX_3MPIMAGE_SIZE_ALLOWED:I = 0x2dc000

.field private static final MAX_5MPIMAGE_SIZE_ALLOWED:I = 0x4c2300

.field private static final MAX_IMAGE_SIZE_ALLOWED:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const v0, 0x4b9ae070    # 2.03E7f

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    sput v0, Lcom/sec/android/app/ve/common/MediaUtils;->MAX_IMAGE_SIZE_ALLOWED:I

    .line 50
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static checkIfMediaSupported(Ljava/lang/String;)I
    .locals 1
    .param p0, "filePath"    # Ljava/lang/String;

    .prologue
    .line 928
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/sec/android/app/ve/common/MediaUtils;->checkIfMediaSupported(Ljava/lang/String;Z)I

    move-result v0

    return v0
.end method

.method public static checkIfMediaSupported(Ljava/lang/String;Z)I
    .locals 6
    .param p0, "filePath"    # Ljava/lang/String;
    .param p1, "checkThumbnailAvailable"    # Z

    .prologue
    const-wide/16 v0, 0x0

    .line 932
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 933
    .local v2, "file":Ljava/io/File;
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 936
    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v4

    .line 938
    .local v4, "fileSize":J
    cmp-long v3, v4, v0

    if-lez v3, :cond_2

    .line 940
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v3

    invoke-virtual {v3, p0}, Lcom/samsung/app/video/editor/external/NativeInterface;->isJPG(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {p0, p1}, Lcom/sec/android/app/ve/common/MediaUtils;->checkVideoContent(Ljava/lang/String;Z)J

    move-result-wide v0

    .line 943
    .local v0, "duration":J
    :cond_0
    invoke-static {p0, v0, v1, v4, v5}, Lcom/sec/android/app/ve/common/MediaUtils;->isMediaSupportedByEngine(Ljava/lang/String;JJ)Z

    move-result v3

    if-nez v3, :cond_1

    .line 944
    sget v3, Lcom/sec/android/app/ve/common/MediaUtils$FILE_CHECK_STATUS;->NOT_SUPPORTED_BY_ENGINE:I

    .line 950
    .end local v0    # "duration":J
    .end local v4    # "fileSize":J
    :goto_0
    return v3

    .line 946
    .restart local v0    # "duration":J
    .restart local v4    # "fileSize":J
    :cond_1
    sget v3, Lcom/sec/android/app/ve/common/MediaUtils$FILE_CHECK_STATUS;->VALID_FILE:I

    goto :goto_0

    .line 950
    .end local v0    # "duration":J
    .end local v4    # "fileSize":J
    :cond_2
    sget v3, Lcom/sec/android/app/ve/common/MediaUtils$FILE_CHECK_STATUS;->INVALID_FILE:I

    goto :goto_0
.end method

.method public static checkVideoContent(Ljava/lang/String;)J
    .locals 2
    .param p0, "filePath"    # Ljava/lang/String;

    .prologue
    .line 730
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/sec/android/app/ve/common/MediaUtils;->checkVideoContent(Ljava/lang/String;Z)J

    move-result-wide v0

    return-wide v0
.end method

.method public static checkVideoContent(Ljava/lang/String;Z)J
    .locals 5
    .param p0, "filePath"    # Ljava/lang/String;
    .param p1, "checkThumbnailAvailable"    # Z

    .prologue
    .line 734
    new-instance v3, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v3}, Landroid/media/MediaMetadataRetriever;-><init>()V

    .line 735
    .local v3, "retriever":Landroid/media/MediaMetadataRetriever;
    const-wide/16 v0, 0x0

    .line 738
    .local v0, "duration":J
    :try_start_0
    invoke-virtual {v3, p0}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/lang/String;)V

    .line 739
    const/16 v4, 0x9

    invoke-virtual {v3, v4}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    int-to-long v0, v4

    .line 754
    :goto_0
    if-eqz v3, :cond_0

    .line 755
    invoke-virtual {v3}, Landroid/media/MediaMetadataRetriever;->release()V

    .line 757
    :cond_0
    return-wide v0

    .line 750
    :catch_0
    move-exception v2

    .line 751
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static createScaledBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Matrix$ScaleToFit;)Landroid/graphics/Bitmap;
    .locals 16
    .param p0, "srcBitmap"    # Landroid/graphics/Bitmap;
    .param p1, "outWidth"    # F
    .param p2, "outHeight"    # F
    .param p3, "scaleType"    # Landroid/graphics/Matrix$ScaleToFit;

    .prologue
    .line 351
    const/4 v2, 0x0

    cmpl-float v2, p1, v2

    if-lez v2, :cond_1

    const/4 v2, 0x0

    cmpl-float v2, p2, v2

    if-lez v2, :cond_1

    .line 353
    :try_start_0
    new-instance v11, Landroid/graphics/RectF;

    const/4 v2, 0x0

    const/4 v3, 0x0

    move/from16 v0, p1

    move/from16 v1, p2

    invoke-direct {v11, v2, v3, v0, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 354
    .local v11, "dstRect":Landroid/graphics/RectF;
    new-instance v14, Landroid/graphics/RectF;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    int-to-float v5, v5

    invoke-direct {v14, v2, v3, v4, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 355
    .local v14, "srcRect":Landroid/graphics/RectF;
    const/4 v13, 0x0

    .line 358
    .local v13, "lFinalBitmap":Landroid/graphics/Bitmap;
    new-instance v7, Landroid/graphics/Matrix;

    invoke-direct {v7}, Landroid/graphics/Matrix;-><init>()V

    .line 359
    .local v7, "resizeMatrix":Landroid/graphics/Matrix;
    move-object/from16 v0, p3

    invoke-virtual {v7, v14, v11, v0}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    .line 360
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    const/4 v8, 0x1

    move-object/from16 v2, p0

    invoke-static/range {v2 .. v8}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v13

    .line 363
    if-eqz v13, :cond_0

    move-object/from16 v0, p0

    invoke-virtual {v13, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 365
    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Bitmap;->recycle()V

    .line 366
    move-object/from16 p0, v13

    .line 367
    const/4 v13, 0x0

    .line 370
    :cond_0
    sget-object v2, Landroid/graphics/Matrix$ScaleToFit;->CENTER:Landroid/graphics/Matrix$ScaleToFit;

    move-object/from16 v0, p3

    if-ne v0, v2, :cond_1

    .line 379
    invoke-static/range {p1 .. p1}, Ljava/lang/Math;->round(F)I

    move-result v2

    invoke-static/range {p2 .. p2}, Ljava/lang/Math;->round(F)I

    move-result v3

    sget-object v4, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v10

    .line 380
    .local v10, "canvasBitmap":Landroid/graphics/Bitmap;
    new-instance v9, Landroid/graphics/Canvas;

    invoke-direct {v9, v10}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 381
    .local v9, "canvas":Landroid/graphics/Canvas;
    invoke-virtual {v9}, Landroid/graphics/Canvas;->save()I

    .line 382
    const/16 v2, 0x9

    new-array v15, v2, [F

    .line 383
    .local v15, "values":[F
    invoke-virtual {v7, v15}, Landroid/graphics/Matrix;->getValues([F)V

    .line 384
    const/4 v2, 0x2

    aget v2, v15, v2

    const/4 v3, 0x5

    aget v3, v15, v3

    invoke-virtual {v9, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 385
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v9, v0, v2, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 386
    invoke-virtual {v9}, Landroid/graphics/Canvas;->restore()V

    .line 388
    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 389
    move-object/from16 p0, v10

    .line 396
    .end local v7    # "resizeMatrix":Landroid/graphics/Matrix;
    .end local v9    # "canvas":Landroid/graphics/Canvas;
    .end local v10    # "canvasBitmap":Landroid/graphics/Bitmap;
    .end local v11    # "dstRect":Landroid/graphics/RectF;
    .end local v13    # "lFinalBitmap":Landroid/graphics/Bitmap;
    .end local v14    # "srcRect":Landroid/graphics/RectF;
    .end local v15    # "values":[F
    :cond_1
    :goto_0
    return-object p0

    .line 393
    :catch_0
    move-exception v12

    .line 394
    .local v12, "e":Ljava/lang/Exception;
    invoke-virtual {v12}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static getCenterCropBitmap(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;
    .locals 16
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;
    .param p1, "outWidth"    # I
    .param p2, "outHeight"    # I

    .prologue
    .line 513
    if-nez p0, :cond_0

    .line 514
    const/4 v5, 0x0

    .line 543
    :goto_0
    return-object v5

    .line 516
    :cond_0
    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v14

    int-to-float v13, v14

    .line 517
    .local v13, "width":F
    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v14

    int-to-float v6, v14

    .line 518
    .local v6, "height":F
    const/high16 v9, 0x3f800000    # 1.0f

    .line 520
    .local v9, "ratio":F
    move/from16 v0, p1

    int-to-float v14, v0

    div-float v14, v13, v14

    move/from16 v0, p2

    int-to-float v15, v0

    div-float v15, v6, v15

    cmpg-float v14, v14, v15

    if-gez v14, :cond_1

    .line 521
    move/from16 v0, p1

    int-to-float v14, v0

    div-float v9, v13, v14

    .line 525
    :goto_1
    move/from16 v0, p1

    int-to-float v14, v0

    mul-float/2addr v14, v9

    sub-float v14, v13, v14

    float-to-int v14, v14

    div-int/lit8 v7, v14, 0x2

    .line 526
    .local v7, "left":I
    move/from16 v0, p2

    int-to-float v14, v0

    mul-float/2addr v14, v9

    sub-float v14, v6, v14

    float-to-int v14, v14

    div-int/lit8 v12, v14, 0x2

    .line 527
    .local v12, "top":I
    move/from16 v0, p1

    int-to-float v14, v0

    mul-float/2addr v14, v9

    float-to-int v14, v14

    add-int v10, v7, v14

    .line 528
    .local v10, "right":I
    move/from16 v0, p2

    int-to-float v14, v0

    mul-float/2addr v14, v9

    float-to-int v14, v14

    add-int v2, v12, v14

    .line 530
    .local v2, "bottom":I
    new-instance v11, Landroid/graphics/Rect;

    invoke-direct {v11, v7, v12, v10, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 531
    .local v11, "src":Landroid/graphics/Rect;
    new-instance v4, Landroid/graphics/Rect;

    const/4 v14, 0x0

    const/4 v15, 0x0

    move/from16 v0, p1

    move/from16 v1, p2

    invoke-direct {v4, v14, v15, v0, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 533
    .local v4, "dst":Landroid/graphics/Rect;
    sget-object v14, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    move/from16 v0, p1

    move/from16 v1, p2

    invoke-static {v0, v1, v14}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 535
    .local v5, "finalBMP":Landroid/graphics/Bitmap;
    new-instance v8, Landroid/graphics/Paint;

    invoke-direct {v8}, Landroid/graphics/Paint;-><init>()V

    .line 536
    .local v8, "pt":Landroid/graphics/Paint;
    const/4 v14, 0x1

    invoke-virtual {v8, v14}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 537
    const/4 v14, 0x1

    invoke-virtual {v8, v14}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 539
    new-instance v3, Landroid/graphics/Canvas;

    invoke-direct {v3, v5}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 540
    .local v3, "cnvs":Landroid/graphics/Canvas;
    const/high16 v14, -0x1000000

    invoke-virtual {v3, v14}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 541
    move-object/from16 v0, p0

    invoke-virtual {v3, v0, v11, v4, v8}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_0

    .line 523
    .end local v2    # "bottom":I
    .end local v3    # "cnvs":Landroid/graphics/Canvas;
    .end local v4    # "dst":Landroid/graphics/Rect;
    .end local v5    # "finalBMP":Landroid/graphics/Bitmap;
    .end local v7    # "left":I
    .end local v8    # "pt":Landroid/graphics/Paint;
    .end local v10    # "right":I
    .end local v11    # "src":Landroid/graphics/Rect;
    .end local v12    # "top":I
    :cond_1
    move/from16 v0, p2

    int-to-float v14, v0

    div-float v9, v6, v14

    goto :goto_1
.end method

.method public static getDefaultKenburnRects(Lcom/sec/android/app/ve/common/KenburnsDefault$KenburnData;Ljava/lang/String;)Lcom/sec/android/app/ve/common/KenburnsDefault$KenburnData;
    .locals 1
    .param p0, "mKenburnData"    # Lcom/sec/android/app/ve/common/KenburnsDefault$KenburnData;
    .param p1, "mFilename"    # Ljava/lang/String;

    .prologue
    .line 953
    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Lcom/sec/android/app/ve/common/MediaUtils;->getDefaultKenburnRects(Lcom/sec/android/app/ve/common/KenburnsDefault$KenburnData;Ljava/lang/String;Z)Lcom/sec/android/app/ve/common/KenburnsDefault$KenburnData;

    move-result-object v0

    return-object v0
.end method

.method public static getDefaultKenburnRects(Lcom/sec/android/app/ve/common/KenburnsDefault$KenburnData;Ljava/lang/String;Z)Lcom/sec/android/app/ve/common/KenburnsDefault$KenburnData;
    .locals 44
    .param p0, "mKenburnData"    # Lcom/sec/android/app/ve/common/KenburnsDefault$KenburnData;
    .param p1, "mFilename"    # Ljava/lang/String;
    .param p2, "isSupported"    # Z

    .prologue
    .line 958
    const/16 v19, 0x0

    .line 959
    .local v19, "imageWidth":I
    const/16 v16, 0x0

    .line 962
    .local v16, "imageHeight":I
    const/16 v17, 0x0

    .line 965
    .local v17, "imageOrientation":I
    const/4 v6, 0x0

    .line 967
    .local v6, "bitmapHeight":I
    sget v31, Lcom/sec/android/app/ve/common/ConfigUtils;->PREVIEW_HEIGHT:I

    .line 968
    .local v31, "previewHeight":I
    sget v32, Lcom/sec/android/app/ve/common/ConfigUtils;->PREVIEW_WIDTH:I

    .line 971
    .local v32, "previewWidth":I
    const/16 v24, 0x0

    .line 974
    .local v24, "mFaceBitmap":Landroid/graphics/Bitmap;
    const/4 v12, 0x0

    .line 976
    .local v12, "faces":[Landroid/media/FaceDetector$Face;
    const/16 v25, 0x0

    .line 978
    .local v25, "mFaceCount":I
    const/4 v10, 0x0

    .line 979
    .local v10, "eyeDistance":F
    const/16 v27, 0x0

    .line 982
    .local v27, "maxEyeDistance":F
    new-instance v28, Landroid/graphics/PointF;

    invoke-direct/range {v28 .. v28}, Landroid/graphics/PointF;-><init>()V

    .line 986
    .local v28, "midPoint":Landroid/graphics/PointF;
    :try_start_0
    new-instance v21, Landroid/media/ExifInterface;

    move-object/from16 v0, v21

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V

    .line 987
    .local v21, "lExif":Landroid/media/ExifInterface;
    const-string v37, "ImageWidth"

    const/16 v38, 0x64

    move-object/from16 v0, v21

    move-object/from16 v1, v37

    move/from16 v2, v38

    invoke-virtual {v0, v1, v2}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    move-result v19

    .line 988
    const-string v37, "ImageLength"

    const/16 v38, 0x64

    move-object/from16 v0, v21

    move-object/from16 v1, v37

    move/from16 v2, v38

    invoke-virtual {v0, v1, v2}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    move-result v16

    .line 989
    const/16 v17, 0x1

    .line 990
    if-eqz v19, :cond_0

    if-nez v16, :cond_1

    .line 991
    :cond_0
    new-instance v29, Landroid/graphics/BitmapFactory$Options;

    invoke-direct/range {v29 .. v29}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 992
    .local v29, "options":Landroid/graphics/BitmapFactory$Options;
    const/16 v37, 0x1

    move/from16 v0, v37

    move-object/from16 v1, v29

    iput-boolean v0, v1, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 993
    move-object/from16 v0, p1

    move-object/from16 v1, v29

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 994
    move-object/from16 v0, v29

    iget v0, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    move/from16 v19, v0

    .line 995
    move-object/from16 v0, v29

    iget v0, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    move/from16 v16, v0

    .line 998
    .end local v29    # "options":Landroid/graphics/BitmapFactory$Options;
    :cond_1
    const-string v37, "Orientation"

    const/16 v38, 0x64

    move-object/from16 v0, v21

    move-object/from16 v1, v37

    move/from16 v2, v38

    invoke-virtual {v0, v1, v2}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    move-result v37

    const/16 v38, 0x6

    move/from16 v0, v37

    move/from16 v1, v38

    if-eq v0, v1, :cond_2

    .line 999
    const-string v37, "Orientation"

    const/16 v38, 0x64

    move-object/from16 v0, v21

    move-object/from16 v1, v37

    move/from16 v2, v38

    invoke-virtual {v0, v1, v2}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v37

    const/16 v38, 0x8

    move/from16 v0, v37

    move/from16 v1, v38

    if-ne v0, v1, :cond_b

    .line 1001
    :cond_2
    move/from16 v34, v19

    .line 1002
    .local v34, "temp":I
    move/from16 v19, v16

    .line 1003
    move/from16 v16, v34

    .line 1004
    const/16 v17, 0x6

    .line 1020
    .end local v21    # "lExif":Landroid/media/ExifInterface;
    .end local v34    # "temp":I
    :cond_3
    :goto_0
    const/16 v37, 0x6

    move/from16 v0, v17

    move/from16 v1, v37

    if-ne v0, v1, :cond_a

    .line 1022
    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v37, v0

    move/from16 v0, v31

    int-to-float v0, v0

    move/from16 v38, v0

    div-float v33, v37, v38

    .line 1024
    .local v33, "ratio":F
    move/from16 v0, v32

    int-to-float v0, v0

    move/from16 v37, v0

    mul-float v37, v37, v33

    move/from16 v0, v37

    float-to-int v5, v0

    .line 1025
    .local v5, "actWidth":I
    move/from16 v4, v16

    .line 1027
    .local v4, "actHeight":I
    const/16 v35, 0x0

    .line 1028
    .local v35, "topMargin":I
    const/4 v7, 0x0

    .line 1029
    .local v7, "bottomMargin":I
    sub-int v37, v5, v19

    div-int/lit8 v22, v37, 0x2

    .line 1032
    .local v22, "leftMargin":I
    const/16 v37, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v37

    move/from16 v2, p2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/ve/common/MediaUtils;->getImageBitmap(Ljava/lang/String;IZ)Landroid/graphics/Bitmap;

    move-result-object v20

    .line 1034
    .local v20, "lBitmap":Landroid/graphics/Bitmap;
    if-eqz v20, :cond_5

    .line 1035
    sget-object v37, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    const/16 v38, 0x1

    move-object/from16 v0, v20

    move-object/from16 v1, v37

    move/from16 v2, v38

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v24

    .line 1036
    if-eqz v24, :cond_4

    .line 1037
    new-instance v11, Landroid/media/FaceDetector;

    invoke-virtual/range {v24 .. v24}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v37

    .line 1038
    invoke-virtual/range {v24 .. v24}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v38

    const/16 v39, 0xa

    .line 1037
    move/from16 v0, v37

    move/from16 v1, v38

    move/from16 v2, v39

    invoke-direct {v11, v0, v1, v2}, Landroid/media/FaceDetector;-><init>(III)V

    .line 1039
    .local v11, "faceDetector":Landroid/media/FaceDetector;
    const/16 v37, 0xa

    move/from16 v0, v37

    new-array v12, v0, [Landroid/media/FaceDetector$Face;

    .line 1040
    move-object/from16 v0, v24

    invoke-virtual {v11, v0, v12}, Landroid/media/FaceDetector;->findFaces(Landroid/graphics/Bitmap;[Landroid/media/FaceDetector$Face;)I

    move-result v25

    .line 1043
    invoke-virtual/range {v24 .. v24}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    .line 1044
    invoke-virtual/range {v24 .. v24}, Landroid/graphics/Bitmap;->recycle()V

    .line 1048
    .end local v11    # "faceDetector":Landroid/media/FaceDetector;
    :cond_4
    const/4 v15, 0x0

    .local v15, "i":I
    :goto_1
    move/from16 v0, v25

    if-lt v15, v0, :cond_d

    .line 1059
    move-object/from16 v0, v28

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v37, v0

    const/16 v38, 0x0

    cmpl-float v37, v37, v38

    if-eqz v37, :cond_5

    .line 1060
    int-to-float v0, v4

    move/from16 v37, v0

    int-to-float v0, v6

    move/from16 v38, v0

    div-float v36, v37, v38

    .line 1061
    .local v36, "yRatio":F
    move-object/from16 v0, v28

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v37, v0

    mul-float v37, v37, v36

    move/from16 v0, v37

    move-object/from16 v1, v28

    iput v0, v1, Landroid/graphics/PointF;->y:F

    .line 1062
    move-object/from16 v0, v28

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v37, v0

    mul-float v38, v27, v36

    sub-float v37, v37, v38

    move/from16 v0, v37

    float-to-int v0, v0

    move/from16 v35, v0

    .line 1070
    .end local v15    # "i":I
    .end local v36    # "yRatio":F
    :cond_5
    div-int/lit8 v37, v31, 0x2

    move/from16 v0, v35

    move/from16 v1, v37

    if-le v0, v1, :cond_6

    move/from16 v0, v31

    move/from16 v1, v16

    if-lt v0, v1, :cond_7

    .line 1071
    :cond_6
    const/16 v35, 0x0

    .line 1075
    :cond_7
    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v37, v0

    move/from16 v0, v32

    int-to-float v0, v0

    move/from16 v38, v0

    div-float v30, v37, v38

    .line 1076
    .local v30, "prevToWidRatio":F
    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v37, v0

    div-float v37, v37, v30

    move/from16 v0, v37

    float-to-int v0, v0

    move/from16 v22, v0

    .line 1077
    move/from16 v19, v32

    .line 1078
    int-to-float v0, v5

    move/from16 v37, v0

    div-float v37, v37, v30

    move/from16 v0, v37

    float-to-int v5, v0

    .line 1079
    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v37, v0

    div-float v37, v37, v30

    move/from16 v0, v37

    float-to-int v0, v0

    move/from16 v16, v0

    .line 1080
    move/from16 v0, v35

    int-to-float v0, v0

    move/from16 v37, v0

    div-float v37, v37, v30

    move/from16 v0, v37

    float-to-int v0, v0

    move/from16 v35, v0

    .line 1083
    move/from16 v0, v35

    int-to-double v0, v0

    move-wide/from16 v38, v0

    move/from16 v0, v16

    int-to-double v0, v0

    move-wide/from16 v40, v0

    const-wide v42, 0x3fc999999999999aL    # 0.2

    mul-double v40, v40, v42

    add-double v38, v38, v40

    move-wide/from16 v0, v38

    double-to-int v7, v0

    .line 1087
    sub-int v37, v16, v35

    move/from16 v0, v37

    move/from16 v1, v31

    if-ge v0, v1, :cond_8

    .line 1088
    const/16 v35, 0x0

    .line 1090
    :cond_8
    sub-int v37, v16, v7

    move/from16 v0, v37

    move/from16 v1, v31

    if-ge v0, v1, :cond_9

    .line 1091
    sub-int v7, v16, v31

    .line 1095
    :cond_9
    int-to-float v0, v5

    move/from16 v37, v0

    move/from16 v0, v32

    int-to-float v0, v0

    move/from16 v38, v0

    div-float v18, v37, v38

    .line 1097
    .local v18, "imageScaleRatio":F
    const/16 v37, 0x9

    move/from16 v0, v37

    new-array v14, v0, [F

    const/16 v37, 0x0

    aput v18, v14, v37

    const/16 v37, 0x1

    const/16 v38, 0x0

    aput v38, v14, v37

    const/16 v37, 0x2

    move/from16 v0, v22

    neg-int v0, v0

    move/from16 v38, v0

    move/from16 v0, v38

    int-to-float v0, v0

    move/from16 v38, v0

    aput v38, v14, v37

    const/16 v37, 0x3

    const/16 v38, 0x0

    aput v38, v14, v37

    const/16 v37, 0x4

    aput v18, v14, v37

    const/16 v37, 0x5

    move/from16 v0, v35

    neg-int v0, v0

    move/from16 v38, v0

    move/from16 v0, v38

    int-to-float v0, v0

    move/from16 v38, v0

    aput v38, v14, v37

    const/16 v37, 0x6

    const/16 v38, 0x0

    aput v38, v14, v37

    const/16 v37, 0x7

    const/16 v38, 0x0

    aput v38, v14, v37

    const/16 v37, 0x8

    const/high16 v38, 0x3f800000    # 1.0f

    aput v38, v14, v37

    .line 1098
    .local v14, "fstart":[F
    const/16 v37, 0x9

    move/from16 v0, v37

    new-array v13, v0, [F

    const/16 v37, 0x0

    aput v18, v13, v37

    const/16 v37, 0x1

    const/16 v38, 0x0

    aput v38, v13, v37

    const/16 v37, 0x2

    move/from16 v0, v22

    neg-int v0, v0

    move/from16 v38, v0

    move/from16 v0, v38

    int-to-float v0, v0

    move/from16 v38, v0

    aput v38, v13, v37

    const/16 v37, 0x3

    const/16 v38, 0x0

    aput v38, v13, v37

    const/16 v37, 0x4

    aput v18, v13, v37

    const/16 v37, 0x5

    move/from16 v0, v16

    neg-int v0, v0

    move/from16 v38, v0

    add-int v38, v38, v31

    move/from16 v0, v38

    int-to-float v0, v0

    move/from16 v38, v0

    aput v38, v13, v37

    const/16 v37, 0x6

    const/16 v38, 0x0

    aput v38, v13, v37

    const/16 v37, 0x7

    const/16 v38, 0x0

    aput v38, v13, v37

    const/16 v37, 0x8

    const/high16 v38, 0x3f800000    # 1.0f

    aput v38, v13, v37

    .line 1100
    .local v13, "fend":[F
    new-instance v26, Landroid/graphics/Matrix;

    invoke-direct/range {v26 .. v26}, Landroid/graphics/Matrix;-><init>()V

    .line 1101
    .local v26, "mStart":Landroid/graphics/Matrix;
    move-object/from16 v0, v26

    invoke-virtual {v0, v14}, Landroid/graphics/Matrix;->setValues([F)V

    .line 1102
    new-instance v37, Landroid/graphics/Matrix;

    move-object/from16 v0, v37

    move-object/from16 v1, v26

    invoke-direct {v0, v1}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, Lcom/sec/android/app/ve/common/KenburnsDefault$KenburnData;->setStartMatrix(Landroid/graphics/Matrix;)V

    .line 1104
    new-instance v23, Landroid/graphics/Matrix;

    invoke-direct/range {v23 .. v23}, Landroid/graphics/Matrix;-><init>()V

    .line 1105
    .local v23, "mEnd":Landroid/graphics/Matrix;
    move-object/from16 v0, v23

    invoke-virtual {v0, v13}, Landroid/graphics/Matrix;->setValues([F)V

    .line 1106
    new-instance v37, Landroid/graphics/Matrix;

    move-object/from16 v0, v37

    move-object/from16 v1, v23

    invoke-direct {v0, v1}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, Lcom/sec/android/app/ve/common/KenburnsDefault$KenburnData;->setEndMatrix(Landroid/graphics/Matrix;)V

    .line 1108
    new-instance v37, Landroid/graphics/RectF;

    const/16 v38, 0x0

    const/16 v39, 0x0

    move/from16 v0, v32

    int-to-float v0, v0

    move/from16 v40, v0

    move/from16 v0, v31

    int-to-float v0, v0

    move/from16 v41, v0

    invoke-direct/range {v37 .. v41}, Landroid/graphics/RectF;-><init>(FFFF)V

    move-object/from16 v0, p0

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, Lcom/sec/android/app/ve/common/KenburnsDefault$KenburnData;->setRefRect(Landroid/graphics/RectF;)V

    .line 1109
    new-instance v37, Landroid/graphics/RectF;

    move/from16 v0, v22

    neg-int v0, v0

    move/from16 v38, v0

    move/from16 v0, v38

    int-to-float v0, v0

    move/from16 v38, v0

    move/from16 v0, v35

    neg-int v0, v0

    move/from16 v39, v0

    move/from16 v0, v39

    int-to-float v0, v0

    move/from16 v39, v0

    add-int v40, v19, v22

    move/from16 v0, v40

    int-to-float v0, v0

    move/from16 v40, v0

    sub-int v41, v16, v35

    move/from16 v0, v41

    int-to-float v0, v0

    move/from16 v41, v0

    invoke-direct/range {v37 .. v41}, Landroid/graphics/RectF;-><init>(FFFF)V

    move-object/from16 v0, p0

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, Lcom/sec/android/app/ve/common/KenburnsDefault$KenburnData;->setStartRect(Landroid/graphics/RectF;)V

    .line 1110
    new-instance v37, Landroid/graphics/RectF;

    move/from16 v0, v22

    neg-int v0, v0

    move/from16 v38, v0

    move/from16 v0, v38

    int-to-float v0, v0

    move/from16 v38, v0

    neg-int v0, v7

    move/from16 v39, v0

    move/from16 v0, v39

    int-to-float v0, v0

    move/from16 v39, v0

    add-int v40, v19, v22

    move/from16 v0, v40

    int-to-float v0, v0

    move/from16 v40, v0

    sub-int v41, v16, v7

    move/from16 v0, v41

    int-to-float v0, v0

    move/from16 v41, v0

    invoke-direct/range {v37 .. v41}, Landroid/graphics/RectF;-><init>(FFFF)V

    move-object/from16 v0, p0

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, Lcom/sec/android/app/ve/common/KenburnsDefault$KenburnData;->setEndRect(Landroid/graphics/RectF;)V

    .line 1113
    .end local v4    # "actHeight":I
    .end local v5    # "actWidth":I
    .end local v7    # "bottomMargin":I
    .end local v13    # "fend":[F
    .end local v14    # "fstart":[F
    .end local v18    # "imageScaleRatio":F
    .end local v20    # "lBitmap":Landroid/graphics/Bitmap;
    .end local v22    # "leftMargin":I
    .end local v23    # "mEnd":Landroid/graphics/Matrix;
    .end local v26    # "mStart":Landroid/graphics/Matrix;
    .end local v30    # "prevToWidRatio":F
    .end local v33    # "ratio":F
    .end local v35    # "topMargin":I
    :cond_a
    return-object p0

    .line 1005
    .restart local v21    # "lExif":Landroid/media/ExifInterface;
    :cond_b
    move/from16 v0, v19

    move/from16 v1, v16

    if-ge v0, v1, :cond_c

    .line 1006
    const/16 v17, 0x6

    .line 1007
    goto/16 :goto_0

    :cond_c
    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v37, v0

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v38, v0

    div-float v37, v37, v38

    move/from16 v0, v37

    float-to-double v0, v0

    move-wide/from16 v38, v0

    const-wide v40, 0x3ffb333333333333L    # 1.7

    cmpg-double v37, v38, v40

    if-gez v37, :cond_3

    .line 1008
    const/16 v17, 0x6

    goto/16 :goto_0

    .line 1016
    .end local v21    # "lExif":Landroid/media/ExifInterface;
    :catch_0
    move-exception v9

    .line 1017
    .local v9, "e":Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 1049
    .end local v9    # "e":Ljava/io/IOException;
    .restart local v4    # "actHeight":I
    .restart local v5    # "actWidth":I
    .restart local v7    # "bottomMargin":I
    .restart local v15    # "i":I
    .restart local v20    # "lBitmap":Landroid/graphics/Bitmap;
    .restart local v22    # "leftMargin":I
    .restart local v33    # "ratio":F
    .restart local v35    # "topMargin":I
    :cond_d
    aget-object v37, v12, v15

    invoke-virtual/range {v37 .. v37}, Landroid/media/FaceDetector$Face;->eyesDistance()F

    move-result v10

    .line 1050
    aget-object v37, v12, v15

    invoke-virtual/range {v37 .. v37}, Landroid/media/FaceDetector$Face;->confidence()F

    move-result v8

    .line 1051
    .local v8, "confidence":F
    cmpl-float v37, v10, v27

    if-ltz v37, :cond_e

    float-to-double v0, v8

    move-wide/from16 v38, v0

    const-wide v40, 0x3fe051eb851eb852L    # 0.51

    cmpl-double v37, v38, v40

    if-ltz v37, :cond_e

    .line 1054
    aget-object v37, v12, v15

    move-object/from16 v0, v37

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/media/FaceDetector$Face;->getMidPoint(Landroid/graphics/PointF;)V

    .line 1055
    move/from16 v27, v10

    .line 1048
    :cond_e
    add-int/lit8 v15, v15, 0x1

    goto/16 :goto_1
.end method

.method public static getFitCenterBitmap(Ljava/lang/String;IIILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    .locals 6
    .param p0, "filename"    # Ljava/lang/String;
    .param p1, "outWid"    # I
    .param p2, "outHei"    # I
    .param p3, "color"    # I
    .param p4, "config"    # Landroid/graphics/Bitmap$Config;

    .prologue
    .line 400
    const/4 v5, 0x1

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/ve/common/MediaUtils;->getFitCenterBitmap(Ljava/lang/String;IIILandroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public static getFitCenterBitmap(Ljava/lang/String;IIILandroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;
    .locals 21
    .param p0, "filename"    # Ljava/lang/String;
    .param p1, "outWid"    # I
    .param p2, "outHei"    # I
    .param p3, "color"    # I
    .param p4, "config"    # Landroid/graphics/Bitmap$Config;
    .param p5, "needOrientation"    # Z

    .prologue
    .line 405
    if-eqz p0, :cond_0

    if-lez p1, :cond_0

    if-lez p2, :cond_0

    if-nez p4, :cond_1

    .line 406
    :cond_0
    const/16 v17, 0x0

    .line 508
    :goto_0
    return-object v17

    .line 408
    :cond_1
    move/from16 v0, p1

    move/from16 v1, p2

    move-object/from16 v2, p4

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v17

    .line 409
    .local v17, "res":Landroid/graphics/Bitmap;
    new-instance v18, Landroid/graphics/Canvas;

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 410
    .local v18, "resCnvs":Landroid/graphics/Canvas;
    move-object/from16 v0, v18

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 413
    const/4 v6, 0x0

    .line 414
    .local v6, "width":I
    const/4 v7, 0x0

    .line 415
    .local v7, "height":I
    const/4 v13, 0x0

    .line 419
    .local v13, "orientation":I
    :try_start_0
    new-instance v11, Landroid/media/ExifInterface;

    move-object/from16 v0, p0

    invoke-direct {v11, v0}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V

    .line 420
    .local v11, "lExif":Landroid/media/ExifInterface;
    const-string v4, "ImageWidth"

    const/16 v5, 0x64

    invoke-virtual {v11, v4, v5}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    move-result v6

    .line 421
    const-string v4, "ImageLength"

    const/16 v5, 0x64

    invoke-virtual {v11, v4, v5}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    move-result v7

    .line 423
    const-string v4, "Orientation"

    const/16 v5, 0x64

    invoke-virtual {v11, v4, v5}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    move-result v4

    const/4 v5, 0x6

    if-ne v4, v5, :cond_2

    .line 424
    const-string v4, "ImageWidth"

    const/16 v5, 0x64

    invoke-virtual {v11, v4, v5}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    move-result v7

    .line 425
    const-string v4, "ImageLength"

    const/16 v5, 0x64

    invoke-virtual {v11, v4, v5}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    move-result v6

    .line 426
    const/4 v13, 0x6

    .line 429
    :cond_2
    const-string v4, "Orientation"

    const/16 v5, 0x64

    invoke-virtual {v11, v4, v5}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    move-result v4

    const/16 v5, 0x8

    if-ne v4, v5, :cond_3

    .line 430
    const-string v4, "ImageWidth"

    const/16 v5, 0x64

    invoke-virtual {v11, v4, v5}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    move-result v7

    .line 431
    const-string v4, "ImageLength"

    const/16 v5, 0x64

    invoke-virtual {v11, v4, v5}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    move-result v6

    .line 432
    const/16 v13, 0x8

    .line 434
    :cond_3
    const-string v4, "Orientation"

    const/16 v5, 0x64

    invoke-virtual {v11, v4, v5}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    move-result v4

    const/4 v5, 0x3

    if-ne v4, v5, :cond_4

    .line 435
    const-string v4, "ImageWidth"

    const/16 v5, 0x64

    invoke-virtual {v11, v4, v5}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    move-result v7

    .line 436
    const-string v4, "ImageLength"

    const/16 v5, 0x64

    invoke-virtual {v11, v4, v5}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v6

    .line 437
    const/4 v13, 0x3

    .line 443
    .end local v11    # "lExif":Landroid/media/ExifInterface;
    :cond_4
    :goto_1
    int-to-float v4, v6

    move/from16 v0, p1

    int-to-float v5, v0

    div-float v15, v4, v5

    .line 444
    .local v15, "ratio1":F
    int-to-float v4, v7

    move/from16 v0, p2

    int-to-float v5, v0

    div-float v16, v4, v5

    .line 445
    .local v16, "ratio2":F
    cmpl-float v4, v15, v16

    if-lez v4, :cond_c

    move/from16 v19, v15

    .line 447
    .local v19, "sampleSize":F
    :goto_2
    :try_start_1
    new-instance v12, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v12}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 448
    .local v12, "opt":Landroid/graphics/BitmapFactory$Options;
    const/high16 v4, 0x40000000    # 2.0f

    cmpl-float v4, v19, v4

    if-ltz v4, :cond_5

    move/from16 v0, v19

    float-to-int v4, v0

    iput v4, v12, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 450
    :cond_5
    move-object/from16 v0, p0

    invoke-static {v0, v12}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 451
    .local v3, "temp1":Landroid/graphics/Bitmap;
    if-eqz v3, :cond_6

    .line 452
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    .line 453
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 456
    :cond_6
    if-eqz p5, :cond_9

    .line 458
    const/4 v4, 0x6

    if-ne v13, v4, :cond_7

    .line 459
    new-instance v8, Landroid/graphics/Matrix;

    invoke-direct {v8}, Landroid/graphics/Matrix;-><init>()V

    .line 460
    .local v8, "matrix":Landroid/graphics/Matrix;
    const/high16 v4, 0x3f100000    # 0.5625f

    const/high16 v5, 0x3f100000    # 0.5625f

    invoke-virtual {v8, v4, v5}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 461
    const/high16 v4, 0x42b40000    # 90.0f

    invoke-virtual {v8, v4}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 462
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v9, 0x1

    invoke-static/range {v3 .. v9}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 465
    .end local v8    # "matrix":Landroid/graphics/Matrix;
    :cond_7
    const/4 v4, 0x3

    if-ne v13, v4, :cond_8

    .line 466
    new-instance v8, Landroid/graphics/Matrix;

    invoke-direct {v8}, Landroid/graphics/Matrix;-><init>()V

    .line 467
    .restart local v8    # "matrix":Landroid/graphics/Matrix;
    const v4, 0x3fe374bc    # 1.777f

    const v5, 0x3fe374bc    # 1.777f

    invoke-virtual {v8, v4, v5}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 468
    const/high16 v4, 0x43340000    # 180.0f

    invoke-virtual {v8, v4}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 469
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v9, 0x1

    invoke-static/range {v3 .. v9}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 471
    .end local v8    # "matrix":Landroid/graphics/Matrix;
    :cond_8
    const/16 v4, 0x8

    if-ne v13, v4, :cond_9

    .line 472
    new-instance v8, Landroid/graphics/Matrix;

    invoke-direct {v8}, Landroid/graphics/Matrix;-><init>()V

    .line 473
    .restart local v8    # "matrix":Landroid/graphics/Matrix;
    const/high16 v4, 0x3f100000    # 0.5625f

    const/high16 v5, 0x3f100000    # 0.5625f

    invoke-virtual {v8, v4, v5}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 474
    const/high16 v4, 0x43870000    # 270.0f

    invoke-virtual {v8, v4}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 475
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v9, 0x1

    invoke-static/range {v3 .. v9}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 479
    .end local v8    # "matrix":Landroid/graphics/Matrix;
    :cond_9
    if-eqz v3, :cond_a

    .line 480
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    .line 481
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 484
    :cond_a
    int-to-float v4, v6

    move/from16 v0, p1

    int-to-float v5, v0

    div-float v15, v4, v5

    .line 485
    int-to-float v4, v7

    move/from16 v0, p2

    int-to-float v5, v0

    div-float v16, v4, v5

    .line 486
    cmpl-float v4, v15, v16

    if-lez v4, :cond_d

    move/from16 v19, v15

    .line 488
    :goto_3
    int-to-float v4, v6

    div-float v4, v4, v19

    float-to-int v4, v4

    int-to-float v5, v7

    div-float v5, v5, v19

    float-to-int v5, v5

    const/4 v9, 0x1

    invoke-static {v3, v4, v5, v9}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v20

    .line 489
    .local v20, "temp2":Landroid/graphics/Bitmap;
    if-eqz v3, :cond_b

    if-eqz v20, :cond_b

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_b

    .line 490
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->recycle()V

    .line 494
    :cond_b
    invoke-virtual/range {v20 .. v20}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    .line 495
    invoke-virtual/range {v20 .. v20}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 497
    new-instance v14, Landroid/graphics/Paint;

    invoke-direct {v14}, Landroid/graphics/Paint;-><init>()V

    .line 498
    .local v14, "pt":Landroid/graphics/Paint;
    const/4 v4, 0x1

    invoke-virtual {v14, v4}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 499
    const/4 v4, 0x1

    invoke-virtual {v14, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 500
    const/4 v4, 0x1

    invoke-virtual {v14, v4}, Landroid/graphics/Paint;->setDither(Z)V

    .line 501
    move/from16 v0, p1

    int-to-float v4, v0

    int-to-float v5, v6

    sub-float/2addr v4, v5

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    move/from16 v0, p2

    int-to-float v5, v0

    int-to-float v9, v7

    sub-float/2addr v5, v9

    const/high16 v9, 0x40000000    # 2.0f

    div-float/2addr v5, v9

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v4, v5, v14}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 502
    invoke-virtual/range {v20 .. v20}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 504
    .end local v3    # "temp1":Landroid/graphics/Bitmap;
    .end local v12    # "opt":Landroid/graphics/BitmapFactory$Options;
    .end local v14    # "pt":Landroid/graphics/Paint;
    .end local v15    # "ratio1":F
    .end local v16    # "ratio2":F
    .end local v19    # "sampleSize":F
    .end local v20    # "temp2":Landroid/graphics/Bitmap;
    :catch_0
    move-exception v10

    .line 505
    .local v10, "e":Ljava/lang/Exception;
    invoke-virtual {v10}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 439
    .end local v10    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v10

    .line 440
    .local v10, "e":Ljava/io/IOException;
    :try_start_2
    invoke-virtual {v10}, Ljava/io/IOException;->printStackTrace()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_1

    .end local v10    # "e":Ljava/io/IOException;
    .restart local v15    # "ratio1":F
    .restart local v16    # "ratio2":F
    :cond_c
    move/from16 v19, v16

    .line 445
    goto/16 :goto_2

    .restart local v3    # "temp1":Landroid/graphics/Bitmap;
    .restart local v12    # "opt":Landroid/graphics/BitmapFactory$Options;
    .restart local v19    # "sampleSize":F
    :cond_d
    move/from16 v19, v16

    .line 486
    goto :goto_3
.end method

.method public static getImageBitmap(Ljava/lang/String;I)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "filePath"    # Ljava/lang/String;
    .param p1, "kind"    # I

    .prologue
    .line 230
    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Lcom/sec/android/app/ve/common/MediaUtils;->getImageBitmap(Ljava/lang/String;IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public static getImageBitmap(Ljava/lang/String;IFF)Landroid/graphics/Bitmap;
    .locals 8
    .param p0, "filePath"    # Ljava/lang/String;
    .param p1, "kind"    # I
    .param p2, "outWidth"    # F
    .param p3, "outHeight"    # F

    .prologue
    const/4 v7, 0x0

    .line 195
    const/4 v0, 0x0

    .line 200
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    :try_start_0
    invoke-static {p0, p1}, Landroid/media/ThumbnailUtils;->createImageThumbnail(Ljava/lang/String;I)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    move-result-object v0

    .line 212
    :goto_0
    if-eqz v0, :cond_1

    cmpl-float v5, p2, v7

    if-lez v5, :cond_1

    cmpl-float v5, p3, v7

    if-lez v5, :cond_1

    .line 214
    const/4 v5, 0x2

    :try_start_1
    invoke-static {p0, v0, p2, p3, v5}, Lcom/sec/android/app/ve/common/MediaUtils;->scaleBitmapToFrame(Ljava/lang/String;Landroid/graphics/Bitmap;FFI)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 215
    .local v3, "lFinalBitmap":Landroid/graphics/Bitmap;
    if-eqz v3, :cond_0

    invoke-virtual {v3, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 216
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 217
    :cond_0
    move-object v0, v3

    .line 227
    .end local v3    # "lFinalBitmap":Landroid/graphics/Bitmap;
    :cond_1
    :goto_1
    return-object v0

    .line 201
    :catch_0
    move-exception v1

    .line 202
    .local v1, "e":Ljava/lang/OutOfMemoryError;
    new-instance v4, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v4}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 203
    .local v4, "mBitmapOptions":Landroid/graphics/BitmapFactory$Options;
    invoke-static {p0}, Lcom/sec/android/app/ve/common/MediaUtils;->getImageSampleSize(Ljava/lang/String;)I

    move-result v5

    iput v5, v4, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    .line 206
    :try_start_2
    invoke-static {p0, v4}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    move-result-object v0

    goto :goto_0

    .line 207
    :catch_1
    move-exception v2

    .line 208
    .local v2, "e1":Ljava/lang/OutOfMemoryError;
    :try_start_3
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Got out of memory Utils getImageBitmap decode file with options"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/app/ve/common/LogUtils;->log(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/OutOfMemoryError; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    goto :goto_0

    .line 220
    .end local v1    # "e":Ljava/lang/OutOfMemoryError;
    .end local v2    # "e1":Ljava/lang/OutOfMemoryError;
    .end local v4    # "mBitmapOptions":Landroid/graphics/BitmapFactory$Options;
    :catch_2
    move-exception v1

    .line 221
    .restart local v1    # "e":Ljava/lang/OutOfMemoryError;
    const-string v5, "Got Outofmemory error while creating Bitmap"

    invoke-static {v5}, Lcom/sec/android/app/ve/common/LogUtils;->log(Ljava/lang/String;)V

    goto :goto_1

    .line 223
    .end local v1    # "e":Ljava/lang/OutOfMemoryError;
    :catch_3
    move-exception v1

    .line 224
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public static getImageBitmap(Ljava/lang/String;IZ)Landroid/graphics/Bitmap;
    .locals 13
    .param p0, "filePath"    # Ljava/lang/String;
    .param p1, "kind"    # I
    .param p2, "isSupported"    # Z

    .prologue
    .line 237
    const/4 v6, 0x0

    .line 240
    .local v6, "bitmap":Landroid/graphics/Bitmap;
    if-eqz p2, :cond_0

    .line 241
    :try_start_0
    invoke-static {p0, p1}, Landroid/media/ThumbnailUtils;->createImageThumbnail(Ljava/lang/String;I)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 267
    :goto_0
    if-eqz v6, :cond_2

    .line 268
    const/4 v0, 0x2

    invoke-static {p0, v0}, Lcom/sec/android/app/ve/common/MediaUtils;->getRotateDegree(Ljava/lang/String;I)I

    move-result v12

    .line 269
    .local v12, "orientation":I
    invoke-static {v6, v12}, Lcom/sec/android/app/ve/common/MediaUtils;->getRotatedBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 275
    .end local v12    # "orientation":I
    :goto_1
    return-object v0

    .line 245
    :cond_0
    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 246
    .local v1, "imagesUri":Landroid/net/Uri;
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "_id"

    aput-object v3, v2, v0

    .line 248
    .local v2, "projection":[Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v3, "_data LIKE ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p0, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 249
    .local v8, "cursor":Landroid/database/Cursor;
    if-nez v8, :cond_1

    .line 250
    sget-object v1, Landroid/provider/MediaStore$Images$Media;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 251
    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v3, "_data LIKE ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p0, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 252
    if-nez v8, :cond_1

    .line 253
    const/4 v0, 0x0

    goto :goto_1

    .line 256
    :cond_1
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 258
    const/4 v0, 0x0

    aget-object v0, v2, v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    .line 259
    .local v7, "columnIndex":I
    invoke-interface {v8, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 261
    .local v10, "imageId":J
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 264
    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-static {v0, v10, v11, v3, v4}, Landroid/provider/MediaStore$Images$Thumbnails;->getThumbnail(Landroid/content/ContentResolver;JILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    goto :goto_0

    .line 272
    .end local v1    # "imagesUri":Landroid/net/Uri;
    .end local v2    # "projection":[Ljava/lang/String;
    .end local v7    # "columnIndex":I
    .end local v8    # "cursor":Landroid/database/Cursor;
    .end local v10    # "imageId":J
    :catch_0
    move-exception v9

    .line 273
    .local v9, "e":Ljava/lang/Exception;
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V

    .line 275
    .end local v9    # "e":Ljava/lang/Exception;
    :cond_2
    :goto_2
    const/4 v0, 0x0

    goto :goto_1

    .line 271
    :catch_1
    move-exception v0

    goto :goto_2
.end method

.method public static getImageFilePathByUri(Landroid/net/Uri;)Ljava/lang/String;
    .locals 11
    .param p0, "uri"    # Landroid/net/Uri;

    .prologue
    .line 899
    const/4 v9, 0x0

    .line 900
    .local v9, "lFilepath":Ljava/lang/String;
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "_data"

    aput-object v1, v2, v0

    .line 902
    .local v2, "cols":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 905
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 907
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 908
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 910
    const-string v0, "_data"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v9

    .line 919
    :goto_0
    if-eqz v6, :cond_0

    .line 920
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_0
    move-object v10, v9

    .line 924
    .end local v9    # "lFilepath":Ljava/lang/String;
    .local v10, "lFilepath":Ljava/lang/String;
    :goto_1
    return-object v10

    .line 912
    .end local v10    # "lFilepath":Ljava/lang/String;
    .restart local v9    # "lFilepath":Ljava/lang/String;
    :cond_1
    :try_start_1
    const-string v0, "getImageFilePathByUri curson is null"

    invoke-static {v0}, Lcom/sec/android/app/ve/common/LogUtils;->log(Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 914
    :catch_0
    move-exception v7

    .line 919
    .local v7, "e":Landroid/database/sqlite/SQLiteException;
    if-eqz v6, :cond_2

    .line 920
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    move-object v10, v9

    .line 915
    .end local v9    # "lFilepath":Ljava/lang/String;
    .restart local v10    # "lFilepath":Ljava/lang/String;
    goto :goto_1

    .line 916
    .end local v7    # "e":Landroid/database/sqlite/SQLiteException;
    .end local v10    # "lFilepath":Ljava/lang/String;
    .restart local v9    # "lFilepath":Ljava/lang/String;
    :catch_1
    move-exception v8

    .line 919
    .local v8, "ex":Ljava/lang/UnsupportedOperationException;
    if-eqz v6, :cond_3

    .line 920
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    move-object v10, v9

    .line 917
    .end local v9    # "lFilepath":Ljava/lang/String;
    .restart local v10    # "lFilepath":Ljava/lang/String;
    goto :goto_1

    .line 918
    .end local v8    # "ex":Ljava/lang/UnsupportedOperationException;
    .end local v10    # "lFilepath":Ljava/lang/String;
    .restart local v9    # "lFilepath":Ljava/lang/String;
    :catchall_0
    move-exception v0

    .line 919
    if-eqz v6, :cond_4

    .line 920
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 922
    :cond_4
    throw v0
.end method

.method public static getImageSampleSize(Ljava/lang/String;)I
    .locals 7
    .param p0, "aFilepath"    # Ljava/lang/String;

    .prologue
    .line 81
    const/4 v4, 0x2

    .line 84
    .local v4, "sampling":I
    :try_start_0
    new-instance v1, Landroid/media/ExifInterface;

    invoke-direct {v1, p0}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V

    .line 86
    .local v1, "lExif":Landroid/media/ExifInterface;
    const-string v5, "ImageWidth"

    const/16 v6, 0x64

    invoke-virtual {v1, v5, v6}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    move-result v3

    .line 88
    .local v3, "lWidth":I
    const-string v5, "ImageLength"

    const/16 v6, 0x64

    invoke-virtual {v1, v5, v6}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    move-result v2

    .line 90
    .local v2, "lHeight":I
    mul-int v5, v3, v2

    sget v6, Lcom/sec/android/app/ve/common/MediaUtils;->MAX_IMAGE_SIZE_ALLOWED:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-lt v5, v6, :cond_1

    .line 91
    const/16 v4, 0xc

    .line 102
    .end local v1    # "lExif":Landroid/media/ExifInterface;
    .end local v2    # "lHeight":I
    .end local v3    # "lWidth":I
    :cond_0
    :goto_0
    return v4

    .line 92
    .restart local v1    # "lExif":Landroid/media/ExifInterface;
    .restart local v2    # "lHeight":I
    .restart local v3    # "lWidth":I
    :cond_1
    mul-int v5, v3, v2

    const v6, 0x4c2300

    if-lt v5, v6, :cond_2

    .line 93
    const/16 v4, 0x8

    .line 94
    goto :goto_0

    :cond_2
    mul-int v5, v3, v2

    const v6, 0x2dc000

    if-lt v5, v6, :cond_0

    .line 95
    const/4 v4, 0x4

    goto :goto_0

    .line 97
    .end local v1    # "lExif":Landroid/media/ExifInterface;
    .end local v2    # "lHeight":I
    .end local v3    # "lWidth":I
    :catch_0
    move-exception v0

    .line 98
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static getRetrieverForSource(Landroid/content/res/AssetManager;Ljava/lang/String;Z)Landroid/media/MediaMetadataRetriever;
    .locals 10
    .param p0, "am"    # Landroid/content/res/AssetManager;
    .param p1, "Uri"    # Ljava/lang/String;
    .param p2, "isAssetResource"    # Z

    .prologue
    .line 671
    new-instance v0, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v0}, Landroid/media/MediaMetadataRetriever;-><init>()V

    .line 672
    .local v0, "retriever":Landroid/media/MediaMetadataRetriever;
    const/4 v8, 0x0

    .line 673
    .local v8, "fis":Ljava/io/FileInputStream;
    const/4 v6, 0x0

    .line 675
    .local v6, "afd":Landroid/content/res/AssetFileDescriptor;
    if-eqz p2, :cond_2

    if-eqz p0, :cond_2

    .line 677
    :try_start_0
    invoke-virtual {p0, p1}, Landroid/content/res/AssetManager;->openFd(Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;

    move-result-object v6

    .line 678
    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v1

    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->getStartOffset()J

    move-result-wide v2

    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->getLength()J

    move-result-wide v4

    invoke-virtual/range {v0 .. v5}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/io/FileDescriptor;JJ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 691
    :goto_0
    if-eqz v8, :cond_0

    .line 693
    :try_start_1
    invoke-virtual {v8}, Ljava/io/FileInputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_5

    .line 698
    :cond_0
    :goto_1
    if-eqz v6, :cond_1

    .line 700
    :try_start_2
    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_6

    .line 708
    :cond_1
    :goto_2
    return-object v0

    .line 682
    :cond_2
    :try_start_3
    new-instance v9, Ljava/io/FileInputStream;

    invoke-direct {v9, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 683
    .end local v8    # "fis":Ljava/io/FileInputStream;
    .local v9, "fis":Ljava/io/FileInputStream;
    :try_start_4
    invoke-virtual {v9}, Ljava/io/FileInputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/io/FileDescriptor;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_7
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-object v8, v9

    .line 686
    .end local v9    # "fis":Ljava/io/FileInputStream;
    .restart local v8    # "fis":Ljava/io/FileInputStream;
    goto :goto_0

    :catch_0
    move-exception v7

    .line 687
    .local v7, "e":Ljava/lang/Exception;
    :goto_3
    :try_start_5
    invoke-static {v0}, Lcom/sec/android/app/ve/common/MediaUtils;->releaseRetriever(Landroid/media/MediaMetadataRetriever;)V

    .line 688
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 691
    if-eqz v8, :cond_3

    .line 693
    :try_start_6
    invoke-virtual {v8}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    .line 698
    .end local v7    # "e":Ljava/lang/Exception;
    :cond_3
    :goto_4
    if-eqz v6, :cond_1

    .line 700
    :try_start_7
    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1

    goto :goto_2

    .line 701
    :catch_1
    move-exception v7

    .line 702
    .local v7, "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 694
    .local v7, "e":Ljava/lang/Exception;
    :catch_2
    move-exception v7

    .line 695
    .local v7, "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 690
    .end local v7    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v1

    .line 691
    :goto_5
    if-eqz v8, :cond_4

    .line 693
    :try_start_8
    invoke-virtual {v8}, Ljava/io/FileInputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3

    .line 698
    :cond_4
    :goto_6
    if-eqz v6, :cond_5

    .line 700
    :try_start_9
    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_4

    .line 706
    :cond_5
    :goto_7
    throw v1

    .line 694
    :catch_3
    move-exception v7

    .line 695
    .restart local v7    # "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 701
    .end local v7    # "e":Ljava/io/IOException;
    :catch_4
    move-exception v7

    .line 702
    .restart local v7    # "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_7

    .line 694
    .end local v7    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v7

    .line 695
    .restart local v7    # "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 701
    .end local v7    # "e":Ljava/io/IOException;
    :catch_6
    move-exception v7

    .line 702
    .restart local v7    # "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 690
    .end local v7    # "e":Ljava/io/IOException;
    .end local v8    # "fis":Ljava/io/FileInputStream;
    .restart local v9    # "fis":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v1

    move-object v8, v9

    .end local v9    # "fis":Ljava/io/FileInputStream;
    .restart local v8    # "fis":Ljava/io/FileInputStream;
    goto :goto_5

    .line 686
    .end local v8    # "fis":Ljava/io/FileInputStream;
    .restart local v9    # "fis":Ljava/io/FileInputStream;
    :catch_7
    move-exception v7

    move-object v8, v9

    .end local v9    # "fis":Ljava/io/FileInputStream;
    .restart local v8    # "fis":Ljava/io/FileInputStream;
    goto :goto_3
.end method

.method public static getRotateDegree(Ljava/lang/String;I)I
    .locals 12
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "elementType"    # I

    .prologue
    const/4 v11, 0x0

    .line 547
    const/4 v10, 0x0

    .line 548
    .local v10, "rotation":I
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    move v0, v10

    .line 597
    :goto_0
    return v0

    .line 553
    :cond_1
    const/4 v6, 0x0

    .line 556
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v0, 0x2

    if-ne p1, v0, :cond_5

    .line 557
    :try_start_0
    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 558
    .local v1, "imagesUri":Landroid/net/Uri;
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "orientation"

    aput-object v3, v2, v0

    .line 560
    .local v2, "projection":[Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v3, "_data LIKE ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p0, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 561
    if-nez v6, :cond_3

    .line 562
    sget-object v1, Landroid/provider/MediaStore$Images$Media;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 563
    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v3, "_data LIKE ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p0, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v6

    .line 564
    if-nez v6, :cond_3

    .line 592
    if-eqz v6, :cond_2

    .line 593
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    move v0, v11

    .line 565
    goto :goto_0

    .line 568
    :cond_3
    :try_start_1
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_5

    .line 569
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 570
    const-string v0, "orientation"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v10

    .line 592
    if-eqz v6, :cond_4

    .line 593
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_4
    move v0, v10

    .line 571
    goto :goto_0

    .line 575
    .end local v1    # "imagesUri":Landroid/net/Uri;
    .end local v2    # "projection":[Ljava/lang/String;
    :cond_5
    :try_start_2
    new-instance v8, Landroid/media/ExifInterface;

    invoke-direct {v8, p0}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V

    .line 577
    .local v8, "exif":Landroid/media/ExifInterface;
    const-string v0, "Orientation"

    .line 578
    const/4 v3, 0x1

    .line 577
    invoke-virtual {v8, v0, v3}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v9

    .line 580
    .local v9, "exifOrientation":I
    const/4 v0, 0x6

    if-ne v9, v0, :cond_8

    .line 581
    const/16 v10, 0x5a

    .line 592
    :cond_6
    :goto_1
    if-eqz v6, :cond_7

    .line 593
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .end local v8    # "exif":Landroid/media/ExifInterface;
    .end local v9    # "exifOrientation":I
    :cond_7
    :goto_2
    move v0, v10

    .line 597
    goto :goto_0

    .line 582
    .restart local v8    # "exif":Landroid/media/ExifInterface;
    .restart local v9    # "exifOrientation":I
    :cond_8
    const/4 v0, 0x3

    if-ne v9, v0, :cond_9

    .line 583
    const/16 v10, 0xb4

    .line 584
    goto :goto_1

    :cond_9
    const/16 v0, 0x8

    if-ne v9, v0, :cond_6

    .line 585
    const/16 v10, 0x10e

    goto :goto_1

    .line 588
    .end local v8    # "exif":Landroid/media/ExifInterface;
    .end local v9    # "exifOrientation":I
    :catch_0
    move-exception v7

    .line 589
    .local v7, "e":Ljava/io/IOException;
    :try_start_3
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 592
    if-eqz v6, :cond_7

    .line 593
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_2

    .line 591
    .end local v7    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v0

    .line 592
    if-eqz v6, :cond_a

    .line 593
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 594
    :cond_a
    throw v0
.end method

.method public static getRotatedBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    .locals 8
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;
    .param p1, "rotation"    # I

    .prologue
    const/4 v1, 0x0

    .line 279
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    move-object v7, p0

    .line 295
    :goto_0
    return-object v7

    .line 283
    :cond_1
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 284
    .local v5, "mtx":Landroid/graphics/Matrix;
    int-to-float v0, p1

    invoke-virtual {v5, v0}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 286
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    const/4 v6, 0x1

    move-object v0, p0

    move v2, v1

    .line 285
    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 288
    .local v7, "lFinalBitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v7, p0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 289
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0

    .line 292
    :cond_2
    invoke-virtual {v7}, Landroid/graphics/Bitmap;->recycle()V

    .line 293
    move-object v7, p0

    goto :goto_0
.end method

.method public static getThumbnail(Ljava/lang/String;III)Landroid/graphics/Bitmap;
    .locals 11
    .param p0, "filePath"    # Ljava/lang/String;
    .param p1, "outWidth"    # I
    .param p2, "outHeight"    # I
    .param p3, "elemType"    # I

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 107
    if-nez p0, :cond_1

    move-object v0, v1

    .line 161
    :cond_0
    :goto_0
    return-object v0

    .line 110
    :cond_1
    const/4 v0, 0x0

    .line 114
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-ne v2, p3, :cond_3

    .line 115
    const/4 v2, 0x1

    :try_start_0
    invoke-static {p0, v2}, Landroid/media/ThumbnailUtils;->createVideoThumbnail(Ljava/lang/String;I)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 128
    :cond_2
    :goto_1
    if-nez v0, :cond_4

    move-object v0, v1

    .line 129
    goto :goto_0

    .line 116
    :cond_3
    const/4 v2, 0x2

    if-ne v2, p3, :cond_2

    .line 118
    const/4 v2, 0x1

    :try_start_1
    invoke-static {p0, v2}, Landroid/media/ThumbnailUtils;->createImageThumbnail(Ljava/lang/String;I)Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto :goto_1

    .line 119
    :catch_0
    move-exception v8

    .line 120
    .local v8, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V

    .line 121
    new-instance v9, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v9}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 122
    .local v9, "mBitmapOptions":Landroid/graphics/BitmapFactory$Options;
    invoke-static {p0}, Lcom/sec/android/app/ve/common/MediaUtils;->getImageSampleSize(Ljava/lang/String;)I

    move-result v2

    iput v2, v9, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 124
    invoke-static {p0, v9}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_1

    .line 131
    .end local v8    # "e":Ljava/lang/Exception;
    .end local v9    # "mBitmapOptions":Landroid/graphics/BitmapFactory$Options;
    :cond_4
    invoke-static {p0, p3}, Lcom/sec/android/app/ve/common/MediaUtils;->getRotateDegree(Ljava/lang/String;I)I

    move-result v7

    .line 133
    .local v7, "angle":I
    if-eqz v7, :cond_6

    .line 134
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 135
    .local v5, "m":Landroid/graphics/Matrix;
    int-to-float v1, v7

    invoke-virtual {v5, v1}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 137
    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    const/4 v6, 0x1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v10

    .line 139
    .local v10, "temp":Landroid/graphics/Bitmap;
    if-eqz v10, :cond_5

    invoke-virtual {v10, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 140
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 142
    :cond_5
    move-object v0, v10

    .line 146
    .end local v5    # "m":Landroid/graphics/Matrix;
    .end local v10    # "temp":Landroid/graphics/Bitmap;
    :cond_6
    if-eqz v0, :cond_0

    if-lez p1, :cond_0

    if-lez p2, :cond_0

    .line 148
    invoke-static {v0, p1, p2}, Lcom/sec/android/app/ve/common/MediaUtils;->getCenterCropBitmap(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v10

    .line 150
    .restart local v10    # "temp":Landroid/graphics/Bitmap;
    if-eqz v10, :cond_7

    invoke-virtual {v10, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 151
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 153
    :cond_7
    move-object v0, v10

    .line 154
    goto :goto_0

    .line 157
    .end local v7    # "angle":I
    .end local v10    # "temp":Landroid/graphics/Bitmap;
    :catch_1
    move-exception v8

    .line 158
    .restart local v8    # "e":Ljava/lang/Exception;
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static getVideoDuration(ZLandroid/content/res/AssetManager;Landroid/media/MediaMetadataRetriever;Ljava/lang/String;)I
    .locals 12
    .param p0, "isAssetResource"    # Z
    .param p1, "assetManager"    # Landroid/content/res/AssetManager;
    .param p2, "retriever"    # Landroid/media/MediaMetadataRetriever;
    .param p3, "Uri"    # Ljava/lang/String;

    .prologue
    .line 776
    const/4 v7, 0x0

    .line 777
    .local v7, "duration":I
    if-nez p2, :cond_1

    .line 778
    new-instance p2, Landroid/media/MediaMetadataRetriever;

    .end local p2    # "retriever":Landroid/media/MediaMetadataRetriever;
    invoke-direct {p2}, Landroid/media/MediaMetadataRetriever;-><init>()V

    .line 779
    .restart local p2    # "retriever":Landroid/media/MediaMetadataRetriever;
    const/4 v10, 0x0

    .line 780
    .local v10, "fis":Ljava/io/FileInputStream;
    const/4 v6, 0x0

    .line 782
    .local v6, "afd":Landroid/content/res/AssetFileDescriptor;
    if-eqz p0, :cond_2

    .line 783
    :try_start_0
    invoke-virtual {p1, p3}, Landroid/content/res/AssetManager;->openFd(Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;

    move-result-object v6

    .line 784
    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v1

    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->getStartOffset()J

    move-result-wide v2

    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->getLength()J

    move-result-wide v4

    move-object v0, p2

    invoke-virtual/range {v0 .. v5}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/io/FileDescriptor;JJ)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_6
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_9
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 800
    :goto_0
    if-eqz v10, :cond_0

    .line 802
    :try_start_1
    invoke-virtual {v10}, Ljava/io/FileInputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_e

    .line 807
    :cond_0
    :goto_1
    if-eqz v6, :cond_1

    .line 809
    :try_start_2
    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_f

    .line 816
    .end local v6    # "afd":Landroid/content/res/AssetFileDescriptor;
    .end local v10    # "fis":Ljava/io/FileInputStream;
    :cond_1
    :goto_2
    if-eqz p2, :cond_9

    .line 819
    const/16 v0, 0x9

    .line 818
    :try_start_3
    invoke-virtual {p2, v0}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    .line 820
    invoke-virtual {p2}, Landroid/media/MediaMetadataRetriever;->release()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_10

    move v8, v7

    .line 827
    .end local v7    # "duration":I
    .local v8, "duration":I
    :goto_3
    return v8

    .line 787
    .end local v8    # "duration":I
    .restart local v6    # "afd":Landroid/content/res/AssetFileDescriptor;
    .restart local v7    # "duration":I
    .restart local v10    # "fis":Ljava/io/FileInputStream;
    :cond_2
    :try_start_4
    new-instance v11, Ljava/io/FileInputStream;

    invoke-direct {v11, p3}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_6
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_9
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 788
    .end local v10    # "fis":Ljava/io/FileInputStream;
    .local v11, "fis":Ljava/io/FileInputStream;
    :try_start_5
    invoke-virtual {v11}, Ljava/io/FileInputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/io/FileDescriptor;)V
    :try_end_5
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_5} :catch_14
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_13
    .catch Ljava/lang/IllegalArgumentException; {:try_start_5 .. :try_end_5} :catch_12
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_11
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-object v10, v11

    .line 790
    .end local v11    # "fis":Ljava/io/FileInputStream;
    .restart local v10    # "fis":Ljava/io/FileInputStream;
    goto :goto_0

    :catch_0
    move-exception v9

    .line 791
    .local v9, "e":Ljava/io/FileNotFoundException;
    :goto_4
    :try_start_6
    invoke-virtual {v9}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 800
    if-eqz v10, :cond_3

    .line 802
    :try_start_7
    invoke-virtual {v10}, Ljava/io/FileInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2

    .line 807
    .end local v9    # "e":Ljava/io/FileNotFoundException;
    :cond_3
    :goto_5
    if-eqz v6, :cond_1

    .line 809
    :try_start_8
    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1

    goto :goto_2

    .line 810
    :catch_1
    move-exception v9

    .line 811
    .local v9, "e":Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 803
    .local v9, "e":Ljava/io/FileNotFoundException;
    :catch_2
    move-exception v9

    .line 804
    .local v9, "e":Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 792
    .end local v9    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v9

    .line 793
    .restart local v9    # "e":Ljava/io/IOException;
    :goto_6
    :try_start_9
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 800
    if-eqz v10, :cond_4

    .line 802
    :try_start_a
    invoke-virtual {v10}, Ljava/io/FileInputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_5

    .line 807
    :cond_4
    :goto_7
    if-eqz v6, :cond_1

    .line 809
    :try_start_b
    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_4

    goto :goto_2

    .line 810
    :catch_4
    move-exception v9

    .line 811
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 803
    :catch_5
    move-exception v9

    .line 804
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_7

    .line 794
    .end local v9    # "e":Ljava/io/IOException;
    :catch_6
    move-exception v9

    .line 795
    .local v9, "e":Ljava/lang/IllegalArgumentException;
    :goto_8
    :try_start_c
    invoke-virtual {v9}, Ljava/lang/IllegalArgumentException;->printStackTrace()V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    .line 800
    if-eqz v10, :cond_5

    .line 802
    :try_start_d
    invoke-virtual {v10}, Ljava/io/FileInputStream;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_8

    .line 807
    .end local v9    # "e":Ljava/lang/IllegalArgumentException;
    :cond_5
    :goto_9
    if-eqz v6, :cond_1

    .line 809
    :try_start_e
    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_7

    goto :goto_2

    .line 810
    :catch_7
    move-exception v9

    .line 811
    .local v9, "e":Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 803
    .local v9, "e":Ljava/lang/IllegalArgumentException;
    :catch_8
    move-exception v9

    .line 804
    .local v9, "e":Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_9

    .line 796
    .end local v9    # "e":Ljava/io/IOException;
    :catch_9
    move-exception v9

    .line 797
    .local v9, "e":Ljava/lang/Exception;
    :goto_a
    :try_start_f
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    .line 800
    if-eqz v10, :cond_6

    .line 802
    :try_start_10
    invoke-virtual {v10}, Ljava/io/FileInputStream;->close()V
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_b

    .line 807
    .end local v9    # "e":Ljava/lang/Exception;
    :cond_6
    :goto_b
    if-eqz v6, :cond_1

    .line 809
    :try_start_11
    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_11
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_a

    goto :goto_2

    .line 810
    :catch_a
    move-exception v9

    .line 811
    .local v9, "e":Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 803
    .local v9, "e":Ljava/lang/Exception;
    :catch_b
    move-exception v9

    .line 804
    .local v9, "e":Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_b

    .line 799
    .end local v9    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v0

    .line 800
    :goto_c
    if-eqz v10, :cond_7

    .line 802
    :try_start_12
    invoke-virtual {v10}, Ljava/io/FileInputStream;->close()V
    :try_end_12
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_12} :catch_c

    .line 807
    :cond_7
    :goto_d
    if-eqz v6, :cond_8

    .line 809
    :try_start_13
    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_13
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_13} :catch_d

    .line 814
    :cond_8
    :goto_e
    throw v0

    .line 803
    :catch_c
    move-exception v9

    .line 804
    .restart local v9    # "e":Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_d

    .line 810
    .end local v9    # "e":Ljava/io/IOException;
    :catch_d
    move-exception v9

    .line 811
    .restart local v9    # "e":Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_e

    .line 803
    .end local v9    # "e":Ljava/io/IOException;
    :catch_e
    move-exception v9

    .line 804
    .restart local v9    # "e":Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_1

    .line 810
    .end local v9    # "e":Ljava/io/IOException;
    :catch_f
    move-exception v9

    .line 811
    .restart local v9    # "e":Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_2

    .line 822
    .end local v6    # "afd":Landroid/content/res/AssetFileDescriptor;
    .end local v9    # "e":Ljava/io/IOException;
    .end local v10    # "fis":Ljava/io/FileInputStream;
    :catch_10
    move-exception v9

    .line 823
    .local v9, "e":Ljava/lang/Exception;
    invoke-static {p2}, Lcom/sec/android/app/ve/common/MediaUtils;->releaseRetriever(Landroid/media/MediaMetadataRetriever;)V

    .line 824
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V

    .end local v9    # "e":Ljava/lang/Exception;
    :cond_9
    move v8, v7

    .line 827
    .end local v7    # "duration":I
    .restart local v8    # "duration":I
    goto/16 :goto_3

    .line 799
    .end local v8    # "duration":I
    .restart local v6    # "afd":Landroid/content/res/AssetFileDescriptor;
    .restart local v7    # "duration":I
    .restart local v11    # "fis":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v0

    move-object v10, v11

    .end local v11    # "fis":Ljava/io/FileInputStream;
    .restart local v10    # "fis":Ljava/io/FileInputStream;
    goto :goto_c

    .line 796
    .end local v10    # "fis":Ljava/io/FileInputStream;
    .restart local v11    # "fis":Ljava/io/FileInputStream;
    :catch_11
    move-exception v9

    move-object v10, v11

    .end local v11    # "fis":Ljava/io/FileInputStream;
    .restart local v10    # "fis":Ljava/io/FileInputStream;
    goto :goto_a

    .line 794
    .end local v10    # "fis":Ljava/io/FileInputStream;
    .restart local v11    # "fis":Ljava/io/FileInputStream;
    :catch_12
    move-exception v9

    move-object v10, v11

    .end local v11    # "fis":Ljava/io/FileInputStream;
    .restart local v10    # "fis":Ljava/io/FileInputStream;
    goto :goto_8

    .line 792
    .end local v10    # "fis":Ljava/io/FileInputStream;
    .restart local v11    # "fis":Ljava/io/FileInputStream;
    :catch_13
    move-exception v9

    move-object v10, v11

    .end local v11    # "fis":Ljava/io/FileInputStream;
    .restart local v10    # "fis":Ljava/io/FileInputStream;
    goto/16 :goto_6

    .line 790
    .end local v10    # "fis":Ljava/io/FileInputStream;
    .restart local v11    # "fis":Ljava/io/FileInputStream;
    :catch_14
    move-exception v9

    move-object v10, v11

    .end local v11    # "fis":Ljava/io/FileInputStream;
    .restart local v10    # "fis":Ljava/io/FileInputStream;
    goto/16 :goto_4
.end method

.method public static getVideoFileInfoByUri(Landroid/net/Uri;)Landroid/database/Cursor;
    .locals 10
    .param p0, "uri"    # Landroid/net/Uri;

    .prologue
    .line 881
    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    .line 882
    const-string v1, "_data"

    aput-object v1, v2, v0

    const/4 v0, 0x1

    const-string v1, "duration"

    aput-object v1, v2, v0

    .line 885
    .local v2, "cols":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 888
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v6

    move-object v7, v6

    .line 895
    .end local v6    # "c":Landroid/database/Cursor;
    .local v7, "c":Landroid/database/Cursor;
    :goto_0
    return-object v7

    .line 889
    .end local v7    # "c":Landroid/database/Cursor;
    .restart local v6    # "c":Landroid/database/Cursor;
    :catch_0
    move-exception v8

    .local v8, "e":Landroid/database/sqlite/SQLiteException;
    move-object v7, v6

    .line 890
    .end local v6    # "c":Landroid/database/Cursor;
    .restart local v7    # "c":Landroid/database/Cursor;
    goto :goto_0

    .line 891
    .end local v7    # "c":Landroid/database/Cursor;
    .end local v8    # "e":Landroid/database/sqlite/SQLiteException;
    .restart local v6    # "c":Landroid/database/Cursor;
    :catch_1
    move-exception v9

    .local v9, "ex":Ljava/lang/Exception;
    move-object v7, v6

    .line 892
    .end local v6    # "c":Landroid/database/Cursor;
    .restart local v7    # "c":Landroid/database/Cursor;
    goto :goto_0
.end method

.method public static getVideoFrame(Landroid/media/MediaMetadataRetriever;F)Landroid/graphics/Bitmap;
    .locals 5
    .param p0, "retriever"    # Landroid/media/MediaMetadataRetriever;
    .param p1, "time"    # F

    .prologue
    .line 715
    .line 716
    const/16 v4, 0x9

    .line 715
    :try_start_0
    invoke-virtual {p0, v4}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 718
    .local v0, "duration":I
    int-to-float v4, v0

    cmpl-float v4, p1, v4

    if-lez v4, :cond_0

    add-int/lit8 v4, v0, -0x64

    int-to-float p1, v4

    .line 719
    :cond_0
    const/high16 v4, 0x447a0000    # 1000.0f

    mul-float/2addr v4, p1

    float-to-long v2, v4

    .line 721
    .local v2, "microSecs":J
    invoke-virtual {p0, v2, v3}, Landroid/media/MediaMetadataRetriever;->getFrameAtTime(J)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 726
    .end local v0    # "duration":I
    .end local v2    # "microSecs":J
    :goto_0
    return-object v4

    .line 722
    :catch_0
    move-exception v1

    .line 723
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 726
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public static getVideoThumbnail(Ljava/lang/String;IFF)Landroid/graphics/Bitmap;
    .locals 9
    .param p0, "filePath"    # Ljava/lang/String;
    .param p1, "kind"    # I
    .param p2, "outWidth"    # F
    .param p3, "outHeight"    # F

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 166
    const/4 v0, 0x0

    .line 168
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    const/4 v5, 0x1

    :try_start_0
    invoke-static {p0, v5}, Landroid/media/ThumbnailUtils;->createVideoThumbnail(Ljava/lang/String;I)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 181
    :goto_0
    if-eqz v0, :cond_1

    cmpl-float v5, p2, v7

    if-lez v5, :cond_1

    cmpl-float v5, p3, v7

    if-lez v5, :cond_1

    .line 183
    invoke-static {p0, v0, p2, p3, v8}, Lcom/sec/android/app/ve/common/MediaUtils;->scaleBitmapToFrame(Ljava/lang/String;Landroid/graphics/Bitmap;FFI)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 185
    .local v3, "lFinalBitmap":Landroid/graphics/Bitmap;
    if-eqz v3, :cond_0

    invoke-virtual {v3, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 186
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 188
    :cond_0
    move-object v0, v3

    .line 191
    .end local v3    # "lFinalBitmap":Landroid/graphics/Bitmap;
    :cond_1
    return-object v0

    .line 169
    :catch_0
    move-exception v1

    .line 171
    .local v1, "e":Ljava/lang/OutOfMemoryError;
    new-instance v4, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v4}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 172
    .local v4, "mBitmapOptions":Landroid/graphics/BitmapFactory$Options;
    const/16 v5, 0x8

    iput v5, v4, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 175
    :try_start_1
    invoke-static {p0, v4}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    goto :goto_0

    .line 176
    :catch_1
    move-exception v2

    .line 177
    .local v2, "e1":Ljava/lang/OutOfMemoryError;
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Got out of memory loadVideoThumbs decodeFile"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/app/ve/common/LogUtils;->log(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static isFullHD(Landroid/media/MediaMetadataRetriever;)Z
    .locals 4
    .param p0, "retriever"    # Landroid/media/MediaMetadataRetriever;

    .prologue
    const/4 v2, 0x0

    .line 762
    .line 763
    const/16 v3, 0x12

    .line 762
    :try_start_0
    invoke-virtual {p0, v3}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 764
    .local v1, "width":I
    const/16 v3, 0x500

    if-le v1, v3, :cond_0

    .line 765
    const/4 v2, 0x1

    .line 771
    .end local v1    # "width":I
    :cond_0
    :goto_0
    return v2

    .line 768
    :catch_0
    move-exception v0

    .line 769
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static isImageLarger13MP(Ljava/lang/String;)Z
    .locals 12
    .param p0, "aFilepath"    # Ljava/lang/String;

    .prologue
    .line 60
    const/4 v1, 0x0

    .line 61
    .local v1, "isLarge":Z
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    const-string v10, ".png"

    invoke-virtual {v3, v10}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v3, v1

    .line 77
    :goto_0
    return v3

    .line 65
    :cond_0
    :try_start_0
    new-instance v2, Landroid/media/ExifInterface;

    invoke-direct {v2, p0}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V

    .line 66
    .local v2, "lExif":Landroid/media/ExifInterface;
    const-string v3, "ImageWidth"

    sget v10, Lcom/sec/android/app/ve/common/MediaUtils;->MAX_IMAGE_SIZE_ALLOWED:I

    invoke-virtual {v2, v3, v10}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    move-result v3

    int-to-long v6, v3

    .line 67
    .local v6, "lWidth":J
    const-string v3, "ImageLength"

    sget v10, Lcom/sec/android/app/ve/common/MediaUtils;->MAX_IMAGE_SIZE_ALLOWED:I

    invoke-virtual {v2, v3, v10}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    move-result v3

    int-to-long v4, v3

    .line 68
    .local v4, "lHeight":J
    mul-long v8, v6, v4

    .line 69
    .local v8, "size":J
    const-wide/16 v10, 0x0

    cmp-long v3, v8, v10

    if-eqz v3, :cond_1

    sget v3, Lcom/sec/android/app/ve/common/MediaUtils;->MAX_IMAGE_SIZE_ALLOWED:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    int-to-long v10, v3

    cmp-long v3, v8, v10

    if-gtz v3, :cond_1

    .line 70
    const/4 v1, 0x0

    :cond_1
    move v3, v1

    .line 77
    goto :goto_0

    .line 72
    .end local v2    # "lExif":Landroid/media/ExifInterface;
    .end local v4    # "lHeight":J
    .end local v6    # "lWidth":J
    .end local v8    # "size":J
    :catch_0
    move-exception v0

    .line 73
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 74
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public static isMediaNotMounted(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 843
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v3

    .line 844
    const-string v4, "mounted"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 846
    const-string v3, ""

    invoke-static {p0, v3, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 847
    .local v0, "storageToast":Landroid/widget/Toast;
    invoke-virtual {v0, v1}, Landroid/widget/Toast;->setDuration(I)V

    .line 848
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 853
    .end local v0    # "storageToast":Landroid/widget/Toast;
    :goto_0
    return v1

    :cond_0
    move v1, v2

    goto :goto_0
.end method

.method public static isMediaScannerScanning(Landroid/content/Context;)Z
    .locals 10
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 857
    const/4 v7, 0x0

    .line 860
    .local v7, "result":Z
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 861
    invoke-static {}, Landroid/provider/MediaStore;->getMediaScannerUri()Landroid/net/Uri;

    move-result-object v1

    .line 862
    new-array v2, v9, [Ljava/lang/String;

    const-string v4, "volume"

    aput-object v4, v2, v8

    move-object v4, v3

    move-object v5, v3

    .line 861
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 865
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_1

    .line 866
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-ne v0, v9, :cond_0

    .line 867
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 869
    const-string v0, "external"

    invoke-interface {v6, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 870
    const-string v0, "internal"

    invoke-interface {v6, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    move v7, v8

    .line 873
    :cond_0
    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 876
    :cond_1
    return v7

    :cond_2
    move v7, v9

    .line 869
    goto :goto_0
.end method

.method private static isMediaSupportedByEngine(Ljava/lang/String;JJ)Z
    .locals 9
    .param p0, "filePath"    # Ljava/lang/String;
    .param p1, "duration"    # J
    .param p3, "fileSize"    # J

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 608
    const-wide/16 v6, 0x0

    cmp-long v6, v6, p1

    if-nez v6, :cond_1

    .line 611
    const-wide/32 v6, 0x3200000

    cmp-long v6, p3, v6

    if-gez v6, :cond_7

    .line 612
    invoke-static {p0}, Lcom/sec/android/app/ve/common/MediaUtils;->isImageLarger13MP(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_7

    .line 613
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v6

    invoke-virtual {v6, p0}, Lcom/samsung/app/video/editor/external/NativeInterface;->isJPG(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 651
    :cond_0
    :goto_0
    return v4

    .line 621
    :cond_1
    const-string v6, "ro.product.device"

    invoke-static {v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 622
    .local v2, "modelName":Ljava/lang/String;
    if-eqz v2, :cond_2

    const-string v6, "m2"

    invoke-virtual {v2, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 623
    :cond_2
    new-instance v3, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v3}, Landroid/media/MediaMetadataRetriever;-><init>()V

    .line 625
    .local v3, "retriever":Landroid/media/MediaMetadataRetriever;
    :try_start_0
    invoke-virtual {v3, p0}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/lang/String;)V

    .line 626
    invoke-static {v3}, Lcom/sec/android/app/ve/util/CommonUtils;->isUHD(Landroid/media/MediaMetadataRetriever;)Z

    move-result v6

    if-nez v6, :cond_3

    invoke-static {v3}, Lcom/sec/android/app/ve/util/CommonUtils;->isWQHD(Landroid/media/MediaMetadataRetriever;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 627
    :cond_3
    if-eqz v3, :cond_4

    .line 628
    invoke-virtual {v3}, Landroid/media/MediaMetadataRetriever;->release()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_4
    move v4, v5

    .line 629
    goto :goto_0

    .line 632
    :catch_0
    move-exception v0

    .line 633
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 635
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_5
    if-eqz v3, :cond_6

    .line 636
    invoke-virtual {v3}, Landroid/media/MediaMetadataRetriever;->release()V

    .line 638
    .end local v3    # "retriever":Landroid/media/MediaMetadataRetriever;
    :cond_6
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    .line 640
    .local v1, "lowerCapsFilePath":Ljava/lang/String;
    sget-wide v6, Lcom/sec/android/app/ve/common/ConfigUtils;->MAX_SUPPORTED_VIDEO_FILE_SIZE:J

    cmp-long v6, p3, v6

    if-gez v6, :cond_7

    .line 641
    sget v6, Lcom/sec/android/app/ve/common/ConfigUtils;->STORYBOARD_TIME_LIMIT:I

    int-to-long v6, v6

    cmp-long v6, p1, v6

    if-gez v6, :cond_7

    .line 642
    const-string v6, "jpg"

    invoke-virtual {v1, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_7

    const-string v6, "jpeg"

    invoke-virtual {v1, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_7

    const-string v6, "png"

    invoke-virtual {v1, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_7

    .line 644
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v6

    invoke-virtual {v6, p0}, Lcom/samsung/app/video/editor/external/NativeInterface;->GetCodecFilePropertiesFromEngine(Ljava/lang/String;)I

    move-result v6

    if-gez v6, :cond_0

    .end local v1    # "lowerCapsFilePath":Ljava/lang/String;
    .end local v2    # "modelName":Ljava/lang/String;
    :cond_7
    move v4, v5

    .line 651
    goto :goto_0
.end method

.method public static releaseRetriever(Landroid/media/MediaMetadataRetriever;)V
    .locals 1
    .param p0, "retriever"    # Landroid/media/MediaMetadataRetriever;

    .prologue
    .line 832
    if-nez p0, :cond_0

    .line 840
    :goto_0
    return-void

    .line 836
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Landroid/media/MediaMetadataRetriever;->release()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 837
    :catch_0
    move-exception v0

    .line 838
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static resizeBitmap(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;
    .locals 10
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;
    .param p1, "newWidth"    # I
    .param p2, "newHeight"    # I

    .prologue
    const/4 v1, 0x0

    .line 655
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 656
    .local v3, "width":I
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    .line 657
    .local v4, "height":I
    int-to-float v0, p1

    int-to-float v2, v3

    div-float v9, v0, v2

    .line 658
    .local v9, "scaleWidth":F
    int-to-float v0, p2

    int-to-float v2, v4

    div-float v8, v0, v2

    .line 659
    .local v8, "scaleHeight":F
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 661
    .local v5, "matrix":Landroid/graphics/Matrix;
    invoke-virtual {v5, v9, v8}, Landroid/graphics/Matrix;->postScale(FF)Z

    move-object v0, p0

    move v2, v1

    move v6, v1

    .line 663
    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 666
    .local v7, "resizeBitmap":Landroid/graphics/Bitmap;
    return-object v7
.end method

.method public static scaleBitmapToFrame(Ljava/lang/String;Landroid/graphics/Bitmap;FFI)Landroid/graphics/Bitmap;
    .locals 11
    .param p0, "filePath"    # Ljava/lang/String;
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "outWidth"    # F
    .param p3, "outHeight"    # F
    .param p4, "elemType"    # I

    .prologue
    const/4 v1, 0x0

    .line 300
    if-nez p1, :cond_0

    .line 301
    const/4 v0, 0x0

    .line 339
    :goto_0
    return-object v0

    .line 303
    :cond_0
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 305
    .local v5, "mtx":Landroid/graphics/Matrix;
    const/high16 v9, 0x3f800000    # 1.0f

    .line 306
    .local v9, "scale":F
    const/4 v7, 0x0

    .line 308
    .local v7, "angle":F
    if-eqz p0, :cond_1

    .line 310
    invoke-static {p0, p4}, Lcom/sec/android/app/ve/common/MediaUtils;->getRotateDegree(Ljava/lang/String;I)I

    move-result v0

    int-to-float v7, v0

    .line 313
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    int-to-float v10, v0

    .line 314
    .local v10, "width":F
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    int-to-float v8, v0

    .line 316
    .local v8, "height":F
    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v0

    if-eqz v0, :cond_4

    .line 318
    invoke-virtual {v5, v7}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 320
    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v0

    const/16 v2, 0xb4

    if-eq v0, v2, :cond_2

    .line 323
    div-float v0, v8, v10

    div-float v2, p2, p3

    cmpl-float v0, v0, v2

    if-lez v0, :cond_3

    .line 324
    div-float v9, v10, p3

    .line 337
    :cond_2
    :goto_1
    invoke-virtual {v5, v9, v9}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 339
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    const/4 v6, 0x1

    move-object v0, p1

    move v2, v1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 326
    :cond_3
    div-float v9, v8, p2

    .line 328
    goto :goto_1

    .line 331
    :cond_4
    div-float v0, v10, v8

    div-float v2, p2, p3

    cmpl-float v0, v0, v2

    if-lez v0, :cond_5

    .line 332
    div-float v9, v8, p3

    goto :goto_1

    .line 334
    :cond_5
    div-float v9, v10, p2

    goto :goto_1
.end method
