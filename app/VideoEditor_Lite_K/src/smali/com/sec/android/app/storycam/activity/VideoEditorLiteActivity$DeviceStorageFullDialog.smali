.class public Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DeviceStorageFullDialog;
.super Lcom/sec/android/app/ve/common/AndroidUtils$DlgFragment;
.source "VideoEditorLiteActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DeviceStorageFullDialog"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2684
    invoke-direct {p0}, Lcom/sec/android/app/ve/common/AndroidUtils$DlgFragment;-><init>()V

    return-void
.end method

.method public static newInstance(II)Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DeviceStorageFullDialog;
    .locals 3
    .param p0, "sizeInMB"    # I
    .param p1, "expResIndex"    # I

    .prologue
    .line 2689
    new-instance v1, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DeviceStorageFullDialog;

    invoke-direct {v1}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DeviceStorageFullDialog;-><init>()V

    .line 2690
    .local v1, "frag":Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DeviceStorageFullDialog;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2691
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "sizeInMB"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2692
    const-string v2, "expResIndex"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2693
    invoke-virtual {v1, v0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DeviceStorageFullDialog;->setArguments(Landroid/os/Bundle;)V

    .line 2694
    return-object v1
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 2728
    invoke-super {p0, p1}, Lcom/sec/android/app/ve/common/AndroidUtils$DlgFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 2729
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DeviceStorageFullDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DeviceStorageFullDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p1, Landroid/content/res/Configuration;->fontScale:F

    # getter for: Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->fontScale_value:F
    invoke-static {}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->access$7()F

    move-result v1

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    .line 2730
    iget v0, p1, Landroid/content/res/Configuration;->fontScale:F

    invoke-static {v0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->access$8(F)V

    .line 2731
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DeviceStorageFullDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/ve/common/AndroidUtils;->dismissDialog(Landroid/app/Activity;)V

    .line 2734
    :cond_0
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 2698
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DeviceStorageFullDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "sizeInMB"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 2699
    .local v3, "sizeInMB":I
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DeviceStorageFullDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "expResIndex"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 2700
    .local v1, "expResIndex":I
    const v4, 0x7f07007b

    invoke-static {v4}, Lcom/sec/android/app/ve/VEApp;->getStringValue(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2702
    .local v2, "message":Ljava/lang/String;
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DeviceStorageFullDialog;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-direct {v0, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 2704
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    const v4, 0x7f070283

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    .line 2705
    invoke-virtual {v4, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    .line 2706
    const v5, 0x7f0700c7

    new-instance v6, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DeviceStorageFullDialog$1;

    invoke-direct {v6, p0, v1}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DeviceStorageFullDialog$1;-><init>(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DeviceStorageFullDialog;I)V

    invoke-virtual {v4, v5, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    .line 2714
    const v5, 0x7f07005e

    new-instance v6, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DeviceStorageFullDialog$2;

    invoke-direct {v6, p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DeviceStorageFullDialog$2;-><init>(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DeviceStorageFullDialog;)V

    invoke-virtual {v4, v5, v6}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 2723
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v4

    return-object v4
.end method
