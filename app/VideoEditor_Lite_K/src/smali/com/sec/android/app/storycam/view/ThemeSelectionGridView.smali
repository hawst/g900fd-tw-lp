.class public Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;
.super Landroid/widget/GridView;
.source "ThemeSelectionGridView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/storycam/view/ThemeSelectionGridView$ThemeSelectionAdapter;
    }
.end annotation


# instance fields
.field private clickListener:Landroid/view/View$OnClickListener;

.field private isM2Model:Z

.field private mAdapter:Lcom/sec/android/app/storycam/view/ThemeSelectionGridView$ThemeSelectionAdapter;

.field private mItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

.field private mTranscodeElementListener:Lcom/sec/android/app/storycam/AppDataManager$TranscodeElementChangeListener;

.field private mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 60
    invoke-direct {p0, p1}, Landroid/widget/GridView;-><init>(Landroid/content/Context;)V

    .line 38
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;->isM2Model:Z

    .line 40
    new-instance v0, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView$1;-><init>(Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;->mItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 47
    new-instance v0, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView$2;-><init>(Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;->clickListener:Landroid/view/View$OnClickListener;

    .line 319
    new-instance v0, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView$3;-><init>(Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;->mTranscodeElementListener:Lcom/sec/android/app/storycam/AppDataManager$TranscodeElementChangeListener;

    .line 61
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Landroid/widget/GridView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 38
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;->isM2Model:Z

    .line 40
    new-instance v0, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView$1;-><init>(Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;->mItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 47
    new-instance v0, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView$2;-><init>(Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;->clickListener:Landroid/view/View$OnClickListener;

    .line 319
    new-instance v0, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView$3;-><init>(Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;->mTranscodeElementListener:Lcom/sec/android/app/storycam/AppDataManager$TranscodeElementChangeListener;

    .line 57
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 64
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/GridView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 38
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;->isM2Model:Z

    .line 40
    new-instance v0, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView$1;-><init>(Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;->mItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 47
    new-instance v0, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView$2;-><init>(Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;->clickListener:Landroid/view/View$OnClickListener;

    .line 319
    new-instance v0, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView$3;-><init>(Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;->mTranscodeElementListener:Lcom/sec/android/app/storycam/AppDataManager$TranscodeElementChangeListener;

    .line 65
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;I)V
    .locals 0

    .prologue
    .line 119
    invoke-direct {p0, p1}, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;->clickOperation(I)V

    return-void
.end method

.method static synthetic access$1(Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;)Landroid/widget/AdapterView$OnItemClickListener;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;->mItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;)Z
    .locals 1

    .prologue
    .line 38
    iget-boolean v0, p0, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;->isM2Model:Z

    return v0
.end method

.method static synthetic access$3(Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;Ljava/lang/Boolean;)V
    .locals 0

    .prologue
    .line 176
    invoke-direct {p0, p1}, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;->isM2ModelAdjustLayout(Ljava/lang/Boolean;)V

    return-void
.end method

.method static synthetic access$4(Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;)Lcom/sec/android/app/storycam/view/ThemeSelectionGridView$ThemeSelectionAdapter;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;->mAdapter:Lcom/sec/android/app/storycam/view/ThemeSelectionGridView$ThemeSelectionAdapter;

    return-object v0
.end method

.method static synthetic access$5(Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;->clickListener:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method private clickOperation(I)V
    .locals 10
    .param p1, "position"    # I

    .prologue
    .line 126
    sget-object v0, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v0}, Lcom/sec/android/app/storycam/AppDataManager;->getUHDCheckFlag()Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    .line 175
    :cond_0
    :goto_0
    return-void

    .line 130
    :cond_1
    sget-object v0, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v0}, Lcom/sec/android/app/storycam/AppDataManager;->getCurrentTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v8

    .line 131
    .local v8, "tElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    if-eqz v8, :cond_2

    .line 132
    invoke-static {p1}, Lcom/sec/android/app/storycam/Utils;->mapThemeIndexToID(I)I

    move-result v6

    .line 133
    .local v6, "newThemeID":I
    invoke-virtual {v8}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getThemeName()I

    move-result v0

    if-eq v6, v0, :cond_0

    .line 137
    .end local v6    # "newThemeID":I
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    invoke-interface {v0}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;->saveCaption()V

    .line 138
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;->requestFocusForMainActivity(Z)V

    .line 140
    const/4 v3, 0x0

    .line 141
    .local v3, "wasPreviewPlaying":Z
    const-wide/16 v4, 0x0

    .line 142
    .local v4, "pos":J
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    invoke-interface {v0}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;->getPreviewPlayer()Lcom/sec/android/app/ve/PreviewPlayerInterface;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 143
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    invoke-interface {v0}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;->getPreviewPlayer()Lcom/sec/android/app/ve/PreviewPlayerInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/ve/PreviewPlayerInterface;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 144
    const/4 v3, 0x1

    .line 145
    sget-object v0, Lcom/sec/android/app/ve/VEApp;->gExport:Lcom/sec/android/app/ve/export/Export;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/export/Export;->isExportRunning()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 146
    const-wide/16 v4, 0x0

    .line 152
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    invoke-interface {v0}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;->getPreviewPlayer()Lcom/sec/android/app/ve/PreviewPlayerInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/ve/PreviewPlayerInterface;->stop()V

    .line 155
    :cond_4
    if-eqz v8, :cond_0

    .line 156
    invoke-virtual {v8}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getThemeName()I

    move-result v9

    .line 157
    .local v9, "theme":I
    invoke-static {p1}, Lcom/sec/android/app/storycam/Utils;->mapThemeIndexToID(I)I

    move-result v6

    .line 158
    .restart local v6    # "newThemeID":I
    sget-object v0, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v0}, Lcom/sec/android/app/storycam/AppDataManager;->getOriginalTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v7

    .line 159
    .local v7, "origTransElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    if-eq v9, v6, :cond_0

    .line 160
    if-eqz v7, :cond_5

    .line 161
    invoke-virtual {v7, v6}, Lcom/samsung/app/video/editor/external/TranscodeElement;->setThemeName(I)V

    .line 164
    :cond_5
    invoke-static {}, Lcom/sec/android/app/ve/theme/ThemeDataManager;->getInstance()Lcom/sec/android/app/ve/theme/ThemeDataManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, v6}, Lcom/sec/android/app/ve/theme/ThemeDataManager;->createThemeProject(Landroid/content/Context;I)Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v2

    .line 166
    .local v2, "newTElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    sget-object v0, Lcom/sec/android/app/ve/VEApp;->gBGMManager:Lcom/sec/android/app/ve/bgm/BGMManager;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/bgm/BGMManager;->isThemeDefaultBGM()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 167
    sget-object v0, Lcom/sec/android/app/ve/VEApp;->gBGMManager:Lcom/sec/android/app/ve/bgm/BGMManager;

    sget-object v1, Lcom/sec/android/app/ve/VEApp;->gBGMManager:Lcom/sec/android/app/ve/bgm/BGMManager;

    invoke-virtual {v1, v6}, Lcom/sec/android/app/ve/bgm/BGMManager;->mapThemeToBGM(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/ve/bgm/BGMManager;->setSavedBGMPos(I)V

    .line 168
    :cond_6
    invoke-static {}, Lcom/sec/android/app/ve/theme/ThemeDataManager;->getInstance()Lcom/sec/android/app/ve/theme/ThemeDataManager;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/ve/VEApp;->gBGMManager:Lcom/sec/android/app/ve/bgm/BGMManager;

    invoke-virtual {v0, v2, v1}, Lcom/sec/android/app/ve/theme/ThemeDataManager;->modifyTranscodeWithBGMTranstionSlots(Lcom/samsung/app/video/editor/external/TranscodeElement;Lcom/sec/android/app/ve/bgm/BGMManager;)V

    .line 169
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    iget-object v1, p0, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    invoke-interface {v1}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;->getSummaryHandler()Landroid/os/Handler;

    move-result-object v1

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/storycam/summary/Summary;->summarize(Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;Landroid/os/Handler;Lcom/samsung/app/video/editor/external/TranscodeElement;ZJ)V

    goto/16 :goto_0

    .line 149
    .end local v2    # "newTElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    .end local v6    # "newThemeID":I
    .end local v7    # "origTransElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    .end local v9    # "theme":I
    :cond_7
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/app/video/editor/external/NativeInterface;->getCurrentPosition()I

    move-result v0

    int-to-long v4, v0

    goto :goto_1
.end method

.method private init()V
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 109
    new-instance v1, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView$ThemeSelectionAdapter;

    invoke-direct {v1, p0}, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView$ThemeSelectionAdapter;-><init>(Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;)V

    iput-object v1, p0, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;->mAdapter:Lcom/sec/android/app/storycam/view/ThemeSelectionGridView$ThemeSelectionAdapter;

    .line 110
    const-string v1, "ro.product.device"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 111
    .local v0, "modelName":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v1, "m2"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_0
    move v1, v3

    :goto_0
    iput-boolean v1, p0, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;->isM2Model:Z

    .line 112
    iget-object v1, p0, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;->mAdapter:Lcom/sec/android/app/storycam/view/ThemeSelectionGridView$ThemeSelectionAdapter;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 113
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;->getContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    iput-object v1, p0, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    .line 114
    sget-object v1, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    iget-object v4, p0, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;->mTranscodeElementListener:Lcom/sec/android/app/storycam/AppDataManager$TranscodeElementChangeListener;

    invoke-virtual {v1, v4}, Lcom/sec/android/app/storycam/AppDataManager;->addTranscodeElementChangeListener(Lcom/sec/android/app/storycam/AppDataManager$TranscodeElementChangeListener;)V

    .line 115
    sget-object v1, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v1}, Lcom/sec/android/app/storycam/AppDataManager;->getUHDCheckFlag()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 116
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v4, 0x7f07001c

    invoke-static {v4}, Lcom/sec/android/app/ve/VEApp;->getStringValue(I)Ljava/lang/String;

    move-result-object v4

    new-array v3, v3, [Ljava/lang/Object;

    const-string v5, "UHD/WQHD"

    aput-object v5, v3, v2

    invoke-static {v4, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3, v2}, Lcom/sec/android/app/ve/VEApp;->showToast(Landroid/content/Context;Ljava/lang/String;I)V

    .line 117
    :cond_1
    return-void

    :cond_2
    move v1, v2

    .line 111
    goto :goto_0
.end method

.method private isM2ModelAdjustLayout(Ljava/lang/Boolean;)V
    .locals 2
    .param p1, "isM2Model"    # Ljava/lang/Boolean;

    .prologue
    .line 177
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 179
    .local v0, "tempParams":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 180
    const v1, 0x7f06036b

    invoke-static {v1}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 181
    const v1, 0x7f09000b

    invoke-static {v1}, Lcom/sec/android/app/ve/VEApp;->getIntegerValue(I)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;->setNumColumns(I)V

    .line 188
    :goto_0
    return-void

    .line 185
    :cond_0
    const v1, 0x7f06036a

    invoke-static {v1}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 186
    const v1, 0x7f09000a

    invoke-static {v1}, Lcom/sec/android/app/ve/VEApp;->getIntegerValue(I)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;->setNumColumns(I)V

    goto :goto_0
.end method


# virtual methods
.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 6
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 87
    invoke-super {p0, p1}, Landroid/widget/GridView;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 89
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 90
    .local v0, "tempParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-boolean v1, p0, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;->isM2Model:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;->isM2ModelAdjustLayout(Ljava/lang/Boolean;)V

    .line 91
    const v1, 0x7f0603bb

    invoke-static {v1}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 92
    const v1, 0x7f060312

    invoke-static {v1}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 93
    const v1, 0x7f06030f

    invoke-static {v1}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v1

    .line 94
    const v2, 0x7f06030e

    invoke-static {v2}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v2

    .line 95
    const v3, 0x7f060310

    invoke-static {v3}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v3

    .line 96
    const v4, 0x7f060311

    invoke-static {v4}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v4

    .line 93
    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;->setPadding(IIII)V

    .line 97
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    .line 98
    const v2, 0x7f06036d

    invoke-static {v2}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v2

    .line 99
    const v3, 0x7f0603bd

    invoke-static {v3}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v3

    .line 100
    const v4, 0x7f06036e

    invoke-static {v4}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v4

    .line 101
    const/4 v5, 0x0

    .line 97
    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/widget/RelativeLayout;->setPadding(IIII)V

    .line 102
    invoke-virtual {p0, v0}, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 103
    iget-object v1, p0, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;->mAdapter:Lcom/sec/android/app/storycam/view/ThemeSelectionGridView$ThemeSelectionAdapter;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 106
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 70
    sget-object v0, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    if-eqz v0, :cond_0

    .line 71
    sget-object v0, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    iget-object v1, p0, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;->mTranscodeElementListener:Lcom/sec/android/app/storycam/AppDataManager$TranscodeElementChangeListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/storycam/AppDataManager;->removeTranscodeElementChangeListener(Lcom/sec/android/app/storycam/AppDataManager$TranscodeElementChangeListener;)V

    .line 72
    :cond_0
    invoke-super {p0}, Landroid/widget/GridView;->onDetachedFromWindow()V

    .line 73
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 77
    invoke-super {p0}, Landroid/widget/GridView;->onFinishInflate()V

    .line 78
    invoke-direct {p0}, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;->init()V

    .line 79
    const v0, 0x7f09000a

    invoke-static {v0}, Lcom/sec/android/app/ve/VEApp;->getIntegerValue(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/storycam/view/ThemeSelectionGridView;->setNumColumns(I)V

    .line 83
    return-void
.end method
