.class Lcom/sec/android/app/storycam/view/Caption1Line$3;
.super Ljava/lang/Object;
.source "Caption1Line.java"

# interfaces
.implements Landroid/text/InputFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/storycam/view/Caption1Line;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/storycam/view/Caption1Line;


# direct methods
.method constructor <init>(Lcom/sec/android/app/storycam/view/Caption1Line;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/storycam/view/Caption1Line$3;->this$0:Lcom/sec/android/app/storycam/view/Caption1Line;

    .line 187
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;
    .locals 11
    .param p1, "text"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "end"    # I
    .param p4, "spanned"    # Landroid/text/Spanned;
    .param p5, "dstStart"    # I
    .param p6, "dstEnd"    # I

    .prologue
    .line 191
    sub-int v9, p3, p2

    const/16 v10, 0xa0

    if-le v9, v10, :cond_0

    .line 192
    add-int/lit16 p3, p2, 0xa0

    .line 194
    :cond_0
    invoke-interface {p1, p2, p3}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v9

    invoke-interface {v9}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    .line 195
    .local v6, "origTxt":Ljava/lang/String;
    move-object v7, v6

    .line 197
    .local v7, "validTxt":Ljava/lang/String;
    const/4 v4, 0x0

    .line 201
    .local v4, "invalidFlag":Z
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    sget-object v9, Lcom/sec/android/app/storycam/view/Caption1Line;->INVALID_CHAR:[Ljava/lang/String;

    array-length v9, v9

    if-lt v1, v9, :cond_1

    .line 237
    if-eqz v4, :cond_5

    .line 238
    iget-object v9, p0, Lcom/sec/android/app/storycam/view/Caption1Line$3;->this$0:Lcom/sec/android/app/storycam/view/Caption1Line;

    iget-object v10, p0, Lcom/sec/android/app/storycam/view/Caption1Line$3;->this$0:Lcom/sec/android/app/storycam/view/Caption1Line;

    # getter for: Lcom/sec/android/app/storycam/view/Caption1Line;->invalidCharToast:Landroid/widget/Toast;
    invoke-static {v10}, Lcom/sec/android/app/storycam/view/Caption1Line;->access$3(Lcom/sec/android/app/storycam/view/Caption1Line;)Landroid/widget/Toast;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/sec/android/app/storycam/view/Caption1Line;->showToastMessage(Landroid/widget/Toast;)V

    .line 241
    .end local v7    # "validTxt":Ljava/lang/String;
    :goto_1
    return-object v7

    .line 203
    .restart local v7    # "validTxt":Ljava/lang/String;
    :cond_1
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v8

    .line 205
    .local v8, "validTxtLength":I
    const/4 v5, 0x0

    .local v5, "j":I
    :goto_2
    if-lt v5, v8, :cond_2

    .line 201
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 206
    :cond_2
    sget-object v9, Lcom/sec/android/app/storycam/view/Caption1Line;->INVALID_CHAR:[Ljava/lang/String;

    aget-object v9, v9, v1

    invoke-virtual {v7, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    .line 207
    .local v3, "index":I
    if-ltz v3, :cond_4

    .line 208
    const/4 v4, 0x1

    .line 209
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v9

    if-ge v3, v9, :cond_3

    .line 211
    new-instance v9, Ljava/lang/StringBuilder;

    const/4 v10, 0x0

    invoke-virtual {v7, v10, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v10, v3, 0x1

    invoke-virtual {v7, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 205
    :cond_3
    :goto_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 219
    :cond_4
    sget-object v9, Lcom/sec/android/app/storycam/view/Caption1Line;->INVALID_CHAR:[Ljava/lang/String;

    aget-object v9, v9, v1

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 220
    .local v0, "c":C
    const/16 v9, 0x21

    if-lt v0, v9, :cond_3

    const/16 v9, 0x7e

    if-ge v0, v9, :cond_3

    const/16 v9, 0x3f

    if-eq v0, v9, :cond_3

    .line 221
    const v9, 0xfee0

    add-int/2addr v9, v0

    int-to-char v0, v9

    .line 222
    invoke-virtual {v7, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    .line 223
    .local v2, "iDBC":I
    if-ltz v2, :cond_3

    .line 224
    const/4 v4, 0x1

    .line 225
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v9

    if-ge v2, v9, :cond_3

    .line 227
    new-instance v9, Ljava/lang/StringBuilder;

    const/4 v10, 0x0

    invoke-virtual {v7, v10, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v10, v2, 0x1

    invoke-virtual {v7, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    goto :goto_3

    .line 241
    .end local v0    # "c":C
    .end local v2    # "iDBC":I
    .end local v3    # "index":I
    .end local v5    # "j":I
    .end local v8    # "validTxtLength":I
    :cond_5
    const/4 v7, 0x0

    goto :goto_1
.end method
