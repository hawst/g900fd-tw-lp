.class public Lcom/sec/android/app/storycam/bgm/VintageBGM;
.super Lcom/sec/android/app/ve/bgm/BGM;
.source "VintageBGM.java"


# static fields
.field private static synthetic $SWITCH_TABLE$com$sec$android$app$ve$bgm$BGM$BGMModes:[I


# instance fields
.field private fastModeBGM:[I

.field private mediumModeBGM:[I

.field private slowModeBGM:[I

.field private totalDurationBGM:I


# direct methods
.method static synthetic $SWITCH_TABLE$com$sec$android$app$ve$bgm$BGM$BGMModes()[I
    .locals 3

    .prologue
    .line 5
    sget-object v0, Lcom/sec/android/app/storycam/bgm/VintageBGM;->$SWITCH_TABLE$com$sec$android$app$ve$bgm$BGM$BGMModes:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/sec/android/app/ve/bgm/BGM$BGMModes;->values()[Lcom/sec/android/app/ve/bgm/BGM$BGMModes;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/sec/android/app/ve/bgm/BGM$BGMModes;->FAST:Lcom/sec/android/app/ve/bgm/BGM$BGMModes;

    invoke-virtual {v1}, Lcom/sec/android/app/ve/bgm/BGM$BGMModes;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    :try_start_1
    sget-object v1, Lcom/sec/android/app/ve/bgm/BGM$BGMModes;->MEDIUM:Lcom/sec/android/app/ve/bgm/BGM$BGMModes;

    invoke-virtual {v1}, Lcom/sec/android/app/ve/bgm/BGM$BGMModes;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    :try_start_2
    sget-object v1, Lcom/sec/android/app/ve/bgm/BGM$BGMModes;->SLOW:Lcom/sec/android/app/ve/bgm/BGM$BGMModes;

    invoke-virtual {v1}, Lcom/sec/android/app/ve/bgm/BGM$BGMModes;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_3
    sput-object v0, Lcom/sec/android/app/storycam/bgm/VintageBGM;->$SWITCH_TABLE$com$sec$android$app$ve$bgm$BGM$BGMModes:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_1
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/sec/android/app/ve/bgm/BGM;-><init>()V

    .line 7
    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    .line 12
    iput-object v0, p0, Lcom/sec/android/app/storycam/bgm/VintageBGM;->slowModeBGM:[I

    .line 15
    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    .line 22
    iput-object v0, p0, Lcom/sec/android/app/storycam/bgm/VintageBGM;->mediumModeBGM:[I

    .line 25
    const/16 v0, 0xb

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    .line 35
    iput-object v0, p0, Lcom/sec/android/app/storycam/bgm/VintageBGM;->fastModeBGM:[I

    .line 38
    const v0, 0x91dc

    iput v0, p0, Lcom/sec/android/app/storycam/bgm/VintageBGM;->totalDurationBGM:I

    .line 5
    return-void

    .line 7
    nop

    :array_0
    .array-data 4
        0x170c
        0x2580
        0x3840
        0x4b00
        0x5e24
        0x7594
    .end array-data

    .line 15
    :array_1
    .array-data 4
        0x125c
        0x20d0
        0x30d4
        0x4074
        0x5014
        0x5eec
        0x7080
        0x7ef4
    .end array-data

    .line 25
    :array_2
    .array-data 4
        0xdac
        0x1964
        0x251c
        0x30d4
        0x3c8c
        0x4844
        0x53fc
        0x62d4
        0x6e8c
        0x7a44
        0x85fc
    .end array-data
.end method


# virtual methods
.method protected getBGMFilepath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    const-string v0, "themedata/oldfilm/bgm/D5_Vintage.ogg"

    return-object v0
.end method

.method protected getBGMName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    const-string v0, "Vintage"

    return-object v0
.end method

.method protected getTotalDurationBGM()I
    .locals 1

    .prologue
    .line 56
    iget v0, p0, Lcom/sec/android/app/storycam/bgm/VintageBGM;->totalDurationBGM:I

    return v0
.end method

.method protected getTransitionTimeStamps(Lcom/sec/android/app/ve/bgm/BGM$BGMModes;)[I
    .locals 2
    .param p1, "mode"    # Lcom/sec/android/app/ve/bgm/BGM$BGMModes;

    .prologue
    .line 42
    invoke-static {}, Lcom/sec/android/app/storycam/bgm/VintageBGM;->$SWITCH_TABLE$com$sec$android$app$ve$bgm$BGM$BGMModes()[I

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/android/app/ve/bgm/BGM$BGMModes;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 50
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 44
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/storycam/bgm/VintageBGM;->slowModeBGM:[I

    goto :goto_0

    .line 46
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/storycam/bgm/VintageBGM;->mediumModeBGM:[I

    goto :goto_0

    .line 48
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/storycam/bgm/VintageBGM;->fastModeBGM:[I

    goto :goto_0

    .line 42
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
