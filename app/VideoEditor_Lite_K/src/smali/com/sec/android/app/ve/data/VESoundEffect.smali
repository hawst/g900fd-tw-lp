.class public Lcom/sec/android/app/ve/data/VESoundEffect;
.super Ljava/lang/Object;
.source "VESoundEffect.java"


# instance fields
.field private mSongDuration:I

.field private mSongId:I

.field private mSongName:Ljava/lang/String;

.field private mSongPath:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;II)V
    .locals 0
    .param p1, "aSongName"    # Ljava/lang/String;
    .param p2, "aSongPath"    # Ljava/lang/String;
    .param p3, "amSongDuration"    # I
    .param p4, "index"    # I

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/sec/android/app/ve/data/VESoundEffect;->mSongName:Ljava/lang/String;

    .line 21
    iput-object p2, p0, Lcom/sec/android/app/ve/data/VESoundEffect;->mSongPath:Ljava/lang/String;

    .line 22
    iput p3, p0, Lcom/sec/android/app/ve/data/VESoundEffect;->mSongDuration:I

    .line 23
    iput p4, p0, Lcom/sec/android/app/ve/data/VESoundEffect;->mSongId:I

    .line 24
    return-void
.end method


# virtual methods
.method public getSongDuration()I
    .locals 1

    .prologue
    .line 55
    iget v0, p0, Lcom/sec/android/app/ve/data/VESoundEffect;->mSongDuration:I

    return v0
.end method

.method public getSongId()I
    .locals 1

    .prologue
    .line 47
    iget v0, p0, Lcom/sec/android/app/ve/data/VESoundEffect;->mSongId:I

    return v0
.end method

.method public getVESongName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/app/ve/data/VESoundEffect;->mSongName:Ljava/lang/String;

    return-object v0
.end method

.method public getVESongPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/ve/data/VESoundEffect;->mSongPath:Ljava/lang/String;

    return-object v0
.end method

.method public setSongDuration(I)V
    .locals 0
    .param p1, "aSongDuration"    # I

    .prologue
    .line 51
    iput p1, p0, Lcom/sec/android/app/ve/data/VESoundEffect;->mSongDuration:I

    .line 52
    return-void
.end method

.method public setSongId(I)V
    .locals 0
    .param p1, "aSongId"    # I

    .prologue
    .line 43
    iput p1, p0, Lcom/sec/android/app/ve/data/VESoundEffect;->mSongId:I

    .line 44
    return-void
.end method

.method public setVESongName(Ljava/lang/String;)V
    .locals 0
    .param p1, "aSongName"    # Ljava/lang/String;

    .prologue
    .line 27
    iput-object p1, p0, Lcom/sec/android/app/ve/data/VESoundEffect;->mSongName:Ljava/lang/String;

    .line 28
    return-void
.end method

.method public setVESongPath(Ljava/lang/String;)V
    .locals 0
    .param p1, "aSongPath"    # Ljava/lang/String;

    .prologue
    .line 35
    iput-object p1, p0, Lcom/sec/android/app/ve/data/VESoundEffect;->mSongPath:Ljava/lang/String;

    .line 36
    return-void
.end method
