.class public Lcom/sec/android/app/storycam/view/MediaEditGridView;
.super Landroid/widget/GridLayout;
.source "MediaEditGridView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;
    }
.end annotation


# static fields
.field public static final DIRECTION_LEFT:I = 0x2

.field private static final DIRECTION_RIGHT:I = 0x3

.field private static final MSG_DRAG_DELAY:I = 0x64

.field private static final MSG_DRAG_ENTERED:I = 0x1

.field private static final START_SCROLL:I = 0x65

.field private static mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;


# instance fields
.field private final mAddMediaClickListener:Landroid/view/View$OnClickListener;

.field private mAutoScroll:Z

.field private mCurrentDropIndex:I

.field private final mDragListener:Landroid/view/View$OnDragListener;

.field private mDragMovementListener:Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;

.field private mDraggingView:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;

.field private final mGridItemListener:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MediaGridItemInterface;

.field private final mHandler:Landroid/os/Handler;

.field private mImageUnsupported:Landroid/graphics/Bitmap;

.field private mInflater:Landroid/view/LayoutInflater;

.field private final mLongClickListener:Landroid/view/View$OnLongClickListener;

.field private final mThumbCacheListener:Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheUpdateListener;

.field private final mThumbDataProvider:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$ThumbnailDataProvider;

.field private mTouchX:F

.field private mTouchY:F

.field private mVideoUnsupported:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 402
    invoke-direct {p0, p1}, Landroid/widget/GridLayout;-><init>(Landroid/content/Context;)V

    .line 68
    new-instance v0, Lcom/sec/android/app/storycam/view/MediaEditGridView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/MediaEditGridView$1;-><init>(Lcom/sec/android/app/storycam/view/MediaEditGridView;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mThumbCacheListener:Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheUpdateListener;

    .line 75
    new-instance v0, Lcom/sec/android/app/storycam/view/MediaEditGridView$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/MediaEditGridView$2;-><init>(Lcom/sec/android/app/storycam/view/MediaEditGridView;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mGridItemListener:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MediaGridItemInterface;

    .line 95
    new-instance v0, Lcom/sec/android/app/storycam/view/MediaEditGridView$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/MediaEditGridView$3;-><init>(Lcom/sec/android/app/storycam/view/MediaEditGridView;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mThumbDataProvider:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$ThumbnailDataProvider;

    .line 126
    new-instance v0, Lcom/sec/android/app/storycam/view/MediaEditGridView$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/MediaEditGridView$4;-><init>(Lcom/sec/android/app/storycam/view/MediaEditGridView;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mLongClickListener:Landroid/view/View$OnLongClickListener;

    .line 151
    new-instance v0, Lcom/sec/android/app/storycam/view/MediaEditGridView$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/MediaEditGridView$5;-><init>(Lcom/sec/android/app/storycam/view/MediaEditGridView;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mHandler:Landroid/os/Handler;

    .line 206
    new-instance v0, Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;-><init>(Lcom/sec/android/app/storycam/view/MediaEditGridView;Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mDragMovementListener:Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;

    .line 253
    new-instance v0, Lcom/sec/android/app/storycam/view/MediaEditGridView$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/MediaEditGridView$6;-><init>(Lcom/sec/android/app/storycam/view/MediaEditGridView;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mDragListener:Landroid/view/View$OnDragListener;

    .line 384
    new-instance v0, Lcom/sec/android/app/storycam/view/MediaEditGridView$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/MediaEditGridView$7;-><init>(Lcom/sec/android/app/storycam/view/MediaEditGridView;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mAddMediaClickListener:Landroid/view/View$OnClickListener;

    .line 403
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 398
    invoke-direct {p0, p1, p2}, Landroid/widget/GridLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 68
    new-instance v0, Lcom/sec/android/app/storycam/view/MediaEditGridView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/MediaEditGridView$1;-><init>(Lcom/sec/android/app/storycam/view/MediaEditGridView;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mThumbCacheListener:Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheUpdateListener;

    .line 75
    new-instance v0, Lcom/sec/android/app/storycam/view/MediaEditGridView$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/MediaEditGridView$2;-><init>(Lcom/sec/android/app/storycam/view/MediaEditGridView;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mGridItemListener:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MediaGridItemInterface;

    .line 95
    new-instance v0, Lcom/sec/android/app/storycam/view/MediaEditGridView$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/MediaEditGridView$3;-><init>(Lcom/sec/android/app/storycam/view/MediaEditGridView;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mThumbDataProvider:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$ThumbnailDataProvider;

    .line 126
    new-instance v0, Lcom/sec/android/app/storycam/view/MediaEditGridView$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/MediaEditGridView$4;-><init>(Lcom/sec/android/app/storycam/view/MediaEditGridView;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mLongClickListener:Landroid/view/View$OnLongClickListener;

    .line 151
    new-instance v0, Lcom/sec/android/app/storycam/view/MediaEditGridView$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/MediaEditGridView$5;-><init>(Lcom/sec/android/app/storycam/view/MediaEditGridView;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mHandler:Landroid/os/Handler;

    .line 206
    new-instance v0, Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;-><init>(Lcom/sec/android/app/storycam/view/MediaEditGridView;Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mDragMovementListener:Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;

    .line 253
    new-instance v0, Lcom/sec/android/app/storycam/view/MediaEditGridView$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/MediaEditGridView$6;-><init>(Lcom/sec/android/app/storycam/view/MediaEditGridView;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mDragListener:Landroid/view/View$OnDragListener;

    .line 384
    new-instance v0, Lcom/sec/android/app/storycam/view/MediaEditGridView$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/MediaEditGridView$7;-><init>(Lcom/sec/android/app/storycam/view/MediaEditGridView;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mAddMediaClickListener:Landroid/view/View$OnClickListener;

    .line 399
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 406
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/GridLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 68
    new-instance v0, Lcom/sec/android/app/storycam/view/MediaEditGridView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/MediaEditGridView$1;-><init>(Lcom/sec/android/app/storycam/view/MediaEditGridView;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mThumbCacheListener:Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheUpdateListener;

    .line 75
    new-instance v0, Lcom/sec/android/app/storycam/view/MediaEditGridView$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/MediaEditGridView$2;-><init>(Lcom/sec/android/app/storycam/view/MediaEditGridView;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mGridItemListener:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MediaGridItemInterface;

    .line 95
    new-instance v0, Lcom/sec/android/app/storycam/view/MediaEditGridView$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/MediaEditGridView$3;-><init>(Lcom/sec/android/app/storycam/view/MediaEditGridView;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mThumbDataProvider:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$ThumbnailDataProvider;

    .line 126
    new-instance v0, Lcom/sec/android/app/storycam/view/MediaEditGridView$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/MediaEditGridView$4;-><init>(Lcom/sec/android/app/storycam/view/MediaEditGridView;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mLongClickListener:Landroid/view/View$OnLongClickListener;

    .line 151
    new-instance v0, Lcom/sec/android/app/storycam/view/MediaEditGridView$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/MediaEditGridView$5;-><init>(Lcom/sec/android/app/storycam/view/MediaEditGridView;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mHandler:Landroid/os/Handler;

    .line 206
    new-instance v0, Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;-><init>(Lcom/sec/android/app/storycam/view/MediaEditGridView;Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mDragMovementListener:Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;

    .line 253
    new-instance v0, Lcom/sec/android/app/storycam/view/MediaEditGridView$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/MediaEditGridView$6;-><init>(Lcom/sec/android/app/storycam/view/MediaEditGridView;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mDragListener:Landroid/view/View$OnDragListener;

    .line 384
    new-instance v0, Lcom/sec/android/app/storycam/view/MediaEditGridView$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/MediaEditGridView$7;-><init>(Lcom/sec/android/app/storycam/view/MediaEditGridView;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mAddMediaClickListener:Landroid/view/View$OnClickListener;

    .line 407
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/app/storycam/view/MediaEditGridView;)V
    .locals 0

    .prologue
    .line 639
    invoke-direct {p0}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->refreshAllViews()V

    return-void
.end method

.method static synthetic access$1()Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    return-object v0
.end method

.method static synthetic access$10(Lcom/sec/android/app/storycam/view/MediaEditGridView;)Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mDragMovementListener:Lcom/sec/android/app/storycam/view/MediaEditGridView$DragMovementListener;

    return-object v0
.end method

.method static synthetic access$11(Lcom/sec/android/app/storycam/view/MediaEditGridView;)V
    .locals 0

    .prologue
    .line 696
    invoke-direct {p0}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->resetDragState()V

    return-void
.end method

.method static synthetic access$12(Lcom/sec/android/app/storycam/view/MediaEditGridView;Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;)V
    .locals 0

    .prologue
    .line 56
    iput-object p1, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mDraggingView:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;

    return-void
.end method

.method static synthetic access$13(Lcom/sec/android/app/storycam/view/MediaEditGridView;I)V
    .locals 0

    .prologue
    .line 57
    iput p1, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mCurrentDropIndex:I

    return-void
.end method

.method static synthetic access$14(Lcom/sec/android/app/storycam/view/MediaEditGridView;II)V
    .locals 0

    .prologue
    .line 488
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->changeElementPosition(II)V

    return-void
.end method

.method static synthetic access$2(Lcom/sec/android/app/storycam/view/MediaEditGridView;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mImageUnsupported:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$3(Lcom/sec/android/app/storycam/view/MediaEditGridView;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mVideoUnsupported:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$4(Lcom/sec/android/app/storycam/view/MediaEditGridView;)F
    .locals 1

    .prologue
    .line 65
    iget v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mTouchX:F

    return v0
.end method

.method static synthetic access$5(Lcom/sec/android/app/storycam/view/MediaEditGridView;)F
    .locals 1

    .prologue
    .line 66
    iget v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mTouchY:F

    return v0
.end method

.method static synthetic access$6(Lcom/sec/android/app/storycam/view/MediaEditGridView;)Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mDraggingView:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;

    return-object v0
.end method

.method static synthetic access$7(Lcom/sec/android/app/storycam/view/MediaEditGridView;)I
    .locals 1

    .prologue
    .line 57
    iget v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mCurrentDropIndex:I

    return v0
.end method

.method static synthetic access$8(Lcom/sec/android/app/storycam/view/MediaEditGridView;)Z
    .locals 1

    .prologue
    .line 58
    iget-boolean v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mAutoScroll:Z

    return v0
.end method

.method static synthetic access$9(Lcom/sec/android/app/storycam/view/MediaEditGridView;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private changeElementPosition(II)V
    .locals 4
    .param p1, "draggingViewPosition"    # I
    .param p2, "droppedViewPosition"    # I

    .prologue
    .line 489
    sget-object v3, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v3}, Lcom/sec/android/app/storycam/AppDataManager;->getOriginalTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v2

    .line 490
    .local v2, "trans":Lcom/samsung/app/video/editor/external/TranscodeElement;
    if-eqz v2, :cond_1

    .line 491
    invoke-virtual {v2, p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElement(I)Lcom/samsung/app/video/editor/external/Element;

    move-result-object v0

    .line 492
    .local v0, "draggedElement":Lcom/samsung/app/video/editor/external/Element;
    if-eqz v0, :cond_0

    .line 493
    invoke-direct {p0, v2}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->getCurrentTransitionMap(Lcom/samsung/app/video/editor/external/TranscodeElement;)Ljava/util/HashMap;

    move-result-object v1

    .line 494
    .local v1, "editMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Lcom/samsung/app/video/editor/external/Edit;>;"
    invoke-virtual {v2, v0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->removeElement(Lcom/samsung/app/video/editor/external/Element;)Ljava/util/List;

    .line 495
    invoke-virtual {v2, v0, p2}, Lcom/samsung/app/video/editor/external/TranscodeElement;->insertElement(Lcom/samsung/app/video/editor/external/Element;I)V

    .line 496
    invoke-direct {p0, v1, v2}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->changeTransitions(Ljava/util/HashMap;Lcom/samsung/app/video/editor/external/TranscodeElement;)V

    .line 498
    .end local v1    # "editMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Lcom/samsung/app/video/editor/external/Edit;>;"
    :cond_0
    sget-object v3, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    invoke-interface {v3}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;->requestForOptionMenuRefresh()V

    .line 500
    .end local v0    # "draggedElement":Lcom/samsung/app/video/editor/external/Element;
    :cond_1
    return-void
.end method

.method private changeTransitions(Ljava/util/HashMap;Lcom/samsung/app/video/editor/external/TranscodeElement;)V
    .locals 5
    .param p2, "transcodeElement"    # Lcom/samsung/app/video/editor/external/TranscodeElement;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/samsung/app/video/editor/external/Edit;",
            ">;",
            "Lcom/samsung/app/video/editor/external/TranscodeElement;",
            ")V"
        }
    .end annotation

    .prologue
    .line 477
    .local p1, "editHashMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Lcom/samsung/app/video/editor/external/Edit;>;"
    invoke-virtual {p2}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementList()Ljava/util/List;

    move-result-object v2

    .line 478
    .local v2, "elementList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    .line 479
    .local v0, "count":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-lt v3, v0, :cond_0

    .line 486
    return-void

    .line 480
    :cond_0
    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/app/video/editor/external/Element;

    .line 481
    .local v1, "element":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/Element;->removetransitionEdit()V

    .line 483
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-eq v3, v4, :cond_1

    .line 484
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/app/video/editor/external/Edit;

    invoke-virtual {v1, v4}, Lcom/samsung/app/video/editor/external/Element;->addEdit(Lcom/samsung/app/video/editor/external/Edit;)V

    .line 479
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method private doLayout()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x0

    .line 504
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v3

    .line 505
    .local v3, "layoutTransition":Landroid/animation/LayoutTransition;
    invoke-virtual {p0, v9}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    .line 506
    sget-object v6, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v6}, Lcom/sec/android/app/storycam/AppDataManager;->getOriginalTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v4

    .line 507
    .local v4, "trans":Lcom/samsung/app/video/editor/external/TranscodeElement;
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->removeAllViews()V

    .line 509
    if-eqz v4, :cond_0

    .line 510
    sget-object v6, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    if-eqz v6, :cond_1

    sget-object v6, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    invoke-interface {v6}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;->getCurrentConfig()Landroid/content/res/Configuration;

    move-result-object v6

    iget v6, v6, Landroid/content/res/Configuration;->orientation:I

    const/4 v7, 0x2

    if-ne v6, v7, :cond_1

    .line 511
    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v6

    add-int/lit8 v0, v6, 0x1

    .line 515
    .local v0, "colCount":I
    :goto_0
    invoke-virtual {p0, v0}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->setColumnCount(I)V

    .line 516
    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v1

    .line 517
    .local v1, "count":I
    if-lez v1, :cond_3

    .line 518
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-le v2, v1, :cond_2

    .line 528
    .end local v0    # "colCount":I
    .end local v1    # "count":I
    .end local v2    # "i":I
    :cond_0
    :goto_2
    iget-object v6, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mDragListener:Landroid/view/View$OnDragListener;

    invoke-virtual {p0, v6}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    .line 529
    invoke-virtual {p0, v3}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    .line 530
    return-void

    .line 513
    :cond_1
    const v6, 0x7f090008

    invoke-static {v6}, Lcom/sec/android/app/ve/VEApp;->getIntegerValue(I)I

    move-result v0

    .restart local v0    # "colCount":I
    goto :goto_0

    .line 519
    .restart local v1    # "count":I
    .restart local v2    # "i":I
    :cond_2
    invoke-virtual {v4, v2}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElement(I)Lcom/samsung/app/video/editor/external/Element;

    move-result-object v6

    invoke-direct {p0, v2, v6, v1}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->getView(ILcom/samsung/app/video/editor/external/Element;I)Landroid/view/View;

    move-result-object v5

    .line 520
    .local v5, "view":Landroid/view/View;
    invoke-static {}, Landroid/view/View;->generateViewId()I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/view/View;->setId(I)V

    .line 521
    invoke-virtual {p0, v5, v2}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->addView(Landroid/view/View;I)V

    .line 518
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 525
    .end local v2    # "i":I
    .end local v5    # "view":Landroid/view/View;
    :cond_3
    invoke-direct {p0, v8, v9, v8}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->getView(ILcom/samsung/app/video/editor/external/Element;I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {p0, v6, v8}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->addView(Landroid/view/View;I)V

    goto :goto_2
.end method

.method private getCurrentTransitionMap(Lcom/samsung/app/video/editor/external/TranscodeElement;)Ljava/util/HashMap;
    .locals 8
    .param p1, "transcodeElement"    # Lcom/samsung/app/video/editor/external/TranscodeElement;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/app/video/editor/external/TranscodeElement;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/samsung/app/video/editor/external/Edit;",
            ">;"
        }
    .end annotation

    .prologue
    .line 460
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 461
    .local v1, "editMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Lcom/samsung/app/video/editor/external/Edit;>;"
    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementList()Ljava/util/List;

    move-result-object v3

    .line 463
    .local v3, "elementList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    if-eqz v3, :cond_0

    .line 464
    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v6

    add-int/lit8 v0, v6, -0x1

    .line 465
    .local v0, "count":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-lt v4, v0, :cond_1

    .line 472
    .end local v0    # "count":I
    .end local v4    # "i":I
    :cond_0
    return-object v1

    .line 466
    .restart local v0    # "count":I
    .restart local v4    # "i":I
    :cond_1
    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/app/video/editor/external/Element;

    .line 467
    .local v2, "element":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/Element;->getTransitionEdit()Lcom/samsung/app/video/editor/external/Edit;

    move-result-object v5

    .line 468
    .local v5, "transEdit":Lcom/samsung/app/video/editor/external/Edit;
    if-eqz v5, :cond_2

    .line 469
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    new-instance v7, Lcom/samsung/app/video/editor/external/Edit;

    invoke-direct {v7, v5}, Lcom/samsung/app/video/editor/external/Edit;-><init>(Lcom/samsung/app/video/editor/external/Edit;)V

    invoke-virtual {v1, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 465
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method

.method private getView(ILcom/samsung/app/video/editor/external/Element;I)Landroid/view/View;
    .locals 16
    .param p1, "position"    # I
    .param p2, "element"    # Lcom/samsung/app/video/editor/external/Element;
    .param p3, "elementCount"    # I

    .prologue
    .line 535
    move/from16 v0, p1

    move/from16 v1, p3

    if-ne v0, v1, :cond_1

    .line 536
    const/4 v9, 0x0

    .line 537
    .local v9, "addMediaView":Landroid/widget/ImageButton;
    const/4 v8, 0x0

    .line 538
    .local v8, "addMediaLayoutView":Landroid/widget/RelativeLayout;
    new-instance v8, Landroid/widget/RelativeLayout;

    .end local v8    # "addMediaLayoutView":Landroid/widget/RelativeLayout;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mContext:Landroid/content/Context;

    invoke-direct {v8, v3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 539
    .restart local v8    # "addMediaLayoutView":Landroid/widget/RelativeLayout;
    new-instance v9, Landroid/widget/ImageButton;

    .end local v9    # "addMediaView":Landroid/widget/ImageButton;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mContext:Landroid/content/Context;

    invoke-direct {v9, v3}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 540
    .restart local v9    # "addMediaView":Landroid/widget/ImageButton;
    new-instance v12, Landroid/widget/GridLayout$LayoutParams;

    invoke-direct {v12}, Landroid/widget/GridLayout$LayoutParams;-><init>()V

    .line 541
    .local v12, "params":Landroid/widget/GridLayout$LayoutParams;
    new-instance v11, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v3, -0x2

    const/4 v4, -0x2

    invoke-direct {v11, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 543
    .local v11, "imageParams":Landroid/widget/RelativeLayout$LayoutParams;
    sget-object v3, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    if-eqz v3, :cond_0

    sget-object v3, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    invoke-interface {v3}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;->getCurrentConfig()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_0

    .line 544
    const v3, 0x7f060377

    invoke-static {v3}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v3

    iput v3, v12, Landroid/widget/GridLayout$LayoutParams;->width:I

    .line 548
    :goto_0
    const v3, 0x7f060397

    invoke-static {v3}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v3

    iput v3, v11, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 549
    const/16 v3, 0xd

    invoke-virtual {v11, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 550
    const v3, 0x7f0602f5

    invoke-static {v3}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v3

    iput v3, v12, Landroid/widget/GridLayout$LayoutParams;->height:I

    .line 551
    const v3, 0x7f060398

    invoke-static {v3}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v3

    iput v3, v11, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 552
    const v3, 0x7f060344

    invoke-static {v3}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v3

    iput v3, v12, Landroid/widget/GridLayout$LayoutParams;->topMargin:I

    .line 553
    const v3, 0x7f020005

    invoke-virtual {v9, v3}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 554
    const v3, 0x7f0602f3

    invoke-static {v3}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v3

    const/4 v4, 0x0

    const v5, 0x7f0602f3

    invoke-static {v5}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v5

    const/4 v6, 0x0

    invoke-virtual {v9, v3, v4, v5, v6}, Landroid/widget/ImageButton;->setPadding(IIII)V

    .line 555
    invoke-virtual {v9, v11}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 556
    const/4 v3, 0x1

    invoke-virtual {v9, v3}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 557
    const v3, 0x7f020029

    invoke-virtual {v9, v3}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 558
    invoke-virtual {v8, v12}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 559
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mAddMediaClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v9, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 560
    invoke-virtual {v8, v9}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 561
    const v3, 0x7f0702fe

    invoke-static {v3}, Lcom/sec/android/app/ve/VEApp;->getStringValue(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v9, v3}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 562
    const/4 v3, 0x1

    invoke-virtual {v9, v3}, Landroid/widget/ImageButton;->setHovered(Z)V

    .line 563
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v9}, Lcom/sec/android/app/ve/common/ConfigUtils;->setHovering(Landroid/content/Context;Landroid/view/View;)V

    .line 635
    .end local v8    # "addMediaLayoutView":Landroid/widget/RelativeLayout;
    .end local v9    # "addMediaView":Landroid/widget/ImageButton;
    .end local v11    # "imageParams":Landroid/widget/RelativeLayout$LayoutParams;
    :goto_1
    return-object v8

    .line 546
    .restart local v8    # "addMediaLayoutView":Landroid/widget/RelativeLayout;
    .restart local v9    # "addMediaView":Landroid/widget/ImageButton;
    .restart local v11    # "imageParams":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_0
    const/4 v3, -0x1

    iput v3, v12, Landroid/widget/GridLayout$LayoutParams;->width:I

    goto :goto_0

    .line 567
    .end local v8    # "addMediaLayoutView":Landroid/widget/RelativeLayout;
    .end local v9    # "addMediaView":Landroid/widget/ImageButton;
    .end local v11    # "imageParams":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v12    # "params":Landroid/widget/GridLayout$LayoutParams;
    :cond_1
    const/4 v2, 0x0

    .line 568
    .local v2, "layout":Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mInflater:Landroid/view/LayoutInflater;

    if-nez v3, :cond_2

    .line 569
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mInflater:Landroid/view/LayoutInflater;

    .line 571
    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mInflater:Landroid/view/LayoutInflater;

    const v4, 0x7f030009

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .end local v2    # "layout":Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;
    check-cast v2, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;

    .line 572
    .restart local v2    # "layout":Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mDragListener:Landroid/view/View$OnDragListener;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    .line 573
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 575
    new-instance v12, Landroid/widget/GridLayout$LayoutParams;

    invoke-direct {v12}, Landroid/widget/GridLayout$LayoutParams;-><init>()V

    .line 576
    .restart local v12    # "params":Landroid/widget/GridLayout$LayoutParams;
    const v3, 0x7f0d002e

    invoke-virtual {v2, v3}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/RelativeLayout;

    .line 578
    .local v14, "unsupportedLayout":Landroid/widget/RelativeLayout;
    sget-object v3, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    if-eqz v3, :cond_6

    sget-object v3, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    invoke-interface {v3}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;->getCurrentConfig()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_6

    .line 579
    const v3, 0x7f060379

    invoke-static {v3}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v3

    iput v3, v12, Landroid/widget/GridLayout$LayoutParams;->width:I

    .line 580
    const v3, 0x7f060344

    invoke-static {v3}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v3

    iput v3, v12, Landroid/widget/GridLayout$LayoutParams;->topMargin:I

    .line 581
    const/4 v3, -0x1

    iput v3, v12, Landroid/widget/GridLayout$LayoutParams;->height:I

    .line 593
    :goto_2
    invoke-virtual {v2, v12}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 595
    if-eqz p2, :cond_e

    .line 596
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mGridItemListener:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MediaGridItemInterface;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mThumbDataProvider:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$ThumbnailDataProvider;

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/app/video/editor/external/Element;->getType()I

    move-result v6

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/app/video/editor/external/Element;->getID()I

    move-result v3

    const/4 v7, -0x1

    if-eq v3, v7, :cond_8

    const/4 v7, 0x1

    :goto_3
    move/from16 v3, p1

    invoke-virtual/range {v2 .. v7}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->setMetaData(ILcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MediaGridItemInterface;Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$ThumbnailDataProvider;IZ)V

    .line 597
    sget-object v3, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    if-eqz v3, :cond_3

    .line 598
    sget-object v3, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    invoke-interface {v3}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;->getCurrentConfig()Landroid/content/res/Configuration;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->setConfig(Landroid/content/res/Configuration;)V

    .line 599
    :cond_3
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/app/video/editor/external/Element;->getType()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_4

    .line 600
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/app/video/editor/external/Element;->isUHD()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 601
    const v3, 0x7f0d0030

    invoke-virtual {v2, v3}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/ImageView;

    .line 602
    .local v13, "uhdIcon":Landroid/widget/ImageView;
    const/4 v3, 0x0

    invoke-virtual {v13, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 609
    .end local v13    # "uhdIcon":Landroid/widget/ImageView;
    :cond_4
    :goto_4
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/app/video/editor/external/Element;->getID()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_a

    .line 610
    sget-object v3, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/app/video/editor/external/Element;->getFilePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/storycam/AppDataManager;->getThumbnail(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->refreshView(Landroid/graphics/Bitmap;Z)V

    .line 611
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/app/video/editor/external/Element;->getType()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_5

    .line 612
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/app/video/editor/external/Element;->getDuration()J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/sec/android/app/ve/util/CommonUtils;->getTimeInText(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->setMediaDuration(Ljava/lang/String;)V

    :cond_5
    :goto_5
    move-object v8, v2

    .line 635
    goto/16 :goto_1

    .line 583
    :cond_6
    const/4 v3, -0x1

    iput v3, v12, Landroid/widget/GridLayout$LayoutParams;->width:I

    .line 584
    if-nez p1, :cond_7

    .line 585
    const/4 v3, 0x0

    iput v3, v12, Landroid/widget/GridLayout$LayoutParams;->topMargin:I

    .line 589
    :goto_6
    const v3, 0x7f0602f6

    invoke-static {v3}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v3

    iput v3, v12, Landroid/widget/GridLayout$LayoutParams;->height:I

    goto :goto_2

    .line 587
    :cond_7
    const v3, 0x7f060344

    invoke-static {v3}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v3

    iput v3, v12, Landroid/widget/GridLayout$LayoutParams;->topMargin:I

    goto :goto_6

    .line 596
    :cond_8
    const/4 v7, 0x0

    goto :goto_3

    .line 603
    :cond_9
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/app/video/editor/external/Element;->isWQHD()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 604
    const v3, 0x7f0d0031

    invoke-virtual {v2, v3}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/ImageView;

    .line 605
    .local v15, "wqhdIcon":Landroid/widget/ImageView;
    const/4 v3, 0x0

    invoke-virtual {v15, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_4

    .line 615
    .end local v15    # "wqhdIcon":Landroid/widget/ImageView;
    :cond_a
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/app/video/editor/external/Element;->getType()I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_c

    .line 616
    sget-object v3, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/app/video/editor/external/Element;->getFilePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/storycam/AppDataManager;->getThumbnail(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v10

    .line 617
    .local v10, "bmp":Landroid/graphics/Bitmap;
    if-eqz v10, :cond_b

    invoke-virtual {v10}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v3

    if-nez v3, :cond_b

    .line 618
    const/4 v3, 0x1

    invoke-virtual {v2, v10, v3}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->refreshView(Landroid/graphics/Bitmap;Z)V

    .line 622
    :goto_7
    const/4 v3, 0x0

    invoke-virtual {v14, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_5

    .line 620
    :cond_b
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mImageUnsupported:Landroid/graphics/Bitmap;

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->refreshView(Landroid/graphics/Bitmap;Z)V

    goto :goto_7

    .line 624
    .end local v10    # "bmp":Landroid/graphics/Bitmap;
    :cond_c
    sget-object v3, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/app/video/editor/external/Element;->getFilePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/storycam/AppDataManager;->getThumbnail(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v10

    .line 625
    .restart local v10    # "bmp":Landroid/graphics/Bitmap;
    if-eqz v10, :cond_d

    invoke-virtual {v10}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v3

    if-nez v3, :cond_d

    .line 626
    const/4 v3, 0x1

    invoke-virtual {v2, v10, v3}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->refreshView(Landroid/graphics/Bitmap;Z)V

    .line 630
    :goto_8
    const/4 v3, 0x0

    invoke-virtual {v14, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_5

    .line 628
    :cond_d
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mVideoUnsupported:Landroid/graphics/Bitmap;

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->refreshView(Landroid/graphics/Bitmap;Z)V

    goto :goto_8

    .line 634
    .end local v10    # "bmp":Landroid/graphics/Bitmap;
    :cond_e
    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->refreshView(Landroid/graphics/Bitmap;Z)V

    goto/16 :goto_5
.end method

.method private init()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x32

    .line 428
    sget-object v3, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    iget-object v4, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mThumbCacheListener:Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheUpdateListener;

    invoke-virtual {v3, v4}, Lcom/sec/android/app/storycam/AppDataManager;->addThumbnailCacheUpdateListener(Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheUpdateListener;)V

    .line 429
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->getContext()Landroid/content/Context;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    sput-object v3, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    .line 430
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mInflater:Landroid/view/LayoutInflater;

    .line 431
    iget-object v3, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mImageUnsupported:Landroid/graphics/Bitmap;

    if-nez v3, :cond_0

    .line 432
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f020023

    invoke-static {v3, v4}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mImageUnsupported:Landroid/graphics/Bitmap;

    .line 433
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mVideoUnsupported:Landroid/graphics/Bitmap;

    if-nez v3, :cond_1

    .line 434
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f020016

    invoke-static {v3, v4}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mVideoUnsupported:Landroid/graphics/Bitmap;

    .line 436
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v2

    .line 437
    .local v2, "transition":Landroid/animation/LayoutTransition;
    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Landroid/animation/LayoutTransition;->getAnimator(I)Landroid/animation/Animator;

    move-result-object v0

    .line 438
    .local v0, "anim":Landroid/animation/Animator;
    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Landroid/animation/LayoutTransition;->getAnimator(I)Landroid/animation/Animator;

    move-result-object v1

    .line 439
    .local v1, "animDis":Landroid/animation/Animator;
    invoke-virtual {v0, v6, v7}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 440
    invoke-virtual {v1, v6, v7}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 442
    invoke-direct {p0}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->doLayout()V

    .line 443
    invoke-direct {p0}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->showToastForShortClips()V

    .line 444
    return-void
.end method

.method private refreshAllViews()V
    .locals 17

    .prologue
    .line 640
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->getChildCount()I

    move-result v8

    .line 641
    .local v8, "childCount":I
    sget-object v1, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v1}, Lcom/sec/android/app/storycam/AppDataManager;->getOriginalTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v11

    .line 642
    .local v11, "trans":Lcom/samsung/app/video/editor/external/TranscodeElement;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-lt v2, v8, :cond_0

    .line 694
    return-void

    .line 643
    :cond_0
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v14

    .line 644
    .local v14, "view":Landroid/view/View;
    if-eqz v14, :cond_2

    instance-of v1, v14, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;

    if-eqz v1, :cond_2

    .line 645
    invoke-virtual {v11, v2}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElement(I)Lcom/samsung/app/video/editor/external/Element;

    move-result-object v9

    .local v9, "element":Lcom/samsung/app/video/editor/external/Element;
    move-object v1, v14

    .line 646
    check-cast v1, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;

    const v3, 0x7f0d002e

    invoke-virtual {v1, v3}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/RelativeLayout;

    .line 647
    .local v13, "unsupLayout":Landroid/widget/RelativeLayout;
    const/16 v1, 0x8

    invoke-virtual {v13, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    move-object v1, v14

    .line 648
    check-cast v1, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;

    const v3, 0x7f0d0030

    invoke-virtual {v1, v3}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/ImageView;

    .line 649
    .local v12, "uhdIcon":Landroid/widget/ImageView;
    const/16 v1, 0x8

    invoke-virtual {v12, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    move-object v1, v14

    .line 650
    check-cast v1, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;

    const v3, 0x7f0d0031

    invoke-virtual {v1, v3}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/ImageView;

    .line 651
    .local v15, "wqhdIcon":Landroid/widget/ImageView;
    const/16 v1, 0x8

    invoke-virtual {v15, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 652
    if-eqz v9, :cond_2

    move-object v1, v14

    .line 653
    check-cast v1, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mGridItemListener:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MediaGridItemInterface;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mThumbDataProvider:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$ThumbnailDataProvider;

    invoke-virtual {v9}, Lcom/samsung/app/video/editor/external/Element;->getType()I

    move-result v5

    invoke-virtual {v9}, Lcom/samsung/app/video/editor/external/Element;->getID()I

    move-result v6

    const/16 v16, -0x1

    move/from16 v0, v16

    if-eq v6, v0, :cond_3

    const/4 v6, 0x1

    :goto_1
    invoke-virtual/range {v1 .. v6}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->setMetaData(ILcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MediaGridItemInterface;Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$ThumbnailDataProvider;IZ)V

    .line 654
    invoke-virtual {v9}, Lcom/samsung/app/video/editor/external/Element;->getType()I

    move-result v1

    const/4 v3, 0x1

    if-ne v1, v3, :cond_1

    .line 655
    invoke-virtual {v9}, Lcom/samsung/app/video/editor/external/Element;->isUHD()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 656
    const/4 v1, 0x0

    invoke-virtual {v12, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 661
    :cond_1
    :goto_2
    invoke-virtual {v9}, Lcom/samsung/app/video/editor/external/Element;->getID()I

    move-result v1

    const/4 v3, -0x1

    if-eq v1, v3, :cond_5

    move-object v1, v14

    .line 662
    check-cast v1, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;

    sget-object v3, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v9}, Lcom/samsung/app/video/editor/external/Element;->getFilePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/storycam/AppDataManager;->getThumbnail(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v1, v3, v4}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->refreshView(Landroid/graphics/Bitmap;Z)V

    .line 684
    :goto_3
    invoke-virtual {v14}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v10

    check-cast v10, Landroid/widget/GridLayout$LayoutParams;

    .line 685
    .local v10, "params":Landroid/widget/GridLayout$LayoutParams;
    if-nez v2, :cond_9

    sget-object v1, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    if-eqz v1, :cond_9

    sget-object v1, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    invoke-interface {v1}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;->getCurrentConfig()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x1

    if-ne v1, v3, :cond_9

    .line 686
    const/4 v1, 0x0

    iput v1, v10, Landroid/widget/GridLayout$LayoutParams;->topMargin:I

    .line 690
    :goto_4
    invoke-virtual {v14, v10}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 642
    .end local v9    # "element":Lcom/samsung/app/video/editor/external/Element;
    .end local v10    # "params":Landroid/widget/GridLayout$LayoutParams;
    .end local v12    # "uhdIcon":Landroid/widget/ImageView;
    .end local v13    # "unsupLayout":Landroid/widget/RelativeLayout;
    .end local v15    # "wqhdIcon":Landroid/widget/ImageView;
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    .line 653
    .restart local v9    # "element":Lcom/samsung/app/video/editor/external/Element;
    .restart local v12    # "uhdIcon":Landroid/widget/ImageView;
    .restart local v13    # "unsupLayout":Landroid/widget/RelativeLayout;
    .restart local v15    # "wqhdIcon":Landroid/widget/ImageView;
    :cond_3
    const/4 v6, 0x0

    goto :goto_1

    .line 657
    :cond_4
    invoke-virtual {v9}, Lcom/samsung/app/video/editor/external/Element;->isWQHD()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 658
    const/4 v1, 0x0

    invoke-virtual {v15, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2

    .line 664
    :cond_5
    invoke-virtual {v9}, Lcom/samsung/app/video/editor/external/Element;->getType()I

    move-result v1

    const/4 v3, 0x2

    if-ne v1, v3, :cond_7

    .line 665
    sget-object v1, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v9}, Lcom/samsung/app/video/editor/external/Element;->getFilePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/sec/android/app/storycam/AppDataManager;->getThumbnail(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 666
    .local v7, "bmp":Landroid/graphics/Bitmap;
    if-eqz v7, :cond_6

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-nez v1, :cond_6

    move-object v1, v14

    .line 667
    check-cast v1, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;

    const/4 v3, 0x1

    invoke-virtual {v1, v7, v3}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->refreshView(Landroid/graphics/Bitmap;Z)V

    .line 671
    :goto_5
    const/4 v1, 0x0

    invoke-virtual {v13, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_3

    :cond_6
    move-object v1, v14

    .line 669
    check-cast v1, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mImageUnsupported:Landroid/graphics/Bitmap;

    const/4 v4, 0x1

    invoke-virtual {v1, v3, v4}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->refreshView(Landroid/graphics/Bitmap;Z)V

    goto :goto_5

    .line 673
    .end local v7    # "bmp":Landroid/graphics/Bitmap;
    :cond_7
    sget-object v1, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v9}, Lcom/samsung/app/video/editor/external/Element;->getFilePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/sec/android/app/storycam/AppDataManager;->getThumbnail(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 674
    .restart local v7    # "bmp":Landroid/graphics/Bitmap;
    if-eqz v7, :cond_8

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-nez v1, :cond_8

    move-object v1, v14

    .line 675
    check-cast v1, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;

    const/4 v3, 0x1

    invoke-virtual {v1, v7, v3}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->refreshView(Landroid/graphics/Bitmap;Z)V

    .line 679
    :goto_6
    const/4 v1, 0x0

    invoke-virtual {v13, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_3

    :cond_8
    move-object v1, v14

    .line 677
    check-cast v1, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mVideoUnsupported:Landroid/graphics/Bitmap;

    const/4 v4, 0x1

    invoke-virtual {v1, v3, v4}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->refreshView(Landroid/graphics/Bitmap;Z)V

    goto :goto_6

    .line 689
    .end local v7    # "bmp":Landroid/graphics/Bitmap;
    .restart local v10    # "params":Landroid/widget/GridLayout$LayoutParams;
    :cond_9
    const v1, 0x7f060344

    invoke-static {v1}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v1

    iput v1, v10, Landroid/widget/GridLayout$LayoutParams;->topMargin:I

    goto :goto_4
.end method

.method private resetDragState()V
    .locals 2

    .prologue
    .line 697
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 698
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mCurrentDropIndex:I

    .line 699
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mDraggingView:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;

    .line 700
    return-void
.end method

.method private showToastForShortClips()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 447
    const/4 v0, 0x0

    .line 448
    .local v0, "elem":Lcom/samsung/app/video/editor/external/Element;
    sget-object v4, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v4}, Lcom/sec/android/app/storycam/AppDataManager;->getOriginalTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v3

    .line 449
    .local v3, "trans":Lcom/samsung/app/video/editor/external/TranscodeElement;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v4

    if-lt v1, v4, :cond_0

    .line 457
    :goto_1
    return-void

    .line 450
    :cond_0
    invoke-virtual {v3, v1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElement(I)Lcom/samsung/app/video/editor/external/Element;

    move-result-object v0

    .line 451
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/samsung/app/video/editor/external/Element;->getType()I

    move-result v4

    if-ne v4, v9, :cond_1

    invoke-virtual {v0}, Lcom/samsung/app/video/editor/external/Element;->getDuration()J

    move-result-wide v4

    const-wide/16 v6, 0x3a98

    cmp-long v4, v4, v6

    if-gtz v4, :cond_1

    .line 452
    const v4, 0x7f070296

    invoke-static {v4}, Lcom/sec/android/app/ve/VEApp;->getStringValue(I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v9, [Ljava/lang/Object;

    const/16 v6, 0xf

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 453
    .local v2, "toastString":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v2, v8}, Lcom/sec/android/app/ve/VEApp;->showToast(Landroid/content/Context;Ljava/lang/String;I)V

    goto :goto_1

    .line 449
    .end local v2    # "toastString":Ljava/lang/String;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method protected onAttachedToWindow()V
    .locals 0

    .prologue
    .line 411
    invoke-super {p0}, Landroid/widget/GridLayout;->onAttachedToWindow()V

    .line 412
    invoke-direct {p0}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->init()V

    .line 413
    return-void
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 423
    invoke-super {p0, p1}, Landroid/widget/GridLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 424
    invoke-direct {p0}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->doLayout()V

    .line 425
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 417
    invoke-super {p0}, Landroid/widget/GridLayout;->onDetachedFromWindow()V

    .line 418
    sget-object v0, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    iget-object v1, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mThumbCacheListener:Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheUpdateListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/storycam/AppDataManager;->removeThumbnailCacheUpdateListener(Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheUpdateListener;)V

    .line 419
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 708
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mTouchX:F

    .line 709
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mTouchY:F

    .line 710
    invoke-super {p0, p1}, Landroid/widget/GridLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public refresh()V
    .locals 0

    .prologue
    .line 703
    invoke-direct {p0}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->doLayout()V

    .line 704
    return-void
.end method

.method public declared-synchronized startAutoScroll(I)V
    .locals 4
    .param p1, "direction"    # I

    .prologue
    .line 191
    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mAutoScroll:Z

    if-nez v1, :cond_0

    .line 192
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mAutoScroll:Z

    .line 193
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 194
    .local v0, "msg":Landroid/os/Message;
    const/16 v1, 0x65

    iput v1, v0, Landroid/os/Message;->what:I

    .line 195
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 196
    iget-object v1, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x14

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 198
    .end local v0    # "msg":Landroid/os/Message;
    :cond_0
    monitor-exit p0

    return-void

    .line 191
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized stopAutoScoll(Landroid/view/DragEvent;)V
    .locals 1
    .param p1, "event"    # Landroid/view/DragEvent;

    .prologue
    .line 201
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mAutoScroll:Z

    if-eqz v0, :cond_0

    .line 202
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView;->mAutoScroll:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 204
    :cond_0
    monitor-exit p0

    return-void

    .line 201
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
