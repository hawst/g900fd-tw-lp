.class public Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$UnsupportedContentsDialog;
.super Lcom/sec/android/app/ve/common/AndroidUtils$DlgFragment;
.source "VideoEditorLiteActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "UnsupportedContentsDialog"
.end annotation


# static fields
.field public static final ALL_UNSUPPORTED:Ljava/lang/String; = "ALL_UNSUPPORTED"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2299
    invoke-direct {p0}, Lcom/sec/android/app/ve/common/AndroidUtils$DlgFragment;-><init>()V

    .line 2300
    return-void
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 2309
    invoke-super {p0, p1}, Lcom/sec/android/app/ve/common/AndroidUtils$DlgFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 2310
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$UnsupportedContentsDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$UnsupportedContentsDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p1, Landroid/content/res/Configuration;->fontScale:F

    # getter for: Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->fontScale_value:F
    invoke-static {}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->access$7()F

    move-result v1

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    .line 2311
    iget v0, p1, Landroid/content/res/Configuration;->fontScale:F

    invoke-static {v0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->access$8(F)V

    .line 2312
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$UnsupportedContentsDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/ve/common/AndroidUtils;->dismissDialog(Landroid/app/Activity;)V

    .line 2315
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 2304
    invoke-super {p0, p1}, Lcom/sec/android/app/ve/common/AndroidUtils$DlgFragment;->onCreate(Landroid/os/Bundle;)V

    .line 2305
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 2319
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$UnsupportedContentsDialog;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 2321
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    const-string v2, "ro.product.device"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2322
    .local v1, "modelName":Ljava/lang/String;
    if-eqz v1, :cond_0

    const-string v2, "m2"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2323
    :cond_0
    const v2, 0x7f070263

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 2330
    :goto_0
    const v2, 0x7f070265

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 2331
    const v3, 0x7f0700ab

    new-instance v4, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$UnsupportedContentsDialog$1;

    invoke-direct {v4, p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$UnsupportedContentsDialog$1;-><init>(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$UnsupportedContentsDialog;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 2338
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    return-object v2

    .line 2326
    :cond_1
    const v2, 0x7f070264

    invoke-static {v2}, Lcom/sec/android/app/ve/VEApp;->getStringValue(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "UHD/WQHD"

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    goto :goto_0
.end method
