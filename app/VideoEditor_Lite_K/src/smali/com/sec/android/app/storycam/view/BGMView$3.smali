.class Lcom/sec/android/app/storycam/view/BGMView$3;
.super Ljava/lang/Object;
.source "BGMView.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/storycam/view/BGMView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/storycam/view/BGMView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/storycam/view/BGMView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/storycam/view/BGMView$3;->this$0:Lcom/sec/android/app/storycam/view/BGMView;

    .line 197
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 10
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v4, 0x1

    .line 202
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView$3;->this$0:Lcom/sec/android/app/storycam/view/BGMView;

    invoke-virtual {v0}, Lcom/sec/android/app/storycam/view/BGMView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/telephony/TelephonyManager;

    .line 203
    .local v9, "telephonyManager":Landroid/telephony/TelephonyManager;
    invoke-virtual {v9}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v0

    if-eqz v0, :cond_0

    .line 204
    const v0, 0x7f0700f9

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/VEApp;->showToast(II)Landroid/widget/Toast;

    .line 229
    .end local v9    # "telephonyManager":Landroid/telephony/TelephonyManager;
    :goto_0
    return-void

    .line 208
    .restart local v9    # "telephonyManager":Landroid/telephony/TelephonyManager;
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView$3;->this$0:Lcom/sec/android/app/storycam/view/BGMView;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/android/app/storycam/view/BGMView;->access$1(Lcom/sec/android/app/storycam/view/BGMView;Z)V

    .line 209
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView$3;->this$0:Lcom/sec/android/app/storycam/view/BGMView;

    # getter for: Lcom/sec/android/app/storycam/view/BGMView;->mAudioManager:Landroid/media/AudioManager;
    invoke-static {v0}, Lcom/sec/android/app/storycam/view/BGMView;->access$11(Lcom/sec/android/app/storycam/view/BGMView;)Landroid/media/AudioManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/storycam/view/BGMView$3;->this$0:Lcom/sec/android/app/storycam/view/BGMView;

    # getter for: Lcom/sec/android/app/storycam/view/BGMView;->mAudioChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;
    invoke-static {v1}, Lcom/sec/android/app/storycam/view/BGMView;->access$12(Lcom/sec/android/app/storycam/view/BGMView;)Landroid/media/AudioManager$OnAudioFocusChangeListener;

    move-result-object v1

    const/4 v2, 0x3

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    move-result v8

    .line 210
    .local v8, "lAudioFocus":I
    if-ne v8, v4, :cond_2

    .line 211
    invoke-static {}, Lcom/sec/android/app/storycam/VEAppSpecific;->getAppAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/ve/VEApp;->gBGMManager:Lcom/sec/android/app/ve/bgm/BGMManager;

    invoke-virtual {v1, p3}, Lcom/sec/android/app/ve/bgm/BGMManager;->getBGMFilePath(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/res/AssetManager;->openFd(Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;

    move-result-object v6

    .line 212
    .local v6, "afd":Landroid/content/res/AssetFileDescriptor;
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView$3;->this$0:Lcom/sec/android/app/storycam/view/BGMView;

    # getter for: Lcom/sec/android/app/storycam/view/BGMView;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v0}, Lcom/sec/android/app/storycam/view/BGMView;->access$0(Lcom/sec/android/app/storycam/view/BGMView;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->reset()V

    .line 213
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView$3;->this$0:Lcom/sec/android/app/storycam/view/BGMView;

    # getter for: Lcom/sec/android/app/storycam/view/BGMView;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v0}, Lcom/sec/android/app/storycam/view/BGMView;->access$0(Lcom/sec/android/app/storycam/view/BGMView;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v1

    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->getStartOffset()J

    move-result-wide v2

    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->getLength()J

    move-result-wide v4

    invoke-virtual/range {v0 .. v5}, Landroid/media/MediaPlayer;->setDataSource(Ljava/io/FileDescriptor;JJ)V

    .line 214
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView$3;->this$0:Lcom/sec/android/app/storycam/view/BGMView;

    # getter for: Lcom/sec/android/app/storycam/view/BGMView;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v0}, Lcom/sec/android/app/storycam/view/BGMView;->access$0(Lcom/sec/android/app/storycam/view/BGMView;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->prepare()V

    .line 215
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView$3;->this$0:Lcom/sec/android/app/storycam/view/BGMView;

    # getter for: Lcom/sec/android/app/storycam/view/BGMView;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v0}, Lcom/sec/android/app/storycam/view/BGMView;->access$0(Lcom/sec/android/app/storycam/view/BGMView;)Landroid/media/MediaPlayer;

    move-result-object v0

    const/high16 v1, 0x3f000000    # 0.5f

    const/high16 v2, 0x3f000000    # 0.5f

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaPlayer;->setVolume(FF)V

    .line 216
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView$3;->this$0:Lcom/sec/android/app/storycam/view/BGMView;

    # getter for: Lcom/sec/android/app/storycam/view/BGMView;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v0}, Lcom/sec/android/app/storycam/view/BGMView;->access$0(Lcom/sec/android/app/storycam/view/BGMView;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    .line 221
    .end local v6    # "afd":Landroid/content/res/AssetFileDescriptor;
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView$3;->this$0:Lcom/sec/android/app/storycam/view/BGMView;

    # getter for: Lcom/sec/android/app/storycam/view/BGMView;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;
    invoke-static {v0}, Lcom/sec/android/app/storycam/view/BGMView;->access$2(Lcom/sec/android/app/storycam/view/BGMView;)Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;->setUserSelectedBgm(Z)V

    .line 222
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView$3;->this$0:Lcom/sec/android/app/storycam/view/BGMView;

    invoke-static {v0, p3}, Lcom/sec/android/app/storycam/view/BGMView;->access$7(Lcom/sec/android/app/storycam/view/BGMView;I)V

    .line 223
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView$3;->this$0:Lcom/sec/android/app/storycam/view/BGMView;

    # getter for: Lcom/sec/android/app/storycam/view/BGMView;->mBgmTitle:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/storycam/view/BGMView;->access$8(Lcom/sec/android/app/storycam/view/BGMView;)Landroid/widget/TextView;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/ve/VEApp;->gBGMManager:Lcom/sec/android/app/ve/bgm/BGMManager;

    iget-object v2, p0, Lcom/sec/android/app/storycam/view/BGMView$3;->this$0:Lcom/sec/android/app/storycam/view/BGMView;

    # getter for: Lcom/sec/android/app/storycam/view/BGMView;->mSelectedPos:I
    invoke-static {v2}, Lcom/sec/android/app/storycam/view/BGMView;->access$3(Lcom/sec/android/app/storycam/view/BGMView;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/ve/bgm/BGMManager;->getBGMName(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 224
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView$3;->this$0:Lcom/sec/android/app/storycam/view/BGMView;

    # getter for: Lcom/sec/android/app/storycam/view/BGMView;->mListAdapter:Lcom/sec/android/app/storycam/view/BGMView$BGMmBgmIconAdapter;
    invoke-static {v0}, Lcom/sec/android/app/storycam/view/BGMView;->access$10(Lcom/sec/android/app/storycam/view/BGMView;)Lcom/sec/android/app/storycam/view/BGMView$BGMmBgmIconAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/storycam/view/BGMView$BGMmBgmIconAdapter;->notifyDataSetChanged()V

    .line 225
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView$3;->this$0:Lcom/sec/android/app/storycam/view/BGMView;

    # getter for: Lcom/sec/android/app/storycam/view/BGMView;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;
    invoke-static {v0}, Lcom/sec/android/app/storycam/view/BGMView;->access$2(Lcom/sec/android/app/storycam/view/BGMView;)Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;->requestForOptionMenuRefresh()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 226
    .end local v8    # "lAudioFocus":I
    .end local v9    # "telephonyManager":Landroid/telephony/TelephonyManager;
    :catch_0
    move-exception v7

    .line 227
    .local v7, "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 218
    .end local v7    # "e":Ljava/io/IOException;
    .restart local v8    # "lAudioFocus":I
    .restart local v9    # "telephonyManager":Landroid/telephony/TelephonyManager;
    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView$3;->this$0:Lcom/sec/android/app/storycam/view/BGMView;

    # getter for: Lcom/sec/android/app/storycam/view/BGMView;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v0}, Lcom/sec/android/app/storycam/view/BGMView;->access$0(Lcom/sec/android/app/storycam/view/BGMView;)Landroid/media/MediaPlayer;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView$3;->this$0:Lcom/sec/android/app/storycam/view/BGMView;

    # getter for: Lcom/sec/android/app/storycam/view/BGMView;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v0}, Lcom/sec/android/app/storycam/view/BGMView;->access$0(Lcom/sec/android/app/storycam/view/BGMView;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 219
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView$3;->this$0:Lcom/sec/android/app/storycam/view/BGMView;

    # getter for: Lcom/sec/android/app/storycam/view/BGMView;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v0}, Lcom/sec/android/app/storycam/view/BGMView;->access$0(Lcom/sec/android/app/storycam/view/BGMView;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method
