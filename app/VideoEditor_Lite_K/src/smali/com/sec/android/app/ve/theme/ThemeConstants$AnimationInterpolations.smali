.class public Lcom/sec/android/app/ve/theme/ThemeConstants$AnimationInterpolations;
.super Ljava/lang/Object;
.source "ThemeConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/ve/theme/ThemeConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AnimationInterpolations"
.end annotation


# static fields
.field public static final VE_INTR_CUSTOM:I = 0x8

.field public static final VE_INTR_EASEIN:I = 0x1

.field public static final VE_INTR_EASEINOUT:I = 0x6

.field public static final VE_INTR_EASEOUT:I = 0x2

.field public static final VE_INTR_EASEOUTIN:I = 0x5

.field public static final VE_INTR_EXPIN:I = 0x3

.field public static final VE_INTR_EXPOUT:I = 0x4

.field public static final VE_INTR_INVALID:I = -0x1

.field public static final VE_INTR_LINEAR:I = 0x0

.field public static final VE_INTR_OSCILLATORY:I = 0x7

.field public static final VE_INTR_QUINTOUT50:I = 0x9

.field public static final VE_INTR_QUINTOUT80:I = 0xa

.field public static final VE_INTR_SINEIN33:I = 0xb

.field public static final VE_INTR_SINEINOUT33:I = 0xd

.field public static final VE_INTR_SINEINOUT50:I = 0xe

.field public static final VE_INTR_SINEINOUT60:I = 0xf

.field public static final VE_INTR_SINEINOUT70:I = 0x10

.field public static final VE_INTR_SINEINOUT80:I = 0x11

.field public static final VE_INTR_SINEINOUT90:I = 0x12

.field public static final VE_INTR_SINEOUT33:I = 0xc


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 141
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
