.class Lcom/sec/android/app/storycam/view/PreviewViewGroup$2;
.super Lcom/sec/android/app/ve/PreviewPlayerInterface$Adapter;
.source "PreviewViewGroup.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/storycam/view/PreviewViewGroup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$2;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    .line 403
    invoke-direct {p0}, Lcom/sec/android/app/ve/PreviewPlayerInterface$Adapter;-><init>()V

    return-void
.end method

.method private onSurfaceUpdate()V
    .locals 10

    .prologue
    const/4 v6, 0x0

    .line 407
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$2;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    # getter for: Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;
    invoke-static {v0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$4(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$2;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    # getter for: Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;
    invoke-static {v0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$4(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;->getCurrentState()Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;->STATE_DEFAULT:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    if-ne v0, v1, :cond_0

    .line 408
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$2;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    # getter for: Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mProgressBar:Landroid/widget/SeekBar;
    invoke-static {v0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$2(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)Landroid/widget/SeekBar;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 409
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$2;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    invoke-virtual {v0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->refreshPreviewToCurrentSeekBar()V

    .line 421
    :cond_0
    :goto_0
    return-void

    .line 411
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$2;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    # getter for: Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mCurrTransElement:Lcom/samsung/app/video/editor/external/TranscodeElement;
    invoke-static {v0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$1(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 412
    const/4 v2, 0x0

    .line 413
    .local v2, "current":Lcom/samsung/app/video/editor/external/Element;
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$2;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    # getter for: Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mCurrTransElement:Lcom/samsung/app/video/editor/external/TranscodeElement;
    invoke-static {v0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$1(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElement(I)Lcom/samsung/app/video/editor/external/Element;

    move-result-object v2

    .line 414
    const-wide/16 v4, 0x0

    .line 415
    .local v4, "time":J
    const-wide/16 v8, 0x0

    .line 416
    .local v8, "storyBoradtime":J
    iget-object v1, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$2;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    const/4 v3, 0x0

    .line 417
    long-to-float v7, v8

    .line 416
    invoke-virtual/range {v1 .. v7}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->refreshPreview(Lcom/samsung/app/video/editor/external/Element;Lcom/samsung/app/video/editor/external/Element;JIF)V

    goto :goto_0
.end method


# virtual methods
.method public getCurrentTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;
    .locals 1

    .prologue
    .line 492
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$2;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    # getter for: Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mCurrTransElement:Lcom/samsung/app/video/editor/external/TranscodeElement;
    invoke-static {v0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$1(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v0

    return-object v0
.end method

.method public onAudioFocusLost()V
    .locals 2

    .prologue
    .line 498
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$2;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    # getter for: Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mFSListener:Lcom/sec/android/app/storycam/view/PreviewViewGroup$PlayerControlInterface;
    invoke-static {v0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$9(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)Lcom/sec/android/app/storycam/view/PreviewViewGroup$PlayerControlInterface;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup$PlayerControlInterface;->pause_player()V

    .line 499
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$2;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->setPlayButtonVisibility(I)V

    .line 500
    return-void
.end method

.method public onCompleted()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 442
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$2;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    # getter for: Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$5(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 444
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$2;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    # getter for: Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mProgressBar:Landroid/widget/SeekBar;
    invoke-static {v0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$2(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)Landroid/widget/SeekBar;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 445
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$2;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->setPlayButtonVisibility(I)V

    .line 446
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$2;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    # invokes: Lcom/sec/android/app/storycam/view/PreviewViewGroup;->refreshPreviewForCurrentSeekBarPosition(I)V
    invoke-static {v0, v2}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$6(Lcom/sec/android/app/storycam/view/PreviewViewGroup;I)V

    .line 447
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$2;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    # getter for: Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mStoryBoardCurrTime:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$7(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)Landroid/widget/TextView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 448
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$2;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    invoke-virtual {v0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->updateCurrentTimeContentDesc()V

    .line 449
    :cond_0
    return-void
.end method

.method public onError()V
    .locals 2

    .prologue
    .line 483
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$2;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    # getter for: Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mFSListener:Lcom/sec/android/app/storycam/view/PreviewViewGroup$PlayerControlInterface;
    invoke-static {v0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$9(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)Lcom/sec/android/app/storycam/view/PreviewViewGroup$PlayerControlInterface;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup$PlayerControlInterface;->pause_player()V

    .line 484
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$2;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->setPlayButtonVisibility(I)V

    .line 485
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$2;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    # getter for: Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mStoryBoardCurrTime:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$7(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)Landroid/widget/TextView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 486
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$2;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    invoke-virtual {v0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->updateCurrentTimeContentDesc()V

    .line 487
    :cond_0
    return-void
.end method

.method public onPlayed()V
    .locals 2

    .prologue
    .line 435
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$2;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    # getter for: Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$5(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 436
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$2;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->setPlayButtonVisibility(I)V

    .line 437
    return-void
.end method

.method public onSeeked(J)V
    .locals 0
    .param p1, "pos"    # J

    .prologue
    .line 478
    return-void
.end method

.method public onStopped()V
    .locals 6

    .prologue
    .line 454
    iget-object v4, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$2;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->setPlayButtonVisibility(I)V

    .line 455
    iget-object v4, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$2;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    # getter for: Lcom/sec/android/app/storycam/view/PreviewViewGroup;->wasPlayingBeforeProgressTouch:Z
    invoke-static {v4}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$8(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$2;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    # getter for: Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mCurrTransElement:Lcom/samsung/app/video/editor/external/TranscodeElement;
    invoke-static {v4}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$1(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$2;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    # getter for: Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mProgressBar:Landroid/widget/SeekBar;
    invoke-static {v4}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$2(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)Landroid/widget/SeekBar;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 457
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/NativeInterface;->getCurrentPosition()I

    move-result v4

    int-to-long v0, v4

    .line 458
    .local v0, "cur_playing_time":J
    iget-object v4, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$2;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    # getter for: Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mCurrTransElement:Lcom/samsung/app/video/editor/external/TranscodeElement;
    invoke-static {v4}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$1(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getTotalDuration()J

    move-result-wide v2

    .line 459
    .local v2, "total_time":J
    cmp-long v4, v0, v2

    if-ltz v4, :cond_2

    .line 461
    iget-object v4, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$2;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    # getter for: Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mProgressBar:Landroid/widget/SeekBar;
    invoke-static {v4}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$2(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)Landroid/widget/SeekBar;

    move-result-object v4

    long-to-int v5, v2

    invoke-virtual {v4, v5}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 468
    :goto_0
    iget-object v4, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$2;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    invoke-virtual {v4}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->refreshPreviewToCurrentSeekBar()V

    .line 470
    .end local v0    # "cur_playing_time":J
    .end local v2    # "total_time":J
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$2;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    # getter for: Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mStoryBoardCurrTime:Landroid/widget/TextView;
    invoke-static {v4}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$7(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)Landroid/widget/TextView;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 471
    iget-object v4, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$2;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    invoke-virtual {v4}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->updateCurrentTimeContentDesc()V

    .line 472
    :cond_1
    return-void

    .line 465
    .restart local v0    # "cur_playing_time":J
    .restart local v2    # "total_time":J
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$2;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    # getter for: Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mProgressBar:Landroid/widget/SeekBar;
    invoke-static {v4}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$2(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)Landroid/widget/SeekBar;

    move-result-object v4

    long-to-int v5, v0

    invoke-virtual {v4, v5}, Landroid/widget/SeekBar;->setProgress(I)V

    goto :goto_0
.end method

.method public onSurfaceChanged()V
    .locals 0

    .prologue
    .line 429
    invoke-direct {p0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup$2;->onSurfaceUpdate()V

    .line 430
    return-void
.end method

.method public onSurfaceCreated()V
    .locals 0

    .prologue
    .line 424
    invoke-direct {p0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup$2;->onSurfaceUpdate()V

    .line 425
    return-void
.end method
