.class public Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper$ProjectShareInfoDialog;
.super Lcom/sec/android/app/ve/common/AndroidUtils$DlgFragment;
.source "ProjectCompatibilityHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ProjectShareInfoDialog"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 916
    invoke-direct {p0}, Lcom/sec/android/app/ve/common/AndroidUtils$DlgFragment;-><init>()V

    .line 917
    return-void
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 921
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper$ProjectShareInfoDialog;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 922
    sget v2, Lcom/sec/android/app/ve/R$string;->prj_sharing_menu:I

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 923
    sget v2, Lcom/sec/android/app/ve/R$string;->prj_sharing_connect_msg:I

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 924
    sget v2, Lcom/sec/android/app/ve/R$string;->ok:I

    new-instance v3, Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper$ProjectShareInfoDialog$1;

    invoke-direct {v3, p0}, Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper$ProjectShareInfoDialog$1;-><init>(Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper$ProjectShareInfoDialog;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 933
    sget v2, Lcom/sec/android/app/ve/R$string;->cancel:I

    new-instance v3, Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper$ProjectShareInfoDialog$2;

    invoke-direct {v3, p0}, Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper$ProjectShareInfoDialog$2;-><init>(Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper$ProjectShareInfoDialog;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 939
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 941
    .local v0, "dialog":Landroid/app/AlertDialog;
    return-object v0
.end method
