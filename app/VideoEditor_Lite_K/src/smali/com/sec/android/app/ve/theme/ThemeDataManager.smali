.class public Lcom/sec/android/app/ve/theme/ThemeDataManager;
.super Ljava/lang/Object;
.source "ThemeDataManager.java"


# static fields
.field private static final RAW_FILES_DIR:Ljava/lang/String; = "raw_files"

.field private static final THEME_DATA_DIR:Ljava/lang/String; = "themedata"

.field private static final _instance:Lcom/sec/android/app/ve/theme/ThemeDataManager;


# instance fields
.field private themeSlotGlobalDuration:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    new-instance v0, Lcom/sec/android/app/ve/theme/ThemeDataManager;

    invoke-direct {v0}, Lcom/sec/android/app/ve/theme/ThemeDataManager;-><init>()V

    sput-object v0, Lcom/sec/android/app/ve/theme/ThemeDataManager;->_instance:Lcom/sec/android/app/ve/theme/ThemeDataManager;

    .line 46
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/ve/theme/ThemeDataManager;->themeSlotGlobalDuration:I

    .line 49
    return-void
.end method

.method private applyThemeBGMMusic(Lcom/samsung/app/video/editor/external/Element;Lcom/samsung/app/video/editor/external/TranscodeElement;)V
    .locals 2
    .param p1, "bgmElement"    # Lcom/samsung/app/video/editor/external/Element;
    .param p2, "trans"    # Lcom/samsung/app/video/editor/external/TranscodeElement;

    .prologue
    .line 598
    invoke-virtual {p2}, Lcom/samsung/app/video/editor/external/TranscodeElement;->removeBGMMusic()V

    .line 599
    const-wide/16 v0, 0x0

    invoke-virtual {p1, v0, v1}, Lcom/samsung/app/video/editor/external/Element;->setStoryBoardStartTime(J)V

    .line 600
    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/Element;->getDuration()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lcom/samsung/app/video/editor/external/Element;->setStoryBoardEndTime(J)V

    .line 601
    invoke-virtual {p2, p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->addMusicEleList(Lcom/samsung/app/video/editor/external/Element;)V

    .line 603
    return-void
.end method

.method private createBGMElement(Ljava/lang/String;Ljava/lang/String;JLandroid/content/res/AssetManager;)Lcom/samsung/app/video/editor/external/Element;
    .locals 5
    .param p1, "bgmPath"    # Ljava/lang/String;
    .param p2, "bgmName"    # Ljava/lang/String;
    .param p3, "duration"    # J
    .param p5, "assetManager"    # Landroid/content/res/AssetManager;

    .prologue
    const/4 v4, 0x1

    .line 441
    new-instance v1, Lcom/samsung/app/video/editor/external/Element;

    invoke-direct {v1}, Lcom/samsung/app/video/editor/external/Element;-><init>()V

    .line 442
    .local v1, "lMusicElement":Lcom/samsung/app/video/editor/external/Element;
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/samsung/app/video/editor/external/Element;->setType(I)V

    .line 443
    invoke-virtual {v1, p1}, Lcom/samsung/app/video/editor/external/Element;->setFilePath(Ljava/lang/String;)V

    .line 446
    const-wide/16 v2, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/samsung/app/video/editor/external/Element;->setStartTime(J)V

    .line 447
    invoke-virtual {v1, p3, p4}, Lcom/samsung/app/video/editor/external/Element;->setEndTime(J)V

    .line 448
    invoke-virtual {v1, p3, p4}, Lcom/samsung/app/video/editor/external/Element;->setDuration(J)V

    .line 449
    new-instance v0, Lcom/samsung/app/video/editor/external/Edit;

    invoke-direct {v0}, Lcom/samsung/app/video/editor/external/Edit;-><init>()V

    .line 450
    .local v0, "edit":Lcom/samsung/app/video/editor/external/Edit;
    invoke-virtual {v0, v4}, Lcom/samsung/app/video/editor/external/Edit;->setType(I)V

    .line 451
    invoke-virtual {v1, v0}, Lcom/samsung/app/video/editor/external/Element;->addEdit(Lcom/samsung/app/video/editor/external/Edit;)V

    .line 452
    invoke-virtual {v1, p2}, Lcom/samsung/app/video/editor/external/Element;->setAudioDisplayName(Ljava/lang/String;)V

    .line 454
    invoke-virtual {v1, v4}, Lcom/samsung/app/video/editor/external/Element;->setAssetResource(Z)V

    .line 455
    return-object v1
.end method

.method private createBitmapAnimationData(Lorg/json/JSONObject;I)Lcom/samsung/app/video/editor/external/BitmapAnimationData;
    .locals 6
    .param p1, "bitmapAnimObj"    # Lorg/json/JSONObject;
    .param p2, "theme"    # I

    .prologue
    .line 421
    new-instance v0, Lcom/samsung/app/video/editor/external/BitmapAnimationData;

    invoke-direct {v0}, Lcom/samsung/app/video/editor/external/BitmapAnimationData;-><init>()V

    .line 423
    .local v0, "bitmapAnimationData":Lcom/samsung/app/video/editor/external/BitmapAnimationData;
    :try_start_0
    const-string v4, "BLENDING_FILES_START_TIME"

    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 424
    .local v3, "startFrame":I
    const-string v4, "BLENDING_FILES_END_TIME"

    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 425
    .local v2, "endFrame":I
    invoke-virtual {v0, v3}, Lcom/samsung/app/video/editor/external/BitmapAnimationData;->setOriginalStartFrameWithinElement(I)V

    .line 426
    invoke-virtual {v0, v2}, Lcom/samsung/app/video/editor/external/BitmapAnimationData;->setOriginalEndFrameWithinElement(I)V

    .line 427
    invoke-virtual {v0, v3}, Lcom/samsung/app/video/editor/external/BitmapAnimationData;->setStartFrameWithinElement(I)V

    .line 428
    invoke-virtual {v0, v2}, Lcom/samsung/app/video/editor/external/BitmapAnimationData;->setEndFrameWithinElement(I)V

    .line 429
    const-string v4, "BLENDING_FILES_START_INDEX"

    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v0, v4}, Lcom/samsung/app/video/editor/external/BitmapAnimationData;->setStartIndexInTemplate(I)V

    .line 430
    const-string v4, "BLENDING_FILES_END_INDEX"

    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v0, v4}, Lcom/samsung/app/video/editor/external/BitmapAnimationData;->setEndIndexInTemplate(I)V

    .line 431
    new-instance v4, Ljava/lang/StringBuilder;

    sget-object v5, Lcom/sec/android/app/ve/VEApp;->gThemeMgr:Lcom/sec/android/app/ve/theme/ThemeManager;

    invoke-virtual {v5, p2}, Lcom/sec/android/app/ve/theme/ThemeManager;->getThemeDataPath(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "BLENDING_FILES_PNG_FILE_NAME"

    invoke-virtual {p1, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/samsung/app/video/editor/external/BitmapAnimationData;->setPNGTemplate(Ljava/lang/String;)V

    .line 432
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Lcom/samsung/app/video/editor/external/BitmapAnimationData;->setAssetResource(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 437
    .end local v2    # "endFrame":I
    .end local v3    # "startFrame":I
    :goto_0
    return-object v0

    .line 434
    :catch_0
    move-exception v1

    .line 435
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private createDefaultCaption(Landroid/content/Context;Lorg/json/JSONObject;ILandroid/content/res/AssetManager;)Lcom/samsung/app/video/editor/external/ClipartParams;
    .locals 93
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "captionObj"    # Lorg/json/JSONObject;
    .param p3, "theme"    # I
    .param p4, "assetMgr"    # Landroid/content/res/AssetManager;

    .prologue
    .line 197
    new-instance v19, Lcom/samsung/app/video/editor/external/ClipartParams;

    invoke-direct/range {v19 .. v19}, Lcom/samsung/app/video/editor/external/ClipartParams;-><init>()V

    .line 199
    .local v19, "caption":Lcom/samsung/app/video/editor/external/ClipartParams;
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v75

    .line 200
    .local v75, "res":Landroid/content/res/Resources;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v67

    .line 201
    .local v67, "pkgName":Ljava/lang/String;
    const-string v6, "CAPTION_TEXT"

    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v41

    .line 202
    .local v41, "captionTextId":Ljava/lang/String;
    const-string v6, "string"

    move-object/from16 v0, v75

    move-object/from16 v1, v41

    move-object/from16 v2, v67

    invoke-virtual {v0, v1, v6, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v82

    .line 204
    .local v82, "stringId":I
    const-string v40, ""

    .line 205
    .local v40, "captionText":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v6

    iget-object v0, v6, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    move-object/from16 v63, v0

    .line 206
    .local v63, "locale":Ljava/util/Locale;
    invoke-virtual/range {v63 .. v63}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v64

    .line 207
    .local v64, "localeLang":Ljava/lang/String;
    sget-object v6, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v6}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v64

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v6}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v64

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    sget-object v6, Ljava/util/Locale;->UK:Ljava/util/Locale;

    invoke-virtual {v6}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v64

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 208
    sget-object v6, Ljava/util/Locale;->KOREA:Ljava/util/Locale;

    invoke-virtual {v6}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v64

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    sget-object v6, Ljava/util/Locale;->KOREAN:Ljava/util/Locale;

    invoke-virtual {v6}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v64

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 209
    :cond_0
    move-object/from16 v0, v75

    move/from16 v1, v82

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v40

    .line 215
    :goto_0
    const-string v6, "TOP"

    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v88

    .line 216
    .local v88, "top":I
    const-string v6, "LEFT"

    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v62

    .line 217
    .local v62, "left":I
    const-string v6, "BOTTOM"

    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v37

    .line 218
    .local v37, "bottom":I
    const-string v6, "RIGHT"

    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v76

    .line 219
    .local v76, "right":I
    const-string v6, "FONT_FILE"

    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 220
    .local v7, "fontFilePath":Ljava/lang/String;
    const-string v6, "themedata"

    invoke-virtual {v7, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 221
    new-instance v6, Ljava/lang/StringBuilder;

    sget-object v17, Lcom/sec/android/app/ve/VEApp;->gThemeMgr:Lcom/sec/android/app/ve/theme/ThemeManager;

    move-object/from16 v0, v17

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/sec/android/app/ve/theme/ThemeManager;->getThemeDataPath(I)Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-direct {v6, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v7

    .line 222
    :cond_1
    const/4 v8, 0x0

    .line 223
    .local v8, "isPlatformFont":Z
    const/4 v9, 0x0

    .line 226
    .local v9, "fontTextStyle":I
    :try_start_1
    const-string v6, "IS_PLATFORM_FONT"

    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v8

    .line 227
    const-string v6, "FONT_TEXT_STYLE"

    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result v9

    .line 232
    :goto_1
    const/16 v18, 0x1

    .line 234
    .local v18, "paintFlags":I
    :try_start_2
    const-string v6, "PAINT_FLAGS"

    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    move-result v18

    .line 238
    :goto_2
    :try_start_3
    const-string v6, "FONT_SIZE"

    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v10

    .line 239
    .local v10, "fontSize":I
    const-string v6, "FONT_COLOR"

    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v17, "color"

    move-object/from16 v0, v75

    move-object/from16 v1, v17

    move-object/from16 v2, v67

    invoke-virtual {v0, v6, v1, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v42

    .line 240
    .local v42, "colorId":I
    move-object/from16 v0, v75

    move/from16 v1, v42

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v11

    .line 241
    .local v11, "fontColor":I
    const-string v6, "SHADOW_COLOR"

    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v17, "color"

    move-object/from16 v0, v75

    move-object/from16 v1, v17

    move-object/from16 v2, v67

    invoke-virtual {v0, v6, v1, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v77

    .line 242
    .local v77, "shadowColorId":I
    move-object/from16 v0, v75

    move/from16 v1, v77

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v15

    .line 243
    .local v15, "shadowColor":I
    const-string v6, "SHADOW_X"

    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v13

    .line 244
    .local v13, "shadowX":I
    const-string v6, "SHADOW_Y"

    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v14

    .line 245
    .local v14, "shadowY":I
    const-string v6, "SHADOW_R"

    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v12

    .line 246
    .local v12, "shadowR":I
    const-string v6, "FONT_ALIGN"

    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v32

    .line 247
    .local v32, "align":I
    const/16 v23, 0x3

    .line 248
    .local v23, "alignFlag":I
    const-string v6, "DEFAULT_TRANSLATEX"

    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v20

    move-wide/from16 v0, v20

    double-to-float v0, v0

    move/from16 v50, v0

    .line 249
    .local v50, "defaultTranslateX":F
    const-string v6, "DEFAULT_TRANSLATEY"

    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v20

    move-wide/from16 v0, v20

    double-to-float v0, v0

    move/from16 v51, v0

    .line 250
    .local v51, "defaultTranslateY":F
    const-string v6, "DEFAULT_ALPHA"

    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v20

    move-wide/from16 v0, v20

    double-to-float v0, v0

    move/from16 v47, v0

    .line 251
    .local v47, "defaultAlpha":F
    const-string v6, "DEFAULT_SCALE"

    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v20

    move-wide/from16 v0, v20

    double-to-float v0, v0

    move/from16 v49, v0

    .line 252
    .local v49, "defaultScale":F
    const-string v6, "DEFAULT_ROTATE"

    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v20

    move-wide/from16 v0, v20

    double-to-float v0, v0

    move/from16 v48, v0

    .line 253
    .local v48, "defaultRotate":F
    const-string v6, "PIVOT_X"

    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v20

    move-wide/from16 v0, v20

    double-to-float v0, v0

    move/from16 v46, v0

    .line 254
    .local v46, "defPivotX":F
    const-string v6, "PIVOT_Y"

    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    move-result-wide v20

    move-wide/from16 v0, v20

    double-to-float v0, v0

    move/from16 v31, v0

    .line 255
    .local v31, "defPivotY":F
    const/16 v73, 0x0

    .line 257
    .local v73, "rawFilePath":Ljava/lang/String;
    :try_start_4
    const-string v6, "CAPTION_RAW_FILE"

    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v73

    .line 258
    new-instance v6, Ljava/lang/StringBuilder;

    sget-object v17, Lcom/sec/android/app/ve/VEApp;->gThemeMgr:Lcom/sec/android/app/ve/theme/ThemeManager;

    move-object/from16 v0, v17

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/sec/android/app/ve/theme/ThemeManager;->getThemeDataPath(I)Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-direct {v6, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v73

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_4
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_3
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    move-result-object v73

    .line 262
    :goto_3
    :try_start_5
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v44

    .line 264
    .local v44, "currentSystemLanguage":Ljava/lang/String;
    sget-object v6, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v6}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v44

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v6}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v44

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    sget-object v6, Ljava/util/Locale;->UK:Ljava/util/Locale;

    invoke-virtual {v6}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v44

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 265
    sget-object v6, Ljava/util/Locale;->KOREA:Ljava/util/Locale;

    invoke-virtual {v6}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v44

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    sget-object v6, Ljava/util/Locale;->KOREAN:Ljava/util/Locale;

    invoke-virtual {v6}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v44

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 267
    sget-object v6, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v6}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v44

    .line 270
    :cond_2
    sget-object v6, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v6}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v44

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 271
    sget-object v6, Ljava/util/Locale;->UK:Ljava/util/Locale;

    invoke-virtual {v6}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v44

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 272
    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v6}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v44

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 273
    :cond_3
    if-eqz v73, :cond_8

    .line 274
    const/4 v6, 0x1

    move-object/from16 v0, v19

    move-object/from16 v1, v73

    invoke-virtual {v0, v1, v6}, Lcom/samsung/app/video/editor/external/ClipartParams;->setRawFilePath(Ljava/lang/String;Z)V

    .line 275
    const/4 v6, 0x1

    move-object/from16 v0, v19

    invoke-virtual {v0, v6}, Lcom/samsung/app/video/editor/external/ClipartParams;->setAssetResource(Z)V

    .line 330
    :cond_4
    :goto_4
    const/4 v6, 0x1

    move/from16 v0, v32

    if-ne v0, v6, :cond_10

    .line 331
    const/16 v23, 0x11

    :cond_5
    :goto_5
    move-object/from16 v20, v7

    move/from16 v21, v8

    move/from16 v22, v9

    move/from16 v24, v10

    move/from16 v25, v11

    move/from16 v26, v15

    move/from16 v27, v13

    move/from16 v28, v14

    move/from16 v29, v12

    move/from16 v30, v18

    .line 334
    invoke-virtual/range {v19 .. v30}, Lcom/samsung/app/video/editor/external/ClipartParams;->setFontParams(Ljava/lang/String;ZIIIIIIIII)V

    move-object/from16 v24, v19

    move/from16 v25, v47

    move/from16 v26, v49

    move/from16 v27, v50

    move/from16 v28, v51

    move/from16 v29, v48

    move/from16 v30, v46

    .line 335
    invoke-virtual/range {v24 .. v31}, Lcom/samsung/app/video/editor/external/ClipartParams;->setDefaultTransformations(FFFFFFF)V

    .line 337
    move-object/from16 v0, v40

    move-object/from16 v1, v19

    iput-object v0, v1, Lcom/samsung/app/video/editor/external/ClipartParams;->data:Ljava/lang/String;

    .line 338
    sub-int v6, v76, v62

    move-object/from16 v0, v19

    iput v6, v0, Lcom/samsung/app/video/editor/external/ClipartParams;->clipart_width:I

    .line 339
    sub-int v6, v37, v88

    move-object/from16 v0, v19

    iput v6, v0, Lcom/samsung/app/video/editor/external/ClipartParams;->clipart_height:I

    .line 340
    move/from16 v0, v62

    move-object/from16 v1, v19

    iput v0, v1, Lcom/samsung/app/video/editor/external/ClipartParams;->clipart_x_pos:I

    .line 341
    move/from16 v0, v88

    move-object/from16 v1, v19

    iput v0, v1, Lcom/samsung/app/video/editor/external/ClipartParams;->clipart_y_pos:I

    .line 343
    const-string v6, "STORYBOARD_START_FRAME"

    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, v19

    invoke-virtual {v0, v6}, Lcom/samsung/app/video/editor/external/ClipartParams;->setStoryboardStartFrame(I)V

    .line 344
    const-string v6, "STORYBOARD_END_FRAME"

    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, v19

    invoke-virtual {v0, v6}, Lcom/samsung/app/video/editor/external/ClipartParams;->setStoryboardEndFrame(I)V

    .line 345
    const/4 v6, 0x1

    move-object/from16 v0, v19

    invoke-virtual {v0, v6}, Lcom/samsung/app/video/editor/external/ClipartParams;->setThemeDefaultText(Z)V

    .line 346
    const-string v6, "CAPTION_EDITABLE"

    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v6

    move-object/from16 v0, v19

    invoke-virtual {v0, v6}, Lcom/samsung/app/video/editor/external/ClipartParams;->setEditable(Z)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    .line 349
    :try_start_6
    const-string v6, "CAPTION_ID"

    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, v19

    invoke-virtual {v0, v6}, Lcom/samsung/app/video/editor/external/ClipartParams;->setCaptionID(I)V
    :try_end_6
    .catch Lorg/json/JSONException; {:try_start_6 .. :try_end_6} :catch_4
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1

    .line 355
    :goto_6
    :try_start_7
    const-string v6, "ANIM_ARRAY"

    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v33

    .line 357
    .local v33, "animArray":Lorg/json/JSONArray;
    if-eqz v33, :cond_6

    invoke-virtual/range {v33 .. v33}, Lorg/json/JSONArray;->length()I

    move-result v6

    if-lez v6, :cond_6

    .line 358
    invoke-virtual/range {v33 .. v33}, Lorg/json/JSONArray;->length()I

    move-result v78

    .line 359
    .local v78, "size":I
    const/16 v59, 0x0

    .local v59, "i":I
    :goto_7
    move/from16 v0, v59

    move/from16 v1, v78

    if-lt v0, v1, :cond_11

    .line 417
    .end local v7    # "fontFilePath":Ljava/lang/String;
    .end local v8    # "isPlatformFont":Z
    .end local v9    # "fontTextStyle":I
    .end local v10    # "fontSize":I
    .end local v11    # "fontColor":I
    .end local v12    # "shadowR":I
    .end local v13    # "shadowX":I
    .end local v14    # "shadowY":I
    .end local v15    # "shadowColor":I
    .end local v18    # "paintFlags":I
    .end local v23    # "alignFlag":I
    .end local v31    # "defPivotY":F
    .end local v32    # "align":I
    .end local v33    # "animArray":Lorg/json/JSONArray;
    .end local v37    # "bottom":I
    .end local v40    # "captionText":Ljava/lang/String;
    .end local v41    # "captionTextId":Ljava/lang/String;
    .end local v42    # "colorId":I
    .end local v44    # "currentSystemLanguage":Ljava/lang/String;
    .end local v46    # "defPivotX":F
    .end local v47    # "defaultAlpha":F
    .end local v48    # "defaultRotate":F
    .end local v49    # "defaultScale":F
    .end local v50    # "defaultTranslateX":F
    .end local v51    # "defaultTranslateY":F
    .end local v59    # "i":I
    .end local v62    # "left":I
    .end local v63    # "locale":Ljava/util/Locale;
    .end local v64    # "localeLang":Ljava/lang/String;
    .end local v67    # "pkgName":Ljava/lang/String;
    .end local v73    # "rawFilePath":Ljava/lang/String;
    .end local v75    # "res":Landroid/content/res/Resources;
    .end local v76    # "right":I
    .end local v77    # "shadowColorId":I
    .end local v78    # "size":I
    .end local v82    # "stringId":I
    .end local v88    # "top":I
    :cond_6
    :goto_8
    return-object v19

    .line 212
    .restart local v40    # "captionText":Ljava/lang/String;
    .restart local v41    # "captionTextId":Ljava/lang/String;
    .restart local v63    # "locale":Ljava/util/Locale;
    .restart local v64    # "localeLang":Ljava/lang/String;
    .restart local v67    # "pkgName":Ljava/lang/String;
    .restart local v75    # "res":Landroid/content/res/Resources;
    .restart local v82    # "stringId":I
    :cond_7
    sget-object v6, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v82

    invoke-direct {v0, v1, v6, v2}, Lcom/sec/android/app/ve/theme/ThemeDataManager;->translate(Landroid/content/Context;Ljava/util/Locale;I)Ljava/lang/String;

    move-result-object v40

    goto/16 :goto_0

    .line 228
    .restart local v7    # "fontFilePath":Ljava/lang/String;
    .restart local v8    # "isPlatformFont":Z
    .restart local v9    # "fontTextStyle":I
    .restart local v37    # "bottom":I
    .restart local v62    # "left":I
    .restart local v76    # "right":I
    .restart local v88    # "top":I
    :catch_0
    move-exception v68

    .line 229
    .local v68, "platformFont":Lorg/json/JSONException;
    const-string v6, "This caption doesn\'t support platform fonts data"

    invoke-static {v6}, Lcom/sec/android/app/ve/common/LogUtils;->log(Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1

    goto/16 :goto_1

    .line 414
    .end local v7    # "fontFilePath":Ljava/lang/String;
    .end local v8    # "isPlatformFont":Z
    .end local v9    # "fontTextStyle":I
    .end local v37    # "bottom":I
    .end local v40    # "captionText":Ljava/lang/String;
    .end local v41    # "captionTextId":Ljava/lang/String;
    .end local v62    # "left":I
    .end local v63    # "locale":Ljava/util/Locale;
    .end local v64    # "localeLang":Ljava/lang/String;
    .end local v67    # "pkgName":Ljava/lang/String;
    .end local v68    # "platformFont":Lorg/json/JSONException;
    .end local v75    # "res":Landroid/content/res/Resources;
    .end local v76    # "right":I
    .end local v82    # "stringId":I
    .end local v88    # "top":I
    :catch_1
    move-exception v52

    .line 415
    .local v52, "e":Ljava/lang/Exception;
    invoke-virtual/range {v52 .. v52}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_8

    .line 235
    .end local v52    # "e":Ljava/lang/Exception;
    .restart local v7    # "fontFilePath":Ljava/lang/String;
    .restart local v8    # "isPlatformFont":Z
    .restart local v9    # "fontTextStyle":I
    .restart local v18    # "paintFlags":I
    .restart local v37    # "bottom":I
    .restart local v40    # "captionText":Ljava/lang/String;
    .restart local v41    # "captionTextId":Ljava/lang/String;
    .restart local v62    # "left":I
    .restart local v63    # "locale":Ljava/util/Locale;
    .restart local v64    # "localeLang":Ljava/lang/String;
    .restart local v67    # "pkgName":Ljava/lang/String;
    .restart local v75    # "res":Landroid/content/res/Resources;
    .restart local v76    # "right":I
    .restart local v82    # "stringId":I
    .restart local v88    # "top":I
    :catch_2
    move-exception v66

    .line 236
    .local v66, "paintException":Lorg/json/JSONException;
    :try_start_8
    const-string v6, "This caption does not support paintFlags"

    invoke-static {v6}, Lcom/sec/android/app/ve/common/LogUtils;->log(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 259
    .end local v66    # "paintException":Lorg/json/JSONException;
    .restart local v10    # "fontSize":I
    .restart local v11    # "fontColor":I
    .restart local v12    # "shadowR":I
    .restart local v13    # "shadowX":I
    .restart local v14    # "shadowY":I
    .restart local v15    # "shadowColor":I
    .restart local v23    # "alignFlag":I
    .restart local v31    # "defPivotY":F
    .restart local v32    # "align":I
    .restart local v42    # "colorId":I
    .restart local v46    # "defPivotX":F
    .restart local v47    # "defaultAlpha":F
    .restart local v48    # "defaultRotate":F
    .restart local v49    # "defaultScale":F
    .restart local v50    # "defaultTranslateX":F
    .restart local v51    # "defaultTranslateY":F
    .restart local v73    # "rawFilePath":Ljava/lang/String;
    .restart local v77    # "shadowColorId":I
    :catch_3
    move-exception v74

    .line 260
    .local v74, "rawFilePathException":Lorg/json/JSONException;
    const-string v6, "This Caption does not have precreated raw file"

    invoke-static {v6}, Lcom/sec/android/app/ve/common/LogUtils;->log(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 278
    .end local v74    # "rawFilePathException":Lorg/json/JSONException;
    .restart local v44    # "currentSystemLanguage":Ljava/lang/String;
    :cond_8
    if-eqz v73, :cond_4

    .line 279
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v6

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v56

    .line 280
    .local v56, "filesDir":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static/range {v56 .. v56}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-direct {v6, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v17, Ljava/io/File;->separator:Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v17, "themedata"

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v84

    .line 281
    .local v84, "themeDataPath":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static/range {v84 .. v84}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-direct {v6, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v17, Ljava/io/File;->separator:Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v17, Lcom/sec/android/app/ve/VEApp;->gThemeMgr:Lcom/sec/android/app/ve/theme/ThemeManager;

    move-object/from16 v0, v17

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/sec/android/app/ve/theme/ThemeManager;->getThemeName(I)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v83

    .line 282
    .local v83, "themeDataCurrentTheme":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static/range {v83 .. v83}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-direct {v6, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v17, Ljava/io/File;->separator:Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v17, "raw_files"

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v72

    .line 283
    .local v72, "rawFileDirForCurrentTheme":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static/range {v72 .. v72}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-direct {v6, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v17, Ljava/io/File;->separator:Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v44

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v71

    .line 284
    .local v71, "rawFileDirForCurrentLang":Ljava/lang/String;
    new-instance v70, Ljava/io/File;

    move-object/from16 v0, v70

    move-object/from16 v1, v73

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 285
    .local v70, "rawFile":Ljava/io/File;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static/range {v71 .. v71}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-direct {v6, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v17, Ljava/io/File;->separator:Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {v70 .. v70}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v87

    .line 286
    .local v87, "thisRawFileAbsPath":Ljava/lang/String;
    new-instance v85, Ljava/io/File;

    move-object/from16 v0, v85

    move-object/from16 v1, v84

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 287
    .local v85, "themedataDir":Ljava/io/File;
    if-eqz v85, :cond_9

    invoke-virtual/range {v85 .. v85}, Ljava/io/File;->exists()Z

    move-result v6

    if-nez v6, :cond_9

    .line 288
    invoke-virtual/range {v85 .. v85}, Ljava/io/File;->mkdir()Z

    .line 290
    :cond_9
    new-instance v45, Ljava/io/File;

    move-object/from16 v0, v45

    move-object/from16 v1, v83

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 291
    .local v45, "currentThemeDir":Ljava/io/File;
    if-eqz v45, :cond_a

    invoke-virtual/range {v45 .. v45}, Ljava/io/File;->exists()Z

    move-result v6

    if-nez v6, :cond_a

    .line 292
    invoke-virtual/range {v45 .. v45}, Ljava/io/File;->mkdir()Z

    .line 294
    :cond_a
    new-instance v69, Ljava/io/File;

    move-object/from16 v0, v69

    move-object/from16 v1, v72

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 295
    .local v69, "rawDirForCurrentTheme":Ljava/io/File;
    if-eqz v69, :cond_b

    invoke-virtual/range {v69 .. v69}, Ljava/io/File;->exists()Z

    move-result v6

    if-nez v6, :cond_b

    .line 296
    invoke-virtual/range {v69 .. v69}, Ljava/io/File;->mkdir()Z

    .line 298
    :cond_b
    new-instance v43, Ljava/io/File;

    move-object/from16 v0, v43

    move-object/from16 v1, v71

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 299
    .local v43, "currentLangRawFileDir":Ljava/io/File;
    if-eqz v43, :cond_c

    invoke-virtual/range {v43 .. v43}, Ljava/io/File;->exists()Z

    move-result v6

    if-nez v6, :cond_c

    .line 300
    invoke-virtual/range {v43 .. v43}, Ljava/io/File;->mkdir()Z

    .line 302
    :cond_c
    new-instance v86, Ljava/io/File;

    invoke-direct/range {v86 .. v87}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 303
    .local v86, "thisRawFile":Ljava/io/File;
    if-eqz v86, :cond_e

    invoke-virtual/range {v86 .. v86}, Ljava/io/File;->exists()Z

    move-result v6

    if-nez v6, :cond_e

    .line 304
    sget-object v16, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    .line 305
    .local v16, "paintAlign":Landroid/graphics/Paint$Align;
    const/16 v57, 0x0

    .line 306
    .local v57, "fontTranslateX":F
    const/4 v6, 0x1

    move/from16 v0, v32

    if-ne v0, v6, :cond_f

    .line 307
    sget-object v16, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    .line 308
    sub-int v6, v76, v62

    int-to-float v6, v6

    const/high16 v17, 0x40000000    # 2.0f

    div-float v57, v6, v17

    .line 314
    :cond_d
    :goto_9
    sub-int v6, v76, v62

    sub-int v17, v37, v88

    sget-object v20, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move/from16 v0, v17

    move-object/from16 v1, v20

    invoke-static {v6, v0, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v36

    .line 315
    .local v36, "bitmap":Landroid/graphics/Bitmap;
    new-instance v38, Landroid/graphics/Canvas;

    move-object/from16 v0, v38

    move-object/from16 v1, v36

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .local v38, "canvas":Landroid/graphics/Canvas;
    move-object/from16 v6, p1

    move-object/from16 v17, p4

    .line 316
    invoke-static/range {v6 .. v18}, Lcom/sec/android/app/ve/util/CommonUtils;->createTextPaint(Landroid/content/Context;Ljava/lang/String;ZIIIIIIILandroid/graphics/Paint$Align;Landroid/content/res/AssetManager;I)Landroid/graphics/Paint;

    move-result-object v65

    .line 317
    .local v65, "paint":Landroid/graphics/Paint;
    sub-int v6, v37, v88

    int-to-float v6, v6

    invoke-virtual/range {v65 .. v65}, Landroid/graphics/Paint;->ascent()F

    move-result v17

    invoke-virtual/range {v65 .. v65}, Landroid/graphics/Paint;->descent()F

    move-result v20

    add-float v17, v17, v20

    sub-float v6, v6, v17

    const/high16 v17, 0x40000000    # 2.0f

    div-float v58, v6, v17

    .line 318
    .local v58, "fontTranslateY":F
    move-object/from16 v0, v38

    move-object/from16 v1, v40

    move/from16 v2, v57

    move/from16 v3, v58

    move-object/from16 v4, v65

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 319
    move-object/from16 v0, p0

    move-object/from16 v1, v36

    move-object/from16 v2, v87

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/ve/theme/ThemeDataManager;->dumpRAWFile(Landroid/graphics/Bitmap;Ljava/lang/String;)V

    .line 320
    invoke-virtual/range {v36 .. v36}, Landroid/graphics/Bitmap;->recycle()V

    .line 321
    const/16 v36, 0x0

    .line 325
    .end local v16    # "paintAlign":Landroid/graphics/Paint$Align;
    .end local v36    # "bitmap":Landroid/graphics/Bitmap;
    .end local v38    # "canvas":Landroid/graphics/Canvas;
    .end local v57    # "fontTranslateX":F
    .end local v58    # "fontTranslateY":F
    .end local v65    # "paint":Landroid/graphics/Paint;
    :cond_e
    invoke-virtual/range {v86 .. v86}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    const/16 v17, 0x1

    move-object/from16 v0, v19

    move/from16 v1, v17

    invoke-virtual {v0, v6, v1}, Lcom/samsung/app/video/editor/external/ClipartParams;->setRawFilePath(Ljava/lang/String;Z)V

    .line 326
    const/4 v6, 0x0

    move-object/from16 v0, v19

    invoke-virtual {v0, v6}, Lcom/samsung/app/video/editor/external/ClipartParams;->setAssetResource(Z)V

    goto/16 :goto_4

    .line 310
    .restart local v16    # "paintAlign":Landroid/graphics/Paint$Align;
    .restart local v57    # "fontTranslateX":F
    :cond_f
    const/4 v6, 0x2

    move/from16 v0, v32

    if-ne v0, v6, :cond_d

    .line 311
    sget-object v16, Landroid/graphics/Paint$Align;->RIGHT:Landroid/graphics/Paint$Align;

    .line 312
    sub-int v6, v76, v62

    int-to-float v0, v6

    move/from16 v57, v0

    goto :goto_9

    .line 332
    .end local v16    # "paintAlign":Landroid/graphics/Paint$Align;
    .end local v43    # "currentLangRawFileDir":Ljava/io/File;
    .end local v45    # "currentThemeDir":Ljava/io/File;
    .end local v56    # "filesDir":Ljava/lang/String;
    .end local v57    # "fontTranslateX":F
    .end local v69    # "rawDirForCurrentTheme":Ljava/io/File;
    .end local v70    # "rawFile":Ljava/io/File;
    .end local v71    # "rawFileDirForCurrentLang":Ljava/lang/String;
    .end local v72    # "rawFileDirForCurrentTheme":Ljava/lang/String;
    .end local v83    # "themeDataCurrentTheme":Ljava/lang/String;
    .end local v84    # "themeDataPath":Ljava/lang/String;
    .end local v85    # "themedataDir":Ljava/io/File;
    .end local v86    # "thisRawFile":Ljava/io/File;
    .end local v87    # "thisRawFileAbsPath":Ljava/lang/String;
    :cond_10
    const/4 v6, 0x2

    move/from16 v0, v32

    if-ne v0, v6, :cond_5

    .line 333
    const/16 v23, 0x5

    goto/16 :goto_5

    .line 350
    :catch_4
    move-exception v39

    .line 351
    .local v39, "captionIDException":Lorg/json/JSONException;
    const-string v6, "This caption does not support ID"

    invoke-static {v6}, Lcom/sec/android/app/ve/common/LogUtils;->log(Ljava/lang/String;)V

    goto/16 :goto_6

    .line 360
    .end local v39    # "captionIDException":Lorg/json/JSONException;
    .restart local v33    # "animArray":Lorg/json/JSONArray;
    .restart local v59    # "i":I
    .restart local v78    # "size":I
    :cond_11
    new-instance v24, Lcom/samsung/app/video/editor/external/TextAnimationData;

    invoke-direct/range {v24 .. v24}, Lcom/samsung/app/video/editor/external/TextAnimationData;-><init>()V

    .line 361
    .local v24, "animationData":Lcom/samsung/app/video/editor/external/TextAnimationData;
    move-object/from16 v0, v33

    move/from16 v1, v59

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v34

    .line 362
    .local v34, "animation":Lorg/json/JSONObject;
    const-string v6, "ANIM_TYPE"

    move-object/from16 v0, v34

    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v35

    .line 363
    .local v35, "animationType":I
    const-string v6, "ANIM_START"

    move-object/from16 v0, v34

    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v81

    .line 364
    .local v81, "startFrame":I
    const-string v6, "ANIM_END"

    move-object/from16 v0, v34

    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_1

    move-result v55

    .line 365
    .local v55, "endFrame":I
    const/16 v60, 0x0

    .line 367
    .local v60, "interpolation":I
    :try_start_9
    const-string v6, "ANIM_INTERPOLATION"

    move-object/from16 v0, v34

    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
    :try_end_9
    .catch Lorg/json/JSONException; {:try_start_9 .. :try_end_9} :catch_5
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_1

    move-result v60

    .line 372
    :goto_a
    :try_start_a
    move-object/from16 v0, v24

    move/from16 v1, v81

    move/from16 v2, v55

    move/from16 v3, v35

    move/from16 v4, v60

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/samsung/app/video/editor/external/TextAnimationData;->setAnimationType(IIII)V

    .line 373
    packed-switch v35, :pswitch_data_0

    .line 410
    :goto_b
    move-object/from16 v0, v19

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/samsung/app/video/editor/external/ClipartParams;->addAnimationToList(Lcom/samsung/app/video/editor/external/TextAnimationData;)V

    .line 359
    add-int/lit8 v59, v59, 0x1

    goto/16 :goto_7

    .line 369
    :catch_5
    move-exception v61

    .line 370
    .local v61, "interpolationException":Lorg/json/JSONException;
    const-string v6, "This theme does not support interpolation for animations, so linear by default"

    invoke-static {v6}, Lcom/sec/android/app/ve/common/LogUtils;->log(Ljava/lang/String;)V

    goto :goto_a

    .line 375
    .end local v61    # "interpolationException":Lorg/json/JSONException;
    :pswitch_0
    const-string v6, "ALPHA_START"

    move-object/from16 v0, v34

    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v20

    move-wide/from16 v0, v20

    double-to-float v0, v0

    move/from16 v79, v0

    .line 376
    .local v79, "startAlpha":F
    const-string v6, "ALPHA_END"

    move-object/from16 v0, v34

    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v20

    move-wide/from16 v0, v20

    double-to-float v0, v0

    move/from16 v53, v0

    .line 377
    .local v53, "endAlpha":F
    move-object/from16 v0, v24

    move/from16 v1, v79

    move/from16 v2, v53

    invoke-virtual {v0, v1, v2}, Lcom/samsung/app/video/editor/external/TextAnimationData;->setAlphaRange(FF)V

    goto :goto_b

    .line 381
    .end local v53    # "endAlpha":F
    .end local v79    # "startAlpha":F
    :pswitch_1
    const-string v6, "ROTATION_START"

    move-object/from16 v0, v34

    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v20

    move-wide/from16 v0, v20

    double-to-float v0, v0

    move/from16 v80, v0

    .line 382
    .local v80, "startAngle":F
    const-string v6, "ROTATION_END"

    move-object/from16 v0, v34

    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v20

    move-wide/from16 v0, v20

    double-to-float v0, v0

    move/from16 v54, v0

    .line 383
    .local v54, "endAngle":F
    const-string v6, "PIVOT_X"

    move-object/from16 v0, v34

    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v20

    move-wide/from16 v0, v20

    double-to-float v0, v0

    move/from16 v29, v0

    .line 384
    .local v29, "pivotX":F
    const-string v6, "PIVOT_Y"

    move-object/from16 v0, v34

    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v20

    move-wide/from16 v0, v20

    double-to-float v0, v0

    move/from16 v30, v0

    .line 385
    .local v30, "pivotY":F
    move-object/from16 v0, v24

    move/from16 v1, v80

    move/from16 v2, v54

    move/from16 v3, v29

    move/from16 v4, v30

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/samsung/app/video/editor/external/TextAnimationData;->setRotationRange(FFFF)V

    goto :goto_b

    .line 389
    .end local v29    # "pivotX":F
    .end local v30    # "pivotY":F
    .end local v54    # "endAngle":F
    .end local v80    # "startAngle":F
    :pswitch_2
    const-string v6, "SCALE_START_X"

    move-object/from16 v0, v34

    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v20

    move-wide/from16 v0, v20

    double-to-float v0, v0

    move/from16 v25, v0

    .line 390
    .local v25, "scaleStartX":F
    const-string v6, "SCALE_END_X"

    move-object/from16 v0, v34

    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v20

    move-wide/from16 v0, v20

    double-to-float v0, v0

    move/from16 v26, v0

    .line 391
    .local v26, "scaleEndX":F
    const-string v6, "SCALE_START_Y"

    move-object/from16 v0, v34

    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v20

    move-wide/from16 v0, v20

    double-to-float v0, v0

    move/from16 v27, v0

    .line 392
    .local v27, "scaleStartY":F
    const-string v6, "SCALE_END_Y"

    move-object/from16 v0, v34

    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v20

    move-wide/from16 v0, v20

    double-to-float v0, v0

    move/from16 v28, v0

    .line 394
    .local v28, "scaleEndY":F
    const-string v6, "PIVOT_X"

    move-object/from16 v0, v34

    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v20

    move-wide/from16 v0, v20

    double-to-float v0, v0

    move/from16 v29, v0

    .line 395
    .restart local v29    # "pivotX":F
    const-string v6, "PIVOT_Y"

    move-object/from16 v0, v34

    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v20

    move-wide/from16 v0, v20

    double-to-float v0, v0

    move/from16 v30, v0

    .line 396
    .restart local v30    # "pivotY":F
    invoke-virtual/range {v24 .. v30}, Lcom/samsung/app/video/editor/external/TextAnimationData;->setScaleRange(FFFFFF)V

    goto/16 :goto_b

    .line 400
    .end local v25    # "scaleStartX":F
    .end local v26    # "scaleEndX":F
    .end local v27    # "scaleStartY":F
    .end local v28    # "scaleEndY":F
    .end local v29    # "pivotX":F
    .end local v30    # "pivotY":F
    :pswitch_3
    const-string v6, "TRANSLATION_FROM_X"

    move-object/from16 v0, v34

    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v20

    move-wide/from16 v0, v20

    double-to-float v0, v0

    move/from16 v89, v0

    .line 401
    .local v89, "translateFromX":F
    const-string v6, "TRANSLATION_TO_X"

    move-object/from16 v0, v34

    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v20

    move-wide/from16 v0, v20

    double-to-float v0, v0

    move/from16 v91, v0

    .line 402
    .local v91, "translateToX":F
    const-string v6, "TRANSLATION_FROM_Y"

    move-object/from16 v0, v34

    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v20

    move-wide/from16 v0, v20

    double-to-float v0, v0

    move/from16 v90, v0

    .line 403
    .local v90, "translateFromY":F
    const-string v6, "TRANSLATION_TO_Y"

    move-object/from16 v0, v34

    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v20

    move-wide/from16 v0, v20

    double-to-float v0, v0

    move/from16 v92, v0

    .line 404
    .local v92, "translateToY":F
    move-object/from16 v0, v24

    move/from16 v1, v89

    move/from16 v2, v91

    move/from16 v3, v90

    move/from16 v4, v92

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/samsung/app/video/editor/external/TextAnimationData;->setTranslateRange(FFFF)V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_1

    goto/16 :goto_b

    .line 373
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method private createThemeDefaultElement(Landroid/content/res/AssetManager;Lorg/json/JSONObject;II)Lcom/samsung/app/video/editor/external/Element;
    .locals 30
    .param p1, "assetManager"    # Landroid/content/res/AssetManager;
    .param p2, "elementObj"    # Lorg/json/JSONObject;
    .param p3, "theme"    # I
    .param p4, "themeFilter"    # I

    .prologue
    .line 77
    new-instance v2, Lcom/samsung/app/video/editor/external/Element;

    invoke-direct {v2}, Lcom/samsung/app/video/editor/external/Element;-><init>()V

    .line 79
    .local v2, "element":Lcom/samsung/app/video/editor/external/Element;
    const/4 v9, 0x0

    .line 80
    .local v9, "assetFilePath":Ljava/lang/String;
    const/16 v22, 0x0

    .line 82
    .local v22, "filePath":Ljava/lang/String;
    :try_start_0
    const-string v27, "ELEMENT_FILE_PATH"

    move-object/from16 v0, p2

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 83
    const-string v27, "themedata"

    move-object/from16 v0, v27

    invoke-virtual {v9, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_4

    .line 84
    new-instance v27, Ljava/lang/StringBuilder;

    sget-object v28, Lcom/sec/android/app/ve/VEApp;->gThemeMgr:Lcom/sec/android/app/ve/theme/ThemeManager;

    move-object/from16 v0, v28

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/sec/android/app/ve/theme/ThemeManager;->getThemeDataPath(I)Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v28 .. v28}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v28

    invoke-direct/range {v27 .. v28}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v27

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v22

    .line 90
    :goto_0
    :try_start_1
    const-string v27, "ELEMENT_TYPE"

    move-object/from16 v0, p2

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v20

    .line 91
    .local v20, "elementType":I
    const-string v27, "ELEMENT_SUB_TYPE"

    move-object/from16 v0, p2

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v19

    .line 92
    .local v19, "elementSubType":I
    const-string v27, "ELEMENT_DURATION"

    move-object/from16 v0, p2

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v27

    move/from16 v0, v27

    int-to-long v12, v0

    .line 93
    .local v12, "duration":J
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/ve/theme/ThemeDataManager;->themeSlotGlobalDuration:I

    move/from16 v27, v0

    const/16 v28, -0x1

    move/from16 v0, v27

    move/from16 v1, v28

    if-eq v0, v1, :cond_0

    .line 94
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/ve/theme/ThemeDataManager;->themeSlotGlobalDuration:I

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-long v12, v0

    .line 96
    :cond_0
    move-object/from16 v0, v22

    invoke-virtual {v2, v0}, Lcom/samsung/app/video/editor/external/Element;->setFilePath(Ljava/lang/String;)V

    .line 97
    move/from16 v0, v20

    invoke-virtual {v2, v0}, Lcom/samsung/app/video/editor/external/Element;->setType(I)V

    .line 98
    move/from16 v0, v19

    invoke-virtual {v2, v0}, Lcom/samsung/app/video/editor/external/Element;->setSubType(I)V

    .line 99
    const-wide/16 v28, 0x0

    move-wide/from16 v0, v28

    invoke-virtual {v2, v0, v1}, Lcom/samsung/app/video/editor/external/Element;->setStartTime(J)V

    .line 100
    invoke-virtual {v2, v12, v13}, Lcom/samsung/app/video/editor/external/Element;->setEndTime(J)V

    .line 101
    invoke-virtual {v2, v12, v13}, Lcom/samsung/app/video/editor/external/Element;->setDuration(J)V

    .line 102
    if-eqz v22, :cond_5

    const/16 v27, 0x1

    :goto_1
    move/from16 v0, v27

    invoke-virtual {v2, v0}, Lcom/samsung/app/video/editor/external/Element;->setAssetResource(Z)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 104
    :try_start_2
    const-string v27, "FADE_TRANS_TYPE"

    move-object/from16 v0, p2

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v21

    .line 105
    .local v21, "fadeTransType":I
    if-eqz v21, :cond_1

    .line 106
    new-instance v14, Lcom/samsung/app/video/editor/external/Edit;

    invoke-direct {v14}, Lcom/samsung/app/video/editor/external/Edit;-><init>()V

    .line 107
    .local v14, "edit":Lcom/samsung/app/video/editor/external/Edit;
    const/16 v27, 0x9

    move/from16 v0, v27

    invoke-virtual {v14, v0}, Lcom/samsung/app/video/editor/external/Edit;->setType(I)V

    .line 108
    move/from16 v0, v21

    invoke-virtual {v14, v0}, Lcom/samsung/app/video/editor/external/Edit;->setSubType(I)V

    .line 109
    invoke-virtual {v2, v14}, Lcom/samsung/app/video/editor/external/Element;->addEdit(Lcom/samsung/app/video/editor/external/Edit;)V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_2

    .line 115
    .end local v14    # "edit":Lcom/samsung/app/video/editor/external/Edit;
    .end local v21    # "fadeTransType":I
    :cond_1
    :goto_2
    :try_start_3
    const-string v27, "EFFECTS_ARRAY"

    move-object/from16 v0, p2

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v16

    .line 116
    .local v16, "effectArray":Lorg/json/JSONArray;
    invoke-virtual/range {v16 .. v16}, Lorg/json/JSONArray;->length()I
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_3

    move-result v10

    .line 117
    .local v10, "count":I
    const/16 v23, 0x0

    .local v23, "i":I
    :goto_3
    move/from16 v0, v23

    if-lt v0, v10, :cond_6

    .line 155
    .end local v10    # "count":I
    .end local v16    # "effectArray":Lorg/json/JSONArray;
    .end local v23    # "i":I
    :goto_4
    :try_start_4
    const-string v27, "TRANSITION_TYPE"

    move-object/from16 v0, p2

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v24

    .line 156
    .local v24, "transition":I
    if-eqz v24, :cond_2

    .line 157
    new-instance v14, Lcom/samsung/app/video/editor/external/Edit;

    invoke-direct {v14}, Lcom/samsung/app/video/editor/external/Edit;-><init>()V

    .line 158
    .restart local v14    # "edit":Lcom/samsung/app/video/editor/external/Edit;
    const/16 v27, 0x6

    move/from16 v0, v27

    invoke-virtual {v14, v0}, Lcom/samsung/app/video/editor/external/Edit;->setType(I)V

    .line 159
    move/from16 v0, v24

    invoke-virtual {v14, v0}, Lcom/samsung/app/video/editor/external/Edit;->setSubType(I)V

    .line 160
    const-string v27, "TRANSITION_DURATION"

    move-object/from16 v0, p2

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v27

    move/from16 v0, v27

    invoke-virtual {v14, v0}, Lcom/samsung/app/video/editor/external/Edit;->setTrans_duration(I)V
    :try_end_4
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_6

    .line 162
    :try_start_5
    new-instance v27, Ljava/lang/StringBuilder;

    sget-object v28, Lcom/sec/android/app/ve/VEApp;->gThemeMgr:Lcom/sec/android/app/ve/theme/ThemeManager;

    move-object/from16 v0, v28

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/sec/android/app/ve/theme/ThemeManager;->getThemeDataPath(I)Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v28 .. v28}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v28

    invoke-direct/range {v27 .. v28}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v28, "EFFECT_RESOURCE_PATH"

    move-object/from16 v0, p2

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    .line 163
    .local v25, "transitionFilePath":Ljava/lang/String;
    move-object/from16 v0, v25

    invoke-virtual {v14, v0}, Lcom/samsung/app/video/editor/external/Edit;->setEffectResourceFile(Ljava/lang/String;)V
    :try_end_5
    .catch Lorg/json/JSONException; {:try_start_5 .. :try_end_5} :catch_5

    .line 168
    .end local v25    # "transitionFilePath":Ljava/lang/String;
    :goto_5
    :try_start_6
    invoke-virtual {v2, v14}, Lcom/samsung/app/video/editor/external/Element;->addEdit(Lcom/samsung/app/video/editor/external/Edit;)V
    :try_end_6
    .catch Lorg/json/JSONException; {:try_start_6 .. :try_end_6} :catch_6

    .line 174
    .end local v14    # "edit":Lcom/samsung/app/video/editor/external/Edit;
    .end local v24    # "transition":I
    :cond_2
    :goto_6
    if-eqz p4, :cond_3

    .line 175
    :try_start_7
    new-instance v14, Lcom/samsung/app/video/editor/external/Edit;

    invoke-direct {v14}, Lcom/samsung/app/video/editor/external/Edit;-><init>()V

    .line 176
    .restart local v14    # "edit":Lcom/samsung/app/video/editor/external/Edit;
    const/16 v27, 0x4

    move/from16 v0, v27

    invoke-virtual {v14, v0}, Lcom/samsung/app/video/editor/external/Edit;->setType(I)V

    .line 177
    move/from16 v0, p4

    invoke-virtual {v14, v0}, Lcom/samsung/app/video/editor/external/Edit;->setSubType(I)V

    .line 178
    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/Element;->getStartTime()J

    move-result-wide v28

    move-wide/from16 v0, v28

    long-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    invoke-virtual {v14, v0}, Lcom/samsung/app/video/editor/external/Edit;->setEffectStartTime(I)V

    .line 179
    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/Element;->getEndTime()J

    move-result-wide v28

    move-wide/from16 v0, v28

    long-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    invoke-virtual {v14, v0}, Lcom/samsung/app/video/editor/external/Edit;->setEffectEndTime(I)V

    .line 180
    const/16 v27, 0x0

    move/from16 v0, v27

    invoke-virtual {v2, v14, v0}, Lcom/samsung/app/video/editor/external/Element;->addEdit(Lcom/samsung/app/video/editor/external/Edit;Z)V

    .line 185
    .end local v12    # "duration":J
    .end local v14    # "edit":Lcom/samsung/app/video/editor/external/Edit;
    .end local v19    # "elementSubType":I
    .end local v20    # "elementType":I
    :cond_3
    :goto_7
    return-object v2

    .line 86
    :cond_4
    move-object/from16 v22, v9

    goto/16 :goto_0

    .line 87
    :catch_0
    move-exception v11

    .line 88
    .local v11, "e":Lorg/json/JSONException;
    const-string v27, "This theme does not support ELEMENT_FILE_PATH"

    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/ve/common/LogUtils;->log(Ljava/lang/String;)V
    :try_end_7
    .catch Lorg/json/JSONException; {:try_start_7 .. :try_end_7} :catch_1

    goto/16 :goto_0

    .line 182
    .end local v11    # "e":Lorg/json/JSONException;
    :catch_1
    move-exception v11

    .line 183
    .restart local v11    # "e":Lorg/json/JSONException;
    invoke-virtual {v11}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_7

    .line 102
    .end local v11    # "e":Lorg/json/JSONException;
    .restart local v12    # "duration":J
    .restart local v19    # "elementSubType":I
    .restart local v20    # "elementType":I
    :cond_5
    const/16 v27, 0x0

    goto/16 :goto_1

    .line 111
    :catch_2
    move-exception v11

    .line 112
    .restart local v11    # "e":Lorg/json/JSONException;
    :try_start_8
    const-string v27, "This theme does not support FADE_TRANS_TYPE"

    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/ve/common/LogUtils;->log(Ljava/lang/String;)V
    :try_end_8
    .catch Lorg/json/JSONException; {:try_start_8 .. :try_end_8} :catch_1

    goto/16 :goto_2

    .line 118
    .end local v11    # "e":Lorg/json/JSONException;
    .restart local v10    # "count":I
    .restart local v16    # "effectArray":Lorg/json/JSONArray;
    .restart local v23    # "i":I
    :cond_6
    :try_start_9
    move-object/from16 v0, v16

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v15

    .line 119
    .local v15, "effect":Lorg/json/JSONObject;
    new-instance v14, Lcom/samsung/app/video/editor/external/Edit;

    invoke-direct {v14}, Lcom/samsung/app/video/editor/external/Edit;-><init>()V

    .line 120
    .restart local v14    # "edit":Lcom/samsung/app/video/editor/external/Edit;
    const/16 v27, 0x4

    move/from16 v0, v27

    invoke-virtual {v14, v0}, Lcom/samsung/app/video/editor/external/Edit;->setType(I)V

    .line 121
    const-string v27, "EFFECT_TYPE"

    move-object/from16 v0, v27

    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v27

    move/from16 v0, v27

    invoke-virtual {v14, v0}, Lcom/samsung/app/video/editor/external/Edit;->setSubType(I)V

    .line 122
    const-string v27, "EFFECT_START_TIME"

    move-object/from16 v0, v27

    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v27

    move/from16 v0, v27

    invoke-virtual {v14, v0}, Lcom/samsung/app/video/editor/external/Edit;->setEffectStartTime(I)V

    .line 123
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/ve/theme/ThemeDataManager;->themeSlotGlobalDuration:I

    move/from16 v27, v0

    const/16 v28, -0x1

    move/from16 v0, v27

    move/from16 v1, v28

    if-eq v0, v1, :cond_9

    .line 124
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/ve/theme/ThemeDataManager;->themeSlotGlobalDuration:I

    move/from16 v27, v0

    move/from16 v0, v27

    invoke-virtual {v14, v0}, Lcom/samsung/app/video/editor/external/Edit;->setEffectEndTime(I)V
    :try_end_9
    .catch Lorg/json/JSONException; {:try_start_9 .. :try_end_9} :catch_3

    .line 130
    :goto_8
    :try_start_a
    new-instance v27, Ljava/lang/StringBuilder;

    sget-object v28, Lcom/sec/android/app/ve/VEApp;->gThemeMgr:Lcom/sec/android/app/ve/theme/ThemeManager;

    move-object/from16 v0, v28

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/sec/android/app/ve/theme/ThemeManager;->getThemeDataPath(I)Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v28 .. v28}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v28

    invoke-direct/range {v27 .. v28}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v28, "EFFECT_RESOURCE_PATH"

    move-object/from16 v0, v28

    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    .line 131
    .local v17, "effectFilePath":Ljava/lang/String;
    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Lcom/samsung/app/video/editor/external/Edit;->setEffectResourceFile(Ljava/lang/String;)V
    :try_end_a
    .catch Lorg/json/JSONException; {:try_start_a .. :try_end_a} :catch_4

    .line 136
    .end local v17    # "effectFilePath":Ljava/lang/String;
    :goto_9
    :try_start_b
    const-string v27, "ALPHA_START"

    move-object/from16 v0, v27

    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v28

    move-wide/from16 v0, v28

    double-to-float v0, v0

    move/from16 v27, v0

    const-string v28, "ALPHA_END"

    move-object/from16 v0, v28

    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v28

    move-wide/from16 v0, v28

    double-to-float v0, v0

    move/from16 v28, v0

    move/from16 v0, v27

    move/from16 v1, v28

    invoke-virtual {v14, v0, v1}, Lcom/samsung/app/video/editor/external/Edit;->setEffectAlpha(FF)V

    .line 138
    invoke-virtual {v14}, Lcom/samsung/app/video/editor/external/Edit;->getSubType()I

    move-result v27

    const/16 v28, 0x27

    move/from16 v0, v27

    move/from16 v1, v28

    if-eq v0, v1, :cond_7

    .line 139
    invoke-virtual {v14}, Lcom/samsung/app/video/editor/external/Edit;->getSubType()I

    move-result v27

    const/16 v28, 0x4d

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_8

    .line 140
    :cond_7
    const-string v27, "SCALE_START_X"

    move-object/from16 v0, v27

    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v28

    move-wide/from16 v0, v28

    double-to-float v3, v0

    .line 141
    .local v3, "startScale":F
    const-string v27, "SCALE_END_X"

    move-object/from16 v0, v27

    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v28

    move-wide/from16 v0, v28

    double-to-float v4, v0

    .line 142
    .local v4, "endScale":F
    const-string v27, "TRANSLATION_FROM_X"

    move-object/from16 v0, v27

    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v28

    move-wide/from16 v0, v28

    double-to-float v5, v0

    .line 143
    .local v5, "startTranslateX":F
    const-string v27, "TRANSLATION_TO_X"

    move-object/from16 v0, v27

    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v28

    move-wide/from16 v0, v28

    double-to-float v6, v0

    .line 144
    .local v6, "endTranslateX":F
    const-string v27, "TRANSLATION_FROM_Y"

    move-object/from16 v0, v27

    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v28

    move-wide/from16 v0, v28

    double-to-float v7, v0

    .line 145
    .local v7, "startTranslateY":F
    const-string v27, "TRANSLATION_TO_Y"

    move-object/from16 v0, v27

    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v28

    move-wide/from16 v0, v28

    double-to-float v8, v0

    .line 146
    .local v8, "endTranslateY":F
    invoke-static/range {v2 .. v8}, Lcom/sec/android/app/ve/util/CommonUtils;->applyKenburnsToElement(Lcom/samsung/app/video/editor/external/Element;FFFFFF)V

    .line 148
    .end local v3    # "startScale":F
    .end local v4    # "endScale":F
    .end local v5    # "startTranslateX":F
    .end local v6    # "endTranslateX":F
    .end local v7    # "startTranslateY":F
    .end local v8    # "endTranslateY":F
    :cond_8
    const/16 v27, 0x0

    move/from16 v0, v27

    invoke-virtual {v2, v14, v0}, Lcom/samsung/app/video/editor/external/Element;->addEdit(Lcom/samsung/app/video/editor/external/Edit;Z)V

    .line 117
    add-int/lit8 v23, v23, 0x1

    goto/16 :goto_3

    .line 127
    :cond_9
    const-string v27, "EFFECT_END_TIME"

    move-object/from16 v0, v27

    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v27

    move/from16 v0, v27

    invoke-virtual {v14, v0}, Lcom/samsung/app/video/editor/external/Edit;->setEffectEndTime(I)V
    :try_end_b
    .catch Lorg/json/JSONException; {:try_start_b .. :try_end_b} :catch_3

    goto/16 :goto_8

    .line 150
    .end local v10    # "count":I
    .end local v14    # "edit":Lcom/samsung/app/video/editor/external/Edit;
    .end local v15    # "effect":Lorg/json/JSONObject;
    .end local v16    # "effectArray":Lorg/json/JSONArray;
    .end local v23    # "i":I
    :catch_3
    move-exception v11

    .line 151
    .restart local v11    # "e":Lorg/json/JSONException;
    :try_start_c
    const-string v27, "This theme does not support EFFECTS_ARRAY"

    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/ve/common/LogUtils;->log(Ljava/lang/String;)V
    :try_end_c
    .catch Lorg/json/JSONException; {:try_start_c .. :try_end_c} :catch_1

    goto/16 :goto_4

    .line 133
    .end local v11    # "e":Lorg/json/JSONException;
    .restart local v10    # "count":I
    .restart local v14    # "edit":Lcom/samsung/app/video/editor/external/Edit;
    .restart local v15    # "effect":Lorg/json/JSONObject;
    .restart local v16    # "effectArray":Lorg/json/JSONArray;
    .restart local v23    # "i":I
    :catch_4
    move-exception v18

    .line 134
    .local v18, "effectPathException":Lorg/json/JSONException;
    :try_start_d
    const-string v27, "This effect does not support EFFECT_RESOURCE_PATH"

    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/ve/common/LogUtils;->log(Ljava/lang/String;)V
    :try_end_d
    .catch Lorg/json/JSONException; {:try_start_d .. :try_end_d} :catch_3

    goto/16 :goto_9

    .line 165
    .end local v10    # "count":I
    .end local v15    # "effect":Lorg/json/JSONObject;
    .end local v16    # "effectArray":Lorg/json/JSONArray;
    .end local v18    # "effectPathException":Lorg/json/JSONException;
    .end local v23    # "i":I
    .restart local v24    # "transition":I
    :catch_5
    move-exception v26

    .line 166
    .local v26, "transitionResourceException":Lorg/json/JSONException;
    :try_start_e
    const-string v27, "this transition doesnt support EFFECT_RESOURCE_PATH"

    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/ve/common/LogUtils;->log(Ljava/lang/String;)V
    :try_end_e
    .catch Lorg/json/JSONException; {:try_start_e .. :try_end_e} :catch_6

    goto/16 :goto_5

    .line 171
    .end local v14    # "edit":Lcom/samsung/app/video/editor/external/Edit;
    .end local v24    # "transition":I
    .end local v26    # "transitionResourceException":Lorg/json/JSONException;
    :catch_6
    move-exception v11

    .line 172
    .restart local v11    # "e":Lorg/json/JSONException;
    :try_start_f
    const-string v27, "This theme does not support TRANSITION_TYPE"

    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/ve/common/LogUtils;->log(Ljava/lang/String;)V
    :try_end_f
    .catch Lorg/json/JSONException; {:try_start_f .. :try_end_f} :catch_1

    goto/16 :goto_6
.end method

.method private dumpRAWFile(Landroid/graphics/Bitmap;Ljava/lang/String;)V
    .locals 6
    .param p1, "bmp"    # Landroid/graphics/Bitmap;
    .param p2, "filePath"    # Ljava/lang/String;

    .prologue
    .line 460
    :try_start_0
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getByteCount()I

    move-result v5

    new-array v0, v5, [B

    .line 461
    .local v0, "array":[B
    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 462
    .local v1, "buffer":Ljava/nio/ByteBuffer;
    invoke-virtual {p1, v1}, Landroid/graphics/Bitmap;->copyPixelsToBuffer(Ljava/nio/Buffer;)V

    .line 463
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 464
    .local v3, "file":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->createNewFile()Z

    .line 465
    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 466
    .local v4, "fileOutputStream":Ljava/io/FileOutputStream;
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    move-result-object v1

    .end local v1    # "buffer":Ljava/nio/ByteBuffer;
    check-cast v1, Ljava/nio/ByteBuffer;

    .line 467
    .restart local v1    # "buffer":Ljava/nio/ByteBuffer;
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/FileOutputStream;->write([B)V

    .line 468
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 469
    const/4 v0, 0x0

    .line 474
    .end local v0    # "array":[B
    .end local v1    # "buffer":Ljava/nio/ByteBuffer;
    .end local v3    # "file":Ljava/io/File;
    .end local v4    # "fileOutputStream":Ljava/io/FileOutputStream;
    :goto_0
    return-void

    .line 471
    :catch_0
    move-exception v2

    .line 472
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static getInstance()Lcom/sec/android/app/ve/theme/ThemeDataManager;
    .locals 1

    .prologue
    .line 477
    sget-object v0, Lcom/sec/android/app/ve/theme/ThemeDataManager;->_instance:Lcom/sec/android/app/ve/theme/ThemeDataManager;

    return-object v0
.end method

.method private getThemeJSONObject(Landroid/content/res/AssetManager;I)Lorg/json/JSONObject;
    .locals 7
    .param p1, "assetManager"    # Landroid/content/res/AssetManager;
    .param p2, "theme"    # I

    .prologue
    .line 52
    const/4 v4, 0x0

    .line 54
    .local v4, "themeObj":Lorg/json/JSONObject;
    const/4 v3, 0x0

    .line 57
    .local v3, "stream":Ljava/io/FileInputStream;
    :try_start_0
    sget-object v6, Lcom/sec/android/app/ve/VEApp;->gThemeMgr:Lcom/sec/android/app/ve/theme/ThemeManager;

    invoke-virtual {v6, p2}, Lcom/sec/android/app/ve/theme/ThemeManager;->getThemeJSONFilePath(I)Ljava/lang/String;

    move-result-object v2

    .line 59
    .local v2, "jsonFilePath":Ljava/lang/String;
    invoke-virtual {p1, v2}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/app/ve/util/CommonUtils;->convertStreamToString(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v1

    .line 60
    .local v1, "jSonString":Ljava/lang/String;
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 67
    .end local v4    # "themeObj":Lorg/json/JSONObject;
    .local v5, "themeObj":Lorg/json/JSONObject;
    if-eqz v3, :cond_2

    .line 68
    :try_start_1
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    move-object v4, v5

    .line 73
    .end local v1    # "jSonString":Ljava/lang/String;
    .end local v2    # "jsonFilePath":Ljava/lang/String;
    .end local v5    # "themeObj":Lorg/json/JSONObject;
    .restart local v4    # "themeObj":Lorg/json/JSONObject;
    :cond_0
    :goto_0
    return-object v4

    .line 62
    :catch_0
    move-exception v0

    .line 63
    .local v0, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 67
    if-eqz v3, :cond_0

    .line 68
    :try_start_3
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    .line 69
    :catch_1
    move-exception v0

    .line 70
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 65
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v6

    .line 67
    if-eqz v3, :cond_1

    .line 68
    :try_start_4
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 72
    :cond_1
    :goto_1
    throw v6

    .line 69
    :catch_2
    move-exception v0

    .line 70
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 69
    .end local v0    # "e":Ljava/io/IOException;
    .end local v4    # "themeObj":Lorg/json/JSONObject;
    .restart local v1    # "jSonString":Ljava/lang/String;
    .restart local v2    # "jsonFilePath":Ljava/lang/String;
    .restart local v5    # "themeObj":Lorg/json/JSONObject;
    :catch_3
    move-exception v0

    .line 70
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .end local v0    # "e":Ljava/io/IOException;
    :cond_2
    move-object v4, v5

    .end local v5    # "themeObj":Lorg/json/JSONObject;
    .restart local v4    # "themeObj":Lorg/json/JSONObject;
    goto :goto_0
.end method

.method private translate(Landroid/content/Context;Ljava/util/Locale;I)Ljava/lang/String;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "locale"    # Ljava/util/Locale;
    .param p3, "resId"    # I

    .prologue
    .line 189
    const-string v1, ""

    .line 190
    .local v1, "translated":Ljava/lang/String;
    new-instance v0, Landroid/content/res/Configuration;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/content/res/Configuration;-><init>(Landroid/content/res/Configuration;)V

    .line 191
    .local v0, "config":Landroid/content/res/Configuration;
    invoke-virtual {v0, p2}, Landroid/content/res/Configuration;->setLocale(Ljava/util/Locale;)V

    .line 192
    invoke-virtual {p1, v0}, Landroid/content/Context;->createConfigurationContext(Landroid/content/res/Configuration;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, p3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 193
    return-object v1
.end method


# virtual methods
.method public createThemeProject(Landroid/content/Context;I)Lcom/samsung/app/video/editor/external/TranscodeElement;
    .locals 33
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "theme"    # I

    .prologue
    .line 515
    new-instance v29, Lcom/samsung/app/video/editor/external/TranscodeElement;

    invoke-direct/range {v29 .. v29}, Lcom/samsung/app/video/editor/external/TranscodeElement;-><init>()V

    .line 516
    .local v29, "tElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    move-object/from16 v0, v29

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->setThemeName(I)V

    .line 518
    if-lez p2, :cond_1

    .line 519
    :try_start_0
    sget-object v5, Lcom/sec/android/app/ve/VEApp;->gThemeMgr:Lcom/sec/android/app/ve/theme/ThemeManager;

    move/from16 v0, p2

    invoke-virtual {v5, v0}, Lcom/sec/android/app/ve/theme/ThemeManager;->getThemeAssetManager(I)Landroid/content/res/AssetManager;

    move-result-object v10

    .line 520
    .local v10, "assetManager":Landroid/content/res/AssetManager;
    if-eqz v10, :cond_1

    .line 521
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-direct {v0, v10, v1}, Lcom/sec/android/app/ve/theme/ThemeDataManager;->getThemeJSONObject(Landroid/content/res/AssetManager;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v31

    .line 522
    .local v31, "themeObj":Lorg/json/JSONObject;
    if-eqz v31, :cond_1

    .line 523
    const/16 v30, 0x0

    .line 525
    .local v30, "themeFilter":I
    :try_start_1
    const-string v5, "THEME_FILTER"

    move-object/from16 v0, v31

    invoke-virtual {v0, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result v30

    .line 530
    :goto_0
    :try_start_2
    const-string v5, "ELEMENT_ARRAY"

    move-object/from16 v0, v31

    invoke-virtual {v0, v5}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v23

    .line 531
    .local v23, "elementArray":Lorg/json/JSONArray;
    move-object/from16 v0, v29

    invoke-virtual {v0, v10}, Lcom/samsung/app/video/editor/external/TranscodeElement;->setAssetManager(Landroid/content/res/AssetManager;)V

    .line 533
    invoke-virtual/range {v23 .. v23}, Lorg/json/JSONArray;->length()I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    move-result v19

    .line 534
    .local v19, "count":I
    const/16 v26, 0x0

    .local v26, "i":I
    :goto_1
    move/from16 v0, v26

    move/from16 v1, v19

    if-lt v0, v1, :cond_2

    .line 546
    :try_start_3
    const-string v5, "CAPTION_LIST"

    move-object/from16 v0, v31

    invoke-virtual {v0, v5}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v16

    .line 547
    .local v16, "captionArray":Lorg/json/JSONArray;
    invoke-virtual/range {v16 .. v16}, Lorg/json/JSONArray;->length()I
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    move-result v17

    .line 548
    .local v17, "captionCount":I
    const/16 v27, 0x0

    .local v27, "j":I
    :goto_2
    move/from16 v0, v27

    move/from16 v1, v17

    if-lt v0, v1, :cond_4

    .line 562
    .end local v16    # "captionArray":Lorg/json/JSONArray;
    .end local v17    # "captionCount":I
    .end local v27    # "j":I
    :goto_3
    :try_start_4
    const-string v5, "BITMAP_ANIM_ARRAY"

    move-object/from16 v0, v31

    invoke-virtual {v0, v5}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v11

    .line 563
    .local v11, "bitmapAnimArray":Lorg/json/JSONArray;
    if-eqz v11, :cond_0

    .line 564
    invoke-virtual {v11}, Lorg/json/JSONArray;->length()I
    :try_end_4
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_3
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    move-result v14

    .line 565
    .local v14, "bitmapCount":I
    const/16 v28, 0x0

    .local v28, "k":I
    :goto_4
    move/from16 v0, v28

    if-lt v0, v14, :cond_6

    .line 578
    .end local v11    # "bitmapAnimArray":Lorg/json/JSONArray;
    .end local v14    # "bitmapCount":I
    .end local v28    # "k":I
    :cond_0
    :goto_5
    :try_start_5
    new-instance v5, Ljava/lang/StringBuilder;

    sget-object v32, Lcom/sec/android/app/ve/VEApp;->gThemeMgr:Lcom/sec/android/app/ve/theme/ThemeManager;

    move-object/from16 v0, v32

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/ve/theme/ThemeManager;->getThemeDataPath(I)Ljava/lang/String;

    move-result-object v32

    invoke-static/range {v32 .. v32}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, v32

    invoke-direct {v5, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v32, "THEME_BGM"

    invoke-virtual/range {v31 .. v32}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, v32

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 579
    .local v6, "bgmPath":Ljava/lang/String;
    const-string v5, "THEME_BGM_DISPLAY_NAME"

    move-object/from16 v0, v31

    invoke-virtual {v0, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 580
    .local v7, "bgmName":Ljava/lang/String;
    const-string v5, "THEME_BGM_DURATION"

    move-object/from16 v0, v31

    invoke-virtual {v0, v5}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v8

    .local v8, "bgmDuration":J
    move-object/from16 v5, p0

    .line 581
    invoke-direct/range {v5 .. v10}, Lcom/sec/android/app/ve/theme/ThemeDataManager;->createBGMElement(Ljava/lang/String;Ljava/lang/String;JLandroid/content/res/AssetManager;)Lcom/samsung/app/video/editor/external/Element;

    move-result-object v4

    .line 582
    .local v4, "bgmElement":Lcom/samsung/app/video/editor/external/Element;
    move-object/from16 v0, p0

    move-object/from16 v1, v29

    invoke-direct {v0, v4, v1}, Lcom/sec/android/app/ve/theme/ThemeDataManager;->applyThemeBGMMusic(Lcom/samsung/app/video/editor/external/Element;Lcom/samsung/app/video/editor/external/TranscodeElement;)V
    :try_end_5
    .catch Lorg/json/JSONException; {:try_start_5 .. :try_end_5} :catch_4
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    .line 593
    .end local v4    # "bgmElement":Lcom/samsung/app/video/editor/external/Element;
    .end local v6    # "bgmPath":Ljava/lang/String;
    .end local v7    # "bgmName":Ljava/lang/String;
    .end local v8    # "bgmDuration":J
    .end local v10    # "assetManager":Landroid/content/res/AssetManager;
    .end local v19    # "count":I
    .end local v23    # "elementArray":Lorg/json/JSONArray;
    .end local v26    # "i":I
    .end local v30    # "themeFilter":I
    .end local v31    # "themeObj":Lorg/json/JSONObject;
    :cond_1
    :goto_6
    return-object v29

    .line 527
    .restart local v10    # "assetManager":Landroid/content/res/AssetManager;
    .restart local v30    # "themeFilter":I
    .restart local v31    # "themeObj":Lorg/json/JSONObject;
    :catch_0
    move-exception v20

    .line 528
    .local v20, "e":Lorg/json/JSONException;
    :try_start_6
    const-string v5, "This theme does not support THEME_FILTER"

    invoke-static {v5}, Lcom/sec/android/app/ve/common/LogUtils;->log(Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1

    goto/16 :goto_0

    .line 590
    .end local v10    # "assetManager":Landroid/content/res/AssetManager;
    .end local v20    # "e":Lorg/json/JSONException;
    .end local v30    # "themeFilter":I
    .end local v31    # "themeObj":Lorg/json/JSONObject;
    :catch_1
    move-exception v20

    .line 591
    .local v20, "e":Ljava/lang/Exception;
    invoke-virtual/range {v20 .. v20}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_6

    .line 535
    .end local v20    # "e":Ljava/lang/Exception;
    .restart local v10    # "assetManager":Landroid/content/res/AssetManager;
    .restart local v19    # "count":I
    .restart local v23    # "elementArray":Lorg/json/JSONArray;
    .restart local v26    # "i":I
    .restart local v30    # "themeFilter":I
    .restart local v31    # "themeObj":Lorg/json/JSONObject;
    :cond_2
    :try_start_7
    move-object/from16 v0, v23

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v24

    .line 536
    .local v24, "elementObj":Lorg/json/JSONObject;
    const-string v5, "ELEMENT_SUB_TYPE"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v25

    .line 537
    .local v25, "elementSubType":I
    const/4 v5, 0x4

    move/from16 v0, v25

    if-eq v0, v5, :cond_3

    .line 538
    invoke-virtual/range {v29 .. v29}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getAssetManager()Landroid/content/res/AssetManager;

    move-result-object v5

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    move/from16 v2, p2

    move/from16 v3, v30

    invoke-direct {v0, v5, v1, v2, v3}, Lcom/sec/android/app/ve/theme/ThemeDataManager;->createThemeDefaultElement(Landroid/content/res/AssetManager;Lorg/json/JSONObject;II)Lcom/samsung/app/video/editor/external/Element;

    move-result-object v21

    .line 539
    .local v21, "ele":Lcom/samsung/app/video/editor/external/Element;
    move-object/from16 v0, v21

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/samsung/app/video/editor/external/Element;->setGroupID(I)V

    .line 540
    const/4 v5, 0x0

    move-object/from16 v0, v29

    move-object/from16 v1, v21

    invoke-virtual {v0, v1, v5}, Lcom/samsung/app/video/editor/external/TranscodeElement;->addElement(Lcom/samsung/app/video/editor/external/Element;Z)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1

    .line 534
    .end local v21    # "ele":Lcom/samsung/app/video/editor/external/Element;
    :cond_3
    add-int/lit8 v26, v26, 0x1

    goto/16 :goto_1

    .line 549
    .end local v24    # "elementObj":Lorg/json/JSONObject;
    .end local v25    # "elementSubType":I
    .restart local v16    # "captionArray":Lorg/json/JSONArray;
    .restart local v17    # "captionCount":I
    .restart local v27    # "j":I
    :cond_4
    :try_start_8
    move-object/from16 v0, v16

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v18

    .line 550
    .local v18, "captionObj":Lorg/json/JSONObject;
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v18

    move/from16 v3, p2

    invoke-direct {v0, v1, v2, v3, v10}, Lcom/sec/android/app/ve/theme/ThemeDataManager;->createDefaultCaption(Landroid/content/Context;Lorg/json/JSONObject;ILandroid/content/res/AssetManager;)Lcom/samsung/app/video/editor/external/ClipartParams;

    move-result-object v15

    .line 551
    .local v15, "caption":Lcom/samsung/app/video/editor/external/ClipartParams;
    move-object/from16 v0, v29

    invoke-virtual {v0, v15}, Lcom/samsung/app/video/editor/external/TranscodeElement;->addTextEleList(Lcom/samsung/app/video/editor/external/ClipartParams;)V

    .line 552
    const-string v5, "STORYBOARD_ELEMENT_INDEX"

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v29

    invoke-virtual {v0, v5}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElement(I)Lcom/samsung/app/video/editor/external/Element;

    move-result-object v22

    .line 553
    .local v22, "element":Lcom/samsung/app/video/editor/external/Element;
    if-eqz v22, :cond_5

    if-eqz v15, :cond_5

    .line 554
    invoke-virtual/range {v22 .. v22}, Lcom/samsung/app/video/editor/external/Element;->getGroupID()I

    move-result v5

    invoke-virtual {v15, v5}, Lcom/samsung/app/video/editor/external/ClipartParams;->setElementID(I)V
    :try_end_8
    .catch Lorg/json/JSONException; {:try_start_8 .. :try_end_8} :catch_2
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_1

    .line 548
    :cond_5
    add-int/lit8 v27, v27, 0x1

    goto/16 :goto_2

    .line 557
    .end local v15    # "caption":Lcom/samsung/app/video/editor/external/ClipartParams;
    .end local v16    # "captionArray":Lorg/json/JSONArray;
    .end local v17    # "captionCount":I
    .end local v18    # "captionObj":Lorg/json/JSONObject;
    .end local v22    # "element":Lcom/samsung/app/video/editor/external/Element;
    .end local v27    # "j":I
    :catch_2
    move-exception v20

    .line 558
    .local v20, "e":Lorg/json/JSONException;
    :try_start_9
    const-string v5, "This theme does not support CAPTION_LIST"

    invoke-static {v5}, Lcom/sec/android/app/ve/common/LogUtils;->log(Ljava/lang/String;)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_1

    goto/16 :goto_3

    .line 566
    .end local v20    # "e":Lorg/json/JSONException;
    .restart local v11    # "bitmapAnimArray":Lorg/json/JSONArray;
    .restart local v14    # "bitmapCount":I
    .restart local v28    # "k":I
    :cond_6
    :try_start_a
    move/from16 v0, v28

    invoke-virtual {v11, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v13

    .line 567
    .local v13, "bitmapAnimObj":Lorg/json/JSONObject;
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-direct {v0, v13, v1}, Lcom/sec/android/app/ve/theme/ThemeDataManager;->createBitmapAnimationData(Lorg/json/JSONObject;I)Lcom/samsung/app/video/editor/external/BitmapAnimationData;

    move-result-object v12

    .line 568
    .local v12, "bitmapAnimData":Lcom/samsung/app/video/editor/external/BitmapAnimationData;
    const-string v5, "BLENDING_FILES_ELEMENT_INDEX"

    invoke-virtual {v13, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v29

    invoke-virtual {v0, v5}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElement(I)Lcom/samsung/app/video/editor/external/Element;

    move-result-object v22

    .line 569
    .restart local v22    # "element":Lcom/samsung/app/video/editor/external/Element;
    if-eqz v22, :cond_7

    .line 570
    move-object/from16 v0, v22

    invoke-virtual {v0, v12}, Lcom/samsung/app/video/editor/external/Element;->addBitmapAnimationsData(Lcom/samsung/app/video/editor/external/BitmapAnimationData;)V
    :try_end_a
    .catch Lorg/json/JSONException; {:try_start_a .. :try_end_a} :catch_3
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_1

    .line 565
    :cond_7
    add-int/lit8 v28, v28, 0x1

    goto/16 :goto_4

    .line 574
    .end local v11    # "bitmapAnimArray":Lorg/json/JSONArray;
    .end local v12    # "bitmapAnimData":Lcom/samsung/app/video/editor/external/BitmapAnimationData;
    .end local v13    # "bitmapAnimObj":Lorg/json/JSONObject;
    .end local v14    # "bitmapCount":I
    .end local v22    # "element":Lcom/samsung/app/video/editor/external/Element;
    .end local v28    # "k":I
    :catch_3
    move-exception v20

    .line 575
    .restart local v20    # "e":Lorg/json/JSONException;
    :try_start_b
    const-string v5, "This theme does not support BITMAP_ANIM_ARRAYs"

    invoke-static {v5}, Lcom/sec/android/app/ve/common/LogUtils;->log(Ljava/lang/String;)V

    goto/16 :goto_5

    .line 584
    .end local v20    # "e":Lorg/json/JSONException;
    :catch_4
    move-exception v20

    .line 585
    .restart local v20    # "e":Lorg/json/JSONException;
    const-string v5, "This theme does not support THEME_BGM"

    invoke-static {v5}, Lcom/sec/android/app/ve/common/LogUtils;->log(Ljava/lang/String;)V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_1

    goto/16 :goto_6
.end method

.method public getCreditsElement(Landroid/content/res/AssetManager;I)Lcom/samsung/app/video/editor/external/Element;
    .locals 8
    .param p1, "assetManager"    # Landroid/content/res/AssetManager;
    .param p2, "theme"    # I

    .prologue
    .line 639
    const/4 v2, 0x0

    .line 641
    .local v2, "element":Lcom/samsung/app/video/editor/external/Element;
    :try_start_0
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/ve/theme/ThemeDataManager;->getThemeJSONObject(Landroid/content/res/AssetManager;I)Lorg/json/JSONObject;

    move-result-object v5

    .line 642
    .local v5, "themeObj":Lorg/json/JSONObject;
    if-eqz v5, :cond_0

    .line 643
    const-string v6, "ELEMENT_ARRAY"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    .line 644
    .local v3, "elementArray":Lorg/json/JSONArray;
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v0

    .line 645
    .local v0, "count":I
    add-int/lit8 v6, v0, -0x1

    invoke-virtual {v3, v6}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v6

    const/4 v7, 0x0

    invoke-direct {p0, p1, v6, p2, v7}, Lcom/sec/android/app/ve/theme/ThemeDataManager;->createThemeDefaultElement(Landroid/content/res/AssetManager;Lorg/json/JSONObject;II)Lcom/samsung/app/video/editor/external/Element;

    move-result-object v2

    .line 646
    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/Element;->getSubType()I

    move-result v6

    const/4 v7, 0x4

    if-ne v6, v7, :cond_1

    .line 647
    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/Element;->getFilePath()Ljava/lang/String;

    move-result-object v4

    .line 648
    .local v4, "filePath":Ljava/lang/String;
    sget-object v6, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v4, v6}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v6

    add-int/lit8 v6, v6, 0x1

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {v4, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 649
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getFilesDirectory()Ljava/io/File;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/samsung/app/video/editor/external/Element;->setFilePath(Ljava/lang/String;)V

    .line 650
    const/4 v6, 0x0

    invoke-virtual {v2, v6}, Lcom/samsung/app/video/editor/external/Element;->setAssetResource(Z)V

    .line 651
    add-int/lit8 v6, v0, -0x1

    invoke-virtual {v2, v6}, Lcom/samsung/app/video/editor/external/Element;->setGroupID(I)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 660
    .end local v0    # "count":I
    .end local v3    # "elementArray":Lorg/json/JSONArray;
    .end local v4    # "filePath":Ljava/lang/String;
    .end local v5    # "themeObj":Lorg/json/JSONObject;
    :cond_0
    :goto_0
    return-object v2

    .line 654
    .restart local v0    # "count":I
    .restart local v3    # "elementArray":Lorg/json/JSONArray;
    .restart local v5    # "themeObj":Lorg/json/JSONObject;
    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    .line 657
    .end local v0    # "count":I
    .end local v3    # "elementArray":Lorg/json/JSONArray;
    .end local v5    # "themeObj":Lorg/json/JSONObject;
    :catch_0
    move-exception v1

    .line 658
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getDefaultThemeElement(Landroid/content/res/AssetManager;Landroid/content/Context;II)Lcom/samsung/app/video/editor/external/Element;
    .locals 10
    .param p1, "assetManager"    # Landroid/content/res/AssetManager;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "theme"    # I
    .param p4, "elementPosition"    # I

    .prologue
    .line 606
    const/4 v4, 0x0

    .line 608
    .local v4, "element":Lcom/samsung/app/video/editor/external/Element;
    :try_start_0
    invoke-direct {p0, p1, p3}, Lcom/sec/android/app/ve/theme/ThemeDataManager;->getThemeJSONObject(Landroid/content/res/AssetManager;I)Lorg/json/JSONObject;

    move-result-object v8

    .line 609
    .local v8, "themeObj":Lorg/json/JSONObject;
    if-eqz v8, :cond_0

    .line 610
    const-string v9, "ELEMENT_ARRAY"

    invoke-virtual {v8, v9}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v5

    .line 611
    .local v5, "elementArray":Lorg/json/JSONArray;
    const/4 v7, 0x0

    .line 613
    .local v7, "themeFilter":I
    :try_start_1
    const-string v9, "THEME_FILTER"

    invoke-virtual {v8, v9}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v7

    .line 618
    :goto_0
    :try_start_2
    invoke-virtual {v5, p4}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v9

    invoke-direct {p0, p1, v9, p3, v7}, Lcom/sec/android/app/ve/theme/ThemeDataManager;->createThemeDefaultElement(Landroid/content/res/AssetManager;Lorg/json/JSONObject;II)Lcom/samsung/app/video/editor/external/Element;

    move-result-object v4

    .line 619
    invoke-virtual {v4, p4}, Lcom/samsung/app/video/editor/external/Element;->setGroupID(I)V

    .line 620
    const-string v9, "BITMAP_ANIM_ARRAY"

    invoke-virtual {v8, v9}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    .line 621
    .local v0, "bitmapAnimArray":Lorg/json/JSONArray;
    if-eqz v0, :cond_0

    .line 622
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v2

    .line 623
    .local v2, "count":I
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_1
    if-lt v6, v2, :cond_1

    .line 635
    .end local v0    # "bitmapAnimArray":Lorg/json/JSONArray;
    .end local v2    # "count":I
    .end local v5    # "elementArray":Lorg/json/JSONArray;
    .end local v6    # "i":I
    .end local v7    # "themeFilter":I
    .end local v8    # "themeObj":Lorg/json/JSONObject;
    :cond_0
    :goto_2
    return-object v4

    .line 615
    .restart local v5    # "elementArray":Lorg/json/JSONArray;
    .restart local v7    # "themeFilter":I
    .restart local v8    # "themeObj":Lorg/json/JSONObject;
    :catch_0
    move-exception v3

    .line 616
    .local v3, "e":Lorg/json/JSONException;
    const-string v9, "This theme does not support THEME_FILTER"

    invoke-static {v9}, Lcom/sec/android/app/ve/common/LogUtils;->log(Ljava/lang/String;)V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 632
    .end local v3    # "e":Lorg/json/JSONException;
    .end local v5    # "elementArray":Lorg/json/JSONArray;
    .end local v7    # "themeFilter":I
    .end local v8    # "themeObj":Lorg/json/JSONObject;
    :catch_1
    move-exception v3

    .line 633
    .restart local v3    # "e":Lorg/json/JSONException;
    invoke-virtual {v3}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_2

    .line 624
    .end local v3    # "e":Lorg/json/JSONException;
    .restart local v0    # "bitmapAnimArray":Lorg/json/JSONArray;
    .restart local v2    # "count":I
    .restart local v5    # "elementArray":Lorg/json/JSONArray;
    .restart local v6    # "i":I
    .restart local v7    # "themeFilter":I
    .restart local v8    # "themeObj":Lorg/json/JSONObject;
    :cond_1
    :try_start_3
    invoke-virtual {v0, v6}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v1

    .line 625
    .local v1, "bitmapAnimObj":Lorg/json/JSONObject;
    const-string v9, "BLENDING_FILES_ELEMENT_INDEX"

    invoke-virtual {v1, v9}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v9

    if-ne v9, p4, :cond_2

    .line 626
    invoke-direct {p0, v1, p3}, Lcom/sec/android/app/ve/theme/ThemeDataManager;->createBitmapAnimationData(Lorg/json/JSONObject;I)Lcom/samsung/app/video/editor/external/BitmapAnimationData;

    move-result-object v9

    invoke-virtual {v4, v9}, Lcom/samsung/app/video/editor/external/Element;->addBitmapAnimationsData(Lcom/samsung/app/video/editor/external/BitmapAnimationData;)V
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_1

    .line 623
    :cond_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_1
.end method

.method public getThemeDataDir()Ljava/lang/String;
    .locals 1

    .prologue
    .line 671
    const-string v0, "themedata"

    return-object v0
.end method

.method public getThemeSlotGlobalDuration()I
    .locals 1

    .prologue
    .line 664
    iget v0, p0, Lcom/sec/android/app/ve/theme/ThemeDataManager;->themeSlotGlobalDuration:I

    return v0
.end method

.method public modifyTranscodeWithBGMTranstionSlots(Lcom/samsung/app/video/editor/external/TranscodeElement;Lcom/sec/android/app/ve/bgm/BGMManager;)V
    .locals 8
    .param p1, "tElement"    # Lcom/samsung/app/video/editor/external/TranscodeElement;
    .param p2, "bgmManager"    # Lcom/sec/android/app/ve/bgm/BGMManager;

    .prologue
    .line 487
    new-instance v4, Lcom/samsung/app/video/editor/external/TranscodeElement;

    invoke-direct {v4, p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;-><init>(Lcom/samsung/app/video/editor/external/TranscodeElement;)V

    .line 488
    .local v4, "transElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    const/4 v2, 0x0

    .line 489
    .local v2, "k":I
    const/4 v1, 0x0

    .line 492
    .local v1, "element":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {p2}, Lcom/sec/android/app/ve/bgm/BGMManager;->getBgmMode()Lcom/sec/android/app/ve/bgm/BGM$BGMModes;

    move-result-object v5

    invoke-virtual {p2}, Lcom/sec/android/app/ve/bgm/BGMManager;->getSavedBGMPos()I

    move-result v6

    invoke-virtual {p2, v5, v6}, Lcom/sec/android/app/ve/bgm/BGMManager;->calculateSlotDurations(Lcom/sec/android/app/ve/bgm/BGM$BGMModes;I)[I

    move-result-object v3

    .line 493
    .local v3, "slotDurations":[I
    if-eqz v3, :cond_0

    .line 494
    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v0

    .line 496
    .local v0, "count":I
    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->clear()V

    .line 497
    :goto_0
    array-length v5, v3

    if-ge v2, v5, :cond_0

    if-nez v0, :cond_1

    .line 507
    .end local v0    # "count":I
    :cond_0
    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/TranscodeElement;->deleteAllClipArts()V

    .line 509
    return-void

    .line 499
    .restart local v0    # "count":I
    :cond_1
    new-instance v1, Lcom/samsung/app/video/editor/external/Element;

    .end local v1    # "element":Lcom/samsung/app/video/editor/external/Element;
    rem-int v5, v2, v0

    invoke-virtual {v4, v5}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElement(I)Lcom/samsung/app/video/editor/external/Element;

    move-result-object v5

    invoke-direct {v1, v5}, Lcom/samsung/app/video/editor/external/Element;-><init>(Lcom/samsung/app/video/editor/external/Element;)V

    .line 500
    .restart local v1    # "element":Lcom/samsung/app/video/editor/external/Element;
    aget v5, v3, v2

    int-to-long v6, v5

    invoke-virtual {v1, v6, v7}, Lcom/samsung/app/video/editor/external/Element;->setDuration(J)V

    .line 501
    const/4 v5, 0x0

    invoke-virtual {p1, v1, v5}, Lcom/samsung/app/video/editor/external/TranscodeElement;->addElement(Lcom/samsung/app/video/editor/external/Element;Z)V

    .line 502
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public setThemeSlotGlobalDuration(I)V
    .locals 0
    .param p1, "dur"    # I

    .prologue
    .line 667
    iput p1, p0, Lcom/sec/android/app/ve/theme/ThemeDataManager;->themeSlotGlobalDuration:I

    .line 668
    return-void
.end method
