.class public Lcom/sec/android/app/ve/export/Export;
.super Ljava/lang/Object;
.source "Export.java"

# interfaces
.implements Landroid/media/AudioManager$OnAudioFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/ve/export/Export$Adapter;,
        Lcom/sec/android/app/ve/export/Export$ExportServiceConnection;,
        Lcom/sec/android/app/ve/export/Export$ThreadHandler;
    }
.end annotation


# static fields
.field public static final MSG_EXPORT_CANCELLED:I = 0x2

.field public static final MSG_EXPORT_COMPLETED:I = 0x1

.field public static final MSG_EXPORT_FAILED:I = 0x0

.field public static final MSG_EXPORT_STARTED:I = 0x3

.field public static final MSG_EXPORT_UPDATE:I = 0x4

.field public static final STATE_EXPORT_PAUSED:I = 0x2

.field public static final STATE_EXPORT_STARTED:I = 0x1

.field public static final STATE_EXPORT_STOPPED:I

.field private static resEnumValue:I


# instance fields
.field private estimatedSize:F

.field private expFile:Ljava/io/File;

.field private exportState:I

.field private isBGExport:Z

.field private mActivity:Landroid/app/Activity;

.field public mAdapter:Lcom/sec/android/app/ve/export/Export$Adapter;

.field private final mConnection:Lcom/sec/android/app/ve/export/Export$ExportServiceConnection;

.field private mFileName:Ljava/lang/String;

.field private final mHandler:Lcom/sec/android/app/ve/export/Export$ThreadHandler;

.field private mService:Lcom/sec/android/app/ve/export/IExportService;

.field private mStblzReqd:Z

.field private mTrans:Lcom/samsung/app/video/editor/external/TranscodeElement;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 58
    const/4 v0, 0x2

    sput v0, Lcom/sec/android/app/ve/export/Export;->resEnumValue:I

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    new-instance v0, Lcom/sec/android/app/ve/export/Export$ExportServiceConnection;

    invoke-direct {v0, p0}, Lcom/sec/android/app/ve/export/Export$ExportServiceConnection;-><init>(Lcom/sec/android/app/ve/export/Export;)V

    iput-object v0, p0, Lcom/sec/android/app/ve/export/Export;->mConnection:Lcom/sec/android/app/ve/export/Export$ExportServiceConnection;

    .line 48
    new-instance v0, Lcom/sec/android/app/ve/export/Export$ThreadHandler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/ve/export/Export$ThreadHandler;-><init>(Lcom/sec/android/app/ve/export/Export;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/android/app/ve/export/Export;->mHandler:Lcom/sec/android/app/ve/export/Export$ThreadHandler;

    .line 50
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/ve/export/Export;->isBGExport:Z

    .line 51
    iput-object v2, p0, Lcom/sec/android/app/ve/export/Export;->mAdapter:Lcom/sec/android/app/ve/export/Export$Adapter;

    .line 52
    iput-object v2, p0, Lcom/sec/android/app/ve/export/Export;->mTrans:Lcom/samsung/app/video/editor/external/TranscodeElement;

    .line 53
    iput-object v2, p0, Lcom/sec/android/app/ve/export/Export;->mActivity:Landroid/app/Activity;

    .line 54
    iput-object v2, p0, Lcom/sec/android/app/ve/export/Export;->mFileName:Ljava/lang/String;

    .line 55
    iput-boolean v3, p0, Lcom/sec/android/app/ve/export/Export;->mStblzReqd:Z

    .line 56
    iput-object v2, p0, Lcom/sec/android/app/ve/export/Export;->mService:Lcom/sec/android/app/ve/export/IExportService;

    .line 57
    iput v3, p0, Lcom/sec/android/app/ve/export/Export;->exportState:I

    .line 412
    iput-object v2, p0, Lcom/sec/android/app/ve/export/Export;->expFile:Ljava/io/File;

    .line 36
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/app/ve/export/Export;Lcom/sec/android/app/ve/export/IExportService;)V
    .locals 0

    .prologue
    .line 56
    iput-object p1, p0, Lcom/sec/android/app/ve/export/Export;->mService:Lcom/sec/android/app/ve/export/IExportService;

    return-void
.end method

.method static synthetic access$1(Lcom/sec/android/app/ve/export/Export;)Lcom/sec/android/app/ve/export/IExportService;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/ve/export/Export;->mService:Lcom/sec/android/app/ve/export/IExportService;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/app/ve/export/Export;I)V
    .locals 0

    .prologue
    .line 134
    invoke-direct {p0, p1}, Lcom/sec/android/app/ve/export/Export;->setExportState(I)V

    return-void
.end method

.method static synthetic access$3(Lcom/sec/android/app/ve/export/Export;)Lcom/sec/android/app/ve/export/Export$ThreadHandler;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/android/app/ve/export/Export;->mHandler:Lcom/sec/android/app/ve/export/Export$ThreadHandler;

    return-object v0
.end method

.method static synthetic access$4(Lcom/sec/android/app/ve/export/Export;)V
    .locals 0

    .prologue
    .line 190
    invoke-direct {p0}, Lcom/sec/android/app/ve/export/Export;->releaseService()V

    return-void
.end method

.method static synthetic access$5(Lcom/sec/android/app/ve/export/Export;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/ve/export/Export;->mFileName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$6(Lcom/sec/android/app/ve/export/Export;)V
    .locals 0

    .prologue
    .line 195
    invoke-direct {p0}, Lcom/sec/android/app/ve/export/Export;->runMediaScanner()V

    return-void
.end method

.method static synthetic access$7(Lcom/sec/android/app/ve/export/Export;)Lcom/samsung/app/video/editor/external/TranscodeElement;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/app/ve/export/Export;->mTrans:Lcom/samsung/app/video/editor/external/TranscodeElement;

    return-object v0
.end method

.method static synthetic access$8(Lcom/sec/android/app/ve/export/Export;)I
    .locals 1

    .prologue
    .line 172
    invoke-direct {p0}, Lcom/sec/android/app/ve/export/Export;->getExportState()I

    move-result v0

    return v0
.end method

.method static synthetic access$9()I
    .locals 1

    .prologue
    .line 58
    sget v0, Lcom/sec/android/app/ve/export/Export;->resEnumValue:I

    return v0
.end method

.method private getExportState()I
    .locals 2

    .prologue
    .line 175
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/ve/export/Export;->mService:Lcom/sec/android/app/ve/export/IExportService;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/ve/export/Export;->mService:Lcom/sec/android/app/ve/export/IExportService;

    invoke-interface {v1}, Lcom/sec/android/app/ve/export/IExportService;->isEThreadAlive()Z

    move-result v1

    if-nez v1, :cond_1

    .line 176
    :cond_0
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/sec/android/app/ve/export/Export;->setExportState(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 180
    :cond_1
    :goto_0
    iget v1, p0, Lcom/sec/android/app/ve/export/Export;->exportState:I

    return v1

    .line 177
    :catch_0
    move-exception v0

    .line 178
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method private initService(Ljava/lang/String;)V
    .locals 2
    .param p1, "pkgName"    # Ljava/lang/String;

    .prologue
    .line 185
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 186
    .local v0, "i":Landroid/content/Intent;
    const-class v1, Lcom/sec/android/app/ve/export/ExportService;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 187
    iget-object v1, p0, Lcom/sec/android/app/ve/export/Export;->mConnection:Lcom/sec/android/app/ve/export/Export$ExportServiceConnection;

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/VEApp;->bindToService(Landroid/content/Intent;Landroid/content/ServiceConnection;)V

    .line 188
    return-void
.end method

.method private releaseService()V
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Lcom/sec/android/app/ve/export/Export;->mService:Lcom/sec/android/app/ve/export/IExportService;

    if-eqz v0, :cond_0

    .line 192
    iget-object v0, p0, Lcom/sec/android/app/ve/export/Export;->mConnection:Lcom/sec/android/app/ve/export/Export$ExportServiceConnection;

    invoke-static {v0}, Lcom/sec/android/app/ve/VEApp;->unbindFromService(Landroid/content/ServiceConnection;)V

    .line 193
    :cond_0
    return-void
.end method

.method private runMediaScanner()V
    .locals 4

    .prologue
    .line 197
    const/4 v1, 0x1

    new-array v0, v1, [Ljava/lang/String;

    .line 198
    .local v0, "exPath":[Ljava/lang/String;
    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/sec/android/app/ve/export/Export;->getFileName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 199
    iget-object v1, p0, Lcom/sec/android/app/ve/export/Export;->mActivity:Landroid/app/Activity;

    const/4 v2, 0x0

    new-instance v3, Lcom/sec/android/app/ve/export/Export$1;

    invoke-direct {v3, p0}, Lcom/sec/android/app/ve/export/Export$1;-><init>(Lcom/sec/android/app/ve/export/Export;)V

    invoke-static {v1, v0, v2, v3}, Landroid/media/MediaScannerConnection;->scanFile(Landroid/content/Context;[Ljava/lang/String;[Ljava/lang/String;Landroid/media/MediaScannerConnection$OnScanCompletedListener;)V

    .line 206
    return-void
.end method

.method private setExportState(I)V
    .locals 0
    .param p1, "state"    # I

    .prologue
    .line 136
    iput p1, p0, Lcom/sec/android/app/ve/export/Export;->exportState:I

    .line 137
    return-void
.end method

.method public static setResolutionValue(I)V
    .locals 1
    .param p0, "index"    # I

    .prologue
    .line 546
    sget-object v0, Lcom/sec/android/app/ve/VEApp;->gAdaper:Lcom/sec/android/app/ve/VEAdapter;

    invoke-interface {v0, p0}, Lcom/sec/android/app/ve/VEAdapter;->getResolutionValue(I)I

    move-result v0

    sput v0, Lcom/sec/android/app/ve/export/Export;->resEnumValue:I

    .line 547
    return-void
.end method


# virtual methods
.method public changeNotificationContentView(Landroid/content/Context;Landroid/widget/RemoteViews;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "contentView"    # Landroid/widget/RemoteViews;

    .prologue
    .line 581
    iget-object v0, p0, Lcom/sec/android/app/ve/export/Export;->mAdapter:Lcom/sec/android/app/ve/export/Export$Adapter;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/ve/export/Export$Adapter;->changeNotificationContentView(Landroid/content/Context;Landroid/widget/RemoteViews;)V

    .line 582
    return-void
.end method

.method public getActivity()Landroid/app/Activity;
    .locals 1

    .prologue
    .line 497
    iget-object v0, p0, Lcom/sec/android/app/ve/export/Export;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method public getEstimatedFileSize()F
    .locals 1

    .prologue
    .line 451
    iget v0, p0, Lcom/sec/android/app/ve/export/Export;->estimatedSize:F

    return v0
.end method

.method public getExportProgress()I
    .locals 8

    .prologue
    .line 417
    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getJNIVersion()F

    move-result v5

    const/high16 v6, 0x40000000    # 2.0f

    cmpl-float v5, v5, v6

    if-nez v5, :cond_1

    .line 419
    :try_start_0
    iget-object v5, p0, Lcom/sec/android/app/ve/export/Export;->mService:Lcom/sec/android/app/ve/export/IExportService;

    invoke-interface {v5}, Lcom/sec/android/app/ve/export/IExportService;->getExportProgress()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    .line 447
    :cond_0
    :goto_0
    return v4

    .line 420
    :catch_0
    move-exception v2

    .line 422
    .local v2, "e":Landroid/os/RemoteException;
    invoke-virtual {v2}, Landroid/os/RemoteException;->printStackTrace()V

    .line 447
    const/4 v4, 0x0

    goto :goto_0

    .line 426
    .end local v2    # "e":Landroid/os/RemoteException;
    :cond_1
    const/4 v4, 0x0

    .line 427
    .local v4, "progress":I
    iget-object v5, p0, Lcom/sec/android/app/ve/export/Export;->expFile:Ljava/io/File;

    if-nez v5, :cond_2

    .line 428
    new-instance v3, Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/sec/android/app/ve/export/Export;->mFileName:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, ".tmp"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v5}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 429
    .local v3, "path":Ljava/lang/String;
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v5, p0, Lcom/sec/android/app/ve/export/Export;->expFile:Ljava/io/File;

    .line 431
    .end local v3    # "path":Ljava/lang/String;
    :cond_2
    iget v5, p0, Lcom/sec/android/app/ve/export/Export;->estimatedSize:F

    float-to-int v5, v5

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/sec/android/app/ve/export/Export;->expFile:Ljava/io/File;

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/sec/android/app/ve/export/Export;->expFile:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/sec/android/app/ve/export/Export;->expFile:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->isFile()Z

    move-result v5

    if-nez v5, :cond_4

    .line 433
    :cond_3
    const/4 v4, 0x0

    .line 441
    :goto_1
    const/16 v5, 0x64

    if-le v4, v5, :cond_0

    .line 442
    const-string v5, "EXPORT"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "progress "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "%, exceeded 100% "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 443
    const/16 v4, 0x64

    goto :goto_0

    .line 437
    :cond_4
    iget-object v5, p0, Lcom/sec/android/app/ve/export/Export;->expFile:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->length()J

    move-result-wide v0

    .line 438
    .local v0, "currentSize":J
    const-wide/16 v6, 0x64

    mul-long/2addr v6, v0

    long-to-float v5, v6

    iget v6, p0, Lcom/sec/android/app/ve/export/Export;->estimatedSize:F

    div-float/2addr v5, v6

    float-to-int v4, v5

    goto :goto_1
.end method

.method public getExportedTransode(I)Lcom/samsung/app/video/editor/external/TranscodeElement$ExportedTranscodeElement;
    .locals 5
    .param p1, "exportedRes"    # I

    .prologue
    const/4 v2, 0x0

    .line 510
    iget-object v3, p0, Lcom/sec/android/app/ve/export/Export;->mTrans:Lcom/samsung/app/video/editor/external/TranscodeElement;

    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getexportHM()Ljava/util/HashMap;

    move-result-object v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/sec/android/app/ve/export/Export;->mTrans:Lcom/samsung/app/video/editor/external/TranscodeElement;

    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getexportHM()Ljava/util/HashMap;

    move-result-object v3

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 512
    iget-object v3, p0, Lcom/sec/android/app/ve/export/Export;->mTrans:Lcom/samsung/app/video/editor/external/TranscodeElement;

    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getexportHM()Ljava/util/HashMap;

    move-result-object v3

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/app/video/editor/external/TranscodeElement$ExportedTranscodeElement;

    .line 513
    .local v0, "expT":Lcom/samsung/app/video/editor/external/TranscodeElement$ExportedTranscodeElement;
    new-instance v1, Ljava/io/File;

    invoke-virtual {v0}, Lcom/samsung/app/video/editor/external/TranscodeElement$ExportedTranscodeElement;->getfilepath()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 514
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 523
    .end local v0    # "expT":Lcom/samsung/app/video/editor/external/TranscodeElement$ExportedTranscodeElement;
    .end local v1    # "file":Ljava/io/File;
    :goto_0
    return-object v0

    .line 518
    .restart local v0    # "expT":Lcom/samsung/app/video/editor/external/TranscodeElement$ExportedTranscodeElement;
    .restart local v1    # "file":Ljava/io/File;
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/ve/export/Export;->mTrans:Lcom/samsung/app/video/editor/external/TranscodeElement;

    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getexportHM()Ljava/util/HashMap;

    move-result-object v3

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-object v0, v2

    .line 519
    goto :goto_0

    .end local v0    # "expT":Lcom/samsung/app/video/editor/external/TranscodeElement$ExportedTranscodeElement;
    .end local v1    # "file":Ljava/io/File;
    :cond_1
    move-object v0, v2

    .line 523
    goto :goto_0
.end method

.method public getFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/sec/android/app/ve/export/Export;->mFileName:Ljava/lang/String;

    return-object v0
.end method

.method public getHandler()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/sec/android/app/ve/export/Export;->mHandler:Lcom/sec/android/app/ve/export/Export$ThreadHandler;

    return-object v0
.end method

.method public getIsBackgroundExport()Z
    .locals 1

    .prologue
    .line 131
    iget-boolean v0, p0, Lcom/sec/android/app/ve/export/Export;->isBGExport:Z

    return v0
.end method

.method public getNotificationLayoutID()I
    .locals 1

    .prologue
    .line 551
    iget-object v0, p0, Lcom/sec/android/app/ve/export/Export;->mAdapter:Lcom/sec/android/app/ve/export/Export$Adapter;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/export/Export$Adapter;->getNotificationLayoutID()I

    move-result v0

    return v0
.end method

.method public getOngoingNotificationLayoutID()I
    .locals 1

    .prologue
    .line 556
    iget-object v0, p0, Lcom/sec/android/app/ve/export/Export;->mAdapter:Lcom/sec/android/app/ve/export/Export$Adapter;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/export/Export$Adapter;->getOngoingNotificationLayoutID()I

    move-result v0

    return v0
.end method

.method public getOngoingNotificationStringID()I
    .locals 1

    .prologue
    .line 565
    iget-object v0, p0, Lcom/sec/android/app/ve/export/Export;->mAdapter:Lcom/sec/android/app/ve/export/Export$Adapter;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/export/Export$Adapter;->getOngoingNotificationStringID()I

    move-result v0

    return v0
.end method

.method public getResolutionValue()I
    .locals 1

    .prologue
    .line 569
    iget-object v0, p0, Lcom/sec/android/app/ve/export/Export;->mAdapter:Lcom/sec/android/app/ve/export/Export$Adapter;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/export/Export$Adapter;->getResolutionValue()I

    move-result v0

    return v0
.end method

.method public getTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/sec/android/app/ve/export/Export;->mTrans:Lcom/samsung/app/video/editor/external/TranscodeElement;

    return-object v0
.end method

.method public initExportProgress(Ljava/lang/String;)V
    .locals 5
    .param p1, "filename"    # Ljava/lang/String;

    .prologue
    .line 456
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/app/ve/export/Export;->expFile:Ljava/io/File;

    .line 457
    sget v2, Lcom/sec/android/app/ve/export/Export;->resEnumValue:I

    invoke-static {v2}, Lcom/samsung/app/video/editor/external/Constants;->getVideoBitRate(I)I

    move-result v0

    .line 458
    .local v0, "bitRate":I
    iget-object v2, p0, Lcom/sec/android/app/ve/export/Export;->mTrans:Lcom/samsung/app/video/editor/external/TranscodeElement;

    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getTotalDuration()J

    move-result-wide v2

    long-to-float v2, v2

    const/high16 v3, 0x447a0000    # 1000.0f

    div-float v1, v2, v3

    .line 459
    .local v1, "totalMovieDuration":F
    const/high16 v2, 0x44800000    # 1024.0f

    int-to-float v3, v0

    mul-float/2addr v3, v1

    .line 460
    const/high16 v4, 0x43000000    # 128.0f

    mul-float/2addr v4, v1

    add-float/2addr v3, v4

    const/high16 v4, 0x41000000    # 8.0f

    div-float/2addr v3, v4

    mul-float/2addr v2, v3

    .line 459
    iput v2, p0, Lcom/sec/android/app/ve/export/Export;->estimatedSize:F

    .line 461
    return-void
.end method

.method public initiateNotificationContentView(Landroid/content/Context;Landroid/widget/RemoteViews;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "contentView"    # Landroid/widget/RemoteViews;

    .prologue
    .line 573
    iget-object v0, p0, Lcom/sec/android/app/ve/export/Export;->mAdapter:Lcom/sec/android/app/ve/export/Export$Adapter;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/ve/export/Export$Adapter;->initiateNotificationContentView(Landroid/content/Context;Landroid/widget/RemoteViews;)V

    .line 574
    return-void
.end method

.method public isDefaultPause()Z
    .locals 3

    .prologue
    .line 62
    const/4 v1, 0x0

    .line 63
    .local v1, "retVal":Z
    iget-object v2, p0, Lcom/sec/android/app/ve/export/Export;->mService:Lcom/sec/android/app/ve/export/IExportService;

    if-eqz v2, :cond_0

    .line 65
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/ve/export/Export;->mService:Lcom/sec/android/app/ve/export/IExportService;

    invoke-interface {v2}, Lcom/sec/android/app/ve/export/IExportService;->isDefaultPause()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 70
    :cond_0
    :goto_0
    return v1

    .line 66
    :catch_0
    move-exception v0

    .line 67
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public isExportAfterAutoEdit()Z
    .locals 1

    .prologue
    .line 493
    iget-object v0, p0, Lcom/sec/android/app/ve/export/Export;->mAdapter:Lcom/sec/android/app/ve/export/Export$Adapter;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/export/Export$Adapter;->isExportAfterAutoEdit()Z

    move-result v0

    return v0
.end method

.method public isExportRunning()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 164
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/ve/export/Export;->mService:Lcom/sec/android/app/ve/export/IExportService;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/ve/export/Export;->mService:Lcom/sec/android/app/ve/export/IExportService;

    invoke-interface {v2}, Lcom/sec/android/app/ve/export/IExportService;->isEThreadAlive()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-eqz v2, :cond_0

    .line 168
    :goto_0
    return v1

    .line 164
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 165
    :catch_0
    move-exception v0

    .line 167
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onAudioFocusChange(I)V
    .locals 2
    .param p1, "arg0"    # I

    .prologue
    const/4 v1, 0x1

    .line 402
    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    .line 403
    const/4 v0, -0x2

    if-eq p1, v0, :cond_0

    .line 404
    const/4 v0, -0x3

    if-ne p1, v0, :cond_1

    .line 405
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/ve/export/Export;->getExportState()I

    move-result v0

    if-ne v0, v1, :cond_1

    .line 407
    invoke-virtual {p0, v1}, Lcom/sec/android/app/ve/export/Export;->pauseExport(Z)V

    .line 410
    :cond_1
    return-void
.end method

.method public onExportProgressCancelled()V
    .locals 0

    .prologue
    .line 395
    invoke-virtual {p0}, Lcom/sec/android/app/ve/export/Export;->stopExport()V

    .line 396
    return-void
.end method

.method public onNotificationClick()V
    .locals 2

    .prologue
    .line 477
    invoke-direct {p0}, Lcom/sec/android/app/ve/export/Export;->getExportState()I

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/ve/export/Export;->mService:Lcom/sec/android/app/ve/export/IExportService;

    if-eqz v1, :cond_0

    .line 481
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/ve/export/Export;->mService:Lcom/sec/android/app/ve/export/IExportService;

    invoke-interface {v1}, Lcom/sec/android/app/ve/export/IExportService;->cancelNotification()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 486
    :cond_0
    :goto_0
    return-void

    .line 482
    :catch_0
    move-exception v0

    .line 483
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public pauseExport(Z)V
    .locals 3
    .param p1, "defaultPause"    # Z

    .prologue
    .line 92
    :try_start_0
    invoke-direct {p0}, Lcom/sec/android/app/ve/export/Export;->getExportState()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/ve/export/Export;->mService:Lcom/sec/android/app/ve/export/IExportService;

    if-eqz v1, :cond_0

    .line 95
    iget-object v1, p0, Lcom/sec/android/app/ve/export/Export;->mService:Lcom/sec/android/app/ve/export/IExportService;

    invoke-interface {v1, p1}, Lcom/sec/android/app/ve/export/IExportService;->pauseExport(Z)V

    .line 96
    iget-object v1, p0, Lcom/sec/android/app/ve/export/Export;->mAdapter:Lcom/sec/android/app/ve/export/Export$Adapter;

    invoke-virtual {v1}, Lcom/sec/android/app/ve/export/Export$Adapter;->nativeProcAfterExportPause()V

    .line 97
    const/4 v1, 0x2

    invoke-direct {p0, v1}, Lcom/sec/android/app/ve/export/Export;->setExportState(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 102
    :cond_0
    :goto_0
    return-void

    .line 99
    :catch_0
    move-exception v0

    .line 100
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public removeSavedTranscode(I)V
    .locals 2
    .param p1, "exportresolution"    # I

    .prologue
    .line 535
    iget-object v0, p0, Lcom/sec/android/app/ve/export/Export;->mTrans:Lcom/samsung/app/video/editor/external/TranscodeElement;

    invoke-virtual {v0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getexportHM()Ljava/util/HashMap;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/ve/export/Export;->mTrans:Lcom/samsung/app/video/editor/external/TranscodeElement;

    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getResolutionEnumValue()I

    move-result v1

    add-int/lit8 v1, v1, -0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 536
    iget-object v0, p0, Lcom/sec/android/app/ve/export/Export;->mTrans:Lcom/samsung/app/video/editor/external/TranscodeElement;

    invoke-virtual {v0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getexportHM()Ljava/util/HashMap;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/ve/export/Export;->mTrans:Lcom/samsung/app/video/editor/external/TranscodeElement;

    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getResolutionEnumValue()I

    move-result v1

    add-int/lit8 v1, v1, -0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 538
    :cond_0
    return-void
.end method

.method public resumeExport()V
    .locals 6

    .prologue
    .line 106
    const/16 v1, 0x3e8

    .line 109
    .local v1, "newDelay":I
    :try_start_0
    invoke-direct {p0}, Lcom/sec/android/app/ve/export/Export;->getExportState()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/ve/export/Export;->mService:Lcom/sec/android/app/ve/export/IExportService;

    if-eqz v2, :cond_1

    .line 111
    iget-object v2, p0, Lcom/sec/android/app/ve/export/Export;->mAdapter:Lcom/sec/android/app/ve/export/Export$Adapter;

    invoke-virtual {v2}, Lcom/sec/android/app/ve/export/Export$Adapter;->nativeProcBeforeExportResume()V

    .line 112
    iget-object v2, p0, Lcom/sec/android/app/ve/export/Export;->mService:Lcom/sec/android/app/ve/export/IExportService;

    invoke-interface {v2}, Lcom/sec/android/app/ve/export/IExportService;->resumeExport()V

    .line 113
    const/4 v2, 0x1

    invoke-direct {p0, v2}, Lcom/sec/android/app/ve/export/Export;->setExportState(I)V

    .line 114
    iget-object v2, p0, Lcom/sec/android/app/ve/export/Export;->mAdapter:Lcom/sec/android/app/ve/export/Export$Adapter;

    invoke-virtual {v2}, Lcom/sec/android/app/ve/export/Export$Adapter;->onExportResumed()V

    .line 115
    iget-object v2, p0, Lcom/sec/android/app/ve/export/Export;->mHandler:Lcom/sec/android/app/ve/export/Export$ThreadHandler;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Lcom/sec/android/app/ve/export/Export$ThreadHandler;->hasMessages(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 116
    iget-object v2, p0, Lcom/sec/android/app/ve/export/Export;->mHandler:Lcom/sec/android/app/ve/export/Export$ThreadHandler;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Lcom/sec/android/app/ve/export/Export$ThreadHandler;->removeMessages(I)V

    .line 117
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/ve/export/Export;->mHandler:Lcom/sec/android/app/ve/export/Export$ThreadHandler;

    const/4 v3, 0x4

    int-to-long v4, v1

    invoke-virtual {v2, v3, v4, v5}, Lcom/sec/android/app/ve/export/Export$ThreadHandler;->sendEmptyMessageDelayed(IJ)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 122
    :cond_1
    :goto_0
    return-void

    .line 119
    :catch_0
    move-exception v0

    .line 120
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public saveexportedtranscode(Lcom/samsung/app/video/editor/external/TranscodeElement;Ljava/lang/String;I)V
    .locals 3
    .param p1, "transcodeElement"    # Lcom/samsung/app/video/editor/external/TranscodeElement;
    .param p2, "fileName"    # Ljava/lang/String;
    .param p3, "i"    # I

    .prologue
    .line 542
    iget-object v0, p0, Lcom/sec/android/app/ve/export/Export;->mTrans:Lcom/samsung/app/video/editor/external/TranscodeElement;

    invoke-virtual {v0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getexportHM()Ljava/util/HashMap;

    move-result-object v0

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/ve/export/Export;->setExportedTranscode(Lcom/samsung/app/video/editor/external/TranscodeElement;Ljava/lang/String;)Lcom/samsung/app/video/editor/external/TranscodeElement$ExportedTranscodeElement;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 543
    return-void
.end method

.method public setActivity(Landroid/app/Activity;)V
    .locals 0
    .param p1, "acvt"    # Landroid/app/Activity;

    .prologue
    .line 501
    iput-object p1, p0, Lcom/sec/android/app/ve/export/Export;->mActivity:Landroid/app/Activity;

    .line 502
    return-void
.end method

.method public setAdapter(Lcom/sec/android/app/ve/export/Export$Adapter;)V
    .locals 0
    .param p1, "adp"    # Lcom/sec/android/app/ve/export/Export$Adapter;

    .prologue
    .line 505
    iput-object p1, p0, Lcom/sec/android/app/ve/export/Export;->mAdapter:Lcom/sec/android/app/ve/export/Export$Adapter;

    .line 506
    return-void
.end method

.method public setExportedTranscode(Lcom/samsung/app/video/editor/external/TranscodeElement;Ljava/lang/String;)Lcom/samsung/app/video/editor/external/TranscodeElement$ExportedTranscodeElement;
    .locals 1
    .param p1, "exportedtranscode"    # Lcom/samsung/app/video/editor/external/TranscodeElement;
    .param p2, "fileName"    # Ljava/lang/String;

    .prologue
    .line 527
    new-instance v0, Lcom/samsung/app/video/editor/external/TranscodeElement$ExportedTranscodeElement;

    invoke-direct {v0}, Lcom/samsung/app/video/editor/external/TranscodeElement$ExportedTranscodeElement;-><init>()V

    .line 528
    .local v0, "exportobject":Lcom/samsung/app/video/editor/external/TranscodeElement$ExportedTranscodeElement;
    invoke-virtual {v0, p2}, Lcom/samsung/app/video/editor/external/TranscodeElement$ExportedTranscodeElement;->setfilePath(Ljava/lang/String;)V

    .line 529
    invoke-virtual {v0, p1}, Lcom/samsung/app/video/editor/external/TranscodeElement$ExportedTranscodeElement;->setexportedTE(Lcom/samsung/app/video/editor/external/TranscodeElement;)V

    .line 530
    return-object v0
.end method

.method public setIsBackgroundExport(Z)V
    .locals 0
    .param p1, "isBGExport"    # Z

    .prologue
    .line 127
    iput-boolean p1, p0, Lcom/sec/android/app/ve/export/Export;->isBGExport:Z

    .line 128
    return-void
.end method

.method public setTranscodeElement(Lcom/samsung/app/video/editor/external/TranscodeElement;)V
    .locals 0
    .param p1, "trans"    # Lcom/samsung/app/video/editor/external/TranscodeElement;

    .prologue
    .line 141
    iput-object p1, p0, Lcom/sec/android/app/ve/export/Export;->mTrans:Lcom/samsung/app/video/editor/external/TranscodeElement;

    .line 142
    return-void
.end method

.method public stabilizationRequired()Z
    .locals 1

    .prologue
    .line 489
    iget-boolean v0, p0, Lcom/sec/android/app/ve/export/Export;->mStblzReqd:Z

    return v0
.end method

.method public start(Ljava/lang/String;IZLjava/lang/String;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "pos"    # I
    .param p3, "check"    # Z
    .param p4, "pkgName"    # Ljava/lang/String;

    .prologue
    .line 367
    iget-object v0, p0, Lcom/sec/android/app/ve/export/Export;->mAdapter:Lcom/sec/android/app/ve/export/Export$Adapter;

    if-eqz v0, :cond_0

    .line 384
    iget-object v0, p0, Lcom/sec/android/app/ve/export/Export;->mTrans:Lcom/samsung/app/video/editor/external/TranscodeElement;

    sget-object v1, Lcom/sec/android/app/ve/VEApp;->gAdaper:Lcom/sec/android/app/ve/VEAdapter;

    invoke-interface {v1, p2}, Lcom/sec/android/app/ve/VEAdapter;->getResolutionValue(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->setResolutionEnumValue(I)V

    .line 385
    sget-object v0, Lcom/sec/android/app/ve/VEApp;->gAdaper:Lcom/sec/android/app/ve/VEAdapter;

    invoke-interface {v0, p2}, Lcom/sec/android/app/ve/VEAdapter;->getResolutionValue(I)I

    move-result v0

    sput v0, Lcom/sec/android/app/ve/export/Export;->resEnumValue:I

    .line 386
    iput-boolean p3, p0, Lcom/sec/android/app/ve/export/Export;->mStblzReqd:Z

    .line 387
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/sec/android/app/ve/common/ConfigUtils;->EXPORT_PATH:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".mp4"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/ve/export/Export;->mFileName:Ljava/lang/String;

    .line 388
    invoke-direct {p0, p4}, Lcom/sec/android/app/ve/export/Export;->initService(Ljava/lang/String;)V

    .line 391
    :cond_0
    return-void
.end method

.method public stopExport()V
    .locals 3

    .prologue
    .line 76
    iget-object v1, p0, Lcom/sec/android/app/ve/export/Export;->mService:Lcom/sec/android/app/ve/export/IExportService;

    if-eqz v1, :cond_0

    .line 78
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/ve/export/Export;->mHandler:Lcom/sec/android/app/ve/export/Export$ThreadHandler;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/sec/android/app/ve/export/Export$ThreadHandler;->removeMessages(I)V

    .line 79
    iget-object v1, p0, Lcom/sec/android/app/ve/export/Export;->mService:Lcom/sec/android/app/ve/export/IExportService;

    invoke-interface {v1}, Lcom/sec/android/app/ve/export/IExportService;->stopExport()V

    .line 80
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/sec/android/app/ve/export/Export;->setExportState(I)V

    .line 81
    iget-object v1, p0, Lcom/sec/android/app/ve/export/Export;->mService:Lcom/sec/android/app/ve/export/IExportService;

    invoke-interface {v1}, Lcom/sec/android/app/ve/export/IExportService;->cancelNotification()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 86
    :cond_0
    :goto_0
    return-void

    .line 82
    :catch_0
    move-exception v0

    .line 83
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public togglePauseResume()V
    .locals 2

    .prologue
    .line 464
    invoke-direct {p0}, Lcom/sec/android/app/ve/export/Export;->getExportState()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 466
    invoke-virtual {p0}, Lcom/sec/android/app/ve/export/Export;->resumeExport()V

    .line 473
    :cond_0
    :goto_0
    return-void

    .line 468
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/ve/export/Export;->getExportState()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 470
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/ve/export/Export;->pauseExport(Z)V

    goto :goto_0
.end method

.method public updateNotificationContentView(Landroid/content/Context;Landroid/widget/RemoteViews;II)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "contentView"    # Landroid/widget/RemoteViews;
    .param p3, "progress"    # I
    .param p4, "state"    # I

    .prologue
    .line 577
    iget-object v0, p0, Lcom/sec/android/app/ve/export/Export;->mAdapter:Lcom/sec/android/app/ve/export/Export$Adapter;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/sec/android/app/ve/export/Export$Adapter;->updateNotificationContentView(Landroid/content/Context;Landroid/widget/RemoteViews;II)V

    .line 578
    return-void
.end method
