.class public Lcom/sec/android/app/storycam/view/PreviewViewGroup;
.super Landroid/widget/RelativeLayout;
.source "PreviewViewGroup.java"

# interfaces
.implements Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ActivityEventsCallback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/storycam/view/PreviewViewGroup$PlayerControlInterface;
    }
.end annotation


# static fields
.field private static final MSG_PLAY_AFTER_PROGRESS_CHANGE:I = 0x2

.field private static final MSG_UPDATE_STORY_TIME:I = 0x1

.field public static final PLAY_START_ERROR:I = -0x1

.field public static final PLAY_START_SUCCESS:I


# instance fields
.field private keyListener:Landroid/view/View$OnKeyListener;

.field private listener:Landroid/view/View$OnClickListener;

.field private mActivityStateListener:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityStateChangedListener;

.field private mCurrTransElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

.field private mFSListener:Lcom/sec/android/app/storycam/view/PreviewViewGroup$PlayerControlInterface;

.field private mHandle:Landroid/os/Handler;

.field private mHandler:Landroid/os/Handler;

.field private mPreview:Lcom/sec/android/app/ve/PreviewPlayerInterface;

.field private mPreviewListener:Lcom/sec/android/app/ve/PreviewPlayerInterface$Adapter;

.field private mProgressBar:Landroid/widget/SeekBar;

.field mSeekListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field private mStoryBoardCurrTime:Landroid/widget/TextView;

.field private mStoryBoardFullTime:Landroid/widget/TextView;

.field private mThemeVideoLengthSetter:Landroid/widget/ImageView;

.field private mTranscodeElementListener:Lcom/sec/android/app/storycam/AppDataManager$TranscodeElementChangeListener;

.field private mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

.field private mpreviewPlayImageView:Landroid/widget/ImageView;

.field private previewBottomDivider:Landroid/widget/ImageView;

.field private wasPlayingBeforeProgressTouch:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 104
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 57
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mCurrTransElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    .line 61
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mHandle:Landroid/os/Handler;

    .line 71
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->wasPlayingBeforeProgressTouch:Z

    .line 73
    new-instance v0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup$1;-><init>(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mHandler:Landroid/os/Handler;

    .line 403
    new-instance v0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup$2;-><init>(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mPreviewListener:Lcom/sec/android/app/ve/PreviewPlayerInterface$Adapter;

    .line 512
    new-instance v0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup$3;-><init>(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mFSListener:Lcom/sec/android/app/storycam/view/PreviewViewGroup$PlayerControlInterface;

    .line 623
    new-instance v0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup$4;-><init>(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->listener:Landroid/view/View$OnClickListener;

    .line 657
    new-instance v0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup$5;-><init>(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mTranscodeElementListener:Lcom/sec/android/app/storycam/AppDataManager$TranscodeElementChangeListener;

    .line 677
    new-instance v0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup$6;-><init>(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mActivityStateListener:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityStateChangedListener;

    .line 694
    new-instance v0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup$7;-><init>(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->keyListener:Landroid/view/View$OnKeyListener;

    .line 721
    new-instance v0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$8;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup$8;-><init>(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mSeekListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 105
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attr"    # Landroid/util/AttributeSet;

    .prologue
    .line 108
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 57
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mCurrTransElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    .line 61
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mHandle:Landroid/os/Handler;

    .line 71
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->wasPlayingBeforeProgressTouch:Z

    .line 73
    new-instance v0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup$1;-><init>(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mHandler:Landroid/os/Handler;

    .line 403
    new-instance v0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup$2;-><init>(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mPreviewListener:Lcom/sec/android/app/ve/PreviewPlayerInterface$Adapter;

    .line 512
    new-instance v0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup$3;-><init>(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mFSListener:Lcom/sec/android/app/storycam/view/PreviewViewGroup$PlayerControlInterface;

    .line 623
    new-instance v0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup$4;-><init>(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->listener:Landroid/view/View$OnClickListener;

    .line 657
    new-instance v0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup$5;-><init>(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mTranscodeElementListener:Lcom/sec/android/app/storycam/AppDataManager$TranscodeElementChangeListener;

    .line 677
    new-instance v0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup$6;-><init>(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mActivityStateListener:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityStateChangedListener;

    .line 694
    new-instance v0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup$7;-><init>(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->keyListener:Landroid/view/View$OnKeyListener;

    .line 721
    new-instance v0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$8;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup$8;-><init>(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mSeekListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 109
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)Lcom/sec/android/app/ve/PreviewPlayerInterface;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mPreview:Lcom/sec/android/app/ve/PreviewPlayerInterface;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)Lcom/samsung/app/video/editor/external/TranscodeElement;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mCurrTransElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    return-object v0
.end method

.method static synthetic access$10(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$11(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)J
    .locals 2

    .prologue
    .line 380
    invoke-direct {p0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->getSeekTimeForPlayback()J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic access$12(Lcom/sec/android/app/storycam/view/PreviewViewGroup;Lcom/samsung/app/video/editor/external/TranscodeElement;)V
    .locals 0

    .prologue
    .line 57
    iput-object p1, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mCurrTransElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    return-void
.end method

.method static synthetic access$13(Lcom/sec/android/app/storycam/view/PreviewViewGroup;I)V
    .locals 0

    .prologue
    .line 640
    invoke-direct {p0, p1}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->updateSeekBar(I)V

    return-void
.end method

.method static synthetic access$2(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)Landroid/widget/SeekBar;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mProgressBar:Landroid/widget/SeekBar;

    return-object v0
.end method

.method static synthetic access$3(Lcom/sec/android/app/storycam/view/PreviewViewGroup;Z)V
    .locals 0

    .prologue
    .line 71
    iput-boolean p1, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->wasPlayingBeforeProgressTouch:Z

    return-void
.end method

.method static synthetic access$4(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    return-object v0
.end method

.method static synthetic access$5(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$6(Lcom/sec/android/app/storycam/view/PreviewViewGroup;I)V
    .locals 0

    .prologue
    .line 272
    invoke-direct {p0, p1}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->refreshPreviewForCurrentSeekBarPosition(I)V

    return-void
.end method

.method static synthetic access$7(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mStoryBoardCurrTime:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$8(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)Z
    .locals 1

    .prologue
    .line 71
    iget-boolean v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->wasPlayingBeforeProgressTouch:Z

    return v0
.end method

.method static synthetic access$9(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)Lcom/sec/android/app/storycam/view/PreviewViewGroup$PlayerControlInterface;
    .locals 1

    .prologue
    .line 512
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mFSListener:Lcom/sec/android/app/storycam/view/PreviewViewGroup$PlayerControlInterface;

    return-object v0
.end method

.method private getSeekTimeForPlayback()J
    .locals 4

    .prologue
    .line 381
    iget-object v1, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mProgressBar:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    .line 382
    .local v0, "progress":I
    iget-object v1, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mProgressBar:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getMax()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 384
    const-wide/16 v2, 0x0

    .line 386
    :goto_0
    return-wide v2

    :cond_0
    int-to-long v2, v0

    goto :goto_0
.end method

.method private getTotalDurationForPlay()J
    .locals 14

    .prologue
    .line 356
    const-wide/16 v8, 0x0

    .line 358
    .local v8, "totalDuration":J
    iget-object v7, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mCurrTransElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    if-eqz v7, :cond_0

    .line 359
    iget-object v7, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mCurrTransElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    invoke-virtual {v7}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementList()Ljava/util/List;

    move-result-object v6

    .line 360
    .local v6, "list":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    iget-object v7, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mCurrTransElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    invoke-virtual {v7}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v0

    .line 361
    .local v0, "count":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-lt v4, v0, :cond_1

    .line 377
    .end local v0    # "count":I
    .end local v4    # "i":I
    .end local v6    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    :cond_0
    return-wide v8

    .line 362
    .restart local v0    # "count":I
    .restart local v4    # "i":I
    .restart local v6    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    :cond_1
    invoke-interface {v6, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/app/video/editor/external/Element;

    .line 363
    .local v3, "element":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/Element;->getEndTime()J

    move-result-wide v10

    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/Element;->getStartTime()J

    move-result-wide v12

    sub-long/2addr v10, v12

    add-long/2addr v8, v10

    .line 364
    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/Element;->getEditList()Ljava/util/List;

    move-result-object v7

    if-eqz v7, :cond_2

    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/Element;->getEditListSize()I

    move-result v7

    if-eqz v7, :cond_2

    add-int/lit8 v7, v0, -0x1

    if-eq v4, v7, :cond_2

    .line 365
    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/Element;->getEditList()Ljava/util/List;

    move-result-object v1

    .line 366
    .local v1, "editList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Edit;>;"
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    .line 367
    .local v2, "editSize":I
    const/4 v5, 0x0

    .local v5, "j":I
    :goto_1
    if-lt v5, v2, :cond_3

    .line 361
    .end local v1    # "editList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Edit;>;"
    .end local v2    # "editSize":I
    .end local v5    # "j":I
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 368
    .restart local v1    # "editList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Edit;>;"
    .restart local v2    # "editSize":I
    .restart local v5    # "j":I
    :cond_3
    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/app/video/editor/external/Edit;

    invoke-virtual {v7}, Lcom/samsung/app/video/editor/external/Edit;->getType()I

    move-result v7

    const/4 v10, 0x6

    if-ne v7, v10, :cond_4

    .line 369
    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/app/video/editor/external/Edit;

    invoke-virtual {v7}, Lcom/samsung/app/video/editor/external/Edit;->getSubType()I

    move-result v7

    if-eqz v7, :cond_4

    .line 371
    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/app/video/editor/external/Edit;

    invoke-virtual {v7}, Lcom/samsung/app/video/editor/external/Edit;->getTrans_duration()I

    move-result v7

    int-to-long v10, v7

    sub-long/2addr v8, v10

    .line 367
    :cond_4
    add-int/lit8 v5, v5, 0x1

    goto :goto_1
.end method

.method private init()V
    .locals 4

    .prologue
    .line 201
    const v0, 0x7f0d0041

    invoke-virtual {p0, v0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/ve/PreviewPlayerInterface;

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mPreview:Lcom/sec/android/app/ve/PreviewPlayerInterface;

    .line 202
    const v0, 0x7f0d0043

    invoke-virtual {p0, v0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mpreviewPlayImageView:Landroid/widget/ImageView;

    .line 203
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mpreviewPlayImageView:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 204
    new-instance v0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$9;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup$9;-><init>(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)V

    .line 209
    const-wide/16 v2, 0x12c

    .line 204
    invoke-virtual {p0, v0, v2, v3}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 210
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mpreviewPlayImageView:Landroid/widget/ImageView;

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/ConfigUtils;->setHovering(Landroid/content/Context;Landroid/view/View;)V

    .line 211
    const v0, 0x7f0d0047

    invoke-virtual {p0, v0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->previewBottomDivider:Landroid/widget/ImageView;

    .line 212
    sget-object v0, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    iget-object v1, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mTranscodeElementListener:Lcom/sec/android/app/storycam/AppDataManager$TranscodeElementChangeListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/storycam/AppDataManager;->addTranscodeElementChangeListener(Lcom/sec/android/app/storycam/AppDataManager$TranscodeElementChangeListener;)V

    .line 213
    const v0, 0x7f0d0044

    invoke-virtual {p0, v0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mStoryBoardCurrTime:Landroid/widget/TextView;

    .line 214
    const v0, 0x7f0d0046

    invoke-virtual {p0, v0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mStoryBoardFullTime:Landroid/widget/TextView;

    .line 215
    const v0, 0x7f0d0045

    invoke-virtual {p0, v0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mProgressBar:Landroid/widget/SeekBar;

    .line 216
    sget-object v0, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    const-string v1, "5"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 217
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mProgressBar:Landroid/widget/SeekBar;

    const v1, 0x7f020026

    invoke-static {v1}, Lcom/sec/android/app/ve/VEApp;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setThumb(Landroid/graphics/drawable/Drawable;)V

    .line 218
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mProgressBar:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mSeekListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 219
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mProgressBar:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->keyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 220
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mPreview:Lcom/sec/android/app/ve/PreviewPlayerInterface;

    iget-object v1, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mPreviewListener:Lcom/sec/android/app/ve/PreviewPlayerInterface$Adapter;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/ve/PreviewPlayerInterface;->setAdapter(Lcom/sec/android/app/ve/PreviewPlayerInterface$Adapter;)V

    .line 221
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    .line 222
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    iget-object v1, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mPreview:Lcom/sec/android/app/ve/PreviewPlayerInterface;

    invoke-interface {v0, v1}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;->setPreviewPlayer(Lcom/sec/android/app/ve/PreviewPlayerInterface;)V

    .line 223
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    iget-object v1, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mActivityStateListener:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityStateChangedListener;

    invoke-interface {v0, v1}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;->addActivityStateChangedListener(Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityStateChangedListener;)V

    .line 224
    const v0, 0x7f0d0048

    invoke-virtual {p0, v0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mThemeVideoLengthSetter:Landroid/widget/ImageView;

    .line 225
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mThemeVideoLengthSetter:Landroid/widget/ImageView;

    new-instance v1, Lcom/sec/android/app/storycam/view/PreviewViewGroup$10;

    invoke-direct {v1, p0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup$10;-><init>(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 234
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mThemeVideoLengthSetter:Landroid/widget/ImageView;

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/ConfigUtils;->setHovering(Landroid/content/Context;Landroid/view/View;)V

    .line 237
    return-void
.end method

.method private refreshPreviewForCurrentSeekBarPosition(I)V
    .locals 13
    .param p1, "progress"    # I

    .prologue
    .line 274
    iget-object v1, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mCurrTransElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mCurrTransElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v1

    if-lez v1, :cond_0

    .line 276
    iget-object v1, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mCurrTransElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementList()Ljava/util/List;

    move-result-object v9

    .line 277
    .local v9, "elementList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    const/4 v1, 0x0

    invoke-interface {v9, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/app/video/editor/external/Element;

    .line 278
    .local v2, "firstElement":Lcom/samsung/app/video/editor/external/Element;
    move v12, p1

    .line 279
    .local v12, "storyBoardTime":I
    const/4 v3, 0x0

    .line 282
    .local v3, "secondElement":Lcom/samsung/app/video/editor/external/Element;
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v0

    .line 284
    .local v0, "count":I
    const/4 v11, 0x1

    .local v11, "i":I
    :goto_0
    if-lt v11, v0, :cond_1

    .line 307
    :goto_1
    if-nez v3, :cond_4

    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/Element;->getStartTime()J

    move-result-wide v4

    :goto_2
    int-to-long v6, v12

    add-long/2addr v4, v6

    const/4 v6, 0x0

    int-to-float v7, p1

    move-object v1, p0

    invoke-virtual/range {v1 .. v7}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->refreshPreview(Lcom/samsung/app/video/editor/external/Element;Lcom/samsung/app/video/editor/external/Element;JIF)V

    .line 310
    .end local v0    # "count":I
    .end local v2    # "firstElement":Lcom/samsung/app/video/editor/external/Element;
    .end local v3    # "secondElement":Lcom/samsung/app/video/editor/external/Element;
    .end local v9    # "elementList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    .end local v11    # "i":I
    .end local v12    # "storyBoardTime":I
    :cond_0
    return-void

    .line 285
    .restart local v0    # "count":I
    .restart local v2    # "firstElement":Lcom/samsung/app/video/editor/external/Element;
    .restart local v3    # "secondElement":Lcom/samsung/app/video/editor/external/Element;
    .restart local v9    # "elementList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    .restart local v11    # "i":I
    .restart local v12    # "storyBoardTime":I
    :cond_1
    invoke-interface {v9, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/app/video/editor/external/Element;

    .line 286
    .local v8, "ele":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/Element;->getEndTime()J

    move-result-wide v4

    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/Element;->getStartTime()J

    move-result-wide v6

    sub-long/2addr v4, v6

    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/Element;->getOverLapAtEnd()I

    move-result v1

    int-to-long v6, v1

    sub-long/2addr v4, v6

    long-to-int v10, v4

    .line 288
    .local v10, "elementNonOverlapDuration":I
    if-gt v10, v12, :cond_3

    .line 289
    sub-int/2addr v12, v10

    .line 292
    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/Element;->getOverLapAtEnd()I

    move-result v1

    shl-int/lit8 v1, v1, 0x1

    if-ge v12, v1, :cond_2

    .line 293
    move-object v3, v8

    .line 294
    goto :goto_1

    .line 297
    :cond_2
    move-object v2, v8

    .line 298
    const/4 v3, 0x0

    .line 299
    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/Element;->getOverLapAtEnd()I

    move-result v1

    sub-int/2addr v12, v1

    .line 284
    add-int/lit8 v11, v11, 0x1

    goto :goto_0

    .line 303
    :cond_3
    const/4 v3, 0x0

    .line 304
    goto :goto_1

    .line 307
    .end local v8    # "ele":Lcom/samsung/app/video/editor/external/Element;
    .end local v10    # "elementNonOverlapDuration":I
    :cond_4
    const-wide/16 v4, 0x0

    goto :goto_2
.end method

.method private updateSeekBar(I)V
    .locals 7
    .param p1, "total_time"    # I

    .prologue
    const/16 v6, 0x3a

    .line 641
    iget-object v2, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mProgressBar:Landroid/widget/SeekBar;

    invoke-virtual {v2, p1}, Landroid/widget/SeekBar;->setMax(I)V

    .line 642
    int-to-long v2, p1

    sget-object v4, Lcom/sec/android/app/ve/util/CommonUtils$TimeInTextUnits;->MMSS:Lcom/sec/android/app/ve/util/CommonUtils$TimeInTextUnits;

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/ve/util/CommonUtils;->getTimeInText(JLcom/sec/android/app/ve/util/CommonUtils$TimeInTextUnits;)Ljava/lang/String;

    move-result-object v1

    .line 643
    .local v1, "fullTimeStr":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mStoryBoardFullTime:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 644
    iget-object v2, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mStoryBoardFullTime:Landroid/widget/TextView;

    .line 645
    new-instance v3, Ljava/lang/StringBuilder;

    const/4 v4, 0x0

    invoke-virtual {v1, v6}, Ljava/lang/String;->indexOf(I)I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v1, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 646
    const v4, 0x7f0701fd

    invoke-static {v4}, Lcom/sec/android/app/storycam/VEAppSpecific;->getStringValue(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1, v6}, Ljava/lang/String;->indexOf(I)I

    move-result v4

    .line 647
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    .line 646
    invoke-virtual {v1, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 647
    const v4, 0x7f0701fe

    invoke-static {v4}, Lcom/sec/android/app/storycam/VEAppSpecific;->getStringValue(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 645
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 644
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 649
    iget-object v2, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mProgressBar:Landroid/widget/SeekBar;

    invoke-virtual {v2}, Landroid/widget/SeekBar;->getProgress()I

    move-result v2

    int-to-long v2, v2

    sget-object v4, Lcom/sec/android/app/ve/util/CommonUtils$TimeInTextUnits;->MMSS:Lcom/sec/android/app/ve/util/CommonUtils$TimeInTextUnits;

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/ve/util/CommonUtils;->getTimeInText(JLcom/sec/android/app/ve/util/CommonUtils$TimeInTextUnits;)Ljava/lang/String;

    move-result-object v0

    .line 650
    .local v0, "currTimeStr":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mStoryBoardCurrTime:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 651
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->updateCurrentTimeContentDesc()V

    .line 653
    iget-object v2, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mProgressBar:Landroid/widget/SeekBar;

    invoke-virtual {v2}, Landroid/widget/SeekBar;->getProgress()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->refreshPreviewForCurrentSeekBarPosition(I)V

    .line 655
    return-void
.end method


# virtual methods
.method public clearNativeJobBeforeExport()Z
    .locals 1

    .prologue
    .line 313
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mPreview:Lcom/sec/android/app/ve/PreviewPlayerInterface;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mPreview:Lcom/sec/android/app/ve/PreviewPlayerInterface;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/PreviewPlayerInterface;->isPlaying()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mPreview:Lcom/sec/android/app/ve/PreviewPlayerInterface;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/PreviewPlayerInterface;->clearNativeJobBeforeExport()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public disablePlayButton()V
    .locals 3

    .prologue
    .line 618
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mpreviewPlayImageView:Landroid/widget/ImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 619
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mpreviewPlayImageView:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0700ae

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 621
    return-void
.end method

.method public enablePlayButton()V
    .locals 3

    .prologue
    .line 609
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mpreviewPlayImageView:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 610
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mpreviewPlayImageView:Landroid/widget/ImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 611
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mpreviewPlayImageView:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->listener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 612
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mpreviewPlayImageView:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0700b0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 614
    return-void
.end method

.method public isPreviewPlaying()Z
    .locals 1

    .prologue
    .line 317
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mPreview:Lcom/sec/android/app/ve/PreviewPlayerInterface;

    if-eqz v0, :cond_0

    .line 318
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mPreview:Lcom/sec/android/app/ve/PreviewPlayerInterface;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/PreviewPlayerInterface;->isPlaying()Z

    move-result v0

    .line 320
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onActivityActionBarTapped()V
    .locals 2

    .prologue
    .line 766
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->wasPlayingBeforeProgressTouch:Z

    .line 767
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 768
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 769
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mPreview:Lcom/sec/android/app/ve/PreviewPlayerInterface;

    if-eqz v0, :cond_0

    .line 770
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mFSListener:Lcom/sec/android/app/storycam/view/PreviewViewGroup$PlayerControlInterface;

    invoke-interface {v0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup$PlayerControlInterface;->stop_play()V

    .line 771
    :cond_0
    return-void
.end method

.method public onActivityPaused()V
    .locals 2

    .prologue
    .line 757
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->wasPlayingBeforeProgressTouch:Z

    .line 758
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 759
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 760
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mPreview:Lcom/sec/android/app/ve/PreviewPlayerInterface;

    if-eqz v0, :cond_0

    .line 761
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mFSListener:Lcom/sec/android/app/storycam/view/PreviewViewGroup$PlayerControlInterface;

    invoke-interface {v0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup$PlayerControlInterface;->stop_play()V

    .line 762
    :cond_0
    return-void
.end method

.method public onActivityResumed()V
    .locals 2

    .prologue
    .line 776
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    invoke-interface {v0}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;->getCurrentState()Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;->STATE_DEFAULT:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    if-ne v0, v1, :cond_0

    .line 777
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->refreshPreviewToCurrentSeekBar()V

    .line 778
    :cond_0
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 118
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onAttachedToWindow()V

    .line 119
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    if-eqz v0, :cond_0

    .line 120
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    invoke-interface {v0, p0}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;->setActivityEventsCallback(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ActivityEventsCallback;)V

    .line 121
    :cond_0
    return-void
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 11
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const v10, 0x7f0602e0

    const v9, 0x7f0602da

    const v8, 0x7f0602d9

    .line 139
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 141
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    check-cast v6, Landroid/widget/RelativeLayout$LayoutParams;

    .line 142
    .local v6, "tempParams":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-static {v8}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v7

    iput v7, v6, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 143
    invoke-static {v9}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v7

    iput v7, v6, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 144
    const v7, 0x7f060358

    invoke-static {v7}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v7

    iput v7, v6, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 145
    const v7, 0x7f060357

    invoke-static {v7}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v7

    iput v7, v6, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 147
    iget-object v7, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mPreview:Lcom/sec/android/app/ve/PreviewPlayerInterface;

    if-eqz v7, :cond_0

    .line 148
    iget-object v7, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mPreview:Lcom/sec/android/app/ve/PreviewPlayerInterface;

    invoke-virtual {v7}, Lcom/sec/android/app/ve/PreviewPlayerInterface;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 149
    .local v1, "params":Landroid/widget/RelativeLayout$LayoutParams;
    const v7, 0x7f06032f

    invoke-static {v7}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v7

    iput v7, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 150
    const v7, 0x7f060330

    invoke-static {v7}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v7

    iput v7, v1, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 151
    const v7, 0x7f060331

    invoke-static {v7}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v7

    iput v7, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 152
    const v7, 0x7f0602dc

    invoke-static {v7}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v7

    iput v7, v1, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 153
    const v7, 0x7f0602db

    invoke-static {v7}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v7

    iput v7, v1, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 154
    iget-object v7, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mPreview:Lcom/sec/android/app/ve/PreviewPlayerInterface;

    invoke-virtual {v7, v1}, Lcom/sec/android/app/ve/PreviewPlayerInterface;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 156
    .end local v1    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_0
    const v7, 0x7f0d0045

    invoke-virtual {p0, v7}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/SeekBar;

    .line 157
    .local v3, "seekBar":Landroid/widget/SeekBar;
    invoke-virtual {v3}, Landroid/widget/SeekBar;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout$LayoutParams;

    .line 158
    .local v4, "seekBarParams":Landroid/widget/RelativeLayout$LayoutParams;
    const v7, 0x7f060336

    invoke-static {v7}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v7

    iput v7, v4, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 159
    const v7, 0x7f060337

    invoke-static {v7}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v7

    iput v7, v4, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 160
    const v7, 0x7f06033a

    invoke-static {v7}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v7

    iput v7, v4, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 161
    invoke-virtual {v3, v4}, Landroid/widget/SeekBar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 163
    invoke-virtual {v3}, Landroid/widget/SeekBar;->getParent()Landroid/view/ViewParent;

    move-result-object v7

    check-cast v7, Landroid/view/ViewGroup;

    invoke-virtual {v7}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Landroid/widget/RelativeLayout$LayoutParams;

    .line 164
    .local v5, "seekBarParent":Landroid/widget/RelativeLayout$LayoutParams;
    const v7, 0x7f060335

    invoke-static {v7}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v7

    iput v7, v5, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 166
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 167
    .restart local v1    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-static {v8}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v7

    iput v7, v1, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 168
    invoke-static {v9}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v7

    iput v7, v1, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 169
    invoke-virtual {p0, v1}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 172
    iget-object v7, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mCurrTransElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    if-eqz v7, :cond_1

    .line 173
    iget-object v7, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mCurrTransElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    sget v8, Lcom/sec/android/app/ve/common/ConfigUtils;->PREVIEW_WIDTH:I

    invoke-virtual {v7, v8}, Lcom/samsung/app/video/editor/external/TranscodeElement;->setTargetDispWidth(I)V

    .line 174
    iget-object v7, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mCurrTransElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    sget v8, Lcom/sec/android/app/ve/common/ConfigUtils;->PREVIEW_HEIGHT:I

    invoke-virtual {v7, v8}, Lcom/samsung/app/video/editor/external/TranscodeElement;->setTargetDispHeight(I)V

    .line 177
    :cond_1
    iget-object v7, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mStoryBoardCurrTime:Landroid/widget/TextView;

    if-eqz v7, :cond_2

    .line 178
    iget-object v7, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mStoryBoardCurrTime:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 179
    .local v0, "lParams":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-static {v10}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v7

    iput v7, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 180
    iget-object v7, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mStoryBoardCurrTime:Landroid/widget/TextView;

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 182
    .end local v0    # "lParams":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_2
    iget-object v7, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mStoryBoardFullTime:Landroid/widget/TextView;

    if-eqz v7, :cond_3

    .line 183
    iget-object v7, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mStoryBoardFullTime:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 184
    .restart local v0    # "lParams":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-static {v10}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v7

    iput v7, v0, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 185
    iget-object v7, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mStoryBoardFullTime:Landroid/widget/TextView;

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 187
    .end local v0    # "lParams":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_3
    iget-object v7, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->previewBottomDivider:Landroid/widget/ImageView;

    if-eqz v7, :cond_4

    .line 188
    iget-object v7, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->previewBottomDivider:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 189
    .restart local v0    # "lParams":Landroid/widget/RelativeLayout$LayoutParams;
    const v7, 0x7f060309

    invoke-static {v7}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v7

    iput v7, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 192
    .end local v0    # "lParams":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_4
    iget-object v7, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mProgressBar:Landroid/widget/SeekBar;

    if-eqz v7, :cond_5

    .line 193
    iget-object v7, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mProgressBar:Landroid/widget/SeekBar;

    invoke-virtual {v7}, Landroid/widget/SeekBar;->getProgress()I

    move-result v2

    .line 194
    .local v2, "progress":I
    iget-object v7, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    if-eqz v7, :cond_5

    iget-object v7, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    invoke-interface {v7}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;->getCurrentState()Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    move-result-object v7

    sget-object v8, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;->STATE_DEFAULT:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    if-ne v7, v8, :cond_5

    .line 195
    invoke-direct {p0, v2}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->refreshPreviewForCurrentSeekBarPosition(I)V

    .line 198
    .end local v2    # "progress":I
    :cond_5
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 125
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onDetachedFromWindow()V

    .line 126
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->wasPlayingBeforeProgressTouch:Z

    .line 127
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 128
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 129
    sget-object v0, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    if-eqz v0, :cond_0

    .line 130
    sget-object v0, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    iget-object v1, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mTranscodeElementListener:Lcom/sec/android/app/storycam/AppDataManager$TranscodeElementChangeListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/storycam/AppDataManager;->removeTranscodeElementChangeListener(Lcom/sec/android/app/storycam/AppDataManager$TranscodeElementChangeListener;)V

    .line 131
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    if-eqz v0, :cond_1

    .line 132
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    invoke-interface {v0, p0}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;->removeActivityEventsCallback(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ActivityEventsCallback;)V

    .line 133
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    iget-object v1, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mActivityStateListener:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityStateChangedListener;

    invoke-interface {v0, v1}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;->removeActivityStateChangedListener(Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityStateChangedListener;)V

    .line 135
    :cond_1
    return-void
.end method

.method protected onFinishInflate()V
    .locals 0

    .prologue
    .line 112
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 113
    invoke-direct {p0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->init()V

    .line 114
    return-void
.end method

.method public pause_player()V
    .locals 2

    .prologue
    .line 333
    const-string v0, "VideoEditor_AutomationTest"

    const-string v1, "Player Paused"

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 334
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mFSListener:Lcom/sec/android/app/storycam/view/PreviewViewGroup$PlayerControlInterface;

    invoke-interface {v0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup$PlayerControlInterface;->pause_player()V

    .line 335
    return-void
.end method

.method public refreshPreview(Lcom/samsung/app/video/editor/external/Element;Lcom/samsung/app/video/editor/external/Element;JIF)V
    .locals 11
    .param p1, "firstElem"    # Lcom/samsung/app/video/editor/external/Element;
    .param p2, "secondElem"    # Lcom/samsung/app/video/editor/external/Element;
    .param p3, "time"    # J
    .param p5, "prevType"    # I
    .param p6, "storyBoardTime"    # F

    .prologue
    .line 341
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mCurrTransElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    if-eqz v0, :cond_0

    .line 342
    const/4 v8, 0x0

    .line 343
    .local v8, "clipartList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mCurrTransElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    invoke-virtual {v0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getTextEleList()Ljava/util/List;

    move-result-object v8

    .line 345
    const/4 v9, 0x0

    .line 346
    .local v9, "drawList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mCurrTransElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    invoke-virtual {v0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getDrawEleList()Ljava/util/List;

    move-result-object v9

    .line 348
    iget-object v1, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mPreview:Lcom/sec/android/app/ve/PreviewPlayerInterface;

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move/from16 v6, p5

    move/from16 v7, p6

    invoke-virtual/range {v1 .. v9}, Lcom/sec/android/app/ve/PreviewPlayerInterface;->update(Lcom/samsung/app/video/editor/external/Element;Lcom/samsung/app/video/editor/external/Element;JIFLjava/util/List;Ljava/util/List;)V

    .line 350
    .end local v8    # "clipartList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    .end local v9    # "drawList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    :cond_0
    return-void
.end method

.method public refreshPreviewToCurrentSeekBar()V
    .locals 4

    .prologue
    .line 260
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mProgressBar:Landroid/widget/SeekBar;

    if-eqz v0, :cond_0

    .line 261
    new-instance v0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$11;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup$11;-><init>(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)V

    .line 268
    const-wide/16 v2, 0xa

    .line 261
    invoke-virtual {p0, v0, v2, v3}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 271
    :cond_0
    return-void
.end method

.method public setCurrentTranscodeElement(Lcom/samsung/app/video/editor/external/TranscodeElement;)V
    .locals 1
    .param p1, "transElement"    # Lcom/samsung/app/video/editor/external/TranscodeElement;

    .prologue
    .line 254
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mCurrTransElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    if-eqz v0, :cond_0

    .line 255
    iput-object p1, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mCurrTransElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    .line 256
    :cond_0
    return-void
.end method

.method public setPlayButtonVisibility(I)V
    .locals 1
    .param p1, "visibility"    # I

    .prologue
    .line 589
    if-nez p1, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->wasPlayingBeforeProgressTouch:Z

    if-nez v0, :cond_0

    .line 590
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->enablePlayButton()V

    .line 596
    :goto_0
    return-void

    .line 593
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->disablePlayButton()V

    goto :goto_0
.end method

.method public setPreviewBackground(II)V
    .locals 4
    .param p1, "color"    # I
    .param p2, "delay"    # I

    .prologue
    .line 391
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mHandle:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/storycam/view/PreviewViewGroup$12;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/storycam/view/PreviewViewGroup$12;-><init>(Lcom/sec/android/app/storycam/view/PreviewViewGroup;I)V

    .line 400
    int-to-long v2, p2

    .line 391
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 401
    return-void
.end method

.method public setProgressBar(I)V
    .locals 1
    .param p1, "progress"    # I

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mProgressBar:Landroid/widget/SeekBar;

    if-eqz v0, :cond_0

    .line 244
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mProgressBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, p1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 246
    :cond_0
    return-void
.end method

.method public start_play()I
    .locals 2

    .prologue
    .line 325
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    if-eqz v0, :cond_0

    .line 326
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;->requestFocusForMainActivity(Z)V

    .line 327
    :cond_0
    const-string v0, "VideoEditor_AutomationTest"

    const-string v1, "Player Started"

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 328
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mFSListener:Lcom/sec/android/app/storycam/view/PreviewViewGroup$PlayerControlInterface;

    invoke-interface {v0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup$PlayerControlInterface;->start_play()I

    move-result v0

    return v0
.end method

.method protected updateCurrentTimeContentDesc()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/16 v6, 0x3a

    .line 598
    iget-object v4, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mStoryBoardCurrTime:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    .line 599
    .local v3, "totalTime":Ljava/lang/String;
    invoke-virtual {v3, v6}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 600
    .local v1, "secStr":Ljava/lang/String;
    invoke-virtual {v3, v6}, Ljava/lang/String;->indexOf(I)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v3, v7, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 601
    .local v2, "temp":Ljava/lang/String;
    invoke-virtual {v2, v6}, Ljava/lang/String;->indexOf(I)I

    move-result v4

    invoke-virtual {v2, v7, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 603
    .local v0, "minStr":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mStoryBoardCurrTime:Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 604
    const v6, 0x7f0701fd

    invoke-static {v6}, Lcom/sec/android/app/storycam/VEAppSpecific;->getStringValue(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f0701fe

    invoke-static {v6}, Lcom/sec/android/app/storycam/VEAppSpecific;->getStringValue(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 603
    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 605
    return-void
.end method
