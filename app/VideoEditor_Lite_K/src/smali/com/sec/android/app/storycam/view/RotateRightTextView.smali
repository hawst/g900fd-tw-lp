.class public Lcom/sec/android/app/storycam/view/RotateRightTextView;
.super Landroid/widget/TextView;
.source "RotateRightTextView.java"


# instance fields
.field private height:I

.field private width:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 14
    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 15
    return-void
.end method


# virtual methods
.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v1, 0x0

    .line 19
    const/high16 v0, 0x42b40000    # 90.0f

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->rotate(F)V

    .line 20
    iget v0, p0, Lcom/sec/android/app/storycam/view/RotateRightTextView;->width:I

    neg-int v0, v0

    iget v2, p0, Lcom/sec/android/app/storycam/view/RotateRightTextView;->height:I

    add-int/2addr v0, v2

    int-to-float v0, v0

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v0, v2

    iget v2, p0, Lcom/sec/android/app/storycam/view/RotateRightTextView;->width:I

    neg-int v2, v2

    int-to-float v2, v2

    invoke-virtual {p1, v0, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 21
    iget v0, p0, Lcom/sec/android/app/storycam/view/RotateRightTextView;->width:I

    int-to-float v3, v0

    iget v0, p0, Lcom/sec/android/app/storycam/view/RotateRightTextView;->height:I

    int-to-float v4, v0

    sget-object v5, Landroid/graphics/Region$Op;->REPLACE:Landroid/graphics/Region$Op;

    move-object v0, p1

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->clipRect(FFFFLandroid/graphics/Region$Op;)Z

    .line 22
    invoke-super {p0, p1}, Landroid/widget/TextView;->onDraw(Landroid/graphics/Canvas;)V

    .line 23
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 0
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 26
    iput p1, p0, Lcom/sec/android/app/storycam/view/RotateRightTextView;->width:I

    .line 27
    iput p2, p0, Lcom/sec/android/app/storycam/view/RotateRightTextView;->height:I

    .line 28
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/TextView;->onSizeChanged(IIII)V

    .line 29
    return-void
.end method
