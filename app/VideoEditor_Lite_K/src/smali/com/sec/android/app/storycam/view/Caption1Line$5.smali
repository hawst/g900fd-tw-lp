.class Lcom/sec/android/app/storycam/view/Caption1Line$5;
.super Ljava/lang/Object;
.source "Caption1Line.java"

# interfaces
.implements Landroid/widget/TextView$OnEditorActionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/storycam/view/Caption1Line;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/storycam/view/Caption1Line;


# direct methods
.method constructor <init>(Lcom/sec/android/app/storycam/view/Caption1Line;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/storycam/view/Caption1Line$5;->this$0:Lcom/sec/android/app/storycam/view/Caption1Line;

    .line 390
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 5
    .param p1, "v"    # Landroid/widget/TextView;
    .param p2, "actionId"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 394
    const/4 v3, 0x6

    if-ne p2, v3, :cond_1

    .line 395
    iget-object v3, p0, Lcom/sec/android/app/storycam/view/Caption1Line$5;->this$0:Lcom/sec/android/app/storycam/view/Caption1Line;

    iget-object v4, p0, Lcom/sec/android/app/storycam/view/Caption1Line$5;->this$0:Lcom/sec/android/app/storycam/view/Caption1Line;

    invoke-virtual {v4}, Lcom/sec/android/app/storycam/view/Caption1Line;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-interface {v4}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/storycam/view/Caption1Line;->updateTitle(Ljava/lang/String;)V

    .line 397
    invoke-static {}, Landroid/view/inputmethod/InputMethodManager;->peekInstance()Landroid/view/inputmethod/InputMethodManager;

    move-result-object v0

    .line 398
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Landroid/view/inputmethod/InputMethodManager;->isActive(Landroid/view/View;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 399
    invoke-virtual {p1}, Landroid/widget/TextView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v3

    invoke-virtual {v0, v3, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 401
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/storycam/view/Caption1Line$5;->this$0:Lcom/sec/android/app/storycam/view/Caption1Line;

    # getter for: Lcom/sec/android/app/storycam/view/Caption1Line;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;
    invoke-static {v2}, Lcom/sec/android/app/storycam/view/Caption1Line;->access$4(Lcom/sec/android/app/storycam/view/Caption1Line;)Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    move-result-object v2

    invoke-interface {v2, v1}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;->requestFocusForMainActivity(Z)V

    .line 404
    .end local v0    # "imm":Landroid/view/inputmethod/InputMethodManager;
    :goto_0
    return v1

    :cond_1
    move v1, v2

    goto :goto_0
.end method
