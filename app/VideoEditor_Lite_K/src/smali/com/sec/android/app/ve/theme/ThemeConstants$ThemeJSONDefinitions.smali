.class public Lcom/sec/android/app/ve/theme/ThemeConstants$ThemeJSONDefinitions;
.super Ljava/lang/Object;
.source "ThemeConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/ve/theme/ThemeConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ThemeJSONDefinitions"
.end annotation


# static fields
.field public static final ALPHA_END:Ljava/lang/String; = "ALPHA_END"

.field public static final ALPHA_START:Ljava/lang/String; = "ALPHA_START"

.field public static final ANIM_ARRAY:Ljava/lang/String; = "ANIM_ARRAY"

.field public static final ANIM_END:Ljava/lang/String; = "ANIM_END"

.field public static final ANIM_INTERPOLATION:Ljava/lang/String; = "ANIM_INTERPOLATION"

.field public static final ANIM_START:Ljava/lang/String; = "ANIM_START"

.field public static final ANIM_TYPE:Ljava/lang/String; = "ANIM_TYPE"

.field public static final BITMAP_ANIM_ARRAY:Ljava/lang/String; = "BITMAP_ANIM_ARRAY"

.field public static final BLENDING_FILES_ELEMENT_INDEX:Ljava/lang/String; = "BLENDING_FILES_ELEMENT_INDEX"

.field public static final BLENDING_FILES_END_FRAME:Ljava/lang/String; = "BLENDING_FILES_END_TIME"

.field public static final BLENDING_FILES_END_INDEX:Ljava/lang/String; = "BLENDING_FILES_END_INDEX"

.field public static final BLENDING_FILES_PNG_FILE_NAME:Ljava/lang/String; = "BLENDING_FILES_PNG_FILE_NAME"

.field public static final BLENDING_FILES_START_FRAME:Ljava/lang/String; = "BLENDING_FILES_START_TIME"

.field public static final BLENDING_FILES_START_INDEX:Ljava/lang/String; = "BLENDING_FILES_START_INDEX"

.field public static final BOTTOM:Ljava/lang/String; = "BOTTOM"

.field public static final CAPTION_EDITABLE:Ljava/lang/String; = "CAPTION_EDITABLE"

.field public static final CAPTION_ID:Ljava/lang/String; = "CAPTION_ID"

.field public static final CAPTION_LIST:Ljava/lang/String; = "CAPTION_LIST"

.field public static final CAPTION_RAW_FILE:Ljava/lang/String; = "CAPTION_RAW_FILE"

.field public static final CAPTION_TEXT:Ljava/lang/String; = "CAPTION_TEXT"

.field public static final DEFAULT_ALPHA:Ljava/lang/String; = "DEFAULT_ALPHA"

.field public static final DEFAULT_ROTATE:Ljava/lang/String; = "DEFAULT_ROTATE"

.field public static final DEFAULT_SCALE:Ljava/lang/String; = "DEFAULT_SCALE"

.field public static final DEFAULT_TRANSLATE_X:Ljava/lang/String; = "DEFAULT_TRANSLATEX"

.field public static final DEFAULT_TRANSLATE_Y:Ljava/lang/String; = "DEFAULT_TRANSLATEY"

.field public static final EFFECTS_ARRAY:Ljava/lang/String; = "EFFECTS_ARRAY"

.field public static final EFFECT_END_TIME:Ljava/lang/String; = "EFFECT_END_TIME"

.field public static final EFFECT_RESOURCE_PATH:Ljava/lang/String; = "EFFECT_RESOURCE_PATH"

.field public static final EFFECT_START_TIME:Ljava/lang/String; = "EFFECT_START_TIME"

.field public static final EFFECT_SUB_TYPE:Ljava/lang/String; = "EFFECT_TYPE"

.field public static final ELEMENT:Ljava/lang/String; = "ELEMENT"

.field public static final ELEMENT_ARRAY:Ljava/lang/String; = "ELEMENT_ARRAY"

.field public static final ELEMENT_DURATION:Ljava/lang/String; = "ELEMENT_DURATION"

.field public static final ELEMENT_FILE_PATH:Ljava/lang/String; = "ELEMENT_FILE_PATH"

.field public static final ELEMENT_SUB_TYPE:Ljava/lang/String; = "ELEMENT_SUB_TYPE"

.field public static final ELEMENT_TYPE:Ljava/lang/String; = "ELEMENT_TYPE"

.field public static final FADE_TRANS_TYPE:Ljava/lang/String; = "FADE_TRANS_TYPE"

.field public static final FONT:Ljava/lang/String; = "FONT"

.field public static final FONT_ALIGN:Ljava/lang/String; = "FONT_ALIGN"

.field public static final FONT_CENTER:I = 0x1

.field public static final FONT_COLOR:Ljava/lang/String; = "FONT_COLOR"

.field public static final FONT_FILE:Ljava/lang/String; = "FONT_FILE"

.field public static final FONT_LEFT:I = 0x0

.field public static final FONT_RIGHT:I = 0x2

.field public static final FONT_SIZE:Ljava/lang/String; = "FONT_SIZE"

.field public static final FONT_TEXT_STYLE:Ljava/lang/String; = "FONT_TEXT_STYLE"

.field public static final IS_PLATFORM_FONT:Ljava/lang/String; = "IS_PLATFORM_FONT"

.field public static final LEFT:Ljava/lang/String; = "LEFT"

.field public static final PAINT_FLAGS:Ljava/lang/String; = "PAINT_FLAGS"

.field public static final PIVOT_X:Ljava/lang/String; = "PIVOT_X"

.field public static final PIVOT_Y:Ljava/lang/String; = "PIVOT_Y"

.field public static final RIGHT:Ljava/lang/String; = "RIGHT"

.field public static final ROTATION_END:Ljava/lang/String; = "ROTATION_END"

.field public static final ROTATION_START:Ljava/lang/String; = "ROTATION_START"

.field public static final SCALE_END_X:Ljava/lang/String; = "SCALE_END_X"

.field public static final SCALE_END_Y:Ljava/lang/String; = "SCALE_END_Y"

.field public static final SCALE_START_X:Ljava/lang/String; = "SCALE_START_X"

.field public static final SCALE_START_Y:Ljava/lang/String; = "SCALE_START_Y"

.field public static final SHADOW_COLOR:Ljava/lang/String; = "SHADOW_COLOR"

.field public static final SHADOW_R:Ljava/lang/String; = "SHADOW_R"

.field public static final SHADOW_X:Ljava/lang/String; = "SHADOW_X"

.field public static final SHADOW_Y:Ljava/lang/String; = "SHADOW_Y"

.field public static final STORYBOARD_ELEMENT_INDEX:Ljava/lang/String; = "STORYBOARD_ELEMENT_INDEX"

.field public static final STORYBOARD_END_FRAME:Ljava/lang/String; = "STORYBOARD_END_FRAME"

.field public static final STORYBOARD_START_FRAME:Ljava/lang/String; = "STORYBOARD_START_FRAME"

.field public static final THEME_BGM:Ljava/lang/String; = "THEME_BGM"

.field public static final THEME_BGM_DISPLAY_NAME:Ljava/lang/String; = "THEME_BGM_DISPLAY_NAME"

.field public static final THEME_BGM_DURATION:Ljava/lang/String; = "THEME_BGM_DURATION"

.field public static final THEME_FILTER:Ljava/lang/String; = "THEME_FILTER"

.field public static final TOP:Ljava/lang/String; = "TOP"

.field public static final TRANSITION_DURATION:Ljava/lang/String; = "TRANSITION_DURATION"

.field public static final TRANSITION_TYPE:Ljava/lang/String; = "TRANSITION_TYPE"

.field public static final TRANSLATION_FROM_X:Ljava/lang/String; = "TRANSLATION_FROM_X"

.field public static final TRANSLATION_FROM_Y:Ljava/lang/String; = "TRANSLATION_FROM_Y"

.field public static final TRANSLATION_TO_X:Ljava/lang/String; = "TRANSLATION_TO_X"

.field public static final TRANSLATION_TO_Y:Ljava/lang/String; = "TRANSLATION_TO_Y"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
