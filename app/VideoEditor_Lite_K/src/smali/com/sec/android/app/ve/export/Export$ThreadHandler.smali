.class Lcom/sec/android/app/ve/export/Export$ThreadHandler;
.super Landroid/os/Handler;
.source "Export.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/ve/export/Export;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ThreadHandler"
.end annotation


# instance fields
.field mDelay:I

.field mProgress:I

.field final synthetic this$0:Lcom/sec/android/app/ve/export/Export;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/ve/export/Export;Landroid/os/Looper;)V
    .locals 1
    .param p2, "mainLooper"    # Landroid/os/Looper;

    .prologue
    .line 244
    iput-object p1, p0, Lcom/sec/android/app/ve/export/Export$ThreadHandler;->this$0:Lcom/sec/android/app/ve/export/Export;

    .line 245
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 241
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/ve/export/Export$ThreadHandler;->mProgress:I

    .line 242
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/sec/android/app/ve/export/Export$ThreadHandler;->mDelay:I

    .line 246
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 10
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v5, 0x2

    const/4 v9, 0x4

    const/4 v8, 0x0

    .line 250
    iget v4, p1, Landroid/os/Message;->what:I

    packed-switch v4, :pswitch_data_0

    .line 362
    :cond_0
    :goto_0
    return-void

    .line 253
    :pswitch_0
    iget-object v4, p0, Lcom/sec/android/app/ve/export/Export$ThreadHandler;->this$0:Lcom/sec/android/app/ve/export/Export;

    # invokes: Lcom/sec/android/app/ve/export/Export;->releaseService()V
    invoke-static {v4}, Lcom/sec/android/app/ve/export/Export;->access$4(Lcom/sec/android/app/ve/export/Export;)V

    .line 254
    invoke-virtual {p0, v9}, Lcom/sec/android/app/ve/export/Export$ThreadHandler;->removeMessages(I)V

    .line 255
    iget-object v4, p0, Lcom/sec/android/app/ve/export/Export$ThreadHandler;->this$0:Lcom/sec/android/app/ve/export/Export;

    # invokes: Lcom/sec/android/app/ve/export/Export;->setExportState(I)V
    invoke-static {v4, v8}, Lcom/sec/android/app/ve/export/Export;->access$2(Lcom/sec/android/app/ve/export/Export;I)V

    .line 256
    iput v8, p0, Lcom/sec/android/app/ve/export/Export$ThreadHandler;->mProgress:I

    .line 259
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/app/ve/export/Export$ThreadHandler;->this$0:Lcom/sec/android/app/ve/export/Export;

    iget-object v4, v4, Lcom/sec/android/app/ve/export/Export;->mAdapter:Lcom/sec/android/app/ve/export/Export$Adapter;

    invoke-virtual {v4}, Lcom/sec/android/app/ve/export/Export$Adapter;->onExportStopped()V

    .line 260
    iget-object v4, p0, Lcom/sec/android/app/ve/export/Export$ThreadHandler;->this$0:Lcom/sec/android/app/ve/export/Export;

    # getter for: Lcom/sec/android/app/ve/export/Export;->mService:Lcom/sec/android/app/ve/export/IExportService;
    invoke-static {v4}, Lcom/sec/android/app/ve/export/Export;->access$1(Lcom/sec/android/app/ve/export/Export;)Lcom/sec/android/app/ve/export/IExportService;

    move-result-object v4

    invoke-interface {v4}, Lcom/sec/android/app/ve/export/IExportService;->cancelNotification()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 261
    :catch_0
    move-exception v0

    .line 263
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 268
    .end local v0    # "e":Ljava/lang/Exception;
    :pswitch_1
    new-instance v4, Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/sec/android/app/ve/export/Export$ThreadHandler;->this$0:Lcom/sec/android/app/ve/export/Export;

    # getter for: Lcom/sec/android/app/ve/export/Export;->mFileName:Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/app/ve/export/Export;->access$5(Lcom/sec/android/app/ve/export/Export;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, ".tmp"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 269
    .local v2, "path":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 270
    .local v1, "from":Ljava/io/File;
    new-instance v3, Ljava/io/File;

    iget-object v4, p0, Lcom/sec/android/app/ve/export/Export$ThreadHandler;->this$0:Lcom/sec/android/app/ve/export/Export;

    # getter for: Lcom/sec/android/app/ve/export/Export;->mFileName:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/app/ve/export/Export;->access$5(Lcom/sec/android/app/ve/export/Export;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 271
    .local v3, "to":Ljava/io/File;
    invoke-virtual {v1, v3}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 274
    :try_start_1
    iget-object v4, p0, Lcom/sec/android/app/ve/export/Export$ThreadHandler;->this$0:Lcom/sec/android/app/ve/export/Export;

    # getter for: Lcom/sec/android/app/ve/export/Export;->mService:Lcom/sec/android/app/ve/export/IExportService;
    invoke-static {v4}, Lcom/sec/android/app/ve/export/Export;->access$1(Lcom/sec/android/app/ve/export/Export;)Lcom/sec/android/app/ve/export/IExportService;

    move-result-object v4

    invoke-interface {v4}, Lcom/sec/android/app/ve/export/IExportService;->changeNotification()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 279
    :goto_1
    iget-object v4, p0, Lcom/sec/android/app/ve/export/Export$ThreadHandler;->this$0:Lcom/sec/android/app/ve/export/Export;

    iget-object v4, v4, Lcom/sec/android/app/ve/export/Export;->mAdapter:Lcom/sec/android/app/ve/export/Export$Adapter;

    const/16 v5, 0x64

    invoke-virtual {v4, v5}, Lcom/sec/android/app/ve/export/Export$Adapter;->onExportProgressUpdate(I)V

    .line 280
    iget-object v4, p0, Lcom/sec/android/app/ve/export/Export$ThreadHandler;->this$0:Lcom/sec/android/app/ve/export/Export;

    iget-object v4, v4, Lcom/sec/android/app/ve/export/Export;->mAdapter:Lcom/sec/android/app/ve/export/Export$Adapter;

    invoke-virtual {v4}, Lcom/sec/android/app/ve/export/Export$Adapter;->onExportCompleted()V

    .line 281
    iget-object v4, p0, Lcom/sec/android/app/ve/export/Export$ThreadHandler;->this$0:Lcom/sec/android/app/ve/export/Export;

    # invokes: Lcom/sec/android/app/ve/export/Export;->releaseService()V
    invoke-static {v4}, Lcom/sec/android/app/ve/export/Export;->access$4(Lcom/sec/android/app/ve/export/Export;)V

    .line 282
    iget-object v4, p0, Lcom/sec/android/app/ve/export/Export$ThreadHandler;->this$0:Lcom/sec/android/app/ve/export/Export;

    # invokes: Lcom/sec/android/app/ve/export/Export;->runMediaScanner()V
    invoke-static {v4}, Lcom/sec/android/app/ve/export/Export;->access$6(Lcom/sec/android/app/ve/export/Export;)V

    .line 284
    iget-object v4, p0, Lcom/sec/android/app/ve/export/Export$ThreadHandler;->this$0:Lcom/sec/android/app/ve/export/Export;

    iget-object v5, p0, Lcom/sec/android/app/ve/export/Export$ThreadHandler;->this$0:Lcom/sec/android/app/ve/export/Export;

    # getter for: Lcom/sec/android/app/ve/export/Export;->mTrans:Lcom/samsung/app/video/editor/external/TranscodeElement;
    invoke-static {v5}, Lcom/sec/android/app/ve/export/Export;->access$7(Lcom/sec/android/app/ve/export/Export;)Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getResolutionEnumValue()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/ve/export/Export;->removeSavedTranscode(I)V

    .line 286
    iget-object v4, p0, Lcom/sec/android/app/ve/export/Export$ThreadHandler;->this$0:Lcom/sec/android/app/ve/export/Export;

    new-instance v5, Lcom/samsung/app/video/editor/external/TranscodeElement;

    iget-object v6, p0, Lcom/sec/android/app/ve/export/Export$ThreadHandler;->this$0:Lcom/sec/android/app/ve/export/Export;

    # getter for: Lcom/sec/android/app/ve/export/Export;->mTrans:Lcom/samsung/app/video/editor/external/TranscodeElement;
    invoke-static {v6}, Lcom/sec/android/app/ve/export/Export;->access$7(Lcom/sec/android/app/ve/export/Export;)Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/samsung/app/video/editor/external/TranscodeElement;-><init>(Lcom/samsung/app/video/editor/external/TranscodeElement;)V

    iget-object v6, p0, Lcom/sec/android/app/ve/export/Export$ThreadHandler;->this$0:Lcom/sec/android/app/ve/export/Export;

    # getter for: Lcom/sec/android/app/ve/export/Export;->mFileName:Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/app/ve/export/Export;->access$5(Lcom/sec/android/app/ve/export/Export;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/ve/export/Export$ThreadHandler;->this$0:Lcom/sec/android/app/ve/export/Export;

    # getter for: Lcom/sec/android/app/ve/export/Export;->mTrans:Lcom/samsung/app/video/editor/external/TranscodeElement;
    invoke-static {v7}, Lcom/sec/android/app/ve/export/Export;->access$7(Lcom/sec/android/app/ve/export/Export;)Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getResolutionEnumValue()I

    move-result v7

    invoke-virtual {v4, v5, v6, v7}, Lcom/sec/android/app/ve/export/Export;->saveexportedtranscode(Lcom/samsung/app/video/editor/external/TranscodeElement;Ljava/lang/String;I)V

    .line 288
    iget-object v4, p0, Lcom/sec/android/app/ve/export/Export$ThreadHandler;->this$0:Lcom/sec/android/app/ve/export/Export;

    # getter for: Lcom/sec/android/app/ve/export/Export;->mTrans:Lcom/samsung/app/video/editor/external/TranscodeElement;
    invoke-static {v4}, Lcom/sec/android/app/ve/export/Export;->access$7(Lcom/sec/android/app/ve/export/Export;)Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/sec/android/app/ve/export/Export$ThreadHandler;->this$0:Lcom/sec/android/app/ve/export/Export;

    # getter for: Lcom/sec/android/app/ve/export/Export;->mTrans:Lcom/samsung/app/video/editor/external/TranscodeElement;
    invoke-static {v4}, Lcom/sec/android/app/ve/export/Export;->access$7(Lcom/sec/android/app/ve/export/Export;)Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getProjectFileName()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-static {}, Lcom/sec/android/app/ve/pm/ProjectManager;->getInstance()Lcom/sec/android/app/ve/pm/ProjectManager;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/ve/export/Export$ThreadHandler;->this$0:Lcom/sec/android/app/ve/export/Export;

    # getter for: Lcom/sec/android/app/ve/export/Export;->mTrans:Lcom/samsung/app/video/editor/external/TranscodeElement;
    invoke-static {v5}, Lcom/sec/android/app/ve/export/Export;->access$7(Lcom/sec/android/app/ve/export/Export;)Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getProjectFileName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/ve/pm/ProjectManager;->getTranscodeElement(Ljava/lang/String;)Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 289
    iget-object v4, p0, Lcom/sec/android/app/ve/export/Export$ThreadHandler;->this$0:Lcom/sec/android/app/ve/export/Export;

    # getter for: Lcom/sec/android/app/ve/export/Export;->mTrans:Lcom/samsung/app/video/editor/external/TranscodeElement;
    invoke-static {v4}, Lcom/sec/android/app/ve/export/Export;->access$7(Lcom/sec/android/app/ve/export/Export;)Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v4

    invoke-static {}, Lcom/sec/android/app/ve/pm/ProjectManager;->getInstance()Lcom/sec/android/app/ve/pm/ProjectManager;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/ve/export/Export$ThreadHandler;->this$0:Lcom/sec/android/app/ve/export/Export;

    # getter for: Lcom/sec/android/app/ve/export/Export;->mTrans:Lcom/samsung/app/video/editor/external/TranscodeElement;
    invoke-static {v6}, Lcom/sec/android/app/ve/export/Export;->access$7(Lcom/sec/android/app/ve/export/Export;)Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getProjectFileName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/ve/pm/ProjectManager;->getTranscodeElement(Ljava/lang/String;)Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/app/video/editor/external/TranscodeElement;->isTranscodeContentSame(Lcom/samsung/app/video/editor/external/TranscodeElement;Lcom/samsung/app/video/editor/external/TranscodeElement;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 291
    :try_start_2
    invoke-static {}, Lcom/sec/android/app/ve/pm/ProjectManager;->getInstance()Lcom/sec/android/app/ve/pm/ProjectManager;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/ve/export/Export$ThreadHandler;->this$0:Lcom/sec/android/app/ve/export/Export;

    # getter for: Lcom/sec/android/app/ve/export/Export;->mTrans:Lcom/samsung/app/video/editor/external/TranscodeElement;
    invoke-static {v5}, Lcom/sec/android/app/ve/export/Export;->access$7(Lcom/sec/android/app/ve/export/Export;)Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/ve/pm/ProjectManager;->saveProject(Lcom/samsung/app/video/editor/external/TranscodeElement;)Z
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    .line 301
    :cond_1
    :goto_2
    invoke-virtual {p0, v9}, Lcom/sec/android/app/ve/export/Export$ThreadHandler;->removeMessages(I)V

    .line 302
    iget-object v4, p0, Lcom/sec/android/app/ve/export/Export$ThreadHandler;->this$0:Lcom/sec/android/app/ve/export/Export;

    # invokes: Lcom/sec/android/app/ve/export/Export;->setExportState(I)V
    invoke-static {v4, v8}, Lcom/sec/android/app/ve/export/Export;->access$2(Lcom/sec/android/app/ve/export/Export;I)V

    .line 303
    iput v8, p0, Lcom/sec/android/app/ve/export/Export$ThreadHandler;->mProgress:I

    goto/16 :goto_0

    .line 275
    :catch_1
    move-exception v0

    .line 277
    .restart local v0    # "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_1

    .line 292
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_2
    move-exception v0

    .line 293
    .local v0, "e":Ljava/io/FileNotFoundException;
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_2

    .line 294
    .end local v0    # "e":Ljava/io/FileNotFoundException;
    :catch_3
    move-exception v0

    .line 295
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 308
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "from":Ljava/io/File;
    .end local v2    # "path":Ljava/lang/String;
    .end local v3    # "to":Ljava/io/File;
    :pswitch_2
    iget-object v4, p0, Lcom/sec/android/app/ve/export/Export$ThreadHandler;->this$0:Lcom/sec/android/app/ve/export/Export;

    # invokes: Lcom/sec/android/app/ve/export/Export;->releaseService()V
    invoke-static {v4}, Lcom/sec/android/app/ve/export/Export;->access$4(Lcom/sec/android/app/ve/export/Export;)V

    .line 309
    invoke-virtual {p0, v9}, Lcom/sec/android/app/ve/export/Export$ThreadHandler;->removeMessages(I)V

    .line 310
    iget-object v4, p0, Lcom/sec/android/app/ve/export/Export$ThreadHandler;->this$0:Lcom/sec/android/app/ve/export/Export;

    # invokes: Lcom/sec/android/app/ve/export/Export;->setExportState(I)V
    invoke-static {v4, v8}, Lcom/sec/android/app/ve/export/Export;->access$2(Lcom/sec/android/app/ve/export/Export;I)V

    .line 311
    iput v8, p0, Lcom/sec/android/app/ve/export/Export$ThreadHandler;->mProgress:I

    .line 312
    iget-object v4, p0, Lcom/sec/android/app/ve/export/Export$ThreadHandler;->this$0:Lcom/sec/android/app/ve/export/Export;

    iget-object v4, v4, Lcom/sec/android/app/ve/export/Export;->mAdapter:Lcom/sec/android/app/ve/export/Export$Adapter;

    invoke-virtual {v4}, Lcom/sec/android/app/ve/export/Export$Adapter;->onExportFailed()V

    .line 315
    :try_start_3
    iget-object v4, p0, Lcom/sec/android/app/ve/export/Export$ThreadHandler;->this$0:Lcom/sec/android/app/ve/export/Export;

    # getter for: Lcom/sec/android/app/ve/export/Export;->mService:Lcom/sec/android/app/ve/export/IExportService;
    invoke-static {v4}, Lcom/sec/android/app/ve/export/Export;->access$1(Lcom/sec/android/app/ve/export/Export;)Lcom/sec/android/app/ve/export/IExportService;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 316
    iget-object v4, p0, Lcom/sec/android/app/ve/export/Export$ThreadHandler;->this$0:Lcom/sec/android/app/ve/export/Export;

    # getter for: Lcom/sec/android/app/ve/export/Export;->mService:Lcom/sec/android/app/ve/export/IExportService;
    invoke-static {v4}, Lcom/sec/android/app/ve/export/Export;->access$1(Lcom/sec/android/app/ve/export/Export;)Lcom/sec/android/app/ve/export/IExportService;

    move-result-object v4

    invoke-interface {v4}, Lcom/sec/android/app/ve/export/IExportService;->cancelNotification()V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_4

    goto/16 :goto_0

    .line 317
    :catch_4
    move-exception v0

    .line 319
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_0

    .line 324
    .end local v0    # "e":Landroid/os/RemoteException;
    :pswitch_3
    iget-object v4, p0, Lcom/sec/android/app/ve/export/Export$ThreadHandler;->this$0:Lcom/sec/android/app/ve/export/Export;

    iget-object v5, p0, Lcom/sec/android/app/ve/export/Export$ThreadHandler;->this$0:Lcom/sec/android/app/ve/export/Export;

    # getter for: Lcom/sec/android/app/ve/export/Export;->mFileName:Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/app/ve/export/Export;->access$5(Lcom/sec/android/app/ve/export/Export;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/ve/export/Export;->initExportProgress(Ljava/lang/String;)V

    .line 326
    iput v8, p0, Lcom/sec/android/app/ve/export/Export$ThreadHandler;->mProgress:I

    .line 327
    iget v4, p0, Lcom/sec/android/app/ve/export/Export$ThreadHandler;->mDelay:I

    int-to-long v4, v4

    invoke-virtual {p0, v9, v4, v5}, Lcom/sec/android/app/ve/export/Export$ThreadHandler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    .line 331
    :pswitch_4
    invoke-virtual {p0, v9}, Lcom/sec/android/app/ve/export/Export$ThreadHandler;->removeMessages(I)V

    .line 333
    iget-object v4, p0, Lcom/sec/android/app/ve/export/Export$ThreadHandler;->this$0:Lcom/sec/android/app/ve/export/Export;

    invoke-virtual {v4}, Lcom/sec/android/app/ve/export/Export;->getExportProgress()I

    move-result v4

    iput v4, p0, Lcom/sec/android/app/ve/export/Export$ThreadHandler;->mProgress:I

    .line 334
    iget-object v4, p0, Lcom/sec/android/app/ve/export/Export$ThreadHandler;->this$0:Lcom/sec/android/app/ve/export/Export;

    # invokes: Lcom/sec/android/app/ve/export/Export;->getExportState()I
    invoke-static {v4}, Lcom/sec/android/app/ve/export/Export;->access$8(Lcom/sec/android/app/ve/export/Export;)I

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/sec/android/app/ve/export/Export$ThreadHandler;->this$0:Lcom/sec/android/app/ve/export/Export;

    # invokes: Lcom/sec/android/app/ve/export/Export;->getExportState()I
    invoke-static {v4}, Lcom/sec/android/app/ve/export/Export;->access$8(Lcom/sec/android/app/ve/export/Export;)I

    move-result v4

    if-eq v4, v5, :cond_2

    .line 336
    iget v4, p0, Lcom/sec/android/app/ve/export/Export$ThreadHandler;->mDelay:I

    int-to-long v4, v4

    invoke-virtual {p0, v9, v4, v5}, Lcom/sec/android/app/ve/export/Export$ThreadHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 337
    iget-object v4, p0, Lcom/sec/android/app/ve/export/Export$ThreadHandler;->this$0:Lcom/sec/android/app/ve/export/Export;

    iget-object v4, v4, Lcom/sec/android/app/ve/export/Export;->mAdapter:Lcom/sec/android/app/ve/export/Export$Adapter;

    iget v5, p0, Lcom/sec/android/app/ve/export/Export$ThreadHandler;->mProgress:I

    invoke-virtual {v4, v5}, Lcom/sec/android/app/ve/export/Export$Adapter;->onExportProgressUpdate(I)V

    .line 340
    :try_start_4
    iget-object v4, p0, Lcom/sec/android/app/ve/export/Export$ThreadHandler;->this$0:Lcom/sec/android/app/ve/export/Export;

    # getter for: Lcom/sec/android/app/ve/export/Export;->mService:Lcom/sec/android/app/ve/export/IExportService;
    invoke-static {v4}, Lcom/sec/android/app/ve/export/Export;->access$1(Lcom/sec/android/app/ve/export/Export;)Lcom/sec/android/app/ve/export/IExportService;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/app/ve/export/Export$ThreadHandler;->mProgress:I

    iget-object v6, p0, Lcom/sec/android/app/ve/export/Export$ThreadHandler;->this$0:Lcom/sec/android/app/ve/export/Export;

    # invokes: Lcom/sec/android/app/ve/export/Export;->getExportState()I
    invoke-static {v6}, Lcom/sec/android/app/ve/export/Export;->access$8(Lcom/sec/android/app/ve/export/Export;)I

    move-result v6

    invoke-interface {v4, v5, v6}, Lcom/sec/android/app/ve/export/IExportService;->updateNotification(II)V
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_5

    goto/16 :goto_0

    .line 341
    :catch_5
    move-exception v0

    .line 343
    .restart local v0    # "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_0

    .line 345
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/ve/export/Export$ThreadHandler;->this$0:Lcom/sec/android/app/ve/export/Export;

    # invokes: Lcom/sec/android/app/ve/export/Export;->getExportState()I
    invoke-static {v4}, Lcom/sec/android/app/ve/export/Export;->access$8(Lcom/sec/android/app/ve/export/Export;)I

    move-result v4

    if-ne v4, v5, :cond_0

    .line 346
    iget-object v4, p0, Lcom/sec/android/app/ve/export/Export$ThreadHandler;->this$0:Lcom/sec/android/app/ve/export/Export;

    iget-object v4, v4, Lcom/sec/android/app/ve/export/Export;->mAdapter:Lcom/sec/android/app/ve/export/Export$Adapter;

    iget v5, p0, Lcom/sec/android/app/ve/export/Export$ThreadHandler;->mProgress:I

    invoke-virtual {v4, v5}, Lcom/sec/android/app/ve/export/Export$Adapter;->onExportProgressUpdate(I)V

    .line 347
    iget-object v4, p0, Lcom/sec/android/app/ve/export/Export$ThreadHandler;->this$0:Lcom/sec/android/app/ve/export/Export;

    iget-object v4, v4, Lcom/sec/android/app/ve/export/Export;->mAdapter:Lcom/sec/android/app/ve/export/Export$Adapter;

    invoke-virtual {v4}, Lcom/sec/android/app/ve/export/Export$Adapter;->onExportPaused()V

    .line 350
    :try_start_5
    iget-object v4, p0, Lcom/sec/android/app/ve/export/Export$ThreadHandler;->this$0:Lcom/sec/android/app/ve/export/Export;

    # getter for: Lcom/sec/android/app/ve/export/Export;->mService:Lcom/sec/android/app/ve/export/IExportService;
    invoke-static {v4}, Lcom/sec/android/app/ve/export/Export;->access$1(Lcom/sec/android/app/ve/export/Export;)Lcom/sec/android/app/ve/export/IExportService;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/app/ve/export/Export$ThreadHandler;->mProgress:I

    iget-object v6, p0, Lcom/sec/android/app/ve/export/Export$ThreadHandler;->this$0:Lcom/sec/android/app/ve/export/Export;

    # invokes: Lcom/sec/android/app/ve/export/Export;->getExportState()I
    invoke-static {v6}, Lcom/sec/android/app/ve/export/Export;->access$8(Lcom/sec/android/app/ve/export/Export;)I

    move-result v6

    invoke-interface {v4, v5, v6}, Lcom/sec/android/app/ve/export/IExportService;->updateNotification(II)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_5} :catch_6

    goto/16 :goto_0

    .line 351
    :catch_6
    move-exception v0

    .line 353
    .restart local v0    # "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_0

    .line 250
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
