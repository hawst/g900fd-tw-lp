.class Lcom/sec/android/app/ve/common/KenburnsDefault$ApplyDefaultKenburnsTask;
.super Lcom/sec/android/app/ve/thread/SimpleTask;
.source "KenburnsDefault.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/ve/common/KenburnsDefault;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ApplyDefaultKenburnsTask"
.end annotation


# instance fields
.field private mElement:Lcom/samsung/app/video/editor/external/Element;

.field final synthetic this$0:Lcom/sec/android/app/ve/common/KenburnsDefault;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/ve/common/KenburnsDefault;Lcom/samsung/app/video/editor/external/Element;)V
    .locals 0
    .param p2, "element"    # Lcom/samsung/app/video/editor/external/Element;

    .prologue
    .line 219
    iput-object p1, p0, Lcom/sec/android/app/ve/common/KenburnsDefault$ApplyDefaultKenburnsTask;->this$0:Lcom/sec/android/app/ve/common/KenburnsDefault;

    invoke-direct {p0}, Lcom/sec/android/app/ve/thread/SimpleTask;-><init>()V

    .line 220
    iput-object p2, p0, Lcom/sec/android/app/ve/common/KenburnsDefault$ApplyDefaultKenburnsTask;->mElement:Lcom/samsung/app/video/editor/external/Element;

    .line 221
    return-void
.end method


# virtual methods
.method protected getParam()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 226
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getPool()Lcom/sec/android/app/ve/thread/AsyncTaskPool;
    .locals 1

    .prologue
    .line 505
    iget-object v0, p0, Lcom/sec/android/app/ve/common/KenburnsDefault$ApplyDefaultKenburnsTask;->this$0:Lcom/sec/android/app/ve/common/KenburnsDefault;

    # getter for: Lcom/sec/android/app/ve/common/KenburnsDefault;->mTaskPool:Lcom/sec/android/app/ve/thread/AsyncTaskPool;
    invoke-static {v0}, Lcom/sec/android/app/ve/common/KenburnsDefault;->access$4(Lcom/sec/android/app/ve/common/KenburnsDefault;)Lcom/sec/android/app/ve/thread/AsyncTaskPool;

    move-result-object v0

    return-object v0
.end method

.method protected onBackgroundTask(Ljava/lang/Object;)Lcom/samsung/app/video/editor/external/Element;
    .locals 54
    .param p1, "param"    # Ljava/lang/Object;

    .prologue
    .line 234
    const/16 v23, 0x0

    .line 237
    .local v23, "imageOrientation":I
    const/16 v26, 0x0

    .line 238
    .local v26, "imageWidth":I
    const/16 v22, 0x0

    .line 241
    .local v22, "imageHeight":I
    const/16 v30, 0x0

    .line 243
    .local v30, "mFaceBitmap":Landroid/graphics/Bitmap;
    const/4 v15, 0x0

    .line 245
    .local v15, "faceCount":I
    sget v34, Lcom/sec/android/app/ve/common/ConfigUtils;->PREVIEW_HEIGHT:I

    .line 246
    .local v34, "previewHeight":I
    sget v35, Lcom/sec/android/app/ve/common/ConfigUtils;->PREVIEW_WIDTH:I

    .line 248
    .local v35, "previewWidth":I
    const/4 v8, 0x0

    .line 249
    .local v8, "bitmapHeight":I
    const/4 v9, 0x0

    .line 252
    .local v9, "bitmapWidth":I
    const/16 v19, 0x0

    .line 253
    .local v19, "fstart":[F
    const/16 v18, 0x0

    .line 255
    .local v18, "fend":[F
    new-instance v31, Landroid/graphics/Matrix;

    invoke-direct/range {v31 .. v31}, Landroid/graphics/Matrix;-><init>()V

    .line 256
    .local v31, "mStart":Landroid/graphics/Matrix;
    new-instance v29, Landroid/graphics/Matrix;

    invoke-direct/range {v29 .. v29}, Landroid/graphics/Matrix;-><init>()V

    .line 258
    .local v29, "mEnd":Landroid/graphics/Matrix;
    new-instance v37, Landroid/graphics/RectF;

    const/16 v47, 0x0

    const/16 v48, 0x0

    move/from16 v0, v35

    int-to-float v0, v0

    move/from16 v49, v0

    move/from16 v0, v34

    int-to-float v0, v0

    move/from16 v50, v0

    move-object/from16 v0, v37

    move/from16 v1, v47

    move/from16 v2, v48

    move/from16 v3, v49

    move/from16 v4, v50

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 260
    .local v37, "rRect":Landroid/graphics/RectF;
    new-instance v43, Landroid/graphics/RectF;

    const/16 v47, 0x0

    const/16 v48, 0x0

    move/from16 v0, v35

    int-to-float v0, v0

    move/from16 v49, v0

    move/from16 v0, v34

    int-to-float v0, v0

    move/from16 v50, v0

    move-object/from16 v0, v43

    move/from16 v1, v47

    move/from16 v2, v48

    move/from16 v3, v49

    move/from16 v4, v50

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 261
    .local v43, "sRect":Landroid/graphics/RectF;
    new-instance v13, Landroid/graphics/RectF;

    const/16 v47, 0x0

    const/16 v48, 0x0

    move/from16 v0, v35

    int-to-float v0, v0

    move/from16 v49, v0

    move/from16 v0, v34

    int-to-float v0, v0

    move/from16 v50, v0

    move/from16 v0, v47

    move/from16 v1, v48

    move/from16 v2, v49

    move/from16 v3, v50

    invoke-direct {v13, v0, v1, v2, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 264
    .local v13, "eRect":Landroid/graphics/RectF;
    new-instance v33, Landroid/graphics/PointF;

    invoke-direct/range {v33 .. v33}, Landroid/graphics/PointF;-><init>()V

    .line 266
    .local v33, "midPoint":Landroid/graphics/PointF;
    const/4 v10, 0x0

    .line 268
    .local v10, "bottom":I
    const/16 v32, 0x0

    .line 273
    .local v32, "maxEyeDistance":F
    :try_start_0
    new-instance v27, Landroid/media/ExifInterface;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/ve/common/KenburnsDefault$ApplyDefaultKenburnsTask;->mElement:Lcom/samsung/app/video/editor/external/Element;

    move-object/from16 v47, v0

    move-object/from16 v0, v47

    iget-object v0, v0, Lcom/samsung/app/video/editor/external/Element;->filePath:Ljava/lang/String;

    move-object/from16 v47, v0

    move-object/from16 v0, v27

    move-object/from16 v1, v47

    invoke-direct {v0, v1}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V

    .line 274
    .local v27, "lExif":Landroid/media/ExifInterface;
    const-string v47, "ImageWidth"

    const/16 v48, 0x64

    move-object/from16 v0, v27

    move-object/from16 v1, v47

    move/from16 v2, v48

    invoke-virtual {v0, v1, v2}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    move-result v26

    .line 275
    const-string v47, "ImageLength"

    const/16 v48, 0x64

    move-object/from16 v0, v27

    move-object/from16 v1, v47

    move/from16 v2, v48

    invoke-virtual {v0, v1, v2}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    move-result v22

    .line 276
    const/16 v23, 0x1

    .line 278
    const-string v47, "Orientation"

    const/16 v48, 0x64

    move-object/from16 v0, v27

    move-object/from16 v1, v47

    move/from16 v2, v48

    invoke-virtual {v0, v1, v2}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    move-result v47

    const/16 v48, 0x6

    move/from16 v0, v47

    move/from16 v1, v48

    if-eq v0, v1, :cond_0

    .line 279
    const-string v47, "Orientation"

    const/16 v48, 0x64

    move-object/from16 v0, v27

    move-object/from16 v1, v47

    move/from16 v2, v48

    invoke-virtual {v0, v1, v2}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    move-result v47

    const/16 v48, 0x8

    move/from16 v0, v47

    move/from16 v1, v48

    if-ne v0, v1, :cond_a

    .line 280
    :cond_0
    const-string v47, "ImageWidth"

    const/16 v48, 0x64

    move-object/from16 v0, v27

    move-object/from16 v1, v47

    move/from16 v2, v48

    invoke-virtual {v0, v1, v2}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    move-result v22

    .line 281
    const-string v47, "ImageLength"

    const/16 v48, 0x64

    move-object/from16 v0, v27

    move-object/from16 v1, v47

    move/from16 v2, v48

    invoke-virtual {v0, v1, v2}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v26

    .line 282
    const/16 v23, 0x6

    .line 301
    .end local v27    # "lExif":Landroid/media/ExifInterface;
    :cond_1
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/ve/common/KenburnsDefault$ApplyDefaultKenburnsTask;->mElement:Lcom/samsung/app/video/editor/external/Element;

    move-object/from16 v47, v0

    move-object/from16 v0, v47

    iget-object v0, v0, Lcom/samsung/app/video/editor/external/Element;->filePath:Ljava/lang/String;

    move-object/from16 v48, v0

    const/16 v49, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/ve/common/KenburnsDefault$ApplyDefaultKenburnsTask;->mElement:Lcom/samsung/app/video/editor/external/Element;

    move-object/from16 v47, v0

    invoke-virtual/range {v47 .. v47}, Lcom/samsung/app/video/editor/external/Element;->getID()I

    move-result v47

    const/16 v50, -0x1

    move/from16 v0, v47

    move/from16 v1, v50

    if-eq v0, v1, :cond_c

    const/16 v47, 0x1

    :goto_1
    move-object/from16 v0, v48

    move/from16 v1, v49

    move/from16 v2, v47

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/ve/common/MediaUtils;->getImageBitmap(Ljava/lang/String;IZ)Landroid/graphics/Bitmap;

    move-result-object v44

    .line 303
    .local v44, "thumbBitmap":Landroid/graphics/Bitmap;
    if-eqz v44, :cond_3

    .line 306
    const/4 v14, 0x0

    .line 309
    .local v14, "eyeDistance":F
    sget-object v47, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    const/16 v48, 0x1

    move-object/from16 v0, v44

    move-object/from16 v1, v47

    move/from16 v2, v48

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v30

    .line 310
    if-eqz v30, :cond_2

    .line 311
    new-instance v16, Landroid/media/FaceDetector;

    invoke-virtual/range {v30 .. v30}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v47

    invoke-virtual/range {v30 .. v30}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v48

    const/16 v49, 0xa

    move-object/from16 v0, v16

    move/from16 v1, v47

    move/from16 v2, v48

    move/from16 v3, v49

    invoke-direct {v0, v1, v2, v3}, Landroid/media/FaceDetector;-><init>(III)V

    .line 312
    .local v16, "faceDetector":Landroid/media/FaceDetector;
    const/16 v47, 0xa

    move/from16 v0, v47

    new-array v0, v0, [Landroid/media/FaceDetector$Face;

    move-object/from16 v17, v0

    .line 313
    .local v17, "faces":[Landroid/media/FaceDetector$Face;
    move-object/from16 v0, v16

    move-object/from16 v1, v30

    move-object/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Landroid/media/FaceDetector;->findFaces(Landroid/graphics/Bitmap;[Landroid/media/FaceDetector$Face;)I

    move-result v15

    .line 315
    invoke-virtual/range {v30 .. v30}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v9

    .line 316
    invoke-virtual/range {v30 .. v30}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    .line 317
    invoke-virtual/range {v30 .. v30}, Landroid/graphics/Bitmap;->recycle()V

    .line 320
    const/16 v21, 0x0

    .local v21, "i":I
    :goto_2
    move/from16 v0, v21

    if-lt v0, v15, :cond_d

    .line 332
    .end local v16    # "faceDetector":Landroid/media/FaceDetector;
    .end local v17    # "faces":[Landroid/media/FaceDetector$Face;
    .end local v21    # "i":I
    :cond_2
    invoke-virtual/range {v44 .. v44}, Landroid/graphics/Bitmap;->recycle()V

    .line 339
    .end local v14    # "eyeDistance":F
    :cond_3
    const/16 v47, 0x6

    move/from16 v0, v23

    move/from16 v1, v47

    if-ne v0, v1, :cond_f

    .line 342
    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v47, v0

    move/from16 v0, v34

    int-to-float v0, v0

    move/from16 v48, v0

    div-float v39, v47, v48

    .line 344
    .local v39, "ratio":F
    move/from16 v0, v35

    int-to-float v0, v0

    move/from16 v47, v0

    mul-float v47, v47, v39

    move/from16 v0, v47

    float-to-int v7, v0

    .line 345
    .local v7, "actWidth":I
    move/from16 v6, v22

    .line 347
    .local v6, "actHeight":I
    const/16 v45, 0x0

    .line 348
    .local v45, "top":I
    const/4 v10, 0x0

    .line 349
    sub-int v47, v7, v26

    div-int/lit8 v28, v47, 0x2

    .line 351
    .local v28, "left":I
    move-object/from16 v0, v33

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v47, v0

    const/16 v48, 0x0

    cmpl-float v47, v47, v48

    if-eqz v47, :cond_4

    .line 352
    int-to-float v0, v6

    move/from16 v47, v0

    int-to-float v0, v8

    move/from16 v48, v0

    div-float v46, v47, v48

    .line 353
    .local v46, "yRatio":F
    move-object/from16 v0, v33

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v47, v0

    mul-float v47, v47, v46

    move/from16 v0, v47

    move-object/from16 v1, v33

    iput v0, v1, Landroid/graphics/PointF;->y:F

    .line 354
    move-object/from16 v0, v33

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v47, v0

    mul-float v48, v32, v46

    const/high16 v49, 0x40000000    # 2.0f

    div-float v48, v48, v49

    sub-float v47, v47, v48

    move/from16 v0, v47

    float-to-int v0, v0

    move/from16 v45, v0

    .line 359
    .end local v46    # "yRatio":F
    :cond_4
    if-nez v45, :cond_5

    if-nez v15, :cond_5

    .line 360
    move/from16 v0, v22

    int-to-double v0, v0

    move-wide/from16 v48, v0

    const-wide v50, 0x3fd3333333333333L    # 0.3

    mul-double v48, v48, v50

    move-wide/from16 v0, v48

    double-to-int v0, v0

    move/from16 v45, v0

    .line 367
    :cond_5
    div-int/lit8 v47, v34, 0x2

    move/from16 v0, v45

    move/from16 v1, v47

    if-le v0, v1, :cond_6

    move/from16 v0, v34

    move/from16 v1, v22

    if-lt v0, v1, :cond_7

    .line 368
    :cond_6
    const/16 v45, 0x0

    .line 371
    :cond_7
    move/from16 v0, v26

    int-to-float v0, v0

    move/from16 v47, v0

    move/from16 v0, v35

    int-to-float v0, v0

    move/from16 v48, v0

    div-float v25, v47, v48

    .line 372
    .local v25, "imageToPrevRatio":F
    move/from16 v0, v28

    int-to-float v0, v0

    move/from16 v47, v0

    div-float v47, v47, v25

    move/from16 v0, v47

    float-to-int v0, v0

    move/from16 v28, v0

    .line 373
    move/from16 v26, v35

    .line 374
    int-to-float v0, v7

    move/from16 v47, v0

    div-float v47, v47, v25

    move/from16 v0, v47

    float-to-int v7, v0

    .line 375
    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v47, v0

    div-float v47, v47, v25

    move/from16 v0, v47

    float-to-int v0, v0

    move/from16 v22, v0

    .line 376
    move/from16 v0, v45

    int-to-float v0, v0

    move/from16 v47, v0

    div-float v47, v47, v25

    move/from16 v0, v47

    float-to-int v0, v0

    move/from16 v45, v0

    .line 379
    move/from16 v0, v45

    int-to-double v0, v0

    move-wide/from16 v48, v0

    move/from16 v0, v22

    int-to-double v0, v0

    move-wide/from16 v50, v0

    const-wide v52, 0x3fc999999999999aL    # 0.2

    mul-double v50, v50, v52

    add-double v48, v48, v50

    move-wide/from16 v0, v48

    double-to-int v10, v0

    .line 383
    sub-int v47, v22, v45

    move/from16 v0, v47

    move/from16 v1, v34

    if-ge v0, v1, :cond_8

    .line 384
    const/16 v45, 0x0

    .line 387
    :cond_8
    sub-int v47, v22, v10

    move/from16 v0, v47

    move/from16 v1, v34

    if-ge v0, v1, :cond_9

    .line 388
    sub-int v10, v22, v34

    .line 391
    :cond_9
    int-to-float v0, v7

    move/from16 v47, v0

    move/from16 v0, v35

    int-to-float v0, v0

    move/from16 v48, v0

    div-float v24, v47, v48

    .line 393
    .local v24, "imageScaleRatio":F
    const/16 v47, 0x9

    move/from16 v0, v47

    new-array v0, v0, [F

    move-object/from16 v19, v0

    .end local v19    # "fstart":[F
    const/16 v47, 0x0

    aput v24, v19, v47

    const/16 v47, 0x1

    const/16 v48, 0x0

    aput v48, v19, v47

    const/16 v47, 0x2

    move/from16 v0, v28

    neg-int v0, v0

    move/from16 v48, v0

    move/from16 v0, v48

    int-to-float v0, v0

    move/from16 v48, v0

    aput v48, v19, v47

    const/16 v47, 0x3

    const/16 v48, 0x0

    aput v48, v19, v47

    const/16 v47, 0x4

    aput v24, v19, v47

    const/16 v47, 0x5

    move/from16 v0, v45

    neg-int v0, v0

    move/from16 v48, v0

    move/from16 v0, v48

    int-to-float v0, v0

    move/from16 v48, v0

    aput v48, v19, v47

    const/16 v47, 0x6

    const/16 v48, 0x0

    aput v48, v19, v47

    const/16 v47, 0x7

    const/16 v48, 0x0

    aput v48, v19, v47

    const/16 v47, 0x8

    const/high16 v48, 0x3f800000    # 1.0f

    aput v48, v19, v47

    .line 394
    .restart local v19    # "fstart":[F
    const/16 v47, 0x9

    move/from16 v0, v47

    new-array v0, v0, [F

    move-object/from16 v18, v0

    .end local v18    # "fend":[F
    const/16 v47, 0x0

    aput v24, v18, v47

    const/16 v47, 0x1

    const/16 v48, 0x0

    aput v48, v18, v47

    const/16 v47, 0x2

    move/from16 v0, v28

    neg-int v0, v0

    move/from16 v48, v0

    move/from16 v0, v48

    int-to-float v0, v0

    move/from16 v48, v0

    aput v48, v18, v47

    const/16 v47, 0x3

    const/16 v48, 0x0

    aput v48, v18, v47

    const/16 v47, 0x4

    aput v24, v18, v47

    const/16 v47, 0x5

    move/from16 v0, v22

    neg-int v0, v0

    move/from16 v48, v0

    add-int v48, v48, v34

    move/from16 v0, v48

    int-to-float v0, v0

    move/from16 v48, v0

    aput v48, v18, v47

    const/16 v47, 0x6

    const/16 v48, 0x0

    aput v48, v18, v47

    const/16 v47, 0x7

    const/16 v48, 0x0

    aput v48, v18, v47

    const/16 v47, 0x8

    const/high16 v48, 0x3f800000    # 1.0f

    aput v48, v18, v47

    .line 397
    .restart local v18    # "fend":[F
    move-object/from16 v0, v31

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->setValues([F)V

    .line 398
    move-object/from16 v0, v29

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->setValues([F)V

    .line 400
    move/from16 v0, v28

    neg-int v0, v0

    move/from16 v47, v0

    move/from16 v0, v47

    int-to-float v0, v0

    move/from16 v47, v0

    move/from16 v0, v45

    neg-int v0, v0

    move/from16 v48, v0

    move/from16 v0, v48

    int-to-float v0, v0

    move/from16 v48, v0

    add-int v49, v26, v28

    move/from16 v0, v49

    int-to-float v0, v0

    move/from16 v49, v0

    sub-int v50, v22, v45

    move/from16 v0, v50

    int-to-float v0, v0

    move/from16 v50, v0

    move-object/from16 v0, v43

    move/from16 v1, v47

    move/from16 v2, v48

    move/from16 v3, v49

    move/from16 v4, v50

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 401
    move/from16 v0, v28

    neg-int v0, v0

    move/from16 v47, v0

    move/from16 v0, v47

    int-to-float v0, v0

    move/from16 v47, v0

    neg-int v0, v10

    move/from16 v48, v0

    move/from16 v0, v48

    int-to-float v0, v0

    move/from16 v48, v0

    add-int v49, v26, v28

    move/from16 v0, v49

    int-to-float v0, v0

    move/from16 v49, v0

    sub-int v50, v22, v10

    move/from16 v0, v50

    int-to-float v0, v0

    move/from16 v50, v0

    move/from16 v0, v47

    move/from16 v1, v48

    move/from16 v2, v49

    move/from16 v3, v50

    invoke-virtual {v13, v0, v1, v2, v3}, Landroid/graphics/RectF;->set(FFFF)V

    .line 474
    .end local v6    # "actHeight":I
    .end local v7    # "actWidth":I
    .end local v24    # "imageScaleRatio":F
    .end local v25    # "imageToPrevRatio":F
    .end local v28    # "left":I
    .end local v39    # "ratio":F
    .end local v45    # "top":I
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/ve/common/KenburnsDefault$ApplyDefaultKenburnsTask;->mElement:Lcom/samsung/app/video/editor/external/Element;

    move-object/from16 v47, v0

    invoke-virtual/range {v47 .. v47}, Lcom/samsung/app/video/editor/external/Element;->getGroupID()I

    move-result v20

    .line 476
    .local v20, "groupID":I
    rem-int/lit8 v47, v20, 0x2

    if-nez v47, :cond_16

    .line 477
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/ve/common/KenburnsDefault$ApplyDefaultKenburnsTask;->mElement:Lcom/samsung/app/video/editor/external/Element;

    move-object/from16 v47, v0

    move-object/from16 v0, v47

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, Lcom/samsung/app/video/editor/external/Element;->setRefRect(Landroid/graphics/RectF;)V

    .line 478
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/ve/common/KenburnsDefault$ApplyDefaultKenburnsTask;->mElement:Lcom/samsung/app/video/editor/external/Element;

    move-object/from16 v47, v0

    move-object/from16 v0, v47

    move-object/from16 v1, v43

    invoke-virtual {v0, v1}, Lcom/samsung/app/video/editor/external/Element;->setStartRect(Landroid/graphics/RectF;)V

    .line 479
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/ve/common/KenburnsDefault$ApplyDefaultKenburnsTask;->mElement:Lcom/samsung/app/video/editor/external/Element;

    move-object/from16 v47, v0

    move-object/from16 v0, v47

    invoke-virtual {v0, v13}, Lcom/samsung/app/video/editor/external/Element;->setEndRect(Landroid/graphics/RectF;)V

    .line 480
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/ve/common/KenburnsDefault$ApplyDefaultKenburnsTask;->mElement:Lcom/samsung/app/video/editor/external/Element;

    move-object/from16 v47, v0

    const/16 v48, 0x1

    move-object/from16 v0, v47

    move-object/from16 v1, v31

    move/from16 v2, v48

    invoke-virtual {v0, v1, v2}, Lcom/samsung/app/video/editor/external/Element;->setMatrix(Landroid/graphics/Matrix;Z)V

    .line 481
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/ve/common/KenburnsDefault$ApplyDefaultKenburnsTask;->mElement:Lcom/samsung/app/video/editor/external/Element;

    move-object/from16 v47, v0

    const/16 v48, 0x0

    move-object/from16 v0, v47

    move-object/from16 v1, v29

    move/from16 v2, v48

    invoke-virtual {v0, v1, v2}, Lcom/samsung/app/video/editor/external/Element;->setMatrix(Landroid/graphics/Matrix;Z)V

    .line 491
    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/ve/common/KenburnsDefault$ApplyDefaultKenburnsTask;->mElement:Lcom/samsung/app/video/editor/external/Element;

    move-object/from16 v47, v0

    new-instance v48, Landroid/graphics/RectF;

    const/16 v49, 0x0

    const/16 v50, 0x0

    move/from16 v0, v35

    int-to-float v0, v0

    move/from16 v51, v0

    move/from16 v0, v34

    int-to-float v0, v0

    move/from16 v52, v0

    invoke-direct/range {v48 .. v52}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-virtual/range {v47 .. v48}, Lcom/samsung/app/video/editor/external/Element;->setRefRect(Landroid/graphics/RectF;)V

    .line 493
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/ve/common/KenburnsDefault$ApplyDefaultKenburnsTask;->mElement:Lcom/samsung/app/video/editor/external/Element;

    move-object/from16 v47, v0

    return-object v47

    .line 283
    .end local v20    # "groupID":I
    .end local v44    # "thumbBitmap":Landroid/graphics/Bitmap;
    .restart local v27    # "lExif":Landroid/media/ExifInterface;
    :cond_a
    move/from16 v0, v26

    move/from16 v1, v22

    if-ge v0, v1, :cond_b

    .line 284
    :try_start_1
    const-string v47, "ImageWidth"

    const/16 v48, 0x64

    move-object/from16 v0, v27

    move-object/from16 v1, v47

    move/from16 v2, v48

    invoke-virtual {v0, v1, v2}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    move-result v26

    .line 285
    const-string v47, "ImageLength"

    const/16 v48, 0x64

    move-object/from16 v0, v27

    move-object/from16 v1, v47

    move/from16 v2, v48

    invoke-virtual {v0, v1, v2}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    move-result v22

    .line 286
    const/16 v23, 0x6

    .line 287
    goto/16 :goto_0

    :cond_b
    move/from16 v0, v26

    int-to-float v0, v0

    move/from16 v47, v0

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v48, v0

    div-float v47, v47, v48

    move/from16 v0, v47

    float-to-double v0, v0

    move-wide/from16 v48, v0

    const-wide v50, 0x3ffb333333333333L    # 1.7

    cmpg-double v47, v48, v50

    if-gez v47, :cond_1

    .line 288
    const-string v47, "ImageWidth"

    const/16 v48, 0x64

    move-object/from16 v0, v27

    move-object/from16 v1, v47

    move/from16 v2, v48

    invoke-virtual {v0, v1, v2}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    move-result v26

    .line 289
    const-string v47, "ImageLength"

    const/16 v48, 0x64

    move-object/from16 v0, v27

    move-object/from16 v1, v47

    move/from16 v2, v48

    invoke-virtual {v0, v1, v2}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v22

    .line 290
    const/16 v23, 0x6

    goto/16 :goto_0

    .line 296
    .end local v27    # "lExif":Landroid/media/ExifInterface;
    :catch_0
    move-exception v12

    .line 297
    .local v12, "e":Ljava/io/IOException;
    invoke-virtual {v12}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 301
    .end local v12    # "e":Ljava/io/IOException;
    :cond_c
    const/16 v47, 0x0

    goto/16 :goto_1

    .line 321
    .restart local v14    # "eyeDistance":F
    .restart local v16    # "faceDetector":Landroid/media/FaceDetector;
    .restart local v17    # "faces":[Landroid/media/FaceDetector$Face;
    .restart local v21    # "i":I
    .restart local v44    # "thumbBitmap":Landroid/graphics/Bitmap;
    :cond_d
    aget-object v47, v17, v21

    invoke-virtual/range {v47 .. v47}, Landroid/media/FaceDetector$Face;->eyesDistance()F

    move-result v14

    .line 322
    aget-object v47, v17, v21

    invoke-virtual/range {v47 .. v47}, Landroid/media/FaceDetector$Face;->confidence()F

    move-result v11

    .line 323
    .local v11, "confidence":F
    cmpl-float v47, v14, v32

    if-ltz v47, :cond_e

    .line 326
    aget-object v47, v17, v21

    move-object/from16 v0, v47

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Landroid/media/FaceDetector$Face;->getMidPoint(Landroid/graphics/PointF;)V

    .line 327
    move/from16 v32, v14

    .line 320
    :cond_e
    add-int/lit8 v21, v21, 0x1

    goto/16 :goto_2

    .line 409
    .end local v11    # "confidence":F
    .end local v14    # "eyeDistance":F
    .end local v16    # "faceDetector":Landroid/media/FaceDetector;
    .end local v17    # "faces":[Landroid/media/FaceDetector$Face;
    .end local v21    # "i":I
    :cond_f
    if-eqz v15, :cond_15

    .line 410
    move-object/from16 v0, v33

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v47, v0

    move/from16 v0, v47

    float-to-int v0, v0

    move/from16 v47, v0

    mul-int v47, v47, v35

    div-int v40, v47, v9

    .line 411
    .local v40, "refX":I
    move-object/from16 v0, v33

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v47, v0

    move/from16 v0, v47

    float-to-int v0, v0

    move/from16 v47, v0

    mul-int v47, v47, v34

    div-int v41, v47, v8

    .line 415
    .local v41, "refY":I
    div-int/lit8 v47, v35, 0x2

    move/from16 v0, v35

    int-to-double v0, v0

    move-wide/from16 v48, v0

    const-wide v50, 0x3fa999999999999aL    # 0.05

    mul-double v48, v48, v50

    move-wide/from16 v0, v48

    double-to-int v0, v0

    move/from16 v48, v0

    div-int/lit8 v48, v48, 0x2

    add-int v47, v47, v48

    move/from16 v0, v40

    move/from16 v1, v47

    if-ge v0, v1, :cond_10

    div-int/lit8 v47, v35, 0x2

    move/from16 v0, v35

    int-to-double v0, v0

    move-wide/from16 v48, v0

    const-wide v50, 0x3fa999999999999aL    # 0.05

    mul-double v48, v48, v50

    move-wide/from16 v0, v48

    double-to-int v0, v0

    move/from16 v48, v0

    div-int/lit8 v48, v48, 0x2

    sub-int v47, v47, v48

    move/from16 v0, v40

    move/from16 v1, v47

    if-le v0, v1, :cond_10

    .line 416
    move/from16 v0, v35

    int-to-double v0, v0

    move-wide/from16 v48, v0

    const-wide v50, 0x3fa999999999999aL    # 0.05

    mul-double v48, v48, v50

    move-wide/from16 v0, v48

    double-to-int v0, v0

    move/from16 v47, v0

    move/from16 v0, v47

    neg-int v0, v0

    move/from16 v47, v0

    div-int/lit8 v28, v47, 0x2

    .line 417
    .restart local v28    # "left":I
    move/from16 v0, v35

    int-to-double v0, v0

    move-wide/from16 v48, v0

    const-wide v50, 0x3fa999999999999aL    # 0.05

    mul-double v48, v48, v50

    move-wide/from16 v0, v48

    double-to-int v0, v0

    move/from16 v47, v0

    div-int/lit8 v47, v47, 0x2

    add-int v42, v35, v47

    .line 426
    .local v42, "right":I
    :goto_5
    div-int/lit8 v47, v34, 0x2

    move/from16 v0, v34

    int-to-double v0, v0

    move-wide/from16 v48, v0

    const-wide v50, 0x3fa999999999999aL    # 0.05

    mul-double v48, v48, v50

    move-wide/from16 v0, v48

    double-to-int v0, v0

    move/from16 v48, v0

    div-int/lit8 v48, v48, 0x2

    add-int v47, v47, v48

    move/from16 v0, v41

    move/from16 v1, v47

    if-ge v0, v1, :cond_12

    div-int/lit8 v47, v34, 0x2

    move/from16 v0, v34

    int-to-double v0, v0

    move-wide/from16 v48, v0

    const-wide v50, 0x3fa999999999999aL    # 0.05

    mul-double v48, v48, v50

    move-wide/from16 v0, v48

    double-to-int v0, v0

    move/from16 v48, v0

    div-int/lit8 v48, v48, 0x2

    sub-int v47, v47, v48

    move/from16 v0, v41

    move/from16 v1, v47

    if-le v0, v1, :cond_12

    .line 427
    move/from16 v0, v34

    int-to-double v0, v0

    move-wide/from16 v48, v0

    const-wide v50, 0x3fa999999999999aL    # 0.05

    mul-double v48, v48, v50

    move-wide/from16 v0, v48

    double-to-int v0, v0

    move/from16 v47, v0

    move/from16 v0, v47

    neg-int v0, v0

    move/from16 v47, v0

    div-int/lit8 v45, v47, 0x2

    .line 428
    .restart local v45    # "top":I
    move/from16 v0, v34

    int-to-double v0, v0

    move-wide/from16 v48, v0

    const-wide v50, 0x3fa999999999999aL    # 0.05

    mul-double v48, v48, v50

    move-wide/from16 v0, v48

    double-to-int v0, v0

    move/from16 v47, v0

    div-int/lit8 v47, v47, 0x2

    add-int v10, v34, v47

    .line 438
    :goto_6
    new-instance v36, Ljava/util/Random;

    invoke-direct/range {v36 .. v36}, Ljava/util/Random;-><init>()V

    .line 439
    .local v36, "r":Ljava/util/Random;
    const/16 v47, 0x2

    move-object/from16 v0, v36

    move/from16 v1, v47

    invoke-virtual {v0, v1}, Ljava/util/Random;->nextInt(I)I

    move-result v38

    .line 441
    .local v38, "randomIndex":I
    const/16 v47, 0x9

    move/from16 v0, v47

    new-array v0, v0, [F

    move-object/from16 v19, v0

    .end local v19    # "fstart":[F
    fill-array-data v19, :array_0

    .line 442
    .restart local v19    # "fstart":[F
    const/16 v47, 0x9

    move/from16 v0, v47

    new-array v0, v0, [F

    move-object/from16 v18, v0

    .end local v18    # "fend":[F
    fill-array-data v18, :array_1

    .line 444
    .restart local v18    # "fend":[F
    if-nez v38, :cond_14

    .line 445
    const/16 v47, 0x0

    const/16 v48, 0x0

    move/from16 v0, v35

    int-to-float v0, v0

    move/from16 v49, v0

    move/from16 v0, v34

    int-to-float v0, v0

    move/from16 v50, v0

    move-object/from16 v0, v43

    move/from16 v1, v47

    move/from16 v2, v48

    move/from16 v3, v49

    move/from16 v4, v50

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 446
    move/from16 v0, v28

    int-to-float v0, v0

    move/from16 v47, v0

    move/from16 v0, v45

    int-to-float v0, v0

    move/from16 v48, v0

    move/from16 v0, v42

    int-to-float v0, v0

    move/from16 v49, v0

    int-to-float v0, v10

    move/from16 v50, v0

    move/from16 v0, v47

    move/from16 v1, v48

    move/from16 v2, v49

    move/from16 v3, v50

    invoke-virtual {v13, v0, v1, v2, v3}, Landroid/graphics/RectF;->set(FFFF)V

    .line 447
    move-object/from16 v0, v31

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->setValues([F)V

    .line 448
    move-object/from16 v0, v29

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->setValues([F)V

    goto/16 :goto_3

    .line 418
    .end local v28    # "left":I
    .end local v36    # "r":Ljava/util/Random;
    .end local v38    # "randomIndex":I
    .end local v42    # "right":I
    .end local v45    # "top":I
    :cond_10
    div-int/lit8 v47, v35, 0x2

    move/from16 v0, v35

    int-to-double v0, v0

    move-wide/from16 v48, v0

    const-wide v50, 0x3fa999999999999aL    # 0.05

    mul-double v48, v48, v50

    move-wide/from16 v0, v48

    double-to-int v0, v0

    move/from16 v48, v0

    div-int/lit8 v48, v48, 0x2

    sub-int v47, v47, v48

    move/from16 v0, v40

    move/from16 v1, v47

    if-ge v0, v1, :cond_11

    .line 419
    const/16 v28, 0x0

    .line 420
    .restart local v28    # "left":I
    move/from16 v0, v35

    int-to-double v0, v0

    move-wide/from16 v48, v0

    const-wide v50, 0x3fa999999999999aL    # 0.05

    mul-double v48, v48, v50

    move-wide/from16 v0, v48

    double-to-int v0, v0

    move/from16 v47, v0

    add-int v42, v35, v47

    .line 421
    .restart local v42    # "right":I
    goto/16 :goto_5

    .line 422
    .end local v28    # "left":I
    .end local v42    # "right":I
    :cond_11
    move/from16 v0, v35

    int-to-double v0, v0

    move-wide/from16 v48, v0

    const-wide v50, 0x3fa999999999999aL    # 0.05

    mul-double v48, v48, v50

    move-wide/from16 v0, v48

    double-to-int v0, v0

    move/from16 v47, v0

    move/from16 v0, v47

    neg-int v0, v0

    move/from16 v28, v0

    .line 423
    .restart local v28    # "left":I
    move/from16 v42, v35

    .restart local v42    # "right":I
    goto/16 :goto_5

    .line 429
    :cond_12
    div-int/lit8 v47, v34, 0x2

    move/from16 v0, v34

    int-to-double v0, v0

    move-wide/from16 v48, v0

    const-wide v50, 0x3fa999999999999aL    # 0.05

    mul-double v48, v48, v50

    move-wide/from16 v0, v48

    double-to-int v0, v0

    move/from16 v48, v0

    div-int/lit8 v48, v48, 0x2

    sub-int v47, v47, v48

    move/from16 v0, v41

    move/from16 v1, v47

    if-ge v0, v1, :cond_13

    .line 430
    const/16 v45, 0x0

    .line 431
    .restart local v45    # "top":I
    move/from16 v0, v34

    int-to-double v0, v0

    move-wide/from16 v48, v0

    const-wide v50, 0x3fa999999999999aL    # 0.05

    mul-double v48, v48, v50

    move-wide/from16 v0, v48

    double-to-int v0, v0

    move/from16 v47, v0

    add-int v10, v34, v47

    .line 432
    goto/16 :goto_6

    .line 433
    .end local v45    # "top":I
    :cond_13
    move/from16 v0, v34

    int-to-double v0, v0

    move-wide/from16 v48, v0

    const-wide v50, 0x3fa999999999999aL    # 0.05

    mul-double v48, v48, v50

    move-wide/from16 v0, v48

    double-to-int v0, v0

    move/from16 v47, v0

    move/from16 v0, v47

    neg-int v0, v0

    move/from16 v45, v0

    .line 434
    .restart local v45    # "top":I
    move/from16 v10, v34

    goto/16 :goto_6

    .line 450
    .restart local v36    # "r":Ljava/util/Random;
    .restart local v38    # "randomIndex":I
    :cond_14
    const/16 v47, 0x0

    const/16 v48, 0x0

    move/from16 v0, v35

    int-to-float v0, v0

    move/from16 v49, v0

    move/from16 v0, v34

    int-to-float v0, v0

    move/from16 v50, v0

    move/from16 v0, v47

    move/from16 v1, v48

    move/from16 v2, v49

    move/from16 v3, v50

    invoke-virtual {v13, v0, v1, v2, v3}, Landroid/graphics/RectF;->set(FFFF)V

    .line 451
    move/from16 v0, v28

    int-to-float v0, v0

    move/from16 v47, v0

    move/from16 v0, v45

    int-to-float v0, v0

    move/from16 v48, v0

    move/from16 v0, v42

    int-to-float v0, v0

    move/from16 v49, v0

    int-to-float v0, v10

    move/from16 v50, v0

    move-object/from16 v0, v43

    move/from16 v1, v47

    move/from16 v2, v48

    move/from16 v3, v49

    move/from16 v4, v50

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 452
    move-object/from16 v0, v31

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->setValues([F)V

    .line 453
    move-object/from16 v0, v29

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->setValues([F)V

    goto/16 :goto_3

    .line 460
    .end local v28    # "left":I
    .end local v36    # "r":Ljava/util/Random;
    .end local v38    # "randomIndex":I
    .end local v40    # "refX":I
    .end local v41    # "refY":I
    .end local v42    # "right":I
    .end local v45    # "top":I
    :cond_15
    new-instance v36, Ljava/util/Random;

    invoke-direct/range {v36 .. v36}, Ljava/util/Random;-><init>()V

    .line 461
    .restart local v36    # "r":Ljava/util/Random;
    sget v47, Lcom/samsung/app/video/editor/external/Constants;->KENBURN_EFFECTS_COUNT:I

    move-object/from16 v0, v36

    move/from16 v1, v47

    invoke-virtual {v0, v1}, Ljava/util/Random;->nextInt(I)I

    move-result v38

    .line 464
    .restart local v38    # "randomIndex":I
    # getter for: Lcom/sec/android/app/ve/common/KenburnsDefault;->startMatrix:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/android/app/ve/common/KenburnsDefault;->access$0()Ljava/util/ArrayList;

    move-result-object v47

    move-object/from16 v0, v47

    move/from16 v1, v38

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v47

    check-cast v47, [F

    move-object/from16 v0, v31

    move-object/from16 v1, v47

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->setValues([F)V

    .line 465
    # getter for: Lcom/sec/android/app/ve/common/KenburnsDefault;->startRect:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/android/app/ve/common/KenburnsDefault;->access$1()Ljava/util/ArrayList;

    move-result-object v47

    move-object/from16 v0, v47

    move/from16 v1, v38

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v47

    check-cast v47, Landroid/graphics/RectF;

    move-object/from16 v0, v43

    move-object/from16 v1, v47

    invoke-virtual {v0, v1}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 468
    # getter for: Lcom/sec/android/app/ve/common/KenburnsDefault;->endMatrix:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/android/app/ve/common/KenburnsDefault;->access$2()Ljava/util/ArrayList;

    move-result-object v47

    move-object/from16 v0, v47

    move/from16 v1, v38

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v47

    check-cast v47, [F

    move-object/from16 v0, v29

    move-object/from16 v1, v47

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->setValues([F)V

    .line 469
    # getter for: Lcom/sec/android/app/ve/common/KenburnsDefault;->endRect:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/android/app/ve/common/KenburnsDefault;->access$3()Ljava/util/ArrayList;

    move-result-object v47

    move-object/from16 v0, v47

    move/from16 v1, v38

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v47

    check-cast v47, Landroid/graphics/RectF;

    move-object/from16 v0, v47

    invoke-virtual {v13, v0}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    goto/16 :goto_3

    .line 484
    .end local v36    # "r":Ljava/util/Random;
    .end local v38    # "randomIndex":I
    .restart local v20    # "groupID":I
    :cond_16
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/ve/common/KenburnsDefault$ApplyDefaultKenburnsTask;->mElement:Lcom/samsung/app/video/editor/external/Element;

    move-object/from16 v47, v0

    move-object/from16 v0, v47

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, Lcom/samsung/app/video/editor/external/Element;->setRefRect(Landroid/graphics/RectF;)V

    .line 485
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/ve/common/KenburnsDefault$ApplyDefaultKenburnsTask;->mElement:Lcom/samsung/app/video/editor/external/Element;

    move-object/from16 v47, v0

    move-object/from16 v0, v47

    invoke-virtual {v0, v13}, Lcom/samsung/app/video/editor/external/Element;->setStartRect(Landroid/graphics/RectF;)V

    .line 486
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/ve/common/KenburnsDefault$ApplyDefaultKenburnsTask;->mElement:Lcom/samsung/app/video/editor/external/Element;

    move-object/from16 v47, v0

    move-object/from16 v0, v47

    move-object/from16 v1, v43

    invoke-virtual {v0, v1}, Lcom/samsung/app/video/editor/external/Element;->setEndRect(Landroid/graphics/RectF;)V

    .line 487
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/ve/common/KenburnsDefault$ApplyDefaultKenburnsTask;->mElement:Lcom/samsung/app/video/editor/external/Element;

    move-object/from16 v47, v0

    const/16 v48, 0x0

    move-object/from16 v0, v47

    move-object/from16 v1, v31

    move/from16 v2, v48

    invoke-virtual {v0, v1, v2}, Lcom/samsung/app/video/editor/external/Element;->setMatrix(Landroid/graphics/Matrix;Z)V

    .line 488
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/ve/common/KenburnsDefault$ApplyDefaultKenburnsTask;->mElement:Lcom/samsung/app/video/editor/external/Element;

    move-object/from16 v47, v0

    const/16 v48, 0x1

    move-object/from16 v0, v47

    move-object/from16 v1, v29

    move/from16 v2, v48

    invoke-virtual {v0, v1, v2}, Lcom/samsung/app/video/editor/external/Element;->setMatrix(Landroid/graphics/Matrix;Z)V

    goto/16 :goto_4

    .line 441
    :array_0
    .array-data 4
        0x3f800008    # 1.000001f
        0x0
        0x0
        0x0
        0x3f800008    # 1.000001f
        0x0
        0x0
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 442
    :array_1
    .array-data 4
        0x3f866666    # 1.05f
        0x0
        0x0
        0x0
        0x3f866666    # 1.05f
        0x0
        0x0
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method protected bridge synthetic onBackgroundTask(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lcom/sec/android/app/ve/common/KenburnsDefault$ApplyDefaultKenburnsTask;->onBackgroundTask(Ljava/lang/Object;)Lcom/samsung/app/video/editor/external/Element;

    move-result-object v0

    return-object v0
.end method

.method protected onUIThreadFinally(Ljava/lang/Object;)V
    .locals 0
    .param p1, "result"    # Ljava/lang/Object;

    .prologue
    .line 500
    return-void
.end method
