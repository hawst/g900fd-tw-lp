.class public Lcom/sec/android/app/ve/util/CommonUtils;
.super Ljava/lang/Object;
.source "CommonUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/ve/util/CommonUtils$TimeInTextUnits;
    }
.end annotation


# static fields
.field public static ADDMEDIA_THUMBNAIL_HEIGHT:I = 0x0

.field public static ADDMEDIA_THUMBNAIL_WIDTH:I = 0x0

.field private static final INVALID_CHAR:[Ljava/lang/String;

.field private static final LOW_STORAGE_THRESHOLD:J = 0xa00000L

.field public static final MSG_ON_BACKKEY_PRESSED:I = 0xff

.field public static final MSG_ON_FONTSCALE_CHANGED:I = 0x6f

.field public static final THREAD_WAIT_TIME:I = 0x7d0

.field private static currentSelection:I

.field private static index:I

.field private static mMaxCharToastHandler:Landroid/os/Handler;

.field private static mToast:Landroid/widget/Toast;

.field private static mToastInvalidChar:Landroid/widget/Toast;

.field private static mTooSoonEventHandler:Landroid/os/Handler;

.field public static mexportFileSize:F

.field private static final transForAutoEditMedia:[I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x7

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 89
    sput-object v1, Lcom/sec/android/app/ve/util/CommonUtils;->mToast:Landroid/widget/Toast;

    .line 91
    const/16 v0, 0xe9

    sput v0, Lcom/sec/android/app/ve/util/CommonUtils;->ADDMEDIA_THUMBNAIL_WIDTH:I

    .line 92
    const/16 v0, 0x94

    sput v0, Lcom/sec/android/app/ve/util/CommonUtils;->ADDMEDIA_THUMBNAIL_HEIGHT:I

    .line 96
    sput v2, Lcom/sec/android/app/ve/util/CommonUtils;->currentSelection:I

    .line 97
    sput-object v1, Lcom/sec/android/app/ve/util/CommonUtils;->mToastInvalidChar:Landroid/widget/Toast;

    .line 98
    sput v3, Lcom/sec/android/app/ve/util/CommonUtils;->index:I

    .line 99
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    .line 100
    const-string v1, "\\"

    aput-object v1, v0, v2

    const-string v1, "/"

    aput-object v1, v0, v3

    const/4 v1, 0x2

    const-string v2, ":"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "*"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "?"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "\""

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "<"

    aput-object v2, v0, v1

    const-string v1, ">"

    aput-object v1, v0, v4

    const/16 v1, 0x8

    const-string v2, "|"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, ";"

    aput-object v2, v0, v1

    .line 99
    sput-object v0, Lcom/sec/android/app/ve/util/CommonUtils;->INVALID_CHAR:[Ljava/lang/String;

    .line 105
    new-array v0, v4, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/android/app/ve/util/CommonUtils;->transForAutoEditMedia:[I

    .line 115
    new-instance v0, Lcom/sec/android/app/ve/util/CommonUtils$1;

    invoke-direct {v0}, Lcom/sec/android/app/ve/util/CommonUtils$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/ve/util/CommonUtils;->mMaxCharToastHandler:Landroid/os/Handler;

    .line 714
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/sec/android/app/ve/util/CommonUtils;->mTooSoonEventHandler:Landroid/os/Handler;

    return-void

    .line 105
    nop

    :array_0
    .array-data 4
        0x29
        0x44
        0x45
        0x2a
        0x28
        0x39
        0x5
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 99
    sget-object v0, Lcom/sec/android/app/ve/util/CommonUtils;->INVALID_CHAR:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1()Landroid/widget/Toast;
    .locals 1

    .prologue
    .line 97
    sget-object v0, Lcom/sec/android/app/ve/util/CommonUtils;->mToastInvalidChar:Landroid/widget/Toast;

    return-object v0
.end method

.method static synthetic access$2(Landroid/widget/Toast;)V
    .locals 0

    .prologue
    .line 97
    sput-object p0, Lcom/sec/android/app/ve/util/CommonUtils;->mToastInvalidChar:Landroid/widget/Toast;

    return-void
.end method

.method static synthetic access$3()V
    .locals 0

    .prologue
    .line 893
    invoke-static {}, Lcom/sec/android/app/ve/util/CommonUtils;->showToastMaxChars()V

    return-void
.end method

.method public static addUniqueStringToList(Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 1
    .param p1, "newPath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1399
    .local p0, "fileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz p0, :cond_0

    invoke-virtual {p0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1400
    invoke-virtual {p0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1401
    :cond_0
    return-void
.end method

.method public static applyKenburnsToElement(Lcom/samsung/app/video/editor/external/Element;FFFFFF)V
    .locals 15
    .param p0, "element"    # Lcom/samsung/app/video/editor/external/Element;
    .param p1, "scaleStart"    # F
    .param p2, "scaleEnd"    # F
    .param p3, "translateStartX"    # F
    .param p4, "translateEndX"    # F
    .param p5, "translateStartY"    # F
    .param p6, "translateEndY"    # F

    .prologue
    .line 1504
    const/16 v11, 0x9

    new-array v5, v11, [F

    fill-array-data v5, :array_0

    .line 1505
    .local v5, "init_values":[F
    sget v7, Lcom/sec/android/app/ve/common/ConfigUtils;->PREVIEW_WIDTH:I

    .line 1506
    .local v7, "previewWidth":I
    sget v6, Lcom/sec/android/app/ve/common/ConfigUtils;->PREVIEW_HEIGHT:I

    .line 1508
    .local v6, "previewHeight":I
    new-instance v4, Landroid/graphics/Matrix;

    invoke-direct {v4}, Landroid/graphics/Matrix;-><init>()V

    .line 1509
    .local v4, "identityMatrix":Landroid/graphics/Matrix;
    invoke-virtual {v4, v5}, Landroid/graphics/Matrix;->setValues([F)V

    .line 1511
    new-instance v9, Landroid/graphics/Matrix;

    invoke-direct {v9, v4}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    .line 1512
    .local v9, "startM":Landroid/graphics/Matrix;
    new-instance v2, Landroid/graphics/Matrix;

    invoke-direct {v2, v4}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    .line 1513
    .local v2, "endM":Landroid/graphics/Matrix;
    new-instance v10, Landroid/graphics/RectF;

    const/4 v11, 0x0

    const/4 v12, 0x0

    int-to-float v13, v7

    int-to-float v14, v6

    invoke-direct {v10, v11, v12, v13, v14}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1514
    .local v10, "startR":Landroid/graphics/RectF;
    new-instance v3, Landroid/graphics/RectF;

    const/4 v11, 0x0

    const/4 v12, 0x0

    int-to-float v13, v7

    int-to-float v14, v6

    invoke-direct {v3, v11, v12, v13, v14}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1515
    .local v3, "endR":Landroid/graphics/RectF;
    new-instance v8, Landroid/graphics/RectF;

    const/4 v11, 0x0

    const/4 v12, 0x0

    int-to-float v13, v7

    int-to-float v14, v6

    invoke-direct {v8, v11, v12, v13, v14}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1517
    .local v8, "refR":Landroid/graphics/RectF;
    move/from16 v0, p1

    move/from16 v1, p1

    invoke-virtual {v9, v0, v1}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 1518
    move/from16 v0, p3

    move/from16 v1, p5

    invoke-virtual {v9, v0, v1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 1519
    invoke-virtual {v9, v10, v8}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 1521
    move/from16 v0, p2

    move/from16 v1, p2

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 1522
    move/from16 v0, p4

    move/from16 v1, p6

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 1523
    invoke-virtual {v2, v3, v8}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 1524
    invoke-virtual {p0, v10}, Lcom/samsung/app/video/editor/external/Element;->setStartRect(Landroid/graphics/RectF;)V

    .line 1525
    const/4 v11, 0x1

    invoke-virtual {p0, v9, v11}, Lcom/samsung/app/video/editor/external/Element;->setMatrix(Landroid/graphics/Matrix;Z)V

    .line 1526
    invoke-virtual {p0, v3}, Lcom/samsung/app/video/editor/external/Element;->setEndRect(Landroid/graphics/RectF;)V

    .line 1527
    const/4 v11, 0x0

    invoke-virtual {p0, v2, v11}, Lcom/samsung/app/video/editor/external/Element;->setMatrix(Landroid/graphics/Matrix;Z)V

    .line 1528
    invoke-virtual {p0, v8}, Lcom/samsung/app/video/editor/external/Element;->setRefRect(Landroid/graphics/RectF;)V

    .line 1529
    return-void

    .line 1504
    :array_0
    .array-data 4
        0x3f800008    # 1.000001f
        0x0
        0x0
        0x0
        0x3f800008    # 1.000001f
        0x0
        0x0
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public static checkStorage()Z
    .locals 4

    .prologue
    .line 489
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v0

    const-wide/32 v2, 0xa00000

    invoke-static {v0, v2, v3}, Lcom/sec/android/app/ve/util/CommonUtils;->isStorageSizeAvailable(Ljava/lang/String;J)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static cleanupRAWFiles(Ljava/util/List;)V
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/TranscodeElement;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1327
    .local p0, "projectList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/TranscodeElement;>;"
    invoke-static {}, Lcom/samsung/app/video/editor/external/ClipartParams;->getRawDirPath()Ljava/lang/String;

    move-result-object v14

    .line 1328
    .local v14, "rawFileDirPath":Ljava/lang/String;
    new-instance v13, Ljava/io/File;

    invoke-direct {v13, v14}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1330
    .local v13, "rawFileDir":Ljava/io/File;
    new-instance v18, Lcom/sec/android/app/ve/util/CommonUtils$5;

    invoke-direct/range {v18 .. v18}, Lcom/sec/android/app/ve/util/CommonUtils$5;-><init>()V

    move-object/from16 v0, v18

    invoke-virtual {v13, v0}, Ljava/io/File;->listFiles(Ljava/io/FileFilter;)[Ljava/io/File;

    move-result-object v8

    .line 1346
    .local v8, "files":[Ljava/io/File;
    if-eqz p0, :cond_a

    invoke-interface/range {p0 .. p0}, Ljava/util/List;->isEmpty()Z

    move-result v18

    if-nez v18, :cond_a

    .line 1347
    invoke-interface/range {p0 .. p0}, Ljava/util/List;->size()I

    move-result v12

    .line 1348
    .local v12, "projectCount":I
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 1350
    .local v7, "fileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v10, 0x0

    .local v10, "j":I
    :goto_0
    if-lt v10, v12, :cond_1

    .line 1376
    if-eqz v8, :cond_0

    array-length v0, v8

    move/from16 v18, v0

    if-lez v18, :cond_0

    .line 1377
    const/4 v11, 0x0

    .local v11, "p":I
    :goto_1
    array-length v0, v8

    move/from16 v18, v0

    move/from16 v0, v18

    if-lt v11, v0, :cond_8

    .line 1396
    .end local v7    # "fileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v10    # "j":I
    .end local v11    # "p":I
    .end local v12    # "projectCount":I
    :cond_0
    return-void

    .line 1351
    .restart local v7    # "fileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v10    # "j":I
    .restart local v12    # "projectCount":I
    :cond_1
    move-object/from16 v0, p0

    invoke-interface {v0, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/samsung/app/video/editor/external/TranscodeElement;

    .line 1352
    .local v16, "transcodeElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getTextEleList()Ljava/util/List;

    move-result-object v3

    .line 1353
    .local v3, "clipartList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    if-eqz v3, :cond_2

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v18

    if-nez v18, :cond_2

    .line 1354
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    .line 1355
    .local v2, "captionCount":I
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_2
    if-lt v9, v2, :cond_4

    .line 1362
    .end local v2    # "captionCount":I
    .end local v9    # "i":I
    :cond_2
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getDrawEleList()Ljava/util/List;

    move-result-object v6

    .line 1363
    .local v6, "drawingList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    if-eqz v6, :cond_3

    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v18

    if-nez v18, :cond_3

    .line 1364
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v5

    .line 1365
    .local v5, "drawingCount":I
    const/16 v17, 0x0

    .local v17, "x":I
    :goto_3
    move/from16 v0, v17

    if-lt v0, v5, :cond_6

    .line 1350
    .end local v5    # "drawingCount":I
    .end local v17    # "x":I
    :cond_3
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 1356
    .end local v6    # "drawingList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    .restart local v2    # "captionCount":I
    .restart local v9    # "i":I
    :cond_4
    invoke-interface {v3, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/app/video/editor/external/ClipartParams;

    .line 1357
    .local v1, "caption":Lcom/samsung/app/video/editor/external/ClipartParams;
    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/ClipartParams;->isAssetResource()Z

    move-result v18

    if-nez v18, :cond_5

    .line 1358
    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/ClipartParams;->getFilePath()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-static {v7, v0}, Lcom/sec/android/app/ve/util/CommonUtils;->addUniqueStringToList(Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 1355
    :cond_5
    add-int/lit8 v9, v9, 0x1

    goto :goto_2

    .line 1366
    .end local v1    # "caption":Lcom/samsung/app/video/editor/external/ClipartParams;
    .end local v2    # "captionCount":I
    .end local v9    # "i":I
    .restart local v5    # "drawingCount":I
    .restart local v6    # "drawingList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    .restart local v17    # "x":I
    :cond_6
    move/from16 v0, v17

    invoke-interface {v6, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/app/video/editor/external/ClipartParams;

    .line 1367
    .local v4, "drawing":Lcom/samsung/app/video/editor/external/ClipartParams;
    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/ClipartParams;->isAssetResource()Z

    move-result v18

    if-nez v18, :cond_7

    .line 1368
    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/ClipartParams;->getFilePath()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-static {v7, v0}, Lcom/sec/android/app/ve/util/CommonUtils;->addUniqueStringToList(Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 1369
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/ClipartParams;->getFilePath()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v19, ".ves"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-static {v7, v0}, Lcom/sec/android/app/ve/util/CommonUtils;->addUniqueStringToList(Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 1365
    :cond_7
    add-int/lit8 v17, v17, 0x1

    goto :goto_3

    .line 1378
    .end local v3    # "clipartList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    .end local v4    # "drawing":Lcom/samsung/app/video/editor/external/ClipartParams;
    .end local v5    # "drawingCount":I
    .end local v6    # "drawingList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    .end local v16    # "transcodeElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    .end local v17    # "x":I
    .restart local v11    # "p":I
    :cond_8
    aget-object v18, v8, v11

    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v15

    .line 1379
    .local v15, "rawFilePath":Ljava/lang/String;
    invoke-virtual {v7, v15}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v18

    if-nez v18, :cond_9

    .line 1380
    aget-object v18, v8, v11

    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->exists()Z

    move-result v18

    if-eqz v18, :cond_9

    .line 1381
    aget-object v18, v8, v11

    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->delete()Z

    .line 1377
    :cond_9
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_1

    .line 1388
    .end local v7    # "fileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v10    # "j":I
    .end local v11    # "p":I
    .end local v12    # "projectCount":I
    .end local v15    # "rawFilePath":Ljava/lang/String;
    :cond_a
    if-eqz v8, :cond_0

    array-length v0, v8

    move/from16 v18, v0

    if-lez v18, :cond_0

    .line 1389
    const/4 v11, 0x0

    .restart local v11    # "p":I
    :goto_4
    array-length v0, v8

    move/from16 v18, v0

    move/from16 v0, v18

    if-ge v11, v0, :cond_0

    .line 1390
    aget-object v18, v8, v11

    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->exists()Z

    move-result v18

    if-eqz v18, :cond_b

    .line 1391
    aget-object v18, v8, v11

    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->delete()Z

    .line 1389
    :cond_b
    add-int/lit8 v11, v11, 0x1

    goto :goto_4
.end method

.method public static clearAllExportNotification(Landroid/app/Activity;)V
    .locals 2
    .param p0, "activity"    # Landroid/app/Activity;

    .prologue
    .line 1073
    const-string v1, "notification"

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 1074
    .local v0, "manager":Landroid/app/NotificationManager;
    sget v1, Lcom/sec/android/app/ve/export/ExportService;->EXPORT_NOTFICATION_ID:I

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 1075
    sget v1, Lcom/sec/android/app/ve/export/ExportService;->EXPORT_COMPLETEION_ID:I

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 1076
    invoke-static {p0}, Lcom/sec/android/app/ve/common/AndroidUtils;->dismissDialog(Landroid/app/Activity;)V

    .line 1077
    return-void
.end method

.method public static clearExportNotification(Landroid/app/Activity;)V
    .locals 2
    .param p0, "activity"    # Landroid/app/Activity;

    .prologue
    .line 1067
    const-string v1, "notification"

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 1068
    .local v0, "manager":Landroid/app/NotificationManager;
    sget v1, Lcom/sec/android/app/ve/export/ExportService;->EXPORT_NOTFICATION_ID:I

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 1069
    invoke-static {p0}, Lcom/sec/android/app/ve/common/AndroidUtils;->dismissDialog(Landroid/app/Activity;)V

    .line 1070
    return-void
.end method

.method public static compareDates(Ljava/lang/String;Lcom/samsung/app/video/editor/external/TranscodeElement;)Ljava/lang/String;
    .locals 14
    .param p0, "projectCreationDate"    # Ljava/lang/String;
    .param p1, "trans"    # Lcom/samsung/app/video/editor/external/TranscodeElement;

    .prologue
    .line 987
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/android/app/ve/util/CommonUtils;->getDate(Ljava/lang/Long;)Ljava/lang/String;

    move-result-object v3

    .line 988
    .local v3, "currentDate":Ljava/lang/String;
    invoke-virtual {v3, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 989
    sget v9, Lcom/sec/android/app/ve/R$string;->today:I

    invoke-static {v9}, Lcom/sec/android/app/ve/VEApp;->getStringValue(I)Ljava/lang/String;

    move-result-object p0

    .line 1034
    .end local p0    # "projectCreationDate":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object p0

    .line 992
    .restart local p0    # "projectCreationDate":Ljava/lang/String;
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    const-wide/32 v12, 0x5265c00

    sub-long/2addr v10, v12

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/android/app/ve/util/CommonUtils;->getDate(Ljava/lang/Long;)Ljava/lang/String;

    move-result-object v3

    .line 993
    invoke-virtual {v3, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 994
    sget v9, Lcom/sec/android/app/ve/R$string;->yesterday:I

    invoke-static {v9}, Lcom/sec/android/app/ve/VEApp;->getStringValue(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 997
    :cond_2
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 998
    .local v1, "cal":Ljava/util/Calendar;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-virtual {v1, v10, v11}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 999
    const/4 v9, 0x1

    invoke-virtual {v1, v9}, Ljava/util/Calendar;->get(I)I

    move-result v2

    .line 1001
    .local v2, "currYear":I
    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getProjectModifiedTime()J

    move-result-wide v10

    invoke-virtual {v1, v10, v11}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1002
    const/4 v9, 0x1

    invoke-virtual {v1, v9}, Ljava/util/Calendar;->get(I)I

    move-result v8

    .line 1003
    .local v8, "projCreaYear":I
    const/4 v9, 0x2

    invoke-virtual {v1, v9}, Ljava/util/Calendar;->get(I)I

    move-result v9

    add-int/lit8 v7, v9, 0x1

    .line 1004
    .local v7, "projCreaMonth":I
    const/4 v9, 0x5

    invoke-virtual {v1, v9}, Ljava/util/Calendar;->get(I)I

    move-result v6

    .line 1006
    .local v6, "projCreaDate":I
    if-ne v2, v8, :cond_0

    .line 1008
    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getInitialApplication()Landroid/app/Application;

    move-result-object v9

    invoke-static {v9}, Landroid/text/format/DateFormat;->getDateFormatOrder(Landroid/content/Context;)[C

    move-result-object v5

    .line 1009
    .local v5, "format":[C
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v5}, Ljava/lang/String;-><init>([C)V

    .line 1010
    .local v4, "dateFormat":Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 1011
    .local v0, "buffer":Ljava/lang/StringBuffer;
    if-eqz v4, :cond_7

    const-string v9, "mdy"

    invoke-virtual {v4, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_3

    const-string v9, "ymd"

    invoke-virtual {v4, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 1013
    :cond_3
    const/16 v9, 0xa

    if-ge v7, v9, :cond_4

    .line 1014
    const/4 v9, 0x0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 1015
    :cond_4
    invoke-virtual {v0, v7}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 1016
    const-string v9, "/"

    invoke-virtual {v0, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1017
    const/16 v9, 0xa

    if-ge v6, v9, :cond_5

    .line 1018
    const/4 v9, 0x0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 1019
    :cond_5
    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 1031
    :cond_6
    :goto_1
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p0

    goto/16 :goto_0

    .line 1021
    :cond_7
    if-eqz v4, :cond_6

    const-string v9, "dmy"

    invoke-virtual {v4, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_8

    const-string v9, "ydm"

    invoke-virtual {v4, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 1023
    :cond_8
    const/16 v9, 0xa

    if-ge v6, v9, :cond_9

    .line 1024
    const/4 v9, 0x0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 1025
    :cond_9
    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 1026
    const-string v9, "/"

    invoke-virtual {v0, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1027
    const/16 v9, 0xa

    if-ge v7, v9, :cond_a

    .line 1028
    const/4 v9, 0x0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 1029
    :cond_a
    invoke-virtual {v0, v7}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    goto :goto_1
.end method

.method public static computeRotationAngle(II)I
    .locals 2
    .param p0, "rotateDirection"    # I
    .param p1, "currentAngle"    # I

    .prologue
    .line 1464
    sget v0, Lcom/samsung/app/video/editor/external/Constants$ROTATION_DEGREES;->ZERO:I

    .line 1465
    .local v0, "orientation":I
    sget v1, Lcom/samsung/app/video/editor/external/Constants;->ROTATE_LEFT:I

    if-ne p0, v1, :cond_1

    .line 1466
    sparse-switch p1, :sswitch_data_0

    .line 1500
    :cond_0
    :goto_0
    return v0

    .line 1468
    :sswitch_0
    sget v0, Lcom/samsung/app/video/editor/external/Constants$ROTATION_DEGREES;->TWO_SEVENTY:I

    .line 1469
    goto :goto_0

    .line 1471
    :sswitch_1
    sget v0, Lcom/samsung/app/video/editor/external/Constants$ROTATION_DEGREES;->ZERO:I

    .line 1472
    goto :goto_0

    .line 1474
    :sswitch_2
    sget v0, Lcom/samsung/app/video/editor/external/Constants$ROTATION_DEGREES;->NINETY:I

    .line 1475
    goto :goto_0

    .line 1477
    :sswitch_3
    sget v0, Lcom/samsung/app/video/editor/external/Constants$ROTATION_DEGREES;->ONE_EIGHTY:I

    .line 1478
    goto :goto_0

    .line 1482
    :cond_1
    sget v1, Lcom/samsung/app/video/editor/external/Constants;->ROTATE_RIGHT:I

    if-ne p0, v1, :cond_0

    .line 1483
    sparse-switch p1, :sswitch_data_1

    goto :goto_0

    .line 1485
    :sswitch_4
    sget v0, Lcom/samsung/app/video/editor/external/Constants$ROTATION_DEGREES;->NINETY:I

    .line 1486
    goto :goto_0

    .line 1488
    :sswitch_5
    sget v0, Lcom/samsung/app/video/editor/external/Constants$ROTATION_DEGREES;->ONE_EIGHTY:I

    .line 1489
    goto :goto_0

    .line 1491
    :sswitch_6
    sget v0, Lcom/samsung/app/video/editor/external/Constants$ROTATION_DEGREES;->TWO_SEVENTY:I

    .line 1492
    goto :goto_0

    .line 1494
    :sswitch_7
    sget v0, Lcom/samsung/app/video/editor/external/Constants$ROTATION_DEGREES;->ZERO:I

    .line 1495
    goto :goto_0

    .line 1466
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x5a -> :sswitch_1
        0xb4 -> :sswitch_2
        0x10e -> :sswitch_3
    .end sparse-switch

    .line 1483
    :sswitch_data_1
    .sparse-switch
        0x0 -> :sswitch_4
        0x5a -> :sswitch_5
        0xb4 -> :sswitch_6
        0x10e -> :sswitch_7
    .end sparse-switch
.end method

.method public static containsPrivateModeContents(Lcom/samsung/app/video/editor/external/TranscodeElement;Landroid/content/Context;)Z
    .locals 9
    .param p0, "tElement"    # Lcom/samsung/app/video/editor/external/TranscodeElement;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 1082
    if-eqz p0, :cond_3

    .line 1083
    :try_start_0
    invoke-static {p1}, Lcom/samsung/android/secretmode/SecretModeManager;->getPersonalPageRoot(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    .line 1085
    .local v4, "privateFolderRoot":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementList()Ljava/util/List;

    move-result-object v1

    .line 1086
    .local v1, "elementList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    if-eqz v1, :cond_1

    invoke-static {v1, v4}, Lcom/sec/android/app/ve/util/CommonUtils;->containsPrivateModeContents(Ljava/util/List;Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 1106
    .end local v1    # "elementList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    .end local v4    # "privateFolderRoot":Ljava/lang/String;
    :cond_0
    :goto_0
    return v6

    .line 1089
    .restart local v1    # "elementList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    .restart local v4    # "privateFolderRoot":Ljava/lang/String;
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getAdditionlAudioEleList()Ljava/util/List;

    move-result-object v0

    .line 1090
    .local v0, "audioEleList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    if-eqz v0, :cond_2

    invoke-static {v0, v4}, Lcom/sec/android/app/ve/util/CommonUtils;->containsPrivateModeContents(Ljava/util/List;Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 1093
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getAdditionlRecordEleList()Ljava/util/List;

    move-result-object v5

    .line 1094
    .local v5, "recordEleList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    if-eqz v5, :cond_3

    invoke-static {v5, v4}, Lcom/sec/android/app/ve/util/CommonUtils;->containsPrivateModeContents(Ljava/util/List;Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_1

    move-result v8

    if-nez v8, :cond_0

    .end local v0    # "audioEleList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    .end local v1    # "elementList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    .end local v4    # "privateFolderRoot":Ljava/lang/String;
    .end local v5    # "recordEleList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    :cond_3
    move v6, v7

    .line 1106
    goto :goto_0

    .line 1098
    :catch_0
    move-exception v2

    .line 1099
    .local v2, "noClassDefError":Ljava/lang/NoClassDefFoundError;
    invoke-virtual {v2}, Ljava/lang/NoClassDefFoundError;->printStackTrace()V

    move v6, v7

    .line 1100
    goto :goto_0

    .line 1102
    .end local v2    # "noClassDefError":Ljava/lang/NoClassDefFoundError;
    :catch_1
    move-exception v3

    .line 1103
    .local v3, "noMethodError":Ljava/lang/NoSuchMethodError;
    invoke-virtual {v3}, Ljava/lang/NoSuchMethodError;->printStackTrace()V

    move v6, v7

    .line 1104
    goto :goto_0
.end method

.method private static containsPrivateModeContents(Ljava/util/List;Ljava/lang/String;)Z
    .locals 5
    .param p1, "rootFolder"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/Element;",
            ">;",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 1110
    .local p0, "elementList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    .line 1111
    .local v0, "count":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-lt v3, v0, :cond_0

    .line 1119
    const/4 v4, 0x0

    :goto_1
    return v4

    .line 1112
    :cond_0
    invoke-interface {p0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/app/video/editor/external/Element;

    .line 1113
    .local v1, "element":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/Element;->getFilePath()Ljava/lang/String;

    move-result-object v2

    .line 1115
    .local v2, "filePath":Ljava/lang/String;
    if-eqz v2, :cond_1

    invoke-virtual {v2, p1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1116
    const/4 v4, 0x1

    goto :goto_1

    .line 1111
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public static convertStreamToString(Ljava/io/InputStream;)Ljava/lang/String;
    .locals 6
    .param p0, "is"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 466
    if-eqz p0, :cond_1

    .line 467
    new-instance v3, Ljava/io/StringWriter;

    invoke-direct {v3}, Ljava/io/StringWriter;-><init>()V

    .line 469
    .local v3, "writer":Ljava/io/Writer;
    const/16 v4, 0x400

    new-array v0, v4, [C

    .line 471
    .local v0, "buffer":[C
    :try_start_0
    new-instance v2, Ljava/io/BufferedReader;

    .line 472
    new-instance v4, Ljava/io/InputStreamReader;

    const-string v5, "UTF-8"

    invoke-direct {v4, p0, v5}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 471
    invoke-direct {v2, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 473
    .local v2, "reader":Ljava/io/Reader;
    invoke-virtual {v2, v0}, Ljava/io/Reader;->read([C)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    .line 474
    .local v1, "n":I
    :goto_0
    const/4 v4, -0x1

    if-ne v1, v4, :cond_0

    .line 479
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V

    .line 481
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 483
    .end local v0    # "buffer":[C
    .end local v1    # "n":I
    .end local v2    # "reader":Ljava/io/Reader;
    .end local v3    # "writer":Ljava/io/Writer;
    :goto_1
    return-object v4

    .line 475
    .restart local v0    # "buffer":[C
    .restart local v1    # "n":I
    .restart local v2    # "reader":Ljava/io/Reader;
    .restart local v3    # "writer":Ljava/io/Writer;
    :cond_0
    const/4 v4, 0x0

    :try_start_1
    invoke-virtual {v3, v0, v4, v1}, Ljava/io/Writer;->write([CII)V

    .line 476
    invoke-virtual {v2, v0}, Ljava/io/Reader;->read([C)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    goto :goto_0

    .line 478
    .end local v1    # "n":I
    .end local v2    # "reader":Ljava/io/Reader;
    :catchall_0
    move-exception v4

    .line 479
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V

    .line 480
    throw v4

    .line 483
    .end local v0    # "buffer":[C
    .end local v3    # "writer":Ljava/io/Writer;
    :cond_1
    const-string v4, ""

    goto :goto_1
.end method

.method public static copyBitmapLocally(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 21
    .param p0, "lBitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 609
    if-eqz p0, :cond_1

    .line 611
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v16

    .line 612
    .local v16, "width":I
    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v14

    .line 614
    .local v14, "height":I
    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    .line 615
    .local v4, "DEFAULT_THUMBNAIL_WIDTH":I
    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    .line 617
    .local v3, "DEFAULT_THUMBNAIL_HEIGHT":I
    div-int/lit8 v12, v16, 0x2

    .line 618
    .local v12, "focusX":I
    div-int/lit8 v13, v14, 0x2

    .line 625
    .local v13, "focusY":I
    mul-int v17, v4, v14

    mul-int v18, v3, v16

    move/from16 v0, v17

    move/from16 v1, v18

    if-ge v0, v1, :cond_0

    .line 627
    mul-int v17, v4, v14

    div-int v7, v17, v3

    .line 628
    .local v7, "cropWidth":I
    const/16 v17, 0x0

    .line 629
    div-int/lit8 v18, v7, 0x2

    sub-int v18, v12, v18

    .line 630
    sub-int v19, v16, v7

    .line 629
    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->min(II)I

    move-result v18

    .line 628
    invoke-static/range {v17 .. v18}, Ljava/lang/Math;->max(II)I

    move-result v8

    .line 631
    .local v8, "cropX":I
    const/4 v9, 0x0

    .line 632
    .local v9, "cropY":I
    move v6, v14

    .line 644
    .local v6, "cropHeight":I
    :goto_0
    sget-object v17, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 643
    move-object/from16 v0, v17

    invoke-static {v4, v3, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v11

    .line 645
    .local v11, "finalBitmap":Landroid/graphics/Bitmap;
    new-instance v5, Landroid/graphics/Canvas;

    invoke-direct {v5, v11}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 646
    .local v5, "canvas":Landroid/graphics/Canvas;
    new-instance v15, Landroid/graphics/Paint;

    invoke-direct {v15}, Landroid/graphics/Paint;-><init>()V

    .line 647
    .local v15, "paint":Landroid/graphics/Paint;
    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v15, v0}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 648
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v5, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 650
    new-instance v17, Landroid/graphics/Rect;

    add-int v18, v8, v7

    .line 651
    add-int v19, v9, v6

    .line 650
    move-object/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-direct {v0, v8, v9, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 652
    new-instance v18, Landroid/graphics/Rect;

    const/16 v19, 0x0

    const/16 v20, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-direct {v0, v1, v2, v4, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 649
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    invoke-virtual {v5, v0, v1, v2, v15}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 655
    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Bitmap;->recycle()V

    .line 663
    .end local v3    # "DEFAULT_THUMBNAIL_HEIGHT":I
    .end local v4    # "DEFAULT_THUMBNAIL_WIDTH":I
    .end local v5    # "canvas":Landroid/graphics/Canvas;
    .end local v6    # "cropHeight":I
    .end local v7    # "cropWidth":I
    .end local v8    # "cropX":I
    .end local v9    # "cropY":I
    .end local v11    # "finalBitmap":Landroid/graphics/Bitmap;
    .end local v12    # "focusX":I
    .end local v13    # "focusY":I
    .end local v14    # "height":I
    .end local v15    # "paint":Landroid/graphics/Paint;
    .end local v16    # "width":I
    :goto_1
    return-object v11

    .line 635
    .restart local v3    # "DEFAULT_THUMBNAIL_HEIGHT":I
    .restart local v4    # "DEFAULT_THUMBNAIL_WIDTH":I
    .restart local v12    # "focusX":I
    .restart local v13    # "focusY":I
    .restart local v14    # "height":I
    .restart local v16    # "width":I
    :cond_0
    mul-int v17, v3, v16

    div-int v6, v17, v4

    .line 636
    .restart local v6    # "cropHeight":I
    const/16 v17, 0x0

    .line 637
    div-int/lit8 v18, v6, 0x2

    sub-int v18, v13, v18

    .line 638
    sub-int v19, v14, v6

    .line 637
    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->min(II)I

    move-result v18

    .line 636
    invoke-static/range {v17 .. v18}, Ljava/lang/Math;->max(II)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v9

    .line 639
    .restart local v9    # "cropY":I
    const/4 v8, 0x0

    .line 640
    .restart local v8    # "cropX":I
    move/from16 v7, v16

    .restart local v7    # "cropWidth":I
    goto :goto_0

    .line 658
    .end local v3    # "DEFAULT_THUMBNAIL_HEIGHT":I
    .end local v4    # "DEFAULT_THUMBNAIL_WIDTH":I
    .end local v6    # "cropHeight":I
    .end local v7    # "cropWidth":I
    .end local v8    # "cropX":I
    .end local v9    # "cropY":I
    .end local v12    # "focusX":I
    .end local v13    # "focusY":I
    .end local v14    # "height":I
    .end local v16    # "width":I
    :catch_0
    move-exception v10

    .line 659
    .local v10, "e":Ljava/lang/Exception;
    new-instance v17, Ljava/lang/StringBuilder;

    const-string v18, "Exception Utils copyBitmapLocally"

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/ve/common/LogUtils;->log(Ljava/lang/String;)V

    .end local v10    # "e":Ljava/lang/Exception;
    :cond_1
    move-object/from16 v11, p0

    .line 663
    goto :goto_1
.end method

.method public static copyFile(Ljava/lang/String;Ljava/lang/String;)V
    .locals 10
    .param p0, "sourcePath"    # Ljava/lang/String;
    .param p1, "destPath"    # Ljava/lang/String;

    .prologue
    .line 554
    const/4 v3, 0x0

    .line 555
    .local v3, "inStream":Ljava/io/InputStream;
    const/4 v6, 0x0

    .line 559
    .local v6, "outStream":Ljava/io/OutputStream;
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 590
    :cond_0
    :goto_0
    return-void

    .line 563
    :cond_1
    :try_start_0
    new-instance v8, Ljava/io/File;

    invoke-direct {v8, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 564
    .local v8, "sourceFile":Ljava/io/File;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 566
    .local v1, "destFile":Ljava/io/File;
    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 570
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v9

    if-nez v9, :cond_2

    .line 571
    invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z

    .line 574
    :cond_2
    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, v8}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 575
    .end local v3    # "inStream":Ljava/io/InputStream;
    .local v4, "inStream":Ljava/io/InputStream;
    :try_start_1
    new-instance v7, Ljava/io/FileOutputStream;

    invoke-direct {v7, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 577
    .end local v6    # "outStream":Ljava/io/OutputStream;
    .local v7, "outStream":Ljava/io/OutputStream;
    const/16 v9, 0x400

    :try_start_2
    new-array v0, v9, [B

    .line 581
    .local v0, "buffer":[B
    :goto_1
    invoke-virtual {v4, v0}, Ljava/io/InputStream;->read([B)I

    move-result v5

    .local v5, "length":I
    if-gtz v5, :cond_3

    .line 584
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    .line 585
    invoke-virtual {v7}, Ljava/io/OutputStream;->close()V

    move-object v6, v7

    .end local v7    # "outStream":Ljava/io/OutputStream;
    .restart local v6    # "outStream":Ljava/io/OutputStream;
    move-object v3, v4

    .line 587
    .end local v4    # "inStream":Ljava/io/InputStream;
    .restart local v3    # "inStream":Ljava/io/InputStream;
    goto :goto_0

    .line 582
    .end local v3    # "inStream":Ljava/io/InputStream;
    .end local v6    # "outStream":Ljava/io/OutputStream;
    .restart local v4    # "inStream":Ljava/io/InputStream;
    .restart local v7    # "outStream":Ljava/io/OutputStream;
    :cond_3
    const/4 v9, 0x0

    invoke-virtual {v7, v0, v9, v5}, Ljava/io/OutputStream;->write([BII)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    .line 587
    .end local v0    # "buffer":[B
    .end local v5    # "length":I
    :catch_0
    move-exception v2

    move-object v6, v7

    .end local v7    # "outStream":Ljava/io/OutputStream;
    .restart local v6    # "outStream":Ljava/io/OutputStream;
    move-object v3, v4

    .line 588
    .end local v1    # "destFile":Ljava/io/File;
    .end local v4    # "inStream":Ljava/io/InputStream;
    .end local v8    # "sourceFile":Ljava/io/File;
    .local v2, "e":Ljava/io/IOException;
    .restart local v3    # "inStream":Ljava/io/InputStream;
    :goto_2
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 587
    .end local v2    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v2

    goto :goto_2

    .end local v3    # "inStream":Ljava/io/InputStream;
    .restart local v1    # "destFile":Ljava/io/File;
    .restart local v4    # "inStream":Ljava/io/InputStream;
    .restart local v8    # "sourceFile":Ljava/io/File;
    :catch_2
    move-exception v2

    move-object v3, v4

    .end local v4    # "inStream":Ljava/io/InputStream;
    .restart local v3    # "inStream":Ljava/io/InputStream;
    goto :goto_2
.end method

.method public static copyFileFromAssetsToFilesDir(Landroid/content/res/AssetManager;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9
    .param p0, "assetManager"    # Landroid/content/res/AssetManager;
    .param p1, "assetSourceFilePath"    # Ljava/lang/String;
    .param p2, "destinationFilePath"    # Ljava/lang/String;

    .prologue
    .line 1278
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1279
    .local v2, "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_2

    .line 1282
    const/4 v3, 0x0

    .line 1283
    .local v3, "in":Ljava/io/InputStream;
    const/4 v4, 0x0

    .line 1285
    .local v4, "out":Ljava/io/FileOutputStream;
    :try_start_0
    invoke-virtual {p0, p1}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v3

    .line 1286
    new-instance v5, Ljava/io/FileOutputStream;

    invoke-direct {v5, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1287
    .end local v4    # "out":Ljava/io/FileOutputStream;
    .local v5, "out":Ljava/io/FileOutputStream;
    const/16 v7, 0x400

    :try_start_1
    new-array v0, v7, [B

    .line 1288
    .local v0, "buffer":[B
    invoke-virtual {v3, v0}, Ljava/io/InputStream;->read([B)I

    move-result v6

    .line 1289
    .local v6, "read":I
    :goto_0
    const/4 v7, -0x1

    if-ne v6, v7, :cond_3

    .line 1293
    const/4 v7, 0x1

    const/4 v8, 0x1

    invoke-virtual {v2, v7, v8}, Ljava/io/File;->setExecutable(ZZ)Z

    .line 1294
    const/4 v7, 0x1

    const/4 v8, 0x1

    invoke-virtual {v2, v7, v8}, Ljava/io/File;->setWritable(ZZ)Z

    .line 1295
    const/4 v7, 0x1

    const/4 v8, 0x1

    invoke-virtual {v2, v7, v8}, Ljava/io/File;->setReadable(ZZ)Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_a
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1300
    if-eqz v3, :cond_0

    .line 1301
    :try_start_2
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_7

    .line 1306
    :cond_0
    :goto_1
    const/4 v3, 0x0

    .line 1308
    if-eqz v5, :cond_1

    .line 1309
    :try_start_3
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->flush()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_8

    .line 1315
    :cond_1
    :goto_2
    if-eqz v5, :cond_2

    .line 1316
    :try_start_4
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_9

    .line 1324
    .end local v0    # "buffer":[B
    .end local v3    # "in":Ljava/io/InputStream;
    .end local v5    # "out":Ljava/io/FileOutputStream;
    .end local v6    # "read":I
    :cond_2
    :goto_3
    return-void

    .line 1290
    .restart local v0    # "buffer":[B
    .restart local v3    # "in":Ljava/io/InputStream;
    .restart local v5    # "out":Ljava/io/FileOutputStream;
    .restart local v6    # "read":I
    :cond_3
    const/4 v7, 0x0

    :try_start_5
    invoke-virtual {v5, v0, v7, v6}, Ljava/io/FileOutputStream;->write([BII)V

    .line 1291
    invoke-virtual {v3, v0}, Ljava/io/InputStream;->read([B)I
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_a
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-result v6

    goto :goto_0

    .line 1296
    .end local v0    # "buffer":[B
    .end local v5    # "out":Ljava/io/FileOutputStream;
    .end local v6    # "read":I
    .restart local v4    # "out":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v1

    .line 1297
    .local v1, "e":Ljava/io/IOException;
    :goto_4
    :try_start_6
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 1300
    if-eqz v3, :cond_4

    .line 1301
    :try_start_7
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1

    .line 1306
    :cond_4
    :goto_5
    const/4 v3, 0x0

    .line 1308
    if-eqz v4, :cond_5

    .line 1309
    :try_start_8
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->flush()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_2

    .line 1315
    :cond_5
    :goto_6
    if-eqz v4, :cond_6

    .line 1316
    :try_start_9
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_3

    .line 1321
    :cond_6
    :goto_7
    const/4 v4, 0x0

    goto :goto_3

    .line 1302
    :catch_1
    move-exception v1

    .line 1304
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 1310
    :catch_2
    move-exception v1

    .line 1312
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 1317
    :catch_3
    move-exception v1

    .line 1319
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_7

    .line 1298
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v7

    .line 1300
    :goto_8
    if-eqz v3, :cond_7

    .line 1301
    :try_start_a
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_4

    .line 1306
    :cond_7
    :goto_9
    const/4 v3, 0x0

    .line 1308
    if-eqz v4, :cond_8

    .line 1309
    :try_start_b
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->flush()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_5

    .line 1315
    :cond_8
    :goto_a
    if-eqz v4, :cond_9

    .line 1316
    :try_start_c
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_6

    .line 1321
    :cond_9
    :goto_b
    const/4 v4, 0x0

    .line 1322
    throw v7

    .line 1302
    :catch_4
    move-exception v1

    .line 1304
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_9

    .line 1310
    .end local v1    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v1

    .line 1312
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_a

    .line 1317
    .end local v1    # "e":Ljava/io/IOException;
    :catch_6
    move-exception v1

    .line 1319
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_b

    .line 1302
    .end local v1    # "e":Ljava/io/IOException;
    .end local v4    # "out":Ljava/io/FileOutputStream;
    .restart local v0    # "buffer":[B
    .restart local v5    # "out":Ljava/io/FileOutputStream;
    .restart local v6    # "read":I
    :catch_7
    move-exception v1

    .line 1304
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 1310
    .end local v1    # "e":Ljava/io/IOException;
    :catch_8
    move-exception v1

    .line 1312
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 1317
    .end local v1    # "e":Ljava/io/IOException;
    :catch_9
    move-exception v1

    .line 1319
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 1298
    .end local v0    # "buffer":[B
    .end local v1    # "e":Ljava/io/IOException;
    .end local v6    # "read":I
    :catchall_1
    move-exception v7

    move-object v4, v5

    .end local v5    # "out":Ljava/io/FileOutputStream;
    .restart local v4    # "out":Ljava/io/FileOutputStream;
    goto :goto_8

    .line 1296
    .end local v4    # "out":Ljava/io/FileOutputStream;
    .restart local v5    # "out":Ljava/io/FileOutputStream;
    :catch_a
    move-exception v1

    move-object v4, v5

    .end local v5    # "out":Ljava/io/FileOutputStream;
    .restart local v4    # "out":Ljava/io/FileOutputStream;
    goto :goto_4
.end method

.method public static createTextPaint(Landroid/content/Context;Ljava/lang/String;ZIIIIIIILandroid/graphics/Paint$Align;Landroid/content/res/AssetManager;I)Landroid/graphics/Paint;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "ttfFilePath"    # Ljava/lang/String;
    .param p2, "isPlatformFont"    # Z
    .param p3, "fontTextStyle"    # I
    .param p4, "fontSize"    # I
    .param p5, "fontColor"    # I
    .param p6, "shadowR"    # I
    .param p7, "shadowDx"    # I
    .param p8, "shadowDy"    # I
    .param p9, "shadowColor"    # I
    .param p10, "alignFlag"    # Landroid/graphics/Paint$Align;
    .param p11, "assetMgr"    # Landroid/content/res/AssetManager;
    .param p12, "paintFlags"    # I

    .prologue
    .line 961
    new-instance v1, Landroid/graphics/Paint;

    or-int/lit8 v3, p12, 0x1

    invoke-direct {v1, v3}, Landroid/graphics/Paint;-><init>(I)V

    .line 962
    .local v1, "paint":Landroid/graphics/Paint;
    const/4 v2, 0x0

    .line 963
    .local v2, "typeface":Landroid/graphics/Typeface;
    if-eqz p2, :cond_1

    .line 964
    invoke-static {p1, p3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v2

    .line 969
    :goto_0
    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 970
    int-to-float v3, p4

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 971
    invoke-virtual {v1, p5}, Landroid/graphics/Paint;->setColor(I)V

    .line 972
    if-nez p10, :cond_0

    sget-object p10, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    .line 973
    :cond_0
    move-object/from16 v0, p10

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 974
    int-to-float v3, p6

    int-to-float v4, p7

    int-to-float v5, p8

    invoke-virtual {v1, v3, v4, v5, p9}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    .line 975
    return-object v1

    .line 967
    :cond_1
    move-object/from16 v0, p11

    invoke-static {v0, p1}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v2

    goto :goto_0
.end method

.method public static deepCopy(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7
    .param p0, "object"    # Ljava/lang/Object;

    .prologue
    .line 531
    const/4 v3, 0x0

    .line 534
    .local v3, "obj":Ljava/lang/Object;
    :try_start_0
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 535
    .local v1, "bos":Ljava/io/ByteArrayOutputStream;
    new-instance v5, Ljava/io/ObjectOutputStream;

    invoke-direct {v5, v1}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 536
    .local v5, "oos":Ljava/io/ObjectOutputStream;
    invoke-virtual {v5, p0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 537
    invoke-virtual {v5}, Ljava/io/ObjectOutputStream;->flush()V

    .line 538
    invoke-virtual {v5}, Ljava/io/ObjectOutputStream;->close()V

    .line 540
    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v6

    invoke-direct {v0, v6}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 541
    .local v0, "bis":Ljava/io/ByteArrayInputStream;
    new-instance v4, Ljava/io/ObjectInputStream;

    invoke-direct {v4, v0}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V

    .line 542
    .local v4, "ois":Ljava/io/ObjectInputStream;
    invoke-virtual {v4}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v3

    .line 543
    invoke-virtual {v4}, Ljava/io/ObjectInputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 550
    .end local v0    # "bis":Ljava/io/ByteArrayInputStream;
    .end local v1    # "bos":Ljava/io/ByteArrayOutputStream;
    .end local v3    # "obj":Ljava/lang/Object;
    .end local v4    # "ois":Ljava/io/ObjectInputStream;
    .end local v5    # "oos":Ljava/io/ObjectOutputStream;
    :goto_0
    return-object v3

    .line 546
    :catch_0
    move-exception v2

    .line 547
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static disableSmileysInFileRenameDialog(I)I
    .locals 0
    .param p0, "type"    # I

    .prologue
    .line 861
    and-int/lit8 p0, p0, -0x41

    .line 862
    return p0
.end method

.method public static exportResolutionString(I)Ljava/lang/String;
    .locals 1
    .param p0, "mappedResolution"    # I

    .prologue
    .line 1432
    packed-switch p0, :pswitch_data_0

    .line 1460
    const-string v0, "1920x1080"

    :goto_0
    return-object v0

    .line 1434
    :pswitch_0
    const-string v0, "176x144"

    goto :goto_0

    .line 1437
    :pswitch_1
    const-string v0, "320x240"

    goto :goto_0

    .line 1440
    :pswitch_2
    const-string v0, "640x480"

    goto :goto_0

    .line 1443
    :pswitch_3
    const-string v0, "720x480"

    goto :goto_0

    .line 1446
    :pswitch_4
    const-string v0, "1280x720"

    goto :goto_0

    .line 1449
    :pswitch_5
    const-string v0, "1920x1080"

    goto :goto_0

    .line 1452
    :pswitch_6
    const-string v0, "3840*2160"

    goto :goto_0

    .line 1455
    :pswitch_7
    const-string v0, "640x360"

    goto :goto_0

    .line 1432
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public static getBitmapFromEngine(Lcom/samsung/app/video/editor/external/TranscodeElement;J)Landroid/graphics/Bitmap;
    .locals 17
    .param p0, "transcodeElement"    # Lcom/samsung/app/video/editor/external/TranscodeElement;
    .param p1, "projectPreviewTime"    # J

    .prologue
    .line 401
    const-string v2, "VIDEO EDITOR"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v8, " Bitmap need to be created ,call  getProjectPreviewImage , ProjectpreviewTime= "

    invoke-direct {v3, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, p1

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 402
    const/16 v6, 0x300

    .line 403
    .local v6, "width":I
    const/16 v7, 0x1b0

    .line 406
    .local v7, "height":I
    sget-object v2, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    .line 405
    invoke-static {v6, v7, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v12

    .line 411
    .local v12, "bitmap":Landroid/graphics/Bitmap;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getTotalDuration()J

    move-result-wide v2

    cmp-long v2, p1, v2

    if-lez v2, :cond_0

    .line 412
    const-wide/16 p1, 0x1

    .line 415
    :cond_0
    invoke-virtual/range {p0 .. p2}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementAt(J)Lcom/samsung/app/video/editor/external/Element;

    move-result-object v4

    .line 417
    .local v4, "ele":Lcom/samsung/app/video/editor/external/Element;
    if-eqz v4, :cond_4

    .line 418
    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getStoryTimeTillElement(Lcom/samsung/app/video/editor/external/Element;)J

    move-result-wide v2

    long-to-float v14, v2

    .line 419
    .local v14, "eleStartTimeInList":F
    move-wide/from16 v0, p1

    long-to-float v2, v0

    sub-float/2addr v2, v14

    float-to-int v5, v2

    .line 421
    .local v5, "time":I
    int-to-long v2, v5

    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/Element;->getEndTime()J

    move-result-wide v8

    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/Element;->getStartTime()J

    move-result-wide v10

    sub-long/2addr v8, v10

    cmp-long v2, v2, v8

    if-lez v2, :cond_1

    .line 422
    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/Element;->getEndTime()J

    move-result-wide v2

    long-to-int v5, v2

    .line 426
    :cond_1
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v2

    .line 427
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getAssetManager()Landroid/content/res/AssetManager;

    move-result-object v3

    const/4 v8, 0x1

    .line 428
    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    .line 426
    invoke-virtual/range {v2 .. v11}, Lcom/samsung/app/video/editor/external/NativeInterface;->_getProjectPreviewImage(Landroid/content/res/AssetManager;Lcom/samsung/app/video/editor/external/Element;IIIIIII)[B

    move-result-object v13

    .line 431
    .local v13, "bytes":[B
    if-eqz v13, :cond_3

    .line 438
    invoke-static {v13}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v15

    .line 440
    .local v15, "pixelBuffer":Ljava/nio/ByteBuffer;
    if-eqz v12, :cond_2

    .line 441
    invoke-virtual {v12, v15}, Landroid/graphics/Bitmap;->copyPixelsFromBuffer(Ljava/nio/Buffer;)V

    .line 450
    .end local v5    # "time":I
    .end local v12    # "bitmap":Landroid/graphics/Bitmap;
    .end local v13    # "bytes":[B
    .end local v14    # "eleStartTimeInList":F
    .end local v15    # "pixelBuffer":Ljava/nio/ByteBuffer;
    :cond_2
    :goto_0
    return-object v12

    .line 447
    .restart local v5    # "time":I
    .restart local v12    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v13    # "bytes":[B
    .restart local v14    # "eleStartTimeInList":F
    :cond_3
    const/4 v12, 0x0

    goto :goto_0

    .line 450
    .end local v5    # "time":I
    .end local v13    # "bytes":[B
    .end local v14    # "eleStartTimeInList":F
    :cond_4
    const/4 v12, 0x0

    goto :goto_0
.end method

.method public static getCeilingTimeToNearestSeconds(J)J
    .locals 8
    .param p0, "time"    # J

    .prologue
    .line 749
    const-wide/16 v2, 0x0

    .line 750
    .local v2, "ceilingTime":J
    long-to-double v4, p0

    const-wide v6, 0x408f400000000000L    # 1000.0

    div-double v0, v4, v6

    .line 751
    .local v0, "actualTimeInSecs":D
    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    mul-long v2, v4, v6

    .line 752
    return-wide v2
.end method

.method public static getDate(Ljava/lang/Long;)Ljava/lang/String;
    .locals 6
    .param p0, "value"    # Ljava/lang/Long;

    .prologue
    .line 979
    new-instance v0, Ljava/util/Date;

    invoke-virtual {p0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-direct {v0, v4, v5}, Ljava/util/Date;-><init>(J)V

    .line 980
    .local v0, "date":Ljava/util/Date;
    const-string v2, ""

    .line 981
    .local v2, "frmtDate":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getInitialApplication()Landroid/app/Application;

    move-result-object v3

    invoke-static {v3}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v1

    .line 982
    .local v1, "dateFrmt":Ljava/text/DateFormat;
    invoke-virtual {v1, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    .line 983
    return-object v2
.end method

.method public static getDuplicateProjectWithID(Ljava/util/List;Ljava/lang/String;)Lcom/samsung/app/video/editor/external/TranscodeElement;
    .locals 4
    .param p1, "uniqueID"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/TranscodeElement;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/samsung/app/video/editor/external/TranscodeElement;"
        }
    .end annotation

    .prologue
    .line 1266
    .local p0, "projectList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/TranscodeElement;>;"
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1267
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    .line 1268
    .local v1, "size":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, v1, :cond_2

    .line 1274
    .end local v0    # "i":I
    .end local v1    # "size":I
    :cond_0
    const/4 v2, 0x0

    :cond_1
    return-object v2

    .line 1269
    .restart local v0    # "i":I
    .restart local v1    # "size":I
    :cond_2
    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/app/video/editor/external/TranscodeElement;

    .line 1270
    .local v2, "tElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getUniqueProjectID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1268
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static getEditTextFilter(I)[Landroid/text/InputFilter;
    .locals 3
    .param p0, "length"    # I

    .prologue
    const/16 v1, 0x18

    .line 792
    const/4 v2, 0x2

    new-array v0, v2, [Landroid/text/InputFilter;

    .line 793
    .local v0, "FilterArray":[Landroid/text/InputFilter;
    if-le p0, v1, :cond_0

    .line 794
    :goto_0
    const/4 v1, 0x0

    sget v2, Lcom/sec/android/app/ve/R$string;->toast_max_characters:I

    invoke-static {p0, v2}, Lcom/sec/android/app/ve/util/CommonUtils;->getLengthFilterWithToast(II)Landroid/text/InputFilter;

    move-result-object v2

    aput-object v2, v0, v1

    .line 795
    const/4 v1, 0x1

    new-instance v2, Lcom/sec/android/app/ve/util/CommonUtils$2;

    invoke-direct {v2}, Lcom/sec/android/app/ve/util/CommonUtils$2;-><init>()V

    aput-object v2, v0, v1

    .line 857
    return-object v0

    :cond_0
    move p0, v1

    .line 793
    goto :goto_0
.end method

.method public static getFlooringTimeToNearestSeconds(J)J
    .locals 6
    .param p0, "time"    # J

    .prologue
    const-wide/16 v4, 0x3e8

    .line 737
    const-wide/16 v2, 0x0

    .line 738
    .local v2, "flooringTime":J
    div-long v0, p0, v4

    .line 739
    .local v0, "actualTimeInSecs":J
    mul-long v2, v0, v4

    .line 740
    return-wide v2
.end method

.method public static getFrameRateOfVideo(Lcom/samsung/app/video/editor/external/Element;)F
    .locals 7
    .param p0, "mCurElement"    # Lcom/samsung/app/video/editor/external/Element;

    .prologue
    .line 318
    const/4 v3, 0x0

    .line 319
    .local v3, "fps":I
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/Element;->getType()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_0

    .line 320
    const/16 v3, 0x1e

    .line 322
    :try_start_0
    new-instance v1, Landroid/media/MediaExtractor;

    invoke-direct {v1}, Landroid/media/MediaExtractor;-><init>()V

    .line 323
    .local v1, "extractor":Landroid/media/MediaExtractor;
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/Element;->getFilePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/media/MediaExtractor;->setDataSource(Ljava/lang/String;)V

    .line 324
    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Landroid/media/MediaExtractor;->getTrackFormat(I)Landroid/media/MediaFormat;

    move-result-object v2

    .line 325
    .local v2, "format":Landroid/media/MediaFormat;
    const-string v4, "frame-rate"

    invoke-virtual {v2, v4}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    .line 331
    .end local v1    # "extractor":Landroid/media/MediaExtractor;
    .end local v2    # "format":Landroid/media/MediaFormat;
    :cond_0
    :goto_0
    int-to-float v4, v3

    return v4

    .line 327
    :catch_0
    move-exception v0

    .line 328
    .local v0, "e":Ljava/lang/Exception;
    const-string v4, "Abhishek"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "excep:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static getFreeStorage()J
    .locals 8

    .prologue
    .line 707
    new-instance v2, Landroid/os/StatFs;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 708
    .local v2, "stat":Landroid/os/StatFs;
    invoke-virtual {v2}, Landroid/os/StatFs;->getBlockSize()I

    move-result v3

    int-to-long v4, v3

    invoke-virtual {v2}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v3

    int-to-long v6, v3

    mul-long v0, v4, v6

    .line 711
    .local v0, "bytesAvailable":J
    const-wide/32 v4, 0x100000

    div-long v4, v0, v4

    return-wide v4
.end method

.method private static getLengthFilterWithToast(II)Landroid/text/InputFilter;
    .locals 1
    .param p0, "length"    # I
    .param p1, "stringResource"    # I

    .prologue
    .line 866
    new-instance v0, Lcom/sec/android/app/ve/util/CommonUtils$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/ve/util/CommonUtils$3;-><init>(I)V

    .line 890
    .local v0, "inputFilter":Landroid/text/InputFilter;
    return-object v0
.end method

.method public static getNextTransitionForAutoEditGroup()I
    .locals 2

    .prologue
    .line 667
    sget v0, Lcom/sec/android/app/ve/util/CommonUtils;->currentSelection:I

    if-nez v0, :cond_0

    .line 668
    const/4 v0, 0x1

    sput v0, Lcom/sec/android/app/ve/util/CommonUtils;->index:I

    .line 669
    :cond_0
    sget v0, Lcom/sec/android/app/ve/util/CommonUtils;->currentSelection:I

    sget-object v1, Lcom/sec/android/app/ve/util/CommonUtils;->transForAutoEditMedia:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_1

    .line 670
    sget v0, Lcom/sec/android/app/ve/util/CommonUtils;->currentSelection:I

    sget v1, Lcom/sec/android/app/ve/util/CommonUtils;->index:I

    add-int/2addr v0, v1

    sput v0, Lcom/sec/android/app/ve/util/CommonUtils;->currentSelection:I

    .line 671
    sget-object v0, Lcom/sec/android/app/ve/util/CommonUtils;->transForAutoEditMedia:[I

    sget v1, Lcom/sec/android/app/ve/util/CommonUtils;->currentSelection:I

    aget v0, v0, v1

    .line 677
    :goto_0
    return v0

    .line 672
    :cond_1
    sget v0, Lcom/sec/android/app/ve/util/CommonUtils;->currentSelection:I

    sget-object v1, Lcom/sec/android/app/ve/util/CommonUtils;->transForAutoEditMedia:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_2

    .line 673
    const/4 v0, -0x1

    sput v0, Lcom/sec/android/app/ve/util/CommonUtils;->index:I

    .line 674
    sget v0, Lcom/sec/android/app/ve/util/CommonUtils;->currentSelection:I

    sget v1, Lcom/sec/android/app/ve/util/CommonUtils;->index:I

    add-int/2addr v0, v1

    sput v0, Lcom/sec/android/app/ve/util/CommonUtils;->currentSelection:I

    .line 675
    sget-object v0, Lcom/sec/android/app/ve/util/CommonUtils;->transForAutoEditMedia:[I

    sget v1, Lcom/sec/android/app/ve/util/CommonUtils;->currentSelection:I

    aget v0, v0, v1

    goto :goto_0

    .line 677
    :cond_2
    sget-object v0, Lcom/sec/android/app/ve/util/CommonUtils;->transForAutoEditMedia:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    goto :goto_0
.end method

.method public static getPattern()Ljava/util/regex/Pattern;
    .locals 8

    .prologue
    .line 1546
    const/4 v0, 0x0

    .line 1547
    .local v0, "EMOJI_PATTERN":Ljava/util/regex/Pattern;
    const/4 v3, 0x0

    .line 1548
    .local v3, "patternMade":Z
    if-nez v3, :cond_0

    .line 1549
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/sec/android/app/ve/util/EmojiList;->getUnicodeList()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    mul-int/lit8 v5, v5, 0x2

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 1551
    .local v1, "emojiPattern":Ljava/lang/StringBuilder;
    const/16 v5, 0x28

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1553
    invoke-static {}, Lcom/sec/android/app/ve/util/EmojiList;->getUnicodeList()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "itr":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_1

    .line 1558
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v6

    const-string v7, ")"

    invoke-virtual {v1, v5, v6, v7}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 1560
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    .line 1561
    const/4 v3, 0x1

    .line 1563
    .end local v1    # "emojiPattern":Ljava/lang/StringBuilder;
    .end local v2    # "itr":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_0
    return-object v0

    .line 1554
    .restart local v1    # "emojiPattern":Ljava/lang/StringBuilder;
    .restart local v2    # "itr":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 1555
    .local v4, "str":Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/app/ve/util/CommonUtils;->unicodeToUTF16(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/util/regex/Pattern;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1556
    const/16 v5, 0x7c

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public static getPrivateStoragePath(Landroid/content/Context;)Ljava/lang/String;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1138
    const-string v2, ""

    .line 1140
    .local v2, "privatePath":Ljava/lang/String;
    :try_start_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {p0}, Lcom/samsung/android/secretmode/SecretModeManager;->getPersonalPageRoot(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v4, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 1148
    :goto_0
    return-object v2

    .line 1142
    :catch_0
    move-exception v0

    .line 1143
    .local v0, "noClassDefError":Ljava/lang/NoClassDefFoundError;
    invoke-virtual {v0}, Ljava/lang/NoClassDefFoundError;->printStackTrace()V

    goto :goto_0

    .line 1145
    .end local v0    # "noClassDefError":Ljava/lang/NoClassDefFoundError;
    :catch_1
    move-exception v1

    .line 1146
    .local v1, "noMethodError":Ljava/lang/NoSuchMethodError;
    invoke-virtual {v1}, Ljava/lang/NoSuchMethodError;->printStackTrace()V

    goto :goto_0
.end method

.method public static getRecordedMode(Lcom/samsung/app/video/editor/external/Element;)I
    .locals 5
    .param p0, "element"    # Lcom/samsung/app/video/editor/external/Element;

    .prologue
    .line 335
    const/4 v1, 0x1

    .line 336
    .local v1, "mode":I
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/Element;->getType()I

    move-result v3

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/Element;->getType()I

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    move v2, v1

    .line 349
    .end local v1    # "mode":I
    .local v2, "mode":I
    :goto_0
    return v2

    .line 339
    .end local v2    # "mode":I
    .restart local v1    # "mode":I
    :cond_1
    invoke-static {p0}, Lcom/sec/android/app/ve/util/CommonUtils;->getFrameRateOfVideo(Lcom/samsung/app/video/editor/external/Element;)F

    move-result v0

    .line 343
    .local v0, "frameRate":F
    const/high16 v3, 0x41f00000    # 30.0f

    cmpl-float v3, v0, v3

    if-nez v3, :cond_3

    .line 344
    const/4 v1, 0x1

    :cond_2
    :goto_1
    move v2, v1

    .line 349
    .end local v1    # "mode":I
    .restart local v2    # "mode":I
    goto :goto_0

    .line 346
    .end local v2    # "mode":I
    .restart local v1    # "mode":I
    :cond_3
    const/high16 v3, 0x425c0000    # 55.0f

    cmpl-float v3, v0, v3

    if-ltz v3, :cond_2

    .line 347
    const/4 v1, 0x3

    goto :goto_1
.end method

.method public static getRetrieverForSource(Landroid/content/res/AssetManager;Ljava/lang/String;Z)Landroid/media/MediaMetadataRetriever;
    .locals 11
    .param p0, "am"    # Landroid/content/res/AssetManager;
    .param p1, "Uri"    # Ljava/lang/String;
    .param p2, "isAssetResource"    # Z

    .prologue
    .line 140
    new-instance v0, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v0}, Landroid/media/MediaMetadataRetriever;-><init>()V

    .line 141
    .local v0, "retriever":Landroid/media/MediaMetadataRetriever;
    const/4 v9, 0x0

    .line 142
    .local v9, "fis":Ljava/io/FileInputStream;
    const/4 v6, 0x0

    .line 144
    .local v6, "afd":Landroid/content/res/AssetFileDescriptor;
    if-eqz p2, :cond_2

    if-eqz p0, :cond_2

    .line 146
    :try_start_0
    invoke-virtual {p0, p1}, Landroid/content/res/AssetManager;->openFd(Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;

    move-result-object v6

    .line 147
    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v1

    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->getStartOffset()J

    move-result-wide v2

    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->getLength()J

    move-result-wide v4

    invoke-virtual/range {v0 .. v5}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/io/FileDescriptor;JJ)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_6
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_9
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_c
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 168
    :goto_0
    if-eqz v9, :cond_0

    .line 170
    :try_start_1
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_11

    .line 175
    :cond_0
    :goto_1
    if-eqz v6, :cond_1

    .line 177
    :try_start_2
    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_12

    .line 185
    :cond_1
    :goto_2
    return-object v0

    .line 151
    :cond_2
    :try_start_3
    new-instance v10, Ljava/io/FileInputStream;

    invoke-direct {v10, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_6
    .catch Ljava/lang/IllegalStateException; {:try_start_3 .. :try_end_3} :catch_9
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_c
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 152
    .end local v9    # "fis":Ljava/io/FileInputStream;
    .local v10, "fis":Ljava/io/FileInputStream;
    :try_start_4
    invoke-virtual {v10}, Ljava/io/FileInputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/io/FileDescriptor;)V
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_17
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_16
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_15
    .catch Ljava/lang/IllegalStateException; {:try_start_4 .. :try_end_4} :catch_14
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_13
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-object v9, v10

    .line 155
    .end local v10    # "fis":Ljava/io/FileInputStream;
    .restart local v9    # "fis":Ljava/io/FileInputStream;
    goto :goto_0

    :catch_0
    move-exception v7

    .line 156
    .local v7, "e":Ljava/io/FileNotFoundException;
    :goto_3
    :try_start_5
    invoke-virtual {v7}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 168
    if-eqz v9, :cond_3

    .line 170
    :try_start_6
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    .line 175
    .end local v7    # "e":Ljava/io/FileNotFoundException;
    :cond_3
    :goto_4
    if-eqz v6, :cond_1

    .line 177
    :try_start_7
    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1

    goto :goto_2

    .line 178
    :catch_1
    move-exception v7

    .line 179
    .local v7, "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 171
    .local v7, "e":Ljava/io/FileNotFoundException;
    :catch_2
    move-exception v7

    .line 172
    .local v7, "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 157
    .end local v7    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v7

    .line 158
    .restart local v7    # "e":Ljava/io/IOException;
    :goto_5
    :try_start_8
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 168
    if-eqz v9, :cond_4

    .line 170
    :try_start_9
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_5

    .line 175
    :cond_4
    :goto_6
    if-eqz v6, :cond_1

    .line 177
    :try_start_a
    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_4

    goto :goto_2

    .line 178
    :catch_4
    move-exception v7

    .line 179
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 171
    :catch_5
    move-exception v7

    .line 172
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 159
    .end local v7    # "e":Ljava/io/IOException;
    :catch_6
    move-exception v7

    .line 160
    .local v7, "e":Ljava/lang/IllegalArgumentException;
    :goto_7
    :try_start_b
    invoke-virtual {v7}, Ljava/lang/IllegalArgumentException;->printStackTrace()V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 168
    if-eqz v9, :cond_5

    .line 170
    :try_start_c
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_8

    .line 175
    .end local v7    # "e":Ljava/lang/IllegalArgumentException;
    :cond_5
    :goto_8
    if-eqz v6, :cond_1

    .line 177
    :try_start_d
    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_7

    goto :goto_2

    .line 178
    :catch_7
    move-exception v7

    .line 179
    .local v7, "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 171
    .local v7, "e":Ljava/lang/IllegalArgumentException;
    :catch_8
    move-exception v7

    .line 172
    .local v7, "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_8

    .line 161
    .end local v7    # "e":Ljava/io/IOException;
    :catch_9
    move-exception v8

    .line 162
    .local v8, "ex":Ljava/lang/IllegalStateException;
    :goto_9
    :try_start_e
    invoke-virtual {v8}, Ljava/lang/IllegalStateException;->printStackTrace()V
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    .line 168
    if-eqz v9, :cond_6

    .line 170
    :try_start_f
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_b

    .line 175
    :cond_6
    :goto_a
    if-eqz v6, :cond_1

    .line 177
    :try_start_10
    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_a

    goto :goto_2

    .line 178
    :catch_a
    move-exception v7

    .line 179
    .restart local v7    # "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 171
    .end local v7    # "e":Ljava/io/IOException;
    :catch_b
    move-exception v7

    .line 172
    .restart local v7    # "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_a

    .line 163
    .end local v7    # "e":Ljava/io/IOException;
    .end local v8    # "ex":Ljava/lang/IllegalStateException;
    :catch_c
    move-exception v7

    .line 164
    .local v7, "e":Ljava/lang/Exception;
    :goto_b
    :try_start_11
    invoke-static {v0}, Lcom/sec/android/app/ve/util/CommonUtils;->releaseRetriever(Landroid/media/MediaMetadataRetriever;)V

    .line 165
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_0

    .line 168
    if-eqz v9, :cond_7

    .line 170
    :try_start_12
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V
    :try_end_12
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_12} :catch_e

    .line 175
    .end local v7    # "e":Ljava/lang/Exception;
    :cond_7
    :goto_c
    if-eqz v6, :cond_1

    .line 177
    :try_start_13
    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_13
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_13} :catch_d

    goto/16 :goto_2

    .line 178
    :catch_d
    move-exception v7

    .line 179
    .local v7, "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_2

    .line 171
    .local v7, "e":Ljava/lang/Exception;
    :catch_e
    move-exception v7

    .line 172
    .local v7, "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_c

    .line 167
    .end local v7    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v1

    .line 168
    :goto_d
    if-eqz v9, :cond_8

    .line 170
    :try_start_14
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V
    :try_end_14
    .catch Ljava/io/IOException; {:try_start_14 .. :try_end_14} :catch_f

    .line 175
    :cond_8
    :goto_e
    if-eqz v6, :cond_9

    .line 177
    :try_start_15
    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_15
    .catch Ljava/io/IOException; {:try_start_15 .. :try_end_15} :catch_10

    .line 183
    :cond_9
    :goto_f
    throw v1

    .line 171
    :catch_f
    move-exception v7

    .line 172
    .restart local v7    # "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_e

    .line 178
    .end local v7    # "e":Ljava/io/IOException;
    :catch_10
    move-exception v7

    .line 179
    .restart local v7    # "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_f

    .line 171
    .end local v7    # "e":Ljava/io/IOException;
    :catch_11
    move-exception v7

    .line 172
    .restart local v7    # "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_1

    .line 178
    .end local v7    # "e":Ljava/io/IOException;
    :catch_12
    move-exception v7

    .line 179
    .restart local v7    # "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_2

    .line 167
    .end local v7    # "e":Ljava/io/IOException;
    .end local v9    # "fis":Ljava/io/FileInputStream;
    .restart local v10    # "fis":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v1

    move-object v9, v10

    .end local v10    # "fis":Ljava/io/FileInputStream;
    .restart local v9    # "fis":Ljava/io/FileInputStream;
    goto :goto_d

    .line 163
    .end local v9    # "fis":Ljava/io/FileInputStream;
    .restart local v10    # "fis":Ljava/io/FileInputStream;
    :catch_13
    move-exception v7

    move-object v9, v10

    .end local v10    # "fis":Ljava/io/FileInputStream;
    .restart local v9    # "fis":Ljava/io/FileInputStream;
    goto :goto_b

    .line 161
    .end local v9    # "fis":Ljava/io/FileInputStream;
    .restart local v10    # "fis":Ljava/io/FileInputStream;
    :catch_14
    move-exception v8

    move-object v9, v10

    .end local v10    # "fis":Ljava/io/FileInputStream;
    .restart local v9    # "fis":Ljava/io/FileInputStream;
    goto :goto_9

    .line 159
    .end local v9    # "fis":Ljava/io/FileInputStream;
    .restart local v10    # "fis":Ljava/io/FileInputStream;
    :catch_15
    move-exception v7

    move-object v9, v10

    .end local v10    # "fis":Ljava/io/FileInputStream;
    .restart local v9    # "fis":Ljava/io/FileInputStream;
    goto :goto_7

    .line 157
    .end local v9    # "fis":Ljava/io/FileInputStream;
    .restart local v10    # "fis":Ljava/io/FileInputStream;
    :catch_16
    move-exception v7

    move-object v9, v10

    .end local v10    # "fis":Ljava/io/FileInputStream;
    .restart local v9    # "fis":Ljava/io/FileInputStream;
    goto/16 :goto_5

    .line 155
    .end local v9    # "fis":Ljava/io/FileInputStream;
    .restart local v10    # "fis":Ljava/io/FileInputStream;
    :catch_17
    move-exception v7

    move-object v9, v10

    .end local v10    # "fis":Ljava/io/FileInputStream;
    .restart local v9    # "fis":Ljava/io/FileInputStream;
    goto/16 :goto_3
.end method

.method public static getSpeedFactor(Lcom/samsung/app/video/editor/external/Element;)F
    .locals 6
    .param p0, "elem"    # Lcom/samsung/app/video/editor/external/Element;

    .prologue
    const/4 v5, 0x2

    .line 353
    const/high16 v1, 0x3f800000    # 1.0f

    .line 355
    .local v1, "speedRate":F
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/Element;->getType()I

    move-result v3

    if-eq v3, v5, :cond_0

    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/Element;->getType()I

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    move v2, v1

    .line 388
    .end local v1    # "speedRate":F
    .local v2, "speedRate":F
    :goto_0
    return v2

    .line 359
    .end local v2    # "speedRate":F
    .restart local v1    # "speedRate":F
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/Element;->getSpeedForElement()I

    move-result v0

    .line 360
    .local v0, "speed":I
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/Element;->getRecordingMode()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_5

    .line 361
    sget v3, Lcom/samsung/app/video/editor/external/Constants$SLOW_MOTION_SPEED;->SPEED_NORMAL:I

    if-ne v0, v3, :cond_3

    .line 362
    const/high16 v1, 0x3f800000    # 1.0f

    :cond_2
    :goto_1
    move v2, v1

    .line 388
    .end local v1    # "speedRate":F
    .restart local v2    # "speedRate":F
    goto :goto_0

    .line 363
    .end local v2    # "speedRate":F
    .restart local v1    # "speedRate":F
    :cond_3
    sget v3, Lcom/samsung/app/video/editor/external/Constants$SLOW_MOTION_SPEED;->SPEED_X_1_BY_2:I

    if-ne v0, v3, :cond_4

    .line 364
    const/high16 v1, 0x40000000    # 2.0f

    goto :goto_1

    .line 365
    :cond_4
    sget v3, Lcom/samsung/app/video/editor/external/Constants$SLOW_MOTION_SPEED;->SPEED_X2:I

    if-ne v0, v3, :cond_2

    .line 366
    const/high16 v1, 0x3f000000    # 0.5f

    .line 367
    goto :goto_1

    :cond_5
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/Element;->getRecordingMode()I

    move-result v3

    if-ne v3, v5, :cond_a

    .line 368
    sget v3, Lcom/samsung/app/video/editor/external/Constants$SLOW_MOTION_SPEED;->SPEED_NORMAL:I

    if-ne v0, v3, :cond_7

    .line 369
    const/high16 v1, 0x3e800000    # 0.25f

    .line 376
    :cond_6
    :goto_2
    invoke-static {p0}, Lcom/sec/android/app/ve/util/CommonUtils;->getFrameRateOfVideo(Lcom/samsung/app/video/editor/external/Element;)F

    move-result v3

    const/high16 v4, 0x41f00000    # 30.0f

    div-float/2addr v3, v4

    mul-float/2addr v1, v3

    .line 377
    goto :goto_1

    .line 370
    :cond_7
    sget v3, Lcom/samsung/app/video/editor/external/Constants$SLOW_MOTION_SPEED;->SPEED_X_1_BY_2:I

    if-ne v0, v3, :cond_8

    .line 371
    const/high16 v1, 0x3f000000    # 0.5f

    goto :goto_2

    .line 372
    :cond_8
    sget v3, Lcom/samsung/app/video/editor/external/Constants$SLOW_MOTION_SPEED;->SPEED_X_1_BY_4:I

    if-ne v0, v3, :cond_9

    .line 373
    const/high16 v1, 0x3f800000    # 1.0f

    goto :goto_2

    .line 374
    :cond_9
    sget v3, Lcom/samsung/app/video/editor/external/Constants$SLOW_MOTION_SPEED;->SPEED_X_1_BY_8:I

    if-ne v0, v3, :cond_6

    .line 375
    const/high16 v1, 0x3e000000    # 0.125f

    goto :goto_2

    .line 377
    :cond_a
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/Element;->getRecordingMode()I

    move-result v3

    const/4 v4, 0x3

    if-ne v3, v4, :cond_2

    .line 380
    sget v3, Lcom/samsung/app/video/editor/external/Constants$SLOW_MOTION_SPEED;->SPEED_NORMAL:I

    if-ne v0, v3, :cond_b

    .line 381
    const/high16 v1, 0x3f800000    # 1.0f

    goto :goto_1

    .line 382
    :cond_b
    sget v3, Lcom/samsung/app/video/editor/external/Constants$SLOW_MOTION_SPEED;->SPEED_X_1_BY_2:I

    if-ne v0, v3, :cond_c

    .line 383
    const/high16 v1, 0x40000000    # 2.0f

    goto :goto_1

    .line 384
    :cond_c
    sget v3, Lcom/samsung/app/video/editor/external/Constants$SLOW_MOTION_SPEED;->SPEED_X_1_BY_4:I

    if-ne v0, v3, :cond_2

    .line 385
    const/high16 v1, 0x40800000    # 4.0f

    goto :goto_1
.end method

.method public static getThemeTitle(Lcom/samsung/app/video/editor/external/TranscodeElement;)Ljava/lang/String;
    .locals 8
    .param p0, "trans"    # Lcom/samsung/app/video/editor/external/TranscodeElement;

    .prologue
    .line 1206
    const-string v6, ""

    .line 1207
    .local v6, "themeTitle":Ljava/lang/String;
    if-eqz p0, :cond_0

    .line 1208
    const/4 v7, 0x0

    invoke-virtual {p0, v7}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElement(I)Lcom/samsung/app/video/editor/external/Element;

    move-result-object v3

    .line 1209
    .local v3, "firstelemElement":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {p0, v3}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getTextClipartParamsListForElement(Lcom/samsung/app/video/editor/external/Element;)Ljava/util/List;

    move-result-object v2

    .line 1211
    .local v2, "captions":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    if-eqz v2, :cond_0

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_0

    .line 1212
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v5

    .line 1214
    .local v5, "size":I
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 1215
    .local v0, "buffer":Ljava/lang/StringBuffer;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-lt v4, v5, :cond_1

    .line 1225
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1229
    .end local v0    # "buffer":Ljava/lang/StringBuffer;
    .end local v2    # "captions":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    .end local v3    # "firstelemElement":Lcom/samsung/app/video/editor/external/Element;
    .end local v4    # "i":I
    .end local v5    # "size":I
    :cond_0
    return-object v6

    .line 1216
    .restart local v0    # "buffer":Ljava/lang/StringBuffer;
    .restart local v2    # "captions":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    .restart local v3    # "firstelemElement":Lcom/samsung/app/video/editor/external/Element;
    .restart local v4    # "i":I
    .restart local v5    # "size":I
    :cond_1
    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/app/video/editor/external/ClipartParams;

    .line 1217
    .local v1, "caption":Lcom/samsung/app/video/editor/external/ClipartParams;
    iget-object v7, v1, Lcom/samsung/app/video/editor/external/ClipartParams;->data:Ljava/lang/String;

    if-eqz v7, :cond_3

    iget-object v7, v1, Lcom/samsung/app/video/editor/external/ClipartParams;->data:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    if-lez v7, :cond_3

    .line 1218
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->length()I

    move-result v7

    if-lez v7, :cond_2

    .line 1219
    const-string v7, " "

    invoke-virtual {v0, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1221
    :cond_2
    iget-object v7, v1, Lcom/samsung/app/video/editor/external/ClipartParams;->data:Ljava/lang/String;

    invoke-virtual {v0, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1215
    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method

.method public static getTimeInText(J)Ljava/lang/String;
    .locals 2
    .param p0, "time"    # J

    .prologue
    .line 756
    sget-object v0, Lcom/sec/android/app/ve/util/CommonUtils$TimeInTextUnits;->HHMMSS:Lcom/sec/android/app/ve/util/CommonUtils$TimeInTextUnits;

    invoke-static {p0, p1, v0}, Lcom/sec/android/app/ve/util/CommonUtils;->getTimeInText(JLcom/sec/android/app/ve/util/CommonUtils$TimeInTextUnits;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getTimeInText(JLcom/sec/android/app/ve/util/CommonUtils$TimeInTextUnits;)Ljava/lang/String;
    .locals 10
    .param p0, "time"    # J
    .param p2, "unit"    # Lcom/sec/android/app/ve/util/CommonUtils$TimeInTextUnits;

    .prologue
    const/4 v8, 0x0

    const-wide/16 v6, 0x3c

    .line 761
    invoke-static {}, Ljava/text/NumberFormat;->getInstance()Ljava/text/NumberFormat;

    move-result-object v0

    .line 762
    .local v0, "numForm":Ljava/text/NumberFormat;
    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Ljava/text/NumberFormat;->setMinimumIntegerDigits(I)V

    .line 764
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 767
    .local v1, "sb":Ljava/lang/StringBuilder;
    const-wide/16 v2, 0x1f4

    add-long/2addr v2, p0

    const-wide/16 v4, 0x3e8

    div-long p0, v2, v4

    .line 768
    sget-object v2, Lcom/sec/android/app/ve/util/CommonUtils$TimeInTextUnits;->SS:Lcom/sec/android/app/ve/util/CommonUtils$TimeInTextUnits;

    if-ne p2, v2, :cond_0

    .line 769
    invoke-virtual {v0, p0, p1}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 783
    :goto_0
    return-object v2

    .line 772
    :cond_0
    rem-long v2, p0, v6

    invoke-virtual {v0, v2, v3}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 773
    div-long/2addr p0, v6

    .line 774
    sget-object v2, Lcom/sec/android/app/ve/util/CommonUtils$TimeInTextUnits;->MMSS:Lcom/sec/android/app/ve/util/CommonUtils$TimeInTextUnits;

    if-ne p2, v2, :cond_1

    .line 775
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0, p1}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v8, v2}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 778
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    rem-long v4, p0, v6

    invoke-virtual {v0, v4, v5}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v8, v2}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 779
    div-long/2addr p0, v6

    .line 782
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0, p1}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v8, v2}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 783
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public static getVideoDuration(ZLandroid/content/res/AssetManager;Landroid/media/MediaMetadataRetriever;Ljava/lang/String;)I
    .locals 12
    .param p0, "isAssetResource"    # Z
    .param p1, "assetManager"    # Landroid/content/res/AssetManager;
    .param p2, "retriever"    # Landroid/media/MediaMetadataRetriever;
    .param p3, "Uri"    # Ljava/lang/String;

    .prologue
    .line 251
    const/4 v7, 0x0

    .line 252
    .local v7, "duration":I
    if-nez p2, :cond_1

    .line 253
    new-instance p2, Landroid/media/MediaMetadataRetriever;

    .end local p2    # "retriever":Landroid/media/MediaMetadataRetriever;
    invoke-direct {p2}, Landroid/media/MediaMetadataRetriever;-><init>()V

    .line 254
    .restart local p2    # "retriever":Landroid/media/MediaMetadataRetriever;
    const/4 v10, 0x0

    .line 255
    .local v10, "fis":Ljava/io/FileInputStream;
    const/4 v6, 0x0

    .line 257
    .local v6, "afd":Landroid/content/res/AssetFileDescriptor;
    if-eqz p0, :cond_2

    .line 258
    :try_start_0
    invoke-virtual {p1, p3}, Landroid/content/res/AssetManager;->openFd(Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;

    move-result-object v6

    .line 259
    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v1

    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->getStartOffset()J

    move-result-wide v2

    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->getLength()J

    move-result-wide v4

    move-object v0, p2

    invoke-virtual/range {v0 .. v5}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/io/FileDescriptor;JJ)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_6
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_9
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 275
    :goto_0
    if-eqz v10, :cond_0

    .line 277
    :try_start_1
    invoke-virtual {v10}, Ljava/io/FileInputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_e

    .line 282
    :cond_0
    :goto_1
    if-eqz v6, :cond_1

    .line 284
    :try_start_2
    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_f

    .line 291
    .end local v6    # "afd":Landroid/content/res/AssetFileDescriptor;
    .end local v10    # "fis":Ljava/io/FileInputStream;
    :cond_1
    :goto_2
    if-eqz p2, :cond_9

    .line 294
    const/16 v0, 0x9

    .line 293
    :try_start_3
    invoke-virtual {p2, v0}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    .line 295
    invoke-virtual {p2}, Landroid/media/MediaMetadataRetriever;->release()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_10

    move v8, v7

    .line 302
    .end local v7    # "duration":I
    .local v8, "duration":I
    :goto_3
    return v8

    .line 262
    .end local v8    # "duration":I
    .restart local v6    # "afd":Landroid/content/res/AssetFileDescriptor;
    .restart local v7    # "duration":I
    .restart local v10    # "fis":Ljava/io/FileInputStream;
    :cond_2
    :try_start_4
    new-instance v11, Ljava/io/FileInputStream;

    invoke-direct {v11, p3}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_6
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_9
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 263
    .end local v10    # "fis":Ljava/io/FileInputStream;
    .local v11, "fis":Ljava/io/FileInputStream;
    :try_start_5
    invoke-virtual {v11}, Ljava/io/FileInputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/io/FileDescriptor;)V
    :try_end_5
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_5} :catch_14
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_13
    .catch Ljava/lang/IllegalArgumentException; {:try_start_5 .. :try_end_5} :catch_12
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_11
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-object v10, v11

    .line 265
    .end local v11    # "fis":Ljava/io/FileInputStream;
    .restart local v10    # "fis":Ljava/io/FileInputStream;
    goto :goto_0

    :catch_0
    move-exception v9

    .line 266
    .local v9, "e":Ljava/io/FileNotFoundException;
    :goto_4
    :try_start_6
    invoke-virtual {v9}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 275
    if-eqz v10, :cond_3

    .line 277
    :try_start_7
    invoke-virtual {v10}, Ljava/io/FileInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2

    .line 282
    .end local v9    # "e":Ljava/io/FileNotFoundException;
    :cond_3
    :goto_5
    if-eqz v6, :cond_1

    .line 284
    :try_start_8
    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1

    goto :goto_2

    .line 285
    :catch_1
    move-exception v9

    .line 286
    .local v9, "e":Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 278
    .local v9, "e":Ljava/io/FileNotFoundException;
    :catch_2
    move-exception v9

    .line 279
    .local v9, "e":Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 267
    .end local v9    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v9

    .line 268
    .restart local v9    # "e":Ljava/io/IOException;
    :goto_6
    :try_start_9
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 275
    if-eqz v10, :cond_4

    .line 277
    :try_start_a
    invoke-virtual {v10}, Ljava/io/FileInputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_5

    .line 282
    :cond_4
    :goto_7
    if-eqz v6, :cond_1

    .line 284
    :try_start_b
    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_4

    goto :goto_2

    .line 285
    :catch_4
    move-exception v9

    .line 286
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 278
    :catch_5
    move-exception v9

    .line 279
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_7

    .line 269
    .end local v9    # "e":Ljava/io/IOException;
    :catch_6
    move-exception v9

    .line 270
    .local v9, "e":Ljava/lang/IllegalArgumentException;
    :goto_8
    :try_start_c
    invoke-virtual {v9}, Ljava/lang/IllegalArgumentException;->printStackTrace()V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    .line 275
    if-eqz v10, :cond_5

    .line 277
    :try_start_d
    invoke-virtual {v10}, Ljava/io/FileInputStream;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_8

    .line 282
    .end local v9    # "e":Ljava/lang/IllegalArgumentException;
    :cond_5
    :goto_9
    if-eqz v6, :cond_1

    .line 284
    :try_start_e
    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_7

    goto :goto_2

    .line 285
    :catch_7
    move-exception v9

    .line 286
    .local v9, "e":Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 278
    .local v9, "e":Ljava/lang/IllegalArgumentException;
    :catch_8
    move-exception v9

    .line 279
    .local v9, "e":Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_9

    .line 271
    .end local v9    # "e":Ljava/io/IOException;
    :catch_9
    move-exception v9

    .line 272
    .local v9, "e":Ljava/lang/Exception;
    :goto_a
    :try_start_f
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    .line 275
    if-eqz v10, :cond_6

    .line 277
    :try_start_10
    invoke-virtual {v10}, Ljava/io/FileInputStream;->close()V
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_b

    .line 282
    .end local v9    # "e":Ljava/lang/Exception;
    :cond_6
    :goto_b
    if-eqz v6, :cond_1

    .line 284
    :try_start_11
    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_11
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_a

    goto :goto_2

    .line 285
    :catch_a
    move-exception v9

    .line 286
    .local v9, "e":Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 278
    .local v9, "e":Ljava/lang/Exception;
    :catch_b
    move-exception v9

    .line 279
    .local v9, "e":Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_b

    .line 274
    .end local v9    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v0

    .line 275
    :goto_c
    if-eqz v10, :cond_7

    .line 277
    :try_start_12
    invoke-virtual {v10}, Ljava/io/FileInputStream;->close()V
    :try_end_12
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_12} :catch_c

    .line 282
    :cond_7
    :goto_d
    if-eqz v6, :cond_8

    .line 284
    :try_start_13
    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_13
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_13} :catch_d

    .line 289
    :cond_8
    :goto_e
    throw v0

    .line 278
    :catch_c
    move-exception v9

    .line 279
    .restart local v9    # "e":Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_d

    .line 285
    .end local v9    # "e":Ljava/io/IOException;
    :catch_d
    move-exception v9

    .line 286
    .restart local v9    # "e":Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_e

    .line 278
    .end local v9    # "e":Ljava/io/IOException;
    :catch_e
    move-exception v9

    .line 279
    .restart local v9    # "e":Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_1

    .line 285
    .end local v9    # "e":Ljava/io/IOException;
    :catch_f
    move-exception v9

    .line 286
    .restart local v9    # "e":Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_2

    .line 297
    .end local v6    # "afd":Landroid/content/res/AssetFileDescriptor;
    .end local v9    # "e":Ljava/io/IOException;
    .end local v10    # "fis":Ljava/io/FileInputStream;
    :catch_10
    move-exception v9

    .line 298
    .local v9, "e":Ljava/lang/Exception;
    invoke-static {p2}, Lcom/sec/android/app/ve/util/CommonUtils;->releaseRetriever(Landroid/media/MediaMetadataRetriever;)V

    .line 299
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V

    .end local v9    # "e":Ljava/lang/Exception;
    :cond_9
    move v8, v7

    .line 302
    .end local v7    # "duration":I
    .restart local v8    # "duration":I
    goto/16 :goto_3

    .line 274
    .end local v8    # "duration":I
    .restart local v6    # "afd":Landroid/content/res/AssetFileDescriptor;
    .restart local v7    # "duration":I
    .restart local v11    # "fis":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v0

    move-object v10, v11

    .end local v11    # "fis":Ljava/io/FileInputStream;
    .restart local v10    # "fis":Ljava/io/FileInputStream;
    goto :goto_c

    .line 271
    .end local v10    # "fis":Ljava/io/FileInputStream;
    .restart local v11    # "fis":Ljava/io/FileInputStream;
    :catch_11
    move-exception v9

    move-object v10, v11

    .end local v11    # "fis":Ljava/io/FileInputStream;
    .restart local v10    # "fis":Ljava/io/FileInputStream;
    goto :goto_a

    .line 269
    .end local v10    # "fis":Ljava/io/FileInputStream;
    .restart local v11    # "fis":Ljava/io/FileInputStream;
    :catch_12
    move-exception v9

    move-object v10, v11

    .end local v11    # "fis":Ljava/io/FileInputStream;
    .restart local v10    # "fis":Ljava/io/FileInputStream;
    goto :goto_8

    .line 267
    .end local v10    # "fis":Ljava/io/FileInputStream;
    .restart local v11    # "fis":Ljava/io/FileInputStream;
    :catch_13
    move-exception v9

    move-object v10, v11

    .end local v11    # "fis":Ljava/io/FileInputStream;
    .restart local v10    # "fis":Ljava/io/FileInputStream;
    goto/16 :goto_6

    .line 265
    .end local v10    # "fis":Ljava/io/FileInputStream;
    .restart local v11    # "fis":Ljava/io/FileInputStream;
    :catch_14
    move-exception v9

    move-object v10, v11

    .end local v11    # "fis":Ljava/io/FileInputStream;
    .restart local v10    # "fis":Ljava/io/FileInputStream;
    goto/16 :goto_4
.end method

.method public static hasEmojiString(Ljava/lang/CharSequence;)Z
    .locals 4
    .param p0, "chars"    # Ljava/lang/CharSequence;

    .prologue
    const/4 v3, 0x0

    .line 1533
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/ve/util/CommonUtils;->getPattern()Ljava/util/regex/Pattern;

    move-result-object v2

    .line 1534
    .local v2, "ptn":Ljava/util/regex/Pattern;
    if-eqz v2, :cond_0

    .line 1535
    invoke-virtual {v2, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 1536
    .local v1, "match":Ljava/util/regex/Matcher;
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    .line 1541
    .end local v1    # "match":Ljava/util/regex/Matcher;
    .end local v2    # "ptn":Ljava/util/regex/Pattern;
    :cond_0
    :goto_0
    return v3

    .line 1540
    :catch_0
    move-exception v0

    .line 1541
    .local v0, "e":Ljava/lang/Exception;
    goto :goto_0
.end method

.method public static init()V
    .locals 0

    .prologue
    .line 136
    return-void
.end method

.method private static isElementFileExists(Lcom/samsung/app/video/editor/external/Element;Landroid/content/res/AssetManager;)Z
    .locals 5
    .param p0, "element"    # Lcom/samsung/app/video/editor/external/Element;
    .param p1, "assetMgr"    # Landroid/content/res/AssetManager;

    .prologue
    .line 1183
    const/4 v3, 0x1

    .line 1184
    .local v3, "fileExists":Z
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/Element;->isAssetResource()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1186
    if-eqz p1, :cond_0

    .line 1187
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/Element;->getFilePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Landroid/content/res/AssetManager;->openFd(Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;

    move-result-object v0

    .line 1188
    .local v0, "afd":Landroid/content/res/AssetFileDescriptor;
    invoke-virtual {v0}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1202
    .end local v0    # "afd":Landroid/content/res/AssetFileDescriptor;
    :cond_0
    :goto_0
    return v3

    .line 1191
    :catch_0
    move-exception v1

    .line 1192
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 1193
    const/4 v3, 0x0

    .line 1195
    goto :goto_0

    .line 1197
    .end local v1    # "e":Ljava/io/IOException;
    :cond_1
    new-instance v2, Ljava/io/File;

    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/Element;->getFilePath()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1198
    .local v2, "elefile":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v2}, Ljava/io/File;->isFile()Z

    move-result v4

    if-nez v4, :cond_0

    .line 1199
    :cond_2
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public static isExportServiceRunning(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "servicePackage"    # Ljava/lang/String;

    .prologue
    const v3, 0x7fffffff

    .line 1041
    const-string v2, "activity"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 1042
    .local v0, "manager":Landroid/app/ActivityManager;
    if-eqz v0, :cond_1

    invoke-virtual {v0, v3}, Landroid/app/ActivityManager;->getRunningServices(I)Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 1043
    invoke-virtual {v0, v3}, Landroid/app/ActivityManager;->getRunningServices(I)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_2

    .line 1049
    :cond_1
    const/4 v2, 0x0

    :goto_0
    return v2

    .line 1043
    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager$RunningServiceInfo;

    .line 1044
    .local v1, "service":Landroid/app/ActivityManager$RunningServiceInfo;
    iget-object v3, v1, Landroid/app/ActivityManager$RunningServiceInfo;->process:Ljava/lang/String;

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1045
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public static isExportSupported(IF)Z
    .locals 8
    .param p0, "bitRate"    # I
    .param p1, "totalMovieDuration"    # F

    .prologue
    const/4 v1, 0x0

    const/high16 v4, 0x447a0000    # 1000.0f

    .line 683
    div-float v2, p1, v4

    int-to-float v3, p0

    mul-float/2addr v2, v3

    div-float v3, p1, v4

    const/high16 v4, 0x42800000    # 64.0f

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    const/high16 v3, 0x46000000    # 8192.0f

    div-float v0, v2, v3

    .line 686
    .local v0, "expectedOutputSize":F
    float-to-double v2, v0

    const-wide v4, 0x3ff199999999999aL    # 1.1

    mul-double/2addr v2, v4

    double-to-float v0, v2

    .line 687
    sput v0, Lcom/sec/android/app/ve/util/CommonUtils;->mexportFileSize:F

    .line 689
    const/high16 v2, 0x45000000    # 2048.0f

    cmpl-float v2, v0, v2

    if-lez v2, :cond_1

    .line 702
    :cond_0
    :goto_0
    return v1

    .line 697
    :cond_1
    invoke-static {}, Lcom/sec/android/app/ve/util/CommonUtils;->getFreeStorage()J

    move-result-wide v2

    long-to-double v2, v2

    float-to-double v4, v0

    const-wide/high16 v6, 0x4004000000000000L    # 2.5

    mul-double/2addr v4, v6

    cmpg-double v2, v2, v4

    if-ltz v2, :cond_0

    .line 702
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static isExternalKeyboardPresent(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 935
    if-eqz p0, :cond_0

    sget-object v2, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    const-string v3, "4.1"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 936
    const-string v2, "input"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/input/InputManager;

    .line 937
    .local v0, "inputMgr":Landroid/hardware/input/InputManager;
    invoke-virtual {v0}, Landroid/hardware/input/InputManager;->getInputDeviceIds()[I

    move-result-object v2

    array-length v2, v2

    const/4 v3, 0x6

    if-le v2, v3, :cond_0

    .line 939
    const/4 v1, 0x1

    .line 944
    .end local v0    # "inputMgr":Landroid/hardware/input/InputManager;
    :cond_0
    return v1
.end method

.method public static isFileFromVOS(Ljava/lang/String;)Z
    .locals 3
    .param p0, "aFilePath"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 593
    if-nez p0, :cond_1

    .line 605
    :cond_0
    :goto_0
    return v1

    .line 597
    :cond_1
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v0

    .line 599
    .local v0, "lSDPath":Ljava/lang/String;
    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "/storage/extSdCard"

    invoke-virtual {p0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 600
    const-string v2, "/mnt/extSdCard"

    invoke-virtual {p0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 602
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static isPackageEnabled(Landroid/app/Activity;Ljava/lang/String;)Z
    .locals 5
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 1417
    invoke-virtual {p0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 1419
    .local v2, "pm":Landroid/content/pm/PackageManager;
    if-eqz v2, :cond_0

    .line 1420
    const/16 v4, 0x80

    :try_start_0
    invoke-virtual {v2, p1, v4}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    .line 1422
    .local v0, "ai":Landroid/content/pm/ApplicationInfo;
    iget-boolean v3, v0, Landroid/content/pm/ApplicationInfo;->enabled:Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1428
    .end local v0    # "ai":Landroid/content/pm/ApplicationInfo;
    :cond_0
    :goto_0
    return v3

    .line 1425
    :catch_0
    move-exception v1

    .line 1426
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    goto :goto_0
.end method

.method public static isPackageInstalled(Landroid/app/Activity;Ljava/lang/String;)Z
    .locals 3
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 1404
    invoke-virtual {p0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 1406
    .local v1, "pm":Landroid/content/pm/PackageManager;
    if-eqz v1, :cond_0

    .line 1407
    const/16 v2, 0x80

    :try_start_0
    invoke-virtual {v1, p1, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1411
    :cond_0
    const/4 v2, 0x1

    :goto_0
    return v2

    .line 1408
    :catch_0
    move-exception v0

    .line 1409
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static isPrivateModeContent(Ljava/lang/String;Landroid/content/Context;)Z
    .locals 5
    .param p0, "filePath"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 1233
    if-eqz p0, :cond_0

    .line 1235
    :try_start_0
    invoke-static {p1}, Lcom/samsung/android/secretmode/SecretModeManager;->getPersonalPageRoot(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 1236
    .local v2, "privateFolderRoot":Ljava/lang/String;
    if-eqz p0, :cond_0

    invoke-virtual {p0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_1

    move-result v4

    if-eqz v4, :cond_0

    .line 1237
    const/4 v3, 0x1

    .line 1249
    .end local v2    # "privateFolderRoot":Ljava/lang/String;
    :cond_0
    :goto_0
    return v3

    .line 1240
    :catch_0
    move-exception v0

    .line 1241
    .local v0, "noClassDefError":Ljava/lang/NoClassDefFoundError;
    invoke-virtual {v0}, Ljava/lang/NoClassDefFoundError;->printStackTrace()V

    goto :goto_0

    .line 1244
    .end local v0    # "noClassDefError":Ljava/lang/NoClassDefFoundError;
    :catch_1
    move-exception v1

    .line 1245
    .local v1, "noMethodError":Ljava/lang/NoSuchMethodError;
    invoke-virtual {v1}, Ljava/lang/NoSuchMethodError;->printStackTrace()V

    goto :goto_0
.end method

.method public static isPrivateModeON(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1123
    const/4 v0, 0x0

    .line 1126
    .local v0, "isPrivateMode":Z
    :try_start_0
    invoke-static {p0}, Lcom/samsung/android/secretmode/SecretModeManager;->isPersonalPageMounted(Landroid/content/Context;)Z
    :try_end_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_1

    move-result v0

    .line 1134
    :goto_0
    return v0

    .line 1128
    :catch_0
    move-exception v1

    .line 1129
    .local v1, "noClassDefError":Ljava/lang/NoClassDefFoundError;
    invoke-virtual {v1}, Ljava/lang/NoClassDefFoundError;->printStackTrace()V

    goto :goto_0

    .line 1131
    .end local v1    # "noClassDefError":Ljava/lang/NoClassDefFoundError;
    :catch_1
    move-exception v2

    .line 1132
    .local v2, "noMethodError":Ljava/lang/NoSuchMethodError;
    invoke-virtual {v2}, Ljava/lang/NoSuchMethodError;->printStackTrace()V

    goto :goto_0
.end method

.method public static isProjectNameDuplicate(Ljava/util/List;Ljava/lang/String;)Z
    .locals 5
    .param p1, "newProjectName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/TranscodeElement;",
            ">;",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 1253
    .local p0, "projectList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/TranscodeElement;>;"
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 1254
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v2

    .line 1255
    .local v2, "size":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, v2, :cond_1

    .line 1262
    .end local v0    # "i":I
    .end local v2    # "size":I
    :cond_0
    const/4 v4, 0x0

    :goto_1
    return v4

    .line 1256
    .restart local v0    # "i":I
    .restart local v2    # "size":I
    :cond_1
    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/app/video/editor/external/TranscodeElement;

    .line 1257
    .local v3, "tElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getProjectName()Ljava/lang/String;

    move-result-object v1

    .line 1258
    .local v1, "name":Ljava/lang/String;
    if-eqz v1, :cond_2

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1259
    const/4 v4, 0x1

    goto :goto_1

    .line 1255
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static isServiceRunning(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "servicePackage"    # Ljava/lang/String;

    .prologue
    const v3, 0x7fffffff

    .line 1054
    const-string v2, "activity"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 1055
    .local v0, "manager":Landroid/app/ActivityManager;
    if-eqz v0, :cond_1

    invoke-virtual {v0, v3}, Landroid/app/ActivityManager;->getRunningServices(I)Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 1056
    invoke-virtual {v0, v3}, Landroid/app/ActivityManager;->getRunningServices(I)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_2

    .line 1062
    :cond_1
    const/4 v2, 0x0

    :goto_0
    return v2

    .line 1056
    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager$RunningServiceInfo;

    .line 1057
    .local v1, "service":Landroid/app/ActivityManager$RunningServiceInfo;
    iget-object v3, v1, Landroid/app/ActivityManager$RunningServiceInfo;->service:Landroid/content/ComponentName;

    invoke-virtual {v3}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1058
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public static isStorageSizeAvailable(Ljava/lang/String;J)Z
    .locals 9
    .param p0, "dirPath"    # Ljava/lang/String;
    .param p1, "storageSizeRequiredBytes"    # J

    .prologue
    .line 499
    new-instance v2, Landroid/os/StatFs;

    invoke-direct {v2, p0}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 501
    .local v2, "stat":Landroid/os/StatFs;
    const-wide/16 v0, 0x0

    .line 503
    .local v0, "bytesAvailable":J
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x12

    if-lt v3, v4, :cond_0

    .line 504
    invoke-virtual {v2}, Landroid/os/StatFs;->getBlockSizeLong()J

    move-result-wide v4

    invoke-virtual {v2}, Landroid/os/StatFs;->getAvailableBlocksLong()J

    move-result-wide v6

    mul-long v0, v4, v6

    .line 509
    :goto_0
    cmp-long v3, v0, p1

    if-lez v3, :cond_1

    .line 510
    const/4 v3, 0x1

    .line 513
    :goto_1
    return v3

    .line 507
    :cond_0
    invoke-virtual {v2}, Landroid/os/StatFs;->getBlockSize()I

    move-result v3

    int-to-long v4, v3

    invoke-virtual {v2}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v3

    int-to-long v6, v3

    mul-long v0, v4, v6

    goto :goto_0

    .line 513
    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public static isUHD(Landroid/media/MediaMetadataRetriever;)Z
    .locals 4
    .param p0, "retriever"    # Landroid/media/MediaMetadataRetriever;

    .prologue
    const/4 v2, 0x0

    .line 190
    .line 191
    const/16 v3, 0x12

    .line 190
    :try_start_0
    invoke-virtual {p0, v3}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 192
    .local v1, "width":I
    const/16 v3, 0xa00

    if-le v1, v3, :cond_0

    .line 193
    const/4 v2, 0x1

    .line 199
    .end local v1    # "width":I
    :cond_0
    :goto_0
    return v2

    .line 196
    :catch_0
    move-exception v0

    .line 197
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static isUHD(Ljava/lang/String;)Z
    .locals 5
    .param p0, "filepath"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 217
    new-instance v1, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v1}, Landroid/media/MediaMetadataRetriever;-><init>()V

    .line 219
    .local v1, "retriever":Landroid/media/MediaMetadataRetriever;
    :try_start_0
    invoke-virtual {v1, p0}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/lang/String;)V

    .line 220
    const/16 v4, 0x12

    invoke-virtual {v1, v4}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    .line 221
    .local v2, "width":I
    const/16 v4, 0xa00

    if-le v2, v4, :cond_0

    .line 228
    invoke-static {v1}, Lcom/sec/android/app/ve/common/MediaUtils;->releaseRetriever(Landroid/media/MediaMetadataRetriever;)V

    .line 222
    const/4 v3, 0x1

    .line 230
    .end local v2    # "width":I
    :goto_0
    return v3

    .line 228
    .restart local v2    # "width":I
    :cond_0
    invoke-static {v1}, Lcom/sec/android/app/ve/common/MediaUtils;->releaseRetriever(Landroid/media/MediaMetadataRetriever;)V

    goto :goto_0

    .line 225
    .end local v2    # "width":I
    :catch_0
    move-exception v0

    .line 226
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 228
    invoke-static {v1}, Lcom/sec/android/app/ve/common/MediaUtils;->releaseRetriever(Landroid/media/MediaMetadataRetriever;)V

    goto :goto_0

    .line 227
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    .line 228
    invoke-static {v1}, Lcom/sec/android/app/ve/common/MediaUtils;->releaseRetriever(Landroid/media/MediaMetadataRetriever;)V

    .line 229
    throw v3
.end method

.method public static isUserEventTooSoonAfterPrev()Z
    .locals 6

    .prologue
    .line 717
    const/4 v1, 0x0

    .line 718
    .local v1, "res":Z
    const/16 v0, 0x7d1

    .line 720
    .local v0, "TOO_EVENT_EVENT_MSG":I
    sget-object v2, Lcom/sec/android/app/ve/util/CommonUtils;->mTooSoonEventHandler:Landroid/os/Handler;

    invoke-virtual {v2, v0}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 722
    sget-object v2, Lcom/sec/android/app/ve/util/CommonUtils;->mTooSoonEventHandler:Landroid/os/Handler;

    invoke-virtual {v2, v0}, Landroid/os/Handler;->removeMessages(I)V

    .line 723
    const/4 v1, 0x1

    .line 726
    :cond_0
    sget-object v2, Lcom/sec/android/app/ve/util/CommonUtils;->mTooSoonEventHandler:Landroid/os/Handler;

    const-wide/16 v4, 0x1f4

    invoke-virtual {v2, v0, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 728
    return v1
.end method

.method public static isWQHD(Landroid/media/MediaMetadataRetriever;)Z
    .locals 4
    .param p0, "retriever"    # Landroid/media/MediaMetadataRetriever;

    .prologue
    const/4 v2, 0x0

    .line 204
    .line 205
    const/16 v3, 0x12

    .line 204
    :try_start_0
    invoke-virtual {p0, v3}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 206
    .local v1, "width":I
    const/16 v3, 0x780

    if-le v1, v3, :cond_0

    const/16 v3, 0xa00

    if-gt v1, v3, :cond_0

    .line 207
    const/4 v2, 0x1

    .line 213
    .end local v1    # "width":I
    :cond_0
    :goto_0
    return v2

    .line 210
    :catch_0
    move-exception v0

    .line 211
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static isWQHD(Ljava/lang/String;)Z
    .locals 5
    .param p0, "filepath"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 234
    new-instance v1, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v1}, Landroid/media/MediaMetadataRetriever;-><init>()V

    .line 236
    .local v1, "retriever":Landroid/media/MediaMetadataRetriever;
    :try_start_0
    invoke-virtual {v1, p0}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/lang/String;)V

    .line 237
    const/16 v4, 0x12

    invoke-virtual {v1, v4}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    .line 238
    .local v2, "width":I
    const/16 v4, 0x780

    if-le v2, v4, :cond_0

    const/16 v4, 0xa00

    if-gt v2, v4, :cond_0

    .line 245
    invoke-static {v1}, Lcom/sec/android/app/ve/common/MediaUtils;->releaseRetriever(Landroid/media/MediaMetadataRetriever;)V

    .line 239
    const/4 v3, 0x1

    .line 247
    .end local v2    # "width":I
    :goto_0
    return v3

    .line 245
    .restart local v2    # "width":I
    :cond_0
    invoke-static {v1}, Lcom/sec/android/app/ve/common/MediaUtils;->releaseRetriever(Landroid/media/MediaMetadataRetriever;)V

    goto :goto_0

    .line 242
    .end local v2    # "width":I
    :catch_0
    move-exception v0

    .line 243
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 245
    invoke-static {v1}, Lcom/sec/android/app/ve/common/MediaUtils;->releaseRetriever(Landroid/media/MediaMetadataRetriever;)V

    goto :goto_0

    .line 244
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    .line 245
    invoke-static {v1}, Lcom/sec/android/app/ve/common/MediaUtils;->releaseRetriever(Landroid/media/MediaMetadataRetriever;)V

    .line 246
    throw v3
.end method

.method public static playVideo(Ljava/lang/String;)V
    .locals 3
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 519
    if-eqz p0, :cond_0

    .line 520
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 522
    .local v1, "uri":Landroid/net/Uri;
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 523
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v2, 0x10000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 524
    const-string v2, "video/mp4"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 525
    invoke-static {v0}, Lcom/sec/android/app/ve/VEApp;->startActivity(Landroid/content/Intent;)V

    .line 527
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v1    # "uri":Landroid/net/Uri;
    :cond_0
    return-void
.end method

.method public static releaseRetriever(Landroid/media/MediaMetadataRetriever;)V
    .locals 1
    .param p0, "retriever"    # Landroid/media/MediaMetadataRetriever;

    .prologue
    .line 307
    if-nez p0, :cond_0

    .line 315
    :goto_0
    return-void

    .line 311
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Landroid/media/MediaMetadataRetriever;->release()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 312
    :catch_0
    move-exception v0

    .line 313
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static removeNonExistentElements(Lcom/samsung/app/video/editor/external/TranscodeElement;Landroid/content/Context;)V
    .locals 3
    .param p0, "transcodeElement"    # Lcom/samsung/app/video/editor/external/TranscodeElement;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 1152
    if-eqz p0, :cond_0

    .line 1153
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getAssetManager()Landroid/content/res/AssetManager;

    move-result-object v0

    .line 1155
    .local v0, "assetMgr":Landroid/content/res/AssetManager;
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getAdditionlAudioEleList()Ljava/util/List;

    move-result-object v1

    invoke-static {p0, p1, v1, v0, v2}, Lcom/sec/android/app/ve/util/CommonUtils;->removeNonExistentElements(Lcom/samsung/app/video/editor/external/TranscodeElement;Landroid/content/Context;Ljava/util/List;Landroid/content/res/AssetManager;Z)Z

    .line 1156
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getAdditionlRecordEleList()Ljava/util/List;

    move-result-object v1

    invoke-static {p0, p1, v1, v0, v2}, Lcom/sec/android/app/ve/util/CommonUtils;->removeNonExistentElements(Lcom/samsung/app/video/editor/external/TranscodeElement;Landroid/content/Context;Ljava/util/List;Landroid/content/res/AssetManager;Z)Z

    .line 1157
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getAdditionlSoundEleList()Ljava/util/List;

    move-result-object v1

    invoke-static {p0, p1, v1, v0, v2}, Lcom/sec/android/app/ve/util/CommonUtils;->removeNonExistentElements(Lcom/samsung/app/video/editor/external/TranscodeElement;Landroid/content/Context;Ljava/util/List;Landroid/content/res/AssetManager;Z)Z

    .line 1159
    .end local v0    # "assetMgr":Landroid/content/res/AssetManager;
    :cond_0
    return-void
.end method

.method public static removeNonExistentElements(Lcom/samsung/app/video/editor/external/TranscodeElement;Landroid/content/Context;Ljava/util/List;Landroid/content/res/AssetManager;Z)Z
    .locals 6
    .param p0, "transcodeElement"    # Lcom/samsung/app/video/editor/external/TranscodeElement;
    .param p1, "context"    # Landroid/content/Context;
    .param p3, "assetMgr"    # Landroid/content/res/AssetManager;
    .param p4, "isMediaList"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/app/video/editor/external/TranscodeElement;",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/Element;",
            ">;",
            "Landroid/content/res/AssetManager;",
            "Z)Z"
        }
    .end annotation

    .prologue
    .line 1162
    .local p2, "elementList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    const/4 v2, 0x0

    .line 1163
    .local v2, "mediaDeleted":Z
    if-eqz p2, :cond_0

    .line 1164
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v1, v4, -0x1

    .local v1, "i":I
    :goto_0
    if-gez v1, :cond_1

    .line 1179
    .end local v1    # "i":I
    :cond_0
    return v2

    .line 1165
    .restart local v1    # "i":I
    :cond_1
    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/app/video/editor/external/Element;

    .line 1166
    .local v0, "element":Lcom/samsung/app/video/editor/external/Element;
    invoke-static {v0, p3}, Lcom/sec/android/app/ve/util/CommonUtils;->isElementFileExists(Lcom/samsung/app/video/editor/external/Element;Landroid/content/res/AssetManager;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 1168
    invoke-virtual {p0, v0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->removeElement(Lcom/samsung/app/video/editor/external/Element;)Ljava/util/List;

    .line 1169
    const/4 v2, 0x1

    .line 1170
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getThemeName()I

    move-result v4

    if-eqz v4, :cond_2

    if-eqz p4, :cond_2

    .line 1171
    invoke-static {}, Lcom/sec/android/app/ve/theme/ThemeDataManager;->getInstance()Lcom/sec/android/app/ve/theme/ThemeDataManager;

    move-result-object v4

    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getThemeName()I

    move-result v5

    invoke-virtual {v4, p3, p1, v5, v1}, Lcom/sec/android/app/ve/theme/ThemeDataManager;->getDefaultThemeElement(Landroid/content/res/AssetManager;Landroid/content/Context;II)Lcom/samsung/app/video/editor/external/Element;

    move-result-object v3

    .line 1172
    .local v3, "themeEle":Lcom/samsung/app/video/editor/external/Element;
    if-eqz v3, :cond_2

    .line 1173
    invoke-virtual {p0, v3, v1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->insertElement(Lcom/samsung/app/video/editor/external/Element;I)V

    .line 1164
    .end local v3    # "themeEle":Lcom/samsung/app/video/editor/external/Element;
    :cond_2
    add-int/lit8 v1, v1, -0x1

    goto :goto_0
.end method

.method public static showProgressDialog(Landroid/app/Activity;Landroid/os/Handler;I)Landroid/app/ProgressDialog;
    .locals 8
    .param p0, "context"    # Landroid/app/Activity;
    .param p1, "handler"    # Landroid/os/Handler;
    .param p2, "toast"    # I

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v7, 0x0

    .line 902
    .line 903
    invoke-static {p2}, Lcom/sec/android/app/ve/VEApp;->getStringValue(I)Ljava/lang/String;

    move-result-object v2

    move-object v0, p0

    move v4, v3

    move-object v5, v1

    .line 902
    invoke-static/range {v0 .. v5}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZLandroid/content/DialogInterface$OnCancelListener;)Landroid/app/ProgressDialog;

    move-result-object v6

    .line 905
    .local v6, "pd":Landroid/app/ProgressDialog;
    new-instance v0, Lcom/sec/android/app/ve/util/CommonUtils$4;

    invoke-direct {v0, p1}, Lcom/sec/android/app/ve/util/CommonUtils$4;-><init>(Landroid/os/Handler;)V

    invoke-virtual {v6, v0}, Landroid/app/ProgressDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 915
    invoke-virtual {v6, v7}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 916
    invoke-virtual {v6, v7}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 917
    return-object v6
.end method

.method public static showToast(Landroid/content/Context;IIII)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "aId"    # I
    .param p2, "gravity"    # I
    .param p3, "xOff"    # I
    .param p4, "yOff"    # I

    .prologue
    .line 454
    if-nez p0, :cond_0

    .line 462
    :goto_0
    return-void

    .line 457
    :cond_0
    sget-object v0, Lcom/sec/android/app/ve/util/CommonUtils;->mToast:Landroid/widget/Toast;

    if-nez v0, :cond_1

    if-eqz p0, :cond_1

    .line 458
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/ve/util/CommonUtils;->mToast:Landroid/widget/Toast;

    .line 460
    :cond_1
    sget-object v0, Lcom/sec/android/app/ve/util/CommonUtils;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0, p1}, Landroid/widget/Toast;->setText(I)V

    .line 461
    sget-object v0, Lcom/sec/android/app/ve/util/CommonUtils;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method private static showToastMaxChars()V
    .locals 3

    .prologue
    .line 895
    const/16 v0, 0x7d1

    .line 898
    .local v0, "MAX_TOAST_MESSAGE":I
    sget-object v1, Lcom/sec/android/app/ve/util/CommonUtils;->mMaxCharToastHandler:Landroid/os/Handler;

    const/16 v2, 0x7d1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 899
    return-void
.end method

.method public static themeProjectAllslotsFilled(Lcom/samsung/app/video/editor/external/TranscodeElement;)Z
    .locals 6
    .param p0, "transcodeElement"    # Lcom/samsung/app/video/editor/external/TranscodeElement;

    .prologue
    .line 921
    const/4 v0, 0x1

    .line 922
    .local v0, "bMediaFilled":Z
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v1

    .line 923
    .local v1, "count":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-lt v3, v1, :cond_0

    .line 930
    :goto_1
    return v0

    .line 924
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/app/video/editor/external/Element;

    .line 925
    .local v2, "element":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/Element;->getSubType()I

    move-result v4

    const/4 v5, 0x3

    if-ne v4, v5, :cond_1

    .line 926
    const/4 v0, 0x0

    .line 927
    goto :goto_1

    .line 923
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public static unicodeToUTF16(Ljava/lang/String;)Ljava/lang/String;
    .locals 14
    .param p0, "unicode"    # Ljava/lang/String;

    .prologue
    .line 1567
    const-string v0, ""

    .line 1569
    .local v0, "convertedUTF16Str":Ljava/lang/String;
    invoke-static {p0}, Ljava/lang/Integer;->decode(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v9

    .line 1571
    .local v9, "unicodeInteger":I
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v12

    add-int/lit8 v12, v12, -0x2

    const/4 v13, 0x4

    if-le v12, v13, :cond_0

    .line 1572
    const/high16 v12, 0x1f0000

    and-int/2addr v12, v9

    shr-int/lit8 v11, v12, 0x10

    .line 1573
    .local v11, "zzzzz":I
    add-int/lit8 v10, v11, -0x1

    .line 1575
    .local v10, "yyyy":I
    const/4 v5, 0x0

    .line 1576
    .local v5, "rear2byte":I
    const v6, 0xdc00

    .line 1577
    .local v6, "rear2byteFixed":I
    and-int/lit16 v12, v9, 0x3ff

    or-int v5, v6, v12

    .line 1578
    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "0x"

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v5}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 1580
    .local v8, "rear2byteString":Ljava/lang/String;
    const/4 v1, 0x0

    .line 1581
    .local v1, "former2byte":I
    const v2, 0xd800

    .line 1582
    .local v2, "former2byteFixed":I
    const v12, 0xfc00

    and-int/2addr v12, v9

    shr-int/lit8 v12, v12, 0xa

    or-int v1, v2, v12

    .line 1583
    shl-int/lit8 v12, v10, 0x6

    or-int/2addr v1, v12

    .line 1584
    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "0x"

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1586
    .local v4, "former2byteString":Ljava/lang/String;
    invoke-static {v8}, Ljava/lang/Integer;->decode(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v7

    .line 1587
    .local v7, "rear2byteInt":I
    invoke-static {v4}, Ljava/lang/Integer;->decode(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 1589
    .local v3, "former2byteInt":I
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    int-to-char v13, v3

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v12

    int-to-char v13, v7

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1595
    .end local v1    # "former2byte":I
    .end local v2    # "former2byteFixed":I
    .end local v3    # "former2byteInt":I
    .end local v4    # "former2byteString":Ljava/lang/String;
    .end local v5    # "rear2byte":I
    .end local v6    # "rear2byteFixed":I
    .end local v7    # "rear2byteInt":I
    .end local v8    # "rear2byteString":Ljava/lang/String;
    .end local v10    # "yyyy":I
    .end local v11    # "zzzzz":I
    :goto_0
    return-object v0

    .line 1592
    :cond_0
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    int-to-char v13, v9

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
