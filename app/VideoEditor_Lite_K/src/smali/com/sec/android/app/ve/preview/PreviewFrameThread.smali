.class public Lcom/sec/android/app/ve/preview/PreviewFrameThread;
.super Ljava/lang/Thread;
.source "PreviewFrameThread.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/ve/preview/PreviewFrameThread$Job;,
        Lcom/sec/android/app/ve/preview/PreviewFrameThread$PreviewFrameThreadAdapter;
    }
.end annotation


# instance fields
.field private isUIThreadBusy:Z

.field private mAdapter:Lcom/sec/android/app/ve/preview/PreviewFrameThread$PreviewFrameThreadAdapter;

.field private mCurrentElementType:I

.field private mInitFile:Ljava/lang/String;

.field private mPendingJob:Lcom/sec/android/app/ve/preview/PreviewFrameThread$Job;

.field private mSurfaceAvailable:Z

.field private mTerminate:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 26
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 28
    iput-boolean v0, p0, Lcom/sec/android/app/ve/preview/PreviewFrameThread;->mTerminate:Z

    .line 35
    iput v0, p0, Lcom/sec/android/app/ve/preview/PreviewFrameThread;->mCurrentElementType:I

    .line 37
    iput-boolean v0, p0, Lcom/sec/android/app/ve/preview/PreviewFrameThread;->mSurfaceAvailable:Z

    .line 39
    iput-boolean v0, p0, Lcom/sec/android/app/ve/preview/PreviewFrameThread;->isUIThreadBusy:Z

    .line 26
    return-void
.end method

.method private deInitCurrentFile()Z
    .locals 2

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/PreviewFrameThread;->mInitFile:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 55
    sget-boolean v0, Lcom/sec/android/app/ve/VEApp;->EngineInitImageOnFly:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/ve/preview/PreviewFrameThread;->mCurrentElementType:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 56
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/ve/preview/PreviewFrameThread;->mInitFile:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/app/video/editor/external/NativeInterface;->EngineDeinitImage(Ljava/lang/String;)V

    .line 58
    :cond_0
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/ve/preview/PreviewFrameThread;->mInitFile:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/app/video/editor/external/NativeInterface;->deInitDecoderForFile(Ljava/lang/String;)V

    .line 60
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/ve/preview/PreviewFrameThread;->mInitFile:Ljava/lang/String;

    .line 61
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/ve/preview/PreviewFrameThread;->mCurrentElementType:I

    .line 62
    const/4 v0, 0x1

    return v0
.end method

.method private displayBlackFrame(Lcom/samsung/app/video/editor/external/TranscodeElement;)V
    .locals 10
    .param p1, "trans"    # Lcom/samsung/app/video/editor/external/TranscodeElement;

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 235
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v0

    .line 236
    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getAssetManager()Landroid/content/res/AssetManager;

    move-result-object v2

    .line 235
    invoke-virtual {v0, v2, v1}, Lcom/samsung/app/video/editor/external/NativeInterface;->initDecoderForFile(Landroid/content/res/AssetManager;Ljava/lang/String;)Z

    .line 237
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v0

    const/4 v6, 0x1

    const/4 v9, 0x0

    move-object v2, v1

    move v4, v3

    move v5, v3

    move-object v7, v1

    move-object v8, v1

    invoke-virtual/range {v0 .. v9}, Lcom/samsung/app/video/editor/external/NativeInterface;->previewFrame(Lcom/samsung/app/video/editor/external/Element;Lcom/samsung/app/video/editor/external/Element;IIIILjava/util/List;Ljava/util/List;F)V

    .line 238
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/samsung/app/video/editor/external/NativeInterface;->deInitDecoderForFile(Ljava/lang/String;)V

    .line 239
    return-void
.end method


# virtual methods
.method public addOperation(Lcom/samsung/app/video/editor/external/Element;Lcom/samsung/app/video/editor/external/Element;JIIIFLjava/util/List;Ljava/util/List;)V
    .locals 15
    .param p1, "firstElement"    # Lcom/samsung/app/video/editor/external/Element;
    .param p2, "secondElement"    # Lcom/samsung/app/video/editor/external/Element;
    .param p3, "time"    # J
    .param p5, "width"    # I
    .param p6, "height"    # I
    .param p7, "previewtype"    # I
    .param p8, "storyBoardTime"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/app/video/editor/external/Element;",
            "Lcom/samsung/app/video/editor/external/Element;",
            "JIIIF",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/ClipartParams;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/ClipartParams;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 74
    .local p9, "clipartList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    .local p10, "drawList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    if-eqz p7, :cond_0

    .line 75
    const/4 v2, 0x1

    move/from16 v0, p7

    if-eq v0, v2, :cond_0

    .line 76
    const/16 p7, 0x1

    .line 78
    :cond_0
    new-instance v3, Lcom/sec/android/app/ve/preview/PreviewFrameThread$Job;

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move-wide/from16 v6, p3

    move/from16 v8, p5

    move/from16 v9, p6

    move/from16 v10, p7

    move/from16 v11, p8

    move-object/from16 v12, p9

    move-object/from16 v13, p10

    invoke-direct/range {v3 .. v13}, Lcom/sec/android/app/ve/preview/PreviewFrameThread$Job;-><init>(Lcom/samsung/app/video/editor/external/Element;Lcom/samsung/app/video/editor/external/Element;JIIIFLjava/util/List;Ljava/util/List;)V

    iput-object v3, p0, Lcom/sec/android/app/ve/preview/PreviewFrameThread;->mPendingJob:Lcom/sec/android/app/ve/preview/PreviewFrameThread$Job;

    .line 80
    monitor-enter p0

    .line 81
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V

    .line 80
    monitor-exit p0

    .line 83
    return-void

    .line 80
    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public clearPendingOperation()V
    .locals 1

    .prologue
    .line 101
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/ve/preview/PreviewFrameThread;->mPendingJob:Lcom/sec/android/app/ve/preview/PreviewFrameThread$Job;

    .line 103
    monitor-enter p0

    .line 104
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V

    .line 103
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 106
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/ve/preview/PreviewFrameThread;->isUIThreadBusy:Z

    .line 107
    return-void

    .line 103
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public init(Lcom/sec/android/app/ve/preview/PreviewFrameThread$PreviewFrameThreadAdapter;)Lcom/sec/android/app/ve/preview/PreviewFrameThread;
    .locals 1
    .param p1, "callBack"    # Lcom/sec/android/app/ve/preview/PreviewFrameThread$PreviewFrameThreadAdapter;

    .prologue
    .line 42
    invoke-virtual {p0}, Lcom/sec/android/app/ve/preview/PreviewFrameThread;->isAlive()Z

    move-result v0

    if-nez v0, :cond_0

    .line 43
    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lcom/sec/android/app/ve/preview/PreviewFrameThread;->setPriority(I)V

    .line 44
    invoke-virtual {p0}, Lcom/sec/android/app/ve/preview/PreviewFrameThread;->start()V

    .line 45
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/ve/preview/PreviewFrameThread;->mInitFile:Ljava/lang/String;

    .line 46
    iput-object p1, p0, Lcom/sec/android/app/ve/preview/PreviewFrameThread;->mAdapter:Lcom/sec/android/app/ve/preview/PreviewFrameThread$PreviewFrameThreadAdapter;

    .line 49
    :cond_0
    return-object p0
.end method

.method public isSurfaceAvailable()Z
    .locals 1

    .prologue
    .line 94
    iget-boolean v0, p0, Lcom/sec/android/app/ve/preview/PreviewFrameThread;->mSurfaceAvailable:Z

    return v0
.end method

.method public run()V
    .locals 13

    .prologue
    .line 115
    :cond_0
    :goto_0
    iget-boolean v0, p0, Lcom/sec/android/app/ve/preview/PreviewFrameThread;->mTerminate:Z

    if-eqz v0, :cond_1

    .line 232
    return-void

    .line 117
    :cond_1
    iget-object v11, p0, Lcom/sec/android/app/ve/preview/PreviewFrameThread;->mPendingJob:Lcom/sec/android/app/ve/preview/PreviewFrameThread$Job;

    .line 118
    .local v11, "onGoingJob":Lcom/sec/android/app/ve/preview/PreviewFrameThread$Job;
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/ve/preview/PreviewFrameThread;->mPendingJob:Lcom/sec/android/app/ve/preview/PreviewFrameThread$Job;

    .line 119
    const/4 v12, 0x0

    .line 121
    .local v12, "trans":Lcom/samsung/app/video/editor/external/TranscodeElement;
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/PreviewFrameThread;->mAdapter:Lcom/sec/android/app/ve/preview/PreviewFrameThread$PreviewFrameThreadAdapter;

    if-eqz v0, :cond_2

    .line 122
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/PreviewFrameThread;->mAdapter:Lcom/sec/android/app/ve/preview/PreviewFrameThread$PreviewFrameThreadAdapter;

    invoke-interface {v0}, Lcom/sec/android/app/ve/preview/PreviewFrameThread$PreviewFrameThreadAdapter;->getCurrentTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v12

    .line 124
    :cond_2
    if-eqz v12, :cond_a

    if-eqz v11, :cond_a

    iget-object v0, v11, Lcom/sec/android/app/ve/preview/PreviewFrameThread$Job;->firstElement:Lcom/samsung/app/video/editor/external/Element;

    if-eqz v0, :cond_a

    iget-boolean v0, p0, Lcom/sec/android/app/ve/preview/PreviewFrameThread;->mSurfaceAvailable:Z

    if-eqz v0, :cond_a

    .line 128
    :try_start_0
    invoke-virtual {v12}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementList()Ljava/util/List;

    move-result-object v0

    iget-object v4, v11, Lcom/sec/android/app/ve/preview/PreviewFrameThread$Job;->firstElement:Lcom/samsung/app/video/editor/external/Element;

    invoke-interface {v0, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 131
    iget v3, v11, Lcom/sec/android/app/ve/preview/PreviewFrameThread$Job;->time:I

    .line 133
    .local v3, "time":I
    iget-object v0, v11, Lcom/sec/android/app/ve/preview/PreviewFrameThread$Job;->firstElement:Lcom/samsung/app/video/editor/external/Element;

    invoke-virtual {v0, v12}, Lcom/samsung/app/video/editor/external/Element;->isStartElement(Lcom/samsung/app/video/editor/external/TranscodeElement;)V

    .line 134
    iget-object v0, v11, Lcom/sec/android/app/ve/preview/PreviewFrameThread$Job;->firstElement:Lcom/samsung/app/video/editor/external/Element;

    invoke-virtual {v0, v12}, Lcom/samsung/app/video/editor/external/Element;->isEndElement(Lcom/samsung/app/video/editor/external/TranscodeElement;)V

    .line 136
    iget-object v0, v11, Lcom/sec/android/app/ve/preview/PreviewFrameThread$Job;->firstElement:Lcom/samsung/app/video/editor/external/Element;

    invoke-static {v0}, Lcom/sec/android/app/ve/util/CommonUtils;->deepCopy(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/app/video/editor/external/Element;

    .line 138
    .local v1, "firstElement":Lcom/samsung/app/video/editor/external/Element;
    const/4 v2, 0x0

    .line 139
    .local v2, "secondElement":Lcom/samsung/app/video/editor/external/Element;
    iget-object v0, v11, Lcom/sec/android/app/ve/preview/PreviewFrameThread$Job;->secondElement:Lcom/samsung/app/video/editor/external/Element;

    if-eqz v0, :cond_3

    .line 140
    iget-object v0, v11, Lcom/sec/android/app/ve/preview/PreviewFrameThread$Job;->secondElement:Lcom/samsung/app/video/editor/external/Element;

    invoke-static {v0}, Lcom/sec/android/app/ve/util/CommonUtils;->deepCopy(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "secondElement":Lcom/samsung/app/video/editor/external/Element;
    check-cast v2, Lcom/samsung/app/video/editor/external/Element;

    .line 142
    .restart local v2    # "secondElement":Lcom/samsung/app/video/editor/external/Element;
    :cond_3
    const/4 v7, 0x0

    .line 143
    .local v7, "clipartList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    iget-object v0, v11, Lcom/sec/android/app/ve/preview/PreviewFrameThread$Job;->clipartList:Ljava/util/List;

    if-eqz v0, :cond_4

    .line 144
    iget-object v0, v11, Lcom/sec/android/app/ve/preview/PreviewFrameThread$Job;->clipartList:Ljava/util/List;

    invoke-static {v0}, Lcom/sec/android/app/ve/util/CommonUtils;->deepCopy(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    .end local v7    # "clipartList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    check-cast v7, Ljava/util/List;

    .line 147
    .restart local v7    # "clipartList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    :cond_4
    const/4 v8, 0x0

    .line 148
    .local v8, "drawList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    iget-object v0, v11, Lcom/sec/android/app/ve/preview/PreviewFrameThread$Job;->drawList:Ljava/util/List;

    if-eqz v0, :cond_5

    .line 149
    iget-object v0, v11, Lcom/sec/android/app/ve/preview/PreviewFrameThread$Job;->drawList:Ljava/util/List;

    invoke-static {v0}, Lcom/sec/android/app/ve/util/CommonUtils;->deepCopy(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    .end local v8    # "drawList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    check-cast v8, Ljava/util/List;

    .line 153
    .restart local v8    # "drawList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    :cond_5
    sget-object v0, Lcom/sec/android/app/ve/VEApp;->gExport:Lcom/sec/android/app/ve/export/Export;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/export/Export;->isExportRunning()Z

    move-result v0

    if-nez v0, :cond_a

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/Element;->getFilePath()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 160
    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getJNIVersion()F

    move-result v0

    const/high16 v4, 0x40000000    # 2.0f

    cmpl-float v0, v0, v4

    if-eqz v0, :cond_6

    .line 161
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/PreviewFrameThread;->mAdapter:Lcom/sec/android/app/ve/preview/PreviewFrameThread$PreviewFrameThreadAdapter;

    invoke-interface {v0}, Lcom/sec/android/app/ve/preview/PreviewFrameThread$PreviewFrameThreadAdapter;->setEngineSurface()V

    .line 170
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/PreviewFrameThread;->mInitFile:Ljava/lang/String;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/sec/android/app/ve/preview/PreviewFrameThread;->mInitFile:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/Element;->getFilePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_7

    .line 171
    invoke-direct {p0}, Lcom/sec/android/app/ve/preview/PreviewFrameThread;->deInitCurrentFile()Z

    .line 172
    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/Element;->getType()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/ve/preview/PreviewFrameThread;->mCurrentElementType:I

    .line 175
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/PreviewFrameThread;->mInitFile:Ljava/lang/String;

    if-nez v0, :cond_9

    .line 177
    sget-boolean v0, Lcom/sec/android/app/ve/VEApp;->EngineInitImageOnFly:Z

    if-eqz v0, :cond_8

    iget v0, p0, Lcom/sec/android/app/ve/preview/PreviewFrameThread;->mCurrentElementType:I

    const/4 v4, 0x2

    if-ne v0, v4, :cond_8

    .line 178
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v0

    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/Element;->getFilePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/samsung/app/video/editor/external/NativeInterface;->EngineInitImage(Ljava/lang/String;)Z

    .line 180
    :cond_8
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v0

    invoke-virtual {v12}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getAssetManager()Landroid/content/res/AssetManager;

    move-result-object v4

    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/Element;->getFilePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Lcom/samsung/app/video/editor/external/NativeInterface;->initDecoderForFile(Landroid/content/res/AssetManager;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 181
    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/Element;->getFilePath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/ve/preview/PreviewFrameThread;->mInitFile:Ljava/lang/String;

    .line 184
    :cond_9
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/PreviewFrameThread;->mInitFile:Ljava/lang/String;

    if-eqz v0, :cond_e

    .line 186
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v0

    .line 187
    iget v4, v11, Lcom/sec/android/app/ve/preview/PreviewFrameThread$Job;->width:I

    .line 188
    iget v5, v11, Lcom/sec/android/app/ve/preview/PreviewFrameThread$Job;->height:I

    iget v6, v11, Lcom/sec/android/app/ve/preview/PreviewFrameThread$Job;->previewType:I

    .line 189
    iget v9, v11, Lcom/sec/android/app/ve/preview/PreviewFrameThread$Job;->storyBoardTime:F

    .line 186
    invoke-virtual/range {v0 .. v9}, Lcom/samsung/app/video/editor/external/NativeInterface;->previewFrame(Lcom/samsung/app/video/editor/external/Element;Lcom/samsung/app/video/editor/external/Element;IIIILjava/util/List;Ljava/util/List;F)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 209
    .end local v1    # "firstElement":Lcom/samsung/app/video/editor/external/Element;
    .end local v2    # "secondElement":Lcom/samsung/app/video/editor/external/Element;
    .end local v3    # "time":I
    .end local v7    # "clipartList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    .end local v8    # "drawList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    :cond_a
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/PreviewFrameThread;->mPendingJob:Lcom/sec/android/app/ve/preview/PreviewFrameThread$Job;

    if-nez v0, :cond_0

    .line 212
    monitor-enter p0

    .line 215
    :try_start_1
    iget-boolean v0, p0, Lcom/sec/android/app/ve/preview/PreviewFrameThread;->isUIThreadBusy:Z

    if-nez v0, :cond_b

    .line 219
    invoke-direct {p0}, Lcom/sec/android/app/ve/preview/PreviewFrameThread;->deInitCurrentFile()Z

    .line 221
    :cond_b
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/PreviewFrameThread;->mAdapter:Lcom/sec/android/app/ve/preview/PreviewFrameThread$PreviewFrameThreadAdapter;

    if-eqz v0, :cond_c

    .line 222
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/PreviewFrameThread;->mAdapter:Lcom/sec/android/app/ve/preview/PreviewFrameThread$PreviewFrameThreadAdapter;

    invoke-interface {v0}, Lcom/sec/android/app/ve/preview/PreviewFrameThread$PreviewFrameThreadAdapter;->onFrameThreadCleared()V

    .line 224
    :cond_c
    iget-boolean v0, p0, Lcom/sec/android/app/ve/preview/PreviewFrameThread;->mTerminate:Z

    if-nez v0, :cond_d

    .line 225
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 212
    :cond_d
    :goto_2
    :try_start_2
    monitor-exit p0

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 193
    .restart local v1    # "firstElement":Lcom/samsung/app/video/editor/external/Element;
    .restart local v2    # "secondElement":Lcom/samsung/app/video/editor/external/Element;
    .restart local v3    # "time":I
    .restart local v7    # "clipartList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    .restart local v8    # "drawList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    :cond_e
    :try_start_3
    invoke-direct {p0, v12}, Lcom/sec/android/app/ve/preview/PreviewFrameThread;->displayBlackFrame(Lcom/samsung/app/video/editor/external/TranscodeElement;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_1

    .line 204
    .end local v1    # "firstElement":Lcom/samsung/app/video/editor/external/Element;
    .end local v2    # "secondElement":Lcom/samsung/app/video/editor/external/Element;
    .end local v3    # "time":I
    .end local v7    # "clipartList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    .end local v8    # "drawList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    :catch_0
    move-exception v10

    .line 205
    .local v10, "e":Ljava/lang/Exception;
    invoke-virtual {v10}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 198
    .end local v10    # "e":Ljava/lang/Exception;
    :cond_f
    :try_start_4
    sget-object v0, Lcom/sec/android/app/ve/VEApp;->gExport:Lcom/sec/android/app/ve/export/Export;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/export/Export;->isExportRunning()Z

    move-result v0

    if-nez v0, :cond_a

    .line 199
    const-string v0, "Preview frame black"

    invoke-static {v0}, Lcom/sec/android/app/ve/common/LogUtils;->log(Ljava/lang/String;)V

    .line 200
    invoke-direct {p0}, Lcom/sec/android/app/ve/preview/PreviewFrameThread;->deInitCurrentFile()Z

    .line 201
    invoke-direct {p0, v12}, Lcom/sec/android/app/ve/preview/PreviewFrameThread;->displayBlackFrame(Lcom/samsung/app/video/editor/external/TranscodeElement;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_1

    .line 227
    :catch_1
    move-exception v10

    .line 228
    .local v10, "e":Ljava/lang/InterruptedException;
    :try_start_5
    invoke-virtual {v10}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_2
.end method

.method public setSurfaceAvailable(Z)V
    .locals 1
    .param p1, "available"    # Z

    .prologue
    .line 86
    iput-boolean p1, p0, Lcom/sec/android/app/ve/preview/PreviewFrameThread;->mSurfaceAvailable:Z

    .line 87
    monitor-enter p0

    .line 88
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V

    .line 87
    monitor-exit p0

    .line 90
    return-void

    .line 87
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setUIThreadIsLoaded(Z)V
    .locals 0
    .param p1, "loaded"    # Z

    .prologue
    .line 66
    iput-boolean p1, p0, Lcom/sec/android/app/ve/preview/PreviewFrameThread;->isUIThreadBusy:Z

    .line 67
    return-void
.end method

.method public terminate()V
    .locals 1

    .prologue
    .line 110
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/ve/preview/PreviewFrameThread;->mTerminate:Z

    .line 111
    invoke-virtual {p0}, Lcom/sec/android/app/ve/preview/PreviewFrameThread;->clearPendingOperation()V

    .line 112
    return-void
.end method
