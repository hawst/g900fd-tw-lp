.class Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;
.super Ljava/lang/Object;
.source "ChordChannelService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/ve/projectsharing/ChordChannelService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ChordSession"
.end annotation


# instance fields
.field private mBytesToTransfer:J

.field private mBytesTransferred:J

.field private mCurrentTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

.field private mDestinationNode:Ljava/lang/String;

.field private mFileSizes:[J

.field private mFilesBeingTransferred:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mIsSender:Z

.field private mNotificationBuilder:Landroid/app/Notification$Builder;

.field private mOverwriteExistingProject:Z

.field private mPartialWakeLock:Landroid/os/PowerManager$WakeLock;

.field private mPercentageCompleted:I

.field private mProjectMetaData:[B

.field private mReceiverFileMapping:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mSourceNode:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 988
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 995
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mIsSender:Z

    .line 999
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mPercentageCompleted:I

    .line 988
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;)V
    .locals 0

    .prologue
    .line 988
    invoke-direct {p0}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;Landroid/os/PowerManager$WakeLock;)V
    .locals 0

    .prologue
    .line 991
    iput-object p1, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mPartialWakeLock:Landroid/os/PowerManager$WakeLock;

    return-void
.end method

.method static synthetic access$1(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;)Landroid/os/PowerManager$WakeLock;
    .locals 1

    .prologue
    .line 991
    iget-object v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mPartialWakeLock:Landroid/os/PowerManager$WakeLock;

    return-object v0
.end method

.method static synthetic access$10(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;)[B
    .locals 1

    .prologue
    .line 1001
    iget-object v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mProjectMetaData:[B

    return-object v0
.end method

.method static synthetic access$11(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 990
    iget-object v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mSourceNode:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$12(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;Z)V
    .locals 0

    .prologue
    .line 1002
    iput-boolean p1, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mOverwriteExistingProject:Z

    return-void
.end method

.method static synthetic access$13(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;)Lcom/samsung/app/video/editor/external/TranscodeElement;
    .locals 1

    .prologue
    .line 992
    iget-object v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mCurrentTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    return-object v0
.end method

.method static synthetic access$14(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;[J)V
    .locals 0

    .prologue
    .line 998
    iput-object p1, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mFileSizes:[J

    return-void
.end method

.method static synthetic access$15(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;)J
    .locals 2

    .prologue
    .line 996
    iget-wide v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mBytesToTransfer:J

    return-wide v0
.end method

.method static synthetic access$16(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 989
    iget-object v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mDestinationNode:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$17(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 993
    iput-object p1, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mFilesBeingTransferred:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$18(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;)I
    .locals 1

    .prologue
    .line 999
    iget v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mPercentageCompleted:I

    return v0
.end method

.method static synthetic access$19(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;)Landroid/app/Notification$Builder;
    .locals 1

    .prologue
    .line 1000
    iget-object v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mNotificationBuilder:Landroid/app/Notification$Builder;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;J)V
    .locals 1

    .prologue
    .line 996
    iput-wide p1, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mBytesToTransfer:J

    return-void
.end method

.method static synthetic access$20(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;I)V
    .locals 0

    .prologue
    .line 999
    iput p1, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mPercentageCompleted:I

    return-void
.end method

.method static synthetic access$22(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;Ljava/util/HashMap;)V
    .locals 0

    .prologue
    .line 994
    iput-object p1, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mReceiverFileMapping:Ljava/util/HashMap;

    return-void
.end method

.method static synthetic access$23(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 990
    iput-object p1, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mSourceNode:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$24(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;[B)V
    .locals 0

    .prologue
    .line 1001
    iput-object p1, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mProjectMetaData:[B

    return-void
.end method

.method static synthetic access$25(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;Z)V
    .locals 0

    .prologue
    .line 995
    iput-boolean p1, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mIsSender:Z

    return-void
.end method

.method static synthetic access$26(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 989
    iput-object p1, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mDestinationNode:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$27(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;Lcom/samsung/app/video/editor/external/TranscodeElement;)V
    .locals 0

    .prologue
    .line 992
    iput-object p1, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mCurrentTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    return-void
.end method

.method static synthetic access$3(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;J)V
    .locals 1

    .prologue
    .line 997
    iput-wide p1, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mBytesTransferred:J

    return-void
.end method

.method static synthetic access$4(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;Landroid/app/Notification$Builder;)V
    .locals 0

    .prologue
    .line 1000
    iput-object p1, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mNotificationBuilder:Landroid/app/Notification$Builder;

    return-void
.end method

.method static synthetic access$5(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;)J
    .locals 2

    .prologue
    .line 997
    iget-wide v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mBytesTransferred:J

    return-wide v0
.end method

.method static synthetic access$6(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;)[J
    .locals 1

    .prologue
    .line 998
    iget-object v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mFileSizes:[J

    return-object v0
.end method

.method static synthetic access$7(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 993
    iget-object v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mFilesBeingTransferred:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$8(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;)Z
    .locals 1

    .prologue
    .line 995
    iget-boolean v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mIsSender:Z

    return v0
.end method

.method static synthetic access$9(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;)Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 994
    iget-object v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mReceiverFileMapping:Ljava/util/HashMap;

    return-object v0
.end method
