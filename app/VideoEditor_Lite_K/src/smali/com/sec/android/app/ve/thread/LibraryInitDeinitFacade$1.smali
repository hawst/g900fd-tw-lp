.class Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade$1;
.super Ljava/lang/Object;
.source "LibraryInitDeinitFacade.java"

# interfaces
.implements Lcom/sec/android/app/ve/thread/LibInitDeinitForElementTask$LibInitForElementTaskListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade;


# direct methods
.method constructor <init>(Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade$1;->this$0:Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade;

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMediaDeinitialized(Ljava/lang/String;J)V
    .locals 4
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "startTime"    # J

    .prologue
    .line 39
    new-instance v0, Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade$MediaDetail;

    invoke-direct {v0, p1, p2, p3}, Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade$MediaDetail;-><init>(Ljava/lang/String;J)V

    .line 40
    .local v0, "md":Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade$MediaDetail;
    iget-object v1, p0, Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade$1;->this$0:Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade;

    # getter for: Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade;->cache:Ljava/util/HashMap;
    invoke-static {v1}, Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade;->access$0(Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade;)Ljava/util/HashMap;

    move-result-object v2

    monitor-enter v2

    .line 41
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade$1;->this$0:Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade;

    # getter for: Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade;->cache:Ljava/util/HashMap;
    invoke-static {v1}, Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade;->access$0(Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade;)Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 40
    monitor-exit v2

    .line 44
    return-void

    .line 40
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public onMediaInitialized(Ljava/lang/String;J)V
    .locals 4
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "startTime"    # J

    .prologue
    .line 28
    new-instance v0, Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade$MediaDetail;

    invoke-direct {v0, p1, p2, p3}, Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade$MediaDetail;-><init>(Ljava/lang/String;J)V

    .line 29
    .local v0, "md":Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade$MediaDetail;
    iget-object v1, p0, Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade$1;->this$0:Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade;

    # getter for: Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade;->cache:Ljava/util/HashMap;
    invoke-static {v1}, Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade;->access$0(Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade;)Ljava/util/HashMap;

    move-result-object v2

    monitor-enter v2

    .line 30
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade$1;->this$0:Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade;

    # getter for: Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade;->cache:Ljava/util/HashMap;
    invoke-static {v1}, Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade;->access$0(Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade;)Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 31
    iget-object v1, p0, Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade$1;->this$0:Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade;

    # getter for: Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade;->cache:Ljava/util/HashMap;
    invoke-static {v1}, Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade;->access$0(Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade;)Ljava/util/HashMap;

    move-result-object v1

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 29
    :cond_0
    monitor-exit v2

    .line 35
    return-void

    .line 29
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
