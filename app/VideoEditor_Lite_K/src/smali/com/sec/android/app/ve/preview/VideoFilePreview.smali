.class public Lcom/sec/android/app/ve/preview/VideoFilePreview;
.super Lcom/sec/android/app/ve/PreviewPlayerInterface;
.source "VideoFilePreview.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/ve/preview/VideoFilePreview$UnplugHeadsetReceiver;
    }
.end annotation


# instance fields
.field private mAdapter:Lcom/sec/android/app/ve/PreviewPlayerInterface$Adapter;

.field private mContext:Landroid/content/Context;

.field private mVideoCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

.field private mVideoErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

.field private mVideoView:Lcom/sec/android/app/ve/preview/ScaleableVideoView;

.field private receiver:Lcom/sec/android/app/ve/preview/VideoFilePreview$UnplugHeadsetReceiver;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 90
    invoke-direct {p0, p1}, Lcom/sec/android/app/ve/PreviewPlayerInterface;-><init>(Landroid/content/Context;)V

    .line 47
    new-instance v0, Lcom/sec/android/app/ve/preview/VideoFilePreview$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/ve/preview/VideoFilePreview$1;-><init>(Lcom/sec/android/app/ve/preview/VideoFilePreview;)V

    iput-object v0, p0, Lcom/sec/android/app/ve/preview/VideoFilePreview;->mVideoCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    .line 55
    new-instance v0, Lcom/sec/android/app/ve/preview/VideoFilePreview$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/ve/preview/VideoFilePreview$2;-><init>(Lcom/sec/android/app/ve/preview/VideoFilePreview;)V

    iput-object v0, p0, Lcom/sec/android/app/ve/preview/VideoFilePreview;->mVideoErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    .line 91
    iput-object p1, p0, Lcom/sec/android/app/ve/preview/VideoFilePreview;->mContext:Landroid/content/Context;

    .line 92
    invoke-direct {p0}, Lcom/sec/android/app/ve/preview/VideoFilePreview;->init()V

    .line 93
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 84
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/ve/PreviewPlayerInterface;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 47
    new-instance v0, Lcom/sec/android/app/ve/preview/VideoFilePreview$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/ve/preview/VideoFilePreview$1;-><init>(Lcom/sec/android/app/ve/preview/VideoFilePreview;)V

    iput-object v0, p0, Lcom/sec/android/app/ve/preview/VideoFilePreview;->mVideoCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    .line 55
    new-instance v0, Lcom/sec/android/app/ve/preview/VideoFilePreview$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/ve/preview/VideoFilePreview$2;-><init>(Lcom/sec/android/app/ve/preview/VideoFilePreview;)V

    iput-object v0, p0, Lcom/sec/android/app/ve/preview/VideoFilePreview;->mVideoErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    .line 85
    iput-object p1, p0, Lcom/sec/android/app/ve/preview/VideoFilePreview;->mContext:Landroid/content/Context;

    .line 86
    invoke-direct {p0}, Lcom/sec/android/app/ve/preview/VideoFilePreview;->init()V

    .line 87
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 96
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/ve/PreviewPlayerInterface;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 47
    new-instance v0, Lcom/sec/android/app/ve/preview/VideoFilePreview$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/ve/preview/VideoFilePreview$1;-><init>(Lcom/sec/android/app/ve/preview/VideoFilePreview;)V

    iput-object v0, p0, Lcom/sec/android/app/ve/preview/VideoFilePreview;->mVideoCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    .line 55
    new-instance v0, Lcom/sec/android/app/ve/preview/VideoFilePreview$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/ve/preview/VideoFilePreview$2;-><init>(Lcom/sec/android/app/ve/preview/VideoFilePreview;)V

    iput-object v0, p0, Lcom/sec/android/app/ve/preview/VideoFilePreview;->mVideoErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    .line 97
    iput-object p1, p0, Lcom/sec/android/app/ve/preview/VideoFilePreview;->mContext:Landroid/content/Context;

    .line 98
    invoke-direct {p0}, Lcom/sec/android/app/ve/preview/VideoFilePreview;->init()V

    .line 99
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/app/ve/preview/VideoFilePreview;)Lcom/sec/android/app/ve/PreviewPlayerInterface$Adapter;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/VideoFilePreview;->mAdapter:Lcom/sec/android/app/ve/PreviewPlayerInterface$Adapter;

    return-object v0
.end method

.method private init()V
    .locals 3

    .prologue
    .line 102
    invoke-virtual {p0}, Lcom/sec/android/app/ve/preview/VideoFilePreview;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 103
    .local v0, "inflater":Landroid/view/LayoutInflater;
    sget v1, Lcom/sec/android/app/ve/R$layout;->videofile_preview:I

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 104
    sget v1, Lcom/sec/android/app/ve/R$id;->videoView:I

    invoke-virtual {p0, v1}, Lcom/sec/android/app/ve/preview/VideoFilePreview;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/ve/preview/ScaleableVideoView;

    iput-object v1, p0, Lcom/sec/android/app/ve/preview/VideoFilePreview;->mVideoView:Lcom/sec/android/app/ve/preview/ScaleableVideoView;

    .line 105
    iget-object v1, p0, Lcom/sec/android/app/ve/preview/VideoFilePreview;->mVideoView:Lcom/sec/android/app/ve/preview/ScaleableVideoView;

    iget-object v2, p0, Lcom/sec/android/app/ve/preview/VideoFilePreview;->mVideoCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/ve/preview/ScaleableVideoView;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 106
    iget-object v1, p0, Lcom/sec/android/app/ve/preview/VideoFilePreview;->mVideoView:Lcom/sec/android/app/ve/preview/ScaleableVideoView;

    iget-object v2, p0, Lcom/sec/android/app/ve/preview/VideoFilePreview;->mVideoErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/ve/preview/ScaleableVideoView;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 107
    invoke-direct {p0}, Lcom/sec/android/app/ve/preview/VideoFilePreview;->registerForHeadSetUnplugged()V

    .line 108
    return-void
.end method

.method private registerForHeadSetUnplugged()V
    .locals 3

    .prologue
    .line 69
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.media.AUDIO_BECOMING_NOISY"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 70
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    new-instance v1, Lcom/sec/android/app/ve/preview/VideoFilePreview$UnplugHeadsetReceiver;

    invoke-direct {v1, p0}, Lcom/sec/android/app/ve/preview/VideoFilePreview$UnplugHeadsetReceiver;-><init>(Lcom/sec/android/app/ve/preview/VideoFilePreview;)V

    iput-object v1, p0, Lcom/sec/android/app/ve/preview/VideoFilePreview;->receiver:Lcom/sec/android/app/ve/preview/VideoFilePreview$UnplugHeadsetReceiver;

    .line 71
    iget-object v1, p0, Lcom/sec/android/app/ve/preview/VideoFilePreview;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/ve/preview/VideoFilePreview;->receiver:Lcom/sec/android/app/ve/preview/VideoFilePreview$UnplugHeadsetReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 72
    return-void
.end method

.method private unRegisterForHeadSetUnplugged()V
    .locals 3

    .prologue
    .line 76
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/ve/preview/VideoFilePreview;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/ve/preview/VideoFilePreview;->receiver:Lcom/sec/android/app/ve/preview/VideoFilePreview$UnplugHeadsetReceiver;

    if-eqz v1, :cond_0

    .line 77
    iget-object v1, p0, Lcom/sec/android/app/ve/preview/VideoFilePreview;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/ve/preview/VideoFilePreview;->receiver:Lcom/sec/android/app/ve/preview/VideoFilePreview$UnplugHeadsetReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 82
    :cond_0
    :goto_0
    return-void

    .line 78
    :catch_0
    move-exception v0

    .line 79
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public clearPendingPreviewRequest()V
    .locals 0

    .prologue
    .line 164
    return-void
.end method

.method public getMaxVolume()I
    .locals 1

    .prologue
    .line 178
    const/4 v0, 0x0

    return v0
.end method

.method public getSurfaceView()Landroid/view/SurfaceView;
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/VideoFilePreview;->mVideoView:Lcom/sec/android/app/ve/preview/ScaleableVideoView;

    return-object v0
.end method

.method public getVolume()I
    .locals 1

    .prologue
    .line 173
    const/4 v0, 0x0

    return v0
.end method

.method public isPlaying()Z
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/VideoFilePreview;->mVideoView:Lcom/sec/android/app/ve/preview/ScaleableVideoView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/ve/preview/VideoFilePreview;->mVideoView:Lcom/sec/android/app/ve/preview/ScaleableVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/preview/ScaleableVideoView;->isPlaying()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 193
    invoke-virtual {p0}, Lcom/sec/android/app/ve/preview/VideoFilePreview;->stop()V

    .line 194
    invoke-direct {p0}, Lcom/sec/android/app/ve/preview/VideoFilePreview;->unRegisterForHeadSetUnplugged()V

    .line 195
    invoke-super {p0}, Lcom/sec/android/app/ve/PreviewPlayerInterface;->onDetachedFromWindow()V

    .line 197
    return-void
.end method

.method public pause()V
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/VideoFilePreview;->mVideoView:Lcom/sec/android/app/ve/preview/ScaleableVideoView;

    if-eqz v0, :cond_0

    .line 135
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/VideoFilePreview;->mVideoView:Lcom/sec/android/app/ve/preview/ScaleableVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/preview/ScaleableVideoView;->pause()V

    .line 136
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/VideoFilePreview;->mAdapter:Lcom/sec/android/app/ve/PreviewPlayerInterface$Adapter;

    if-eqz v0, :cond_1

    .line 137
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/VideoFilePreview;->mAdapter:Lcom/sec/android/app/ve/PreviewPlayerInterface$Adapter;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/PreviewPlayerInterface$Adapter;->onStopped()V

    .line 138
    :cond_1
    return-void
.end method

.method public play(J)V
    .locals 1
    .param p1, "pos"    # J

    .prologue
    .line 128
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/VideoFilePreview;->mVideoView:Lcom/sec/android/app/ve/preview/ScaleableVideoView;

    if-eqz v0, :cond_0

    .line 129
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/VideoFilePreview;->mVideoView:Lcom/sec/android/app/ve/preview/ScaleableVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/preview/ScaleableVideoView;->start()V

    .line 130
    :cond_0
    return-void
.end method

.method public resume(J)V
    .locals 1
    .param p1, "pos"    # J

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/VideoFilePreview;->mVideoView:Lcom/sec/android/app/ve/preview/ScaleableVideoView;

    if-eqz v0, :cond_0

    .line 143
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/VideoFilePreview;->mVideoView:Lcom/sec/android/app/ve/preview/ScaleableVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/preview/ScaleableVideoView;->resume()V

    .line 144
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/VideoFilePreview;->mAdapter:Lcom/sec/android/app/ve/PreviewPlayerInterface$Adapter;

    if-eqz v0, :cond_1

    .line 145
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/VideoFilePreview;->mAdapter:Lcom/sec/android/app/ve/PreviewPlayerInterface$Adapter;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/PreviewPlayerInterface$Adapter;->onPlayed()V

    .line 146
    :cond_1
    return-void
.end method

.method public seekTo(J)V
    .locals 0
    .param p1, "pos"    # J

    .prologue
    .line 159
    return-void
.end method

.method public setActivity(Landroid/app/Activity;)V
    .locals 0
    .param p1, "actvt"    # Landroid/app/Activity;

    .prologue
    .line 124
    return-void
.end method

.method public setAdapter(Lcom/sec/android/app/ve/PreviewPlayerInterface$Adapter;)V
    .locals 0
    .param p1, "adap"    # Lcom/sec/android/app/ve/PreviewPlayerInterface$Adapter;

    .prologue
    .line 168
    iput-object p1, p0, Lcom/sec/android/app/ve/preview/VideoFilePreview;->mAdapter:Lcom/sec/android/app/ve/PreviewPlayerInterface$Adapter;

    .line 169
    return-void
.end method

.method public setDataSource(Landroid/net/Uri;)V
    .locals 3
    .param p1, "videoURI"    # Landroid/net/Uri;

    .prologue
    .line 223
    move-object v0, p1

    .line 224
    .local v0, "videoPath":Landroid/net/Uri;
    invoke-virtual {p0}, Lcom/sec/android/app/ve/preview/VideoFilePreview;->removeAllViews()V

    .line 225
    invoke-direct {p0}, Lcom/sec/android/app/ve/preview/VideoFilePreview;->init()V

    .line 226
    iget-object v1, p0, Lcom/sec/android/app/ve/preview/VideoFilePreview;->mVideoView:Lcom/sec/android/app/ve/preview/ScaleableVideoView;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 227
    iget-object v1, p0, Lcom/sec/android/app/ve/preview/VideoFilePreview;->mVideoView:Lcom/sec/android/app/ve/preview/ScaleableVideoView;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/ve/preview/ScaleableVideoView;->setVideoURI(Landroid/net/Uri;)V

    .line 232
    :goto_0
    return-void

    .line 229
    :cond_0
    const-string v1, "Fatal Handled"

    const-string v2, "setVideo Failed"

    invoke-static {v1, v2}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setDataSource(Ljava/lang/String;)V
    .locals 2
    .param p1, "videoPath"    # Ljava/lang/String;

    .prologue
    .line 112
    invoke-virtual {p0}, Lcom/sec/android/app/ve/preview/VideoFilePreview;->removeAllViews()V

    .line 113
    invoke-direct {p0}, Lcom/sec/android/app/ve/preview/VideoFilePreview;->init()V

    .line 114
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/VideoFilePreview;->mVideoView:Lcom/sec/android/app/ve/preview/ScaleableVideoView;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 115
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/VideoFilePreview;->mVideoView:Lcom/sec/android/app/ve/preview/ScaleableVideoView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/ve/preview/ScaleableVideoView;->setVideoPath(Ljava/lang/String;)V

    .line 119
    :goto_0
    return-void

    .line 117
    :cond_0
    const-string v0, "Fatal Handled"

    const-string v1, "setVideo Failed"

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setFixedSurfaceRect(Landroid/graphics/Rect;)V
    .locals 3
    .param p1, "surfaceRect"    # Landroid/graphics/Rect;

    .prologue
    .line 201
    iget-object v1, p0, Lcom/sec/android/app/ve/preview/VideoFilePreview;->mVideoView:Lcom/sec/android/app/ve/preview/ScaleableVideoView;

    invoke-virtual {v1}, Lcom/sec/android/app/ve/preview/ScaleableVideoView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 202
    .local v0, "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 203
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 204
    iget v1, p1, Landroid/graphics/Rect;->left:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 205
    iget v1, p1, Landroid/graphics/Rect;->top:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 206
    iget-object v1, p0, Lcom/sec/android/app/ve/preview/VideoFilePreview;->mVideoView:Lcom/sec/android/app/ve/preview/ScaleableVideoView;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/ve/preview/ScaleableVideoView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 207
    iget-object v1, p0, Lcom/sec/android/app/ve/preview/VideoFilePreview;->mVideoView:Lcom/sec/android/app/ve/preview/ScaleableVideoView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/app/ve/preview/ScaleableVideoView;->setFixedSizeFromLayout(Z)V

    .line 208
    return-void
.end method

.method public setVolume(I)V
    .locals 0
    .param p1, "volume"    # I

    .prologue
    .line 184
    return-void
.end method

.method public stop()V
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/VideoFilePreview;->mVideoView:Lcom/sec/android/app/ve/preview/ScaleableVideoView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/ve/preview/VideoFilePreview;->mVideoView:Lcom/sec/android/app/ve/preview/ScaleableVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/preview/ScaleableVideoView;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 151
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/VideoFilePreview;->mVideoView:Lcom/sec/android/app/ve/preview/ScaleableVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/preview/ScaleableVideoView;->stopPlayback()V

    .line 152
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/VideoFilePreview;->mAdapter:Lcom/sec/android/app/ve/PreviewPlayerInterface$Adapter;

    if-eqz v0, :cond_1

    .line 153
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/VideoFilePreview;->mAdapter:Lcom/sec/android/app/ve/PreviewPlayerInterface$Adapter;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/PreviewPlayerInterface$Adapter;->onStopped()V

    .line 154
    :cond_1
    return-void
.end method

.method public update(Lcom/samsung/app/video/editor/external/Element;Lcom/samsung/app/video/editor/external/Element;JIFLjava/util/List;Ljava/util/List;)V
    .locals 0
    .param p1, "firstElem"    # Lcom/samsung/app/video/editor/external/Element;
    .param p2, "secondElem"    # Lcom/samsung/app/video/editor/external/Element;
    .param p3, "time"    # J
    .param p5, "prevType"    # I
    .param p6, "storyBoardTime"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/app/video/editor/external/Element;",
            "Lcom/samsung/app/video/editor/external/Element;",
            "JIF",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/ClipartParams;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/ClipartParams;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 214
    .local p7, "clipartLis":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    .local p8, "drawList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    return-void
.end method
