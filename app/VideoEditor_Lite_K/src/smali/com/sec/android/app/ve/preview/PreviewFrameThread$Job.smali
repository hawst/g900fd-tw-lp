.class Lcom/sec/android/app/ve/preview/PreviewFrameThread$Job;
.super Ljava/lang/Object;
.source "PreviewFrameThread.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/ve/preview/PreviewFrameThread;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Job"
.end annotation


# instance fields
.field public clipartList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/ClipartParams;",
            ">;"
        }
    .end annotation
.end field

.field public drawList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/ClipartParams;",
            ">;"
        }
    .end annotation
.end field

.field public firstElement:Lcom/samsung/app/video/editor/external/Element;

.field public height:I

.field public previewType:I

.field public secondElement:Lcom/samsung/app/video/editor/external/Element;

.field public storyBoardTime:F

.field public time:I

.field public width:I


# direct methods
.method public constructor <init>(Lcom/samsung/app/video/editor/external/Element;Lcom/samsung/app/video/editor/external/Element;JIIIFLjava/util/List;Ljava/util/List;)V
    .locals 1
    .param p1, "firstElement"    # Lcom/samsung/app/video/editor/external/Element;
    .param p2, "secondElement"    # Lcom/samsung/app/video/editor/external/Element;
    .param p3, "time"    # J
    .param p5, "width"    # I
    .param p6, "height"    # I
    .param p7, "type"    # I
    .param p8, "storyBoardTime"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/app/video/editor/external/Element;",
            "Lcom/samsung/app/video/editor/external/Element;",
            "JIIIF",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/ClipartParams;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/ClipartParams;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 253
    .local p9, "clipartList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    .local p10, "drawList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 254
    iput-object p1, p0, Lcom/sec/android/app/ve/preview/PreviewFrameThread$Job;->firstElement:Lcom/samsung/app/video/editor/external/Element;

    .line 255
    iput-object p2, p0, Lcom/sec/android/app/ve/preview/PreviewFrameThread$Job;->secondElement:Lcom/samsung/app/video/editor/external/Element;

    .line 256
    iput p5, p0, Lcom/sec/android/app/ve/preview/PreviewFrameThread$Job;->width:I

    .line 257
    iput p6, p0, Lcom/sec/android/app/ve/preview/PreviewFrameThread$Job;->height:I

    .line 258
    long-to-int v0, p3

    iput v0, p0, Lcom/sec/android/app/ve/preview/PreviewFrameThread$Job;->time:I

    .line 259
    iput p7, p0, Lcom/sec/android/app/ve/preview/PreviewFrameThread$Job;->previewType:I

    .line 260
    iput p8, p0, Lcom/sec/android/app/ve/preview/PreviewFrameThread$Job;->storyBoardTime:F

    .line 261
    iput-object p9, p0, Lcom/sec/android/app/ve/preview/PreviewFrameThread$Job;->clipartList:Ljava/util/List;

    .line 262
    iput-object p10, p0, Lcom/sec/android/app/ve/preview/PreviewFrameThread$Job;->drawList:Ljava/util/List;

    .line 263
    return-void
.end method
