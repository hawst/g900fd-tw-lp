.class public Lcom/sec/android/app/ve/bgm/BGMManager;
.super Ljava/lang/Object;
.source "BGMManager.java"


# instance fields
.field private BGMList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/ve/bgm/BGM;",
            ">;"
        }
    .end annotation
.end field

.field private bgmMode:Lcom/sec/android/app/ve/bgm/BGM$BGMModes;

.field private isThemeDefaultBGM:Z

.field private final overlapTrans:I

.field private savedBGMPos:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    sget-object v0, Lcom/sec/android/app/ve/bgm/BGM$BGMModes;->MEDIUM:Lcom/sec/android/app/ve/bgm/BGM$BGMModes;

    iput-object v0, p0, Lcom/sec/android/app/ve/bgm/BGMManager;->bgmMode:Lcom/sec/android/app/ve/bgm/BGM$BGMModes;

    .line 35
    const/16 v0, 0xf

    iput v0, p0, Lcom/sec/android/app/ve/bgm/BGMManager;->savedBGMPos:I

    .line 37
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/ve/bgm/BGMManager;->isThemeDefaultBGM:Z

    .line 41
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/ve/bgm/BGMManager;->overlapTrans:I

    .line 13
    return-void
.end method


# virtual methods
.method public addBGM(Lcom/sec/android/app/ve/bgm/BGM;)V
    .locals 1
    .param p1, "bgm"    # Lcom/sec/android/app/ve/bgm/BGM;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/android/app/ve/bgm/BGMManager;->BGMList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 80
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/ve/bgm/BGMManager;->BGMList:Ljava/util/ArrayList;

    .line 82
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/ve/bgm/BGMManager;->BGMList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 83
    return-void
.end method

.method public calculateSlotDurations(Lcom/sec/android/app/ve/bgm/BGM$BGMModes;I)[I
    .locals 8
    .param p1, "mode"    # Lcom/sec/android/app/ve/bgm/BGM$BGMModes;
    .param p2, "position"    # I

    .prologue
    const/4 v6, 0x0

    .line 154
    invoke-virtual {p0, p2}, Lcom/sec/android/app/ve/bgm/BGMManager;->getBGM(I)Lcom/sec/android/app/ve/bgm/BGM;

    move-result-object v1

    .line 155
    .local v1, "bgm":Lcom/sec/android/app/ve/bgm/BGM;
    if-nez v1, :cond_0

    .line 156
    const/4 v3, 0x0

    .line 169
    :goto_0
    return-object v3

    .line 158
    :cond_0
    invoke-virtual {v1, p1}, Lcom/sec/android/app/ve/bgm/BGM;->getTransitionTimeStamps(Lcom/sec/android/app/ve/bgm/BGM$BGMModes;)[I

    move-result-object v4

    .line 159
    .local v4, "transitionStamps":[I
    array-length v5, v4

    add-int/lit8 v0, v5, 0x1

    .line 160
    .local v0, "No_Of_Slots":I
    new-array v3, v0, [I

    .line 161
    .local v3, "slotDurations":[I
    aget v5, v4, v6

    add-int/lit8 v5, v5, 0x0

    aput v5, v3, v6

    .line 162
    const/4 v2, 0x1

    .local v2, "i":I
    :goto_1
    array-length v5, v4

    if-lt v2, v5, :cond_1

    .line 165
    add-int/lit8 v5, v0, -0x1

    invoke-virtual {v1}, Lcom/sec/android/app/ve/bgm/BGM;->getTotalDurationBGM()I

    move-result v6

    array-length v7, v4

    add-int/lit8 v7, v7, -0x1

    aget v7, v4, v7

    sub-int/2addr v6, v7

    aput v6, v3, v5

    goto :goto_0

    .line 163
    :cond_1
    aget v5, v4, v2

    add-int/lit8 v6, v2, -0x1

    aget v6, v4, v6

    sub-int/2addr v5, v6

    add-int/lit8 v5, v5, 0x0

    aput v5, v3, v2

    .line 162
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method public getBGM(I)Lcom/sec/android/app/ve/bgm/BGM;
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 90
    const/4 v0, 0x0

    .line 91
    .local v0, "bgm":Lcom/sec/android/app/ve/bgm/BGM;
    const/4 v1, -0x1

    if-ne p1, v1, :cond_0

    .line 92
    const/4 v1, 0x0

    .line 97
    :goto_0
    return-object v1

    .line 94
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/ve/bgm/BGMManager;->BGMList:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/ve/bgm/BGMManager;->BGMList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge p1, v1, :cond_1

    .line 95
    iget-object v1, p0, Lcom/sec/android/app/ve/bgm/BGMManager;->BGMList:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "bgm":Lcom/sec/android/app/ve/bgm/BGM;
    check-cast v0, Lcom/sec/android/app/ve/bgm/BGM;

    .restart local v0    # "bgm":Lcom/sec/android/app/ve/bgm/BGM;
    :cond_1
    move-object v1, v0

    .line 97
    goto :goto_0
.end method

.method public getBGMCount()I
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/app/ve/bgm/BGMManager;->BGMList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 137
    const/4 v0, 0x0

    .line 139
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/ve/bgm/BGMManager;->BGMList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getBGMFilePath(I)Ljava/lang/String;
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 107
    const-string v1, ""

    .line 108
    .local v1, "filePath":Ljava/lang/String;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/ve/bgm/BGMManager;->getBGM(I)Lcom/sec/android/app/ve/bgm/BGM;

    move-result-object v0

    .line 109
    .local v0, "bgm":Lcom/sec/android/app/ve/bgm/BGM;
    if-eqz v0, :cond_0

    .line 110
    invoke-virtual {v0}, Lcom/sec/android/app/ve/bgm/BGM;->getBGMFilepath()Ljava/lang/String;

    move-result-object v1

    .line 112
    :cond_0
    return-object v1
.end method

.method public getBGMName(I)Ljava/lang/String;
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 122
    const-string v1, ""

    .line 123
    .local v1, "bgmName":Ljava/lang/String;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/ve/bgm/BGMManager;->getBGM(I)Lcom/sec/android/app/ve/bgm/BGM;

    move-result-object v0

    .line 124
    .local v0, "bgm":Lcom/sec/android/app/ve/bgm/BGM;
    if-eqz v0, :cond_0

    .line 125
    invoke-virtual {v0}, Lcom/sec/android/app/ve/bgm/BGM;->getBGMName()Ljava/lang/String;

    move-result-object v1

    .line 126
    :cond_0
    return-object v1
.end method

.method public getBgmMode()Lcom/sec/android/app/ve/bgm/BGM$BGMModes;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/app/ve/bgm/BGMManager;->bgmMode:Lcom/sec/android/app/ve/bgm/BGM$BGMModes;

    return-object v0
.end method

.method public getSavedBGMPos()I
    .locals 1

    .prologue
    .line 49
    iget v0, p0, Lcom/sec/android/app/ve/bgm/BGMManager;->savedBGMPos:I

    return v0
.end method

.method public getTotalDurationBGM(I)I
    .locals 2
    .param p1, "pos"    # I

    .prologue
    .line 178
    const/4 v1, 0x0

    .line 179
    .local v1, "duration":I
    invoke-virtual {p0, p1}, Lcom/sec/android/app/ve/bgm/BGMManager;->getBGM(I)Lcom/sec/android/app/ve/bgm/BGM;

    move-result-object v0

    .line 180
    .local v0, "bgm":Lcom/sec/android/app/ve/bgm/BGM;
    if-eqz v0, :cond_0

    .line 181
    invoke-virtual {v0}, Lcom/sec/android/app/ve/bgm/BGM;->getTotalDurationBGM()I

    move-result v1

    .line 183
    :cond_0
    return v1
.end method

.method public isThemeDefaultBGM()Z
    .locals 1

    .prologue
    .line 64
    iget-boolean v0, p0, Lcom/sec/android/app/ve/bgm/BGMManager;->isThemeDefaultBGM:Z

    return v0
.end method

.method public mapThemeToBGM(I)I
    .locals 1
    .param p1, "themeId"    # I

    .prologue
    .line 192
    const/4 v0, 0x0

    .line 193
    .local v0, "bgmPos":I
    packed-switch p1, :pswitch_data_0

    .line 245
    :goto_0
    :pswitch_0
    return v0

    .line 195
    :pswitch_1
    const/16 v0, 0xf

    .line 196
    goto :goto_0

    .line 198
    :pswitch_2
    const/16 v0, 0x18

    .line 199
    goto :goto_0

    .line 201
    :pswitch_3
    const/16 v0, 0x17

    .line 202
    goto :goto_0

    .line 204
    :pswitch_4
    const/16 v0, 0x12

    .line 205
    goto :goto_0

    .line 207
    :pswitch_5
    const/16 v0, 0x9

    .line 208
    goto :goto_0

    .line 210
    :pswitch_6
    const/16 v0, 0x11

    .line 211
    goto :goto_0

    .line 213
    :pswitch_7
    const/4 v0, 0x7

    .line 214
    goto :goto_0

    .line 216
    :pswitch_8
    const/16 v0, 0x10

    .line 217
    goto :goto_0

    .line 219
    :pswitch_9
    const/16 v0, 0xe

    .line 220
    goto :goto_0

    .line 222
    :pswitch_a
    const/4 v0, 0x2

    .line 223
    goto :goto_0

    .line 225
    :pswitch_b
    const/4 v0, 0x6

    .line 226
    goto :goto_0

    .line 228
    :pswitch_c
    const/16 v0, 0xd

    .line 229
    goto :goto_0

    .line 231
    :pswitch_d
    const/16 v0, 0xa

    .line 232
    goto :goto_0

    .line 234
    :pswitch_e
    const/4 v0, 0x5

    .line 235
    goto :goto_0

    .line 237
    :pswitch_f
    const/16 v0, 0x16

    .line 238
    goto :goto_0

    .line 240
    :pswitch_10
    const/16 v0, 0x13

    .line 241
    goto :goto_0

    .line 193
    nop

    :pswitch_data_0
    .packed-switch 0xd
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_f
        :pswitch_0
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_10
        :pswitch_7
    .end packed-switch
.end method

.method public setBgmMode(Lcom/sec/android/app/ve/bgm/BGM$BGMModes;)V
    .locals 0
    .param p1, "bgmMode"    # Lcom/sec/android/app/ve/bgm/BGM$BGMModes;

    .prologue
    .line 71
    iput-object p1, p0, Lcom/sec/android/app/ve/bgm/BGMManager;->bgmMode:Lcom/sec/android/app/ve/bgm/BGM$BGMModes;

    .line 72
    return-void
.end method

.method public setSavedBGMPos(I)V
    .locals 0
    .param p1, "savedBGMPos"    # I

    .prologue
    .line 56
    iput p1, p0, Lcom/sec/android/app/ve/bgm/BGMManager;->savedBGMPos:I

    .line 57
    return-void
.end method

.method public setThemeDefaultBGM(Z)V
    .locals 0
    .param p1, "isDefault"    # Z

    .prologue
    .line 60
    iput-boolean p1, p0, Lcom/sec/android/app/ve/bgm/BGMManager;->isThemeDefaultBGM:Z

    .line 61
    return-void
.end method
