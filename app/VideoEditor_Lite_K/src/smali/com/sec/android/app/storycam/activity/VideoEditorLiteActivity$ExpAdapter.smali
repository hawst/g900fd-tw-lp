.class Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExpAdapter;
.super Lcom/sec/android/app/ve/export/Export$Adapter;
.source "VideoEditorLiteActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ExpAdapter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;)V
    .locals 0

    .prologue
    .line 1950
    iput-object p1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExpAdapter;->this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;

    invoke-direct {p0}, Lcom/sec/android/app/ve/export/Export$Adapter;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExpAdapter;)V
    .locals 0

    .prologue
    .line 1950
    invoke-direct {p0, p1}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExpAdapter;-><init>(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;)V

    return-void
.end method

.method private refreshPreviewAfterExport()V
    .locals 4

    .prologue
    .line 2005
    invoke-static {}, Lcom/sec/android/app/ve/AppLifeCycleHandler;->isApplicationInForeground()Z

    move-result v2

    if-nez v2, :cond_1

    .line 2011
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/NativeInterface;->_native_terminate()V

    .line 2019
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExpAdapter;->this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;

    # getter for: Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mRootView:Landroid/widget/RelativeLayout;
    invoke-static {v2}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->access$0(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;)Landroid/widget/RelativeLayout;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 2020
    iget-object v2, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExpAdapter;->this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;

    # getter for: Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mRootView:Landroid/widget/RelativeLayout;
    invoke-static {v2}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->access$0(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;)Landroid/widget/RelativeLayout;

    move-result-object v2

    const v3, 0x7f0d0003

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    .line 2021
    .local v1, "pvg":Lcom/sec/android/app/storycam/view/PreviewViewGroup;
    const v2, 0x7f0d0041

    invoke-virtual {v1, v2}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/ve/PreviewPlayerInterface;

    .line 2022
    .local v0, "player":Lcom/sec/android/app/ve/PreviewPlayerInterface;
    invoke-virtual {v0}, Lcom/sec/android/app/ve/PreviewPlayerInterface;->setEngineSurface()V

    .line 2023
    if-eqz v1, :cond_0

    .line 2024
    invoke-virtual {v1}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->refreshPreviewToCurrentSeekBar()V

    .line 2026
    .end local v0    # "player":Lcom/sec/android/app/ve/PreviewPlayerInterface;
    .end local v1    # "pvg":Lcom/sec/android/app/storycam/view/PreviewViewGroup;
    :cond_0
    return-void

    .line 2016
    :cond_1
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/NativeInterface;->_native_initfimc()V

    goto :goto_0
.end method


# virtual methods
.method public clearNativeJobBeforeExport()Z
    .locals 3

    .prologue
    .line 1953
    iget-object v1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExpAdapter;->this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;

    # getter for: Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mRootView:Landroid/widget/RelativeLayout;
    invoke-static {v1}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->access$0(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;)Landroid/widget/RelativeLayout;

    move-result-object v1

    const v2, 0x7f0d0003

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    .line 1954
    .local v0, "pvg":Lcom/sec/android/app/storycam/view/PreviewViewGroup;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->clearNativeJobBeforeExport()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isPlayRequiredAfterExport()Z
    .locals 1

    .prologue
    .line 1974
    const/4 v0, 0x0

    return v0
.end method

.method public isShareViaCall()Z
    .locals 1

    .prologue
    .line 1969
    const/4 v0, 0x0

    return v0
.end method

.method public nativeProcAfterExportPause()V
    .locals 1

    .prologue
    .line 1984
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/app/video/editor/external/NativeInterface;->_native_terminate()V

    .line 1985
    return-void
.end method

.method public nativeProcBeforeExportResume()V
    .locals 1

    .prologue
    .line 1979
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/app/video/editor/external/NativeInterface;->_native_initfimc()V

    .line 1980
    return-void
.end method

.method public onExportCompleted()V
    .locals 1

    .prologue
    .line 1999
    invoke-static {}, Lcom/sec/android/app/ve/AppLifeCycleHandler;->isApplicationInForeground()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2000
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/app/video/editor/external/NativeInterface;->_native_terminate()V

    .line 2002
    :cond_0
    return-void
.end method

.method public onExportFailed()V
    .locals 0

    .prologue
    .line 1989
    invoke-direct {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExpAdapter;->refreshPreviewAfterExport()V

    .line 1990
    return-void
.end method

.method public onExportFinished(ZLandroid/net/Uri;)V
    .locals 3
    .param p1, "flag"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    .line 1959
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1960
    .local v0, "data":Landroid/content/Intent;
    const-string v1, "ve_expfilepath"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1961
    iget-object v1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExpAdapter;->this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->setResult(ILandroid/content/Intent;)V

    .line 1962
    iget-object v1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExpAdapter;->this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->finish()V

    .line 1963
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/sec/android/app/ve/util/CommonUtils;->cleanupRAWFiles(Ljava/util/List;)V

    .line 1965
    return-void
.end method

.method public onExportStopped()V
    .locals 0

    .prologue
    .line 1994
    invoke-direct {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExpAdapter;->refreshPreviewAfterExport()V

    .line 1995
    return-void
.end method
