.class Lcom/sec/android/app/storycam/view/ExportResolutionItemView$CustomRadioButton;
.super Landroid/view/View;
.source "ExportResolutionItemView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/storycam/view/ExportResolutionItemView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CustomRadioButton"
.end annotation


# static fields
.field private static final BACKGROUND_CIRCLE_DIFF:F

.field private static final INNER_RADIUS_DIFF:F

.field private static final OUTER_CIRCLE_STROKE_WIDTH:F

.field private static final VIEW_OFFSET:F


# instance fields
.field private mChecked:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 59
    const v0, 0x7f060389

    invoke-static {v0}, Lcom/sec/android/app/storycam/VEAppSpecific;->getPixDimen(I)I

    move-result v0

    int-to-float v0, v0

    sput v0, Lcom/sec/android/app/storycam/view/ExportResolutionItemView$CustomRadioButton;->VIEW_OFFSET:F

    .line 60
    const v0, 0x7f060387

    invoke-static {v0}, Lcom/sec/android/app/storycam/VEAppSpecific;->getPixDimen(I)I

    move-result v0

    int-to-float v0, v0

    sput v0, Lcom/sec/android/app/storycam/view/ExportResolutionItemView$CustomRadioButton;->OUTER_CIRCLE_STROKE_WIDTH:F

    .line 61
    const v0, 0x7f060386

    invoke-static {v0}, Lcom/sec/android/app/storycam/VEAppSpecific;->getPixDimen(I)I

    move-result v0

    int-to-float v0, v0

    sput v0, Lcom/sec/android/app/storycam/view/ExportResolutionItemView$CustomRadioButton;->INNER_RADIUS_DIFF:F

    .line 62
    const v0, 0x7f060388

    invoke-static {v0}, Lcom/sec/android/app/storycam/VEAppSpecific;->getPixDimen(I)I

    move-result v0

    int-to-float v0, v0

    sput v0, Lcom/sec/android/app/storycam/view/ExportResolutionItemView$CustomRadioButton;->BACKGROUND_CIRCLE_DIFF:F

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 67
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 64
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/storycam/view/ExportResolutionItemView$CustomRadioButton;->mChecked:Z

    .line 68
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 71
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 64
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/storycam/view/ExportResolutionItemView$CustomRadioButton;->mChecked:Z

    .line 72
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 75
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 64
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/storycam/view/ExportResolutionItemView$CustomRadioButton;->mChecked:Z

    .line 76
    return-void
.end method


# virtual methods
.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 10
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const v9, 0x7f0500a7

    const/high16 v8, 0x40000000    # 2.0f

    .line 85
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 86
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/view/ExportResolutionItemView$CustomRadioButton;->getWidth()I

    move-result v5

    .line 87
    .local v5, "width":I
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/view/ExportResolutionItemView$CustomRadioButton;->getHeight()I

    move-result v3

    .line 89
    .local v3, "height":I
    invoke-static {v5, v3}, Ljava/lang/Math;->min(II)I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v6, v8

    sget v7, Lcom/sec/android/app/storycam/view/ExportResolutionItemView$CustomRadioButton;->VIEW_OFFSET:F

    sub-float v2, v6, v7

    .line 90
    .local v2, "_radius":F
    int-to-float v6, v5

    div-float v0, v6, v8

    .line 91
    .local v0, "_centerX":F
    int-to-float v6, v3

    div-float v1, v6, v8

    .line 93
    .local v1, "_centerY":F
    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4}, Landroid/graphics/Paint;-><init>()V

    .line 94
    .local v4, "paint":Landroid/graphics/Paint;
    const/4 v6, 0x1

    invoke-virtual {v4, v6}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 95
    sget v6, Lcom/sec/android/app/storycam/view/ExportResolutionItemView$CustomRadioButton;->OUTER_CIRCLE_STROKE_WIDTH:F

    invoke-virtual {v4, v6}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 96
    sget-object v6, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v4, v6}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 97
    const v6, 0x7f0500a5

    invoke-static {v6}, Lcom/sec/android/app/ve/VEApp;->getColor(I)I

    move-result v6

    invoke-virtual {v4, v6}, Landroid/graphics/Paint;->setColor(I)V

    .line 98
    invoke-virtual {p1, v0, v1, v2, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 101
    sget v6, Lcom/sec/android/app/storycam/view/ExportResolutionItemView$CustomRadioButton;->BACKGROUND_CIRCLE_DIFF:F

    sub-float/2addr v2, v6

    .line 102
    sget-object v6, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v4, v6}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 103
    const v6, 0x7f0500a6

    invoke-static {v6}, Lcom/sec/android/app/ve/VEApp;->getColor(I)I

    move-result v6

    invoke-virtual {v4, v6}, Landroid/graphics/Paint;->setColor(I)V

    .line 104
    invoke-virtual {p1, v0, v1, v2, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 106
    iget-boolean v6, p0, Lcom/sec/android/app/storycam/view/ExportResolutionItemView$CustomRadioButton;->mChecked:Z

    if-eqz v6, :cond_0

    .line 107
    invoke-static {v5, v3}, Ljava/lang/Math;->min(II)I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v6, v8

    sget v7, Lcom/sec/android/app/storycam/view/ExportResolutionItemView$CustomRadioButton;->VIEW_OFFSET:F

    sub-float v2, v6, v7

    .line 108
    sget v6, Lcom/sec/android/app/storycam/view/ExportResolutionItemView$CustomRadioButton;->OUTER_CIRCLE_STROKE_WIDTH:F

    invoke-virtual {v4, v6}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 109
    sget-object v6, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v4, v6}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 110
    invoke-static {v9}, Lcom/sec/android/app/ve/VEApp;->getColor(I)I

    move-result v6

    invoke-virtual {v4, v6}, Landroid/graphics/Paint;->setColor(I)V

    .line 111
    invoke-virtual {p1, v0, v1, v2, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 112
    sget v6, Lcom/sec/android/app/storycam/view/ExportResolutionItemView$CustomRadioButton;->INNER_RADIUS_DIFF:F

    sub-float/2addr v2, v6

    .line 113
    sget-object v6, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v4, v6}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 114
    invoke-static {v9}, Lcom/sec/android/app/ve/VEApp;->getColor(I)I

    move-result v6

    invoke-virtual {v4, v6}, Landroid/graphics/Paint;->setColor(I)V

    .line 115
    invoke-virtual {p1, v0, v1, v2, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 117
    :cond_0
    return-void
.end method

.method public setChecked(Z)V
    .locals 0
    .param p1, "checked"    # Z

    .prologue
    .line 79
    iput-boolean p1, p0, Lcom/sec/android/app/storycam/view/ExportResolutionItemView$CustomRadioButton;->mChecked:Z

    .line 80
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/view/ExportResolutionItemView$CustomRadioButton;->invalidate()V

    .line 81
    return-void
.end method
