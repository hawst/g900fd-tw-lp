.class public Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioTransition;
.super Lorg/json/JSONObject;
.source "VEJSONAudioTransition.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lorg/json/JSONObject;-><init>()V

    .line 17
    return-void
.end method

.method protected constructor <init>(Lorg/json/JSONObject;)V
    .locals 1
    .param p1, "source"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 20
    invoke-virtual {p1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 21
    return-void
.end method


# virtual methods
.method public getDuration()I
    .locals 3

    .prologue
    .line 73
    const/4 v0, 0x0

    .line 75
    .local v0, "duration":I
    :try_start_0
    const-string v2, "TRANSITION_DURATION"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioTransition;->getInt(Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 80
    :goto_0
    return v0

    .line 77
    :catch_0
    move-exception v1

    .line 78
    .local v1, "jException":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getType()I
    .locals 3

    .prologue
    .line 44
    const/4 v1, 0x0

    .line 46
    .local v1, "type":I
    :try_start_0
    const-string v2, "TRANSITION_TYPE"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioTransition;->getInt(Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 51
    :goto_0
    return v1

    .line 48
    :catch_0
    move-exception v0

    .line 49
    .local v0, "jException":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public setDuration(I)V
    .locals 2
    .param p1, "duration"    # I

    .prologue
    .line 60
    :try_start_0
    const-string v1, "TRANSITION_DURATION"

    invoke-virtual {p0, v1, p1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioTransition;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 65
    :goto_0
    return-void

    .line 62
    :catch_0
    move-exception v0

    .line 63
    .local v0, "jException":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public setType(I)V
    .locals 2
    .param p1, "type"    # I

    .prologue
    .line 31
    :try_start_0
    const-string v1, "TRANSITION_TYPE"

    invoke-virtual {p0, v1, p1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioTransition;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 36
    :goto_0
    return-void

    .line 33
    :catch_0
    move-exception v0

    .line 34
    .local v0, "jException":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method
