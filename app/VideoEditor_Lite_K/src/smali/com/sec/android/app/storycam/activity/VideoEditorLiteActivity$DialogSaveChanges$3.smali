.class Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogSaveChanges$3;
.super Ljava/lang/Object;
.source "VideoEditorLiteActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogSaveChanges;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogSaveChanges;


# direct methods
.method constructor <init>(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogSaveChanges;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogSaveChanges$3;->this$1:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogSaveChanges;

    .line 1003
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "id"    # I

    .prologue
    .line 1005
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 1006
    sget v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->VERSION_CODE:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 1008
    invoke-static {}, Lcom/sec/android/app/storycam/summary/Summary;->stopSummary()V

    .line 1009
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->access$9(Z)V

    .line 1010
    iget-object v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogSaveChanges$3;->this$1:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogSaveChanges;

    # getter for: Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogSaveChanges;->activity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogSaveChanges;->access$0(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogSaveChanges;)Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1011
    iget-object v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogSaveChanges$3;->this$1:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogSaveChanges;

    # getter for: Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogSaveChanges;->activity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogSaveChanges;->access$0(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogSaveChanges;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 1013
    :cond_0
    return-void
.end method
