.class public interface abstract Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;
.super Ljava/lang/Object;
.source "UIManagerInterface.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;,
        Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityStateChangedListener;
    }
.end annotation


# virtual methods
.method public abstract addActivityStateChangedListener(Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityStateChangedListener;)V
.end method

.method public abstract changeActivityState(Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;ZZ)V
.end method

.method public abstract getCurrentConfig()Landroid/content/res/Configuration;
.end method

.method public abstract getCurrentState()Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;
.end method

.method public abstract getPreviewPlayer()Lcom/sec/android/app/ve/PreviewPlayerInterface;
.end method

.method public abstract getSummaryHandler()Landroid/os/Handler;
.end method

.method public abstract getUserSelectedBgmpos()I
.end method

.method public abstract isBgmStateOn()Z
.end method

.method public abstract isUserSelectedBgm()Z
.end method

.method public abstract launchDialog(I)V
.end method

.method public abstract launchGalleryForAddMedia()V
.end method

.method public abstract notifyActivityStateListeners(Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;)V
.end method

.method public abstract removeActivityEventsCallback(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ActivityEventsCallback;)V
.end method

.method public abstract removeActivityStateChangedListener(Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityStateChangedListener;)V
.end method

.method public abstract requestFocusForMainActivity(Z)V
.end method

.method public abstract requestForOptionMenuRefresh()V
.end method

.method public abstract saveCaption()V
.end method

.method public abstract setActivityEventsCallback(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ActivityEventsCallback;)V
.end method

.method public abstract setBgmState(Z)V
.end method

.method public abstract setPreviewPlayer(Lcom/sec/android/app/ve/PreviewPlayerInterface;)V
.end method

.method public abstract setUserSelectedBgm(Z)V
.end method

.method public abstract setUserSelectedBgmPos(I)V
.end method

.method public abstract showMaxItemsDialog()V
.end method
