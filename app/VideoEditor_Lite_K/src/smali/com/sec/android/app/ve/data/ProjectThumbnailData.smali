.class public Lcom/sec/android/app/ve/data/ProjectThumbnailData;
.super Ljava/lang/Object;
.source "ProjectThumbnailData.java"


# instance fields
.field private filePath:Ljava/lang/String;

.field private projectName:Ljava/lang/String;

.field private projectThumbnailBitmap:Landroid/graphics/Bitmap;

.field private projectThumbnailReflectionBitmap:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;Ljava/lang/String;)V
    .locals 0
    .param p1, "projectThumbnailBitmap"    # Landroid/graphics/Bitmap;
    .param p2, "projectThumbnailReflectionBitmap"    # Landroid/graphics/Bitmap;
    .param p3, "projectName"    # Ljava/lang/String;

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object p1, p0, Lcom/sec/android/app/ve/data/ProjectThumbnailData;->projectThumbnailBitmap:Landroid/graphics/Bitmap;

    .line 14
    iput-object p2, p0, Lcom/sec/android/app/ve/data/ProjectThumbnailData;->projectThumbnailReflectionBitmap:Landroid/graphics/Bitmap;

    .line 15
    iput-object p3, p0, Lcom/sec/android/app/ve/data/ProjectThumbnailData;->projectName:Ljava/lang/String;

    .line 16
    return-void
.end method


# virtual methods
.method public getFilePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/ve/data/ProjectThumbnailData;->filePath:Ljava/lang/String;

    return-object v0
.end method

.method public getProjectName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/ve/data/ProjectThumbnailData;->projectName:Ljava/lang/String;

    return-object v0
.end method

.method public getProjectThumbnailBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/android/app/ve/data/ProjectThumbnailData;->projectThumbnailBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getProjectThumbnailReflectionBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/ve/data/ProjectThumbnailData;->projectThumbnailReflectionBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public recycle()V
    .locals 2

    .prologue
    .line 60
    :try_start_0
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 61
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/ve/data/ProjectThumbnailData;->projectThumbnailBitmap:Landroid/graphics/Bitmap;

    .line 62
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/ve/data/ProjectThumbnailData;->projectThumbnailReflectionBitmap:Landroid/graphics/Bitmap;

    .line 63
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/ve/data/ProjectThumbnailData;->projectName:Ljava/lang/String;

    .line 64
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/ve/data/ProjectThumbnailData;->filePath:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 68
    :goto_0
    return-void

    .line 65
    :catch_0
    move-exception v0

    .line 66
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public setFilePath(Ljava/lang/String;)V
    .locals 0
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 44
    iput-object p1, p0, Lcom/sec/android/app/ve/data/ProjectThumbnailData;->filePath:Ljava/lang/String;

    .line 45
    return-void
.end method

.method public setProjectName(Ljava/lang/String;)V
    .locals 0
    .param p1, "projectName"    # Ljava/lang/String;

    .prologue
    .line 37
    iput-object p1, p0, Lcom/sec/android/app/ve/data/ProjectThumbnailData;->projectName:Ljava/lang/String;

    .line 38
    return-void
.end method

.method public setProjectThumbnailBitmap(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "projectThumbnailBitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 22
    iput-object p1, p0, Lcom/sec/android/app/ve/data/ProjectThumbnailData;->projectThumbnailBitmap:Landroid/graphics/Bitmap;

    .line 23
    return-void
.end method

.method public setProjectThumbnailReflectionBitmap(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "projectThumbnailReflectionBitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/sec/android/app/ve/data/ProjectThumbnailData;->projectThumbnailReflectionBitmap:Landroid/graphics/Bitmap;

    .line 31
    return-void
.end method
