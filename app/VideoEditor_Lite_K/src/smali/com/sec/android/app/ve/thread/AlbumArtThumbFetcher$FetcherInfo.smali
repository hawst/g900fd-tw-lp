.class Lcom/sec/android/app/ve/thread/AlbumArtThumbFetcher$FetcherInfo;
.super Ljava/lang/Object;
.source "AlbumArtThumbFetcher.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/ve/thread/AlbumArtThumbFetcher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "FetcherInfo"
.end annotation


# instance fields
.field private callback:Lcom/sec/android/app/ve/thread/AlbumArtThumbFetcher$AlbumArtThumbFetcherFetcherCallbak;

.field private filePath:Ljava/lang/String;

.field private position:I

.field private targetView:Landroid/view/View;


# direct methods
.method public constructor <init>(Ljava/lang/String;ILandroid/view/View;Lcom/sec/android/app/ve/thread/AlbumArtThumbFetcher$AlbumArtThumbFetcherFetcherCallbak;)V
    .locals 0
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "position"    # I
    .param p3, "targetView"    # Landroid/view/View;
    .param p4, "callback"    # Lcom/sec/android/app/ve/thread/AlbumArtThumbFetcher$AlbumArtThumbFetcherFetcherCallbak;

    .prologue
    .line 130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 131
    iput-object p1, p0, Lcom/sec/android/app/ve/thread/AlbumArtThumbFetcher$FetcherInfo;->filePath:Ljava/lang/String;

    .line 132
    iput p2, p0, Lcom/sec/android/app/ve/thread/AlbumArtThumbFetcher$FetcherInfo;->position:I

    .line 133
    iput-object p3, p0, Lcom/sec/android/app/ve/thread/AlbumArtThumbFetcher$FetcherInfo;->targetView:Landroid/view/View;

    .line 134
    iput-object p4, p0, Lcom/sec/android/app/ve/thread/AlbumArtThumbFetcher$FetcherInfo;->callback:Lcom/sec/android/app/ve/thread/AlbumArtThumbFetcher$AlbumArtThumbFetcherFetcherCallbak;

    .line 135
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/app/ve/thread/AlbumArtThumbFetcher$FetcherInfo;)Lcom/sec/android/app/ve/thread/AlbumArtThumbFetcher$AlbumArtThumbFetcherFetcherCallbak;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/sec/android/app/ve/thread/AlbumArtThumbFetcher$FetcherInfo;->callback:Lcom/sec/android/app/ve/thread/AlbumArtThumbFetcher$AlbumArtThumbFetcherFetcherCallbak;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/app/ve/thread/AlbumArtThumbFetcher$FetcherInfo;)Landroid/view/View;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/android/app/ve/thread/AlbumArtThumbFetcher$FetcherInfo;->targetView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/app/ve/thread/AlbumArtThumbFetcher$FetcherInfo;)I
    .locals 1

    .prologue
    .line 126
    iget v0, p0, Lcom/sec/android/app/ve/thread/AlbumArtThumbFetcher$FetcherInfo;->position:I

    return v0
.end method

.method static synthetic access$3(Lcom/sec/android/app/ve/thread/AlbumArtThumbFetcher$FetcherInfo;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/android/app/ve/thread/AlbumArtThumbFetcher$FetcherInfo;->filePath:Ljava/lang/String;

    return-object v0
.end method
