.class public interface abstract Lcom/sec/android/app/ve/PlayerInterface;
.super Ljava/lang/Object;
.source "PlayerInterface.java"


# virtual methods
.method public abstract getMaxVolume()I
.end method

.method public abstract getVolume()I
.end method

.method public abstract isPlaying()Z
.end method

.method public abstract pause()V
.end method

.method public abstract play(J)V
.end method

.method public abstract resume(J)V
.end method

.method public abstract seekTo(J)V
.end method

.method public abstract setActivity(Landroid/app/Activity;)V
.end method

.method public abstract setDataSource(Landroid/net/Uri;)V
.end method

.method public abstract setDataSource(Ljava/lang/String;)V
.end method

.method public abstract setVolume(I)V
.end method

.method public abstract stop()V
.end method
