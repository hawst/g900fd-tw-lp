.class public Lcom/sec/android/app/ve/preview/ScaleableVideoView;
.super Landroid/widget/VideoView;
.source "ScaleableVideoView.java"


# instance fields
.field private mRespectLayoutSize:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 20
    invoke-direct {p0, p1}, Landroid/widget/VideoView;-><init>(Landroid/content/Context;)V

    .line 21
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 16
    invoke-direct {p0, p1, p2}, Landroid/widget/VideoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 17
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 24
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/VideoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 25
    return-void
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 4
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/4 v3, 0x0

    .line 29
    iget-boolean v2, p0, Lcom/sec/android/app/ve/preview/ScaleableVideoView;->mRespectLayoutSize:Z

    if-eqz v2, :cond_0

    .line 30
    invoke-static {v3, p1}, Lcom/sec/android/app/ve/preview/ScaleableVideoView;->getDefaultSize(II)I

    move-result v1

    .line 31
    .local v1, "width":I
    invoke-static {v3, p2}, Lcom/sec/android/app/ve/preview/ScaleableVideoView;->getDefaultSize(II)I

    move-result v0

    .line 33
    .local v0, "height":I
    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/ve/preview/ScaleableVideoView;->setMeasuredDimension(II)V

    .line 37
    .end local v0    # "height":I
    .end local v1    # "width":I
    :goto_0
    return-void

    .line 36
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/VideoView;->onMeasure(II)V

    goto :goto_0
.end method

.method public setFixedSizeFromLayout(Z)V
    .locals 0
    .param p1, "respectLayoutSize"    # Z

    .prologue
    .line 40
    iput-boolean p1, p0, Lcom/sec/android/app/ve/preview/ScaleableVideoView;->mRespectLayoutSize:Z

    .line 41
    return-void
.end method
