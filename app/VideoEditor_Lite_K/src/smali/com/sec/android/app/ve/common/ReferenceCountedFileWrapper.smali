.class public Lcom/sec/android/app/ve/common/ReferenceCountedFileWrapper;
.super Ljava/lang/Object;
.source "ReferenceCountedFileWrapper.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x3bbd11a54227ab76L


# instance fields
.field private mDeleteGaurd:Z

.field private mFilePath:Ljava/lang/String;

.field private transient mRefCount:I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "filepath"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput v1, p0, Lcom/sec/android/app/ve/common/ReferenceCountedFileWrapper;->mRefCount:I

    .line 22
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/ve/common/ReferenceCountedFileWrapper;->mFilePath:Ljava/lang/String;

    .line 23
    iput v1, p0, Lcom/sec/android/app/ve/common/ReferenceCountedFileWrapper;->mRefCount:I

    .line 24
    return-void
.end method


# virtual methods
.method public addRef()I
    .locals 1

    .prologue
    .line 27
    iget v0, p0, Lcom/sec/android/app/ve/common/ReferenceCountedFileWrapper;->mRefCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/app/ve/common/ReferenceCountedFileWrapper;->mRefCount:I

    .line 28
    iget v0, p0, Lcom/sec/android/app/ve/common/ReferenceCountedFileWrapper;->mRefCount:I

    return v0
.end method

.method public delete(Z)V
    .locals 2
    .param p1, "forceDelete"    # Z

    .prologue
    .line 32
    iget v1, p0, Lcom/sec/android/app/ve/common/ReferenceCountedFileWrapper;->mRefCount:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/sec/android/app/ve/common/ReferenceCountedFileWrapper;->mRefCount:I

    .line 33
    iget-boolean v1, p0, Lcom/sec/android/app/ve/common/ReferenceCountedFileWrapper;->mDeleteGaurd:Z

    if-eqz v1, :cond_1

    .line 44
    :cond_0
    :goto_0
    return-void

    .line 37
    :cond_1
    iget v1, p0, Lcom/sec/android/app/ve/common/ReferenceCountedFileWrapper;->mRefCount:I

    if-lez v1, :cond_2

    if-eqz p1, :cond_0

    .line 38
    :cond_2
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/sec/android/app/ve/common/ReferenceCountedFileWrapper;->mFilePath:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 39
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 40
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    goto :goto_0
.end method

.method public preventDeletionOfFile(Z)V
    .locals 0
    .param p1, "gaurd"    # Z

    .prologue
    .line 47
    iput-boolean p1, p0, Lcom/sec/android/app/ve/common/ReferenceCountedFileWrapper;->mDeleteGaurd:Z

    .line 48
    return-void
.end method
