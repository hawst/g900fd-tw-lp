.class Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$11;
.super Ljava/lang/Object;
.source "VideoEditorLiteActivity.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->prepareContentView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$11;->this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;

    .line 1856
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$11;)Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;
    .locals 1

    .prologue
    .line 1856
    iget-object v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$11;->this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;

    return-object v0
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "arg1"    # Landroid/view/MotionEvent;

    .prologue
    .line 1862
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 1863
    iget-object v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$11;->this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;

    iget-object v1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$11;->this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;

    # getter for: Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->captionText:Lcom/sec/android/app/storycam/view/Caption1Line;
    invoke-static {v1}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->access$14(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;)Lcom/sec/android/app/storycam/view/Caption1Line;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/storycam/view/Caption1Line;->isFocused()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->access$18(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;Z)V

    .line 1864
    iget-object v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$11;->this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;

    iget-object v1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$11;->this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;

    # getter for: Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->captionText:Lcom/sec/android/app/storycam/view/Caption1Line;
    invoke-static {v1}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->access$14(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;)Lcom/sec/android/app/storycam/view/Caption1Line;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/storycam/view/Caption1Line;->getSelectionEnd()I

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->access$19(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;I)V

    .line 1865
    iget-object v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$11;->this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;

    # getter for: Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->isCaptionFcused:Z
    invoke-static {v0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->access$20(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1866
    iget-object v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$11;->this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;

    # getter for: Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->captionText:Lcom/sec/android/app/storycam/view/Caption1Line;
    invoke-static {v0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->access$14(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;)Lcom/sec/android/app/storycam/view/Caption1Line;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$11;->this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02000a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/storycam/view/Caption1Line;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1870
    :cond_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 1871
    iget-object v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$11;->this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;

    # getter for: Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->isCaptionFcused:Z
    invoke-static {v0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->access$20(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1872
    iget-object v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$11;->this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;

    # getter for: Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mSummaryHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->access$13(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$11$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$11$1;-><init>(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$11;)V

    .line 1881
    const-wide/16 v2, 0xfa

    .line 1872
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1887
    :cond_1
    const/4 v0, 0x0

    return v0
.end method
