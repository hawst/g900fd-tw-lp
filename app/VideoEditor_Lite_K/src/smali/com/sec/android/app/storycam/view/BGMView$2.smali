.class Lcom/sec/android/app/storycam/view/BGMView$2;
.super Ljava/lang/Object;
.source "BGMView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/storycam/view/BGMView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/storycam/view/BGMView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/storycam/view/BGMView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/storycam/view/BGMView$2;->this$0:Lcom/sec/android/app/storycam/view/BGMView;

    .line 136
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    const/4 v1, 0x1

    .line 139
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView$2;->this$0:Lcom/sec/android/app/storycam/view/BGMView;

    invoke-static {v0, v1}, Lcom/sec/android/app/storycam/view/BGMView;->access$1(Lcom/sec/android/app/storycam/view/BGMView;Z)V

    .line 140
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView$2;->this$0:Lcom/sec/android/app/storycam/view/BGMView;

    # getter for: Lcom/sec/android/app/storycam/view/BGMView;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;
    invoke-static {v0}, Lcom/sec/android/app/storycam/view/BGMView;->access$2(Lcom/sec/android/app/storycam/view/BGMView;)Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;->isBgmStateOn()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 141
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView$2;->this$0:Lcom/sec/android/app/storycam/view/BGMView;

    # getter for: Lcom/sec/android/app/storycam/view/BGMView;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;
    invoke-static {v0}, Lcom/sec/android/app/storycam/view/BGMView;->access$2(Lcom/sec/android/app/storycam/view/BGMView;)Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;->setBgmState(Z)V

    .line 142
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView$2;->this$0:Lcom/sec/android/app/storycam/view/BGMView;

    iget-object v1, p0, Lcom/sec/android/app/storycam/view/BGMView$2;->this$0:Lcom/sec/android/app/storycam/view/BGMView;

    # getter for: Lcom/sec/android/app/storycam/view/BGMView;->mSelectedPos:I
    invoke-static {v1}, Lcom/sec/android/app/storycam/view/BGMView;->access$3(Lcom/sec/android/app/storycam/view/BGMView;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/android/app/storycam/view/BGMView;->access$4(Lcom/sec/android/app/storycam/view/BGMView;I)V

    .line 143
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView$2;->this$0:Lcom/sec/android/app/storycam/view/BGMView;

    # invokes: Lcom/sec/android/app/storycam/view/BGMView;->setBGMVisibility()V
    invoke-static {v0}, Lcom/sec/android/app/storycam/view/BGMView;->access$5(Lcom/sec/android/app/storycam/view/BGMView;)V

    .line 144
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView$2;->this$0:Lcom/sec/android/app/storycam/view/BGMView;

    # getter for: Lcom/sec/android/app/storycam/view/BGMView;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v0}, Lcom/sec/android/app/storycam/view/BGMView;->access$0(Lcom/sec/android/app/storycam/view/BGMView;)Landroid/media/MediaPlayer;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView$2;->this$0:Lcom/sec/android/app/storycam/view/BGMView;

    # getter for: Lcom/sec/android/app/storycam/view/BGMView;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v0}, Lcom/sec/android/app/storycam/view/BGMView;->access$0(Lcom/sec/android/app/storycam/view/BGMView;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 145
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView$2;->this$0:Lcom/sec/android/app/storycam/view/BGMView;

    # getter for: Lcom/sec/android/app/storycam/view/BGMView;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v0}, Lcom/sec/android/app/storycam/view/BGMView;->access$0(Lcom/sec/android/app/storycam/view/BGMView;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    .line 151
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView$2;->this$0:Lcom/sec/android/app/storycam/view/BGMView;

    # getter for: Lcom/sec/android/app/storycam/view/BGMView;->mBgmTitle:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/storycam/view/BGMView;->access$8(Lcom/sec/android/app/storycam/view/BGMView;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/storycam/view/BGMView$2;->this$0:Lcom/sec/android/app/storycam/view/BGMView;

    # getter for: Lcom/sec/android/app/storycam/view/BGMView;->mBgmName:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/storycam/view/BGMView;->access$9(Lcom/sec/android/app/storycam/view/BGMView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 152
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView$2;->this$0:Lcom/sec/android/app/storycam/view/BGMView;

    # getter for: Lcom/sec/android/app/storycam/view/BGMView;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;
    invoke-static {v0}, Lcom/sec/android/app/storycam/view/BGMView;->access$2(Lcom/sec/android/app/storycam/view/BGMView;)Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;->requestForOptionMenuRefresh()V

    .line 153
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView$2;->this$0:Lcom/sec/android/app/storycam/view/BGMView;

    # getter for: Lcom/sec/android/app/storycam/view/BGMView;->mListAdapter:Lcom/sec/android/app/storycam/view/BGMView$BGMmBgmIconAdapter;
    invoke-static {v0}, Lcom/sec/android/app/storycam/view/BGMView;->access$10(Lcom/sec/android/app/storycam/view/BGMView;)Lcom/sec/android/app/storycam/view/BGMView$BGMmBgmIconAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/storycam/view/BGMView$BGMmBgmIconAdapter;->notifyDataSetChanged()V

    .line 154
    return-void

    .line 147
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView$2;->this$0:Lcom/sec/android/app/storycam/view/BGMView;

    # getter for: Lcom/sec/android/app/storycam/view/BGMView;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;
    invoke-static {v0}, Lcom/sec/android/app/storycam/view/BGMView;->access$2(Lcom/sec/android/app/storycam/view/BGMView;)Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;->setBgmState(Z)V

    .line 148
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView$2;->this$0:Lcom/sec/android/app/storycam/view/BGMView;

    iget-object v1, p0, Lcom/sec/android/app/storycam/view/BGMView$2;->this$0:Lcom/sec/android/app/storycam/view/BGMView;

    # getter for: Lcom/sec/android/app/storycam/view/BGMView;->mPrevSelPos:I
    invoke-static {v1}, Lcom/sec/android/app/storycam/view/BGMView;->access$6(Lcom/sec/android/app/storycam/view/BGMView;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/android/app/storycam/view/BGMView;->access$7(Lcom/sec/android/app/storycam/view/BGMView;I)V

    .line 149
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView$2;->this$0:Lcom/sec/android/app/storycam/view/BGMView;

    # invokes: Lcom/sec/android/app/storycam/view/BGMView;->setBGMVisibility()V
    invoke-static {v0}, Lcom/sec/android/app/storycam/view/BGMView;->access$5(Lcom/sec/android/app/storycam/view/BGMView;)V

    goto :goto_0
.end method
