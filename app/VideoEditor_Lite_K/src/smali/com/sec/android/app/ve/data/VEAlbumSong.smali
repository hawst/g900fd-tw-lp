.class public Lcom/sec/android/app/ve/data/VEAlbumSong;
.super Ljava/lang/Object;
.source "VEAlbumSong.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x4beb9cd612d39284L


# instance fields
.field private bAdded:Z

.field private mArtistName:Ljava/lang/String;

.field private mSongDuration:I

.field private mSongId:J

.field private mSongName:Ljava/lang/String;

.field private mSongPath:Ljava/lang/String;

.field private mSupported:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/ve/data/VEAlbumSong;->mSupported:Z

    .line 14
    return-void
.end method


# virtual methods
.method public getSongArtistName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/android/app/ve/data/VEAlbumSong;->mArtistName:Ljava/lang/String;

    return-object v0
.end method

.method public getSongDuration()I
    .locals 1

    .prologue
    .line 61
    iget v0, p0, Lcom/sec/android/app/ve/data/VEAlbumSong;->mSongDuration:I

    return v0
.end method

.method public getSongId()J
    .locals 2

    .prologue
    .line 52
    iget-wide v0, p0, Lcom/sec/android/app/ve/data/VEAlbumSong;->mSongId:J

    return-wide v0
.end method

.method public getSupported()Z
    .locals 1

    .prologue
    .line 89
    iget-boolean v0, p0, Lcom/sec/android/app/ve/data/VEAlbumSong;->mSupported:Z

    return v0
.end method

.method public getVESongName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/ve/data/VEAlbumSong;->mSongName:Ljava/lang/String;

    return-object v0
.end method

.method public getVESongPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/app/ve/data/VEAlbumSong;->mSongPath:Ljava/lang/String;

    return-object v0
.end method

.method public isSongAdded()Z
    .locals 1

    .prologue
    .line 81
    iget-boolean v0, p0, Lcom/sec/android/app/ve/data/VEAlbumSong;->bAdded:Z

    return v0
.end method

.method public setSongAdded(Z)V
    .locals 0
    .param p1, "added"    # Z

    .prologue
    .line 66
    iput-boolean p1, p0, Lcom/sec/android/app/ve/data/VEAlbumSong;->bAdded:Z

    .line 67
    return-void
.end method

.method public setSongArtistName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 71
    iput-object p1, p0, Lcom/sec/android/app/ve/data/VEAlbumSong;->mArtistName:Ljava/lang/String;

    .line 72
    return-void
.end method

.method public setSongDuration(I)V
    .locals 0
    .param p1, "aSongDuration"    # I

    .prologue
    .line 57
    iput p1, p0, Lcom/sec/android/app/ve/data/VEAlbumSong;->mSongDuration:I

    .line 58
    return-void
.end method

.method public setSongId(J)V
    .locals 1
    .param p1, "aSongId"    # J

    .prologue
    .line 48
    iput-wide p1, p0, Lcom/sec/android/app/ve/data/VEAlbumSong;->mSongId:J

    .line 49
    return-void
.end method

.method public setSupported(Z)V
    .locals 0
    .param p1, "supported"    # Z

    .prologue
    .line 85
    iput-boolean p1, p0, Lcom/sec/android/app/ve/data/VEAlbumSong;->mSupported:Z

    .line 86
    return-void
.end method

.method public setVESongName(Ljava/lang/String;)V
    .locals 0
    .param p1, "aSongName"    # Ljava/lang/String;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/sec/android/app/ve/data/VEAlbumSong;->mSongName:Ljava/lang/String;

    .line 31
    return-void
.end method

.method public setVESongPath(Ljava/lang/String;)V
    .locals 0
    .param p1, "aSongPath"    # Ljava/lang/String;

    .prologue
    .line 39
    iput-object p1, p0, Lcom/sec/android/app/ve/data/VEAlbumSong;->mSongPath:Ljava/lang/String;

    .line 40
    return-void
.end method
