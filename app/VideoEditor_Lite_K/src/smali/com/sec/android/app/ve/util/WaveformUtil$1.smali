.class Lcom/sec/android/app/ve/util/WaveformUtil$1;
.super Ljava/lang/Object;
.source "WaveformUtil.java"

# interfaces
.implements Lcom/sec/android/app/ve/thread/WaveformDataFetcher$WaveformDataFetcherCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/ve/util/WaveformUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/ve/util/WaveformUtil;


# direct methods
.method constructor <init>(Lcom/sec/android/app/ve/util/WaveformUtil;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/ve/util/WaveformUtil$1;->this$0:Lcom/sec/android/app/ve/util/WaveformUtil;

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public fetchingComplete(Lcom/sec/android/app/ve/data/WaveformDAO;)Z
    .locals 3
    .param p1, "waveformDAO"    # Lcom/sec/android/app/ve/data/WaveformDAO;

    .prologue
    .line 73
    iget-object v2, p0, Lcom/sec/android/app/ve/util/WaveformUtil$1;->this$0:Lcom/sec/android/app/ve/util/WaveformUtil;

    # invokes: Lcom/sec/android/app/ve/util/WaveformUtil;->addWaveform(Lcom/sec/android/app/ve/data/WaveformDAO;)Z
    invoke-static {v2, p1}, Lcom/sec/android/app/ve/util/WaveformUtil;->access$1(Lcom/sec/android/app/ve/util/WaveformUtil;Lcom/sec/android/app/ve/data/WaveformDAO;)Z

    .line 74
    iget-object v2, p0, Lcom/sec/android/app/ve/util/WaveformUtil$1;->this$0:Lcom/sec/android/app/ve/util/WaveformUtil;

    # getter for: Lcom/sec/android/app/ve/util/WaveformUtil;->clientsCallback:Ljava/util/concurrent/CopyOnWriteArrayList;
    invoke-static {v2}, Lcom/sec/android/app/ve/util/WaveformUtil;->access$0(Lcom/sec/android/app/ve/util/WaveformUtil;)Ljava/util/concurrent/CopyOnWriteArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 76
    .local v1, "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/app/ve/thread/WaveformDataFetcher$WaveformDataFetcherCallback;>;"
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 83
    const/4 v2, 0x1

    return v2

    .line 77
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/ve/thread/WaveformDataFetcher$WaveformDataFetcherCallback;

    .line 78
    .local v0, "callback":Lcom/sec/android/app/ve/thread/WaveformDataFetcher$WaveformDataFetcherCallback;
    invoke-interface {v0, p1}, Lcom/sec/android/app/ve/thread/WaveformDataFetcher$WaveformDataFetcherCallback;->fetchingComplete(Lcom/sec/android/app/ve/data/WaveformDAO;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 80
    iget-object v2, p0, Lcom/sec/android/app/ve/util/WaveformUtil$1;->this$0:Lcom/sec/android/app/ve/util/WaveformUtil;

    invoke-virtual {v2, v0}, Lcom/sec/android/app/ve/util/WaveformUtil;->deRegisterWaveformFetcherCallback(Lcom/sec/android/app/ve/thread/WaveformDataFetcher$WaveformDataFetcherCallback;)V

    goto :goto_0
.end method

.method public fetchingProgress(Lcom/sec/android/app/ve/data/WaveformDAO;I)Z
    .locals 4
    .param p1, "waveformDAO"    # Lcom/sec/android/app/ve/data/WaveformDAO;
    .param p2, "index"    # I

    .prologue
    .line 58
    iget-object v3, p0, Lcom/sec/android/app/ve/util/WaveformUtil$1;->this$0:Lcom/sec/android/app/ve/util/WaveformUtil;

    # getter for: Lcom/sec/android/app/ve/util/WaveformUtil;->clientsCallback:Ljava/util/concurrent/CopyOnWriteArrayList;
    invoke-static {v3}, Lcom/sec/android/app/ve/util/WaveformUtil;->access$0(Lcom/sec/android/app/ve/util/WaveformUtil;)Ljava/util/concurrent/CopyOnWriteArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 60
    .local v2, "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/app/ve/thread/WaveformDataFetcher$WaveformDataFetcherCallback;>;"
    const/4 v1, 0x0

    .line 61
    .local v1, "callbackValid":Z
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 68
    return v1

    .line 62
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/ve/thread/WaveformDataFetcher$WaveformDataFetcherCallback;

    .line 63
    .local v0, "callback":Lcom/sec/android/app/ve/thread/WaveformDataFetcher$WaveformDataFetcherCallback;
    invoke-interface {v0, p1, p2}, Lcom/sec/android/app/ve/thread/WaveformDataFetcher$WaveformDataFetcherCallback;->fetchingProgress(Lcom/sec/android/app/ve/data/WaveformDAO;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 65
    const/4 v1, 0x1

    goto :goto_0
.end method
