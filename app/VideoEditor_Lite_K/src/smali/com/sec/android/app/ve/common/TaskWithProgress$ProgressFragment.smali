.class public Lcom/sec/android/app/ve/common/TaskWithProgress$ProgressFragment;
.super Lcom/sec/android/app/ve/common/AndroidUtils$DlgFragment;
.source "TaskWithProgress.java"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/ve/common/TaskWithProgress;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ProgressFragment"
.end annotation


# instance fields
.field private mRoot:Landroid/widget/RelativeLayout;

.field private mSurfHolder:Landroid/view/SurfaceHolder;

.field private mTWP:Lcom/sec/android/app/ve/common/TaskWithProgress;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 143
    invoke-direct {p0}, Lcom/sec/android/app/ve/common/AndroidUtils$DlgFragment;-><init>()V

    .line 131
    iput-object v0, p0, Lcom/sec/android/app/ve/common/TaskWithProgress$ProgressFragment;->mRoot:Landroid/widget/RelativeLayout;

    .line 132
    iput-object v0, p0, Lcom/sec/android/app/ve/common/TaskWithProgress$ProgressFragment;->mSurfHolder:Landroid/view/SurfaceHolder;

    .line 133
    iput-object v0, p0, Lcom/sec/android/app/ve/common/TaskWithProgress$ProgressFragment;->mTWP:Lcom/sec/android/app/ve/common/TaskWithProgress;

    .line 144
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/app/ve/common/TaskWithProgress$ProgressFragment;)Lcom/sec/android/app/ve/common/TaskWithProgress;
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/sec/android/app/ve/common/TaskWithProgress$ProgressFragment;->mTWP:Lcom/sec/android/app/ve/common/TaskWithProgress;

    return-object v0
.end method

.method public static show(Lcom/sec/android/app/ve/common/TaskWithProgress;Landroid/app/Activity;)V
    .locals 1
    .param p0, "twp"    # Lcom/sec/android/app/ve/common/TaskWithProgress;
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 136
    new-instance v0, Lcom/sec/android/app/ve/common/TaskWithProgress$ProgressFragment;

    invoke-direct {v0}, Lcom/sec/android/app/ve/common/TaskWithProgress$ProgressFragment;-><init>()V

    .line 137
    .local v0, "dialog":Lcom/sec/android/app/ve/common/TaskWithProgress$ProgressFragment;
    invoke-virtual {v0, p0}, Lcom/sec/android/app/ve/common/TaskWithProgress$ProgressFragment;->setParam(Lcom/sec/android/app/ve/common/TaskWithProgress;)V

    .line 138
    invoke-static {v0, p1}, Lcom/sec/android/app/ve/common/AndroidUtils;->showDialog(Landroid/app/DialogFragment;Landroid/app/Activity;)V

    .line 139
    return-void
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x0

    .line 154
    :try_start_0
    sget v4, Lcom/sec/android/app/ve/R$layout;->wait_progress:I

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {p1, v4, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout;

    iput-object v4, p0, Lcom/sec/android/app/ve/common/TaskWithProgress$ProgressFragment;->mRoot:Landroid/widget/RelativeLayout;

    .line 156
    iget-object v4, p0, Lcom/sec/android/app/ve/common/TaskWithProgress$ProgressFragment;->mRoot:Landroid/widget/RelativeLayout;

    sget v6, Lcom/sec/android/app/ve/R$id;->wait_progress_view:I

    invoke-virtual {v4, v6}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/SurfaceView;

    .line 157
    .local v2, "sv":Landroid/view/SurfaceView;
    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Landroid/view/SurfaceView;->setZOrderOnTop(Z)V

    .line 158
    invoke-virtual {v2}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/ve/common/TaskWithProgress$ProgressFragment;->mSurfHolder:Landroid/view/SurfaceHolder;

    .line 159
    iget-object v4, p0, Lcom/sec/android/app/ve/common/TaskWithProgress$ProgressFragment;->mSurfHolder:Landroid/view/SurfaceHolder;

    const/4 v6, -0x3

    invoke-interface {v4, v6}, Landroid/view/SurfaceHolder;->setFormat(I)V

    .line 160
    iget-object v4, p0, Lcom/sec/android/app/ve/common/TaskWithProgress$ProgressFragment;->mSurfHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v4, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 162
    iget-object v4, p0, Lcom/sec/android/app/ve/common/TaskWithProgress$ProgressFragment;->mTWP:Lcom/sec/android/app/ve/common/TaskWithProgress;

    # getter for: Lcom/sec/android/app/ve/common/TaskWithProgress;->mMessage:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/app/ve/common/TaskWithProgress;->access$0(Lcom/sec/android/app/ve/common/TaskWithProgress;)Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/ve/common/TaskWithProgress$ProgressFragment;->mTWP:Lcom/sec/android/app/ve/common/TaskWithProgress;

    const-string v6, ""

    invoke-static {v4, v6}, Lcom/sec/android/app/ve/common/TaskWithProgress;->access$1(Lcom/sec/android/app/ve/common/TaskWithProgress;Ljava/lang/String;)V

    .line 163
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/ve/common/TaskWithProgress$ProgressFragment;->mRoot:Landroid/widget/RelativeLayout;

    sget v6, Lcom/sec/android/app/ve/R$id;->wait_progress_text:I

    invoke-virtual {v4, v6}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 164
    .local v3, "txtView":Landroid/widget/TextView;
    iget-object v4, p0, Lcom/sec/android/app/ve/common/TaskWithProgress$ProgressFragment;->mTWP:Lcom/sec/android/app/ve/common/TaskWithProgress;

    # getter for: Lcom/sec/android/app/ve/common/TaskWithProgress;->mMessage:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/app/ve/common/TaskWithProgress;->access$0(Lcom/sec/android/app/ve/common/TaskWithProgress;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 166
    invoke-virtual {p0}, Lcom/sec/android/app/ve/common/TaskWithProgress$ProgressFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    .line 167
    .local v0, "dialog":Landroid/app/Dialog;
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Landroid/app/Dialog;->requestWindowFeature(I)Z

    .line 168
    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 170
    iget-object v4, p0, Lcom/sec/android/app/ve/common/TaskWithProgress$ProgressFragment;->mRoot:Landroid/widget/RelativeLayout;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 175
    .end local v0    # "dialog":Landroid/app/Dialog;
    .end local v2    # "sv":Landroid/view/SurfaceView;
    .end local v3    # "txtView":Landroid/widget/TextView;
    :goto_0
    return-object v4

    .line 171
    :catch_0
    move-exception v1

    .line 172
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    move-object v4, v5

    .line 175
    goto :goto_0
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 221
    iput-object v0, p0, Lcom/sec/android/app/ve/common/TaskWithProgress$ProgressFragment;->mRoot:Landroid/widget/RelativeLayout;

    .line 222
    iput-object v0, p0, Lcom/sec/android/app/ve/common/TaskWithProgress$ProgressFragment;->mSurfHolder:Landroid/view/SurfaceHolder;

    .line 223
    iput-object v0, p0, Lcom/sec/android/app/ve/common/TaskWithProgress$ProgressFragment;->mTWP:Lcom/sec/android/app/ve/common/TaskWithProgress;

    .line 224
    # getter for: Lcom/sec/android/app/ve/common/TaskWithProgress;->mAnimThread:Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;
    invoke-static {}, Lcom/sec/android/app/ve/common/TaskWithProgress;->access$4()Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;->reset()V

    .line 225
    invoke-super {p0}, Lcom/sec/android/app/ve/common/AndroidUtils$DlgFragment;->onDestroyView()V

    .line 226
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 205
    invoke-super {p0}, Lcom/sec/android/app/ve/common/AndroidUtils$DlgFragment;->onPause()V

    .line 206
    # getter for: Lcom/sec/android/app/ve/common/TaskWithProgress;->mAnimThread:Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;
    invoke-static {}, Lcom/sec/android/app/ve/common/TaskWithProgress;->access$4()Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;->stopAnimation()V

    .line 207
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 198
    invoke-super {p0}, Lcom/sec/android/app/ve/common/AndroidUtils$DlgFragment;->onResume()V

    .line 199
    # getter for: Lcom/sec/android/app/ve/common/TaskWithProgress;->mAnimThread:Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;
    invoke-static {}, Lcom/sec/android/app/ve/common/TaskWithProgress;->access$4()Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;->startAnimation()V

    .line 200
    return-void
.end method

.method public setParam(Lcom/sec/android/app/ve/common/TaskWithProgress;)V
    .locals 0
    .param p1, "twp"    # Lcom/sec/android/app/ve/common/TaskWithProgress;

    .prologue
    .line 147
    iput-object p1, p0, Lcom/sec/android/app/ve/common/TaskWithProgress$ProgressFragment;->mTWP:Lcom/sec/android/app/ve/common/TaskWithProgress;

    .line 148
    return-void
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 4
    .param p1, "arg0"    # Landroid/view/SurfaceHolder;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I
    .param p4, "arg3"    # I

    .prologue
    .line 181
    iget-object v0, p0, Lcom/sec/android/app/ve/common/TaskWithProgress$ProgressFragment;->mTWP:Lcom/sec/android/app/ve/common/TaskWithProgress;

    # getter for: Lcom/sec/android/app/ve/common/TaskWithProgress;->mUIThreadTask:Z
    invoke-static {v0}, Lcom/sec/android/app/ve/common/TaskWithProgress;->access$2(Lcom/sec/android/app/ve/common/TaskWithProgress;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 183
    iget-object v0, p0, Lcom/sec/android/app/ve/common/TaskWithProgress$ProgressFragment;->mRoot:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/sec/android/app/ve/common/TaskWithProgress$ProgressFragment$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/ve/common/TaskWithProgress$ProgressFragment$1;-><init>(Lcom/sec/android/app/ve/common/TaskWithProgress$ProgressFragment;)V

    .line 191
    const-wide/16 v2, 0x32

    .line 183
    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/RelativeLayout;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 193
    :cond_0
    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 2
    .param p1, "arg0"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 211
    # getter for: Lcom/sec/android/app/ve/common/TaskWithProgress;->mAnimThread:Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;
    invoke-static {}, Lcom/sec/android/app/ve/common/TaskWithProgress;->access$4()Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/ve/common/TaskWithProgress$ProgressFragment;->mSurfHolder:Landroid/view/SurfaceHolder;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;->setSurface(Landroid/view/SurfaceHolder;)V

    .line 212
    # getter for: Lcom/sec/android/app/ve/common/TaskWithProgress;->mAnimThread:Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;
    invoke-static {}, Lcom/sec/android/app/ve/common/TaskWithProgress;->access$4()Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;->startAnimation()V

    .line 213
    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 0
    .param p1, "arg0"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 217
    return-void
.end method
