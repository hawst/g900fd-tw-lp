.class public Lcom/sec/android/app/storycam/projectsharing/ProjectPluginManager$AudioTransitionPluginImpl;
.super Ljava/lang/Object;
.source "ProjectPluginManager.java"

# interfaces
.implements Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework$AudioTransitionPlugin;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/storycam/projectsharing/ProjectPluginManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AudioTransitionPluginImpl"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 387
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public mapAudioTransitionFromGeneric(I)I
    .locals 1
    .param p1, "genericTransition"    # I

    .prologue
    .line 408
    const/4 v0, 0x0

    .line 409
    .local v0, "internalTransition":I
    packed-switch p1, :pswitch_data_0

    .line 420
    :goto_0
    return v0

    .line 411
    :pswitch_0
    const/16 v0, 0x14

    .line 412
    goto :goto_0

    .line 414
    :pswitch_1
    const/16 v0, 0x13

    .line 415
    goto :goto_0

    .line 417
    :pswitch_2
    const/16 v0, 0x15

    goto :goto_0

    .line 409
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public mapAudioTransitionToGeneric(I)I
    .locals 1
    .param p1, "internalTransition"    # I

    .prologue
    .line 391
    const/4 v0, 0x0

    .line 392
    .local v0, "genericTransition":I
    packed-switch p1, :pswitch_data_0

    .line 403
    :goto_0
    return v0

    .line 394
    :pswitch_0
    const/4 v0, 0x1

    .line 395
    goto :goto_0

    .line 397
    :pswitch_1
    const/4 v0, 0x3

    .line 398
    goto :goto_0

    .line 400
    :pswitch_2
    const/4 v0, 0x2

    goto :goto_0

    .line 392
    nop

    :pswitch_data_0
    .packed-switch 0x13
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
