.class Lcom/sec/android/app/ve/theme/Download$1;
.super Landroid/content/BroadcastReceiver;
.source "Download.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/ve/theme/Download;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/ve/theme/Download;


# direct methods
.method constructor <init>(Lcom/sec/android/app/ve/theme/Download;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/ve/theme/Download$1;->this$0:Lcom/sec/android/app/ve/theme/Download;

    .line 71
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "arg0"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 75
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 77
    .local v1, "intentAction":Ljava/lang/String;
    if-nez v1, :cond_1

    .line 97
    :cond_0
    :goto_0
    return-void

    .line 80
    :cond_1
    const-string v3, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 81
    const-string v3, "android.intent.action.PACKAGE_CHANGED"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 82
    const-string v3, "android.intent.action.PACKAGE_REPLACED"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 84
    :cond_2
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 86
    .local v0, "data":Landroid/net/Uri;
    if-eqz v0, :cond_0

    .line 89
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "package:"

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 91
    .local v2, "pckg":Ljava/lang/String;
    if-eqz v2, :cond_3

    iget-object v3, p0, Lcom/sec/android/app/ve/theme/Download$1;->this$0:Lcom/sec/android/app/ve/theme/Download;

    # invokes: Lcom/sec/android/app/ve/theme/Download;->getThemePackage()Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/ve/theme/Download;->access$0(Lcom/sec/android/app/ve/theme/Download;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/sec/android/app/ve/theme/Download$1;->this$0:Lcom/sec/android/app/ve/theme/Download;

    # invokes: Lcom/sec/android/app/ve/theme/Download;->getThemePackage()Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/ve/theme/Download;->access$0(Lcom/sec/android/app/ve/theme/Download;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 94
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/ve/theme/Download$1;->this$0:Lcom/sec/android/app/ve/theme/Download;

    # getter for: Lcom/sec/android/app/ve/theme/Download;->mAdapter:Lcom/sec/android/app/ve/theme/Download$Adapter;
    invoke-static {v3}, Lcom/sec/android/app/ve/theme/Download;->access$1(Lcom/sec/android/app/ve/theme/Download;)Lcom/sec/android/app/ve/theme/Download$Adapter;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 95
    iget-object v3, p0, Lcom/sec/android/app/ve/theme/Download$1;->this$0:Lcom/sec/android/app/ve/theme/Download;

    # getter for: Lcom/sec/android/app/ve/theme/Download;->mAdapter:Lcom/sec/android/app/ve/theme/Download$Adapter;
    invoke-static {v3}, Lcom/sec/android/app/ve/theme/Download;->access$1(Lcom/sec/android/app/ve/theme/Download;)Lcom/sec/android/app/ve/theme/Download$Adapter;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/ve/theme/Download$1;->this$0:Lcom/sec/android/app/ve/theme/Download;

    # getter for: Lcom/sec/android/app/ve/theme/Download;->mAdapter:Lcom/sec/android/app/ve/theme/Download$Adapter;
    invoke-static {v4}, Lcom/sec/android/app/ve/theme/Download;->access$1(Lcom/sec/android/app/ve/theme/Download;)Lcom/sec/android/app/ve/theme/Download$Adapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/ve/theme/Download$Adapter;->getThemePosition()I

    move-result v4

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/ve/theme/Download$Adapter;->onThemeInstalled(IZ)V

    goto :goto_0
.end method
