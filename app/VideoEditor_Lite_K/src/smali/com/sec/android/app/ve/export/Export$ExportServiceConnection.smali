.class Lcom/sec/android/app/ve/export/Export$ExportServiceConnection;
.super Ljava/lang/Object;
.source "Export.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/ve/export/Export;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ExportServiceConnection"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/ve/export/Export;


# direct methods
.method constructor <init>(Lcom/sec/android/app/ve/export/Export;)V
    .locals 0

    .prologue
    .line 212
    iput-object p1, p0, Lcom/sec/android/app/ve/export/Export$ExportServiceConnection;->this$0:Lcom/sec/android/app/ve/export/Export;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 6
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "boundService"    # Landroid/os/IBinder;

    .prologue
    .line 216
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/ve/export/Export$ExportServiceConnection;->this$0:Lcom/sec/android/app/ve/export/Export;

    invoke-static {p2}, Lcom/sec/android/app/ve/export/IExportService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/android/app/ve/export/IExportService;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/ve/export/Export;->access$0(Lcom/sec/android/app/ve/export/Export;Lcom/sec/android/app/ve/export/IExportService;)V

    .line 217
    iget-object v1, p0, Lcom/sec/android/app/ve/export/Export$ExportServiceConnection;->this$0:Lcom/sec/android/app/ve/export/Export;

    # getter for: Lcom/sec/android/app/ve/export/Export;->mService:Lcom/sec/android/app/ve/export/IExportService;
    invoke-static {v1}, Lcom/sec/android/app/ve/export/Export;->access$1(Lcom/sec/android/app/ve/export/Export;)Lcom/sec/android/app/ve/export/IExportService;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 218
    iget-object v1, p0, Lcom/sec/android/app/ve/export/Export$ExportServiceConnection;->this$0:Lcom/sec/android/app/ve/export/Export;

    iget-object v1, v1, Lcom/sec/android/app/ve/export/Export;->mAdapter:Lcom/sec/android/app/ve/export/Export$Adapter;

    invoke-virtual {v1}, Lcom/sec/android/app/ve/export/Export$Adapter;->clearNativeJobBeforeExport()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 219
    iget-object v1, p0, Lcom/sec/android/app/ve/export/Export$ExportServiceConnection;->this$0:Lcom/sec/android/app/ve/export/Export;

    # getter for: Lcom/sec/android/app/ve/export/Export;->mService:Lcom/sec/android/app/ve/export/IExportService;
    invoke-static {v1}, Lcom/sec/android/app/ve/export/Export;->access$1(Lcom/sec/android/app/ve/export/Export;)Lcom/sec/android/app/ve/export/IExportService;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/ve/export/IExportService;->startExport()V

    .line 220
    iget-object v1, p0, Lcom/sec/android/app/ve/export/Export$ExportServiceConnection;->this$0:Lcom/sec/android/app/ve/export/Export;

    const/4 v2, 0x1

    # invokes: Lcom/sec/android/app/ve/export/Export;->setExportState(I)V
    invoke-static {v1, v2}, Lcom/sec/android/app/ve/export/Export;->access$2(Lcom/sec/android/app/ve/export/Export;I)V

    .line 221
    iget-object v1, p0, Lcom/sec/android/app/ve/export/Export$ExportServiceConnection;->this$0:Lcom/sec/android/app/ve/export/Export;

    # getter for: Lcom/sec/android/app/ve/export/Export;->mService:Lcom/sec/android/app/ve/export/IExportService;
    invoke-static {v1}, Lcom/sec/android/app/ve/export/Export;->access$1(Lcom/sec/android/app/ve/export/Export;)Lcom/sec/android/app/ve/export/IExportService;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/ve/export/IExportService;->initNotification()V

    .line 222
    iget-object v1, p0, Lcom/sec/android/app/ve/export/Export$ExportServiceConnection;->this$0:Lcom/sec/android/app/ve/export/Export;

    # getter for: Lcom/sec/android/app/ve/export/Export;->mHandler:Lcom/sec/android/app/ve/export/Export$ThreadHandler;
    invoke-static {v1}, Lcom/sec/android/app/ve/export/Export;->access$3(Lcom/sec/android/app/ve/export/Export;)Lcom/sec/android/app/ve/export/Export$ThreadHandler;

    move-result-object v1

    const/4 v2, 0x3

    const-wide/16 v4, 0xa

    invoke-virtual {v1, v2, v4, v5}, Lcom/sec/android/app/ve/export/Export$ThreadHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 223
    iget-object v1, p0, Lcom/sec/android/app/ve/export/Export$ExportServiceConnection;->this$0:Lcom/sec/android/app/ve/export/Export;

    iget-object v1, v1, Lcom/sec/android/app/ve/export/Export;->mAdapter:Lcom/sec/android/app/ve/export/Export$Adapter;

    invoke-virtual {v1}, Lcom/sec/android/app/ve/export/Export$Adapter;->onExportStarted()V

    .line 232
    :cond_0
    :goto_0
    return-void

    .line 226
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/ve/export/Export$ExportServiceConnection;->this$0:Lcom/sec/android/app/ve/export/Export;

    # getter for: Lcom/sec/android/app/ve/export/Export;->mHandler:Lcom/sec/android/app/ve/export/Export$ThreadHandler;
    invoke-static {v1}, Lcom/sec/android/app/ve/export/Export;->access$3(Lcom/sec/android/app/ve/export/Export;)Lcom/sec/android/app/ve/export/Export$ThreadHandler;

    move-result-object v1

    const/4 v2, 0x0

    const-wide/16 v4, 0xa

    invoke-virtual {v1, v2, v4, v5}, Lcom/sec/android/app/ve/export/Export$ThreadHandler;->sendEmptyMessageDelayed(IJ)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 229
    :catch_0
    move-exception v0

    .line 230
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 235
    iget-object v0, p0, Lcom/sec/android/app/ve/export/Export$ExportServiceConnection;->this$0:Lcom/sec/android/app/ve/export/Export;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/export/Export;->access$0(Lcom/sec/android/app/ve/export/Export;Lcom/sec/android/app/ve/export/IExportService;)V

    .line 236
    return-void
.end method
