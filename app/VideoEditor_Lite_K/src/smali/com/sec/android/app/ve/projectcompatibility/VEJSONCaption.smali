.class public Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;
.super Lorg/json/JSONObject;
.source "VEJSONCaption.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lorg/json/JSONObject;-><init>()V

    .line 33
    return-void
.end method

.method protected constructor <init>(Lorg/json/JSONObject;)V
    .locals 7
    .param p1, "sourceCaption"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 35
    invoke-direct {p0}, Lorg/json/JSONObject;-><init>()V

    .line 38
    :try_start_0
    invoke-virtual {p1}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v4

    .line 39
    .local v4, "sourceIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    if-eqz v4, :cond_1

    .line 40
    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_2

    .line 57
    .end local v4    # "sourceIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_1
    :goto_1
    return-void

    .line 41
    .restart local v4    # "sourceIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 42
    .local v3, "key":Ljava/lang/String;
    const-string v5, "ANIM_ARRAY"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 43
    invoke-static {p1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->getAnimationList(Lorg/json/JSONObject;)Lorg/json/JSONArray;

    move-result-object v0

    .line 44
    .local v0, "animList":Lorg/json/JSONArray;
    if-eqz v0, :cond_0

    .line 45
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_2
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v5

    if-ge v2, v5, :cond_0

    .line 46
    new-instance v5, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAnimation;

    invoke-virtual {v0, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAnimation;-><init>(Lorg/json/JSONObject;)V

    invoke-virtual {p0, v5}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->addAnimation(Lcom/sec/android/app/ve/projectcompatibility/VEJSONAnimation;)V

    .line 45
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 51
    .end local v0    # "animList":Lorg/json/JSONArray;
    .end local v2    # "i":I
    :cond_3
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {p0, v3, v5}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 55
    .end local v3    # "key":Ljava/lang/String;
    .end local v4    # "sourceIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :catch_0
    move-exception v1

    .line 56
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_1
.end method

.method private static getAnimationList(Lorg/json/JSONObject;)Lorg/json/JSONArray;
    .locals 3
    .param p0, "fromElement"    # Lorg/json/JSONObject;

    .prologue
    .line 849
    const/4 v1, 0x0

    .line 851
    .local v1, "list":Lorg/json/JSONArray;
    :try_start_0
    const-string v2, "ANIM_ARRAY"

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 856
    :goto_0
    return-object v1

    .line 853
    :catch_0
    move-exception v0

    .line 854
    .local v0, "jException":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public addAnimation(Lcom/sec/android/app/ve/projectcompatibility/VEJSONAnimation;)V
    .locals 5
    .param p1, "animation"    # Lcom/sec/android/app/ve/projectcompatibility/VEJSONAnimation;

    .prologue
    .line 824
    const/4 v0, 0x0

    .line 826
    .local v0, "animList":Lorg/json/JSONArray;
    :try_start_0
    const-string v4, "ANIM_ARRAY"

    invoke-virtual {p0, v4}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 837
    :goto_0
    invoke-virtual {v0, p1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 838
    return-void

    .line 828
    :catch_0
    move-exception v3

    .line 830
    .local v3, "jException":Lorg/json/JSONException;
    :try_start_1
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 831
    .end local v0    # "animList":Lorg/json/JSONArray;
    .local v1, "animList":Lorg/json/JSONArray;
    :try_start_2
    const-string v4, "ANIM_ARRAY"

    invoke-virtual {p0, v4, v1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_2

    move-object v0, v1

    .end local v1    # "animList":Lorg/json/JSONArray;
    .restart local v0    # "animList":Lorg/json/JSONArray;
    goto :goto_0

    .line 833
    :catch_1
    move-exception v2

    .line 834
    .local v2, "e":Lorg/json/JSONException;
    :goto_1
    invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0

    .line 833
    .end local v0    # "animList":Lorg/json/JSONArray;
    .end local v2    # "e":Lorg/json/JSONException;
    .restart local v1    # "animList":Lorg/json/JSONArray;
    :catch_2
    move-exception v2

    move-object v0, v1

    .end local v1    # "animList":Lorg/json/JSONArray;
    .restart local v0    # "animList":Lorg/json/JSONArray;
    goto :goto_1
.end method

.method public getAnimationList()Lorg/json/JSONArray;
    .locals 1

    .prologue
    .line 845
    invoke-static {p0}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->getAnimationList(Lorg/json/JSONObject;)Lorg/json/JSONArray;

    move-result-object v0

    return-object v0
.end method

.method public getCaptionAlignment()I
    .locals 3

    .prologue
    .line 407
    const/4 v0, 0x0

    .line 409
    .local v0, "alignment":I
    :try_start_0
    const-string v2, "CAPTION_ALIGNMENT"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->getInt(Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 414
    :goto_0
    return v0

    .line 411
    :catch_0
    move-exception v1

    .line 412
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getCaptionBottomCoordinate()D
    .locals 4

    .prologue
    .line 222
    const-wide/16 v0, 0x0

    .line 224
    .local v0, "bottom":D
    :try_start_0
    const-string v3, "BOTTOM"

    invoke-virtual {p0, v3}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->getDouble(Ljava/lang/String;)D
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 229
    :goto_0
    return-wide v0

    .line 226
    :catch_0
    move-exception v2

    .line 227
    .local v2, "jException":Lorg/json/JSONException;
    invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getCaptionCoordinateSpaceX()D
    .locals 4

    .prologue
    .line 238
    const-wide/16 v0, 0x0

    .line 240
    .local v0, "coordinateSpaceX":D
    :try_start_0
    const-string v3, "CAPTION_COORDINATE_REF_X"

    invoke-virtual {p0, v3}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->getDouble(Ljava/lang/String;)D
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 245
    :goto_0
    return-wide v0

    .line 242
    :catch_0
    move-exception v2

    .line 243
    .local v2, "jException":Lorg/json/JSONException;
    invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getCaptionCoordinateSpaceY()D
    .locals 4

    .prologue
    .line 254
    const-wide/16 v0, 0x0

    .line 256
    .local v0, "coordinateSpaceY":D
    :try_start_0
    const-string v3, "CAPTION_COORDINATE_REF_Y"

    invoke-virtual {p0, v3}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->getDouble(Ljava/lang/String;)D
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 261
    :goto_0
    return-wide v0

    .line 258
    :catch_0
    move-exception v2

    .line 259
    .local v2, "jException":Lorg/json/JSONException;
    invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getCaptionLeftCoordinate()D
    .locals 4

    .prologue
    .line 174
    const-wide/16 v2, 0x0

    .line 176
    .local v2, "left":D
    :try_start_0
    const-string v1, "LEFT"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->getDouble(Ljava/lang/String;)D
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 181
    :goto_0
    return-wide v2

    .line 178
    :catch_0
    move-exception v0

    .line 179
    .local v0, "jException":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getCaptionRightCoordinate()D
    .locals 4

    .prologue
    .line 206
    const-wide/16 v2, 0x0

    .line 208
    .local v2, "right":D
    :try_start_0
    const-string v1, "RIGHT"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->getDouble(Ljava/lang/String;)D
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 213
    :goto_0
    return-wide v2

    .line 210
    :catch_0
    move-exception v0

    .line 211
    .local v0, "jException":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getCaptionText()Ljava/lang/String;
    .locals 3

    .prologue
    .line 78
    const/4 v1, 0x0

    .line 80
    .local v1, "text":Ljava/lang/String;
    :try_start_0
    const-string v2, "CAPTION_TEXT"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 85
    :goto_0
    return-object v1

    .line 82
    :catch_0
    move-exception v0

    .line 83
    .local v0, "jException":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getCaptionTheme()I
    .locals 3

    .prologue
    .line 440
    const/4 v1, 0x0

    .line 442
    .local v1, "theme":I
    :try_start_0
    const-string v2, "CAPTION_THEME"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->getInt(Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 447
    :goto_0
    return v1

    .line 444
    :catch_0
    move-exception v0

    .line 445
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getCaptionTopCoordinate()D
    .locals 4

    .prologue
    .line 190
    const-wide/16 v2, 0x0

    .line 192
    .local v2, "top":D
    :try_start_0
    const-string v1, "TOP"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->getDouble(Ljava/lang/String;)D
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 197
    :goto_0
    return-wide v2

    .line 194
    :catch_0
    move-exception v0

    .line 195
    .local v0, "jException":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getDefaultAlpha()F
    .locals 4

    .prologue
    .line 731
    const/high16 v0, 0x3f800000    # 1.0f

    .line 733
    .local v0, "alpha":F
    :try_start_0
    const-string v2, "DEFAULT_ALPHA"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->getDouble(Ljava/lang/String;)D
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    double-to-float v0, v2

    .line 738
    :goto_0
    return v0

    .line 735
    :catch_0
    move-exception v1

    .line 736
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getDefaultRotation()F
    .locals 4

    .prologue
    .line 762
    const/4 v1, 0x0

    .line 764
    .local v1, "rotation":F
    :try_start_0
    const-string v2, "DEFAULT_ROTATION"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->getDouble(Ljava/lang/String;)D
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    double-to-float v1, v2

    .line 769
    :goto_0
    return v1

    .line 766
    :catch_0
    move-exception v0

    .line 767
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getDefaultScaleX()F
    .locals 4

    .prologue
    .line 685
    const/high16 v1, 0x3f800000    # 1.0f

    .line 687
    .local v1, "scaleX":F
    :try_start_0
    const-string v2, "DEFAULT_SCALEX"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->getDouble(Ljava/lang/String;)D
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    double-to-float v1, v2

    .line 692
    :goto_0
    return v1

    .line 689
    :catch_0
    move-exception v0

    .line 690
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getDefaultScaleY()F
    .locals 4

    .prologue
    .line 700
    const/high16 v1, 0x3f800000    # 1.0f

    .line 702
    .local v1, "scaleY":F
    :try_start_0
    const-string v2, "DEFAULT_SCALEY"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->getDouble(Ljava/lang/String;)D
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    double-to-float v1, v2

    .line 707
    :goto_0
    return v1

    .line 704
    :catch_0
    move-exception v0

    .line 705
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getDefaultTranslationX()F
    .locals 4

    .prologue
    .line 637
    const/4 v1, 0x0

    .line 639
    .local v1, "transX":F
    :try_start_0
    const-string v2, "DEFAULT_TRANSLATE_X"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->getDouble(Ljava/lang/String;)D
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    double-to-float v1, v2

    .line 644
    :goto_0
    return v1

    .line 641
    :catch_0
    move-exception v0

    .line 642
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getDefaultTranslationY()F
    .locals 4

    .prologue
    .line 652
    const/4 v1, 0x0

    .line 654
    .local v1, "transY":F
    :try_start_0
    const-string v2, "DEFAULT_TRANSLATE_Y"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->getDouble(Ljava/lang/String;)D
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    double-to-float v1, v2

    .line 659
    :goto_0
    return v1

    .line 656
    :catch_0
    move-exception v0

    .line 657
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getElementID()I
    .locals 3

    .prologue
    .line 376
    const/4 v1, 0x0

    .line 378
    .local v1, "id":I
    :try_start_0
    const-string v2, "CAPTION_ELEMENT_ID"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->getInt(Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 383
    :goto_0
    return v1

    .line 380
    :catch_0
    move-exception v0

    .line 381
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getFontColor()I
    .locals 3

    .prologue
    .line 525
    const/4 v0, 0x0

    .line 527
    .local v0, "color":I
    :try_start_0
    const-string v2, "FONT_COLOR"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->getInt(Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 532
    :goto_0
    return v0

    .line 529
    :catch_0
    move-exception v1

    .line 530
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getFontFilePath()Ljava/lang/String;
    .locals 3

    .prologue
    .line 468
    const/4 v1, 0x0

    .line 470
    .local v1, "path":Ljava/lang/String;
    :try_start_0
    const-string v2, "FONT_FILE"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 475
    :goto_0
    return-object v1

    .line 472
    :catch_0
    move-exception v0

    .line 473
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getFontSize()I
    .locals 3

    .prologue
    .line 496
    const/4 v1, 0x0

    .line 498
    .local v1, "fontSize":I
    :try_start_0
    const-string v2, "FONT_SIZE"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->getInt(Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 503
    :goto_0
    return v1

    .line 500
    :catch_0
    move-exception v0

    .line 501
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getPivotX()F
    .locals 4

    .prologue
    .line 794
    const/4 v1, 0x0

    .line 796
    .local v1, "pivotX":F
    :try_start_0
    const-string v2, "PIVOT_X"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->getDouble(Ljava/lang/String;)D
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    double-to-float v1, v2

    .line 801
    :goto_0
    return v1

    .line 798
    :catch_0
    move-exception v0

    .line 799
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getPivotY()F
    .locals 4

    .prologue
    .line 809
    const/4 v1, 0x0

    .line 811
    .local v1, "pivotY":F
    :try_start_0
    const-string v2, "PIVOT_Y"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->getDouble(Ljava/lang/String;)D
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    double-to-float v1, v2

    .line 816
    :goto_0
    return v1

    .line 813
    :catch_0
    move-exception v0

    .line 814
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getRawFilePath()Ljava/lang/String;
    .locals 3

    .prologue
    .line 106
    const/4 v1, 0x0

    .line 108
    .local v1, "path":Ljava/lang/String;
    :try_start_0
    const-string v2, "CAPTION_RAW_FILE"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 113
    :goto_0
    return-object v1

    .line 110
    :catch_0
    move-exception v0

    .line 111
    .local v0, "jException":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getShadowColor()I
    .locals 3

    .prologue
    .line 604
    const/4 v1, 0x0

    .line 606
    .local v1, "shadowColor":I
    :try_start_0
    const-string v2, "SHADOW_COLOR"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->getInt(Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 611
    :goto_0
    return v1

    .line 608
    :catch_0
    move-exception v0

    .line 609
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getShadowRadius()I
    .locals 3

    .prologue
    .line 589
    const/4 v1, 0x0

    .line 591
    .local v1, "shadowR":I
    :try_start_0
    const-string v2, "SHADOW_R"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->getInt(Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 596
    :goto_0
    return v1

    .line 593
    :catch_0
    move-exception v0

    .line 594
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getShadowX()I
    .locals 3

    .prologue
    .line 559
    const/4 v1, 0x0

    .line 561
    .local v1, "shadowX":I
    :try_start_0
    const-string v2, "SHADOW_X"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->getInt(Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 566
    :goto_0
    return v1

    .line 563
    :catch_0
    move-exception v0

    .line 564
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getShadowY()I
    .locals 3

    .prologue
    .line 574
    const/4 v1, 0x0

    .line 576
    .local v1, "shadowY":I
    :try_start_0
    const-string v2, "SHADOW_Y"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->getInt(Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 581
    :goto_0
    return v1

    .line 578
    :catch_0
    move-exception v0

    .line 579
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getStoryboardEndFrame()I
    .locals 3

    .prologue
    .line 316
    const/4 v0, 0x0

    .line 318
    .local v0, "endFrame":I
    :try_start_0
    const-string v2, "STORYBOARD_END_FRAME"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->getInt(Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 323
    :goto_0
    return v0

    .line 320
    :catch_0
    move-exception v1

    .line 321
    .local v1, "jException":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getStoryboardReferenceFrameRate()I
    .locals 3

    .prologue
    .line 346
    const/16 v0, 0x1e

    .line 348
    .local v0, "fps":I
    :try_start_0
    const-string v2, "STORYBOARD_REF_FRAMERATE"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->getInt(Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 353
    :goto_0
    return v0

    .line 350
    :catch_0
    move-exception v1

    .line 351
    .local v1, "jException":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getStoryboardStartFrame()I
    .locals 3

    .prologue
    .line 286
    const/4 v1, 0x0

    .line 288
    .local v1, "startFrame":I
    :try_start_0
    const-string v2, "STORYBOARD_START_FRAME"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->getInt(Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 293
    :goto_0
    return v1

    .line 290
    :catch_0
    move-exception v0

    .line 291
    .local v0, "jException":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public isEditable()Z
    .locals 3

    .prologue
    .line 134
    const/4 v0, 0x1

    .line 136
    .local v0, "editable":Z
    :try_start_0
    const-string v2, "CAPTION_EDITABLE"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->getBoolean(Ljava/lang/String;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 141
    :goto_0
    return v0

    .line 138
    :catch_0
    move-exception v1

    .line 139
    .local v1, "jException":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public setCaptionAlignment(I)V
    .locals 2
    .param p1, "alignment"    # I

    .prologue
    .line 393
    :try_start_0
    const-string v1, "CAPTION_ALIGNMENT"

    invoke-virtual {p0, v1, p1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 398
    :goto_0
    return-void

    .line 395
    :catch_0
    move-exception v0

    .line 396
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public setCaptionRawFilePath(Ljava/lang/String;)V
    .locals 2
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 94
    :try_start_0
    const-string v1, "CAPTION_RAW_FILE"

    invoke-virtual {p0, v1, p1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 99
    :goto_0
    return-void

    .line 96
    :catch_0
    move-exception v0

    .line 97
    .local v0, "jException":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public setCaptionRect(DDDDDD)V
    .locals 3
    .param p1, "left"    # D
    .param p3, "top"    # D
    .param p5, "right"    # D
    .param p7, "bottom"    # D
    .param p9, "coordinateSpaceX"    # D
    .param p11, "coordinateSpaceY"    # D

    .prologue
    .line 156
    :try_start_0
    const-string v1, "LEFT"

    invoke-virtual {p0, v1, p1, p2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 157
    const-string v1, "RIGHT"

    invoke-virtual {p0, v1, p5, p6}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 158
    const-string v1, "TOP"

    invoke-virtual {p0, v1, p3, p4}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 159
    const-string v1, "BOTTOM"

    invoke-virtual {p0, v1, p7, p8}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 160
    const-string v1, "CAPTION_COORDINATE_REF_X"

    invoke-virtual {p0, v1, p9, p10}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 161
    const-string v1, "CAPTION_COORDINATE_REF_Y"

    invoke-virtual {p0, v1, p11, p12}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->put(Ljava/lang/String;D)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 166
    :goto_0
    return-void

    .line 163
    :catch_0
    move-exception v0

    .line 164
    .local v0, "jException":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public setCaptionText(Ljava/lang/String;)V
    .locals 2
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 66
    :try_start_0
    const-string v1, "CAPTION_TEXT"

    invoke-virtual {p0, v1, p1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 71
    :goto_0
    return-void

    .line 68
    :catch_0
    move-exception v0

    .line 69
    .local v0, "jException":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public setCaptionTheme(I)V
    .locals 2
    .param p1, "theme"    # I

    .prologue
    .line 426
    :try_start_0
    const-string v1, "CAPTION_THEME"

    invoke-virtual {p0, v1, p1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 431
    :goto_0
    return-void

    .line 428
    :catch_0
    move-exception v0

    .line 429
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public setDefaultAlpha(F)V
    .locals 4
    .param p1, "alpha"    # F

    .prologue
    .line 719
    :try_start_0
    const-string v1, "DEFAULT_ALPHA"

    float-to-double v2, p1

    invoke-virtual {p0, v1, v2, v3}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->put(Ljava/lang/String;D)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 724
    :goto_0
    return-void

    .line 721
    :catch_0
    move-exception v0

    .line 722
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public setDefaultRotation(F)V
    .locals 4
    .param p1, "rotationAngle"    # F

    .prologue
    .line 750
    :try_start_0
    const-string v1, "DEFAULT_ROTATION"

    float-to-double v2, p1

    invoke-virtual {p0, v1, v2, v3}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->put(Ljava/lang/String;D)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 755
    :goto_0
    return-void

    .line 752
    :catch_0
    move-exception v0

    .line 753
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public setDefaultScale(FF)V
    .locals 4
    .param p1, "scaleX"    # F
    .param p2, "scaleY"    # F

    .prologue
    .line 672
    :try_start_0
    const-string v1, "DEFAULT_SCALEX"

    float-to-double v2, p1

    invoke-virtual {p0, v1, v2, v3}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 673
    const-string v1, "DEFAULT_SCALEY"

    float-to-double v2, p2

    invoke-virtual {p0, v1, v2, v3}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->put(Ljava/lang/String;D)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 678
    :goto_0
    return-void

    .line 675
    :catch_0
    move-exception v0

    .line 676
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public setDefaultTranslations(FF)V
    .locals 4
    .param p1, "translationX"    # F
    .param p2, "translationY"    # F

    .prologue
    .line 624
    :try_start_0
    const-string v1, "DEFAULT_TRANSLATE_X"

    float-to-double v2, p1

    invoke-virtual {p0, v1, v2, v3}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 625
    const-string v1, "DEFAULT_TRANSLATE_Y"

    float-to-double v2, p2

    invoke-virtual {p0, v1, v2, v3}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->put(Ljava/lang/String;D)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 630
    :goto_0
    return-void

    .line 627
    :catch_0
    move-exception v0

    .line 628
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public setEditable(Z)V
    .locals 2
    .param p1, "editable"    # Z

    .prologue
    .line 122
    :try_start_0
    const-string v1, "CAPTION_EDITABLE"

    invoke-virtual {p0, v1, p1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 127
    :goto_0
    return-void

    .line 124
    :catch_0
    move-exception v0

    .line 125
    .local v0, "jException":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public setElementID(I)V
    .locals 2
    .param p1, "elementId"    # I

    .prologue
    .line 363
    :try_start_0
    const-string v1, "CAPTION_ELEMENT_ID"

    invoke-virtual {p0, v1, p1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 368
    :goto_0
    return-void

    .line 365
    :catch_0
    move-exception v0

    .line 366
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public setFontColor(I)V
    .locals 2
    .param p1, "color"    # I

    .prologue
    .line 513
    :try_start_0
    const-string v1, "FONT_COLOR"

    invoke-virtual {p0, v1, p1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 518
    :goto_0
    return-void

    .line 515
    :catch_0
    move-exception v0

    .line 516
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public setFontFilePath(Ljava/lang/String;)V
    .locals 2
    .param p1, "pathToFontFile"    # Ljava/lang/String;

    .prologue
    .line 456
    :try_start_0
    const-string v1, "FONT_FILE"

    invoke-virtual {p0, v1, p1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 461
    :goto_0
    return-void

    .line 458
    :catch_0
    move-exception v0

    .line 459
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public setFontShadow(IIII)V
    .locals 2
    .param p1, "shadowX"    # I
    .param p2, "shadowY"    # I
    .param p3, "shadowR"    # I
    .param p4, "shadowColor"    # I

    .prologue
    .line 544
    :try_start_0
    const-string v1, "SHADOW_COLOR"

    invoke-virtual {p0, v1, p4}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 545
    const-string v1, "SHADOW_X"

    invoke-virtual {p0, v1, p1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 546
    const-string v1, "SHADOW_Y"

    invoke-virtual {p0, v1, p2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 547
    const-string v1, "SHADOW_R"

    invoke-virtual {p0, v1, p3}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 552
    :goto_0
    return-void

    .line 549
    :catch_0
    move-exception v0

    .line 550
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public setFontSize(I)V
    .locals 2
    .param p1, "fontSize"    # I

    .prologue
    .line 484
    :try_start_0
    const-string v1, "FONT_SIZE"

    invoke-virtual {p0, v1, p1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 489
    :goto_0
    return-void

    .line 486
    :catch_0
    move-exception v0

    .line 487
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public setPivotPoints(FF)V
    .locals 4
    .param p1, "pivotX"    # F
    .param p2, "pivotY"    # F

    .prologue
    .line 781
    :try_start_0
    const-string v1, "PIVOT_X"

    float-to-double v2, p1

    invoke-virtual {p0, v1, v2, v3}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 782
    const-string v1, "PIVOT_Y"

    float-to-double v2, p2

    invoke-virtual {p0, v1, v2, v3}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->put(Ljava/lang/String;D)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 787
    :goto_0
    return-void

    .line 784
    :catch_0
    move-exception v0

    .line 785
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public setStoryboardEndFrame(I)V
    .locals 2
    .param p1, "frame"    # I

    .prologue
    .line 304
    :try_start_0
    const-string v1, "STORYBOARD_END_FRAME"

    invoke-virtual {p0, v1, p1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 309
    :goto_0
    return-void

    .line 306
    :catch_0
    move-exception v0

    .line 307
    .local v0, "jException":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public setStoryboardReferenceFrameRate(I)V
    .locals 2
    .param p1, "fps"    # I

    .prologue
    .line 334
    :try_start_0
    const-string v1, "STORYBOARD_REF_FRAMERATE"

    invoke-virtual {p0, v1, p1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 339
    :goto_0
    return-void

    .line 336
    :catch_0
    move-exception v0

    .line 337
    .local v0, "jException":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public setStoryboardStartFrame(I)V
    .locals 2
    .param p1, "frame"    # I

    .prologue
    .line 274
    :try_start_0
    const-string v1, "STORYBOARD_START_FRAME"

    invoke-virtual {p0, v1, p1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 279
    :goto_0
    return-void

    .line 276
    :catch_0
    move-exception v0

    .line 277
    .local v0, "jException":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method
