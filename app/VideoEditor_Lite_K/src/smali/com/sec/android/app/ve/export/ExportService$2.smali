.class Lcom/sec/android/app/ve/export/ExportService$2;
.super Lcom/sec/android/app/ve/export/IExportService$Stub;
.source "ExportService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/ve/export/ExportService;->onBind(Landroid/content/Intent;)Lcom/sec/android/app/ve/export/IExportService$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/ve/export/ExportService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/ve/export/ExportService;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/ve/export/ExportService$2;->this$0:Lcom/sec/android/app/ve/export/ExportService;

    .line 40
    invoke-direct {p0}, Lcom/sec/android/app/ve/export/IExportService$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public cancelNotification()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 203
    sget-object v0, Lcom/sec/android/app/ve/VEApp;->gExport:Lcom/sec/android/app/ve/export/Export;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/export/Export;->getIsBackgroundExport()Z

    move-result v0

    if-nez v0, :cond_0

    .line 209
    :goto_0
    return-void

    .line 206
    :cond_0
    const-string v0, "EXPORT"

    const-string v1, " <<<<<<<<<<<< cancelNotification - BACKGROUND EXPORT ENABLED >>>>>>>>>>  "

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    iget-object v0, p0, Lcom/sec/android/app/ve/export/ExportService$2;->this$0:Lcom/sec/android/app/ve/export/ExportService;

    # getter for: Lcom/sec/android/app/ve/export/ExportService;->nManager:Landroid/app/NotificationManager;
    invoke-static {v0}, Lcom/sec/android/app/ve/export/ExportService;->access$11(Lcom/sec/android/app/ve/export/ExportService;)Landroid/app/NotificationManager;

    move-result-object v0

    sget v1, Lcom/sec/android/app/ve/export/ExportService;->EXPORT_NOTFICATION_ID:I

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 208
    iget-object v0, p0, Lcom/sec/android/app/ve/export/ExportService$2;->this$0:Lcom/sec/android/app/ve/export/ExportService;

    # getter for: Lcom/sec/android/app/ve/export/ExportService;->nManager:Landroid/app/NotificationManager;
    invoke-static {v0}, Lcom/sec/android/app/ve/export/ExportService;->access$11(Lcom/sec/android/app/ve/export/ExportService;)Landroid/app/NotificationManager;

    move-result-object v0

    sget v1, Lcom/sec/android/app/ve/export/ExportService;->EXPORT_COMPLETEION_ID:I

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    goto :goto_0
.end method

.method public changeNotification()V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 173
    iget-object v5, p0, Lcom/sec/android/app/ve/export/ExportService$2;->this$0:Lcom/sec/android/app/ve/export/ExportService;

    # getter for: Lcom/sec/android/app/ve/export/ExportService;->nManager:Landroid/app/NotificationManager;
    invoke-static {v5}, Lcom/sec/android/app/ve/export/ExportService;->access$11(Lcom/sec/android/app/ve/export/ExportService;)Landroid/app/NotificationManager;

    move-result-object v5

    sget v6, Lcom/sec/android/app/ve/export/ExportService;->EXPORT_NOTFICATION_ID:I

    invoke-virtual {v5, v6}, Landroid/app/NotificationManager;->cancel(I)V

    .line 174
    sget-object v5, Lcom/sec/android/app/ve/VEApp;->gExport:Lcom/sec/android/app/ve/export/Export;

    invoke-virtual {v5}, Lcom/sec/android/app/ve/export/Export;->getIsBackgroundExport()Z

    move-result v5

    if-nez v5, :cond_1

    .line 199
    :cond_0
    :goto_0
    return-void

    .line 177
    :cond_1
    iget-object v5, p0, Lcom/sec/android/app/ve/export/ExportService$2;->this$0:Lcom/sec/android/app/ve/export/ExportService;

    new-instance v6, Landroid/app/Notification;

    sget-object v7, Lcom/sec/android/app/ve/VEApp;->gAdaper:Lcom/sec/android/app/ve/VEAdapter;

    invoke-interface {v7}, Lcom/sec/android/app/ve/VEAdapter;->getAppIconResource()I

    move-result v7

    const/4 v8, 0x0

    .line 178
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-direct {v6, v7, v8, v10, v11}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    .line 177
    invoke-static {v5, v6}, Lcom/sec/android/app/ve/export/ExportService;->access$12(Lcom/sec/android/app/ve/export/ExportService;Landroid/app/Notification;)V

    .line 180
    sget-object v5, Lcom/sec/android/app/ve/VEApp;->gExport:Lcom/sec/android/app/ve/export/Export;

    invoke-virtual {v5}, Lcom/sec/android/app/ve/export/Export;->getFileName()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 181
    sget-object v5, Lcom/sec/android/app/ve/VEApp;->gExport:Lcom/sec/android/app/ve/export/Export;

    invoke-virtual {v5}, Lcom/sec/android/app/ve/export/Export;->getFileName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 183
    .local v4, "uri":Landroid/net/Uri;
    new-instance v2, Landroid/content/Intent;

    const-string v5, "android.intent.action.VIEW"

    invoke-direct {v2, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 184
    .local v2, "intent":Landroid/content/Intent;
    const/high16 v5, 0x10000000

    invoke-virtual {v2, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 185
    const-string v5, "video/mp4"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 187
    new-instance v1, Ljava/util/Random;

    invoke-direct {v1}, Ljava/util/Random;-><init>()V

    .line 189
    .local v1, "generator":Ljava/util/Random;
    iget-object v5, p0, Lcom/sec/android/app/ve/export/ExportService$2;->this$0:Lcom/sec/android/app/ve/export/ExportService;

    invoke-virtual {v1}, Ljava/util/Random;->nextInt()I

    move-result v6

    const/high16 v7, 0x8000000

    invoke-static {v5, v6, v2, v7}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    .line 191
    .local v3, "pIntnt":Landroid/app/PendingIntent;
    new-instance v0, Landroid/widget/RemoteViews;

    iget-object v5, p0, Lcom/sec/android/app/ve/export/ExportService$2;->this$0:Lcom/sec/android/app/ve/export/ExportService;

    invoke-virtual {v5}, Lcom/sec/android/app/ve/export/ExportService;->getPackageName()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/sec/android/app/ve/VEApp;->gExport:Lcom/sec/android/app/ve/export/Export;

    invoke-virtual {v6}, Lcom/sec/android/app/ve/export/Export;->getNotificationLayoutID()I

    move-result v6

    invoke-direct {v0, v5, v6}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 192
    .local v0, "contentView":Landroid/widget/RemoteViews;
    sget-object v5, Lcom/sec/android/app/ve/VEApp;->gExport:Lcom/sec/android/app/ve/export/Export;

    iget-object v6, p0, Lcom/sec/android/app/ve/export/ExportService$2;->this$0:Lcom/sec/android/app/ve/export/ExportService;

    invoke-virtual {v5, v6, v0}, Lcom/sec/android/app/ve/export/Export;->changeNotificationContentView(Landroid/content/Context;Landroid/widget/RemoteViews;)V

    .line 194
    iget-object v5, p0, Lcom/sec/android/app/ve/export/ExportService$2;->this$0:Lcom/sec/android/app/ve/export/ExportService;

    # getter for: Lcom/sec/android/app/ve/export/ExportService;->notification:Landroid/app/Notification;
    invoke-static {v5}, Lcom/sec/android/app/ve/export/ExportService;->access$13(Lcom/sec/android/app/ve/export/ExportService;)Landroid/app/Notification;

    move-result-object v5

    iput-object v0, v5, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    .line 195
    iget-object v5, p0, Lcom/sec/android/app/ve/export/ExportService$2;->this$0:Lcom/sec/android/app/ve/export/ExportService;

    # getter for: Lcom/sec/android/app/ve/export/ExportService;->notification:Landroid/app/Notification;
    invoke-static {v5}, Lcom/sec/android/app/ve/export/ExportService;->access$13(Lcom/sec/android/app/ve/export/ExportService;)Landroid/app/Notification;

    move-result-object v5

    iput-object v3, v5, Landroid/app/Notification;->contentIntent:Landroid/app/PendingIntent;

    .line 196
    iget-object v5, p0, Lcom/sec/android/app/ve/export/ExportService$2;->this$0:Lcom/sec/android/app/ve/export/ExportService;

    # getter for: Lcom/sec/android/app/ve/export/ExportService;->notification:Landroid/app/Notification;
    invoke-static {v5}, Lcom/sec/android/app/ve/export/ExportService;->access$13(Lcom/sec/android/app/ve/export/ExportService;)Landroid/app/Notification;

    move-result-object v5

    iget v6, v5, Landroid/app/Notification;->flags:I

    or-int/lit8 v6, v6, 0x10

    iput v6, v5, Landroid/app/Notification;->flags:I

    .line 197
    iget-object v5, p0, Lcom/sec/android/app/ve/export/ExportService$2;->this$0:Lcom/sec/android/app/ve/export/ExportService;

    # getter for: Lcom/sec/android/app/ve/export/ExportService;->nManager:Landroid/app/NotificationManager;
    invoke-static {v5}, Lcom/sec/android/app/ve/export/ExportService;->access$11(Lcom/sec/android/app/ve/export/ExportService;)Landroid/app/NotificationManager;

    move-result-object v5

    sget v6, Lcom/sec/android/app/ve/export/ExportService;->EXPORT_COMPLETEION_ID:I

    iget-object v7, p0, Lcom/sec/android/app/ve/export/ExportService$2;->this$0:Lcom/sec/android/app/ve/export/ExportService;

    # getter for: Lcom/sec/android/app/ve/export/ExportService;->notification:Landroid/app/Notification;
    invoke-static {v7}, Lcom/sec/android/app/ve/export/ExportService;->access$13(Lcom/sec/android/app/ve/export/ExportService;)Landroid/app/Notification;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto/16 :goto_0
.end method

.method public getExportProgress()I
    .locals 2

    .prologue
    .line 100
    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getJNIVersion()F

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 101
    iget-object v0, p0, Lcom/sec/android/app/ve/export/ExportService$2;->this$0:Lcom/sec/android/app/ve/export/ExportService;

    # getter for: Lcom/sec/android/app/ve/export/ExportService;->mExpInterface:Lcom/sec/android/app/ve/export/ExportInterface;
    invoke-static {v0}, Lcom/sec/android/app/ve/export/ExportService;->access$2(Lcom/sec/android/app/ve/export/ExportService;)Lcom/sec/android/app/ve/export/ExportInterface;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 102
    iget-object v0, p0, Lcom/sec/android/app/ve/export/ExportService$2;->this$0:Lcom/sec/android/app/ve/export/ExportService;

    # getter for: Lcom/sec/android/app/ve/export/ExportService;->mExpInterface:Lcom/sec/android/app/ve/export/ExportInterface;
    invoke-static {v0}, Lcom/sec/android/app/ve/export/ExportService;->access$2(Lcom/sec/android/app/ve/export/ExportService;)Lcom/sec/android/app/ve/export/ExportInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/ve/export/ExportInterface;->getExportProgress()I

    move-result v0

    .line 105
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public initNotification()V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 135
    sget-object v2, Lcom/sec/android/app/ve/VEApp;->gExport:Lcom/sec/android/app/ve/export/Export;

    invoke-virtual {v2}, Lcom/sec/android/app/ve/export/Export;->getIsBackgroundExport()Z

    move-result v2

    if-nez v2, :cond_0

    .line 157
    :goto_0
    return-void

    .line 138
    :cond_0
    const-string v2, "EXPORT"

    const-string v3, " <<<<<<<<<<<< initNotification - BACKGROUND EXPORT ENABLED >>>>>>>>>>  "

    invoke-static {v2, v3}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    iget-object v3, p0, Lcom/sec/android/app/ve/export/ExportService$2;->this$0:Lcom/sec/android/app/ve/export/ExportService;

    iget-object v2, p0, Lcom/sec/android/app/ve/export/ExportService$2;->this$0:Lcom/sec/android/app/ve/export/ExportService;

    const-string v4, "notification"

    invoke-virtual {v2, v4}, Lcom/sec/android/app/ve/export/ExportService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/NotificationManager;

    invoke-static {v3, v2}, Lcom/sec/android/app/ve/export/ExportService;->access$6(Lcom/sec/android/app/ve/export/ExportService;Landroid/app/NotificationManager;)V

    .line 141
    iget-object v2, p0, Lcom/sec/android/app/ve/export/ExportService$2;->this$0:Lcom/sec/android/app/ve/export/ExportService;

    new-instance v3, Landroid/app/Notification;

    sget-object v4, Lcom/sec/android/app/ve/VEApp;->gAdaper:Lcom/sec/android/app/ve/VEAdapter;

    invoke-interface {v4}, Lcom/sec/android/app/ve/VEAdapter;->getAppIconResource()I

    move-result v4

    iget-object v5, p0, Lcom/sec/android/app/ve/export/ExportService$2;->this$0:Lcom/sec/android/app/ve/export/ExportService;

    sget-object v6, Lcom/sec/android/app/ve/VEApp;->gExport:Lcom/sec/android/app/ve/export/Export;

    invoke-virtual {v6}, Lcom/sec/android/app/ve/export/Export;->getOngoingNotificationStringID()I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/ve/export/ExportService;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 142
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    .line 141
    invoke-static {v2, v3}, Lcom/sec/android/app/ve/export/ExportService;->access$7(Lcom/sec/android/app/ve/export/ExportService;Landroid/app/Notification;)V

    .line 144
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/android/app/ve/export/ExportService$2;->this$0:Lcom/sec/android/app/ve/export/ExportService;

    sget-object v3, Lcom/sec/android/app/ve/VEApp;->gExport:Lcom/sec/android/app/ve/export/Export;

    invoke-virtual {v3}, Lcom/sec/android/app/ve/export/Export;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 146
    .local v1, "notiIntent":Landroid/content/Intent;
    const-string v2, "fromNotification"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 147
    iget-object v2, p0, Lcom/sec/android/app/ve/export/ExportService$2;->this$0:Lcom/sec/android/app/ve/export/ExportService;

    iget-object v3, p0, Lcom/sec/android/app/ve/export/ExportService$2;->this$0:Lcom/sec/android/app/ve/export/ExportService;

    const/4 v4, 0x0

    const/high16 v5, 0x8000000

    invoke-static {v3, v4, v1, v5}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/ve/export/ExportService;->access$8(Lcom/sec/android/app/ve/export/ExportService;Landroid/app/PendingIntent;)V

    .line 149
    new-instance v0, Landroid/widget/RemoteViews;

    iget-object v2, p0, Lcom/sec/android/app/ve/export/ExportService$2;->this$0:Lcom/sec/android/app/ve/export/ExportService;

    invoke-virtual {v2}, Lcom/sec/android/app/ve/export/ExportService;->getPackageName()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/ve/VEApp;->gExport:Lcom/sec/android/app/ve/export/Export;

    invoke-virtual {v3}, Lcom/sec/android/app/ve/export/Export;->getOngoingNotificationLayoutID()I

    move-result v3

    invoke-direct {v0, v2, v3}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 151
    .local v0, "contentView":Landroid/widget/RemoteViews;
    sget-object v2, Lcom/sec/android/app/ve/VEApp;->gExport:Lcom/sec/android/app/ve/export/Export;

    iget-object v3, p0, Lcom/sec/android/app/ve/export/ExportService$2;->this$0:Lcom/sec/android/app/ve/export/ExportService;

    invoke-virtual {v2, v3, v0}, Lcom/sec/android/app/ve/export/Export;->initiateNotificationContentView(Landroid/content/Context;Landroid/widget/RemoteViews;)V

    .line 152
    iget-object v2, p0, Lcom/sec/android/app/ve/export/ExportService$2;->this$0:Lcom/sec/android/app/ve/export/ExportService;

    # getter for: Lcom/sec/android/app/ve/export/ExportService;->onGoingNotification:Landroid/app/Notification;
    invoke-static {v2}, Lcom/sec/android/app/ve/export/ExportService;->access$9(Lcom/sec/android/app/ve/export/ExportService;)Landroid/app/Notification;

    move-result-object v2

    iput-object v0, v2, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    .line 153
    iget-object v2, p0, Lcom/sec/android/app/ve/export/ExportService$2;->this$0:Lcom/sec/android/app/ve/export/ExportService;

    # getter for: Lcom/sec/android/app/ve/export/ExportService;->onGoingNotification:Landroid/app/Notification;
    invoke-static {v2}, Lcom/sec/android/app/ve/export/ExportService;->access$9(Lcom/sec/android/app/ve/export/ExportService;)Landroid/app/Notification;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/ve/export/ExportService$2;->this$0:Lcom/sec/android/app/ve/export/ExportService;

    # getter for: Lcom/sec/android/app/ve/export/ExportService;->pIntent:Landroid/app/PendingIntent;
    invoke-static {v3}, Lcom/sec/android/app/ve/export/ExportService;->access$10(Lcom/sec/android/app/ve/export/ExportService;)Landroid/app/PendingIntent;

    move-result-object v3

    iput-object v3, v2, Landroid/app/Notification;->contentIntent:Landroid/app/PendingIntent;

    .line 154
    iget-object v2, p0, Lcom/sec/android/app/ve/export/ExportService$2;->this$0:Lcom/sec/android/app/ve/export/ExportService;

    # getter for: Lcom/sec/android/app/ve/export/ExportService;->onGoingNotification:Landroid/app/Notification;
    invoke-static {v2}, Lcom/sec/android/app/ve/export/ExportService;->access$9(Lcom/sec/android/app/ve/export/ExportService;)Landroid/app/Notification;

    move-result-object v2

    iget v3, v2, Landroid/app/Notification;->flags:I

    or-int/lit8 v3, v3, 0x20

    iput v3, v2, Landroid/app/Notification;->flags:I

    .line 155
    iget-object v2, p0, Lcom/sec/android/app/ve/export/ExportService$2;->this$0:Lcom/sec/android/app/ve/export/ExportService;

    # getter for: Lcom/sec/android/app/ve/export/ExportService;->onGoingNotification:Landroid/app/Notification;
    invoke-static {v2}, Lcom/sec/android/app/ve/export/ExportService;->access$9(Lcom/sec/android/app/ve/export/ExportService;)Landroid/app/Notification;

    move-result-object v2

    iget v3, v2, Landroid/app/Notification;->flags:I

    or-int/lit8 v3, v3, 0x2

    iput v3, v2, Landroid/app/Notification;->flags:I

    .line 156
    iget-object v2, p0, Lcom/sec/android/app/ve/export/ExportService$2;->this$0:Lcom/sec/android/app/ve/export/ExportService;

    sget v3, Lcom/sec/android/app/ve/export/ExportService;->EXPORT_NOTFICATION_ID:I

    iget-object v4, p0, Lcom/sec/android/app/ve/export/ExportService$2;->this$0:Lcom/sec/android/app/ve/export/ExportService;

    # getter for: Lcom/sec/android/app/ve/export/ExportService;->onGoingNotification:Landroid/app/Notification;
    invoke-static {v4}, Lcom/sec/android/app/ve/export/ExportService;->access$9(Lcom/sec/android/app/ve/export/ExportService;)Landroid/app/Notification;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/ve/export/ExportService;->startForeground(ILandroid/app/Notification;)V

    goto/16 :goto_0
.end method

.method public isDefaultPause()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 111
    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getJNIVersion()F

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 112
    iget-object v0, p0, Lcom/sec/android/app/ve/export/ExportService$2;->this$0:Lcom/sec/android/app/ve/export/ExportService;

    # getter for: Lcom/sec/android/app/ve/export/ExportService;->mExpInterface:Lcom/sec/android/app/ve/export/ExportInterface;
    invoke-static {v0}, Lcom/sec/android/app/ve/export/ExportService;->access$2(Lcom/sec/android/app/ve/export/ExportService;)Lcom/sec/android/app/ve/export/ExportInterface;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/ve/export/ExportService$2;->this$0:Lcom/sec/android/app/ve/export/ExportService;

    # getter for: Lcom/sec/android/app/ve/export/ExportService;->mExpInterface:Lcom/sec/android/app/ve/export/ExportInterface;
    invoke-static {v0}, Lcom/sec/android/app/ve/export/ExportService;->access$2(Lcom/sec/android/app/ve/export/ExportService;)Lcom/sec/android/app/ve/export/ExportInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/ve/export/ExportInterface;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 113
    iget-object v0, p0, Lcom/sec/android/app/ve/export/ExportService$2;->this$0:Lcom/sec/android/app/ve/export/ExportService;

    iget-boolean v0, v0, Lcom/sec/android/app/ve/export/ExportService;->defaultPaused:Z

    .line 120
    :goto_0
    return v0

    .line 117
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/ve/export/ExportService$2;->this$0:Lcom/sec/android/app/ve/export/ExportService;

    # getter for: Lcom/sec/android/app/ve/export/ExportService;->eThread:Lcom/sec/android/app/ve/export/ExportThread;
    invoke-static {v0}, Lcom/sec/android/app/ve/export/ExportService;->access$5(Lcom/sec/android/app/ve/export/ExportService;)Lcom/sec/android/app/ve/export/ExportThread;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/ve/export/ExportService$2;->this$0:Lcom/sec/android/app/ve/export/ExportService;

    # getter for: Lcom/sec/android/app/ve/export/ExportService;->eThread:Lcom/sec/android/app/ve/export/ExportThread;
    invoke-static {v0}, Lcom/sec/android/app/ve/export/ExportService;->access$5(Lcom/sec/android/app/ve/export/ExportService;)Lcom/sec/android/app/ve/export/ExportThread;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/ve/export/ExportThread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 118
    iget-object v0, p0, Lcom/sec/android/app/ve/export/ExportService$2;->this$0:Lcom/sec/android/app/ve/export/ExportService;

    iget-boolean v0, v0, Lcom/sec/android/app/ve/export/ExportService;->defaultPaused:Z

    goto :goto_0

    .line 120
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isEThreadAlive()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 125
    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getJNIVersion()F

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 126
    iget-object v0, p0, Lcom/sec/android/app/ve/export/ExportService$2;->this$0:Lcom/sec/android/app/ve/export/ExportService;

    # getter for: Lcom/sec/android/app/ve/export/ExportService;->mExpInterface:Lcom/sec/android/app/ve/export/ExportInterface;
    invoke-static {v0}, Lcom/sec/android/app/ve/export/ExportService;->access$2(Lcom/sec/android/app/ve/export/ExportService;)Lcom/sec/android/app/ve/export/ExportInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/ve/export/ExportInterface;->isPlaying()Z

    move-result v0

    .line 129
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/ve/export/ExportService$2;->this$0:Lcom/sec/android/app/ve/export/ExportService;

    # getter for: Lcom/sec/android/app/ve/export/ExportService;->eThread:Lcom/sec/android/app/ve/export/ExportThread;
    invoke-static {v0}, Lcom/sec/android/app/ve/export/ExportService;->access$5(Lcom/sec/android/app/ve/export/ExportService;)Lcom/sec/android/app/ve/export/ExportThread;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/ve/export/ExportService$2;->this$0:Lcom/sec/android/app/ve/export/ExportService;

    # getter for: Lcom/sec/android/app/ve/export/ExportService;->eThread:Lcom/sec/android/app/ve/export/ExportThread;
    invoke-static {v0}, Lcom/sec/android/app/ve/export/ExportService;->access$5(Lcom/sec/android/app/ve/export/ExportService;)Lcom/sec/android/app/ve/export/ExportThread;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/ve/export/ExportThread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public pauseExport(Z)V
    .locals 2
    .param p1, "defaultPause"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 67
    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getJNIVersion()F

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    .line 68
    iget-object v0, p0, Lcom/sec/android/app/ve/export/ExportService$2;->this$0:Lcom/sec/android/app/ve/export/ExportService;

    # getter for: Lcom/sec/android/app/ve/export/ExportService;->mExpInterface:Lcom/sec/android/app/ve/export/ExportInterface;
    invoke-static {v0}, Lcom/sec/android/app/ve/export/ExportService;->access$2(Lcom/sec/android/app/ve/export/ExportService;)Lcom/sec/android/app/ve/export/ExportInterface;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 69
    iget-object v0, p0, Lcom/sec/android/app/ve/export/ExportService$2;->this$0:Lcom/sec/android/app/ve/export/ExportService;

    iput-boolean p1, v0, Lcom/sec/android/app/ve/export/ExportService;->defaultPaused:Z

    .line 70
    iget-object v0, p0, Lcom/sec/android/app/ve/export/ExportService$2;->this$0:Lcom/sec/android/app/ve/export/ExportService;

    # getter for: Lcom/sec/android/app/ve/export/ExportService;->mExpInterface:Lcom/sec/android/app/ve/export/ExportInterface;
    invoke-static {v0}, Lcom/sec/android/app/ve/export/ExportService;->access$2(Lcom/sec/android/app/ve/export/ExportService;)Lcom/sec/android/app/ve/export/ExportInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/ve/export/ExportInterface;->pause()V

    .line 79
    :cond_0
    :goto_0
    return-void

    .line 74
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/ve/export/ExportService$2;->this$0:Lcom/sec/android/app/ve/export/ExportService;

    # getter for: Lcom/sec/android/app/ve/export/ExportService;->eThread:Lcom/sec/android/app/ve/export/ExportThread;
    invoke-static {v0}, Lcom/sec/android/app/ve/export/ExportService;->access$5(Lcom/sec/android/app/ve/export/ExportService;)Lcom/sec/android/app/ve/export/ExportThread;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 75
    iget-object v0, p0, Lcom/sec/android/app/ve/export/ExportService$2;->this$0:Lcom/sec/android/app/ve/export/ExportService;

    iput-boolean p1, v0, Lcom/sec/android/app/ve/export/ExportService;->defaultPaused:Z

    .line 76
    iget-object v0, p0, Lcom/sec/android/app/ve/export/ExportService$2;->this$0:Lcom/sec/android/app/ve/export/ExportService;

    # getter for: Lcom/sec/android/app/ve/export/ExportService;->eThread:Lcom/sec/android/app/ve/export/ExportThread;
    invoke-static {v0}, Lcom/sec/android/app/ve/export/ExportService;->access$5(Lcom/sec/android/app/ve/export/ExportService;)Lcom/sec/android/app/ve/export/ExportThread;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/ve/export/ExportThread;->pauseExport()V

    goto :goto_0
.end method

.method public resumeExport()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 84
    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getJNIVersion()F

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    .line 85
    iget-object v0, p0, Lcom/sec/android/app/ve/export/ExportService$2;->this$0:Lcom/sec/android/app/ve/export/ExportService;

    # getter for: Lcom/sec/android/app/ve/export/ExportService;->mExpInterface:Lcom/sec/android/app/ve/export/ExportInterface;
    invoke-static {v0}, Lcom/sec/android/app/ve/export/ExportService;->access$2(Lcom/sec/android/app/ve/export/ExportService;)Lcom/sec/android/app/ve/export/ExportInterface;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/sec/android/app/ve/export/ExportService$2;->this$0:Lcom/sec/android/app/ve/export/ExportService;

    iput-boolean v2, v0, Lcom/sec/android/app/ve/export/ExportService;->defaultPaused:Z

    .line 87
    iget-object v0, p0, Lcom/sec/android/app/ve/export/ExportService$2;->this$0:Lcom/sec/android/app/ve/export/ExportService;

    # getter for: Lcom/sec/android/app/ve/export/ExportService;->mExpInterface:Lcom/sec/android/app/ve/export/ExportInterface;
    invoke-static {v0}, Lcom/sec/android/app/ve/export/ExportService;->access$2(Lcom/sec/android/app/ve/export/ExportService;)Lcom/sec/android/app/ve/export/ExportInterface;

    move-result-object v0

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/ve/export/ExportInterface;->resume(J)V

    .line 96
    :cond_0
    :goto_0
    return-void

    .line 91
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/ve/export/ExportService$2;->this$0:Lcom/sec/android/app/ve/export/ExportService;

    # getter for: Lcom/sec/android/app/ve/export/ExportService;->eThread:Lcom/sec/android/app/ve/export/ExportThread;
    invoke-static {v0}, Lcom/sec/android/app/ve/export/ExportService;->access$5(Lcom/sec/android/app/ve/export/ExportService;)Lcom/sec/android/app/ve/export/ExportThread;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/sec/android/app/ve/export/ExportService$2;->this$0:Lcom/sec/android/app/ve/export/ExportService;

    iput-boolean v2, v0, Lcom/sec/android/app/ve/export/ExportService;->defaultPaused:Z

    .line 93
    iget-object v0, p0, Lcom/sec/android/app/ve/export/ExportService$2;->this$0:Lcom/sec/android/app/ve/export/ExportService;

    # getter for: Lcom/sec/android/app/ve/export/ExportService;->eThread:Lcom/sec/android/app/ve/export/ExportThread;
    invoke-static {v0}, Lcom/sec/android/app/ve/export/ExportService;->access$5(Lcom/sec/android/app/ve/export/ExportService;)Lcom/sec/android/app/ve/export/ExportThread;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/ve/export/ExportThread;->resumeExport()V

    goto :goto_0
.end method

.method public startExport()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 42
    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getJNIVersion()F

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 43
    iget-object v0, p0, Lcom/sec/android/app/ve/export/ExportService$2;->this$0:Lcom/sec/android/app/ve/export/ExportService;

    new-instance v1, Lcom/sec/android/app/ve/export/ExportInterface;

    invoke-direct {v1}, Lcom/sec/android/app/ve/export/ExportInterface;-><init>()V

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/export/ExportService;->access$1(Lcom/sec/android/app/ve/export/ExportService;Lcom/sec/android/app/ve/export/ExportInterface;)V

    .line 44
    iget-object v0, p0, Lcom/sec/android/app/ve/export/ExportService$2;->this$0:Lcom/sec/android/app/ve/export/ExportService;

    # getter for: Lcom/sec/android/app/ve/export/ExportService;->mExpInterface:Lcom/sec/android/app/ve/export/ExportInterface;
    invoke-static {v0}, Lcom/sec/android/app/ve/export/ExportService;->access$2(Lcom/sec/android/app/ve/export/ExportService;)Lcom/sec/android/app/ve/export/ExportInterface;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/ve/export/ExportService$2;->this$0:Lcom/sec/android/app/ve/export/ExportService;

    # getter for: Lcom/sec/android/app/ve/export/ExportService;->mAdapter:Lcom/sec/android/app/ve/export/ExportInterface$Adapter;
    invoke-static {v1}, Lcom/sec/android/app/ve/export/ExportService;->access$3(Lcom/sec/android/app/ve/export/ExportService;)Lcom/sec/android/app/ve/export/ExportInterface$Adapter;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/ve/export/ExportInterface;->setAdapter(Lcom/sec/android/app/ve/export/ExportInterface$Adapter;)V

    .line 45
    iget-object v0, p0, Lcom/sec/android/app/ve/export/ExportService$2;->this$0:Lcom/sec/android/app/ve/export/ExportService;

    # getter for: Lcom/sec/android/app/ve/export/ExportService;->mExpInterface:Lcom/sec/android/app/ve/export/ExportInterface;
    invoke-static {v0}, Lcom/sec/android/app/ve/export/ExportService;->access$2(Lcom/sec/android/app/ve/export/ExportService;)Lcom/sec/android/app/ve/export/ExportInterface;

    move-result-object v0

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/ve/export/ExportInterface;->play(J)V

    .line 51
    :goto_0
    return-void

    .line 48
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/ve/export/ExportService$2;->this$0:Lcom/sec/android/app/ve/export/ExportService;

    new-instance v1, Lcom/sec/android/app/ve/export/ExportThread;

    invoke-direct {v1}, Lcom/sec/android/app/ve/export/ExportThread;-><init>()V

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/export/ExportService;->access$4(Lcom/sec/android/app/ve/export/ExportService;Lcom/sec/android/app/ve/export/ExportThread;)V

    .line 49
    iget-object v0, p0, Lcom/sec/android/app/ve/export/ExportService$2;->this$0:Lcom/sec/android/app/ve/export/ExportService;

    # getter for: Lcom/sec/android/app/ve/export/ExportService;->eThread:Lcom/sec/android/app/ve/export/ExportThread;
    invoke-static {v0}, Lcom/sec/android/app/ve/export/ExportService;->access$5(Lcom/sec/android/app/ve/export/ExportService;)Lcom/sec/android/app/ve/export/ExportThread;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/ve/export/ExportThread;->start()V

    goto :goto_0
.end method

.method public stopExport()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 54
    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getJNIVersion()F

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    .line 55
    iget-object v0, p0, Lcom/sec/android/app/ve/export/ExportService$2;->this$0:Lcom/sec/android/app/ve/export/ExportService;

    # getter for: Lcom/sec/android/app/ve/export/ExportService;->mExpInterface:Lcom/sec/android/app/ve/export/ExportInterface;
    invoke-static {v0}, Lcom/sec/android/app/ve/export/ExportService;->access$2(Lcom/sec/android/app/ve/export/ExportService;)Lcom/sec/android/app/ve/export/ExportInterface;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 56
    iget-object v0, p0, Lcom/sec/android/app/ve/export/ExportService$2;->this$0:Lcom/sec/android/app/ve/export/ExportService;

    # getter for: Lcom/sec/android/app/ve/export/ExportService;->mExpInterface:Lcom/sec/android/app/ve/export/ExportInterface;
    invoke-static {v0}, Lcom/sec/android/app/ve/export/ExportService;->access$2(Lcom/sec/android/app/ve/export/ExportService;)Lcom/sec/android/app/ve/export/ExportInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/ve/export/ExportInterface;->stop()V

    .line 63
    :cond_0
    :goto_0
    return-void

    .line 60
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/ve/export/ExportService$2;->this$0:Lcom/sec/android/app/ve/export/ExportService;

    # getter for: Lcom/sec/android/app/ve/export/ExportService;->eThread:Lcom/sec/android/app/ve/export/ExportThread;
    invoke-static {v0}, Lcom/sec/android/app/ve/export/ExportService;->access$5(Lcom/sec/android/app/ve/export/ExportService;)Lcom/sec/android/app/ve/export/ExportThread;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 61
    iget-object v0, p0, Lcom/sec/android/app/ve/export/ExportService$2;->this$0:Lcom/sec/android/app/ve/export/ExportService;

    # getter for: Lcom/sec/android/app/ve/export/ExportService;->eThread:Lcom/sec/android/app/ve/export/ExportThread;
    invoke-static {v0}, Lcom/sec/android/app/ve/export/ExportService;->access$5(Lcom/sec/android/app/ve/export/ExportService;)Lcom/sec/android/app/ve/export/ExportThread;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/ve/export/ExportThread;->stopExport()V

    goto :goto_0
.end method

.method public updateNotification(II)V
    .locals 3
    .param p1, "progress"    # I
    .param p2, "state"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 163
    sget-object v0, Lcom/sec/android/app/ve/VEApp;->gExport:Lcom/sec/android/app/ve/export/Export;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/export/Export;->getIsBackgroundExport()Z

    move-result v0

    if-nez v0, :cond_0

    .line 169
    :goto_0
    return-void

    .line 166
    :cond_0
    sget-object v0, Lcom/sec/android/app/ve/VEApp;->gExport:Lcom/sec/android/app/ve/export/Export;

    iget-object v1, p0, Lcom/sec/android/app/ve/export/ExportService$2;->this$0:Lcom/sec/android/app/ve/export/ExportService;

    iget-object v2, p0, Lcom/sec/android/app/ve/export/ExportService$2;->this$0:Lcom/sec/android/app/ve/export/ExportService;

    # getter for: Lcom/sec/android/app/ve/export/ExportService;->onGoingNotification:Landroid/app/Notification;
    invoke-static {v2}, Lcom/sec/android/app/ve/export/ExportService;->access$9(Lcom/sec/android/app/ve/export/ExportService;)Landroid/app/Notification;

    move-result-object v2

    iget-object v2, v2, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    invoke-virtual {v0, v1, v2, p1, p2}, Lcom/sec/android/app/ve/export/Export;->updateNotificationContentView(Landroid/content/Context;Landroid/widget/RemoteViews;II)V

    .line 167
    iget-object v0, p0, Lcom/sec/android/app/ve/export/ExportService$2;->this$0:Lcom/sec/android/app/ve/export/ExportService;

    # getter for: Lcom/sec/android/app/ve/export/ExportService;->nManager:Landroid/app/NotificationManager;
    invoke-static {v0}, Lcom/sec/android/app/ve/export/ExportService;->access$11(Lcom/sec/android/app/ve/export/ExportService;)Landroid/app/NotificationManager;

    move-result-object v0

    sget v1, Lcom/sec/android/app/ve/export/ExportService;->EXPORT_NOTFICATION_ID:I

    iget-object v2, p0, Lcom/sec/android/app/ve/export/ExportService$2;->this$0:Lcom/sec/android/app/ve/export/ExportService;

    # getter for: Lcom/sec/android/app/ve/export/ExportService;->onGoingNotification:Landroid/app/Notification;
    invoke-static {v2}, Lcom/sec/android/app/ve/export/ExportService;->access$9(Lcom/sec/android/app/ve/export/ExportService;)Landroid/app/Notification;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto :goto_0
.end method
