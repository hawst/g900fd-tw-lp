.class public Lcom/sec/android/app/ve/VEApp;
.super Landroid/app/AppGlobals;
.source "VEApp.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/ve/VEApp$ApplicationNotifier;
    }
.end annotation


# static fields
.field public static EngineInitImageOnFly:Z = false

.field public static final FULL_VERSION_SUMMARY:I = 0x0

.field public static final JNI_VERSION_H:I = 0x1

.field public static final JNI_VERSION_J:I = 0x0

.field public static final JNI_VERSION_T:I = 0x2

.field public static final LITE_VERSION_SUMMARY:I = 0x1

.field public static gAdaper:Lcom/sec/android/app/ve/VEAdapter;

.field public static gBGMManager:Lcom/sec/android/app/ve/bgm/BGMManager;

.field private static gContext:Landroid/content/Context;

.field public static gExport:Lcom/sec/android/app/ve/export/Export;

.field public static gProjectPluginManager:Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework;

.field public static gThemeMgr:Lcom/sec/android/app/ve/theme/ThemeManager;

.field public static gThumbFetcher:Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcher;

.field public static isThreadBasedInitImges:Z

.field private static mCurrentJNIVersion:F

.field private static volatile mNotifier:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/ve/VEApp$ApplicationNotifier;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 32
    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getInitialApplication()Landroid/app/Application;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/ve/VEApp;->gContext:Landroid/content/Context;

    .line 33
    new-instance v0, Lcom/sec/android/app/ve/theme/ThemeManager;

    invoke-direct {v0}, Lcom/sec/android/app/ve/theme/ThemeManager;-><init>()V

    sput-object v0, Lcom/sec/android/app/ve/VEApp;->gThemeMgr:Lcom/sec/android/app/ve/theme/ThemeManager;

    .line 34
    new-instance v0, Lcom/sec/android/app/ve/export/Export;

    invoke-direct {v0}, Lcom/sec/android/app/ve/export/Export;-><init>()V

    sput-object v0, Lcom/sec/android/app/ve/VEApp;->gExport:Lcom/sec/android/app/ve/export/Export;

    .line 35
    new-instance v0, Lcom/sec/android/app/ve/bgm/BGMManager;

    invoke-direct {v0}, Lcom/sec/android/app/ve/bgm/BGMManager;-><init>()V

    sput-object v0, Lcom/sec/android/app/ve/VEApp;->gBGMManager:Lcom/sec/android/app/ve/bgm/BGMManager;

    .line 36
    sput-object v2, Lcom/sec/android/app/ve/VEApp;->gAdaper:Lcom/sec/android/app/ve/VEAdapter;

    .line 37
    new-instance v0, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcher;

    invoke-direct {v0}, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcher;-><init>()V

    sput-object v0, Lcom/sec/android/app/ve/VEApp;->gThumbFetcher:Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcher;

    .line 38
    sput-object v2, Lcom/sec/android/app/ve/VEApp;->mNotifier:Ljava/util/ArrayList;

    .line 40
    sput-boolean v1, Lcom/sec/android/app/ve/VEApp;->EngineInitImageOnFly:Z

    .line 47
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/ve/VEApp;->mCurrentJNIVersion:F

    .line 48
    sput-boolean v1, Lcom/sec/android/app/ve/VEApp;->isThreadBasedInitImges:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Landroid/app/AppGlobals;-><init>()V

    return-void
.end method

.method public static addTimeNotifier(Lcom/sec/android/app/ve/VEApp$ApplicationNotifier;)V
    .locals 1
    .param p0, "adp"    # Lcom/sec/android/app/ve/VEApp$ApplicationNotifier;

    .prologue
    .line 256
    sget-object v0, Lcom/sec/android/app/ve/VEApp;->mNotifier:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 257
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/sec/android/app/ve/VEApp;->mNotifier:Ljava/util/ArrayList;

    .line 259
    :cond_0
    sget-object v0, Lcom/sec/android/app/ve/VEApp;->mNotifier:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 260
    return-void
.end method

.method public static bindToService(Landroid/content/Intent;Landroid/content/ServiceConnection;)V
    .locals 2
    .param p0, "i"    # Landroid/content/Intent;
    .param p1, "connection"    # Landroid/content/ServiceConnection;

    .prologue
    .line 129
    sget-object v0, Lcom/sec/android/app/ve/VEApp;->gContext:Landroid/content/Context;

    const/4 v1, 0x1

    invoke-virtual {v0, p0, p1, v1}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 130
    return-void
.end method

.method public static checkAccessibillityEnabled()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 153
    sget-object v2, Lcom/sec/android/app/ve/VEApp;->gContext:Landroid/content/Context;

    const-string v3, "accessibility"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    .line 154
    .local v0, "accessibilityManager":Landroid/view/accessibility/AccessibilityManager;
    if-nez v0, :cond_1

    .line 157
    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static clearAllTimeNotifier()V
    .locals 1

    .prologue
    .line 249
    sget-object v0, Lcom/sec/android/app/ve/VEApp;->mNotifier:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 250
    sget-object v0, Lcom/sec/android/app/ve/VEApp;->mNotifier:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 252
    :cond_0
    return-void
.end method

.method public static createDownload()Lcom/sec/android/app/ve/theme/Download;
    .locals 1

    .prologue
    .line 119
    new-instance v0, Lcom/sec/android/app/ve/theme/Download;

    invoke-direct {v0}, Lcom/sec/android/app/ve/theme/Download;-><init>()V

    return-object v0
.end method

.method public static getAppAssets()Landroid/content/res/AssetManager;
    .locals 1

    .prologue
    .line 149
    sget-object v0, Lcom/sec/android/app/ve/VEApp;->gContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    return-object v0
.end method

.method public static getAppPackageManager()Landroid/content/pm/PackageManager;
    .locals 1

    .prologue
    .line 145
    sget-object v0, Lcom/sec/android/app/ve/VEApp;->gContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    return-object v0
.end method

.method public static getBooleanValue(I)Z
    .locals 1
    .param p0, "resId"    # I

    .prologue
    .line 109
    sget-object v0, Lcom/sec/android/app/ve/VEApp;->gContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    return v0
.end method

.method public static getColor(I)I
    .locals 1
    .param p0, "resId"    # I

    .prologue
    .line 74
    sget-object v0, Lcom/sec/android/app/ve/VEApp;->gContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    return v0
.end method

.method public static getContentResolver()Landroid/content/ContentResolver;
    .locals 1

    .prologue
    .line 140
    sget-object v0, Lcom/sec/android/app/ve/VEApp;->gContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    return-object v0
.end method

.method public static getDateFormat()Ljava/text/DateFormat;
    .locals 1

    .prologue
    .line 289
    sget-object v0, Lcom/sec/android/app/ve/VEApp;->gContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    return-object v0
.end method

.method public static getDrawable(I)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p0, "resId"    # I

    .prologue
    .line 84
    sget-object v0, Lcom/sec/android/app/ve/VEApp;->gContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public static getFilesDirectory()Ljava/io/File;
    .locals 1

    .prologue
    .line 114
    sget-object v0, Lcom/sec/android/app/ve/VEApp;->gContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public static getFullySpecifiedExternalFilesPath()Ljava/lang/String;
    .locals 3

    .prologue
    .line 188
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/sec/android/app/ve/VEApp;->gContext:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getFullySpecifiedInternalFilepath(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "fileNameInPrivateFolder"    # Ljava/lang/String;

    .prologue
    .line 184
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/ve/VEApp;->gContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getInflater()Landroid/view/LayoutInflater;
    .locals 2

    .prologue
    .line 69
    sget-object v0, Lcom/sec/android/app/ve/VEApp;->gContext:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    return-object v0
.end method

.method public static getIntegerValue(I)I
    .locals 1
    .param p0, "resId"    # I

    .prologue
    .line 104
    sget-object v0, Lcom/sec/android/app/ve/VEApp;->gContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    return v0
.end method

.method public static getJNIVersion()F
    .locals 1

    .prologue
    .line 282
    sget v0, Lcom/sec/android/app/ve/VEApp;->mCurrentJNIVersion:F

    return v0
.end method

.method public static getLocaleName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 89
    sget-object v0, Lcom/sec/android/app/ve/VEApp;->gContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getPackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 285
    sget-object v0, Lcom/sec/android/app/ve/VEApp;->gContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getPixDimen(I)I
    .locals 1
    .param p0, "resId"    # I

    .prologue
    .line 79
    sget-object v0, Lcom/sec/android/app/ve/VEApp;->gContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method

.method public static getStringValue(I)Ljava/lang/String;
    .locals 1
    .param p0, "resId"    # I

    .prologue
    .line 94
    sget-object v0, Lcom/sec/android/app/ve/VEApp;->gContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getStringValue(ILjava/lang/Object;)Ljava/lang/String;
    .locals 3
    .param p0, "resId"    # I
    .param p1, "args"    # Ljava/lang/Object;

    .prologue
    .line 99
    sget-object v0, Lcom/sec/android/app/ve/VEApp;->gContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {v0, p0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getSystemService(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1
    .param p0, "inputMethodService"    # Ljava/lang/String;

    .prologue
    .line 124
    sget-object v0, Lcom/sec/android/app/ve/VEApp;->gContext:Landroid/content/Context;

    invoke-virtual {v0, p0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static getresource()Landroid/content/res/Resources;
    .locals 1

    .prologue
    .line 274
    sget-object v0, Lcom/sec/android/app/ve/VEApp;->gContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    return-object v0
.end method

.method public static init(Lcom/sec/android/app/ve/VEAdapter;)V
    .locals 8
    .param p0, "adap"    # Lcom/sec/android/app/ve/VEAdapter;

    .prologue
    .line 52
    sput-object p0, Lcom/sec/android/app/ve/VEApp;->gAdaper:Lcom/sec/android/app/ve/VEAdapter;

    .line 54
    new-instance v1, Ljava/io/File;

    sget-object v4, Lcom/sec/android/app/ve/common/ConfigUtils;->EXPORT_PATH:Ljava/lang/String;

    invoke-direct {v1, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 55
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 56
    invoke-virtual {v1}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v0

    .line 57
    .local v0, "allfileName":[Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 58
    array-length v5, v0

    const/4 v4, 0x0

    :goto_0
    if-lt v4, v5, :cond_1

    .line 66
    .end local v0    # "allfileName":[Ljava/lang/String;
    :cond_0
    return-void

    .line 58
    .restart local v0    # "allfileName":[Ljava/lang/String;
    :cond_1
    aget-object v2, v0, v4

    .line 59
    .local v2, "fileName":Ljava/lang/String;
    const-string v6, ".tmp"

    invoke-virtual {v2, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 60
    new-instance v3, Ljava/io/File;

    new-instance v6, Ljava/lang/StringBuilder;

    sget-object v7, Lcom/sec/android/app/ve/common/ConfigUtils;->EXPORT_PATH:Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 61
    .local v3, "tempFile":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 58
    .end local v3    # "tempFile":Ljava/io/File;
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method

.method public static makeToast(I)Landroid/widget/Toast;
    .locals 1
    .param p0, "aId"    # I

    .prologue
    .line 229
    sget-object v0, Lcom/sec/android/app/ve/VEApp;->gContext:Landroid/content/Context;

    invoke-static {v0, p0}, Lcom/sec/android/app/ve/VEApp;->makeToast(Landroid/content/Context;I)Landroid/widget/Toast;

    move-result-object v0

    return-object v0
.end method

.method public static makeToast(Landroid/content/Context;I)Landroid/widget/Toast;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "aId"    # I

    .prologue
    .line 232
    if-nez p0, :cond_0

    .line 233
    sget-object p0, Lcom/sec/android/app/ve/VEApp;->gContext:Landroid/content/Context;

    .line 234
    :cond_0
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    return-object v0
.end method

.method public static notifyOnTimeLineChanged()V
    .locals 3

    .prologue
    .line 263
    sget-object v2, Lcom/sec/android/app/ve/VEApp;->mNotifier:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 264
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    sget-object v2, Lcom/sec/android/app/ve/VEApp;->mNotifier:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    if-lt v1, v0, :cond_1

    .line 267
    :cond_0
    return-void

    .line 265
    :cond_1
    sget-object v2, Lcom/sec/android/app/ve/VEApp;->mNotifier:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/ve/VEApp$ApplicationNotifier;

    invoke-virtual {v2}, Lcom/sec/android/app/ve/VEApp$ApplicationNotifier;->onTimeLineChanged()V

    .line 264
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static declared-synchronized openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;
    .locals 4
    .param p0, "fileName"    # Ljava/lang/String;

    .prologue
    .line 173
    const-class v3, Lcom/sec/android/app/ve/VEApp;

    monitor-enter v3

    const/4 v1, 0x0

    .line 175
    .local v1, "fis":Ljava/io/FileInputStream;
    :try_start_0
    sget-object v2, Lcom/sec/android/app/ve/VEApp;->gContext:Landroid/content/Context;

    invoke-virtual {v2, p0}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 180
    :goto_0
    monitor-exit v3

    return-object v1

    .line 176
    :catch_0
    move-exception v0

    .line 177
    .local v0, "e":Ljava/io/FileNotFoundException;
    :try_start_1
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 173
    .end local v0    # "e":Ljava/io/FileNotFoundException;
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2
.end method

.method public static declared-synchronized openFileOutput(Ljava/lang/String;)Ljava/io/FileOutputStream;
    .locals 5
    .param p0, "fileName"    # Ljava/lang/String;

    .prologue
    .line 163
    const-class v3, Lcom/sec/android/app/ve/VEApp;

    monitor-enter v3

    const/4 v1, 0x0

    .line 165
    .local v1, "fos":Ljava/io/FileOutputStream;
    :try_start_0
    sget-object v2, Lcom/sec/android/app/ve/VEApp;->gContext:Landroid/content/Context;

    const/4 v4, 0x0

    invoke-virtual {v2, p0, v4}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 169
    :goto_0
    monitor-exit v3

    return-object v1

    .line 166
    :catch_0
    move-exception v0

    .line 167
    .local v0, "e":Ljava/io/FileNotFoundException;
    :try_start_1
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 163
    .end local v0    # "e":Ljava/io/FileNotFoundException;
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2
.end method

.method public static removeTimeNotifier(Lcom/sec/android/app/ve/VEApp$ApplicationNotifier;)V
    .locals 1
    .param p0, "adp"    # Lcom/sec/android/app/ve/VEApp$ApplicationNotifier;

    .prologue
    .line 243
    sget-object v0, Lcom/sec/android/app/ve/VEApp;->mNotifier:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 244
    sget-object v0, Lcom/sec/android/app/ve/VEApp;->mNotifier:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 246
    :cond_0
    return-void
.end method

.method public static setJNIVersion(F)V
    .locals 0
    .param p0, "version"    # F

    .prologue
    .line 278
    sput p0, Lcom/sec/android/app/ve/VEApp;->mCurrentJNIVersion:F

    .line 279
    return-void
.end method

.method public static showToast(II)Landroid/widget/Toast;
    .locals 1
    .param p0, "aId"    # I
    .param p1, "duration"    # I

    .prologue
    .line 215
    sget-object v0, Lcom/sec/android/app/ve/VEApp;->gContext:Landroid/content/Context;

    invoke-static {v0, p0, p1}, Lcom/sec/android/app/ve/VEApp;->showToast(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    return-object v0
.end method

.method public static showToast(Landroid/content/Context;II)Landroid/widget/Toast;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "aId"    # I
    .param p2, "duration"    # I

    .prologue
    .line 220
    if-nez p0, :cond_0

    .line 221
    sget-object p0, Lcom/sec/android/app/ve/VEApp;->gContext:Landroid/content/Context;

    .line 222
    :cond_0
    const/4 v1, 0x0

    invoke-static {p0, v1, p2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 223
    .local v0, "toast":Landroid/widget/Toast;
    invoke-virtual {v0, p1}, Landroid/widget/Toast;->setText(I)V

    .line 224
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 225
    return-object v0
.end method

.method public static showToast(IIII)V
    .locals 1
    .param p0, "aId"    # I
    .param p1, "gravity"    # I
    .param p2, "xOff"    # I
    .param p3, "yOff"    # I

    .prologue
    .line 193
    sget-object v0, Lcom/sec/android/app/ve/VEApp;->gContext:Landroid/content/Context;

    invoke-static {v0, p0, p1, p2, p3}, Lcom/sec/android/app/ve/VEApp;->showToast(Landroid/content/Context;IIII)V

    .line 194
    return-void
.end method

.method public static showToast(Landroid/content/Context;IIII)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "aId"    # I
    .param p2, "gravity"    # I
    .param p3, "xOff"    # I
    .param p4, "yOff"    # I

    .prologue
    .line 197
    if-nez p0, :cond_0

    .line 198
    sget-object p0, Lcom/sec/android/app/ve/VEApp;->gContext:Landroid/content/Context;

    .line 199
    :cond_0
    const/4 v1, 0x0

    invoke-static {p0, v1, p4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 200
    .local v0, "toast":Landroid/widget/Toast;
    invoke-virtual {v0, p2, p3, p4}, Landroid/widget/Toast;->setGravity(III)V

    .line 201
    invoke-virtual {v0, p1}, Landroid/widget/Toast;->setText(I)V

    .line 202
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 203
    return-void
.end method

.method public static showToast(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "textSeq"    # Ljava/lang/String;
    .param p2, "duration"    # I

    .prologue
    .line 208
    if-nez p0, :cond_0

    .line 209
    sget-object p0, Lcom/sec/android/app/ve/VEApp;->gContext:Landroid/content/Context;

    .line 210
    :cond_0
    const/4 v1, 0x0

    invoke-static {p0, v1, p2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 211
    .local v0, "toast":Landroid/widget/Toast;
    invoke-virtual {v0, p1}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    .line 212
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 213
    return-void
.end method

.method public static showToast(Ljava/lang/String;I)V
    .locals 1
    .param p0, "textSeq"    # Ljava/lang/String;
    .param p1, "duration"    # I

    .prologue
    .line 205
    sget-object v0, Lcom/sec/android/app/ve/VEApp;->gContext:Landroid/content/Context;

    invoke-static {v0, p0, p1}, Lcom/sec/android/app/ve/VEApp;->showToast(Landroid/content/Context;Ljava/lang/String;I)V

    .line 206
    return-void
.end method

.method public static startActivity(Landroid/content/Intent;)V
    .locals 1
    .param p0, "intent"    # Landroid/content/Intent;

    .prologue
    .line 270
    sget-object v0, Lcom/sec/android/app/ve/VEApp;->gContext:Landroid/content/Context;

    invoke-virtual {v0, p0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 271
    return-void
.end method

.method public static unbindFromService(Landroid/content/ServiceConnection;)V
    .locals 1
    .param p0, "connection"    # Landroid/content/ServiceConnection;

    .prologue
    .line 134
    if-eqz p0, :cond_0

    .line 135
    sget-object v0, Lcom/sec/android/app/ve/VEApp;->gContext:Landroid/content/Context;

    invoke-virtual {v0, p0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 136
    :cond_0
    return-void
.end method
