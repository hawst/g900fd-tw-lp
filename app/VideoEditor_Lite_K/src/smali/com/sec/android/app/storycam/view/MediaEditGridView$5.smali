.class Lcom/sec/android/app/storycam/view/MediaEditGridView$5;
.super Landroid/os/Handler;
.source "MediaEditGridView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/storycam/view/MediaEditGridView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/storycam/view/MediaEditGridView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView$5;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    .line 151
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 154
    iget v2, p1, Landroid/os/Message;->what:I

    sparse-switch v2, :sswitch_data_0

    .line 187
    :cond_0
    :goto_0
    return-void

    .line 156
    :sswitch_0
    iget-object v2, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView$5;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    iget-object v3, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView$5;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    # getter for: Lcom/sec/android/app/storycam/view/MediaEditGridView;->mDraggingView:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;
    invoke-static {v3}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->access$6(Lcom/sec/android/app/storycam/view/MediaEditGridView;)Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->indexOfChild(Landroid/view/View;)I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView$5;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    # getter for: Lcom/sec/android/app/storycam/view/MediaEditGridView;->mCurrentDropIndex:I
    invoke-static {v3}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->access$7(Lcom/sec/android/app/storycam/view/MediaEditGridView;)I

    move-result v3

    if-eq v2, v3, :cond_1

    .line 157
    iget-object v2, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView$5;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    iget-object v3, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView$5;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    # getter for: Lcom/sec/android/app/storycam/view/MediaEditGridView;->mDraggingView:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;
    invoke-static {v3}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->access$6(Lcom/sec/android/app/storycam/view/MediaEditGridView;)Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->removeView(Landroid/view/View;)V

    .line 158
    iget-object v2, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView$5;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    iget-object v3, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView$5;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    # getter for: Lcom/sec/android/app/storycam/view/MediaEditGridView;->mDraggingView:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;
    invoke-static {v3}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->access$6(Lcom/sec/android/app/storycam/view/MediaEditGridView;)Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView$5;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    # getter for: Lcom/sec/android/app/storycam/view/MediaEditGridView;->mCurrentDropIndex:I
    invoke-static {v4}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->access$7(Lcom/sec/android/app/storycam/view/MediaEditGridView;)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->addView(Landroid/view/View;I)V

    .line 160
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView$5;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    # getter for: Lcom/sec/android/app/storycam/view/MediaEditGridView;->mDraggingView:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;
    invoke-static {v2}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->access$6(Lcom/sec/android/app/storycam/view/MediaEditGridView;)Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v5}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->refreshView(Landroid/graphics/Bitmap;Z)V

    goto :goto_0

    .line 163
    :sswitch_1
    iget-object v2, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView$5;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    # getter for: Lcom/sec/android/app/storycam/view/MediaEditGridView;->mAutoScroll:Z
    invoke-static {v2}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->access$8(Lcom/sec/android/app/storycam/view/MediaEditGridView;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 164
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 165
    .local v0, "direction":I
    const/4 v2, 0x2

    if-ne v0, v2, :cond_3

    .line 166
    # getter for: Lcom/sec/android/app/storycam/view/MediaEditGridView;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;
    invoke-static {}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->access$1()Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;->getCurrentConfig()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    if-ne v2, v3, :cond_2

    .line 167
    iget-object v2, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView$5;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    invoke-virtual {v2}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/storycam/view/AutoScrollableVerticalScrollView;

    invoke-virtual {v2, v5, v4}, Lcom/sec/android/app/storycam/view/AutoScrollableVerticalScrollView;->customSmoothScrollBy(II)V

    .line 178
    :goto_1
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v1

    .line 179
    .local v1, "msg1":Landroid/os/Message;
    const/16 v2, 0x65

    iput v2, v1, Landroid/os/Message;->what:I

    .line 180
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 181
    iget-object v2, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView$5;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    # getter for: Lcom/sec/android/app/storycam/view/MediaEditGridView;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->access$9(Lcom/sec/android/app/storycam/view/MediaEditGridView;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 169
    .end local v1    # "msg1":Landroid/os/Message;
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView$5;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    invoke-virtual {v2}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/storycam/view/AutoScrollableHorizontalScrollView;

    invoke-virtual {v2, v4, v5}, Lcom/sec/android/app/storycam/view/AutoScrollableHorizontalScrollView;->customSmoothScrollBy(II)V

    goto :goto_1

    .line 172
    :cond_3
    # getter for: Lcom/sec/android/app/storycam/view/MediaEditGridView;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;
    invoke-static {}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->access$1()Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;->getCurrentConfig()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    if-ne v2, v3, :cond_4

    .line 173
    iget-object v2, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView$5;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    invoke-virtual {v2}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/storycam/view/AutoScrollableVerticalScrollView;

    invoke-virtual {v2, v5, v3}, Lcom/sec/android/app/storycam/view/AutoScrollableVerticalScrollView;->customSmoothScrollBy(II)V

    goto :goto_1

    .line 175
    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView$5;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    invoke-virtual {v2}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/storycam/view/AutoScrollableHorizontalScrollView;

    invoke-virtual {v2, v3, v5}, Lcom/sec/android/app/storycam/view/AutoScrollableHorizontalScrollView;->customSmoothScrollBy(II)V

    goto :goto_1

    .line 154
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x65 -> :sswitch_1
    .end sparse-switch
.end method
