.class public Lcom/sec/android/app/ve/data/VEAlbum;
.super Ljava/lang/Object;
.source "VEAlbum.java"


# instance fields
.field private mAlbumArtPath:Ljava/lang/String;

.field private mAlbumArtistName:Ljava/lang/String;

.field private mAlbumId:I

.field private mAlbumName:Ljava/lang/String;

.field private mSongsCount:I

.field private musicCategoryId:Ljava/lang/String;

.field private musicCategoryName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getMusicCategoryId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/ve/data/VEAlbum;->musicCategoryId:Ljava/lang/String;

    return-object v0
.end method

.method public getMusicCategoryName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/ve/data/VEAlbum;->musicCategoryName:Ljava/lang/String;

    return-object v0
.end method

.method public getSongsCount()I
    .locals 1

    .prologue
    .line 57
    iget v0, p0, Lcom/sec/android/app/ve/data/VEAlbum;->mSongsCount:I

    return v0
.end method

.method public getVEAlbumArtPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/ve/data/VEAlbum;->mAlbumArtPath:Ljava/lang/String;

    return-object v0
.end method

.method public getVEAlbumArtistName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/ve/data/VEAlbum;->mAlbumArtistName:Ljava/lang/String;

    return-object v0
.end method

.method public getVEAlbumId()I
    .locals 1

    .prologue
    .line 53
    iget v0, p0, Lcom/sec/android/app/ve/data/VEAlbum;->mAlbumId:I

    return v0
.end method

.method public getVEAlbumName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/app/ve/data/VEAlbum;->mAlbumName:Ljava/lang/String;

    return-object v0
.end method

.method public setMusicCategoryId(Ljava/lang/String;)V
    .locals 0
    .param p1, "genresId"    # Ljava/lang/String;

    .prologue
    .line 69
    iput-object p1, p0, Lcom/sec/android/app/ve/data/VEAlbum;->musicCategoryId:Ljava/lang/String;

    .line 70
    return-void
.end method

.method public setMusicCategoryName(Ljava/lang/String;)V
    .locals 0
    .param p1, "genresName"    # Ljava/lang/String;

    .prologue
    .line 77
    iput-object p1, p0, Lcom/sec/android/app/ve/data/VEAlbum;->musicCategoryName:Ljava/lang/String;

    .line 78
    return-void
.end method

.method public setSongsCount(I)V
    .locals 0
    .param p1, "mSongsCount"    # I

    .prologue
    .line 61
    iput p1, p0, Lcom/sec/android/app/ve/data/VEAlbum;->mSongsCount:I

    .line 62
    return-void
.end method

.method public setVEAlbumArtPath(Ljava/lang/String;)V
    .locals 0
    .param p1, "aAlbumArtPath"    # Ljava/lang/String;

    .prologue
    .line 41
    iput-object p1, p0, Lcom/sec/android/app/ve/data/VEAlbum;->mAlbumArtPath:Ljava/lang/String;

    .line 42
    return-void
.end method

.method public setVEAlbumArtistName(Ljava/lang/String;)V
    .locals 0
    .param p1, "aAlbumArtistName"    # Ljava/lang/String;

    .prologue
    .line 33
    iput-object p1, p0, Lcom/sec/android/app/ve/data/VEAlbum;->mAlbumArtistName:Ljava/lang/String;

    .line 34
    return-void
.end method

.method public setVEAlbumId(I)V
    .locals 0
    .param p1, "aAlbumId"    # I

    .prologue
    .line 49
    iput p1, p0, Lcom/sec/android/app/ve/data/VEAlbum;->mAlbumId:I

    .line 50
    return-void
.end method

.method public setVEAlbumName(Ljava/lang/String;)V
    .locals 0
    .param p1, "aAlbumName"    # Ljava/lang/String;

    .prologue
    .line 25
    iput-object p1, p0, Lcom/sec/android/app/ve/data/VEAlbum;->mAlbumName:Ljava/lang/String;

    .line 26
    return-void
.end method
