.class public Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher;
.super Ljava/lang/Thread;
.source "FolderThumbnailFetcher.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher$FolderThumbCallback;,
        Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher$FolderThumbFetcherInfo;
    }
.end annotation


# static fields
.field private static final CANVAS_HEIGHT:I

.field private static final CANVAS_WIDTH:I

.field private static final IMAGE_TYPE:I = 0x5

.field private static final RADIUS:I

.field private static final ROT_THUMB_HEIGHT:I

.field private static final ROT_THUMB_WIDTH:I

.field private static final STROKE_THICKNESS:I = 0x2

.field private static final THUMB_HEIGHT:I

.field private static final THUMB_WIDTH:I

.field private static final VIDEO_TYPE:I = 0x4

.field private static final mJobList:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher$FolderThumbFetcherInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mFolderThumbCbk:Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher$FolderThumbCallback;

.field private mPaint:Landroid/graphics/Paint;

.field private mRotAngles:[F

.field protected mTerminate:Z

.field private mediaType:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const-wide v4, 0x3feeb851eb851eb8L    # 0.96

    const-wide v2, 0x3feccccccccccccdL    # 0.9

    .line 35
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    sput-object v0, Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher;->mJobList:Ljava/util/Vector;

    .line 40
    sget v0, Lcom/sec/android/app/ve/R$dimen;->video_image_picker_item_width:I

    invoke-static {v0}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v0

    sput v0, Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher;->CANVAS_WIDTH:I

    .line 41
    sget v0, Lcom/sec/android/app/ve/R$dimen;->video_image_picker_item_height:I

    invoke-static {v0}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v0

    sput v0, Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher;->CANVAS_HEIGHT:I

    .line 42
    sget v0, Lcom/sec/android/app/ve/R$dimen;->video_image_picker_item_width:I

    invoke-static {v0}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v0

    int-to-double v0, v0

    mul-double/2addr v0, v4

    double-to-int v0, v0

    sput v0, Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher;->THUMB_WIDTH:I

    .line 43
    sget v0, Lcom/sec/android/app/ve/R$dimen;->video_image_picker_item_height:I

    invoke-static {v0}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v0

    int-to-double v0, v0

    mul-double/2addr v0, v4

    double-to-int v0, v0

    sput v0, Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher;->THUMB_HEIGHT:I

    .line 44
    sget v0, Lcom/sec/android/app/ve/R$dimen;->video_image_picker_item_width:I

    invoke-static {v0}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v0

    int-to-double v0, v0

    mul-double/2addr v0, v2

    double-to-int v0, v0

    sput v0, Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher;->ROT_THUMB_WIDTH:I

    .line 45
    sget v0, Lcom/sec/android/app/ve/R$dimen;->video_image_picker_item_height:I

    invoke-static {v0}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v0

    int-to-double v0, v0

    mul-double/2addr v0, v2

    double-to-int v0, v0

    sput v0, Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher;->ROT_THUMB_HEIGHT:I

    .line 46
    sget v0, Lcom/sec/android/app/ve/R$dimen;->addmedia_imagevideo_corner_radius:I

    invoke-static {v0}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v0

    sput v0, Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher;->RADIUS:I

    .line 49
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 39
    const/4 v0, 0x3

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher;->mRotAngles:[F

    .line 50
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher;->mediaType:I

    .line 54
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x7

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher;->mPaint:Landroid/graphics/Paint;

    .line 55
    iget-object v0, p0, Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher;->mPaint:Landroid/graphics/Paint;

    sget v1, Lcom/sec/android/app/ve/R$color;->folderview_thumb_frameColor:I

    invoke-static {v1}, Lcom/sec/android/app/ve/VEApp;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 56
    return-void

    .line 39
    :array_0
    .array-data 4
        0x0
        -0x3fc00000    # -3.0f
        0x40400000    # 3.0f
    .end array-data
.end method

.method private checkJobExists(Ljava/lang/String;)Z
    .locals 4
    .param p1, "bucketId"    # Ljava/lang/String;

    .prologue
    .line 81
    :try_start_0
    sget-object v3, Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher;->mJobList:Ljava/util/Vector;

    invoke-virtual {v3}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 82
    .local v2, "lIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher$FolderThumbFetcherInfo;>;"
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 93
    .end local v2    # "lIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher$FolderThumbFetcherInfo;>;"
    :goto_0
    const/4 v3, 0x0

    :goto_1
    return v3

    .line 83
    .restart local v2    # "lIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher$FolderThumbFetcherInfo;>;"
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher$FolderThumbFetcherInfo;

    .line 84
    .local v1, "fetcherInfo":Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher$FolderThumbFetcherInfo;
    # getter for: Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher$FolderThumbFetcherInfo;->mBucketId:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher$FolderThumbFetcherInfo;->access$0(Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher$FolderThumbFetcherInfo;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-nez v3, :cond_0

    .line 85
    const/4 v3, 0x1

    goto :goto_1

    .line 88
    .end local v1    # "fetcherInfo":Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher$FolderThumbFetcherInfo;
    .end local v2    # "lIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher$FolderThumbFetcherInfo;>;"
    :catch_0
    move-exception v0

    .line 90
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public addOperation(Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 2
    .param p1, "bucketId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 102
    .local p2, "thumbPaths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct {p0, p1}, Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher;->checkJobExists(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 104
    new-instance v0, Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher$FolderThumbFetcherInfo;

    invoke-direct {v0, p1, p2}, Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher$FolderThumbFetcherInfo;-><init>(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 105
    .local v0, "info":Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher$FolderThumbFetcherInfo;
    sget-object v1, Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher;->mJobList:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 107
    monitor-enter p0

    .line 108
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V

    .line 107
    monitor-exit p0

    .line 111
    .end local v0    # "info":Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher$FolderThumbFetcherInfo;
    :cond_0
    return-void

    .line 107
    .restart local v0    # "info":Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher$FolderThumbFetcherInfo;
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public clearPendingOperations()V
    .locals 2

    .prologue
    .line 119
    :try_start_0
    sget-object v1, Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher;->mJobList:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->clear()V

    .line 121
    monitor-enter p0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 122
    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V

    .line 121
    monitor-exit p0

    .line 127
    :goto_0
    return-void

    .line 121
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 124
    :catch_0
    move-exception v0

    .line 125
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method createFolderViewBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    .locals 25
    .param p1, "src"    # Landroid/graphics/Bitmap;
    .param p2, "index"    # I

    .prologue
    .line 226
    if-nez p1, :cond_0

    .line 227
    const/4 v4, 0x0

    .line 300
    :goto_0
    return-object v4

    .line 229
    :cond_0
    if-nez p2, :cond_3

    sget v18, Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher;->THUMB_WIDTH:I

    .line 230
    .local v18, "dstWidth":I
    :goto_1
    if-nez p2, :cond_4

    sget v17, Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher;->THUMB_HEIGHT:I

    .line 232
    .local v17, "dstHeight":I
    :goto_2
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    int-to-float v4, v4

    move/from16 v0, v18

    int-to-float v11, v0

    div-float v24, v4, v11

    .line 233
    .local v24, "widRatio":F
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    int-to-float v4, v4

    move/from16 v0, v17

    int-to-float v11, v0

    div-float v19, v4, v11

    .line 235
    .local v19, "heiRatio":F
    new-instance v9, Landroid/graphics/Matrix;

    invoke-direct {v9}, Landroid/graphics/Matrix;-><init>()V

    .line 236
    .local v9, "mat":Landroid/graphics/Matrix;
    invoke-virtual {v9}, Landroid/graphics/Matrix;->reset()V

    .line 238
    const/4 v5, 0x0

    .line 239
    .local v5, "x":I
    const/4 v6, 0x0

    .line 240
    .local v6, "y":I
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    .line 241
    .local v7, "width":I
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    .line 243
    .local v8, "height":I
    cmpg-float v4, v24, v19

    if-gez v4, :cond_5

    .line 245
    move/from16 v0, v17

    int-to-float v4, v0

    mul-float v4, v4, v24

    float-to-int v8, v4

    .line 246
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    sub-int/2addr v4, v8

    div-int/lit8 v6, v4, 0x2

    .line 247
    const/high16 v4, 0x3f800000    # 1.0f

    div-float v4, v4, v24

    const/high16 v11, 0x3f800000    # 1.0f

    div-float v11, v11, v24

    invoke-virtual {v9, v4, v11}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 256
    :goto_3
    const/4 v10, 0x1

    move-object/from16 v4, p1

    invoke-static/range {v4 .. v10}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v23

    .line 257
    .local v23, "temp":Landroid/graphics/Bitmap;
    const/4 v10, 0x0

    .line 259
    .local v10, "rndBmp":Landroid/graphics/Bitmap;
    if-eqz v23, :cond_1

    .line 261
    invoke-virtual/range {v23 .. v23}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    invoke-virtual/range {v23 .. v23}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v11

    sget-object v12, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v4, v11, v12}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v10

    .line 262
    new-instance v22, Landroid/graphics/Canvas;

    move-object/from16 v0, v22

    invoke-direct {v0, v10}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 263
    .local v22, "rndCnvs":Landroid/graphics/Canvas;
    new-instance v21, Landroid/graphics/RectF;

    const/4 v4, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {v23 .. v23}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v12

    int-to-float v12, v12

    invoke-virtual/range {v23 .. v23}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v13

    int-to-float v13, v13

    move-object/from16 v0, v21

    invoke-direct {v0, v4, v11, v12, v13}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 265
    .local v21, "rect":Landroid/graphics/RectF;
    new-instance v20, Landroid/graphics/Paint;

    invoke-direct/range {v20 .. v20}, Landroid/graphics/Paint;-><init>()V

    .line 266
    .local v20, "paint":Landroid/graphics/Paint;
    const/4 v4, 0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 267
    const/high16 v4, -0x1000000

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 268
    const/4 v4, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v0, v4, v11, v12, v13}, Landroid/graphics/Canvas;->drawARGB(IIII)V

    .line 269
    sget v4, Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher;->RADIUS:I

    int-to-float v4, v4

    sget v11, Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher;->RADIUS:I

    int-to-float v11, v11

    move-object/from16 v0, v22

    move-object/from16 v1, v21

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v4, v11, v2}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 271
    new-instance v20, Landroid/graphics/Paint;

    .end local v20    # "paint":Landroid/graphics/Paint;
    invoke-direct/range {v20 .. v20}, Landroid/graphics/Paint;-><init>()V

    .line 272
    .restart local v20    # "paint":Landroid/graphics/Paint;
    const/4 v4, 0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 273
    new-instance v4, Landroid/graphics/PorterDuffXfermode;

    sget-object v11, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v4, v11}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 274
    invoke-virtual/range {v22 .. v22}, Landroid/graphics/Canvas;->getClipBounds()Landroid/graphics/Rect;

    move-result-object v4

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    move-object/from16 v2, v21

    move-object/from16 v3, v20

    invoke-virtual {v0, v1, v4, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 276
    new-instance v20, Landroid/graphics/Paint;

    .end local v20    # "paint":Landroid/graphics/Paint;
    invoke-direct/range {v20 .. v20}, Landroid/graphics/Paint;-><init>()V

    .line 277
    .restart local v20    # "paint":Landroid/graphics/Paint;
    const/4 v4, 0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 278
    const/high16 v4, -0x1000000

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 279
    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 280
    const/high16 v4, 0x40000000    # 2.0f

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 281
    sget v4, Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher;->RADIUS:I

    int-to-float v4, v4

    sget v11, Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher;->RADIUS:I

    int-to-float v11, v11

    move-object/from16 v0, v22

    move-object/from16 v1, v21

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v4, v11, v2}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 283
    invoke-virtual/range {v23 .. v23}, Landroid/graphics/Bitmap;->recycle()V

    .line 284
    const/16 v23, 0x0

    .line 287
    .end local v20    # "paint":Landroid/graphics/Paint;
    .end local v21    # "rect":Landroid/graphics/RectF;
    .end local v22    # "rndCnvs":Landroid/graphics/Canvas;
    :cond_1
    if-eqz v10, :cond_2

    .line 289
    invoke-virtual {v9}, Landroid/graphics/Matrix;->reset()V

    .line 290
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher;->mRotAngles:[F

    aget v4, v4, p2

    invoke-virtual {v9, v4}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 292
    const/4 v11, 0x0

    const/4 v12, 0x0

    .line 293
    invoke-virtual {v10}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v13

    invoke-virtual {v10}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v14

    const/16 v16, 0x1

    move-object v15, v9

    .line 292
    invoke-static/range {v10 .. v16}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object p1

    .line 295
    invoke-virtual {v10}, Landroid/graphics/Bitmap;->recycle()V

    .line 296
    const/4 v10, 0x0

    .line 299
    :cond_2
    invoke-static {}, Ljava/lang/System;->gc()V

    move-object/from16 v4, p1

    .line 300
    goto/16 :goto_0

    .line 229
    .end local v5    # "x":I
    .end local v6    # "y":I
    .end local v7    # "width":I
    .end local v8    # "height":I
    .end local v9    # "mat":Landroid/graphics/Matrix;
    .end local v10    # "rndBmp":Landroid/graphics/Bitmap;
    .end local v17    # "dstHeight":I
    .end local v18    # "dstWidth":I
    .end local v19    # "heiRatio":F
    .end local v23    # "temp":Landroid/graphics/Bitmap;
    .end local v24    # "widRatio":F
    :cond_3
    sget v18, Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher;->ROT_THUMB_WIDTH:I

    goto/16 :goto_1

    .line 230
    .restart local v18    # "dstWidth":I
    :cond_4
    sget v17, Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher;->ROT_THUMB_HEIGHT:I

    goto/16 :goto_2

    .line 251
    .restart local v5    # "x":I
    .restart local v6    # "y":I
    .restart local v7    # "width":I
    .restart local v8    # "height":I
    .restart local v9    # "mat":Landroid/graphics/Matrix;
    .restart local v17    # "dstHeight":I
    .restart local v19    # "heiRatio":F
    .restart local v24    # "widRatio":F
    :cond_5
    move/from16 v0, v18

    int-to-float v4, v0

    mul-float v4, v4, v19

    float-to-int v7, v4

    .line 252
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    sub-int/2addr v4, v7

    div-int/lit8 v5, v4, 0x2

    .line 253
    const/high16 v4, 0x3f800000    # 1.0f

    div-float v4, v4, v19

    const/high16 v11, 0x3f800000    # 1.0f

    div-float v11, v11, v19

    invoke-virtual {v9, v4, v11}, Landroid/graphics/Matrix;->postScale(FF)Z

    goto/16 :goto_3
.end method

.method public run()V
    .locals 18

    .prologue
    .line 134
    new-instance v10, Landroid/graphics/Matrix;

    invoke-direct {v10}, Landroid/graphics/Matrix;-><init>()V

    .line 135
    .local v10, "mat":Landroid/graphics/Matrix;
    :goto_0
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher;->mTerminate:Z

    if-eqz v14, :cond_2

    .line 222
    return-void

    .line 139
    :cond_0
    sget-object v14, Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher;->mJobList:Ljava/util/Vector;

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher$FolderThumbFetcherInfo;

    .line 141
    .local v8, "info":Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher$FolderThumbFetcherInfo;
    :try_start_0
    # getter for: Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher$FolderThumbFetcherInfo;->mThumbnailPaths:Ljava/util/ArrayList;
    invoke-static {v8}, Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher$FolderThumbFetcherInfo;->access$1(Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher$FolderThumbFetcherInfo;)Ljava/util/ArrayList;

    move-result-object v14

    if-eqz v14, :cond_1

    # getter for: Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher$FolderThumbFetcherInfo;->mThumbnailPaths:Ljava/util/ArrayList;
    invoke-static {v8}, Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher$FolderThumbFetcherInfo;->access$1(Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher$FolderThumbFetcherInfo;)Ljava/util/ArrayList;

    move-result-object v14

    invoke-virtual {v14}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v14

    if-nez v14, :cond_1

    .line 143
    sget v14, Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher;->CANVAS_WIDTH:I

    sget v15, Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher;->CANVAS_HEIGHT:I

    sget-object v16, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static/range {v14 .. v16}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 144
    .local v4, "finalBitmap":Landroid/graphics/Bitmap;
    new-instance v5, Landroid/graphics/Canvas;

    invoke-direct {v5, v4}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 145
    .local v5, "finalCanvas":Landroid/graphics/Canvas;
    # getter for: Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher$FolderThumbFetcherInfo;->mThumbnailPaths:Ljava/util/ArrayList;
    invoke-static {v8}, Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher$FolderThumbFetcherInfo;->access$1(Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher$FolderThumbFetcherInfo;)Ljava/util/ArrayList;

    move-result-object v14

    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v14

    add-int/lit8 v6, v14, -0x1

    .local v6, "i":I
    :goto_1
    if-gez v6, :cond_4

    .line 200
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher;->mFolderThumbCbk:Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher$FolderThumbCallback;

    # getter for: Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher$FolderThumbFetcherInfo;->mBucketId:Ljava/lang/String;
    invoke-static {v8}, Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher$FolderThumbFetcherInfo;->access$0(Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher$FolderThumbFetcherInfo;)Ljava/lang/String;

    move-result-object v15

    invoke-interface {v14, v4, v15}, Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher$FolderThumbCallback;->folderThumbnailCreated(Landroid/graphics/Bitmap;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 207
    .end local v4    # "finalBitmap":Landroid/graphics/Bitmap;
    .end local v5    # "finalCanvas":Landroid/graphics/Canvas;
    .end local v6    # "i":I
    :cond_1
    :goto_2
    sget-object v14, Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher;->mJobList:Ljava/util/Vector;

    invoke-virtual {v14}, Ljava/util/Vector;->isEmpty()Z

    move-result v14

    if-nez v14, :cond_2

    .line 208
    sget-object v14, Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher;->mJobList:Ljava/util/Vector;

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Ljava/util/Vector;->remove(I)Ljava/lang/Object;

    .line 137
    .end local v8    # "info":Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher$FolderThumbFetcherInfo;
    :cond_2
    sget-object v14, Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher;->mJobList:Ljava/util/Vector;

    invoke-virtual {v14}, Ljava/util/Vector;->isEmpty()Z

    move-result v14

    if-eqz v14, :cond_0

    .line 212
    monitor-enter p0

    .line 214
    :try_start_1
    sget-object v14, Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher;->mJobList:Ljava/util/Vector;

    invoke-virtual {v14}, Ljava/util/Vector;->isEmpty()Z

    move-result v14

    if-eqz v14, :cond_3

    .line 215
    const-wide/16 v14, 0x7d0

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v15}, Ljava/lang/Object;->wait(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 212
    :cond_3
    :goto_3
    :try_start_2
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v14

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v14

    .line 147
    .restart local v4    # "finalBitmap":Landroid/graphics/Bitmap;
    .restart local v5    # "finalCanvas":Landroid/graphics/Canvas;
    .restart local v6    # "i":I
    .restart local v8    # "info":Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher$FolderThumbFetcherInfo;
    :cond_4
    :try_start_3
    invoke-virtual {v5}, Landroid/graphics/Canvas;->save()I

    .line 149
    const/4 v12, 0x0

    .line 150
    .local v12, "tempBitmap":Landroid/graphics/Bitmap;
    const/4 v7, 0x0

    .line 152
    .local v7, "imgBitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v10}, Landroid/graphics/Matrix;->reset()V

    .line 153
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher;->mRotAngles:[F

    aget v14, v14, v6

    invoke-virtual {v10, v14}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 155
    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher;->mediaType:I

    const/4 v15, 0x5

    if-ne v14, v15, :cond_8

    .line 156
    # getter for: Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher$FolderThumbFetcherInfo;->mThumbnailPaths:Ljava/util/ArrayList;
    invoke-static {v8}, Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher$FolderThumbFetcherInfo;->access$1(Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher$FolderThumbFetcherInfo;)Ljava/util/ArrayList;

    move-result-object v14

    invoke-virtual {v14, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/String;

    const/4 v15, 0x1

    sget v16, Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher;->THUMB_WIDTH:I

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    sget v17, Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher;->THUMB_HEIGHT:I

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    invoke-static/range {v14 .. v17}, Lcom/sec/android/app/ve/common/MediaUtils;->getImageBitmap(Ljava/lang/String;IFF)Landroid/graphics/Bitmap;

    move-result-object v12

    .line 160
    :cond_5
    :goto_4
    if-eqz v12, :cond_6

    .line 162
    move-object/from16 v0, p0

    invoke-virtual {v0, v12, v6}, Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher;->createFolderViewBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 163
    invoke-virtual {v12}, Landroid/graphics/Bitmap;->recycle()V

    .line 164
    const/4 v12, 0x0

    .line 167
    :cond_6
    if-eqz v7, :cond_7

    .line 169
    invoke-virtual {v10}, Landroid/graphics/Matrix;->reset()V

    .line 171
    new-instance v11, Landroid/graphics/Rect;

    const/4 v14, 0x0

    const/4 v15, 0x0

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v16

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v17

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-direct {v11, v14, v15, v0, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 173
    .local v11, "srcRect":Landroid/graphics/Rect;
    const/4 v9, 0x0

    .line 174
    .local v9, "left":I
    const/4 v13, 0x0

    .line 176
    .local v13, "top":I
    if-nez v6, :cond_9

    .line 178
    sget v14, Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher;->CANVAS_WIDTH:I

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v15

    sub-int/2addr v14, v15

    div-int/lit8 v9, v14, 0x3

    .line 179
    sget v14, Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher;->CANVAS_HEIGHT:I

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v15

    sub-int/2addr v14, v15

    div-int/lit8 v13, v14, 0x2

    .line 192
    :goto_5
    new-instance v2, Landroid/graphics/Rect;

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v14

    add-int/2addr v14, v9

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v15

    add-int/2addr v15, v13

    invoke-direct {v2, v9, v13, v14, v15}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 194
    .local v2, "dstRect":Landroid/graphics/Rect;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v5, v7, v11, v2, v14}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 195
    invoke-virtual {v5}, Landroid/graphics/Canvas;->restore()V

    .line 145
    .end local v2    # "dstRect":Landroid/graphics/Rect;
    .end local v9    # "left":I
    .end local v11    # "srcRect":Landroid/graphics/Rect;
    .end local v13    # "top":I
    :cond_7
    add-int/lit8 v6, v6, -0x1

    goto/16 :goto_1

    .line 157
    :cond_8
    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher;->mediaType:I

    const/4 v15, 0x4

    if-ne v14, v15, :cond_5

    .line 158
    # getter for: Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher$FolderThumbFetcherInfo;->mThumbnailPaths:Ljava/util/ArrayList;
    invoke-static {v8}, Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher$FolderThumbFetcherInfo;->access$1(Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher$FolderThumbFetcherInfo;)Ljava/util/ArrayList;

    move-result-object v14

    invoke-virtual {v14, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/String;

    const/4 v15, 0x1

    invoke-static {v14, v15}, Landroid/media/ThumbnailUtils;->createVideoThumbnail(Ljava/lang/String;I)Landroid/graphics/Bitmap;

    move-result-object v12

    goto :goto_4

    .line 181
    .restart local v9    # "left":I
    .restart local v11    # "srcRect":Landroid/graphics/Rect;
    .restart local v13    # "top":I
    :cond_9
    const/4 v14, 0x1

    if-ne v14, v6, :cond_a

    .line 183
    const/4 v9, 0x0

    .line 184
    sget v14, Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher;->CANVAS_HEIGHT:I

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v15

    sub-int v13, v14, v15

    .line 185
    goto :goto_5

    .line 188
    :cond_a
    sget v14, Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher;->CANVAS_WIDTH:I

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v15

    sub-int v9, v14, v15

    .line 189
    sget v14, Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher;->CANVAS_HEIGHT:I

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v15

    sub-int/2addr v14, v15

    div-int/lit8 v13, v14, 0x2
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_5

    .line 203
    .end local v4    # "finalBitmap":Landroid/graphics/Bitmap;
    .end local v5    # "finalCanvas":Landroid/graphics/Canvas;
    .end local v6    # "i":I
    .end local v7    # "imgBitmap":Landroid/graphics/Bitmap;
    .end local v9    # "left":I
    .end local v11    # "srcRect":Landroid/graphics/Rect;
    .end local v12    # "tempBitmap":Landroid/graphics/Bitmap;
    .end local v13    # "top":I
    :catch_0
    move-exception v3

    .line 204
    .local v3, "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_2

    .line 217
    .end local v3    # "e":Ljava/lang/Exception;
    .end local v8    # "info":Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher$FolderThumbFetcherInfo;
    :catch_1
    move-exception v3

    .line 218
    .local v3, "e":Ljava/lang/InterruptedException;
    :try_start_4
    invoke-virtual {v3}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_3
.end method

.method public setFolderThumbCallback(Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher$FolderThumbCallback;)V
    .locals 0
    .param p1, "cbk"    # Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher$FolderThumbCallback;

    .prologue
    .line 333
    if-eqz p1, :cond_0

    .line 334
    iput-object p1, p0, Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher;->mFolderThumbCbk:Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher$FolderThumbCallback;

    .line 335
    :cond_0
    return-void
.end method

.method public setMediaType(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 59
    iput p1, p0, Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher;->mediaType:I

    .line 60
    return-void
.end method

.method public terminate()V
    .locals 1

    .prologue
    .line 66
    sget-object v0, Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher;->mJobList:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clear()V

    .line 67
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher;->mTerminate:Z

    .line 69
    monitor-enter p0

    .line 70
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V

    .line 69
    monitor-exit p0

    .line 72
    return-void

    .line 69
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
