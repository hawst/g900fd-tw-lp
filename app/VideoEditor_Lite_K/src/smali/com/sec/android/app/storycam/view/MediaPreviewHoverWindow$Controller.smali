.class public interface abstract Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$Controller;
.super Ljava/lang/Object;
.source "MediaPreviewHoverWindow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Controller"
.end annotation


# virtual methods
.method public abstract doLayout(Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;Landroid/view/View$OnHoverListener;)Landroid/view/View;
.end method

.method public abstract refreshThumbnails(Ljava/util/LinkedHashMap;I)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/graphics/Bitmap;",
            ">;I)V"
        }
    .end annotation
.end method

.method public abstract release()V
.end method
