.class public Lcom/sec/android/app/ve/thread/ImageThumbFetcher;
.super Lcom/sec/android/app/ve/thread/VideoImageThumbFetcher;
.source "ImageThumbFetcher.java"


# static fields
.field private static _instance:Lcom/sec/android/app/ve/thread/ImageThumbFetcher;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    new-instance v0, Lcom/sec/android/app/ve/thread/ImageThumbFetcher;

    invoke-direct {v0}, Lcom/sec/android/app/ve/thread/ImageThumbFetcher;-><init>()V

    sput-object v0, Lcom/sec/android/app/ve/thread/ImageThumbFetcher;->_instance:Lcom/sec/android/app/ve/thread/ImageThumbFetcher;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/sec/android/app/ve/thread/VideoImageThumbFetcher;-><init>()V

    .line 24
    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/sec/android/app/ve/thread/ImageThumbFetcher;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 29
    sget-object v0, Lcom/sec/android/app/ve/thread/ImageThumbFetcher;->_instance:Lcom/sec/android/app/ve/thread/ImageThumbFetcher;

    return-object v0
.end method


# virtual methods
.method public addOperation(Ljava/lang/String;IJLcom/sec/android/app/ve/thread/VideoImageThumbFetcher$VideoImageThumbnaiFetcherCallbak;)V
    .locals 3
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "aPosition"    # I
    .param p3, "aId"    # J
    .param p5, "callback"    # Lcom/sec/android/app/ve/thread/VideoImageThumbFetcher$VideoImageThumbnaiFetcherCallbak;

    .prologue
    .line 47
    invoke-virtual {p0, p1}, Lcom/sec/android/app/ve/thread/ImageThumbFetcher;->checkJobExists(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 49
    invoke-super/range {p0 .. p5}, Lcom/sec/android/app/ve/thread/VideoImageThumbFetcher;->addOperation(Ljava/lang/String;IJLcom/sec/android/app/ve/thread/VideoImageThumbFetcher$VideoImageThumbnaiFetcherCallbak;)V

    .line 50
    sget-object v1, Lcom/sec/android/app/ve/thread/ImageThumbFetcher;->_instance:Lcom/sec/android/app/ve/thread/ImageThumbFetcher;

    monitor-enter v1

    .line 51
    :try_start_0
    sget-object v0, Lcom/sec/android/app/ve/thread/ImageThumbFetcher;->_instance:Lcom/sec/android/app/ve/thread/ImageThumbFetcher;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 50
    monitor-exit v1

    .line 54
    :cond_0
    return-void

    .line 50
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public clearPendingOperations()V
    .locals 2

    .prologue
    .line 59
    invoke-super {p0}, Lcom/sec/android/app/ve/thread/VideoImageThumbFetcher;->clearPendingOperations()V

    .line 60
    sget-object v0, Lcom/sec/android/app/ve/thread/ImageThumbFetcher;->_instance:Lcom/sec/android/app/ve/thread/ImageThumbFetcher;

    if-nez v0, :cond_0

    .line 65
    :goto_0
    return-void

    .line 62
    :cond_0
    sget-object v1, Lcom/sec/android/app/ve/thread/ImageThumbFetcher;->_instance:Lcom/sec/android/app/ve/thread/ImageThumbFetcher;

    monitor-enter v1

    .line 63
    :try_start_0
    sget-object v0, Lcom/sec/android/app/ve/thread/ImageThumbFetcher;->_instance:Lcom/sec/android/app/ve/thread/ImageThumbFetcher;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 62
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public run()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x0

    .line 68
    :cond_0
    :goto_0
    iget-boolean v3, p0, Lcom/sec/android/app/ve/thread/ImageThumbFetcher;->mTerminate:Z

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/sec/android/app/ve/thread/ImageThumbFetcher;->mJobList:Ljava/util/Vector;

    if-nez v3, :cond_4

    .line 105
    :cond_1
    return-void

    .line 71
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/ve/thread/ImageThumbFetcher;->mJobList:Ljava/util/Vector;

    invoke-virtual {v3, v8}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/ve/thread/VideoImageThumbFetcher$FetcherInfo;

    .line 72
    .local v1, "info":Lcom/sec/android/app/ve/thread/VideoImageThumbFetcher$FetcherInfo;
    iget-object v3, v1, Lcom/sec/android/app/ve/thread/VideoImageThumbFetcher$FetcherInfo;->callback:Lcom/sec/android/app/ve/thread/VideoImageThumbFetcher$VideoImageThumbnaiFetcherCallbak;

    iget v4, v1, Lcom/sec/android/app/ve/thread/VideoImageThumbFetcher$FetcherInfo;->position:I

    iget-object v5, v1, Lcom/sec/android/app/ve/thread/VideoImageThumbFetcher$FetcherInfo;->filePath:Ljava/lang/String;

    invoke-interface {v3, v4, v5, v9}, Lcom/sec/android/app/ve/thread/VideoImageThumbFetcher$VideoImageThumbnaiFetcherCallbak;->isTargetViewExist(ILjava/lang/String;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 75
    :try_start_0
    iget-object v3, v1, Lcom/sec/android/app/ve/thread/VideoImageThumbFetcher$FetcherInfo;->filePath:Ljava/lang/String;

    .line 76
    const/4 v4, 0x1

    .line 77
    sget v5, Lcom/sec/android/app/ve/util/CommonUtils;->ADDMEDIA_THUMBNAIL_WIDTH:I

    int-to-float v5, v5

    .line 78
    sget v6, Lcom/sec/android/app/ve/util/CommonUtils;->ADDMEDIA_THUMBNAIL_HEIGHT:I

    int-to-float v6, v6

    .line 75
    invoke-static {v3, v4, v5, v6}, Lcom/sec/android/app/ve/common/MediaUtils;->getImageBitmap(Ljava/lang/String;IFF)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 80
    .local v2, "lBitmap":Landroid/graphics/Bitmap;
    iget-object v3, v1, Lcom/sec/android/app/ve/thread/VideoImageThumbFetcher$FetcherInfo;->callback:Lcom/sec/android/app/ve/thread/VideoImageThumbFetcher$VideoImageThumbnaiFetcherCallbak;

    iget v4, v1, Lcom/sec/android/app/ve/thread/VideoImageThumbFetcher$FetcherInfo;->position:I

    iget-object v5, v1, Lcom/sec/android/app/ve/thread/VideoImageThumbFetcher$FetcherInfo;->filePath:Ljava/lang/String;

    const/4 v6, 0x0

    invoke-interface {v3, v4, v2, v5, v6}, Lcom/sec/android/app/ve/thread/VideoImageThumbFetcher$VideoImageThumbnaiFetcherCallbak;->bitmapCreated(ILandroid/graphics/Bitmap;Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 87
    .end local v2    # "lBitmap":Landroid/graphics/Bitmap;
    :cond_3
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/ve/thread/ImageThumbFetcher;->mJobList:Ljava/util/Vector;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/sec/android/app/ve/thread/ImageThumbFetcher;->mJobList:Ljava/util/Vector;

    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v3

    if-lez v3, :cond_4

    .line 88
    iget-object v3, p0, Lcom/sec/android/app/ve/thread/ImageThumbFetcher;->mJobList:Ljava/util/Vector;

    invoke-virtual {v3, v8}, Ljava/util/Vector;->remove(I)Ljava/lang/Object;

    .line 69
    .end local v1    # "info":Lcom/sec/android/app/ve/thread/VideoImageThumbFetcher$FetcherInfo;
    :cond_4
    iget-object v3, p0, Lcom/sec/android/app/ve/thread/ImageThumbFetcher;->mJobList:Ljava/util/Vector;

    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v3

    if-gtz v3, :cond_2

    .line 91
    sget-object v3, Lcom/sec/android/app/ve/thread/ImageThumbFetcher;->_instance:Lcom/sec/android/app/ve/thread/ImageThumbFetcher;

    if-eqz v3, :cond_0

    .line 93
    sget-object v4, Lcom/sec/android/app/ve/thread/ImageThumbFetcher;->_instance:Lcom/sec/android/app/ve/thread/ImageThumbFetcher;

    monitor-enter v4

    .line 95
    :try_start_1
    iget-object v3, p0, Lcom/sec/android/app/ve/thread/ImageThumbFetcher;->mJobList:Ljava/util/Vector;

    invoke-virtual {v3}, Ljava/util/Vector;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 96
    sget-object v3, Lcom/sec/android/app/ve/thread/ImageThumbFetcher;->_instance:Lcom/sec/android/app/ve/thread/ImageThumbFetcher;

    const-wide/16 v6, 0x7d0

    invoke-virtual {v3, v6, v7}, Ljava/lang/Object;->wait(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 93
    :cond_5
    :goto_2
    :try_start_2
    monitor-exit v4

    goto :goto_0

    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v3

    .line 81
    .restart local v1    # "info":Lcom/sec/android/app/ve/thread/VideoImageThumbFetcher$FetcherInfo;
    :catch_0
    move-exception v0

    .line 82
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 98
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "info":Lcom/sec/android/app/ve/thread/VideoImageThumbFetcher$FetcherInfo;
    :catch_1
    move-exception v0

    .line 99
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2
.end method

.method public terminate()V
    .locals 2

    .prologue
    .line 35
    invoke-super {p0}, Lcom/sec/android/app/ve/thread/VideoImageThumbFetcher;->terminate()V

    .line 36
    sget-object v1, Lcom/sec/android/app/ve/thread/ImageThumbFetcher;->_instance:Lcom/sec/android/app/ve/thread/ImageThumbFetcher;

    monitor-enter v1

    .line 37
    :try_start_0
    sget-object v0, Lcom/sec/android/app/ve/thread/ImageThumbFetcher;->_instance:Lcom/sec/android/app/ve/thread/ImageThumbFetcher;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 36
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 40
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/ve/thread/ImageThumbFetcher;->_instance:Lcom/sec/android/app/ve/thread/ImageThumbFetcher;

    .line 41
    return-void

    .line 36
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
