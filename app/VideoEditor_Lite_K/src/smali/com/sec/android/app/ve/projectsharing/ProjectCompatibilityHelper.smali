.class public Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper;
.super Ljava/lang/Object;
.source "ProjectCompatibilityHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper$AudioListType;,
        Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper$ProjectShareInfoDialog;
    }
.end annotation


# static fields
.field public static final EXTRA_PROJECT_SHARE_NOTIFICATION:Ljava/lang/String; = "EXTRA_PROJECT_SHARE_NOTIFICATION"

.field public static final EXTRA_SHOW_NEARBY_DEVICES:Ljava/lang/String; = "EXTRA_SHOW_NEARBY_DEVICES"

.field public static final EXTRA_TO_SHARE_TE:Ljava/lang/String; = "EXTRA_TO_SHARE_TE"

.field private static final JSON_KEY_FILES_LIST:Ljava/lang/String; = "JSON_KEY_FILES_LIST"

.field private static final JSON_KEY_FILE_EXISTS:Ljava/lang/String; = "JSON_KEY_FILE_EXISTS"

.field private static final JSON_KEY_FILE_NAME:Ljava/lang/String; = "JSON_KEY_FILE_NAME"

.field private static final JSON_KEY_FILE_SIZE:Ljava/lang/String; = "JSON_KEY_FILE_SIZE"

.field private static final JSON_KEY_PROJECT_NAME:Ljava/lang/String; = "JSON_KEY_PROJECT_NAME"

.field private static final JSON_KEY_UNIQUE_PROJECT_ID:Ljava/lang/String; = "JSON_KEY_UNIQUE_PROJECT_ID"

.field public static final PATH_FOR_SHARED_PRJ_CONTENTS:Ljava/lang/String;

.field public static final SHARED_PROJ_FORMAT:Ljava/lang/String; = ".vejson"

.field public static final TAG:Ljava/lang/String; = "ProjectSharing"

.field public static final TEMP_FILES_LIST_FORMAT:Ljava/lang/String; = ".flist"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 71
    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getFullySpecifiedExternalFilesPath()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper;->PATH_FOR_SHARED_PRJ_CONTENTS:Ljava/lang/String;

    .line 85
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Landroid/content/Context;Lcom/samsung/app/video/editor/external/TranscodeElement;)V
    .locals 0

    .prologue
    .line 851
    invoke-static {p0, p1}, Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper;->launchProjectSharingHandler(Landroid/content/Context;Lcom/samsung/app/video/editor/external/TranscodeElement;)V

    return-void
.end method

.method private static addAudioToProjectRoot(Ljava/util/List;Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper$AudioListType;)V
    .locals 12
    .param p1, "projectRoot"    # Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;
    .param p2, "audioType"    # Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper$AudioListType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/Element;",
            ">;",
            "Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;",
            "Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper$AudioListType;",
            ")V"
        }
    .end annotation

    .prologue
    .line 88
    .local p0, "audioList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    if-eqz p0, :cond_0

    .line 89
    sget-object v8, Lcom/sec/android/app/ve/VEApp;->gProjectPluginManager:Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework;

    .line 90
    .local v8, "projectPlugin":Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework;
    const/4 v7, 0x0

    .local v7, "k":I
    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v10

    if-lt v7, v10, :cond_1

    .line 132
    .end local v7    # "k":I
    .end local v8    # "projectPlugin":Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework;
    :cond_0
    return-void

    .line 92
    .restart local v7    # "k":I
    .restart local v8    # "projectPlugin":Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework;
    :cond_1
    new-instance v3, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioElement;

    invoke-direct {v3}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioElement;-><init>()V

    .line 93
    .local v3, "audioElemJson":Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioElement;
    invoke-interface {p0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/app/video/editor/external/Element;

    .line 94
    .local v2, "audioEle":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/Element;->getFilePath()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3, v10}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioElement;->setFilePath(Ljava/lang/String;)V

    .line 95
    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/Element;->getDuration()J

    move-result-wide v10

    long-to-int v10, v10

    invoke-virtual {v3, v10}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioElement;->setDuration(I)V

    .line 96
    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/Element;->getStoryBoardEndTime()J

    move-result-wide v10

    long-to-int v10, v10

    invoke-virtual {v3, v10}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioElement;->setStoryboardEndTime(I)V

    .line 97
    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/Element;->getStoryBoardStartTime()J

    move-result-wide v10

    long-to-int v10, v10

    invoke-virtual {v3, v10}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioElement;->setStoryboardStartTime(I)V

    .line 98
    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/Element;->getAudioDisplayName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3, v10}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioElement;->setDisplayName(Ljava/lang/String;)V

    .line 100
    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/Element;->getEditList()Ljava/util/List;

    move-result-object v5

    .line 101
    .local v5, "editList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Edit;>;"
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_1
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v10

    if-lt v6, v10, :cond_3

    .line 124
    sget-object v10, Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper$AudioListType;->BGM:Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper$AudioListType;

    if-ne p2, v10, :cond_7

    .line 125
    invoke-virtual {p1, v3}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;->addBGMAudioElement(Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioElement;)V

    .line 90
    :cond_2
    :goto_2
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 103
    :cond_3
    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/app/video/editor/external/Edit;

    .line 104
    .local v0, "audioEffect":Lcom/samsung/app/video/editor/external/Edit;
    if-eqz v0, :cond_4

    .line 105
    invoke-virtual {v0}, Lcom/samsung/app/video/editor/external/Edit;->getType()I

    move-result v9

    .line 106
    .local v9, "type":I
    const/16 v10, 0xa

    if-ne v9, v10, :cond_5

    .line 107
    new-instance v1, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioEffect;

    invoke-direct {v1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioEffect;-><init>()V

    .line 108
    .local v1, "audioEffectInJson":Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioEffect;
    invoke-virtual {v0}, Lcom/samsung/app/video/editor/external/Edit;->getEffectStartTime()I

    move-result v10

    invoke-virtual {v1, v10}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioEffect;->setEffectStartTime(I)V

    .line 109
    invoke-virtual {v0}, Lcom/samsung/app/video/editor/external/Edit;->getEffectEndTime()I

    move-result v10

    invoke-virtual {v1, v10}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioEffect;->setEffectEndTime(I)V

    .line 110
    iget-object v10, v8, Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework;->mAudioEffectPlugin:Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework$AudioEffectPlugin;

    invoke-virtual {v0}, Lcom/samsung/app/video/editor/external/Edit;->getSubType()I

    move-result v11

    invoke-interface {v10, v11}, Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework$AudioEffectPlugin;->mapEffectToGeneric(I)I

    move-result v10

    invoke-virtual {v1, v10}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioEffect;->setEffectType(I)V

    .line 101
    .end local v1    # "audioEffectInJson":Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioEffect;
    .end local v9    # "type":I
    :cond_4
    :goto_3
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 112
    .restart local v9    # "type":I
    :cond_5
    const/4 v10, 0x7

    if-ne v9, v10, :cond_6

    .line 113
    new-instance v4, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioTransition;

    invoke-direct {v4}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioTransition;-><init>()V

    .line 114
    .local v4, "audioTransJson":Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioTransition;
    invoke-virtual {v0}, Lcom/samsung/app/video/editor/external/Edit;->getTrans_duration()I

    move-result v10

    invoke-virtual {v4, v10}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioTransition;->setDuration(I)V

    .line 115
    iget-object v10, v8, Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework;->mAudioTransitionPlugin:Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework$AudioTransitionPlugin;

    invoke-virtual {v0}, Lcom/samsung/app/video/editor/external/Edit;->getSubType()I

    move-result v11

    invoke-interface {v10, v11}, Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework$AudioTransitionPlugin;->mapAudioTransitionToGeneric(I)I

    move-result v10

    invoke-virtual {v4, v10}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioTransition;->setType(I)V

    .line 116
    invoke-virtual {v3, v4}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioElement;->setAudioTransition(Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioTransition;)V

    goto :goto_3

    .line 118
    .end local v4    # "audioTransJson":Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioTransition;
    :cond_6
    const/4 v10, 0x1

    if-ne v9, v10, :cond_4

    .line 119
    invoke-virtual {v0}, Lcom/samsung/app/video/editor/external/Edit;->getVolumeLevel()I

    move-result v10

    invoke-virtual {v3, v10}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioElement;->setVolumeLevel(I)V

    goto :goto_3

    .line 126
    .end local v0    # "audioEffect":Lcom/samsung/app/video/editor/external/Edit;
    .end local v9    # "type":I
    :cond_7
    sget-object v10, Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper$AudioListType;->SOUND_EFFECTS:Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper$AudioListType;

    if-ne p2, v10, :cond_8

    .line 127
    invoke-virtual {p1, v3}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;->addSoundEffectAudioElement(Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioElement;)V

    goto :goto_2

    .line 128
    :cond_8
    sget-object v10, Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper$AudioListType;->VOICE_OVER:Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper$AudioListType;

    if-ne p2, v10, :cond_2

    .line 129
    invoke-virtual {p1, v3}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;->addVoiceOverAudioElement(Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioElement;)V

    goto :goto_2
.end method

.method private static addAudioToTranscodeElement(Lorg/json/JSONArray;Lcom/samsung/app/video/editor/external/TranscodeElement;Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper$AudioListType;Ljava/util/HashMap;)V
    .locals 20
    .param p0, "audioArray"    # Lorg/json/JSONArray;
    .param p1, "transcodeElement"    # Lcom/samsung/app/video/editor/external/TranscodeElement;
    .param p2, "audioType"    # Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper$AudioListType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONArray;",
            "Lcom/samsung/app/video/editor/external/TranscodeElement;",
            "Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper$AudioListType;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 137
    .local p3, "receivedFilesMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz p0, :cond_0

    .line 139
    :try_start_0
    sget-object v13, Lcom/sec/android/app/ve/VEApp;->gProjectPluginManager:Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework;

    .line 140
    .local v13, "projectPlugin":Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework;
    const/16 v16, 0x0

    .local v16, "y":I
    :goto_0
    invoke-virtual/range {p0 .. p0}, Lorg/json/JSONArray;->length()I

    move-result v17

    move/from16 v0, v16

    move/from16 v1, v17

    if-lt v0, v1, :cond_1

    .line 202
    .end local v13    # "projectPlugin":Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework;
    .end local v16    # "y":I
    :cond_0
    :goto_1
    return-void

    .line 142
    .restart local v13    # "projectPlugin":Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework;
    .restart local v16    # "y":I
    :cond_1
    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioElement;

    .line 143
    .local v5, "bgmJson":Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioElement;
    new-instance v12, Lcom/samsung/app/video/editor/external/Element;

    invoke-direct {v12}, Lcom/samsung/app/video/editor/external/Element;-><init>()V

    .line 144
    .local v12, "lMusicElement":Lcom/samsung/app/video/editor/external/Element;
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v12, v0}, Lcom/samsung/app/video/editor/external/Element;->setType(I)V

    .line 146
    invoke-virtual {v5}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioElement;->getStoryboardStartTime()I

    move-result v17

    move/from16 v0, v17

    int-to-long v0, v0

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    invoke-virtual {v12, v0, v1}, Lcom/samsung/app/video/editor/external/Element;->setStoryBoardStartTime(J)V

    .line 147
    invoke-virtual {v5}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioElement;->getStoryboardEndTime()I

    move-result v17

    move/from16 v0, v17

    int-to-long v0, v0

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    invoke-virtual {v12, v0, v1}, Lcom/samsung/app/video/editor/external/Element;->setStoryBoardEndTime(J)V

    .line 148
    invoke-virtual {v5}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioElement;->getDuration()I

    move-result v4

    .line 149
    .local v4, "bgmDuration":I
    const-wide/16 v18, 0x0

    move-wide/from16 v0, v18

    invoke-virtual {v12, v0, v1}, Lcom/samsung/app/video/editor/external/Element;->setStartTime(J)V

    .line 150
    int-to-long v0, v4

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    invoke-virtual {v12, v0, v1}, Lcom/samsung/app/video/editor/external/Element;->setEndTime(J)V

    .line 151
    int-to-long v0, v4

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    invoke-virtual {v12, v0, v1}, Lcom/samsung/app/video/editor/external/Element;->setDuration(J)V

    .line 152
    invoke-virtual {v5}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioElement;->getDisplayName()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Lcom/samsung/app/video/editor/external/Element;->setAudioDisplayName(Ljava/lang/String;)V

    .line 153
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getThemeName()I

    move-result v14

    .line 154
    .local v14, "theme":I
    if-nez v14, :cond_2

    invoke-virtual {v5}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioElement;->getFilePath()Ljava/lang/String;

    move-result-object v17

    invoke-static {}, Lcom/sec/android/app/ve/theme/ThemeDataManager;->getInstance()Lcom/sec/android/app/ve/theme/ThemeDataManager;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/ve/theme/ThemeDataManager;->getThemeDataDir()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_5

    .line 155
    :cond_2
    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v12, v0}, Lcom/samsung/app/video/editor/external/Element;->setAssetResource(Z)V

    .line 156
    invoke-virtual {v5}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioElement;->getFilePath()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Lcom/samsung/app/video/editor/external/Element;->setFilePath(Ljava/lang/String;)V

    .line 163
    :goto_2
    invoke-virtual {v5}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioElement;->getAudioTransition()Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioTransition;

    move-result-object v3

    .line 164
    .local v3, "audioTransJson":Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioTransition;
    if-eqz v3, :cond_3

    .line 166
    new-instance v7, Lcom/samsung/app/video/editor/external/Edit;

    invoke-direct {v7}, Lcom/samsung/app/video/editor/external/Edit;-><init>()V

    .line 167
    .local v7, "edi":Lcom/samsung/app/video/editor/external/Edit;
    invoke-virtual {v3}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioTransition;->getDuration()I

    move-result v17

    move/from16 v0, v17

    invoke-virtual {v7, v0}, Lcom/samsung/app/video/editor/external/Edit;->setTrans_duration(I)V

    .line 168
    const/16 v17, 0x7

    move/from16 v0, v17

    invoke-virtual {v7, v0}, Lcom/samsung/app/video/editor/external/Edit;->setType(I)V

    .line 169
    iget-object v0, v13, Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework;->mAudioTransitionPlugin:Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework$AudioTransitionPlugin;

    move-object/from16 v17, v0

    invoke-virtual {v3}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioTransition;->getType()I

    move-result v18

    invoke-interface/range {v17 .. v18}, Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework$AudioTransitionPlugin;->mapAudioTransitionFromGeneric(I)I

    move-result v17

    move/from16 v0, v17

    invoke-virtual {v7, v0}, Lcom/samsung/app/video/editor/external/Edit;->setSubType(I)V

    .line 170
    invoke-virtual {v12, v7}, Lcom/samsung/app/video/editor/external/Element;->addEdit(Lcom/samsung/app/video/editor/external/Edit;)V

    .line 173
    .end local v7    # "edi":Lcom/samsung/app/video/editor/external/Edit;
    :cond_3
    invoke-virtual {v5}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioElement;->getAudioEffectList()Lorg/json/JSONArray;

    move-result-object v10

    .line 174
    .local v10, "effectList":Lorg/json/JSONArray;
    if-eqz v10, :cond_4

    .line 175
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_3
    invoke-virtual {v10}, Lorg/json/JSONArray;->length()I

    move-result v17

    move/from16 v0, v17

    if-lt v11, v0, :cond_7

    .line 186
    .end local v11    # "i":I
    :cond_4
    new-instance v15, Lcom/samsung/app/video/editor/external/Edit;

    invoke-direct {v15}, Lcom/samsung/app/video/editor/external/Edit;-><init>()V

    .line 187
    .local v15, "volumeEdit":Lcom/samsung/app/video/editor/external/Edit;
    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v15, v0}, Lcom/samsung/app/video/editor/external/Edit;->setType(I)V

    .line 188
    invoke-virtual {v5}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioElement;->getVolumeLevel()I

    move-result v17

    move/from16 v0, v17

    invoke-virtual {v15, v0}, Lcom/samsung/app/video/editor/external/Edit;->setVolumeLevel(I)V

    .line 189
    invoke-virtual {v12, v15}, Lcom/samsung/app/video/editor/external/Element;->addEdit(Lcom/samsung/app/video/editor/external/Edit;)V

    .line 191
    sget-object v17, Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper$AudioListType;->BGM:Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper$AudioListType;

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    if-ne v0, v1, :cond_8

    .line 192
    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Lcom/samsung/app/video/editor/external/TranscodeElement;->addAdditionlAudioEleList(Lcom/samsung/app/video/editor/external/Element;)V

    .line 140
    :goto_4
    add-int/lit8 v16, v16, 0x1

    goto/16 :goto_0

    .line 159
    .end local v3    # "audioTransJson":Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioTransition;
    .end local v10    # "effectList":Lorg/json/JSONArray;
    .end local v15    # "volumeEdit":Lcom/samsung/app/video/editor/external/Edit;
    :cond_5
    if-eqz p3, :cond_6

    invoke-virtual {v5}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioElement;->getFilePath()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p3

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/String;

    move-object/from16 v2, v17

    .line 160
    .local v2, "audioPath":Ljava/lang/String;
    :goto_5
    invoke-virtual {v12, v2}, Lcom/samsung/app/video/editor/external/Element;->setFilePath(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 198
    .end local v2    # "audioPath":Ljava/lang/String;
    .end local v4    # "bgmDuration":I
    .end local v5    # "bgmJson":Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioElement;
    .end local v12    # "lMusicElement":Lcom/samsung/app/video/editor/external/Element;
    .end local v13    # "projectPlugin":Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework;
    .end local v14    # "theme":I
    .end local v16    # "y":I
    :catch_0
    move-exception v6

    .line 199
    .local v6, "e":Lorg/json/JSONException;
    invoke-virtual {v6}, Lorg/json/JSONException;->printStackTrace()V

    goto/16 :goto_1

    .line 159
    .end local v6    # "e":Lorg/json/JSONException;
    .restart local v4    # "bgmDuration":I
    .restart local v5    # "bgmJson":Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioElement;
    .restart local v12    # "lMusicElement":Lcom/samsung/app/video/editor/external/Element;
    .restart local v13    # "projectPlugin":Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework;
    .restart local v14    # "theme":I
    .restart local v16    # "y":I
    :cond_6
    :try_start_1
    invoke-virtual {v5}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioElement;->getFilePath()Ljava/lang/String;

    move-result-object v2

    goto :goto_5

    .line 176
    .restart local v3    # "audioTransJson":Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioTransition;
    .restart local v10    # "effectList":Lorg/json/JSONArray;
    .restart local v11    # "i":I
    :cond_7
    invoke-virtual {v10, v11}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v9

    check-cast v9, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioEffect;

    .line 177
    .local v9, "effectJson":Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioEffect;
    new-instance v8, Lcom/samsung/app/video/editor/external/Edit;

    invoke-direct {v8}, Lcom/samsung/app/video/editor/external/Edit;-><init>()V

    .line 178
    .local v8, "edit":Lcom/samsung/app/video/editor/external/Edit;
    const/16 v17, 0xa

    move/from16 v0, v17

    invoke-virtual {v8, v0}, Lcom/samsung/app/video/editor/external/Edit;->setType(I)V

    .line 179
    iget-object v0, v13, Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework;->mAudioEffectPlugin:Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework$AudioEffectPlugin;

    move-object/from16 v17, v0

    invoke-virtual {v9}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioEffect;->getEffectType()I

    move-result v18

    invoke-interface/range {v17 .. v18}, Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework$AudioEffectPlugin;->mapEffectFromGeneric(I)I

    move-result v17

    move/from16 v0, v17

    invoke-virtual {v8, v0}, Lcom/samsung/app/video/editor/external/Edit;->setSubType(I)V

    .line 180
    invoke-virtual {v9}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioEffect;->getEffectStartTime()I

    move-result v17

    move/from16 v0, v17

    invoke-virtual {v8, v0}, Lcom/samsung/app/video/editor/external/Edit;->setEffectStartTime(I)V

    .line 181
    invoke-virtual {v9}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioEffect;->getEffectEndTime()I

    move-result v17

    move/from16 v0, v17

    invoke-virtual {v8, v0}, Lcom/samsung/app/video/editor/external/Edit;->setEffectEndTime(I)V

    .line 182
    invoke-virtual {v12, v8}, Lcom/samsung/app/video/editor/external/Element;->addEdit(Lcom/samsung/app/video/editor/external/Edit;)V

    .line 175
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_3

    .line 193
    .end local v8    # "edit":Lcom/samsung/app/video/editor/external/Edit;
    .end local v9    # "effectJson":Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioEffect;
    .end local v11    # "i":I
    .restart local v15    # "volumeEdit":Lcom/samsung/app/video/editor/external/Edit;
    :cond_8
    sget-object v17, Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper$AudioListType;->SOUND_EFFECTS:Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper$AudioListType;

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    if-ne v0, v1, :cond_9

    .line 194
    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Lcom/samsung/app/video/editor/external/TranscodeElement;->addSoundEleList(Lcom/samsung/app/video/editor/external/Element;)V

    goto :goto_4

    .line 196
    :cond_9
    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Lcom/samsung/app/video/editor/external/TranscodeElement;->addRecordEleList(Lcom/samsung/app/video/editor/external/Element;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_4
.end method

.method public static createProjectJSONFile(Lcom/samsung/app/video/editor/external/TranscodeElement;)Ljava/lang/String;
    .locals 49
    .param p0, "transcodeElement"    # Lcom/samsung/app/video/editor/external/TranscodeElement;

    .prologue
    .line 221
    const/4 v2, 0x0

    .line 223
    .local v2, "absPathToJSON":Ljava/lang/String;
    if-eqz p0, :cond_2

    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v4

    if-lez v4, :cond_2

    .line 225
    sget-object v42, Lcom/sec/android/app/ve/VEApp;->gProjectPluginManager:Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework;

    .line 227
    .local v42, "projectPlugin":Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework;
    new-instance v44, Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;

    invoke-direct/range {v44 .. v44}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;-><init>()V

    .line 228
    .local v44, "rootJson":Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getFullMovieDuration()I

    move-result v4

    move-object/from16 v0, v44

    invoke-virtual {v0, v4}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;->setProjectDuration(I)V

    .line 229
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getProjectName()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v44

    invoke-virtual {v0, v4}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;->setProjectName(Ljava/lang/String;)V

    .line 230
    move-object/from16 v0, v42

    iget-object v4, v0, Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework;->mThemesPlugin:Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework$ThemesPlugin;

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getThemeName()I

    move-result v5

    invoke-interface {v4, v5}, Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework$ThemesPlugin;->mapThemeToGeneric(I)I

    move-result v4

    move-object/from16 v0, v44

    invoke-virtual {v0, v4}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;->setTheme(I)V

    .line 232
    const/16 v37, 0x0

    .local v37, "k":I
    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v4

    move/from16 v0, v37

    if-lt v0, v4, :cond_3

    .line 317
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getTextEleList()Ljava/util/List;

    move-result-object v23

    .line 318
    .local v23, "clipartList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    if-eqz v23, :cond_0

    .line 319
    const/16 v36, 0x0

    .local v36, "j":I
    :goto_1
    invoke-interface/range {v23 .. v23}, Ljava/util/List;->size()I

    move-result v4

    move/from16 v0, v36

    if-lt v0, v4, :cond_d

    .line 364
    .end local v36    # "j":I
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getAdditionlAudioEleList()Ljava/util/List;

    move-result-object v18

    .line 365
    .local v18, "audioEleList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    sget-object v4, Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper$AudioListType;->BGM:Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper$AudioListType;

    move-object/from16 v0, v18

    move-object/from16 v1, v44

    invoke-static {v0, v1, v4}, Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper;->addAudioToProjectRoot(Ljava/util/List;Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper$AudioListType;)V

    .line 367
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getAdditionlRecordEleList()Ljava/util/List;

    move-result-object v43

    .line 368
    .local v43, "recordEleList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    sget-object v4, Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper$AudioListType;->VOICE_OVER:Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper$AudioListType;

    move-object/from16 v0, v43

    move-object/from16 v1, v44

    invoke-static {v0, v1, v4}, Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper;->addAudioToProjectRoot(Ljava/util/List;Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper$AudioListType;)V

    .line 370
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getAdditionlSoundEleList()Ljava/util/List;

    move-result-object v45

    .line 371
    .local v45, "soundEleList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    sget-object v4, Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper$AudioListType;->SOUND_EFFECTS:Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper$AudioListType;

    move-object/from16 v0, v45

    move-object/from16 v1, v44

    invoke-static {v0, v1, v4}, Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper;->addAudioToProjectRoot(Ljava/util/List;Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper$AudioListType;)V

    .line 373
    new-instance v33, Ljava/io/File;

    sget-object v4, Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper;->PATH_FOR_SHARED_PRJ_CONTENTS:Ljava/lang/String;

    move-object/from16 v0, v33

    invoke-direct {v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 374
    .local v33, "f":Ljava/io/File;
    invoke-virtual/range {v33 .. v33}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_1

    .line 375
    invoke-virtual/range {v33 .. v33}, Ljava/io/File;->mkdir()Z

    .line 377
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    sget-object v5, Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper;->PATH_FOR_SHARED_PRJ_CONTENTS:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getProjectName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".vejson"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 378
    move-object/from16 v0, v44

    invoke-static {v0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectHelper;->saveProject(Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;Ljava/lang/String;)V

    .line 384
    .end local v18    # "audioEleList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    .end local v23    # "clipartList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    .end local v33    # "f":Ljava/io/File;
    .end local v37    # "k":I
    .end local v42    # "projectPlugin":Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework;
    .end local v43    # "recordEleList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    .end local v44    # "rootJson":Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;
    .end local v45    # "soundEleList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    :cond_2
    :goto_2
    return-object v2

    .line 234
    .restart local v37    # "k":I
    .restart local v42    # "projectPlugin":Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework;
    .restart local v44    # "rootJson":Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;
    :cond_3
    new-instance v39, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;

    invoke-direct/range {v39 .. v39}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;-><init>()V

    .line 236
    .local v39, "mediaElementJSON":Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;
    move-object/from16 v0, p0

    move/from16 v1, v37

    invoke-virtual {v0, v1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElement(I)Lcom/samsung/app/video/editor/external/Element;

    move-result-object v30

    .line 237
    .local v30, "elem":Lcom/samsung/app/video/editor/external/Element;
    move-object/from16 v0, v30

    iget-object v4, v0, Lcom/samsung/app/video/editor/external/Element;->filePath:Ljava/lang/String;

    move-object/from16 v0, v39

    invoke-virtual {v0, v4}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;->setFilePath(Ljava/lang/String;)V

    .line 238
    move-object/from16 v0, v42

    iget-object v4, v0, Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework;->mMediaElementPlugin:Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework$MediaElementPlugin;

    move-object/from16 v0, v30

    iget v5, v0, Lcom/samsung/app/video/editor/external/Element;->type:I

    invoke-interface {v4, v5}, Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework$MediaElementPlugin;->mapElementTypeToGeneric(I)I

    move-result v4

    move-object/from16 v0, v39

    invoke-virtual {v0, v4}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;->setElementType(I)V

    .line 239
    move-object/from16 v0, v42

    iget-object v4, v0, Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework;->mMediaElementPlugin:Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework$MediaElementPlugin;

    move-object/from16 v0, v30

    iget v5, v0, Lcom/samsung/app/video/editor/external/Element;->subType:I

    invoke-interface {v4, v5}, Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework$MediaElementPlugin;->mapElementSubTypeToGeneric(I)I

    move-result v4

    move-object/from16 v0, v39

    invoke-virtual {v0, v4}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;->setElementSubType(I)V

    .line 240
    invoke-virtual/range {v30 .. v30}, Lcom/samsung/app/video/editor/external/Element;->getStartTime()J

    move-result-wide v4

    long-to-int v4, v4

    move-object/from16 v0, v39

    invoke-virtual {v0, v4}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;->setElementStartTime(I)V

    .line 241
    invoke-virtual/range {v30 .. v30}, Lcom/samsung/app/video/editor/external/Element;->getEndTime()J

    move-result-wide v4

    long-to-int v4, v4

    move-object/from16 v0, v39

    invoke-virtual {v0, v4}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;->setElementEndTime(I)V

    .line 242
    invoke-virtual/range {v30 .. v30}, Lcom/samsung/app/video/editor/external/Element;->getDuration()J

    move-result-wide v4

    long-to-int v4, v4

    move-object/from16 v0, v39

    invoke-virtual {v0, v4}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;->setElementDuration(I)V

    .line 243
    invoke-virtual/range {v30 .. v30}, Lcom/samsung/app/video/editor/external/Element;->getSplitTime()J

    move-result-wide v4

    long-to-int v4, v4

    move-object/from16 v0, v39

    invoke-virtual {v0, v4}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;->setSplitPosTime(I)V

    .line 244
    invoke-virtual/range {v30 .. v30}, Lcom/samsung/app/video/editor/external/Element;->getGroupID()I

    move-result v4

    move-object/from16 v0, v39

    invoke-virtual {v0, v4}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;->setGroupID(I)V

    .line 245
    invoke-virtual/range {v30 .. v30}, Lcom/samsung/app/video/editor/external/Element;->getOrientation()I

    move-result v4

    move-object/from16 v0, v39

    invoke-virtual {v0, v4}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;->setRotationOrientation(I)V

    .line 246
    invoke-virtual/range {v30 .. v30}, Lcom/samsung/app/video/editor/external/Element;->getTransitionEdit()Lcom/samsung/app/video/editor/external/Edit;

    move-result-object v26

    .line 247
    .local v26, "edit":Lcom/samsung/app/video/editor/external/Edit;
    if-eqz v26, :cond_4

    .line 249
    new-instance v40, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaTransition;

    invoke-direct/range {v40 .. v40}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaTransition;-><init>()V

    .line 250
    .local v40, "mediaTransitionJson":Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaTransition;
    move-object/from16 v0, v42

    iget-object v4, v0, Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework;->mMediaTransitionPlugin:Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework$MediaTransitionPlugin;

    invoke-virtual/range {v26 .. v26}, Lcom/samsung/app/video/editor/external/Edit;->getSubType()I

    move-result v5

    invoke-interface {v4, v5}, Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework$MediaTransitionPlugin;->mapTransitionToGeneric(I)I

    move-result v4

    move-object/from16 v0, v40

    invoke-virtual {v0, v4}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaTransition;->setType(I)V

    .line 251
    invoke-virtual/range {v26 .. v26}, Lcom/samsung/app/video/editor/external/Edit;->getTrans_duration()I

    move-result v4

    move-object/from16 v0, v40

    invoke-virtual {v0, v4}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaTransition;->setDuration(I)V

    .line 252
    invoke-virtual/range {v26 .. v26}, Lcom/samsung/app/video/editor/external/Edit;->getEffectResourceFile()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v40

    invoke-virtual {v0, v4}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaTransition;->setEffectResourcePath(Ljava/lang/String;)V

    .line 253
    invoke-virtual/range {v39 .. v40}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;->setMediaTransition(Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaTransition;)V

    .line 256
    .end local v40    # "mediaTransitionJson":Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaTransition;
    :cond_4
    invoke-virtual/range {v30 .. v30}, Lcom/samsung/app/video/editor/external/Element;->getEditList()Ljava/util/List;

    move-result-object v27

    .line 257
    .local v27, "editList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Edit;>;"
    if-eqz v27, :cond_5

    .line 258
    const/16 v35, 0x0

    .local v35, "i":I
    :goto_3
    invoke-interface/range {v27 .. v27}, Ljava/util/List;->size()I

    move-result v4

    move/from16 v0, v35

    if-lt v0, v4, :cond_7

    .line 301
    .end local v35    # "i":I
    :cond_5
    invoke-virtual/range {v30 .. v30}, Lcom/samsung/app/video/editor/external/Element;->getBitmapAnimationsDataList()Ljava/util/List;

    move-result-object v21

    .line 302
    .local v21, "bitmapAnimationList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/BitmapAnimationData;>;"
    if-eqz v21, :cond_6

    .line 303
    const/16 v41, 0x0

    .local v41, "p":I
    :goto_4
    invoke-interface/range {v21 .. v21}, Ljava/util/List;->size()I

    move-result v4

    move/from16 v0, v41

    if-lt v0, v4, :cond_c

    .line 314
    .end local v41    # "p":I
    :cond_6
    move-object/from16 v0, v44

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;->addMediaElement(Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;)V

    .line 232
    add-int/lit8 v37, v37, 0x1

    goto/16 :goto_0

    .line 260
    .end local v21    # "bitmapAnimationList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/BitmapAnimationData;>;"
    .restart local v35    # "i":I
    :cond_7
    new-instance v38, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaEffect;

    invoke-direct/range {v38 .. v38}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaEffect;-><init>()V

    .line 261
    .local v38, "mediaEffectJSON":Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaEffect;
    move-object/from16 v0, v27

    move/from16 v1, v35

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Lcom/samsung/app/video/editor/external/Edit;

    .line 262
    .local v25, "edi":Lcom/samsung/app/video/editor/external/Edit;
    invoke-virtual/range {v25 .. v25}, Lcom/samsung/app/video/editor/external/Edit;->getType()I

    move-result v29

    .line 263
    .local v29, "effectType":I
    invoke-virtual/range {v25 .. v25}, Lcom/samsung/app/video/editor/external/Edit;->getSubType()I

    move-result v28

    .line 265
    .local v28, "effectSubType":I
    const/4 v4, 0x4

    move/from16 v0, v29

    if-ne v0, v4, :cond_b

    .line 267
    move-object/from16 v0, v42

    iget-object v4, v0, Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework;->mMediaEffectsPlugin:Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework$MediaEffectsPlugin;

    move/from16 v0, v28

    invoke-interface {v4, v0}, Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework$MediaEffectsPlugin;->mapEffectTypeToGeneric(I)I

    move-result v4

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaEffect;->setEffectType(I)V

    .line 268
    invoke-virtual/range {v25 .. v25}, Lcom/samsung/app/video/editor/external/Edit;->getEffectEndTime()I

    move-result v4

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaEffect;->setEffectEndTime(I)V

    .line 269
    invoke-virtual/range {v25 .. v25}, Lcom/samsung/app/video/editor/external/Edit;->getEffectStartTime()I

    move-result v4

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaEffect;->setEffectStartTime(I)V

    .line 270
    invoke-virtual/range {v25 .. v25}, Lcom/samsung/app/video/editor/external/Edit;->getEffectResourceFile()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaEffect;->setEffectResourcePath(Ljava/lang/String;)V

    .line 272
    const/16 v4, 0x27

    move/from16 v0, v28

    if-ne v0, v4, :cond_9

    .line 274
    const/4 v4, 0x1

    move-object/from16 v0, v30

    invoke-virtual {v0, v4}, Lcom/samsung/app/video/editor/external/Element;->getMatrix(Z)Landroid/graphics/Matrix;

    move-result-object v46

    .line 275
    .local v46, "startM":Landroid/graphics/Matrix;
    if-eqz v46, :cond_8

    .line 277
    const/16 v4, 0x9

    new-array v0, v4, [F

    move-object/from16 v47, v0

    .line 278
    .local v47, "startValues":[F
    invoke-virtual/range {v46 .. v47}, Landroid/graphics/Matrix;->getValues([F)V

    .line 279
    const/4 v4, 0x0

    aget v4, v47, v4

    float-to-double v4, v4

    const/4 v6, 0x4

    aget v6, v47, v6

    float-to-double v6, v6

    move-object/from16 v0, v38

    invoke-virtual {v0, v4, v5, v6, v7}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaEffect;->setScaleStartValues(DD)V

    .line 280
    const/4 v4, 0x2

    aget v4, v47, v4

    float-to-double v4, v4

    const/4 v6, 0x5

    aget v6, v47, v6

    float-to-double v6, v6

    move-object/from16 v0, v38

    invoke-virtual {v0, v4, v5, v6, v7}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaEffect;->setTranslateStartValues(DD)V

    .line 282
    .end local v47    # "startValues":[F
    :cond_8
    const/4 v4, 0x0

    move-object/from16 v0, v30

    invoke-virtual {v0, v4}, Lcom/samsung/app/video/editor/external/Element;->getMatrix(Z)Landroid/graphics/Matrix;

    move-result-object v31

    .line 283
    .local v31, "endM":Landroid/graphics/Matrix;
    if-eqz v31, :cond_9

    .line 285
    const/16 v4, 0x9

    new-array v0, v4, [F

    move-object/from16 v32, v0

    .line 286
    .local v32, "endValues":[F
    invoke-virtual/range {v31 .. v32}, Landroid/graphics/Matrix;->getValues([F)V

    .line 287
    const/4 v4, 0x0

    aget v4, v32, v4

    float-to-double v4, v4

    const/4 v6, 0x4

    aget v6, v32, v6

    float-to-double v6, v6

    move-object/from16 v0, v38

    invoke-virtual {v0, v4, v5, v6, v7}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaEffect;->setScaleEndValues(DD)V

    .line 288
    const/4 v4, 0x2

    aget v4, v32, v4

    float-to-double v4, v4

    const/4 v6, 0x5

    aget v6, v32, v6

    float-to-double v6, v6

    move-object/from16 v0, v38

    invoke-virtual {v0, v4, v5, v6, v7}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaEffect;->setTranslateEndValues(DD)V

    .line 289
    const-string v4, "ProjectSharing"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "end rectangle "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v30 .. v30}, Lcom/samsung/app/video/editor/external/Element;->getEndRect()Landroid/graphics/RectF;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 290
    const-string v4, "ProjectSharing"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "end matrix "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v31

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 293
    .end local v31    # "endM":Landroid/graphics/Matrix;
    .end local v32    # "endValues":[F
    .end local v46    # "startM":Landroid/graphics/Matrix;
    :cond_9
    move-object/from16 v0, v39

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;->addMediaEffect(Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaEffect;)V

    .line 258
    :cond_a
    :goto_5
    add-int/lit8 v35, v35, 0x1

    goto/16 :goto_3

    .line 295
    :cond_b
    const/4 v4, 0x1

    move/from16 v0, v29

    if-ne v0, v4, :cond_a

    .line 296
    invoke-virtual/range {v25 .. v25}, Lcom/samsung/app/video/editor/external/Edit;->getVolumeLevel()I

    move-result v4

    move-object/from16 v0, v39

    invoke-virtual {v0, v4}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;->setVolumeLevel(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_5

    .line 381
    .end local v25    # "edi":Lcom/samsung/app/video/editor/external/Edit;
    .end local v26    # "edit":Lcom/samsung/app/video/editor/external/Edit;
    .end local v27    # "editList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Edit;>;"
    .end local v28    # "effectSubType":I
    .end local v29    # "effectType":I
    .end local v30    # "elem":Lcom/samsung/app/video/editor/external/Element;
    .end local v35    # "i":I
    .end local v37    # "k":I
    .end local v38    # "mediaEffectJSON":Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaEffect;
    .end local v39    # "mediaElementJSON":Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;
    .end local v42    # "projectPlugin":Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework;
    .end local v44    # "rootJson":Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;
    :catch_0
    move-exception v24

    .line 382
    .local v24, "e":Ljava/lang/Exception;
    invoke-virtual/range {v24 .. v24}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_2

    .line 304
    .end local v24    # "e":Ljava/lang/Exception;
    .restart local v21    # "bitmapAnimationList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/BitmapAnimationData;>;"
    .restart local v26    # "edit":Lcom/samsung/app/video/editor/external/Edit;
    .restart local v27    # "editList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Edit;>;"
    .restart local v30    # "elem":Lcom/samsung/app/video/editor/external/Element;
    .restart local v37    # "k":I
    .restart local v39    # "mediaElementJSON":Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;
    .restart local v41    # "p":I
    .restart local v42    # "projectPlugin":Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework;
    .restart local v44    # "rootJson":Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;
    :cond_c
    :try_start_1
    new-instance v20, Lcom/sec/android/app/ve/projectcompatibility/VEJSONBitmapBlendAnimation;

    invoke-direct/range {v20 .. v20}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONBitmapBlendAnimation;-><init>()V

    .line 305
    .local v20, "bitmapAnimationJson":Lcom/sec/android/app/ve/projectcompatibility/VEJSONBitmapBlendAnimation;
    move-object/from16 v0, v21

    move/from16 v1, v41

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/samsung/app/video/editor/external/BitmapAnimationData;

    .line 306
    .local v19, "bitmapAnimation":Lcom/samsung/app/video/editor/external/BitmapAnimationData;
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/app/video/editor/external/BitmapAnimationData;->getStartFrameWithinElement()I

    move-result v4

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/app/video/editor/external/BitmapAnimationData;->getOriginalStartFrameWithinElement()I

    move-result v5

    move-object/from16 v0, v20

    invoke-virtual {v0, v4, v5}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONBitmapBlendAnimation;->setStartFrameWithinElement(II)V

    .line 307
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/app/video/editor/external/BitmapAnimationData;->getEndFrameWithinElement()I

    move-result v4

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/app/video/editor/external/BitmapAnimationData;->getOriginalEndFrameWithinElement()I

    move-result v5

    move-object/from16 v0, v20

    invoke-virtual {v0, v4, v5}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONBitmapBlendAnimation;->setEndFrameWithinElement(II)V

    .line 308
    move-object/from16 v0, v19

    iget-object v4, v0, Lcom/samsung/app/video/editor/external/BitmapAnimationData;->pngTemplateFileName:Ljava/lang/String;

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONBitmapBlendAnimation;->setFilenameTemplateForBlendingBitmap(Ljava/lang/String;)V

    .line 309
    move-object/from16 v0, v19

    iget v4, v0, Lcom/samsung/app/video/editor/external/BitmapAnimationData;->startIndexInTemplate:I

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONBitmapBlendAnimation;->setStartIndexInTemplate(I)V

    .line 310
    move-object/from16 v0, v19

    iget v4, v0, Lcom/samsung/app/video/editor/external/BitmapAnimationData;->endIndexInTemplate:I

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONBitmapBlendAnimation;->setEndIndexInTemplate(I)V

    .line 311
    move-object/from16 v0, v39

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;->addBitmapBlendAnimation(Lcom/sec/android/app/ve/projectcompatibility/VEJSONBitmapBlendAnimation;)V

    .line 303
    add-int/lit8 v41, v41, 0x1

    goto/16 :goto_4

    .line 321
    .end local v19    # "bitmapAnimation":Lcom/samsung/app/video/editor/external/BitmapAnimationData;
    .end local v20    # "bitmapAnimationJson":Lcom/sec/android/app/ve/projectcompatibility/VEJSONBitmapBlendAnimation;
    .end local v21    # "bitmapAnimationList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/BitmapAnimationData;>;"
    .end local v26    # "edit":Lcom/samsung/app/video/editor/external/Edit;
    .end local v27    # "editList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Edit;>;"
    .end local v30    # "elem":Lcom/samsung/app/video/editor/external/Element;
    .end local v39    # "mediaElementJSON":Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;
    .end local v41    # "p":I
    .restart local v23    # "clipartList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    .restart local v36    # "j":I
    :cond_d
    new-instance v3, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;

    invoke-direct {v3}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;-><init>()V

    .line 322
    .local v3, "captionElementJson":Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;
    move-object/from16 v0, v23

    move/from16 v1, v36

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lcom/samsung/app/video/editor/external/ClipartParams;

    .line 323
    .local v22, "clipParam":Lcom/samsung/app/video/editor/external/ClipartParams;
    invoke-virtual/range {v22 .. v22}, Lcom/samsung/app/video/editor/external/ClipartParams;->getFilePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->setCaptionRawFilePath(Ljava/lang/String;)V

    .line 324
    move-object/from16 v0, v22

    iget-object v4, v0, Lcom/samsung/app/video/editor/external/ClipartParams;->data:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->setCaptionText(Ljava/lang/String;)V

    .line 325
    move-object/from16 v0, v22

    iget v4, v0, Lcom/samsung/app/video/editor/external/ClipartParams;->clipart_x_pos:I

    int-to-double v4, v4

    move-object/from16 v0, v22

    iget v6, v0, Lcom/samsung/app/video/editor/external/ClipartParams;->clipart_y_pos:I

    int-to-double v6, v6

    move-object/from16 v0, v22

    iget v8, v0, Lcom/samsung/app/video/editor/external/ClipartParams;->clipart_x_pos:I

    move-object/from16 v0, v22

    iget v9, v0, Lcom/samsung/app/video/editor/external/ClipartParams;->clipart_width:I

    add-int/2addr v8, v9

    int-to-double v8, v8

    move-object/from16 v0, v22

    iget v10, v0, Lcom/samsung/app/video/editor/external/ClipartParams;->clipart_y_pos:I

    move-object/from16 v0, v22

    iget v11, v0, Lcom/samsung/app/video/editor/external/ClipartParams;->clipart_height:I

    add-int/2addr v10, v11

    int-to-double v10, v10

    const-wide/high16 v12, 0x4094000000000000L    # 1280.0

    const-wide v14, 0x4086800000000000L    # 720.0

    invoke-virtual/range {v3 .. v15}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->setCaptionRect(DDDDDD)V

    .line 326
    invoke-virtual/range {v22 .. v22}, Lcom/samsung/app/video/editor/external/ClipartParams;->isEditable()Z

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->setEditable(Z)V

    .line 327
    invoke-virtual/range {v22 .. v22}, Lcom/samsung/app/video/editor/external/ClipartParams;->getStoryboardStartFrame()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->setStoryboardStartFrame(I)V

    .line 328
    invoke-virtual/range {v22 .. v22}, Lcom/samsung/app/video/editor/external/ClipartParams;->getStoryboardEndFrame()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->setStoryboardEndFrame(I)V

    .line 329
    invoke-virtual/range {v22 .. v22}, Lcom/samsung/app/video/editor/external/ClipartParams;->getElementID()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->setElementID(I)V

    .line 330
    move-object/from16 v0, v42

    iget-object v4, v0, Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework;->mCaptionPlugin:Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework$CaptionPlugin;

    invoke-virtual/range {v22 .. v22}, Lcom/samsung/app/video/editor/external/ClipartParams;->getCaptionTextAlignment()I

    move-result v5

    invoke-interface {v4, v5}, Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework$CaptionPlugin;->mapCaptionAlignmentToGeneric(I)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->setCaptionAlignment(I)V

    .line 331
    move-object/from16 v0, v42

    iget-object v4, v0, Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework;->mCaptionPlugin:Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework$CaptionPlugin;

    move-object/from16 v0, v22

    iget v5, v0, Lcom/samsung/app/video/editor/external/ClipartParams;->themeId:I

    invoke-interface {v4, v5}, Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework$CaptionPlugin;->mapCaptionThemeToGeneric(I)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->setCaptionTheme(I)V

    .line 332
    invoke-virtual/range {v22 .. v22}, Lcom/samsung/app/video/editor/external/ClipartParams;->getFontParams()Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;

    move-result-object v34

    .line 333
    .local v34, "fontParams":Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;
    if-eqz v34, :cond_e

    .line 334
    move-object/from16 v0, v34

    iget-object v4, v0, Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;->mFontAssetFilePath:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->setFontFilePath(Ljava/lang/String;)V

    .line 335
    move-object/from16 v0, v34

    iget v4, v0, Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;->mFontSize:I

    invoke-virtual {v3, v4}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->setFontSize(I)V

    .line 336
    move-object/from16 v0, v34

    iget v4, v0, Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;->mFontColor:I

    invoke-virtual {v3, v4}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->setFontColor(I)V

    .line 337
    move-object/from16 v0, v34

    iget v4, v0, Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;->mShadowX:I

    move-object/from16 v0, v34

    iget v5, v0, Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;->mShadowY:I

    move-object/from16 v0, v34

    iget v6, v0, Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;->mShadowR:I

    move-object/from16 v0, v34

    iget v7, v0, Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;->mShadowColor:I

    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->setFontShadow(IIII)V

    .line 338
    move-object/from16 v0, v42

    iget-object v4, v0, Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework;->mCaptionPlugin:Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework$CaptionPlugin;

    move-object/from16 v0, v34

    iget v5, v0, Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;->mAlignFlag:I

    invoke-interface {v4, v5}, Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework$CaptionPlugin;->mapThemeCaptionAlignmentToGeneric(I)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->setCaptionAlignment(I)V

    .line 340
    :cond_e
    move-object/from16 v0, v22

    iget v4, v0, Lcom/samsung/app/video/editor/external/ClipartParams;->mDefaultTranslateX:F

    move-object/from16 v0, v22

    iget v5, v0, Lcom/samsung/app/video/editor/external/ClipartParams;->mDefaultTranslateY:F

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->setDefaultTranslations(FF)V

    .line 341
    move-object/from16 v0, v22

    iget v4, v0, Lcom/samsung/app/video/editor/external/ClipartParams;->mDefaultAlpha:F

    invoke-virtual {v3, v4}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->setDefaultAlpha(F)V

    .line 342
    move-object/from16 v0, v22

    iget v4, v0, Lcom/samsung/app/video/editor/external/ClipartParams;->mDefaultScale:F

    move-object/from16 v0, v22

    iget v5, v0, Lcom/samsung/app/video/editor/external/ClipartParams;->mDefaultScale:F

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->setDefaultScale(FF)V

    .line 343
    move-object/from16 v0, v22

    iget v4, v0, Lcom/samsung/app/video/editor/external/ClipartParams;->mDefaultRotate:F

    invoke-virtual {v3, v4}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->setDefaultRotation(F)V

    .line 344
    move-object/from16 v0, v22

    iget v4, v0, Lcom/samsung/app/video/editor/external/ClipartParams;->mPivotX:F

    move-object/from16 v0, v22

    iget v5, v0, Lcom/samsung/app/video/editor/external/ClipartParams;->mPivotY:F

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->setPivotPoints(FF)V

    .line 346
    move-object/from16 v0, v22

    iget-object v4, v0, Lcom/samsung/app/video/editor/external/ClipartParams;->mAnimationList:Ljava/util/List;

    if-eqz v4, :cond_f

    .line 347
    const/16 v48, 0x0

    .local v48, "x":I
    :goto_6
    move-object/from16 v0, v22

    iget-object v4, v0, Lcom/samsung/app/video/editor/external/ClipartParams;->mAnimationList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    move/from16 v0, v48

    if-lt v0, v4, :cond_10

    .line 360
    .end local v48    # "x":I
    :cond_f
    move-object/from16 v0, v44

    invoke-virtual {v0, v3}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;->addCaption(Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;)V

    .line 319
    add-int/lit8 v36, v36, 0x1

    goto/16 :goto_1

    .line 348
    .restart local v48    # "x":I
    :cond_10
    move-object/from16 v0, v22

    iget-object v4, v0, Lcom/samsung/app/video/editor/external/ClipartParams;->mAnimationList:Ljava/util/List;

    move/from16 v0, v48

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/samsung/app/video/editor/external/TextAnimationData;

    .line 349
    .local v16, "animation":Lcom/samsung/app/video/editor/external/TextAnimationData;
    new-instance v17, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAnimation;

    invoke-direct/range {v17 .. v17}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAnimation;-><init>()V

    .line 350
    .local v17, "animationJson":Lcom/sec/android/app/ve/projectcompatibility/VEJSONAnimation;
    move-object/from16 v0, v42

    iget-object v4, v0, Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework;->mAnimationPlugin:Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework$AnimationPlugin;

    move-object/from16 v0, v16

    iget v5, v0, Lcom/samsung/app/video/editor/external/TextAnimationData;->mAnimationType:I

    invoke-interface {v4, v5}, Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework$AnimationPlugin;->mapAnimationTypeToGeneric(I)I

    move-result v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAnimation;->setAnimationType(I)V

    .line 351
    move-object/from16 v0, v16

    iget v4, v0, Lcom/samsung/app/video/editor/external/TextAnimationData;->mStartFrame:I

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAnimation;->setAnimationStartFrame(I)V

    .line 352
    move-object/from16 v0, v16

    iget v4, v0, Lcom/samsung/app/video/editor/external/TextAnimationData;->mEndFrame:I

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAnimation;->setAnimationEndFrame(I)V

    .line 353
    move-object/from16 v0, v16

    iget v4, v0, Lcom/samsung/app/video/editor/external/TextAnimationData;->mStartTranslateX:F

    move-object/from16 v0, v16

    iget v5, v0, Lcom/samsung/app/video/editor/external/TextAnimationData;->mEndTranslateX:F

    move-object/from16 v0, v16

    iget v6, v0, Lcom/samsung/app/video/editor/external/TextAnimationData;->mStartTranslateY:F

    move-object/from16 v0, v16

    iget v7, v0, Lcom/samsung/app/video/editor/external/TextAnimationData;->mEndTranslateY:F

    move-object/from16 v0, v17

    invoke-virtual {v0, v4, v5, v6, v7}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAnimation;->setTranslations(FFFF)V

    .line 354
    move-object/from16 v0, v16

    iget v4, v0, Lcom/samsung/app/video/editor/external/TextAnimationData;->mStartScaleX:F

    move-object/from16 v0, v16

    iget v5, v0, Lcom/samsung/app/video/editor/external/TextAnimationData;->mEndScaleX:F

    move-object/from16 v0, v16

    iget v6, v0, Lcom/samsung/app/video/editor/external/TextAnimationData;->mStartScaleY:F

    move-object/from16 v0, v16

    iget v7, v0, Lcom/samsung/app/video/editor/external/TextAnimationData;->mEndScaleY:F

    move-object/from16 v0, v17

    invoke-virtual {v0, v4, v5, v6, v7}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAnimation;->setScale(FFFF)V

    .line 355
    move-object/from16 v0, v16

    iget v4, v0, Lcom/samsung/app/video/editor/external/TextAnimationData;->mStartAlpha:F

    move-object/from16 v0, v16

    iget v5, v0, Lcom/samsung/app/video/editor/external/TextAnimationData;->mEndAlpha:F

    move-object/from16 v0, v17

    invoke-virtual {v0, v4, v5}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAnimation;->setAlpha(FF)V

    .line 356
    move-object/from16 v0, v16

    iget v4, v0, Lcom/samsung/app/video/editor/external/TextAnimationData;->mStartRotate:F

    move-object/from16 v0, v16

    iget v5, v0, Lcom/samsung/app/video/editor/external/TextAnimationData;->mEndRotate:F

    move-object/from16 v0, v17

    invoke-virtual {v0, v4, v5}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAnimation;->setRotation(FF)V

    .line 357
    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->addAnimation(Lcom/sec/android/app/ve/projectcompatibility/VEJSONAnimation;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 347
    add-int/lit8 v48, v48, 0x1

    goto/16 :goto_6
.end method

.method public static createTempFilesList(Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 5
    .param p0, "jsonFilePath"    # Ljava/lang/String;
    .param p2, "absFilePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 439
    .local p1, "listOfFiles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 440
    .local v3, "tmpFileList":Ljava/io/File;
    const/4 v1, 0x0

    .line 442
    .local v1, "outputStream":Ljava/io/ObjectOutputStream;
    :try_start_0
    invoke-virtual {v3}, Ljava/io/File;->createNewFile()Z

    .line 443
    new-instance v2, Ljava/io/ObjectOutputStream;

    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, p2}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v4}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 444
    .end local v1    # "outputStream":Ljava/io/ObjectOutputStream;
    .local v2, "outputStream":Ljava/io/ObjectOutputStream;
    :try_start_1
    invoke-virtual {v2, p1}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 449
    if-eqz v2, :cond_2

    .line 451
    :try_start_2
    invoke-virtual {v2}, Ljava/io/ObjectOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    move-object v1, v2

    .line 456
    .end local v2    # "outputStream":Ljava/io/ObjectOutputStream;
    .restart local v1    # "outputStream":Ljava/io/ObjectOutputStream;
    :cond_0
    :goto_0
    return-void

    .line 445
    :catch_0
    move-exception v0

    .line 446
    .local v0, "e":Ljava/io/IOException;
    :goto_1
    :try_start_3
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 449
    if-eqz v1, :cond_0

    .line 451
    :try_start_4
    invoke-virtual {v1}, Ljava/io/ObjectOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_0

    .line 452
    :catch_1
    move-exception v0

    .line 453
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 448
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    .line 449
    :goto_2
    if-eqz v1, :cond_1

    .line 451
    :try_start_5
    invoke-virtual {v1}, Ljava/io/ObjectOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    .line 455
    :cond_1
    :goto_3
    throw v4

    .line 452
    :catch_2
    move-exception v0

    .line 453
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 452
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "outputStream":Ljava/io/ObjectOutputStream;
    .restart local v2    # "outputStream":Ljava/io/ObjectOutputStream;
    :catch_3
    move-exception v0

    .line 453
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .end local v0    # "e":Ljava/io/IOException;
    :cond_2
    move-object v1, v2

    .end local v2    # "outputStream":Ljava/io/ObjectOutputStream;
    .restart local v1    # "outputStream":Ljava/io/ObjectOutputStream;
    goto :goto_0

    .line 448
    .end local v1    # "outputStream":Ljava/io/ObjectOutputStream;
    .restart local v2    # "outputStream":Ljava/io/ObjectOutputStream;
    :catchall_1
    move-exception v4

    move-object v1, v2

    .end local v2    # "outputStream":Ljava/io/ObjectOutputStream;
    .restart local v1    # "outputStream":Ljava/io/ObjectOutputStream;
    goto :goto_2

    .line 445
    .end local v1    # "outputStream":Ljava/io/ObjectOutputStream;
    .restart local v2    # "outputStream":Ljava/io/ObjectOutputStream;
    :catch_4
    move-exception v0

    move-object v1, v2

    .end local v2    # "outputStream":Ljava/io/ObjectOutputStream;
    .restart local v1    # "outputStream":Ljava/io/ObjectOutputStream;
    goto :goto_1
.end method

.method public static createTranscodeFromJSON(Ljava/lang/String;Ljava/util/HashMap;)Lcom/samsung/app/video/editor/external/TranscodeElement;
    .locals 60
    .param p0, "jsonFilePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/samsung/app/video/editor/external/TranscodeElement;"
        }
    .end annotation

    .prologue
    .line 552
    .local p1, "receivedPathMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const/16 v54, 0x0

    .line 554
    .local v54, "trans":Lcom/samsung/app/video/editor/external/TranscodeElement;
    :try_start_0
    invoke-static/range {p0 .. p0}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectHelper;->openSavedProject(Ljava/lang/String;)Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;

    move-result-object v50

    .line 555
    .local v50, "rootJson":Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;
    new-instance v55, Lcom/samsung/app/video/editor/external/TranscodeElement;

    invoke-direct/range {v55 .. v55}, Lcom/samsung/app/video/editor/external/TranscodeElement;-><init>()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 556
    .end local v54    # "trans":Lcom/samsung/app/video/editor/external/TranscodeElement;
    .local v55, "trans":Lcom/samsung/app/video/editor/external/TranscodeElement;
    :try_start_1
    sget-object v49, Lcom/sec/android/app/ve/VEApp;->gProjectPluginManager:Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework;

    .line 558
    .local v49, "projectPlugin":Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework;
    const/16 v31, 0x0

    .line 559
    .local v31, "count":I
    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "Imported_"

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v50 .. v50}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;->getProjectName()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v53

    .line 560
    .local v53, "tmpName":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/ve/pm/ProjectManager;->getInstance()Lcom/sec/android/app/ve/pm/ProjectManager;

    move-result-object v13

    invoke-virtual {v13}, Lcom/sec/android/app/ve/pm/ProjectManager;->getProjectList()Ljava/util/List;

    move-result-object v48

    .line 561
    .local v48, "projectList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/TranscodeElement;>;"
    move-object/from16 v0, v48

    move-object/from16 v1, v53

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/util/CommonUtils;->isProjectNameDuplicate(Ljava/util/List;Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_5

    .line 562
    :goto_0
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-static/range {v53 .. v53}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v14, "_"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move/from16 v0, v31

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, v48

    invoke-static {v0, v13}, Lcom/sec/android/app/ve/util/CommonUtils;->isProjectNameDuplicate(Ljava/util/List;Ljava/lang/String;)Z

    move-result v13

    if-nez v13, :cond_4

    .line 565
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-static/range {v53 .. v53}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v14, "_"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move/from16 v0, v31

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, v55

    invoke-virtual {v0, v13}, Lcom/samsung/app/video/editor/external/TranscodeElement;->setProjectName(Ljava/lang/String;)V

    .line 570
    :goto_1
    invoke-virtual/range {v50 .. v50}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;->getProjectDuration()I

    move-result v13

    move-object/from16 v0, v55

    invoke-virtual {v0, v13}, Lcom/samsung/app/video/editor/external/TranscodeElement;->setFullMovieDuration(I)V

    .line 571
    move-object/from16 v0, v49

    iget-object v13, v0, Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework;->mThemesPlugin:Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework$ThemesPlugin;

    invoke-virtual/range {v50 .. v50}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;->getTheme()I

    move-result v14

    invoke-interface {v13, v14}, Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework$ThemesPlugin;->mapThemeFromGeneric(I)I

    move-result v52

    .line 572
    .local v52, "theme":I
    move-object/from16 v0, v55

    move/from16 v1, v52

    invoke-virtual {v0, v1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->setThemeName(I)V

    .line 573
    sget-object v13, Lcom/sec/android/app/ve/VEApp;->gThemeMgr:Lcom/sec/android/app/ve/theme/ThemeManager;

    move/from16 v0, v52

    invoke-virtual {v13, v0}, Lcom/sec/android/app/ve/theme/ThemeManager;->getThemeAssetManager(I)Landroid/content/res/AssetManager;

    move-result-object v22

    .line 574
    .local v22, "assetManager":Landroid/content/res/AssetManager;
    move-object/from16 v0, v55

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->setAssetManager(Landroid/content/res/AssetManager;)V

    .line 576
    invoke-virtual/range {v50 .. v50}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;->getMediaElementList()Lorg/json/JSONArray;

    move-result-object v43

    .line 577
    .local v43, "mediaList":Lorg/json/JSONArray;
    if-eqz v43, :cond_0

    .line 578
    const/16 v41, 0x0

    .local v41, "k":I
    :goto_2
    invoke-virtual/range {v43 .. v43}, Lorg/json/JSONArray;->length()I

    move-result v13

    move/from16 v0, v41

    if-lt v0, v13, :cond_7

    .line 673
    .end local v41    # "k":I
    :cond_0
    invoke-virtual/range {v50 .. v50}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;->getCaptionList()Lorg/json/JSONArray;

    move-result-object v28

    .line 674
    .local v28, "captionList":Lorg/json/JSONArray;
    if-eqz v28, :cond_1

    .line 675
    const/16 v58, 0x0

    .local v58, "y":I
    :goto_3
    invoke-virtual/range {v28 .. v28}, Lorg/json/JSONArray;->length()I

    move-result v13

    move/from16 v0, v58

    if-lt v0, v13, :cond_13

    .line 756
    .end local v58    # "y":I
    :cond_1
    invoke-virtual/range {v50 .. v50}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;->getBGMAudioElementList()Lorg/json/JSONArray;

    move-result-object v23

    .line 757
    .local v23, "bgmList":Lorg/json/JSONArray;
    sget-object v13, Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper$AudioListType;->BGM:Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper$AudioListType;

    move-object/from16 v0, v23

    move-object/from16 v1, v55

    move-object/from16 v2, p1

    invoke-static {v0, v1, v13, v2}, Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper;->addAudioToTranscodeElement(Lorg/json/JSONArray;Lcom/samsung/app/video/editor/external/TranscodeElement;Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper$AudioListType;Ljava/util/HashMap;)V

    .line 759
    invoke-virtual/range {v50 .. v50}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;->getSoundEffectAudioElementList()Lorg/json/JSONArray;

    move-result-object v51

    .line 760
    .local v51, "soundEffectList":Lorg/json/JSONArray;
    sget-object v13, Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper$AudioListType;->SOUND_EFFECTS:Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper$AudioListType;

    move-object/from16 v0, v51

    move-object/from16 v1, v55

    move-object/from16 v2, p1

    invoke-static {v0, v1, v13, v2}, Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper;->addAudioToTranscodeElement(Lorg/json/JSONArray;Lcom/samsung/app/video/editor/external/TranscodeElement;Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper$AudioListType;Ljava/util/HashMap;)V

    .line 762
    invoke-virtual/range {v50 .. v50}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;->getVoiceOverAudioElementList()Lorg/json/JSONArray;

    move-result-object v57

    .line 763
    .local v57, "voiceOverList":Lorg/json/JSONArray;
    sget-object v13, Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper$AudioListType;->VOICE_OVER:Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper$AudioListType;

    move-object/from16 v0, v57

    move-object/from16 v1, v55

    move-object/from16 v2, p1

    invoke-static {v0, v1, v13, v2}, Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper;->addAudioToTranscodeElement(Lorg/json/JSONArray;Lcom/samsung/app/video/editor/external/TranscodeElement;Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper$AudioListType;Ljava/util/HashMap;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 771
    new-instance v39, Ljava/io/File;

    move-object/from16 v0, v39

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 772
    .local v39, "file":Ljava/io/File;
    invoke-virtual/range {v39 .. v39}, Ljava/io/File;->exists()Z

    move-result v13

    if-eqz v13, :cond_2

    .line 773
    invoke-virtual/range {v39 .. v39}, Ljava/io/File;->delete()Z

    :cond_2
    move-object/from16 v54, v55

    .line 776
    .end local v22    # "assetManager":Landroid/content/res/AssetManager;
    .end local v23    # "bgmList":Lorg/json/JSONArray;
    .end local v28    # "captionList":Lorg/json/JSONArray;
    .end local v31    # "count":I
    .end local v43    # "mediaList":Lorg/json/JSONArray;
    .end local v48    # "projectList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/TranscodeElement;>;"
    .end local v49    # "projectPlugin":Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework;
    .end local v50    # "rootJson":Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;
    .end local v51    # "soundEffectList":Lorg/json/JSONArray;
    .end local v52    # "theme":I
    .end local v53    # "tmpName":Ljava/lang/String;
    .end local v55    # "trans":Lcom/samsung/app/video/editor/external/TranscodeElement;
    .end local v57    # "voiceOverList":Lorg/json/JSONArray;
    .restart local v54    # "trans":Lcom/samsung/app/video/editor/external/TranscodeElement;
    :cond_3
    :goto_4
    return-object v54

    .line 563
    .end local v39    # "file":Ljava/io/File;
    .end local v54    # "trans":Lcom/samsung/app/video/editor/external/TranscodeElement;
    .restart local v31    # "count":I
    .restart local v48    # "projectList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/TranscodeElement;>;"
    .restart local v49    # "projectPlugin":Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework;
    .restart local v50    # "rootJson":Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;
    .restart local v53    # "tmpName":Ljava/lang/String;
    .restart local v55    # "trans":Lcom/samsung/app/video/editor/external/TranscodeElement;
    :cond_4
    add-int/lit8 v31, v31, 0x1

    goto/16 :goto_0

    .line 568
    :cond_5
    :try_start_2
    move-object/from16 v0, v55

    move-object/from16 v1, v53

    invoke-virtual {v0, v1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->setProjectName(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_1

    .line 764
    .end local v31    # "count":I
    .end local v48    # "projectList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/TranscodeElement;>;"
    .end local v49    # "projectPlugin":Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework;
    .end local v53    # "tmpName":Ljava/lang/String;
    :catch_0
    move-exception v32

    move-object/from16 v54, v55

    .line 765
    .end local v50    # "rootJson":Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;
    .end local v55    # "trans":Lcom/samsung/app/video/editor/external/TranscodeElement;
    .local v32, "e":Ljava/lang/Exception;
    .restart local v54    # "trans":Lcom/samsung/app/video/editor/external/TranscodeElement;
    :goto_5
    if-eqz v54, :cond_6

    .line 766
    :try_start_3
    invoke-virtual/range {v54 .. v54}, Lcom/samsung/app/video/editor/external/TranscodeElement;->deleteAllClipArts()V

    .line 767
    :cond_6
    const/16 v54, 0x0

    .line 768
    invoke-virtual/range {v32 .. v32}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 771
    new-instance v39, Ljava/io/File;

    move-object/from16 v0, v39

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 772
    .restart local v39    # "file":Ljava/io/File;
    invoke-virtual/range {v39 .. v39}, Ljava/io/File;->exists()Z

    move-result v13

    if-eqz v13, :cond_3

    .line 773
    invoke-virtual/range {v39 .. v39}, Ljava/io/File;->delete()Z

    goto :goto_4

    .line 580
    .end local v32    # "e":Ljava/lang/Exception;
    .end local v39    # "file":Ljava/io/File;
    .end local v54    # "trans":Lcom/samsung/app/video/editor/external/TranscodeElement;
    .restart local v22    # "assetManager":Landroid/content/res/AssetManager;
    .restart local v31    # "count":I
    .restart local v41    # "k":I
    .restart local v43    # "mediaList":Lorg/json/JSONArray;
    .restart local v48    # "projectList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/TranscodeElement;>;"
    .restart local v49    # "projectPlugin":Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework;
    .restart local v50    # "rootJson":Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;
    .restart local v52    # "theme":I
    .restart local v53    # "tmpName":Ljava/lang/String;
    .restart local v55    # "trans":Lcom/samsung/app/video/editor/external/TranscodeElement;
    :cond_7
    :try_start_4
    new-instance v4, Lcom/samsung/app/video/editor/external/Element;

    invoke-direct {v4}, Lcom/samsung/app/video/editor/external/Element;-><init>()V

    .line 581
    .local v4, "element":Lcom/samsung/app/video/editor/external/Element;
    move-object/from16 v0, v43

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v42

    check-cast v42, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;

    .line 582
    .local v42, "mediaElementJson":Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;
    move-object/from16 v0, v49

    iget-object v13, v0, Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework;->mMediaElementPlugin:Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework$MediaElementPlugin;

    invoke-virtual/range {v42 .. v42}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;->getElementSubType()I

    move-result v14

    invoke-interface {v13, v14}, Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework$MediaElementPlugin;->mapElementSubTypeFromGeneric(I)I

    move-result v38

    .line 584
    .local v38, "elementSubType":I
    move-object/from16 v0, v49

    iget-object v13, v0, Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework;->mMediaElementPlugin:Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework$MediaElementPlugin;

    invoke-virtual/range {v42 .. v42}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;->getElementType()I

    move-result v14

    invoke-interface {v13, v14}, Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework$MediaElementPlugin;->mapElementTypeFromGeneric(I)I

    move-result v13

    invoke-virtual {v4, v13}, Lcom/samsung/app/video/editor/external/Element;->setType(I)V

    .line 585
    move/from16 v0, v38

    invoke-virtual {v4, v0}, Lcom/samsung/app/video/editor/external/Element;->setSubType(I)V

    .line 587
    const/4 v13, 0x3

    move/from16 v0, v38

    if-eq v0, v13, :cond_8

    const/4 v13, 0x1

    move/from16 v0, v38

    if-ne v0, v13, :cond_c

    .line 588
    :cond_8
    const/4 v13, 0x1

    invoke-virtual {v4, v13}, Lcom/samsung/app/video/editor/external/Element;->setAssetResource(Z)V

    .line 589
    invoke-virtual/range {v42 .. v42}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;->getFilePath()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v4, v13}, Lcom/samsung/app/video/editor/external/Element;->setFilePath(Ljava/lang/String;)V

    .line 596
    :goto_6
    invoke-virtual/range {v42 .. v42}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;->getElementStartTime()I

    move-result v13

    int-to-long v14, v13

    invoke-virtual {v4, v14, v15}, Lcom/samsung/app/video/editor/external/Element;->setStartTime(J)V

    .line 597
    invoke-virtual/range {v42 .. v42}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;->getElementEndTime()I

    move-result v13

    int-to-long v14, v13

    invoke-virtual {v4, v14, v15}, Lcom/samsung/app/video/editor/external/Element;->setEndTime(J)V

    .line 598
    invoke-virtual/range {v42 .. v42}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;->getElementDuration()I

    move-result v13

    int-to-long v14, v13

    invoke-virtual {v4, v14, v15}, Lcom/samsung/app/video/editor/external/Element;->setDuration(J)V

    .line 599
    invoke-virtual/range {v42 .. v42}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;->getSplitPosTime()I

    move-result v13

    int-to-long v14, v13

    invoke-virtual {v4, v14, v15}, Lcom/samsung/app/video/editor/external/Element;->setSplitTime(J)V

    .line 600
    invoke-virtual/range {v42 .. v42}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;->getMediaTransition()Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaTransition;

    move-result-object v56

    .line 601
    .local v56, "transitionJson":Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaTransition;
    if-eqz v56, :cond_9

    invoke-virtual/range {v56 .. v56}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaTransition;->getType()I

    move-result v13

    if-eqz v13, :cond_9

    .line 603
    new-instance v33, Lcom/samsung/app/video/editor/external/Edit;

    invoke-direct/range {v33 .. v33}, Lcom/samsung/app/video/editor/external/Edit;-><init>()V

    .line 604
    .local v33, "edit":Lcom/samsung/app/video/editor/external/Edit;
    const/4 v13, 0x6

    move-object/from16 v0, v33

    invoke-virtual {v0, v13}, Lcom/samsung/app/video/editor/external/Edit;->setType(I)V

    .line 605
    move-object/from16 v0, v49

    iget-object v13, v0, Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework;->mMediaTransitionPlugin:Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework$MediaTransitionPlugin;

    invoke-virtual/range {v56 .. v56}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaTransition;->getType()I

    move-result v14

    invoke-interface {v13, v14}, Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework$MediaTransitionPlugin;->mapTransitionFromGeneric(I)I

    move-result v13

    move-object/from16 v0, v33

    invoke-virtual {v0, v13}, Lcom/samsung/app/video/editor/external/Edit;->setSubType(I)V

    .line 606
    invoke-virtual/range {v56 .. v56}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaTransition;->getDuration()I

    move-result v13

    move-object/from16 v0, v33

    invoke-virtual {v0, v13}, Lcom/samsung/app/video/editor/external/Edit;->setTrans_duration(I)V

    .line 607
    invoke-virtual/range {v56 .. v56}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaTransition;->getEffectResourcePath()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, v33

    invoke-virtual {v0, v13}, Lcom/samsung/app/video/editor/external/Edit;->setEffectResourceFile(Ljava/lang/String;)V

    .line 608
    move-object/from16 v0, v33

    invoke-virtual {v4, v0}, Lcom/samsung/app/video/editor/external/Element;->addEdit(Lcom/samsung/app/video/editor/external/Edit;)V

    .line 611
    .end local v33    # "edit":Lcom/samsung/app/video/editor/external/Edit;
    :cond_9
    invoke-virtual/range {v42 .. v42}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;->getElementType()I

    move-result v13

    const/4 v14, 0x1

    if-ne v13, v14, :cond_a

    .line 612
    new-instance v33, Lcom/samsung/app/video/editor/external/Edit;

    invoke-direct/range {v33 .. v33}, Lcom/samsung/app/video/editor/external/Edit;-><init>()V

    .line 613
    .restart local v33    # "edit":Lcom/samsung/app/video/editor/external/Edit;
    const/4 v13, 0x1

    move-object/from16 v0, v33

    invoke-virtual {v0, v13}, Lcom/samsung/app/video/editor/external/Edit;->setType(I)V

    .line 614
    invoke-virtual/range {v42 .. v42}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;->getVolumeLevel()I

    move-result v13

    move-object/from16 v0, v33

    invoke-virtual {v0, v13}, Lcom/samsung/app/video/editor/external/Edit;->setVolumeLevel(I)V

    .line 615
    move-object/from16 v0, v33

    invoke-virtual {v4, v0}, Lcom/samsung/app/video/editor/external/Element;->addEdit(Lcom/samsung/app/video/editor/external/Edit;)V

    .line 619
    .end local v33    # "edit":Lcom/samsung/app/video/editor/external/Edit;
    :cond_a
    invoke-virtual/range {v42 .. v42}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;->getEffectList()Lorg/json/JSONArray;

    move-result-object v35

    .line 620
    .local v35, "effectList":Lorg/json/JSONArray;
    if-eqz v35, :cond_11

    .line 621
    const/16 v40, 0x0

    .local v40, "j":I
    :goto_7
    invoke-virtual/range {v35 .. v35}, Lorg/json/JSONArray;->length()I

    move-result v13

    move/from16 v0, v40

    if-lt v0, v13, :cond_f

    .line 651
    .end local v40    # "j":I
    :goto_8
    invoke-virtual/range {v42 .. v42}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;->getBitmapBlendAnimationList()Lorg/json/JSONArray;

    move-result-object v25

    .line 652
    .local v25, "bitmapAnimationListJSON":Lorg/json/JSONArray;
    if-eqz v25, :cond_b

    .line 653
    const/16 v45, 0x0

    .local v45, "p":I
    :goto_9
    invoke-virtual/range {v25 .. v25}, Lorg/json/JSONArray;->length()I

    move-result v13

    move/from16 v0, v45

    if-lt v0, v13, :cond_12

    .line 667
    .end local v45    # "p":I
    :cond_b
    const/4 v13, 0x0

    move-object/from16 v0, v55

    invoke-virtual {v0, v4, v13}, Lcom/samsung/app/video/editor/external/TranscodeElement;->addElement(Lcom/samsung/app/video/editor/external/Element;Z)V

    .line 668
    invoke-virtual/range {v42 .. v42}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;->getGroupID()I

    move-result v13

    invoke-virtual {v4, v13}, Lcom/samsung/app/video/editor/external/Element;->setGroupID(I)V

    .line 669
    invoke-virtual/range {v42 .. v42}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;->getRotationOrientation()I

    move-result v13

    invoke-virtual {v4, v13}, Lcom/samsung/app/video/editor/external/Element;->setOrientation(I)V

    .line 578
    add-int/lit8 v41, v41, 0x1

    goto/16 :goto_2

    .line 592
    .end local v25    # "bitmapAnimationListJSON":Lorg/json/JSONArray;
    .end local v35    # "effectList":Lorg/json/JSONArray;
    .end local v56    # "transitionJson":Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaTransition;
    :cond_c
    if-eqz p1, :cond_e

    invoke-virtual/range {v42 .. v42}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;->getFilePath()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    move-object/from16 v37, v13

    .line 593
    .local v37, "elementPath":Ljava/lang/String;
    :goto_a
    move-object/from16 v0, v37

    invoke-virtual {v4, v0}, Lcom/samsung/app/video/editor/external/Element;->setFilePath(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_6

    .line 770
    .end local v4    # "element":Lcom/samsung/app/video/editor/external/Element;
    .end local v22    # "assetManager":Landroid/content/res/AssetManager;
    .end local v31    # "count":I
    .end local v37    # "elementPath":Ljava/lang/String;
    .end local v38    # "elementSubType":I
    .end local v41    # "k":I
    .end local v42    # "mediaElementJson":Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;
    .end local v43    # "mediaList":Lorg/json/JSONArray;
    .end local v48    # "projectList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/TranscodeElement;>;"
    .end local v49    # "projectPlugin":Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework;
    .end local v52    # "theme":I
    .end local v53    # "tmpName":Ljava/lang/String;
    :catchall_0
    move-exception v13

    move-object/from16 v54, v55

    .line 771
    .end local v50    # "rootJson":Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;
    .end local v55    # "trans":Lcom/samsung/app/video/editor/external/TranscodeElement;
    .restart local v54    # "trans":Lcom/samsung/app/video/editor/external/TranscodeElement;
    :goto_b
    new-instance v39, Ljava/io/File;

    move-object/from16 v0, v39

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 772
    .restart local v39    # "file":Ljava/io/File;
    invoke-virtual/range {v39 .. v39}, Ljava/io/File;->exists()Z

    move-result v14

    if-eqz v14, :cond_d

    .line 773
    invoke-virtual/range {v39 .. v39}, Ljava/io/File;->delete()Z

    .line 775
    :cond_d
    throw v13

    .line 592
    .end local v39    # "file":Ljava/io/File;
    .end local v54    # "trans":Lcom/samsung/app/video/editor/external/TranscodeElement;
    .restart local v4    # "element":Lcom/samsung/app/video/editor/external/Element;
    .restart local v22    # "assetManager":Landroid/content/res/AssetManager;
    .restart local v31    # "count":I
    .restart local v38    # "elementSubType":I
    .restart local v41    # "k":I
    .restart local v42    # "mediaElementJson":Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;
    .restart local v43    # "mediaList":Lorg/json/JSONArray;
    .restart local v48    # "projectList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/TranscodeElement;>;"
    .restart local v49    # "projectPlugin":Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework;
    .restart local v50    # "rootJson":Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;
    .restart local v52    # "theme":I
    .restart local v53    # "tmpName":Ljava/lang/String;
    .restart local v55    # "trans":Lcom/samsung/app/video/editor/external/TranscodeElement;
    :cond_e
    :try_start_5
    invoke-virtual/range {v42 .. v42}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;->getFilePath()Ljava/lang/String;

    move-result-object v37

    goto :goto_a

    .line 623
    .restart local v35    # "effectList":Lorg/json/JSONArray;
    .restart local v40    # "j":I
    .restart local v56    # "transitionJson":Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaTransition;
    :cond_f
    move-object/from16 v0, v35

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v34

    check-cast v34, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaEffect;

    .line 624
    .local v34, "effectJson":Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaEffect;
    invoke-virtual/range {v34 .. v34}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaEffect;->getEffectType()I

    move-result v36

    .line 625
    .local v36, "effectTypeJson":I
    new-instance v33, Lcom/samsung/app/video/editor/external/Edit;

    invoke-direct/range {v33 .. v33}, Lcom/samsung/app/video/editor/external/Edit;-><init>()V

    .line 626
    .restart local v33    # "edit":Lcom/samsung/app/video/editor/external/Edit;
    const/4 v13, 0x4

    move-object/from16 v0, v33

    invoke-virtual {v0, v13}, Lcom/samsung/app/video/editor/external/Edit;->setType(I)V

    .line 627
    move-object/from16 v0, v49

    iget-object v13, v0, Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework;->mMediaEffectsPlugin:Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework$MediaEffectsPlugin;

    move/from16 v0, v36

    invoke-interface {v13, v0}, Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework$MediaEffectsPlugin;->mapEffectTypeFromGeneric(I)I

    move-result v13

    move-object/from16 v0, v33

    invoke-virtual {v0, v13}, Lcom/samsung/app/video/editor/external/Edit;->setSubType(I)V

    .line 628
    invoke-virtual/range {v34 .. v34}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaEffect;->getEffectStartTime()I

    move-result v13

    move-object/from16 v0, v33

    invoke-virtual {v0, v13}, Lcom/samsung/app/video/editor/external/Edit;->setEffectStartTime(I)V

    .line 629
    invoke-virtual/range {v34 .. v34}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaEffect;->getEffectEndTime()I

    move-result v13

    move-object/from16 v0, v33

    invoke-virtual {v0, v13}, Lcom/samsung/app/video/editor/external/Edit;->setEffectEndTime(I)V

    .line 630
    invoke-virtual/range {v34 .. v34}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaEffect;->getEffectResourcePath()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, v33

    invoke-virtual {v0, v13}, Lcom/samsung/app/video/editor/external/Edit;->setEffectResourceFile(Ljava/lang/String;)V

    .line 631
    const/16 v13, 0x8

    move/from16 v0, v36

    if-ne v0, v13, :cond_10

    .line 633
    invoke-virtual/range {v34 .. v34}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaEffect;->getScaleStartX()D

    move-result-wide v14

    double-to-float v5, v14

    .line 634
    .local v5, "startScale":F
    invoke-virtual/range {v34 .. v34}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaEffect;->getScaleEndX()D

    move-result-wide v14

    double-to-float v6, v14

    .line 635
    .local v6, "endScale":F
    invoke-virtual/range {v34 .. v34}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaEffect;->getTranslateStartX()D

    move-result-wide v14

    double-to-float v7, v14

    .line 636
    .local v7, "startTranslateX":F
    invoke-virtual/range {v34 .. v34}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaEffect;->getTranslateEndX()D

    move-result-wide v14

    double-to-float v8, v14

    .line 637
    .local v8, "endTranslateX":F
    invoke-virtual/range {v34 .. v34}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaEffect;->getTranslateStartY()D

    move-result-wide v14

    double-to-float v9, v14

    .line 638
    .local v9, "startTranslateY":F
    invoke-virtual/range {v34 .. v34}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaEffect;->getTranslateEndY()D

    move-result-wide v14

    double-to-float v10, v14

    .line 639
    .local v10, "endTranslateY":F
    invoke-static/range {v4 .. v10}, Lcom/sec/android/app/ve/util/CommonUtils;->applyKenburnsToElement(Lcom/samsung/app/video/editor/external/Element;FFFFFF)V

    .line 641
    .end local v5    # "startScale":F
    .end local v6    # "endScale":F
    .end local v7    # "startTranslateX":F
    .end local v8    # "endTranslateX":F
    .end local v9    # "startTranslateY":F
    .end local v10    # "endTranslateY":F
    :cond_10
    const/4 v13, 0x0

    move-object/from16 v0, v33

    invoke-virtual {v4, v0, v13}, Lcom/samsung/app/video/editor/external/Element;->addEdit(Lcom/samsung/app/video/editor/external/Edit;Z)V

    .line 621
    add-int/lit8 v40, v40, 0x1

    goto/16 :goto_7

    .line 645
    .end local v33    # "edit":Lcom/samsung/app/video/editor/external/Edit;
    .end local v34    # "effectJson":Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaEffect;
    .end local v36    # "effectTypeJson":I
    .end local v40    # "j":I
    :cond_11
    new-instance v44, Lcom/samsung/app/video/editor/external/Edit;

    invoke-direct/range {v44 .. v44}, Lcom/samsung/app/video/editor/external/Edit;-><init>()V

    .line 646
    .local v44, "none_effect":Lcom/samsung/app/video/editor/external/Edit;
    const/4 v13, 0x4

    move-object/from16 v0, v44

    invoke-virtual {v0, v13}, Lcom/samsung/app/video/editor/external/Edit;->setType(I)V

    .line 647
    const/16 v13, 0x16

    move-object/from16 v0, v44

    invoke-virtual {v0, v13}, Lcom/samsung/app/video/editor/external/Edit;->setSubType(I)V

    .line 648
    move-object/from16 v0, v44

    invoke-virtual {v4, v0}, Lcom/samsung/app/video/editor/external/Element;->addEdit(Lcom/samsung/app/video/editor/external/Edit;)V

    goto/16 :goto_8

    .line 654
    .end local v44    # "none_effect":Lcom/samsung/app/video/editor/external/Edit;
    .restart local v25    # "bitmapAnimationListJSON":Lorg/json/JSONArray;
    .restart local v45    # "p":I
    :cond_12
    move-object/from16 v0, v25

    move/from16 v1, v45

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lcom/sec/android/app/ve/projectcompatibility/VEJSONBitmapBlendAnimation;

    .line 655
    .local v24, "bitmapAnimationJson":Lcom/sec/android/app/ve/projectcompatibility/VEJSONBitmapBlendAnimation;
    new-instance v26, Lcom/samsung/app/video/editor/external/BitmapAnimationData;

    invoke-direct/range {v26 .. v26}, Lcom/samsung/app/video/editor/external/BitmapAnimationData;-><init>()V

    .line 656
    .local v26, "bmpAnimationData":Lcom/samsung/app/video/editor/external/BitmapAnimationData;
    const/4 v13, 0x1

    move-object/from16 v0, v26

    invoke-virtual {v0, v13}, Lcom/samsung/app/video/editor/external/BitmapAnimationData;->setAssetResource(Z)V

    .line 657
    invoke-virtual/range {v24 .. v24}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONBitmapBlendAnimation;->getCurrentStartFrameWithinElement()I

    move-result v13

    move-object/from16 v0, v26

    invoke-virtual {v0, v13}, Lcom/samsung/app/video/editor/external/BitmapAnimationData;->setStartFrameWithinElement(I)V

    .line 658
    invoke-virtual/range {v24 .. v24}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONBitmapBlendAnimation;->getCurrentEndFrameWithinElement()I

    move-result v13

    move-object/from16 v0, v26

    invoke-virtual {v0, v13}, Lcom/samsung/app/video/editor/external/BitmapAnimationData;->setEndFrameWithinElement(I)V

    .line 659
    invoke-virtual/range {v24 .. v24}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONBitmapBlendAnimation;->getOriginalStartFrameWithinElement()I

    move-result v13

    move-object/from16 v0, v26

    invoke-virtual {v0, v13}, Lcom/samsung/app/video/editor/external/BitmapAnimationData;->setOriginalStartFrameWithinElement(I)V

    .line 660
    invoke-virtual/range {v24 .. v24}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONBitmapBlendAnimation;->getOriginalEndFrameWithinElement()I

    move-result v13

    move-object/from16 v0, v26

    invoke-virtual {v0, v13}, Lcom/samsung/app/video/editor/external/BitmapAnimationData;->setOriginalEndFrameWithinElement(I)V

    .line 661
    invoke-virtual/range {v24 .. v24}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONBitmapBlendAnimation;->getStartIndexInTemplate()I

    move-result v13

    move-object/from16 v0, v26

    invoke-virtual {v0, v13}, Lcom/samsung/app/video/editor/external/BitmapAnimationData;->setStartIndexInTemplate(I)V

    .line 662
    invoke-virtual/range {v24 .. v24}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONBitmapBlendAnimation;->getEndIndexInTemplate()I

    move-result v13

    move-object/from16 v0, v26

    invoke-virtual {v0, v13}, Lcom/samsung/app/video/editor/external/BitmapAnimationData;->setEndIndexInTemplate(I)V

    .line 663
    invoke-virtual/range {v24 .. v24}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONBitmapBlendAnimation;->getFilenameTemplateForBlendingBitmap()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, v26

    invoke-virtual {v0, v13}, Lcom/samsung/app/video/editor/external/BitmapAnimationData;->setPNGTemplate(Ljava/lang/String;)V

    .line 664
    move-object/from16 v0, v26

    invoke-virtual {v4, v0}, Lcom/samsung/app/video/editor/external/Element;->addBitmapAnimationsData(Lcom/samsung/app/video/editor/external/BitmapAnimationData;)V

    .line 653
    add-int/lit8 v45, v45, 0x1

    goto/16 :goto_9

    .line 677
    .end local v4    # "element":Lcom/samsung/app/video/editor/external/Element;
    .end local v24    # "bitmapAnimationJson":Lcom/sec/android/app/ve/projectcompatibility/VEJSONBitmapBlendAnimation;
    .end local v25    # "bitmapAnimationListJSON":Lorg/json/JSONArray;
    .end local v26    # "bmpAnimationData":Lcom/samsung/app/video/editor/external/BitmapAnimationData;
    .end local v35    # "effectList":Lorg/json/JSONArray;
    .end local v38    # "elementSubType":I
    .end local v41    # "k":I
    .end local v42    # "mediaElementJson":Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;
    .end local v45    # "p":I
    .end local v56    # "transitionJson":Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaTransition;
    .restart local v28    # "captionList":Lorg/json/JSONArray;
    .restart local v58    # "y":I
    :cond_13
    move-object/from16 v0, v28

    move/from16 v1, v58

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v27

    check-cast v27, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;

    .line 678
    .local v27, "captionJson":Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;
    new-instance v11, Lcom/samsung/app/video/editor/external/ClipartParams;

    invoke-direct {v11}, Lcom/samsung/app/video/editor/external/ClipartParams;-><init>()V

    .line 679
    .local v11, "clipParams":Lcom/samsung/app/video/editor/external/ClipartParams;
    invoke-virtual/range {v27 .. v27}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->getRawFilePath()Ljava/lang/String;

    move-result-object v30

    .line 681
    .local v30, "captionRawFilePath":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/ve/theme/ThemeDataManager;->getInstance()Lcom/sec/android/app/ve/theme/ThemeDataManager;

    move-result-object v13

    invoke-virtual {v13}, Lcom/sec/android/app/ve/theme/ThemeDataManager;->getThemeDataDir()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, v30

    invoke-virtual {v0, v13}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_15

    .line 682
    const/4 v13, 0x1

    invoke-virtual {v11, v13}, Lcom/samsung/app/video/editor/external/ClipartParams;->setAssetResource(Z)V

    .line 683
    const/4 v13, 0x1

    move-object/from16 v0, v30

    invoke-virtual {v11, v0, v13}, Lcom/samsung/app/video/editor/external/ClipartParams;->setRawFilePath(Ljava/lang/String;Z)V

    .line 691
    :goto_c
    invoke-virtual/range {v27 .. v27}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->getCaptionText()Ljava/lang/String;

    move-result-object v13

    iput-object v13, v11, Lcom/samsung/app/video/editor/external/ClipartParams;->data:Ljava/lang/String;

    .line 692
    invoke-virtual/range {v27 .. v27}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->getCaptionRightCoordinate()D

    move-result-wide v14

    invoke-virtual/range {v27 .. v27}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->getCaptionLeftCoordinate()D

    move-result-wide v16

    sub-double v14, v14, v16

    double-to-int v13, v14

    iput v13, v11, Lcom/samsung/app/video/editor/external/ClipartParams;->clipart_width:I

    .line 693
    invoke-virtual/range {v27 .. v27}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->getCaptionBottomCoordinate()D

    move-result-wide v14

    invoke-virtual/range {v27 .. v27}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->getCaptionTopCoordinate()D

    move-result-wide v16

    sub-double v14, v14, v16

    double-to-int v13, v14

    iput v13, v11, Lcom/samsung/app/video/editor/external/ClipartParams;->clipart_height:I

    .line 694
    invoke-virtual/range {v27 .. v27}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->getCaptionLeftCoordinate()D

    move-result-wide v14

    double-to-int v13, v14

    iput v13, v11, Lcom/samsung/app/video/editor/external/ClipartParams;->clipart_x_pos:I

    .line 695
    invoke-virtual/range {v27 .. v27}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->getCaptionTopCoordinate()D

    move-result-wide v14

    double-to-int v13, v14

    iput v13, v11, Lcom/samsung/app/video/editor/external/ClipartParams;->clipart_y_pos:I

    .line 696
    invoke-virtual/range {v27 .. v27}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->getStoryboardStartFrame()I

    move-result v13

    invoke-virtual {v11, v13}, Lcom/samsung/app/video/editor/external/ClipartParams;->setStoryboardStartFrame(I)V

    .line 697
    invoke-virtual/range {v27 .. v27}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->getStoryboardEndFrame()I

    move-result v13

    invoke-virtual {v11, v13}, Lcom/samsung/app/video/editor/external/ClipartParams;->setStoryboardEndFrame(I)V

    .line 698
    invoke-virtual/range {v27 .. v27}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->isEditable()Z

    move-result v13

    invoke-virtual {v11, v13}, Lcom/samsung/app/video/editor/external/ClipartParams;->setEditable(Z)V

    .line 699
    invoke-virtual/range {v27 .. v27}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->getElementID()I

    move-result v13

    invoke-virtual {v11, v13}, Lcom/samsung/app/video/editor/external/ClipartParams;->setElementID(I)V

    .line 700
    move-object/from16 v0, v49

    iget-object v13, v0, Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework;->mCaptionPlugin:Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework$CaptionPlugin;

    invoke-virtual/range {v27 .. v27}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->getCaptionAlignment()I

    move-result v14

    invoke-interface {v13, v14}, Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework$CaptionPlugin;->mapCaptionAlignmentFromGeneric(I)I

    move-result v13

    iput v13, v11, Lcom/samsung/app/video/editor/external/ClipartParams;->mCaptionTextAlignment:I

    .line 701
    move-object/from16 v0, v49

    iget-object v13, v0, Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework;->mCaptionPlugin:Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework$CaptionPlugin;

    invoke-virtual/range {v27 .. v27}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->getCaptionTheme()I

    move-result v14

    invoke-interface {v13, v14}, Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework$CaptionPlugin;->mapCaptionThemeFromGeneric(I)I

    move-result v13

    iput v13, v11, Lcom/samsung/app/video/editor/external/ClipartParams;->themeId:I

    .line 703
    if-eqz v52, :cond_14

    .line 704
    invoke-virtual/range {v27 .. v27}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->getPivotX()F

    move-result v46

    .line 705
    .local v46, "pivotX":F
    invoke-virtual/range {v27 .. v27}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->getPivotY()F

    move-result v47

    .line 707
    .local v47, "pivotY":F
    invoke-virtual/range {v27 .. v27}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->getFontFilePath()Ljava/lang/String;

    move-result-object v12

    .line 708
    move-object/from16 v0, v49

    iget-object v13, v0, Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework;->mCaptionPlugin:Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework$CaptionPlugin;

    iget v14, v11, Lcom/samsung/app/video/editor/external/ClipartParams;->mCaptionTextAlignment:I

    invoke-interface {v13, v14}, Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework$CaptionPlugin;->mapThemeCaptionAlignmentFromGeneric(I)I

    move-result v13

    .line 709
    invoke-virtual/range {v27 .. v27}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->getFontSize()I

    move-result v14

    .line 710
    invoke-virtual/range {v27 .. v27}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->getFontColor()I

    move-result v15

    .line 711
    invoke-virtual/range {v27 .. v27}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->getShadowColor()I

    move-result v16

    .line 712
    invoke-virtual/range {v27 .. v27}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->getShadowX()I

    move-result v17

    .line 713
    invoke-virtual/range {v27 .. v27}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->getShadowY()I

    move-result v18

    .line 714
    invoke-virtual/range {v27 .. v27}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->getShadowRadius()I

    move-result v19

    .line 707
    invoke-virtual/range {v11 .. v19}, Lcom/samsung/app/video/editor/external/ClipartParams;->setFontParams(Ljava/lang/String;IIIIIII)V

    .line 715
    const/4 v13, 0x1

    invoke-virtual {v11, v13}, Lcom/samsung/app/video/editor/external/ClipartParams;->setThemeDefaultText(Z)V

    .line 716
    invoke-virtual/range {v27 .. v27}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->getDefaultAlpha()F

    move-result v12

    .line 717
    invoke-virtual/range {v27 .. v27}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->getDefaultScaleX()F

    move-result v13

    .line 718
    invoke-virtual/range {v27 .. v27}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->getDefaultTranslationX()F

    move-result v14

    .line 719
    invoke-virtual/range {v27 .. v27}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->getDefaultTranslationY()F

    move-result v15

    .line 720
    invoke-virtual/range {v27 .. v27}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->getDefaultRotation()F

    move-result v16

    move/from16 v17, v46

    move/from16 v18, v47

    .line 716
    invoke-virtual/range {v11 .. v18}, Lcom/samsung/app/video/editor/external/ClipartParams;->setDefaultTransformations(FFFFFFF)V

    .line 723
    invoke-virtual/range {v27 .. v27}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;->getAnimationList()Lorg/json/JSONArray;

    move-result-object v21

    .line 724
    .local v21, "animationlist":Lorg/json/JSONArray;
    if-eqz v21, :cond_14

    invoke-virtual/range {v21 .. v21}, Lorg/json/JSONArray;->length()I

    move-result v13

    if-lez v13, :cond_14

    .line 725
    const/16 v59, 0x0

    .local v59, "z":I
    :goto_d
    invoke-virtual/range {v21 .. v21}, Lorg/json/JSONArray;->length()I

    move-result v13

    move/from16 v0, v59

    if-lt v0, v13, :cond_17

    .line 752
    .end local v21    # "animationlist":Lorg/json/JSONArray;
    .end local v46    # "pivotX":F
    .end local v47    # "pivotY":F
    .end local v59    # "z":I
    :cond_14
    move-object/from16 v0, v55

    invoke-virtual {v0, v11}, Lcom/samsung/app/video/editor/external/TranscodeElement;->addTextEleList(Lcom/samsung/app/video/editor/external/ClipartParams;)V

    .line 675
    add-int/lit8 v58, v58, 0x1

    goto/16 :goto_3

    .line 686
    :cond_15
    const/4 v13, 0x0

    invoke-virtual {v11, v13}, Lcom/samsung/app/video/editor/external/ClipartParams;->setAssetResource(Z)V

    .line 687
    if-eqz p1, :cond_16

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    move-object/from16 v29, v13

    .line 688
    .local v29, "captionPath":Ljava/lang/String;
    :goto_e
    const/4 v13, 0x0

    move-object/from16 v0, v29

    invoke-virtual {v11, v0, v13}, Lcom/samsung/app/video/editor/external/ClipartParams;->setRawFilePath(Ljava/lang/String;Z)V

    goto/16 :goto_c

    .end local v29    # "captionPath":Ljava/lang/String;
    :cond_16
    move-object/from16 v29, v30

    .line 687
    goto :goto_e

    .line 726
    .restart local v21    # "animationlist":Lorg/json/JSONArray;
    .restart local v46    # "pivotX":F
    .restart local v47    # "pivotY":F
    .restart local v59    # "z":I
    :cond_17
    move-object/from16 v0, v21

    move/from16 v1, v59

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAnimation;

    .line 727
    .local v20, "animationJson":Lcom/sec/android/app/ve/projectcompatibility/VEJSONAnimation;
    new-instance v12, Lcom/samsung/app/video/editor/external/TextAnimationData;

    invoke-direct {v12}, Lcom/samsung/app/video/editor/external/TextAnimationData;-><init>()V

    .line 728
    .local v12, "textAnimation":Lcom/samsung/app/video/editor/external/TextAnimationData;
    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAnimation;->getAnimationStartFrame()I

    move-result v13

    .line 729
    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAnimation;->getAnimationEndFrame()I

    move-result v14

    .line 730
    move-object/from16 v0, v49

    iget-object v15, v0, Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework;->mAnimationPlugin:Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework$AnimationPlugin;

    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAnimation;->getAnimationType()I

    move-result v16

    invoke-interface/range {v15 .. v16}, Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework$AnimationPlugin;->mapAnimationTypeFromGeneric(I)I

    move-result v15

    .line 731
    const/16 v16, 0x0

    .line 728
    invoke-virtual/range {v12 .. v16}, Lcom/samsung/app/video/editor/external/TextAnimationData;->setAnimationType(IIII)V

    .line 732
    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAnimation;->getStartAlpha()F

    move-result v13

    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAnimation;->getEndAlpha()F

    move-result v14

    invoke-virtual {v12, v13, v14}, Lcom/samsung/app/video/editor/external/TextAnimationData;->setAlphaRange(FF)V

    .line 733
    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAnimation;->getRotationStart()F

    move-result v13

    .line 734
    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAnimation;->getRotationEnd()F

    move-result v14

    .line 733
    move/from16 v0, v46

    move/from16 v1, v47

    invoke-virtual {v12, v13, v14, v0, v1}, Lcom/samsung/app/video/editor/external/TextAnimationData;->setRotationRange(FFFF)V

    .line 737
    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAnimation;->getTranslateStartX()F

    move-result v13

    .line 738
    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAnimation;->getTranslateEndX()F

    move-result v14

    .line 739
    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAnimation;->getTranslateStartY()F

    move-result v15

    .line 740
    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAnimation;->getTranslateEndY()F

    move-result v16

    .line 737
    invoke-virtual/range {v12 .. v16}, Lcom/samsung/app/video/editor/external/TextAnimationData;->setTranslateRange(FFFF)V

    .line 741
    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAnimation;->getScaleStartX()F

    move-result v13

    .line 742
    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAnimation;->getScaleEndX()F

    move-result v14

    .line 743
    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAnimation;->getScaleStartY()F

    move-result v15

    .line 744
    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAnimation;->getScaleEndY()F

    move-result v16

    move/from16 v17, v46

    move/from16 v18, v47

    .line 741
    invoke-virtual/range {v12 .. v18}, Lcom/samsung/app/video/editor/external/TextAnimationData;->setScaleRange(FFFFFF)V

    .line 747
    invoke-virtual {v11, v12}, Lcom/samsung/app/video/editor/external/ClipartParams;->addAnimationToList(Lcom/samsung/app/video/editor/external/TextAnimationData;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 725
    add-int/lit8 v59, v59, 0x1

    goto/16 :goto_d

    .line 770
    .end local v11    # "clipParams":Lcom/samsung/app/video/editor/external/ClipartParams;
    .end local v12    # "textAnimation":Lcom/samsung/app/video/editor/external/TextAnimationData;
    .end local v20    # "animationJson":Lcom/sec/android/app/ve/projectcompatibility/VEJSONAnimation;
    .end local v21    # "animationlist":Lorg/json/JSONArray;
    .end local v22    # "assetManager":Landroid/content/res/AssetManager;
    .end local v27    # "captionJson":Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;
    .end local v28    # "captionList":Lorg/json/JSONArray;
    .end local v30    # "captionRawFilePath":Ljava/lang/String;
    .end local v31    # "count":I
    .end local v43    # "mediaList":Lorg/json/JSONArray;
    .end local v46    # "pivotX":F
    .end local v47    # "pivotY":F
    .end local v48    # "projectList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/TranscodeElement;>;"
    .end local v49    # "projectPlugin":Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework;
    .end local v50    # "rootJson":Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;
    .end local v52    # "theme":I
    .end local v53    # "tmpName":Ljava/lang/String;
    .end local v55    # "trans":Lcom/samsung/app/video/editor/external/TranscodeElement;
    .end local v58    # "y":I
    .end local v59    # "z":I
    .restart local v54    # "trans":Lcom/samsung/app/video/editor/external/TranscodeElement;
    :catchall_1
    move-exception v13

    goto/16 :goto_b

    .line 764
    :catch_1
    move-exception v32

    goto/16 :goto_5
.end method

.method public static deleteMediaFromProjects(Ljava/util/List;Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/TranscodeElement;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/TranscodeElement;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 884
    .local p0, "permanentlyDeltedProjects":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/TranscodeElement;>;"
    .local p1, "savedProjects":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/TranscodeElement;>;"
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_0

    .line 885
    invoke-static {p0}, Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper;->getListOfFilesFromThisProjectList(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v2

    .line 886
    .local v2, "filesFromDeletedProjects":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-static {p1}, Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper;->getListOfFilesFromThisProjectList(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v3

    .line 888
    .local v3, "filesFromSavedProjects":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_0

    .line 889
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v5

    .line 890
    .local v5, "totalCountOfFiles":I
    const/4 v4, 0x0

    .local v4, "j":I
    :goto_0
    if-lt v4, v5, :cond_1

    .line 902
    .end local v2    # "filesFromDeletedProjects":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v3    # "filesFromSavedProjects":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v4    # "j":I
    .end local v5    # "totalCountOfFiles":I
    :cond_0
    return-void

    .line 891
    .restart local v2    # "filesFromDeletedProjects":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v3    # "filesFromSavedProjects":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v4    # "j":I
    .restart local v5    # "totalCountOfFiles":I
    :cond_1
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 892
    .local v1, "filePath":Ljava/lang/String;
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_2

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 893
    :cond_2
    sget-object v6, Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper;->PATH_FOR_SHARED_PRJ_CONTENTS:Ljava/lang/String;

    invoke-virtual {v1, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 894
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 895
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 896
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 890
    .end local v0    # "file":Ljava/io/File;
    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method

.method public static exportProject(Lcom/samsung/app/video/editor/external/TranscodeElement;Landroid/content/Context;)V
    .locals 6
    .param p0, "project"    # Lcom/samsung/app/video/editor/external/TranscodeElement;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 825
    invoke-static {p0}, Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper;->createProjectJSONFile(Lcom/samsung/app/video/editor/external/TranscodeElement;)Ljava/lang/String;

    move-result-object v3

    .line 826
    .local v3, "jsonFilePath":Ljava/lang/String;
    if-eqz v3, :cond_2

    .line 827
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Project json File is created for DEMO ONLY @ "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " check LOGS with TAG "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "ProjectSharing"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " for files used in this project"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    invoke-static {p1, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 828
    invoke-static {p0}, Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper;->getListOfFilesInTranscodeElement(Lcom/samsung/app/video/editor/external/TranscodeElement;)Ljava/util/ArrayList;

    move-result-object v0

    .line 829
    .local v0, "fileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 830
    const-string v4, "ProjectSharing"

    const-string v5, "-------------Begin List of files in this exported Project-------------------"

    invoke-static {v4, v5}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 831
    const-string v1, ""

    .line 832
    .local v1, "fileListStr":Ljava/lang/String;
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt v2, v4, :cond_1

    .line 835
    const-string v4, "ProjectSharing"

    invoke-static {v4, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 836
    const-string v4, "ProjectSharing"

    const-string v5, "-------------End List of files in this exported Project-------------------"

    invoke-static {v4, v5}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 841
    .end local v0    # "fileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v1    # "fileListStr":Ljava/lang/String;
    .end local v2    # "j":I
    :cond_0
    :goto_1
    return-void

    .line 833
    .restart local v0    # "fileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v1    # "fileListStr":Ljava/lang/String;
    .restart local v2    # "j":I
    :cond_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 832
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 840
    .end local v0    # "fileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v1    # "fileListStr":Ljava/lang/String;
    .end local v2    # "j":I
    :cond_2
    const-string v4, "Error creating Project File"

    const/4 v5, 0x0

    invoke-static {p1, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    goto :goto_1
.end method

.method public static generateUniqueProjectID(Landroid/content/Context;)Ljava/lang/String;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 909
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "android_id"

    invoke-static {v1, v2}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 910
    .local v0, "android_id":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private static getListOfFilesFromThisProjectList(Ljava/util/List;)Ljava/util/ArrayList;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/TranscodeElement;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 205
    .local p0, "projectList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/TranscodeElement;>;"
    const/4 v3, 0x0

    .line 206
    .local v3, "listOfMedia":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_0

    .line 207
    new-instance v3, Ljava/util/ArrayList;

    .end local v3    # "listOfMedia":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 208
    .restart local v3    # "listOfMedia":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    .line 209
    .local v0, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-lt v2, v0, :cond_1

    .line 217
    .end local v0    # "count":I
    .end local v2    # "i":I
    :cond_0
    return-object v3

    .line 210
    .restart local v0    # "count":I
    .restart local v2    # "i":I
    :cond_1
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/app/video/editor/external/TranscodeElement;

    .line 211
    .local v4, "transcodeElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    invoke-static {v4}, Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper;->getListOfFilesInTranscodeElement(Lcom/samsung/app/video/editor/external/TranscodeElement;)Ljava/util/ArrayList;

    move-result-object v1

    .line 212
    .local v1, "filesInThisProject":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_2

    .line 213
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 209
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public static getListOfFilesInTranscodeElement(Lcom/samsung/app/video/editor/external/TranscodeElement;)Ljava/util/ArrayList;
    .locals 15
    .param p0, "transcodeElement"    # Lcom/samsung/app/video/editor/external/TranscodeElement;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/app/video/editor/external/TranscodeElement;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 388
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 390
    .local v8, "fileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz p0, :cond_3

    .line 391
    const/4 v9, 0x0

    .line 392
    .local v9, "i":I
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v7

    .line 393
    .local v7, "elementCount":I
    const/4 v9, 0x0

    :goto_0
    if-lt v9, v7, :cond_4

    .line 399
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getAdditionlAudioEleList()Ljava/util/List;

    move-result-object v0

    .line 400
    .local v0, "audioEleList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    if-eqz v0, :cond_0

    .line 401
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 402
    .local v2, "bgmCount":I
    const/4 v9, 0x0

    :goto_1
    if-lt v9, v2, :cond_6

    .line 409
    .end local v2    # "bgmCount":I
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getAdditionlRecordEleList()Ljava/util/List;

    move-result-object v11

    .line 410
    .local v11, "recordEleList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    if-eqz v11, :cond_1

    .line 411
    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v10

    .line 412
    .local v10, "recordCount":I
    const/4 v9, 0x0

    :goto_2
    if-lt v9, v10, :cond_8

    .line 417
    .end local v10    # "recordCount":I
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getAdditionlSoundEleList()Ljava/util/List;

    move-result-object v13

    .line 418
    .local v13, "soundEleList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    if-eqz v13, :cond_2

    .line 419
    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v12

    .line 420
    .local v12, "soundCount":I
    const/4 v9, 0x0

    :goto_3
    if-lt v9, v12, :cond_9

    .line 425
    .end local v12    # "soundCount":I
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getTextEleList()Ljava/util/List;

    move-result-object v5

    .line 426
    .local v5, "clipartList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    if-eqz v5, :cond_3

    .line 427
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v4

    .line 428
    .local v4, "captionCount":I
    const/4 v9, 0x0

    :goto_4
    if-lt v9, v4, :cond_a

    .line 435
    .end local v0    # "audioEleList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    .end local v4    # "captionCount":I
    .end local v5    # "clipartList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    .end local v7    # "elementCount":I
    .end local v9    # "i":I
    .end local v11    # "recordEleList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    .end local v13    # "soundEleList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    :cond_3
    return-object v8

    .line 394
    .restart local v7    # "elementCount":I
    .restart local v9    # "i":I
    :cond_4
    invoke-virtual {p0, v9}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElement(I)Lcom/samsung/app/video/editor/external/Element;

    move-result-object v6

    .line 395
    .local v6, "element":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {v6}, Lcom/samsung/app/video/editor/external/Element;->isAssetResource()Z

    move-result v14

    if-nez v14, :cond_5

    .line 396
    invoke-virtual {v6}, Lcom/samsung/app/video/editor/external/Element;->getFilePath()Ljava/lang/String;

    move-result-object v14

    invoke-static {v8, v14}, Lcom/sec/android/app/ve/util/CommonUtils;->addUniqueStringToList(Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 393
    :cond_5
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 403
    .end local v6    # "element":Lcom/samsung/app/video/editor/external/Element;
    .restart local v0    # "audioEleList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    .restart local v2    # "bgmCount":I
    :cond_6
    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/app/video/editor/external/Element;

    .line 404
    .local v1, "audioElement":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/Element;->isAssetResource()Z

    move-result v14

    if-nez v14, :cond_7

    .line 405
    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/Element;->getFilePath()Ljava/lang/String;

    move-result-object v14

    invoke-static {v8, v14}, Lcom/sec/android/app/ve/util/CommonUtils;->addUniqueStringToList(Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 402
    :cond_7
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 413
    .end local v1    # "audioElement":Lcom/samsung/app/video/editor/external/Element;
    .end local v2    # "bgmCount":I
    .restart local v10    # "recordCount":I
    .restart local v11    # "recordEleList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    :cond_8
    invoke-interface {v11, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/samsung/app/video/editor/external/Element;

    invoke-virtual {v14}, Lcom/samsung/app/video/editor/external/Element;->getFilePath()Ljava/lang/String;

    move-result-object v14

    invoke-static {v8, v14}, Lcom/sec/android/app/ve/util/CommonUtils;->addUniqueStringToList(Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 412
    add-int/lit8 v9, v9, 0x1

    goto :goto_2

    .line 421
    .end local v10    # "recordCount":I
    .restart local v12    # "soundCount":I
    .restart local v13    # "soundEleList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    :cond_9
    invoke-interface {v13, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/samsung/app/video/editor/external/Element;

    invoke-virtual {v14}, Lcom/samsung/app/video/editor/external/Element;->getFilePath()Ljava/lang/String;

    move-result-object v14

    invoke-static {v8, v14}, Lcom/sec/android/app/ve/util/CommonUtils;->addUniqueStringToList(Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 420
    add-int/lit8 v9, v9, 0x1

    goto :goto_3

    .line 429
    .end local v12    # "soundCount":I
    .restart local v4    # "captionCount":I
    .restart local v5    # "clipartList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    :cond_a
    invoke-interface {v5, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/app/video/editor/external/ClipartParams;

    .line 430
    .local v3, "caption":Lcom/samsung/app/video/editor/external/ClipartParams;
    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/ClipartParams;->isAssetResource()Z

    move-result v14

    if-nez v14, :cond_b

    .line 431
    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/ClipartParams;->getFilePath()Ljava/lang/String;

    move-result-object v14

    invoke-static {v8, v14}, Lcom/sec/android/app/ve/util/CommonUtils;->addUniqueStringToList(Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 428
    :cond_b
    add-int/lit8 v9, v9, 0x1

    goto :goto_4
.end method

.method public static getProjectIDFromMetaData([B)Ljava/lang/String;
    .locals 4
    .param p0, "metaDataArray"    # [B

    .prologue
    .line 872
    const/4 v2, 0x0

    .line 874
    .local v2, "uniqueID":Ljava/lang/String;
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, p0}, Ljava/lang/String;-><init>([B)V

    invoke-direct {v1, v3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 875
    .local v1, "metaDataJsonObj":Lorg/json/JSONObject;
    const-string v3, "JSON_KEY_UNIQUE_PROJECT_ID"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 880
    .end local v1    # "metaDataJsonObj":Lorg/json/JSONObject;
    :goto_0
    return-object v2

    .line 877
    :catch_0
    move-exception v0

    .line 878
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method private static launchProjectSharingHandler(Landroid/content/Context;Lcom/samsung/app/video/editor/external/TranscodeElement;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "shareTE"    # Lcom/samsung/app/video/editor/external/TranscodeElement;

    .prologue
    .line 852
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 853
    .local v0, "intent":Landroid/content/Intent;
    sget-object v1, Lcom/sec/android/app/ve/VEApp;->gAdaper:Lcom/sec/android/app/ve/VEAdapter;

    invoke-interface {v1}, Lcom/sec/android/app/ve/VEAdapter;->getProjectShareHandlerActivity()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    .line 854
    const-string v1, "EXTRA_SHOW_NEARBY_DEVICES"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 855
    const-string v1, "EXTRA_TO_SHARE_TE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 856
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 857
    return-void
.end method

.method public static launchProjectSharingInfoDialog(Landroid/content/Context;Lcom/samsung/app/video/editor/external/TranscodeElement;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "shareTE"    # Lcom/samsung/app/video/editor/external/TranscodeElement;

    .prologue
    .line 844
    new-instance v1, Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper$ProjectShareInfoDialog;

    invoke-direct {v1}, Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper$ProjectShareInfoDialog;-><init>()V

    .line 845
    .local v1, "dialog":Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper$ProjectShareInfoDialog;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 846
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "EXTRA_TO_SHARE_TE"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 847
    invoke-virtual {v1, v0}, Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper$ProjectShareInfoDialog;->setArguments(Landroid/os/Bundle;)V

    .line 848
    check-cast p0, Landroid/app/Activity;

    .end local p0    # "context":Landroid/content/Context;
    invoke-static {v1, p0}, Lcom/sec/android/app/ve/common/AndroidUtils;->showDialog(Landroid/app/DialogFragment;Landroid/app/Activity;)V

    .line 849
    return-void
.end method

.method public static prepareFinalListOfFilesToTransferAtSender([[B)Ljava/util/ArrayList;
    .locals 10
    .param p0, "payload"    # [[B
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([[B)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 526
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 527
    .local v4, "finalList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v6, 0x0

    .line 528
    .local v6, "metaDataJsonObj":Lorg/json/JSONObject;
    const/4 v3, 0x0

    .line 530
    .local v3, "fileListJsonObj":Lorg/json/JSONArray;
    :try_start_0
    new-instance v7, Lorg/json/JSONObject;

    new-instance v8, Ljava/lang/String;

    const/4 v9, 0x0

    aget-object v9, p0, v9

    invoke-direct {v8, v9}, Ljava/lang/String;-><init>([B)V

    invoke-direct {v7, v8}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 531
    .end local v6    # "metaDataJsonObj":Lorg/json/JSONObject;
    .local v7, "metaDataJsonObj":Lorg/json/JSONObject;
    :try_start_1
    const-string v8, "JSON_KEY_FILES_LIST"

    invoke-virtual {v7, v8}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    .line 532
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v0

    .line 533
    .local v0, "count":I
    if-lez v0, :cond_2

    .line 534
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    if-lt v5, v0, :cond_0

    move-object v6, v7

    .line 548
    .end local v0    # "count":I
    .end local v5    # "i":I
    .end local v7    # "metaDataJsonObj":Lorg/json/JSONObject;
    .restart local v6    # "metaDataJsonObj":Lorg/json/JSONObject;
    :goto_1
    return-object v4

    .line 535
    .end local v6    # "metaDataJsonObj":Lorg/json/JSONObject;
    .restart local v0    # "count":I
    .restart local v5    # "i":I
    .restart local v7    # "metaDataJsonObj":Lorg/json/JSONObject;
    :cond_0
    invoke-virtual {v3, v5}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    .line 536
    .local v2, "fileJSONObj":Lorg/json/JSONObject;
    const-string v8, "JSON_KEY_FILE_EXISTS"

    invoke-virtual {v2, v8}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 537
    const-string v8, "JSON_KEY_FILE_NAME"

    invoke-virtual {v2, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 534
    :cond_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 545
    .end local v0    # "count":I
    .end local v2    # "fileJSONObj":Lorg/json/JSONObject;
    .end local v5    # "i":I
    .end local v7    # "metaDataJsonObj":Lorg/json/JSONObject;
    .restart local v6    # "metaDataJsonObj":Lorg/json/JSONObject;
    :catch_0
    move-exception v1

    .line 546
    .local v1, "e":Lorg/json/JSONException;
    :goto_2
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_1

    .line 545
    .end local v1    # "e":Lorg/json/JSONException;
    .end local v6    # "metaDataJsonObj":Lorg/json/JSONObject;
    .restart local v7    # "metaDataJsonObj":Lorg/json/JSONObject;
    :catch_1
    move-exception v1

    move-object v6, v7

    .end local v7    # "metaDataJsonObj":Lorg/json/JSONObject;
    .restart local v6    # "metaDataJsonObj":Lorg/json/JSONObject;
    goto :goto_2

    .end local v6    # "metaDataJsonObj":Lorg/json/JSONObject;
    .restart local v0    # "count":I
    .restart local v7    # "metaDataJsonObj":Lorg/json/JSONObject;
    :cond_2
    move-object v6, v7

    .end local v7    # "metaDataJsonObj":Lorg/json/JSONObject;
    .restart local v6    # "metaDataJsonObj":Lorg/json/JSONObject;
    goto :goto_1
.end method

.method public static prepareProjectMetaDataAtSender(Lcom/samsung/app/video/editor/external/TranscodeElement;)Lorg/json/JSONObject;
    .locals 12
    .param p0, "transcodeElement"    # Lcom/samsung/app/video/editor/external/TranscodeElement;

    .prologue
    .line 460
    new-instance v6, Lorg/json/JSONArray;

    invoke-direct {v6}, Lorg/json/JSONArray;-><init>()V

    .line 461
    .local v6, "jsonfileList":Lorg/json/JSONArray;
    new-instance v7, Lorg/json/JSONObject;

    invoke-direct {v7}, Lorg/json/JSONObject;-><init>()V

    .line 463
    .local v7, "metaDataJSONObj":Lorg/json/JSONObject;
    :try_start_0
    const-string v9, "JSON_KEY_PROJECT_NAME"

    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getProjectName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v9, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 464
    const-string v9, "JSON_KEY_UNIQUE_PROJECT_ID"

    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getUniqueProjectID()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v9, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 465
    invoke-static {p0}, Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper;->getListOfFilesInTranscodeElement(Lcom/samsung/app/video/editor/external/TranscodeElement;)Ljava/util/ArrayList;

    move-result-object v2

    .line 466
    .local v2, "fileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_0

    .line 467
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v8

    .line 468
    .local v8, "size":I
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    if-lt v5, v8, :cond_1

    .line 480
    .end local v5    # "i":I
    .end local v8    # "size":I
    :cond_0
    const-string v9, "JSON_KEY_FILES_LIST"

    invoke-virtual {v7, v9, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 485
    .end local v2    # "fileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :goto_1
    return-object v7

    .line 469
    .restart local v2    # "fileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v5    # "i":I
    .restart local v8    # "size":I
    :cond_1
    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 470
    .local v4, "filePath":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 471
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_2

    .line 472
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    .line 473
    .local v3, "fileObj":Lorg/json/JSONObject;
    const-string v9, "JSON_KEY_FILE_NAME"

    invoke-virtual {v3, v9, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 474
    const-string v9, "JSON_KEY_FILE_SIZE"

    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v10

    invoke-virtual {v3, v9, v10, v11}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 475
    const-string v9, "JSON_KEY_FILE_EXISTS"

    const/4 v10, 0x0

    invoke-virtual {v3, v9, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 476
    invoke-virtual {v6, v3}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 468
    .end local v3    # "fileObj":Lorg/json/JSONObject;
    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 482
    .end local v1    # "file":Ljava/io/File;
    .end local v2    # "fileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v4    # "filePath":Ljava/lang/String;
    .end local v5    # "i":I
    .end local v8    # "size":I
    :catch_0
    move-exception v0

    .line 483
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_1
.end method

.method public static prepareProjectResponseAtReceiver([BLjava/util/HashMap;)Lorg/json/JSONObject;
    .locals 14
    .param p0, "payload"    # [B
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lorg/json/JSONObject;"
        }
    .end annotation

    .prologue
    .line 493
    .local p1, "sessionHashMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v8, 0x0

    .line 494
    .local v8, "metaDataJsonObj":Lorg/json/JSONObject;
    const/4 v4, 0x0

    .line 496
    .local v4, "fileListJsonObj":Lorg/json/JSONArray;
    :try_start_0
    new-instance v9, Lorg/json/JSONObject;

    new-instance v10, Ljava/lang/String;

    invoke-direct {v10, p0}, Ljava/lang/String;-><init>([B)V

    invoke-direct {v9, v10}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 497
    .end local v8    # "metaDataJsonObj":Lorg/json/JSONObject;
    .local v9, "metaDataJsonObj":Lorg/json/JSONObject;
    :try_start_1
    const-string v10, "JSON_KEY_FILES_LIST"

    invoke-virtual {v9, v10}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v4

    .line 498
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v0

    .line 499
    .local v0, "count":I
    if-lez v0, :cond_2

    .line 500
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    if-lt v7, v0, :cond_0

    move-object v8, v9

    .line 519
    .end local v0    # "count":I
    .end local v7    # "i":I
    .end local v9    # "metaDataJsonObj":Lorg/json/JSONObject;
    .restart local v8    # "metaDataJsonObj":Lorg/json/JSONObject;
    :goto_1
    return-object v8

    .line 501
    .end local v8    # "metaDataJsonObj":Lorg/json/JSONObject;
    .restart local v0    # "count":I
    .restart local v7    # "i":I
    .restart local v9    # "metaDataJsonObj":Lorg/json/JSONObject;
    :cond_0
    invoke-virtual {v4, v7}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    .line 502
    .local v3, "fileJSONObj":Lorg/json/JSONObject;
    const-string v10, "JSON_KEY_FILE_NAME"

    invoke-virtual {v3, v10}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 503
    .local v6, "filePathAtSender":Ljava/lang/String;
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 504
    .local v2, "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    .line 505
    .local v5, "fileName":Ljava/lang/String;
    new-instance v2, Ljava/io/File;

    .end local v2    # "file":Ljava/io/File;
    new-instance v10, Ljava/lang/StringBuilder;

    sget-object v11, Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper;->PATH_FOR_SHARED_PRJ_CONTENTS:Ljava/lang/String;

    invoke-static {v11}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v2, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 507
    .restart local v2    # "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_1

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v10

    const-string v12, "JSON_KEY_FILE_SIZE"

    invoke-virtual {v3, v12}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v12

    cmp-long v10, v10, v12

    if-nez v10, :cond_1

    .line 509
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1, v6, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 510
    const-string v10, "JSON_KEY_FILE_EXISTS"

    const/4 v11, 0x1

    invoke-virtual {v3, v10, v11}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 500
    :cond_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 516
    .end local v0    # "count":I
    .end local v2    # "file":Ljava/io/File;
    .end local v3    # "fileJSONObj":Lorg/json/JSONObject;
    .end local v5    # "fileName":Ljava/lang/String;
    .end local v6    # "filePathAtSender":Ljava/lang/String;
    .end local v7    # "i":I
    .end local v9    # "metaDataJsonObj":Lorg/json/JSONObject;
    .restart local v8    # "metaDataJsonObj":Lorg/json/JSONObject;
    :catch_0
    move-exception v1

    .line 517
    .local v1, "e":Lorg/json/JSONException;
    :goto_2
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_1

    .line 516
    .end local v1    # "e":Lorg/json/JSONException;
    .end local v8    # "metaDataJsonObj":Lorg/json/JSONObject;
    .restart local v9    # "metaDataJsonObj":Lorg/json/JSONObject;
    :catch_1
    move-exception v1

    move-object v8, v9

    .end local v9    # "metaDataJsonObj":Lorg/json/JSONObject;
    .restart local v8    # "metaDataJsonObj":Lorg/json/JSONObject;
    goto :goto_2

    .end local v8    # "metaDataJsonObj":Lorg/json/JSONObject;
    .restart local v0    # "count":I
    .restart local v9    # "metaDataJsonObj":Lorg/json/JSONObject;
    :cond_2
    move-object v8, v9

    .end local v9    # "metaDataJsonObj":Lorg/json/JSONObject;
    .restart local v8    # "metaDataJsonObj":Lorg/json/JSONObject;
    goto :goto_1
.end method

.method public static scanForSharedProjects()Z
    .locals 8

    .prologue
    .line 799
    const/4 v3, 0x0

    .line 800
    .local v3, "success":Z
    new-instance v4, Ljava/io/File;

    sget-object v6, Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper;->PATH_FOR_SHARED_PRJ_CONTENTS:Ljava/lang/String;

    invoke-direct {v4, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 802
    .local v4, "vePrjDir":Ljava/io/File;
    new-instance v6, Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper$1;

    invoke-direct {v6}, Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper$1;-><init>()V

    invoke-virtual {v4, v6}, Ljava/io/File;->list(Ljava/io/FilenameFilter;)[Ljava/lang/String;

    move-result-object v5

    .line 813
    .local v5, "vejsonFileList":[Ljava/lang/String;
    if-eqz v5, :cond_0

    array-length v6, v5

    if-lez v6, :cond_0

    .line 814
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v6, v5

    if-lt v0, v6, :cond_1

    .line 821
    .end local v0    # "i":I
    :cond_0
    return v3

    .line 815
    .restart local v0    # "i":I
    :cond_1
    new-instance v6, Ljava/lang/StringBuilder;

    sget-object v7, Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper;->PATH_FOR_SHARED_PRJ_CONTENTS:Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v7, v5, v0

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 816
    .local v1, "path":Ljava/lang/String;
    const/4 v6, 0x0

    invoke-static {v1, v6}, Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper;->scanThisSharedProject(Ljava/lang/String;Ljava/util/HashMap;)Z

    move-result v2

    .line 817
    .local v2, "ret":Z
    if-nez v3, :cond_2

    if-eqz v2, :cond_2

    .line 818
    const/4 v3, 0x1

    .line 814
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static scanThisSharedProject(Ljava/lang/String;Ljava/util/HashMap;)Z
    .locals 6
    .param p0, "jsonFilePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 780
    .local p1, "receivedPathMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v1, 0x0

    .line 783
    .local v1, "success":Z
    :try_start_0
    invoke-static {p0, p1}, Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper;->createTranscodeFromJSON(Ljava/lang/String;Ljava/util/HashMap;)Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v2

    .line 784
    .local v2, "trans":Lcom/samsung/app/video/editor/external/TranscodeElement;
    if-eqz v2, :cond_0

    .line 785
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/app/video/editor/external/TranscodeElement;->setProjectFileName(Ljava/lang/String;)V

    .line 786
    invoke-static {}, Lcom/sec/android/app/ve/pm/ProjectManager;->getInstance()Lcom/sec/android/app/ve/pm/ProjectManager;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/sec/android/app/ve/pm/ProjectManager;->saveProject(Lcom/samsung/app/video/editor/external/TranscodeElement;)Z

    .line 787
    const/4 v1, 0x1

    .line 788
    const-string v3, "ProjectSharing"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Succesfully created vep file @ "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getProjectFileName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 795
    .end local v2    # "trans":Lcom/samsung/app/video/editor/external/TranscodeElement;
    :cond_0
    :goto_0
    return v1

    .line 790
    :catch_0
    move-exception v0

    .line 791
    .local v0, "e":Ljava/io/FileNotFoundException;
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 792
    .end local v0    # "e":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v0

    .line 793
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public static startChordService(Landroid/content/Context;Z)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "fromBR"    # Z

    .prologue
    .line 861
    const/4 v1, 0x0

    .line 862
    .local v1, "ret":Z
    const-class v2, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/sec/android/app/ve/util/CommonUtils;->isServiceRunning(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 863
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 864
    .local v0, "chordServiceIntent":Landroid/content/Intent;
    const-string v2, "INTENT_EXTRA_FROM_BROADCASTRECEIVER"

    invoke-virtual {v0, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 865
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 866
    const/4 v1, 0x1

    .line 868
    .end local v0    # "chordServiceIntent":Landroid/content/Intent;
    .end local v1    # "ret":Z
    :cond_0
    return v1
.end method
