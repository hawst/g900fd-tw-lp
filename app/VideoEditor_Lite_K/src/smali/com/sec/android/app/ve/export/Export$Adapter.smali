.class public abstract Lcom/sec/android/app/ve/export/Export$Adapter;
.super Ljava/lang/Object;
.source "Export.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/ve/export/Export;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Adapter"
.end annotation


# instance fields
.field protected numberFormat:Ljava/text/NumberFormat;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 584
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public changeNotificationContentView(Landroid/content/Context;Landroid/widget/RemoteViews;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "contentView"    # Landroid/widget/RemoteViews;

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 730
    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "com.sec.android.app.storycam"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 731
    sget v3, Lcom/sec/android/app/ve/R$id;->export_done_text:I

    sget v4, Lcom/sec/android/app/ve/R$string;->ve_lite_export_done:I

    invoke-static {v4}, Lcom/sec/android/app/ve/VEApp;->getStringValue(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v3, v4}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 737
    :goto_0
    sget v3, Lcom/sec/android/app/ve/R$id;->status_icon:I

    sget-object v4, Lcom/sec/android/app/ve/VEApp;->gAdaper:Lcom/sec/android/app/ve/VEAdapter;

    invoke-interface {v4}, Lcom/sec/android/app/ve/VEAdapter;->getAppIconResource()I

    move-result v4

    invoke-virtual {p2, v3, v4}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 739
    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getJNIVersion()F

    move-result v3

    const/high16 v4, 0x40000000    # 2.0f

    cmpl-float v3, v3, v4

    if-nez v3, :cond_1

    .line 740
    sget v3, Lcom/sec/android/app/ve/R$id;->export_filename:I

    invoke-virtual {p0}, Lcom/sec/android/app/ve/export/Export$Adapter;->getFileName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v3, v4}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 746
    :goto_1
    const/4 v2, 0x0

    .line 747
    .local v2, "retriever":Landroid/media/MediaMetadataRetriever;
    sget-object v3, Lcom/sec/android/app/ve/VEApp;->gExport:Lcom/sec/android/app/ve/export/Export;

    invoke-virtual {v3}, Lcom/sec/android/app/ve/export/Export;->getFileName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v6, v3, v5}, Lcom/sec/android/app/ve/util/CommonUtils;->getRetrieverForSource(Landroid/content/res/AssetManager;Ljava/lang/String;Z)Landroid/media/MediaMetadataRetriever;

    move-result-object v2

    .line 748
    if-eqz v2, :cond_2

    .line 749
    sget-object v3, Lcom/sec/android/app/ve/VEApp;->gExport:Lcom/sec/android/app/ve/export/Export;

    invoke-virtual {v3}, Lcom/sec/android/app/ve/export/Export;->getFileName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v5, v6, v2, v3}, Lcom/sec/android/app/ve/util/CommonUtils;->getVideoDuration(ZLandroid/content/res/AssetManager;Landroid/media/MediaMetadataRetriever;Ljava/lang/String;)I

    move-result v3

    int-to-long v0, v3

    .line 750
    .local v0, "elementDur":J
    sget v3, Lcom/sec/android/app/ve/R$id;->percent_progress:I

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/util/CommonUtils;->getTimeInText(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v3, v4}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 756
    .end local v0    # "elementDur":J
    :goto_2
    return-void

    .line 735
    .end local v2    # "retriever":Landroid/media/MediaMetadataRetriever;
    :cond_0
    sget v3, Lcom/sec/android/app/ve/R$id;->export_done_text:I

    sget v4, Lcom/sec/android/app/ve/R$string;->ve_export_done:I

    invoke-static {v4}, Lcom/sec/android/app/ve/VEApp;->getStringValue(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v3, v4}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto :goto_0

    .line 743
    :cond_1
    sget v3, Lcom/sec/android/app/ve/R$id;->export_filename:I

    invoke-virtual {p0}, Lcom/sec/android/app/ve/export/Export$Adapter;->getFileName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v3, v4}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto :goto_1

    .line 752
    .restart local v2    # "retriever":Landroid/media/MediaMetadataRetriever;
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/ve/export/Export$Adapter;->numberFormat:Ljava/text/NumberFormat;

    if-nez v3, :cond_3

    .line 753
    invoke-static {}, Ljava/text/NumberFormat;->getInstance()Ljava/text/NumberFormat;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/ve/export/Export$Adapter;->numberFormat:Ljava/text/NumberFormat;

    .line 754
    :cond_3
    sget v3, Lcom/sec/android/app/ve/R$id;->percent_progress:I

    new-instance v4, Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/sec/android/app/ve/export/Export$Adapter;->numberFormat:Ljava/text/NumberFormat;

    const-wide/16 v6, 0x64

    invoke-virtual {v5, v6, v7}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "%"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v3, v4}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto :goto_2
.end method

.method public clearNativeJobBeforeExport()Z
    .locals 1

    .prologue
    .line 593
    const/4 v0, 0x1

    return v0
.end method

.method protected getExportResolution(I)Ljava/lang/String;
    .locals 2
    .param p1, "resolutionValue"    # I

    .prologue
    .line 766
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    .line 767
    const-string v0, " "

    .line 768
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, " ( "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/sec/android/app/ve/util/CommonUtils;->exportResolutionString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ) "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected getFileName()Ljava/lang/String;
    .locals 3

    .prologue
    .line 760
    sget-object v2, Lcom/sec/android/app/ve/VEApp;->gExport:Lcom/sec/android/app/ve/export/Export;

    invoke-virtual {v2}, Lcom/sec/android/app/ve/export/Export;->getFileName()Ljava/lang/String;

    move-result-object v1

    .line 761
    .local v1, "fileName":Ljava/lang/String;
    const/16 v2, 0x2f

    invoke-virtual {v1, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 762
    .local v0, "fName":Ljava/lang/String;
    return-object v0
.end method

.method public getNotificationLayoutID()I
    .locals 1

    .prologue
    .line 665
    sget v0, Lcom/sec/android/app/ve/R$layout;->notification_layout:I

    return v0
.end method

.method public getOngoingNotificationLayoutID()I
    .locals 1

    .prologue
    .line 670
    sget v0, Lcom/sec/android/app/ve/R$layout;->ongoing_notification_layout:I

    return v0
.end method

.method public getOngoingNotificationStringID()I
    .locals 1

    .prologue
    .line 674
    sget v0, Lcom/sec/android/app/ve/R$string;->exporting:I

    return v0
.end method

.method public getResolutionValue()I
    .locals 1

    .prologue
    .line 679
    # getter for: Lcom/sec/android/app/ve/export/Export;->resEnumValue:I
    invoke-static {}, Lcom/sec/android/app/ve/export/Export;->access$9()I

    move-result v0

    return v0
.end method

.method public initiateNotificationContentView(Landroid/content/Context;Landroid/widget/RemoteViews;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "contentView"    # Landroid/widget/RemoteViews;

    .prologue
    const/4 v3, 0x0

    .line 689
    sget v0, Lcom/sec/android/app/ve/R$id;->status_icon:I

    .line 690
    sget-object v1, Lcom/sec/android/app/ve/VEApp;->gAdaper:Lcom/sec/android/app/ve/VEAdapter;

    invoke-interface {v1}, Lcom/sec/android/app/ve/VEAdapter;->getAppIconResource()I

    move-result v1

    .line 689
    invoke-virtual {p2, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 691
    sget v0, Lcom/sec/android/app/ve/R$id;->status_text:I

    sget-object v1, Lcom/sec/android/app/ve/VEApp;->gExport:Lcom/sec/android/app/ve/export/Export;

    invoke-virtual {v1}, Lcom/sec/android/app/ve/export/Export;->getOngoingNotificationStringID()I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 693
    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getJNIVersion()F

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    .line 696
    sget v0, Lcom/sec/android/app/ve/R$id;->export_filename:I

    invoke-virtual {p0}, Lcom/sec/android/app/ve/export/Export$Adapter;->getFileName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 702
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/ve/export/Export$Adapter;->numberFormat:Ljava/text/NumberFormat;

    if-nez v0, :cond_0

    .line 703
    invoke-static {}, Ljava/text/NumberFormat;->getInstance()Ljava/text/NumberFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/ve/export/Export$Adapter;->numberFormat:Ljava/text/NumberFormat;

    .line 704
    :cond_0
    sget v0, Lcom/sec/android/app/ve/R$id;->percent_progress:I

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/sec/android/app/ve/export/Export$Adapter;->numberFormat:Ljava/text/NumberFormat;

    const-wide/16 v4, 0x0

    invoke-virtual {v2, v4, v5}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 705
    sget v0, Lcom/sec/android/app/ve/R$id;->status_progress:I

    const/16 v1, 0x64

    invoke-virtual {p2, v0, v1, v3, v3}, Landroid/widget/RemoteViews;->setProgressBar(IIIZ)V

    .line 706
    return-void

    .line 699
    :cond_1
    sget v0, Lcom/sec/android/app/ve/R$id;->export_filename:I

    invoke-virtual {p0}, Lcom/sec/android/app/ve/export/Export$Adapter;->getFileName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public isExportAfterAutoEdit()Z
    .locals 1

    .prologue
    .line 685
    const/4 v0, 0x0

    return v0
.end method

.method public abstract isPlayRequiredAfterExport()Z
.end method

.method public abstract isShareViaCall()Z
.end method

.method public nativeProcAfterExportPause()V
    .locals 0

    .prologue
    .line 627
    return-void
.end method

.method public nativeProcBeforeExportResume()V
    .locals 0

    .prologue
    .line 621
    return-void
.end method

.method public onExportCompleted()V
    .locals 0

    .prologue
    .line 642
    return-void
.end method

.method public onExportDropped()V
    .locals 0

    .prologue
    .line 609
    return-void
.end method

.method public onExportFailed()V
    .locals 0

    .prologue
    .line 648
    return-void
.end method

.method public onExportFinished(ZLandroid/net/Uri;)V
    .locals 0
    .param p1, "playSel"    # Z
    .param p2, "fileUri"    # Landroid/net/Uri;

    .prologue
    .line 653
    return-void
.end method

.method public onExportPaused()V
    .locals 0

    .prologue
    .line 615
    return-void
.end method

.method public onExportProgressUpdate(I)V
    .locals 0
    .param p1, "progress"    # I

    .prologue
    .line 655
    return-void
.end method

.method public onExportResumed()V
    .locals 0

    .prologue
    .line 632
    return-void
.end method

.method public onExportStarted()V
    .locals 0

    .prologue
    .line 601
    return-void
.end method

.method public onExportStopped()V
    .locals 0

    .prologue
    .line 637
    return-void
.end method

.method public onScanCompletedAfterExport(Landroid/net/Uri;)V
    .locals 0
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 661
    return-void
.end method

.method public updateNotificationContentView(Landroid/content/Context;Landroid/widget/RemoteViews;II)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "contentView"    # Landroid/widget/RemoteViews;
    .param p3, "progress"    # I
    .param p4, "state"    # I

    .prologue
    .line 709
    iget-object v1, p0, Lcom/sec/android/app/ve/export/Export$Adapter;->numberFormat:Ljava/text/NumberFormat;

    if-nez v1, :cond_0

    .line 710
    invoke-static {}, Ljava/text/NumberFormat;->getInstance()Ljava/text/NumberFormat;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/ve/export/Export$Adapter;->numberFormat:Ljava/text/NumberFormat;

    .line 711
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/sec/android/app/ve/export/Export$Adapter;->numberFormat:Ljava/text/NumberFormat;

    int-to-long v4, p3

    invoke-virtual {v2, v4, v5}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 712
    .local v0, "progressPercent":Ljava/lang/String;
    const/4 v1, 0x1

    if-ne p4, v1, :cond_2

    .line 713
    sget v1, Lcom/sec/android/app/ve/R$id;->status_text:I

    .line 714
    sget-object v2, Lcom/sec/android/app/ve/VEApp;->gExport:Lcom/sec/android/app/ve/export/Export;

    invoke-virtual {v2}, Lcom/sec/android/app/ve/export/Export;->getOngoingNotificationStringID()I

    move-result v2

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 713
    invoke-virtual {p2, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 715
    sget v1, Lcom/sec/android/app/ve/R$id;->percent_progress:I

    invoke-virtual {p2, v1, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 722
    :cond_1
    :goto_0
    sget v1, Lcom/sec/android/app/ve/R$id;->status_progress:I

    const/16 v2, 0x64

    .line 723
    const/4 v3, 0x0

    .line 722
    invoke-virtual {p2, v1, v2, p3, v3}, Landroid/widget/RemoteViews;->setProgressBar(IIIZ)V

    .line 724
    return-void

    .line 717
    :cond_2
    const/4 v1, 0x2

    if-ne p4, v1, :cond_1

    .line 718
    sget v1, Lcom/sec/android/app/ve/R$id;->status_text:I

    .line 719
    sget v2, Lcom/sec/android/app/ve/R$string;->export_message_paused:I

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 718
    invoke-virtual {p2, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 720
    sget v1, Lcom/sec/android/app/ve/R$id;->percent_progress:I

    invoke-virtual {p2, v1, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto :goto_0
.end method
