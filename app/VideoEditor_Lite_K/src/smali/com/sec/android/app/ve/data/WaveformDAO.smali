.class public Lcom/sec/android/app/ve/data/WaveformDAO;
.super Ljava/lang/Object;
.source "WaveformDAO.java"


# instance fields
.field private dataFileName:Ljava/lang/String;

.field private mediaSourcePath:Ljava/lang/String;

.field private waveformData:Ljava/nio/ByteBuffer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 28
    invoke-direct {p0, v0, v0, v0}, Lcom/sec/android/app/ve/data/WaveformDAO;-><init>(Ljava/nio/ByteBuffer;Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "dataFileName"    # Ljava/lang/String;
    .param p2, "mediaSourcePath"    # Ljava/lang/String;

    .prologue
    .line 31
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1, p2}, Lcom/sec/android/app/ve/data/WaveformDAO;-><init>(Ljava/nio/ByteBuffer;Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    return-void
.end method

.method public constructor <init>(Ljava/nio/ByteBuffer;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "waveformData"    # Ljava/nio/ByteBuffer;
    .param p2, "dataFileName"    # Ljava/lang/String;
    .param p3, "mediaSourcePath"    # Ljava/lang/String;

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/sec/android/app/ve/data/WaveformDAO;->waveformData:Ljava/nio/ByteBuffer;

    .line 35
    iput-object p2, p0, Lcom/sec/android/app/ve/data/WaveformDAO;->dataFileName:Ljava/lang/String;

    .line 36
    iput-object p3, p0, Lcom/sec/android/app/ve/data/WaveformDAO;->mediaSourcePath:Ljava/lang/String;

    .line 37
    return-void
.end method


# virtual methods
.method public getDataFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/app/ve/data/WaveformDAO;->dataFileName:Ljava/lang/String;

    return-object v0
.end method

.method public getMediaSourcePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/app/ve/data/WaveformDAO;->mediaSourcePath:Ljava/lang/String;

    return-object v0
.end method

.method public getWaveformData()Ljava/nio/ByteBuffer;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/app/ve/data/WaveformDAO;->waveformData:Ljava/nio/ByteBuffer;

    return-object v0
.end method

.method public setDataFileName(Ljava/lang/String;)V
    .locals 0
    .param p1, "dataFileName"    # Ljava/lang/String;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/sec/android/app/ve/data/WaveformDAO;->dataFileName:Ljava/lang/String;

    .line 65
    return-void
.end method

.method public setMediaSourcePath(Ljava/lang/String;)V
    .locals 0
    .param p1, "mediaSourcePath"    # Ljava/lang/String;

    .prologue
    .line 77
    iput-object p1, p0, Lcom/sec/android/app/ve/data/WaveformDAO;->mediaSourcePath:Ljava/lang/String;

    .line 78
    return-void
.end method

.method public setWaveformData(Ljava/nio/ByteBuffer;)V
    .locals 0
    .param p1, "waveformData"    # Ljava/nio/ByteBuffer;

    .prologue
    .line 50
    iput-object p1, p0, Lcom/sec/android/app/ve/data/WaveformDAO;->waveformData:Ljava/nio/ByteBuffer;

    .line 51
    return-void
.end method
