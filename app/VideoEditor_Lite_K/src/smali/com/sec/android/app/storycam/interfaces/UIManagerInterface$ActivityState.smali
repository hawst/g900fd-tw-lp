.class public final enum Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;
.super Ljava/lang/Enum;
.source "UIManagerInterface.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ActivityState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

.field public static final enum STATE_BGM:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

.field public static final enum STATE_DEFAULT:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

.field public static final enum STATE_EDIT:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 40
    new-instance v0, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    const-string v1, "STATE_DEFAULT"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;->STATE_DEFAULT:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    .line 41
    new-instance v0, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    const-string v1, "STATE_EDIT"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;->STATE_EDIT:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    .line 42
    new-instance v0, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    const-string v1, "STATE_BGM"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;->STATE_BGM:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    .line 39
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    sget-object v1, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;->STATE_DEFAULT:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;->STATE_EDIT:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;->STATE_BGM:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;->ENUM$VALUES:[Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;->ENUM$VALUES:[Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    array-length v1, v0

    new-array v2, v1, [Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
