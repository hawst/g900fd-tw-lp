.class public Lcom/sec/android/app/storycam/view/BGMView$BGMmBgmIconAdapter;
.super Landroid/widget/BaseAdapter;
.source "BGMView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/storycam/view/BGMView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "BGMmBgmIconAdapter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/storycam/view/BGMView;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/storycam/view/BGMView;)V
    .locals 0

    .prologue
    .line 339
    iput-object p1, p0, Lcom/sec/android/app/storycam/view/BGMView$BGMmBgmIconAdapter;->this$0:Lcom/sec/android/app/storycam/view/BGMView;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 343
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView$BGMmBgmIconAdapter;->this$0:Lcom/sec/android/app/storycam/view/BGMView;

    # getter for: Lcom/sec/android/app/storycam/view/BGMView;->mThumbIds:[Ljava/lang/Integer;
    invoke-static {v0}, Lcom/sec/android/app/storycam/view/BGMView;->access$13(Lcom/sec/android/app/storycam/view/BGMView;)[Ljava/lang/Integer;

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "arg0"    # I

    .prologue
    .line 348
    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "arg0"    # I

    .prologue
    .line 353
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const v8, 0x7f06034d

    .line 358
    const/4 v3, 0x0

    .line 359
    .local v3, "layout":Landroid/widget/RelativeLayout;
    if-nez p2, :cond_0

    .line 360
    iget-object v5, p0, Lcom/sec/android/app/storycam/view/BGMView$BGMmBgmIconAdapter;->this$0:Lcom/sec/android/app/storycam/view/BGMView;

    # getter for: Lcom/sec/android/app/storycam/view/BGMView;->mInflater:Landroid/view/LayoutInflater;
    invoke-static {v5}, Lcom/sec/android/app/storycam/view/BGMView;->access$14(Lcom/sec/android/app/storycam/view/BGMView;)Landroid/view/LayoutInflater;

    move-result-object v5

    const v6, 0x7f030004

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .end local v3    # "layout":Landroid/widget/RelativeLayout;
    check-cast v3, Landroid/widget/RelativeLayout;

    .line 364
    .restart local v3    # "layout":Landroid/widget/RelativeLayout;
    :goto_0
    const v5, 0x7f0d001c

    invoke-virtual {v3, v5}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 365
    .local v0, "bgmthumbnail":Landroid/widget/ImageView;
    const v5, 0x7f0d001e

    invoke-virtual {v3, v5}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 366
    .local v1, "bgmthumbnailBorder":Landroid/widget/ImageView;
    const v5, 0x7f0d001d

    invoke-virtual {v3, v5}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 367
    .local v2, "bgmthumbnailSelected":Landroid/widget/ImageView;
    iget-object v5, p0, Lcom/sec/android/app/storycam/view/BGMView$BGMmBgmIconAdapter;->this$0:Lcom/sec/android/app/storycam/view/BGMView;

    # getter for: Lcom/sec/android/app/storycam/view/BGMView;->mThumbIds:[Ljava/lang/Integer;
    invoke-static {v5}, Lcom/sec/android/app/storycam/view/BGMView;->access$13(Lcom/sec/android/app/storycam/view/BGMView;)[Ljava/lang/Integer;

    move-result-object v5

    aget-object v5, v5, p1

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 368
    const v5, 0x7f020006

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 369
    const v5, 0x7f02004f

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 370
    iget-object v5, p0, Lcom/sec/android/app/storycam/view/BGMView$BGMmBgmIconAdapter;->this$0:Lcom/sec/android/app/storycam/view/BGMView;

    # getter for: Lcom/sec/android/app/storycam/view/BGMView;->mSelectedPos:I
    invoke-static {v5}, Lcom/sec/android/app/storycam/view/BGMView;->access$3(Lcom/sec/android/app/storycam/view/BGMView;)I

    move-result v5

    if-ne p1, v5, :cond_1

    .line 371
    const v5, 0x7f020050

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 372
    const/4 v5, 0x0

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 377
    :goto_1
    sget-object v5, Lcom/sec/android/app/ve/VEApp;->gBGMManager:Lcom/sec/android/app/ve/bgm/BGMManager;

    invoke-virtual {v5, p1}, Lcom/sec/android/app/ve/bgm/BGMManager;->getBGMName(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 378
    iget-object v5, p0, Lcom/sec/android/app/storycam/view/BGMView$BGMmBgmIconAdapter;->this$0:Lcom/sec/android/app/storycam/view/BGMView;

    # getter for: Lcom/sec/android/app/storycam/view/BGMView;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/android/app/storycam/view/BGMView;->access$15(Lcom/sec/android/app/storycam/view/BGMView;)Landroid/content/Context;

    move-result-object v5

    invoke-static {v5, v0}, Lcom/sec/android/app/ve/common/ConfigUtils;->setHovering(Landroid/content/Context;Landroid/view/View;)V

    .line 379
    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout$LayoutParams;

    .line 380
    .local v4, "params":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-static {v8}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v5

    iput v5, v4, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 381
    invoke-static {v8}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v5

    iput v5, v4, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 382
    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 383
    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 384
    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 385
    return-object v3

    .end local v0    # "bgmthumbnail":Landroid/widget/ImageView;
    .end local v1    # "bgmthumbnailBorder":Landroid/widget/ImageView;
    .end local v2    # "bgmthumbnailSelected":Landroid/widget/ImageView;
    .end local v4    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_0
    move-object v3, p2

    .line 362
    check-cast v3, Landroid/widget/RelativeLayout;

    goto :goto_0

    .line 374
    .restart local v0    # "bgmthumbnail":Landroid/widget/ImageView;
    .restart local v1    # "bgmthumbnailBorder":Landroid/widget/ImageView;
    .restart local v2    # "bgmthumbnailSelected":Landroid/widget/ImageView;
    :cond_1
    iget-object v5, p0, Lcom/sec/android/app/storycam/view/BGMView$BGMmBgmIconAdapter;->this$0:Lcom/sec/android/app/storycam/view/BGMView;

    # getter for: Lcom/sec/android/app/storycam/view/BGMView;->mThumbIds:[Ljava/lang/Integer;
    invoke-static {v5}, Lcom/sec/android/app/storycam/view/BGMView;->access$13(Lcom/sec/android/app/storycam/view/BGMView;)[Ljava/lang/Integer;

    move-result-object v5

    aget-object v5, v5, p1

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 375
    const/16 v5, 0x8

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1
.end method
