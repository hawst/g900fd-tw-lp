.class Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$11$1;
.super Ljava/lang/Object;
.source "VideoEditorLiteActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$11;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$11;


# direct methods
.method constructor <init>(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$11;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$11$1;->this$1:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$11;

    .line 1872
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 1876
    iget-object v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$11$1;->this$1:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$11;

    # getter for: Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$11;->this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;
    invoke-static {v0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$11;->access$0(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$11;)Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;

    move-result-object v0

    # getter for: Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->captionText:Lcom/sec/android/app/storycam/view/Caption1Line;
    invoke-static {v0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->access$14(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;)Lcom/sec/android/app/storycam/view/Caption1Line;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/storycam/view/Caption1Line;->requestFocus()Z

    .line 1877
    iget-object v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$11$1;->this$1:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$11;

    # getter for: Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$11;->this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;
    invoke-static {v0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$11;->access$0(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$11;)Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;

    move-result-object v0

    # getter for: Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->captionText:Lcom/sec/android/app/storycam/view/Caption1Line;
    invoke-static {v0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->access$14(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;)Lcom/sec/android/app/storycam/view/Caption1Line;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$11$1;->this$1:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$11;

    # getter for: Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$11;->this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;
    invoke-static {v1}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$11;->access$0(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$11;)Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;

    move-result-object v1

    # getter for: Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->cursorPos:I
    invoke-static {v1}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->access$21(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/storycam/view/Caption1Line;->setSelection(I)V

    .line 1878
    iget-object v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$11$1;->this$1:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$11;

    # getter for: Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$11;->this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;
    invoke-static {v0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$11;->access$0(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$11;)Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;

    move-result-object v0

    # getter for: Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->captionText:Lcom/sec/android/app/storycam/view/Caption1Line;
    invoke-static {v0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->access$14(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;)Lcom/sec/android/app/storycam/view/Caption1Line;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$11$1;->this$1:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$11;

    # getter for: Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$11;->this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;
    invoke-static {v1}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$11;->access$0(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$11;)Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020009

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/storycam/view/Caption1Line;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1880
    return-void
.end method
