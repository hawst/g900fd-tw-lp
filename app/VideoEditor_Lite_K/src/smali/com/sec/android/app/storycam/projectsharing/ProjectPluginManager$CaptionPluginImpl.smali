.class public Lcom/sec/android/app/storycam/projectsharing/ProjectPluginManager$CaptionPluginImpl;
.super Ljava/lang/Object;
.source "ProjectPluginManager.java"

# interfaces
.implements Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework$CaptionPlugin;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/storycam/projectsharing/ProjectPluginManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CaptionPluginImpl"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 425
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public mapCaptionAlignmentFromGeneric(I)I
    .locals 1
    .param p1, "genericAlignment"    # I

    .prologue
    .line 480
    const/4 v0, 0x0

    .line 481
    .local v0, "internalAlignment":I
    packed-switch p1, :pswitch_data_0

    .line 490
    :goto_0
    return v0

    .line 483
    :pswitch_0
    const/4 v0, 0x1

    .line 484
    goto :goto_0

    .line 486
    :pswitch_1
    const/4 v0, 0x2

    .line 487
    goto :goto_0

    .line 481
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public mapCaptionAlignmentToGeneric(I)I
    .locals 1
    .param p1, "internalAlignment"    # I

    .prologue
    .line 465
    const/4 v0, 0x0

    .line 466
    .local v0, "genericAlignment":I
    packed-switch p1, :pswitch_data_0

    .line 475
    :goto_0
    return v0

    .line 468
    :pswitch_0
    const/4 v0, 0x1

    .line 469
    goto :goto_0

    .line 471
    :pswitch_1
    const/4 v0, 0x2

    .line 472
    goto :goto_0

    .line 466
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public mapCaptionThemeFromGeneric(I)I
    .locals 1
    .param p1, "genericTheme"    # I

    .prologue
    .line 447
    const/4 v0, 0x0

    .line 448
    .local v0, "internalTheme":I
    packed-switch p1, :pswitch_data_0

    .line 460
    :goto_0
    return v0

    .line 450
    :pswitch_0
    const/4 v0, 0x1

    .line 451
    goto :goto_0

    .line 453
    :pswitch_1
    const/4 v0, 0x2

    .line 454
    goto :goto_0

    .line 456
    :pswitch_2
    const/4 v0, 0x3

    .line 457
    goto :goto_0

    .line 448
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public mapCaptionThemeToGeneric(I)I
    .locals 1
    .param p1, "internalTheme"    # I

    .prologue
    .line 429
    const/4 v0, 0x0

    .line 430
    .local v0, "genericTheme":I
    packed-switch p1, :pswitch_data_0

    .line 442
    :goto_0
    return v0

    .line 432
    :pswitch_0
    const/4 v0, 0x1

    .line 433
    goto :goto_0

    .line 435
    :pswitch_1
    const/4 v0, 0x2

    .line 436
    goto :goto_0

    .line 438
    :pswitch_2
    const/4 v0, 0x3

    .line 439
    goto :goto_0

    .line 430
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public mapThemeCaptionAlignmentFromGeneric(I)I
    .locals 1
    .param p1, "genericAlignment"    # I

    .prologue
    .line 511
    const/4 v0, 0x3

    .line 512
    .local v0, "internalAlignment":I
    packed-switch p1, :pswitch_data_0

    .line 522
    :goto_0
    return v0

    .line 514
    :pswitch_0
    const/16 v0, 0x11

    .line 515
    goto :goto_0

    .line 517
    :pswitch_1
    const/4 v0, 0x5

    .line 518
    goto :goto_0

    .line 512
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public mapThemeCaptionAlignmentToGeneric(I)I
    .locals 1
    .param p1, "internalAlignment"    # I

    .prologue
    .line 495
    const/4 v0, 0x0

    .line 496
    .local v0, "genericAlignment":I
    sparse-switch p1, :sswitch_data_0

    .line 506
    :goto_0
    return v0

    .line 498
    :sswitch_0
    const/4 v0, 0x1

    .line 499
    goto :goto_0

    .line 501
    :sswitch_1
    const/4 v0, 0x2

    .line 502
    goto :goto_0

    .line 496
    nop

    :sswitch_data_0
    .sparse-switch
        0x5 -> :sswitch_1
        0x11 -> :sswitch_0
    .end sparse-switch
.end method
