.class public Lcom/sec/android/app/storycam/bgm/BlueJonesBGM;
.super Lcom/sec/android/app/ve/bgm/BGM;
.source "BlueJonesBGM.java"


# static fields
.field private static synthetic $SWITCH_TABLE$com$sec$android$app$ve$bgm$BGM$BGMModes:[I


# instance fields
.field private fastModeBGM:[I

.field private mediumModeBGM:[I

.field private slowModeBGM:[I

.field private totalDurationBGM:I


# direct methods
.method static synthetic $SWITCH_TABLE$com$sec$android$app$ve$bgm$BGM$BGMModes()[I
    .locals 3

    .prologue
    .line 5
    sget-object v0, Lcom/sec/android/app/storycam/bgm/BlueJonesBGM;->$SWITCH_TABLE$com$sec$android$app$ve$bgm$BGM$BGMModes:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/sec/android/app/ve/bgm/BGM$BGMModes;->values()[Lcom/sec/android/app/ve/bgm/BGM$BGMModes;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/sec/android/app/ve/bgm/BGM$BGMModes;->FAST:Lcom/sec/android/app/ve/bgm/BGM$BGMModes;

    invoke-virtual {v1}, Lcom/sec/android/app/ve/bgm/BGM$BGMModes;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    :try_start_1
    sget-object v1, Lcom/sec/android/app/ve/bgm/BGM$BGMModes;->MEDIUM:Lcom/sec/android/app/ve/bgm/BGM$BGMModes;

    invoke-virtual {v1}, Lcom/sec/android/app/ve/bgm/BGM$BGMModes;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    :try_start_2
    sget-object v1, Lcom/sec/android/app/ve/bgm/BGM$BGMModes;->SLOW:Lcom/sec/android/app/ve/bgm/BGM$BGMModes;

    invoke-virtual {v1}, Lcom/sec/android/app/ve/bgm/BGM$BGMModes;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_3
    sput-object v0, Lcom/sec/android/app/storycam/bgm/BlueJonesBGM;->$SWITCH_TABLE$com$sec$android$app$ve$bgm$BGM$BGMModes:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_1
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/sec/android/app/ve/bgm/BGM;-><init>()V

    .line 8
    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    .line 13
    iput-object v0, p0, Lcom/sec/android/app/storycam/bgm/BlueJonesBGM;->slowModeBGM:[I

    .line 16
    const/16 v0, 0x9

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    .line 24
    iput-object v0, p0, Lcom/sec/android/app/storycam/bgm/BlueJonesBGM;->mediumModeBGM:[I

    .line 27
    const/16 v0, 0xc

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    .line 38
    iput-object v0, p0, Lcom/sec/android/app/storycam/bgm/BlueJonesBGM;->fastModeBGM:[I

    .line 41
    const v0, 0x91f0

    iput v0, p0, Lcom/sec/android/app/storycam/bgm/BlueJonesBGM;->totalDurationBGM:I

    .line 5
    return-void

    .line 8
    nop

    :array_0
    .array-data 4
        0x1450
        0x27d8
        0x3afc
        0x4f4c
        0x6338
        0x7594
    .end array-data

    .line 16
    :array_1
    .array-data 4
        0xc1c
        0x1ce8
        0x2a30
        0x3778
        0x49d4
        0x5848
        0x6590
        0x7594
        0x814c
    .end array-data

    .line 27
    :array_2
    .array-data 4
        0x898
        0x1450
        0x2008
        0x27d8
        0x3390
        0x4074
        0x4c90
        0x5848
        0x6400
        0x6fb8
        0x7b70
        0x8854
    .end array-data
.end method


# virtual methods
.method protected getBGMFilepath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    const-string v0, "themedata/bgm/C2_Blue Jones.ogg"

    return-object v0
.end method

.method protected getBGMName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    const-string v0, "Blue Jones"

    return-object v0
.end method

.method protected getTotalDurationBGM()I
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Lcom/sec/android/app/storycam/bgm/BlueJonesBGM;->totalDurationBGM:I

    return v0
.end method

.method protected getTransitionTimeStamps(Lcom/sec/android/app/ve/bgm/BGM$BGMModes;)[I
    .locals 2
    .param p1, "mode"    # Lcom/sec/android/app/ve/bgm/BGM$BGMModes;

    .prologue
    .line 45
    invoke-static {}, Lcom/sec/android/app/storycam/bgm/BlueJonesBGM;->$SWITCH_TABLE$com$sec$android$app$ve$bgm$BGM$BGMModes()[I

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/android/app/ve/bgm/BGM$BGMModes;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 53
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 47
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/storycam/bgm/BlueJonesBGM;->slowModeBGM:[I

    goto :goto_0

    .line 49
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/storycam/bgm/BlueJonesBGM;->mediumModeBGM:[I

    goto :goto_0

    .line 51
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/storycam/bgm/BlueJonesBGM;->fastModeBGM:[I

    goto :goto_0

    .line 45
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
