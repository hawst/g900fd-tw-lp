.class Lcom/sec/android/app/storycam/view/MediaEditGridView$7;
.super Ljava/lang/Object;
.source "MediaEditGridView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/storycam/view/MediaEditGridView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/storycam/view/MediaEditGridView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView$7;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    .line 384
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 387
    invoke-static {}, Lcom/sec/android/app/ve/util/CommonUtils;->isUserEventTooSoonAfterPrev()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 394
    :goto_0
    return-void

    .line 390
    :cond_0
    sget-object v0, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v0}, Lcom/sec/android/app/storycam/AppDataManager;->getOriginalTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v0

    const/16 v1, 0xf

    if-lt v0, v1, :cond_1

    .line 391
    # getter for: Lcom/sec/android/app/storycam/view/MediaEditGridView;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;
    invoke-static {}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->access$1()Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;->showMaxItemsDialog()V

    goto :goto_0

    .line 393
    :cond_1
    # getter for: Lcom/sec/android/app/storycam/view/MediaEditGridView;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;
    invoke-static {}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->access$1()Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;->launchGalleryForAddMedia()V

    goto :goto_0
.end method
