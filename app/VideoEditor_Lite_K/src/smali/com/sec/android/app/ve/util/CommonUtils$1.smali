.class Lcom/sec/android/app/ve/util/CommonUtils$1;
.super Landroid/os/Handler;
.source "CommonUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/ve/util/CommonUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private maxCharsToast:Landroid/widget/Toast;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 115
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 1
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 121
    iget-object v0, p0, Lcom/sec/android/app/ve/util/CommonUtils$1;->maxCharsToast:Landroid/widget/Toast;

    if-nez v0, :cond_0

    .line 122
    sget v0, Lcom/sec/android/app/ve/R$string;->toast_max_characters:I

    invoke-static {v0}, Lcom/sec/android/app/ve/VEApp;->makeToast(I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/ve/util/CommonUtils$1;->maxCharsToast:Landroid/widget/Toast;

    .line 125
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/ve/util/CommonUtils$1;->maxCharsToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getWindowVisibility()I

    move-result v0

    if-eqz v0, :cond_1

    .line 126
    iget-object v0, p0, Lcom/sec/android/app/ve/util/CommonUtils$1;->maxCharsToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 127
    :cond_1
    return-void
.end method
