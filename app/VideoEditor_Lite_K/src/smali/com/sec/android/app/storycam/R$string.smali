.class public final Lcom/sec/android/app/storycam/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/storycam/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final Bgm_layer_bg_text:I = 0x7f07030b

.field public static final Follow_template:I = 0x7f070306

.field public static final Record_layer_bg_text:I = 0x7f07030c

.field public static final Text_layer_bg_text:I = 0x7f07030d

.field public static final Voice_effect:I = 0x7f0702ec

.field public static final about:I = 0x7f0702b2

.field public static final about_title:I = 0x7f0702c2

.field public static final accesibility_audio_effects:I = 0x7f070049

.field public static final accesibility_caption_cancel:I = 0x7f07003d

.field public static final accesibility_file_not_supported:I = 0x7f070044

.field public static final accesibility_sound_effect:I = 0x7f0701f0

.field public static final accesibility_stop:I = 0x7f070043

.field public static final accesibility_text_effects:I = 0x7f070047

.field public static final accesibility_videoclip:I = 0x7f07003c

.field public static final accesibility_visual_effects:I = 0x7f070046

.field public static final accesibility_volume_control:I = 0x7f070048

.field public static final accesibilty_play:I = 0x7f070037

.field public static final accessibility_Trimbar_left:I = 0x7f07003a

.field public static final accessibility_Trimbar_right:I = 0x7f07003b

.field public static final accessibility_add_media:I = 0x7f0702fd

.field public static final accessibility_add_media_capture_image:I = 0x7f070000

.field public static final accessibility_audio:I = 0x7f070001

.field public static final accessibility_bars:I = 0x7f070199

.field public static final accessibility_black:I = 0x7f070016

.field public static final accessibility_block:I = 0x7f07019a

.field public static final accessibility_camera:I = 0x7f070002

.field public static final accessibility_cartoon:I = 0x7f070275

.field public static final accessibility_circle:I = 0x7f07019e

.field public static final accessibility_cubeturn_a:I = 0x7f070013

.field public static final accessibility_cubeturn_b:I = 0x7f070014

.field public static final accessibility_delete_button:I = 0x7f070039

.field public static final accessibility_dissolve:I = 0x7f07019b

.field public static final accessibility_download:I = 0x7f07003e

.field public static final accessibility_edititem:I = 0x7f070045

.field public static final accessibility_effect_disabled:I = 0x7f070040

.field public static final accessibility_fade:I = 0x7f070198

.field public static final accessibility_fade_inout:I = 0x7f070012

.field public static final accessibility_faded_colors:I = 0x7f070276

.field public static final accessibility_forward:I = 0x7f070003

.field public static final accessibility_fullscreenbutton:I = 0x7f07001d

.field public static final accessibility_grayscale:I = 0x7f070272

.field public static final accessibility_images:I = 0x7f070004

.field public static final accessibility_menu_cancel:I = 0x7f070005

.field public static final accessibility_menu_done:I = 0x7f070006

.field public static final accessibility_menu_next:I = 0x7f070007

.field public static final accessibility_menudeleteproj:I = 0x7f070008

.field public static final accessibility_menuexportproj:I = 0x7f070009

.field public static final accessibility_miniscreenbutton:I = 0x7f07001e

.field public static final accessibility_mirror:I = 0x7f07027f

.field public static final accessibility_mosaic:I = 0x7f07027b

.field public static final accessibility_none_transition:I = 0x7f070197

.field public static final accessibility_oil_painting:I = 0x7f07027a

.field public static final accessibility_old_film:I = 0x7f070280

.field public static final accessibility_origami:I = 0x7f07019c

.field public static final accessibility_out_of_focus:I = 0x7f070278

.field public static final accessibility_pause:I = 0x7f07000a

.field public static final accessibility_pixelise:I = 0x7f070019

.field public static final accessibility_play:I = 0x7f07000c

.field public static final accessibility_playbutton:I = 0x7f07000e

.field public static final accessibility_rewind:I = 0x7f07000f

.field public static final accessibility_rugged:I = 0x7f07027d

.field public static final accessibility_sepia:I = 0x7f070273

.field public static final accessibility_shining:I = 0x7f070277

.field public static final accessibility_simple:I = 0x7f070271

.field public static final accessibility_slide_left:I = 0x7f070018

.field public static final accessibility_slide_top:I = 0x7f070017

.field public static final accessibility_split:I = 0x7f070042

.field public static final accessibility_stop:I = 0x7f070010

.field public static final accessibility_swap:I = 0x7f07019d

.field public static final accessibility_tap_to_center:I = 0x7f070041

.field public static final accessibility_tap_to_select:I = 0x7f07003f

.field public static final accessibility_thermal:I = 0x7f07027e

.field public static final accessibility_tint:I = 0x7f07027c

.field public static final accessibility_video:I = 0x7f070011

.field public static final accessibility_vignette:I = 0x7f070279

.field public static final accessibility_vintage:I = 0x7f070274

.field public static final accessibility_wheel:I = 0x7f07019f

.field public static final accessibility_white:I = 0x7f070015

.field public static final accessibility_wiperight:I = 0x7f0701a0

.field public static final action_big_screens:I = 0x7f070155

.field public static final action_brilliant_mind:I = 0x7f07014a

.field public static final action_credit:I = 0x7f070156

.field public static final action_credit_credits:I = 0x7f070158

.field public static final action_credit_default_film:I = 0x7f07015b

.field public static final action_credit_default_film_eg:I = 0x7f07015c

.field public static final action_credit_director:I = 0x7f070159

.field public static final action_credit_director_eg:I = 0x7f07015a

.field public static final action_credit_main_cast:I = 0x7f07015d

.field public static final action_credit_main_cast_eg:I = 0x7f07015e

.field public static final action_credit_supporting_actor:I = 0x7f07015f

.field public static final action_credit_supporting_actor_eg:I = 0x7f070160

.field public static final action_credit_title:I = 0x7f070157

.field public static final action_credit_venue:I = 0x7f070161

.field public static final action_credit_venue_eg:I = 0x7f070162

.field public static final action_diverse:I = 0x7f07014b

.field public static final action_dominate:I = 0x7f070154

.field public static final action_ensure_peace:I = 0x7f070145

.field public static final action_enthralling_story:I = 0x7f070151

.field public static final action_fierce_action:I = 0x7f07014e

.field public static final action_filled_action:I = 0x7f070152

.field public static final action_hero_engages:I = 0x7f07014d

.field public static final action_lightning_speed:I = 0x7f070149

.field public static final action_lurks:I = 0x7f070144

.field public static final action_man_mobilizes:I = 0x7f070146

.field public static final action_moving_at:I = 0x7f070148

.field public static final action_of_hero:I = 0x7f070141

.field public static final action_our_hero:I = 0x7f070147

.field public static final action_peaceful_land:I = 0x7f070142

.field public static final action_restore_peace:I = 0x7f070150

.field public static final action_return:I = 0x7f070140

.field public static final action_techniques:I = 0x7f07014c

.field public static final action_theme_desciption:I = 0x7f0700a8

.field public static final action_unknown:I = 0x7f070143

.field public static final action_will_he_be_able:I = 0x7f07014f

.field public static final action_will_unfold:I = 0x7f070153

.field public static final add:I = 0x7f07004a

.field public static final add_media:I = 0x7f0702fe

.field public static final add_media_first:I = 0x7f07004c

.field public static final add_media_music_option_device_music:I = 0x7f07000d

.field public static final add_title:I = 0x7f07004d

.field public static final addmedia_image:I = 0x7f0701ff

.field public static final addmedia_music:I = 0x7f070200

.field public static final album_song:I = 0x7f07004e

.field public static final album_songs:I = 0x7f07004f

.field public static final albums:I = 0x7f070050

.field public static final alien:I = 0x7f070051

.field public static final allsongs:I = 0x7f070052

.field public static final always:I = 0x7f070260

.field public static final app_name:I = 0x7f070053

.field public static final apply_theme:I = 0x7f070054

.field public static final approx:I = 0x7f070261

.field public static final approx_percent_mb:I = 0x7f0702b7

.field public static final approx_percent_mb_full:I = 0x7f0702b8

.field public static final arabic_locale:I = 0x7f07024e

.field public static final artists:I = 0x7f070055

.field public static final attention:I = 0x7f070234

.field public static final audio:I = 0x7f070056

.field public static final auto_edit:I = 0x7f0702fb

.field public static final auto_edit_failure:I = 0x7f070313

.field public static final auto_edit_progress_msg:I = 0x7f07020e

.field public static final auto_making:I = 0x7f070057

.field public static final autoedit_too_short:I = 0x7f070058

.field public static final back_string_bgm_list:I = 0x7f0702f7

.field public static final battery_low:I = 0x7f070059

.field public static final battery_low_stopplayer:I = 0x7f07005a

.field public static final bg_music:I = 0x7f07005d

.field public static final bgm:I = 0x7f07005b

.field public static final bgm_a_day_on_the_train:I = 0x7f0702a2

.field public static final bgm_a_day_on_the_trains:I = 0x7f070334

.field public static final bgm_actio:I = 0x7f0702a5

.field public static final bgm_action:I = 0x7f070337

.field public static final bgm_blue_duc:I = 0x7f0702b0

.field public static final bgm_blue_duck:I = 0x7f070341

.field public static final bgm_blue_jone:I = 0x7f07029d

.field public static final bgm_blue_jones:I = 0x7f07032f

.field public static final bgm_bolt_to:I = 0x7f0702a1

.field public static final bgm_bolt_toy:I = 0x7f070333

.field public static final bgm_button:I = 0x7f0701fa

.field public static final bgm_dubb:I = 0x7f0702a4

.field public static final bgm_dubby:I = 0x7f070336

.field public static final bgm_ego_tri:I = 0x7f0702ac

.field public static final bgm_ego_trip:I = 0x7f07033d

.field public static final bgm_famil:I = 0x7f0702a8

.field public static final bgm_family:I = 0x7f070339

.field public static final bgm_fear_of_darknes:I = 0x7f07029f

.field public static final bgm_fear_of_darkness:I = 0x7f070331

.field public static final bgm_for_childre:I = 0x7f0702aa

.field public static final bgm_for_children:I = 0x7f07033b

.field public static final bgm_funk_work:I = 0x7f0702ad

.field public static final bgm_funk_works:I = 0x7f07033e

.field public static final bgm_good:I = 0x7f0702ab

.field public static final bgm_goody:I = 0x7f07033c

.field public static final bgm_in_the_clu:I = 0x7f07029a

.field public static final bgm_in_the_club:I = 0x7f07032c

.field public static final bgm_little_jo:I = 0x7f0702a6

.field public static final bgm_little_joy:I = 0x7f0702f3

.field public static final bgm_mild_nov:I = 0x7f0702a3

.field public static final bgm_mild_nova:I = 0x7f070335

.field public static final bgm_off:I = 0x7f07026c

.field public static final bgm_on:I = 0x7f07026b

.field public static final bgm_rai:I = 0x7f0702a0

.field public static final bgm_rain:I = 0x7f070332

.field public static final bgm_ru:I = 0x7f07029c

.field public static final bgm_run:I = 0x7f07032e

.field public static final bgm_selection_activiy_title:I = 0x7f07000b

.field public static final bgm_sights_in_the_cit:I = 0x7f07029e

.field public static final bgm_sights_in_the_city:I = 0x7f070330

.field public static final bgm_sound:I = 0x7f070303

.field public static final bgm_special_da:I = 0x7f070299

.field public static final bgm_special_day:I = 0x7f07032b

.field public static final bgm_tabl:I = 0x7f0702af

.field public static final bgm_table:I = 0x7f070340

.field public static final bgm_tonigh:I = 0x7f07029b

.field public static final bgm_tonight:I = 0x7f07032d

.field public static final bgm_vacatio:I = 0x7f070298

.field public static final bgm_vacation:I = 0x7f07032a

.field public static final bgm_vintag:I = 0x7f0702a7

.field public static final bgm_vintage:I = 0x7f070338

.field public static final bgm_whistle_your_care:I = 0x7f0702a9

.field public static final bgm_whistle_your_cares:I = 0x7f07033a

.field public static final bgm_young_ja:I = 0x7f0702ae

.field public static final bgm_young_jay:I = 0x7f07033f

.field public static final calm:I = 0x7f07026a

.field public static final cancel:I = 0x7f07005e

.field public static final cancel_auto_edit:I = 0x7f07004b

.field public static final cancel_button:I = 0x7f070060

.field public static final cancel_capital:I = 0x7f07005f

.field public static final cancel_edit_message:I = 0x7f070250

.field public static final cannot_be_re_edited:I = 0x7f070061

.field public static final cannot_edit:I = 0x7f070062

.field public static final cannot_edit_summary:I = 0x7f0702fa

.field public static final cannot_export:I = 0x7f0701f2

.field public static final change_media_part_fir:I = 0x7f0701a4

.field public static final change_media_part_sec:I = 0x7f0701a5

.field public static final comic:I = 0x7f070064

.field public static final comics:I = 0x7f070065

.field public static final credit_home:I = 0x7f070173

.field public static final credit_home_credits:I = 0x7f070175

.field public static final credit_home_defaultfilm:I = 0x7f070178

.field public static final credit_home_defaultfilm_eg:I = 0x7f070179

.field public static final credit_home_director:I = 0x7f070176

.field public static final credit_home_director_eg:I = 0x7f070177

.field public static final credit_home_maincast:I = 0x7f07017a

.field public static final credit_home_maincast_eg:I = 0x7f07017b

.field public static final credit_home_supportingactor:I = 0x7f07017c

.field public static final credit_home_supportingactor_eg:I = 0x7f07017d

.field public static final credit_home_title:I = 0x7f070174

.field public static final credit_present:I = 0x7f070123

.field public static final credit_romance:I = 0x7f070116

.field public static final credit_romance_credits:I = 0x7f070118

.field public static final credit_romance_defaultfilm:I = 0x7f07011b

.field public static final credit_romance_defaultfilm_eg:I = 0x7f07011c

.field public static final credit_romance_director:I = 0x7f070119

.field public static final credit_romance_director_eg:I = 0x7f07011a

.field public static final credit_romance_maincast:I = 0x7f07011d

.field public static final credit_romance_maincast_eg:I = 0x7f07011e

.field public static final credit_romance_supportingactor:I = 0x7f07011f

.field public static final credit_romance_supportingactor_eg:I = 0x7f070120

.field public static final credit_romance_title:I = 0x7f070117

.field public static final credit_romance_venue:I = 0x7f070121

.field public static final credit_romance_venue_eg:I = 0x7f070122

.field public static final curation_event_projects:I = 0x7f070230

.field public static final curation_event_projects_expln:I = 0x7f070231

.field public static final curation_home_city:I = 0x7f07022d

.field public static final curation_home_city_expln:I = 0x7f07022e

.field public static final curation_home_switch_on_alert:I = 0x7f07022f

.field public static final curation_settings:I = 0x7f07022c

.field public static final curation_travel_projects:I = 0x7f070232

.field public static final curation_travel_projects_expln:I = 0x7f070233

.field public static final customize_video:I = 0x7f070305

.field public static final daytoday:I = 0x7f070066

.field public static final deleteProject:I = 0x7f070038

.field public static final deleting_popup:I = 0x7f070281

.field public static final detailed_mode_not_supported:I = 0x7f070308

.field public static final device_storage_full_title:I = 0x7f070283

.field public static final dialog_project_delete:I = 0x7f070068

.field public static final dialog_share_title:I = 0x7f07006a

.field public static final discard_bgm_edit_msg:I = 0x7f07021e

.field public static final discard_bgm_edit_title:I = 0x7f07021d

.field public static final discard_video:I = 0x7f07021b

.field public static final discard_video_msg:I = 0x7f07021c

.field public static final do_not_show:I = 0x7f07006b

.field public static final done:I = 0x7f07006e

.field public static final done_button:I = 0x7f070073

.field public static final done_capital:I = 0x7f07006f

.field public static final download_theme:I = 0x7f0701a1

.field public static final download_theme_btn_text:I = 0x7f0701a2

.field public static final download_theme_text:I = 0x7f0702b5

.field public static final download_vefull:I = 0x7f0702ef

.field public static final drawing:I = 0x7f070075

.field public static final duration:I = 0x7f0701fc

.field public static final echo:I = 0x7f070076

.field public static final edit:I = 0x7f070077

.field public static final edit_caption_text:I = 0x7f0701e9

.field public static final edit_media:I = 0x7f0702dc

.field public static final editmode_tip:I = 0x7f0701fb

.field public static final effect_cartoon:I = 0x7f070288

.field public static final effect_faded_colors:I = 0x7f070289

.field public static final effect_grayscale:I = 0x7f070285

.field public static final effect_mirror:I = 0x7f070292

.field public static final effect_mosaic:I = 0x7f07028e

.field public static final effect_oil_painting:I = 0x7f07028d

.field public static final effect_old_film:I = 0x7f070293

.field public static final effect_rugged:I = 0x7f070290

.field public static final effect_sepia:I = 0x7f070286

.field public static final effect_shining:I = 0x7f07028a

.field public static final effect_simple:I = 0x7f070284

.field public static final effect_sound:I = 0x7f070078

.field public static final effect_thermal:I = 0x7f070291

.field public static final effect_tint:I = 0x7f07028f

.field public static final effect_turquoise:I = 0x7f07028b

.field public static final effect_vignette:I = 0x7f07028c

.field public static final effect_vintage:I = 0x7f070287

.field public static final eraser:I = 0x7f070079

.field public static final eraser_settings:I = 0x7f07007a

.field public static final event:I = 0x7f070219

.field public static final event_projects_text:I = 0x7f070246

.field public static final event_projects_text1:I = 0x7f070239

.field public static final event_projects_text2:I = 0x7f07023a

.field public static final event_projects_text3:I = 0x7f07023b

.field public static final event_projects_text4:I = 0x7f07023c

.field public static final exciting:I = 0x7f070269

.field public static final export_but:I = 0x7f070317

.field public static final export_dialog_title:I = 0x7f070316

.field public static final export_download_SDcard_popup_message:I = 0x7f0702f9

.field public static final export_download_SDcard_popup_title:I = 0x7f0702f8

.field public static final export_enter_name:I = 0x7f07007d

.field public static final export_file_name:I = 0x7f07007e

.field public static final export_message_paused:I = 0x7f070319

.field public static final export_paused:I = 0x7f07007f

.field public static final export_play:I = 0x7f07031c

.field public static final export_resolution_title:I = 0x7f07007c

.field public static final export_save_loc:I = 0x7f070243

.field public static final export_settings_blurriness:I = 0x7f0701e1

.field public static final export_settings_resolution:I = 0x7f0701a6

.field public static final export_settings_stabilization:I = 0x7f0701ec

.field public static final export_settings_title:I = 0x7f0701de

.field public static final exporting:I = 0x7f070318

.field public static final exporting_movie:I = 0x7f07031f

.field public static final file_exists:I = 0x7f070080

.field public static final file_name_enter:I = 0x7f070081

.field public static final file_name_exist:I = 0x7f070082

.field public static final genres:I = 0x7f070083

.field public static final glance_recom_event_proj_subtitle:I = 0x7f070227

.field public static final glance_recom_event_proj_title:I = 0x7f070225

.field public static final glance_recom_proj_title2:I = 0x7f070224

.field public static final glance_recom_travel_proj_subtitle:I = 0x7f070228

.field public static final glance_recom_travel_proj_title:I = 0x7f070226

.field public static final gps_off_message:I = 0x7f070235

.field public static final guest_mode_popup:I = 0x7f070282

.field public static final home_and_emotions:I = 0x7f070172

.field public static final home_but_prepare:I = 0x7f070167

.field public static final home_city_pop_up_message:I = 0x7f070237

.field public static final home_family:I = 0x7f070166

.field public static final home_family_comedy:I = 0x7f07016e

.field public static final home_family_fun:I = 0x7f070169

.field public static final home_key:I = 0x7f070084

.field public static final home_laughter:I = 0x7f070171

.field public static final home_theme_desciption:I = 0x7f0700a9

.field public static final home_this_year:I = 0x7f07016f

.field public static final home_we_are_family:I = 0x7f070163

.field public static final home_we_give_you:I = 0x7f070170

.field public static final home_we_look_like:I = 0x7f070164

.field public static final home_will_ache:I = 0x7f07016c

.field public static final home_with_emotion:I = 0x7f07016d

.field public static final home_youll_laugh_till_you_cry:I = 0x7f07016a

.field public static final home_your_average:I = 0x7f070165

.field public static final home_your_heart:I = 0x7f07016b

.field public static final home_yourself_for_some:I = 0x7f070168

.field public static final hq_acoustic_guitar_slide:I = 0x7f0701a8

.field public static final hq_acoustic_guitar_strumming:I = 0x7f0701a9

.field public static final hq_add_text:I = 0x7f0701f6

.field public static final hq_applause:I = 0x7f0701aa

.field public static final hq_arrow_hitting_target:I = 0x7f0701ab

.field public static final hq_audio_effect:I = 0x7f070248

.field public static final hq_auto_edit_title:I = 0x7f0702fc

.field public static final hq_automobile_horn:I = 0x7f0701b4

.field public static final hq_automobile_horn_2:I = 0x7f0701b3

.field public static final hq_bee_sting:I = 0x7f0701ac

.field public static final hq_bicycle_bell:I = 0x7f0701ad

.field public static final hq_bongos:I = 0x7f0701ae

.field public static final hq_brass_section:I = 0x7f0701af

.field public static final hq_car_driving_past:I = 0x7f0701b2

.field public static final hq_cartoon_punch:I = 0x7f0701b0

.field public static final hq_cartoon_swing:I = 0x7f0701b1

.field public static final hq_children_laughing:I = 0x7f0701b5

.field public static final hq_crowd_booing:I = 0x7f0701b7

.field public static final hq_crowd_cheering:I = 0x7f0701b8

.field public static final hq_crowd_laughing:I = 0x7f0701b9

.field public static final hq_cuckoo_clock:I = 0x7f0701ba

.field public static final hq_door_bell:I = 0x7f0701bb

.field public static final hq_door_open:I = 0x7f0701bc

.field public static final hq_download_add_on:I = 0x7f07030a

.field public static final hq_download_ve:I = 0x7f070328

.field public static final hq_drum_fill_in:I = 0x7f0701bd

.field public static final hq_electric_guitar_wah:I = 0x7f0701bf

.field public static final hq_electrical_spark:I = 0x7f0701be

.field public static final hq_engine_start:I = 0x7f0701c0

.field public static final hq_explosion:I = 0x7f0701c1

.field public static final hq_female_laugh:I = 0x7f0701c2

.field public static final hq_fireworks:I = 0x7f0701c3

.field public static final hq_foot_steps:I = 0x7f0701c4

.field public static final hq_harp_glissando:I = 0x7f0701c5

.field public static final hq_hide:I = 0x7f0701d9

.field public static final hq_jet_overhead:I = 0x7f0701c6

.field public static final hq_jump_rocket:I = 0x7f0701c7

.field public static final hq_laser_gun:I = 0x7f0701c8

.field public static final hq_lure_out:I = 0x7f0701cc

.field public static final hq_motorcycle_driving_past:I = 0x7f0701c9

.field public static final hq_my_music:I = 0x7f0702f6

.field public static final hq_no_project:I = 0x7f070329

.field public static final hq_not_enough_free_disk:I = 0x7f0701f4

.field public static final hq_not_enough_free_disk_export:I = 0x7f0702b9

.field public static final hq_not_support_detail:I = 0x7f07030e

.field public static final hq_noti_over_time_n:I = 0x7f07024f

.field public static final hq_ocean_waves:I = 0x7f0701ca

.field public static final hq_party_theme:I = 0x7f0701f5

.field public static final hq_piano_ending:I = 0x7f0701cb

.field public static final hq_pole_jump:I = 0x7f0701b6

.field public static final hq_project_not_export:I = 0x7f070310

.field public static final hq_project_not_save:I = 0x7f070309

.field public static final hq_project_save:I = 0x7f070321

.field public static final hq_recorded_files:I = 0x7f0701ef

.field public static final hq_ringing_phone:I = 0x7f0701cd

.field public static final hq_samsung_apps:I = 0x7f0701f1

.field public static final hq_ship_horn:I = 0x7f0701ce

.field public static final hq_skidding_car:I = 0x7f0701d0

.field public static final hq_slide:I = 0x7f0701d1

.field public static final hq_slot_machine:I = 0x7f0701d2

.field public static final hq_speed:I = 0x7f070342

.field public static final hq_squeaky_toy:I = 0x7f0701d3

.field public static final hq_storyboard_time_limit:I = 0x7f0701f3

.field public static final hq_sword_fight:I = 0x7f0701d4

.field public static final hq_take_a_snooze:I = 0x7f0701cf

.field public static final hq_template:I = 0x7f070307

.field public static final hq_text_effect1:I = 0x7f07024b

.field public static final hq_text_effect2:I = 0x7f070249

.field public static final hq_thunderclap:I = 0x7f0701d5

.field public static final hq_visual_effect1:I = 0x7f07024c

.field public static final hq_visual_effect2:I = 0x7f07024a

.field public static final hq_volume_control:I = 0x7f07024d

.field public static final hq_waring_auto_edit:I = 0x7f0701dd

.field public static final hq_water_drop:I = 0x7f0701d6

.field public static final hq_whistle:I = 0x7f0701d7

.field public static final hq_windy_mountain:I = 0x7f0701d8

.field public static final image_preview:I = 0x7f070086

.field public static final image_preview_select:I = 0x7f070071

.field public static final images:I = 0x7f070087

.field public static final inProgress_notif_text:I = 0x7f070252

.field public static final input_elements_count_reached:I = 0x7f07020f

.field public static final input_limitation_msg:I = 0x7f07020a

.field public static final invalid:I = 0x7f070301

.field public static final invalid_character:I = 0x7f070088

.field public static final joyful:I = 0x7f070268

.field public static final just_once:I = 0x7f07025f

.field public static final k_effect:I = 0x7f0702be

.field public static final kenburn:I = 0x7f070089

.field public static final kenburn_begin:I = 0x7f07008c

.field public static final kenburn_end:I = 0x7f07008d

.field public static final kenburn_loading:I = 0x7f07008a

.field public static final kenburns_not_supported:I = 0x7f07008b

.field public static final launch_video_editor:I = 0x7f070294

.field public static final launch_video_editor_detail:I = 0x7f070295

.field public static final launchcamera:I = 0x7f07008e

.field public static final license_text:I = 0x7f0702b4

.field public static final lite_custom:I = 0x7f07025b

.field public static final lite_export_save_loc:I = 0x7f070244

.field public static final lite_high:I = 0x7f07025c

.field public static final lite_long:I = 0x7f07025a

.field public static final lite_low:I = 0x7f070256

.field public static final lite_medium:I = 0x7f070258

.field public static final lite_medium_resolution:I = 0x7f070257

.field public static final lite_save_changes_dialog_desc:I = 0x7f07021f

.field public static final lite_save_changes_dialog_neutral:I = 0x7f070221

.field public static final lite_save_changes_dialog_positive:I = 0x7f070222

.field public static final lite_save_changes_dialog_title:I = 0x7f070220

.field public static final lite_short:I = 0x7f070259

.field public static final lite_unsupported_dialog_msg:I = 0x7f070262

.field public static final lite_unsupported_dialog_msg_2:I = 0x7f070263

.field public static final lite_unsupported_dialog_msg_3:I = 0x7f070264

.field public static final lite_unsupported_dialog_title:I = 0x7f070265

.field public static final lowstorage:I = 0x7f070209

.field public static final media:I = 0x7f07008f

.field public static final media_scanner_running:I = 0x7f070090

.field public static final menu_auto_making:I = 0x7f070091

.field public static final menu_cancel:I = 0x7f070092

.field public static final menu_done:I = 0x7f070095

.field public static final menu_edit_bgm_cancel:I = 0x7f070093

.field public static final menu_edit_bgm_done:I = 0x7f070094

.field public static final menu_export_movie:I = 0x7f070320

.field public static final menu_rename:I = 0x7f070096

.field public static final menu_save:I = 0x7f070097

.field public static final menu_title_add_media:I = 0x7f0702ff

.field public static final menu_title_edit:I = 0x7f070098

.field public static final menu_title_editmedia:I = 0x7f070099

.field public static final min_clips_needed:I = 0x7f070253

.field public static final mins_secs:I = 0x7f07009a

.field public static final minutes:I = 0x7f0701fd

.field public static final monste:I = 0x7f07009b

.field public static final multi_delete_desc:I = 0x7f070069

.field public static final music_option_music_files:I = 0x7f07005c

.field public static final musical:I = 0x7f07009c

.field public static final my_music_text_album_list:I = 0x7f070302

.field public static final ne_autumn_leaf:I = 0x7f070322

.field public static final ne_fog:I = 0x7f0702df

.field public static final ne_rainfall:I = 0x7f070324

.field public static final ne_snowfall:I = 0x7f070325

.field public static final ne_spring_petal:I = 0x7f070323

.field public static final never_show:I = 0x7f07009d

.field public static final new_project:I = 0x7f07009e

.field public static final new_project_description:I = 0x7f0701e0

.field public static final new_theme_home:I = 0x7f0701da

.field public static final new_video:I = 0x7f07020b

.field public static final next:I = 0x7f07009f

.field public static final no:I = 0x7f0700a0

.field public static final no_export_changes:I = 0x7f070315

.field public static final no_music:I = 0x7f0700a1

.field public static final no_of_proj_selected:I = 0x7f070036

.field public static final no_of_project_selected_for_delete:I = 0x7f070035

.field public static final no_room_in_storyboard:I = 0x7f0700a2

.field public static final no_sd_card:I = 0x7f0702bc

.field public static final none:I = 0x7f07030f

.field public static final none_desciption:I = 0x7f0700a3

.field public static final not_enough_memory_for_bg_exporting:I = 0x7f0702bd

.field public static final not_enough_storage:I = 0x7f0702ba

.field public static final notifications:I = 0x7f070241

.field public static final notifications_hint:I = 0x7f070242

.field public static final off:I = 0x7f0702c1

.field public static final ok:I = 0x7f0700ab

.field public static final on:I = 0x7f0702c0

.field public static final one_sugg_proj:I = 0x7f070211

.field public static final open:I = 0x7f070070

.field public static final open_source_license_text:I = 0x7f0702b1

.field public static final others:I = 0x7f07022b

.field public static final outing_a_time:I = 0x7f070187

.field public static final outing_but_the:I = 0x7f07018a

.field public static final outing_credit:I = 0x7f07018c

.field public static final outing_credit_credits:I = 0x7f07018e

.field public static final outing_credit_default_film:I = 0x7f070191

.field public static final outing_credit_default_film_eg:I = 0x7f070192

.field public static final outing_credit_director:I = 0x7f07018f

.field public static final outing_credit_director_eg:I = 0x7f070190

.field public static final outing_credit_main_cast:I = 0x7f070193

.field public static final outing_credit_main_cast_eg:I = 0x7f070194

.field public static final outing_credit_supporting_actor:I = 0x7f070195

.field public static final outing_credit_supporting_actor_eg:I = 0x7f070196

.field public static final outing_credit_title:I = 0x7f07018d

.field public static final outing_for_people:I = 0x7f070182

.field public static final outing_greatest:I = 0x7f070180

.field public static final outing_last:I = 0x7f07018b

.field public static final outing_let_the:I = 0x7f070184

.field public static final outing_never_forget:I = 0x7f070188

.field public static final outing_party:I = 0x7f070181

.field public static final outing_party_begin:I = 0x7f070185

.field public static final outing_party_with:I = 0x7f070186

.field public static final outing_the:I = 0x7f07017f

.field public static final outing_the_party_may:I = 0x7f070189

.field public static final outing_theme:I = 0x7f07017e

.field public static final outing_theme_desciption:I = 0x7f0700aa

.field public static final outing_theme_name_for_zero:I = 0x7f070312

.field public static final outing_tired_of:I = 0x7f070183

.field public static final overlay_text_content:I = 0x7f07006c

.field public static final overlay_text_content_landscape:I = 0x7f07006d

.field public static final override_project:I = 0x7f0700ac

.field public static final override_project_msg:I = 0x7f0700ad

.field public static final pan_and_zoom:I = 0x7f0702bf

.field public static final passionate:I = 0x7f070267

.field public static final pause:I = 0x7f0700ae

.field public static final pen:I = 0x7f0700a4

.field public static final pen_button:I = 0x7f0701f9

.field public static final pen_settings:I = 0x7f0700a5

.field public static final penmode:I = 0x7f0701f8

.field public static final percent_mb:I = 0x7f0702b6

.field public static final percentage_complete:I = 0x7f0701e7

.field public static final photo:I = 0x7f0700af

.field public static final play:I = 0x7f0700b0

.field public static final play_speed:I = 0x7f0702da

.field public static final playlists:I = 0x7f0700b1

.field public static final preload_editor_pro:I = 0x7f0702f1

.field public static final preload_trim:I = 0x7f0702f2

.field public static final preview:I = 0x7f0700b2

.field public static final prj_recieve_success:I = 0x7f0702c7

.field public static final prj_sharing_approve_msg:I = 0x7f0702cf

.field public static final prj_sharing_approve_msg_null_case:I = 0x7f0702d0

.field public static final prj_sharing_approve_title:I = 0x7f0702ce

.field public static final prj_sharing_busy:I = 0x7f0702ca

.field public static final prj_sharing_connect_msg:I = 0x7f0702d8

.field public static final prj_sharing_connect_msg_china:I = 0x7f0702d9

.field public static final prj_sharing_device_list_title:I = 0x7f0702d1

.field public static final prj_sharing_error:I = 0x7f0702c9

.field public static final prj_sharing_failed_sender:I = 0x7f0702c3

.field public static final prj_sharing_import_failed:I = 0x7f0702c8

.field public static final prj_sharing_menu:I = 0x7f0702cb

.field public static final prj_sharing_no_nearby:I = 0x7f0702cd

.field public static final prj_sharing_no_wifi:I = 0x7f0702cc

.field public static final prj_sharing_receiver_noti_msg:I = 0x7f0702d2

.field public static final prj_sharing_receiver_progress_msg:I = 0x7f0702d6

.field public static final prj_sharing_receiver_progress_title:I = 0x7f0702d7

.field public static final prj_sharing_sender_noti_msg:I = 0x7f0702d4

.field public static final prj_sharing_sender_noti_title:I = 0x7f0702d3

.field public static final prj_sharing_sender_progress_msg:I = 0x7f0702d5

.field public static final prj_sharing_success:I = 0x7f0702c6

.field public static final prj_sharing_timeout:I = 0x7f0702c4

.field public static final prj_sharing_transfer_failed:I = 0x7f0702c5

.field public static final projct_deleted:I = 0x7f0700b3

.field public static final project_count:I = 0x7f0702ee

.field public static final project_name:I = 0x7f0700b4

.field public static final project_name_exists:I = 0x7f07020d

.field public static final project_name_null:I = 0x7f07020c

.field public static final project_remove:I = 0x7f0700b5

.field public static final project_select_all:I = 0x7f070327

.field public static final reach_max_contents:I = 0x7f070255

.field public static final recom_empty_info_text1:I = 0x7f070215

.field public static final recom_empty_info_text2:I = 0x7f070214

.field public static final recom_new:I = 0x7f070251

.field public static final recom_proj_loading:I = 0x7f070229

.field public static final recom_proj_notif_launch:I = 0x7f070218

.field public static final recom_proj_title:I = 0x7f070213

.field public static final record:I = 0x7f0700b6

.field public static final record_failure:I = 0x7f0700b7

.field public static final record_narration:I = 0x7f0700b8

.field public static final record_popup_unable_to_ddMedia:I = 0x7f0700b9

.field public static final record_sound:I = 0x7f0700ba

.field public static final record_video:I = 0x7f0700bb

.field public static final record_warning_checkbox_text:I = 0x7f0700bc

.field public static final record_warning_message:I = 0x7f0700bd

.field public static final recording_exceeds_limit:I = 0x7f0700be

.field public static final redo:I = 0x7f0700bf

.field public static final redo_all:I = 0x7f0700c1

.field public static final redo_all_message:I = 0x7f0700c2

.field public static final redo_button:I = 0x7f0700c0

.field public static final remove:I = 0x7f0700c3

.field public static final remove_extra_clips:I = 0x7f070254

.field public static final rename_cancel:I = 0x7f0700c4

.field public static final rename_msg:I = 0x7f0700c5

.field public static final rename_ok:I = 0x7f0700c6

.field public static final resume:I = 0x7f0700c7

.field public static final retouch_blackwhite:I = 0x7f0700c8

.field public static final retouch_blur:I = 0x7f0700c9

.field public static final retouch_cartoon:I = 0x7f0700ca

.field public static final retouch_kenburn:I = 0x7f0700cb

.field public static final retouch_negative:I = 0x7f0700cc

.field public static final retouch_original:I = 0x7f0700d0

.field public static final retouch_sepia:I = 0x7f0700cd

.field public static final retouch_sharpen:I = 0x7f0700ce

.field public static final retouch_vintage:I = 0x7f0700cf

.field public static final romance_beautiful_lovestory:I = 0x7f070114

.field public static final romance_between:I = 0x7f07010c

.field public static final romance_blossoming_love:I = 0x7f070111

.field public static final romance_by_these2:I = 0x7f070115

.field public static final romance_destined_encounter:I = 0x7f07010b

.field public static final romance_discovery:I = 0x7f070112

.field public static final romance_hearts_racing:I = 0x7f070110

.field public static final romance_led_to:I = 0x7f07010e

.field public static final romance_love_story:I = 0x7f07010a

.field public static final romance_magical_story:I = 0x7f07010f

.field public static final romance_theme_desciption:I = 0x7f0700a6

.field public static final romance_true_happiness:I = 0x7f070113

.field public static final romance_two_ppl:I = 0x7f07010d

.field public static final rotate_left:I = 0x7f0702dd

.field public static final rotate_right:I = 0x7f0702de

.field public static final sample_video_text:I = 0x7f0701f7

.field public static final samsung_apps_disabled_pop_up_message:I = 0x7f070297

.field public static final save:I = 0x7f070074

.field public static final save_as:I = 0x7f07025e

.field public static final save_close:I = 0x7f070311

.field public static final save_ing:I = 0x7f070326

.field public static final save_options:I = 0x7f07025d

.field public static final save_project:I = 0x7f0700d1

.field public static final savesubmenu:I = 0x7f0700d2

.field public static final sd_shared_as_usb:I = 0x7f0700d4

.field public static final seconds:I = 0x7f0701fe

.field public static final secs:I = 0x7f0700d5

.field public static final seek_control:I = 0x7f0700d3

.field public static final select_all_project:I = 0x7f070033

.field public static final select_mode_launher_screen_text:I = 0x7f070072

.field public static final select_theme:I = 0x7f070300

.field public static final set_home_city:I = 0x7f070236

.field public static final settings:I = 0x7f07021a

.field public static final share_via_do_not_show:I = 0x7f07031e

.field public static final slot_selection:I = 0x7f0701df

.field public static final slow_motion:I = 0x7f0702db

.field public static final slow_motion_normal:I = 0x7f0702e6

.field public static final sound:I = 0x7f0700d6

.field public static final sound_effect:I = 0x7f0702eb

.field public static final sound_off:I = 0x7f07026e

.field public static final sound_on:I = 0x7f07026d

.field public static final split_button:I = 0x7f0701ed

.field public static final start_time:I = 0x7f0700d7

.field public static final start_time_s:I = 0x7f0700d8

.field public static final start_time_short:I = 0x7f0700d9

.field public static final stms_appgroup:I = 0x7f0702e7

.field public static final stop:I = 0x7f070208

.field public static final storage_full_dialog_desc:I = 0x7f07007b

.field public static final storage_low:I = 0x7f0700da

.field public static final story_board_full:I = 0x7f0700db

.field public static final story_board_less:I = 0x7f070217

.field public static final story_cam:I = 0x7f070216

.field public static final sugg_projects:I = 0x7f070212

.field public static final suggest_content_count:I = 0x7f0702ea

.field public static final suggest_event_text:I = 0x7f0702e9

.field public static final suggest_travel_text:I = 0x7f0702e8

.field public static final suggestions:I = 0x7f07022a

.field public static final suggestions_event_projects:I = 0x7f070238

.field public static final suggestions_travel_projects:I = 0x7f07023d

.field public static final swipe_two_finger:I = 0x7f0701e8

.field public static final take_picture:I = 0x7f0700dc

.field public static final tap_add_text:I = 0x7f0701ea

.field public static final tap_to_add:I = 0x7f0701e3

.field public static final tap_to_apply:I = 0x7f0701e4

.field public static final tap_to_play:I = 0x7f0701e5

.field public static final text_effect:I = 0x7f070304

.field public static final text_effect_text:I = 0x7f0701ee

.field public static final text_title:I = 0x7f070085

.field public static final theme_action:I = 0x7f0700dd

.field public static final theme_autoedit_msg:I = 0x7f070063

.field public static final theme_blackNwhite:I = 0x7f070201

.field public static final theme_blackNwhite_description:I = 0x7f070202

.field public static final theme_conference:I = 0x7f0700de

.field public static final theme_download_alert:I = 0x7f0701a3

.field public static final theme_family:I = 0x7f0701db

.field public static final theme_home:I = 0x7f0700e6

.field public static final theme_homevideo:I = 0x7f0700df

.field public static final theme_jazz:I = 0x7f070203

.field public static final theme_mode_toast:I = 0x7f0701e2

.field public static final theme_movie:I = 0x7f0700e0

.field public static final theme_none:I = 0x7f0700e1

.field public static final theme_normal:I = 0x7f070207

.field public static final theme_outing:I = 0x7f0700e7

.field public static final theme_party:I = 0x7f0700e2

.field public static final theme_romance:I = 0x7f0700e3

.field public static final theme_selection_menu_next:I = 0x7f070067

.field public static final theme_simple:I = 0x7f070204

.field public static final theme_soft:I = 0x7f070205

.field public static final theme_stage:I = 0x7f0700e4

.field public static final theme_text_tap_to_edit:I = 0x7f070266

.field public static final theme_travel:I = 0x7f0700e5

.field public static final theme_travelogue:I = 0x7f0701dc

.field public static final theme_video_length:I = 0x7f07026f

.field public static final theme_video_length_message:I = 0x7f070270

.field public static final theme_vintage:I = 0x7f070206

.field public static final time:I = 0x7f0700e8

.field public static final title_file_exported:I = 0x7f0700ed

.field public static final title_proj_del:I = 0x7f0700e9

.field public static final title_record:I = 0x7f0700ea

.field public static final title_share_video:I = 0x7f0701e6

.field public static final title_thumbnail_edit:I = 0x7f0700eb

.field public static final toast_all_not_supported:I = 0x7f0700ec

.field public static final toast_cancel_export:I = 0x7f07031d

.field public static final toast_change_alltransitions:I = 0x7f0700ee

.field public static final toast_change_alltransitions_bars:I = 0x7f07001f

.field public static final toast_change_alltransitions_black:I = 0x7f070020

.field public static final toast_change_alltransitions_block:I = 0x7f070021

.field public static final toast_change_alltransitions_cubeturn_a:I = 0x7f070022

.field public static final toast_change_alltransitions_cubeturn_b:I = 0x7f070023

.field public static final toast_change_alltransitions_dissolve:I = 0x7f070024

.field public static final toast_change_alltransitions_fade:I = 0x7f070025

.field public static final toast_change_alltransitions_fadeinout:I = 0x7f070026

.field public static final toast_change_alltransitions_fir:I = 0x7f0702e1

.field public static final toast_change_alltransitions_none:I = 0x7f070027

.field public static final toast_change_alltransitions_origami:I = 0x7f070028

.field public static final toast_change_alltransitions_pixelise:I = 0x7f070029

.field public static final toast_change_alltransitions_sec:I = 0x7f0702e2

.field public static final toast_change_alltransitions_slide_left:I = 0x7f07002a

.field public static final toast_change_alltransitions_slide_top:I = 0x7f07002b

.field public static final toast_change_alltransitions_swap:I = 0x7f07002c

.field public static final toast_change_alltransitions_tiles:I = 0x7f07002d

.field public static final toast_change_alltransitions_title:I = 0x7f0702e0

.field public static final toast_change_alltransitions_wheel:I = 0x7f07002e

.field public static final toast_change_alltransitions_white:I = 0x7f07002f

.field public static final toast_change_alltransitions_wiperight:I = 0x7f070030

.field public static final toast_change_transitions_body:I = 0x7f070032

.field public static final toast_change_transitions_title:I = 0x7f070031

.field public static final toast_element_failed:I = 0x7f0700ef

.field public static final toast_fail_export:I = 0x7f0700f0

.field public static final toast_fail_export_lite:I = 0x7f07031b

.field public static final toast_file_not_found:I = 0x7f0700f1

.field public static final toast_loading:I = 0x7f0700f2

.field public static final toast_max_characters:I = 0x7f0700f3

.field public static final toast_max_characters2:I = 0x7f0700f4

.field public static final toast_max_count_characters:I = 0x7f0700f5

.field public static final toast_not_supported:I = 0x7f0700f6

.field public static final toast_project_notfound:I = 0x7f0700f7

.field public static final toast_record_done:I = 0x7f0700f8

.field public static final toast_theme_full_slot:I = 0x7f0702e5

.field public static final toast_unable_play:I = 0x7f0700f9

.field public static final toast_unable_play_effects:I = 0x7f0700fa

.field public static final toast_unable_record:I = 0x7f0700fb

.field public static final toast_video_too_short:I = 0x7f070109

.field public static final toast_videoclip_less_duration_editMode:I = 0x7f070296

.field public static final today:I = 0x7f0702e4

.field public static final transition:I = 0x7f0700fc

.field public static final transition_pop_up_change_button:I = 0x7f07001a

.field public static final travel:I = 0x7f0700fd

.field public static final travel_a_new_world:I = 0x7f07012b

.field public static final travel_arrive_nervous:I = 0x7f070127

.field public static final travel_but_excited:I = 0x7f070128

.field public static final travel_but_i_still:I = 0x7f070131

.field public static final travel_credit:I = 0x7f070133

.field public static final travel_credit_credits:I = 0x7f070135

.field public static final travel_credit_default_film:I = 0x7f070138

.field public static final travel_credit_default_film_eg:I = 0x7f070139

.field public static final travel_credit_director:I = 0x7f070136

.field public static final travel_credit_director_eg:I = 0x7f070137

.field public static final travel_credit_main_cast:I = 0x7f07013a

.field public static final travel_credit_main_cast_eg:I = 0x7f07013b

.field public static final travel_credit_supporting_actor:I = 0x7f07013c

.field public static final travel_credit_supporting_actor_eg:I = 0x7f07013d

.field public static final travel_credit_title:I = 0x7f070134

.field public static final travel_credit_venue:I = 0x7f07013e

.field public static final travel_credit_venue_eg:I = 0x7f07013f

.field public static final travel_first_step:I = 0x7f070125

.field public static final travel_have_memories:I = 0x7f070132

.field public static final travel_is_done:I = 0x7f070130

.field public static final travel_life:I = 0x7f07012c

.field public static final travel_moments:I = 0x7f07012e

.field public static final travel_most_precious:I = 0x7f07012d

.field public static final travel_my_journey:I = 0x7f07012f

.field public static final travel_new_exp:I = 0x7f07012a

.field public static final travel_projects_text:I = 0x7f070247

.field public static final travel_projects_text1:I = 0x7f07023e

.field public static final travel_projects_text2:I = 0x7f07023f

.field public static final travel_projects_text3:I = 0x7f070240

.field public static final travel_self_discovery:I = 0x7f070126

.field public static final travel_theme_desciption:I = 0x7f0700a7

.field public static final travel_to_new_world:I = 0x7f070124

.field public static final travel_whole:I = 0x7f070129

.field public static final type_audio:I = 0x7f0702f5

.field public static final type_image:I = 0x7f0702ed

.field public static final type_video:I = 0x7f0702f4

.field public static final unable_create_video_clip:I = 0x7f070210

.field public static final unable_to_export:I = 0x7f0702bb

.field public static final undo:I = 0x7f0700fe

.field public static final undo_all:I = 0x7f070100

.field public static final undo_all_message:I = 0x7f070101

.field public static final undo_button:I = 0x7f0700ff

.field public static final unselect_all_project:I = 0x7f070034

.field public static final untitled:I = 0x7f070102

.field public static final ve_export_done:I = 0x7f07031a

.field public static final ve_lite_export_done:I = 0x7f070245

.field public static final ve_lite_unable_to_change_theme:I = 0x7f07001b

.field public static final ve_lite_unable_to_change_theme_general:I = 0x7f07001c

.field public static final version:I = 0x7f0702b3

.field public static final video:I = 0x7f070103

.field public static final video_clip_title:I = 0x7f070223

.field public static final video_editor:I = 0x7f070104

.field public static final video_exported_path:I = 0x7f070314

.field public static final video_sound:I = 0x7f070105

.field public static final video_sound_volume_change_text:I = 0x7f0702f0

.field public static final videos:I = 0x7f070106

.field public static final volume:I = 0x7f070107

.field public static final yes:I = 0x7f070108

.field public static final yesterday:I = 0x7f0702e3

.field public static final youtube_error:I = 0x7f0701a7

.field public static final youtube_warning:I = 0x7f0701eb


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1510
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
