.class Lcom/sec/android/app/storycam/view/Caption1Line$7;
.super Ljava/lang/Object;
.source "Caption1Line.java"

# interfaces
.implements Lcom/sec/android/app/storycam/AppDataManager$TranscodeElementChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/storycam/view/Caption1Line;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/storycam/view/Caption1Line;


# direct methods
.method constructor <init>(Lcom/sec/android/app/storycam/view/Caption1Line;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/storycam/view/Caption1Line$7;->this$0:Lcom/sec/android/app/storycam/view/Caption1Line;

    .line 539
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private getNewTitle()Ljava/lang/String;
    .locals 4

    .prologue
    .line 588
    const/4 v1, 0x0

    .line 589
    .local v1, "newTitle":Ljava/lang/String;
    sget-object v3, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v3}, Lcom/sec/android/app/storycam/AppDataManager;->getOriginalTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v2

    .line 590
    .local v2, "originalTranscode":Lcom/samsung/app/video/editor/external/TranscodeElement;
    if-eqz v2, :cond_0

    .line 591
    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getTextEleList()Ljava/util/List;

    move-result-object v0

    .line 592
    .local v0, "clipParams":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_0

    .line 593
    const/4 v3, 0x0

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/app/video/editor/external/ClipartParams;

    iget-object v1, v3, Lcom/samsung/app/video/editor/external/ClipartParams;->data:Ljava/lang/String;

    .line 597
    .end local v0    # "clipParams":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    :cond_0
    return-object v1
.end method


# virtual methods
.method public onTranscodeElementChanged(Lcom/samsung/app/video/editor/external/TranscodeElement;)V
    .locals 5
    .param p1, "transcodeElement"    # Lcom/samsung/app/video/editor/external/TranscodeElement;

    .prologue
    const/4 v4, 0x0

    .line 543
    if-nez p1, :cond_1

    .line 585
    :cond_0
    :goto_0
    return-void

    .line 545
    :cond_1
    invoke-virtual {p1, v4}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElement(I)Lcom/samsung/app/video/editor/external/Element;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getTextClipartParamsListForElement(Lcom/samsung/app/video/editor/external/Element;)Ljava/util/List;

    move-result-object v1

    .line 546
    .local v1, "text":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 547
    invoke-direct {p0}, Lcom/sec/android/app/storycam/view/Caption1Line$7;->getNewTitle()Ljava/lang/String;

    move-result-object v0

    .line 555
    .local v0, "newTitle":Ljava/lang/String;
    if-eqz v0, :cond_2

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 556
    iget-object v2, p0, Lcom/sec/android/app/storycam/view/Caption1Line$7;->this$0:Lcom/sec/android/app/storycam/view/Caption1Line;

    invoke-virtual {v2, v0}, Lcom/sec/android/app/storycam/view/Caption1Line;->updateTitle(Ljava/lang/String;)V

    .line 558
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/storycam/view/Caption1Line$7;->this$0:Lcom/sec/android/app/storycam/view/Caption1Line;

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/app/video/editor/external/ClipartParams;

    iget-object v2, v2, Lcom/samsung/app/video/editor/external/ClipartParams;->data:Ljava/lang/String;

    invoke-virtual {v3, v2}, Lcom/sec/android/app/storycam/view/Caption1Line;->setText(Ljava/lang/CharSequence;)V

    .line 559
    iget-object v2, p0, Lcom/sec/android/app/storycam/view/Caption1Line$7;->this$0:Lcom/sec/android/app/storycam/view/Caption1Line;

    iget-object v3, p0, Lcom/sec/android/app/storycam/view/Caption1Line$7;->this$0:Lcom/sec/android/app/storycam/view/Caption1Line;

    invoke-virtual {v3}, Lcom/sec/android/app/storycam/view/Caption1Line;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->length()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/storycam/view/Caption1Line;->setSelection(I)V

    goto :goto_0
.end method
