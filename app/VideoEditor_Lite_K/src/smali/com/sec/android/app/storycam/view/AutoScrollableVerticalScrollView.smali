.class public Lcom/sec/android/app/storycam/view/AutoScrollableVerticalScrollView;
.super Landroid/widget/ScrollView;
.source "AutoScrollableVerticalScrollView.java"


# instance fields
.field private mViewStartPos:I

.field private myScroller:Landroid/widget/OverScroller;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;)V

    .line 12
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/storycam/view/AutoScrollableVerticalScrollView;->mViewStartPos:I

    .line 17
    invoke-direct {p0}, Lcom/sec/android/app/storycam/view/AutoScrollableVerticalScrollView;->init()V

    .line 18
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 12
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/storycam/view/AutoScrollableVerticalScrollView;->mViewStartPos:I

    .line 22
    invoke-direct {p0}, Lcom/sec/android/app/storycam/view/AutoScrollableVerticalScrollView;->init()V

    .line 23
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 26
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 12
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/storycam/view/AutoScrollableVerticalScrollView;->mViewStartPos:I

    .line 27
    invoke-direct {p0}, Lcom/sec/android/app/storycam/view/AutoScrollableVerticalScrollView;->init()V

    .line 28
    return-void
.end method

.method private init()V
    .locals 5

    .prologue
    .line 42
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    .line 44
    .local v2, "parent":Ljava/lang/Class;
    :cond_0
    invoke-virtual {v2}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v2

    .line 45
    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "android.widget.ScrollView"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 46
    const-string v3, "mScroller"

    invoke-virtual {v2, v3}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 47
    .local v1, "field":Ljava/lang/reflect/Field;
    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 48
    invoke-virtual {v1, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/widget/OverScroller;

    iput-object v3, p0, Lcom/sec/android/app/storycam/view/AutoScrollableVerticalScrollView;->myScroller:Landroid/widget/OverScroller;
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2

    .line 56
    .end local v1    # "field":Ljava/lang/reflect/Field;
    .end local v2    # "parent":Ljava/lang/Class;
    :goto_0
    return-void

    .line 49
    :catch_0
    move-exception v0

    .line 50
    .local v0, "e":Ljava/lang/NoSuchFieldException;
    invoke-virtual {v0}, Ljava/lang/NoSuchFieldException;->printStackTrace()V

    goto :goto_0

    .line 51
    .end local v0    # "e":Ljava/lang/NoSuchFieldException;
    :catch_1
    move-exception v0

    .line 52
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 53
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v0

    .line 54
    .local v0, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public customSmoothScrollBy(II)V
    .locals 7
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    const/4 v6, 0x0

    .line 59
    iget-object v4, p0, Lcom/sec/android/app/storycam/view/AutoScrollableVerticalScrollView;->myScroller:Landroid/widget/OverScroller;

    if-nez v4, :cond_1

    .line 60
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/storycam/view/AutoScrollableVerticalScrollView;->smoothScrollBy(II)V

    .line 72
    :cond_0
    :goto_0
    return-void

    .line 63
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/view/AutoScrollableVerticalScrollView;->getChildCount()I

    move-result v4

    if-eqz v4, :cond_0

    .line 65
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/view/AutoScrollableVerticalScrollView;->getHeight()I

    move-result v4

    invoke-virtual {p0}, Lcom/sec/android/app/storycam/view/AutoScrollableVerticalScrollView;->getPaddingTop()I

    move-result v5

    sub-int/2addr v4, v5

    invoke-virtual {p0}, Lcom/sec/android/app/storycam/view/AutoScrollableVerticalScrollView;->getPaddingBottom()I

    move-result v5

    sub-int v1, v4, v5

    .line 66
    .local v1, "height":I
    invoke-virtual {p0, v6}, Lcom/sec/android/app/storycam/view/AutoScrollableVerticalScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v0

    .line 67
    .local v0, "bottom":I
    sub-int v4, v0, v1

    invoke-static {v6, v4}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 68
    .local v2, "maxY":I
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/view/AutoScrollableVerticalScrollView;->getScrollY()I

    move-result v3

    .line 69
    .local v3, "scrollY":I
    add-int v4, v3, p2

    invoke-static {v4, v2}, Ljava/lang/Math;->min(II)I

    move-result v4

    invoke-static {v6, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    sub-int p2, v4, v3

    .line 70
    mul-int/lit8 v4, p2, 0x1e

    invoke-virtual {p0, p1, v4}, Lcom/sec/android/app/storycam/view/AutoScrollableVerticalScrollView;->smoothScrollBy(II)V

    .line 71
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/view/AutoScrollableVerticalScrollView;->invalidate()V

    goto :goto_0
.end method

.method public getViewStartPosition()I
    .locals 1

    .prologue
    .line 36
    iget v0, p0, Lcom/sec/android/app/storycam/view/AutoScrollableVerticalScrollView;->mViewStartPos:I

    return v0
.end method

.method protected onScrollChanged(IIII)V
    .locals 0
    .param p1, "l"    # I
    .param p2, "t"    # I
    .param p3, "oldl"    # I
    .param p4, "oldt"    # I

    .prologue
    .line 32
    iput p2, p0, Lcom/sec/android/app/storycam/view/AutoScrollableVerticalScrollView;->mViewStartPos:I

    .line 33
    return-void
.end method
