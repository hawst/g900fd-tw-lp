.class Lcom/sec/android/app/ve/projectsharing/ChordChannelService$3;
.super Ljava/lang/Object;
.source "ChordChannelService.java"

# interfaces
.implements Lcom/samsung/chord/IChordChannelListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/ve/projectsharing/ChordChannelService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$3;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    .line 182
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDataReceived(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[[B)V
    .locals 4
    .param p1, "fromNode"    # Ljava/lang/String;
    .param p2, "fromChannel"    # Ljava/lang/String;
    .param p3, "payloadType"    # Ljava/lang/String;
    .param p4, "payload"    # [[B

    .prologue
    const/4 v3, 0x0

    .line 380
    const-string v0, "MSG_REQUEST_DEVICE_NAME"

    invoke-virtual {p3, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_1

    .line 381
    iget-object v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$3;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    # invokes: Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->handleDeviceNameRequestMsg(Ljava/lang/String;[[B)V
    invoke-static {v0, p1, p4}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->access$15(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;Ljava/lang/String;[[B)V

    .line 399
    :cond_0
    :goto_0
    return-void

    .line 383
    :cond_1
    const-string v0, "MSG_RESPONSE_DEVICE_NAME"

    invoke-virtual {p3, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_2

    .line 384
    iget-object v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$3;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    # invokes: Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->handleDeviceNameResponseMsg(Ljava/lang/String;[[B)V
    invoke-static {v0, p1, p4}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->access$16(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;Ljava/lang/String;[[B)V

    goto :goto_0

    .line 386
    :cond_2
    const-string v0, "MSG_NOTIFY_PRJ_SHARE"

    invoke-virtual {p3, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_4

    .line 387
    iget-object v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$3;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;
    invoke-static {v0}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->access$3(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;)Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    move-result-object v0

    if-nez v0, :cond_3

    .line 388
    iget-object v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$3;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    # invokes: Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->handleReceiveProjectNotificationMsg(Ljava/lang/String;[[B)V
    invoke-static {v0, p1, p4}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->access$17(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;Ljava/lang/String;[[B)V

    .line 389
    iget-object v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$3;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    new-instance v1, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ProjectApprovalMessage;

    const/4 v2, 0x1

    invoke-direct {v1, v2, v3}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ProjectApprovalMessage;-><init>(ZZ)V

    # invokes: Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->approveProjectShare(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ProjectApprovalMessage;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->access$18(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ProjectApprovalMessage;)V

    goto :goto_0

    .line 392
    :cond_3
    const-string v0, "ProjectSharing"

    const-string v1, "Inside MSG_NOTIFY_PRJ_SHARE, a session is already running, disapprove this Project Receive request"

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 393
    iget-object v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$3;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    new-instance v1, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ProjectApprovalMessage;

    invoke-direct {v1, v3, v3}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ProjectApprovalMessage;-><init>(ZZ)V

    # invokes: Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->approveProjectShare(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ProjectApprovalMessage;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->access$18(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ProjectApprovalMessage;)V

    goto :goto_0

    .line 396
    :cond_4
    const-string v0, "MSG_ACK_PRJ_SHARE"

    invoke-virtual {p3, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$3;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;
    invoke-static {v0}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->access$3(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;)Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 397
    iget-object v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$3;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    # invokes: Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->handleProjectShareAckMsg(Ljava/lang/String;[[B)V
    invoke-static {v0, p1, p4}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->access$19(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;Ljava/lang/String;[[B)V

    goto :goto_0
.end method

.method public onFileChunkReceived(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)V
    .locals 0
    .param p1, "fromNode"    # Ljava/lang/String;
    .param p2, "fromChannel"    # Ljava/lang/String;
    .param p3, "fileName"    # Ljava/lang/String;
    .param p4, "hash"    # Ljava/lang/String;
    .param p5, "fileType"    # Ljava/lang/String;
    .param p6, "exchangeId"    # Ljava/lang/String;
    .param p7, "fileSize"    # J
    .param p9, "offset"    # J

    .prologue
    .line 376
    return-void
.end method

.method public onFileChunkSent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJ)V
    .locals 0
    .param p1, "toNode"    # Ljava/lang/String;
    .param p2, "toChannel"    # Ljava/lang/String;
    .param p3, "fileName"    # Ljava/lang/String;
    .param p4, "hash"    # Ljava/lang/String;
    .param p5, "fileType"    # Ljava/lang/String;
    .param p6, "exchangeId"    # Ljava/lang/String;
    .param p7, "fileSize"    # J
    .param p9, "offset"    # J
    .param p11, "chunkSize"    # J

    .prologue
    .line 371
    return-void
.end method

.method public onFileFailed(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0
    .param p1, "node"    # Ljava/lang/String;
    .param p2, "channel"    # Ljava/lang/String;
    .param p3, "fileName"    # Ljava/lang/String;
    .param p4, "hash"    # Ljava/lang/String;
    .param p5, "exchangeId"    # Ljava/lang/String;
    .param p6, "reason"    # I

    .prologue
    .line 365
    return-void
.end method

.method public onFileReceived(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)V
    .locals 0
    .param p1, "fromNode"    # Ljava/lang/String;
    .param p2, "fromChannel"    # Ljava/lang/String;
    .param p3, "fileName"    # Ljava/lang/String;
    .param p4, "hash"    # Ljava/lang/String;
    .param p5, "fileType"    # Ljava/lang/String;
    .param p6, "exchangeId"    # Ljava/lang/String;
    .param p7, "fileSize"    # J
    .param p9, "tmpFilePath"    # Ljava/lang/String;

    .prologue
    .line 359
    return-void
.end method

.method public onFileSent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "toNode"    # Ljava/lang/String;
    .param p2, "toChannel"    # Ljava/lang/String;
    .param p3, "fileName"    # Ljava/lang/String;
    .param p4, "hash"    # Ljava/lang/String;
    .param p5, "fileType"    # Ljava/lang/String;
    .param p6, "exchangeId"    # Ljava/lang/String;

    .prologue
    .line 353
    return-void
.end method

.method public onFileWillReceive(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 0
    .param p1, "fromNode"    # Ljava/lang/String;
    .param p2, "fromChannel"    # Ljava/lang/String;
    .param p3, "fileName"    # Ljava/lang/String;
    .param p4, "hash"    # Ljava/lang/String;
    .param p5, "fileType"    # Ljava/lang/String;
    .param p6, "exchangeId"    # Ljava/lang/String;
    .param p7, "fileSize"    # J

    .prologue
    .line 347
    return-void
.end method

.method public onMultiFilesChunkReceived(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;JJ)V
    .locals 3
    .param p1, "fromNode"    # Ljava/lang/String;
    .param p2, "fromChannel"    # Ljava/lang/String;
    .param p3, "fileName"    # Ljava/lang/String;
    .param p4, "taskId"    # Ljava/lang/String;
    .param p5, "index"    # I
    .param p6, "fileType"    # Ljava/lang/String;
    .param p7, "fileSize"    # J
    .param p9, "offset"    # J

    .prologue
    .line 339
    iget-object v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$3;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    const/4 v1, 0x1

    const/4 v2, 0x0

    # invokes: Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->updateReceiverProgressNotification(JZZ)Landroid/app/Notification;
    invoke-static {v0, p9, p10, v1, v2}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->access$10(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;JZZ)Landroid/app/Notification;

    .line 341
    return-void
.end method

.method public onMultiFilesChunkSent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;JJJ)V
    .locals 3
    .param p1, "toNode"    # Ljava/lang/String;
    .param p2, "toChannel"    # Ljava/lang/String;
    .param p3, "fileName"    # Ljava/lang/String;
    .param p4, "taskId"    # Ljava/lang/String;
    .param p5, "index"    # I
    .param p6, "fileType"    # Ljava/lang/String;
    .param p7, "fileSize"    # J
    .param p9, "offset"    # J
    .param p11, "chunkSize"    # J

    .prologue
    .line 332
    iget-object v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$3;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    const/4 v1, 0x1

    const/4 v2, 0x0

    # invokes: Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->updateSenderProgressNotification(JZZ)Landroid/app/Notification;
    invoke-static {v0, p9, p10, v1, v2}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->access$11(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;JZZ)Landroid/app/Notification;

    .line 334
    return-void
.end method

.method public onMultiFilesFailed(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 0
    .param p1, "node"    # Ljava/lang/String;
    .param p2, "channel"    # Ljava/lang/String;
    .param p3, "fileName"    # Ljava/lang/String;
    .param p4, "taskId"    # Ljava/lang/String;
    .param p5, "index"    # I
    .param p6, "reason"    # I

    .prologue
    .line 327
    return-void
.end method

.method public onMultiFilesFinished(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 14
    .param p1, "node"    # Ljava/lang/String;
    .param p2, "channel"    # Ljava/lang/String;
    .param p3, "taskId"    # Ljava/lang/String;
    .param p4, "reason"    # I

    .prologue
    .line 256
    iget-object v11, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$3;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;
    invoke-static {v11}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->access$3(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;)Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    move-result-object v11

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mPartialWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v11}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$1(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;)Landroid/os/PowerManager$WakeLock;

    move-result-object v11

    if-eqz v11, :cond_0

    iget-object v11, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$3;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;
    invoke-static {v11}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->access$3(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;)Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    move-result-object v11

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mPartialWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v11}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$1(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;)Landroid/os/PowerManager$WakeLock;

    move-result-object v11

    invoke-virtual {v11}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v11

    if-eqz v11, :cond_0

    .line 257
    iget-object v11, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$3;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;
    invoke-static {v11}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->access$3(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;)Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    move-result-object v11

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mPartialWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v11}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$1(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;)Landroid/os/PowerManager$WakeLock;

    move-result-object v11

    invoke-virtual {v11}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 259
    :cond_0
    const/4 v9, 0x0

    .line 260
    .local v9, "vejsonFile":Ljava/lang/String;
    const/4 v8, 0x0

    .line 261
    .local v8, "tmpFile":Ljava/lang/String;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v11, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$3;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;
    invoke-static {v11}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->access$3(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;)Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    move-result-object v11

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mFilesBeingTransferred:Ljava/util/ArrayList;
    invoke-static {v11}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$7(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;)Ljava/util/ArrayList;

    move-result-object v11

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v11

    if-lt v3, v11, :cond_3

    .line 269
    const-string v11, "ProjectSharing"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "inside onMultiFilesFinished, reason="

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p4

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    const/16 v11, 0x7d6

    move/from16 v0, p4

    if-ne v0, v11, :cond_9

    .line 271
    iget-object v11, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$3;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;
    invoke-static {v11}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->access$3(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;)Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    move-result-object v11

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mIsSender:Z
    invoke-static {v11}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$8(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 272
    iget-object v11, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$3;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    sget v12, Lcom/sec/android/app/ve/R$string;->prj_sharing_success:I

    # invokes: Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->sendToastMessageToUIHandler(I)V
    invoke-static {v11, v12}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->access$13(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;I)V

    .line 310
    :goto_1
    if-eqz v9, :cond_1

    .line 311
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 312
    .local v5, "jsonFile":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v11

    if-eqz v11, :cond_1

    .line 313
    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    .line 315
    .end local v5    # "jsonFile":Ljava/io/File;
    :cond_1
    if-eqz v8, :cond_2

    .line 316
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 317
    .local v7, "tmp":Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v11

    if-eqz v11, :cond_2

    .line 318
    invoke-virtual {v7}, Ljava/io/File;->delete()Z

    .line 320
    .end local v7    # "tmp":Ljava/io/File;
    :cond_2
    iget-object v11, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$3;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    const/4 v12, 0x0

    invoke-static {v11, v12}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->access$14(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;)V

    .line 321
    iget-object v11, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$3;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    const/4 v12, 0x1

    invoke-virtual {v11, v12}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->stopForeground(Z)V

    .line 322
    return-void

    .line 262
    :cond_3
    iget-object v11, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$3;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;
    invoke-static {v11}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->access$3(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;)Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    move-result-object v11

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mFilesBeingTransferred:Ljava/util/ArrayList;
    invoke-static {v11}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$7(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;)Ljava/util/ArrayList;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 263
    .local v4, "item":Ljava/lang/String;
    const-string v11, ".vejson"

    invoke-virtual {v4, v11}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 264
    move-object v9, v4

    .line 261
    :cond_4
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    .line 265
    :cond_5
    const-string v11, ".flist"

    invoke-virtual {v4, v11}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 266
    move-object v8, v4

    goto :goto_2

    .line 275
    .end local v4    # "item":Ljava/lang/String;
    :cond_6
    iget-object v11, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$3;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;
    invoke-static {v11}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->access$3(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;)Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    move-result-object v11

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mReceiverFileMapping:Ljava/util/HashMap;
    invoke-static {v11}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$9(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;)Ljava/util/HashMap;

    move-result-object v11

    invoke-static {v9, v11}, Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper;->scanThisSharedProject(Ljava/lang/String;Ljava/util/HashMap;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 276
    iget-object v11, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$3;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    const-string v12, "BROADCAST_PROJECT_IMPORTED"

    const/4 v13, 0x0

    # invokes: Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->sendLocalBroadcast(Ljava/lang/String;Landroid/os/Bundle;)V
    invoke-static {v11, v12, v13}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->access$8(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 277
    iget-object v11, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$3;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    sget v12, Lcom/sec/android/app/ve/R$string;->prj_recieve_success:I

    # invokes: Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->sendToastMessageToUIHandler(I)V
    invoke-static {v11, v12}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->access$13(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;I)V

    .line 283
    :goto_3
    iget-object v11, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$3;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;
    invoke-static {v11}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->access$3(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;)Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    move-result-object v11

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mFilesBeingTransferred:Ljava/util/ArrayList;
    invoke-static {v11}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$7(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;)Ljava/util/ArrayList;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 284
    iget-object v11, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$3;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;
    invoke-static {v11}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->access$3(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;)Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    move-result-object v11

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mFilesBeingTransferred:Ljava/util/ArrayList;
    invoke-static {v11}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$7(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;)Ljava/util/ArrayList;

    move-result-object v11

    invoke-virtual {v11, v8}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 285
    iget-object v11, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$3;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;
    invoke-static {v11}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->access$3(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;)Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    move-result-object v11

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mFilesBeingTransferred:Ljava/util/ArrayList;
    invoke-static {v11}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$7(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;)Ljava/util/ArrayList;

    move-result-object v11

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v11

    new-array v2, v11, [Ljava/lang/String;

    .line 286
    .local v2, "filePaths":[Ljava/lang/String;
    const/4 v10, 0x0

    .local v10, "x":I
    :goto_4
    array-length v11, v2

    if-lt v10, v11, :cond_8

    .line 289
    iget-object v11, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$3;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    invoke-virtual {v11}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->getApplicationContext()Landroid/content/Context;

    move-result-object v11

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-static {v11, v2, v12, v13}, Landroid/media/MediaScannerConnection;->scanFile(Landroid/content/Context;[Ljava/lang/String;[Ljava/lang/String;Landroid/media/MediaScannerConnection$OnScanCompletedListener;)V

    goto/16 :goto_1

    .line 280
    .end local v2    # "filePaths":[Ljava/lang/String;
    .end local v10    # "x":I
    :cond_7
    iget-object v11, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$3;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    sget v12, Lcom/sec/android/app/ve/R$string;->prj_sharing_error:I

    # invokes: Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->sendToastMessageToUIHandler(I)V
    invoke-static {v11, v12}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->access$13(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;I)V

    goto :goto_3

    .line 287
    .restart local v2    # "filePaths":[Ljava/lang/String;
    .restart local v10    # "x":I
    :cond_8
    iget-object v11, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$3;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;
    invoke-static {v11}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->access$3(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;)Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    move-result-object v11

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mReceiverFileMapping:Ljava/util/HashMap;
    invoke-static {v11}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$9(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;)Ljava/util/HashMap;

    move-result-object v11

    iget-object v12, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$3;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;
    invoke-static {v12}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->access$3(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;)Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    move-result-object v12

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mFilesBeingTransferred:Ljava/util/ArrayList;
    invoke-static {v12}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$7(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;)Ljava/util/ArrayList;

    move-result-object v12

    invoke-virtual {v12, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    aput-object v11, v2, v10

    .line 286
    add-int/lit8 v10, v10, 0x1

    goto :goto_4

    .line 294
    .end local v2    # "filePaths":[Ljava/lang/String;
    .end local v10    # "x":I
    :cond_9
    const-string v11, "ProjectSharing"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "transferring project failed, reason="

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p4

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 295
    iget-object v11, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$3;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;
    invoke-static {v11}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->access$3(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;)Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    move-result-object v11

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mFilesBeingTransferred:Ljava/util/ArrayList;
    invoke-static {v11}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$7(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;)Ljava/util/ArrayList;

    move-result-object v11

    if-eqz v11, :cond_a

    iget-object v11, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$3;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;
    invoke-static {v11}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->access$3(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;)Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    move-result-object v11

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mFilesBeingTransferred:Ljava/util/ArrayList;
    invoke-static {v11}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$7(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;)Ljava/util/ArrayList;

    move-result-object v11

    invoke-virtual {v11}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v11

    if-nez v11, :cond_a

    iget-object v11, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$3;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;
    invoke-static {v11}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->access$3(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;)Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    move-result-object v11

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mIsSender:Z
    invoke-static {v11}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$8(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;)Z

    move-result v11

    if-nez v11, :cond_a

    .line 296
    const/4 v3, 0x0

    :goto_5
    iget-object v11, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$3;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;
    invoke-static {v11}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->access$3(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;)Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    move-result-object v11

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mFilesBeingTransferred:Ljava/util/ArrayList;
    invoke-static {v11}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$7(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;)Ljava/util/ArrayList;

    move-result-object v11

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v11

    if-lt v3, v11, :cond_b

    .line 308
    :cond_a
    iget-object v11, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$3;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    sget v12, Lcom/sec/android/app/ve/R$string;->prj_sharing_error:I

    # invokes: Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->sendToastMessageToUIHandler(I)V
    invoke-static {v11, v12}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->access$13(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;I)V

    goto/16 :goto_1

    .line 297
    :cond_b
    iget-object v11, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$3;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;
    invoke-static {v11}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->access$3(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;)Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    move-result-object v11

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mReceiverFileMapping:Ljava/util/HashMap;
    invoke-static {v11}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$9(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;)Ljava/util/HashMap;

    move-result-object v11

    iget-object v12, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$3;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;
    invoke-static {v12}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->access$3(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;)Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    move-result-object v12

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mFilesBeingTransferred:Ljava/util/ArrayList;
    invoke-static {v12}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$7(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;)Ljava/util/ArrayList;

    move-result-object v12

    invoke-virtual {v12, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 298
    .local v6, "receivedFile":Ljava/lang/String;
    if-eqz v6, :cond_c

    .line 299
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 300
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v11

    if-eqz v11, :cond_c

    .line 301
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 296
    .end local v1    # "file":Ljava/io/File;
    :cond_c
    add-int/lit8 v3, v3, 0x1

    goto :goto_5
.end method

.method public onMultiFilesReceived(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;JLjava/lang/String;)V
    .locals 5
    .param p1, "fromNode"    # Ljava/lang/String;
    .param p2, "fromChannel"    # Ljava/lang/String;
    .param p3, "fileName"    # Ljava/lang/String;
    .param p4, "taskId"    # Ljava/lang/String;
    .param p5, "index"    # I
    .param p6, "fileType"    # Ljava/lang/String;
    .param p7, "fileSize"    # J
    .param p9, "tmpFilePath"    # Ljava/lang/String;

    .prologue
    .line 247
    iget-object v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$3;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    # invokes: Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->saveTempFile(Ljava/lang/String;Ljava/lang/String;I)V
    invoke-static {v0, p9, p3, p5}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->access$12(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;Ljava/lang/String;Ljava/lang/String;I)V

    .line 249
    iget-object v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$3;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;
    invoke-static {v0}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->access$3(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;)Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    move-result-object v0

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mBytesTransferred:J
    invoke-static {v0}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$5(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;)J

    move-result-wide v2

    add-long/2addr v2, p7

    invoke-static {v0, v2, v3}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$3(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;J)V

    .line 250
    iget-object v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$3;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    const-wide/16 v2, 0x0

    const/4 v1, 0x1

    const/4 v4, 0x0

    # invokes: Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->updateReceiverProgressNotification(JZZ)Landroid/app/Notification;
    invoke-static {v0, v2, v3, v1, v4}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->access$10(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;JZZ)Landroid/app/Notification;

    .line 251
    return-void
.end method

.method public onMultiFilesSent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .locals 6
    .param p1, "toNode"    # Ljava/lang/String;
    .param p2, "toChannel"    # Ljava/lang/String;
    .param p3, "fileName"    # Ljava/lang/String;
    .param p4, "taskId"    # Ljava/lang/String;
    .param p5, "index"    # I
    .param p6, "fileType"    # Ljava/lang/String;

    .prologue
    .line 237
    iget-object v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$3;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;
    invoke-static {v0}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->access$3(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;)Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    move-result-object v0

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mBytesTransferred:J
    invoke-static {v0}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$5(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;)J

    move-result-wide v2

    iget-object v1, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$3;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;
    invoke-static {v1}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->access$3(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;)Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    move-result-object v1

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mFileSizes:[J
    invoke-static {v1}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$6(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;)[J

    move-result-object v1

    add-int/lit8 v4, p5, -0x1

    aget-wide v4, v1, v4

    add-long/2addr v2, v4

    invoke-static {v0, v2, v3}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$3(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;J)V

    .line 238
    iget-object v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$3;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    const-wide/16 v2, 0x0

    const/4 v1, 0x1

    const/4 v4, 0x0

    # invokes: Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->updateSenderProgressNotification(JZZ)Landroid/app/Notification;
    invoke-static {v0, v2, v3, v1, v4}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->access$11(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;JZZ)Landroid/app/Notification;

    .line 240
    return-void
.end method

.method public onMultiFilesWillReceive(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;J)V
    .locals 11
    .param p1, "fromNode"    # Ljava/lang/String;
    .param p2, "fromChannel"    # Ljava/lang/String;
    .param p3, "fileName"    # Ljava/lang/String;
    .param p4, "taskId"    # Ljava/lang/String;
    .param p5, "totalCount"    # I
    .param p6, "fileType"    # Ljava/lang/String;
    .param p7, "fileSize"    # J

    .prologue
    .line 209
    iget-object v3, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$3;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mChordManager:Lcom/samsung/chord/ChordManager;
    invoke-static {v3}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->access$0(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;)Lcom/samsung/chord/ChordManager;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 210
    iget-object v3, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$3;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mChordManager:Lcom/samsung/chord/ChordManager;
    invoke-static {v3}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->access$0(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;)Lcom/samsung/chord/ChordManager;

    move-result-object v3

    const-string v4, "CHORD_PRJ_SHARING_CHANNEL"

    invoke-virtual {v3, v4}, Lcom/samsung/chord/ChordManager;->getJoinedChannel(Ljava/lang/String;)Lcom/samsung/chord/IChordChannel;

    move-result-object v2

    .line 211
    .local v2, "channel":Lcom/samsung/chord/IChordChannel;
    if-eqz v2, :cond_0

    .line 212
    iget-object v3, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$3;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    move-wide/from16 v0, p7

    # invokes: Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->checkAvailableMemory(J)Z
    invoke-static {v3, v0, v1}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->access$9(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;J)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 213
    iget-object v3, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$3;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    const-string v4, "power"

    invoke-virtual {v3, v4}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/os/PowerManager;

    .line 214
    .local v8, "pm":Landroid/os/PowerManager;
    iget-object v3, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$3;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;
    invoke-static {v3}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->access$3(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;)Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    move-result-object v3

    const/4 v4, 0x1

    const-string v5, "ProjectSharingThread"

    invoke-virtual {v8, v4, v5}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$0(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;Landroid/os/PowerManager$WakeLock;)V

    .line 215
    iget-object v3, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$3;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;
    invoke-static {v3}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->access$3(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;)Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    move-result-object v3

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->mPartialWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v3}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$1(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;)Landroid/os/PowerManager$WakeLock;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 216
    iget-object v3, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$3;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;
    invoke-static {v3}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->access$3(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;)Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    move-result-object v3

    move-wide/from16 v0, p7

    invoke-static {v3, v0, v1}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$2(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;J)V

    .line 217
    iget-object v3, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$3;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;
    invoke-static {v3}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->access$3(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;)Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    move-result-object v3

    const-wide/16 v4, 0x0

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$3(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;J)V

    .line 218
    iget-object v3, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$3;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;
    invoke-static {v3}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->access$3(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;)Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    move-result-object v3

    new-instance v4, Landroid/app/Notification$Builder;

    iget-object v5, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$3;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    invoke-virtual {v5}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    invoke-static {v3, v4}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;->access$4(Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;Landroid/app/Notification$Builder;)V

    .line 220
    const/16 v4, 0x7530

    const/4 v5, 0x2

    const-wide/32 v6, 0x4b000

    move-object v3, p4

    invoke-interface/range {v2 .. v7}, Lcom/samsung/chord/IChordChannel;->acceptMultiFiles(Ljava/lang/String;IIJ)V

    .line 224
    iget-object v3, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$3;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    const/16 v4, 0xb

    iget-object v5, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$3;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    const-wide/16 v6, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x1

    # invokes: Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->updateReceiverProgressNotification(JZZ)Landroid/app/Notification;
    invoke-static {v5, v6, v7, v9, v10}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->access$10(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;JZZ)Landroid/app/Notification;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->startForeground(ILandroid/app/Notification;)V

    .line 232
    .end local v2    # "channel":Lcom/samsung/chord/IChordChannel;
    .end local v8    # "pm":Landroid/os/PowerManager;
    :cond_0
    :goto_0
    return-void

    .line 227
    .restart local v2    # "channel":Lcom/samsung/chord/IChordChannel;
    :cond_1
    invoke-interface {v2, p4}, Lcom/samsung/chord/IChordChannel;->rejectMultiFiles(Ljava/lang/String;)V

    .line 228
    const-string v3, "ProjectSharing"

    const-string v4, "Rejected multi files"

    invoke-static {v3, v4}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onNodeJoined(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "fromNode"    # Ljava/lang/String;
    .param p2, "fromChannel"    # Ljava/lang/String;

    .prologue
    .line 193
    iget-object v3, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$3;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mDeviceMap:Ljava/util/HashMap;
    invoke-static {v3}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->access$6(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;)Ljava/util/HashMap;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_0

    .line 194
    iget-object v3, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$3;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mChordManager:Lcom/samsung/chord/ChordManager;
    invoke-static {v3}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->access$0(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;)Lcom/samsung/chord/ChordManager;

    move-result-object v3

    const-string v4, "CHORD_PRJ_SHARING_CHANNEL"

    invoke-virtual {v3, v4}, Lcom/samsung/chord/ChordManager;->getJoinedChannel(Ljava/lang/String;)Lcom/samsung/chord/IChordChannel;

    move-result-object v1

    .line 195
    .local v1, "channel":Lcom/samsung/chord/IChordChannel;
    if-eqz v1, :cond_0

    .line 196
    const/4 v3, 0x1

    new-array v2, v3, [[B

    .line 197
    .local v2, "payload":[[B
    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$3;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    # invokes: Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->getMyDeviceName()Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->access$7(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    aput-object v4, v2, v3

    .line 198
    const-string v3, "MSG_REQUEST_DEVICE_NAME"

    invoke-interface {v1, p1, v3, v2}, Lcom/samsung/chord/IChordChannel;->sendData(Ljava/lang/String;Ljava/lang/String;[[B)Z

    .line 201
    .end local v1    # "channel":Lcom/samsung/chord/IChordChannel;
    .end local v2    # "payload":[[B
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 202
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v3, "BROADCAST_EXTRA_PEER_CHORD_ID"

    invoke-virtual {v0, v3, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    iget-object v3, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$3;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    const-string v4, "BROADCAST_NEW_PEER_AVAILABLE"

    # invokes: Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->sendLocalBroadcast(Ljava/lang/String;Landroid/os/Bundle;)V
    invoke-static {v3, v4, v0}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->access$8(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 204
    return-void
.end method

.method public onNodeLeft(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "fromNode"    # Ljava/lang/String;
    .param p2, "fromChannel"    # Ljava/lang/String;

    .prologue
    .line 186
    iget-object v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$3;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mDeviceMap:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->access$6(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 187
    iget-object v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$3;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mDeviceMap:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->access$6(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 188
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$3;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    const-string v1, "BROADCAST_PEER_LEFT"

    const/4 v2, 0x0

    # invokes: Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->sendLocalBroadcast(Ljava/lang/String;Landroid/os/Bundle;)V
    invoke-static {v0, v1, v2}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->access$8(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 189
    return-void
.end method
