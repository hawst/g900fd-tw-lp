.class Lcom/sec/android/app/ve/projectcompatibility/VEJSONKeyDefinitions;
.super Ljava/lang/Object;
.source "VEJSONKeyDefinitions.java"


# static fields
.field protected static final ALPHA_END:Ljava/lang/String; = "ALPHA_END"

.field protected static final ALPHA_START:Ljava/lang/String; = "ALPHA_START"

.field protected static final ANIM_ARRAY:Ljava/lang/String; = "ANIM_ARRAY"

.field protected static final ANIM_END:Ljava/lang/String; = "ANIM_END"

.field protected static final ANIM_INTERPOLATION:Ljava/lang/String; = "ANIM_INTERPOLATION"

.field protected static final ANIM_START:Ljava/lang/String; = "ANIM_START"

.field protected static final ANIM_TYPE:Ljava/lang/String; = "ANIM_TYPE"

.field protected static final AUDIO_DISPLAY_NAME:Ljava/lang/String; = "AUDIO_DISPLAY_NAME"

.field protected static final BGM_ARRAY:Ljava/lang/String; = "BGM_ARRAY"

.field protected static final BITMAP_ANIM_ARRAY:Ljava/lang/String; = "BITMAP_ANIM_ARRAY"

.field protected static final BLENDING_FILES_CURRENT_END_FRAME:Ljava/lang/String; = "BLENDING_FILES_CURRENT_END_FRAME"

.field protected static final BLENDING_FILES_CURRENT_START_FRAME:Ljava/lang/String; = "BLENDING_FILES_CURRENT_START_FRAME"

.field protected static final BLENDING_FILES_END_INDEX:Ljava/lang/String; = "BLENDING_FILES_END_INDEX"

.field protected static final BLENDING_FILES_ORIG_END_FRAME:Ljava/lang/String; = "BLENDING_FILES_ORIG_END_FRAME"

.field protected static final BLENDING_FILES_ORIG_START_FRAME:Ljava/lang/String; = "BLENDING_FILES_ORIG_START_FRAME"

.field protected static final BLENDING_FILES_PNG_FILE_NAME:Ljava/lang/String; = "BLENDING_FILES_PNG_FILE_NAME"

.field protected static final BLENDING_FILES_START_INDEX:Ljava/lang/String; = "BLENDING_FILES_START_INDEX"

.field protected static final BOTTOM:Ljava/lang/String; = "BOTTOM"

.field protected static final CAPTION_ALIGNMENT:Ljava/lang/String; = "CAPTION_ALIGNMENT"

.field protected static final CAPTION_COORDINATE_REF_X:Ljava/lang/String; = "CAPTION_COORDINATE_REF_X"

.field protected static final CAPTION_COORDINATE_REF_Y:Ljava/lang/String; = "CAPTION_COORDINATE_REF_Y"

.field protected static final CAPTION_EDITABLE:Ljava/lang/String; = "CAPTION_EDITABLE"

.field protected static final CAPTION_ELEMENT_ID:Ljava/lang/String; = "CAPTION_ELEMENT_ID"

.field protected static final CAPTION_LIST:Ljava/lang/String; = "CAPTION_LIST"

.field protected static final CAPTION_RAW_FILE:Ljava/lang/String; = "CAPTION_RAW_FILE"

.field protected static final CAPTION_TEXT:Ljava/lang/String; = "CAPTION_TEXT"

.field protected static final CAPTION_THEME:Ljava/lang/String; = "CAPTION_THEME"

.field protected static final DEFAULT_ALPHA:Ljava/lang/String; = "DEFAULT_ALPHA"

.field protected static final DEFAULT_ROTATION:Ljava/lang/String; = "DEFAULT_ROTATION"

.field protected static final DEFAULT_SCALEX:Ljava/lang/String; = "DEFAULT_SCALEX"

.field protected static final DEFAULT_SCALEY:Ljava/lang/String; = "DEFAULT_SCALEY"

.field protected static final DEFAULT_TRANSLATE_X:Ljava/lang/String; = "DEFAULT_TRANSLATE_X"

.field protected static final DEFAULT_TRANSLATE_Y:Ljava/lang/String; = "DEFAULT_TRANSLATE_Y"

.field protected static final EFFECTS_LIST:Ljava/lang/String; = "EFFECTS_LIST"

.field protected static final EFFECT_END_TIME:Ljava/lang/String; = "EFFECT_END_TIME"

.field protected static final EFFECT_RESOURCE_PATH:Ljava/lang/String; = "EFFECT_RESOURCE_PATH"

.field protected static final EFFECT_START_TIME:Ljava/lang/String; = "EFFECT_START_TIME"

.field protected static final EFFECT_TYPE:Ljava/lang/String; = "EFFECT_TYPE"

.field protected static final ELEMENT_ARRAY:Ljava/lang/String; = "ELEMENT_ARRAY"

.field protected static final ELEMENT_DURATION:Ljava/lang/String; = "ELEMENT_DURATION"

.field protected static final ELEMENT_END_TIME:Ljava/lang/String; = "ELEMENT_END_TIME"

.field protected static final ELEMENT_FILE_PATH:Ljava/lang/String; = "ELEMENT_FILE_PATH"

.field protected static final ELEMENT_GRP_ID:Ljava/lang/String; = "ELEMENT_GRP_ID"

.field protected static final ELEMENT_ROTATION_ORIENTATION:Ljava/lang/String; = "ELEMENT_ROTATION_ORIENTATION"

.field protected static final ELEMENT_SPLIT_POS_TIME:Ljava/lang/String; = "ELEMENT_SPLIT_TIME"

.field protected static final ELEMENT_START_TIME:Ljava/lang/String; = "ELEMENT_START_TIME"

.field protected static final ELEMENT_SUB_TYPE:Ljava/lang/String; = "ELEMENT_SUB_TYPE"

.field protected static final ELEMENT_TYPE:Ljava/lang/String; = "ELEMENT_TYPE"

.field protected static final FONT_COLOR:Ljava/lang/String; = "FONT_COLOR"

.field protected static final FONT_FILE:Ljava/lang/String; = "FONT_FILE"

.field protected static final FONT_SIZE:Ljava/lang/String; = "FONT_SIZE"

.field protected static final LEFT:Ljava/lang/String; = "LEFT"

.field protected static final PIVOT_X:Ljava/lang/String; = "PIVOT_X"

.field protected static final PIVOT_Y:Ljava/lang/String; = "PIVOT_Y"

.field protected static final PROJECT_DURATION:Ljava/lang/String; = "PROJECT_DURATION"

.field protected static final PROJECT_NAME:Ljava/lang/String; = "PROJECT_NAME"

.field protected static final PROJECT_THEME_NAME:Ljava/lang/String; = "THEME_NAME"

.field protected static final PROJECT_UNIQUE_ID:Ljava/lang/String; = "PROJECT_UNIQUE_ID"

.field protected static final RIGHT:Ljava/lang/String; = "RIGHT"

.field protected static final ROTATION_END:Ljava/lang/String; = "ROTATION_END"

.field protected static final ROTATION_START:Ljava/lang/String; = "ROTATION_START"

.field protected static final SCALE_END_X:Ljava/lang/String; = "SCALE_END_X"

.field protected static final SCALE_END_Y:Ljava/lang/String; = "SCALE_END_Y"

.field protected static final SCALE_START_X:Ljava/lang/String; = "SCALE_START_X"

.field protected static final SCALE_START_Y:Ljava/lang/String; = "SCALE_START_Y"

.field protected static final SHADOW_COLOR:Ljava/lang/String; = "SHADOW_COLOR"

.field protected static final SHADOW_R:Ljava/lang/String; = "SHADOW_R"

.field protected static final SHADOW_X:Ljava/lang/String; = "SHADOW_X"

.field protected static final SHADOW_Y:Ljava/lang/String; = "SHADOW_Y"

.field protected static final SOUND_ARRAY:Ljava/lang/String; = "SOUND_ARRAY"

.field protected static final STORYBOARD_END_FRAME:Ljava/lang/String; = "STORYBOARD_END_FRAME"

.field protected static final STORYBOARD_END_TIME:Ljava/lang/String; = "STORYBOARD_END_TIME"

.field protected static final STORYBOARD_REF_FRAMERATE:Ljava/lang/String; = "STORYBOARD_REF_FRAMERATE"

.field protected static final STORYBOARD_START_FRAME:Ljava/lang/String; = "STORYBOARD_START_FRAME"

.field protected static final STORYBOARD_START_TIME:Ljava/lang/String; = "STORYBOARD_START_TIME"

.field protected static final TOP:Ljava/lang/String; = "TOP"

.field protected static final TRANSITION:Ljava/lang/String; = "TRANSITION"

.field protected static final TRANSITION_DURATION:Ljava/lang/String; = "TRANSITION_DURATION"

.field protected static final TRANSITION_TYPE:Ljava/lang/String; = "TRANSITION_TYPE"

.field protected static final TRANSLATION_FROM_X:Ljava/lang/String; = "TRANSLATION_FROM_X"

.field protected static final TRANSLATION_FROM_Y:Ljava/lang/String; = "TRANSLATION_FROM_Y"

.field protected static final TRANSLATION_TO_X:Ljava/lang/String; = "TRANSLATION_TO_X"

.field protected static final TRANSLATION_TO_Y:Ljava/lang/String; = "TRANSLATION_TO_Y"

.field protected static final VOICEOVER_ARRAY:Ljava/lang/String; = "VOICEOVER_ARRAY"

.field protected static final VOLUME_LEVEL:Ljava/lang/String; = "VOLUME_LEVEL"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
