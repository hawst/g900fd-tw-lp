.class public Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;
.super Landroid/app/Activity;
.source "VideoEditorLiteActivity.java"

# interfaces
.implements Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ActivityEventsCallback;,
        Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$BGMModesDialog;,
        Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DeviceStorageFullDialog;,
        Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogFileNameExist;,
        Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogFileNameNull;,
        Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogHelpPage;,
        Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogInputMaxLimit;,
        Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogRemoveClips;,
        Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogSaveChanges;,
        Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$EnableAppDialog;,
        Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExpAdapter;,
        Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog;,
        Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$LaunchVideoEditorFullDialog;,
        Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$UnsupportedContentsDialog;
    }
.end annotation


# static fields
.field private static final CURRENT_TRANSCODE_ELEMENT:Ljava/lang/String; = "CurrentTranscodeElement"

.field public static final EXPORT_RESOLUTION:Ljava/lang/String; = "EXPORT_RESOLUTION"

.field private static final FONTSCALE_VALUE:Ljava/lang/String; = "font_scale_value"

.field private static final GALLERY_REQUEST_CODE:I = 0xff

.field private static final KEYBOARD_WAS_VISIBLE:Ljava/lang/String; = "KEYBOARD_WAS_VISIBLE"

.field private static final ORIGINAL_TRANSCODE_ELEMENT:Ljava/lang/String; = "OriginalTranscodeElement"

.field private static final OVERLAY_HELP_SHOWN:Ljava/lang/String; = "OVERLAY_HELP_SHOWN"

.field private static final PEN_DRAW_ACTIVITY:I = 0xfe

.field private static final SHOW_LAUNCH_VIDEO_EDITOR:Ljava/lang/String; = "SHOW_LAUNCH_VIDEO_EDITOR"

.field private static final UNCOMMITED_TITLE:Ljava/lang/String; = "UNCOMMITED_TITLE"

.field public static VERSION_CODE:I

.field private static bgmsAvailable:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/sec/android/app/ve/bgm/BGM$BGMModes;",
            ">;"
        }
    .end annotation
.end field

.field private static fontScale_value:F

.field private static mBGMDurationList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static mIsCancelDialogShown:Z

.field private static saveNeeded:Z


# instance fields
.field private captionText:Lcom/sec/android/app/storycam/view/Caption1Line;

.field private cursorPos:I

.field private hScrollView:Landroid/widget/HorizontalScrollView;

.field private isCaptionFcused:Z

.field private volatile isOkToResumeExport:Z

.field private isUserSelected:Z

.field private mActionBar:Landroid/app/ActionBar;

.field private mActivityEventCallbacks:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ActivityEventsCallback;",
            ">;"
        }
    .end annotation
.end field

.field private mActivityStateListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityStateChangedListener;",
            ">;"
        }
    .end annotation
.end field

.field private mBeforeEditCaptionString:Ljava/lang/String;

.field private mBgmInstance:Lcom/sec/android/app/storycam/view/BGMView;

.field private mConfig:Landroid/content/res/Configuration;

.field private mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

.field private mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

.field private mCurrentActivityState:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

.field private final mDragListener:Landroid/view/View$OnDragListener;

.field private mEditSessionTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

.field private mExportWrapper:Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;

.field private mGlobalBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mHardKeyboardHidden:I

.field private mIsBgmOn:Z

.field private mPreviewPlayer:Lcom/sec/android/app/ve/PreviewPlayerInterface;

.field private mRootView:Landroid/widget/RelativeLayout;

.field private mScover:Lcom/samsung/android/sdk/cover/Scover;

.field private mSelectedBGMPos:I

.field private mSummaryHandler:Landroid/os/Handler;

.field private mViewClickListener:Landroid/view/View$OnClickListener;

.field private mediaEditGrid:Lcom/sec/android/app/storycam/view/MediaEditGridView;

.field private mediaSession:Lcom/sec/android/app/storycam/VEMediaSession;

.field private spaceString:Ljava/lang/String;

.field private veMediaSessionCallback:Lcom/sec/android/app/storycam/VEMediaSession$VEMediaSessionCallback;

.field private wasEditing:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 122
    sget v0, Landroid/os/Build$VERSION;->RESOURCES_SDK_INT:I

    sput v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->VERSION_CODE:I

    .line 140
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mBGMDurationList:Ljava/util/ArrayList;

    .line 141
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->bgmsAvailable:Ljava/util/Map;

    .line 143
    sput-boolean v1, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->saveNeeded:Z

    .line 144
    sput-boolean v1, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mIsCancelDialogShown:Z

    .line 147
    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 118
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 119
    sget-object v0, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;->STATE_DEFAULT:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    iput-object v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mCurrentActivityState:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    .line 125
    new-instance v0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;

    invoke-direct {v0}, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mExportWrapper:Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;

    .line 126
    iput-object v2, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mEditSessionTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    .line 128
    const-string v0, " "

    iput-object v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->spaceString:Ljava/lang/String;

    .line 142
    iput-object v2, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->captionText:Lcom/sec/android/app/storycam/view/Caption1Line;

    .line 145
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mActivityEventCallbacks:Ljava/util/ArrayList;

    .line 146
    iput-boolean v1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->isOkToResumeExport:Z

    .line 154
    iput-boolean v1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->isCaptionFcused:Z

    .line 159
    iput v1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->cursorPos:I

    .line 162
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mSelectedBGMPos:I

    .line 163
    iput-boolean v1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->isUserSelected:Z

    .line 164
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mIsBgmOn:Z

    .line 174
    new-instance v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$1;-><init>(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->veMediaSessionCallback:Lcom/sec/android/app/storycam/VEMediaSession$VEMediaSessionCallback;

    .line 206
    new-instance v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$2;-><init>(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

    .line 218
    new-instance v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$3;-><init>(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mDragListener:Landroid/view/View$OnDragListener;

    .line 229
    new-instance v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$4;-><init>(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mGlobalBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 784
    new-instance v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$5;-><init>(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mViewClickListener:Landroid/view/View$OnClickListener;

    .line 1653
    new-instance v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$6;-><init>(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mSummaryHandler:Landroid/os/Handler;

    .line 118
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;)Landroid/widget/RelativeLayout;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mRootView:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;)Lcom/sec/android/app/ve/PreviewPlayerInterface;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mPreviewPlayer:Lcom/sec/android/app/ve/PreviewPlayerInterface;

    return-object v0
.end method

.method static synthetic access$10()I
    .locals 1

    .prologue
    .line 2207
    invoke-static {}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->updateBGMDurationList()I

    move-result v0

    return v0
.end method

.method static synthetic access$11()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 140
    sget-object v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mBGMDurationList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$12()Ljava/util/Map;
    .locals 1

    .prologue
    .line 141
    sget-object v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->bgmsAvailable:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$13(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 1653
    iget-object v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mSummaryHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$14(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;)Lcom/sec/android/app/storycam/view/Caption1Line;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->captionText:Lcom/sec/android/app/storycam/view/Caption1Line;

    return-object v0
.end method

.method static synthetic access$15(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;Z)V
    .locals 0

    .prologue
    .line 173
    iput-boolean p1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->wasEditing:Z

    return-void
.end method

.method static synthetic access$16(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;)Z
    .locals 1

    .prologue
    .line 146
    iget-boolean v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->isOkToResumeExport:Z

    return v0
.end method

.method static synthetic access$17(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;Z)V
    .locals 0

    .prologue
    .line 146
    iput-boolean p1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->isOkToResumeExport:Z

    return-void
.end method

.method static synthetic access$18(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;Z)V
    .locals 0

    .prologue
    .line 154
    iput-boolean p1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->isCaptionFcused:Z

    return-void
.end method

.method static synthetic access$19(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;I)V
    .locals 0

    .prologue
    .line 159
    iput p1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->cursorPos:I

    return-void
.end method

.method static synthetic access$2(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;Ljava/lang/String;Z)Z
    .locals 1

    .prologue
    .line 2137
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->recheckFiles(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method static synthetic access$20(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;)Z
    .locals 1

    .prologue
    .line 154
    iget-boolean v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->isCaptionFcused:Z

    return v0
.end method

.method static synthetic access$21(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;)I
    .locals 1

    .prologue
    .line 159
    iget v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->cursorPos:I

    return v0
.end method

.method static synthetic access$3(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->spaceString:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$4(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;)Lcom/sec/android/app/storycam/view/BGMView;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mBgmInstance:Lcom/sec/android/app/storycam/view/BGMView;

    return-object v0
.end method

.method static synthetic access$5(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mActivityEventCallbacks:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$6(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;)V
    .locals 0

    .prologue
    .line 1767
    invoke-direct {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->handleBackPressed()V

    return-void
.end method

.method static synthetic access$7()F
    .locals 1

    .prologue
    .line 129
    sget v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->fontScale_value:F

    return v0
.end method

.method static synthetic access$8(F)V
    .locals 0

    .prologue
    .line 129
    sput p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->fontScale_value:F

    return-void
.end method

.method static synthetic access$9(Z)V
    .locals 0

    .prologue
    .line 143
    sput-boolean p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->saveNeeded:Z

    return-void
.end method

.method private changeCaptionDividerWidth()V
    .locals 3

    .prologue
    .line 1359
    const v2, 0x7f0d0004

    invoke-virtual {p0, v2}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 1360
    .local v0, "captionLayout":Landroid/widget/RelativeLayout;
    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1361
    .local v1, "captionLayoutparams":Landroid/widget/RelativeLayout$LayoutParams;
    const v2, 0x7f060356

    invoke-static {v2}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v2

    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 1362
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1364
    return-void
.end method

.method private handleActivityDestruction(Z)V
    .locals 2
    .param p1, "clearListeners"    # Z

    .prologue
    .line 1607
    sget-object v1, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    if-eqz v1, :cond_0

    .line 1608
    sget-object v1, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v1}, Lcom/sec/android/app/storycam/AppDataManager;->clearAllImageCachesInEngine()V

    .line 1609
    sget-object v1, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v1, p1}, Lcom/sec/android/app/storycam/AppDataManager;->recycle(Z)V

    .line 1612
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mEditSessionTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    if-eqz v1, :cond_1

    .line 1613
    iget-object v1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mEditSessionTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->deleteAllClipArts()V

    .line 1614
    :cond_1
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mEditSessionTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    .line 1615
    if-eqz p1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mGlobalBroadcastReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_2

    .line 1617
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mGlobalBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1625
    :cond_2
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mExportWrapper:Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;

    invoke-virtual {v1}, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;->isExportRunning()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1626
    iget-object v1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mExportWrapper:Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;

    invoke-virtual {v1}, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;->stopExport()V

    .line 1627
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/NativeInterface;->_native_terminate()V

    .line 1630
    :cond_3
    invoke-static {}, Lcom/sec/android/app/storycam/summary/Summary;->deinit()V

    .line 1631
    return-void

    .line 1619
    :catch_0
    move-exception v0

    .line 1620
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0
.end method

.method private handleActivityInit()V
    .locals 4

    .prologue
    .line 1634
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1635
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/NativeInterface;->_native_initfimc()V

    .line 1636
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mRootView:Landroid/widget/RelativeLayout;

    if-eqz v2, :cond_1

    .line 1640
    iget-object v2, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mRootView:Landroid/widget/RelativeLayout;

    const v3, 0x7f0d0003

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    .line 1641
    .local v1, "pvg":Lcom/sec/android/app/storycam/view/PreviewViewGroup;
    const v2, 0x7f0d0041

    invoke-virtual {v1, v2}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/ve/PreviewPlayerInterface;

    .line 1642
    .local v0, "player":Lcom/sec/android/app/ve/PreviewPlayerInterface;
    invoke-virtual {v0}, Lcom/sec/android/app/ve/PreviewPlayerInterface;->isSurfaceAvailable()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1643
    invoke-virtual {v0}, Lcom/sec/android/app/ve/PreviewPlayerInterface;->setEngineSurface()V

    .line 1646
    .end local v0    # "player":Lcom/sec/android/app/ve/PreviewPlayerInterface;
    .end local v1    # "pvg":Lcom/sec/android/app/storycam/view/PreviewViewGroup;
    :cond_1
    sget-object v2, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    if-eqz v2, :cond_2

    .line 1647
    sget-object v2, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v2}, Lcom/sec/android/app/storycam/AppDataManager;->startFetchingThumbnails()V

    .line 1649
    :cond_2
    return-void
.end method

.method private handleBackPressed()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 1769
    sget-object v3, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;->STATE_DEFAULT:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    iget-object v4, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mCurrentActivityState:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    invoke-virtual {v3, v4}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 1770
    iget-object v3, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mPreviewPlayer:Lcom/sec/android/app/ve/PreviewPlayerInterface;

    if-eqz v3, :cond_0

    .line 1771
    iget-object v3, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mPreviewPlayer:Lcom/sec/android/app/ve/PreviewPlayerInterface;

    invoke-virtual {v3}, Lcom/sec/android/app/ve/PreviewPlayerInterface;->stop()V

    .line 1772
    :cond_0
    sget-boolean v3, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->saveNeeded:Z

    if-nez v3, :cond_1

    invoke-direct {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->isSaveNeeded()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1776
    :cond_1
    new-instance v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogSaveChanges;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogSaveChanges;-><init>(Landroid/app/Activity;)V

    .line 1777
    .local v0, "cancelDialog":Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogSaveChanges;
    sget-boolean v3, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mIsCancelDialogShown:Z

    if-eqz v3, :cond_3

    .line 1778
    invoke-static {p0}, Lcom/sec/android/app/ve/common/AndroidUtils;->dismissDialog(Landroid/app/Activity;)V

    .line 1779
    sput-boolean v5, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mIsCancelDialogShown:Z

    .line 1780
    invoke-static {v0, p0}, Lcom/sec/android/app/ve/common/AndroidUtils;->showDialog(Landroid/app/DialogFragment;Landroid/app/Activity;)V

    .line 1786
    :goto_0
    sput-boolean v6, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->saveNeeded:Z

    .line 1843
    .end local v0    # "cancelDialog":Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogSaveChanges;
    :cond_2
    :goto_1
    return-void

    .line 1782
    .restart local v0    # "cancelDialog":Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogSaveChanges;
    :cond_3
    sput-boolean v5, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mIsCancelDialogShown:Z

    .line 1783
    invoke-static {v0, p0}, Lcom/sec/android/app/ve/common/AndroidUtils;->showDialog(Landroid/app/DialogFragment;Landroid/app/Activity;)V

    goto :goto_0

    .line 1792
    .end local v0    # "cancelDialog":Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogSaveChanges;
    :cond_4
    invoke-static {}, Lcom/sec/android/app/storycam/summary/Summary;->stopSummary()V

    .line 1793
    if-eqz p0, :cond_2

    .line 1794
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->finish()V

    goto :goto_1

    .line 1796
    :cond_5
    sget-object v3, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;->STATE_EDIT:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    iget-object v4, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mCurrentActivityState:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    invoke-virtual {v3, v4}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_f

    .line 1797
    sget-object v3, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v3}, Lcom/sec/android/app/storycam/AppDataManager;->getOriginalTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v2

    .line 1798
    .local v2, "transElem":Lcom/samsung/app/video/editor/external/TranscodeElement;
    if-nez v2, :cond_6

    .line 1799
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->finish()V

    goto :goto_1

    .line 1802
    :cond_6
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_2
    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v3

    if-lt v1, v3, :cond_8

    .line 1813
    sget-object v3, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v3, v2}, Lcom/sec/android/app/storycam/AppDataManager;->are2UHDFilesInTransElement(Lcom/samsung/app/video/editor/external/TranscodeElement;)Z

    move-result v3

    if-nez v3, :cond_7

    sget-object v3, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v3}, Lcom/sec/android/app/storycam/AppDataManager;->getOriginalTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/storycam/Utils;->isTranscodeElementValid(Lcom/samsung/app/video/editor/external/TranscodeElement;)Z

    move-result v3

    if-eqz v3, :cond_7

    if-nez v2, :cond_a

    :cond_7
    sget-object v3, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v3}, Lcom/sec/android/app/storycam/AppDataManager;->getCurrentTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v3

    if-nez v3, :cond_a

    .line 1814
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->finish()V

    goto :goto_1

    .line 1803
    :cond_8
    invoke-virtual {v2, v1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElement(I)Lcom/samsung/app/video/editor/external/Element;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/Element;->getID()I

    move-result v3

    const/4 v4, -0x1

    if-ne v3, v4, :cond_9

    .line 1804
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->finish()V

    goto :goto_1

    .line 1802
    :cond_9
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1817
    :cond_a
    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v3

    if-nez v3, :cond_b

    .line 1818
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->finish()V

    goto :goto_1

    .line 1826
    :cond_b
    sget-object v3, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v3, v2}, Lcom/sec/android/app/storycam/AppDataManager;->are2UHDFilesInTransElement(Lcom/samsung/app/video/editor/external/TranscodeElement;)Z

    move-result v3

    if-nez v3, :cond_c

    sget-object v3, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v3}, Lcom/sec/android/app/storycam/AppDataManager;->getOriginalTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/storycam/Utils;->isTranscodeElementValid(Lcom/samsung/app/video/editor/external/TranscodeElement;)Z

    move-result v3

    if-nez v3, :cond_d

    :cond_c
    sget-object v3, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v3}, Lcom/sec/android/app/storycam/AppDataManager;->getCurrentTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v3

    if-eqz v3, :cond_d

    .line 1827
    sget-object v3, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;->STATE_DEFAULT:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    invoke-virtual {p0, v3, v6, v5}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->changeActivityState(Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;ZZ)V

    goto/16 :goto_1

    .line 1830
    :cond_d
    sget-object v3, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v3}, Lcom/sec/android/app/storycam/AppDataManager;->getOriginalTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mEditSessionTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    invoke-static {v3, v4}, Lcom/samsung/app/video/editor/external/TranscodeElement;->isTranscodeSame(Lcom/samsung/app/video/editor/external/TranscodeElement;Lcom/samsung/app/video/editor/external/TranscodeElement;)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 1831
    sget-object v3, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;->STATE_DEFAULT:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    invoke-virtual {p0, v3, v6, v5}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->changeActivityState(Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;ZZ)V

    .line 1832
    invoke-static {}, Lcom/sec/android/app/storycam/summary/Summary;->stopSummary()V

    goto/16 :goto_1

    .line 1834
    :cond_e
    sget-object v3, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;->STATE_DEFAULT:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    invoke-virtual {p0, v3, v5, v5}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->changeActivityState(Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;ZZ)V

    goto/16 :goto_1

    .line 1836
    .end local v1    # "i":I
    .end local v2    # "transElem":Lcom/samsung/app/video/editor/external/TranscodeElement;
    :cond_f
    sget-object v3, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;->STATE_BGM:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    iget-object v4, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mCurrentActivityState:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    invoke-virtual {v3, v4}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1837
    sget-object v3, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v3}, Lcom/sec/android/app/storycam/AppDataManager;->getCurrentTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v3

    if-nez v3, :cond_10

    .line 1838
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->finish()V

    goto/16 :goto_1

    .line 1840
    :cond_10
    sget-object v3, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;->STATE_DEFAULT:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    invoke-virtual {p0, v3, v5, v5}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->changeActivityState(Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;ZZ)V

    goto/16 :goto_1
.end method

.method private handleOrientationChange(Landroid/content/res/Configuration;Z)V
    .locals 13
    .param p1, "config"    # Landroid/content/res/Configuration;
    .param p2, "forceConfigChange"    # Z

    .prologue
    const/4 v12, 0x0

    const v11, 0x7f0d0003

    const/16 v10, 0x9

    const/4 v9, 0x3

    const/4 v8, 0x1

    .line 1717
    const v6, 0x7f0d0002

    invoke-virtual {p0, v6}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    .line 1718
    .local v2, "repositionLayout":Landroid/widget/RelativeLayout;
    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1720
    .local v1, "params":Landroid/widget/RelativeLayout$LayoutParams;
    const v6, 0x7f0d004c

    invoke-virtual {p0, v6}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout;

    .line 1721
    .local v4, "themeLayout":Landroid/widget/RelativeLayout;
    invoke-virtual {v4}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1723
    .local v5, "themeLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v6, -0x2

    iput v6, v5, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 1724
    const v6, 0x7f060313

    invoke-static {v6}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v6

    iput v6, v5, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 1725
    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1727
    const v6, 0x7f0d0004

    invoke-virtual {p0, v6}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 1728
    .local v0, "captionContainer":Landroid/widget/RelativeLayout;
    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1729
    .local v3, "tempParam":Landroid/widget/RelativeLayout$LayoutParams;
    const v6, 0x7f060356

    invoke-static {v6}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v6

    iput v6, v3, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 1730
    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1732
    iget-object v6, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->captionText:Lcom/sec/android/app/storycam/view/Caption1Line;

    invoke-virtual {v6}, Lcom/sec/android/app/storycam/view/Caption1Line;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    .end local v3    # "tempParam":Landroid/widget/RelativeLayout$LayoutParams;
    check-cast v3, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1733
    .restart local v3    # "tempParam":Landroid/widget/RelativeLayout$LayoutParams;
    const v6, 0x7f06036f

    invoke-static {v6}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v6

    iput v6, v3, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 1734
    const v6, 0x7f060373

    invoke-static {v6}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v6

    iput v6, v3, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 1735
    const v6, 0x7f060371

    invoke-static {v6}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v6

    iput v6, v3, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 1736
    const v6, 0x7f060372

    invoke-static {v6}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v6

    iput v6, v3, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 1737
    iget-object v6, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->captionText:Lcom/sec/android/app/storycam/view/Caption1Line;

    invoke-virtual {v6, v3}, Lcom/sec/android/app/storycam/view/Caption1Line;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1741
    iget v6, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v7, 0x2

    if-ne v6, v7, :cond_3

    .line 1743
    invoke-virtual {p1}, Landroid/content/res/Configuration;->getLayoutDirection()I

    move-result v6

    if-ne v6, v8, :cond_2

    .line 1744
    invoke-virtual {v1, v9}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 1745
    invoke-virtual {v1, v10}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 1746
    invoke-virtual {v1, v12, v11}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1760
    :cond_0
    :goto_0
    invoke-virtual {v2, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1761
    iget-object v6, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mRootView:Landroid/widget/RelativeLayout;

    if-eqz v6, :cond_1

    if-eqz p2, :cond_1

    .line 1762
    iget-object v6, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mRootView:Landroid/widget/RelativeLayout;

    invoke-virtual {v6, p1}, Landroid/widget/RelativeLayout;->dispatchConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1763
    :cond_1
    return-void

    .line 1749
    :cond_2
    invoke-virtual {v1, v9}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 1750
    invoke-virtual {v1, v10}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 1751
    invoke-virtual {v1, v8, v11}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    goto :goto_0

    .line 1754
    :cond_3
    iget v6, p1, Landroid/content/res/Configuration;->orientation:I

    if-ne v6, v8, :cond_0

    .line 1755
    invoke-virtual {v1, v8}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 1756
    invoke-virtual {v1, v12}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 1757
    invoke-virtual {v1, v10}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1758
    invoke-virtual {v1, v9, v11}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    goto :goto_0
.end method

.method private handleOrientationChangeEdit(Landroid/content/res/Configuration;)V
    .locals 11
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 1319
    const v9, 0x7f0d0027

    invoke-virtual {p0, v9}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    .line 1320
    .local v2, "editMediaLayout":Landroid/widget/RelativeLayout;
    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1322
    .local v3, "editMediaLayoutparams":Landroid/widget/RelativeLayout$LayoutParams;
    const v9, 0x7f0d002a

    invoke-virtual {p0, v9}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/HorizontalScrollView;

    .line 1323
    .local v6, "horizontalLayout":Landroid/widget/HorizontalScrollView;
    invoke-virtual {v6}, Landroid/widget/HorizontalScrollView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    check-cast v7, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1324
    .local v7, "horizontalLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    const v9, 0x7f0d0028

    invoke-virtual {p0, v9}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ScrollView;

    .line 1325
    .local v8, "verticalLayout":Landroid/widget/ScrollView;
    const v9, 0x7f0d002b

    invoke-virtual {p0, v9}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/GridLayout;

    .line 1326
    .local v5, "gridMediaLayout":Landroid/widget/GridLayout;
    invoke-virtual {v5}, Landroid/widget/GridLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/widget/FrameLayout$LayoutParams;

    .line 1328
    .local v4, "gridLayoutParams":Landroid/widget/FrameLayout$LayoutParams;
    const v9, 0x7f0d0004

    invoke-virtual {p0, v9}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 1329
    .local v0, "captionLayout":Landroid/widget/RelativeLayout;
    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1331
    .local v1, "captionLayoutparams":Landroid/widget/RelativeLayout$LayoutParams;
    iget v9, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v10, 0x2

    if-ne v9, v10, :cond_0

    .line 1332
    const/4 v9, 0x0

    invoke-virtual {v6, v9}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    .line 1333
    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 1334
    const v9, 0x7f06003b

    invoke-static {v9}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v9

    iput v9, v1, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 1335
    const v9, 0x7f06003b

    invoke-static {v9}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v9

    iput v9, v3, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 1336
    const v9, 0x7f06003a

    invoke-static {v9}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v9

    iput v9, v3, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 1337
    const v9, 0x7f06003b

    invoke-static {v9}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v9

    iput v9, v7, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 1338
    const v9, 0x7f06003a

    invoke-static {v9}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v9

    iput v9, v7, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 1339
    const v9, 0x7f06003b

    invoke-static {v9}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v9

    iput v9, v4, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 1340
    const v9, 0x7f06003a

    invoke-static {v9}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v9

    iput v9, v4, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 1341
    invoke-virtual {v5, v4}, Landroid/widget/GridLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1342
    invoke-virtual {v6, v7}, Landroid/widget/HorizontalScrollView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1355
    :goto_0
    return-void

    .line 1344
    :cond_0
    const/16 v9, 0x8

    invoke-virtual {v6, v9}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    .line 1345
    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 1346
    const v9, 0x7f060356

    invoke-static {v9}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v9

    iput v9, v1, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 1347
    const v9, 0x7f06003a

    invoke-static {v9}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v9

    iput v9, v3, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 1348
    const v9, 0x7f06003b

    invoke-static {v9}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v9

    iput v9, v3, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 1349
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1350
    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1351
    iget-object v9, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mRootView:Landroid/widget/RelativeLayout;

    invoke-virtual {v9}, Landroid/widget/RelativeLayout;->requestLayout()V

    .line 1352
    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->requestLayout()V

    goto :goto_0
.end method

.method private isSaveNeeded()Z
    .locals 3

    .prologue
    .line 842
    sget-object v2, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v2}, Lcom/sec/android/app/storycam/AppDataManager;->getInitialTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v0

    .line 843
    .local v0, "initialTrans":Lcom/samsung/app/video/editor/external/TranscodeElement;
    sget-object v2, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v2}, Lcom/sec/android/app/storycam/AppDataManager;->getOriginalTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v1

    .line 844
    .local v1, "origiTrans":Lcom/samsung/app/video/editor/external/TranscodeElement;
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 845
    invoke-static {v0, v1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->isTranscodeSame(Lcom/samsung/app/video/editor/external/TranscodeElement;Lcom/samsung/app/video/editor/external/TranscodeElement;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 846
    const/4 v2, 0x1

    sput-boolean v2, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->saveNeeded:Z

    .line 849
    :cond_0
    sget-boolean v2, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->saveNeeded:Z

    return v2
.end method

.method private prepareContentView()V
    .locals 6

    .prologue
    .line 1846
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/NativeInterface;->_native_initfimc()V

    .line 1847
    const/high16 v4, 0x7f030000

    invoke-virtual {p0, v4}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->setContentView(I)V

    .line 1848
    const/high16 v4, 0x7f0d0000

    invoke-virtual {p0, v4}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout;

    iput-object v4, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mRootView:Landroid/widget/RelativeLayout;

    .line 1852
    const v4, 0x7f0d0005

    invoke-virtual {p0, v4}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/storycam/view/Caption1Line;

    iput-object v4, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->captionText:Lcom/sec/android/app/storycam/view/Caption1Line;

    .line 1853
    const v4, 0x7f0d004c

    invoke-virtual {p0, v4}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    .line 1854
    .local v3, "themeLayout":Landroid/widget/RelativeLayout;
    const v4, 0x7f0d004d

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/HorizontalScrollView;

    iput-object v4, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->hScrollView:Landroid/widget/HorizontalScrollView;

    .line 1856
    iget-object v4, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->hScrollView:Landroid/widget/HorizontalScrollView;

    new-instance v5, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$11;

    invoke-direct {v5, p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$11;-><init>(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;)V

    invoke-virtual {v4, v5}, Landroid/widget/HorizontalScrollView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1891
    iget-object v4, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->captionText:Lcom/sec/android/app/storycam/view/Caption1Line;

    iget-object v5, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mDragListener:Landroid/view/View$OnDragListener;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/storycam/view/Caption1Line;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    .line 1892
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 1893
    .local v0, "config":Landroid/content/res/Configuration;
    iget v4, v0, Landroid/content/res/Configuration;->fontScale:F

    sput v4, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->fontScale_value:F

    .line 1894
    iput-object v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mConfig:Landroid/content/res/Configuration;

    .line 1895
    iget v4, v0, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    iput v4, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mHardKeyboardHidden:I

    .line 1896
    if-eqz v0, :cond_0

    iget v4, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v5, 0x1

    if-eq v4, v5, :cond_0

    .line 1897
    const/4 v4, 0x0

    invoke-direct {p0, v0, v4}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->handleOrientationChange(Landroid/content/res/Configuration;Z)V

    .line 1899
    :cond_0
    sget-object v4, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;->STATE_DEFAULT:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    sget-object v5, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;->STATE_DEFAULT:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    invoke-virtual {p0, v4, v5}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->notifyActivityStateListeners(Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;)V

    .line 1901
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 1902
    .local v1, "mediaVolumeIntentFilter":Landroid/content/IntentFilter;
    const-string v4, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v1, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1903
    const-string v4, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v1, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1904
    const-string v4, "android.intent.action.MEDIA_REMOVED"

    invoke-virtual {v1, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1905
    const-string v4, "android.intent.action.MEDIA_SCANNER_STARTED"

    invoke-virtual {v1, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1906
    const-string v4, "android.intent.action.MEDIA_SCANNER_FINISHED"

    invoke-virtual {v1, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1907
    const-string v4, "file"

    invoke-virtual {v1, v4}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 1908
    iget-object v4, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mGlobalBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v4, v1}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1910
    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    .line 1911
    .local v2, "otherFilters":Landroid/content/IntentFilter;
    const-string v4, "android.intent.action.BATTERY_LOW"

    invoke-virtual {v2, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1912
    const-string v4, "com.samsung.sec.android.clockpackage.alarm.ALARM_ALERT"

    invoke-virtual {v2, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1913
    const-string v4, "android.intent.action.PHONE_STATE"

    invoke-virtual {v2, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1914
    const-string v4, "com.samsung.cover.OPEN"

    invoke-virtual {v2, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1916
    iget-object v4, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mGlobalBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v4, v2}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1917
    invoke-direct {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->registerforSCover()V

    .line 1918
    return-void
.end method

.method private recheckFiles(Ljava/lang/String;Z)Z
    .locals 9
    .param p1, "fPath"    # Ljava/lang/String;
    .param p2, "exit"    # Z

    .prologue
    const/4 v1, 0x1

    .line 2138
    const/4 v6, 0x0

    .line 2139
    .local v6, "filesChanged":Z
    sget-object v0, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    if-eqz v0, :cond_3

    .line 2140
    sget-object v0, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v0}, Lcom/sec/android/app/storycam/AppDataManager;->getOriginalTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v7

    .line 2141
    .local v7, "originalTEle":Lcom/samsung/app/video/editor/external/TranscodeElement;
    if-eqz v7, :cond_3

    sget-object v0, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v0, v7, p1}, Lcom/sec/android/app/storycam/AppDataManager;->recheckFilesInTranscodeElement(Lcom/samsung/app/video/editor/external/TranscodeElement;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2142
    invoke-virtual {p0, v7}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->updateExportPath(Lcom/samsung/app/video/editor/external/TranscodeElement;)V

    .line 2144
    invoke-virtual {v7}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v0

    if-gtz v0, :cond_0

    if-eqz p2, :cond_0

    .line 2145
    const v0, 0x7f070301

    invoke-static {p0, v0, v1}, Lcom/sec/android/app/ve/VEApp;->showToast(Landroid/content/Context;II)Landroid/widget/Toast;

    .line 2146
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->finish()V

    move v0, v1

    .line 2181
    .end local v7    # "originalTEle":Lcom/samsung/app/video/editor/external/TranscodeElement;
    :goto_0
    return v0

    .line 2150
    .restart local v7    # "originalTEle":Lcom/samsung/app/video/editor/external/TranscodeElement;
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mCurrentActivityState:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    sget-object v3, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;->STATE_EDIT:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2151
    sget-object v0, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    iget-object v3, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mEditSessionTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    invoke-virtual {v0, v3, p1}, Lcom/sec/android/app/storycam/AppDataManager;->recheckFilesInTranscodeElement(Lcom/samsung/app/video/editor/external/TranscodeElement;Ljava/lang/String;)Z

    .line 2152
    iget-object v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mConfig:Landroid/content/res/Configuration;

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v1, :cond_4

    .line 2153
    const v0, 0x7f0d0029

    invoke-virtual {p0, v0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/storycam/view/MediaEditGridView;

    iput-object v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mediaEditGrid:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    .line 2157
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mediaEditGrid:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    if-eqz v0, :cond_1

    .line 2158
    iget-object v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mediaEditGrid:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    invoke-virtual {v0}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->refresh()V

    .line 2159
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->invalidateOptionsMenu()V

    .line 2165
    :cond_1
    invoke-static {v1}, Lcom/sec/android/app/storycam/summary/Summary;->setAnalysisRequired(Z)V

    .line 2166
    const/16 v8, 0xd

    .line 2167
    .local v8, "theme":I
    sget-object v0, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v0}, Lcom/sec/android/app/storycam/AppDataManager;->getCurrentTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2168
    sget-object v0, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v0}, Lcom/sec/android/app/storycam/AppDataManager;->getCurrentTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getThemeName()I

    move-result v8

    .line 2171
    :cond_2
    invoke-static {}, Lcom/sec/android/app/ve/theme/ThemeDataManager;->getInstance()Lcom/sec/android/app/ve/theme/ThemeDataManager;

    move-result-object v0

    invoke-virtual {v0, p0, v8}, Lcom/sec/android/app/ve/theme/ThemeDataManager;->createThemeProject(Landroid/content/Context;I)Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v2

    .line 2172
    .local v2, "newTElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    invoke-static {}, Lcom/sec/android/app/ve/theme/ThemeDataManager;->getInstance()Lcom/sec/android/app/ve/theme/ThemeDataManager;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/ve/VEApp;->gBGMManager:Lcom/sec/android/app/ve/bgm/BGMManager;

    invoke-virtual {v0, v2, v1}, Lcom/sec/android/app/ve/theme/ThemeDataManager;->modifyTranscodeWithBGMTranstionSlots(Lcom/samsung/app/video/editor/external/TranscodeElement;Lcom/sec/android/app/ve/bgm/BGMManager;)V

    .line 2173
    iget-object v1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mSummaryHandler:Landroid/os/Handler;

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/storycam/summary/Summary;->summarize(Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;Landroid/os/Handler;Lcom/samsung/app/video/editor/external/TranscodeElement;ZJ)V

    .line 2175
    sget-object v0, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v0}, Lcom/sec/android/app/storycam/AppDataManager;->notifyTranscodeElementChanges()V

    .line 2178
    const/4 v6, 0x1

    .end local v2    # "newTElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    .end local v7    # "originalTEle":Lcom/samsung/app/video/editor/external/TranscodeElement;
    .end local v8    # "theme":I
    :cond_3
    move v0, v6

    .line 2181
    goto :goto_0

    .line 2155
    .restart local v7    # "originalTEle":Lcom/samsung/app/video/editor/external/TranscodeElement;
    :cond_4
    const v0, 0x7f0d002b

    invoke-virtual {p0, v0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/storycam/view/MediaEditGridView;

    iput-object v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mediaEditGrid:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    goto :goto_1
.end method

.method private registerforSCover()V
    .locals 3

    .prologue
    .line 1921
    new-instance v1, Lcom/samsung/android/sdk/cover/Scover;

    invoke-direct {v1}, Lcom/samsung/android/sdk/cover/Scover;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mScover:Lcom/samsung/android/sdk/cover/Scover;

    .line 1923
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mScover:Lcom/samsung/android/sdk/cover/Scover;

    invoke-virtual {v1, p0}, Lcom/samsung/android/sdk/cover/Scover;->initialize(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/samsung/android/sdk/SsdkUnsupportedException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1929
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mScover:Lcom/samsung/android/sdk/cover/Scover;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/cover/Scover;->isFeatureEnabled(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1930
    new-instance v1, Lcom/samsung/android/sdk/cover/ScoverManager;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/cover/ScoverManager;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    .line 1931
    iget-object v1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    iget-object v2, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/cover/ScoverManager;->registerListener(Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;)V

    .line 1933
    :cond_0
    return-void

    .line 1924
    :catch_0
    move-exception v0

    .line 1925
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 1926
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 1927
    .local v0, "e":Lcom/samsung/android/sdk/SsdkUnsupportedException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/SsdkUnsupportedException;->printStackTrace()V

    goto :goto_0
.end method

.method private static updateBGMDurationList()I
    .locals 24

    .prologue
    .line 2208
    sget-object v19, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mBGMDurationList:Ljava/util/ArrayList;

    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->clear()V

    .line 2209
    sget-object v19, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->bgmsAvailable:Ljava/util/Map;

    invoke-interface/range {v19 .. v19}, Ljava/util/Map;->clear()V

    .line 2210
    sget-object v19, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/storycam/AppDataManager;->getCurrentTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v11

    .line 2212
    .local v11, "elements":I
    const/16 v19, 0x3

    move/from16 v0, v19

    new-array v14, v0, [J

    .line 2213
    .local v14, "modeDuration":[J
    const/16 v19, 0x3

    move/from16 v0, v19

    new-array v13, v0, [Ljava/lang/String;

    .line 2215
    .local v13, "mmssString":[Ljava/lang/String;
    sget-object v19, Lcom/sec/android/app/storycam/VEAppSpecific;->gBGMManager:Lcom/sec/android/app/ve/bgm/BGMManager;

    sget-object v20, Lcom/sec/android/app/ve/bgm/BGM$BGMModes;->FAST:Lcom/sec/android/app/ve/bgm/BGM$BGMModes;

    sget-object v21, Lcom/sec/android/app/storycam/VEAppSpecific;->gBGMManager:Lcom/sec/android/app/ve/bgm/BGMManager;

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/app/ve/bgm/BGMManager;->getSavedBGMPos()I

    move-result v21

    invoke-virtual/range {v19 .. v21}, Lcom/sec/android/app/ve/bgm/BGMManager;->calculateSlotDurations(Lcom/sec/android/app/ve/bgm/BGM$BGMModes;I)[I

    move-result-object v10

    .line 2217
    .local v10, "elementSlotDurations":[I
    const/4 v5, 0x0

    .local v5, "count":I
    :goto_0
    if-lt v5, v11, :cond_0

    .line 2220
    const/16 v19, 0x0

    const/16 v20, 0x0

    aget-wide v20, v14, v20

    sget-object v22, Lcom/sec/android/app/ve/util/CommonUtils$TimeInTextUnits;->MMSS:Lcom/sec/android/app/ve/util/CommonUtils$TimeInTextUnits;

    invoke-static/range {v20 .. v22}, Lcom/sec/android/app/ve/util/CommonUtils;->getTimeInText(JLcom/sec/android/app/ve/util/CommonUtils$TimeInTextUnits;)Ljava/lang/String;

    move-result-object v20

    aput-object v20, v13, v19

    .line 2223
    sget-object v19, Lcom/sec/android/app/storycam/VEAppSpecific;->gBGMManager:Lcom/sec/android/app/ve/bgm/BGMManager;

    sget-object v20, Lcom/sec/android/app/ve/bgm/BGM$BGMModes;->MEDIUM:Lcom/sec/android/app/ve/bgm/BGM$BGMModes;

    sget-object v21, Lcom/sec/android/app/storycam/VEAppSpecific;->gBGMManager:Lcom/sec/android/app/ve/bgm/BGMManager;

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/app/ve/bgm/BGMManager;->getSavedBGMPos()I

    move-result v21

    invoke-virtual/range {v19 .. v21}, Lcom/sec/android/app/ve/bgm/BGMManager;->calculateSlotDurations(Lcom/sec/android/app/ve/bgm/BGM$BGMModes;I)[I

    move-result-object v10

    .line 2225
    const/4 v5, 0x0

    :goto_1
    if-lt v5, v11, :cond_1

    .line 2228
    const/16 v19, 0x1

    const/16 v20, 0x1

    aget-wide v20, v14, v20

    sget-object v22, Lcom/sec/android/app/ve/util/CommonUtils$TimeInTextUnits;->MMSS:Lcom/sec/android/app/ve/util/CommonUtils$TimeInTextUnits;

    invoke-static/range {v20 .. v22}, Lcom/sec/android/app/ve/util/CommonUtils;->getTimeInText(JLcom/sec/android/app/ve/util/CommonUtils$TimeInTextUnits;)Ljava/lang/String;

    move-result-object v20

    aput-object v20, v13, v19

    .line 2232
    sget-object v19, Lcom/sec/android/app/storycam/VEAppSpecific;->gBGMManager:Lcom/sec/android/app/ve/bgm/BGMManager;

    sget-object v20, Lcom/sec/android/app/ve/bgm/BGM$BGMModes;->SLOW:Lcom/sec/android/app/ve/bgm/BGM$BGMModes;

    sget-object v21, Lcom/sec/android/app/storycam/VEAppSpecific;->gBGMManager:Lcom/sec/android/app/ve/bgm/BGMManager;

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/app/ve/bgm/BGMManager;->getSavedBGMPos()I

    move-result v21

    invoke-virtual/range {v19 .. v21}, Lcom/sec/android/app/ve/bgm/BGMManager;->calculateSlotDurations(Lcom/sec/android/app/ve/bgm/BGM$BGMModes;I)[I

    move-result-object v10

    .line 2234
    const/4 v5, 0x0

    :goto_2
    if-lt v5, v11, :cond_2

    .line 2237
    const/16 v19, 0x2

    const/16 v20, 0x2

    aget-wide v20, v14, v20

    sget-object v22, Lcom/sec/android/app/ve/util/CommonUtils$TimeInTextUnits;->MMSS:Lcom/sec/android/app/ve/util/CommonUtils$TimeInTextUnits;

    invoke-static/range {v20 .. v22}, Lcom/sec/android/app/ve/util/CommonUtils;->getTimeInText(JLcom/sec/android/app/ve/util/CommonUtils$TimeInTextUnits;)Ljava/lang/String;

    move-result-object v20

    aput-object v20, v13, v19

    .line 2240
    sget-object v19, Lcom/sec/android/app/storycam/VEAppSpecific;->gBGMManager:Lcom/sec/android/app/ve/bgm/BGMManager;

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/ve/bgm/BGMManager;->getBgmMode()Lcom/sec/android/app/ve/bgm/BGM$BGMModes;

    move-result-object v7

    .line 2241
    .local v7, "currentMode":Lcom/sec/android/app/ve/bgm/BGM$BGMModes;
    const/4 v6, 0x0

    .line 2242
    .local v6, "curModeIndex":I
    const/16 v19, 0x3

    move/from16 v0, v19

    new-array v4, v0, [Lcom/sec/android/app/ve/bgm/BGM$BGMModes;

    const/16 v19, 0x0

    sget-object v20, Lcom/sec/android/app/ve/bgm/BGM$BGMModes;->FAST:Lcom/sec/android/app/ve/bgm/BGM$BGMModes;

    aput-object v20, v4, v19

    const/16 v19, 0x1

    sget-object v20, Lcom/sec/android/app/ve/bgm/BGM$BGMModes;->MEDIUM:Lcom/sec/android/app/ve/bgm/BGM$BGMModes;

    aput-object v20, v4, v19

    const/16 v19, 0x2

    sget-object v20, Lcom/sec/android/app/ve/bgm/BGM$BGMModes;->SLOW:Lcom/sec/android/app/ve/bgm/BGM$BGMModes;

    aput-object v20, v4, v19

    .line 2244
    .local v4, "bgmModes":[Lcom/sec/android/app/ve/bgm/BGM$BGMModes;
    sget-object v19, Lcom/sec/android/app/ve/bgm/BGM$BGMModes;->FAST:Lcom/sec/android/app/ve/bgm/BGM$BGMModes;

    move-object/from16 v0, v19

    if-ne v7, v0, :cond_3

    .line 2245
    const/4 v6, 0x0

    .line 2254
    :goto_3
    const/16 v17, 0x0

    .line 2255
    .local v17, "x":I
    const/16 v16, 0x0

    .line 2256
    .local v16, "selectedIndex":I
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 2257
    .local v8, "durAdded":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v12, 0x0

    .local v12, "i":I
    move/from16 v18, v17

    .end local v17    # "x":I
    .local v18, "x":I
    :goto_4
    const/16 v19, 0x3

    move/from16 v0, v19

    if-lt v12, v0, :cond_5

    .line 2288
    return v16

    .line 2218
    .end local v4    # "bgmModes":[Lcom/sec/android/app/ve/bgm/BGM$BGMModes;
    .end local v6    # "curModeIndex":I
    .end local v7    # "currentMode":Lcom/sec/android/app/ve/bgm/BGM$BGMModes;
    .end local v8    # "durAdded":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v12    # "i":I
    .end local v16    # "selectedIndex":I
    .end local v18    # "x":I
    :cond_0
    const/16 v19, 0x0

    aget-wide v20, v14, v19

    array-length v0, v10

    move/from16 v22, v0

    rem-int v22, v5, v22

    aget v22, v10, v22

    move/from16 v0, v22

    int-to-long v0, v0

    move-wide/from16 v22, v0

    add-long v20, v20, v22

    aput-wide v20, v14, v19

    .line 2217
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_0

    .line 2226
    :cond_1
    const/16 v19, 0x1

    aget-wide v20, v14, v19

    array-length v0, v10

    move/from16 v22, v0

    rem-int v22, v5, v22

    aget v22, v10, v22

    move/from16 v0, v22

    int-to-long v0, v0

    move-wide/from16 v22, v0

    add-long v20, v20, v22

    aput-wide v20, v14, v19

    .line 2225
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_1

    .line 2235
    :cond_2
    const/16 v19, 0x2

    aget-wide v20, v14, v19

    array-length v0, v10

    move/from16 v22, v0

    rem-int v22, v5, v22

    aget v22, v10, v22

    move/from16 v0, v22

    int-to-long v0, v0

    move-wide/from16 v22, v0

    add-long v20, v20, v22

    aput-wide v20, v14, v19

    .line 2234
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_2

    .line 2247
    .restart local v4    # "bgmModes":[Lcom/sec/android/app/ve/bgm/BGM$BGMModes;
    .restart local v6    # "curModeIndex":I
    .restart local v7    # "currentMode":Lcom/sec/android/app/ve/bgm/BGM$BGMModes;
    :cond_3
    sget-object v19, Lcom/sec/android/app/ve/bgm/BGM$BGMModes;->MEDIUM:Lcom/sec/android/app/ve/bgm/BGM$BGMModes;

    move-object/from16 v0, v19

    if-ne v7, v0, :cond_4

    .line 2248
    const/4 v6, 0x1

    .line 2249
    goto :goto_3

    .line 2251
    :cond_4
    const/4 v6, 0x2

    goto :goto_3

    .line 2258
    .restart local v8    # "durAdded":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v12    # "i":I
    .restart local v16    # "selectedIndex":I
    .restart local v18    # "x":I
    :cond_5
    if-ne v12, v6, :cond_7

    .line 2259
    move/from16 v16, v18

    .line 2260
    sget-object v19, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->bgmsAvailable:Ljava/util/Map;

    add-int/lit8 v17, v18, 0x1

    .end local v18    # "x":I
    .restart local v17    # "x":I
    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    aget-object v21, v4, v12

    invoke-interface/range {v19 .. v21}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2261
    aget-object v19, v13, v12

    move-object/from16 v0, v19

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2263
    aget-wide v20, v14, v12

    sget-object v19, Lcom/sec/android/app/ve/util/CommonUtils$TimeInTextUnits;->MMSS:Lcom/sec/android/app/ve/util/CommonUtils$TimeInTextUnits;

    move-wide/from16 v0, v20

    move-object/from16 v2, v19

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/ve/util/CommonUtils;->getTimeInText(JLcom/sec/android/app/ve/util/CommonUtils$TimeInTextUnits;)Ljava/lang/String;

    move-result-object v19

    const-string v20, ":"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    .line 2265
    .local v9, "duration":[Ljava/lang/String;
    const/16 v19, 0x0

    aget-object v19, v9, v19

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v19

    if-lez v19, :cond_6

    .line 2266
    const v19, 0x7f07009a

    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/storycam/VEAppSpecific;->getStringValue(I)Ljava/lang/String;

    move-result-object v19

    const/16 v20, 0x2

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    const/16 v22, 0x0

    aget-object v22, v9, v22

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v22

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    aput-object v22, v20, v21

    const/16 v21, 0x1

    const/16 v22, 0x1

    aget-object v22, v9, v22

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v22

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    aput-object v22, v20, v21

    invoke-static/range {v19 .. v20}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    .line 2270
    .local v15, "s1":Ljava/lang/String;
    :goto_5
    sget-object v19, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mBGMDurationList:Ljava/util/ArrayList;

    move-object/from16 v0, v19

    invoke-virtual {v0, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2257
    .end local v9    # "duration":[Ljava/lang/String;
    .end local v15    # "s1":Ljava/lang/String;
    :goto_6
    add-int/lit8 v12, v12, 0x1

    move/from16 v18, v17

    .end local v17    # "x":I
    .restart local v18    # "x":I
    goto/16 :goto_4

    .line 2268
    .end local v18    # "x":I
    .restart local v9    # "duration":[Ljava/lang/String;
    .restart local v17    # "x":I
    :cond_6
    const v19, 0x7f0700d5

    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/storycam/VEAppSpecific;->getStringValue(I)Ljava/lang/String;

    move-result-object v19

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    const/16 v22, 0x1

    aget-object v22, v9, v22

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v22

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    aput-object v22, v20, v21

    invoke-static/range {v19 .. v20}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    .restart local v15    # "s1":Ljava/lang/String;
    goto :goto_5

    .line 2274
    .end local v9    # "duration":[Ljava/lang/String;
    .end local v15    # "s1":Ljava/lang/String;
    .end local v17    # "x":I
    .restart local v18    # "x":I
    :cond_7
    aget-object v19, v13, v12

    aget-object v20, v13, v6

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_9

    aget-object v19, v13, v12

    move-object/from16 v0, v19

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_9

    .line 2275
    sget-object v19, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->bgmsAvailable:Ljava/util/Map;

    add-int/lit8 v17, v18, 0x1

    .end local v18    # "x":I
    .restart local v17    # "x":I
    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    aget-object v21, v4, v12

    invoke-interface/range {v19 .. v21}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2276
    aget-object v19, v13, v12

    move-object/from16 v0, v19

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2277
    aget-wide v20, v14, v12

    sget-object v19, Lcom/sec/android/app/ve/util/CommonUtils$TimeInTextUnits;->MMSS:Lcom/sec/android/app/ve/util/CommonUtils$TimeInTextUnits;

    move-wide/from16 v0, v20

    move-object/from16 v2, v19

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/ve/util/CommonUtils;->getTimeInText(JLcom/sec/android/app/ve/util/CommonUtils$TimeInTextUnits;)Ljava/lang/String;

    move-result-object v19

    const-string v20, ":"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    .line 2279
    .restart local v9    # "duration":[Ljava/lang/String;
    const/16 v19, 0x0

    aget-object v19, v9, v19

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v19

    if-lez v19, :cond_8

    .line 2280
    const v19, 0x7f07009a

    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/storycam/VEAppSpecific;->getStringValue(I)Ljava/lang/String;

    move-result-object v19

    const/16 v20, 0x2

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    const/16 v22, 0x0

    aget-object v22, v9, v22

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v22

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    aput-object v22, v20, v21

    const/16 v21, 0x1

    const/16 v22, 0x1

    aget-object v22, v9, v22

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v22

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    aput-object v22, v20, v21

    invoke-static/range {v19 .. v20}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    .line 2284
    .restart local v15    # "s1":Ljava/lang/String;
    :goto_7
    sget-object v19, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mBGMDurationList:Ljava/util/ArrayList;

    move-object/from16 v0, v19

    invoke-virtual {v0, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_6

    .line 2282
    .end local v15    # "s1":Ljava/lang/String;
    :cond_8
    const v19, 0x7f0700d5

    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/storycam/VEAppSpecific;->getStringValue(I)Ljava/lang/String;

    move-result-object v19

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    const/16 v22, 0x1

    aget-object v22, v9, v22

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v22

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    aput-object v22, v20, v21

    invoke-static/range {v19 .. v20}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    .restart local v15    # "s1":Ljava/lang/String;
    goto :goto_7

    .end local v9    # "duration":[Ljava/lang/String;
    .end local v15    # "s1":Ljava/lang/String;
    .end local v17    # "x":I
    .restart local v18    # "x":I
    :cond_9
    move/from16 v17, v18

    .end local v18    # "x":I
    .restart local v17    # "x":I
    goto/16 :goto_6
.end method

.method private validateIncomingIntent(Landroid/content/Intent;Z)Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "isNew"    # Z

    .prologue
    .line 1597
    if-eqz p2, :cond_0

    .line 1598
    sget-object v1, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v1, p1}, Lcom/sec/android/app/storycam/AppDataManager;->validateIntentAndCreateTranscodeElement(Landroid/content/Intent;)Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;

    move-result-object v0

    .line 1602
    .local v0, "intentResult":Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;
    :goto_0
    sget-object v1, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v1}, Lcom/sec/android/app/storycam/AppDataManager;->getOriginalTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->updateExportPath(Lcom/samsung/app/video/editor/external/TranscodeElement;)V

    .line 1603
    return-object v0

    .line 1600
    .end local v0    # "intentResult":Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;
    :cond_0
    sget-object v1, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v1, p1}, Lcom/sec/android/app/storycam/AppDataManager;->validateIntentAndAddToTranscodeElement(Landroid/content/Intent;)Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;

    move-result-object v0

    .restart local v0    # "intentResult":Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;
    goto :goto_0
.end method


# virtual methods
.method public addActivityStateChangedListener(Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityStateChangedListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityStateChangedListener;

    .prologue
    .line 1543
    iget-object v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mActivityStateListeners:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 1544
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mActivityStateListeners:Ljava/util/ArrayList;

    .line 1546
    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mActivityStateListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1547
    iget-object v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mActivityStateListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1548
    :cond_1
    return-void
.end method

.method public beginexport(I)V
    .locals 8
    .param p1, "expResIndex"    # I

    .prologue
    const/4 v6, 0x0

    .line 758
    sget-object v3, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v3}, Lcom/sec/android/app/storycam/AppDataManager;->getCurrentTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v3

    if-nez v3, :cond_1

    .line 760
    const-string v3, "VE_LITE"

    const-string v4, " TRANSCODE NULL EXPORT CANNOT BE STARTED"

    invoke-static {v3, v4}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 783
    :cond_0
    :goto_0
    return-void

    .line 763
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mExportWrapper:Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;

    sget-object v4, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v4}, Lcom/sec/android/app/storycam/AppDataManager;->getCurrentTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v4

    new-instance v5, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExpAdapter;

    invoke-direct {v5, p0, v6}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExpAdapter;-><init>(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExpAdapter;)V

    invoke-virtual {v3, p0, v4, v5, p1}, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;->startExport(Landroid/app/Activity;Lcom/samsung/app/video/editor/external/TranscodeElement;Lcom/sec/android/app/ve/export/Export$Adapter;I)Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;

    move-result-object v2

    .line 764
    .local v2, "status":Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;
    sget-object v3, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;->FILENAME_NULL:Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;

    if-ne v2, v3, :cond_2

    .line 765
    invoke-static {}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogFileNameNull;->newInstance()Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogFileNameNull;

    move-result-object v0

    .line 766
    .local v0, "dialog":Landroid/app/DialogFragment;
    invoke-static {v0, p0}, Lcom/sec/android/app/ve/common/AndroidUtils;->showDialog(Landroid/app/DialogFragment;Landroid/app/Activity;)V

    goto :goto_0

    .line 767
    .end local v0    # "dialog":Landroid/app/DialogFragment;
    :cond_2
    sget-object v3, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;->FILENAME_EXISTS:Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;

    if-ne v2, v3, :cond_3

    .line 768
    new-instance v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogFileNameExist;

    iget-object v3, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mExportWrapper:Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;

    new-instance v4, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExpAdapter;

    invoke-direct {v4, p0, v6}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExpAdapter;-><init>(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExpAdapter;)V

    invoke-direct {v0, p0, v3, v4, p1}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogFileNameExist;-><init>(Landroid/app/Activity;Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExpAdapter;I)V

    .line 769
    .restart local v0    # "dialog":Landroid/app/DialogFragment;
    invoke-static {v0, p0}, Lcom/sec/android/app/ve/common/AndroidUtils;->showDialog(Landroid/app/DialogFragment;Landroid/app/Activity;)V

    goto :goto_0

    .line 770
    .end local v0    # "dialog":Landroid/app/DialogFragment;
    :cond_3
    sget-object v3, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;->EXPORT_NO_MEMORY:Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;

    if-ne v2, v3, :cond_0

    .line 773
    sget-object v3, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;->EXPORT_RES:[Ljava/lang/Integer;

    aget-object v3, v3, p1

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v3}, Lcom/samsung/app/video/editor/external/Constants;->getVideoBitRate(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x40

    int-to-long v4, v3

    .line 775
    sget-object v3, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v3}, Lcom/sec/android/app/storycam/AppDataManager;->getCurrentTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getTotalDuration()J

    move-result-wide v6

    .line 772
    mul-long/2addr v4, v6

    long-to-float v3, v4

    .line 777
    const/high16 v4, 0x4afa0000    # 8192000.0f

    .line 772
    div-float/2addr v3, v4

    .line 777
    const/high16 v4, 0x3f000000    # 0.5f

    .line 772
    add-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 779
    .local v1, "expectedOutputSizeBytes":I
    invoke-static {v1, p1}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DeviceStorageFullDialog;->newInstance(II)Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DeviceStorageFullDialog;

    move-result-object v0

    .line 780
    .restart local v0    # "dialog":Landroid/app/DialogFragment;
    invoke-static {v0, p0}, Lcom/sec/android/app/ve/common/AndroidUtils;->showDialog(Landroid/app/DialogFragment;Landroid/app/Activity;)V

    goto :goto_0
.end method

.method public changeActivityState(Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;ZZ)V
    .locals 23
    .param p1, "state"    # Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;
    .param p2, "commitSession"    # Z
    .param p3, "notifyListeners"    # Z

    .prologue
    .line 1389
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mCurrentActivityState:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 1390
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mCurrentActivityState:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    move-object/from16 v20, v0

    .line 1391
    .local v20, "previousState":Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mCurrentActivityState:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    .line 1392
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mCurrentActivityState:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    sget-object v3, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;->STATE_EDIT:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1393
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->requestFocusForMainActivity(Z)V

    .line 1394
    new-instance v2, Lcom/samsung/app/video/editor/external/TranscodeElement;

    sget-object v3, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v3}, Lcom/sec/android/app/storycam/AppDataManager;->getOriginalTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/app/video/editor/external/TranscodeElement;-><init>(Lcom/samsung/app/video/editor/external/TranscodeElement;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mEditSessionTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    .line 1395
    sget-object v2, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v2}, Lcom/sec/android/app/storycam/AppDataManager;->getCurrentTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v12

    .line 1396
    .local v12, "currentTranscodeElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    if-eqz v12, :cond_0

    invoke-virtual {v12}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getTextEleListCount()I

    move-result v2

    if-lez v2, :cond_0

    .line 1397
    invoke-virtual {v12}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getTextEleList()Ljava/util/List;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/app/video/editor/external/ClipartParams;

    iget-object v2, v2, Lcom/samsung/app/video/editor/external/ClipartParams;->data:Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mBeforeEditCaptionString:Ljava/lang/String;

    .line 1399
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mRootView:Landroid/widget/RelativeLayout;

    const v3, 0x7f0d0001

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1400
    invoke-static/range {p0 .. p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v16

    .line 1401
    .local v16, "inflater":Landroid/view/LayoutInflater;
    const v2, 0x7f030008

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mRootView:Landroid/widget/RelativeLayout;

    move-object/from16 v0, v16

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1405
    invoke-static/range {p0 .. p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v19

    .line 1406
    .local v19, "prefs":Landroid/content/SharedPreferences;
    const-string v2, "OVERLAY_HELP_SHOWN"

    const/4 v3, 0x0

    move-object/from16 v0, v19

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1407
    new-instance v15, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogHelpPage;

    invoke-direct {v15}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogHelpPage;-><init>()V

    .line 1408
    .local v15, "helplDialog":Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogHelpPage;
    move-object/from16 v0, p0

    invoke-static {v15, v0}, Lcom/sec/android/app/ve/common/AndroidUtils;->showDialog(Landroid/app/DialogFragment;Landroid/app/Activity;)V

    .line 1410
    .end local v15    # "helplDialog":Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogHelpPage;
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mConfig:Landroid/content/res/Configuration;

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x1

    if-eq v2, v3, :cond_2

    .line 1411
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mConfig:Landroid/content/res/Configuration;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->handleOrientationChangeEdit(Landroid/content/res/Configuration;)V

    .line 1413
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mActionBar:Landroid/app/ActionBar;

    if-eqz v2, :cond_3

    .line 1414
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v2}, Landroid/app/ActionBar;->getDisplayOptions()I

    move-result v18

    .line 1415
    .local v18, "options":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mActionBar:Landroid/app/ActionBar;

    or-int/lit8 v3, v18, 0x8

    invoke-virtual {v2, v3}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 1416
    sget-object v2, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v2}, Lcom/sec/android/app/storycam/AppDataManager;->getOriginalTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 1417
    invoke-static {}, Ljava/text/NumberFormat;->getInstance()Ljava/text/NumberFormat;

    move-result-object v17

    .line 1418
    .local v17, "numberFormat":Ljava/text/NumberFormat;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mActionBar:Landroid/app/ActionBar;

    new-instance v3, Ljava/lang/StringBuilder;

    const v6, 0x7f070077

    invoke-static {v6}, Lcom/sec/android/app/ve/VEApp;->getStringValue(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, " ("

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v6, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v6}, Lcom/sec/android/app/storycam/AppDataManager;->getOriginalTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v6

    int-to-long v6, v6

    move-object/from16 v0, v17

    invoke-virtual {v0, v6, v7}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, "/"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-wide/16 v6, 0xf

    move-object/from16 v0, v17

    invoke-virtual {v0, v6, v7}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, ")"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 1527
    .end local v12    # "currentTranscodeElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    .end local v16    # "inflater":Landroid/view/LayoutInflater;
    .end local v17    # "numberFormat":Ljava/text/NumberFormat;
    .end local v18    # "options":I
    .end local v19    # "prefs":Landroid/content/SharedPreferences;
    :cond_3
    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->invalidateOptionsMenu()V

    .line 1528
    if-eqz p3, :cond_4

    .line 1529
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mCurrentActivityState:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->notifyActivityStateListeners(Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;)V

    .line 1531
    .end local v20    # "previousState":Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;
    :cond_4
    return-void

    .line 1422
    .restart local v20    # "previousState":Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;
    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mCurrentActivityState:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    sget-object v3, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;->STATE_BGM:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 1423
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->requestFocusForMainActivity(Z)V

    .line 1424
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mRootView:Landroid/widget/RelativeLayout;

    const v3, 0x7f0d0001

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1425
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mRootView:Landroid/widget/RelativeLayout;

    const v3, 0x7f0d0004

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1426
    invoke-static/range {p0 .. p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v16

    .line 1427
    .restart local v16    # "inflater":Landroid/view/LayoutInflater;
    const v2, 0x7f030003

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mRootView:Landroid/widget/RelativeLayout;

    move-object/from16 v0, v16

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1428
    const v2, 0x7f0d000e

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/storycam/view/BGMView;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mBgmInstance:Lcom/sec/android/app/storycam/view/BGMView;

    .line 1429
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mActionBar:Landroid/app/ActionBar;

    if-eqz v2, :cond_6

    .line 1430
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v2}, Landroid/app/ActionBar;->getDisplayOptions()I

    move-result v18

    .line 1431
    .restart local v18    # "options":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mActionBar:Landroid/app/ActionBar;

    or-int/lit8 v3, v18, 0x8

    invoke-virtual {v2, v3}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 1432
    sget-object v2, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v2}, Lcom/sec/android/app/storycam/AppDataManager;->getOriginalTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v2

    if-eqz v2, :cond_6

    .line 1433
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mActionBar:Landroid/app/ActionBar;

    const v3, 0x7f070303

    invoke-static {v3}, Lcom/sec/android/app/ve/VEApp;->getStringValue(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 1436
    .end local v18    # "options":I
    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->captionText:Lcom/sec/android/app/storycam/view/Caption1Line;

    if-eqz v2, :cond_3

    .line 1437
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->captionText:Lcom/sec/android/app/storycam/view/Caption1Line;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Lcom/sec/android/app/storycam/view/Caption1Line;->setVisibility(I)V

    goto/16 :goto_0

    .line 1441
    .end local v16    # "inflater":Landroid/view/LayoutInflater;
    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mCurrentActivityState:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    sget-object v3, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;->STATE_DEFAULT:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_11

    sget-object v2, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;->STATE_EDIT:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_11

    .line 1442
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->requestFocusForMainActivity(Z)V

    .line 1443
    const v2, 0x7f0d0027

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/RelativeLayout;

    .line 1444
    .local v13, "editMediaScroll":Landroid/widget/RelativeLayout;
    if-eqz v13, :cond_b

    .line 1445
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/ve/common/AndroidUtils;->hideSoftKeyboard(Landroid/view/View;)V

    .line 1446
    if-eqz p2, :cond_c

    .line 1447
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mRootView:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v13}, Landroid/widget/RelativeLayout;->removeView(Landroid/view/View;)V

    .line 1448
    const/16 v22, 0xd

    .line 1449
    .local v22, "theme":I
    sget-object v2, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    sget-object v3, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v3}, Lcom/sec/android/app/storycam/AppDataManager;->getOriginalTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/storycam/AppDataManager;->setIsUHDFileInTranscodeElement(Lcom/samsung/app/video/editor/external/TranscodeElement;)V

    .line 1450
    sget-object v2, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v2}, Lcom/sec/android/app/storycam/AppDataManager;->getUHDCheckFlag()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 1451
    const v2, 0x7f07001c

    invoke-static {v2}, Lcom/sec/android/app/ve/VEApp;->getStringValue(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, "UHD/WQHD"

    aput-object v7, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v2, v3}, Lcom/sec/android/app/ve/VEApp;->showToast(Landroid/content/Context;Ljava/lang/String;I)V

    .line 1452
    :cond_8
    sget-object v2, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v2}, Lcom/sec/android/app/storycam/AppDataManager;->getCurrentTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v21

    .line 1453
    .local v21, "tCurrentElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    if-eqz v21, :cond_9

    sget-object v2, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v2}, Lcom/sec/android/app/storycam/AppDataManager;->getUHDCheckFlag()Z

    move-result v2

    if-nez v2, :cond_9

    .line 1454
    invoke-virtual/range {v21 .. v21}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getThemeName()I

    move-result v22

    .line 1457
    :cond_9
    sget-object v2, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v2}, Lcom/sec/android/app/storycam/AppDataManager;->getOriginalTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->updateExportPath(Lcom/samsung/app/video/editor/external/TranscodeElement;)V

    .line 1459
    invoke-static {}, Lcom/sec/android/app/ve/theme/ThemeDataManager;->getInstance()Lcom/sec/android/app/ve/theme/ThemeDataManager;

    move-result-object v2

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v2, v0, v1}, Lcom/sec/android/app/ve/theme/ThemeDataManager;->createThemeProject(Landroid/content/Context;I)Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v4

    .line 1460
    .local v4, "newTElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    invoke-static {}, Lcom/sec/android/app/ve/theme/ThemeDataManager;->getInstance()Lcom/sec/android/app/ve/theme/ThemeDataManager;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/ve/VEApp;->gBGMManager:Lcom/sec/android/app/ve/bgm/BGMManager;

    invoke-virtual {v2, v4, v3}, Lcom/sec/android/app/ve/theme/ThemeDataManager;->modifyTranscodeWithBGMTranstionSlots(Lcom/samsung/app/video/editor/external/TranscodeElement;Lcom/sec/android/app/ve/bgm/BGMManager;)V

    .line 1461
    const/4 v2, 0x1

    invoke-static {v2}, Lcom/sec/android/app/storycam/summary/Summary;->setAnalysisRequired(Z)V

    .line 1462
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mSummaryHandler:Landroid/os/Handler;

    const/4 v5, 0x0

    const-wide/16 v6, 0x0

    move-object/from16 v2, p0

    invoke-static/range {v2 .. v7}, Lcom/sec/android/app/storycam/summary/Summary;->summarize(Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;Landroid/os/Handler;Lcom/samsung/app/video/editor/external/TranscodeElement;ZJ)V

    .line 1491
    .end local v4    # "newTElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    .end local v21    # "tCurrentElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    .end local v22    # "theme":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mRootView:Landroid/widget/RelativeLayout;

    const v3, 0x7f0d0001

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1492
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->changeCaptionDividerWidth()V

    .line 1494
    const v2, 0x7f0d0005

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/storycam/view/Caption1Line;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->captionText:Lcom/sec/android/app/storycam/view/Caption1Line;

    .line 1495
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->captionText:Lcom/sec/android/app/storycam/view/Caption1Line;

    if-eqz v2, :cond_a

    .line 1496
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->captionText:Lcom/sec/android/app/storycam/view/Caption1Line;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/sec/android/app/storycam/view/Caption1Line;->setVisibility(I)V

    .line 1499
    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mConfig:Landroid/content/res/Configuration;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->handleOrientationChange(Landroid/content/res/Configuration;Z)V

    .line 1501
    :cond_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mActionBar:Landroid/app/ActionBar;

    if-eqz v2, :cond_3

    .line 1502
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mActionBar:Landroid/app/ActionBar;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 1465
    :cond_c
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mEditSessionTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    .line 1466
    .local v14, "editedTranscodeElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    sget-object v2, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v2, v14}, Lcom/sec/android/app/storycam/AppDataManager;->setOriginalTranscodeElement(Lcom/samsung/app/video/editor/external/TranscodeElement;)V

    .line 1467
    sget-object v2, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v2}, Lcom/sec/android/app/storycam/AppDataManager;->getOriginalTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->updateExportPath(Lcom/samsung/app/video/editor/external/TranscodeElement;)V

    .line 1468
    if-eqz p3, :cond_f

    .line 1469
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->spaceString:Ljava/lang/String;

    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->recheckFiles(Ljava/lang/String;Z)Z

    .line 1472
    :goto_2
    sget-object v2, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v2}, Lcom/sec/android/app/storycam/AppDataManager;->getCurrentTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v12

    .line 1473
    .restart local v12    # "currentTranscodeElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    if-eqz v12, :cond_d

    .line 1474
    invoke-virtual {v12}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getTextEleList()Ljava/util/List;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/app/video/editor/external/ClipartParams;

    .line 1476
    .local v5, "currentCaption":Lcom/samsung/app/video/editor/external/ClipartParams;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f02006c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    check-cast v9, Landroid/graphics/drawable/NinePatchDrawable;

    .line 1477
    .local v9, "npd":Landroid/graphics/drawable/NinePatchDrawable;
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mBeforeEditCaptionString:Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v8

    const v2, 0x7f06037e

    invoke-static {v2}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v10

    invoke-virtual/range {v5 .. v10}, Lcom/samsung/app/video/editor/external/ClipartParams;->changeCaptionData(Ljava/lang/String;Landroid/content/Context;Landroid/content/res/AssetManager;Landroid/graphics/drawable/NinePatchDrawable;I)V

    .line 1478
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->captionText:Lcom/sec/android/app/storycam/view/Caption1Line;

    if-eqz v2, :cond_d

    .line 1479
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mBeforeEditCaptionString:Ljava/lang/String;

    if-eqz v2, :cond_10

    .line 1480
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->captionText:Lcom/sec/android/app/storycam/view/Caption1Line;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mBeforeEditCaptionString:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/storycam/view/Caption1Line;->setText(Ljava/lang/CharSequence;)V

    .line 1486
    .end local v5    # "currentCaption":Lcom/samsung/app/video/editor/external/ClipartParams;
    .end local v9    # "npd":Landroid/graphics/drawable/NinePatchDrawable;
    :cond_d
    :goto_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mRootView:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v13}, Landroid/widget/RelativeLayout;->removeView(Landroid/view/View;)V

    .line 1487
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mEditSessionTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    if-eqz v2, :cond_e

    .line 1488
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mEditSessionTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/TranscodeElement;->deleteAllClipArts()V

    .line 1489
    :cond_e
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mEditSessionTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    goto/16 :goto_1

    .line 1471
    .end local v12    # "currentTranscodeElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    :cond_f
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->spaceString:Ljava/lang/String;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->recheckFiles(Ljava/lang/String;Z)Z

    goto :goto_2

    .line 1482
    .restart local v5    # "currentCaption":Lcom/samsung/app/video/editor/external/ClipartParams;
    .restart local v9    # "npd":Landroid/graphics/drawable/NinePatchDrawable;
    .restart local v12    # "currentTranscodeElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    :cond_10
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->captionText:Lcom/sec/android/app/storycam/view/Caption1Line;

    const-string v3, ""

    invoke-virtual {v2, v3}, Lcom/sec/android/app/storycam/view/Caption1Line;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 1504
    .end local v5    # "currentCaption":Lcom/samsung/app/video/editor/external/ClipartParams;
    .end local v9    # "npd":Landroid/graphics/drawable/NinePatchDrawable;
    .end local v12    # "currentTranscodeElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    .end local v13    # "editMediaScroll":Landroid/widget/RelativeLayout;
    .end local v14    # "editedTranscodeElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    :cond_11
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mCurrentActivityState:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    sget-object v3, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;->STATE_DEFAULT:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    sget-object v2, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;->STATE_BGM:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1505
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->requestFocusForMainActivity(Z)V

    .line 1506
    const v2, 0x7f0d000d

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/RelativeLayout;

    .line 1507
    .local v11, "bgmParent":Landroid/widget/RelativeLayout;
    if-eqz v11, :cond_14

    .line 1508
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mRootView:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v11}, Landroid/widget/RelativeLayout;->removeView(Landroid/view/View;)V

    .line 1509
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mRootView:Landroid/widget/RelativeLayout;

    const v3, 0x7f0d0001

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1510
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mRootView:Landroid/widget/RelativeLayout;

    const v3, 0x7f0d0004

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1511
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->captionText:Lcom/sec/android/app/storycam/view/Caption1Line;

    if-eqz v2, :cond_12

    .line 1512
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->captionText:Lcom/sec/android/app/storycam/view/Caption1Line;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/sec/android/app/storycam/view/Caption1Line;->setVisibility(I)V

    .line 1515
    :cond_12
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mActionBar:Landroid/app/ActionBar;

    if-eqz v2, :cond_13

    .line 1516
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mActionBar:Landroid/app/ActionBar;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 1517
    :cond_13
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mConfig:Landroid/content/res/Configuration;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->handleOrientationChange(Landroid/content/res/Configuration;Z)V

    .line 1519
    :cond_14
    if-eqz p2, :cond_15

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mBgmInstance:Lcom/sec/android/app/storycam/view/BGMView;

    invoke-virtual {v2}, Lcom/sec/android/app/storycam/view/BGMView;->isBGMChanged()Z

    move-result v2

    if-eqz v2, :cond_15

    .line 1520
    const/4 v2, 0x1

    sput-boolean v2, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->saveNeeded:Z

    .line 1521
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mBgmInstance:Lcom/sec/android/app/storycam/view/BGMView;

    invoke-virtual {v2}, Lcom/sec/android/app/storycam/view/BGMView;->saveBgm()V

    goto/16 :goto_0

    .line 1524
    :cond_15
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mBgmInstance:Lcom/sec/android/app/storycam/view/BGMView;

    invoke-virtual {v2}, Lcom/sec/android/app/storycam/view/BGMView;->discardBgm()V

    goto/16 :goto_0
.end method

.method public checkVEFullAndProceed()V
    .locals 3

    .prologue
    .line 2532
    const-string v1, "com.sec.android.app.ve"

    invoke-static {p0, v1}, Lcom/sec/android/app/ve/util/CommonUtils;->isPackageInstalled(Landroid/app/Activity;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2534
    const-string v1, "com.sec.android.app.ve"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->launchVideoEditor(Ljava/lang/String;)V

    .line 2568
    :goto_0
    return-void

    .line 2537
    :cond_0
    const-string v1, "com.sec.android.app.vefull"

    invoke-static {p0, v1}, Lcom/sec/android/app/ve/util/CommonUtils;->isPackageInstalled(Landroid/app/Activity;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2539
    const-string v1, "com.sec.android.app.vefull"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->launchVideoEditor(Ljava/lang/String;)V

    goto :goto_0

    .line 2541
    :cond_1
    const-string v1, "com.sec.android.app.samsungapps"

    invoke-static {p0, v1}, Lcom/sec/android/app/ve/util/CommonUtils;->isPackageInstalled(Landroid/app/Activity;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2543
    const-string v1, "com.sec.android.app.samsungapps"

    invoke-static {p0, v1}, Lcom/sec/android/app/ve/util/CommonUtils;->isPackageEnabled(Landroid/app/Activity;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2546
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2547
    .local v0, "i":Landroid/content/Intent;
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-lt v1, v2, :cond_2

    .line 2549
    const-string v1, "samsungapps://ProductDetail/com.sec.android.app.vefull"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2553
    :goto_1
    const/high16 v1, 0x14000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 2554
    invoke-virtual {p0, v0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 2551
    :cond_2
    const-string v1, "samsungapps://ProductDetail/com.sec.android.app.ve"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    goto :goto_1

    .line 2558
    .end local v0    # "i":Landroid/content/Intent;
    :cond_3
    invoke-static {p0}, Lcom/sec/android/app/ve/common/AndroidUtils;->dismissDialog(Landroid/app/Activity;)V

    .line 2559
    new-instance v1, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$EnableAppDialog;

    invoke-direct {v1}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$EnableAppDialog;-><init>()V

    invoke-static {v1, p0}, Lcom/sec/android/app/ve/common/AndroidUtils;->showDialog(Landroid/app/DialogFragment;Landroid/app/Activity;)V

    goto :goto_0

    .line 2564
    :cond_4
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2565
    .restart local v0    # "i":Landroid/content/Intent;
    const-string v1, "http://apps.samsung.com/mw/apps311.as"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2566
    invoke-virtual {p0, v0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public getCurrentConfig()Landroid/content/res/Configuration;
    .locals 1

    .prologue
    .line 2437
    iget-object v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mConfig:Landroid/content/res/Configuration;

    return-object v0
.end method

.method public getCurrentState()Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;
    .locals 1

    .prologue
    .line 1381
    iget-object v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mCurrentActivityState:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    return-object v0
.end method

.method public getPreviewPlayer()Lcom/sec/android/app/ve/PreviewPlayerInterface;
    .locals 1

    .prologue
    .line 1573
    iget-object v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mPreviewPlayer:Lcom/sec/android/app/ve/PreviewPlayerInterface;

    return-object v0
.end method

.method public getShareableTranscode(Lcom/samsung/app/video/editor/external/TranscodeElement;)Lcom/samsung/app/video/editor/external/TranscodeElement;
    .locals 18
    .param p1, "tElement"    # Lcom/samsung/app/video/editor/external/TranscodeElement;

    .prologue
    .line 2592
    new-instance v13, Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-object/from16 v0, p1

    invoke-direct {v13, v0}, Lcom/samsung/app/video/editor/external/TranscodeElement;-><init>(Lcom/samsung/app/video/editor/external/TranscodeElement;)V

    .line 2598
    .local v13, "shareableTranscode":Lcom/samsung/app/video/editor/external/TranscodeElement;
    invoke-virtual {v13}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementList()Ljava/util/List;

    move-result-object v12

    .line 2599
    .local v12, "shareableElementList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    sget-object v14, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v14}, Lcom/sec/android/app/storycam/AppDataManager;->getOriginalTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v10

    .line 2600
    .local v10, "originalTranscode":Lcom/samsung/app/video/editor/external/TranscodeElement;
    const/4 v5, 0x0

    .line 2601
    .local v5, "i":I
    invoke-interface {v12}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :cond_0
    :goto_0
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-nez v15, :cond_2

    .line 2615
    invoke-virtual {v13}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getTextEleList()Ljava/util/List;

    move-result-object v14

    const/4 v15, 0x0

    invoke-interface {v14, v15}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/app/video/editor/external/ClipartParams;

    .line 2616
    .local v2, "caption":Lcom/samsung/app/video/editor/external/ClipartParams;
    iget-object v14, v2, Lcom/samsung/app/video/editor/external/ClipartParams;->data:Ljava/lang/String;

    if-eqz v14, :cond_1

    iget-object v14, v2, Lcom/samsung/app/video/editor/external/ClipartParams;->data:Ljava/lang/String;

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v14

    if-nez v14, :cond_4

    .line 2617
    :cond_1
    invoke-virtual {v13}, Lcom/samsung/app/video/editor/external/TranscodeElement;->removeTextList()V

    .line 2638
    :goto_1
    return-object v13

    .line 2601
    .end local v2    # "caption":Lcom/samsung/app/video/editor/external/ClipartParams;
    :cond_2
    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/app/video/editor/external/Element;

    .line 2602
    .local v4, "element":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {v10}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementList()Ljava/util/List;

    move-result-object v15

    invoke-interface {v15}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :cond_3
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v16

    if-eqz v16, :cond_0

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/app/video/editor/external/Element;

    .line 2603
    .local v9, "origEle":Lcom/samsung/app/video/editor/external/Element;
    iget-object v0, v4, Lcom/samsung/app/video/editor/external/Element;->filePath:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-virtual {v9}, Lcom/samsung/app/video/editor/external/Element;->getFilePath()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_3

    .line 2604
    invoke-virtual {v9}, Lcom/samsung/app/video/editor/external/Element;->getDuration()J

    move-result-wide v16

    move-wide/from16 v0, v16

    invoke-virtual {v4, v0, v1}, Lcom/samsung/app/video/editor/external/Element;->setDuration(J)V

    .line 2605
    add-int/lit8 v8, v5, 0x1

    .end local v5    # "i":I
    .local v8, "i":I
    invoke-virtual {v4, v5}, Lcom/samsung/app/video/editor/external/Element;->setGroupID(I)V

    move v5, v8

    .line 2606
    .end local v8    # "i":I
    .restart local v5    # "i":I
    goto :goto_0

    .line 2620
    .end local v4    # "element":Lcom/samsung/app/video/editor/external/Element;
    .end local v9    # "origEle":Lcom/samsung/app/video/editor/external/Element;
    .restart local v2    # "caption":Lcom/samsung/app/video/editor/external/ClipartParams;
    :cond_4
    invoke-static {v2}, Lcom/samsung/app/video/editor/external/ClipartParams;->getDeepCopyOf(Lcom/samsung/app/video/editor/external/ClipartParams;)Lcom/samsung/app/video/editor/external/ClipartParams;

    move-result-object v11

    .line 2621
    .local v11, "shareableCaption":Lcom/samsung/app/video/editor/external/ClipartParams;
    const/4 v14, 0x0

    invoke-interface {v12, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/app/video/editor/external/Element;

    .line 2622
    .local v3, "ele":Lcom/samsung/app/video/editor/external/Element;
    const/4 v14, 0x0

    invoke-virtual {v11, v14}, Lcom/samsung/app/video/editor/external/ClipartParams;->setStoryboardStartFrame(I)V

    .line 2623
    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/Element;->getEndTime()J

    move-result-wide v14

    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/Element;->getStartTime()J

    move-result-wide v16

    sub-long v14, v14, v16

    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/Element;->getOverLapAtEnd()I

    move-result v16

    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/Element;->getOverLapAtStart()I

    move-result v17

    add-int v16, v16, v17

    move/from16 v0, v16

    int-to-long v0, v0

    move-wide/from16 v16, v0

    sub-long v6, v14, v16

    .line 2624
    .local v6, "endTime":J
    long-to-float v14, v6

    const/high16 v15, 0x447a0000    # 1000.0f

    div-float/2addr v14, v15

    const/high16 v15, 0x41f00000    # 30.0f

    mul-float/2addr v14, v15

    invoke-static {v14}, Ljava/lang/Math;->round(F)I

    move-result v14

    add-int/lit8 v14, v14, -0x1

    invoke-virtual {v11, v14}, Lcom/samsung/app/video/editor/external/ClipartParams;->setStoryboardEndFrame(I)V

    .line 2625
    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/Element;->getGroupID()I

    move-result v14

    invoke-virtual {v11, v14}, Lcom/samsung/app/video/editor/external/ClipartParams;->setElementID(I)V

    .line 2629
    const/4 v14, 0x3

    iput v14, v11, Lcom/samsung/app/video/editor/external/ClipartParams;->themeId:I

    .line 2632
    invoke-virtual {v13, v2}, Lcom/samsung/app/video/editor/external/TranscodeElement;->removeTextEleList(Lcom/samsung/app/video/editor/external/ClipartParams;)V

    .line 2633
    invoke-virtual {v13, v11}, Lcom/samsung/app/video/editor/external/TranscodeElement;->addTextEleList(Lcom/samsung/app/video/editor/external/ClipartParams;)V

    goto/16 :goto_1
.end method

.method public getSummaryHandler()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 2031
    iget-object v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mSummaryHandler:Landroid/os/Handler;

    return-object v0
.end method

.method public getUserSelectedBgmpos()I
    .locals 1

    .prologue
    .line 2743
    iget v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mSelectedBGMPos:I

    return v0
.end method

.method public isBgmStateOn()Z
    .locals 1

    .prologue
    .line 2763
    iget-boolean v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mIsBgmOn:Z

    return v0
.end method

.method public isUserSelectedBgm()Z
    .locals 1

    .prologue
    .line 2753
    iget-boolean v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->isUserSelected:Z

    return v0
.end method

.method public launchDialog(I)V
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 2188
    packed-switch p1, :pswitch_data_0

    .line 2203
    :goto_0
    return-void

    .line 2190
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->saveCaption()V

    .line 2191
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->requestFocusForMainActivity(Z)V

    .line 2192
    invoke-static {p0}, Lcom/sec/android/app/ve/common/AndroidUtils;->dismissDialog(Landroid/app/Activity;)V

    .line 2193
    new-instance v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$BGMModesDialog;

    invoke-direct {v0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$BGMModesDialog;-><init>()V

    invoke-static {v0, p0}, Lcom/sec/android/app/ve/common/AndroidUtils;->showDialog(Landroid/app/DialogFragment;Landroid/app/Activity;)V

    goto :goto_0

    .line 2196
    :pswitch_1
    invoke-static {p0}, Lcom/sec/android/app/ve/common/AndroidUtils;->dismissDialog(Landroid/app/Activity;)V

    .line 2197
    new-instance v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$LaunchVideoEditorFullDialog;

    invoke-direct {v0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$LaunchVideoEditorFullDialog;-><init>()V

    invoke-static {v0, p0}, Lcom/sec/android/app/ve/common/AndroidUtils;->showDialog(Landroid/app/DialogFragment;Landroid/app/Activity;)V

    goto :goto_0

    .line 2188
    nop

    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public launchGalleryForAddMedia()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1578
    sget-object v3, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v3}, Lcom/sec/android/app/storycam/AppDataManager;->getOriginalTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v2

    .line 1579
    .local v2, "trans":Lcom/samsung/app/video/editor/external/TranscodeElement;
    if-nez v2, :cond_1

    const/16 v3, 0xf

    :goto_0
    rsub-int/lit8 v0, v3, 0xf

    .line 1580
    .local v0, "count":I
    if-lez v0, :cond_0

    .line 1581
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 1582
    .local v1, "intent":Landroid/content/Intent;
    const-string v3, "*/*"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 1583
    const-string v3, "android.intent.action.GET_CONTENT"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1584
    const-string v3, "multi-pick"

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1585
    const-string v3, "pick-max-item"

    invoke-virtual {v1, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1586
    const-string v3, "pick-min-item"

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1587
    const-string v3, "android.intent.extra.LOCAL_ONLY"

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1588
    const-string v3, "com.sec.android.gallery3d"

    const-string v4, "com.sec.android.gallery3d.app.Gallery"

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1589
    const/16 v3, 0xff

    invoke-virtual {p0, v1, v3}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1593
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_0
    return-void

    .line 1579
    .end local v0    # "count":I
    :cond_1
    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v3

    goto :goto_0
.end method

.method public launchVideoEditor(Ljava/lang/String;)V
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 2571
    sget-object v3, Lcom/sec/android/app/ve/VEApp;->gProjectPluginManager:Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework;

    if-nez v3, :cond_0

    .line 2572
    new-instance v3, Lcom/sec/android/app/storycam/projectsharing/ProjectPluginManager;

    invoke-direct {v3}, Lcom/sec/android/app/storycam/projectsharing/ProjectPluginManager;-><init>()V

    sput-object v3, Lcom/sec/android/app/ve/VEApp;->gProjectPluginManager:Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework;

    .line 2574
    :cond_0
    sget-object v3, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v3}, Lcom/sec/android/app/storycam/AppDataManager;->getCurrentTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->getShareableTranscode(Lcom/samsung/app/video/editor/external/TranscodeElement;)Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v1

    .line 2576
    .local v1, "shareableTranscode":Lcom/samsung/app/video/editor/external/TranscodeElement;
    if-nez v1, :cond_2

    .line 2589
    :cond_1
    :goto_0
    return-void

    .line 2579
    :cond_2
    invoke-static {v1}, Lcom/sec/android/app/ve/projectsharing/ProjectCompatibilityHelper;->createProjectJSONFile(Lcom/samsung/app/video/editor/external/TranscodeElement;)Ljava/lang/String;

    move-result-object v0

    .line 2582
    .local v0, "jsonFilePath":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 2583
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.EDIT"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2584
    .local v2, "videoEditorIntent":Landroid/content/Intent;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, ".activity.ProjectEditActivity"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, p1, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2585
    const-string v3, "FROM_VE_CLIP"

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2586
    const-string v3, "JSON_FILEPATH"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2587
    invoke-virtual {p0, v2}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public notifyActivityStateListeners(Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;)V
    .locals 3
    .param p1, "oldState"    # Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;
    .param p2, "currentState"    # Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    .prologue
    .line 1559
    iget-object v1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mActivityStateListeners:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 1560
    iget-object v1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mActivityStateListeners:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1564
    :cond_0
    return-void

    .line 1560
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityStateChangedListener;

    .line 1561
    .local v0, "callback":Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityStateChangedListener;
    invoke-interface {v0, p1, p2}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityStateChangedListener;->onActivityStateChaged(Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;)V

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 7
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v6, 0x0

    const/4 v4, -0x1

    const/4 v5, 0x1

    .line 2036
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    .line 2038
    packed-switch p1, :pswitch_data_0

    .line 2082
    :cond_0
    :goto_0
    return-void

    .line 2040
    :pswitch_0
    if-eq p2, v4, :cond_0

    .line 2041
    iget-object v4, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mRootView:Landroid/widget/RelativeLayout;

    if-eqz v4, :cond_0

    .line 2042
    iget-object v4, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mRootView:Landroid/widget/RelativeLayout;

    const v5, 0x7f0d0003

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    .line 2043
    .local v2, "pvg":Lcom/sec/android/app/storycam/view/PreviewViewGroup;
    if-eqz v2, :cond_0

    .line 2044
    invoke-virtual {v2}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->refreshPreviewToCurrentSeekBar()V

    goto :goto_0

    .line 2049
    .end local v2    # "pvg":Lcom/sec/android/app/storycam/view/PreviewViewGroup;
    :pswitch_1
    if-ne p2, v4, :cond_0

    .line 2050
    invoke-direct {p0, p3, v6}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->validateIncomingIntent(Landroid/content/Intent;Z)Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;

    move-result-object v0

    .line 2052
    .local v0, "intentResult":Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;
    invoke-static {v5}, Lcom/sec/android/app/storycam/summary/Summary;->setAnalysisRequired(Z)V

    .line 2053
    sget-object v4, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;->STATE_EDIT:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    invoke-virtual {p0, v4, v6, v5}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->changeActivityState(Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;ZZ)V

    .line 2055
    iget-object v4, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mConfig:Landroid/content/res/Configuration;

    iget v4, v4, Landroid/content/res/Configuration;->orientation:I

    if-ne v4, v5, :cond_2

    .line 2056
    const v4, 0x7f0d0029

    invoke-virtual {p0, v4}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/storycam/view/MediaEditGridView;

    iput-object v4, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mediaEditGrid:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    .line 2060
    :goto_1
    iget-object v4, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mediaEditGrid:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    if-eqz v4, :cond_1

    .line 2061
    iget-object v4, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mediaEditGrid:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    invoke-virtual {v4}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->refresh()V

    .line 2062
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->invalidateOptionsMenu()V

    .line 2065
    :cond_1
    sget-object v4, Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;->COUNT_EXCEEDS_LIMIT:Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;

    if-ne v0, v4, :cond_3

    .line 2066
    new-instance v1, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogInputMaxLimit;

    invoke-direct {v1}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogInputMaxLimit;-><init>()V

    .line 2067
    .local v1, "maxLimitDialog":Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogInputMaxLimit;
    invoke-static {v1, p0}, Lcom/sec/android/app/ve/common/AndroidUtils;->showDialog(Landroid/app/DialogFragment;Landroid/app/Activity;)V

    goto :goto_0

    .line 2058
    .end local v1    # "maxLimitDialog":Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogInputMaxLimit;
    :cond_2
    const v4, 0x7f0d002b

    invoke-virtual {p0, v4}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/storycam/view/MediaEditGridView;

    iput-object v4, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mediaEditGrid:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    goto :goto_1

    .line 2068
    :cond_3
    sget-object v4, Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;->SOME_FILES_SUPPORTED:Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;

    if-eq v0, v4, :cond_4

    sget-object v4, Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;->NONE_SUPPORTED:Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;

    if-ne v0, v4, :cond_5

    .line 2069
    :cond_4
    new-instance v3, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$UnsupportedContentsDialog;

    invoke-direct {v3}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$UnsupportedContentsDialog;-><init>()V

    .line 2070
    .local v3, "unsupportedDialog":Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$UnsupportedContentsDialog;
    invoke-static {v3, p0}, Lcom/sec/android/app/ve/common/AndroidUtils;->showDialog(Landroid/app/DialogFragment;Landroid/app/Activity;)V

    goto :goto_0

    .line 2071
    .end local v3    # "unsupportedDialog":Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$UnsupportedContentsDialog;
    :cond_5
    sget-object v4, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    sget-object v5, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v5}, Lcom/sec/android/app/storycam/AppDataManager;->getOriginalTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/storycam/AppDataManager;->are2UHDFilesInTransElement(Lcom/samsung/app/video/editor/external/TranscodeElement;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2072
    new-instance v3, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$UnsupportedContentsDialog;

    invoke-direct {v3}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$UnsupportedContentsDialog;-><init>()V

    .line 2073
    .restart local v3    # "unsupportedDialog":Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$UnsupportedContentsDialog;
    invoke-static {v3, p0}, Lcom/sec/android/app/ve/common/AndroidUtils;->showDialog(Landroid/app/DialogFragment;Landroid/app/Activity;)V

    goto/16 :goto_0

    .line 2038
    :pswitch_data_0
    .packed-switch 0xfe
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1289
    sget-object v0, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;->STATE_BGM:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    iget-object v1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mCurrentActivityState:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1290
    sget-object v0, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;->STATE_DEFAULT:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    invoke-virtual {p0, v0, v2, v2}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->changeActivityState(Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;ZZ)V

    .line 1293
    :goto_0
    return-void

    .line 1292
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->handleBackPressed()V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 1297
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1299
    iget-object v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mConfig:Landroid/content/res/Configuration;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mHardKeyboardHidden:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget v0, p1, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 1300
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->getPreviewPlayer()Lcom/sec/android/app/ve/PreviewPlayerInterface;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1301
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->getPreviewPlayer()Lcom/sec/android/app/ve/PreviewPlayerInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/ve/PreviewPlayerInterface;->stop()V

    .line 1303
    :cond_0
    iput-object p1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mConfig:Landroid/content/res/Configuration;

    .line 1304
    iget-object v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mConfig:Landroid/content/res/Configuration;

    iget v0, v0, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    iput v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mHardKeyboardHidden:I

    .line 1306
    sget-object v0, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;->STATE_BGM:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    iget-object v1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mCurrentActivityState:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mBgmInstance:Lcom/sec/android/app/storycam/view/BGMView;

    if-eqz v0, :cond_1

    .line 1307
    iget-object v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mBgmInstance:Lcom/sec/android/app/storycam/view/BGMView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/storycam/view/BGMView;->handleOrientationChangeBGM(Landroid/content/res/Configuration;)V

    .line 1315
    :goto_0
    return-void

    .line 1309
    :cond_1
    sget-object v0, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;->STATE_EDIT:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    iget-object v1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mCurrentActivityState:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1310
    invoke-direct {p0, p1}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->handleOrientationChangeEdit(Landroid/content/res/Configuration;)V

    goto :goto_0

    .line 1313
    :cond_2
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->handleOrientationChange(Landroid/content/res/Configuration;Z)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 12
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x1

    .line 370
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 371
    invoke-static {}, Lcom/sec/android/app/storycam/VEAppSpecific;->init()Z

    .line 372
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->getWindow()Landroid/view/Window;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    .line 373
    .local v2, "lp":Landroid/view/WindowManager$LayoutParams;
    iget v7, v2, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    or-int/lit8 v7, v7, 0x2

    iput v7, v2, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    .line 375
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->getWindow()Landroid/view/Window;

    move-result-object v7

    invoke-virtual {v7, v2}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 376
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mActionBar:Landroid/app/ActionBar;

    .line 377
    new-instance v7, Lcom/sec/android/app/storycam/VEMediaSession;

    invoke-direct {v7}, Lcom/sec/android/app/storycam/VEMediaSession;-><init>()V

    iput-object v7, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mediaSession:Lcom/sec/android/app/storycam/VEMediaSession;

    .line 378
    iget-object v7, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mediaSession:Lcom/sec/android/app/storycam/VEMediaSession;

    const-class v8, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;

    invoke-virtual {v8}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->veMediaSessionCallback:Lcom/sec/android/app/storycam/VEMediaSession$VEMediaSessionCallback;

    invoke-virtual {v7, p0, v8, v9}, Lcom/sec/android/app/storycam/VEMediaSession;->createSession(Landroid/content/Context;Ljava/lang/String;Lcom/sec/android/app/storycam/VEMediaSession$VEMediaSessionCallback;)V

    .line 382
    invoke-static {p0}, Lcom/sec/android/app/ve/util/CommonUtils;->clearExportNotification(Landroid/app/Activity;)V

    .line 384
    const/4 v4, 0x0

    .line 385
    .local v4, "savedCurrentTElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    const/4 v5, 0x0

    .line 386
    .local v5, "savedOriginalTElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    if-eqz p1, :cond_1

    .line 387
    const-string v7, "CurrentTranscodeElement"

    invoke-virtual {p1, v7}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v4

    .end local v4    # "savedCurrentTElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    check-cast v4, Lcom/samsung/app/video/editor/external/TranscodeElement;

    .line 388
    .restart local v4    # "savedCurrentTElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    const-string v7, "OriginalTranscodeElement"

    invoke-virtual {p1, v7}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v5

    .end local v5    # "savedOriginalTElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    check-cast v5, Lcom/samsung/app/video/editor/external/TranscodeElement;

    .line 389
    .restart local v5    # "savedOriginalTElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    invoke-static {p0}, Lcom/sec/android/app/ve/common/AndroidUtils;->dismissDialog(Landroid/app/Activity;)V

    .line 390
    sput-boolean v11, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mIsCancelDialogShown:Z

    .line 398
    :goto_0
    if-eqz v4, :cond_2

    if-eqz v5, :cond_2

    .line 399
    const-string v7, "summarized transcode element is saved, restoring it from the bundle"

    invoke-static {v7}, Lcom/sec/android/app/ve/common/LogUtils;->log(Ljava/lang/String;)V

    .line 400
    invoke-direct {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->prepareContentView()V

    .line 401
    sget-object v7, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v7, v4}, Lcom/sec/android/app/storycam/AppDataManager;->setCurrentTranscodeElement(Lcom/samsung/app/video/editor/external/TranscodeElement;)V

    .line 402
    sget-object v7, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v7, v5}, Lcom/sec/android/app/storycam/AppDataManager;->setOriginalTranscodeElement(Lcom/samsung/app/video/editor/external/TranscodeElement;)V

    .line 403
    invoke-static {v10}, Lcom/sec/android/app/storycam/summary/Summary;->setAnalysisRequired(Z)V

    .line 404
    iget-object v7, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->captionText:Lcom/sec/android/app/storycam/view/Caption1Line;

    if-eqz v7, :cond_0

    .line 405
    iget-object v7, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->captionText:Lcom/sec/android/app/storycam/view/Caption1Line;

    const-string v8, "UNCOMMITED_TITLE"

    invoke-virtual {p1, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/sec/android/app/storycam/view/Caption1Line;->setText(Ljava/lang/CharSequence;)V

    .line 455
    :cond_0
    :goto_1
    sget-object v7, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    sget-object v8, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v8}, Lcom/sec/android/app/storycam/AppDataManager;->getOriginalTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/sec/android/app/storycam/AppDataManager;->setInitialTransocdeElement(Lcom/samsung/app/video/editor/external/TranscodeElement;)V

    .line 457
    :goto_2
    return-void

    .line 393
    :cond_1
    invoke-static {v10}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$BGMModesDialog;->access$0(I)V

    .line 394
    sget-object v7, Lcom/sec/android/app/ve/VEApp;->gBGMManager:Lcom/sec/android/app/ve/bgm/BGMManager;

    invoke-virtual {v7, v10}, Lcom/sec/android/app/ve/bgm/BGMManager;->setThemeDefaultBGM(Z)V

    .line 395
    sget-object v7, Lcom/sec/android/app/ve/VEApp;->gBGMManager:Lcom/sec/android/app/ve/bgm/BGMManager;

    const/16 v8, 0xf

    invoke-virtual {v7, v8}, Lcom/sec/android/app/ve/bgm/BGMManager;->setSavedBGMPos(I)V

    .line 396
    sget-object v7, Lcom/sec/android/app/ve/VEApp;->gBGMManager:Lcom/sec/android/app/ve/bgm/BGMManager;

    sget-object v8, Lcom/sec/android/app/ve/bgm/BGM$BGMModes;->MEDIUM:Lcom/sec/android/app/ve/bgm/BGM$BGMModes;

    invoke-virtual {v7, v8}, Lcom/sec/android/app/ve/bgm/BGMManager;->setBgmMode(Lcom/sec/android/app/ve/bgm/BGM$BGMModes;)V

    goto :goto_0

    .line 409
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->getIntent()Landroid/content/Intent;

    move-result-object v7

    invoke-direct {p0, v7, v10}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->validateIncomingIntent(Landroid/content/Intent;Z)Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;

    move-result-object v1

    .line 410
    .local v1, "intentResult":Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;
    sget-object v7, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v7}, Lcom/sec/android/app/storycam/AppDataManager;->getOriginalTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v7

    if-eqz v7, :cond_3

    .line 411
    sget-object v7, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v7}, Lcom/sec/android/app/storycam/AppDataManager;->getOriginalTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v7

    if-nez v7, :cond_4

    .line 412
    :cond_3
    const v7, 0x7f070301

    invoke-static {p0, v7, v10}, Lcom/sec/android/app/ve/VEApp;->showToast(Landroid/content/Context;II)Landroid/widget/Toast;

    .line 413
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->finish()V

    goto :goto_2

    .line 417
    :cond_4
    sget-object v7, Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;->NONE_SUPPORTED:Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;

    if-ne v1, v7, :cond_5

    .line 418
    invoke-direct {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->prepareContentView()V

    .line 419
    new-instance v6, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$UnsupportedContentsDialog;

    invoke-direct {v6}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$UnsupportedContentsDialog;-><init>()V

    .line 420
    .local v6, "unsupportedDialog":Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$UnsupportedContentsDialog;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 421
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v7, "ALL_UNSUPPORTED"

    invoke-virtual {v0, v7, v10}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 422
    invoke-virtual {v6, v0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$UnsupportedContentsDialog;->setArguments(Landroid/os/Bundle;)V

    .line 423
    invoke-static {v6, p0}, Lcom/sec/android/app/ve/common/AndroidUtils;->showDialog(Landroid/app/DialogFragment;Landroid/app/Activity;)V

    .line 424
    invoke-static {v10}, Lcom/sec/android/app/storycam/summary/Summary;->setAnalysisRequired(Z)V

    .line 425
    sget-object v7, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;->STATE_EDIT:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    invoke-virtual {p0, v7, v11, v10}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->changeActivityState(Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;ZZ)V

    goto :goto_1

    .line 426
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v6    # "unsupportedDialog":Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$UnsupportedContentsDialog;
    :cond_5
    sget-object v7, Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;->ALL_FILES_SUPPORTED:Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;

    if-ne v1, v7, :cond_6

    sget-object v7, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    sget-object v8, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v8}, Lcom/sec/android/app/storycam/AppDataManager;->getOriginalTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/sec/android/app/storycam/AppDataManager;->are2UHDFilesInTransElement(Lcom/samsung/app/video/editor/external/TranscodeElement;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 427
    :cond_6
    invoke-direct {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->prepareContentView()V

    .line 429
    sget-object v7, Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;->COUNT_EXCEEDS_LIMIT:Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;

    if-ne v1, v7, :cond_7

    .line 431
    invoke-static {v10}, Lcom/sec/android/app/storycam/summary/Summary;->setAnalysisRequired(Z)V

    .line 432
    sget-object v7, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;->STATE_EDIT:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    invoke-virtual {p0, v7, v11, v10}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->changeActivityState(Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;ZZ)V

    .line 433
    new-instance v3, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogInputMaxLimit;

    invoke-direct {v3}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogInputMaxLimit;-><init>()V

    .line 434
    .local v3, "maxLimitDialog":Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogInputMaxLimit;
    invoke-static {v3, p0}, Lcom/sec/android/app/ve/common/AndroidUtils;->showDialog(Landroid/app/DialogFragment;Landroid/app/Activity;)V

    goto/16 :goto_1

    .line 437
    .end local v3    # "maxLimitDialog":Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogInputMaxLimit;
    :cond_7
    new-instance v6, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$UnsupportedContentsDialog;

    invoke-direct {v6}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$UnsupportedContentsDialog;-><init>()V

    .line 438
    .restart local v6    # "unsupportedDialog":Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$UnsupportedContentsDialog;
    invoke-static {v6, p0}, Lcom/sec/android/app/ve/common/AndroidUtils;->showDialog(Landroid/app/DialogFragment;Landroid/app/Activity;)V

    .line 439
    invoke-static {v10}, Lcom/sec/android/app/storycam/summary/Summary;->setAnalysisRequired(Z)V

    .line 440
    sget-object v7, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;->STATE_EDIT:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    invoke-virtual {p0, v7, v11, v10}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->changeActivityState(Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;ZZ)V

    goto/16 :goto_1

    .line 443
    .end local v6    # "unsupportedDialog":Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$UnsupportedContentsDialog;
    :cond_8
    sget-object v7, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v7}, Lcom/sec/android/app/storycam/AppDataManager;->getOriginalTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v7

    if-lt v7, v10, :cond_9

    .line 444
    sget-object v7, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    sget-object v8, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v8}, Lcom/sec/android/app/storycam/AppDataManager;->getOriginalTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/sec/android/app/storycam/AppDataManager;->setIsUHDFileInTranscodeElement(Lcom/samsung/app/video/editor/external/TranscodeElement;)V

    .line 445
    invoke-direct {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->prepareContentView()V

    goto/16 :goto_1

    .line 449
    :cond_9
    invoke-direct {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->prepareContentView()V

    .line 450
    invoke-static {v10}, Lcom/sec/android/app/storycam/summary/Summary;->setAnalysisRequired(Z)V

    .line 451
    sget-object v7, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;->STATE_EDIT:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    invoke-virtual {p0, v7, v11, v10}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->changeActivityState(Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;ZZ)V

    goto/16 :goto_1
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 11
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const v10, 0x7f0d0006

    const v9, 0x7f030001

    const/16 v8, 0x10

    const/4 v7, -0x1

    const/4 v6, 0x1

    .line 660
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v4

    const/4 v5, 0x4

    invoke-virtual {v4, v5}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 661
    sget-object v4, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;->STATE_DEFAULT:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    iget-object v5, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mCurrentActivityState:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 662
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v4

    const/high16 v5, 0x7f0c0000

    invoke-virtual {v4, v5, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 663
    const v4, 0x7f0d0060

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    invoke-interface {v4, v6}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 664
    iget-object v4, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mActionBar:Landroid/app/ActionBar;

    const-string v5, ""

    invoke-virtual {v4, v5}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 700
    :cond_0
    :goto_0
    return v6

    .line 666
    :cond_1
    sget-object v4, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;->STATE_EDIT:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    iget-object v5, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mCurrentActivityState:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 667
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v4

    invoke-virtual {v4, v8}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 668
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v9, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 669
    .local v2, "customView":Landroid/view/View;
    new-instance v0, Landroid/app/ActionBar$LayoutParams;

    invoke-direct {v0, v7, v7}, Landroid/app/ActionBar$LayoutParams;-><init>(II)V

    .line 670
    .local v0, "actionBarLayoutParams":Landroid/app/ActionBar$LayoutParams;
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v4

    invoke-virtual {v4, v2, v0}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    .line 672
    invoke-virtual {v2, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 673
    .local v1, "cancelText":Landroid/widget/TextView;
    const v4, 0x7f0d0009

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 674
    .local v3, "doneText":Landroid/widget/TextView;
    iget-object v4, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mViewClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 675
    iget-object v4, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mViewClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 676
    sget-object v4, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    sget-object v5, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v5}, Lcom/sec/android/app/storycam/AppDataManager;->getOriginalTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/storycam/AppDataManager;->are2UHDFilesInTransElement(Lcom/samsung/app/video/editor/external/TranscodeElement;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 677
    sget-object v4, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v4}, Lcom/sec/android/app/storycam/AppDataManager;->getOriginalTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/storycam/Utils;->isTranscodeElementValid(Lcom/samsung/app/video/editor/external/TranscodeElement;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 678
    sget-object v4, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v4}, Lcom/sec/android/app/storycam/AppDataManager;->getOriginalTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v4

    if-ge v4, v6, :cond_3

    .line 679
    :cond_2
    const v4, 0x3ecccccd    # 0.4f

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setAlpha(F)V

    .line 680
    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_0

    .line 683
    :cond_3
    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setAlpha(F)V

    .line 684
    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_0

    .line 687
    .end local v0    # "actionBarLayoutParams":Landroid/app/ActionBar$LayoutParams;
    .end local v1    # "cancelText":Landroid/widget/TextView;
    .end local v2    # "customView":Landroid/view/View;
    .end local v3    # "doneText":Landroid/widget/TextView;
    :cond_4
    sget-object v4, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;->STATE_BGM:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    iget-object v5, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mCurrentActivityState:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 688
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v4

    invoke-virtual {v4, v8}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 689
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v4

    .line 690
    const/4 v5, 0x0

    .line 689
    invoke-virtual {v4, v9, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 692
    .restart local v2    # "customView":Landroid/view/View;
    new-instance v0, Landroid/app/ActionBar$LayoutParams;

    invoke-direct {v0, v7, v7}, Landroid/app/ActionBar$LayoutParams;-><init>(II)V

    .line 693
    .restart local v0    # "actionBarLayoutParams":Landroid/app/ActionBar$LayoutParams;
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v4

    invoke-virtual {v4, v2, v0}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    .line 694
    invoke-virtual {v2, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 695
    .restart local v1    # "cancelText":Landroid/widget/TextView;
    const v4, 0x7f0d0009

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 696
    .restart local v3    # "doneText":Landroid/widget/TextView;
    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 697
    iget-object v4, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mViewClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 698
    iget-object v4, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mViewClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 461
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->handleActivityDestruction(Z)V

    .line 462
    iget-object v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    if-eqz v0, :cond_0

    .line 463
    iget-object v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    iget-object v1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/cover/ScoverManager;->unregisterListener(Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;)V

    .line 464
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 465
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 10
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 568
    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    .line 569
    invoke-virtual {p1}, Landroid/content/Intent;->getFlags()I

    move-result v6

    const/high16 v7, 0x100000

    and-int/2addr v6, v7

    if-nez v6, :cond_1

    const-string v6, "fromNotification"

    invoke-virtual {p1, v6, v9}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 575
    iget-object v6, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mExportWrapper:Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;

    if-nez v6, :cond_0

    .line 576
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->finish()V

    .line 656
    :goto_0
    return-void

    .line 579
    :cond_0
    iget-object v6, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mExportWrapper:Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;

    invoke-virtual {v6}, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;->onNotificationClick()V

    goto :goto_0

    .line 581
    :cond_1
    iput-boolean v9, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->wasEditing:Z

    .line 582
    invoke-virtual {p0, p1}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->setIntent(Landroid/content/Intent;)V

    .line 583
    iput-boolean v8, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mIsBgmOn:Z

    .line 584
    invoke-static {v8}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$BGMModesDialog;->access$0(I)V

    .line 585
    sget-object v6, Lcom/sec/android/app/ve/VEApp;->gBGMManager:Lcom/sec/android/app/ve/bgm/BGMManager;

    invoke-virtual {v6, v8}, Lcom/sec/android/app/ve/bgm/BGMManager;->setThemeDefaultBGM(Z)V

    .line 586
    sget-object v6, Lcom/sec/android/app/ve/VEApp;->gBGMManager:Lcom/sec/android/app/ve/bgm/BGMManager;

    const/16 v7, 0xf

    invoke-virtual {v6, v7}, Lcom/sec/android/app/ve/bgm/BGMManager;->setSavedBGMPos(I)V

    .line 587
    sget-object v6, Lcom/sec/android/app/ve/VEApp;->gBGMManager:Lcom/sec/android/app/ve/bgm/BGMManager;

    sget-object v7, Lcom/sec/android/app/ve/bgm/BGM$BGMModes;->MEDIUM:Lcom/sec/android/app/ve/bgm/BGM$BGMModes;

    invoke-virtual {v6, v7}, Lcom/sec/android/app/ve/bgm/BGMManager;->setBgmMode(Lcom/sec/android/app/ve/bgm/BGM$BGMModes;)V

    .line 588
    sput-boolean v9, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->saveNeeded:Z

    .line 591
    iget-object v6, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mExportWrapper:Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;

    invoke-virtual {v6}, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;->isExportRunning()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 592
    iget-object v6, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mExportWrapper:Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;

    invoke-virtual {v6}, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;->stopExport()V

    .line 596
    :cond_2
    sget-object v6, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;->STATE_DEFAULT:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    invoke-virtual {p0, v6, v9, v9}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->changeActivityState(Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;ZZ)V

    .line 597
    invoke-direct {p0, v9}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->handleActivityDestruction(Z)V

    .line 598
    invoke-static {p0}, Lcom/sec/android/app/ve/common/AndroidUtils;->dismissDialog(Landroid/app/Activity;)V

    .line 599
    sput-boolean v9, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mIsCancelDialogShown:Z

    .line 600
    const v6, 0x7f0d0005

    invoke-virtual {p0, v6}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    .line 601
    .local v1, "et":Landroid/widget/EditText;
    if-eqz v1, :cond_3

    .line 602
    const-string v6, ""

    invoke-virtual {v1, v6}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 603
    :cond_3
    invoke-direct {p0, p1, v8}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->validateIncomingIntent(Landroid/content/Intent;Z)Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;

    move-result-object v2

    .line 604
    .local v2, "intentResult":Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;
    sget-object v6, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v6}, Lcom/sec/android/app/storycam/AppDataManager;->getOriginalTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v6

    if-eqz v6, :cond_4

    .line 605
    sget-object v6, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v6}, Lcom/sec/android/app/storycam/AppDataManager;->getOriginalTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v6

    if-nez v6, :cond_5

    .line 606
    :cond_4
    const v6, 0x7f070301

    invoke-static {p0, v6, v8}, Lcom/sec/android/app/ve/VEApp;->showToast(Landroid/content/Context;II)Landroid/widget/Toast;

    .line 607
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->finish()V

    goto :goto_0

    .line 611
    :cond_5
    sget-object v6, Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;->NONE_SUPPORTED:Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;

    if-ne v2, v6, :cond_6

    .line 612
    new-instance v5, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$UnsupportedContentsDialog;

    invoke-direct {v5}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$UnsupportedContentsDialog;-><init>()V

    .line 613
    .local v5, "unsupportedDialog":Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$UnsupportedContentsDialog;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 614
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v6, "ALL_UNSUPPORTED"

    invoke-virtual {v0, v6, v8}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 615
    invoke-virtual {v5, v0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$UnsupportedContentsDialog;->setArguments(Landroid/os/Bundle;)V

    .line 616
    invoke-static {v5, p0}, Lcom/sec/android/app/ve/common/AndroidUtils;->showDialog(Landroid/app/DialogFragment;Landroid/app/Activity;)V

    .line 617
    invoke-static {v8}, Lcom/sec/android/app/storycam/summary/Summary;->setAnalysisRequired(Z)V

    .line 618
    sget-object v6, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;->STATE_EDIT:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    invoke-virtual {p0, v6, v9, v8}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->changeActivityState(Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;ZZ)V

    goto/16 :goto_0

    .line 620
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v5    # "unsupportedDialog":Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$UnsupportedContentsDialog;
    :cond_6
    sget-object v6, Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;->ALL_FILES_SUPPORTED:Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;

    if-ne v2, v6, :cond_7

    sget-object v6, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    sget-object v7, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v7}, Lcom/sec/android/app/storycam/AppDataManager;->getOriginalTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/sec/android/app/storycam/AppDataManager;->are2UHDFilesInTransElement(Lcom/samsung/app/video/editor/external/TranscodeElement;)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 622
    :cond_7
    invoke-static {v8}, Lcom/sec/android/app/storycam/summary/Summary;->setAnalysisRequired(Z)V

    .line 623
    iget-object v6, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mConfig:Landroid/content/res/Configuration;

    iget v6, v6, Landroid/content/res/Configuration;->orientation:I

    if-ne v6, v8, :cond_a

    .line 624
    const v6, 0x7f0d0029

    invoke-virtual {p0, v6}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/storycam/view/MediaEditGridView;

    iput-object v6, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mediaEditGrid:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    .line 628
    :goto_1
    iget-object v6, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mediaEditGrid:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    if-eqz v6, :cond_8

    .line 629
    iget-object v6, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mediaEditGrid:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    invoke-virtual {v6}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->refresh()V

    .line 630
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->invalidateOptionsMenu()V

    .line 632
    :cond_8
    sget-object v6, Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;->COUNT_EXCEEDS_LIMIT:Lcom/sec/android/app/storycam/AppDataManager$IntentErrors;

    if-ne v2, v6, :cond_b

    .line 634
    invoke-static {v8}, Lcom/sec/android/app/storycam/summary/Summary;->setAnalysisRequired(Z)V

    .line 635
    sget-object v6, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;->STATE_EDIT:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    invoke-virtual {p0, v6, v9, v8}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->changeActivityState(Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;ZZ)V

    .line 636
    new-instance v3, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogInputMaxLimit;

    invoke-direct {v3}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogInputMaxLimit;-><init>()V

    .line 637
    .local v3, "maxLimitDialog":Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogInputMaxLimit;
    invoke-static {v3, p0}, Lcom/sec/android/app/ve/common/AndroidUtils;->showDialog(Landroid/app/DialogFragment;Landroid/app/Activity;)V

    .line 648
    .end local v3    # "maxLimitDialog":Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogInputMaxLimit;
    :goto_2
    iget-object v6, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mRootView:Landroid/widget/RelativeLayout;

    const v7, 0x7f0d0003

    invoke-virtual {v6, v7}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    .line 649
    .local v4, "pvg":Lcom/sec/android/app/storycam/view/PreviewViewGroup;
    if-eqz v4, :cond_9

    .line 650
    invoke-virtual {v4, v9}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->setProgressBar(I)V

    .line 651
    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->setCurrentTranscodeElement(Lcom/samsung/app/video/editor/external/TranscodeElement;)V

    .line 653
    :cond_9
    sget-object v6, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    sget-object v7, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v7}, Lcom/sec/android/app/storycam/AppDataManager;->getOriginalTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/sec/android/app/storycam/AppDataManager;->setInitialTransocdeElement(Lcom/samsung/app/video/editor/external/TranscodeElement;)V

    .line 654
    sget-object v6, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    sget-object v7, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v7}, Lcom/sec/android/app/storycam/AppDataManager;->getOriginalTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/sec/android/app/storycam/AppDataManager;->setIsUHDFileInTranscodeElement(Lcom/samsung/app/video/editor/external/TranscodeElement;)V

    goto/16 :goto_0

    .line 626
    .end local v4    # "pvg":Lcom/sec/android/app/storycam/view/PreviewViewGroup;
    :cond_a
    const v6, 0x7f0d002b

    invoke-virtual {p0, v6}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/storycam/view/MediaEditGridView;

    iput-object v6, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mediaEditGrid:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    goto :goto_1

    .line 640
    :cond_b
    new-instance v5, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$UnsupportedContentsDialog;

    invoke-direct {v5}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$UnsupportedContentsDialog;-><init>()V

    .line 641
    .restart local v5    # "unsupportedDialog":Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$UnsupportedContentsDialog;
    invoke-static {v5, p0}, Lcom/sec/android/app/ve/common/AndroidUtils;->showDialog(Landroid/app/DialogFragment;Landroid/app/Activity;)V

    .line 642
    invoke-static {v8}, Lcom/sec/android/app/storycam/summary/Summary;->setAnalysisRequired(Z)V

    .line 643
    sget-object v6, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;->STATE_EDIT:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    invoke-virtual {p0, v6, v9, v8}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->changeActivityState(Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;ZZ)V

    goto :goto_2

    .line 646
    .end local v5    # "unsupportedDialog":Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$UnsupportedContentsDialog;
    :cond_c
    sget-object v6, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;->STATE_DEFAULT:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    invoke-virtual {p0, v6, v9, v8}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->changeActivityState(Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;ZZ)V

    goto :goto_2
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 10
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 711
    invoke-static {}, Lcom/sec/android/app/ve/util/CommonUtils;->isUserEventTooSoonAfterPrev()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 754
    :goto_0
    return v8

    .line 715
    :cond_0
    iget-object v6, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mActivityEventCallbacks:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_1

    .line 718
    invoke-virtual {p0, v9}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->requestFocusForMainActivity(Z)V

    .line 719
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v6

    sparse-switch v6, :sswitch_data_0

    goto :goto_0

    .line 721
    :sswitch_0
    invoke-direct {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->handleBackPressed()V

    goto :goto_0

    .line 715
    :cond_1
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ActivityEventsCallback;

    .line 716
    .local v0, "callback":Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ActivityEventsCallback;
    invoke-interface {v0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ActivityEventsCallback;->onActivityActionBarTapped()V

    goto :goto_1

    .line 724
    .end local v0    # "callback":Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ActivityEventsCallback;
    :sswitch_1
    invoke-direct {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->handleBackPressed()V

    goto :goto_0

    .line 727
    :sswitch_2
    sget-object v6, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v6}, Lcom/sec/android/app/storycam/AppDataManager;->getCurrentTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v3

    .line 728
    .local v3, "trans":Lcom/samsung/app/video/editor/external/TranscodeElement;
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getTotalDuration()J

    move-result-wide v4

    .line 729
    .local v4, "totalDuration":J
    :goto_2
    invoke-static {v4, v5}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog;->newInstance(J)Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog;

    move-result-object v1

    .line 730
    .local v1, "exportdialog":Landroid/app/DialogFragment;
    invoke-static {v1, p0}, Lcom/sec/android/app/ve/common/AndroidUtils;->showDialog(Landroid/app/DialogFragment;Landroid/app/Activity;)V

    goto :goto_0

    .line 728
    .end local v1    # "exportdialog":Landroid/app/DialogFragment;
    .end local v4    # "totalDuration":J
    :cond_2
    const-wide/16 v4, 0x0

    goto :goto_2

    .line 733
    .end local v3    # "trans":Lcom/samsung/app/video/editor/external/TranscodeElement;
    :sswitch_3
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/app/ve/common/AndroidUtils;->hideSoftKeyboard(Landroid/view/View;)V

    .line 734
    sget-object v6, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;->STATE_EDIT:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    invoke-virtual {p0, v6, v9, v8}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->changeActivityState(Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;ZZ)V

    goto :goto_0

    .line 737
    :sswitch_4
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 738
    .local v2, "prefs":Landroid/content/SharedPreferences;
    const-string v6, "SHOW_LAUNCH_VIDEO_EDITOR"

    invoke-interface {v2, v6, v8}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 739
    const/16 v6, 0x65

    invoke-virtual {p0, v6}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->launchDialog(I)V

    goto :goto_0

    .line 742
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->checkVEFullAndProceed()V

    goto :goto_0

    .line 747
    .end local v2    # "prefs":Landroid/content/SharedPreferences;
    :sswitch_5
    iget-object v6, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->captionText:Lcom/sec/android/app/storycam/view/Caption1Line;

    if-eqz v6, :cond_4

    .line 748
    iget-object v6, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->captionText:Lcom/sec/android/app/storycam/view/Caption1Line;

    invoke-virtual {v6}, Lcom/sec/android/app/storycam/view/Caption1Line;->saveCaption()V

    .line 749
    :cond_4
    sget-object v6, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;->STATE_BGM:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    invoke-virtual {p0, v6, v9, v8}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->changeActivityState(Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;ZZ)V

    goto :goto_0

    .line 719
    nop

    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f0d005c -> :sswitch_5
        0x7f0d005d -> :sswitch_3
        0x7f0d005e -> :sswitch_4
        0x7f0d005f -> :sswitch_1
        0x7f0d0060 -> :sswitch_2
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 469
    iget-object v1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->captionText:Lcom/sec/android/app/storycam/view/Caption1Line;

    invoke-virtual {v1}, Lcom/sec/android/app/storycam/view/Caption1Line;->hasFocus()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 470
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->wasEditing:Z

    .line 472
    :cond_0
    invoke-static {}, Lcom/sec/android/app/storycam/summary/Summary;->stopSummary()V

    .line 474
    iget-object v1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mActivityEventCallbacks:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_3

    .line 478
    iget-object v1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mExportWrapper:Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;

    invoke-virtual {v1}, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;->isExportRunning()Z

    move-result v1

    if-nez v1, :cond_4

    .line 479
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/NativeInterface;->_native_terminate()V

    .line 486
    :cond_1
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mBgmInstance:Lcom/sec/android/app/storycam/view/BGMView;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mBgmInstance:Lcom/sec/android/app/storycam/view/BGMView;

    invoke-virtual {v1}, Lcom/sec/android/app/storycam/view/BGMView;->isMediaplayerPlaying()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 487
    iget-object v1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mBgmInstance:Lcom/sec/android/app/storycam/view/BGMView;

    invoke-virtual {v1}, Lcom/sec/android/app/storycam/view/BGMView;->stopMediaPlayer()V

    .line 488
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mediaSession:Lcom/sec/android/app/storycam/VEMediaSession;

    invoke-virtual {v1}, Lcom/sec/android/app/storycam/VEMediaSession;->deactivateSession()V

    .line 489
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 490
    return-void

    .line 474
    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ActivityEventsCallback;

    .line 475
    .local v0, "callback":Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ActivityEventsCallback;
    const-string v2, "notifying keyEvents callaback listeners"

    invoke-static {v2}, Lcom/sec/android/app/ve/common/LogUtils;->log(Ljava/lang/String;)V

    .line 476
    invoke-interface {v0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ActivityEventsCallback;->onActivityPaused()V

    goto :goto_0

    .line 481
    .end local v0    # "callback":Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ActivityEventsCallback;
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->getPreviewPlayer()Lcom/sec/android/app/ve/PreviewPlayerInterface;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 482
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->getPreviewPlayer()Lcom/sec/android/app/ve/PreviewPlayerInterface;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/ve/PreviewPlayerInterface;->stop()V

    .line 483
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->getPreviewPlayer()Lcom/sec/android/app/ve/PreviewPlayerInterface;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/ve/PreviewPlayerInterface;->getSurfaceView()Landroid/view/SurfaceView;

    move-result-object v1

    const/high16 v2, -0x1000000

    invoke-virtual {v1, v2}, Landroid/view/SurfaceView;->setBackgroundColor(I)V

    goto :goto_1
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 706
    invoke-super {p0, p1}, Landroid/app/Activity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "arg0"    # Landroid/os/Bundle;

    .prologue
    .line 493
    invoke-super {p0, p1}, Landroid/app/Activity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 494
    const-string v0, "KEYBOARD_WAS_VISIBLE"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 495
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->wasEditing:Z

    .line 497
    :cond_0
    const-string v0, "font_scale_value"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v0

    sput v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->fontScale_value:F

    .line 498
    return-void
.end method

.method protected onResume()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x1f4

    .line 501
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 502
    invoke-direct {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->handleActivityInit()V

    .line 504
    iget-object v1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->spaceString:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-direct {p0, v1, v2}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->recheckFiles(Ljava/lang/String;Z)Z

    .line 506
    iget-object v1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mRootView:Landroid/widget/RelativeLayout;

    new-instance v2, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$7;

    invoke-direct {v2, p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$7;-><init>(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;)V

    .line 513
    const-wide/16 v4, 0x3e8

    .line 506
    invoke-virtual {v1, v2, v4, v5}, Landroid/widget/RelativeLayout;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 515
    sget-object v1, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;->STATE_DEFAULT:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    iget-object v2, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mCurrentActivityState:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 516
    iget-object v1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mRootView:Landroid/widget/RelativeLayout;

    new-instance v2, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$8;

    invoke-direct {v2, p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$8;-><init>(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;)V

    .line 521
    const-wide/16 v4, 0x32

    .line 516
    invoke-virtual {v1, v2, v4, v5}, Landroid/widget/RelativeLayout;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 525
    :cond_0
    iget-boolean v1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->wasEditing:Z

    if-eqz v1, :cond_1

    .line 527
    iget-object v1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->captionText:Lcom/sec/android/app/storycam/view/Caption1Line;

    invoke-virtual {v1}, Lcom/sec/android/app/storycam/view/Caption1Line;->requestFocus()Z

    .line 528
    iget-object v1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->captionText:Lcom/sec/android/app/storycam/view/Caption1Line;

    new-instance v2, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$9;

    invoke-direct {v2, p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$9;-><init>(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;)V

    invoke-virtual {v1, v2, v6, v7}, Lcom/sec/android/app/storycam/view/Caption1Line;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 548
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mActivityEventCallbacks:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_2

    .line 552
    iget-object v1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mRootView:Landroid/widget/RelativeLayout;

    new-instance v2, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$10;

    invoke-direct {v2, p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$10;-><init>(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;)V

    invoke-virtual {v1, v2, v6, v7}, Landroid/widget/RelativeLayout;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 562
    iget-object v1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mediaSession:Lcom/sec/android/app/storycam/VEMediaSession;

    invoke-virtual {v1, p0}, Lcom/sec/android/app/storycam/VEMediaSession;->activateSession(Landroid/content/Context;)V

    .line 563
    return-void

    .line 543
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/ve/common/AndroidUtils;->hideSoftKeyboard(Landroid/view/View;)V

    .line 544
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->requestFocusForMainActivity(Z)V

    goto :goto_0

    .line 548
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ActivityEventsCallback;

    .line 549
    .local v0, "callback":Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ActivityEventsCallback;
    invoke-interface {v0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ActivityEventsCallback;->onActivityResumed()V

    goto :goto_1
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 1368
    iget-boolean v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->wasEditing:Z

    if-eqz v0, :cond_0

    .line 1369
    const-string v0, "KEYBOARD_WAS_VISIBLE"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1370
    const-string v0, "UNCOMMITED_TITLE"

    iget-object v1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->captionText:Lcom/sec/android/app/storycam/view/Caption1Line;

    invoke-virtual {v1}, Lcom/sec/android/app/storycam/view/Caption1Line;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1372
    :cond_0
    const-string v0, "font_scale_value"

    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->fontScale:F

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 1373
    const-string v0, "CurrentTranscodeElement"

    sget-object v1, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v1}, Lcom/sec/android/app/storycam/AppDataManager;->getCurrentTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 1374
    const-string v0, "OriginalTranscodeElement"

    sget-object v1, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v1}, Lcom/sec/android/app/storycam/AppDataManager;->getOriginalTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 1375
    const-string v0, "saved the current transcode element inside onSaveInstanceState"

    invoke-static {v0}, Lcom/sec/android/app/ve/common/LogUtils;->log(Ljava/lang/String;)V

    .line 1376
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1377
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 2
    .param p1, "hasFocus"    # Z

    .prologue
    .line 1713
    iget-object v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->spaceString:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->recheckFiles(Ljava/lang/String;Z)Z

    .line 1714
    return-void
.end method

.method public removeActivityEventsCallback(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ActivityEventsCallback;)V
    .locals 1
    .param p1, "aActivityEventsCallback"    # Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ActivityEventsCallback;

    .prologue
    .line 2129
    iget-object v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mActivityEventCallbacks:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 2130
    return-void
.end method

.method public removeActivityStateChangedListener(Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityStateChangedListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityStateChangedListener;

    .prologue
    .line 1552
    iget-object v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mActivityStateListeners:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 1553
    iget-object v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mActivityStateListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1555
    :cond_0
    return-void
.end method

.method public requestFocusForMainActivity(Z)V
    .locals 1
    .param p1, "verify"    # Z

    .prologue
    .line 1946
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->captionText:Lcom/sec/android/app/storycam/view/Caption1Line;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->captionText:Lcom/sec/android/app/storycam/view/Caption1Line;

    invoke-virtual {v0}, Lcom/sec/android/app/storycam/view/Caption1Line;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1947
    :cond_0
    const v0, 0x7f0d0007

    invoke-virtual {p0, v0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 1948
    :cond_1
    return-void
.end method

.method public requestForOptionMenuRefresh()V
    .locals 0

    .prologue
    .line 1537
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->invalidateOptionsMenu()V

    .line 1538
    return-void
.end method

.method public saveCaption()V
    .locals 1

    .prologue
    .line 2086
    iget-object v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->captionText:Lcom/sec/android/app/storycam/view/Caption1Line;

    if-eqz v0, :cond_0

    .line 2087
    iget-object v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->captionText:Lcom/sec/android/app/storycam/view/Caption1Line;

    invoke-virtual {v0}, Lcom/sec/android/app/storycam/view/Caption1Line;->saveCaption()V

    .line 2088
    :cond_0
    return-void
.end method

.method public setActivityEventsCallback(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ActivityEventsCallback;)V
    .locals 1
    .param p1, "aActivityEventsCallback"    # Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ActivityEventsCallback;

    .prologue
    .line 2118
    iget-object v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mActivityEventCallbacks:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2119
    iget-object v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mActivityEventCallbacks:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2121
    :cond_0
    return-void
.end method

.method public setBgmState(Z)V
    .locals 0
    .param p1, "val"    # Z

    .prologue
    .line 2758
    iput-boolean p1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mIsBgmOn:Z

    .line 2759
    return-void
.end method

.method public setPreviewPlayer(Lcom/sec/android/app/ve/PreviewPlayerInterface;)V
    .locals 0
    .param p1, "preview"    # Lcom/sec/android/app/ve/PreviewPlayerInterface;

    .prologue
    .line 1568
    iput-object p1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mPreviewPlayer:Lcom/sec/android/app/ve/PreviewPlayerInterface;

    .line 1569
    return-void
.end method

.method public setUserSelectedBgm(Z)V
    .locals 0
    .param p1, "val"    # Z

    .prologue
    .line 2748
    iput-boolean p1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->isUserSelected:Z

    .line 2749
    return-void
.end method

.method public setUserSelectedBgmPos(I)V
    .locals 0
    .param p1, "pos"    # I

    .prologue
    .line 2738
    iput p1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mSelectedBGMPos:I

    .line 2739
    return-void
.end method

.method public showMaxItemsDialog()V
    .locals 1

    .prologue
    .line 2092
    new-instance v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogRemoveClips;

    invoke-direct {v0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogRemoveClips;-><init>()V

    .line 2093
    .local v0, "dialog":Landroid/app/DialogFragment;
    invoke-static {v0, p0}, Lcom/sec/android/app/ve/common/AndroidUtils;->showDialog(Landroid/app/DialogFragment;Landroid/app/Activity;)V

    .line 2095
    return-void
.end method

.method public updateExportPath(Lcom/samsung/app/video/editor/external/TranscodeElement;)V
    .locals 5
    .param p1, "tElement"    # Lcom/samsung/app/video/editor/external/TranscodeElement;

    .prologue
    .line 2443
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementList()Ljava/util/List;

    move-result-object v2

    if-nez v2, :cond_1

    .line 2464
    :cond_0
    :goto_0
    return-void

    .line 2447
    :cond_1
    const/4 v1, 0x0

    .line 2448
    .local v1, "personalPagePresent":Z
    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_3

    .line 2455
    :goto_1
    if-eqz v1, :cond_4

    .line 2456
    new-instance v2, Ljava/lang/StringBuilder;

    sget-object v3, Lcom/sec/android/app/storycam/VEAppSpecific;->EXPORT_PATH_PERSONALPAGE:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "Studio"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/sec/android/app/ve/common/ConfigUtils;->EXPORT_PATH:Ljava/lang/String;

    .line 2457
    iget-object v2, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mExportWrapper:Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;

    const-string v3, "Private/Studio"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;->setExportFolderName(Ljava/lang/String;)V

    goto :goto_0

    .line 2448
    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/app/video/editor/external/Element;

    .line 2449
    .local v0, "ele":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {v0}, Lcom/samsung/app/video/editor/external/Element;->getFilePath()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/sec/android/app/storycam/VEAppSpecific;->EXPORT_PATH_PERSONALPAGE:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2450
    const/4 v1, 0x1

    .line 2452
    goto :goto_1

    .line 2461
    .end local v0    # "ele":Lcom/samsung/app/video/editor/external/Element;
    :cond_4
    sget-object v2, Lcom/sec/android/app/storycam/VEAppSpecific;->EXPORT_PATH_DEFAULT:Ljava/lang/String;

    sput-object v2, Lcom/sec/android/app/ve/common/ConfigUtils;->EXPORT_PATH:Ljava/lang/String;

    .line 2462
    iget-object v2, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mExportWrapper:Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;

    const-string v3, "Studio"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;->setExportFolderName(Ljava/lang/String;)V

    goto :goto_0
.end method
