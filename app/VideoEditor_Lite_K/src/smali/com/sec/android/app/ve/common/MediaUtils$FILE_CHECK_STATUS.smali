.class public Lcom/sec/android/app/ve/common/MediaUtils$FILE_CHECK_STATUS;
.super Ljava/lang/Object;
.source "MediaUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/ve/common/MediaUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FILE_CHECK_STATUS"
.end annotation


# static fields
.field public static INVALID_FILE:I

.field public static NOT_SUPPORTED_BY_ENGINE:I

.field public static VALID_FILE:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/ve/common/MediaUtils$FILE_CHECK_STATUS;->VALID_FILE:I

    .line 54
    const/4 v0, -0x1

    sput v0, Lcom/sec/android/app/ve/common/MediaUtils$FILE_CHECK_STATUS;->NOT_SUPPORTED_BY_ENGINE:I

    .line 55
    const/4 v0, -0x2

    sput v0, Lcom/sec/android/app/ve/common/MediaUtils$FILE_CHECK_STATUS;->INVALID_FILE:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
