.class public Lcom/sec/android/app/ve/theme/ThemeConstants$Themes;
.super Ljava/lang/Object;
.source "ThemeConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/ve/theme/ThemeConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Themes"
.end annotation


# static fields
.field public static final THEME_ACTION:I = 0x2

.field public static final THEME_HOME:I = 0x4

.field public static final THEME_NONE:I = 0x0

.field public static final THEME_OUTING:I = 0x5

.field public static final THEME_ROMANCE:I = 0x1

.field public static final THEME_TRAVEL:I = 0x3

.field public static final THEME_VELITE_ACTION:I = 0xb

.field public static final THEME_VELITE_BW:I = 0x7

.field public static final THEME_VELITE_CARTOON:I = 0x11

.field public static final THEME_VELITE_FADEDCOLORS:I = 0x12

.field public static final THEME_VELITE_GREYSCALE:I = 0xe

.field public static final THEME_VELITE_JAZZ:I = 0x8

.field public static final THEME_VELITE_MIRROR:I = 0x1b

.field public static final THEME_VELITE_MOSAIC:I = 0x17

.field public static final THEME_VELITE_NEW_VINTAGE:I = 0x10

.field public static final THEME_VELITE_NORMAL:I = 0x6

.field public static final THEME_VELITE_OIL_PASTEL:I = 0x16

.field public static final THEME_VELITE_OLD_FILM:I = 0x1c

.field public static final THEME_VELITE_ORIGINAL:I = 0xd

.field public static final THEME_VELITE_OUTFOCUS:I = 0x14

.field public static final THEME_VELITE_RUGGED:I = 0x19

.field public static final THEME_VELITE_SEPIA:I = 0xf

.field public static final THEME_VELITE_SHINING:I = 0x13

.field public static final THEME_VELITE_SIMPLE:I = 0x9

.field public static final THEME_VELITE_SOFT:I = 0xa

.field public static final THEME_VELITE_THERMAL:I = 0x1a

.field public static final THEME_VELITE_TINT:I = 0x18

.field public static final THEME_VELITE_TURQUOISE:I = 0x1d

.field public static final THEME_VELITE_VIGNETTE:I = 0x15

.field public static final THEME_VELITE_VINTAGE:I = 0xc


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
