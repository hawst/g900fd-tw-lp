.class public Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogFileNameExist;
.super Lcom/sec/android/app/ve/common/AndroidUtils$DlgFragment;
.source "VideoEditorLiteActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DialogFileNameExist"
.end annotation


# instance fields
.field activity:Landroid/app/Activity;

.field expAdapter:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExpAdapter;

.field private expRes:I

.field expWrapper:Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1218
    invoke-direct {p0}, Lcom/sec/android/app/ve/common/AndroidUtils$DlgFragment;-><init>()V

    .line 1219
    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExpAdapter;I)V
    .locals 0
    .param p1, "acvt"    # Landroid/app/Activity;
    .param p2, "expWrap"    # Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;
    .param p3, "expAdap"    # Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExpAdapter;
    .param p4, "expRes"    # I

    .prologue
    .line 1222
    invoke-direct {p0}, Lcom/sec/android/app/ve/common/AndroidUtils$DlgFragment;-><init>()V

    .line 1223
    iput-object p1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogFileNameExist;->activity:Landroid/app/Activity;

    .line 1224
    iput-object p2, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogFileNameExist;->expWrapper:Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;

    .line 1225
    iput-object p3, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogFileNameExist;->expAdapter:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExpAdapter;

    .line 1226
    iput p4, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogFileNameExist;->expRes:I

    .line 1227
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogFileNameExist;)I
    .locals 1

    .prologue
    .line 1216
    iget v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogFileNameExist;->expRes:I

    return v0
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 1236
    invoke-super {p0, p1}, Lcom/sec/android/app/ve/common/AndroidUtils$DlgFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1237
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogFileNameExist;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogFileNameExist;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p1, Landroid/content/res/Configuration;->fontScale:F

    # getter for: Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->fontScale_value:F
    invoke-static {}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->access$7()F

    move-result v1

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    .line 1238
    iget v0, p1, Landroid/content/res/Configuration;->fontScale:F

    invoke-static {v0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->access$8(F)V

    .line 1239
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogFileNameExist;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/ve/common/AndroidUtils;->dismissDialog(Landroid/app/Activity;)V

    .line 1242
    :cond_0
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x0

    const/4 v7, 0x0

    .line 1246
    sget-object v6, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v6}, Lcom/sec/android/app/storycam/AppDataManager;->getCurrentTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v4

    .line 1247
    .local v4, "trans":Lcom/samsung/app/video/editor/external/TranscodeElement;
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v6

    if-gez v6, :cond_1

    .line 1248
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogFileNameExist;->dismiss()V

    .line 1279
    :goto_0
    return-object v5

    .line 1252
    :cond_1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogFileNameExist;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-direct {v0, v6}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1253
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v4, v7}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElement(I)Lcom/samsung/app/video/editor/external/Element;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getTextClipartParamsListForElement(Lcom/samsung/app/video/editor/external/Element;)Ljava/util/List;

    move-result-object v1

    .line 1254
    .local v1, "clipartList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    if-eqz v1, :cond_2

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v6

    if-lez v6, :cond_2

    .line 1255
    invoke-interface {v1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/app/video/editor/external/ClipartParams;

    iget-object v2, v5, Lcom/samsung/app/video/editor/external/ClipartParams;->data:Ljava/lang/String;

    .line 1256
    .local v2, "exportFilename":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "\""

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ".mp4"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1257
    .local v3, "exportFilenameWithQuotes":Ljava/lang/String;
    const v5, 0x7f07020d

    invoke-static {v5}, Lcom/sec/android/app/ve/VEApp;->getStringValue(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v3, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    .line 1258
    const v6, 0x7f07020b

    invoke-virtual {v5, v6}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    .line 1259
    const v6, 0x7f07006e

    new-instance v7, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogFileNameExist$1;

    invoke-direct {v7, p0, v2, v4}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogFileNameExist$1;-><init>(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogFileNameExist;Ljava/lang/String;Lcom/samsung/app/video/editor/external/TranscodeElement;)V

    invoke-virtual {v5, v6, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    .line 1271
    const v6, 0x7f07005e

    new-instance v7, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogFileNameExist$2;

    invoke-direct {v7, p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogFileNameExist$2;-><init>(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogFileNameExist;)V

    invoke-virtual {v5, v6, v7}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1276
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v5

    goto :goto_0

    .line 1278
    .end local v2    # "exportFilename":Ljava/lang/String;
    .end local v3    # "exportFilenameWithQuotes":Ljava/lang/String;
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogFileNameExist;->dismiss()V

    goto :goto_0
.end method

.method public onDestroyView()V
    .locals 0

    .prologue
    .line 1231
    invoke-super {p0}, Lcom/sec/android/app/ve/common/AndroidUtils$DlgFragment;->onDestroyView()V

    .line 1232
    return-void
.end method
