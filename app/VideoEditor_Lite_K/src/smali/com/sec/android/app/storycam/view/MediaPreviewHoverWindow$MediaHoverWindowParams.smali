.class public Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;
.super Ljava/lang/Object;
.source "MediaPreviewHoverWindow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MediaHoverWindowParams"
.end annotation


# instance fields
.field private mBitmap:Landroid/graphics/Bitmap;

.field private mFilePath:Ljava/lang/String;

.field private mHeight:I

.field private mIsSupported:Z

.field private mLeft:I

.field private mMediaType:Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowType;

.field private mTop:I

.field private mViewRect:Landroid/graphics/RectF;

.field private mWidth:I


# direct methods
.method public constructor <init>(IIIILandroid/graphics/Bitmap;Ljava/lang/String;Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowType;Landroid/graphics/RectF;IZ)V
    .locals 1
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "width"    # I
    .param p4, "height"    # I
    .param p5, "bmp"    # Landroid/graphics/Bitmap;
    .param p6, "path"    # Ljava/lang/String;
    .param p7, "mediaType"    # Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowType;
    .param p8, "viewRect"    # Landroid/graphics/RectF;
    .param p9, "thumbCount"    # I
    .param p10, "isSupported"    # Z

    .prologue
    .line 193
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 203
    iput p1, p0, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;->mLeft:I

    .line 204
    iput p2, p0, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;->mTop:I

    .line 205
    iput p3, p0, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;->mWidth:I

    .line 206
    iput p4, p0, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;->mHeight:I

    .line 207
    iput-object p5, p0, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;->mBitmap:Landroid/graphics/Bitmap;

    .line 208
    iput-object p6, p0, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;->mFilePath:Ljava/lang/String;

    .line 209
    iput-object p7, p0, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;->mMediaType:Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowType;

    .line 210
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, p8}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;->mViewRect:Landroid/graphics/RectF;

    .line 211
    iput-boolean p10, p0, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;->mIsSupported:Z

    .line 212
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;)I
    .locals 1

    .prologue
    .line 183
    iget v0, p0, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;->mLeft:I

    return v0
.end method

.method static synthetic access$1(Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;)I
    .locals 1

    .prologue
    .line 184
    iget v0, p0, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;->mTop:I

    return v0
.end method

.method static synthetic access$2(Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;)I
    .locals 1

    .prologue
    .line 185
    iget v0, p0, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;->mWidth:I

    return v0
.end method

.method static synthetic access$3(Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;)I
    .locals 1

    .prologue
    .line 186
    iget v0, p0, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;->mHeight:I

    return v0
.end method

.method static synthetic access$4(Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;->mBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$5(Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;)Z
    .locals 1

    .prologue
    .line 191
    iget-boolean v0, p0, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;->mIsSupported:Z

    return v0
.end method

.method static synthetic access$6(Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;->mFilePath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$7(Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;)Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;->mViewRect:Landroid/graphics/RectF;

    return-object v0
.end method

.method static synthetic access$8(Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;)Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowType;
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;->mMediaType:Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowType;

    return-object v0
.end method
