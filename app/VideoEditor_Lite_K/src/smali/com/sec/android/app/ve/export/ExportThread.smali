.class public Lcom/sec/android/app/ve/export/ExportThread;
.super Ljava/lang/Thread;
.source "ExportThread.java"


# instance fields
.field private fileName:Ljava/lang/String;

.field private mStblzReqd:Z

.field private mTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

.field private wakelock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 23
    sget-object v0, Lcom/sec/android/app/ve/VEApp;->gExport:Lcom/sec/android/app/ve/export/Export;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/export/Export;->getTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/ve/export/ExportThread;->mTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    .line 24
    sget-object v0, Lcom/sec/android/app/ve/VEApp;->gExport:Lcom/sec/android/app/ve/export/Export;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/export/Export;->getFileName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/ve/export/ExportThread;->fileName:Ljava/lang/String;

    .line 25
    sget-object v0, Lcom/sec/android/app/ve/VEApp;->gExport:Lcom/sec/android/app/ve/export/Export;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/export/Export;->stabilizationRequired()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/ve/export/ExportThread;->mStblzReqd:Z

    .line 27
    return-void
.end method


# virtual methods
.method public getFileName()Ljava/lang/String;
    .locals 4

    .prologue
    .line 77
    iget-object v1, p0, Lcom/sec/android/app/ve/export/ExportThread;->fileName:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/app/ve/export/ExportThread;->fileName:Ljava/lang/String;

    const/16 v3, 0x2f

    invoke-virtual {v2, v3}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 78
    .local v0, "fName":Ljava/lang/String;
    return-object v0
.end method

.method public pauseExport()V
    .locals 1

    .prologue
    .line 68
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/app/video/editor/external/NativeInterface;->pauseExport()V

    .line 69
    return-void
.end method

.method public resumeExport()V
    .locals 1

    .prologue
    .line 72
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/app/video/editor/external/NativeInterface;->resumeExport()V

    .line 73
    return-void
.end method

.method public run()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 30
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/sec/android/app/ve/export/ExportThread;->wakelock:Landroid/os/PowerManager$WakeLock;

    .line 32
    const/4 v3, 0x1

    :try_start_0
    new-array v1, v3, [Ljava/lang/String;

    .line 33
    .local v1, "path":[Ljava/lang/String;
    const/4 v3, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/sec/android/app/ve/export/ExportThread;->fileName:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, ".tmp"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v3

    .line 35
    const-string v3, "power"

    invoke-static {v3}, Lcom/sec/android/app/ve/VEApp;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/PowerManager;

    const v4, 0x2000000a

    const-string v5, "VE Export Thread"

    invoke-virtual {v3, v4, v5}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/ve/export/ExportThread;->wakelock:Landroid/os/PowerManager$WakeLock;

    .line 36
    iget-object v3, p0, Lcom/sec/android/app/ve/export/ExportThread;->wakelock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v3}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 39
    iget-object v3, p0, Lcom/sec/android/app/ve/export/ExportThread;->mTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    iget-object v4, p0, Lcom/sec/android/app/ve/export/ExportThread;->mTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getTotalDuration()J

    move-result-wide v4

    long-to-int v4, v4

    invoke-virtual {v3, v4}, Lcom/samsung/app/video/editor/external/TranscodeElement;->setFullMovieDuration(I)V

    .line 40
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/ve/export/ExportThread;->mTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    const/4 v5, 0x0

    aget-object v5, v1, v5

    iget-boolean v6, p0, Lcom/sec/android/app/ve/export/ExportThread;->mStblzReqd:Z

    invoke-virtual {v3, v4, v5, v6}, Lcom/samsung/app/video/editor/external/NativeInterface;->exportProject(Lcom/samsung/app/video/editor/external/TranscodeElement;Ljava/lang/String;Z)I

    move-result v2

    .line 42
    .local v2, "res":I
    iget-object v3, p0, Lcom/sec/android/app/ve/export/ExportThread;->wakelock:Landroid/os/PowerManager$WakeLock;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/ve/export/ExportThread;->wakelock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v3}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 43
    iget-object v3, p0, Lcom/sec/android/app/ve/export/ExportThread;->wakelock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v3}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 45
    :cond_0
    if-ne v2, v7, :cond_1

    .line 46
    sget-object v3, Lcom/sec/android/app/ve/VEApp;->gExport:Lcom/sec/android/app/ve/export/Export;

    invoke-virtual {v3}, Lcom/sec/android/app/ve/export/Export;->getHandler()Landroid/os/Handler;

    move-result-object v3

    const/4 v4, 0x1

    const-wide/16 v6, 0xa

    invoke-virtual {v3, v4, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 61
    .end local v1    # "path":[Ljava/lang/String;
    .end local v2    # "res":I
    :goto_0
    return-void

    .line 50
    .restart local v1    # "path":[Ljava/lang/String;
    .restart local v2    # "res":I
    :cond_1
    const/16 v3, 0x1c

    if-ne v2, v3, :cond_2

    .line 51
    sget-object v3, Lcom/sec/android/app/ve/VEApp;->gExport:Lcom/sec/android/app/ve/export/Export;

    invoke-virtual {v3}, Lcom/sec/android/app/ve/export/Export;->getHandler()Landroid/os/Handler;

    move-result-object v3

    const/4 v4, 0x2

    const-wide/16 v6, 0xa

    invoke-virtual {v3, v4, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 56
    .end local v1    # "path":[Ljava/lang/String;
    .end local v2    # "res":I
    :catch_0
    move-exception v0

    .line 57
    .local v0, "ex":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 54
    .end local v0    # "ex":Ljava/lang/Exception;
    .restart local v1    # "path":[Ljava/lang/String;
    .restart local v2    # "res":I
    :cond_2
    :try_start_1
    sget-object v3, Lcom/sec/android/app/ve/VEApp;->gExport:Lcom/sec/android/app/ve/export/Export;

    invoke-virtual {v3}, Lcom/sec/android/app/ve/export/Export;->getHandler()Landroid/os/Handler;

    move-result-object v3

    const/4 v4, 0x0

    const-wide/16 v6, 0xa

    invoke-virtual {v3, v4, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public stopExport()V
    .locals 1

    .prologue
    .line 64
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/app/video/editor/external/NativeInterface;->stopExport()V

    .line 65
    return-void
.end method
