.class public Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogFileNameNull;
.super Lcom/sec/android/app/ve/common/AndroidUtils$DlgFragment;
.source "VideoEditorLiteActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DialogFileNameNull"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1177
    invoke-direct {p0}, Lcom/sec/android/app/ve/common/AndroidUtils$DlgFragment;-><init>()V

    .line 1178
    return-void
.end method

.method static newInstance()Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogFileNameNull;
    .locals 1

    .prologue
    .line 1181
    new-instance v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogFileNameNull;

    invoke-direct {v0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogFileNameNull;-><init>()V

    .line 1182
    .local v0, "dialog":Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogFileNameNull;
    return-object v0
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 1191
    invoke-super {p0, p1}, Lcom/sec/android/app/ve/common/AndroidUtils$DlgFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1192
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogFileNameNull;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogFileNameNull;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p1, Landroid/content/res/Configuration;->fontScale:F

    # getter for: Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->fontScale_value:F
    invoke-static {}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->access$7()F

    move-result v1

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    .line 1193
    iget v0, p1, Landroid/content/res/Configuration;->fontScale:F

    invoke-static {v0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->access$8(F)V

    .line 1194
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogFileNameNull;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/ve/common/AndroidUtils;->dismissDialog(Landroid/app/Activity;)V

    .line 1197
    :cond_0
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 1200
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogFileNameNull;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1201
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    const v1, 0x7f07020c

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 1202
    const v2, 0x7f07020b

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 1203
    const v2, 0x7f0700ab

    new-instance v3, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogFileNameNull$1;

    invoke-direct {v3, p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogFileNameNull$1;-><init>(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogFileNameNull;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1208
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    return-object v1
.end method

.method public onDestroyView()V
    .locals 0

    .prologue
    .line 1187
    invoke-super {p0}, Lcom/sec/android/app/ve/common/AndroidUtils$DlgFragment;->onDestroyView()V

    .line 1188
    return-void
.end method
