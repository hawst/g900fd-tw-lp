.class public Lcom/sec/android/app/ve/thread/WaveformDataFetcher;
.super Ljava/lang/Thread;
.source "WaveformDataFetcher.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/ve/thread/WaveformDataFetcher$WaveformDataFetcherCallback;,
        Lcom/sec/android/app/ve/thread/WaveformDataFetcher$WaveformJob;
    }
.end annotation


# static fields
.field private static final BUFFER_MAX_SIZE:I = 0x32000

.field private static final PROGRESS_UPDATE_FREQ:I = 0x1388

.field private static final TAG:Ljava/lang/String; = "WaveformDataFetcher"


# instance fields
.field private mJobList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/ve/thread/WaveformDataFetcher$WaveformJob;",
            ">;"
        }
    .end annotation
.end field

.field private mTerminate:Z

.field private utilCallback:Lcom/sec/android/app/ve/thread/WaveformDataFetcher$WaveformDataFetcherCallback;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 37
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/ve/thread/WaveformDataFetcher;->mJobList:Ljava/util/List;

    .line 42
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/ve/thread/WaveformDataFetcher;->utilCallback:Lcom/sec/android/app/ve/thread/WaveformDataFetcher$WaveformDataFetcherCallback;

    .line 47
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/ve/thread/WaveformDataFetcher;->mTerminate:Z

    .line 20
    return-void
.end method

.method private startDataLoadingFromNative(Lcom/sec/android/app/ve/thread/WaveformDataFetcher$WaveformJob;Lcom/sec/android/app/ve/data/WaveformDAO;)V
    .locals 12
    .param p1, "job"    # Lcom/sec/android/app/ve/thread/WaveformDataFetcher$WaveformJob;
    .param p2, "waveformDAO"    # Lcom/sec/android/app/ve/data/WaveformDAO;

    .prologue
    .line 79
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v8

    invoke-virtual {p1}, Lcom/sec/android/app/ve/thread/WaveformDataFetcher$WaveformJob;->getMediaSourcePath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/samsung/app/video/editor/external/NativeInterface;->waveformEngineInit(Ljava/lang/String;)I

    move-result v6

    .line 81
    .local v6, "mHandle":I
    const/4 v8, -0x1

    if-ne v6, v8, :cond_1

    .line 82
    const-string v8, "WaveformDataFetcher"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "waveform init failure for "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/sec/android/app/ve/thread/WaveformDataFetcher$WaveformJob;->getMediaSourcePath()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/app/ve/common/LogUtils;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    :cond_0
    :goto_0
    return-void

    .line 88
    :cond_1
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v8

    invoke-virtual {v8, v6}, Lcom/samsung/app/video/editor/external/NativeInterface;->waveformEngineSampleCount(I)I

    move-result v7

    .line 90
    .local v7, "sampleCount":I
    const/4 v5, 0x1

    .line 91
    .local v5, "freq":I
    const v8, 0x32000

    if-le v7, v8, :cond_2

    .line 94
    int-to-double v8, v7

    const-wide/high16 v10, 0x4109000000000000L    # 204800.0

    div-double/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v8

    double-to-int v5, v8

    .line 97
    :cond_2
    int-to-double v8, v7

    int-to-double v10, v5

    div-double/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v8

    double-to-int v2, v8

    .line 99
    .local v2, "bufferCapacity":I
    invoke-static {v2}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 100
    .local v1, "buffer":Ljava/nio/ByteBuffer;
    invoke-virtual {p2, v1}, Lcom/sec/android/app/ve/data/WaveformDAO;->setWaveformData(Ljava/nio/ByteBuffer;)V

    .line 101
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v8

    invoke-virtual {v8, v6, v5}, Lcom/samsung/app/video/editor/external/NativeInterface;->waveformEngineSetFrequency(II)I

    .line 104
    const/4 v3, -0x1

    .line 105
    .local v3, "bufferValidIndex":I
    const/4 v4, 0x1

    .line 106
    .local v4, "completeDecode":Z
    :cond_3
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v8

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v8, v6, v3}, Lcom/samsung/app/video/editor/external/NativeInterface;->waveformEngineProcess(II)I

    move-result v0

    .local v0, "b":I
    if-gez v0, :cond_4

    .line 126
    :goto_1
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v8

    invoke-virtual {v8, v6}, Lcom/samsung/app/video/editor/external/NativeInterface;->waveformEngineDeinit(I)I

    .line 127
    if-eqz v4, :cond_0

    if-lez v3, :cond_0

    if-eqz v1, :cond_0

    .line 129
    invoke-virtual {p0, p1, v1, v3}, Lcom/sec/android/app/ve/thread/WaveformDataFetcher;->saveBufferToFile(Lcom/sec/android/app/ve/thread/WaveformDataFetcher$WaveformJob;Ljava/nio/ByteBuffer;I)Z

    .line 130
    iget-object v8, p0, Lcom/sec/android/app/ve/thread/WaveformDataFetcher;->utilCallback:Lcom/sec/android/app/ve/thread/WaveformDataFetcher$WaveformDataFetcherCallback;

    if-eqz v8, :cond_0

    .line 131
    iget-object v8, p0, Lcom/sec/android/app/ve/thread/WaveformDataFetcher;->utilCallback:Lcom/sec/android/app/ve/thread/WaveformDataFetcher$WaveformDataFetcherCallback;

    invoke-interface {v8, p2}, Lcom/sec/android/app/ve/thread/WaveformDataFetcher$WaveformDataFetcherCallback;->fetchingComplete(Lcom/sec/android/app/ve/data/WaveformDAO;)Z

    goto :goto_0

    .line 107
    :cond_4
    if-lt v3, v2, :cond_5

    .line 108
    add-int/lit8 v3, v2, -0x1

    .line 109
    goto :goto_1

    .line 111
    :cond_5
    if-eqz v1, :cond_6

    iget-object v8, p0, Lcom/sec/android/app/ve/thread/WaveformDataFetcher;->utilCallback:Lcom/sec/android/app/ve/thread/WaveformDataFetcher$WaveformDataFetcherCallback;

    if-nez v8, :cond_7

    .line 112
    :cond_6
    const/4 v4, 0x0

    .line 113
    goto :goto_1

    .line 115
    :cond_7
    int-to-byte v8, v0

    invoke-virtual {v1, v3, v8}, Ljava/nio/ByteBuffer;->put(IB)Ljava/nio/ByteBuffer;

    .line 116
    rem-int/lit16 v8, v3, 0x1388

    if-nez v8, :cond_3

    iget-object v8, p0, Lcom/sec/android/app/ve/thread/WaveformDataFetcher;->utilCallback:Lcom/sec/android/app/ve/thread/WaveformDataFetcher$WaveformDataFetcherCallback;

    if-eqz v8, :cond_3

    .line 117
    iget-object v8, p0, Lcom/sec/android/app/ve/thread/WaveformDataFetcher;->utilCallback:Lcom/sec/android/app/ve/thread/WaveformDataFetcher$WaveformDataFetcherCallback;

    add-int/lit8 v9, v3, 0x1

    invoke-interface {v8, p2, v9}, Lcom/sec/android/app/ve/thread/WaveformDataFetcher$WaveformDataFetcherCallback;->fetchingProgress(Lcom/sec/android/app/ve/data/WaveformDAO;I)Z

    move-result v8

    if-nez v8, :cond_3

    .line 119
    const/4 v4, 0x0

    .line 120
    goto :goto_1
.end method


# virtual methods
.method public addOperation(Ljava/lang/String;)V
    .locals 2
    .param p1, "filepath"    # Ljava/lang/String;

    .prologue
    .line 179
    invoke-virtual {p0, p1}, Lcom/sec/android/app/ve/thread/WaveformDataFetcher;->isJobPresent(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 180
    new-instance v0, Lcom/sec/android/app/ve/thread/WaveformDataFetcher$WaveformJob;

    invoke-direct {v0, p1}, Lcom/sec/android/app/ve/thread/WaveformDataFetcher$WaveformJob;-><init>(Ljava/lang/String;)V

    .line 181
    .local v0, "job":Lcom/sec/android/app/ve/thread/WaveformDataFetcher$WaveformJob;
    iget-object v1, p0, Lcom/sec/android/app/ve/thread/WaveformDataFetcher;->mJobList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 182
    monitor-enter p0

    .line 183
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V

    .line 182
    monitor-exit p0

    .line 186
    .end local v0    # "job":Lcom/sec/android/app/ve/thread/WaveformDataFetcher$WaveformJob;
    :cond_0
    return-void

    .line 182
    .restart local v0    # "job":Lcom/sec/android/app/ve/thread/WaveformDataFetcher$WaveformJob;
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public isJobPresent(Ljava/lang/String;)Z
    .locals 6
    .param p1, "filepath"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 189
    iget-object v4, p0, Lcom/sec/android/app/ve/thread/WaveformDataFetcher;->mJobList:Ljava/util/List;

    if-nez v4, :cond_0

    .line 214
    :goto_0
    return v3

    .line 194
    :cond_0
    :try_start_0
    iget-object v5, p0, Lcom/sec/android/app/ve/thread/WaveformDataFetcher;->mJobList:Ljava/util/List;

    monitor-enter v5
    :try_end_0
    .catch Ljava/util/NoSuchElementException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/ConcurrentModificationException; {:try_start_0 .. :try_end_0} :catch_1

    .line 196
    :try_start_1
    iget-object v4, p0, Lcom/sec/android/app/ve/thread/WaveformDataFetcher;->mJobList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 197
    .local v1, "lIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/app/ve/thread/WaveformDataFetcher$WaveformJob;>;"
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_2

    .line 194
    monitor-exit v5

    goto :goto_0

    .end local v1    # "lIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/app/ve/thread/WaveformDataFetcher$WaveformJob;>;"
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v4
    :try_end_2
    .catch Ljava/util/NoSuchElementException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/util/ConcurrentModificationException; {:try_start_2 .. :try_end_2} :catch_1

    .line 206
    :catch_0
    move-exception v0

    .line 208
    .local v0, "e":Ljava/util/NoSuchElementException;
    invoke-virtual {v0}, Ljava/util/NoSuchElementException;->printStackTrace()V

    goto :goto_0

    .line 199
    .end local v0    # "e":Ljava/util/NoSuchElementException;
    .restart local v1    # "lIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/app/ve/thread/WaveformDataFetcher$WaveformJob;>;"
    :cond_2
    :try_start_3
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/ve/thread/WaveformDataFetcher$WaveformJob;

    .line 200
    .local v2, "waveformJob":Lcom/sec/android/app/ve/thread/WaveformDataFetcher$WaveformJob;
    invoke-virtual {v2}, Lcom/sec/android/app/ve/thread/WaveformDataFetcher$WaveformJob;->getMediaSourcePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 201
    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    const/4 v3, 0x1

    goto :goto_0

    .line 210
    .end local v1    # "lIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/app/ve/thread/WaveformDataFetcher$WaveformJob;>;"
    .end local v2    # "waveformJob":Lcom/sec/android/app/ve/thread/WaveformDataFetcher$WaveformJob;
    :catch_1
    move-exception v0

    .line 212
    .local v0, "e":Ljava/util/ConcurrentModificationException;
    invoke-virtual {v0}, Ljava/util/ConcurrentModificationException;->printStackTrace()V

    goto :goto_0
.end method

.method public run()V
    .locals 6

    .prologue
    .line 51
    :cond_0
    :goto_0
    iget-boolean v3, p0, Lcom/sec/android/app/ve/thread/WaveformDataFetcher;->mTerminate:Z

    if-eqz v3, :cond_1

    .line 73
    return-void

    .line 53
    :cond_1
    monitor-enter p0

    .line 54
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/ve/thread/WaveformDataFetcher;->mJobList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-nez v3, :cond_2

    .line 57
    const-wide/16 v4, 0x7d0

    :try_start_1
    invoke-virtual {p0, v4, v5}, Ljava/lang/Object;->wait(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 53
    :cond_2
    :goto_1
    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 64
    iget-object v3, p0, Lcom/sec/android/app/ve/thread/WaveformDataFetcher;->mJobList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_0

    .line 66
    iget-object v3, p0, Lcom/sec/android/app/ve/thread/WaveformDataFetcher;->mJobList:Ljava/util/List;

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/ve/thread/WaveformDataFetcher$WaveformJob;

    .line 67
    .local v1, "job":Lcom/sec/android/app/ve/thread/WaveformDataFetcher$WaveformJob;
    new-instance v2, Lcom/sec/android/app/ve/data/WaveformDAO;

    invoke-direct {v2}, Lcom/sec/android/app/ve/data/WaveformDAO;-><init>()V

    .line 68
    .local v2, "waveformDAO":Lcom/sec/android/app/ve/data/WaveformDAO;
    invoke-virtual {v1}, Lcom/sec/android/app/ve/thread/WaveformDataFetcher$WaveformJob;->getMediaSourcePath()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/ve/util/WaveformUtil;->generateBufferFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/ve/data/WaveformDAO;->setDataFileName(Ljava/lang/String;)V

    .line 69
    invoke-virtual {v1}, Lcom/sec/android/app/ve/thread/WaveformDataFetcher$WaveformJob;->getMediaSourcePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/ve/data/WaveformDAO;->setMediaSourcePath(Ljava/lang/String;)V

    .line 70
    invoke-direct {p0, v1, v2}, Lcom/sec/android/app/ve/thread/WaveformDataFetcher;->startDataLoadingFromNative(Lcom/sec/android/app/ve/thread/WaveformDataFetcher$WaveformJob;Lcom/sec/android/app/ve/data/WaveformDAO;)V

    goto :goto_0

    .line 58
    .end local v1    # "job":Lcom/sec/android/app/ve/thread/WaveformDataFetcher$WaveformJob;
    .end local v2    # "waveformDAO":Lcom/sec/android/app/ve/data/WaveformDAO;
    :catch_0
    move-exception v0

    .line 59
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_3
    const-string v3, "WaveformDataFetcher"

    const-string v4, "Someone woke up this thread."

    invoke-static {v3, v4}, Lcom/sec/android/app/ve/common/LogUtils;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1

    .line 53
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v3

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v3
.end method

.method public saveBufferToFile(Lcom/sec/android/app/ve/thread/WaveformDataFetcher$WaveformJob;Ljava/nio/ByteBuffer;I)Z
    .locals 8
    .param p1, "job"    # Lcom/sec/android/app/ve/thread/WaveformDataFetcher$WaveformJob;
    .param p2, "buffer"    # Ljava/nio/ByteBuffer;
    .param p3, "bufferValidIndex"    # I

    .prologue
    const/4 v6, 0x0

    .line 146
    const/4 v5, 0x1

    .line 147
    .local v5, "result":Z
    const/4 v3, 0x0

    .line 148
    .local v3, "fos":Ljava/io/FileOutputStream;
    if-nez p2, :cond_0

    .line 171
    :goto_0
    return v6

    .line 151
    :cond_0
    :try_start_0
    new-instance v0, Ljava/io/File;

    invoke-virtual {p1}, Lcom/sec/android/app/ve/thread/WaveformDataFetcher$WaveformJob;->getMediaSourcePath()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/app/ve/util/WaveformUtil;->generateBufferFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v0, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 152
    .local v0, "bufferFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v6

    if-nez v6, :cond_1

    .line 153
    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z

    .line 154
    :cond_1
    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 156
    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .local v4, "fos":Ljava/io/FileOutputStream;
    :try_start_1
    monitor-enter p2
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 157
    :try_start_2
    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v4, v6, v7, p3}, Ljava/io/FileOutputStream;->write([BII)V

    .line 156
    monitor-exit p2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 163
    if-eqz v4, :cond_4

    .line 165
    :try_start_3
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    move-object v3, v4

    .end local v0    # "bufferFile":Ljava/io/File;
    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .restart local v3    # "fos":Ljava/io/FileOutputStream;
    :cond_2
    :goto_1
    move v6, v5

    .line 171
    goto :goto_0

    .line 156
    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .restart local v0    # "bufferFile":Ljava/io/File;
    .restart local v4    # "fos":Ljava/io/FileOutputStream;
    :catchall_0
    move-exception v6

    :try_start_4
    monitor-exit p2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    throw v6
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 160
    :catch_0
    move-exception v2

    move-object v3, v4

    .line 161
    .end local v0    # "bufferFile":Ljava/io/File;
    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .local v2, "exception":Ljava/io/IOException;
    .restart local v3    # "fos":Ljava/io/FileOutputStream;
    :goto_2
    const/4 v5, 0x0

    .line 163
    if-eqz v3, :cond_2

    .line 165
    :try_start_6
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    goto :goto_1

    .line 166
    :catch_1
    move-exception v1

    .line 167
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 162
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "exception":Ljava/io/IOException;
    :catchall_1
    move-exception v6

    .line 163
    :goto_3
    if-eqz v3, :cond_3

    .line 165
    :try_start_7
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2

    .line 170
    :cond_3
    :goto_4
    throw v6

    .line 166
    :catch_2
    move-exception v1

    .line 167
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 166
    .end local v1    # "e":Ljava/io/IOException;
    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .restart local v0    # "bufferFile":Ljava/io/File;
    .restart local v4    # "fos":Ljava/io/FileOutputStream;
    :catch_3
    move-exception v1

    .line 167
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .end local v1    # "e":Ljava/io/IOException;
    :cond_4
    move-object v3, v4

    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .restart local v3    # "fos":Ljava/io/FileOutputStream;
    goto :goto_1

    .line 162
    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "fos":Ljava/io/FileOutputStream;
    :catchall_2
    move-exception v6

    move-object v3, v4

    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .restart local v3    # "fos":Ljava/io/FileOutputStream;
    goto :goto_3

    .line 160
    .end local v0    # "bufferFile":Ljava/io/File;
    :catch_4
    move-exception v2

    goto :goto_2
.end method

.method public setWaveformUtilCallback(Lcom/sec/android/app/ve/thread/WaveformDataFetcher$WaveformDataFetcherCallback;)V
    .locals 0
    .param p1, "utilCallback"    # Lcom/sec/android/app/ve/thread/WaveformDataFetcher$WaveformDataFetcherCallback;

    .prologue
    .line 218
    iput-object p1, p0, Lcom/sec/android/app/ve/thread/WaveformDataFetcher;->utilCallback:Lcom/sec/android/app/ve/thread/WaveformDataFetcher$WaveformDataFetcherCallback;

    .line 219
    return-void
.end method

.method public terminate()V
    .locals 1

    .prologue
    .line 222
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/ve/thread/WaveformDataFetcher;->mTerminate:Z

    .line 223
    invoke-virtual {p0}, Lcom/sec/android/app/ve/thread/WaveformDataFetcher;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 224
    monitor-enter p0

    .line 225
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V

    .line 224
    monitor-exit p0

    .line 228
    :cond_0
    return-void

    .line 224
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
