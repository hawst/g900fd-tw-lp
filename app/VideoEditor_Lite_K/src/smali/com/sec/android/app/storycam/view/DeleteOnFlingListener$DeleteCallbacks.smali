.class public interface abstract Lcom/sec/android/app/storycam/view/DeleteOnFlingListener$DeleteCallbacks;
.super Ljava/lang/Object;
.source "DeleteOnFlingListener.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/storycam/view/DeleteOnFlingListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "DeleteCallbacks"
.end annotation


# virtual methods
.method public abstract canDelete()Z
.end method

.method public abstract getConfig()Landroid/content/res/Configuration;
.end method

.method public abstract onDelete(Landroid/view/View;)V
.end method
