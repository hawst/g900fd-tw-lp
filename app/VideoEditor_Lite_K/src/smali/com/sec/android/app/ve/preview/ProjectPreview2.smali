.class public Lcom/sec/android/app/ve/preview/ProjectPreview2;
.super Lcom/sec/android/app/ve/PreviewPlayerInterface;
.source "ProjectPreview2.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/ve/preview/ProjectPreview2$UnplugHeadsetReceiver;,
        Lcom/sec/android/app/ve/preview/ProjectPreview2$notifyUIThread;
    }
.end annotation


# static fields
.field private static final STATE_COMPLETED:I = 0x4

.field private static final STATE_DEINIT:I = 0x3

.field private static final STATE_ERROR:I = -0x1

.field private static final STATE_IDLE:I = 0x0

.field private static final STATE_INIT:I = 0x1

.field private static final STATE_PLAYING:I = 0x2


# instance fields
.field private isCompleted:Z

.field private mActivity:Landroid/app/Activity;

.field public mAdapter:Lcom/sec/android/app/ve/PreviewPlayerInterface$Adapter;

.field private mAudioChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field private mAudioManager:Landroid/media/AudioManager;

.field private mContext:Landroid/content/Context;

.field private mCurrentState:I

.field private mEngineListener:Lcom/samsung/app/video/editor/external/NativeInterface$previewPlayerStateListener;

.field private mFrameThread:Lcom/sec/android/app/ve/preview/PreviewFrameThread;

.field private mFrameThreadAdapter:Lcom/sec/android/app/ve/preview/PreviewFrameThread$PreviewFrameThreadAdapter;

.field private mNextState:I

.field private mPlayPos:J

.field private mSurfaceCallback:Landroid/view/SurfaceHolder$Callback;

.field private mSurfaceHolder:Landroid/view/SurfaceHolder;

.field private mSurfaceView:Landroid/view/SurfaceView;

.field private pm:Landroid/os/PowerManager;

.field private receiver:Lcom/sec/android/app/ve/preview/ProjectPreview2$UnplugHeadsetReceiver;

.field private syncToken:Ljava/lang/Object;

.field private timer:Ljava/util/Timer;

.field private waitForPreviewMsgClear:Z

.field private waitForPreviewMsgClearBefExport:Z

.field private waitForSurfaceCreation:Z

.field private wakelock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 82
    invoke-direct {p0, p1}, Lcom/sec/android/app/ve/PreviewPlayerInterface;-><init>(Landroid/content/Context;)V

    .line 53
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    .line 58
    iput v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mCurrentState:I

    .line 59
    iput v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mNextState:I

    .line 60
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mPlayPos:J

    .line 64
    iput-boolean v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->waitForPreviewMsgClear:Z

    .line 65
    iput-boolean v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->waitForSurfaceCreation:Z

    .line 66
    iput-boolean v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->waitForPreviewMsgClearBefExport:Z

    .line 68
    iput-boolean v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->isCompleted:Z

    .line 123
    new-instance v0, Lcom/sec/android/app/ve/preview/ProjectPreview2$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/ve/preview/ProjectPreview2$1;-><init>(Lcom/sec/android/app/ve/preview/ProjectPreview2;)V

    iput-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mFrameThreadAdapter:Lcom/sec/android/app/ve/preview/PreviewFrameThread$PreviewFrameThreadAdapter;

    .line 564
    new-instance v0, Lcom/sec/android/app/ve/preview/ProjectPreview2$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/ve/preview/ProjectPreview2$2;-><init>(Lcom/sec/android/app/ve/preview/ProjectPreview2;)V

    iput-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mSurfaceCallback:Landroid/view/SurfaceHolder$Callback;

    .line 650
    new-instance v0, Lcom/sec/android/app/ve/preview/ProjectPreview2$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/ve/preview/ProjectPreview2$3;-><init>(Lcom/sec/android/app/ve/preview/ProjectPreview2;)V

    iput-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mEngineListener:Lcom/samsung/app/video/editor/external/NativeInterface$previewPlayerStateListener;

    .line 752
    new-instance v0, Lcom/sec/android/app/ve/preview/ProjectPreview2$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/ve/preview/ProjectPreview2$4;-><init>(Lcom/sec/android/app/ve/preview/ProjectPreview2;)V

    iput-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mAudioChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 83
    iput-object p1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mContext:Landroid/content/Context;

    .line 84
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, 0x0

    .line 77
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/ve/PreviewPlayerInterface;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 53
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    .line 58
    iput v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mCurrentState:I

    .line 59
    iput v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mNextState:I

    .line 60
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mPlayPos:J

    .line 64
    iput-boolean v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->waitForPreviewMsgClear:Z

    .line 65
    iput-boolean v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->waitForSurfaceCreation:Z

    .line 66
    iput-boolean v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->waitForPreviewMsgClearBefExport:Z

    .line 68
    iput-boolean v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->isCompleted:Z

    .line 123
    new-instance v0, Lcom/sec/android/app/ve/preview/ProjectPreview2$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/ve/preview/ProjectPreview2$1;-><init>(Lcom/sec/android/app/ve/preview/ProjectPreview2;)V

    iput-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mFrameThreadAdapter:Lcom/sec/android/app/ve/preview/PreviewFrameThread$PreviewFrameThreadAdapter;

    .line 564
    new-instance v0, Lcom/sec/android/app/ve/preview/ProjectPreview2$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/ve/preview/ProjectPreview2$2;-><init>(Lcom/sec/android/app/ve/preview/ProjectPreview2;)V

    iput-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mSurfaceCallback:Landroid/view/SurfaceHolder$Callback;

    .line 650
    new-instance v0, Lcom/sec/android/app/ve/preview/ProjectPreview2$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/ve/preview/ProjectPreview2$3;-><init>(Lcom/sec/android/app/ve/preview/ProjectPreview2;)V

    iput-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mEngineListener:Lcom/samsung/app/video/editor/external/NativeInterface$previewPlayerStateListener;

    .line 752
    new-instance v0, Lcom/sec/android/app/ve/preview/ProjectPreview2$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/ve/preview/ProjectPreview2$4;-><init>(Lcom/sec/android/app/ve/preview/ProjectPreview2;)V

    iput-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mAudioChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 78
    iput-object p1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mContext:Landroid/content/Context;

    .line 79
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v2, 0x0

    .line 72
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/ve/PreviewPlayerInterface;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 53
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    .line 58
    iput v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mCurrentState:I

    .line 59
    iput v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mNextState:I

    .line 60
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mPlayPos:J

    .line 64
    iput-boolean v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->waitForPreviewMsgClear:Z

    .line 65
    iput-boolean v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->waitForSurfaceCreation:Z

    .line 66
    iput-boolean v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->waitForPreviewMsgClearBefExport:Z

    .line 68
    iput-boolean v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->isCompleted:Z

    .line 123
    new-instance v0, Lcom/sec/android/app/ve/preview/ProjectPreview2$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/ve/preview/ProjectPreview2$1;-><init>(Lcom/sec/android/app/ve/preview/ProjectPreview2;)V

    iput-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mFrameThreadAdapter:Lcom/sec/android/app/ve/preview/PreviewFrameThread$PreviewFrameThreadAdapter;

    .line 564
    new-instance v0, Lcom/sec/android/app/ve/preview/ProjectPreview2$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/ve/preview/ProjectPreview2$2;-><init>(Lcom/sec/android/app/ve/preview/ProjectPreview2;)V

    iput-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mSurfaceCallback:Landroid/view/SurfaceHolder$Callback;

    .line 650
    new-instance v0, Lcom/sec/android/app/ve/preview/ProjectPreview2$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/ve/preview/ProjectPreview2$3;-><init>(Lcom/sec/android/app/ve/preview/ProjectPreview2;)V

    iput-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mEngineListener:Lcom/samsung/app/video/editor/external/NativeInterface$previewPlayerStateListener;

    .line 752
    new-instance v0, Lcom/sec/android/app/ve/preview/ProjectPreview2$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/ve/preview/ProjectPreview2$4;-><init>(Lcom/sec/android/app/ve/preview/ProjectPreview2;)V

    iput-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mAudioChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 73
    iput-object p1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mContext:Landroid/content/Context;

    .line 74
    return-void
.end method

.method private _play(J)V
    .locals 4
    .param p1, "pos"    # J

    .prologue
    const/4 v3, 0x1

    .line 393
    iget v1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mCurrentState:I

    if-nez v1, :cond_5

    sget-boolean v1, Lcom/samsung/app/video/editor/external/NativeInterface;->isInitFIMC:Z

    if-eqz v1, :cond_5

    .line 395
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->syncToken:Ljava/lang/Object;

    .line 396
    iput-boolean v3, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->waitForPreviewMsgClear:Z

    .line 400
    invoke-direct {p0}, Lcom/sec/android/app/ve/preview/ProjectPreview2;->startTimer()V

    .line 401
    iget-object v1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mFrameThread:Lcom/sec/android/app/ve/preview/PreviewFrameThread;

    if-eqz v1, :cond_0

    .line 402
    iget-object v1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mFrameThread:Lcom/sec/android/app/ve/preview/PreviewFrameThread;

    invoke-virtual {v1}, Lcom/sec/android/app/ve/preview/PreviewFrameThread;->clearPendingOperation()V

    .line 405
    :cond_0
    :try_start_0
    iget v1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mNextState:I

    if-eqz v1, :cond_1

    iget v1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mNextState:I

    if-ne v1, v3, :cond_4

    .line 406
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->syncToken:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 411
    :try_start_1
    iget v1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mNextState:I

    if-eqz v1, :cond_2

    iget v1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mNextState:I

    if-ne v1, v3, :cond_3

    .line 412
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->syncToken:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V

    .line 406
    :cond_3
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 417
    :cond_4
    :try_start_2
    invoke-direct {p0}, Lcom/sec/android/app/ve/preview/ProjectPreview2;->handlePlayEventCompletion()V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    .line 423
    :cond_5
    :goto_0
    return-void

    .line 406
    :catchall_0
    move-exception v1

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v1
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0

    .line 419
    :catch_0
    move-exception v0

    .line 420
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method

.method private _stop(Z)V
    .locals 4
    .param p1, "force"    # Z

    .prologue
    const/4 v3, 0x0

    .line 484
    const-string v1, "PLAYER_STATE"

    const-string v2, "_stop"

    invoke-static {v1, v2}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 486
    iget-object v1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mContext:Landroid/content/Context;

    invoke-static {v1, v3}, Lcom/sec/android/app/ve/common/ConfigUtils;->setLockForHDMoviePlay(Landroid/content/Context;Z)V

    .line 488
    invoke-direct {p0}, Lcom/sec/android/app/ve/preview/ProjectPreview2;->releaseWakeLock()V

    .line 490
    if-nez p1, :cond_0

    const/4 v1, 0x2

    iget v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mCurrentState:I

    if-ne v1, v2, :cond_1

    .line 493
    :cond_0
    :try_start_0
    const-string v1, "PLAYER_STATE"

    const-string v2, "calling native _stop"

    invoke-static {v1, v2}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 495
    const-string v1, "PLAYER_STATE"

    const-string v2, "calling wait"

    invoke-static {v1, v2}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 498
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/NativeInterface;->stopVeEngine()V

    .line 502
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mNextState:I

    .line 503
    const-string v1, "PLAYER_STATE"

    const-string v2, "calling wait done"

    invoke-static {v1, v2}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 506
    invoke-direct {p0}, Lcom/sec/android/app/ve/preview/ProjectPreview2;->handleStopEventCompletion()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 512
    :cond_1
    :goto_0
    return-void

    .line 508
    :catch_0
    move-exception v0

    .line 509
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic access$0(Lcom/sec/android/app/ve/preview/ProjectPreview2;)Lcom/sec/android/app/ve/preview/PreviewFrameThread;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mFrameThread:Lcom/sec/android/app/ve/preview/PreviewFrameThread;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/app/ve/preview/ProjectPreview2;Z)V
    .locals 0

    .prologue
    .line 39
    iput-boolean p1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->isSurfaceAvailable:Z

    return-void
.end method

.method static synthetic access$10(Lcom/sec/android/app/ve/preview/ProjectPreview2;)I
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mNextState:I

    return v0
.end method

.method static synthetic access$11(Lcom/sec/android/app/ve/preview/ProjectPreview2;I)V
    .locals 0

    .prologue
    .line 59
    iput p1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mNextState:I

    return-void
.end method

.method static synthetic access$12(Lcom/sec/android/app/ve/preview/ProjectPreview2;)V
    .locals 0

    .prologue
    .line 426
    invoke-direct {p0}, Lcom/sec/android/app/ve/preview/ProjectPreview2;->handlePlayTimeError()V

    return-void
.end method

.method static synthetic access$13(Lcom/sec/android/app/ve/preview/ProjectPreview2;Z)V
    .locals 0

    .prologue
    .line 68
    iput-boolean p1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->isCompleted:Z

    return-void
.end method

.method static synthetic access$14(Lcom/sec/android/app/ve/preview/ProjectPreview2;)V
    .locals 0

    .prologue
    .line 514
    invoke-direct {p0}, Lcom/sec/android/app/ve/preview/ProjectPreview2;->handleCompletedEvent()V

    return-void
.end method

.method static synthetic access$15(Lcom/sec/android/app/ve/preview/ProjectPreview2;Z)V
    .locals 0

    .prologue
    .line 64
    iput-boolean p1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->waitForPreviewMsgClear:Z

    return-void
.end method

.method static synthetic access$16(Lcom/sec/android/app/ve/preview/ProjectPreview2;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->syncToken:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$17(Lcom/sec/android/app/ve/preview/ProjectPreview2;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$18(Lcom/sec/android/app/ve/preview/ProjectPreview2;)Landroid/media/AudioManager;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mAudioManager:Landroid/media/AudioManager;

    return-object v0
.end method

.method static synthetic access$19(Lcom/sec/android/app/ve/preview/ProjectPreview2;)Landroid/media/AudioManager$OnAudioFocusChangeListener;
    .locals 1

    .prologue
    .line 752
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mAudioChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/app/ve/preview/ProjectPreview2;Z)V
    .locals 0

    .prologue
    .line 482
    invoke-direct {p0, p1}, Lcom/sec/android/app/ve/preview/ProjectPreview2;->_stop(Z)V

    return-void
.end method

.method static synthetic access$3(Lcom/sec/android/app/ve/preview/ProjectPreview2;)I
    .locals 1

    .prologue
    .line 58
    iget v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mCurrentState:I

    return v0
.end method

.method static synthetic access$4(Lcom/sec/android/app/ve/preview/ProjectPreview2;Landroid/view/SurfaceHolder;)V
    .locals 0

    .prologue
    .line 53
    iput-object p1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    return-void
.end method

.method static synthetic access$5(Lcom/sec/android/app/ve/preview/ProjectPreview2;)Landroid/view/SurfaceHolder;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    return-object v0
.end method

.method static synthetic access$6(Lcom/sec/android/app/ve/preview/ProjectPreview2;)Z
    .locals 1

    .prologue
    .line 65
    iget-boolean v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->waitForSurfaceCreation:Z

    return v0
.end method

.method static synthetic access$7(Lcom/sec/android/app/ve/preview/ProjectPreview2;Z)V
    .locals 0

    .prologue
    .line 65
    iput-boolean p1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->waitForSurfaceCreation:Z

    return-void
.end method

.method static synthetic access$8(Lcom/sec/android/app/ve/preview/ProjectPreview2;)J
    .locals 2

    .prologue
    .line 60
    iget-wide v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mPlayPos:J

    return-wide v0
.end method

.method static synthetic access$9(Lcom/sec/android/app/ve/preview/ProjectPreview2;J)V
    .locals 1

    .prologue
    .line 388
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/ve/preview/ProjectPreview2;->_play(J)V

    return-void
.end method

.method private acquireWakeLock()V
    .locals 3

    .prologue
    .line 373
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->wakelock:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->wakelock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-nez v0, :cond_1

    .line 374
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->pm:Landroid/os/PowerManager;

    const v1, 0x2000000a

    const-string v2, "VideoEditor - Player"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->wakelock:Landroid/os/PowerManager$WakeLock;

    .line 375
    const-string v0, "PLAYER_STATE"

    const-string v1, "acquiring"

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 376
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->wakelock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 378
    :cond_1
    return-void
.end method

.method private cancelTimer()V
    .locals 1

    .prologue
    .line 812
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->timer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 813
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->timer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 814
    :cond_0
    return-void
.end method

.method private handleCompletedEvent()V
    .locals 1

    .prologue
    .line 515
    new-instance v0, Lcom/sec/android/app/ve/preview/ProjectPreview2$8;

    invoke-direct {v0, p0}, Lcom/sec/android/app/ve/preview/ProjectPreview2$8;-><init>(Lcom/sec/android/app/ve/preview/ProjectPreview2;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/ve/preview/ProjectPreview2;->post(Ljava/lang/Runnable;)Z

    .line 521
    return-void
.end method

.method private handlePlayEventCompletion()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x2

    .line 439
    const-string v0, "PLAYER_STATE"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "handlePlayEventCompletion ==> NextState = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mNextState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " CurrentState = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mCurrentState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 440
    iget v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mCurrentState:I

    if-nez v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mNextState:I

    if-nez v0, :cond_0

    .line 442
    const-string v0, "PLAYER_STATE"

    const-string v1, "handlePlayEventCompletion ==> The error happend on App side before start of playback"

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 480
    :goto_0
    return-void

    .line 455
    :cond_0
    iget v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mNextState:I

    if-ne v0, v3, :cond_2

    .line 457
    iput v3, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mCurrentState:I

    .line 458
    new-instance v0, Lcom/sec/android/app/ve/preview/ProjectPreview2$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/ve/preview/ProjectPreview2$6;-><init>(Lcom/sec/android/app/ve/preview/ProjectPreview2;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/ve/preview/ProjectPreview2;->post(Ljava/lang/Runnable;)Z

    .line 479
    :cond_1
    :goto_1
    const-string v0, "PLAYER_STATE"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "handlePlayEventCompletion ==> NextState = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mNextState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " CurrentState = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mCurrentState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 467
    :cond_2
    iget v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mCurrentState:I

    if-nez v0, :cond_1

    iget v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mNextState:I

    if-eq v0, v3, :cond_1

    .line 469
    iput v4, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mNextState:I

    .line 470
    iput v4, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mCurrentState:I

    .line 471
    new-instance v0, Lcom/sec/android/app/ve/preview/ProjectPreview2$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/ve/preview/ProjectPreview2$7;-><init>(Lcom/sec/android/app/ve/preview/ProjectPreview2;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/ve/preview/ProjectPreview2;->post(Ljava/lang/Runnable;)Z

    goto :goto_1
.end method

.method private handlePlayTimeError()V
    .locals 1

    .prologue
    .line 427
    new-instance v0, Lcom/sec/android/app/ve/preview/ProjectPreview2$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/ve/preview/ProjectPreview2$5;-><init>(Lcom/sec/android/app/ve/preview/ProjectPreview2;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/ve/preview/ProjectPreview2;->post(Ljava/lang/Runnable;)Z

    .line 436
    return-void
.end method

.method private handleStopEventCompletion()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 524
    const-string v0, "PLAYER_STATE"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "handleStopEventCompletion ==> NextState = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mNextState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " CurrentState = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mCurrentState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 525
    const-string v0, "PLAYER_STATE"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "handleStopEventCompletion ==> isCompleted = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->isCompleted:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 526
    iget v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mNextState:I

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->isCompleted:Z

    if-eqz v0, :cond_0

    .line 528
    iput-boolean v4, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->isCompleted:Z

    .line 529
    new-instance v0, Lcom/sec/android/app/ve/preview/ProjectPreview2$9;

    invoke-direct {v0, p0}, Lcom/sec/android/app/ve/preview/ProjectPreview2$9;-><init>(Lcom/sec/android/app/ve/preview/ProjectPreview2;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/ve/preview/ProjectPreview2;->post(Ljava/lang/Runnable;)Z

    .line 551
    :goto_0
    new-instance v0, Lcom/sec/android/app/ve/preview/ProjectPreview2$11;

    invoke-direct {v0, p0}, Lcom/sec/android/app/ve/preview/ProjectPreview2$11;-><init>(Lcom/sec/android/app/ve/preview/ProjectPreview2;)V

    .line 557
    const-wide/16 v2, 0xa

    .line 551
    invoke-virtual {p0, v0, v2, v3}, Lcom/sec/android/app/ve/preview/ProjectPreview2;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 559
    iput v4, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mCurrentState:I

    iput v4, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mNextState:I

    .line 560
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/app/video/editor/external/NativeInterface;->setStackStateListener(Lcom/samsung/app/video/editor/external/NativeInterface$previewPlayerStateListener;)V

    .line 561
    const-string v0, "PLAYER_STATE"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "handleStopEventCompletion ==> NextState = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mNextState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " CurrentState = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mCurrentState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 562
    return-void

    .line 540
    :cond_0
    new-instance v0, Lcom/sec/android/app/ve/preview/ProjectPreview2$10;

    invoke-direct {v0, p0}, Lcom/sec/android/app/ve/preview/ProjectPreview2$10;-><init>(Lcom/sec/android/app/ve/preview/ProjectPreview2;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/ve/preview/ProjectPreview2;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method private initialize()V
    .locals 13

    .prologue
    const/4 v8, 0x0

    .line 104
    new-instance v1, Lcom/sec/android/app/ve/preview/PreviewFrameThread;

    invoke-direct {v1}, Lcom/sec/android/app/ve/preview/PreviewFrameThread;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mFrameThreadAdapter:Lcom/sec/android/app/ve/preview/PreviewFrameThread$PreviewFrameThreadAdapter;

    invoke-virtual {v1, v3}, Lcom/sec/android/app/ve/preview/PreviewFrameThread;->init(Lcom/sec/android/app/ve/preview/PreviewFrameThread$PreviewFrameThreadAdapter;)Lcom/sec/android/app/ve/preview/PreviewFrameThread;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mFrameThread:Lcom/sec/android/app/ve/preview/PreviewFrameThread;

    .line 106
    iget-object v1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mAdapter:Lcom/sec/android/app/ve/PreviewPlayerInterface$Adapter;

    if-nez v1, :cond_0

    .line 121
    :goto_0
    return-void

    .line 109
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mAdapter:Lcom/sec/android/app/ve/PreviewPlayerInterface$Adapter;

    invoke-virtual {v1}, Lcom/sec/android/app/ve/PreviewPlayerInterface$Adapter;->getCurrentTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v12

    .line 110
    .local v12, "trans":Lcom/samsung/app/video/editor/external/TranscodeElement;
    if-eqz v12, :cond_1

    .line 112
    invoke-virtual {v12}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementList()Ljava/util/List;

    move-result-object v0

    .line 113
    .local v0, "elemList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-eqz v1, :cond_1

    .line 115
    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/app/video/editor/external/Element;

    .line 116
    .local v2, "elem":Lcom/samsung/app/video/editor/external/Element;
    iget-object v1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mFrameThread:Lcom/sec/android/app/ve/preview/PreviewFrameThread;

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    sget v6, Lcom/sec/android/app/ve/common/ConfigUtils;->PREVIEW_WIDTH:I

    .line 117
    sget v7, Lcom/sec/android/app/ve/common/ConfigUtils;->PREVIEW_HEIGHT:I

    const/4 v9, 0x0

    invoke-virtual {v12}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getTextEleList()Ljava/util/List;

    move-result-object v10

    invoke-virtual {v12}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getDrawEleList()Ljava/util/List;

    move-result-object v11

    .line 116
    invoke-virtual/range {v1 .. v11}, Lcom/sec/android/app/ve/preview/PreviewFrameThread;->addOperation(Lcom/samsung/app/video/editor/external/Element;Lcom/samsung/app/video/editor/external/Element;JIIIFLjava/util/List;Ljava/util/List;)V

    .line 120
    .end local v0    # "elemList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    .end local v2    # "elem":Lcom/samsung/app/video/editor/external/Element;
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/ve/preview/ProjectPreview2;->registerForHeadSetUnplugged()V

    goto :goto_0
.end method

.method private registerForHeadSetUnplugged()V
    .locals 3

    .prologue
    .line 95
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.media.AUDIO_BECOMING_NOISY"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 96
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    new-instance v1, Lcom/sec/android/app/ve/preview/ProjectPreview2$UnplugHeadsetReceiver;

    invoke-direct {v1, p0}, Lcom/sec/android/app/ve/preview/ProjectPreview2$UnplugHeadsetReceiver;-><init>(Lcom/sec/android/app/ve/preview/ProjectPreview2;)V

    iput-object v1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->receiver:Lcom/sec/android/app/ve/preview/ProjectPreview2$UnplugHeadsetReceiver;

    .line 97
    iget-object v1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->receiver:Lcom/sec/android/app/ve/preview/ProjectPreview2$UnplugHeadsetReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 98
    return-void
.end method

.method private releaseWakeLock()V
    .locals 1

    .prologue
    .line 381
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->wakelock:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->wakelock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 382
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->wakelock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 385
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->wakelock:Landroid/os/PowerManager$WakeLock;

    .line 386
    return-void
.end method

.method private declared-synchronized startTimer()V
    .locals 6

    .prologue
    .line 804
    monitor-enter p0

    :try_start_0
    new-instance v1, Ljava/util/Timer;

    invoke-direct {v1}, Ljava/util/Timer;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->timer:Ljava/util/Timer;

    .line 805
    iget-object v1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->timer:Ljava/util/Timer;

    new-instance v2, Lcom/sec/android/app/ve/preview/ProjectPreview2$notifyUIThread;

    invoke-direct {v2, p0}, Lcom/sec/android/app/ve/preview/ProjectPreview2$notifyUIThread;-><init>(Lcom/sec/android/app/ve/preview/ProjectPreview2;)V

    const-wide/16 v4, 0xfa0

    invoke-virtual {v1, v2, v4, v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 809
    :goto_0
    monitor-exit p0

    return-void

    .line 806
    :catch_0
    move-exception v0

    .line 807
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 804
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private terminatePreviewFrame()V
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mFrameThread:Lcom/sec/android/app/ve/preview/PreviewFrameThread;

    if-eqz v0, :cond_0

    .line 149
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mFrameThread:Lcom/sec/android/app/ve/preview/PreviewFrameThread;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/preview/PreviewFrameThread;->terminate()V

    .line 150
    :cond_0
    return-void
.end method

.method private unRegisterForHeadSetUnplugged()V
    .locals 2

    .prologue
    .line 100
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->receiver:Lcom/sec/android/app/ve/preview/ProjectPreview2$UnplugHeadsetReceiver;

    if-eqz v0, :cond_0

    .line 101
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->receiver:Lcom/sec/android/app/ve/preview/ProjectPreview2$UnplugHeadsetReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 102
    :cond_0
    return-void
.end method


# virtual methods
.method public clearNativeJobBeforeExport()Z
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 281
    const/4 v0, 0x0

    .line 283
    .local v0, "cleared":Z
    const-string v4, "PLAYER_STATE"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "play ==> NextState = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mNextState:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " CurrentState = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mCurrentState:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    iget v4, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mCurrentState:I

    if-nez v4, :cond_0

    iget-boolean v4, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->waitForPreviewMsgClear:Z

    if-nez v4, :cond_0

    iget-boolean v4, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->waitForSurfaceCreation:Z

    if-nez v4, :cond_0

    .line 288
    iget-object v4, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mFrameThread:Lcom/sec/android/app/ve/preview/PreviewFrameThread;

    if-eqz v4, :cond_2

    .line 289
    iput-boolean v3, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->waitForPreviewMsgClearBefExport:Z

    .line 290
    new-instance v4, Ljava/lang/Object;

    invoke-direct {v4}, Ljava/lang/Object;-><init>()V

    iput-object v4, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->syncToken:Ljava/lang/Object;

    .line 292
    iget-object v4, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->syncToken:Ljava/lang/Object;

    monitor-enter v4

    .line 295
    :try_start_0
    invoke-direct {p0}, Lcom/sec/android/app/ve/preview/ProjectPreview2;->startTimer()V

    .line 296
    iget-object v5, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mFrameThread:Lcom/sec/android/app/ve/preview/PreviewFrameThread;

    invoke-virtual {v5}, Lcom/sec/android/app/ve/preview/PreviewFrameThread;->clearPendingOperation()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 298
    :try_start_1
    iget-object v5, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->syncToken:Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 306
    :goto_0
    :try_start_2
    iget-boolean v5, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->waitForPreviewMsgClearBefExport:Z

    if-eqz v5, :cond_1

    move v0, v2

    .line 307
    :goto_1
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->waitForPreviewMsgClearBefExport:Z

    .line 292
    monitor-exit v4

    .line 315
    :cond_0
    :goto_2
    return v0

    .line 299
    :catch_0
    move-exception v1

    .line 301
    .local v1, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v1}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    .line 292
    .end local v1    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v2

    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2

    :cond_1
    move v0, v3

    .line 306
    goto :goto_1

    .line 311
    :cond_2
    const/4 v0, 0x1

    goto :goto_2
.end method

.method public clearPendingPreviewRequest()V
    .locals 1

    .prologue
    .line 222
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mFrameThread:Lcom/sec/android/app/ve/preview/PreviewFrameThread;

    if-eqz v0, :cond_0

    .line 223
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mFrameThread:Lcom/sec/android/app/ve/preview/PreviewFrameThread;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/preview/PreviewFrameThread;->clearPendingOperation()V

    .line 224
    :cond_0
    return-void
.end method

.method public getMaxVolume()I
    .locals 2

    .prologue
    .line 238
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mAudioManager:Landroid/media/AudioManager;

    if-eqz v0, :cond_0

    .line 239
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mAudioManager:Landroid/media/AudioManager;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v0

    .line 241
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSurfaceView()Landroid/view/SurfaceView;
    .locals 1

    .prologue
    .line 254
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mSurfaceView:Landroid/view/SurfaceView;

    return-object v0
.end method

.method public getVolume()I
    .locals 1

    .prologue
    .line 233
    const/16 v0, 0x64

    return v0
.end method

.method public isPlaying()Z
    .locals 2

    .prologue
    .line 785
    iget v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mCurrentState:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onAttachedToWindow()V
    .locals 0

    .prologue
    .line 767
    invoke-super {p0}, Lcom/sec/android/app/ve/PreviewPlayerInterface;->onAttachedToWindow()V

    .line 768
    invoke-direct {p0}, Lcom/sec/android/app/ve/preview/ProjectPreview2;->initialize()V

    .line 769
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 773
    invoke-super {p0}, Lcom/sec/android/app/ve/PreviewPlayerInterface;->onDetachedFromWindow()V

    .line 774
    invoke-direct {p0}, Lcom/sec/android/app/ve/preview/ProjectPreview2;->terminatePreviewFrame()V

    .line 775
    invoke-direct {p0}, Lcom/sec/android/app/ve/preview/ProjectPreview2;->unRegisterForHeadSetUnplugged()V

    .line 776
    return-void
.end method

.method public onFinishInflate()V
    .locals 3

    .prologue
    .line 259
    invoke-super {p0}, Lcom/sec/android/app/ve/PreviewPlayerInterface;->onFinishInflate()V

    .line 260
    invoke-virtual {p0}, Lcom/sec/android/app/ve/preview/ProjectPreview2;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "power"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PowerManager;

    iput-object v1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->pm:Landroid/os/PowerManager;

    .line 262
    invoke-virtual {p0}, Lcom/sec/android/app/ve/preview/ProjectPreview2;->removeAllViews()V

    .line 263
    invoke-virtual {p0}, Lcom/sec/android/app/ve/preview/ProjectPreview2;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 264
    .local v0, "li":Landroid/view/LayoutInflater;
    sget v1, Lcom/sec/android/app/ve/R$layout;->preview:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 266
    sget v1, Lcom/sec/android/app/ve/R$id;->VideoPreview:I

    invoke-virtual {p0, v1}, Lcom/sec/android/app/ve/preview/ProjectPreview2;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/SurfaceView;

    iput-object v1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mSurfaceView:Landroid/view/SurfaceView;

    .line 267
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    .line 268
    iget-object v1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mSurfaceView:Landroid/view/SurfaceView;

    invoke-virtual {v1}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mSurfaceCallback:Landroid/view/SurfaceHolder$Callback;

    invoke-interface {v1, v2}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 269
    iget-object v1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mContext:Landroid/content/Context;

    const-string v2, "audio"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/AudioManager;

    iput-object v1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mAudioManager:Landroid/media/AudioManager;

    .line 270
    return-void
.end method

.method public onFrameThreadCleared()V
    .locals 10

    .prologue
    const/4 v9, -0x1

    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 321
    invoke-direct {p0}, Lcom/sec/android/app/ve/preview/ProjectPreview2;->cancelTimer()V

    .line 322
    iget-boolean v5, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->waitForPreviewMsgClear:Z

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mAdapter:Lcom/sec/android/app/ve/PreviewPlayerInterface$Adapter;

    if-eqz v5, :cond_4

    .line 324
    iput-boolean v3, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->waitForPreviewMsgClear:Z

    .line 326
    iget-object v5, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mAudioManager:Landroid/media/AudioManager;

    iget-object v6, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mAudioChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 327
    const/4 v7, 0x3

    .line 326
    invoke-virtual {v5, v6, v7, v4}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    move-result v0

    .line 329
    .local v0, "lAudioFocus":I
    if-ne v0, v4, :cond_3

    .line 331
    iget-object v5, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mAdapter:Lcom/sec/android/app/ve/PreviewPlayerInterface$Adapter;

    invoke-virtual {v5}, Lcom/sec/android/app/ve/PreviewPlayerInterface$Adapter;->getCurrentTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v2

    .line 332
    .local v2, "transcodeElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    invoke-direct {p0}, Lcom/sec/android/app/ve/preview/ProjectPreview2;->acquireWakeLock()V

    .line 333
    iput v4, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mNextState:I

    .line 334
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mEngineListener:Lcom/samsung/app/video/editor/external/NativeInterface$previewPlayerStateListener;

    invoke-virtual {v5, v6}, Lcom/samsung/app/video/editor/external/NativeInterface;->setStackStateListener(Lcom/samsung/app/video/editor/external/NativeInterface$previewPlayerStateListener;)V

    .line 337
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v5

    iget-wide v6, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mPlayPos:J

    const/4 v8, 0x0

    invoke-virtual {v5, v2, v6, v7, v8}, Lcom/samsung/app/video/editor/external/NativeInterface;->startVeEngine(Lcom/samsung/app/video/editor/external/TranscodeElement;JLcom/samsung/app/video/editor/external/TranscodeElement$ExportParameters;)I

    move-result v1

    .line 338
    .local v1, "play_started":I
    const-string v5, "SANTHOSH"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "********************Player Started? "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-lez v1, :cond_0

    move v3, v4

    :cond_0
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v5, v3}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 339
    if-gtz v1, :cond_2

    .line 340
    iput v9, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mNextState:I

    .line 345
    :goto_0
    const-string v3, "PLAYER_STATE"

    const-string v5, "AFTER START_PREVIEW_PLAYER"

    invoke-static {v3, v5}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 346
    iget-object v5, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->syncToken:Ljava/lang/Object;

    monitor-enter v5

    .line 347
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->syncToken:Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/lang/Object;->notify()V

    .line 346
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 349
    iget-object v3, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mContext:Landroid/content/Context;

    invoke-static {v3, v4}, Lcom/sec/android/app/ve/common/ConfigUtils;->setLockForHDMoviePlay(Landroid/content/Context;Z)V

    .line 370
    .end local v0    # "lAudioFocus":I
    .end local v1    # "play_started":I
    .end local v2    # "transcodeElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    :cond_1
    :goto_1
    return-void

    .line 343
    .restart local v0    # "lAudioFocus":I
    .restart local v1    # "play_started":I
    .restart local v2    # "transcodeElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    :cond_2
    const/4 v3, 0x2

    iput v3, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mNextState:I

    goto :goto_0

    .line 346
    :catchall_0
    move-exception v3

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3

    .line 353
    .end local v1    # "play_started":I
    .end local v2    # "transcodeElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    :cond_3
    iput v9, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mNextState:I

    .line 354
    iget-object v4, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->syncToken:Ljava/lang/Object;

    monitor-enter v4

    .line 355
    :try_start_2
    iget-object v3, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->syncToken:Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/lang/Object;->notify()V

    .line 354
    monitor-exit v4

    goto :goto_1

    :catchall_1
    move-exception v3

    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v3

    .line 358
    .end local v0    # "lAudioFocus":I
    :cond_4
    iget-boolean v4, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->waitForPreviewMsgClear:Z

    if-eqz v4, :cond_5

    .line 359
    iput-boolean v3, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->waitForPreviewMsgClear:Z

    .line 360
    iget-object v4, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->syncToken:Ljava/lang/Object;

    monitor-enter v4

    .line 361
    :try_start_3
    iget-object v3, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->syncToken:Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/lang/Object;->notify()V

    .line 360
    monitor-exit v4

    goto :goto_1

    :catchall_2
    move-exception v3

    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    throw v3

    .line 364
    :cond_5
    iget-boolean v4, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->waitForPreviewMsgClearBefExport:Z

    if-eqz v4, :cond_1

    .line 365
    iput-boolean v3, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->waitForPreviewMsgClearBefExport:Z

    .line 366
    iget-object v4, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->syncToken:Ljava/lang/Object;

    monitor-enter v4

    .line 367
    :try_start_4
    iget-object v3, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->syncToken:Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/lang/Object;->notify()V

    .line 366
    monitor-exit v4

    goto :goto_1

    :catchall_3
    move-exception v3

    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    throw v3
.end method

.method public pause()V
    .locals 2

    .prologue
    .line 183
    iget v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mCurrentState:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 184
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/app/ve/preview/ProjectPreview2;->_stop(Z)V

    .line 185
    :cond_0
    return-void
.end method

.method public play(J)V
    .locals 3
    .param p1, "pos"    # J

    .prologue
    .line 164
    const-string v0, "PLAYER_STATE"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "play ==> NextState = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mNextState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " CurrentState = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mCurrentState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    sget-object v0, Lcom/sec/android/app/ve/VEApp;->gExport:Lcom/sec/android/app/ve/export/Export;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/export/Export;->isExportRunning()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 178
    :cond_0
    :goto_0
    return-void

    .line 168
    :cond_1
    iget v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mCurrentState:I

    if-nez v0, :cond_0

    .line 169
    iput-wide p1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mPlayPos:J

    .line 170
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mFrameThread:Lcom/sec/android/app/ve/preview/PreviewFrameThread;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mFrameThread:Lcom/sec/android/app/ve/preview/PreviewFrameThread;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/preview/PreviewFrameThread;->isSurfaceAvailable()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 171
    invoke-virtual {p0}, Lcom/sec/android/app/ve/preview/ProjectPreview2;->setEngineSurface()V

    .line 172
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->waitForSurfaceCreation:Z

    .line 173
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/ve/preview/ProjectPreview2;->_play(J)V

    goto :goto_0

    .line 175
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->waitForSurfaceCreation:Z

    goto :goto_0
.end method

.method public resume(J)V
    .locals 1
    .param p1, "pos"    # J

    .prologue
    .line 189
    sget-object v0, Lcom/sec/android/app/ve/VEApp;->gExport:Lcom/sec/android/app/ve/export/Export;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/export/Export;->isExportRunning()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 194
    :cond_0
    :goto_0
    return-void

    .line 192
    :cond_1
    iget v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mCurrentState:I

    if-nez v0, :cond_0

    .line 193
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/ve/preview/ProjectPreview2;->_play(J)V

    goto :goto_0
.end method

.method public seekTo(J)V
    .locals 3
    .param p1, "pos"    # J

    .prologue
    .line 206
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/app/video/editor/external/NativeInterface;->pause()I

    .line 207
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v0

    long-to-int v1, p1

    invoke-virtual {v0, v1}, Lcom/samsung/app/video/editor/external/NativeInterface;->seek(I)V

    .line 208
    return-void
.end method

.method public setActivity(Landroid/app/Activity;)V
    .locals 0
    .param p1, "actvt"    # Landroid/app/Activity;

    .prologue
    .line 154
    iput-object p1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mActivity:Landroid/app/Activity;

    .line 155
    return-void
.end method

.method public setAdapter(Lcom/sec/android/app/ve/PreviewPlayerInterface$Adapter;)V
    .locals 0
    .param p1, "adap"    # Lcom/sec/android/app/ve/PreviewPlayerInterface$Adapter;

    .prologue
    .line 228
    iput-object p1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mAdapter:Lcom/sec/android/app/ve/PreviewPlayerInterface$Adapter;

    .line 229
    return-void
.end method

.method public setDataSource(Landroid/net/Uri;)V
    .locals 0
    .param p1, "videoURI"    # Landroid/net/Uri;

    .prologue
    .line 834
    return-void
.end method

.method public setDataSource(Ljava/lang/String;)V
    .locals 0
    .param p1, "videoPath"    # Ljava/lang/String;

    .prologue
    .line 160
    return-void
.end method

.method public setEngineSurface()V
    .locals 3

    .prologue
    .line 790
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    if-eqz v0, :cond_0

    .line 791
    sget-object v0, Lcom/sec/android/app/ve/VEApp;->gExport:Lcom/sec/android/app/ve/export/Export;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/export/Export;->isExportRunning()Z

    move-result v0

    if-nez v0, :cond_1

    .line 792
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-virtual {v0, v1}, Lcom/samsung/app/video/editor/external/NativeInterface;->setEngineSurface(Landroid/view/SurfaceHolder;)V

    .line 799
    :cond_0
    :goto_0
    return-void

    .line 795
    :cond_1
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v0

    sget v1, Lcom/sec/android/app/ve/common/ConfigUtils;->FIXED_PREVIEW_WIDTH:I

    sget v2, Lcom/sec/android/app/ve/common/ConfigUtils;->FIXED_PREVIEW_HEIGHT:I

    invoke-virtual {v0, v1, v2}, Lcom/samsung/app/video/editor/external/NativeInterface;->setDisplayAspectRatio(II)V

    .line 796
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-virtual {v0, v1}, Lcom/samsung/app/video/editor/external/NativeInterface;->setPreviewDisplay(Landroid/view/SurfaceHolder;)V

    goto :goto_0
.end method

.method public setFixedSurfaceRect(Landroid/graphics/Rect;)V
    .locals 0
    .param p1, "surfaceRect"    # Landroid/graphics/Rect;

    .prologue
    .line 781
    return-void
.end method

.method public setUIThreadIsLoaded(Z)V
    .locals 1
    .param p1, "loaded"    # Z

    .prologue
    .line 274
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mFrameThread:Lcom/sec/android/app/ve/preview/PreviewFrameThread;

    if-eqz v0, :cond_0

    .line 275
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mFrameThread:Lcom/sec/android/app/ve/preview/PreviewFrameThread;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/ve/preview/PreviewFrameThread;->setUIThreadIsLoaded(Z)V

    .line 277
    :cond_0
    return-void
.end method

.method public setVolume(I)V
    .locals 3
    .param p1, "volume"    # I

    .prologue
    .line 246
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mAudioManager:Landroid/media/AudioManager;

    if-nez v0, :cond_1

    .line 250
    :cond_0
    :goto_0
    return-void

    .line 248
    :cond_1
    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    .line 249
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mAudioManager:Landroid/media/AudioManager;

    const/4 v1, 0x3

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/media/AudioManager;->setStreamVolume(III)V

    goto :goto_0
.end method

.method public stop()V
    .locals 3

    .prologue
    .line 198
    const-string v0, "PLAYER_STATE"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "stop ==> NextState = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mNextState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " CurrentState = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mCurrentState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    iget v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mCurrentState:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 201
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/app/ve/preview/ProjectPreview2;->_stop(Z)V

    .line 202
    :cond_0
    return-void
.end method

.method public update(Lcom/samsung/app/video/editor/external/Element;Lcom/samsung/app/video/editor/external/Element;JIFLjava/util/List;Ljava/util/List;)V
    .locals 13
    .param p1, "firstElem"    # Lcom/samsung/app/video/editor/external/Element;
    .param p2, "secondElem"    # Lcom/samsung/app/video/editor/external/Element;
    .param p3, "time"    # J
    .param p5, "prevType"    # I
    .param p6, "storyBoardTime"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/app/video/editor/external/Element;",
            "Lcom/samsung/app/video/editor/external/Element;",
            "JIF",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/ClipartParams;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/ClipartParams;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 212
    .local p7, "clipartLis":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    .local p8, "drawList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    iget v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mCurrentState:I

    if-eqz v0, :cond_1

    .line 218
    :cond_0
    :goto_0
    return-void

    .line 214
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mFrameThread:Lcom/sec/android/app/ve/preview/PreviewFrameThread;

    if-eqz v0, :cond_0

    .line 215
    iget-object v1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mFrameThread:Lcom/sec/android/app/ve/preview/PreviewFrameThread;

    .line 216
    sget v6, Lcom/sec/android/app/ve/common/ConfigUtils;->PREVIEW_WIDTH:I

    sget v7, Lcom/sec/android/app/ve/common/ConfigUtils;->PREVIEW_HEIGHT:I

    move-object v2, p1

    move-object v3, p2

    move-wide/from16 v4, p3

    move/from16 v8, p5

    move/from16 v9, p6

    move-object/from16 v10, p7

    move-object/from16 v11, p8

    .line 215
    invoke-virtual/range {v1 .. v11}, Lcom/sec/android/app/ve/preview/PreviewFrameThread;->addOperation(Lcom/samsung/app/video/editor/external/Element;Lcom/samsung/app/video/editor/external/Element;JIIIFLjava/util/List;Ljava/util/List;)V

    goto :goto_0
.end method
