.class public Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;
.super Lorg/json/JSONObject;
.source "VEJSONMediaElement.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lorg/json/JSONObject;-><init>()V

    .line 23
    return-void
.end method

.method protected constructor <init>(Lorg/json/JSONObject;)V
    .locals 9
    .param p1, "sourceElement"    # Lorg/json/JSONObject;

    .prologue
    .line 25
    invoke-direct {p0}, Lorg/json/JSONObject;-><init>()V

    .line 28
    :try_start_0
    invoke-virtual {p1}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v5

    .line 29
    .local v5, "sourceIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    if-eqz v5, :cond_1

    .line 30
    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_2

    .line 61
    .end local v5    # "sourceIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_1
    :goto_1
    return-void

    .line 31
    .restart local v5    # "sourceIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_2
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 33
    .local v4, "key":Ljava/lang/String;
    const-string v7, "EFFECTS_LIST"

    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 34
    invoke-static {p1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;->getEffectList(Lorg/json/JSONObject;)Lorg/json/JSONArray;

    move-result-object v2

    .line 35
    .local v2, "effectList":Lorg/json/JSONArray;
    if-eqz v2, :cond_0

    .line 36
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_2
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v7

    if-ge v3, v7, :cond_0

    .line 37
    new-instance v7, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaEffect;

    invoke-virtual {v2, v3}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaEffect;-><init>(Lorg/json/JSONObject;)V

    invoke-virtual {p0, v7}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;->addMediaEffect(Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaEffect;)V

    .line 36
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 41
    .end local v2    # "effectList":Lorg/json/JSONArray;
    .end local v3    # "i":I
    :cond_3
    const-string v7, "TRANSITION"

    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 42
    invoke-static {p1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;->getMediaTransition(Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object v6

    .line 43
    .local v6, "transition":Lorg/json/JSONObject;
    new-instance v7, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaTransition;

    invoke-direct {v7, v6}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaTransition;-><init>(Lorg/json/JSONObject;)V

    invoke-virtual {p0, v7}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;->setMediaTransition(Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaTransition;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 58
    .end local v4    # "key":Ljava/lang/String;
    .end local v5    # "sourceIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    .end local v6    # "transition":Lorg/json/JSONObject;
    :catch_0
    move-exception v1

    .line 59
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_1

    .line 45
    .end local v1    # "e":Lorg/json/JSONException;
    .restart local v4    # "key":Ljava/lang/String;
    .restart local v5    # "sourceIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_4
    :try_start_1
    const-string v7, "BITMAP_ANIM_ARRAY"

    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 46
    invoke-static {p1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;->getBmpAnimationList(Lorg/json/JSONObject;)Lorg/json/JSONArray;

    move-result-object v0

    .line 47
    .local v0, "bmpList":Lorg/json/JSONArray;
    if-eqz v0, :cond_0

    .line 48
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_3
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v7

    if-ge v3, v7, :cond_0

    .line 49
    new-instance v7, Lcom/sec/android/app/ve/projectcompatibility/VEJSONBitmapBlendAnimation;

    invoke-virtual {v0, v3}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONBitmapBlendAnimation;-><init>(Lorg/json/JSONObject;)V

    invoke-virtual {p0, v7}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;->addBitmapBlendAnimation(Lcom/sec/android/app/ve/projectcompatibility/VEJSONBitmapBlendAnimation;)V

    .line 48
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 54
    .end local v0    # "bmpList":Lorg/json/JSONArray;
    .end local v3    # "i":I
    :cond_5
    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {p0, v4, v7}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method private static getBmpAnimationList(Lorg/json/JSONObject;)Lorg/json/JSONArray;
    .locals 3
    .param p0, "fromElement"    # Lorg/json/JSONObject;

    .prologue
    .line 462
    const/4 v1, 0x0

    .line 464
    .local v1, "list":Lorg/json/JSONArray;
    :try_start_0
    const-string v2, "BITMAP_ANIM_ARRAY"

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 469
    :goto_0
    return-object v1

    .line 466
    :catch_0
    move-exception v0

    .line 467
    .local v0, "jException":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method private static getEffectList(Lorg/json/JSONObject;)Lorg/json/JSONArray;
    .locals 3
    .param p0, "fromElement"    # Lorg/json/JSONObject;

    .prologue
    .line 440
    const/4 v1, 0x0

    .line 442
    .local v1, "list":Lorg/json/JSONArray;
    :try_start_0
    const-string v2, "EFFECTS_LIST"

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 447
    :goto_0
    return-object v1

    .line 444
    :catch_0
    move-exception v0

    .line 445
    .local v0, "jException":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method private static getMediaTransition(Lorg/json/JSONObject;)Lorg/json/JSONObject;
    .locals 3
    .param p0, "fromElement"    # Lorg/json/JSONObject;

    .prologue
    .line 451
    const/4 v1, 0x0

    .line 453
    .local v1, "transition":Lorg/json/JSONObject;
    :try_start_0
    const-string v2, "TRANSITION"

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 458
    :goto_0
    return-object v1

    .line 455
    :catch_0
    move-exception v0

    .line 456
    .local v0, "jException":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public addBitmapBlendAnimation(Lcom/sec/android/app/ve/projectcompatibility/VEJSONBitmapBlendAnimation;)V
    .locals 5
    .param p1, "bitmapAnimation"    # Lcom/sec/android/app/ve/projectcompatibility/VEJSONBitmapBlendAnimation;

    .prologue
    .line 386
    const/4 v0, 0x0

    .line 388
    .local v0, "bmpAnimationList":Lorg/json/JSONArray;
    :try_start_0
    const-string v4, "BITMAP_ANIM_ARRAY"

    invoke-virtual {p0, v4}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 399
    :goto_0
    invoke-virtual {v0, p1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 400
    return-void

    .line 390
    :catch_0
    move-exception v3

    .line 392
    .local v3, "jException":Lorg/json/JSONException;
    :try_start_1
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 393
    .end local v0    # "bmpAnimationList":Lorg/json/JSONArray;
    .local v1, "bmpAnimationList":Lorg/json/JSONArray;
    :try_start_2
    const-string v4, "BITMAP_ANIM_ARRAY"

    invoke-virtual {p0, v4, v1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_2

    move-object v0, v1

    .end local v1    # "bmpAnimationList":Lorg/json/JSONArray;
    .restart local v0    # "bmpAnimationList":Lorg/json/JSONArray;
    goto :goto_0

    .line 395
    :catch_1
    move-exception v2

    .line 396
    .local v2, "e":Lorg/json/JSONException;
    :goto_1
    invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0

    .line 395
    .end local v0    # "bmpAnimationList":Lorg/json/JSONArray;
    .end local v2    # "e":Lorg/json/JSONException;
    .restart local v1    # "bmpAnimationList":Lorg/json/JSONArray;
    :catch_2
    move-exception v2

    move-object v0, v1

    .end local v1    # "bmpAnimationList":Lorg/json/JSONArray;
    .restart local v0    # "bmpAnimationList":Lorg/json/JSONArray;
    goto :goto_1
.end method

.method public addMediaEffect(Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaEffect;)V
    .locals 5
    .param p1, "effect"    # Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaEffect;

    .prologue
    .line 278
    const/4 v1, 0x0

    .line 280
    .local v1, "effectList":Lorg/json/JSONArray;
    :try_start_0
    const-string v4, "EFFECTS_LIST"

    invoke-virtual {p0, v4}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 291
    :goto_0
    invoke-virtual {v1, p1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 292
    return-void

    .line 282
    :catch_0
    move-exception v3

    .line 284
    .local v3, "jException":Lorg/json/JSONException;
    :try_start_1
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2}, Lorg/json/JSONArray;-><init>()V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 285
    .end local v1    # "effectList":Lorg/json/JSONArray;
    .local v2, "effectList":Lorg/json/JSONArray;
    :try_start_2
    const-string v4, "EFFECTS_LIST"

    invoke-virtual {p0, v4, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_2

    move-object v1, v2

    .end local v2    # "effectList":Lorg/json/JSONArray;
    .restart local v1    # "effectList":Lorg/json/JSONArray;
    goto :goto_0

    .line 287
    :catch_1
    move-exception v0

    .line 288
    .local v0, "e":Lorg/json/JSONException;
    :goto_1
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0

    .line 287
    .end local v0    # "e":Lorg/json/JSONException;
    .end local v1    # "effectList":Lorg/json/JSONArray;
    .restart local v2    # "effectList":Lorg/json/JSONArray;
    :catch_2
    move-exception v0

    move-object v1, v2

    .end local v2    # "effectList":Lorg/json/JSONArray;
    .restart local v1    # "effectList":Lorg/json/JSONArray;
    goto :goto_1
.end method

.method public getBitmapBlendAnimationList()Lorg/json/JSONArray;
    .locals 1

    .prologue
    .line 407
    invoke-static {p0}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;->getBmpAnimationList(Lorg/json/JSONObject;)Lorg/json/JSONArray;

    move-result-object v0

    return-object v0
.end method

.method public getEffectList()Lorg/json/JSONArray;
    .locals 1

    .prologue
    .line 299
    invoke-static {p0}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;->getEffectList(Lorg/json/JSONObject;)Lorg/json/JSONArray;

    move-result-object v0

    return-object v0
.end method

.method public getElementDuration()I
    .locals 3

    .prologue
    .line 262
    const/4 v0, 0x0

    .line 264
    .local v0, "duration":I
    :try_start_0
    const-string v2, "ELEMENT_DURATION"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;->getInt(Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 269
    :goto_0
    return v0

    .line 266
    :catch_0
    move-exception v1

    .line 267
    .local v1, "jException":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getElementEndTime()I
    .locals 3

    .prologue
    .line 203
    const/4 v0, 0x0

    .line 205
    .local v0, "endTime":I
    :try_start_0
    const-string v2, "ELEMENT_END_TIME"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;->getInt(Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 210
    :goto_0
    return v0

    .line 207
    :catch_0
    move-exception v1

    .line 208
    .local v1, "jException":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getElementStartTime()I
    .locals 3

    .prologue
    .line 172
    const/4 v1, 0x0

    .line 174
    .local v1, "startTime":I
    :try_start_0
    const-string v2, "ELEMENT_START_TIME"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;->getInt(Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 179
    :goto_0
    return v1

    .line 176
    :catch_0
    move-exception v0

    .line 177
    .local v0, "jException":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getElementSubType()I
    .locals 3

    .prologue
    .line 141
    const/4 v1, 0x0

    .line 143
    .local v1, "subType":I
    :try_start_0
    const-string v2, "ELEMENT_SUB_TYPE"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;->getInt(Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 148
    :goto_0
    return v1

    .line 145
    :catch_0
    move-exception v0

    .line 146
    .local v0, "jException":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getElementType()I
    .locals 3

    .prologue
    .line 111
    const/4 v1, 0x0

    .line 113
    .local v1, "type":I
    :try_start_0
    const-string v2, "ELEMENT_TYPE"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;->getInt(Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 118
    :goto_0
    return v1

    .line 115
    :catch_0
    move-exception v0

    .line 116
    .local v0, "jException":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getFilePath()Ljava/lang/String;
    .locals 3

    .prologue
    .line 81
    const/4 v1, 0x0

    .line 83
    .local v1, "path":Ljava/lang/String;
    :try_start_0
    const-string v2, "ELEMENT_FILE_PATH"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 88
    :goto_0
    return-object v1

    .line 85
    :catch_0
    move-exception v0

    .line 86
    .local v0, "jException":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getGroupID()I
    .locals 3

    .prologue
    .line 370
    const/4 v1, -0x1

    .line 372
    .local v1, "grpId":I
    :try_start_0
    const-string v2, "ELEMENT_GRP_ID"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;->getInt(Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 377
    :goto_0
    return v1

    .line 374
    :catch_0
    move-exception v0

    .line 375
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getMediaTransition()Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaTransition;
    .locals 1

    .prologue
    .line 321
    invoke-static {p0}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;->getMediaTransition(Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaTransition;

    return-object v0
.end method

.method public getRotationOrientation()I
    .locals 3

    .prologue
    .line 429
    const/4 v1, 0x0

    .line 431
    .local v1, "rotation":I
    :try_start_0
    const-string v2, "ELEMENT_ROTATION_ORIENTATION"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;->getInt(Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 436
    :goto_0
    return v1

    .line 433
    :catch_0
    move-exception v0

    .line 434
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getSplitPosTime()I
    .locals 3

    .prologue
    .line 233
    const/4 v1, 0x0

    .line 235
    .local v1, "splitTime":I
    :try_start_0
    const-string v2, "ELEMENT_SPLIT_TIME"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;->getInt(Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 240
    :goto_0
    return v1

    .line 237
    :catch_0
    move-exception v0

    .line 238
    .local v0, "jException":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getVolumeLevel()I
    .locals 3

    .prologue
    .line 342
    const/4 v1, 0x0

    .line 344
    .local v1, "volumeLevel":I
    :try_start_0
    const-string v2, "VOLUME_LEVEL"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;->getInt(Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 349
    :goto_0
    return v1

    .line 346
    :catch_0
    move-exception v0

    .line 347
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public setElementDuration(I)V
    .locals 2
    .param p1, "duration"    # I

    .prologue
    .line 250
    :try_start_0
    const-string v1, "ELEMENT_DURATION"

    invoke-virtual {p0, v1, p1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 255
    :goto_0
    return-void

    .line 252
    :catch_0
    move-exception v0

    .line 253
    .local v0, "jException":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public setElementEndTime(I)V
    .locals 2
    .param p1, "endTime"    # I

    .prologue
    .line 191
    :try_start_0
    const-string v1, "ELEMENT_END_TIME"

    invoke-virtual {p0, v1, p1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 196
    :goto_0
    return-void

    .line 193
    :catch_0
    move-exception v0

    .line 194
    .local v0, "jException":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public setElementStartTime(I)V
    .locals 2
    .param p1, "startTime"    # I

    .prologue
    .line 160
    :try_start_0
    const-string v1, "ELEMENT_START_TIME"

    invoke-virtual {p0, v1, p1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 165
    :goto_0
    return-void

    .line 162
    :catch_0
    move-exception v0

    .line 163
    .local v0, "jException":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public setElementSubType(I)V
    .locals 2
    .param p1, "subType"    # I

    .prologue
    .line 128
    :try_start_0
    const-string v1, "ELEMENT_SUB_TYPE"

    invoke-virtual {p0, v1, p1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 133
    :goto_0
    return-void

    .line 130
    :catch_0
    move-exception v0

    .line 131
    .local v0, "jException":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public setElementType(I)V
    .locals 2
    .param p1, "type"    # I

    .prologue
    .line 98
    :try_start_0
    const-string v1, "ELEMENT_TYPE"

    invoke-virtual {p0, v1, p1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 103
    :goto_0
    return-void

    .line 100
    :catch_0
    move-exception v0

    .line 101
    .local v0, "jException":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public setFilePath(Ljava/lang/String;)V
    .locals 2
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 69
    :try_start_0
    const-string v1, "ELEMENT_FILE_PATH"

    invoke-virtual {p0, v1, p1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 74
    :goto_0
    return-void

    .line 71
    :catch_0
    move-exception v0

    .line 72
    .local v0, "jException":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public setGroupID(I)V
    .locals 2
    .param p1, "groupID"    # I

    .prologue
    .line 358
    :try_start_0
    const-string v1, "ELEMENT_GRP_ID"

    invoke-virtual {p0, v1, p1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 363
    :goto_0
    return-void

    .line 360
    :catch_0
    move-exception v0

    .line 361
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public setMediaTransition(Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaTransition;)V
    .locals 2
    .param p1, "transition"    # Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaTransition;

    .prologue
    .line 309
    :try_start_0
    const-string v1, "TRANSITION"

    invoke-virtual {p0, v1, p1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 314
    :goto_0
    return-void

    .line 311
    :catch_0
    move-exception v0

    .line 312
    .local v0, "jException":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public setRotationOrientation(I)V
    .locals 2
    .param p1, "orientation"    # I

    .prologue
    .line 417
    :try_start_0
    const-string v1, "ELEMENT_ROTATION_ORIENTATION"

    invoke-virtual {p0, v1, p1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 422
    :goto_0
    return-void

    .line 419
    :catch_0
    move-exception v0

    .line 420
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public setSplitPosTime(I)V
    .locals 2
    .param p1, "splitTime"    # I

    .prologue
    .line 221
    :try_start_0
    const-string v1, "ELEMENT_SPLIT_TIME"

    invoke-virtual {p0, v1, p1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 226
    :goto_0
    return-void

    .line 223
    :catch_0
    move-exception v0

    .line 224
    .local v0, "jException":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public setVolumeLevel(I)V
    .locals 2
    .param p1, "volume"    # I

    .prologue
    .line 330
    :try_start_0
    const-string v1, "VOLUME_LEVEL"

    invoke-virtual {p0, v1, p1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 335
    :goto_0
    return-void

    .line 332
    :catch_0
    move-exception v0

    .line 333
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method
