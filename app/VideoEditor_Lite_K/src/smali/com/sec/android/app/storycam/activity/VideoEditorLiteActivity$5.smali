.class Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$5;
.super Ljava/lang/Object;
.source "VideoEditorLiteActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$5;->this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;

    .line 784
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 788
    iget-object v1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$5;->this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;

    # getter for: Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mActivityEventCallbacks:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->access$5(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 791
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 805
    :goto_1
    :pswitch_0
    return-void

    .line 788
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ActivityEventsCallback;

    .line 789
    .local v0, "callback":Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ActivityEventsCallback;
    invoke-interface {v0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ActivityEventsCallback;->onActivityActionBarTapped()V

    goto :goto_0

    .line 793
    .end local v0    # "callback":Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ActivityEventsCallback;
    :pswitch_1
    sget-object v1, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    sget-object v2, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v2}, Lcom/sec/android/app/storycam/AppDataManager;->getOriginalTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/storycam/AppDataManager;->are2UHDFilesInTransElement(Lcom/samsung/app/video/editor/external/TranscodeElement;)Z

    move-result v1

    if-nez v1, :cond_1

    sget-object v1, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v1}, Lcom/sec/android/app/storycam/AppDataManager;->getOriginalTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/storycam/Utils;->isTranscodeElementValid(Lcom/samsung/app/video/editor/external/TranscodeElement;)Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v1}, Lcom/sec/android/app/storycam/AppDataManager;->getOriginalTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v1

    if-eqz v1, :cond_1

    sget-object v1, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v1}, Lcom/sec/android/app/storycam/AppDataManager;->getOriginalTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v1

    if-nez v1, :cond_2

    :cond_1
    sget-object v1, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v1}, Lcom/sec/android/app/storycam/AppDataManager;->getCurrentTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v1

    if-nez v1, :cond_2

    .line 794
    iget-object v1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$5;->this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->finish()V

    goto :goto_1

    .line 797
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$5;->this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;

    sget-object v2, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;->STATE_DEFAULT:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->changeActivityState(Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;ZZ)V

    goto :goto_1

    .line 800
    :pswitch_2
    iget-object v1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$5;->this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;

    # invokes: Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->handleBackPressed()V
    invoke-static {v1}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->access$6(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;)V

    goto :goto_1

    .line 791
    :pswitch_data_0
    .packed-switch 0x7f0d0006
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
