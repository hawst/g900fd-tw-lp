.class public Lcom/sec/android/app/ve/preview/ProjectPreview;
.super Lcom/sec/android/app/ve/PreviewPlayerInterface;
.source "ProjectPreview.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/ve/preview/ProjectPreview$UnplugHeadsetReceiver;,
        Lcom/sec/android/app/ve/preview/ProjectPreview$notifyUIThread;
    }
.end annotation


# static fields
.field private static final STATE_COMPLETED:I = 0x4

.field private static final STATE_DEINIT:I = 0x3

.field private static final STATE_ERROR:I = -0x1

.field private static final STATE_IDLE:I = 0x0

.field private static final STATE_INIT:I = 0x1

.field private static final STATE_PLAYING:I = 0x2


# instance fields
.field private isCompleted:Z

.field private mActivity:Landroid/app/Activity;

.field public mAdapter:Lcom/sec/android/app/ve/PreviewPlayerInterface$Adapter;

.field private mAudioChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field private mAudioManager:Landroid/media/AudioManager;

.field private mContext:Landroid/content/Context;

.field private mCurrentState:I

.field private mEngineListener:Lcom/samsung/app/video/editor/external/NativeInterface$previewPlayerStateListener;

.field private mFrameThread:Lcom/sec/android/app/ve/preview/PreviewFrameThread;

.field private mFrameThreadCallBack:Lcom/sec/android/app/ve/preview/PreviewFrameThread$PreviewFrameThreadAdapter;

.field private mNextState:I

.field private mPlayPos:J

.field private mSurfaceCallback:Landroid/view/SurfaceHolder$Callback;

.field private mSurfaceHolder:Landroid/view/SurfaceHolder;

.field private mSurfaceView:Landroid/view/SurfaceView;

.field private pm:Landroid/os/PowerManager;

.field private receiver:Lcom/sec/android/app/ve/preview/ProjectPreview$UnplugHeadsetReceiver;

.field private syncToken:Ljava/lang/Object;

.field private timer:Ljava/util/Timer;

.field private waitForPreviewMsgClear:Z

.field private waitForSurfaceCreation:Z

.field private wakelock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 81
    invoke-direct {p0, p1}, Lcom/sec/android/app/ve/PreviewPlayerInterface;-><init>(Landroid/content/Context;)V

    .line 53
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    .line 58
    iput v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mCurrentState:I

    .line 59
    iput v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mNextState:I

    .line 60
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mPlayPos:J

    .line 64
    iput-boolean v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->waitForPreviewMsgClear:Z

    .line 65
    iput-boolean v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->waitForSurfaceCreation:Z

    .line 67
    iput-boolean v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->isCompleted:Z

    .line 124
    new-instance v0, Lcom/sec/android/app/ve/preview/ProjectPreview$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/ve/preview/ProjectPreview$1;-><init>(Lcom/sec/android/app/ve/preview/ProjectPreview;)V

    iput-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mFrameThreadCallBack:Lcom/sec/android/app/ve/preview/PreviewFrameThread$PreviewFrameThreadAdapter;

    .line 507
    new-instance v0, Lcom/sec/android/app/ve/preview/ProjectPreview$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/ve/preview/ProjectPreview$2;-><init>(Lcom/sec/android/app/ve/preview/ProjectPreview;)V

    iput-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mSurfaceCallback:Landroid/view/SurfaceHolder$Callback;

    .line 575
    new-instance v0, Lcom/sec/android/app/ve/preview/ProjectPreview$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/ve/preview/ProjectPreview$3;-><init>(Lcom/sec/android/app/ve/preview/ProjectPreview;)V

    iput-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mEngineListener:Lcom/samsung/app/video/editor/external/NativeInterface$previewPlayerStateListener;

    .line 675
    new-instance v0, Lcom/sec/android/app/ve/preview/ProjectPreview$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/ve/preview/ProjectPreview$4;-><init>(Lcom/sec/android/app/ve/preview/ProjectPreview;)V

    iput-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mAudioChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 82
    iput-object p1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mContext:Landroid/content/Context;

    .line 83
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, 0x0

    .line 76
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/ve/PreviewPlayerInterface;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 53
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    .line 58
    iput v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mCurrentState:I

    .line 59
    iput v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mNextState:I

    .line 60
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mPlayPos:J

    .line 64
    iput-boolean v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->waitForPreviewMsgClear:Z

    .line 65
    iput-boolean v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->waitForSurfaceCreation:Z

    .line 67
    iput-boolean v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->isCompleted:Z

    .line 124
    new-instance v0, Lcom/sec/android/app/ve/preview/ProjectPreview$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/ve/preview/ProjectPreview$1;-><init>(Lcom/sec/android/app/ve/preview/ProjectPreview;)V

    iput-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mFrameThreadCallBack:Lcom/sec/android/app/ve/preview/PreviewFrameThread$PreviewFrameThreadAdapter;

    .line 507
    new-instance v0, Lcom/sec/android/app/ve/preview/ProjectPreview$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/ve/preview/ProjectPreview$2;-><init>(Lcom/sec/android/app/ve/preview/ProjectPreview;)V

    iput-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mSurfaceCallback:Landroid/view/SurfaceHolder$Callback;

    .line 575
    new-instance v0, Lcom/sec/android/app/ve/preview/ProjectPreview$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/ve/preview/ProjectPreview$3;-><init>(Lcom/sec/android/app/ve/preview/ProjectPreview;)V

    iput-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mEngineListener:Lcom/samsung/app/video/editor/external/NativeInterface$previewPlayerStateListener;

    .line 675
    new-instance v0, Lcom/sec/android/app/ve/preview/ProjectPreview$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/ve/preview/ProjectPreview$4;-><init>(Lcom/sec/android/app/ve/preview/ProjectPreview;)V

    iput-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mAudioChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 77
    iput-object p1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mContext:Landroid/content/Context;

    .line 78
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v2, 0x0

    .line 71
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/ve/PreviewPlayerInterface;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 53
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    .line 58
    iput v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mCurrentState:I

    .line 59
    iput v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mNextState:I

    .line 60
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mPlayPos:J

    .line 64
    iput-boolean v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->waitForPreviewMsgClear:Z

    .line 65
    iput-boolean v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->waitForSurfaceCreation:Z

    .line 67
    iput-boolean v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->isCompleted:Z

    .line 124
    new-instance v0, Lcom/sec/android/app/ve/preview/ProjectPreview$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/ve/preview/ProjectPreview$1;-><init>(Lcom/sec/android/app/ve/preview/ProjectPreview;)V

    iput-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mFrameThreadCallBack:Lcom/sec/android/app/ve/preview/PreviewFrameThread$PreviewFrameThreadAdapter;

    .line 507
    new-instance v0, Lcom/sec/android/app/ve/preview/ProjectPreview$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/ve/preview/ProjectPreview$2;-><init>(Lcom/sec/android/app/ve/preview/ProjectPreview;)V

    iput-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mSurfaceCallback:Landroid/view/SurfaceHolder$Callback;

    .line 575
    new-instance v0, Lcom/sec/android/app/ve/preview/ProjectPreview$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/ve/preview/ProjectPreview$3;-><init>(Lcom/sec/android/app/ve/preview/ProjectPreview;)V

    iput-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mEngineListener:Lcom/samsung/app/video/editor/external/NativeInterface$previewPlayerStateListener;

    .line 675
    new-instance v0, Lcom/sec/android/app/ve/preview/ProjectPreview$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/ve/preview/ProjectPreview$4;-><init>(Lcom/sec/android/app/ve/preview/ProjectPreview;)V

    iput-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mAudioChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 72
    iput-object p1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mContext:Landroid/content/Context;

    .line 73
    return-void
.end method

.method private _play(J)V
    .locals 6
    .param p1, "pos"    # J

    .prologue
    .line 332
    iget v1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mCurrentState:I

    if-nez v1, :cond_1

    sget-boolean v1, Lcom/samsung/app/video/editor/external/NativeInterface;->isInitFIMC:Z

    if-eqz v1, :cond_1

    .line 334
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->syncToken:Ljava/lang/Object;

    .line 335
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->waitForPreviewMsgClear:Z

    .line 359
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->syncToken:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 360
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mFrameThread:Lcom/sec/android/app/ve/preview/PreviewFrameThread;

    if-eqz v1, :cond_0

    .line 361
    iget-object v1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mFrameThread:Lcom/sec/android/app/ve/preview/PreviewFrameThread;

    invoke-virtual {v1}, Lcom/sec/android/app/ve/preview/PreviewFrameThread;->clearPendingOperation()V

    .line 362
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->syncToken:Ljava/lang/Object;

    const-wide/16 v4, 0xfa0

    invoke-virtual {v1, v4, v5}, Ljava/lang/Object;->wait(J)V

    .line 363
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->waitForPreviewMsgClear:Z

    .line 359
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 366
    :try_start_2
    invoke-direct {p0}, Lcom/sec/android/app/ve/preview/ProjectPreview;->handlePlayEventCompletion()V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    .line 372
    :cond_1
    :goto_0
    return-void

    .line 359
    :catchall_0
    move-exception v1

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v1
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0

    .line 368
    :catch_0
    move-exception v0

    .line 369
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method

.method private _stop(Z)V
    .locals 4
    .param p1, "force"    # Z

    .prologue
    .line 432
    const-string v1, "PLAYER_STATE"

    const-string v2, "_stop"

    invoke-static {v1, v2}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 434
    iget-object v1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mContext:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/sec/android/app/ve/common/ConfigUtils;->setLockForHDMoviePlay(Landroid/content/Context;Z)V

    .line 436
    invoke-direct {p0}, Lcom/sec/android/app/ve/preview/ProjectPreview;->releaseWakeLock()V

    .line 438
    if-nez p1, :cond_0

    const/4 v1, 0x2

    iget v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mCurrentState:I

    if-ne v1, v2, :cond_1

    .line 441
    :cond_0
    :try_start_0
    const-string v1, "PLAYER_STATE"

    const-string v2, "calling native _stop"

    invoke-static {v1, v2}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 442
    iget-object v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->syncToken:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 443
    :try_start_1
    const-string v1, "PLAYER_STATE"

    const-string v3, "calling wait"

    invoke-static {v1, v3}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 444
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/NativeInterface;->stop()V

    .line 445
    iget-object v1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->syncToken:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V

    .line 446
    const-string v1, "PLAYER_STATE"

    const-string v3, "calling wait done"

    invoke-static {v1, v3}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 442
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 449
    :try_start_2
    invoke-direct {p0}, Lcom/sec/android/app/ve/preview/ProjectPreview;->handleStopEventCompletion()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 455
    :cond_1
    :goto_0
    return-void

    .line 442
    :catchall_0
    move-exception v1

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v1
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    .line 451
    :catch_0
    move-exception v0

    .line 452
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic access$0(Lcom/sec/android/app/ve/preview/ProjectPreview;Z)V
    .locals 0

    .prologue
    .line 39
    iput-boolean p1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->isSurfaceAvailable:Z

    return-void
.end method

.method static synthetic access$1(Lcom/sec/android/app/ve/preview/ProjectPreview;)Lcom/sec/android/app/ve/preview/PreviewFrameThread;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mFrameThread:Lcom/sec/android/app/ve/preview/PreviewFrameThread;

    return-object v0
.end method

.method static synthetic access$10(Lcom/sec/android/app/ve/preview/ProjectPreview;)I
    .locals 1

    .prologue
    .line 58
    iget v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mCurrentState:I

    return v0
.end method

.method static synthetic access$11(Lcom/sec/android/app/ve/preview/ProjectPreview;I)V
    .locals 0

    .prologue
    .line 59
    iput p1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mNextState:I

    return-void
.end method

.method static synthetic access$12(Lcom/sec/android/app/ve/preview/ProjectPreview;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->syncToken:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$13(Lcom/sec/android/app/ve/preview/ProjectPreview;)V
    .locals 0

    .prologue
    .line 375
    invoke-direct {p0}, Lcom/sec/android/app/ve/preview/ProjectPreview;->handlePlayTimeError()V

    return-void
.end method

.method static synthetic access$14(Lcom/sec/android/app/ve/preview/ProjectPreview;I)V
    .locals 0

    .prologue
    .line 58
    iput p1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mCurrentState:I

    return-void
.end method

.method static synthetic access$15(Lcom/sec/android/app/ve/preview/ProjectPreview;Z)V
    .locals 0

    .prologue
    .line 67
    iput-boolean p1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->isCompleted:Z

    return-void
.end method

.method static synthetic access$16(Lcom/sec/android/app/ve/preview/ProjectPreview;)V
    .locals 0

    .prologue
    .line 457
    invoke-direct {p0}, Lcom/sec/android/app/ve/preview/ProjectPreview;->handleCompletedEvent()V

    return-void
.end method

.method static synthetic access$17(Lcom/sec/android/app/ve/preview/ProjectPreview;Z)V
    .locals 0

    .prologue
    .line 64
    iput-boolean p1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->waitForPreviewMsgClear:Z

    return-void
.end method

.method static synthetic access$18(Lcom/sec/android/app/ve/preview/ProjectPreview;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$19(Lcom/sec/android/app/ve/preview/ProjectPreview;)Landroid/media/AudioManager;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mAudioManager:Landroid/media/AudioManager;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/app/ve/preview/ProjectPreview;Z)V
    .locals 0

    .prologue
    .line 430
    invoke-direct {p0, p1}, Lcom/sec/android/app/ve/preview/ProjectPreview;->_stop(Z)V

    return-void
.end method

.method static synthetic access$20(Lcom/sec/android/app/ve/preview/ProjectPreview;)Landroid/media/AudioManager$OnAudioFocusChangeListener;
    .locals 1

    .prologue
    .line 675
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mAudioChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    return-object v0
.end method

.method static synthetic access$3(Lcom/sec/android/app/ve/preview/ProjectPreview;Landroid/view/SurfaceHolder;)V
    .locals 0

    .prologue
    .line 53
    iput-object p1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    return-void
.end method

.method static synthetic access$4(Lcom/sec/android/app/ve/preview/ProjectPreview;)Landroid/view/SurfaceHolder;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    return-object v0
.end method

.method static synthetic access$5(Lcom/sec/android/app/ve/preview/ProjectPreview;)Z
    .locals 1

    .prologue
    .line 65
    iget-boolean v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->waitForSurfaceCreation:Z

    return v0
.end method

.method static synthetic access$6(Lcom/sec/android/app/ve/preview/ProjectPreview;Z)V
    .locals 0

    .prologue
    .line 65
    iput-boolean p1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->waitForSurfaceCreation:Z

    return-void
.end method

.method static synthetic access$7(Lcom/sec/android/app/ve/preview/ProjectPreview;)J
    .locals 2

    .prologue
    .line 60
    iget-wide v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mPlayPos:J

    return-wide v0
.end method

.method static synthetic access$8(Lcom/sec/android/app/ve/preview/ProjectPreview;J)V
    .locals 1

    .prologue
    .line 327
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/ve/preview/ProjectPreview;->_play(J)V

    return-void
.end method

.method static synthetic access$9(Lcom/sec/android/app/ve/preview/ProjectPreview;)I
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mNextState:I

    return v0
.end method

.method private acquireWakeLock()V
    .locals 3

    .prologue
    .line 312
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->wakelock:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->wakelock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-nez v0, :cond_1

    .line 313
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->pm:Landroid/os/PowerManager;

    const v1, 0x2000000a

    const-string v2, "VideoEditor - Player"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->wakelock:Landroid/os/PowerManager$WakeLock;

    .line 314
    const-string v0, "PLAYER_STATE"

    const-string v1, "acquiring"

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 315
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->wakelock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 317
    :cond_1
    return-void
.end method

.method private cancelTimer()V
    .locals 1

    .prologue
    .line 728
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->timer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 729
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->timer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 730
    :cond_0
    return-void
.end method

.method private handleCompletedEvent()V
    .locals 1

    .prologue
    .line 458
    new-instance v0, Lcom/sec/android/app/ve/preview/ProjectPreview$9;

    invoke-direct {v0, p0}, Lcom/sec/android/app/ve/preview/ProjectPreview$9;-><init>(Lcom/sec/android/app/ve/preview/ProjectPreview;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/ve/preview/ProjectPreview;->post(Ljava/lang/Runnable;)Z

    .line 464
    return-void
.end method

.method private handlePlayEventCompletion()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 388
    const-string v0, "PLAYER_STATE"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "handlePlayEventCompletion ==> NextState = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mNextState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " CurrentState = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mCurrentState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 389
    iget v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mCurrentState:I

    if-nez v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mNextState:I

    if-nez v0, :cond_0

    .line 391
    const-string v0, "PLAYER_STATE"

    const-string v1, "handlePlayEventCompletion ==> The error happend on App side before start of playback"

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 428
    :goto_0
    return-void

    .line 394
    :cond_0
    iget v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mCurrentState:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    iget v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mNextState:I

    if-eq v0, v3, :cond_2

    .line 396
    new-instance v0, Lcom/sec/android/app/ve/preview/ProjectPreview$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/ve/preview/ProjectPreview$6;-><init>(Lcom/sec/android/app/ve/preview/ProjectPreview;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/ve/preview/ProjectPreview;->post(Ljava/lang/Runnable;)Z

    .line 427
    :cond_1
    :goto_1
    const-string v0, "PLAYER_STATE"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "handlePlayEventCompletion ==> NextState = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mNextState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " CurrentState = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mCurrentState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 404
    :cond_2
    iget v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mNextState:I

    if-ne v0, v3, :cond_3

    .line 406
    iput v3, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mCurrentState:I

    .line 407
    new-instance v0, Lcom/sec/android/app/ve/preview/ProjectPreview$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/ve/preview/ProjectPreview$7;-><init>(Lcom/sec/android/app/ve/preview/ProjectPreview;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/ve/preview/ProjectPreview;->post(Ljava/lang/Runnable;)Z

    goto :goto_1

    .line 416
    :cond_3
    iget v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mCurrentState:I

    if-nez v0, :cond_1

    iget v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mNextState:I

    if-eq v0, v3, :cond_1

    .line 418
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mCurrentState:I

    .line 419
    new-instance v0, Lcom/sec/android/app/ve/preview/ProjectPreview$8;

    invoke-direct {v0, p0}, Lcom/sec/android/app/ve/preview/ProjectPreview$8;-><init>(Lcom/sec/android/app/ve/preview/ProjectPreview;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/ve/preview/ProjectPreview;->post(Ljava/lang/Runnable;)Z

    goto :goto_1
.end method

.method private handlePlayTimeError()V
    .locals 1

    .prologue
    .line 376
    new-instance v0, Lcom/sec/android/app/ve/preview/ProjectPreview$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/ve/preview/ProjectPreview$5;-><init>(Lcom/sec/android/app/ve/preview/ProjectPreview;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/ve/preview/ProjectPreview;->post(Ljava/lang/Runnable;)Z

    .line 385
    return-void
.end method

.method private handleStopEventCompletion()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 467
    const-string v0, "PLAYER_STATE"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "handleStopEventCompletion ==> NextState = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mNextState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " CurrentState = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mCurrentState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 468
    const-string v0, "PLAYER_STATE"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "handleStopEventCompletion ==> isCompleted = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->isCompleted:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 469
    iget v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mNextState:I

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->isCompleted:Z

    if-eqz v0, :cond_0

    .line 471
    iput-boolean v4, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->isCompleted:Z

    .line 472
    new-instance v0, Lcom/sec/android/app/ve/preview/ProjectPreview$10;

    invoke-direct {v0, p0}, Lcom/sec/android/app/ve/preview/ProjectPreview$10;-><init>(Lcom/sec/android/app/ve/preview/ProjectPreview;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/ve/preview/ProjectPreview;->post(Ljava/lang/Runnable;)Z

    .line 494
    :goto_0
    new-instance v0, Lcom/sec/android/app/ve/preview/ProjectPreview$12;

    invoke-direct {v0, p0}, Lcom/sec/android/app/ve/preview/ProjectPreview$12;-><init>(Lcom/sec/android/app/ve/preview/ProjectPreview;)V

    .line 500
    const-wide/16 v2, 0xa

    .line 494
    invoke-virtual {p0, v0, v2, v3}, Lcom/sec/android/app/ve/preview/ProjectPreview;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 502
    iput v4, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mCurrentState:I

    iput v4, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mNextState:I

    .line 503
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/app/video/editor/external/NativeInterface;->setStackStateListener(Lcom/samsung/app/video/editor/external/NativeInterface$previewPlayerStateListener;)V

    .line 504
    const-string v0, "PLAYER_STATE"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "handleStopEventCompletion ==> NextState = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mNextState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " CurrentState = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mCurrentState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 505
    return-void

    .line 483
    :cond_0
    new-instance v0, Lcom/sec/android/app/ve/preview/ProjectPreview$11;

    invoke-direct {v0, p0}, Lcom/sec/android/app/ve/preview/ProjectPreview$11;-><init>(Lcom/sec/android/app/ve/preview/ProjectPreview;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/ve/preview/ProjectPreview;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method private initialize()V
    .locals 13

    .prologue
    const/4 v8, 0x0

    .line 105
    new-instance v1, Lcom/sec/android/app/ve/preview/PreviewFrameThread;

    invoke-direct {v1}, Lcom/sec/android/app/ve/preview/PreviewFrameThread;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mFrameThreadCallBack:Lcom/sec/android/app/ve/preview/PreviewFrameThread$PreviewFrameThreadAdapter;

    invoke-virtual {v1, v3}, Lcom/sec/android/app/ve/preview/PreviewFrameThread;->init(Lcom/sec/android/app/ve/preview/PreviewFrameThread$PreviewFrameThreadAdapter;)Lcom/sec/android/app/ve/preview/PreviewFrameThread;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mFrameThread:Lcom/sec/android/app/ve/preview/PreviewFrameThread;

    .line 107
    iget-object v1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mAdapter:Lcom/sec/android/app/ve/PreviewPlayerInterface$Adapter;

    if-nez v1, :cond_0

    .line 122
    :goto_0
    return-void

    .line 110
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mAdapter:Lcom/sec/android/app/ve/PreviewPlayerInterface$Adapter;

    invoke-virtual {v1}, Lcom/sec/android/app/ve/PreviewPlayerInterface$Adapter;->getCurrentTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v12

    .line 111
    .local v12, "trans":Lcom/samsung/app/video/editor/external/TranscodeElement;
    if-eqz v12, :cond_1

    .line 113
    invoke-virtual {v12}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementList()Ljava/util/List;

    move-result-object v0

    .line 114
    .local v0, "elemList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-eqz v1, :cond_1

    .line 116
    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/app/video/editor/external/Element;

    .line 117
    .local v2, "elem":Lcom/samsung/app/video/editor/external/Element;
    iget-object v1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mFrameThread:Lcom/sec/android/app/ve/preview/PreviewFrameThread;

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    sget v6, Lcom/sec/android/app/ve/common/ConfigUtils;->PREVIEW_WIDTH:I

    .line 118
    sget v7, Lcom/sec/android/app/ve/common/ConfigUtils;->PREVIEW_HEIGHT:I

    const/4 v9, 0x0

    invoke-virtual {v12}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getTextEleList()Ljava/util/List;

    move-result-object v10

    invoke-virtual {v12}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getDrawEleList()Ljava/util/List;

    move-result-object v11

    .line 117
    invoke-virtual/range {v1 .. v11}, Lcom/sec/android/app/ve/preview/PreviewFrameThread;->addOperation(Lcom/samsung/app/video/editor/external/Element;Lcom/samsung/app/video/editor/external/Element;JIIIFLjava/util/List;Ljava/util/List;)V

    .line 121
    .end local v0    # "elemList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    .end local v2    # "elem":Lcom/samsung/app/video/editor/external/Element;
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/ve/preview/ProjectPreview;->registerForHeadSetUnplugged()V

    goto :goto_0
.end method

.method private registerForHeadSetUnplugged()V
    .locals 3

    .prologue
    .line 96
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.media.AUDIO_BECOMING_NOISY"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 97
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    new-instance v1, Lcom/sec/android/app/ve/preview/ProjectPreview$UnplugHeadsetReceiver;

    invoke-direct {v1, p0}, Lcom/sec/android/app/ve/preview/ProjectPreview$UnplugHeadsetReceiver;-><init>(Lcom/sec/android/app/ve/preview/ProjectPreview;)V

    iput-object v1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->receiver:Lcom/sec/android/app/ve/preview/ProjectPreview$UnplugHeadsetReceiver;

    .line 98
    iget-object v1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->receiver:Lcom/sec/android/app/ve/preview/ProjectPreview$UnplugHeadsetReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 99
    return-void
.end method

.method private releaseWakeLock()V
    .locals 1

    .prologue
    .line 320
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->wakelock:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->wakelock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 321
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->wakelock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 324
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->wakelock:Landroid/os/PowerManager$WakeLock;

    .line 325
    return-void
.end method

.method private declared-synchronized startTimer()V
    .locals 6

    .prologue
    .line 720
    monitor-enter p0

    :try_start_0
    new-instance v1, Ljava/util/Timer;

    invoke-direct {v1}, Ljava/util/Timer;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->timer:Ljava/util/Timer;

    .line 721
    iget-object v1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->timer:Ljava/util/Timer;

    new-instance v2, Lcom/sec/android/app/ve/preview/ProjectPreview$notifyUIThread;

    invoke-direct {v2, p0}, Lcom/sec/android/app/ve/preview/ProjectPreview$notifyUIThread;-><init>(Lcom/sec/android/app/ve/preview/ProjectPreview;)V

    const-wide/16 v4, 0xfa0

    invoke-virtual {v1, v2, v4, v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 725
    :goto_0
    monitor-exit p0

    return-void

    .line 722
    :catch_0
    move-exception v0

    .line 723
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 720
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private terminatePreviewFrame()V
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mFrameThread:Lcom/sec/android/app/ve/preview/PreviewFrameThread;

    if-eqz v0, :cond_0

    .line 150
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mFrameThread:Lcom/sec/android/app/ve/preview/PreviewFrameThread;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/preview/PreviewFrameThread;->terminate()V

    .line 151
    :cond_0
    return-void
.end method

.method private unRegisterForHeadSetUnplugged()V
    .locals 2

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->receiver:Lcom/sec/android/app/ve/preview/ProjectPreview$UnplugHeadsetReceiver;

    if-eqz v0, :cond_0

    .line 102
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->receiver:Lcom/sec/android/app/ve/preview/ProjectPreview$UnplugHeadsetReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 103
    :cond_0
    return-void
.end method


# virtual methods
.method public clearPendingPreviewRequest()V
    .locals 1

    .prologue
    .line 223
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mFrameThread:Lcom/sec/android/app/ve/preview/PreviewFrameThread;

    if-eqz v0, :cond_0

    .line 224
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mFrameThread:Lcom/sec/android/app/ve/preview/PreviewFrameThread;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/preview/PreviewFrameThread;->clearPendingOperation()V

    .line 225
    :cond_0
    return-void
.end method

.method public getMaxVolume()I
    .locals 2

    .prologue
    .line 239
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mAudioManager:Landroid/media/AudioManager;

    if-eqz v0, :cond_0

    .line 240
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mAudioManager:Landroid/media/AudioManager;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v0

    .line 242
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSurfaceView()Landroid/view/SurfaceView;
    .locals 1

    .prologue
    .line 255
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mSurfaceView:Landroid/view/SurfaceView;

    return-object v0
.end method

.method public getVolume()I
    .locals 1

    .prologue
    .line 234
    const/16 v0, 0x64

    return v0
.end method

.method public isPlaying()Z
    .locals 2

    .prologue
    .line 708
    iget v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mCurrentState:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onAttachedToWindow()V
    .locals 0

    .prologue
    .line 690
    invoke-super {p0}, Lcom/sec/android/app/ve/PreviewPlayerInterface;->onAttachedToWindow()V

    .line 691
    invoke-direct {p0}, Lcom/sec/android/app/ve/preview/ProjectPreview;->initialize()V

    .line 692
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 696
    invoke-super {p0}, Lcom/sec/android/app/ve/PreviewPlayerInterface;->onDetachedFromWindow()V

    .line 697
    invoke-direct {p0}, Lcom/sec/android/app/ve/preview/ProjectPreview;->terminatePreviewFrame()V

    .line 698
    invoke-direct {p0}, Lcom/sec/android/app/ve/preview/ProjectPreview;->unRegisterForHeadSetUnplugged()V

    .line 699
    return-void
.end method

.method public onFinishInflate()V
    .locals 3

    .prologue
    .line 260
    invoke-super {p0}, Lcom/sec/android/app/ve/PreviewPlayerInterface;->onFinishInflate()V

    .line 261
    invoke-virtual {p0}, Lcom/sec/android/app/ve/preview/ProjectPreview;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "power"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PowerManager;

    iput-object v1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->pm:Landroid/os/PowerManager;

    .line 263
    invoke-virtual {p0}, Lcom/sec/android/app/ve/preview/ProjectPreview;->removeAllViews()V

    .line 264
    invoke-virtual {p0}, Lcom/sec/android/app/ve/preview/ProjectPreview;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 265
    .local v0, "li":Landroid/view/LayoutInflater;
    sget v1, Lcom/sec/android/app/ve/R$layout;->preview:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 267
    sget v1, Lcom/sec/android/app/ve/R$id;->VideoPreview:I

    invoke-virtual {p0, v1}, Lcom/sec/android/app/ve/preview/ProjectPreview;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/SurfaceView;

    iput-object v1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mSurfaceView:Landroid/view/SurfaceView;

    .line 268
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    .line 269
    iget-object v1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mSurfaceView:Landroid/view/SurfaceView;

    invoke-virtual {v1}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mSurfaceCallback:Landroid/view/SurfaceHolder$Callback;

    invoke-interface {v1, v2}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 270
    iget-object v1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mContext:Landroid/content/Context;

    const-string v2, "audio"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/AudioManager;

    iput-object v1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mAudioManager:Landroid/media/AudioManager;

    .line 271
    return-void
.end method

.method public onFrameThreadCleared()V
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/4 v5, 0x1

    .line 283
    iget-boolean v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->waitForPreviewMsgClear:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mAdapter:Lcom/sec/android/app/ve/PreviewPlayerInterface$Adapter;

    if-eqz v2, :cond_2

    .line 285
    iput-boolean v3, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->waitForPreviewMsgClear:Z

    .line 287
    iget-object v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mAudioManager:Landroid/media/AudioManager;

    iget-object v3, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mAudioChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 288
    const/4 v4, 0x3

    .line 287
    invoke-virtual {v2, v3, v4, v5}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    move-result v0

    .line 290
    .local v0, "lAudioFocus":I
    if-ne v0, v5, :cond_1

    .line 292
    iget-object v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mAdapter:Lcom/sec/android/app/ve/PreviewPlayerInterface$Adapter;

    invoke-virtual {v2}, Lcom/sec/android/app/ve/PreviewPlayerInterface$Adapter;->getCurrentTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v1

    .line 293
    .local v1, "transcodeElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    invoke-direct {p0}, Lcom/sec/android/app/ve/preview/ProjectPreview;->acquireWakeLock()V

    .line 294
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mEngineListener:Lcom/samsung/app/video/editor/external/NativeInterface$previewPlayerStateListener;

    invoke-virtual {v2, v3}, Lcom/samsung/app/video/editor/external/NativeInterface;->setStackStateListener(Lcom/samsung/app/video/editor/external/NativeInterface$previewPlayerStateListener;)V

    .line 295
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/samsung/app/video/editor/external/NativeInterface;->setTranscodeElementForEngine(Lcom/samsung/app/video/editor/external/TranscodeElement;)V

    .line 296
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/samsung/app/video/editor/external/NativeInterface;->createMediaPlayer(Lcom/samsung/app/video/editor/external/TranscodeElement;)V

    .line 297
    iget-object v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mContext:Landroid/content/Context;

    invoke-static {v2, v5}, Lcom/sec/android/app/ve/common/ConfigUtils;->setLockForHDMoviePlay(Landroid/content/Context;Z)V

    .line 309
    .end local v0    # "lAudioFocus":I
    .end local v1    # "transcodeElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    :cond_0
    :goto_0
    return-void

    .line 299
    .restart local v0    # "lAudioFocus":I
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->syncToken:Ljava/lang/Object;

    monitor-enter v3

    .line 300
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->syncToken:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->notify()V

    .line 299
    monitor-exit v3

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 303
    .end local v0    # "lAudioFocus":I
    :cond_2
    iget-boolean v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->waitForPreviewMsgClear:Z

    if-eqz v2, :cond_0

    .line 304
    iput-boolean v3, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->waitForPreviewMsgClear:Z

    .line 305
    iget-object v3, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->syncToken:Ljava/lang/Object;

    monitor-enter v3

    .line 306
    :try_start_1
    iget-object v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->syncToken:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->notify()V

    .line 305
    monitor-exit v3

    goto :goto_0

    :catchall_1
    move-exception v2

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw v2
.end method

.method public pause()V
    .locals 2

    .prologue
    .line 184
    iget v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mCurrentState:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 185
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/app/ve/preview/ProjectPreview;->_stop(Z)V

    .line 186
    :cond_0
    return-void
.end method

.method public play(J)V
    .locals 3
    .param p1, "pos"    # J

    .prologue
    .line 165
    const-string v0, "PLAYER_STATE"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "play ==> NextState = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mNextState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " CurrentState = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mCurrentState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    sget-object v0, Lcom/sec/android/app/ve/VEApp;->gExport:Lcom/sec/android/app/ve/export/Export;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/export/Export;->isExportRunning()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 179
    :cond_0
    :goto_0
    return-void

    .line 169
    :cond_1
    iget v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mCurrentState:I

    if-nez v0, :cond_0

    .line 170
    iput-wide p1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mPlayPos:J

    .line 171
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mFrameThread:Lcom/sec/android/app/ve/preview/PreviewFrameThread;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mFrameThread:Lcom/sec/android/app/ve/preview/PreviewFrameThread;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/preview/PreviewFrameThread;->isSurfaceAvailable()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 172
    invoke-virtual {p0}, Lcom/sec/android/app/ve/preview/ProjectPreview;->setEngineSurface()V

    .line 173
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->waitForSurfaceCreation:Z

    .line 174
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/ve/preview/ProjectPreview;->_play(J)V

    goto :goto_0

    .line 176
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->waitForSurfaceCreation:Z

    goto :goto_0
.end method

.method public resume(J)V
    .locals 1
    .param p1, "pos"    # J

    .prologue
    .line 190
    sget-object v0, Lcom/sec/android/app/ve/VEApp;->gExport:Lcom/sec/android/app/ve/export/Export;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/export/Export;->isExportRunning()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 195
    :cond_0
    :goto_0
    return-void

    .line 193
    :cond_1
    iget v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mCurrentState:I

    if-nez v0, :cond_0

    .line 194
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/ve/preview/ProjectPreview;->_play(J)V

    goto :goto_0
.end method

.method public seekTo(J)V
    .locals 3
    .param p1, "pos"    # J

    .prologue
    .line 207
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/app/video/editor/external/NativeInterface;->pause()I

    .line 208
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v0

    long-to-int v1, p1

    invoke-virtual {v0, v1}, Lcom/samsung/app/video/editor/external/NativeInterface;->seek(I)V

    .line 209
    return-void
.end method

.method public setActivity(Landroid/app/Activity;)V
    .locals 0
    .param p1, "actvt"    # Landroid/app/Activity;

    .prologue
    .line 155
    iput-object p1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mActivity:Landroid/app/Activity;

    .line 156
    return-void
.end method

.method public setAdapter(Lcom/sec/android/app/ve/PreviewPlayerInterface$Adapter;)V
    .locals 0
    .param p1, "adap"    # Lcom/sec/android/app/ve/PreviewPlayerInterface$Adapter;

    .prologue
    .line 229
    iput-object p1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mAdapter:Lcom/sec/android/app/ve/PreviewPlayerInterface$Adapter;

    .line 230
    return-void
.end method

.method public setDataSource(Landroid/net/Uri;)V
    .locals 0
    .param p1, "videoURI"    # Landroid/net/Uri;

    .prologue
    .line 750
    return-void
.end method

.method public setDataSource(Ljava/lang/String;)V
    .locals 0
    .param p1, "videoPath"    # Ljava/lang/String;

    .prologue
    .line 161
    return-void
.end method

.method public setEngineSurface()V
    .locals 2

    .prologue
    .line 713
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    if-eqz v0, :cond_0

    .line 714
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-virtual {v0, v1}, Lcom/samsung/app/video/editor/external/NativeInterface;->setEngineSurface(Landroid/view/SurfaceHolder;)V

    .line 715
    :cond_0
    return-void
.end method

.method public setFixedSurfaceRect(Landroid/graphics/Rect;)V
    .locals 0
    .param p1, "surfaceRect"    # Landroid/graphics/Rect;

    .prologue
    .line 704
    return-void
.end method

.method public setUIThreadIsLoaded(Z)V
    .locals 1
    .param p1, "loaded"    # Z

    .prologue
    .line 275
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mFrameThread:Lcom/sec/android/app/ve/preview/PreviewFrameThread;

    if-eqz v0, :cond_0

    .line 276
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mFrameThread:Lcom/sec/android/app/ve/preview/PreviewFrameThread;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/ve/preview/PreviewFrameThread;->setUIThreadIsLoaded(Z)V

    .line 278
    :cond_0
    return-void
.end method

.method public setVolume(I)V
    .locals 3
    .param p1, "volume"    # I

    .prologue
    .line 247
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mAudioManager:Landroid/media/AudioManager;

    if-nez v0, :cond_1

    .line 251
    :cond_0
    :goto_0
    return-void

    .line 249
    :cond_1
    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    .line 250
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mAudioManager:Landroid/media/AudioManager;

    const/4 v1, 0x3

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/media/AudioManager;->setStreamVolume(III)V

    goto :goto_0
.end method

.method public stop()V
    .locals 3

    .prologue
    .line 199
    const-string v0, "PLAYER_STATE"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "stop ==> NextState = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mNextState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " CurrentState = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mCurrentState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    iget v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mCurrentState:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 202
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/app/ve/preview/ProjectPreview;->_stop(Z)V

    .line 203
    :cond_0
    return-void
.end method

.method public update(Lcom/samsung/app/video/editor/external/Element;Lcom/samsung/app/video/editor/external/Element;JIFLjava/util/List;Ljava/util/List;)V
    .locals 13
    .param p1, "firstElem"    # Lcom/samsung/app/video/editor/external/Element;
    .param p2, "secondElem"    # Lcom/samsung/app/video/editor/external/Element;
    .param p3, "time"    # J
    .param p5, "prevType"    # I
    .param p6, "storyBoardTime"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/app/video/editor/external/Element;",
            "Lcom/samsung/app/video/editor/external/Element;",
            "JIF",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/ClipartParams;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/ClipartParams;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 213
    .local p7, "clipartLis":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    .local p8, "drawList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    iget v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mCurrentState:I

    if-eqz v0, :cond_1

    .line 219
    :cond_0
    :goto_0
    return-void

    .line 215
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mFrameThread:Lcom/sec/android/app/ve/preview/PreviewFrameThread;

    if-eqz v0, :cond_0

    .line 216
    iget-object v1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mFrameThread:Lcom/sec/android/app/ve/preview/PreviewFrameThread;

    .line 217
    sget v6, Lcom/sec/android/app/ve/common/ConfigUtils;->PREVIEW_WIDTH:I

    sget v7, Lcom/sec/android/app/ve/common/ConfigUtils;->PREVIEW_HEIGHT:I

    move-object v2, p1

    move-object v3, p2

    move-wide/from16 v4, p3

    move/from16 v8, p5

    move/from16 v9, p6

    move-object/from16 v10, p7

    move-object/from16 v11, p8

    .line 216
    invoke-virtual/range {v1 .. v11}, Lcom/sec/android/app/ve/preview/PreviewFrameThread;->addOperation(Lcom/samsung/app/video/editor/external/Element;Lcom/samsung/app/video/editor/external/Element;JIIIFLjava/util/List;Ljava/util/List;)V

    goto :goto_0
.end method
