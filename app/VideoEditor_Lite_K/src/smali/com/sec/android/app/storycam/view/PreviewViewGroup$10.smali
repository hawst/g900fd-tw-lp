.class Lcom/sec/android/app/storycam/view/PreviewViewGroup$10;
.super Ljava/lang/Object;
.source "PreviewViewGroup.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/storycam/view/PreviewViewGroup;->init()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$10;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    .line 225
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 228
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$10;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    invoke-virtual {v0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->isPreviewPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 229
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$10;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    invoke-virtual {v0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->pause_player()V

    .line 231
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$10;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    # getter for: Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;
    invoke-static {v0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$4(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    move-result-object v0

    const/16 v1, 0x64

    invoke-interface {v0, v1}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;->launchDialog(I)V

    .line 232
    return-void
.end method
