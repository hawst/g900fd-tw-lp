.class Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;
.super Ljava/lang/Object;
.source "AppDataManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/storycam/AppDataManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ThumbnailCacheItem"
.end annotation


# instance fields
.field private mBitmap:Landroid/graphics/Bitmap;

.field private mEndMatrix:Landroid/graphics/Matrix;

.field private mEndRect:Landroid/graphics/RectF;

.field private mRefRect:Landroid/graphics/RectF;

.field private mStartMatrix:Landroid/graphics/Matrix;

.field private mStartRect:Landroid/graphics/RectF;


# direct methods
.method public constructor <init>(Landroid/graphics/Bitmap;Z)V
    .locals 0
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "engineCacheDone"    # Z

    .prologue
    .line 671
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 672
    iput-object p1, p0, Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;->mBitmap:Landroid/graphics/Bitmap;

    .line 673
    return-void
.end method

.method public constructor <init>(Landroid/graphics/Bitmap;ZLandroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix;Landroid/graphics/Matrix;)V
    .locals 0
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "engineCacheDone"    # Z
    .param p3, "refRect"    # Landroid/graphics/RectF;
    .param p4, "startRect"    # Landroid/graphics/RectF;
    .param p5, "endRect"    # Landroid/graphics/RectF;
    .param p6, "startMatrix"    # Landroid/graphics/Matrix;
    .param p7, "endMatrix"    # Landroid/graphics/Matrix;

    .prologue
    .line 675
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 677
    iput-object p1, p0, Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;->mBitmap:Landroid/graphics/Bitmap;

    .line 679
    iput-object p3, p0, Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;->mRefRect:Landroid/graphics/RectF;

    .line 680
    iput-object p4, p0, Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;->mStartRect:Landroid/graphics/RectF;

    .line 681
    iput-object p5, p0, Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;->mEndRect:Landroid/graphics/RectF;

    .line 683
    iput-object p6, p0, Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;->mStartMatrix:Landroid/graphics/Matrix;

    .line 684
    iput-object p7, p0, Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;->mEndMatrix:Landroid/graphics/Matrix;

    .line 685
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 662
    iput-object p1, p0, Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;->mBitmap:Landroid/graphics/Bitmap;

    return-void
.end method

.method static synthetic access$1(Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 662
    iget-object v0, p0, Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;->mBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;)Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 665
    iget-object v0, p0, Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;->mStartRect:Landroid/graphics/RectF;

    return-object v0
.end method

.method static synthetic access$3(Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;)Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 666
    iget-object v0, p0, Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;->mEndRect:Landroid/graphics/RectF;

    return-object v0
.end method

.method static synthetic access$4(Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;)Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 668
    iget-object v0, p0, Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;->mStartMatrix:Landroid/graphics/Matrix;

    return-object v0
.end method

.method static synthetic access$5(Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;)Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 669
    iget-object v0, p0, Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;->mEndMatrix:Landroid/graphics/Matrix;

    return-object v0
.end method

.method static synthetic access$6(Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;)Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 664
    iget-object v0, p0, Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;->mRefRect:Landroid/graphics/RectF;

    return-object v0
.end method
