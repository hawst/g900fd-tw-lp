.class Lcom/sec/android/app/storycam/view/PreviewViewGroup$5;
.super Ljava/lang/Object;
.source "PreviewViewGroup.java"

# interfaces
.implements Lcom/sec/android/app/storycam/AppDataManager$TranscodeElementChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/storycam/view/PreviewViewGroup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$5;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    .line 657
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTranscodeElementChanged(Lcom/samsung/app/video/editor/external/TranscodeElement;)V
    .locals 4
    .param p1, "transcodeElement"    # Lcom/samsung/app/video/editor/external/TranscodeElement;

    .prologue
    .line 660
    if-eqz p1, :cond_1

    .line 661
    iget-object v2, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$5;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    invoke-static {v2, p1}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$12(Lcom/sec/android/app/storycam/view/PreviewViewGroup;Lcom/samsung/app/video/editor/external/TranscodeElement;)V

    .line 663
    iget-object v2, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$5;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    # getter for: Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mProgressBar:Landroid/widget/SeekBar;
    invoke-static {v2}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$2(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)Landroid/widget/SeekBar;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    .line 665
    .local v0, "pos":I
    iget-object v2, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$5;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    # getter for: Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mCurrTransElement:Lcom/samsung/app/video/editor/external/TranscodeElement;
    invoke-static {v2}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$1(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getTotalDuration()J

    move-result-wide v2

    long-to-int v1, v2

    .line 668
    .local v1, "totalProjDuration":I
    if-le v0, v1, :cond_0

    .line 669
    iget-object v2, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$5;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    # getter for: Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mProgressBar:Landroid/widget/SeekBar;
    invoke-static {v2}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$2(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)Landroid/widget/SeekBar;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 671
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$5;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    # getter for: Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mCurrTransElement:Lcom/samsung/app/video/editor/external/TranscodeElement;
    invoke-static {v2}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$1(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->setFullMovieDuration(I)V

    .line 672
    iget-object v2, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$5;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    # invokes: Lcom/sec/android/app/storycam/view/PreviewViewGroup;->updateSeekBar(I)V
    invoke-static {v2, v1}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$13(Lcom/sec/android/app/storycam/view/PreviewViewGroup;I)V

    .line 674
    .end local v0    # "pos":I
    .end local v1    # "totalProjDuration":I
    :cond_1
    return-void
.end method
