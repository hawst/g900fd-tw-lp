.class Lcom/sec/android/app/ve/preview/ProjectPreview2$2;
.super Ljava/lang/Object;
.source "ProjectPreview2.java"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/ve/preview/ProjectPreview2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/ve/preview/ProjectPreview2;


# direct methods
.method constructor <init>(Lcom/sec/android/app/ve/preview/ProjectPreview2;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2$2;->this$0:Lcom/sec/android/app/ve/preview/ProjectPreview2;

    .line 564
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 5
    .param p1, "arg0"    # Landroid/view/SurfaceHolder;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I
    .param p4, "arg3"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 613
    const-string v0, "PLAYER_STATE"

    const-string v1, "PVG::surface surfaceChanged"

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 615
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2$2;->this$0:Lcom/sec/android/app/ve/preview/ProjectPreview2;

    # getter for: Lcom/sec/android/app/ve/preview/ProjectPreview2;->mFrameThread:Lcom/sec/android/app/ve/preview/PreviewFrameThread;
    invoke-static {v0}, Lcom/sec/android/app/ve/preview/ProjectPreview2;->access$0(Lcom/sec/android/app/ve/preview/ProjectPreview2;)Lcom/sec/android/app/ve/preview/PreviewFrameThread;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 616
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2$2;->this$0:Lcom/sec/android/app/ve/preview/ProjectPreview2;

    # getter for: Lcom/sec/android/app/ve/preview/ProjectPreview2;->mFrameThread:Lcom/sec/android/app/ve/preview/PreviewFrameThread;
    invoke-static {v0}, Lcom/sec/android/app/ve/preview/ProjectPreview2;->access$0(Lcom/sec/android/app/ve/preview/ProjectPreview2;)Lcom/sec/android/app/ve/preview/PreviewFrameThread;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/sec/android/app/ve/preview/PreviewFrameThread;->setSurfaceAvailable(Z)V

    .line 618
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2$2;->this$0:Lcom/sec/android/app/ve/preview/ProjectPreview2;

    invoke-static {v0, p1}, Lcom/sec/android/app/ve/preview/ProjectPreview2;->access$4(Lcom/sec/android/app/ve/preview/ProjectPreview2;Landroid/view/SurfaceHolder;)V

    .line 619
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2$2;->this$0:Lcom/sec/android/app/ve/preview/ProjectPreview2;

    # getter for: Lcom/sec/android/app/ve/preview/ProjectPreview2;->mSurfaceHolder:Landroid/view/SurfaceHolder;
    invoke-static {v0}, Lcom/sec/android/app/ve/preview/ProjectPreview2;->access$5(Lcom/sec/android/app/ve/preview/ProjectPreview2;)Landroid/view/SurfaceHolder;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 620
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2$2;->this$0:Lcom/sec/android/app/ve/preview/ProjectPreview2;

    # getter for: Lcom/sec/android/app/ve/preview/ProjectPreview2;->mSurfaceHolder:Landroid/view/SurfaceHolder;
    invoke-static {v0}, Lcom/sec/android/app/ve/preview/ProjectPreview2;->access$5(Lcom/sec/android/app/ve/preview/ProjectPreview2;)Landroid/view/SurfaceHolder;

    move-result-object v0

    sget v1, Lcom/sec/android/app/ve/common/ConfigUtils;->FIXED_PREVIEW_WIDTH:I

    sget v2, Lcom/sec/android/app/ve/common/ConfigUtils;->FIXED_PREVIEW_HEIGHT:I

    invoke-interface {v0, v1, v2}, Landroid/view/SurfaceHolder;->setFixedSize(II)V

    .line 622
    sget-object v0, Lcom/sec/android/app/ve/VEApp;->gExport:Lcom/sec/android/app/ve/export/Export;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/export/Export;->isExportRunning()Z

    move-result v0

    if-nez v0, :cond_4

    .line 623
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2$2;->this$0:Lcom/sec/android/app/ve/preview/ProjectPreview2;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/preview/ProjectPreview2;->setEngineSurface()V

    .line 626
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/app/video/editor/external/NativeInterface;->makeSurfaceBlack()V

    .line 634
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2$2;->this$0:Lcom/sec/android/app/ve/preview/ProjectPreview2;

    # getter for: Lcom/sec/android/app/ve/preview/ProjectPreview2;->mFrameThread:Lcom/sec/android/app/ve/preview/PreviewFrameThread;
    invoke-static {v0}, Lcom/sec/android/app/ve/preview/ProjectPreview2;->access$0(Lcom/sec/android/app/ve/preview/ProjectPreview2;)Lcom/sec/android/app/ve/preview/PreviewFrameThread;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 635
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2$2;->this$0:Lcom/sec/android/app/ve/preview/ProjectPreview2;

    # getter for: Lcom/sec/android/app/ve/preview/ProjectPreview2;->mFrameThread:Lcom/sec/android/app/ve/preview/PreviewFrameThread;
    invoke-static {v0}, Lcom/sec/android/app/ve/preview/ProjectPreview2;->access$0(Lcom/sec/android/app/ve/preview/ProjectPreview2;)Lcom/sec/android/app/ve/preview/PreviewFrameThread;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/ve/preview/PreviewFrameThread;->setSurfaceAvailable(Z)V

    .line 636
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2$2;->this$0:Lcom/sec/android/app/ve/preview/ProjectPreview2;

    invoke-static {v0, v4}, Lcom/sec/android/app/ve/preview/ProjectPreview2;->access$1(Lcom/sec/android/app/ve/preview/ProjectPreview2;Z)V

    .line 638
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2$2;->this$0:Lcom/sec/android/app/ve/preview/ProjectPreview2;

    # getter for: Lcom/sec/android/app/ve/preview/ProjectPreview2;->waitForSurfaceCreation:Z
    invoke-static {v0}, Lcom/sec/android/app/ve/preview/ProjectPreview2;->access$6(Lcom/sec/android/app/ve/preview/ProjectPreview2;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 640
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2$2;->this$0:Lcom/sec/android/app/ve/preview/ProjectPreview2;

    invoke-static {v0, v3}, Lcom/sec/android/app/ve/preview/ProjectPreview2;->access$7(Lcom/sec/android/app/ve/preview/ProjectPreview2;Z)V

    .line 641
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2$2;->this$0:Lcom/sec/android/app/ve/preview/ProjectPreview2;

    iget-object v1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2$2;->this$0:Lcom/sec/android/app/ve/preview/ProjectPreview2;

    # getter for: Lcom/sec/android/app/ve/preview/ProjectPreview2;->mPlayPos:J
    invoke-static {v1}, Lcom/sec/android/app/ve/preview/ProjectPreview2;->access$8(Lcom/sec/android/app/ve/preview/ProjectPreview2;)J

    move-result-wide v2

    # invokes: Lcom/sec/android/app/ve/preview/ProjectPreview2;->_play(J)V
    invoke-static {v0, v2, v3}, Lcom/sec/android/app/ve/preview/ProjectPreview2;->access$9(Lcom/sec/android/app/ve/preview/ProjectPreview2;J)V

    .line 643
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2$2;->this$0:Lcom/sec/android/app/ve/preview/ProjectPreview2;

    iget-object v0, v0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mAdapter:Lcom/sec/android/app/ve/PreviewPlayerInterface$Adapter;

    if-eqz v0, :cond_3

    .line 644
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2$2;->this$0:Lcom/sec/android/app/ve/preview/ProjectPreview2;

    iget-object v0, v0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mAdapter:Lcom/sec/android/app/ve/PreviewPlayerInterface$Adapter;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/PreviewPlayerInterface$Adapter;->onSurfaceChanged()V

    .line 647
    :cond_3
    return-void

    .line 629
    :cond_4
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v0

    sget v1, Lcom/sec/android/app/ve/common/ConfigUtils;->FIXED_PREVIEW_WIDTH:I

    sget v2, Lcom/sec/android/app/ve/common/ConfigUtils;->FIXED_PREVIEW_HEIGHT:I

    invoke-virtual {v0, v1, v2}, Lcom/samsung/app/video/editor/external/NativeInterface;->setDisplayAspectRatio(II)V

    .line 630
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2$2;->this$0:Lcom/sec/android/app/ve/preview/ProjectPreview2;

    # getter for: Lcom/sec/android/app/ve/preview/ProjectPreview2;->mSurfaceHolder:Landroid/view/SurfaceHolder;
    invoke-static {v1}, Lcom/sec/android/app/ve/preview/ProjectPreview2;->access$5(Lcom/sec/android/app/ve/preview/ProjectPreview2;)Landroid/view/SurfaceHolder;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/app/video/editor/external/NativeInterface;->setPreviewDisplay(Landroid/view/SurfaceHolder;)V

    goto :goto_0
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 3
    .param p1, "arg0"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 583
    const-string v0, "PLAYER_STATE"

    const-string v1, "PVG::surface surfaceCreated"

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 584
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2$2;->this$0:Lcom/sec/android/app/ve/preview/ProjectPreview2;

    invoke-static {v0, p1}, Lcom/sec/android/app/ve/preview/ProjectPreview2;->access$4(Lcom/sec/android/app/ve/preview/ProjectPreview2;Landroid/view/SurfaceHolder;)V

    .line 585
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2$2;->this$0:Lcom/sec/android/app/ve/preview/ProjectPreview2;

    # getter for: Lcom/sec/android/app/ve/preview/ProjectPreview2;->mSurfaceHolder:Landroid/view/SurfaceHolder;
    invoke-static {v0}, Lcom/sec/android/app/ve/preview/ProjectPreview2;->access$5(Lcom/sec/android/app/ve/preview/ProjectPreview2;)Landroid/view/SurfaceHolder;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 586
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2$2;->this$0:Lcom/sec/android/app/ve/preview/ProjectPreview2;

    # getter for: Lcom/sec/android/app/ve/preview/ProjectPreview2;->mSurfaceHolder:Landroid/view/SurfaceHolder;
    invoke-static {v0}, Lcom/sec/android/app/ve/preview/ProjectPreview2;->access$5(Lcom/sec/android/app/ve/preview/ProjectPreview2;)Landroid/view/SurfaceHolder;

    move-result-object v0

    sget v1, Lcom/sec/android/app/ve/common/ConfigUtils;->FIXED_PREVIEW_WIDTH:I

    sget v2, Lcom/sec/android/app/ve/common/ConfigUtils;->FIXED_PREVIEW_HEIGHT:I

    invoke-interface {v0, v1, v2}, Landroid/view/SurfaceHolder;->setFixedSize(II)V

    .line 609
    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 3
    .param p1, "arg0"    # Landroid/view/SurfaceHolder;

    .prologue
    const/4 v2, 0x0

    .line 568
    const-string v0, "PLAYER_STATE"

    const-string v1, "PVG::surface destroyed"

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 569
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/samsung/app/video/editor/external/NativeInterface;->_surfaceDestroyed(Landroid/view/SurfaceHolder;)V

    .line 571
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2$2;->this$0:Lcom/sec/android/app/ve/preview/ProjectPreview2;

    # getter for: Lcom/sec/android/app/ve/preview/ProjectPreview2;->mFrameThread:Lcom/sec/android/app/ve/preview/PreviewFrameThread;
    invoke-static {v0}, Lcom/sec/android/app/ve/preview/ProjectPreview2;->access$0(Lcom/sec/android/app/ve/preview/ProjectPreview2;)Lcom/sec/android/app/ve/preview/PreviewFrameThread;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 572
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2$2;->this$0:Lcom/sec/android/app/ve/preview/ProjectPreview2;

    # getter for: Lcom/sec/android/app/ve/preview/ProjectPreview2;->mFrameThread:Lcom/sec/android/app/ve/preview/PreviewFrameThread;
    invoke-static {v0}, Lcom/sec/android/app/ve/preview/ProjectPreview2;->access$0(Lcom/sec/android/app/ve/preview/ProjectPreview2;)Lcom/sec/android/app/ve/preview/PreviewFrameThread;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/ve/preview/PreviewFrameThread;->setSurfaceAvailable(Z)V

    .line 573
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2$2;->this$0:Lcom/sec/android/app/ve/preview/ProjectPreview2;

    invoke-static {v0, v2}, Lcom/sec/android/app/ve/preview/ProjectPreview2;->access$1(Lcom/sec/android/app/ve/preview/ProjectPreview2;Z)V

    .line 575
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2$2;->this$0:Lcom/sec/android/app/ve/preview/ProjectPreview2;

    # invokes: Lcom/sec/android/app/ve/preview/ProjectPreview2;->_stop(Z)V
    invoke-static {v0, v2}, Lcom/sec/android/app/ve/preview/ProjectPreview2;->access$2(Lcom/sec/android/app/ve/preview/ProjectPreview2;Z)V

    .line 577
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2$2;->this$0:Lcom/sec/android/app/ve/preview/ProjectPreview2;

    # getter for: Lcom/sec/android/app/ve/preview/ProjectPreview2;->mCurrentState:I
    invoke-static {v0}, Lcom/sec/android/app/ve/preview/ProjectPreview2;->access$3(Lcom/sec/android/app/ve/preview/ProjectPreview2;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2$2;->this$0:Lcom/sec/android/app/ve/preview/ProjectPreview2;

    iget-object v0, v0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mAdapter:Lcom/sec/android/app/ve/PreviewPlayerInterface$Adapter;

    if-eqz v0, :cond_1

    .line 578
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2$2;->this$0:Lcom/sec/android/app/ve/preview/ProjectPreview2;

    iget-object v0, v0, Lcom/sec/android/app/ve/preview/ProjectPreview2;->mAdapter:Lcom/sec/android/app/ve/PreviewPlayerInterface$Adapter;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/PreviewPlayerInterface$Adapter;->onStopped()V

    .line 579
    :cond_1
    return-void
.end method
