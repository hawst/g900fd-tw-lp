.class Lcom/sec/android/app/ve/util/CommonUtils$2;
.super Ljava/lang/Object;
.source "CommonUtils.java"

# interfaces
.implements Landroid/text/InputFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/ve/util/CommonUtils;->getEditTextFilter(I)[Landroid/text/InputFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 795
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;
    .locals 5
    .param p1, "source"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "end"    # I
    .param p4, "dest"    # Landroid/text/Spanned;
    .param p5, "dstart"    # I
    .param p6, "dend"    # I

    .prologue
    .line 798
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    # getter for: Lcom/sec/android/app/ve/util/CommonUtils;->INVALID_CHAR:[Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/ve/util/CommonUtils;->access$0()[Ljava/lang/String;

    move-result-object v3

    array-length v3, v3

    if-lt v1, v3, :cond_1

    .line 810
    if-eqz p3, :cond_0

    .line 813
    const/4 v1, 0x0

    :goto_1
    if-lt v1, p3, :cond_6

    .line 833
    :cond_0
    :goto_2
    const/4 v3, 0x0

    :goto_3
    return-object v3

    .line 799
    :cond_1
    invoke-interface {p4}, Landroid/text/Spanned;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_2

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    if-gez v3, :cond_3

    :cond_2
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    # getter for: Lcom/sec/android/app/ve/util/CommonUtils;->INVALID_CHAR:[Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/ve/util/CommonUtils;->access$0()[Ljava/lang/String;

    move-result-object v4

    aget-object v4, v4, v1

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    if-ltz v3, :cond_5

    .line 800
    :cond_3
    # getter for: Lcom/sec/android/app/ve/util/CommonUtils;->mToastInvalidChar:Landroid/widget/Toast;
    invoke-static {}, Lcom/sec/android/app/ve/util/CommonUtils;->access$1()Landroid/widget/Toast;

    move-result-object v3

    if-nez v3, :cond_4

    .line 801
    sget v3, Lcom/sec/android/app/ve/R$string;->invalid_character:I

    invoke-static {v3}, Lcom/sec/android/app/ve/VEApp;->makeToast(I)Landroid/widget/Toast;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/ve/util/CommonUtils;->access$2(Landroid/widget/Toast;)V

    .line 805
    :goto_4
    # getter for: Lcom/sec/android/app/ve/util/CommonUtils;->mToastInvalidChar:Landroid/widget/Toast;
    invoke-static {}, Lcom/sec/android/app/ve/util/CommonUtils;->access$1()Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 806
    const-string v3, ""

    goto :goto_3

    .line 803
    :cond_4
    # getter for: Lcom/sec/android/app/ve/util/CommonUtils;->mToastInvalidChar:Landroid/widget/Toast;
    invoke-static {}, Lcom/sec/android/app/ve/util/CommonUtils;->access$1()Landroid/widget/Toast;

    move-result-object v3

    sget v4, Lcom/sec/android/app/ve/R$string;->invalid_character:I

    invoke-virtual {v3, v4}, Landroid/widget/Toast;->setText(I)V

    goto :goto_4

    .line 798
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 814
    :cond_6
    :try_start_0
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/String;->codePointAt(I)I

    move-result v2

    .line 815
    .local v2, "value":I
    invoke-static {p1}, Lcom/sec/android/app/ve/util/CommonUtils;->hasEmojiString(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 817
    # getter for: Lcom/sec/android/app/ve/util/CommonUtils;->mToastInvalidChar:Landroid/widget/Toast;
    invoke-static {}, Lcom/sec/android/app/ve/util/CommonUtils;->access$1()Landroid/widget/Toast;

    move-result-object v3

    if-nez v3, :cond_7

    .line 818
    sget v3, Lcom/sec/android/app/ve/R$string;->invalid_character:I

    invoke-static {v3}, Lcom/sec/android/app/ve/VEApp;->makeToast(I)Landroid/widget/Toast;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/ve/util/CommonUtils;->access$2(Landroid/widget/Toast;)V

    .line 823
    :goto_5
    # getter for: Lcom/sec/android/app/ve/util/CommonUtils;->mToastInvalidChar:Landroid/widget/Toast;
    invoke-static {}, Lcom/sec/android/app/ve/util/CommonUtils;->access$1()Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 825
    const-string v3, ""

    goto :goto_3

    .line 820
    :cond_7
    :try_start_1
    # getter for: Lcom/sec/android/app/ve/util/CommonUtils;->mToastInvalidChar:Landroid/widget/Toast;
    invoke-static {}, Lcom/sec/android/app/ve/util/CommonUtils;->access$1()Landroid/widget/Toast;

    move-result-object v3

    sget v4, Lcom/sec/android/app/ve/R$string;->invalid_character:I

    invoke-virtual {v3, v4}, Landroid/widget/Toast;->setText(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_5

    .line 828
    .end local v2    # "value":I
    :catch_0
    move-exception v0

    .line 829
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_2

    .line 813
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v2    # "value":I
    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_1
.end method
