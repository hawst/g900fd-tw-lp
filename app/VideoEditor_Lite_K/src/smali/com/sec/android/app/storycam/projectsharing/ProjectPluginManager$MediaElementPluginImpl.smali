.class public Lcom/sec/android/app/storycam/projectsharing/ProjectPluginManager$MediaElementPluginImpl;
.super Ljava/lang/Object;
.source "ProjectPluginManager.java"

# interfaces
.implements Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework$MediaElementPlugin;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/storycam/projectsharing/ProjectPluginManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MediaElementPluginImpl"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public mapElementSubTypeFromGeneric(I)I
    .locals 1
    .param p1, "genericElementSubType"    # I

    .prologue
    .line 101
    const/4 v0, 0x0

    .line 102
    .local v0, "internalElementSubType":I
    packed-switch p1, :pswitch_data_0

    .line 119
    :goto_0
    return v0

    .line 104
    :pswitch_0
    const/4 v0, 0x1

    .line 105
    goto :goto_0

    .line 107
    :pswitch_1
    const/4 v0, 0x2

    .line 108
    goto :goto_0

    .line 110
    :pswitch_2
    const/4 v0, 0x3

    .line 111
    goto :goto_0

    .line 113
    :pswitch_3
    const/4 v0, 0x4

    .line 114
    goto :goto_0

    .line 102
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public mapElementSubTypeToGeneric(I)I
    .locals 1
    .param p1, "internalElementSubType"    # I

    .prologue
    .line 78
    const/4 v0, 0x0

    .line 96
    .local v0, "genericElementSubType":I
    return v0
.end method

.method public mapElementTypeFromGeneric(I)I
    .locals 1
    .param p1, "genericElementType"    # I

    .prologue
    .line 61
    const/4 v0, 0x0

    .line 63
    .local v0, "internalElementType":I
    packed-switch p1, :pswitch_data_0

    .line 73
    :goto_0
    return v0

    .line 65
    :pswitch_0
    const/4 v0, 0x2

    .line 66
    goto :goto_0

    .line 68
    :pswitch_1
    const/4 v0, 0x1

    .line 69
    goto :goto_0

    .line 63
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public mapElementTypeToGeneric(I)I
    .locals 1
    .param p1, "internalElementType"    # I

    .prologue
    .line 44
    const/4 v0, 0x0

    .line 46
    .local v0, "genericElementType":I
    packed-switch p1, :pswitch_data_0

    .line 56
    :goto_0
    return v0

    .line 48
    :pswitch_0
    const/4 v0, 0x2

    .line 49
    goto :goto_0

    .line 51
    :pswitch_1
    const/4 v0, 0x1

    .line 52
    goto :goto_0

    .line 46
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
