.class public final Lcom/sec/android/app/ve/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/ve/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final Home_theme_black:I = 0x7f05003a

.field public static final action_credits_default_color:I = 0x7f050038

.field public static final action_credits_names_color:I = 0x7f050039

.field public static final action_credits_presents_color:I = 0x7f050036

.field public static final action_credits_title_color:I = 0x7f050037

.field public static final action_peaceful_land_color:I = 0x7f050035

.field public static final action_return_of_the_hero:I = 0x7f050034

.field public static final addmedia_text_color:I = 0x7f05004e

.field public static final audioeffect_normal:I = 0x7f05004b

.field public static final audioeffect_selected:I = 0x7f05004c

.field public static final audiowave_color:I = 0x7f050020

.field public static final auto_mk_big_text:I = 0x7f050015

.field public static final auto_mk_min_text:I = 0x7f050018

.field public static final auto_mk_normal_text:I = 0x7f050017

.field public static final auto_mk_small_text:I = 0x7f050016

.field public static final bgm_name_dim:I = 0x7f050067

.field public static final bgm_shadow_dim:I = 0x7f050068

.field public static final bgm_text_color:I = 0x7f05001d

.field public static final black:I = 0x7f050003

.field public static final black75:I = 0x7f050004

.field public static final caption_divider_color:I = 0x7f050076

.field public static final caption_hint_color:I = 0x7f050069

.field public static final caption_text_color:I = 0x7f050075

.field public static final capturepicture_item_textcolor:I = 0x7f05001b

.field public static final credit_romance_color:I = 0x7f050023

.field public static final credit_romance_credits_color:I = 0x7f050025

.field public static final credit_romance_defaultfilm_color:I = 0x7f050028

.field public static final credit_romance_defaultfilm_eg_color:I = 0x7f050029

.field public static final credit_romance_director_color:I = 0x7f050026

.field public static final credit_romance_director_eg_color:I = 0x7f050027

.field public static final credit_romance_maincast_color:I = 0x7f05002a

.field public static final credit_romance_maincast_eg_color:I = 0x7f05002b

.field public static final credit_romance_present_color:I = 0x7f050030

.field public static final credit_romance_shadow_color:I = 0x7f050022

.field public static final credit_romance_supportingactor_color:I = 0x7f05002c

.field public static final credit_romance_supportingactor_eg_color:I = 0x7f05002d

.field public static final credit_romance_title_color:I = 0x7f050024

.field public static final credit_romance_venue_color:I = 0x7f05002e

.field public static final credit_romance_venue_eg_color:I = 0x7f05002f

.field public static final delete_color_filter:I = 0x7f050006

.field public static final drop_text_color:I = 0x7f050005

.field public static final effect_item:I = 0x7f05000d

.field public static final export_setting_edittext1_color:I = 0x7f05004f

.field public static final export_setting_text:I = 0x7f05004d

.field public static final export_setting_text4_color:I = 0x7f050050

.field public static final exportresol_textColor:I = 0x7f050011

.field public static final folderview_thumb_frameColor:I = 0x7f05001f

.field public static final kenburn_nevershowdialog_textview_text_color:I = 0x7f050051

.field public static final kenburns_begin_end_color:I = 0x7f05004a

.field public static final launcher_download_color:I = 0x7f050041

.field public static final launcher_itemdate_color:I = 0x7f050045

.field public static final launcher_itemdesc_shadowcolor:I = 0x7f050046

.field public static final launcher_itemdescription_color:I = 0x7f050040

.field public static final launcher_itemname_color:I = 0x7f05003d

.field public static final launcher_itemname_shadowcolor:I = 0x7f050044

.field public static final love_story_color:I = 0x7f050021

.field public static final main_screen_preview_lft_time:I = 0x7f050071

.field public static final main_screen_preview_rght_time:I = 0x7f050072

.field public static final mediaContentLayout_bg:I = 0x7f05001a

.field public static final mediaTypeLayout_bg:I = 0x7f050019

.field public static final music_album_albumname_text_color:I = 0x7f050052

.field public static final music_album_artistname_text_color:I = 0x7f050053

.field public static final music_categories_textview_text_color:I = 0x7f050054

.field public static final music_list_divider_color:I = 0x7f05006a

.field public static final music_option_parent_textview_option_content_counttext_color:I = 0x7f050056

.field public static final music_option_parent_textview_text_color:I = 0x7f050055

.field public static final music_songs_artistname_textview_text_color:I = 0x7f050058

.field public static final music_songs_picker_pickerview_hintcolor:I = 0x7f050059

.field public static final music_songs_songname_textview_text_color:I = 0x7f050057

.field public static final music_text_color:I = 0x7f050012

.field public static final music_thumb_normal_frame_color:I = 0x7f05006b

.field public static final music_thumb_selected_frame_color:I = 0x7f05006c

.field public static final notification_layout_exportname_text_color:I = 0x7f05005a

.field public static final notification_layout_percent_prog_text_color:I = 0x7f05005d

.field public static final notification_layout_status_text_color:I = 0x7f05005b

.field public static final notification_layout_status_text_color_l:I = 0x7f05005c

.field public static final outing_text_color:I = 0x7f05003b

.field public static final preview_play_textview_text_color:I = 0x7f05005e

.field public static final preview_time:I = 0x7f050009

.field public static final preview_trim_right_text:I = 0x7f050014

.field public static final progress_popup_textcolor:I = 0x7f050000

.field public static final project_date_delete_color:I = 0x7f05003f

.field public static final project_name_delete_color:I = 0x7f05003e

.field public static final project_time_color:I = 0x7f050043

.field public static final project_time_shadow_color:I = 0x7f050042

.field public static final record_layout_bg:I = 0x7f050070

.field public static final record_time_color:I = 0x7f05000e

.field public static final record_time_color_noRec:I = 0x7f05000f

.field public static final recordbar_text_color:I = 0x7f05001c

.field public static final recordeffect_name:I = 0x7f050047

.field public static final recordeffect_name_dim:I = 0x7f050066

.field public static final sample_video_text_color:I = 0x7f050074

.field public static final select_all_text_color:I = 0x7f050010

.field public static final selection:I = 0x7f050007

.field public static final shadow_color_pr_name_date:I = 0x7f05003c

.field public static final sound_effects_subviewgroup_textview_tvHRSES_bgcolor:I = 0x7f050062

.field public static final sound_effects_subviewgroup_textview_tvSoundEffectsItemNameSES_textcolor:I = 0x7f050061

.field public static final sound_effects_textview_soundTrackCount_textcolor:I = 0x7f050060

.field public static final sound_effects_textview_tvMusicItemNameSEI_textcolor:I = 0x7f05005f

.field public static final sound_item_duration:I = 0x7f05000c

.field public static final sound_item_name:I = 0x7f05000b

.field public static final soundeffect_name:I = 0x7f050048

.field public static final soundeffect_name_shadow:I = 0x7f050049

.field public static final soundeffect_wavecolor:I = 0x7f050065

.field public static final soundeffectbar_text_color:I = 0x7f05001e

.field public static final texteffectblack:I = 0x7f05006e

.field public static final texteffectnone:I = 0x7f05006f

.field public static final texteffectwhite:I = 0x7f05006d

.field public static final theme_text:I = 0x7f05000a

.field public static final theme_thumbnailview_thumbnail_change_media_icon_bgcolor:I = 0x7f050063

.field public static final time_slot:I = 0x7f050073

.field public static final transparent:I = 0x7f050001

.field public static final travel_credit_color:I = 0x7f050032

.field public static final travel_credits_font_color:I = 0x7f050033

.field public static final travel_shadow_black_50:I = 0x7f050031

.field public static final trimmed_color:I = 0x7f050008

.field public static final video_summary_dialog_textoption_textcolor:I = 0x7f050064

.field public static final videoaudio_text_color:I = 0x7f050013

.field public static final white:I = 0x7f050002

.field public static final white96:I = 0x7f050077


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
