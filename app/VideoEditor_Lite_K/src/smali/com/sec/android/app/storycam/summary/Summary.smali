.class public Lcom/sec/android/app/storycam/summary/Summary;
.super Ljava/lang/Object;
.source "Summary.java"


# static fields
.field private static analysisRequired:Z

.field private static pd:Landroid/app/ProgressDialog;

.field private static summaryThread:Lcom/sec/android/app/ve/thread/HybridSummaryThread;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 32
    sput-object v1, Lcom/sec/android/app/storycam/summary/Summary;->summaryThread:Lcom/sec/android/app/ve/thread/HybridSummaryThread;

    .line 33
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/storycam/summary/Summary;->analysisRequired:Z

    .line 34
    sput-object v1, Lcom/sec/android/app/storycam/summary/Summary;->pd:Landroid/app/ProgressDialog;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0()Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/sec/android/app/storycam/summary/Summary;->pd:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method private static addCaptionAfterSummary(Lcom/samsung/app/video/editor/external/TranscodeElement;Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;)V
    .locals 18
    .param p0, "transcodeElement"    # Lcom/samsung/app/video/editor/external/TranscodeElement;
    .param p1, "activityContext"    # Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    .prologue
    .line 253
    const/4 v1, 0x0

    .line 255
    .local v1, "summaryCaption":Lcom/samsung/app/video/editor/external/ClipartParams;
    sget-object v3, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v3}, Lcom/sec/android/app/storycam/AppDataManager;->getCurrentTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v13

    .line 257
    .local v13, "beforeSummaryTranscode":Lcom/samsung/app/video/editor/external/TranscodeElement;
    const/4 v12, 0x0

    .line 258
    .local v12, "beforeSummaryCaption":Lcom/samsung/app/video/editor/external/ClipartParams;
    if-eqz v13, :cond_3

    invoke-virtual {v13}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getTextEleListCount()I

    move-result v3

    if-lez v3, :cond_3

    .line 259
    invoke-virtual {v13}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getTextEleList()Ljava/util/List;

    move-result-object v3

    const/4 v6, 0x0

    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    .end local v12    # "beforeSummaryCaption":Lcom/samsung/app/video/editor/external/ClipartParams;
    check-cast v12, Lcom/samsung/app/video/editor/external/ClipartParams;

    .line 260
    .restart local v12    # "beforeSummaryCaption":Lcom/samsung/app/video/editor/external/ClipartParams;
    const/4 v13, 0x0

    .line 267
    :cond_0
    :goto_0
    if-eqz p0, :cond_2

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getTextEleListCount()I

    move-result v3

    if-lez v3, :cond_2

    .line 268
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getTextEleList()Ljava/util/List;

    move-result-object v15

    .line 269
    .local v15, "list":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    const/4 v3, 0x0

    invoke-interface {v15, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "summaryCaption":Lcom/samsung/app/video/editor/external/ClipartParams;
    check-cast v1, Lcom/samsung/app/video/editor/external/ClipartParams;

    .line 271
    .restart local v1    # "summaryCaption":Lcom/samsung/app/video/editor/external/ClipartParams;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getThemeName()I

    move-result v17

    .line 272
    .local v17, "theme":I
    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElement(I)Lcom/samsung/app/video/editor/external/Element;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/Element;->getGroupID()I

    move-result v14

    .line 273
    .local v14, "elementID":I
    sget-object v3, Lcom/sec/android/app/ve/VEApp;->gThemeMgr:Lcom/sec/android/app/ve/theme/ThemeManager;

    move/from16 v0, v17

    invoke-virtual {v3, v0}, Lcom/sec/android/app/ve/theme/ThemeManager;->getThemeAssetManager(I)Landroid/content/res/AssetManager;

    move-result-object v4

    .line 274
    .local v4, "assetManager":Landroid/content/res/AssetManager;
    if-eqz v12, :cond_1

    invoke-virtual {v12}, Lcom/samsung/app/video/editor/external/ClipartParams;->isThemeDefaultText()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 276
    :cond_1
    const-string v2, ""

    .local v2, "name":Ljava/lang/String;
    move-object/from16 v3, p1

    .line 277
    check-cast v3, Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v6, 0x7f02006c

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    check-cast v5, Landroid/graphics/drawable/NinePatchDrawable;

    .local v5, "npd":Landroid/graphics/drawable/NinePatchDrawable;
    move-object/from16 v3, p1

    .line 278
    check-cast v3, Landroid/content/Context;

    const v6, 0x7f06037e

    invoke-static {v6}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v6

    invoke-virtual/range {v1 .. v6}, Lcom/samsung/app/video/editor/external/ClipartParams;->changeCaptionData(Ljava/lang/String;Landroid/content/Context;Landroid/content/res/AssetManager;Landroid/graphics/drawable/NinePatchDrawable;I)V

    .line 286
    .end local v2    # "name":Ljava/lang/String;
    :goto_1
    invoke-virtual {v1, v14}, Lcom/samsung/app/video/editor/external/ClipartParams;->setElementID(I)V

    .line 287
    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/ClipartParams;->getCaptionID()I

    move-result v3

    invoke-static {v15, v3, v1, v14}, Lcom/sec/android/app/storycam/Utils;->updateAllTextsWithSameCaptionID(Ljava/util/List;ILcom/samsung/app/video/editor/external/ClipartParams;I)V

    .line 289
    .end local v4    # "assetManager":Landroid/content/res/AssetManager;
    .end local v5    # "npd":Landroid/graphics/drawable/NinePatchDrawable;
    .end local v14    # "elementID":I
    .end local v15    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    .end local v17    # "theme":I
    :cond_2
    return-void

    .line 262
    :cond_3
    if-nez v13, :cond_0

    .line 263
    sget-object v3, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v3}, Lcom/sec/android/app/storycam/AppDataManager;->getOriginalTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v16

    .line 264
    .local v16, "originalTranscodeElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    if-eqz v16, :cond_0

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getTextEleListCount()I

    move-result v3

    if-lez v3, :cond_0

    .line 265
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getTextEleList()Ljava/util/List;

    move-result-object v3

    const/4 v6, 0x0

    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    .end local v12    # "beforeSummaryCaption":Lcom/samsung/app/video/editor/external/ClipartParams;
    check-cast v12, Lcom/samsung/app/video/editor/external/ClipartParams;

    .restart local v12    # "beforeSummaryCaption":Lcom/samsung/app/video/editor/external/ClipartParams;
    goto :goto_0

    .end local v16    # "originalTranscodeElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    .restart local v4    # "assetManager":Landroid/content/res/AssetManager;
    .restart local v14    # "elementID":I
    .restart local v15    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    .restart local v17    # "theme":I
    :cond_4
    move-object/from16 v3, p1

    .line 282
    check-cast v3, Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v6, 0x7f02006c

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    check-cast v5, Landroid/graphics/drawable/NinePatchDrawable;

    .line 283
    .restart local v5    # "npd":Landroid/graphics/drawable/NinePatchDrawable;
    iget-object v7, v12, Lcom/samsung/app/video/editor/external/ClipartParams;->data:Ljava/lang/String;

    move-object/from16 v8, p1

    check-cast v8, Landroid/content/Context;

    const v3, 0x7f06037e

    invoke-static {v3}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v11

    move-object v6, v1

    move-object v9, v4

    move-object v10, v5

    invoke-virtual/range {v6 .. v11}, Lcom/samsung/app/video/editor/external/ClipartParams;->changeCaptionData(Ljava/lang/String;Landroid/content/Context;Landroid/content/res/AssetManager;Landroid/graphics/drawable/NinePatchDrawable;I)V

    .line 284
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Lcom/samsung/app/video/editor/external/ClipartParams;->setThemeDefaultText(Z)V

    goto :goto_1
.end method

.method private static createDefaultProjectWithRepeatItems(Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;Lcom/samsung/app/video/editor/external/TranscodeElement;)Lcom/samsung/app/video/editor/external/TranscodeElement;
    .locals 9
    .param p0, "activityContext"    # Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;
    .param p1, "summarized"    # Lcom/samsung/app/video/editor/external/TranscodeElement;

    .prologue
    .line 71
    sget-object v8, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v8}, Lcom/sec/android/app/storycam/AppDataManager;->getOriginalTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v1

    .line 72
    .local v1, "current":Lcom/samsung/app/video/editor/external/TranscodeElement;
    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v6

    .line 73
    .local v6, "originalSize":I
    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v0

    .line 74
    .local v0, "count":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-lt v3, v0, :cond_1

    .line 91
    :cond_0
    return-object p1

    .line 75
    :cond_1
    const/4 v2, 0x0

    .line 76
    .local v2, "filled":Z
    const/4 v4, 0x0

    .local v4, "j":I
    :goto_1
    if-lt v4, v6, :cond_2

    .line 86
    :goto_2
    if-nez v2, :cond_0

    .line 74
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 77
    :cond_2
    invoke-virtual {v1, v4}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElement(I)Lcom/samsung/app/video/editor/external/Element;

    move-result-object v5

    .line 78
    .local v5, "original":Lcom/samsung/app/video/editor/external/Element;
    mul-int v8, v3, v6

    add-int/2addr v8, v4

    invoke-virtual {p1, v8}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElement(I)Lcom/samsung/app/video/editor/external/Element;

    move-result-object v7

    .line 79
    .local v7, "summary":Lcom/samsung/app/video/editor/external/Element;
    if-nez v7, :cond_3

    .line 80
    const/4 v2, 0x1

    .line 81
    goto :goto_2

    .line 83
    :cond_3
    iget-object v8, v5, Lcom/samsung/app/video/editor/external/Element;->filePath:Ljava/lang/String;

    iput-object v8, v7, Lcom/samsung/app/video/editor/external/Element;->filePath:Ljava/lang/String;

    .line 84
    invoke-virtual {v5}, Lcom/samsung/app/video/editor/external/Element;->getGroupID()I

    move-result v8

    invoke-virtual {v7, v8}, Lcom/samsung/app/video/editor/external/Element;->setGroupID(I)V

    .line 76
    add-int/lit8 v4, v4, 0x1

    goto :goto_1
.end method

.method public static createRepeatItemSummaryTranscode(Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;Lcom/samsung/app/video/editor/external/TranscodeElement;)V
    .locals 0
    .param p0, "activityContext"    # Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;
    .param p1, "summaryTrasncode"    # Lcom/samsung/app/video/editor/external/TranscodeElement;

    .prologue
    .line 65
    invoke-static {p0, p1}, Lcom/sec/android/app/storycam/summary/Summary;->createDefaultProjectWithRepeatItems(Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;Lcom/samsung/app/video/editor/external/TranscodeElement;)Lcom/samsung/app/video/editor/external/TranscodeElement;

    .line 66
    invoke-static {p1, p0}, Lcom/sec/android/app/storycam/summary/Summary;->addCaptionAfterSummary(Lcom/samsung/app/video/editor/external/TranscodeElement;Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;)V

    .line 67
    invoke-static {p1}, Lcom/sec/android/app/storycam/summary/Summary;->updateDrawingAfterSummary(Lcom/samsung/app/video/editor/external/TranscodeElement;)V

    .line 68
    return-void
.end method

.method public static deinit()V
    .locals 1

    .prologue
    .line 292
    invoke-static {}, Lcom/sec/android/app/storycam/summary/Summary;->stopSummary()V

    .line 293
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/app/video/editor/external/NativeInterface;->clearSummaryInstance()V

    .line 294
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/app/storycam/summary/Summary;->analysisRequired:Z

    .line 295
    return-void
.end method

.method public static isSummaryRequired(Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;Lcom/samsung/app/video/editor/external/TranscodeElement;)Z
    .locals 6
    .param p0, "activityContext"    # Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;
    .param p1, "summaryTranscodeElement"    # Lcom/samsung/app/video/editor/external/TranscodeElement;

    .prologue
    const/4 v1, 0x0

    .line 45
    sget-object v2, Lcom/sec/android/app/storycam/summary/Summary;->summaryThread:Lcom/sec/android/app/ve/thread/HybridSummaryThread;

    if-eqz v2, :cond_1

    .line 62
    :cond_0
    :goto_0
    return v1

    .line 50
    :cond_1
    sget-object v2, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v2}, Lcom/sec/android/app/storycam/AppDataManager;->getOriginalTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v0

    .line 51
    .local v0, "current":Lcom/samsung/app/video/editor/external/TranscodeElement;
    if-eqz v0, :cond_0

    .line 55
    invoke-virtual {v0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getTotalDuration()J

    move-result-wide v2

    sget v4, Lcom/sec/android/app/storycam/Utils;->MINIMAL_INPUT_DURATION_LIMIT:I

    int-to-long v4, v4

    cmp-long v2, v2, v4

    if-gez v2, :cond_2

    .line 56
    invoke-static {p0, p1}, Lcom/sec/android/app/storycam/summary/Summary;->createDefaultProjectWithRepeatItems(Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;Lcom/samsung/app/video/editor/external/TranscodeElement;)Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v0

    .line 57
    invoke-static {v0, p0}, Lcom/sec/android/app/storycam/summary/Summary;->addCaptionAfterSummary(Lcom/samsung/app/video/editor/external/TranscodeElement;Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;)V

    .line 58
    invoke-static {v0}, Lcom/sec/android/app/storycam/summary/Summary;->updateDrawingAfterSummary(Lcom/samsung/app/video/editor/external/TranscodeElement;)V

    .line 59
    sget-object v2, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v2, v0}, Lcom/sec/android/app/storycam/AppDataManager;->setCurrentTranscodeElement(Lcom/samsung/app/video/editor/external/TranscodeElement;)V

    goto :goto_0

    .line 62
    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static isSummaryRunning()Z
    .locals 1

    .prologue
    .line 144
    sget-object v0, Lcom/sec/android/app/storycam/summary/Summary;->summaryThread:Lcom/sec/android/app/ve/thread/HybridSummaryThread;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static setAnalysisRequired(Z)V
    .locals 0
    .param p0, "required"    # Z

    .prologue
    .line 37
    if-eqz p0, :cond_0

    .line 38
    invoke-static {}, Lcom/sec/android/app/storycam/summary/Summary;->deinit()V

    .line 39
    :cond_0
    sput-boolean p0, Lcom/sec/android/app/storycam/summary/Summary;->analysisRequired:Z

    .line 40
    return-void
.end method

.method public static showSummary(Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;)V
    .locals 6
    .param p0, "activityContext"    # Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    .prologue
    const/4 v5, 0x0

    .line 161
    sget-object v2, Lcom/sec/android/app/storycam/summary/Summary;->summaryThread:Lcom/sec/android/app/ve/thread/HybridSummaryThread;

    if-eqz v2, :cond_2

    .line 162
    sget-object v2, Lcom/sec/android/app/storycam/summary/Summary;->summaryThread:Lcom/sec/android/app/ve/thread/HybridSummaryThread;

    invoke-virtual {v2}, Lcom/sec/android/app/ve/thread/HybridSummaryThread;->getSummaryTranscode()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v1

    .line 165
    .local v1, "summarizedTElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v2

    if-lez v2, :cond_0

    .line 166
    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementList()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/app/video/editor/external/Element;

    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/Element;->removetransitionEdit()V

    .line 169
    :cond_0
    invoke-static {v1}, Lcom/sec/android/app/storycam/summary/Summary;->updateBGMData(Lcom/samsung/app/video/editor/external/TranscodeElement;)V

    .line 170
    sget-object v2, Lcom/sec/android/app/storycam/summary/Summary;->summaryThread:Lcom/sec/android/app/ve/thread/HybridSummaryThread;

    invoke-virtual {v2}, Lcom/sec/android/app/ve/thread/HybridSummaryThread;->getSummaryTranscode()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v2

    invoke-static {v2, p0}, Lcom/sec/android/app/storycam/summary/Summary;->addCaptionAfterSummary(Lcom/samsung/app/video/editor/external/TranscodeElement;Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;)V

    .line 171
    sget-object v2, Lcom/sec/android/app/storycam/summary/Summary;->summaryThread:Lcom/sec/android/app/ve/thread/HybridSummaryThread;

    invoke-virtual {v2}, Lcom/sec/android/app/ve/thread/HybridSummaryThread;->getSummaryTranscode()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/storycam/summary/Summary;->updateDrawingAfterSummary(Lcom/samsung/app/video/editor/external/TranscodeElement;)V

    .line 172
    sget-object v2, Lcom/sec/android/app/storycam/summary/Summary;->summaryThread:Lcom/sec/android/app/ve/thread/HybridSummaryThread;

    invoke-virtual {v2}, Lcom/sec/android/app/ve/thread/HybridSummaryThread;->getSummaryTranscode()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/storycam/summary/Summary;->updateDefaultKenburns(Lcom/samsung/app/video/editor/external/TranscodeElement;)V

    .line 173
    sget-object v2, Lcom/sec/android/app/storycam/summary/Summary;->summaryThread:Lcom/sec/android/app/ve/thread/HybridSummaryThread;

    invoke-virtual {v2}, Lcom/sec/android/app/ve/thread/HybridSummaryThread;->getSummaryTranscode()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v2

    invoke-static {v2, p0}, Lcom/sec/android/app/storycam/summary/Summary;->updateBgmAfterSummary(Lcom/samsung/app/video/editor/external/TranscodeElement;Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;)V

    .line 174
    invoke-static {}, Lcom/sec/android/app/ve/theme/ThemeDataManager;->getInstance()Lcom/sec/android/app/ve/theme/ThemeDataManager;

    move-result-object v2

    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getAssetManager()Landroid/content/res/AssetManager;

    move-result-object v3

    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getThemeName()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/ve/theme/ThemeDataManager;->getCreditsElement(Landroid/content/res/AssetManager;I)Lcom/samsung/app/video/editor/external/Element;

    move-result-object v0

    .line 175
    .local v0, "credits":Lcom/samsung/app/video/editor/external/Element;
    if-eqz v0, :cond_1

    .line 176
    invoke-virtual {v1, v0, v5}, Lcom/samsung/app/video/editor/external/TranscodeElement;->addElement(Lcom/samsung/app/video/editor/external/Element;Z)V

    .line 177
    :cond_1
    sget-object v2, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    sget-object v3, Lcom/sec/android/app/storycam/summary/Summary;->summaryThread:Lcom/sec/android/app/ve/thread/HybridSummaryThread;

    invoke-virtual {v3}, Lcom/sec/android/app/ve/thread/HybridSummaryThread;->getSummaryTranscode()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/storycam/AppDataManager;->setCurrentTranscodeElement(Lcom/samsung/app/video/editor/external/TranscodeElement;)V

    .line 178
    invoke-static {v5}, Lcom/sec/android/app/storycam/summary/Summary;->setAnalysisRequired(Z)V

    .line 179
    new-instance v2, Ljava/lang/Thread;

    .line 180
    new-instance v3, Lcom/sec/android/app/storycam/summary/Summary$1;

    invoke-direct {v3}, Lcom/sec/android/app/storycam/summary/Summary$1;-><init>()V

    .line 179
    invoke-direct {v2, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 187
    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    .line 190
    .end local v0    # "credits":Lcom/samsung/app/video/editor/external/Element;
    .end local v1    # "summarizedTElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    :cond_2
    const/4 v2, 0x0

    sput-object v2, Lcom/sec/android/app/storycam/summary/Summary;->summaryThread:Lcom/sec/android/app/ve/thread/HybridSummaryThread;

    .line 191
    return-void
.end method

.method public static startSummary(Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;Landroid/os/Handler;)V
    .locals 6
    .param p0, "activityContext"    # Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;
    .param p1, "handler"    # Landroid/os/Handler;

    .prologue
    .line 98
    sget-object v0, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v0}, Lcom/sec/android/app/storycam/AppDataManager;->getCurrentTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v2

    .line 99
    .local v2, "summarized":Lcom/samsung/app/video/editor/external/TranscodeElement;
    if-eqz v2, :cond_1

    .line 110
    :cond_0
    :goto_0
    return-void

    .line 103
    :cond_1
    invoke-static {}, Lcom/sec/android/app/ve/theme/ThemeDataManager;->getInstance()Lcom/sec/android/app/ve/theme/ThemeDataManager;

    move-result-object v1

    move-object v0, p0

    check-cast v0, Landroid/content/Context;

    .line 104
    const/16 v3, 0xd

    .line 103
    invoke-virtual {v1, v0, v3}, Lcom/sec/android/app/ve/theme/ThemeDataManager;->createThemeProject(Landroid/content/Context;I)Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v2

    .line 105
    invoke-static {}, Lcom/sec/android/app/ve/theme/ThemeDataManager;->getInstance()Lcom/sec/android/app/ve/theme/ThemeDataManager;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/ve/VEApp;->gBGMManager:Lcom/sec/android/app/ve/bgm/BGMManager;

    invoke-virtual {v0, v2, v1}, Lcom/sec/android/app/ve/theme/ThemeDataManager;->modifyTranscodeWithBGMTranstionSlots(Lcom/samsung/app/video/editor/external/TranscodeElement;Lcom/sec/android/app/ve/bgm/BGMManager;)V

    .line 106
    invoke-static {p0, v2}, Lcom/sec/android/app/storycam/summary/Summary;->isSummaryRequired(Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;Lcom/samsung/app/video/editor/external/TranscodeElement;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 107
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/sec/android/app/storycam/summary/Summary;->setAnalysisRequired(Z)V

    .line 108
    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/storycam/summary/Summary;->summarize(Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;Landroid/os/Handler;Lcom/samsung/app/video/editor/external/TranscodeElement;ZJ)V

    goto :goto_0
.end method

.method public static stopSummary()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 148
    sget-object v0, Lcom/sec/android/app/storycam/summary/Summary;->summaryThread:Lcom/sec/android/app/ve/thread/HybridSummaryThread;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/app/storycam/summary/Summary;->summaryThread:Lcom/sec/android/app/ve/thread/HybridSummaryThread;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/thread/HybridSummaryThread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 149
    sget-object v0, Lcom/sec/android/app/storycam/summary/Summary;->summaryThread:Lcom/sec/android/app/ve/thread/HybridSummaryThread;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/thread/HybridSummaryThread;->stopSummary()V

    .line 150
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sec/android/app/storycam/summary/Summary;->setAnalysisRequired(Z)V

    .line 153
    :cond_0
    sget-object v0, Lcom/sec/android/app/storycam/summary/Summary;->pd:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_1

    .line 154
    sget-object v0, Lcom/sec/android/app/storycam/summary/Summary;->pd:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 155
    sput-object v1, Lcom/sec/android/app/storycam/summary/Summary;->pd:Landroid/app/ProgressDialog;

    .line 157
    :cond_1
    sput-object v1, Lcom/sec/android/app/storycam/summary/Summary;->summaryThread:Lcom/sec/android/app/ve/thread/HybridSummaryThread;

    .line 158
    return-void
.end method

.method public static summarize(Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;Landroid/os/Handler;Lcom/samsung/app/video/editor/external/TranscodeElement;ZJ)V
    .locals 22
    .param p0, "activityContext"    # Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;
    .param p1, "handler"    # Landroid/os/Handler;
    .param p2, "summaryTranscodeElement"    # Lcom/samsung/app/video/editor/external/TranscodeElement;
    .param p3, "previewPlay"    # Z
    .param p4, "pos"    # J

    .prologue
    .line 114
    const-string v3, "Summary"

    const-string v4, "Start Summary"

    invoke-static {v3, v4}, Lcom/sec/android/app/ve/common/LogUtils;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    sget-object v3, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v3}, Lcom/sec/android/app/storycam/AppDataManager;->getOriginalTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v7

    .line 117
    .local v7, "current":Lcom/samsung/app/video/editor/external/TranscodeElement;
    if-nez v7, :cond_1

    .line 139
    :cond_0
    :goto_0
    return-void

    .line 120
    :cond_1
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-static {v0, v1}, Lcom/sec/android/app/storycam/summary/Summary;->isSummaryRequired(Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;Lcom/samsung/app/video/editor/external/TranscodeElement;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 121
    sget v3, Lcom/sec/android/app/storycam/Utils;->AUTOSUMMARY_DURATION:I

    int-to-long v8, v3

    .line 122
    .local v8, "summaryDuration":J
    if-eqz p2, :cond_0

    move-object/from16 v3, p0

    .line 123
    check-cast v3, Landroid/app/Activity;

    const v4, 0x7f07020e

    move-object/from16 v0, p1

    invoke-static {v3, v0, v4}, Lcom/sec/android/app/ve/util/CommonUtils;->showProgressDialog(Landroid/app/Activity;Landroid/os/Handler;I)Landroid/app/ProgressDialog;

    move-result-object v3

    sput-object v3, Lcom/sec/android/app/storycam/summary/Summary;->pd:Landroid/app/ProgressDialog;

    .line 124
    const-wide/16 v8, 0x0

    .line 125
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementList()Ljava/util/List;

    move-result-object v19

    .line 126
    .local v19, "elementList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v17

    .line 127
    .local v17, "count":I
    const/16 v20, 0x0

    .local v20, "i":I
    :goto_1
    move/from16 v0, v20

    move/from16 v1, v17

    if-lt v0, v1, :cond_2

    .line 133
    new-instance v3, Lcom/sec/android/app/ve/thread/HybridSummaryThread;

    const/16 v5, 0x3eb

    move-object/from16 v6, p0

    .line 134
    check-cast v6, Landroid/content/Context;

    .line 135
    const/4 v10, 0x0

    sget v11, Lcom/samsung/app/video/editor/external/Constants;->AUTOEDIT_MODE_AUTO:I

    sget-boolean v13, Lcom/sec/android/app/storycam/summary/Summary;->analysisRequired:Z

    move-object/from16 v4, p1

    move-object/from16 v12, p2

    move/from16 v14, p3

    move-wide/from16 v15, p4

    .line 133
    invoke-direct/range {v3 .. v16}, Lcom/sec/android/app/ve/thread/HybridSummaryThread;-><init>(Landroid/os/Handler;ILandroid/content/Context;Lcom/samsung/app/video/editor/external/TranscodeElement;JLandroid/os/Bundle;ILcom/samsung/app/video/editor/external/TranscodeElement;ZZJ)V

    sput-object v3, Lcom/sec/android/app/storycam/summary/Summary;->summaryThread:Lcom/sec/android/app/ve/thread/HybridSummaryThread;

    .line 136
    sget-object v3, Lcom/sec/android/app/storycam/summary/Summary;->summaryThread:Lcom/sec/android/app/ve/thread/HybridSummaryThread;

    invoke-virtual {v3}, Lcom/sec/android/app/ve/thread/HybridSummaryThread;->start()V

    goto :goto_0

    .line 128
    :cond_2
    invoke-interface/range {v19 .. v20}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/app/video/editor/external/Element;

    .line 129
    .local v18, "element":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual/range {v18 .. v18}, Lcom/samsung/app/video/editor/external/Element;->getDuration()J

    move-result-wide v3

    add-long/2addr v8, v3

    .line 127
    add-int/lit8 v20, v20, 0x1

    goto :goto_1
.end method

.method private static updateBGMData(Lcom/samsung/app/video/editor/external/TranscodeElement;)V
    .locals 5
    .param p0, "summarizedTElement"    # Lcom/samsung/app/video/editor/external/TranscodeElement;

    .prologue
    const/4 v4, 0x7

    .line 300
    if-nez p0, :cond_1

    .line 313
    :cond_0
    :goto_0
    return-void

    .line 302
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getAudioListCount()I

    move-result v2

    if-lez v2, :cond_0

    .line 303
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getAdditionlAudioEleList()Ljava/util/List;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/app/video/editor/external/Element;

    .line 304
    .local v0, "bgmElement":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getTotalDuration()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/samsung/app/video/editor/external/Element;->setStoryBoardEndTime(J)V

    .line 305
    invoke-virtual {v0, v4}, Lcom/samsung/app/video/editor/external/Element;->getEdit(I)Lcom/samsung/app/video/editor/external/Edit;

    move-result-object v1

    .line 306
    .local v1, "edit":Lcom/samsung/app/video/editor/external/Edit;
    if-nez v1, :cond_0

    .line 307
    new-instance v1, Lcom/samsung/app/video/editor/external/Edit;

    .end local v1    # "edit":Lcom/samsung/app/video/editor/external/Edit;
    invoke-direct {v1}, Lcom/samsung/app/video/editor/external/Edit;-><init>()V

    .line 308
    .restart local v1    # "edit":Lcom/samsung/app/video/editor/external/Edit;
    invoke-virtual {v1, v4}, Lcom/samsung/app/video/editor/external/Edit;->setType(I)V

    .line 309
    const/16 v2, 0x15

    invoke-virtual {v1, v2}, Lcom/samsung/app/video/editor/external/Edit;->setSubType(I)V

    .line 310
    invoke-virtual {v0, v1}, Lcom/samsung/app/video/editor/external/Element;->addEdit(Lcom/samsung/app/video/editor/external/Edit;)V

    goto :goto_0
.end method

.method private static updateBgmAfterSummary(Lcom/samsung/app/video/editor/external/TranscodeElement;Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;)V
    .locals 6
    .param p0, "summaryTranscode"    # Lcom/samsung/app/video/editor/external/TranscodeElement;
    .param p1, "activityContext"    # Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    .prologue
    .line 194
    sget-object v4, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v4}, Lcom/sec/android/app/storycam/AppDataManager;->getCurrentTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v3

    .line 195
    .local v3, "oldTranscodeElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    invoke-interface {p1}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;->isUserSelectedBgm()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 196
    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getAdditionlAudioEleList()Ljava/util/List;

    move-result-object v0

    .line 197
    .local v0, "musicElemList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_0

    .line 198
    const/4 v4, 0x0

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/app/video/editor/external/Element;

    .line 199
    .local v1, "musicElement":Lcom/samsung/app/video/editor/external/Element;
    if-eqz v1, :cond_0

    .line 200
    new-instance v2, Lcom/samsung/app/video/editor/external/Element;

    invoke-direct {v2, v1}, Lcom/samsung/app/video/editor/external/Element;-><init>(Lcom/samsung/app/video/editor/external/Element;)V

    .line 201
    .local v2, "newMusicElement":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getTotalDuration()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/samsung/app/video/editor/external/Element;->setStoryBoardEndTime(J)V

    .line 202
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getAdditionlAudioEleList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->clear()V

    .line 203
    invoke-virtual {p0, v2}, Lcom/samsung/app/video/editor/external/TranscodeElement;->addMusicEleList(Lcom/samsung/app/video/editor/external/Element;)V

    .line 207
    .end local v0    # "musicElemList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    .end local v1    # "musicElement":Lcom/samsung/app/video/editor/external/Element;
    .end local v2    # "newMusicElement":Lcom/samsung/app/video/editor/external/Element;
    :cond_0
    invoke-interface {p1}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;->isBgmStateOn()Z

    move-result v4

    if-nez v4, :cond_1

    .line 208
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->clearBGMMusic()V

    .line 210
    :cond_1
    return-void
.end method

.method private static updateDefaultKenburns(Lcom/samsung/app/video/editor/external/TranscodeElement;)V
    .locals 1
    .param p0, "newTranscodeElement"    # Lcom/samsung/app/video/editor/external/TranscodeElement;

    .prologue
    .line 215
    sget-object v0, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/storycam/AppDataManager;->updateDefaultKenburns(Lcom/samsung/app/video/editor/external/TranscodeElement;)V

    .line 216
    return-void
.end method

.method private static updateDrawingAfterSummary(Lcom/samsung/app/video/editor/external/TranscodeElement;)V
    .locals 12
    .param p0, "newTranscodeElement"    # Lcom/samsung/app/video/editor/external/TranscodeElement;

    .prologue
    const/high16 v11, 0x447a0000    # 1000.0f

    const/high16 v10, 0x41f00000    # 30.0f

    const/4 v8, 0x0

    .line 220
    sget-object v5, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v5}, Lcom/sec/android/app/storycam/AppDataManager;->getCurrentTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v4

    .line 223
    .local v4, "oldTranscodeElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    const/4 v0, 0x0

    .line 224
    .local v0, "beforeSummaryDrawing":Lcom/samsung/app/video/editor/external/ClipartParams;
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getDrawingEleListCount()I

    move-result v5

    if-lez v5, :cond_0

    .line 225
    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getDrawEleList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "beforeSummaryDrawing":Lcom/samsung/app/video/editor/external/ClipartParams;
    check-cast v0, Lcom/samsung/app/video/editor/external/ClipartParams;

    .line 226
    .restart local v0    # "beforeSummaryDrawing":Lcom/samsung/app/video/editor/external/ClipartParams;
    const/4 v4, 0x0

    .line 229
    :cond_0
    if-eqz p0, :cond_1

    if-eqz v0, :cond_1

    .line 230
    new-instance v1, Lcom/samsung/app/video/editor/external/ClipartParams;

    invoke-direct {v1, v0}, Lcom/samsung/app/video/editor/external/ClipartParams;-><init>(Lcom/samsung/app/video/editor/external/ClipartParams;)V

    .line 232
    .local v1, "newDrawClipart":Lcom/samsung/app/video/editor/external/ClipartParams;
    invoke-virtual {p0, v8}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElement(I)Lcom/samsung/app/video/editor/external/Element;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/app/video/editor/external/Element;->getGroupID()I

    move-result v5

    invoke-virtual {v1, v5}, Lcom/samsung/app/video/editor/external/ClipartParams;->setElementID(I)V

    .line 234
    const-wide/16 v6, 0x0

    .line 235
    .local v6, "startTime":J
    invoke-virtual {p0, v8}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElement(I)Lcom/samsung/app/video/editor/external/Element;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/app/video/editor/external/Element;->getDuration()J

    move-result-wide v8

    add-long v2, v6, v8

    .line 238
    .local v2, "endTime":J
    long-to-float v5, v6

    div-float/2addr v5, v11

    mul-float/2addr v5, v10

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    invoke-virtual {v1, v5}, Lcom/samsung/app/video/editor/external/ClipartParams;->setStoryboardStartFrame(I)V

    .line 241
    long-to-float v5, v2

    div-float/2addr v5, v11

    mul-float/2addr v5, v10

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    invoke-virtual {v1, v5}, Lcom/samsung/app/video/editor/external/ClipartParams;->setStoryboardEndFrame(I)V

    .line 244
    invoke-virtual {p0, v1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->addDrawingEleList(Lcom/samsung/app/video/editor/external/ClipartParams;)V

    .line 246
    .end local v1    # "newDrawClipart":Lcom/samsung/app/video/editor/external/ClipartParams;
    .end local v2    # "endTime":J
    .end local v6    # "startTime":J
    :cond_1
    return-void
.end method
