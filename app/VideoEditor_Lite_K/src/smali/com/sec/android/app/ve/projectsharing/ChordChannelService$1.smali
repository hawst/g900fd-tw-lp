.class Lcom/sec/android/app/ve/projectsharing/ChordChannelService$1;
.super Landroid/content/BroadcastReceiver;
.source "ChordChannelService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/ve/projectsharing/ChordChannelService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$1;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    .line 95
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 99
    const-string v4, "BROADCAST_FINISH_CHORD_SERVICE"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 100
    iget-object v4, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$1;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mChordManager:Lcom/samsung/chord/ChordManager;
    invoke-static {v4}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->access$0(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;)Lcom/samsung/chord/ChordManager;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 101
    iget-object v4, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$1;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    # invokes: Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->isWiFiDirectInterfaceAvailable()Z
    invoke-static {v4}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->access$1(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 102
    iget-object v4, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$1;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mHandler:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ServiceHandler;
    invoke-static {v4}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->access$2(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;)Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ServiceHandler;

    move-result-object v4

    const/16 v5, 0x68

    new-instance v6, Ljava/lang/Integer;

    const/4 v7, 0x1

    invoke-direct {v6, v7}, Ljava/lang/Integer;-><init>(I)V

    invoke-static {v4, v5, v6}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    .line 103
    .local v2, "msg":Landroid/os/Message;
    iget-object v4, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$1;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mHandler:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ServiceHandler;
    invoke-static {v4}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->access$2(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;)Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ServiceHandler;

    move-result-object v4

    const-wide/16 v6, 0x32

    invoke-virtual {v4, v2, v6, v7}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ServiceHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 104
    const-string v4, "ProjectSharing"

    const-string v5, "inside mLocalBroadcastReceiver.BROADCAST_FINISH_SERVICE, switching to Wi-Fi Direct"

    invoke-static {v4, v5}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    .end local v2    # "msg":Landroid/os/Message;
    :cond_0
    :goto_0
    return-void

    .line 107
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$1;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    invoke-virtual {v4}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->stopSelf()V

    .line 108
    const-string v4, "ProjectSharing"

    const-string v5, "inside mLocalBroadcastReceiver.BROADCAST_FINISH_SERVICE, stopping ChordChannelService"

    invoke-static {v4, v5}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 112
    :cond_2
    const-string v4, "BROADCAST_START_CHORD_SESSION"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 113
    iget-object v4, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$1;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    # getter for: Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->mSession:Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;
    invoke-static {v4}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->access$3(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;)Lcom/sec/android/app/ve/projectsharing/ChordChannelService$ChordSession;

    move-result-object v4

    if-nez v4, :cond_0

    .line 114
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 115
    .local v0, "bundle":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 117
    const-string v4, "BROADCAST_EXTRA_PEER_CHORD_ID"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 118
    .local v1, "destination":Ljava/lang/String;
    const-string v4, "EXTRA_TO_SHARE_TE"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v3

    check-cast v3, Lcom/samsung/app/video/editor/external/TranscodeElement;

    .line 119
    .local v3, "tElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    if-eqz v1, :cond_3

    if-eqz v3, :cond_3

    .line 120
    const-string v4, "ProjectSharing"

    const-string v5, "PRJ_SHARING_SENDER_STEP 1: Inside ChordChannelService.onReceive, BROADCAST_START_PRJ_SHARING. Starting sharing in BG"

    invoke-static {v4, v5}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    iget-object v4, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$1;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    invoke-virtual {v4, v1, v3}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->startProjectSharingSession(Ljava/lang/String;Lcom/samsung/app/video/editor/external/TranscodeElement;)V

    goto :goto_0

    .line 124
    :cond_3
    const-string v4, "ProjectSharing"

    const-string v5, "PRJ_SHARING_SENDER_STEP 1: Inside ChordChannelService.onReceive, BROADCAST_START_PRJ_SHARING. FAILED coz the bundle didnt have input"

    invoke-static {v4, v5}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
