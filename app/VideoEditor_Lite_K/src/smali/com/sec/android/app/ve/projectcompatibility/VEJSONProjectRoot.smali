.class public Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;
.super Lorg/json/JSONObject;
.source "VEJSONProjectRoot.java"


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 138
    invoke-direct {p0}, Lorg/json/JSONObject;-><init>()V

    .line 139
    return-void
.end method

.method protected constructor <init>(Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;)V
    .locals 9
    .param p1, "source"    # Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;

    .prologue
    .line 88
    invoke-direct {p0}, Lorg/json/JSONObject;-><init>()V

    .line 90
    :try_start_0
    invoke-virtual {p1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;->getTheme()I

    move-result v7

    invoke-virtual {p0, v7}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;->setTheme(I)V

    .line 91
    invoke-virtual {p1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;->getProjectDuration()I

    move-result v7

    invoke-virtual {p0, v7}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;->setProjectDuration(I)V

    .line 92
    invoke-virtual {p1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;->getProjectName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;->setProjectName(Ljava/lang/String;)V

    .line 94
    invoke-virtual {p1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;->getMediaElementList()Lorg/json/JSONArray;

    move-result-object v3

    .line 95
    .local v3, "elementList":Lorg/json/JSONArray;
    if-eqz v3, :cond_0

    .line 96
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v7

    if-lt v4, v7, :cond_5

    .line 101
    .end local v4    # "i":I
    :cond_0
    invoke-virtual {p1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;->getBGMAudioElementList()Lorg/json/JSONArray;

    move-result-object v0

    .line 102
    .local v0, "bgmList":Lorg/json/JSONArray;
    if-eqz v0, :cond_1

    .line 103
    const/4 v4, 0x0

    .restart local v4    # "i":I
    :goto_1
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v7

    if-lt v4, v7, :cond_6

    .line 108
    .end local v4    # "i":I
    :cond_1
    invoke-virtual {p1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;->getSoundEffectAudioElementList()Lorg/json/JSONArray;

    move-result-object v5

    .line 109
    .local v5, "soundEffectList":Lorg/json/JSONArray;
    if-eqz v5, :cond_2

    .line 110
    const/4 v4, 0x0

    .restart local v4    # "i":I
    :goto_2
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v7

    if-lt v4, v7, :cond_7

    .line 115
    .end local v4    # "i":I
    :cond_2
    invoke-virtual {p1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;->getVoiceOverAudioElementList()Lorg/json/JSONArray;

    move-result-object v6

    .line 116
    .local v6, "voiceOverList":Lorg/json/JSONArray;
    if-eqz v6, :cond_3

    .line 117
    const/4 v4, 0x0

    .restart local v4    # "i":I
    :goto_3
    invoke-virtual {v6}, Lorg/json/JSONArray;->length()I

    move-result v7

    if-lt v4, v7, :cond_8

    .line 122
    .end local v4    # "i":I
    :cond_3
    invoke-virtual {p1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;->getCaptionList()Lorg/json/JSONArray;

    move-result-object v1

    .line 123
    .local v1, "captionList":Lorg/json/JSONArray;
    if-eqz v1, :cond_4

    .line 124
    const/4 v4, 0x0

    .restart local v4    # "i":I
    :goto_4
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v7

    if-lt v4, v7, :cond_9

    .line 131
    .end local v0    # "bgmList":Lorg/json/JSONArray;
    .end local v1    # "captionList":Lorg/json/JSONArray;
    .end local v3    # "elementList":Lorg/json/JSONArray;
    .end local v4    # "i":I
    .end local v5    # "soundEffectList":Lorg/json/JSONArray;
    .end local v6    # "voiceOverList":Lorg/json/JSONArray;
    :cond_4
    :goto_5
    return-void

    .line 97
    .restart local v3    # "elementList":Lorg/json/JSONArray;
    .restart local v4    # "i":I
    :cond_5
    new-instance v7, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;

    invoke-virtual {v3, v4}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;-><init>(Lorg/json/JSONObject;)V

    invoke-virtual {p0, v7}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;->addMediaElement(Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;)V

    .line 96
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 104
    .restart local v0    # "bgmList":Lorg/json/JSONArray;
    :cond_6
    new-instance v7, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioElement;

    invoke-virtual {v0, v4}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioElement;-><init>(Lorg/json/JSONObject;)V

    invoke-virtual {p0, v7}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;->addBGMAudioElement(Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioElement;)V

    .line 103
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 111
    .restart local v5    # "soundEffectList":Lorg/json/JSONArray;
    :cond_7
    new-instance v7, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioElement;

    invoke-virtual {v5, v4}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioElement;-><init>(Lorg/json/JSONObject;)V

    invoke-virtual {p0, v7}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;->addSoundEffectAudioElement(Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioElement;)V

    .line 110
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 118
    .restart local v6    # "voiceOverList":Lorg/json/JSONArray;
    :cond_8
    new-instance v7, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioElement;

    invoke-virtual {v6, v4}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioElement;-><init>(Lorg/json/JSONObject;)V

    invoke-virtual {p0, v7}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;->addVoiceOverAudioElement(Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioElement;)V

    .line 117
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 125
    .restart local v1    # "captionList":Lorg/json/JSONArray;
    :cond_9
    new-instance v7, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;

    invoke-virtual {v1, v4}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;-><init>(Lorg/json/JSONObject;)V

    invoke-virtual {p0, v7}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;->addCaption(Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 124
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    .line 128
    .end local v0    # "bgmList":Lorg/json/JSONArray;
    .end local v1    # "captionList":Lorg/json/JSONArray;
    .end local v3    # "elementList":Lorg/json/JSONArray;
    .end local v4    # "i":I
    .end local v5    # "soundEffectList":Lorg/json/JSONArray;
    .end local v6    # "voiceOverList":Lorg/json/JSONArray;
    :catch_0
    move-exception v2

    .line 129
    .local v2, "e":Lorg/json/JSONException;
    invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_5
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "projectInText"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 85
    invoke-direct {p0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 86
    return-void
.end method

.method private addAudioElement(Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioElement;Ljava/lang/String;)V
    .locals 3
    .param p1, "audioElement"    # Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioElement;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 142
    const/4 v0, 0x0

    .line 144
    .local v0, "audioList":Lorg/json/JSONArray;
    :try_start_0
    invoke-virtual {p0, p2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 155
    :goto_0
    invoke-virtual {v0, p1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 156
    return-void

    .line 146
    :catch_0
    move-exception v2

    .line 147
    .local v2, "jException":Lorg/json/JSONException;
    new-instance v0, Lorg/json/JSONArray;

    .end local v0    # "audioList":Lorg/json/JSONArray;
    invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V

    .line 149
    .restart local v0    # "audioList":Lorg/json/JSONArray;
    :try_start_1
    invoke-virtual {p0, p2, v0}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 151
    :catch_1
    move-exception v1

    .line 152
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public addBGMAudioElement(Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioElement;)V
    .locals 1
    .param p1, "audioElement"    # Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioElement;

    .prologue
    .line 317
    const-string v0, "BGM_ARRAY"

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;->addAudioElement(Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioElement;Ljava/lang/String;)V

    .line 318
    return-void
.end method

.method public addCaption(Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;)V
    .locals 4
    .param p1, "caption"    # Lcom/sec/android/app/ve/projectcompatibility/VEJSONCaption;

    .prologue
    .line 392
    const/4 v0, 0x0

    .line 394
    .local v0, "captionList":Lorg/json/JSONArray;
    :try_start_0
    const-string v3, "CAPTION_LIST"

    invoke-virtual {p0, v3}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 405
    :goto_0
    invoke-virtual {v0, p1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 406
    return-void

    .line 396
    :catch_0
    move-exception v2

    .line 397
    .local v2, "ex":Lorg/json/JSONException;
    new-instance v0, Lorg/json/JSONArray;

    .end local v0    # "captionList":Lorg/json/JSONArray;
    invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V

    .line 399
    .restart local v0    # "captionList":Lorg/json/JSONArray;
    :try_start_1
    const-string v3, "CAPTION_LIST"

    invoke-virtual {p0, v3, v0}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 401
    :catch_1
    move-exception v1

    .line 402
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public addMediaElement(Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;)V
    .locals 4
    .param p1, "element"    # Lcom/sec/android/app/ve/projectcompatibility/VEJSONMediaElement;

    .prologue
    .line 279
    const/4 v2, 0x0

    .line 281
    .local v2, "mediaList":Lorg/json/JSONArray;
    :try_start_0
    const-string v3, "ELEMENT_ARRAY"

    invoke-virtual {p0, v3}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 292
    :goto_0
    invoke-virtual {v2, p1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 293
    return-void

    .line 283
    :catch_0
    move-exception v1

    .line 284
    .local v1, "jException":Lorg/json/JSONException;
    new-instance v2, Lorg/json/JSONArray;

    .end local v2    # "mediaList":Lorg/json/JSONArray;
    invoke-direct {v2}, Lorg/json/JSONArray;-><init>()V

    .line 286
    .restart local v2    # "mediaList":Lorg/json/JSONArray;
    :try_start_1
    const-string v3, "ELEMENT_ARRAY"

    invoke-virtual {p0, v3, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 288
    :catch_1
    move-exception v0

    .line 289
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public addSoundEffectAudioElement(Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioElement;)V
    .locals 1
    .param p1, "audioElement"    # Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioElement;

    .prologue
    .line 342
    const-string v0, "SOUND_ARRAY"

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;->addAudioElement(Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioElement;Ljava/lang/String;)V

    .line 343
    return-void
.end method

.method public addVoiceOverAudioElement(Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioElement;)V
    .locals 1
    .param p1, "audioElement"    # Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioElement;

    .prologue
    .line 367
    const-string v0, "VOICEOVER_ARRAY"

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;->addAudioElement(Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioElement;Ljava/lang/String;)V

    .line 368
    return-void
.end method

.method public getBGMAudioElementList()Lorg/json/JSONArray;
    .locals 3

    .prologue
    .line 325
    const/4 v1, 0x0

    .line 327
    .local v1, "list":Lorg/json/JSONArray;
    :try_start_0
    const-string v2, "BGM_ARRAY"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 332
    :goto_0
    return-object v1

    .line 329
    :catch_0
    move-exception v0

    .line 330
    .local v0, "jException":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getCaptionList()Lorg/json/JSONArray;
    .locals 3

    .prologue
    .line 413
    const/4 v1, 0x0

    .line 415
    .local v1, "list":Lorg/json/JSONArray;
    :try_start_0
    const-string v2, "CAPTION_LIST"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 420
    :goto_0
    return-object v1

    .line 417
    :catch_0
    move-exception v0

    .line 418
    .local v0, "jException":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getMediaElementList()Lorg/json/JSONArray;
    .locals 3

    .prologue
    .line 300
    const/4 v1, 0x0

    .line 302
    .local v1, "list":Lorg/json/JSONArray;
    :try_start_0
    const-string v2, "ELEMENT_ARRAY"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 307
    :goto_0
    return-object v1

    .line 304
    :catch_0
    move-exception v0

    .line 305
    .local v0, "jException":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getProjectDuration()I
    .locals 3

    .prologue
    .line 205
    const/4 v0, 0x0

    .line 207
    .local v0, "duration":I
    :try_start_0
    const-string v2, "PROJECT_DURATION"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;->getInt(Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 212
    :goto_0
    return v0

    .line 209
    :catch_0
    move-exception v1

    .line 210
    .local v1, "jException":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getProjectName()Ljava/lang/String;
    .locals 3

    .prologue
    .line 233
    const/4 v1, 0x0

    .line 235
    .local v1, "name":Ljava/lang/String;
    :try_start_0
    const-string v2, "PROJECT_NAME"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 240
    :goto_0
    return-object v1

    .line 237
    :catch_0
    move-exception v0

    .line 238
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getSoundEffectAudioElementList()Lorg/json/JSONArray;
    .locals 3

    .prologue
    .line 350
    const/4 v1, 0x0

    .line 352
    .local v1, "list":Lorg/json/JSONArray;
    :try_start_0
    const-string v2, "SOUND_ARRAY"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 357
    :goto_0
    return-object v1

    .line 354
    :catch_0
    move-exception v0

    .line 355
    .local v0, "jException":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getTheme()I
    .locals 3

    .prologue
    .line 177
    const/4 v1, 0x0

    .line 179
    .local v1, "theme":I
    :try_start_0
    const-string v2, "THEME_NAME"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;->getInt(Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 184
    :goto_0
    return v1

    .line 181
    :catch_0
    move-exception v0

    .line 182
    .local v0, "jException":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getUniqueProjectID()Ljava/lang/String;
    .locals 3

    .prologue
    .line 262
    const/4 v1, 0x0

    .line 264
    .local v1, "uniqueID":Ljava/lang/String;
    :try_start_0
    const-string v2, "PROJECT_UNIQUE_ID"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 269
    :goto_0
    return-object v1

    .line 266
    :catch_0
    move-exception v0

    .line 267
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getVoiceOverAudioElementList()Lorg/json/JSONArray;
    .locals 3

    .prologue
    .line 376
    const/4 v1, 0x0

    .line 378
    .local v1, "list":Lorg/json/JSONArray;
    :try_start_0
    const-string v2, "VOICEOVER_ARRAY"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 383
    :goto_0
    return-object v1

    .line 380
    :catch_0
    move-exception v0

    .line 381
    .local v0, "jException":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public setProjectDuration(I)V
    .locals 2
    .param p1, "duration"    # I

    .prologue
    .line 193
    :try_start_0
    const-string v1, "PROJECT_DURATION"

    invoke-virtual {p0, v1, p1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 198
    :goto_0
    return-void

    .line 195
    :catch_0
    move-exception v0

    .line 196
    .local v0, "jException":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public setProjectName(Ljava/lang/String;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 221
    :try_start_0
    const-string v1, "PROJECT_NAME"

    invoke-virtual {p0, v1, p1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 226
    :goto_0
    return-void

    .line 223
    :catch_0
    move-exception v0

    .line 224
    .local v0, "jException":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public setTheme(I)V
    .locals 2
    .param p1, "themeName"    # I

    .prologue
    .line 165
    :try_start_0
    const-string v1, "THEME_NAME"

    invoke-virtual {p0, v1, p1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 170
    :goto_0
    return-void

    .line 167
    :catch_0
    move-exception v0

    .line 168
    .local v0, "jException":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public setUniqueProjectID(Ljava/lang/String;)V
    .locals 2
    .param p1, "uniqueID"    # Ljava/lang/String;

    .prologue
    .line 250
    :try_start_0
    const-string v1, "PROJECT_UNIQUE_ID"

    invoke-virtual {p0, v1, p1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONProjectRoot;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 255
    :goto_0
    return-void

    .line 252
    :catch_0
    move-exception v0

    .line 253
    .local v0, "jException":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method
