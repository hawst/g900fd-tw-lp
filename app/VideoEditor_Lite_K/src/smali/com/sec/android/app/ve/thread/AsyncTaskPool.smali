.class public Lcom/sec/android/app/ve/thread/AsyncTaskPool;
.super Ljava/lang/Object;
.source "AsyncTaskPool.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/ve/thread/AsyncTaskPool$Listener;
    }
.end annotation


# static fields
.field public static final DEF_MAX_TASK_COUNT:I = 0xa


# instance fields
.field private final mCheckAgainHandler:Landroid/os/Handler;

.field private final mCount:I

.field private mListener:Lcom/sec/android/app/ve/thread/AsyncTaskPool$Listener;

.field private final mRunningTasks:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/ve/thread/SimpleTask;",
            ">;"
        }
    .end annotation
.end field

.field private final mTaskQueue:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/ve/thread/SimpleTask;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1, "count"    # I

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/ve/thread/AsyncTaskPool;->mTaskQueue:Ljava/util/ArrayList;

    .line 162
    new-instance v0, Lcom/sec/android/app/ve/thread/AsyncTaskPool$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/ve/thread/AsyncTaskPool$1;-><init>(Lcom/sec/android/app/ve/thread/AsyncTaskPool;)V

    iput-object v0, p0, Lcom/sec/android/app/ve/thread/AsyncTaskPool;->mCheckAgainHandler:Landroid/os/Handler;

    .line 19
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/ve/thread/AsyncTaskPool;->mRunningTasks:Ljava/util/ArrayList;

    .line 20
    iput p1, p0, Lcom/sec/android/app/ve/thread/AsyncTaskPool;->mCount:I

    .line 21
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/app/ve/thread/AsyncTaskPool;)V
    .locals 0

    .prologue
    .line 104
    invoke-direct {p0}, Lcom/sec/android/app/ve/thread/AsyncTaskPool;->runIfPossible()V

    return-void
.end method

.method private getDuplicateTask(Lcom/sec/android/app/ve/thread/SimpleTask;)Lcom/sec/android/app/ve/thread/SimpleTask;
    .locals 7
    .param p1, "task"    # Lcom/sec/android/app/ve/thread/SimpleTask;

    .prologue
    const/4 v5, 0x0

    .line 144
    iget-object v6, p0, Lcom/sec/android/app/ve/thread/AsyncTaskPool;->mTaskQueue:Ljava/util/ArrayList;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/sec/android/app/ve/thread/AsyncTaskPool;->mRunningTasks:Ljava/util/ArrayList;

    if-nez v6, :cond_2

    :cond_0
    move-object v0, v5

    .line 159
    :cond_1
    :goto_0
    return-object v0

    .line 146
    :cond_2
    iget-object v6, p0, Lcom/sec/android/app/ve/thread/AsyncTaskPool;->mRunningTasks:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 147
    .local v1, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-lt v2, v1, :cond_3

    .line 152
    iget-object v6, p0, Lcom/sec/android/app/ve/thread/AsyncTaskPool;->mTaskQueue:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 153
    .local v4, "taskCount":I
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_2
    if-lt v3, v4, :cond_4

    move-object v0, v5

    .line 159
    goto :goto_0

    .line 148
    .end local v3    # "j":I
    .end local v4    # "taskCount":I
    :cond_3
    iget-object v6, p0, Lcom/sec/android/app/ve/thread/AsyncTaskPool;->mRunningTasks:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/ve/thread/SimpleTask;

    .line 149
    .local v0, "addedTask":Lcom/sec/android/app/ve/thread/SimpleTask;
    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 147
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 154
    .end local v0    # "addedTask":Lcom/sec/android/app/ve/thread/SimpleTask;
    .restart local v3    # "j":I
    .restart local v4    # "taskCount":I
    :cond_4
    iget-object v6, p0, Lcom/sec/android/app/ve/thread/AsyncTaskPool;->mTaskQueue:Ljava/util/ArrayList;

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/ve/thread/SimpleTask;

    .line 155
    .restart local v0    # "addedTask":Lcom/sec/android/app/ve/thread/SimpleTask;
    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 153
    add-int/lit8 v3, v3, 0x1

    goto :goto_2
.end method

.method private moveATaskToRunState()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 130
    iget-object v2, p0, Lcom/sec/android/app/ve/thread/AsyncTaskPool;->mTaskQueue:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_0

    .line 140
    :goto_0
    return v1

    .line 133
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/ve/thread/AsyncTaskPool;->mTaskQueue:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/ve/thread/SimpleTask;

    .line 135
    .local v0, "task":Lcom/sec/android/app/ve/thread/SimpleTask;
    iget-object v1, p0, Lcom/sec/android/app/ve/thread/AsyncTaskPool;->mRunningTasks:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 136
    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/ve/thread/SimpleTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 138
    iget-object v1, p0, Lcom/sec/android/app/ve/thread/AsyncTaskPool;->mTaskQueue:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 140
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private runIfPossible()V
    .locals 9

    .prologue
    const/16 v8, 0xc9

    .line 106
    const/4 v1, 0x0

    .line 107
    .local v1, "checkAgain":Z
    iget-object v5, p0, Lcom/sec/android/app/ve/thread/AsyncTaskPool;->mRunningTasks:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    iget v6, p0, Lcom/sec/android/app/ve/thread/AsyncTaskPool;->mCount:I

    if-ge v5, v6, :cond_0

    .line 108
    invoke-direct {p0}, Lcom/sec/android/app/ve/thread/AsyncTaskPool;->moveATaskToRunState()Z

    move-result v1

    .line 109
    :cond_0
    iget-object v5, p0, Lcom/sec/android/app/ve/thread/AsyncTaskPool;->mRunningTasks:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    add-int/lit8 v2, v5, -0x1

    .line 110
    .local v2, "count":I
    move v3, v2

    .local v3, "i":I
    :goto_0
    if-gez v3, :cond_2

    .line 120
    if-eqz v1, :cond_1

    .line 122
    const/16 v0, 0xc9

    .line 123
    .local v0, "MSG_CHECK_AGAIN":I
    iget-object v5, p0, Lcom/sec/android/app/ve/thread/AsyncTaskPool;->mCheckAgainHandler:Landroid/os/Handler;

    invoke-virtual {v5, v8}, Landroid/os/Handler;->removeMessages(I)V

    .line 124
    iget-object v5, p0, Lcom/sec/android/app/ve/thread/AsyncTaskPool;->mCheckAgainHandler:Landroid/os/Handler;

    const-wide/16 v6, 0x64

    invoke-virtual {v5, v8, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 126
    .end local v0    # "MSG_CHECK_AGAIN":I
    :cond_1
    return-void

    .line 112
    :cond_2
    iget-object v5, p0, Lcom/sec/android/app/ve/thread/AsyncTaskPool;->mRunningTasks:Ljava/util/ArrayList;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/ve/thread/SimpleTask;

    .line 113
    .local v4, "runTask":Lcom/sec/android/app/ve/thread/SimpleTask;
    sget-object v5, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    invoke-virtual {v4}, Lcom/sec/android/app/ve/thread/SimpleTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v6

    if-ne v5, v6, :cond_3

    .line 115
    iget-object v5, p0, Lcom/sec/android/app/ve/thread/AsyncTaskPool;->mRunningTasks:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 116
    const/4 v1, 0x1

    .line 110
    :cond_3
    add-int/lit8 v3, v3, -0x1

    goto :goto_0
.end method


# virtual methods
.method public addTask(Lcom/sec/android/app/ve/thread/SimpleTask;)Lcom/sec/android/app/ve/thread/SimpleTask;
    .locals 1
    .param p1, "task"    # Lcom/sec/android/app/ve/thread/SimpleTask;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/android/app/ve/thread/AsyncTaskPool;->mTaskQueue:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 26
    invoke-direct {p0}, Lcom/sec/android/app/ve/thread/AsyncTaskPool;->runIfPossible()V

    .line 27
    return-object p1
.end method

.method public addUniqueTask(Lcom/sec/android/app/ve/thread/SimpleTask;)Lcom/sec/android/app/ve/thread/SimpleTask;
    .locals 1
    .param p1, "task"    # Lcom/sec/android/app/ve/thread/SimpleTask;

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/sec/android/app/ve/thread/AsyncTaskPool;->getDuplicateTask(Lcom/sec/android/app/ve/thread/SimpleTask;)Lcom/sec/android/app/ve/thread/SimpleTask;

    move-result-object v0

    .line 40
    .local v0, "duplicateTask":Lcom/sec/android/app/ve/thread/SimpleTask;
    if-nez v0, :cond_0

    .line 41
    invoke-virtual {p0, p1}, Lcom/sec/android/app/ve/thread/AsyncTaskPool;->addTask(Lcom/sec/android/app/ve/thread/SimpleTask;)Lcom/sec/android/app/ve/thread/SimpleTask;

    move-result-object v0

    .line 43
    :cond_0
    return-object v0
.end method

.method public addUniqueTaskToTop(Lcom/sec/android/app/ve/thread/SimpleTask;)Lcom/sec/android/app/ve/thread/SimpleTask;
    .locals 3
    .param p1, "task"    # Lcom/sec/android/app/ve/thread/SimpleTask;

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/sec/android/app/ve/thread/AsyncTaskPool;->getDuplicateTask(Lcom/sec/android/app/ve/thread/SimpleTask;)Lcom/sec/android/app/ve/thread/SimpleTask;

    move-result-object v0

    .line 48
    .local v0, "duplicateTask":Lcom/sec/android/app/ve/thread/SimpleTask;
    if-nez v0, :cond_0

    .line 49
    iget-object v1, p0, Lcom/sec/android/app/ve/thread/AsyncTaskPool;->mTaskQueue:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 50
    invoke-direct {p0}, Lcom/sec/android/app/ve/thread/AsyncTaskPool;->runIfPossible()V

    .line 52
    :cond_0
    return-object v0
.end method

.method public clearOrCancelTasks()V
    .locals 5

    .prologue
    .line 74
    iget-object v3, p0, Lcom/sec/android/app/ve/thread/AsyncTaskPool;->mRunningTasks:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v0, v3, -0x1

    .line 75
    .local v0, "count":I
    move v1, v0

    .local v1, "i":I
    :goto_0
    if-gez v1, :cond_0

    .line 82
    iget-object v3, p0, Lcom/sec/android/app/ve/thread/AsyncTaskPool;->mTaskQueue:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 83
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 84
    return-void

    .line 77
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/ve/thread/AsyncTaskPool;->mRunningTasks:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/ve/thread/SimpleTask;

    .line 78
    .local v2, "task":Lcom/sec/android/app/ve/thread/SimpleTask;
    sget-object v3, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    invoke-virtual {v2}, Lcom/sec/android/app/ve/thread/SimpleTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v4

    if-ne v3, v4, :cond_1

    .line 79
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/sec/android/app/ve/thread/SimpleTask;->cancel(Z)Z

    .line 80
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/ve/thread/AsyncTaskPool;->mRunningTasks:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 75
    add-int/lit8 v1, v1, -0x1

    goto :goto_0
.end method

.method public isAllTasksCompleted()Z
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/ve/thread/AsyncTaskPool;->mRunningTasks:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/ve/thread/AsyncTaskPool;->mTaskQueue:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onTaskFinished(Lcom/sec/android/app/ve/thread/SimpleTask;)V
    .locals 1
    .param p1, "task"    # Lcom/sec/android/app/ve/thread/SimpleTask;

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/android/app/ve/thread/AsyncTaskPool;->mRunningTasks:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 89
    invoke-direct {p0}, Lcom/sec/android/app/ve/thread/AsyncTaskPool;->runIfPossible()V

    .line 91
    iget-object v0, p0, Lcom/sec/android/app/ve/thread/AsyncTaskPool;->mRunningTasks:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/sec/android/app/ve/thread/AsyncTaskPool;->mTaskQueue:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 93
    iget-object v0, p0, Lcom/sec/android/app/ve/thread/AsyncTaskPool;->mListener:Lcom/sec/android/app/ve/thread/AsyncTaskPool$Listener;

    if-eqz v0, :cond_0

    .line 95
    iget-object v0, p0, Lcom/sec/android/app/ve/thread/AsyncTaskPool;->mListener:Lcom/sec/android/app/ve/thread/AsyncTaskPool$Listener;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/thread/AsyncTaskPool$Listener;->allPendingTasksCompleted()V

    .line 97
    :cond_0
    return-void
.end method

.method public removeTaskIfPending(Lcom/sec/android/app/ve/thread/SimpleTask;)Z
    .locals 4
    .param p1, "task"    # Lcom/sec/android/app/ve/thread/SimpleTask;

    .prologue
    .line 57
    const/4 v2, 0x0

    .line 59
    .local v2, "res":Z
    iget-object v3, p0, Lcom/sec/android/app/ve/thread/AsyncTaskPool;->mTaskQueue:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v0, v3, -0x1

    .line 60
    .local v0, "count":I
    move v1, v0

    .local v1, "i":I
    :goto_0
    if-gez v1, :cond_0

    .line 69
    return v2

    .line 62
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/ve/thread/AsyncTaskPool;->mTaskQueue:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/ve/thread/SimpleTask;

    invoke-virtual {v3, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 64
    iget-object v3, p0, Lcom/sec/android/app/ve/thread/AsyncTaskPool;->mTaskQueue:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 65
    const/4 v2, 0x1

    .line 60
    :cond_1
    add-int/lit8 v1, v1, -0x1

    goto :goto_0
.end method

.method public setListener(Lcom/sec/android/app/ve/thread/AsyncTaskPool$Listener;)V
    .locals 0
    .param p1, "lstnr"    # Lcom/sec/android/app/ve/thread/AsyncTaskPool$Listener;

    .prologue
    .line 101
    iput-object p1, p0, Lcom/sec/android/app/ve/thread/AsyncTaskPool;->mListener:Lcom/sec/android/app/ve/thread/AsyncTaskPool$Listener;

    .line 102
    return-void
.end method
