.class Lcom/sec/android/app/storycam/AppDataManager$MediaFile;
.super Ljava/lang/Object;
.source "AppDataManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/storycam/AppDataManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MediaFile"
.end annotation


# instance fields
.field private path:Ljava/lang/String;

.field private type:I

.field private valid:Z


# direct methods
.method private constructor <init>(Ljava/lang/String;IZ)V
    .locals 1
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "type"    # I
    .param p3, "valid"    # Z

    .prologue
    .line 636
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 635
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/storycam/AppDataManager$MediaFile;->valid:Z

    .line 637
    iput-object p1, p0, Lcom/sec/android/app/storycam/AppDataManager$MediaFile;->path:Ljava/lang/String;

    .line 638
    iput p2, p0, Lcom/sec/android/app/storycam/AppDataManager$MediaFile;->type:I

    .line 639
    iput-boolean p3, p0, Lcom/sec/android/app/storycam/AppDataManager$MediaFile;->valid:Z

    .line 640
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IZLcom/sec/android/app/storycam/AppDataManager$MediaFile;)V
    .locals 0

    .prologue
    .line 636
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/storycam/AppDataManager$MediaFile;-><init>(Ljava/lang/String;IZ)V

    return-void
.end method


# virtual methods
.method public getPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 645
    iget-object v0, p0, Lcom/sec/android/app/storycam/AppDataManager$MediaFile;->path:Ljava/lang/String;

    return-object v0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 652
    iget v0, p0, Lcom/sec/android/app/storycam/AppDataManager$MediaFile;->type:I

    return v0
.end method

.method public isValid()Z
    .locals 1

    .prologue
    .line 657
    iget-boolean v0, p0, Lcom/sec/android/app/storycam/AppDataManager$MediaFile;->valid:Z

    return v0
.end method
