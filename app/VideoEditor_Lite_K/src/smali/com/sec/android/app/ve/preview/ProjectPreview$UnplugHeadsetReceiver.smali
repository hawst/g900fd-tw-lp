.class public Lcom/sec/android/app/ve/preview/ProjectPreview$UnplugHeadsetReceiver;
.super Landroid/content/BroadcastReceiver;
.source "ProjectPreview.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/ve/preview/ProjectPreview;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "UnplugHeadsetReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/ve/preview/ProjectPreview;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/ve/preview/ProjectPreview;)V
    .locals 0

    .prologue
    .line 85
    iput-object p1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview$UnplugHeadsetReceiver;->this$0:Lcom/sec/android/app/ve/preview/ProjectPreview;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 89
    const-string v0, "android.media.AUDIO_BECOMING_NOISY"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 90
    const-string v0, "PLAYER_STATE"

    const-string v1, "<<<<<<<<<<<<<<<<<<<<<<< UnplugHeadsetReceiver received ACTION_AUDIO_BECOMING_NOISY intent, so stopping play >>>>>>>>>>>>>>>>>>>>>>> "

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview$UnplugHeadsetReceiver;->this$0:Lcom/sec/android/app/ve/preview/ProjectPreview;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/preview/ProjectPreview;->stop()V

    .line 93
    :cond_0
    return-void
.end method
