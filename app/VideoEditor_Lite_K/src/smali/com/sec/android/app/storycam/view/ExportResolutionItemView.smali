.class public Lcom/sec/android/app/storycam/view/ExportResolutionItemView;
.super Landroid/widget/RelativeLayout;
.source "ExportResolutionItemView.java"

# interfaces
.implements Landroid/widget/Checkable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/storycam/view/ExportResolutionItemView$CustomRadioButton;
    }
.end annotation


# instance fields
.field private mRadioBtn:Lcom/sec/android/app/storycam/view/ExportResolutionItemView$CustomRadioButton;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 22
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 23
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 27
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 31
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 32
    return-void
.end method


# virtual methods
.method public isChecked()Z
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x0

    return v0
.end method

.method public onFinishInflate()V
    .locals 1

    .prologue
    .line 36
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 37
    const v0, 0x7f0d0023

    invoke-virtual {p0, v0}, Lcom/sec/android/app/storycam/view/ExportResolutionItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/storycam/view/ExportResolutionItemView$CustomRadioButton;

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/ExportResolutionItemView;->mRadioBtn:Lcom/sec/android/app/storycam/view/ExportResolutionItemView$CustomRadioButton;

    .line 38
    return-void
.end method

.method public setChecked(Z)V
    .locals 1
    .param p1, "checked"    # Z

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/ExportResolutionItemView;->mRadioBtn:Lcom/sec/android/app/storycam/view/ExportResolutionItemView$CustomRadioButton;

    if-eqz v0, :cond_0

    .line 48
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/ExportResolutionItemView;->mRadioBtn:Lcom/sec/android/app/storycam/view/ExportResolutionItemView$CustomRadioButton;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/storycam/view/ExportResolutionItemView$CustomRadioButton;->setChecked(Z)V

    .line 50
    :cond_0
    return-void
.end method

.method public toggle()V
    .locals 0

    .prologue
    .line 55
    return-void
.end method
