.class public Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog;
.super Lcom/sec/android/app/ve/common/AndroidUtils$DlgFragment;
.source "VideoEditorLiteActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ExportResDialog"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog$ExportResolutionAdapter;
    }
.end annotation


# instance fields
.field private exportResOptionsText:[Ljava/lang/String;

.field private exportResOptionsTextDesc:[Ljava/lang/String;

.field private m2Model:I

.field private selectedResIndex:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1050
    invoke-direct {p0}, Lcom/sec/android/app/ve/common/AndroidUtils$DlgFragment;-><init>()V

    .line 1052
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog;->selectedResIndex:I

    .line 1053
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog;->m2Model:I

    .line 1050
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 1054
    iget-object v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog;->exportResOptionsText:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 1055
    iget-object v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog;->exportResOptionsTextDesc:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog;I)V
    .locals 0

    .prologue
    .line 1052
    iput p1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog;->selectedResIndex:I

    return-void
.end method

.method static synthetic access$3(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog;)I
    .locals 1

    .prologue
    .line 1052
    iget v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog;->selectedResIndex:I

    return v0
.end method

.method static synthetic access$4(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog;)I
    .locals 1

    .prologue
    .line 1053
    iget v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog;->m2Model:I

    return v0
.end method

.method public static newInstance(J)Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog;
    .locals 4
    .param p0, "totalDuration"    # J

    .prologue
    .line 1058
    new-instance v1, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog;

    invoke-direct {v1}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog;-><init>()V

    .line 1059
    .local v1, "frag":Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1060
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "totalDuration"

    invoke-virtual {v0, v2, p0, p1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 1061
    invoke-virtual {v1, v0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog;->setArguments(Landroid/os/Bundle;)V

    .line 1062
    return-object v1
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 1067
    invoke-super {p0, p1}, Lcom/sec/android/app/ve/common/AndroidUtils$DlgFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1068
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p1, Landroid/content/res/Configuration;->fontScale:F

    # getter for: Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->fontScale_value:F
    invoke-static {}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->access$7()F

    move-result v1

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    .line 1069
    iget v0, p1, Landroid/content/res/Configuration;->fontScale:F

    invoke-static {v0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->access$8(F)V

    .line 1070
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/ve/common/AndroidUtils;->dismissDialog(Landroid/app/Activity;)V

    .line 1073
    :cond_0
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 13
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v8, 0x0

    const/4 v9, 0x1

    .line 1079
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v5

    const-string v10, "totalDuration"

    invoke-virtual {v5, v10}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v6

    .line 1080
    .local v6, "totalDuration":J
    const-string v5, "ro.product.device"

    invoke-static {v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1082
    .local v4, "modelName":Ljava/lang/String;
    if-eqz v4, :cond_0

    const-string v5, "m2"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    :cond_0
    move v5, v9

    :goto_0
    iput v5, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog;->m2Model:I

    .line 1083
    iget v5, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog;->m2Model:I

    if-ne v5, v9, :cond_2

    .line 1084
    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getresource()Landroid/content/res/Resources;

    move-result-object v5

    const v10, 0x7f080007

    invoke-virtual {v5, v10}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog;->exportResOptionsText:[Ljava/lang/String;

    .line 1090
    :goto_1
    sget-object v5, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;->EXPORT_RES:[Ljava/lang/Integer;

    array-length v5, v5

    new-array v5, v5, [Ljava/lang/String;

    iput-object v5, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog;->exportResOptionsTextDesc:[Ljava/lang/String;

    .line 1092
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_2
    sget-object v5, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;->EXPORT_RES:[Ljava/lang/Integer;

    array-length v5, v5

    iget v10, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog;->m2Model:I

    sub-int/2addr v5, v10

    if-lt v3, v5, :cond_3

    .line 1106
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-direct {v0, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1107
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    const v5, 0x7f07025e

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    .line 1108
    new-instance v8, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog$ExportResolutionAdapter;

    const/4 v10, 0x0

    invoke-direct {v8, p0, v10}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog$ExportResolutionAdapter;-><init>(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog;Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog$ExportResolutionAdapter;)V

    new-instance v10, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog$1;

    invoke-direct {v10, p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog$1;-><init>(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog;)V

    invoke-virtual {v5, v8, v9, v10}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(Landroid/widget/ListAdapter;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    .line 1113
    const v8, 0x7f070074

    new-instance v9, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog$2;

    invoke-direct {v9, p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog$2;-><init>(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog;)V

    invoke-virtual {v5, v8, v9}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    .line 1127
    const v8, 0x7f07005e

    new-instance v9, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog$3;

    invoke-direct {v9, p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog$3;-><init>(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog;)V

    invoke-virtual {v5, v8, v9}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1132
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v5

    return-object v5

    .end local v0    # "builder":Landroid/app/AlertDialog$Builder;
    .end local v3    # "i":I
    :cond_1
    move v5, v8

    .line 1082
    goto :goto_0

    .line 1087
    :cond_2
    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getresource()Landroid/content/res/Resources;

    move-result-object v5

    const v10, 0x7f080006

    invoke-virtual {v5, v10}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog;->exportResOptionsText:[Ljava/lang/String;

    goto :goto_1

    .line 1095
    .restart local v3    # "i":I
    :cond_3
    sget-object v5, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;->EXPORT_RES:[Ljava/lang/Integer;

    iget v10, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog;->m2Model:I

    add-int/2addr v10, v3

    aget-object v5, v5, v10

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-static {v5}, Lcom/samsung/app/video/editor/external/Constants;->getVideoBitRate(I)I

    move-result v5

    add-int/lit8 v5, v5, 0x40

    int-to-long v10, v5

    .line 1094
    mul-long/2addr v10, v6

    long-to-float v5, v10

    .line 1099
    const/high16 v10, 0x4afa0000    # 8192000.0f

    .line 1094
    div-float v1, v5, v10

    .line 1101
    .local v1, "expectedOutputSize":F
    const/high16 v5, 0x3f000000    # 0.5f

    add-float/2addr v5, v1

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 1102
    .local v2, "expectedOutputsizeRound":I
    iget-object v5, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog;->exportResOptionsTextDesc:[Ljava/lang/String;

    const v10, 0x7f0702b7

    invoke-static {v10}, Lcom/sec/android/app/ve/VEApp;->getStringValue(I)Ljava/lang/String;

    move-result-object v10

    new-array v11, v9, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v11, v8

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v5, v3

    .line 1092
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2
.end method
