.class public Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$EnableAppDialog;
.super Lcom/sec/android/app/ve/common/AndroidUtils$DlgFragment;
.source "VideoEditorLiteActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "EnableAppDialog"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2641
    invoke-direct {p0}, Lcom/sec/android/app/ve/common/AndroidUtils$DlgFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 2676
    invoke-super {p0, p1}, Lcom/sec/android/app/ve/common/AndroidUtils$DlgFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 2677
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$EnableAppDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$EnableAppDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p1, Landroid/content/res/Configuration;->fontScale:F

    # getter for: Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->fontScale_value:F
    invoke-static {}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->access$7()F

    move-result v1

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    .line 2678
    iget v0, p1, Landroid/content/res/Configuration;->fontScale:F

    invoke-static {v0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->access$8(F)V

    .line 2679
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$EnableAppDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/ve/common/AndroidUtils;->dismissDialog(Landroid/app/Activity;)V

    .line 2682
    :cond_0
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v3, 0x7f07021a

    .line 2645
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$EnableAppDialog;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 2647
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 2648
    const v2, 0x7f070297

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 2649
    new-instance v2, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$EnableAppDialog$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$EnableAppDialog$1;-><init>(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$EnableAppDialog;)V

    invoke-virtual {v1, v3, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 2663
    const v2, 0x7f07005e

    new-instance v3, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$EnableAppDialog$2;

    invoke-direct {v3, p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$EnableAppDialog$2;-><init>(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$EnableAppDialog;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 2671
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    return-object v1
.end method
