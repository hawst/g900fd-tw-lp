.class Lcom/sec/android/app/ve/thread/SummaryThread$SummaryBit;
.super Ljava/lang/Object;
.source "SummaryThread.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/ve/thread/SummaryThread;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SummaryBit"
.end annotation


# instance fields
.field actualOccurance:J

.field endTime:J

.field startTime:J

.field summaryElement:Lcom/samsung/app/video/editor/external/Element;


# direct methods
.method constructor <init>(Lcom/samsung/app/video/editor/external/Element;)V
    .locals 2
    .param p1, "summaryElement"    # Lcom/samsung/app/video/editor/external/Element;

    .prologue
    .line 565
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 562
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/app/ve/thread/SummaryThread$SummaryBit;->actualOccurance:J

    .line 566
    iput-object p1, p0, Lcom/sec/android/app/ve/thread/SummaryThread$SummaryBit;->summaryElement:Lcom/samsung/app/video/editor/external/Element;

    .line 567
    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/Element;->getStartTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/ve/thread/SummaryThread$SummaryBit;->startTime:J

    .line 568
    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/Element;->getEndTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/ve/thread/SummaryThread$SummaryBit;->endTime:J

    .line 569
    return-void
.end method
