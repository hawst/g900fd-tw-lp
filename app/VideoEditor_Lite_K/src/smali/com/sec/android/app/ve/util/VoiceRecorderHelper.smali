.class public Lcom/sec/android/app/ve/util/VoiceRecorderHelper;
.super Ljava/lang/Object;
.source "VoiceRecorderHelper.java"


# static fields
.field static final AUDIO_ENCODER:I = 0x3

.field static final MIMETYPE_3GPP:Ljava/lang/String; = "audio/3gpp"

.field private static final MSG_RELEASE_WAKELOCK:I = 0x65

.field static final NUM_AUDIO_CHANNELS:I = 0x1

.field static final OUTPUT_FORMAT:I = 0xf5b4

.field private static final RECORDING_DIR:Ljava/lang/String; = "voice"

.field static final REC_EXT:Ljava/lang/String; = ".3gpp"

.field static final SAMPLING_RATE:I = 0xac44

.field private static final VIDEOMAKERSTR:Ljava/lang/String; = "Video Editor"


# instance fields
.field private RECORDING_FILE_PREFIX:Ljava/lang/String;

.field private TAG:Ljava/lang/String;

.field private mAudioManager:Landroid/media/AudioManager;

.field private mContext:Landroid/content/Context;

.field private mDispalyname:Ljava/lang/String;

.field private mFileDuraion:J

.field private mFilename:Ljava/lang/String;

.field private mFilepath:Ljava/lang/String;

.field private mHandler:Landroid/os/Handler;

.field private mMediaRecorder:Lcom/sec/android/secmediarecorder/SecMediaRecorder;

.field private pm:Landroid/os/PowerManager;

.field private wakelock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "aContext"    # Landroid/content/Context;

    .prologue
    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    const-string v0, "VoiceRecorderHelper"

    iput-object v0, p0, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->TAG:Ljava/lang/String;

    .line 65
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->mFileDuraion:J

    .line 88
    sget v0, Lcom/sec/android/app/ve/R$string;->record:I

    invoke-static {v0}, Lcom/sec/android/app/ve/VEApp;->getStringValue(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->RECORDING_FILE_PREFIX:Ljava/lang/String;

    .line 629
    new-instance v0, Lcom/sec/android/app/ve/util/VoiceRecorderHelper$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/ve/util/VoiceRecorderHelper$1;-><init>(Lcom/sec/android/app/ve/util/VoiceRecorderHelper;)V

    iput-object v0, p0, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->mHandler:Landroid/os/Handler;

    .line 102
    iput-object p1, p0, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->mContext:Landroid/content/Context;

    .line 103
    iget-object v0, p0, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->mContext:Landroid/content/Context;

    const-string v1, "power"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iput-object v0, p0, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->pm:Landroid/os/PowerManager;

    .line 104
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/app/ve/util/VoiceRecorderHelper;)V
    .locals 0

    .prologue
    .line 619
    invoke-direct {p0}, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->releaseWakeLock()V

    return-void
.end method

.method private acquireWakeLock()V
    .locals 4

    .prologue
    .line 609
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->wakelock:Landroid/os/PowerManager$WakeLock;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->wakelock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-nez v1, :cond_1

    .line 610
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->pm:Landroid/os/PowerManager;

    const/16 v2, 0x1a

    const-string v3, "VideoEditor  - Voice Recorder"

    invoke-virtual {v1, v2, v3}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->wakelock:Landroid/os/PowerManager$WakeLock;

    .line 611
    iget-object v1, p0, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->wakelock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 617
    :cond_1
    :goto_0
    return-void

    .line 614
    :catch_0
    move-exception v0

    .line 615
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private addToPlaylist(Landroid/content/ContentResolver;IJ)V
    .locals 11
    .param p1, "resolver"    # Landroid/content/ContentResolver;
    .param p2, "audioId"    # I
    .param p3, "playlistId"    # J

    .prologue
    const/4 v9, 0x0

    const/4 v3, 0x0

    .line 505
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    .line 506
    const-string v0, "count(*)"

    aput-object v0, v2, v9

    .line 508
    .local v2, "cols":[Ljava/lang/String;
    const-string v0, "external"

    invoke-static {v0, p3, p4}, Landroid/provider/MediaStore$Audio$Playlists$Members;->getContentUri(Ljava/lang/String;J)Landroid/net/Uri;

    move-result-object v1

    .local v1, "uri":Landroid/net/Uri;
    move-object v0, p1

    move-object v4, v3

    move-object v5, v3

    .line 509
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 510
    .local v7, "cur":Landroid/database/Cursor;
    if-eqz v7, :cond_1

    .line 511
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 513
    invoke-interface {v7, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 515
    .local v6, "base":I
    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    .line 516
    .local v8, "values":Landroid/content/ContentValues;
    const-string v0, "play_order"

    add-int v3, v6, p2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v8, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 517
    const-string v0, "audio_id"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v8, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 518
    invoke-virtual {p1, v1, v8}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 520
    .end local v6    # "base":I
    .end local v8    # "values":Landroid/content/ContentValues;
    :cond_0
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 522
    :cond_1
    return-void
.end method

.method private createPlaylist(Landroid/content/res/Resources;Landroid/content/ContentResolver;)Landroid/net/Uri;
    .locals 4
    .param p1, "res"    # Landroid/content/res/Resources;
    .param p2, "resolver"    # Landroid/content/ContentResolver;

    .prologue
    .line 575
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 576
    .local v0, "cv":Landroid/content/ContentValues;
    const-string v2, "name"

    const-string v3, "Video Editor"

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 577
    const-string v2, "external"

    invoke-static {v2}, Landroid/provider/MediaStore$Audio$Playlists;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {p2, v2, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v1

    .line 580
    .local v1, "uri":Landroid/net/Uri;
    return-object v1
.end method

.method private extractFileNameSuffix(Ljava/lang/String;)I
    .locals 8
    .param p1, "aFilename"    # Ljava/lang/String;

    .prologue
    .line 260
    const/4 v5, 0x0

    .line 262
    .local v5, "lSuffix":I
    const-string v6, " "

    invoke-virtual {p1, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 263
    .local v4, "lSplitArr":[Ljava/lang/String;
    if-eqz v4, :cond_0

    const/4 v6, 0x2

    array-length v7, v4

    if-ne v6, v7, :cond_0

    .line 265
    const/4 v6, 0x1

    aget-object v2, v4, v6

    .line 266
    .local v2, "lNumExtStr":Ljava/lang/String;
    const-string v6, "."

    invoke-virtual {v2, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 267
    .local v1, "lDotIndex":I
    const/4 v6, 0x0

    invoke-virtual {v2, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 269
    .local v3, "lNumStr":Ljava/lang/String;
    :try_start_0
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    .line 277
    .end local v1    # "lDotIndex":I
    .end local v2    # "lNumExtStr":Ljava/lang/String;
    .end local v3    # "lNumStr":Ljava/lang/String;
    :cond_0
    :goto_0
    return v5

    .line 270
    .restart local v1    # "lDotIndex":I
    .restart local v2    # "lNumExtStr":Ljava/lang/String;
    .restart local v3    # "lNumStr":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 272
    .local v0, "e":Ljava/lang/NumberFormatException;
    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->printStackTrace()V

    .line 273
    const/4 v5, 0x0

    goto :goto_0
.end method

.method private getPlaylistId(Landroid/content/res/Resources;)I
    .locals 10
    .param p1, "res"    # Landroid/content/res/Resources;

    .prologue
    const/4 v3, 0x1

    const/4 v9, 0x0

    .line 530
    const-string v0, "external"

    invoke-static {v0}, Landroid/provider/MediaStore$Audio$Playlists;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 531
    .local v1, "uri":Landroid/net/Uri;
    new-array v2, v3, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v9

    .line 532
    .local v2, "ids":[Ljava/lang/String;
    const-string v8, "name=?"

    .line 533
    .local v8, "where":Ljava/lang/String;
    new-array v4, v3, [Ljava/lang/String;

    const-string v0, "Video Editor"

    aput-object v0, v4, v9

    .line 534
    .local v4, "args":[Ljava/lang/String;
    const-string v3, "name=?"

    const/4 v5, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 535
    .local v6, "cursor":Landroid/database/Cursor;
    if-nez v6, :cond_0

    .line 536
    iget-object v0, p0, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->TAG:Ljava/lang/String;

    const-string v3, "query returns null"

    invoke-static {v0, v3}, Lcom/sec/android/app/ve/common/LogUtils;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 538
    :cond_0
    const/4 v7, -0x1

    .line 539
    .local v7, "id":I
    if-eqz v6, :cond_2

    .line 540
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 541
    invoke-interface {v6}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_1

    .line 542
    invoke-interface {v6, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 545
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 548
    :cond_2
    return v7
.end method

.method private getRecNum()I
    .locals 9

    .prologue
    .line 223
    const/4 v5, 0x0

    .line 224
    .local v5, "lRecNum":I
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v7

    invoke-virtual {v7}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    .line 225
    .local v2, "dirpath":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "voice"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 226
    const/4 v6, 0x0

    .line 228
    .local v6, "lSuffix":I
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 230
    .local v1, "dir":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v0

    .line 231
    .local v0, "children":[Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 232
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v7, v0

    if-lt v3, v7, :cond_1

    .line 244
    .end local v3    # "i":I
    :cond_0
    add-int/lit8 v5, v5, 0x1

    .line 245
    return v5

    .line 235
    .restart local v3    # "i":I
    :cond_1
    aget-object v4, v0, v3

    .line 236
    .local v4, "lFilename":Ljava/lang/String;
    invoke-direct {p0, v4}, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->extractFileNameSuffix(Ljava/lang/String;)I

    move-result v6

    .line 237
    if-le v6, v5, :cond_2

    .line 239
    move v5, v6

    .line 232
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method private getRecordingFilePath()Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 181
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v4

    .line 182
    .local v4, "state":Ljava/lang/String;
    const-string v6, "mounted"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 211
    :goto_0
    return-object v5

    .line 186
    :cond_0
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v6

    invoke-virtual {v6}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 188
    .local v1, "dirpath":Ljava/lang/String;
    iput-object v5, p0, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->mFilepath:Ljava/lang/String;

    .line 190
    if-eqz v1, :cond_2

    .line 192
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "voice"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 194
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 195
    .local v0, "dir":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v5

    if-nez v5, :cond_1

    .line 196
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 198
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->getRecNum()I

    move-result v2

    .line 199
    .local v2, "lRecNum":I
    const/4 v3, 0x0

    .line 200
    .local v3, "lRecNumStr":Ljava/lang/String;
    const/16 v5, 0xa

    if-ge v2, v5, :cond_3

    .line 201
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "0"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 205
    :goto_1
    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->RECORDING_FILE_PREFIX:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->mFilename:Ljava/lang/String;

    .line 207
    iget-object v5, p0, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->mFilename:Ljava/lang/String;

    iput-object v5, p0, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->mDispalyname:Ljava/lang/String;

    .line 208
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->mFilename:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ".3gpp"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->mFilepath:Ljava/lang/String;

    .line 211
    .end local v0    # "dir":Ljava/io/File;
    .end local v2    # "lRecNum":I
    .end local v3    # "lRecNumStr":Ljava/lang/String;
    :cond_2
    iget-object v5, p0, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->mFilepath:Ljava/lang/String;

    goto/16 :goto_0

    .line 203
    .restart local v0    # "dir":Ljava/io/File;
    .restart local v2    # "lRecNum":I
    .restart local v3    # "lRecNumStr":Ljava/lang/String;
    :cond_3
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_1
.end method

.method private initRecording(Landroid/media/AudioManager$OnAudioFocusChangeListener;Lcom/sec/android/secmediarecorder/SecMediaRecorder$OnErrorListener;Lcom/sec/android/secmediarecorder/SecMediaRecorder$OnInfoListener;)Z
    .locals 6
    .param p1, "aOnAudioFocusChangeListener"    # Landroid/media/AudioManager$OnAudioFocusChangeListener;
    .param p2, "aOnErrorListener"    # Lcom/sec/android/secmediarecorder/SecMediaRecorder$OnErrorListener;
    .param p3, "aOnInfoListener"    # Lcom/sec/android/secmediarecorder/SecMediaRecorder$OnInfoListener;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 289
    invoke-direct {p0}, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->getRecordingFilePath()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_0

    .line 338
    :goto_0
    return v1

    .line 295
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->releaseMediaRecorder()V

    .line 296
    new-instance v3, Lcom/sec/android/secmediarecorder/SecMediaRecorder;

    invoke-direct {v3}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;-><init>()V

    iput-object v3, p0, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->mMediaRecorder:Lcom/sec/android/secmediarecorder/SecMediaRecorder;

    .line 299
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->mMediaRecorder:Lcom/sec/android/secmediarecorder/SecMediaRecorder;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->setAudioSource(I)V

    .line 300
    iget-object v3, p0, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->mMediaRecorder:Lcom/sec/android/secmediarecorder/SecMediaRecorder;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->setOutputFormat(I)V

    .line 303
    iget-object v3, p0, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->mMediaRecorder:Lcom/sec/android/secmediarecorder/SecMediaRecorder;

    const-wide/16 v4, 0x0

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->setMaxFileSize(J)V

    .line 305
    iget-object v3, p0, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->mMediaRecorder:Lcom/sec/android/secmediarecorder/SecMediaRecorder;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->setMaxDuration(I)V

    .line 310
    iget-object v3, p0, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->mMediaRecorder:Lcom/sec/android/secmediarecorder/SecMediaRecorder;

    iget-object v4, p0, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->mFilepath:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->setOutputFile(Ljava/lang/String;)V

    .line 311
    iget-object v3, p0, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->mMediaRecorder:Lcom/sec/android/secmediarecorder/SecMediaRecorder;

    const v4, 0xf5b4

    invoke-virtual {v3, v4}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->setAudioEncodingBitRate(I)V

    .line 312
    iget-object v3, p0, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->mMediaRecorder:Lcom/sec/android/secmediarecorder/SecMediaRecorder;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->setAudioChannels(I)V

    .line 313
    iget-object v3, p0, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->mMediaRecorder:Lcom/sec/android/secmediarecorder/SecMediaRecorder;

    const v4, 0xac44

    invoke-virtual {v3, v4}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->setAudioSamplingRate(I)V

    .line 314
    iget-object v3, p0, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->mMediaRecorder:Lcom/sec/android/secmediarecorder/SecMediaRecorder;

    const/4 v4, 0x3

    invoke-virtual {v3, v4}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->setAudioEncoder(I)V

    .line 322
    iget-object v3, p0, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->mMediaRecorder:Lcom/sec/android/secmediarecorder/SecMediaRecorder;

    invoke-virtual {v3}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->prepare()V

    .line 324
    iget-object v3, p0, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->mMediaRecorder:Lcom/sec/android/secmediarecorder/SecMediaRecorder;

    invoke-virtual {v3, p2}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->setOnErrorListener(Lcom/sec/android/secmediarecorder/SecMediaRecorder$OnErrorListener;)V

    .line 325
    iget-object v3, p0, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->mMediaRecorder:Lcom/sec/android/secmediarecorder/SecMediaRecorder;

    invoke-virtual {v3, p3}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->setOnInfoListener(Lcom/sec/android/secmediarecorder/SecMediaRecorder$OnInfoListener;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move v1, v2

    .line 338
    goto :goto_0

    .line 327
    :catch_0
    move-exception v0

    .line 328
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    .line 329
    invoke-virtual {p0, p1}, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->cancelRecording(Landroid/media/AudioManager$OnAudioFocusChangeListener;)V

    goto :goto_0

    .line 331
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_1
    move-exception v0

    .line 332
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 333
    invoke-virtual {p0, p1}, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->cancelRecording(Landroid/media/AudioManager$OnAudioFocusChangeListener;)V

    goto :goto_0
.end method

.method private query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 8
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x0

    .line 557
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 558
    .local v0, "resolver":Landroid/content/ContentResolver;
    if-nez v0, :cond_0

    move-object v1, v7

    .line 564
    .end local v0    # "resolver":Landroid/content/ContentResolver;
    :goto_0
    return-object v1

    .restart local v0    # "resolver":Landroid/content/ContentResolver;
    :cond_0
    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    .line 561
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    .line 562
    .end local v0    # "resolver":Landroid/content/ContentResolver;
    :catch_0
    move-exception v6

    .line 563
    .local v6, "e":Ljava/lang/UnsupportedOperationException;
    invoke-virtual {v6}, Ljava/lang/UnsupportedOperationException;->printStackTrace()V

    move-object v1, v7

    .line 564
    goto :goto_0
.end method

.method private releaseMediaRecorder()V
    .locals 6

    .prologue
    .line 344
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->mMediaRecorder:Lcom/sec/android/secmediarecorder/SecMediaRecorder;

    if-eqz v1, :cond_0

    .line 345
    iget-object v1, p0, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x65

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 346
    iget-object v1, p0, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x65

    const-wide/16 v4, 0x7d0

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 347
    iget-object v1, p0, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->mMediaRecorder:Lcom/sec/android/secmediarecorder/SecMediaRecorder;

    invoke-virtual {v1}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->reset()V

    .line 348
    iget-object v1, p0, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->mMediaRecorder:Lcom/sec/android/secmediarecorder/SecMediaRecorder;

    invoke-virtual {v1}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->release()V

    .line 349
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->mMediaRecorder:Lcom/sec/android/secmediarecorder/SecMediaRecorder;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 356
    :cond_0
    :goto_0
    return-void

    .line 351
    :catch_0
    move-exception v0

    .line 352
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private releaseWakeLock()V
    .locals 2

    .prologue
    .line 621
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->wakelock:Landroid/os/PowerManager$WakeLock;

    if-eqz v1, :cond_0

    .line 622
    iget-object v1, p0, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->wakelock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 627
    :cond_0
    :goto_0
    return-void

    .line 624
    :catch_0
    move-exception v0

    .line 625
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private updateExternalDB()V
    .locals 14

    .prologue
    .line 457
    iget-object v11, p0, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    .line 458
    .local v8, "res":Landroid/content/res/Resources;
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 460
    .local v4, "cv":Landroid/content/ContentValues;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 462
    .local v2, "current":J
    new-instance v5, Ljava/io/File;

    iget-object v11, p0, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->mFilepath:Ljava/lang/String;

    invoke-direct {v5, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 463
    .local v5, "file":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->lastModified()J

    move-result-wide v6

    .line 468
    .local v6, "modDate":J
    const-string v11, "is_music"

    const-string v12, "0"

    invoke-virtual {v4, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 471
    const-string v11, "title"

    iget-object v12, p0, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->mDispalyname:Ljava/lang/String;

    invoke-virtual {v4, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 472
    const-string v11, "_data"

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v4, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 473
    const-string v11, "date_added"

    const-wide/16 v12, 0x3e8

    div-long v12, v2, v12

    long-to-int v12, v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v4, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 474
    const-string v11, "date_modified"

    const-wide/16 v12, 0x3e8

    div-long v12, v6, v12

    long-to-int v12, v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v4, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 475
    const-string v11, "mime_type"

    const-string v12, "audio/3gpp"

    invoke-virtual {v4, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 477
    const-string v11, "album"

    const-string v12, "Video Editor"

    invoke-virtual {v4, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 480
    const-string v11, "duration"

    iget-wide v12, p0, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->mFileDuraion:J

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    invoke-virtual {v4, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 481
    const-string v11, "_size"

    invoke-virtual {v5}, Ljava/io/File;->length()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    invoke-virtual {v4, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 485
    iget-object v11, p0, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v11}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    .line 486
    .local v9, "resolver":Landroid/content/ContentResolver;
    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 487
    .local v1, "base":Landroid/net/Uri;
    invoke-virtual {v9, v1, v4}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v10

    .line 489
    .local v10, "result":Landroid/net/Uri;
    invoke-direct {p0, v8}, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->getPlaylistId(Landroid/content/res/Resources;)I

    move-result v11

    const/4 v12, -0x1

    if-ne v11, v12, :cond_0

    .line 490
    invoke-direct {p0, v8, v9}, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->createPlaylist(Landroid/content/res/Resources;Landroid/content/ContentResolver;)Landroid/net/Uri;

    .line 492
    :cond_0
    if-eqz v10, :cond_1

    .line 493
    invoke-virtual {v10}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 494
    .local v0, "audioId":I
    invoke-direct {p0, v8}, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->getPlaylistId(Landroid/content/res/Resources;)I

    move-result v11

    int-to-long v12, v11

    invoke-direct {p0, v9, v0, v12, v13}, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->addToPlaylist(Landroid/content/ContentResolver;IJ)V

    .line 496
    .end local v0    # "audioId":I
    :cond_1
    return-void
.end method


# virtual methods
.method public cancelRecording(Landroid/media/AudioManager$OnAudioFocusChangeListener;)V
    .locals 4
    .param p1, "aOnAudioFocusChangeListener"    # Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .prologue
    .line 590
    invoke-direct {p0}, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->releaseMediaRecorder()V

    .line 592
    iget-object v1, p0, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->mFilepath:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 593
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->mFilepath:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 594
    .local v0, "f":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 597
    .end local v0    # "f":Ljava/io/File;
    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->mFilename:Ljava/lang/String;

    iput-object v1, p0, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->mFilepath:Ljava/lang/String;

    .line 598
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->mFileDuraion:J

    .line 600
    iget-object v1, p0, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->mAudioManager:Landroid/media/AudioManager;

    if-eqz v1, :cond_1

    .line 602
    iget-object v1, p0, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v1, p1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 605
    :cond_1
    return-void
.end method

.method public getAbsoluteFilepath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->mFilepath:Ljava/lang/String;

    return-object v0
.end method

.method public getDisplayFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->mDispalyname:Ljava/lang/String;

    return-object v0
.end method

.method public getFileDuration()J
    .locals 2

    .prologue
    .line 132
    iget-wide v0, p0, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->mFileDuraion:J

    return-wide v0
.end method

.method public pauseRecording()Z
    .locals 3

    .prologue
    .line 419
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->mMediaRecorder:Lcom/sec/android/secmediarecorder/SecMediaRecorder;

    invoke-virtual {v2}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->pause()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 430
    const/4 v2, 0x1

    :goto_0
    return v2

    .line 420
    :catch_0
    move-exception v0

    .line 421
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 423
    const/4 v2, 0x0

    :try_start_1
    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->cancelRecording(Landroid/media/AudioManager$OnAudioFocusChangeListener;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 428
    :goto_1
    const/4 v2, 0x0

    goto :goto_0

    .line 425
    :catch_1
    move-exception v1

    .line 426
    .local v1, "exception":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public resumeRecording()Z
    .locals 3

    .prologue
    .line 436
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->mMediaRecorder:Lcom/sec/android/secmediarecorder/SecMediaRecorder;

    invoke-virtual {v2}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->resume()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 447
    const/4 v2, 0x1

    :goto_0
    return v2

    .line 438
    :catch_0
    move-exception v0

    .line 440
    .local v0, "e":Ljava/lang/Exception;
    const/4 v2, 0x0

    :try_start_1
    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->cancelRecording(Landroid/media/AudioManager$OnAudioFocusChangeListener;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 445
    :goto_1
    const/4 v2, 0x0

    goto :goto_0

    .line 442
    :catch_1
    move-exception v1

    .line 443
    .local v1, "exception":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public startRecording(Landroid/media/AudioManager$OnAudioFocusChangeListener;Lcom/sec/android/secmediarecorder/SecMediaRecorder$OnErrorListener;Lcom/sec/android/secmediarecorder/SecMediaRecorder$OnInfoListener;)Z
    .locals 4
    .param p1, "aOnAudioFocusChangeListener"    # Landroid/media/AudioManager$OnAudioFocusChangeListener;
    .param p2, "aOnErrorListener"    # Lcom/sec/android/secmediarecorder/SecMediaRecorder$OnErrorListener;
    .param p3, "aOnInfoListener"    # Lcom/sec/android/secmediarecorder/SecMediaRecorder$OnInfoListener;

    .prologue
    .line 143
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->mFilename:Ljava/lang/String;

    iput-object v2, p0, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->mFilepath:Ljava/lang/String;

    .line 144
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->mFileDuraion:J

    .line 147
    iget-object v2, p0, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->mContext:Landroid/content/Context;

    const-string v3, "audio"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/media/AudioManager;

    iput-object v2, p0, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->mAudioManager:Landroid/media/AudioManager;

    .line 148
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->initRecording(Landroid/media/AudioManager$OnAudioFocusChangeListener;Lcom/sec/android/secmediarecorder/SecMediaRecorder$OnErrorListener;Lcom/sec/android/secmediarecorder/SecMediaRecorder$OnInfoListener;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 150
    invoke-direct {p0}, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->acquireWakeLock()V

    .line 151
    const/4 v0, 0x1

    .line 153
    .local v0, "bRecording":Z
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->mMediaRecorder:Lcom/sec/android/secmediarecorder/SecMediaRecorder;

    invoke-virtual {v2}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->start()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 171
    :goto_0
    return v0

    .line 154
    :catch_0
    move-exception v1

    .line 155
    .local v1, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->printStackTrace()V

    .line 156
    invoke-direct {p0}, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->releaseMediaRecorder()V

    .line 157
    const/4 v0, 0x0

    goto :goto_0

    .line 158
    .end local v1    # "e":Ljava/lang/IllegalStateException;
    :catch_1
    move-exception v1

    .line 159
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 160
    invoke-direct {p0}, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->releaseMediaRecorder()V

    .line 161
    const/4 v0, 0x0

    .line 164
    goto :goto_0

    .line 168
    .end local v0    # "bRecording":Z
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_0
    const/4 v0, 0x0

    .restart local v0    # "bRecording":Z
    goto :goto_0
.end method

.method public stopRecording(Landroid/media/AudioManager$OnAudioFocusChangeListener;)Z
    .locals 8
    .param p1, "aOnAudioFocusChangeListener"    # Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .prologue
    const/4 v3, 0x0

    .line 369
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->mMediaRecorder:Lcom/sec/android/secmediarecorder/SecMediaRecorder;

    if-eqz v4, :cond_0

    .line 371
    iget-object v4, p0, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->mMediaRecorder:Lcom/sec/android/secmediarecorder/SecMediaRecorder;

    invoke-virtual {v4}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->stop()V

    .line 374
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->mAudioManager:Landroid/media/AudioManager;

    if-eqz v4, :cond_1

    .line 376
    iget-object v4, p0, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v4, p1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 379
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->releaseMediaRecorder()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 388
    new-instance v1, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v1}, Landroid/media/MediaMetadataRetriever;-><init>()V

    .line 389
    .local v1, "retriever":Landroid/media/MediaMetadataRetriever;
    iget-object v4, p0, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->mFilepath:Ljava/lang/String;

    invoke-virtual {v1, v4}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/lang/String;)V

    .line 392
    const/16 v4, 0x9

    invoke-virtual {v1, v4}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v2

    .line 393
    .local v2, "value":Ljava/lang/String;
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    int-to-long v4, v4

    iput-wide v4, p0, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->mFileDuraion:J

    .line 395
    iget-wide v4, p0, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->mFileDuraion:J

    const-wide/16 v6, 0x3e8

    cmp-long v4, v4, v6

    if-gez v4, :cond_2

    .line 397
    invoke-virtual {p0, p1}, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->cancelRecording(Landroid/media/AudioManager$OnAudioFocusChangeListener;)V

    .line 413
    .end local v1    # "retriever":Landroid/media/MediaMetadataRetriever;
    .end local v2    # "value":Ljava/lang/String;
    :goto_0
    return v3

    .line 380
    :catch_0
    move-exception v0

    .line 381
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    .line 382
    invoke-virtual {p0, p1}, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->cancelRecording(Landroid/media/AudioManager$OnAudioFocusChangeListener;)V

    goto :goto_0

    .line 404
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    .restart local v1    # "retriever":Landroid/media/MediaMetadataRetriever;
    .restart local v2    # "value":Ljava/lang/String;
    :cond_2
    :try_start_1
    invoke-direct {p0}, Lcom/sec/android/app/ve/util/VoiceRecorderHelper;->updateExternalDB()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 413
    :goto_1
    const/4 v3, 0x1

    goto :goto_0

    .line 406
    :catch_1
    move-exception v0

    .line 410
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method
