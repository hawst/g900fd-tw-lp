.class public Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade;
.super Ljava/lang/Object;
.source "LibraryInitDeinitFacade.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade$MediaDetail;
    }
.end annotation


# instance fields
.field private cache:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade$MediaDetail;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mLibInitListener:Lcom/sec/android/app/ve/thread/LibInitDeinitForElementTask$LibInitForElementTaskListener;

.field private mTaskPool:Lcom/sec/android/app/ve/thread/AsyncTaskPool;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    new-instance v0, Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade$1;-><init>(Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade;)V

    iput-object v0, p0, Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade;->mLibInitListener:Lcom/sec/android/app/ve/thread/LibInitDeinitForElementTask$LibInitForElementTaskListener;

    .line 48
    sget-boolean v0, Lcom/sec/android/app/ve/VEApp;->isThreadBasedInitImges:Z

    if-eqz v0, :cond_0

    .line 49
    new-instance v0, Lcom/sec/android/app/ve/thread/AsyncTaskPool;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sec/android/app/ve/thread/AsyncTaskPool;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade;->mTaskPool:Lcom/sec/android/app/ve/thread/AsyncTaskPool;

    .line 50
    :cond_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade;->cache:Ljava/util/HashMap;

    .line 52
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade;)Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade;->cache:Ljava/util/HashMap;

    return-object v0
.end method

.method private isDuplicateEntriesPresent(Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade$MediaDetail;Lcom/samsung/app/video/editor/external/TranscodeElement;)Z
    .locals 10
    .param p1, "md"    # Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade$MediaDetail;
    .param p2, "trans"    # Lcom/samsung/app/video/editor/external/TranscodeElement;

    .prologue
    const/4 v5, 0x1

    .line 72
    const/4 v0, 0x0

    .line 73
    .local v0, "count":I
    if-eqz p2, :cond_0

    .line 74
    invoke-virtual {p2}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementList()Ljava/util/List;

    move-result-object v2

    .line 75
    .local v2, "eleList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    if-eqz v2, :cond_0

    .line 76
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    .line 77
    .local v1, "counter":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-lt v4, v1, :cond_1

    .line 85
    .end local v1    # "counter":I
    .end local v2    # "eleList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    .end local v4    # "i":I
    :cond_0
    if-le v0, v5, :cond_3

    .line 87
    :goto_1
    return v5

    .line 78
    .restart local v1    # "counter":I
    .restart local v2    # "eleList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    .restart local v4    # "i":I
    :cond_1
    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/app/video/editor/external/Element;

    .line 79
    .local v3, "element":Lcom/samsung/app/video/editor/external/Element;
    iget-object v6, v3, Lcom/samsung/app/video/editor/external/Element;->filePath:Ljava/lang/String;

    iget-object v7, p1, Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade$MediaDetail;->filePath:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_2

    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/Element;->getStartTime()J

    move-result-wide v6

    iget-wide v8, p1, Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade$MediaDetail;->startTime:J

    cmp-long v6, v6, v8

    if-nez v6, :cond_2

    .line 80
    add-int/lit8 v0, v0, 0x1

    .line 77
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 87
    .end local v1    # "counter":I
    .end local v2    # "eleList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    .end local v3    # "element":Lcom/samsung/app/video/editor/external/Element;
    .end local v4    # "i":I
    :cond_3
    const/4 v5, 0x0

    goto :goto_1
.end method


# virtual methods
.method public destroy()V
    .locals 5

    .prologue
    .line 108
    iget-object v2, p0, Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade;->cache:Ljava/util/HashMap;

    if-eqz v2, :cond_0

    .line 109
    iget-object v2, p0, Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade;->cache:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    .line 110
    .local v0, "mediafiles":Ljava/util/Set;, "Ljava/util/Set<Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade$MediaDetail;>;"
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 113
    iget-object v2, p0, Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade;->cache:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->clear()V

    .line 116
    .end local v0    # "mediafiles":Ljava/util/Set;, "Ljava/util/Set<Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade$MediaDetail;>;"
    :cond_0
    return-void

    .line 110
    .restart local v0    # "mediafiles":Ljava/util/Set;, "Ljava/util/Set<Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade$MediaDetail;>;"
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade$MediaDetail;

    .line 111
    .local v1, "mf":Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade$MediaDetail;
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v3

    iget-object v4, v1, Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade$MediaDetail;->filePath:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/samsung/app/video/editor/external/NativeInterface;->EngineDeinitImage(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public elementDeInit(Lcom/samsung/app/video/editor/external/Element;Lcom/samsung/app/video/editor/external/TranscodeElement;)V
    .locals 6
    .param p1, "element"    # Lcom/samsung/app/video/editor/external/Element;
    .param p2, "trans"    # Lcom/samsung/app/video/editor/external/TranscodeElement;

    .prologue
    .line 92
    new-instance v0, Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade$MediaDetail;

    iget-object v2, p1, Lcom/samsung/app/video/editor/external/Element;->filePath:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/Element;->getStartTime()J

    move-result-wide v4

    invoke-direct {v0, v2, v4, v5}, Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade$MediaDetail;-><init>(Ljava/lang/String;J)V

    .line 93
    .local v0, "md":Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade$MediaDetail;
    iget-object v3, p0, Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade;->cache:Ljava/util/HashMap;

    monitor-enter v3

    .line 94
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade;->cache:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-direct {p0, v0, p2}, Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade;->isDuplicateEntriesPresent(Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade$MediaDetail;Lcom/samsung/app/video/editor/external/TranscodeElement;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 95
    sget-boolean v2, Lcom/sec/android/app/ve/VEApp;->isThreadBasedInitImges:Z

    if-eqz v2, :cond_1

    .line 96
    new-instance v1, Lcom/sec/android/app/ve/thread/LibInitDeinitForElementTask;

    iget-object v2, p0, Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade;->mTaskPool:Lcom/sec/android/app/ve/thread/AsyncTaskPool;

    iget-object v4, p0, Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade;->mLibInitListener:Lcom/sec/android/app/ve/thread/LibInitDeinitForElementTask$LibInitForElementTaskListener;

    const/4 v5, 0x0

    invoke-direct {v1, v2, p1, v4, v5}, Lcom/sec/android/app/ve/thread/LibInitDeinitForElementTask;-><init>(Lcom/sec/android/app/ve/thread/AsyncTaskPool;Lcom/samsung/app/video/editor/external/Element;Lcom/sec/android/app/ve/thread/LibInitDeinitForElementTask$LibInitForElementTaskListener;Z)V

    .line 97
    .local v1, "task":Lcom/sec/android/app/ve/thread/LibInitDeinitForElementTask;
    iget-object v2, p0, Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade;->mTaskPool:Lcom/sec/android/app/ve/thread/AsyncTaskPool;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/ve/thread/AsyncTaskPool;->addUniqueTask(Lcom/sec/android/app/ve/thread/SimpleTask;)Lcom/sec/android/app/ve/thread/SimpleTask;

    .line 93
    .end local v1    # "task":Lcom/sec/android/app/ve/thread/LibInitDeinitForElementTask;
    :cond_0
    :goto_0
    monitor-exit v3

    .line 105
    return-void

    .line 100
    :cond_1
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v2

    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/Element;->getFilePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/samsung/app/video/editor/external/NativeInterface;->EngineDeinitImage(Ljava/lang/String;)V

    .line 101
    iget-object v2, p0, Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade;->cache:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 93
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public elementInit(Lcom/samsung/app/video/editor/external/Element;)V
    .locals 6
    .param p1, "element"    # Lcom/samsung/app/video/editor/external/Element;

    .prologue
    .line 56
    new-instance v0, Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade$MediaDetail;

    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/Element;->getFilePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/Element;->getStartTime()J

    move-result-wide v4

    invoke-direct {v0, v2, v4, v5}, Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade$MediaDetail;-><init>(Ljava/lang/String;J)V

    .line 57
    .local v0, "md":Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade$MediaDetail;
    iget-object v3, p0, Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade;->cache:Ljava/util/HashMap;

    monitor-enter v3

    .line 58
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade;->cache:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 60
    sget-boolean v2, Lcom/sec/android/app/ve/VEApp;->isThreadBasedInitImges:Z

    if-eqz v2, :cond_1

    .line 61
    new-instance v1, Lcom/sec/android/app/ve/thread/LibInitDeinitForElementTask;

    iget-object v2, p0, Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade;->mTaskPool:Lcom/sec/android/app/ve/thread/AsyncTaskPool;

    iget-object v4, p0, Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade;->mLibInitListener:Lcom/sec/android/app/ve/thread/LibInitDeinitForElementTask$LibInitForElementTaskListener;

    const/4 v5, 0x1

    invoke-direct {v1, v2, p1, v4, v5}, Lcom/sec/android/app/ve/thread/LibInitDeinitForElementTask;-><init>(Lcom/sec/android/app/ve/thread/AsyncTaskPool;Lcom/samsung/app/video/editor/external/Element;Lcom/sec/android/app/ve/thread/LibInitDeinitForElementTask$LibInitForElementTaskListener;Z)V

    .line 62
    .local v1, "task":Lcom/sec/android/app/ve/thread/LibInitDeinitForElementTask;
    iget-object v2, p0, Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade;->mTaskPool:Lcom/sec/android/app/ve/thread/AsyncTaskPool;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/ve/thread/AsyncTaskPool;->addUniqueTask(Lcom/sec/android/app/ve/thread/SimpleTask;)Lcom/sec/android/app/ve/thread/SimpleTask;

    .line 57
    .end local v1    # "task":Lcom/sec/android/app/ve/thread/LibInitDeinitForElementTask;
    :cond_0
    :goto_0
    monitor-exit v3

    .line 69
    return-void

    .line 64
    :cond_1
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v2

    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/Element;->getFilePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/samsung/app/video/editor/external/NativeInterface;->EngineInitImage(Ljava/lang/String;)Z

    .line 65
    iget-object v2, p0, Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade;->cache:Ljava/util/HashMap;

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v0, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 57
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method
