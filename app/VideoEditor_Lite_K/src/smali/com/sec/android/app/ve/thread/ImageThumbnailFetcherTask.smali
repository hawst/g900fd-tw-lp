.class public Lcom/sec/android/app/ve/thread/ImageThumbnailFetcherTask;
.super Lcom/sec/android/app/ve/thread/SimpleTask;
.source "ImageThumbnailFetcherTask.java"


# instance fields
.field private mCallback:Lcom/sec/android/app/ve/thread/VideoImageThumbFetcher$VideoImageThumbnaiFetcherCallbak;

.field private mFilename:Ljava/lang/String;

.field private mIsSupported:Z

.field mMediaType:I

.field private mOptionalTag:Ljava/lang/Object;

.field private mPos:I

.field private mTaskPool:Lcom/sec/android/app/ve/thread/AsyncTaskPool;


# direct methods
.method public constructor <init>(Ljava/lang/String;ILcom/sec/android/app/ve/thread/AsyncTaskPool;Lcom/sec/android/app/ve/thread/VideoImageThumbFetcher$VideoImageThumbnaiFetcherCallbak;IILjava/lang/Object;IZ)V
    .locals 0
    .param p1, "str"    # Ljava/lang/String;
    .param p2, "pos"    # I
    .param p3, "pool"    # Lcom/sec/android/app/ve/thread/AsyncTaskPool;
    .param p4, "callback"    # Lcom/sec/android/app/ve/thread/VideoImageThumbFetcher$VideoImageThumbnaiFetcherCallbak;
    .param p5, "thumbnailWidth"    # I
    .param p6, "thumbnailHeight"    # I
    .param p7, "tag"    # Ljava/lang/Object;
    .param p8, "mediaType"    # I
    .param p9, "isSupported"    # Z

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/sec/android/app/ve/thread/SimpleTask;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/sec/android/app/ve/thread/ImageThumbnailFetcherTask;->mFilename:Ljava/lang/String;

    .line 35
    iput p2, p0, Lcom/sec/android/app/ve/thread/ImageThumbnailFetcherTask;->mPos:I

    .line 36
    iput-object p4, p0, Lcom/sec/android/app/ve/thread/ImageThumbnailFetcherTask;->mCallback:Lcom/sec/android/app/ve/thread/VideoImageThumbFetcher$VideoImageThumbnaiFetcherCallbak;

    .line 37
    iput-object p3, p0, Lcom/sec/android/app/ve/thread/ImageThumbnailFetcherTask;->mTaskPool:Lcom/sec/android/app/ve/thread/AsyncTaskPool;

    .line 38
    iput-object p7, p0, Lcom/sec/android/app/ve/thread/ImageThumbnailFetcherTask;->mOptionalTag:Ljava/lang/Object;

    .line 39
    iput p8, p0, Lcom/sec/android/app/ve/thread/ImageThumbnailFetcherTask;->mMediaType:I

    .line 40
    iput-boolean p9, p0, Lcom/sec/android/app/ve/thread/ImageThumbnailFetcherTask;->mIsSupported:Z

    .line 41
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 106
    if-ne p0, p1, :cond_1

    .line 117
    :cond_0
    :goto_0
    return v1

    .line 109
    :cond_1
    instance-of v3, p1, Lcom/sec/android/app/ve/thread/ImageThumbnailFetcherTask;

    if-nez v3, :cond_2

    move v1, v2

    .line 110
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 112
    check-cast v0, Lcom/sec/android/app/ve/thread/ImageThumbnailFetcherTask;

    .line 113
    .local v0, "task":Lcom/sec/android/app/ve/thread/ImageThumbnailFetcherTask;
    iget-object v3, v0, Lcom/sec/android/app/ve/thread/ImageThumbnailFetcherTask;->mFilename:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/app/ve/thread/ImageThumbnailFetcherTask;->mFilename:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-eqz v3, :cond_0

    move v1, v2

    .line 117
    goto :goto_0
.end method

.method protected getParam()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getPool()Lcom/sec/android/app/ve/thread/AsyncTaskPool;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sec/android/app/ve/thread/ImageThumbnailFetcherTask;->mTaskPool:Lcom/sec/android/app/ve/thread/AsyncTaskPool;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 122
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method protected onBackgroundTask(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 11
    .param p1, "param"    # Ljava/lang/Object;

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 50
    new-instance v2, Lcom/sec/android/app/ve/common/KenburnsDefault$KenburnData;

    invoke-direct {v2}, Lcom/sec/android/app/ve/common/KenburnsDefault$KenburnData;-><init>()V

    .line 52
    .local v2, "mKenburnData":Lcom/sec/android/app/ve/common/KenburnsDefault$KenburnData;
    iget-object v4, p0, Lcom/sec/android/app/ve/thread/ImageThumbnailFetcherTask;->mCallback:Lcom/sec/android/app/ve/thread/VideoImageThumbFetcher$VideoImageThumbnaiFetcherCallbak;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/sec/android/app/ve/thread/ImageThumbnailFetcherTask;->mCallback:Lcom/sec/android/app/ve/thread/VideoImageThumbFetcher$VideoImageThumbnaiFetcherCallbak;

    iget v5, p0, Lcom/sec/android/app/ve/thread/ImageThumbnailFetcherTask;->mPos:I

    iget-object v6, p0, Lcom/sec/android/app/ve/thread/ImageThumbnailFetcherTask;->mFilename:Ljava/lang/String;

    iget-object v7, p0, Lcom/sec/android/app/ve/thread/ImageThumbnailFetcherTask;->mOptionalTag:Ljava/lang/Object;

    invoke-interface {v4, v5, v6, v7}, Lcom/sec/android/app/ve/thread/VideoImageThumbFetcher$VideoImageThumbnaiFetcherCallbak;->isTargetViewExist(ILjava/lang/String;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 53
    iget v4, p0, Lcom/sec/android/app/ve/thread/ImageThumbnailFetcherTask;->mMediaType:I

    if-ne v4, v10, :cond_2

    .line 54
    iget-object v4, p0, Lcom/sec/android/app/ve/thread/ImageThumbnailFetcherTask;->mFilename:Ljava/lang/String;

    iget-boolean v5, p0, Lcom/sec/android/app/ve/thread/ImageThumbnailFetcherTask;->mIsSupported:Z

    invoke-static {v4, v8, v5}, Lcom/sec/android/app/ve/common/MediaUtils;->getImageBitmap(Ljava/lang/String;IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 55
    .local v0, "bmp":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    mul-int/2addr v4, v5

    const v5, 0x11170

    if-ge v4, v5, :cond_0

    .line 56
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 57
    .local v1, "mBitmapOptions":Landroid/graphics/BitmapFactory$Options;
    iget-object v4, p0, Lcom/sec/android/app/ve/thread/ImageThumbnailFetcherTask;->mFilename:Ljava/lang/String;

    invoke-static {v4}, Lcom/sec/android/app/ve/common/MediaUtils;->getImageSampleSize(Ljava/lang/String;)I

    move-result v4

    iput v4, v1, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 58
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 59
    iget-object v4, p0, Lcom/sec/android/app/ve/thread/ImageThumbnailFetcherTask;->mFilename:Ljava/lang/String;

    invoke-static {v4, v1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 60
    iget-object v4, p0, Lcom/sec/android/app/ve/thread/ImageThumbnailFetcherTask;->mFilename:Ljava/lang/String;

    invoke-static {v4, v10}, Lcom/sec/android/app/ve/common/MediaUtils;->getRotateDegree(Ljava/lang/String;I)I

    move-result v3

    .line 61
    .local v3, "rotation":I
    if-eqz v3, :cond_0

    .line 62
    invoke-static {v0, v3}, Lcom/sec/android/app/ve/common/MediaUtils;->getRotatedBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 64
    .end local v1    # "mBitmapOptions":Landroid/graphics/BitmapFactory$Options;
    .end local v3    # "rotation":I
    :cond_0
    invoke-virtual {v2, v0}, Lcom/sec/android/app/ve/common/KenburnsDefault$KenburnData;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 65
    iget-object v4, p0, Lcom/sec/android/app/ve/thread/ImageThumbnailFetcherTask;->mFilename:Ljava/lang/String;

    iget-boolean v5, p0, Lcom/sec/android/app/ve/thread/ImageThumbnailFetcherTask;->mIsSupported:Z

    invoke-static {v2, v4, v5}, Lcom/sec/android/app/ve/common/MediaUtils;->getDefaultKenburnRects(Lcom/sec/android/app/ve/common/KenburnsDefault$KenburnData;Ljava/lang/String;Z)Lcom/sec/android/app/ve/common/KenburnsDefault$KenburnData;

    move-result-object v2

    .line 76
    .end local v0    # "bmp":Landroid/graphics/Bitmap;
    :cond_1
    :goto_0
    return-object v2

    .line 67
    :cond_2
    iget v4, p0, Lcom/sec/android/app/ve/thread/ImageThumbnailFetcherTask;->mMediaType:I

    if-ne v4, v8, :cond_1

    .line 73
    iget-object v4, p0, Lcom/sec/android/app/ve/thread/ImageThumbnailFetcherTask;->mFilename:Ljava/lang/String;

    invoke-static {v4, v8, v9, v9}, Lcom/sec/android/app/ve/common/MediaUtils;->getVideoThumbnail(Ljava/lang/String;IFF)Landroid/graphics/Bitmap;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/sec/android/app/ve/common/KenburnsDefault$KenburnData;->setBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method protected onUIThreadFinally(Ljava/lang/Object;)V
    .locals 11
    .param p1, "result"    # Ljava/lang/Object;

    .prologue
    .line 81
    move-object v10, p1

    check-cast v10, Lcom/sec/android/app/ve/common/KenburnsDefault$KenburnData;

    .line 83
    .local v10, "mKenburnData":Lcom/sec/android/app/ve/common/KenburnsDefault$KenburnData;
    iget-object v0, p0, Lcom/sec/android/app/ve/thread/ImageThumbnailFetcherTask;->mCallback:Lcom/sec/android/app/ve/thread/VideoImageThumbFetcher$VideoImageThumbnaiFetcherCallbak;

    if-eqz v0, :cond_1

    .line 84
    invoke-virtual {v10}, Lcom/sec/android/app/ve/common/KenburnsDefault$KenburnData;->getStartRect()Landroid/graphics/RectF;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 85
    invoke-virtual {v10}, Lcom/sec/android/app/ve/common/KenburnsDefault$KenburnData;->getEndRect()Landroid/graphics/RectF;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 86
    invoke-virtual {v10}, Lcom/sec/android/app/ve/common/KenburnsDefault$KenburnData;->getStartMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 87
    invoke-virtual {v10}, Lcom/sec/android/app/ve/common/KenburnsDefault$KenburnData;->getEndMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    if-nez v0, :cond_2

    .line 88
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/ve/thread/ImageThumbnailFetcherTask;->mCallback:Lcom/sec/android/app/ve/thread/VideoImageThumbFetcher$VideoImageThumbnaiFetcherCallbak;

    iget v1, p0, Lcom/sec/android/app/ve/thread/ImageThumbnailFetcherTask;->mPos:I

    invoke-virtual {v10}, Lcom/sec/android/app/ve/common/KenburnsDefault$KenburnData;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/ve/thread/ImageThumbnailFetcherTask;->mFilename:Ljava/lang/String;

    .line 89
    iget-object v4, p0, Lcom/sec/android/app/ve/thread/ImageThumbnailFetcherTask;->mOptionalTag:Ljava/lang/Object;

    .line 88
    invoke-interface {v0, v1, v2, v3, v4}, Lcom/sec/android/app/ve/thread/VideoImageThumbFetcher$VideoImageThumbnaiFetcherCallbak;->bitmapCreated(ILandroid/graphics/Bitmap;Ljava/lang/String;Ljava/lang/Object;)V

    .line 97
    :cond_1
    :goto_0
    return-void

    .line 91
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/ve/thread/ImageThumbnailFetcherTask;->mCallback:Lcom/sec/android/app/ve/thread/VideoImageThumbFetcher$VideoImageThumbnaiFetcherCallbak;

    iget v1, p0, Lcom/sec/android/app/ve/thread/ImageThumbnailFetcherTask;->mPos:I

    invoke-virtual {v10}, Lcom/sec/android/app/ve/common/KenburnsDefault$KenburnData;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/ve/thread/ImageThumbnailFetcherTask;->mFilename:Ljava/lang/String;

    .line 92
    iget-object v4, p0, Lcom/sec/android/app/ve/thread/ImageThumbnailFetcherTask;->mOptionalTag:Ljava/lang/Object;

    invoke-virtual {v10}, Lcom/sec/android/app/ve/common/KenburnsDefault$KenburnData;->getRefRect()Landroid/graphics/RectF;

    move-result-object v5

    invoke-virtual {v10}, Lcom/sec/android/app/ve/common/KenburnsDefault$KenburnData;->getStartRect()Landroid/graphics/RectF;

    move-result-object v6

    .line 93
    invoke-virtual {v10}, Lcom/sec/android/app/ve/common/KenburnsDefault$KenburnData;->getEndRect()Landroid/graphics/RectF;

    move-result-object v7

    invoke-virtual {v10}, Lcom/sec/android/app/ve/common/KenburnsDefault$KenburnData;->getStartMatrix()Landroid/graphics/Matrix;

    move-result-object v8

    .line 94
    invoke-virtual {v10}, Lcom/sec/android/app/ve/common/KenburnsDefault$KenburnData;->getEndMatrix()Landroid/graphics/Matrix;

    move-result-object v9

    .line 91
    invoke-interface/range {v0 .. v9}, Lcom/sec/android/app/ve/thread/VideoImageThumbFetcher$VideoImageThumbnaiFetcherCallbak;->bitmapCreated(ILandroid/graphics/Bitmap;Ljava/lang/String;Ljava/lang/Object;Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix;Landroid/graphics/Matrix;)V

    goto :goto_0
.end method
