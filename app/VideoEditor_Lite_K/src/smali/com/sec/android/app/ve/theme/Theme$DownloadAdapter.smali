.class Lcom/sec/android/app/ve/theme/Theme$DownloadAdapter;
.super Lcom/sec/android/app/ve/theme/Download$Adapter;
.source "Theme.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/ve/theme/Theme;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DownloadAdapter"
.end annotation


# instance fields
.field private final mThemePos:I

.field final synthetic this$0:Lcom/sec/android/app/ve/theme/Theme;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/ve/theme/Theme;I)V
    .locals 0
    .param p2, "themePos"    # I

    .prologue
    .line 116
    iput-object p1, p0, Lcom/sec/android/app/ve/theme/Theme$DownloadAdapter;->this$0:Lcom/sec/android/app/ve/theme/Theme;

    invoke-direct {p0}, Lcom/sec/android/app/ve/theme/Download$Adapter;-><init>()V

    .line 117
    iput p2, p0, Lcom/sec/android/app/ve/theme/Theme$DownloadAdapter;->mThemePos:I

    .line 118
    return-void
.end method


# virtual methods
.method public getActivity()Landroid/app/Activity;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/sec/android/app/ve/theme/Theme$DownloadAdapter;->this$0:Lcom/sec/android/app/ve/theme/Theme;

    # getter for: Lcom/sec/android/app/ve/theme/Theme;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/sec/android/app/ve/theme/Theme;->access$1(Lcom/sec/android/app/ve/theme/Theme;)Landroid/app/Activity;

    move-result-object v0

    return-object v0
.end method

.method public getThemePosition()I
    .locals 1

    .prologue
    .line 133
    iget v0, p0, Lcom/sec/android/app/ve/theme/Theme$DownloadAdapter;->mThemePos:I

    return v0
.end method

.method public onThemeInstalled(IZ)V
    .locals 2
    .param p1, "themePos"    # I
    .param p2, "result"    # Z

    .prologue
    .line 122
    iget-object v0, p0, Lcom/sec/android/app/ve/theme/Theme$DownloadAdapter;->this$0:Lcom/sec/android/app/ve/theme/Theme;

    # getter for: Lcom/sec/android/app/ve/theme/Theme;->mThemeAdapter:Lcom/sec/android/app/ve/theme/ThemeManager$Adapter;
    invoke-static {v0}, Lcom/sec/android/app/ve/theme/Theme;->access$0(Lcom/sec/android/app/ve/theme/Theme;)Lcom/sec/android/app/ve/theme/ThemeManager$Adapter;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 123
    iget-object v0, p0, Lcom/sec/android/app/ve/theme/Theme$DownloadAdapter;->this$0:Lcom/sec/android/app/ve/theme/Theme;

    # getter for: Lcom/sec/android/app/ve/theme/Theme;->mThemeAdapter:Lcom/sec/android/app/ve/theme/ThemeManager$Adapter;
    invoke-static {v0}, Lcom/sec/android/app/ve/theme/Theme;->access$0(Lcom/sec/android/app/ve/theme/Theme;)Lcom/sec/android/app/ve/theme/ThemeManager$Adapter;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/ve/theme/Theme$DownloadAdapter;->mThemePos:I

    invoke-virtual {v0, v1, p2}, Lcom/sec/android/app/ve/theme/ThemeManager$Adapter;->onThemeInstalled(IZ)V

    .line 124
    :cond_0
    return-void
.end method
