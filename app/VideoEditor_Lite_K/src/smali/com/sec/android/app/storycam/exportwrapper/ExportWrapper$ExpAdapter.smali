.class Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExpAdapter;
.super Lcom/sec/android/app/ve/export/Export$Adapter;
.source "ExportWrapper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ExpAdapter"
.end annotation


# instance fields
.field private numberFormat:Ljava/text/NumberFormat;

.field final synthetic this$0:Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;)V
    .locals 1

    .prologue
    .line 248
    iput-object p1, p0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExpAdapter;->this$0:Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;

    invoke-direct {p0}, Lcom/sec/android/app/ve/export/Export$Adapter;-><init>()V

    .line 250
    invoke-static {}, Ljava/text/NumberFormat;->getInstance()Ljava/text/NumberFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExpAdapter;->numberFormat:Ljava/text/NumberFormat;

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExpAdapter;)V
    .locals 0

    .prologue
    .line 248
    invoke-direct {p0, p1}, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExpAdapter;-><init>(Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;)V

    return-void
.end method

.method static synthetic access$1(Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExpAdapter;)Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;
    .locals 1

    .prologue
    .line 248
    iget-object v0, p0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExpAdapter;->this$0:Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;

    return-object v0
.end method


# virtual methods
.method public clearNativeJobBeforeExport()Z
    .locals 1

    .prologue
    .line 253
    iget-object v0, p0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExpAdapter;->this$0:Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;

    iget-object v0, v0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;->mAdapter:Lcom/sec/android/app/ve/export/Export$Adapter;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/export/Export$Adapter;->clearNativeJobBeforeExport()Z

    move-result v0

    return v0
.end method

.method public getNotificationLayoutID()I
    .locals 2

    .prologue
    .line 372
    sget-object v0, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    const-string v1, "5"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 373
    const v0, 0x7f03000c

    .line 375
    :goto_0
    return v0

    :cond_0
    const v0, 0x7f03000b

    goto :goto_0
.end method

.method public getOngoingNotificationLayoutID()I
    .locals 2

    .prologue
    .line 381
    sget-object v0, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    const-string v1, "5"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 382
    const v0, 0x7f03000f

    .line 384
    :goto_0
    return v0

    :cond_0
    const v0, 0x7f03000e

    goto :goto_0
.end method

.method public getResolutionValue()I
    .locals 1

    .prologue
    .line 400
    const/4 v0, -0x1

    return v0
.end method

.method public initiateNotificationContentView(Landroid/content/Context;Landroid/widget/RemoteViews;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "contentView"    # Landroid/widget/RemoteViews;

    .prologue
    .line 389
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/ve/export/Export$Adapter;->initiateNotificationContentView(Landroid/content/Context;Landroid/widget/RemoteViews;)V

    .line 390
    return-void
.end method

.method public isPlayRequiredAfterExport()Z
    .locals 1

    .prologue
    .line 365
    iget-object v0, p0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExpAdapter;->this$0:Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;

    iget-object v0, v0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;->mAdapter:Lcom/sec/android/app/ve/export/Export$Adapter;

    if-eqz v0, :cond_0

    .line 366
    iget-object v0, p0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExpAdapter;->this$0:Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;

    iget-object v0, v0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;->mAdapter:Lcom/sec/android/app/ve/export/Export$Adapter;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/export/Export$Adapter;->isPlayRequiredAfterExport()Z

    move-result v0

    .line 367
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isShareViaCall()Z
    .locals 1

    .prologue
    .line 358
    iget-object v0, p0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExpAdapter;->this$0:Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;

    iget-object v0, v0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;->mAdapter:Lcom/sec/android/app/ve/export/Export$Adapter;

    if-eqz v0, :cond_0

    .line 359
    iget-object v0, p0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExpAdapter;->this$0:Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;

    iget-object v0, v0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;->mAdapter:Lcom/sec/android/app/ve/export/Export$Adapter;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/export/Export$Adapter;->isShareViaCall()Z

    move-result v0

    .line 360
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public nativeProcAfterExportPause()V
    .locals 1

    .prologue
    .line 278
    iget-object v0, p0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExpAdapter;->this$0:Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;

    iget-object v0, v0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;->mAdapter:Lcom/sec/android/app/ve/export/Export$Adapter;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/export/Export$Adapter;->nativeProcAfterExportPause()V

    .line 279
    return-void
.end method

.method public nativeProcBeforeExportResume()V
    .locals 1

    .prologue
    .line 274
    iget-object v0, p0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExpAdapter;->this$0:Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;

    iget-object v0, v0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;->mAdapter:Lcom/sec/android/app/ve/export/Export$Adapter;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/export/Export$Adapter;->nativeProcBeforeExportResume()V

    .line 275
    return-void
.end method

.method public onExportCompleted()V
    .locals 6

    .prologue
    .line 321
    :try_start_0
    # getter for: Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment;->mProgView:Landroid/widget/ProgressBar;
    invoke-static {}, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment;->access$2()Landroid/widget/ProgressBar;

    move-result-object v2

    new-instance v3, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExpAdapter$1;

    invoke-direct {v3, p0}, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExpAdapter$1;-><init>(Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExpAdapter;)V

    .line 330
    const-wide/16 v4, 0x3e8

    .line 321
    invoke-virtual {v2, v3, v4, v5}, Landroid/widget/ProgressBar;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 331
    const v2, 0x7f070244

    invoke-static {v2}, Lcom/sec/android/app/ve/VEApp;->getStringValue(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExpAdapter;->this$0:Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;

    invoke-virtual {v5}, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;->getExportFolderName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 333
    .local v1, "exportSaveLoc":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExpAdapter;->this$0:Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;

    invoke-virtual {v2}, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v1, v3}, Lcom/sec/android/app/ve/VEApp;->showToast(Landroid/content/Context;Ljava/lang/String;I)V

    .line 334
    iget-object v2, p0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExpAdapter;->this$0:Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;

    iget-object v2, v2, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;->mAdapter:Lcom/sec/android/app/ve/export/Export$Adapter;

    invoke-virtual {v2}, Lcom/sec/android/app/ve/export/Export$Adapter;->onExportCompleted()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 339
    .end local v1    # "exportSaveLoc":Ljava/lang/String;
    :goto_0
    return-void

    .line 336
    :catch_0
    move-exception v0

    .line 337
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public onExportFailed()V
    .locals 4

    .prologue
    .line 309
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExpAdapter;->this$0:Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;

    invoke-virtual {v1}, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const v2, 0x7f07031b

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/ve/VEApp;->showToast(Landroid/content/Context;II)Landroid/widget/Toast;

    .line 310
    iget-object v1, p0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExpAdapter;->this$0:Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;

    invoke-virtual {v1}, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/ve/common/AndroidUtils;->dismissDialog(Landroid/app/Activity;)V

    .line 311
    iget-object v1, p0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExpAdapter;->this$0:Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;

    iget-object v1, v1, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;->mAdapter:Lcom/sec/android/app/ve/export/Export$Adapter;

    if-eqz v1, :cond_0

    .line 312
    iget-object v1, p0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExpAdapter;->this$0:Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;

    iget-object v1, v1, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;->mAdapter:Lcom/sec/android/app/ve/export/Export$Adapter;

    invoke-virtual {v1}, Lcom/sec/android/app/ve/export/Export$Adapter;->onExportFailed()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 316
    :cond_0
    :goto_0
    return-void

    .line 313
    :catch_0
    move-exception v0

    .line 314
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public onExportFinished(ZLandroid/net/Uri;)V
    .locals 1
    .param p1, "playSel"    # Z
    .param p2, "fileUri"    # Landroid/net/Uri;

    .prologue
    .line 342
    iget-object v0, p0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExpAdapter;->this$0:Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;

    iget-object v0, v0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;->mAdapter:Lcom/sec/android/app/ve/export/Export$Adapter;

    if-eqz v0, :cond_0

    .line 343
    iget-object v0, p0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExpAdapter;->this$0:Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;

    iget-object v0, v0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;->mAdapter:Lcom/sec/android/app/ve/export/Export$Adapter;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/ve/export/Export$Adapter;->onExportFinished(ZLandroid/net/Uri;)V

    .line 345
    :cond_0
    return-void
.end method

.method public onExportPaused()V
    .locals 3

    .prologue
    .line 267
    # getter for: Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment;->mProgDlg:Landroid/app/AlertDialog;
    invoke-static {}, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment;->access$1()Landroid/app/AlertDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 268
    # getter for: Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment;->mProgDlg:Landroid/app/AlertDialog;
    invoke-static {}, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment;->access$1()Landroid/app/AlertDialog;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    const v1, 0x7f0700c7

    invoke-static {v1}, Lcom/sec/android/app/ve/VEApp;->getStringValue(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 269
    :cond_0
    # getter for: Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment;->mPauseText:Landroid/widget/TextView;
    invoke-static {}, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment;->access$0()Landroid/widget/TextView;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 270
    # getter for: Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment;->mPauseText:Landroid/widget/TextView;
    invoke-static {}, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment;->access$0()Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const v2, 0x7f07007f

    invoke-static {v2}, Lcom/sec/android/app/ve/VEApp;->getStringValue(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "..."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 271
    :cond_1
    return-void
.end method

.method public onExportProgressUpdate(I)V
    .locals 6
    .param p1, "progress"    # I

    .prologue
    .line 348
    # getter for: Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment;->mProgView:Landroid/widget/ProgressBar;
    invoke-static {}, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment;->access$2()Landroid/widget/ProgressBar;

    move-result-object v0

    if-eqz v0, :cond_0

    # getter for: Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment;->mProgDlg:Landroid/app/AlertDialog;
    invoke-static {}, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment;->access$1()Landroid/app/AlertDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 350
    # getter for: Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment;->mProgView:Landroid/widget/ProgressBar;
    invoke-static {}, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment;->access$2()Landroid/widget/ProgressBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 351
    # getter for: Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment;->mProgText:Landroid/widget/TextView;
    invoke-static {}, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment;->access$3()Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExpAdapter;->numberFormat:Ljava/text/NumberFormat;

    int-to-long v4, p1

    invoke-virtual {v2, v4, v5}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 354
    :cond_0
    return-void
.end method

.method public onExportResumed()V
    .locals 2

    .prologue
    .line 282
    # getter for: Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment;->mProgDlg:Landroid/app/AlertDialog;
    invoke-static {}, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment;->access$1()Landroid/app/AlertDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 283
    # getter for: Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment;->mProgDlg:Landroid/app/AlertDialog;
    invoke-static {}, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment;->access$1()Landroid/app/AlertDialog;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    const v1, 0x7f0700ae

    invoke-static {v1}, Lcom/sec/android/app/ve/VEApp;->getStringValue(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 284
    :cond_0
    # getter for: Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment;->mPauseText:Landroid/widget/TextView;
    invoke-static {}, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment;->access$0()Landroid/widget/TextView;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 285
    # getter for: Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment;->mPauseText:Landroid/widget/TextView;
    invoke-static {}, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment;->access$0()Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f070326

    invoke-static {v1}, Lcom/sec/android/app/ve/VEApp;->getStringValue(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 287
    :cond_1
    return-void
.end method

.method public onExportStarted()V
    .locals 2

    .prologue
    .line 258
    iget-object v0, p0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExpAdapter;->this$0:Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;

    invoke-static {v0}, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment;->showDialog(Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;)Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment;

    .line 259
    # getter for: Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment;->mProgDlg:Landroid/app/AlertDialog;
    invoke-static {}, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment;->access$1()Landroid/app/AlertDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 260
    # getter for: Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment;->mProgDlg:Landroid/app/AlertDialog;
    invoke-static {}, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment;->access$1()Landroid/app/AlertDialog;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    const v1, 0x7f0700ae

    invoke-static {v1}, Lcom/sec/android/app/ve/VEApp;->getStringValue(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 261
    :cond_0
    # getter for: Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment;->mPauseText:Landroid/widget/TextView;
    invoke-static {}, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment;->access$0()Landroid/widget/TextView;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 262
    # getter for: Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment;->mPauseText:Landroid/widget/TextView;
    invoke-static {}, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment;->access$0()Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f070326

    invoke-static {v1}, Lcom/sec/android/app/ve/VEApp;->getStringValue(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 263
    :cond_1
    return-void
.end method

.method public onExportStopped()V
    .locals 4

    .prologue
    .line 298
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExpAdapter;->this$0:Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;

    invoke-virtual {v1}, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const v2, 0x7f07031d

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/ve/VEApp;->showToast(Landroid/content/Context;II)Landroid/widget/Toast;

    .line 299
    iget-object v1, p0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExpAdapter;->this$0:Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;

    invoke-virtual {v1}, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/ve/common/AndroidUtils;->dismissDialog(Landroid/app/Activity;)V

    .line 300
    iget-object v1, p0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExpAdapter;->this$0:Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;

    iget-object v1, v1, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;->mAdapter:Lcom/sec/android/app/ve/export/Export$Adapter;

    if-eqz v1, :cond_0

    .line 301
    iget-object v1, p0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExpAdapter;->this$0:Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;

    iget-object v1, v1, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;->mAdapter:Lcom/sec/android/app/ve/export/Export$Adapter;

    invoke-virtual {v1}, Lcom/sec/android/app/ve/export/Export$Adapter;->onExportStopped()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 305
    :cond_0
    :goto_0
    return-void

    .line 302
    :catch_0
    move-exception v0

    .line 303
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public onScanCompletedAfterExport(Landroid/net/Uri;)V
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 291
    iget-object v0, p0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExpAdapter;->this$0:Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;

    iget-object v0, v0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;->mAdapter:Lcom/sec/android/app/ve/export/Export$Adapter;

    if-eqz v0, :cond_0

    .line 292
    iget-object v0, p0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExpAdapter;->this$0:Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;

    iget-object v0, v0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;->mAdapter:Lcom/sec/android/app/ve/export/Export$Adapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/ve/export/Export$Adapter;->onExportFinished(ZLandroid/net/Uri;)V

    .line 293
    :cond_0
    return-void
.end method

.method public updateNotificationContentView(Landroid/content/Context;Landroid/widget/RemoteViews;II)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "contentView"    # Landroid/widget/RemoteViews;
    .param p3, "progress"    # I
    .param p4, "state"    # I

    .prologue
    .line 394
    invoke-super {p0, p1, p2, p3, p4}, Lcom/sec/android/app/ve/export/Export$Adapter;->updateNotificationContentView(Landroid/content/Context;Landroid/widget/RemoteViews;II)V

    .line 395
    return-void
.end method
