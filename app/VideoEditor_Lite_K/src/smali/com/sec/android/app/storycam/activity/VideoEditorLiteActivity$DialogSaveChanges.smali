.class public Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogSaveChanges;
.super Lcom/sec/android/app/ve/common/AndroidUtils$DlgFragment;
.source "VideoEditorLiteActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DialogSaveChanges"
.end annotation


# instance fields
.field private activity:Landroid/app/Activity;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 939
    invoke-direct {p0}, Lcom/sec/android/app/ve/common/AndroidUtils$DlgFragment;-><init>()V

    .line 940
    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;)V
    .locals 0
    .param p1, "acvt"    # Landroid/app/Activity;

    .prologue
    .line 942
    invoke-direct {p0}, Lcom/sec/android/app/ve/common/AndroidUtils$DlgFragment;-><init>()V

    .line 943
    iput-object p1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogSaveChanges;->activity:Landroid/app/Activity;

    .line 944
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogSaveChanges;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 936
    iget-object v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogSaveChanges;->activity:Landroid/app/Activity;

    return-object v0
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 952
    invoke-super {p0, p1}, Lcom/sec/android/app/ve/common/AndroidUtils$DlgFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 953
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogSaveChanges;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogSaveChanges;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p1, Landroid/content/res/Configuration;->fontScale:F

    # getter for: Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->fontScale_value:F
    invoke-static {}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->access$7()F

    move-result v1

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    .line 954
    iget v0, p1, Landroid/content/res/Configuration;->fontScale:F

    invoke-static {v0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->access$8(F)V

    .line 955
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogSaveChanges;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/ve/common/AndroidUtils;->dismissDialog(Landroid/app/Activity;)V

    .line 958
    :cond_0
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 961
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogSaveChanges;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 962
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    const v3, 0x7f070220

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 963
    const v3, 0x7f07021f

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 966
    sget v3, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->VERSION_CODE:I

    const/16 v4, 0x15

    if-lt v3, v4, :cond_0

    .line 968
    const v1, 0x7f070221

    .line 969
    .local v1, "cancel1":I
    const v2, 0x7f07005e

    .line 977
    .local v2, "discard1":I
    :goto_0
    const v3, 0x7f070222

    new-instance v4, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogSaveChanges$1;

    invoke-direct {v4, p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogSaveChanges$1;-><init>(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogSaveChanges;)V

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 988
    new-instance v4, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogSaveChanges$2;

    invoke-direct {v4, p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogSaveChanges$2;-><init>(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogSaveChanges;)V

    invoke-virtual {v3, v2, v4}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 1003
    new-instance v4, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogSaveChanges$3;

    invoke-direct {v4, p0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogSaveChanges$3;-><init>(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogSaveChanges;)V

    invoke-virtual {v3, v1, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1015
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    return-object v3

    .line 973
    .end local v1    # "cancel1":I
    .end local v2    # "discard1":I
    :cond_0
    const v1, 0x7f07005e

    .line 974
    .restart local v1    # "cancel1":I
    const v2, 0x7f070221

    .restart local v2    # "discard1":I
    goto :goto_0
.end method

.method public onDestroyView()V
    .locals 0

    .prologue
    .line 948
    invoke-super {p0}, Lcom/sec/android/app/ve/common/AndroidUtils$DlgFragment;->onDestroyView()V

    .line 949
    return-void
.end method
