.class public abstract Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework;
.super Ljava/lang/Object;
.source "ProjectPluginFramework.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework$AnimationPlugin;,
        Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework$AudioEffectPlugin;,
        Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework$AudioTransitionPlugin;,
        Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework$CaptionPlugin;,
        Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework$MediaEffectsPlugin;,
        Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework$MediaElementPlugin;,
        Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework$MediaTransitionPlugin;,
        Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework$ThemesPlugin;
    }
.end annotation


# instance fields
.field protected mAnimationPlugin:Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework$AnimationPlugin;

.field protected mAudioEffectPlugin:Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework$AudioEffectPlugin;

.field protected mAudioTransitionPlugin:Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework$AudioTransitionPlugin;

.field protected mCaptionPlugin:Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework$CaptionPlugin;

.field protected mMediaEffectsPlugin:Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework$MediaEffectsPlugin;

.field protected mMediaElementPlugin:Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework$MediaElementPlugin;

.field protected mMediaTransitionPlugin:Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework$MediaTransitionPlugin;

.field protected mThemesPlugin:Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework$ThemesPlugin;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
