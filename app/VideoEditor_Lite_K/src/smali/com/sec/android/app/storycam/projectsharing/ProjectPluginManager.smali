.class public Lcom/sec/android/app/storycam/projectsharing/ProjectPluginManager;
.super Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework;
.source "ProjectPluginManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/storycam/projectsharing/ProjectPluginManager$AnimationPluginImpl;,
        Lcom/sec/android/app/storycam/projectsharing/ProjectPluginManager$AudioEffectPluginImpl;,
        Lcom/sec/android/app/storycam/projectsharing/ProjectPluginManager$AudioTransitionPluginImpl;,
        Lcom/sec/android/app/storycam/projectsharing/ProjectPluginManager$CaptionPluginImpl;,
        Lcom/sec/android/app/storycam/projectsharing/ProjectPluginManager$MediaEffectsPluginImpl;,
        Lcom/sec/android/app/storycam/projectsharing/ProjectPluginManager$MediaElementPluginImpl;,
        Lcom/sec/android/app/storycam/projectsharing/ProjectPluginManager$MediaTransitionPluginImpl;,
        Lcom/sec/android/app/storycam/projectsharing/ProjectPluginManager$ThemesPluginImpl;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework;-><init>()V

    .line 30
    new-instance v0, Lcom/sec/android/app/storycam/projectsharing/ProjectPluginManager$MediaElementPluginImpl;

    invoke-direct {v0}, Lcom/sec/android/app/storycam/projectsharing/ProjectPluginManager$MediaElementPluginImpl;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/storycam/projectsharing/ProjectPluginManager;->mMediaElementPlugin:Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework$MediaElementPlugin;

    .line 31
    new-instance v0, Lcom/sec/android/app/storycam/projectsharing/ProjectPluginManager$MediaEffectsPluginImpl;

    invoke-direct {v0}, Lcom/sec/android/app/storycam/projectsharing/ProjectPluginManager$MediaEffectsPluginImpl;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/storycam/projectsharing/ProjectPluginManager;->mMediaEffectsPlugin:Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework$MediaEffectsPlugin;

    .line 32
    new-instance v0, Lcom/sec/android/app/storycam/projectsharing/ProjectPluginManager$MediaTransitionPluginImpl;

    invoke-direct {v0}, Lcom/sec/android/app/storycam/projectsharing/ProjectPluginManager$MediaTransitionPluginImpl;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/storycam/projectsharing/ProjectPluginManager;->mMediaTransitionPlugin:Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework$MediaTransitionPlugin;

    .line 33
    new-instance v0, Lcom/sec/android/app/storycam/projectsharing/ProjectPluginManager$AudioEffectPluginImpl;

    invoke-direct {v0}, Lcom/sec/android/app/storycam/projectsharing/ProjectPluginManager$AudioEffectPluginImpl;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/storycam/projectsharing/ProjectPluginManager;->mAudioEffectPlugin:Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework$AudioEffectPlugin;

    .line 34
    new-instance v0, Lcom/sec/android/app/storycam/projectsharing/ProjectPluginManager$AudioTransitionPluginImpl;

    invoke-direct {v0}, Lcom/sec/android/app/storycam/projectsharing/ProjectPluginManager$AudioTransitionPluginImpl;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/storycam/projectsharing/ProjectPluginManager;->mAudioTransitionPlugin:Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework$AudioTransitionPlugin;

    .line 35
    new-instance v0, Lcom/sec/android/app/storycam/projectsharing/ProjectPluginManager$CaptionPluginImpl;

    invoke-direct {v0}, Lcom/sec/android/app/storycam/projectsharing/ProjectPluginManager$CaptionPluginImpl;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/storycam/projectsharing/ProjectPluginManager;->mCaptionPlugin:Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework$CaptionPlugin;

    .line 36
    new-instance v0, Lcom/sec/android/app/storycam/projectsharing/ProjectPluginManager$ThemesPluginImpl;

    invoke-direct {v0}, Lcom/sec/android/app/storycam/projectsharing/ProjectPluginManager$ThemesPluginImpl;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/storycam/projectsharing/ProjectPluginManager;->mThemesPlugin:Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework$ThemesPlugin;

    .line 37
    new-instance v0, Lcom/sec/android/app/storycam/projectsharing/ProjectPluginManager$AnimationPluginImpl;

    invoke-direct {v0}, Lcom/sec/android/app/storycam/projectsharing/ProjectPluginManager$AnimationPluginImpl;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/storycam/projectsharing/ProjectPluginManager;->mAnimationPlugin:Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework$AnimationPlugin;

    .line 38
    return-void
.end method
