.class public abstract Lcom/sec/android/app/ve/PreviewPlayerInterface$Adapter;
.super Ljava/lang/Object;
.source "PreviewPlayerInterface.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/ve/PreviewPlayerInterface;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Adapter"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract getCurrentTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;
.end method

.method public onAudioFocusLost()V
    .locals 0

    .prologue
    .line 64
    return-void
.end method

.method public onCompleted()V
    .locals 0

    .prologue
    .line 60
    return-void
.end method

.method public onError()V
    .locals 0

    .prologue
    .line 68
    return-void
.end method

.method public onPlayed()V
    .locals 0

    .prologue
    .line 58
    return-void
.end method

.method public onSeeked(J)V
    .locals 0
    .param p1, "pos"    # J

    .prologue
    .line 66
    return-void
.end method

.method public onStopped()V
    .locals 0

    .prologue
    .line 62
    return-void
.end method

.method public onSurfaceChanged()V
    .locals 0

    .prologue
    .line 72
    return-void
.end method

.method public onSurfaceCreated()V
    .locals 0

    .prologue
    .line 70
    return-void
.end method
