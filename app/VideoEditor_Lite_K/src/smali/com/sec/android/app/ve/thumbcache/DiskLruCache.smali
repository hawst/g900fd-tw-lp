.class public Lcom/sec/android/app/ve/thumbcache/DiskLruCache;
.super Landroid/util/LruCache;
.source "DiskLruCache.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/ve/thumbcache/DiskLruCache$LocalCache;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/util/LruCache",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;",
        "Ljava/io/Serializable;"
    }
.end annotation


# static fields
.field private static final DATE_PATTERN:Ljava/lang/String; = "MM/dd/yyyy HH:mm:ss"

.field private static final DATE_PATTERN_LOCALE:Ljava/util/Locale;

.field private static final LOCAL_CACHE_SIZE:I = 0xc8

.field public static final PERS_CACHE_FILE:Ljava/lang/String; = "VEAddMediaThumbnailCache.ser"

.field private static final PERS_CACHE_SIZE:I = 0x1f4

.field public static final RAW_THUMB_FILE_EXT:Ljava/lang/String; = ".thumb"

.field private static final mInstance:Lcom/sec/android/app/ve/thumbcache/DiskLruCache;

.field private static final serialVersionUID:J = 0x7849f4d7e2cf6319L


# instance fields
.field private final transient mLocalCache:Lcom/sec/android/app/ve/thumbcache/DiskLruCache$LocalCache;

.field private final transient mTimelineCache:Lcom/sec/android/app/ve/thumbcache/DiskLruCache$LocalCache;

.field private mVibrantColorCache:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 36
    new-instance v0, Lcom/sec/android/app/ve/thumbcache/DiskLruCache;

    const/16 v1, 0x1f4

    invoke-direct {v0, v1}, Lcom/sec/android/app/ve/thumbcache/DiskLruCache;-><init>(I)V

    sput-object v0, Lcom/sec/android/app/ve/thumbcache/DiskLruCache;->mInstance:Lcom/sec/android/app/ve/thumbcache/DiskLruCache;

    .line 38
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    sput-object v0, Lcom/sec/android/app/ve/thumbcache/DiskLruCache;->DATE_PATTERN_LOCALE:Ljava/util/Locale;

    .line 46
    invoke-static {}, Lcom/sec/android/app/ve/thumbcache/DiskLruCache;->loadFromDisk()Z

    .line 47
    return-void
.end method

.method private constructor <init>(I)V
    .locals 2
    .param p1, "maxSize"    # I

    .prologue
    .line 50
    invoke-direct {p0, p1}, Landroid/util/LruCache;-><init>(I)V

    .line 40
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/ve/thumbcache/DiskLruCache;->mVibrantColorCache:Ljava/util/LinkedHashMap;

    .line 42
    new-instance v0, Lcom/sec/android/app/ve/thumbcache/DiskLruCache$LocalCache;

    const/16 v1, 0xc8

    invoke-direct {v0, v1}, Lcom/sec/android/app/ve/thumbcache/DiskLruCache$LocalCache;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/ve/thumbcache/DiskLruCache;->mLocalCache:Lcom/sec/android/app/ve/thumbcache/DiskLruCache$LocalCache;

    .line 43
    new-instance v0, Lcom/sec/android/app/ve/thumbcache/DiskLruCache$LocalCache;

    const/16 v1, 0x96

    invoke-direct {v0, v1}, Lcom/sec/android/app/ve/thumbcache/DiskLruCache$LocalCache;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/ve/thumbcache/DiskLruCache;->mTimelineCache:Lcom/sec/android/app/ve/thumbcache/DiskLruCache$LocalCache;

    .line 51
    return-void
.end method

.method private static deleteUnrefFiles(Ljava/util/Collection;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 260
    .local p0, "values":Ljava/util/Collection;, "Ljava/util/Collection<*>;"
    new-instance v3, Ljava/io/File;

    const-string v4, ""

    invoke-static {v4}, Lcom/sec/android/app/ve/VEApp;->getFullySpecifiedInternalFilepath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 262
    .local v3, "root":Ljava/io/File;
    new-instance v4, Lcom/sec/android/app/ve/thumbcache/DiskLruCache$1;

    invoke-direct {v4}, Lcom/sec/android/app/ve/thumbcache/DiskLruCache$1;-><init>()V

    invoke-virtual {v3, v4}, Ljava/io/File;->listFiles(Ljava/io/FileFilter;)[Ljava/io/File;

    move-result-object v1

    .line 276
    .local v1, "files":[Ljava/io/File;
    if-eqz v1, :cond_0

    .line 277
    array-length v5, v1

    const/4 v4, 0x0

    :goto_0
    if-lt v4, v5, :cond_1

    .line 293
    :cond_0
    return-void

    .line 277
    :cond_1
    aget-object v0, v1, v4

    .line 278
    .local v0, "file":Ljava/io/File;
    if-nez v0, :cond_3

    .line 277
    :cond_2
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 281
    :cond_3
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    .line 283
    .local v2, "path":Ljava/lang/String;
    if-eqz v2, :cond_2

    .line 286
    invoke-interface {p0, v2}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 287
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    goto :goto_1
.end method

.method public static getInstance()Lcom/sec/android/app/ve/thumbcache/DiskLruCache;
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lcom/sec/android/app/ve/thumbcache/DiskLruCache;->mInstance:Lcom/sec/android/app/ve/thumbcache/DiskLruCache;

    return-object v0
.end method

.method public static loadFromDisk()Z
    .locals 11

    .prologue
    .line 58
    const/4 v3, 0x0

    .line 60
    .local v3, "isLoaded":Z
    :try_start_0
    const-string v10, "VEAddMediaThumbnailCache.ser"

    invoke-static {v10}, Lcom/sec/android/app/ve/VEApp;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;

    move-result-object v2

    .line 61
    .local v2, "fis":Ljava/io/FileInputStream;
    if-eqz v2, :cond_1

    .line 62
    const/4 v8, 0x0

    .line 64
    .local v8, "ois":Ljava/io/ObjectInputStream;
    new-instance v8, Ljava/io/ObjectInputStream;

    .end local v8    # "ois":Ljava/io/ObjectInputStream;
    invoke-direct {v8, v2}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V

    .line 65
    .restart local v8    # "ois":Ljava/io/ObjectInputStream;
    invoke-virtual {v8}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v7

    .line 66
    .local v7, "obj":Ljava/lang/Object;
    invoke-virtual {v8}, Ljava/io/ObjectInputStream;->close()V

    .line 67
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V

    .line 69
    instance-of v10, v7, Ljava/util/LinkedHashMap;

    if-eqz v10, :cond_1

    .line 70
    move-object v0, v7

    check-cast v0, Ljava/util/LinkedHashMap;

    move-object v6, v0

    .line 71
    .local v6, "map":Ljava/util/LinkedHashMap;, "Ljava/util/LinkedHashMap<**>;"
    invoke-virtual {v6}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .line 72
    .local v5, "keys":Ljava/util/Iterator;, "Ljava/util/Iterator<*>;"
    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-nez v10, :cond_2

    .line 79
    invoke-virtual {v6}, Ljava/util/LinkedHashMap;->clear()V

    .line 80
    const/4 v3, 0x1

    .line 82
    invoke-virtual {v6}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v10

    invoke-static {v10}, Lcom/sec/android/app/ve/thumbcache/DiskLruCache;->deleteUnrefFiles(Ljava/util/Collection;)V

    .line 90
    .end local v2    # "fis":Ljava/io/FileInputStream;
    .end local v5    # "keys":Ljava/util/Iterator;, "Ljava/util/Iterator<*>;"
    .end local v6    # "map":Ljava/util/LinkedHashMap;, "Ljava/util/LinkedHashMap<**>;"
    .end local v7    # "obj":Ljava/lang/Object;
    .end local v8    # "ois":Ljava/io/ObjectInputStream;
    :cond_1
    :goto_1
    return v3

    .line 73
    .restart local v2    # "fis":Ljava/io/FileInputStream;
    .restart local v5    # "keys":Ljava/util/Iterator;, "Ljava/util/Iterator<*>;"
    .restart local v6    # "map":Ljava/util/LinkedHashMap;, "Ljava/util/LinkedHashMap<**>;"
    .restart local v7    # "obj":Ljava/lang/Object;
    .restart local v8    # "ois":Ljava/io/ObjectInputStream;
    :cond_2
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 74
    .local v4, "key":Ljava/lang/String;
    invoke-virtual {v6, v4}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    .line 75
    .local v9, "value":Ljava/lang/String;
    if-eqz v4, :cond_0

    if-eqz v9, :cond_0

    .line 76
    sget-object v10, Lcom/sec/android/app/ve/thumbcache/DiskLruCache;->mInstance:Lcom/sec/android/app/ve/thumbcache/DiskLruCache;

    invoke-virtual {v10, v4, v9}, Lcom/sec/android/app/ve/thumbcache/DiskLruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 85
    .end local v2    # "fis":Ljava/io/FileInputStream;
    .end local v4    # "key":Ljava/lang/String;
    .end local v5    # "keys":Ljava/util/Iterator;, "Ljava/util/Iterator<*>;"
    .end local v6    # "map":Ljava/util/LinkedHashMap;, "Ljava/util/LinkedHashMap<**>;"
    .end local v7    # "obj":Ljava/lang/Object;
    .end local v8    # "ois":Ljava/io/ObjectInputStream;
    .end local v9    # "value":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 86
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 87
    const/4 v3, 0x0

    goto :goto_1
.end method


# virtual methods
.method public clearLocalCache()V
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/sec/android/app/ve/thumbcache/DiskLruCache;->mLocalCache:Lcom/sec/android/app/ve/thumbcache/DiskLruCache$LocalCache;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/thumbcache/DiskLruCache$LocalCache;->evictAll()V

    .line 109
    iget-object v0, p0, Lcom/sec/android/app/ve/thumbcache/DiskLruCache;->mTimelineCache:Lcom/sec/android/app/ve/thumbcache/DiskLruCache$LocalCache;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/thumbcache/DiskLruCache$LocalCache;->evictAll()V

    .line 110
    return-void
.end method

.method public bridge synthetic entryRemoved(ZLjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p2, Ljava/lang/String;

    check-cast p3, Ljava/lang/String;

    check-cast p4, Ljava/lang/String;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/sec/android/app/ve/thumbcache/DiskLruCache;->entryRemoved(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public entryRemoved(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "removed"    # Z
    .param p2, "key"    # Ljava/lang/String;
    .param p3, "oldValue"    # Ljava/lang/String;
    .param p4, "newValue"    # Ljava/lang/String;

    .prologue
    .line 251
    if-eqz p1, :cond_0

    .line 252
    new-instance v0, Ljava/io/File;

    invoke-static {p3}, Lcom/sec/android/app/ve/VEApp;->getFullySpecifiedInternalFilepath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 253
    .local v0, "fileToBeDeleted":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 254
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 257
    .end local v0    # "fileToBeDeleted":Ljava/io/File;
    :cond_0
    return-void
.end method

.method public getBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 12
    .param p1, "filepath"    # Ljava/lang/String;

    .prologue
    .line 175
    const/4 v2, 0x0

    .line 176
    .local v2, "bmp":Landroid/graphics/Bitmap;
    if-eqz p1, :cond_3

    .line 178
    :try_start_0
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v9, Ljava/text/SimpleDateFormat;

    const-string v10, "MM/dd/yyyy HH:mm:ss"

    sget-object v11, Lcom/sec/android/app/ve/thumbcache/DiskLruCache;->DATE_PATTERN_LOCALE:Ljava/util/Locale;

    invoke-direct {v9, v10, v11}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    new-instance v10, Ljava/io/File;

    invoke-direct {v10, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->lastModified()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 180
    .local v6, "key":Ljava/lang/String;
    iget-object v8, p0, Lcom/sec/android/app/ve/thumbcache/DiskLruCache;->mLocalCache:Lcom/sec/android/app/ve/thumbcache/DiskLruCache$LocalCache;

    invoke-virtual {v8, v6}, Lcom/sec/android/app/ve/thumbcache/DiskLruCache$LocalCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    move-object v0, v8

    check-cast v0, Landroid/graphics/Bitmap;

    move-object v2, v0

    .line 181
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v8

    if-nez v8, :cond_0

    move-object v3, v2

    .line 204
    .end local v2    # "bmp":Landroid/graphics/Bitmap;
    .end local v6    # "key":Ljava/lang/String;
    .local v3, "bmp":Landroid/graphics/Bitmap;
    :goto_0
    return-object v3

    .line 184
    .end local v3    # "bmp":Landroid/graphics/Bitmap;
    .restart local v2    # "bmp":Landroid/graphics/Bitmap;
    .restart local v6    # "key":Ljava/lang/String;
    :cond_0
    invoke-virtual {p0, v6}, Lcom/sec/android/app/ve/thumbcache/DiskLruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 185
    .local v7, "rawThumbPath":Ljava/lang/String;
    if-eqz v7, :cond_1

    .line 186
    invoke-static {v7}, Lcom/sec/android/app/ve/VEApp;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;

    move-result-object v5

    .line 187
    .local v5, "fis":Ljava/io/FileInputStream;
    if-eqz v5, :cond_1

    .line 188
    invoke-static {v5}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 189
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V

    .line 193
    .end local v5    # "fis":Ljava/io/FileInputStream;
    :cond_1
    if-eqz v2, :cond_2

    .line 194
    iget-object v8, p0, Lcom/sec/android/app/ve/thumbcache/DiskLruCache;->mLocalCache:Lcom/sec/android/app/ve/thumbcache/DiskLruCache$LocalCache;

    invoke-virtual {v8, v6, v2}, Lcom/sec/android/app/ve/thumbcache/DiskLruCache$LocalCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .end local v6    # "key":Ljava/lang/String;
    .end local v7    # "rawThumbPath":Ljava/lang/String;
    :cond_2
    :goto_1
    move-object v3, v2

    .line 204
    .end local v2    # "bmp":Landroid/graphics/Bitmap;
    .restart local v3    # "bmp":Landroid/graphics/Bitmap;
    goto :goto_0

    .line 196
    .end local v3    # "bmp":Landroid/graphics/Bitmap;
    .restart local v2    # "bmp":Landroid/graphics/Bitmap;
    :catch_0
    move-exception v4

    .line 197
    .local v4, "e":Ljava/lang/Exception;
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 201
    .end local v4    # "e":Ljava/lang/Exception;
    :cond_3
    const-string v8, "No bitmap in cache or writing bitmap to file task is not yet completed ******"

    invoke-static {v8}, Lcom/sec/android/app/ve/common/LogUtils;->log(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public getTimelineBitmap(Ljava/lang/String;Ljava/lang/String;Z)Landroid/graphics/Bitmap;
    .locals 12
    .param p1, "filepath"    # Ljava/lang/String;
    .param p2, "time"    # Ljava/lang/String;
    .param p3, "readFromPersistentCache"    # Z

    .prologue
    .line 208
    const/4 v2, 0x0

    .line 209
    .local v2, "bmp":Landroid/graphics/Bitmap;
    if-eqz p1, :cond_3

    .line 211
    :try_start_0
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v9, "?"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    new-instance v9, Ljava/text/SimpleDateFormat;

    const-string v10, "MM/dd/yyyy HH:mm:ss"

    sget-object v11, Lcom/sec/android/app/ve/thumbcache/DiskLruCache;->DATE_PATTERN_LOCALE:Ljava/util/Locale;

    invoke-direct {v9, v10, v11}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    new-instance v10, Ljava/io/File;

    invoke-direct {v10, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->lastModified()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 213
    .local v6, "key":Ljava/lang/String;
    const-string v8, "BITMAP"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Trying to get Bitmap with time: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " from timeline cache"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    iget-object v8, p0, Lcom/sec/android/app/ve/thumbcache/DiskLruCache;->mTimelineCache:Lcom/sec/android/app/ve/thumbcache/DiskLruCache$LocalCache;

    invoke-virtual {v8, v6}, Lcom/sec/android/app/ve/thumbcache/DiskLruCache$LocalCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    move-object v0, v8

    check-cast v0, Landroid/graphics/Bitmap;

    move-object v2, v0

    .line 215
    const-string v8, "Abhishek"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "from cache:"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 216
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v8

    if-nez v8, :cond_0

    move-object v3, v2

    .line 246
    .end local v2    # "bmp":Landroid/graphics/Bitmap;
    .end local v6    # "key":Ljava/lang/String;
    .local v3, "bmp":Landroid/graphics/Bitmap;
    :goto_0
    return-object v3

    .line 221
    .end local v3    # "bmp":Landroid/graphics/Bitmap;
    .restart local v2    # "bmp":Landroid/graphics/Bitmap;
    .restart local v6    # "key":Ljava/lang/String;
    :cond_0
    if-eqz p3, :cond_2

    .line 223
    invoke-virtual {p0, v6}, Lcom/sec/android/app/ve/thumbcache/DiskLruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 224
    .local v7, "rawThumbPath":Ljava/lang/String;
    const-string v8, "Abhishek"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "rawThumbPath:"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 225
    if-eqz v7, :cond_1

    .line 226
    invoke-static {v7}, Lcom/sec/android/app/ve/VEApp;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;

    move-result-object v5

    .line 227
    .local v5, "fis":Ljava/io/FileInputStream;
    if-eqz v5, :cond_1

    .line 228
    invoke-static {v5}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 229
    const-string v8, "Abhishek"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "from persistent:"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 230
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V

    .line 234
    .end local v5    # "fis":Ljava/io/FileInputStream;
    :cond_1
    if-eqz v2, :cond_2

    .line 235
    iget-object v8, p0, Lcom/sec/android/app/ve/thumbcache/DiskLruCache;->mTimelineCache:Lcom/sec/android/app/ve/thumbcache/DiskLruCache$LocalCache;

    invoke-virtual {v8, v6, v2}, Lcom/sec/android/app/ve/thumbcache/DiskLruCache$LocalCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .end local v6    # "key":Ljava/lang/String;
    .end local v7    # "rawThumbPath":Ljava/lang/String;
    :cond_2
    :goto_1
    move-object v3, v2

    .line 246
    .end local v2    # "bmp":Landroid/graphics/Bitmap;
    .restart local v3    # "bmp":Landroid/graphics/Bitmap;
    goto :goto_0

    .line 238
    .end local v3    # "bmp":Landroid/graphics/Bitmap;
    .restart local v2    # "bmp":Landroid/graphics/Bitmap;
    :catch_0
    move-exception v4

    .line 239
    .local v4, "e":Ljava/lang/Exception;
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 243
    .end local v4    # "e":Ljava/lang/Exception;
    :cond_3
    const-string v8, "No bitmap in cache or writing bitmap to file task is not yet completed ******"

    invoke-static {v8}, Lcom/sec/android/app/ve/common/LogUtils;->log(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public getVibrantColorOfMediaThumbnail(Ljava/lang/String;)I
    .locals 3
    .param p1, "filepath"    # Ljava/lang/String;

    .prologue
    .line 346
    iget-object v2, p0, Lcom/sec/android/app/ve/thumbcache/DiskLruCache;->mVibrantColorCache:Ljava/util/LinkedHashMap;

    monitor-enter v2

    .line 347
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/ve/thumbcache/DiskLruCache;->mVibrantColorCache:Ljava/util/LinkedHashMap;

    invoke-virtual {v1, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 348
    .local v0, "color":Ljava/lang/Integer;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    :goto_0
    monitor-exit v2

    return v1

    :cond_0
    const v1, -0x777778

    goto :goto_0

    .line 346
    .end local v0    # "color":Ljava/lang/Integer;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public putBitmap(Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 10
    .param p1, "filepath"    # Ljava/lang/String;
    .param p2, "bmp"    # Landroid/graphics/Bitmap;

    .prologue
    .line 113
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 143
    :cond_0
    :goto_0
    return-void

    .line 117
    :cond_1
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_2

    .line 118
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v5

    if-nez v5, :cond_0

    .line 119
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0

    .line 125
    :cond_2
    :try_start_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v6, Ljava/text/SimpleDateFormat;

    const-string v7, "MM/dd/yyyy HH:mm:ss"

    sget-object v8, Lcom/sec/android/app/ve/thumbcache/DiskLruCache;->DATE_PATTERN_LOCALE:Ljava/util/Locale;

    invoke-direct {v6, v7, v8}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    new-instance v7, Ljava/io/File;

    invoke-direct {v7, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/io/File;->lastModified()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 126
    .local v2, "key":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/app/ve/thumbcache/DiskLruCache;->mLocalCache:Lcom/sec/android/app/ve/thumbcache/DiskLruCache$LocalCache;

    invoke-virtual {v5, v2, p2}, Lcom/sec/android/app/ve/thumbcache/DiskLruCache$LocalCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 128
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, ".thumb"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 129
    .local v4, "rawThumbPath":Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/app/ve/VEApp;->openFileOutput(Ljava/lang/String;)Ljava/io/FileOutputStream;

    move-result-object v1

    .line 130
    .local v1, "fos":Ljava/io/FileOutputStream;
    if-eqz v1, :cond_3

    .line 131
    sget-object v5, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v6, 0x64

    invoke-virtual {p2, v5, v6, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 132
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    .line 134
    :cond_3
    invoke-virtual {p0, v2, v4}, Lcom/sec/android/app/ve/thumbcache/DiskLruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 136
    invoke-static {p2}, Landroid/support/v7/graphics/Palette;->generate(Landroid/graphics/Bitmap;)Landroid/support/v7/graphics/Palette;

    move-result-object v3

    .line 137
    .local v3, "palette":Landroid/support/v7/graphics/Palette;
    iget-object v6, p0, Lcom/sec/android/app/ve/thumbcache/DiskLruCache;->mVibrantColorCache:Ljava/util/LinkedHashMap;

    monitor-enter v6
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 138
    :try_start_1
    iget-object v5, p0, Lcom/sec/android/app/ve/thumbcache/DiskLruCache;->mVibrantColorCache:Ljava/util/LinkedHashMap;

    const v7, -0x777778

    invoke-virtual {v3, v7}, Landroid/support/v7/graphics/Palette;->getDarkVibrantColor(I)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, p1, v7}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 137
    monitor-exit v6

    goto/16 :goto_0

    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v5
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 140
    .end local v1    # "fos":Ljava/io/FileOutputStream;
    .end local v2    # "key":Ljava/lang/String;
    .end local v3    # "palette":Landroid/support/v7/graphics/Palette;
    .end local v4    # "rawThumbPath":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 141
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0
.end method

.method public putTimelineBitmap(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 8
    .param p1, "filepath"    # Ljava/lang/String;
    .param p2, "time"    # Ljava/lang/String;
    .param p3, "bmp"    # Landroid/graphics/Bitmap;

    .prologue
    .line 145
    if-eqz p1, :cond_0

    if-nez p3, :cond_1

    .line 170
    :cond_0
    :goto_0
    return-void

    .line 149
    :cond_1
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_2

    .line 150
    invoke-virtual {p3}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v4

    if-nez v4, :cond_0

    .line 151
    invoke-virtual {p3}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0

    .line 157
    :cond_2
    :try_start_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "?"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    new-instance v5, Ljava/text/SimpleDateFormat;

    const-string v6, "MM/dd/yyyy HH:mm:ss"

    sget-object v7, Lcom/sec/android/app/ve/thumbcache/DiskLruCache;->DATE_PATTERN_LOCALE:Ljava/util/Locale;

    invoke-direct {v5, v6, v7}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    new-instance v6, Ljava/io/File;

    invoke-direct {v6, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/io/File;->lastModified()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 158
    .local v2, "key":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/app/ve/thumbcache/DiskLruCache;->mTimelineCache:Lcom/sec/android/app/ve/thumbcache/DiskLruCache$LocalCache;

    invoke-virtual {v4, v2, p3}, Lcom/sec/android/app/ve/thumbcache/DiskLruCache$LocalCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 160
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtimeNanos()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, ".thumb"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 161
    .local v3, "rawThumbPath":Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/ve/VEApp;->openFileOutput(Ljava/lang/String;)Ljava/io/FileOutputStream;

    move-result-object v1

    .line 162
    .local v1, "fos":Ljava/io/FileOutputStream;
    if-eqz v1, :cond_3

    .line 163
    sget-object v4, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v5, 0x64

    invoke-virtual {p3, v4, v5, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 164
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    .line 166
    :cond_3
    invoke-virtual {p0, v2, v3}, Lcom/sec/android/app/ve/thumbcache/DiskLruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 167
    .end local v1    # "fos":Ljava/io/FileOutputStream;
    .end local v2    # "key":Ljava/lang/String;
    .end local v3    # "rawThumbPath":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 168
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public recheckAfterMediaScanned()Ljava/util/ArrayList;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 296
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 297
    .local v10, "notFoundKeys":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 299
    .local v9, "notFoundFiles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    sget-object v14, Lcom/sec/android/app/ve/thumbcache/DiskLruCache;->mInstance:Lcom/sec/android/app/ve/thumbcache/DiskLruCache;

    invoke-virtual {v14}, Lcom/sec/android/app/ve/thumbcache/DiskLruCache;->snapshot()Ljava/util/Map;

    move-result-object v12

    .line 300
    .local v12, "persCacheMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v12}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v14

    invoke-interface {v14}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .line 301
    .local v8, "keys":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    new-instance v14, Ljava/text/SimpleDateFormat;

    const-string v15, "MM/dd/yyyy HH:mm:ss"

    sget-object v16, Lcom/sec/android/app/ve/thumbcache/DiskLruCache;->DATE_PATTERN_LOCALE:Ljava/util/Locale;

    invoke-direct/range {v14 .. v16}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    invoke-virtual {v14}, Ljava/text/SimpleDateFormat;->toLocalizedPattern()Ljava/lang/String;

    move-result-object v11

    .line 302
    .local v11, "pattern":Ljava/lang/String;
    :cond_0
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-nez v14, :cond_2

    .line 326
    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :cond_1
    :goto_1
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-nez v15, :cond_4

    .line 341
    invoke-virtual {v10}, Ljava/util/ArrayList;->clear()V

    .line 342
    return-object v9

    .line 303
    :cond_2
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 304
    .local v7, "key":Ljava/lang/String;
    const-string v14, "?"

    invoke-virtual {v7, v14}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v14

    if-nez v14, :cond_3

    .line 306
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v14

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v15

    sub-int v3, v14, v15

    .line 307
    .local v3, "end":I
    const/4 v14, 0x0

    invoke-virtual {v7, v14, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 308
    .local v5, "filePath":Ljava/lang/String;
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 309
    .local v4, "file":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v14

    if-nez v14, :cond_0

    .line 310
    invoke-virtual {v10, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 311
    invoke-virtual {v9, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 316
    .end local v3    # "end":I
    .end local v4    # "file":Ljava/io/File;
    .end local v5    # "filePath":Ljava/lang/String;
    :cond_3
    const-string v14, "?"

    invoke-virtual {v7, v14}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    .line 317
    .restart local v3    # "end":I
    const/4 v14, 0x0

    invoke-virtual {v7, v14, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 318
    .restart local v5    # "filePath":Ljava/lang/String;
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 319
    .restart local v4    # "file":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v14

    if-nez v14, :cond_0

    .line 320
    invoke-virtual {v10, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 321
    invoke-virtual {v9, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 326
    .end local v3    # "end":I
    .end local v4    # "file":Ljava/io/File;
    .end local v5    # "filePath":Ljava/lang/String;
    .end local v7    # "key":Ljava/lang/String;
    :cond_4
    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 327
    .restart local v7    # "key":Ljava/lang/String;
    sget-object v15, Lcom/sec/android/app/ve/thumbcache/DiskLruCache;->mInstance:Lcom/sec/android/app/ve/thumbcache/DiskLruCache;

    invoke-virtual {v15, v7}, Lcom/sec/android/app/ve/thumbcache/DiskLruCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    .line 328
    .local v13, "rawThumbPath":Ljava/lang/String;
    new-instance v6, Ljava/io/File;

    invoke-static {v13}, Lcom/sec/android/app/ve/VEApp;->getFullySpecifiedInternalFilepath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-direct {v6, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 329
    .local v6, "fileToBeDeleted":Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v15

    if-eqz v15, :cond_5

    .line 330
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    .line 332
    :cond_5
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/ve/thumbcache/DiskLruCache;->mLocalCache:Lcom/sec/android/app/ve/thumbcache/DiskLruCache$LocalCache;

    invoke-virtual {v15, v7}, Lcom/sec/android/app/ve/thumbcache/DiskLruCache$LocalCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Bitmap;

    .line 333
    .local v2, "bmp":Landroid/graphics/Bitmap;
    if-eqz v2, :cond_6

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v15

    if-nez v15, :cond_6

    .line 334
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 336
    :cond_6
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/ve/thumbcache/DiskLruCache;->mTimelineCache:Lcom/sec/android/app/ve/thumbcache/DiskLruCache$LocalCache;

    invoke-virtual {v15, v7}, Lcom/sec/android/app/ve/thumbcache/DiskLruCache$LocalCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Bitmap;

    .line 337
    .local v1, "bitm":Landroid/graphics/Bitmap;
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v15

    if-nez v15, :cond_1

    .line 338
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    goto/16 :goto_1
.end method

.method public saveToDisk()V
    .locals 4

    .prologue
    .line 95
    :try_start_0
    const-string v3, "VEAddMediaThumbnailCache.ser"

    invoke-static {v3}, Lcom/sec/android/app/ve/VEApp;->openFileOutput(Ljava/lang/String;)Ljava/io/FileOutputStream;

    move-result-object v1

    .line 96
    .local v1, "fos":Ljava/io/FileOutputStream;
    if-eqz v1, :cond_0

    .line 97
    new-instance v2, Ljava/io/ObjectOutputStream;

    invoke-direct {v2, v1}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 98
    .local v2, "oos":Ljava/io/ObjectOutputStream;
    invoke-virtual {p0}, Lcom/sec/android/app/ve/thumbcache/DiskLruCache;->snapshot()Ljava/util/Map;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 99
    invoke-virtual {v2}, Ljava/io/ObjectOutputStream;->close()V

    .line 100
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 105
    .end local v1    # "fos":Ljava/io/FileOutputStream;
    .end local v2    # "oos":Ljava/io/ObjectOutputStream;
    :cond_0
    :goto_0
    return-void

    .line 102
    :catch_0
    move-exception v0

    .line 103
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method
