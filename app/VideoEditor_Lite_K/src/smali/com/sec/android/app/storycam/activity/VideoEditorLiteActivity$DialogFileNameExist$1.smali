.class Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogFileNameExist$1;
.super Ljava/lang/Object;
.source "VideoEditorLiteActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogFileNameExist;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogFileNameExist;

.field private final synthetic val$exportFilename:Ljava/lang/String;

.field private final synthetic val$trans:Lcom/samsung/app/video/editor/external/TranscodeElement;


# direct methods
.method constructor <init>(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogFileNameExist;Ljava/lang/String;Lcom/samsung/app/video/editor/external/TranscodeElement;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogFileNameExist$1;->this$1:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogFileNameExist;

    iput-object p2, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogFileNameExist$1;->val$exportFilename:Ljava/lang/String;

    iput-object p3, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogFileNameExist$1;->val$trans:Lcom/samsung/app/video/editor/external/TranscodeElement;

    .line 1259
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 7
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "id"    # I

    .prologue
    .line 1262
    :try_start_0
    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    sget-object v3, Lcom/sec/android/app/ve/common/ConfigUtils;->EXPORT_PATH:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogFileNameExist$1;->val$exportFilename:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".mp4"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1263
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1264
    invoke-virtual {v1}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1268
    .end local v1    # "file":Ljava/io/File;
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogFileNameExist$1;->this$1:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogFileNameExist;

    iget-object v2, v2, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogFileNameExist;->expWrapper:Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;

    iget-object v3, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogFileNameExist$1;->this$1:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogFileNameExist;

    iget-object v3, v3, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogFileNameExist;->activity:Landroid/app/Activity;

    iget-object v4, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogFileNameExist$1;->val$trans:Lcom/samsung/app/video/editor/external/TranscodeElement;

    iget-object v5, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogFileNameExist$1;->this$1:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogFileNameExist;

    iget-object v5, v5, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogFileNameExist;->expAdapter:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExpAdapter;

    iget-object v6, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogFileNameExist$1;->this$1:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogFileNameExist;

    # getter for: Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogFileNameExist;->expRes:I
    invoke-static {v6}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogFileNameExist;->access$0(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogFileNameExist;)I

    move-result v6

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;->startExport(Landroid/app/Activity;Lcom/samsung/app/video/editor/external/TranscodeElement;Lcom/sec/android/app/ve/export/Export$Adapter;I)Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;

    .line 1269
    return-void

    .line 1265
    :catch_0
    move-exception v0

    .line 1266
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
