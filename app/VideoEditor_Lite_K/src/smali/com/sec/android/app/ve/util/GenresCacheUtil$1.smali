.class Lcom/sec/android/app/ve/util/GenresCacheUtil$1;
.super Ljava/lang/Object;
.source "GenresCacheUtil.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/ve/util/GenresCacheUtil;->loadGenresList()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 26
    # invokes: Lcom/sec/android/app/ve/util/GenresCacheUtil;->readGenresList()Ljava/util/List;
    invoke-static {}, Lcom/sec/android/app/ve/util/GenresCacheUtil;->access$0()Ljava/util/List;

    move-result-object v3

    sput-object v3, Lcom/sec/android/app/ve/util/GenresCacheUtil;->genresList:Ljava/util/List;

    .line 27
    sget-object v3, Lcom/sec/android/app/ve/util/GenresCacheUtil;->genresList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_0

    .line 32
    return-void

    .line 27
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/ve/data/VEAlbum;

    .line 28
    .local v2, "lVEAlbum":Lcom/sec/android/app/ve/data/VEAlbum;
    invoke-virtual {v2}, Lcom/sec/android/app/ve/data/VEAlbum;->getMusicCategoryId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 29
    .local v1, "id":I
    invoke-static {v1}, Lcom/sec/android/app/ve/util/GenresCacheUtil;->readSongsCountOfGenres(I)I

    move-result v0

    .line 30
    .local v0, "count":I
    invoke-virtual {v2, v0}, Lcom/sec/android/app/ve/data/VEAlbum;->setSongsCount(I)V

    goto :goto_0
.end method
