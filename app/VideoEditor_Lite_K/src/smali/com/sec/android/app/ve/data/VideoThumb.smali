.class public Lcom/sec/android/app/ve/data/VideoThumb;
.super Ljava/lang/Object;
.source "VideoThumb.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x529a88bae2cb0550L


# instance fields
.field private transient bMediaAdded:Z

.field private bchecRretriever:Z

.field private mDuration:J

.field private mId:J

.field private mIsSupportedbyEngine:Z

.field private mVideoTitle:Ljava/lang/String;

.field private transient thumbnail:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>(Landroid/graphics/Bitmap;ZZ)V
    .locals 2
    .param p1, "b"    # Landroid/graphics/Bitmap;
    .param p2, "check"    # Z
    .param p3, "supported"    # Z

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput-object v1, p0, Lcom/sec/android/app/ve/data/VideoThumb;->thumbnail:Landroid/graphics/Bitmap;

    .line 11
    iput-boolean v0, p0, Lcom/sec/android/app/ve/data/VideoThumb;->bchecRretriever:Z

    .line 12
    iput-boolean v0, p0, Lcom/sec/android/app/ve/data/VideoThumb;->mIsSupportedbyEngine:Z

    .line 15
    iput-object v1, p0, Lcom/sec/android/app/ve/data/VideoThumb;->mVideoTitle:Ljava/lang/String;

    .line 17
    iput-boolean v0, p0, Lcom/sec/android/app/ve/data/VideoThumb;->bMediaAdded:Z

    .line 20
    iput-object p1, p0, Lcom/sec/android/app/ve/data/VideoThumb;->thumbnail:Landroid/graphics/Bitmap;

    .line 21
    iput-boolean p2, p0, Lcom/sec/android/app/ve/data/VideoThumb;->bchecRretriever:Z

    .line 22
    iput-boolean p3, p0, Lcom/sec/android/app/ve/data/VideoThumb;->mIsSupportedbyEngine:Z

    .line 23
    return-void
.end method


# virtual methods
.method public getDuration()J
    .locals 2

    .prologue
    .line 56
    iget-wide v0, p0, Lcom/sec/android/app/ve/data/VideoThumb;->mDuration:J

    return-wide v0
.end method

.method public getEngineSupported()Z
    .locals 1

    .prologue
    .line 48
    iget-boolean v0, p0, Lcom/sec/android/app/ve/data/VideoThumb;->mIsSupportedbyEngine:Z

    return v0
.end method

.method public getId()J
    .locals 2

    .prologue
    .line 64
    iget-wide v0, p0, Lcom/sec/android/app/ve/data/VideoThumb;->mId:J

    return-wide v0
.end method

.method public getRretrieverChecked()Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/sec/android/app/ve/data/VideoThumb;->bchecRretriever:Z

    return v0
.end method

.method public getThumbnail()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/ve/data/VideoThumb;->thumbnail:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getVideoTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/android/app/ve/data/VideoThumb;->mVideoTitle:Ljava/lang/String;

    return-object v0
.end method

.method public isMediaAdded()Z
    .locals 1

    .prologue
    .line 81
    iget-boolean v0, p0, Lcom/sec/android/app/ve/data/VideoThumb;->bMediaAdded:Z

    return v0
.end method

.method public setDuration(J)V
    .locals 1
    .param p1, "aDuration"    # J

    .prologue
    .line 52
    iput-wide p1, p0, Lcom/sec/android/app/ve/data/VideoThumb;->mDuration:J

    .line 53
    return-void
.end method

.method public setEngineSupported(Z)V
    .locals 0
    .param p1, "aSupported"    # Z

    .prologue
    .line 44
    iput-boolean p1, p0, Lcom/sec/android/app/ve/data/VideoThumb;->mIsSupportedbyEngine:Z

    .line 45
    return-void
.end method

.method public setId(J)V
    .locals 1
    .param p1, "aId"    # J

    .prologue
    .line 60
    iput-wide p1, p0, Lcom/sec/android/app/ve/data/VideoThumb;->mId:J

    .line 61
    return-void
.end method

.method public setMediaAdded(Z)V
    .locals 0
    .param p1, "added"    # Z

    .prologue
    .line 76
    iput-boolean p1, p0, Lcom/sec/android/app/ve/data/VideoThumb;->bMediaAdded:Z

    .line 77
    return-void
.end method

.method public setRretrieverChecked(Z)V
    .locals 0
    .param p1, "check"    # Z

    .prologue
    .line 32
    iput-boolean p1, p0, Lcom/sec/android/app/ve/data/VideoThumb;->bchecRretriever:Z

    .line 33
    return-void
.end method

.method public setThumbnail(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "b"    # Landroid/graphics/Bitmap;

    .prologue
    .line 27
    iput-object p1, p0, Lcom/sec/android/app/ve/data/VideoThumb;->thumbnail:Landroid/graphics/Bitmap;

    .line 28
    return-void
.end method

.method public setVideoTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "aTitle"    # Ljava/lang/String;

    .prologue
    .line 68
    iput-object p1, p0, Lcom/sec/android/app/ve/data/VideoThumb;->mVideoTitle:Ljava/lang/String;

    .line 69
    return-void
.end method
