.class Lcom/sec/android/app/ve/preview/ProjectPreview$4;
.super Ljava/lang/Object;
.source "ProjectPreview.java"

# interfaces
.implements Landroid/media/AudioManager$OnAudioFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/ve/preview/ProjectPreview;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/ve/preview/ProjectPreview;


# direct methods
.method constructor <init>(Lcom/sec/android/app/ve/preview/ProjectPreview;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview$4;->this$0:Lcom/sec/android/app/ve/preview/ProjectPreview;

    .line 675
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAudioFocusChange(I)V
    .locals 3
    .param p1, "focusChange"    # I

    .prologue
    .line 679
    const-string v0, "PLAYER_STATE"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "This is inside onAudioFocusChange of PreviewView Group -->"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 680
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    .line 681
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview$4;->this$0:Lcom/sec/android/app/ve/preview/ProjectPreview;

    iget-object v0, v0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mAdapter:Lcom/sec/android/app/ve/PreviewPlayerInterface$Adapter;

    if-eqz v0, :cond_0

    .line 682
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview$4;->this$0:Lcom/sec/android/app/ve/preview/ProjectPreview;

    iget-object v0, v0, Lcom/sec/android/app/ve/preview/ProjectPreview;->mAdapter:Lcom/sec/android/app/ve/PreviewPlayerInterface$Adapter;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/PreviewPlayerInterface$Adapter;->onAudioFocusLost()V

    .line 684
    :cond_0
    return-void
.end method
