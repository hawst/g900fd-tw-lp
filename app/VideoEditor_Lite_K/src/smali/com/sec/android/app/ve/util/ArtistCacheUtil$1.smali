.class Lcom/sec/android/app/ve/util/ArtistCacheUtil$1;
.super Ljava/lang/Object;
.source "ArtistCacheUtil.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/ve/util/ArtistCacheUtil;->loadArtistList()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 23
    # invokes: Lcom/sec/android/app/ve/util/ArtistCacheUtil;->readArtistList()Ljava/util/List;
    invoke-static {}, Lcom/sec/android/app/ve/util/ArtistCacheUtil;->access$0()Ljava/util/List;

    move-result-object v2

    sput-object v2, Lcom/sec/android/app/ve/util/ArtistCacheUtil;->artistList:Ljava/util/List;

    .line 25
    sget-object v2, Lcom/sec/android/app/ve/util/ArtistCacheUtil;->artistList:Ljava/util/List;

    if-eqz v2, :cond_0

    .line 26
    sget-object v2, Lcom/sec/android/app/ve/util/ArtistCacheUtil;->artistList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 33
    :cond_0
    return-void

    .line 26
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/ve/data/VEAlbum;

    .line 28
    .local v1, "lVEAlbum":Lcom/sec/android/app/ve/data/VEAlbum;
    invoke-virtual {v1}, Lcom/sec/android/app/ve/data/VEAlbum;->getMusicCategoryId()Ljava/lang/String;

    move-result-object v3

    .line 27
    invoke-static {v3}, Lcom/sec/android/app/ve/util/ArtistCacheUtil;->readSongsCountOfArtist(Ljava/lang/String;)I

    move-result v0

    .line 29
    .local v0, "count":I
    invoke-virtual {v1, v0}, Lcom/sec/android/app/ve/data/VEAlbum;->setSongsCount(I)V

    goto :goto_0
.end method
