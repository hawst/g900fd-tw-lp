.class Lcom/sec/android/app/storycam/view/PreviewViewGroup$1;
.super Landroid/os/Handler;
.source "PreviewViewGroup.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/storycam/view/PreviewViewGroup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$1;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    .line 73
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v5, 0x2

    const/4 v6, 0x1

    .line 75
    iget v4, p1, Landroid/os/Message;->what:I

    if-ne v4, v6, :cond_2

    .line 76
    invoke-virtual {p0, v6}, Lcom/sec/android/app/storycam/view/PreviewViewGroup$1;->removeMessages(I)V

    .line 79
    iget-object v4, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$1;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    # getter for: Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mPreview:Lcom/sec/android/app/ve/PreviewPlayerInterface;
    invoke-static {v4}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$0(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)Lcom/sec/android/app/ve/PreviewPlayerInterface;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$1;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    # getter for: Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mPreview:Lcom/sec/android/app/ve/PreviewPlayerInterface;
    invoke-static {v4}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$0(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)Lcom/sec/android/app/ve/PreviewPlayerInterface;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/ve/PreviewPlayerInterface;->isPlaying()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 81
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/NativeInterface;->getCurrentPosition()I

    move-result v4

    int-to-long v0, v4

    .line 82
    .local v0, "cur_playing_time":J
    iget-object v4, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$1;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    # getter for: Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mCurrTransElement:Lcom/samsung/app/video/editor/external/TranscodeElement;
    invoke-static {v4}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$1(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getTotalDuration()J

    move-result-wide v2

    .line 83
    .local v2, "total_time":J
    cmp-long v4, v0, v2

    if-ltz v4, :cond_1

    .line 85
    iget-object v4, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$1;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    # getter for: Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mProgressBar:Landroid/widget/SeekBar;
    invoke-static {v4}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$2(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)Landroid/widget/SeekBar;

    move-result-object v4

    long-to-int v5, v2

    invoke-virtual {v4, v5}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 92
    :goto_0
    const-wide/16 v4, 0x32

    invoke-virtual {p0, v6, v4, v5}, Lcom/sec/android/app/storycam/view/PreviewViewGroup$1;->sendEmptyMessageDelayed(IJ)Z

    .line 100
    .end local v0    # "cur_playing_time":J
    .end local v2    # "total_time":J
    :cond_0
    :goto_1
    return-void

    .line 89
    .restart local v0    # "cur_playing_time":J
    .restart local v2    # "total_time":J
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$1;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    # getter for: Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mProgressBar:Landroid/widget/SeekBar;
    invoke-static {v4}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$2(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)Landroid/widget/SeekBar;

    move-result-object v4

    long-to-int v5, v0

    invoke-virtual {v4, v5}, Landroid/widget/SeekBar;->setProgress(I)V

    goto :goto_0

    .line 95
    .end local v0    # "cur_playing_time":J
    .end local v2    # "total_time":J
    :cond_2
    iget v4, p1, Landroid/os/Message;->what:I

    if-ne v4, v5, :cond_0

    .line 96
    invoke-virtual {p0, v5}, Lcom/sec/android/app/storycam/view/PreviewViewGroup$1;->removeMessages(I)V

    .line 97
    iget-object v4, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$1;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    invoke-virtual {v4}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->start_play()I

    .line 98
    iget-object v4, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$1;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    const/4 v5, 0x0

    invoke-static {v4, v5}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$3(Lcom/sec/android/app/storycam/view/PreviewViewGroup;Z)V

    goto :goto_1
.end method
