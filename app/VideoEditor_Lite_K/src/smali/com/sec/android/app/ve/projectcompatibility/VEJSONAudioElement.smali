.class public Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioElement;
.super Lorg/json/JSONObject;
.source "VEJSONAudioElement.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lorg/json/JSONObject;-><init>()V

    .line 27
    return-void
.end method

.method protected constructor <init>(Lorg/json/JSONObject;)V
    .locals 8
    .param p1, "sourceAudioElement"    # Lorg/json/JSONObject;

    .prologue
    .line 29
    invoke-direct {p0}, Lorg/json/JSONObject;-><init>()V

    .line 32
    :try_start_0
    invoke-virtual {p1}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v4

    .line 33
    .local v4, "sourceIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    if-eqz v4, :cond_1

    .line 34
    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_2

    .line 57
    .end local v4    # "sourceIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_1
    :goto_1
    return-void

    .line 35
    .restart local v4    # "sourceIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 37
    .local v3, "key":Ljava/lang/String;
    const-string v6, "EFFECTS_LIST"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 38
    invoke-static {p1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioElement;->getAudioEffectList(Lorg/json/JSONObject;)Lorg/json/JSONArray;

    move-result-object v1

    .line 39
    .local v1, "effectList":Lorg/json/JSONArray;
    if-eqz v1, :cond_0

    .line 40
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_2
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v6

    if-ge v2, v6, :cond_0

    .line 41
    new-instance v6, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioEffect;

    invoke-virtual {v1, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v7

    invoke-direct {v6, v7}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioEffect;-><init>(Lorg/json/JSONObject;)V

    invoke-virtual {p0, v6}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioElement;->addAudioEffect(Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioEffect;)V

    .line 40
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 45
    .end local v1    # "effectList":Lorg/json/JSONArray;
    .end local v2    # "i":I
    :cond_3
    const-string v6, "TRANSITION"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 46
    invoke-static {p1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioElement;->getAudioTransition(Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object v5

    .line 47
    .local v5, "transition":Lorg/json/JSONObject;
    new-instance v6, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioTransition;

    invoke-direct {v6, v5}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioTransition;-><init>(Lorg/json/JSONObject;)V

    invoke-virtual {p0, v6}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioElement;->setAudioTransition(Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioTransition;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 54
    .end local v3    # "key":Ljava/lang/String;
    .end local v4    # "sourceIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    .end local v5    # "transition":Lorg/json/JSONObject;
    :catch_0
    move-exception v0

    .line 55
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_1

    .line 50
    .end local v0    # "e":Lorg/json/JSONException;
    .restart local v3    # "key":Ljava/lang/String;
    .restart local v4    # "sourceIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_4
    :try_start_1
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {p0, v3, v6}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioElement;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method private static getAudioEffectList(Lorg/json/JSONObject;)Lorg/json/JSONArray;
    .locals 3
    .param p0, "source"    # Lorg/json/JSONObject;

    .prologue
    .line 309
    const/4 v1, 0x0

    .line 311
    .local v1, "list":Lorg/json/JSONArray;
    :try_start_0
    const-string v2, "EFFECTS_LIST"

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 316
    :goto_0
    return-object v1

    .line 313
    :catch_0
    move-exception v0

    .line 314
    .local v0, "jException":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method private static getAudioTransition(Lorg/json/JSONObject;)Lorg/json/JSONObject;
    .locals 3
    .param p0, "source"    # Lorg/json/JSONObject;

    .prologue
    .line 320
    const/4 v1, 0x0

    .line 322
    .local v1, "transition":Lorg/json/JSONObject;
    :try_start_0
    const-string v2, "TRANSITION"

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 327
    :goto_0
    return-object v1

    .line 324
    :catch_0
    move-exception v0

    .line 325
    .local v0, "jException":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public addAudioEffect(Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioEffect;)V
    .locals 4
    .param p1, "effect"    # Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioEffect;

    .prologue
    .line 194
    const/4 v1, 0x0

    .line 196
    .local v1, "effectList":Lorg/json/JSONArray;
    :try_start_0
    const-string v3, "EFFECTS_LIST"

    invoke-virtual {p0, v3}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioElement;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 208
    :goto_0
    invoke-virtual {v1, p1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 209
    return-void

    .line 198
    :catch_0
    move-exception v2

    .line 199
    .local v2, "exception":Lorg/json/JSONException;
    new-instance v1, Lorg/json/JSONArray;

    .end local v1    # "effectList":Lorg/json/JSONArray;
    invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V

    .line 201
    .restart local v1    # "effectList":Lorg/json/JSONArray;
    :try_start_1
    const-string v3, "EFFECTS_LIST"

    invoke-virtual {p0, v3, v1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioElement;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 203
    :catch_1
    move-exception v0

    .line 204
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getAudioEffectList()Lorg/json/JSONArray;
    .locals 1

    .prologue
    .line 218
    invoke-static {p0}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioElement;->getAudioEffectList(Lorg/json/JSONObject;)Lorg/json/JSONArray;

    move-result-object v0

    return-object v0
.end method

.method public getAudioTransition()Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioTransition;
    .locals 1

    .prologue
    .line 274
    invoke-static {p0}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioElement;->getAudioTransition(Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioTransition;

    return-object v0
.end method

.method public getDisplayName()Ljava/lang/String;
    .locals 3

    .prologue
    .line 241
    const/4 v0, 0x0

    .line 243
    .local v0, "displayName":Ljava/lang/String;
    :try_start_0
    const-string v2, "AUDIO_DISPLAY_NAME"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioElement;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 248
    :goto_0
    return-object v0

    .line 245
    :catch_0
    move-exception v1

    .line 246
    .local v1, "jException":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getDuration()I
    .locals 3

    .prologue
    .line 176
    const/4 v0, 0x0

    .line 178
    .local v0, "duration":I
    :try_start_0
    const-string v2, "ELEMENT_DURATION"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioElement;->getInt(Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 183
    :goto_0
    return v0

    .line 180
    :catch_0
    move-exception v1

    .line 181
    .local v1, "jException":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getFilePath()Ljava/lang/String;
    .locals 3

    .prologue
    .line 81
    const/4 v1, 0x0

    .line 83
    .local v1, "path":Ljava/lang/String;
    :try_start_0
    const-string v2, "ELEMENT_FILE_PATH"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioElement;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 88
    :goto_0
    return-object v1

    .line 85
    :catch_0
    move-exception v0

    .line 86
    .local v0, "jException":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getStoryboardEndTime()I
    .locals 3

    .prologue
    .line 144
    const/4 v0, 0x0

    .line 146
    .local v0, "endTime":I
    :try_start_0
    const-string v2, "STORYBOARD_END_TIME"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioElement;->getInt(Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 151
    :goto_0
    return v0

    .line 148
    :catch_0
    move-exception v1

    .line 149
    .local v1, "jException":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getStoryboardStartTime()I
    .locals 3

    .prologue
    .line 113
    const/4 v1, 0x0

    .line 115
    .local v1, "startTime":I
    :try_start_0
    const-string v2, "STORYBOARD_START_TIME"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioElement;->getInt(Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 120
    :goto_0
    return v1

    .line 117
    :catch_0
    move-exception v0

    .line 118
    .local v0, "jException":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getVolumeLevel()I
    .locals 3

    .prologue
    .line 298
    const/4 v1, 0x0

    .line 300
    .local v1, "volume":I
    :try_start_0
    const-string v2, "VOLUME_LEVEL"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioElement;->getInt(Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 305
    :goto_0
    return v1

    .line 302
    :catch_0
    move-exception v0

    .line 303
    .local v0, "jException":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public setAudioTransition(Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioTransition;)V
    .locals 2
    .param p1, "transition"    # Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioTransition;

    .prologue
    .line 260
    :try_start_0
    const-string v1, "TRANSITION"

    invoke-virtual {p0, v1, p1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioElement;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 265
    :goto_0
    return-void

    .line 262
    :catch_0
    move-exception v0

    .line 263
    .local v0, "jException":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public setDisplayName(Ljava/lang/String;)V
    .locals 2
    .param p1, "displayName"    # Ljava/lang/String;

    .prologue
    .line 228
    :try_start_0
    const-string v1, "AUDIO_DISPLAY_NAME"

    invoke-virtual {p0, v1, p1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioElement;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 233
    :goto_0
    return-void

    .line 230
    :catch_0
    move-exception v0

    .line 231
    .local v0, "jException":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public setDuration(I)V
    .locals 2
    .param p1, "duration"    # I

    .prologue
    .line 162
    :try_start_0
    const-string v1, "ELEMENT_DURATION"

    invoke-virtual {p0, v1, p1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioElement;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 167
    :goto_0
    return-void

    .line 164
    :catch_0
    move-exception v0

    .line 165
    .local v0, "jException":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public setFilePath(Ljava/lang/String;)V
    .locals 2
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 67
    :try_start_0
    const-string v1, "ELEMENT_FILE_PATH"

    invoke-virtual {p0, v1, p1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioElement;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 72
    :goto_0
    return-void

    .line 69
    :catch_0
    move-exception v0

    .line 70
    .local v0, "jException":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public setStoryboardEndTime(I)V
    .locals 2
    .param p1, "endTime"    # I

    .prologue
    .line 131
    :try_start_0
    const-string v1, "STORYBOARD_END_TIME"

    invoke-virtual {p0, v1, p1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioElement;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 136
    :goto_0
    return-void

    .line 133
    :catch_0
    move-exception v0

    .line 134
    .local v0, "jException":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public setStoryboardStartTime(I)V
    .locals 2
    .param p1, "startTime"    # I

    .prologue
    .line 99
    :try_start_0
    const-string v1, "STORYBOARD_START_TIME"

    invoke-virtual {p0, v1, p1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioElement;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 104
    :goto_0
    return-void

    .line 101
    :catch_0
    move-exception v0

    .line 102
    .local v0, "jException":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public setVolumeLevel(I)V
    .locals 2
    .param p1, "volume"    # I

    .prologue
    .line 285
    :try_start_0
    const-string v1, "VOLUME_LEVEL"

    invoke-virtual {p0, v1, p1}, Lcom/sec/android/app/ve/projectcompatibility/VEJSONAudioElement;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 290
    :goto_0
    return-void

    .line 287
    :catch_0
    move-exception v0

    .line 288
    .local v0, "jException":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method
