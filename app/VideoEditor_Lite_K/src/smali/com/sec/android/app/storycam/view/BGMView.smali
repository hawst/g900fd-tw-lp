.class public Lcom/sec/android/app/storycam/view/BGMView;
.super Landroid/widget/RelativeLayout;
.source "BGMView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/storycam/view/BGMView$BGMmBgmIconAdapter;
    }
.end annotation


# instance fields
.field private listener:Landroid/view/View$OnClickListener;

.field private mAudioChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field private mAudioManager:Landroid/media/AudioManager;

.field private mBGMChanged:Z

.field private mBgmGridView:Landroid/widget/GridView;

.field private mBgmIcon:Landroid/widget/ImageView;

.field private mBgmItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

.field private mBgmLayout:Landroid/widget/RelativeLayout;

.field private mBgmName:Ljava/lang/String;

.field private mBgmText:Landroid/widget/TextView;

.field private mBgmTitle:Landroid/widget/TextView;

.field private mInflater:Landroid/view/LayoutInflater;

.field private mInitialBGMOnState:Z

.field private mListAdapter:Lcom/sec/android/app/storycam/view/BGMView$BGMmBgmIconAdapter;

.field private mMediaPlayer:Landroid/media/MediaPlayer;

.field private mPrevSelPos:I

.field private mSelectedPos:I

.field private mThumbIds:[Ljava/lang/Integer;

.field private mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 61
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 41
    new-instance v0, Lcom/sec/android/app/storycam/view/BGMView$BGMmBgmIconAdapter;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/BGMView$BGMmBgmIconAdapter;-><init>(Lcom/sec/android/app/storycam/view/BGMView;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView;->mListAdapter:Lcom/sec/android/app/storycam/view/BGMView$BGMmBgmIconAdapter;

    .line 43
    const-string v0, " "

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView;->mBgmName:Ljava/lang/String;

    .line 49
    iput v1, p0, Lcom/sec/android/app/storycam/view/BGMView;->mSelectedPos:I

    .line 50
    iput v1, p0, Lcom/sec/android/app/storycam/view/BGMView;->mPrevSelPos:I

    .line 52
    iput-boolean v3, p0, Lcom/sec/android/app/storycam/view/BGMView;->mInitialBGMOnState:Z

    .line 54
    iput-boolean v2, p0, Lcom/sec/android/app/storycam/view/BGMView;->mBGMChanged:Z

    .line 128
    new-instance v0, Lcom/sec/android/app/storycam/view/BGMView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/BGMView$1;-><init>(Lcom/sec/android/app/storycam/view/BGMView;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView;->mAudioChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 136
    new-instance v0, Lcom/sec/android/app/storycam/view/BGMView$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/BGMView$2;-><init>(Lcom/sec/android/app/storycam/view/BGMView;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView;->listener:Landroid/view/View$OnClickListener;

    .line 197
    new-instance v0, Lcom/sec/android/app/storycam/view/BGMView$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/BGMView$3;-><init>(Lcom/sec/android/app/storycam/view/BGMView;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView;->mBgmItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 389
    const/16 v0, 0x19

    new-array v0, v0, [Ljava/lang/Integer;

    .line 390
    const v1, 0x7f050081

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    const v1, 0x7f050082

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    const/4 v1, 0x2

    const v2, 0x7f050083

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const v2, 0x7f050084

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const v2, 0x7f050085

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    .line 391
    const v2, 0x7f050086

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const v2, 0x7f050087

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const v2, 0x7f050088

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const v2, 0x7f050089

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const v2, 0x7f05008a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xa

    .line 392
    const v2, 0x7f05008b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const v2, 0x7f05008c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const v2, 0x7f05008d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const v2, 0x7f05008e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const v2, 0x7f05008f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xf

    .line 393
    const v2, 0x7f050090

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const v2, 0x7f050091

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const v2, 0x7f050092

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const v2, 0x7f050093

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const v2, 0x7f050094

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x14

    .line 394
    const v2, 0x7f050095

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const v2, 0x7f050096

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const v2, 0x7f050097

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const v2, 0x7f050098

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const v2, 0x7f050099

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView;->mThumbIds:[Ljava/lang/Integer;

    .line 62
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 57
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 41
    new-instance v0, Lcom/sec/android/app/storycam/view/BGMView$BGMmBgmIconAdapter;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/BGMView$BGMmBgmIconAdapter;-><init>(Lcom/sec/android/app/storycam/view/BGMView;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView;->mListAdapter:Lcom/sec/android/app/storycam/view/BGMView$BGMmBgmIconAdapter;

    .line 43
    const-string v0, " "

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView;->mBgmName:Ljava/lang/String;

    .line 49
    iput v1, p0, Lcom/sec/android/app/storycam/view/BGMView;->mSelectedPos:I

    .line 50
    iput v1, p0, Lcom/sec/android/app/storycam/view/BGMView;->mPrevSelPos:I

    .line 52
    iput-boolean v3, p0, Lcom/sec/android/app/storycam/view/BGMView;->mInitialBGMOnState:Z

    .line 54
    iput-boolean v2, p0, Lcom/sec/android/app/storycam/view/BGMView;->mBGMChanged:Z

    .line 128
    new-instance v0, Lcom/sec/android/app/storycam/view/BGMView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/BGMView$1;-><init>(Lcom/sec/android/app/storycam/view/BGMView;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView;->mAudioChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 136
    new-instance v0, Lcom/sec/android/app/storycam/view/BGMView$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/BGMView$2;-><init>(Lcom/sec/android/app/storycam/view/BGMView;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView;->listener:Landroid/view/View$OnClickListener;

    .line 197
    new-instance v0, Lcom/sec/android/app/storycam/view/BGMView$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/BGMView$3;-><init>(Lcom/sec/android/app/storycam/view/BGMView;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView;->mBgmItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 389
    const/16 v0, 0x19

    new-array v0, v0, [Ljava/lang/Integer;

    .line 390
    const v1, 0x7f050081

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    const v1, 0x7f050082

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    const/4 v1, 0x2

    const v2, 0x7f050083

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const v2, 0x7f050084

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const v2, 0x7f050085

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    .line 391
    const v2, 0x7f050086

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const v2, 0x7f050087

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const v2, 0x7f050088

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const v2, 0x7f050089

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const v2, 0x7f05008a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xa

    .line 392
    const v2, 0x7f05008b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const v2, 0x7f05008c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const v2, 0x7f05008d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const v2, 0x7f05008e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const v2, 0x7f05008f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xf

    .line 393
    const v2, 0x7f050090

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const v2, 0x7f050091

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const v2, 0x7f050092

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const v2, 0x7f050093

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const v2, 0x7f050094

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x14

    .line 394
    const v2, 0x7f050095

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const v2, 0x7f050096

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const v2, 0x7f050097

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const v2, 0x7f050098

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const v2, 0x7f050099

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView;->mThumbIds:[Ljava/lang/Integer;

    .line 58
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 65
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 41
    new-instance v0, Lcom/sec/android/app/storycam/view/BGMView$BGMmBgmIconAdapter;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/BGMView$BGMmBgmIconAdapter;-><init>(Lcom/sec/android/app/storycam/view/BGMView;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView;->mListAdapter:Lcom/sec/android/app/storycam/view/BGMView$BGMmBgmIconAdapter;

    .line 43
    const-string v0, " "

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView;->mBgmName:Ljava/lang/String;

    .line 49
    iput v1, p0, Lcom/sec/android/app/storycam/view/BGMView;->mSelectedPos:I

    .line 50
    iput v1, p0, Lcom/sec/android/app/storycam/view/BGMView;->mPrevSelPos:I

    .line 52
    iput-boolean v3, p0, Lcom/sec/android/app/storycam/view/BGMView;->mInitialBGMOnState:Z

    .line 54
    iput-boolean v2, p0, Lcom/sec/android/app/storycam/view/BGMView;->mBGMChanged:Z

    .line 128
    new-instance v0, Lcom/sec/android/app/storycam/view/BGMView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/BGMView$1;-><init>(Lcom/sec/android/app/storycam/view/BGMView;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView;->mAudioChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 136
    new-instance v0, Lcom/sec/android/app/storycam/view/BGMView$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/BGMView$2;-><init>(Lcom/sec/android/app/storycam/view/BGMView;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView;->listener:Landroid/view/View$OnClickListener;

    .line 197
    new-instance v0, Lcom/sec/android/app/storycam/view/BGMView$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/BGMView$3;-><init>(Lcom/sec/android/app/storycam/view/BGMView;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView;->mBgmItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 389
    const/16 v0, 0x19

    new-array v0, v0, [Ljava/lang/Integer;

    .line 390
    const v1, 0x7f050081

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    const v1, 0x7f050082

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    const/4 v1, 0x2

    const v2, 0x7f050083

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const v2, 0x7f050084

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const v2, 0x7f050085

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    .line 391
    const v2, 0x7f050086

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const v2, 0x7f050087

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const v2, 0x7f050088

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const v2, 0x7f050089

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const v2, 0x7f05008a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xa

    .line 392
    const v2, 0x7f05008b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const v2, 0x7f05008c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const v2, 0x7f05008d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const v2, 0x7f05008e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const v2, 0x7f05008f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xf

    .line 393
    const v2, 0x7f050090

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const v2, 0x7f050091

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const v2, 0x7f050092

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const v2, 0x7f050093

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const v2, 0x7f050094

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x14

    .line 394
    const v2, 0x7f050095

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const v2, 0x7f050096

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const v2, 0x7f050097

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const v2, 0x7f050098

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const v2, 0x7f050099

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView;->mThumbIds:[Ljava/lang/Integer;

    .line 66
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/app/storycam/view/BGMView;)Landroid/media/MediaPlayer;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView;->mMediaPlayer:Landroid/media/MediaPlayer;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/app/storycam/view/BGMView;Z)V
    .locals 0

    .prologue
    .line 54
    iput-boolean p1, p0, Lcom/sec/android/app/storycam/view/BGMView;->mBGMChanged:Z

    return-void
.end method

.method static synthetic access$10(Lcom/sec/android/app/storycam/view/BGMView;)Lcom/sec/android/app/storycam/view/BGMView$BGMmBgmIconAdapter;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView;->mListAdapter:Lcom/sec/android/app/storycam/view/BGMView$BGMmBgmIconAdapter;

    return-object v0
.end method

.method static synthetic access$11(Lcom/sec/android/app/storycam/view/BGMView;)Landroid/media/AudioManager;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView;->mAudioManager:Landroid/media/AudioManager;

    return-object v0
.end method

.method static synthetic access$12(Lcom/sec/android/app/storycam/view/BGMView;)Landroid/media/AudioManager$OnAudioFocusChangeListener;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView;->mAudioChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    return-object v0
.end method

.method static synthetic access$13(Lcom/sec/android/app/storycam/view/BGMView;)[Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 389
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView;->mThumbIds:[Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic access$14(Lcom/sec/android/app/storycam/view/BGMView;)Landroid/view/LayoutInflater;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView;->mInflater:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method static synthetic access$15(Lcom/sec/android/app/storycam/view/BGMView;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/app/storycam/view/BGMView;)Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    return-object v0
.end method

.method static synthetic access$3(Lcom/sec/android/app/storycam/view/BGMView;)I
    .locals 1

    .prologue
    .line 49
    iget v0, p0, Lcom/sec/android/app/storycam/view/BGMView;->mSelectedPos:I

    return v0
.end method

.method static synthetic access$4(Lcom/sec/android/app/storycam/view/BGMView;I)V
    .locals 0

    .prologue
    .line 50
    iput p1, p0, Lcom/sec/android/app/storycam/view/BGMView;->mPrevSelPos:I

    return-void
.end method

.method static synthetic access$5(Lcom/sec/android/app/storycam/view/BGMView;)V
    .locals 0

    .prologue
    .line 107
    invoke-direct {p0}, Lcom/sec/android/app/storycam/view/BGMView;->setBGMVisibility()V

    return-void
.end method

.method static synthetic access$6(Lcom/sec/android/app/storycam/view/BGMView;)I
    .locals 1

    .prologue
    .line 50
    iget v0, p0, Lcom/sec/android/app/storycam/view/BGMView;->mPrevSelPos:I

    return v0
.end method

.method static synthetic access$7(Lcom/sec/android/app/storycam/view/BGMView;I)V
    .locals 0

    .prologue
    .line 49
    iput p1, p0, Lcom/sec/android/app/storycam/view/BGMView;->mSelectedPos:I

    return-void
.end method

.method static synthetic access$8(Lcom/sec/android/app/storycam/view/BGMView;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView;->mBgmTitle:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$9(Lcom/sec/android/app/storycam/view/BGMView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView;->mBgmName:Ljava/lang/String;

    return-object v0
.end method

.method private static createBGMElement(Ljava/lang/String;Ljava/lang/String;J)Lcom/samsung/app/video/editor/external/Element;
    .locals 8
    .param p0, "bgmPath"    # Ljava/lang/String;
    .param p1, "bgmName"    # Ljava/lang/String;
    .param p2, "duration"    # J

    .prologue
    const-wide/16 v6, 0x0

    const/4 v4, 0x1

    .line 177
    new-instance v1, Lcom/samsung/app/video/editor/external/Element;

    invoke-direct {v1}, Lcom/samsung/app/video/editor/external/Element;-><init>()V

    .line 178
    .local v1, "lMusicElement":Lcom/samsung/app/video/editor/external/Element;
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/samsung/app/video/editor/external/Element;->setType(I)V

    .line 179
    invoke-virtual {v1, p0}, Lcom/samsung/app/video/editor/external/Element;->setFilePath(Ljava/lang/String;)V

    .line 180
    invoke-virtual {v1, v6, v7}, Lcom/samsung/app/video/editor/external/Element;->setStartTime(J)V

    .line 181
    invoke-virtual {v1, p2, p3}, Lcom/samsung/app/video/editor/external/Element;->setEndTime(J)V

    .line 182
    invoke-virtual {v1, p2, p3}, Lcom/samsung/app/video/editor/external/Element;->setDuration(J)V

    .line 183
    invoke-virtual {v1, v6, v7}, Lcom/samsung/app/video/editor/external/Element;->setStoryBoardStartTime(J)V

    .line 184
    sget-object v2, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v2}, Lcom/sec/android/app/storycam/AppDataManager;->getCurrentTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getTotalDuration()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/samsung/app/video/editor/external/Element;->setStoryBoardEndTime(J)V

    .line 185
    invoke-virtual {v1, p1}, Lcom/samsung/app/video/editor/external/Element;->setAudioDisplayName(Ljava/lang/String;)V

    .line 186
    invoke-virtual {v1, v4}, Lcom/samsung/app/video/editor/external/Element;->setAssetResource(Z)V

    .line 187
    new-instance v0, Lcom/samsung/app/video/editor/external/Edit;

    invoke-direct {v0}, Lcom/samsung/app/video/editor/external/Edit;-><init>()V

    .line 188
    .local v0, "edit":Lcom/samsung/app/video/editor/external/Edit;
    invoke-virtual {v0, v4}, Lcom/samsung/app/video/editor/external/Edit;->setType(I)V

    .line 189
    invoke-virtual {v1, v0}, Lcom/samsung/app/video/editor/external/Element;->addEdit(Lcom/samsung/app/video/editor/external/Edit;)V

    .line 190
    new-instance v0, Lcom/samsung/app/video/editor/external/Edit;

    .end local v0    # "edit":Lcom/samsung/app/video/editor/external/Edit;
    invoke-direct {v0}, Lcom/samsung/app/video/editor/external/Edit;-><init>()V

    .line 191
    .restart local v0    # "edit":Lcom/samsung/app/video/editor/external/Edit;
    const/4 v2, 0x7

    invoke-virtual {v0, v2}, Lcom/samsung/app/video/editor/external/Edit;->setType(I)V

    .line 192
    const/16 v2, 0x15

    invoke-virtual {v0, v2}, Lcom/samsung/app/video/editor/external/Edit;->setSubType(I)V

    .line 193
    invoke-virtual {v1, v0}, Lcom/samsung/app/video/editor/external/Element;->addEdit(Lcom/samsung/app/video/editor/external/Edit;)V

    .line 194
    return-object v1
.end method

.method private init()V
    .locals 3

    .prologue
    .line 82
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/view/BGMView;->getContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    iput-object v1, p0, Lcom/sec/android/app/storycam/view/BGMView;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    .line 83
    const v1, 0x7f0d0017

    invoke-virtual {p0, v1}, Lcom/sec/android/app/storycam/view/BGMView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/app/storycam/view/BGMView;->mBgmLayout:Landroid/widget/RelativeLayout;

    .line 84
    const v1, 0x7f0d0019

    invoke-virtual {p0, v1}, Lcom/sec/android/app/storycam/view/BGMView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/app/storycam/view/BGMView;->mBgmIcon:Landroid/widget/ImageView;

    .line 85
    const v1, 0x7f0d001a

    invoke-virtual {p0, v1}, Lcom/sec/android/app/storycam/view/BGMView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/storycam/view/BGMView;->mBgmText:Landroid/widget/TextView;

    .line 86
    const v1, 0x7f0d0014

    invoke-virtual {p0, v1}, Lcom/sec/android/app/storycam/view/BGMView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/GridView;

    iput-object v1, p0, Lcom/sec/android/app/storycam/view/BGMView;->mBgmGridView:Landroid/widget/GridView;

    .line 87
    const v1, 0x7f0d000f

    invoke-virtual {p0, v1}, Lcom/sec/android/app/storycam/view/BGMView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/storycam/view/BGMView;->mBgmTitle:Landroid/widget/TextView;

    .line 88
    iget-object v1, p0, Lcom/sec/android/app/storycam/view/BGMView;->mBgmGridView:Landroid/widget/GridView;

    iget-object v2, p0, Lcom/sec/android/app/storycam/view/BGMView;->mListAdapter:Lcom/sec/android/app/storycam/view/BGMView$BGMmBgmIconAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 89
    iget-object v1, p0, Lcom/sec/android/app/storycam/view/BGMView;->mBgmIcon:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/sec/android/app/storycam/view/BGMView;->listener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 90
    iget-object v1, p0, Lcom/sec/android/app/storycam/view/BGMView;->mBgmIcon:Landroid/widget/ImageView;

    const v2, 0x7f020029

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 92
    new-instance v1, Landroid/media/MediaPlayer;

    invoke-direct {v1}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/storycam/view/BGMView;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 93
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/view/BGMView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/storycam/view/BGMView;->mInflater:Landroid/view/LayoutInflater;

    .line 94
    iget-object v1, p0, Lcom/sec/android/app/storycam/view/BGMView;->mContext:Landroid/content/Context;

    const-string v2, "audio"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/AudioManager;

    iput-object v1, p0, Lcom/sec/android/app/storycam/view/BGMView;->mAudioManager:Landroid/media/AudioManager;

    .line 95
    iget-object v1, p0, Lcom/sec/android/app/storycam/view/BGMView;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    invoke-interface {v1}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;->isUserSelectedBgm()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/storycam/view/BGMView;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    invoke-interface {v1}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;->getUserSelectedBgmpos()I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 96
    iget-object v1, p0, Lcom/sec/android/app/storycam/view/BGMView;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    invoke-interface {v1}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;->getUserSelectedBgmpos()I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/storycam/view/BGMView;->mSelectedPos:I

    .line 101
    :goto_0
    iget v1, p0, Lcom/sec/android/app/storycam/view/BGMView;->mSelectedPos:I

    iput v1, p0, Lcom/sec/android/app/storycam/view/BGMView;->mPrevSelPos:I

    .line 102
    iget-object v1, p0, Lcom/sec/android/app/storycam/view/BGMView;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    invoke-interface {v1}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;->isBgmStateOn()Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/app/storycam/view/BGMView;->mInitialBGMOnState:Z

    .line 103
    invoke-direct {p0}, Lcom/sec/android/app/storycam/view/BGMView;->setBGMVisibility()V

    .line 104
    iget-object v1, p0, Lcom/sec/android/app/storycam/view/BGMView;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    invoke-interface {v1}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;->getCurrentConfig()Landroid/content/res/Configuration;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/storycam/view/BGMView;->handleOrientationChangeBGM(Landroid/content/res/Configuration;)V

    .line 105
    return-void

    .line 98
    :cond_0
    sget-object v1, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v1}, Lcom/sec/android/app/storycam/AppDataManager;->getCurrentTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getThemeName()I

    move-result v0

    .line 99
    .local v0, "themePos":I
    sget-object v1, Lcom/sec/android/app/ve/VEApp;->gBGMManager:Lcom/sec/android/app/ve/bgm/BGMManager;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/ve/bgm/BGMManager;->mapThemeToBGM(I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/storycam/view/BGMView;->mSelectedPos:I

    goto :goto_0
.end method

.method private setBGMVisibility()V
    .locals 4

    .prologue
    const v3, 0x7f07026c

    const v2, 0x7f07026b

    .line 108
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    invoke-interface {v0}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;->isBgmStateOn()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 109
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView;->mBgmLayout:Landroid/widget/RelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 110
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView;->mBgmGridView:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/sec/android/app/storycam/view/BGMView;->mBgmItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 111
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView;->mBgmIcon:Landroid/widget/ImageView;

    const v1, 0x7f020008

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 112
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView;->mBgmIcon:Landroid/widget/ImageView;

    invoke-static {v2}, Lcom/sec/android/app/ve/VEApp;->getStringValue(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 113
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView;->mBgmText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 114
    sget-object v0, Lcom/sec/android/app/ve/VEApp;->gBGMManager:Lcom/sec/android/app/ve/bgm/BGMManager;

    iget v1, p0, Lcom/sec/android/app/storycam/view/BGMView;->mSelectedPos:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/ve/bgm/BGMManager;->getBGMName(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView;->mBgmName:Ljava/lang/String;

    .line 124
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView;->mBgmTitle:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/storycam/view/BGMView;->mBgmName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 125
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView;->mListAdapter:Lcom/sec/android/app/storycam/view/BGMView$BGMmBgmIconAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/storycam/view/BGMView$BGMmBgmIconAdapter;->notifyDataSetChanged()V

    .line 126
    return-void

    .line 116
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView;->mBgmLayout:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 117
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView;->mBgmGridView:Landroid/widget/GridView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 118
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView;->mBgmIcon:Landroid/widget/ImageView;

    const v1, 0x7f020007

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 119
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView;->mBgmIcon:Landroid/widget/ImageView;

    invoke-static {v3}, Lcom/sec/android/app/ve/VEApp;->getStringValue(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 120
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView;->mBgmText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    .line 121
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/storycam/view/BGMView;->mSelectedPos:I

    .line 122
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView;->mBgmName:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public discardBgm()V
    .locals 2

    .prologue
    .line 398
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    invoke-interface {v0}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;->isUserSelectedBgm()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    invoke-interface {v0}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;->getUserSelectedBgmpos()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 399
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;->setUserSelectedBgm(Z)V

    .line 401
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    iget-boolean v1, p0, Lcom/sec/android/app/storycam/view/BGMView;->mInitialBGMOnState:Z

    invoke-interface {v0, v1}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;->setBgmState(Z)V

    .line 402
    return-void
.end method

.method public handleOrientationChangeBGM(Landroid/content/res/Configuration;)V
    .locals 25
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 234
    const v21, 0x7f0d0011

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/sec/android/app/storycam/view/BGMView;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/RelativeLayout;

    .line 235
    .local v11, "bgmLayout":Landroid/widget/RelativeLayout;
    invoke-virtual {v11}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v14

    check-cast v14, Landroid/widget/RelativeLayout$LayoutParams;

    .line 237
    .local v14, "bgmLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    const v21, 0x7f0d0018

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/sec/android/app/storycam/view/BGMView;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/RelativeLayout;

    .line 238
    .local v9, "bgmIconLayout":Landroid/widget/RelativeLayout;
    invoke-virtual {v9}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v10

    check-cast v10, Landroid/widget/RelativeLayout$LayoutParams;

    .line 240
    .local v10, "bgmIconLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    const v21, 0x7f0d0012

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/sec/android/app/storycam/view/BGMView;->findViewById(I)Landroid/view/View;

    move-result-object v20

    check-cast v20, Landroid/widget/TextView;

    .line 241
    .local v20, "bgmTopTextView":Landroid/widget/TextView;
    invoke-virtual/range {v20 .. v20}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v19

    check-cast v19, Landroid/widget/RelativeLayout$LayoutParams;

    .line 242
    .local v19, "bgmTopTextParams":Landroid/widget/RelativeLayout$LayoutParams;
    const v21, 0x7f060366

    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v21

    const v22, 0x7f060369

    invoke-static/range {v22 .. v22}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v22

    const/16 v23, 0x0

    const v24, 0x7f060349

    invoke-static/range {v24 .. v24}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v24

    move-object/from16 v0, v19

    move/from16 v1, v21

    move/from16 v2, v22

    move/from16 v3, v23

    move/from16 v4, v24

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 244
    const v21, 0x7f0d0016

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/sec/android/app/storycam/view/BGMView;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 245
    .local v6, "bgmBottomTextView":Landroid/widget/TextView;
    invoke-virtual {v6}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Landroid/widget/RelativeLayout$LayoutParams;

    .line 246
    .local v5, "bgmBottomTextParams":Landroid/widget/RelativeLayout$LayoutParams;
    const v21, 0x7f060366

    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v21

    const v22, 0x7f060349

    invoke-static/range {v22 .. v22}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v22

    const/16 v23, 0x0

    const/16 v24, 0x0

    move/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    move/from16 v3, v24

    invoke-virtual {v5, v0, v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 248
    const v21, 0x7f0d0013

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/sec/android/app/storycam/view/BGMView;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/TextView;

    .line 249
    .local v16, "bgmLeftTextView":Landroid/widget/TextView;
    invoke-virtual/range {v16 .. v16}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v15

    check-cast v15, Landroid/widget/RelativeLayout$LayoutParams;

    .line 250
    .local v15, "bgmLeftTextParams":Landroid/widget/RelativeLayout$LayoutParams;
    const v21, 0x7f060395

    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v21

    const v22, 0x7f060348

    invoke-static/range {v22 .. v22}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v22

    const/16 v23, 0x0

    const/16 v24, 0x0

    move/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    move/from16 v3, v24

    invoke-virtual {v15, v0, v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 251
    const v21, 0x7f060395

    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v21

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v21

    move/from16 v2, v22

    move/from16 v3, v23

    move/from16 v4, v24

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 253
    const v21, 0x7f0d0015

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/sec/android/app/storycam/view/BGMView;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/TextView;

    .line 254
    .local v18, "bgmRightTextView":Landroid/widget/TextView;
    invoke-virtual/range {v18 .. v18}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v17

    check-cast v17, Landroid/widget/RelativeLayout$LayoutParams;

    .line 255
    .local v17, "bgmRightTextParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v21, 0x0

    const v22, 0x7f060348

    invoke-static/range {v22 .. v22}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v22

    const v23, 0x7f060396

    invoke-static/range {v23 .. v23}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v23

    const/16 v24, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v21

    move/from16 v2, v22

    move/from16 v3, v23

    move/from16 v4, v24

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 256
    const v21, 0x7f060396

    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v21

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v21

    move/from16 v2, v22

    move/from16 v3, v23

    move/from16 v4, v24

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 258
    const v21, 0x7f0d0017

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/sec/android/app/storycam/view/BGMView;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/RelativeLayout;

    .line 259
    .local v12, "bgmLayoutCover":Landroid/widget/RelativeLayout;
    invoke-virtual {v12}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v13

    check-cast v13, Landroid/widget/RelativeLayout$LayoutParams;

    .line 261
    .local v13, "bgmLayoutCoverParams":Landroid/widget/RelativeLayout$LayoutParams;
    const v21, 0x7f0d0014

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/sec/android/app/storycam/view/BGMView;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/GridView;

    .line 262
    .local v7, "bgmGridView":Landroid/widget/GridView;
    invoke-virtual {v7}, Landroid/widget/GridView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v8

    check-cast v8, Landroid/widget/RelativeLayout$LayoutParams;

    .line 266
    .local v8, "bgmGridViewParams":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p1

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    move/from16 v21, v0

    const/16 v22, 0x2

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_2

    .line 268
    invoke-virtual/range {p1 .. p1}, Landroid/content/res/Configuration;->getLayoutDirection()I

    move-result v21

    const/16 v22, 0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_1

    .line 269
    const v21, 0x7f060365

    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v21

    move/from16 v0, v21

    iput v0, v14, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 270
    const/16 v21, 0x3

    move/from16 v0, v21

    invoke-virtual {v10, v0}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 271
    const/16 v21, 0x0

    const v22, 0x7f0d0011

    move/from16 v0, v21

    move/from16 v1, v22

    invoke-virtual {v10, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 272
    const v21, 0x7f060368

    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v21

    const v22, 0x7f060367

    invoke-static/range {v22 .. v22}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v22

    const/16 v23, 0x0

    const/16 v24, 0x0

    move/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    move/from16 v3, v24

    invoke-virtual {v10, v0, v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 274
    const/16 v21, 0x3

    invoke-virtual/range {v20 .. v21}, Landroid/widget/TextView;->setGravity(I)V

    .line 275
    const/16 v21, 0x3

    move/from16 v0, v21

    invoke-virtual {v6, v0}, Landroid/widget/TextView;->setGravity(I)V

    .line 301
    :goto_0
    const v21, 0x7f060364

    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v21

    move/from16 v0, v21

    iput v0, v14, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 302
    const v21, 0x7f060375

    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v21

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    move/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    move/from16 v3, v24

    invoke-virtual {v14, v0, v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 303
    move-object/from16 v0, p1

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    move/from16 v21, v0

    const/16 v22, 0x2

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_0

    .line 305
    invoke-virtual/range {p1 .. p1}, Landroid/content/res/Configuration;->getLayoutDirection()I

    move-result v21

    const/16 v22, 0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_0

    .line 307
    const v21, 0x7f060375

    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v21

    const/16 v22, 0x0

    const v23, 0x7f06034c

    invoke-static/range {v23 .. v23}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v23

    const/16 v24, 0x0

    move/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    move/from16 v3, v24

    invoke-virtual {v14, v0, v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 311
    :cond_0
    invoke-virtual {v11, v14}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 314
    const v21, 0x7f06034b

    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v21

    move/from16 v0, v21

    iput v0, v13, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 315
    const v21, 0x7f06034b

    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v21

    move/from16 v0, v21

    iput v0, v13, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 316
    invoke-virtual {v12, v13}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 318
    const v21, 0x7f06034b

    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v21

    move/from16 v0, v21

    iput v0, v8, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 319
    const v21, 0x7f06034b

    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v21

    move/from16 v0, v21

    iput v0, v8, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 321
    invoke-virtual {v7, v8}, Landroid/widget/GridView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 323
    invoke-virtual {v9, v10}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 324
    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 325
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/storycam/view/BGMView;->mListAdapter:Lcom/sec/android/app/storycam/view/BGMView$BGMmBgmIconAdapter;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/app/storycam/view/BGMView$BGMmBgmIconAdapter;->notifyDataSetChanged()V

    .line 326
    return-void

    .line 278
    :cond_1
    const v21, 0x7f060365

    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v21

    move/from16 v0, v21

    iput v0, v14, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 279
    const/16 v21, 0x3

    move/from16 v0, v21

    invoke-virtual {v10, v0}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 280
    const/16 v21, 0x1

    const v22, 0x7f0d0011

    move/from16 v0, v21

    move/from16 v1, v22

    invoke-virtual {v10, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 281
    const v21, 0x7f060368

    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v21

    const v22, 0x7f060367

    invoke-static/range {v22 .. v22}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v22

    const/16 v23, 0x0

    const/16 v24, 0x0

    move/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    move/from16 v3, v24

    invoke-virtual {v10, v0, v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 283
    const/16 v21, 0x3

    invoke-virtual/range {v20 .. v21}, Landroid/widget/TextView;->setGravity(I)V

    .line 284
    const/16 v21, 0x3

    move/from16 v0, v21

    invoke-virtual {v6, v0}, Landroid/widget/TextView;->setGravity(I)V

    goto/16 :goto_0

    .line 290
    :cond_2
    const/16 v21, -0x1

    move/from16 v0, v21

    iput v0, v14, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 292
    const/16 v21, 0x1

    move/from16 v0, v21

    invoke-virtual {v10, v0}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 293
    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v10, v0}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 294
    const/16 v21, 0x3

    const v22, 0x7f0d0011

    move/from16 v0, v21

    move/from16 v1, v22

    invoke-virtual {v10, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 295
    const/16 v21, 0x0

    const v22, 0x7f060367

    invoke-static/range {v22 .. v22}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v22

    const/16 v23, 0x0

    const/16 v24, 0x0

    move/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    move/from16 v3, v24

    invoke-virtual {v10, v0, v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 297
    const/16 v21, 0x1

    invoke-virtual/range {v20 .. v21}, Landroid/widget/TextView;->setGravity(I)V

    .line 298
    const/16 v21, 0x1

    move/from16 v0, v21

    invoke-virtual {v6, v0}, Landroid/widget/TextView;->setGravity(I)V

    goto/16 :goto_0
.end method

.method public isBGMChanged()Z
    .locals 1

    .prologue
    .line 405
    iget-boolean v0, p0, Lcom/sec/android/app/storycam/view/BGMView;->mBGMChanged:Z

    return v0
.end method

.method public isMediaplayerPlaying()Z
    .locals 1

    .prologue
    .line 329
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 330
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    .line 331
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onAttachedToWindow()V
    .locals 0

    .prologue
    .line 70
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onAttachedToWindow()V

    .line 71
    invoke-direct {p0}, Lcom/sec/android/app/storycam/view/BGMView;->init()V

    .line 72
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 76
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onDetachedFromWindow()V

    .line 77
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    .line 79
    :cond_0
    return-void
.end method

.method public saveBgm()V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 158
    sget-object v0, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v0}, Lcom/sec/android/app/storycam/AppDataManager;->getCurrentTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v2

    .line 159
    .local v2, "curTransElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    invoke-interface {v0}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;->isBgmStateOn()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 160
    sget-object v0, Lcom/sec/android/app/ve/VEApp;->gBGMManager:Lcom/sec/android/app/ve/bgm/BGMManager;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/ve/bgm/BGMManager;->setThemeDefaultBGM(Z)V

    .line 161
    sget-object v0, Lcom/sec/android/app/ve/VEApp;->gBGMManager:Lcom/sec/android/app/ve/bgm/BGMManager;

    iget v1, p0, Lcom/sec/android/app/storycam/view/BGMView;->mSelectedPos:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/ve/bgm/BGMManager;->getBGMFilePath(I)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/ve/VEApp;->gBGMManager:Lcom/sec/android/app/ve/bgm/BGMManager;

    iget v4, p0, Lcom/sec/android/app/storycam/view/BGMView;->mSelectedPos:I

    invoke-virtual {v1, v4}, Lcom/sec/android/app/ve/bgm/BGMManager;->getBGMName(I)Ljava/lang/String;

    move-result-object v1

    sget-object v4, Lcom/sec/android/app/ve/VEApp;->gBGMManager:Lcom/sec/android/app/ve/bgm/BGMManager;

    iget v5, p0, Lcom/sec/android/app/storycam/view/BGMView;->mSelectedPos:I

    invoke-virtual {v4, v5}, Lcom/sec/android/app/ve/bgm/BGMManager;->getTotalDurationBGM(I)I

    move-result v4

    int-to-long v4, v4

    invoke-static {v0, v1, v4, v5}, Lcom/sec/android/app/storycam/view/BGMView;->createBGMElement(Ljava/lang/String;Ljava/lang/String;J)Lcom/samsung/app/video/editor/external/Element;

    move-result-object v6

    .line 162
    .local v6, "bgmElement":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getAdditionlAudioEleList()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 163
    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getAdditionlAudioEleList()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 164
    :cond_0
    invoke-virtual {v2, v6}, Lcom/samsung/app/video/editor/external/TranscodeElement;->addMusicEleList(Lcom/samsung/app/video/editor/external/Element;)V

    .line 166
    sget-object v0, Lcom/sec/android/app/ve/VEApp;->gBGMManager:Lcom/sec/android/app/ve/bgm/BGMManager;

    iget v1, p0, Lcom/sec/android/app/storycam/view/BGMView;->mSelectedPos:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/ve/bgm/BGMManager;->setSavedBGMPos(I)V

    .line 167
    invoke-static {}, Lcom/sec/android/app/ve/theme/ThemeDataManager;->getInstance()Lcom/sec/android/app/ve/theme/ThemeDataManager;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/ve/VEApp;->gBGMManager:Lcom/sec/android/app/ve/bgm/BGMManager;

    invoke-virtual {v0, v2, v1}, Lcom/sec/android/app/ve/theme/ThemeDataManager;->modifyTranscodeWithBGMTranstionSlots(Lcom/samsung/app/video/editor/external/TranscodeElement;Lcom/sec/android/app/ve/bgm/BGMManager;)V

    .line 168
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    iget-object v1, p0, Lcom/sec/android/app/storycam/view/BGMView;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    invoke-interface {v1}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;->getSummaryHandler()Landroid/os/Handler;

    move-result-object v1

    const-wide/16 v4, 0x0

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/storycam/summary/Summary;->summarize(Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;Landroid/os/Handler;Lcom/samsung/app/video/editor/external/TranscodeElement;ZJ)V

    .line 169
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    invoke-interface {v0}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;->isUserSelectedBgm()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 170
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    iget v1, p0, Lcom/sec/android/app/storycam/view/BGMView;->mSelectedPos:I

    invoke-interface {v0, v1}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;->setUserSelectedBgmPos(I)V

    .line 174
    .end local v6    # "bgmElement":Lcom/samsung/app/video/editor/external/Element;
    :cond_1
    :goto_0
    return-void

    .line 173
    :cond_2
    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/TranscodeElement;->removeBGMMusic()V

    goto :goto_0
.end method

.method public stopMediaPlayer()V
    .locals 1

    .prologue
    .line 335
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 336
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/BGMView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    .line 337
    :cond_0
    return-void
.end method
