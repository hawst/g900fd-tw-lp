.class public Lcom/sec/android/app/storycam/bgm/WhistleYourCaresBGM;
.super Lcom/sec/android/app/ve/bgm/BGM;
.source "WhistleYourCaresBGM.java"


# static fields
.field private static synthetic $SWITCH_TABLE$com$sec$android$app$ve$bgm$BGM$BGMModes:[I


# instance fields
.field private fastModeBGM:[I

.field private mediumModeBGM:[I

.field private slowModeBGM:[I

.field private totalDurationBGM:I


# direct methods
.method static synthetic $SWITCH_TABLE$com$sec$android$app$ve$bgm$BGM$BGMModes()[I
    .locals 3

    .prologue
    .line 6
    sget-object v0, Lcom/sec/android/app/storycam/bgm/WhistleYourCaresBGM;->$SWITCH_TABLE$com$sec$android$app$ve$bgm$BGM$BGMModes:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/sec/android/app/ve/bgm/BGM$BGMModes;->values()[Lcom/sec/android/app/ve/bgm/BGM$BGMModes;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/sec/android/app/ve/bgm/BGM$BGMModes;->FAST:Lcom/sec/android/app/ve/bgm/BGM$BGMModes;

    invoke-virtual {v1}, Lcom/sec/android/app/ve/bgm/BGM$BGMModes;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    :try_start_1
    sget-object v1, Lcom/sec/android/app/ve/bgm/BGM$BGMModes;->MEDIUM:Lcom/sec/android/app/ve/bgm/BGM$BGMModes;

    invoke-virtual {v1}, Lcom/sec/android/app/ve/bgm/BGM$BGMModes;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    :try_start_2
    sget-object v1, Lcom/sec/android/app/ve/bgm/BGM$BGMModes;->SLOW:Lcom/sec/android/app/ve/bgm/BGM$BGMModes;

    invoke-virtual {v1}, Lcom/sec/android/app/ve/bgm/BGM$BGMModes;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_3
    sput-object v0, Lcom/sec/android/app/storycam/bgm/WhistleYourCaresBGM;->$SWITCH_TABLE$com$sec$android$app$ve$bgm$BGM$BGMModes:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_1
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 6
    invoke-direct {p0}, Lcom/sec/android/app/ve/bgm/BGM;-><init>()V

    .line 9
    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    .line 16
    iput-object v0, p0, Lcom/sec/android/app/storycam/bgm/WhistleYourCaresBGM;->slowModeBGM:[I

    .line 19
    const/16 v0, 0xb

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    .line 29
    iput-object v0, p0, Lcom/sec/android/app/storycam/bgm/WhistleYourCaresBGM;->mediumModeBGM:[I

    .line 32
    const/16 v0, 0xe

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    .line 45
    iput-object v0, p0, Lcom/sec/android/app/storycam/bgm/WhistleYourCaresBGM;->fastModeBGM:[I

    .line 48
    const v0, 0xb8e2

    iput v0, p0, Lcom/sec/android/app/storycam/bgm/WhistleYourCaresBGM;->totalDurationBGM:I

    .line 6
    return-void

    .line 9
    :array_0
    .array-data 4
        0x15e0
        0x23f0
        0x3b60
        0x4ee8
        0x60e0
        0x7530
        0x8a48
        0x9c40
    .end array-data

    .line 19
    :array_1
    .array-data 4
        0xc80
        0x1c20
        0x2bc0
        0x3b60
        0x4a38
        0x5910
        0x68b0
        0x7788
        0x8728
        0x959c
        0xa604
    .end array-data

    .line 32
    :array_2
    .array-data 4
        0xc80
        0x1838
        0x23f0
        0x2fa8
        0x3b60
        0x4718
        0x52d0
        0x5f50
        0x6bd0
        0x7788
        0x8214
        0x8dcc
        0x9984
        0xa604
    .end array-data
.end method


# virtual methods
.method protected getBGMFilepath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    const-string v0, "themedata/rugged/bgm/C4_Whistle Your Cares.ogg"

    return-object v0
.end method

.method protected getBGMName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    const-string v0, "Whistle Your Cares"

    return-object v0
.end method

.method protected getTotalDurationBGM()I
    .locals 1

    .prologue
    .line 66
    iget v0, p0, Lcom/sec/android/app/storycam/bgm/WhistleYourCaresBGM;->totalDurationBGM:I

    return v0
.end method

.method protected getTransitionTimeStamps(Lcom/sec/android/app/ve/bgm/BGM$BGMModes;)[I
    .locals 2
    .param p1, "mode"    # Lcom/sec/android/app/ve/bgm/BGM$BGMModes;

    .prologue
    .line 52
    invoke-static {}, Lcom/sec/android/app/storycam/bgm/WhistleYourCaresBGM;->$SWITCH_TABLE$com$sec$android$app$ve$bgm$BGM$BGMModes()[I

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/android/app/ve/bgm/BGM$BGMModes;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 60
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 54
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/storycam/bgm/WhistleYourCaresBGM;->slowModeBGM:[I

    goto :goto_0

    .line 56
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/storycam/bgm/WhistleYourCaresBGM;->mediumModeBGM:[I

    goto :goto_0

    .line 58
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/storycam/bgm/WhistleYourCaresBGM;->fastModeBGM:[I

    goto :goto_0

    .line 52
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
