.class Lcom/sec/android/app/storycam/view/Caption1Line$6;
.super Ljava/lang/Object;
.source "Caption1Line.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/storycam/view/Caption1Line;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/storycam/view/Caption1Line;


# direct methods
.method constructor <init>(Lcom/sec/android/app/storycam/view/Caption1Line;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/storycam/view/Caption1Line$6;->this$0:Lcom/sec/android/app/storycam/view/Caption1Line;

    .line 408
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 18
    .param p1, "v"    # Landroid/view/View;
    .param p2, "hasFocus"    # Z

    .prologue
    .line 412
    if-eqz p2, :cond_1

    .line 413
    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/ve/common/AndroidUtils;->showSoftKeyboard(Landroid/view/View;)Z

    .line 414
    sget-object v1, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v1}, Lcom/sec/android/app/storycam/AppDataManager;->getCurrentTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v16

    .line 415
    .local v16, "tElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    if-eqz v16, :cond_1

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getTextEleListCount()I

    move-result v1

    if-lez v1, :cond_1

    .line 416
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getTextEleList()Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/samsung/app/video/editor/external/ClipartParams;

    .line 417
    .local v14, "caption":Lcom/samsung/app/video/editor/external/ClipartParams;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/storycam/view/Caption1Line$6;->this$0:Lcom/sec/android/app/storycam/view/Caption1Line;

    iget v2, v14, Lcom/samsung/app/video/editor/external/ClipartParams;->clipart_width:I

    add-int/lit8 v2, v2, -0x3c

    invoke-static {v1, v2}, Lcom/sec/android/app/storycam/view/Caption1Line;->access$5(Lcom/sec/android/app/storycam/view/Caption1Line;I)V

    .line 418
    iget-object v15, v14, Lcom/samsung/app/video/editor/external/ClipartParams;->mFontparams:Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;

    .line 419
    .local v15, "mFontparams":Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;
    sget-object v11, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    .line 420
    .local v11, "paintAlign":Landroid/graphics/Paint$Align;
    iget v1, v15, Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;->mAlignFlag:I

    const/16 v2, 0x11

    if-ne v1, v2, :cond_0

    .line 421
    sget-object v11, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    .line 423
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/storycam/view/Caption1Line$6;->this$0:Lcom/sec/android/app/storycam/view/Caption1Line;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/storycam/view/Caption1Line$6;->this$0:Lcom/sec/android/app/storycam/view/Caption1Line;

    invoke-virtual {v1}, Lcom/sec/android/app/storycam/view/Caption1Line;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 424
    iget-object v2, v15, Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;->mFontAssetFilePath:Ljava/lang/String;

    .line 425
    iget-boolean v3, v15, Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;->mIsPlatformFont:Z

    .line 426
    iget v4, v15, Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;->mFontTextStyle:I

    .line 427
    const/16 v5, 0x4e

    .line 428
    iget v6, v15, Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;->mFontColor:I

    .line 429
    iget v7, v15, Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;->mShadowR:I

    .line 430
    iget v8, v15, Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;->mShadowX:I

    .line 431
    iget v9, v15, Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;->mShadowY:I

    .line 432
    iget v10, v15, Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;->mShadowColor:I

    .line 434
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getAssetManager()Landroid/content/res/AssetManager;

    move-result-object v12

    .line 435
    iget v13, v15, Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;->mPaintFlags:I

    .line 423
    invoke-static/range {v1 .. v13}, Lcom/sec/android/app/ve/util/CommonUtils;->createTextPaint(Landroid/content/Context;Ljava/lang/String;ZIIIIIIILandroid/graphics/Paint$Align;Landroid/content/res/AssetManager;I)Landroid/graphics/Paint;

    move-result-object v1

    move-object/from16 v0, v17

    invoke-static {v0, v1}, Lcom/sec/android/app/storycam/view/Caption1Line;->access$6(Lcom/sec/android/app/storycam/view/Caption1Line;Landroid/graphics/Paint;)V

    .line 440
    .end local v11    # "paintAlign":Landroid/graphics/Paint$Align;
    .end local v14    # "caption":Lcom/samsung/app/video/editor/external/ClipartParams;
    .end local v15    # "mFontparams":Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;
    .end local v16    # "tElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    :cond_1
    return-void
.end method
