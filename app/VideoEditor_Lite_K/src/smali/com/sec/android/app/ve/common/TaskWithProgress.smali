.class public Lcom/sec/android/app/ve/common/TaskWithProgress;
.super Ljava/lang/Object;
.source "TaskWithProgress.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;,
        Lcom/sec/android/app/ve/common/TaskWithProgress$ProgressFragment;
    }
.end annotation


# static fields
.field private static final mAnimThread:Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;

.field private static mLoadingIcon:Landroid/graphics/Bitmap;


# instance fields
.field private final mActivity:Landroid/app/Activity;

.field private mMessage:Ljava/lang/String;

.field private final mRunnable:Ljava/lang/Runnable;

.field private final mUIThreadTask:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 30
    sput-object v1, Lcom/sec/android/app/ve/common/TaskWithProgress;->mLoadingIcon:Landroid/graphics/Bitmap;

    .line 31
    new-instance v0, Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;

    invoke-direct {v0, v1}, Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;-><init>(Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;)V

    sput-object v0, Lcom/sec/android/app/ve/common/TaskWithProgress;->mAnimThread:Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;ZLjava/lang/Runnable;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "runOnUIThread"    # Z
    .param p3, "run"    # Ljava/lang/Runnable;

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/ve/common/TaskWithProgress;->mMessage:Ljava/lang/String;

    .line 40
    iput-object p3, p0, Lcom/sec/android/app/ve/common/TaskWithProgress;->mRunnable:Ljava/lang/Runnable;

    .line 41
    iput-boolean p2, p0, Lcom/sec/android/app/ve/common/TaskWithProgress;->mUIThreadTask:Z

    .line 42
    iput-object p1, p0, Lcom/sec/android/app/ve/common/TaskWithProgress;->mActivity:Landroid/app/Activity;

    .line 44
    sget-object v0, Lcom/sec/android/app/ve/common/TaskWithProgress;->mLoadingIcon:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/app/ve/VEApp;->gAdaper:Lcom/sec/android/app/ve/VEAdapter;

    if-eqz v0, :cond_0

    .line 45
    sget-object v0, Lcom/sec/android/app/ve/VEApp;->gAdaper:Lcom/sec/android/app/ve/VEAdapter;

    invoke-interface {v0}, Lcom/sec/android/app/ve/VEAdapter;->getLoadingIconBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/ve/common/TaskWithProgress;->mLoadingIcon:Landroid/graphics/Bitmap;

    .line 47
    :cond_0
    sget-object v0, Lcom/sec/android/app/ve/common/TaskWithProgress;->mAnimThread:Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;->init()V

    .line 48
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/app/ve/common/TaskWithProgress;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/app/ve/common/TaskWithProgress;->mMessage:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/app/ve/common/TaskWithProgress;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 36
    iput-object p1, p0, Lcom/sec/android/app/ve/common/TaskWithProgress;->mMessage:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$2(Lcom/sec/android/app/ve/common/TaskWithProgress;)Z
    .locals 1

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/sec/android/app/ve/common/TaskWithProgress;->mUIThreadTask:Z

    return v0
.end method

.method static synthetic access$3(Lcom/sec/android/app/ve/common/TaskWithProgress;)V
    .locals 0

    .prologue
    .line 113
    invoke-direct {p0}, Lcom/sec/android/app/ve/common/TaskWithProgress;->doTaskAndClose()V

    return-void
.end method

.method static synthetic access$4()Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/sec/android/app/ve/common/TaskWithProgress;->mAnimThread:Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;

    return-object v0
.end method

.method static synthetic access$5()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/sec/android/app/ve/common/TaskWithProgress;->mLoadingIcon:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$6(Lcom/sec/android/app/ve/common/TaskWithProgress;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/ve/common/TaskWithProgress;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method public static dismissProgressDialog(Landroid/app/Activity;)V
    .locals 1
    .param p0, "activity"    # Landroid/app/Activity;

    .prologue
    .line 101
    if-nez p0, :cond_0

    .line 111
    :goto_0
    return-void

    .line 104
    :cond_0
    new-instance v0, Lcom/sec/android/app/ve/common/TaskWithProgress$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/ve/common/TaskWithProgress$3;-><init>(Landroid/app/Activity;)V

    invoke-virtual {p0, v0}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private doTaskAndClose()V
    .locals 2

    .prologue
    .line 115
    iget-object v0, p0, Lcom/sec/android/app/ve/common/TaskWithProgress;->mActivity:Landroid/app/Activity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/ve/common/TaskWithProgress;->mRunnable:Ljava/lang/Runnable;

    if-nez v0, :cond_1

    .line 127
    :cond_0
    :goto_0
    return-void

    .line 118
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/ve/common/TaskWithProgress;->mRunnable:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 120
    iget-object v0, p0, Lcom/sec/android/app/ve/common/TaskWithProgress;->mActivity:Landroid/app/Activity;

    new-instance v1, Lcom/sec/android/app/ve/common/TaskWithProgress$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/ve/common/TaskWithProgress$4;-><init>(Lcom/sec/android/app/ve/common/TaskWithProgress;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public static showProgressDialog(Landroid/app/Activity;Ljava/lang/String;)V
    .locals 1
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 84
    if-nez p0, :cond_0

    .line 97
    :goto_0
    return-void

    .line 87
    :cond_0
    new-instance v0, Lcom/sec/android/app/ve/common/TaskWithProgress$2;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/ve/common/TaskWithProgress$2;-><init>(Landroid/app/Activity;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method


# virtual methods
.method public setLoadingMessage(Ljava/lang/String;)V
    .locals 0
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 52
    iput-object p1, p0, Lcom/sec/android/app/ve/common/TaskWithProgress;->mMessage:Ljava/lang/String;

    .line 53
    return-void
.end method

.method public start()V
    .locals 2

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/app/ve/common/TaskWithProgress;->mActivity:Landroid/app/Activity;

    if-nez v0, :cond_1

    .line 76
    :cond_0
    :goto_0
    return-void

    .line 60
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/ve/common/TaskWithProgress;->mActivity:Landroid/app/Activity;

    new-instance v1, Lcom/sec/android/app/ve/common/TaskWithProgress$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/ve/common/TaskWithProgress$1;-><init>(Lcom/sec/android/app/ve/common/TaskWithProgress;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 72
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-eq v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/ve/common/TaskWithProgress;->mUIThreadTask:Z

    if-nez v0, :cond_0

    .line 74
    invoke-direct {p0}, Lcom/sec/android/app/ve/common/TaskWithProgress;->doTaskAndClose()V

    goto :goto_0
.end method
