.class public Lcom/sec/android/app/ve/projectcompatibility/VEGlobalConstants$VEMediaTransitions;
.super Ljava/lang/Object;
.source "VEGlobalConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/ve/projectcompatibility/VEGlobalConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "VEMediaTransitions"
.end annotation


# static fields
.field public static final VEMEDIATRANSITIONS_BARS:I = 0x2

.field public static final VEMEDIATRANSITIONS_BLACK:I = 0xb

.field public static final VEMEDIATRANSITIONS_BLOCK:I = 0x3

.field public static final VEMEDIATRANSITIONS_CUBE_TURN_A:I = 0xc

.field public static final VEMEDIATRANSITIONS_CUBE_TURN_B:I = 0xd

.field public static final VEMEDIATRANSITIONS_DICEROLL:I = 0x8

.field public static final VEMEDIATRANSITIONS_DISSOLVE:I = 0x4

.field public static final VEMEDIATRANSITIONS_FADE:I = 0x1

.field public static final VEMEDIATRANSITIONS_FADE_IN_OUT:I = 0xf

.field public static final VEMEDIATRANSITIONS_GAUSSIAN_BLUR:I = 0x10

.field public static final VEMEDIATRANSITIONS_LINEAR_DODGE_BLEND_TEXTURE:I = 0x11

.field public static final VEMEDIATRANSITIONS_NONE:I = 0x0

.field public static final VEMEDIATRANSITIONS_ORIGAMI:I = 0x5

.field public static final VEMEDIATRANSITIONS_PIXELISE:I = 0x12

.field public static final VEMEDIATRANSITIONS_ROTATION_Y_AXIS:I = 0x7

.field public static final VEMEDIATRANSITIONS_SWAP:I = 0x6

.field public static final VEMEDIATRANSITIONS_TRANSLATE_BOTTOM_WITH_TEXTURE:I = 0x13

.field public static final VEMEDIATRANSITIONS_TRANSLATE_DIAGONAL_BLEND_WITH_TEXTURE:I = 0xe

.field public static final VEMEDIATRANSITIONS_TRANSLATE_LEFT:I = 0x14

.field public static final VEMEDIATRANSITIONS_TRANSLATE_RIGHT_WITH_TEXTURE:I = 0x15

.field public static final VEMEDIATRANSITIONS_TRANSLATE_TOP:I = 0x16

.field public static final VEMEDIATRANSITIONS_WHEEL:I = 0xa

.field public static final VEMEDIATRANSITIONS_WHITE_TRANSITION:I = 0x17

.field public static final VEMEDIATRANSITIONS_WIPE_RIGHT:I = 0x9


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 271
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
