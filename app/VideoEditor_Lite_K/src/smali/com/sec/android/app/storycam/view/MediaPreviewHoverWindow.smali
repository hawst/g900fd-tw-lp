.class public Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;
.super Landroid/app/Dialog;
.source "MediaPreviewHoverWindow.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$Controller;,
        Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$ImagePreviewController;,
        Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;,
        Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowType;,
        Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$VideoPreviewController;
    }
.end annotation


# static fields
.field public static final DIALOG_AUTO_DISMISS_DELAY:I = 0x7d0

.field public static final DIALOG_DISMISS_DELAY:I = 0x64

.field public static final DIALOG_SHOW_DELAY:I = 0x190

.field private static final MSG_AUTO_DISMISS:I = 0x1

.field private static final MSG_DISMISS:I

.field private static final mHandler:Landroid/os/Handler;


# instance fields
.field private mController:Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$Controller;

.field private mDialogRect:Landroid/graphics/RectF;

.field private final mHoverListener:Landroid/view/View$OnHoverListener;

.field private mViewRect:Landroid/graphics/RectF;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 87
    new-instance v0, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$2;

    invoke-direct {v0}, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$2;-><init>()V

    sput-object v0, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;->mHandler:Landroid/os/Handler;

    .line 100
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 103
    invoke-direct {p0, p1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 48
    new-instance v0, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$1;-><init>(Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;->mHoverListener:Landroid/view/View$OnHoverListener;

    .line 104
    return-void
.end method

.method static synthetic access$0()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 87
    sget-object v0, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;)Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;->mViewRect:Landroid/graphics/RectF;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;)Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;->mDialogRect:Landroid/graphics/RectF;

    return-object v0
.end method


# virtual methods
.method public dismiss()V
    .locals 2

    .prologue
    .line 109
    sget-object v0, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 110
    sget-object v0, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 112
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;->mController:Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$Controller;

    if-eqz v0, :cond_0

    .line 113
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;->mController:Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$Controller;

    invoke-interface {v0}, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$Controller;->release()V

    .line 115
    :cond_0
    invoke-super {p0}, Landroid/app/Dialog;->dismiss()V

    .line 116
    return-void
.end method

.method public dismissDelayed()V
    .locals 4

    .prologue
    .line 160
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 161
    sget-object v1, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x64

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 163
    sget-object v1, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-static {v1, v2, p0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 164
    .local v0, "msg":Landroid/os/Message;
    sget-object v1, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x64

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 166
    .end local v0    # "msg":Landroid/os/Message;
    :cond_0
    return-void
.end method

.method public keepOnScreen()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 169
    sget-object v1, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 170
    sget-object v1, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 172
    sget-object v1, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;->mHandler:Landroid/os/Handler;

    invoke-static {v1, v3, p0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 173
    .local v0, "msg":Landroid/os/Message;
    sget-object v1, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x7d0

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 174
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 1
    .param p1, "hasFocus"    # Z

    .prologue
    .line 120
    invoke-super {p0, p1}, Landroid/app/Dialog;->onWindowFocusChanged(Z)V

    .line 121
    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 122
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;->dismiss()V

    .line 123
    :cond_0
    return-void
.end method

.method public refreshThumbnails(Ljava/util/LinkedHashMap;I)V
    .locals 1
    .param p2, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/graphics/Bitmap;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 177
    .local p1, "thumbmap":Ljava/util/LinkedHashMap;, "Ljava/util/LinkedHashMap<Ljava/lang/Integer;Landroid/graphics/Bitmap;>;"
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/LinkedHashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 178
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;->mController:Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$Controller;

    invoke-interface {v0, p1, p2}, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$Controller;->refreshThumbnails(Ljava/util/LinkedHashMap;I)V

    .line 180
    :cond_0
    return-void
.end method

.method public show(Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;)V
    .locals 11
    .param p1, "params"    # Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;

    .prologue
    const/4 v10, 0x1

    const/4 v9, -0x1

    .line 126
    if-eqz p1, :cond_3

    .line 127
    const/4 v0, 0x0

    .line 128
    .local v0, "layout":Landroid/view/View;
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;->getWindow()Landroid/view/Window;

    move-result-object v2

    .line 129
    .local v2, "window":Landroid/view/Window;
    invoke-virtual {p0, v10}, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;->requestWindowFeature(I)Z

    .line 130
    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Landroid/view/Window;->clearFlags(I)V

    .line 131
    const/16 v3, 0x33

    invoke-virtual {v2, v3}, Landroid/view/Window;->setGravity(I)V

    .line 132
    new-instance v3, Landroid/graphics/drawable/ColorDrawable;

    const/4 v4, 0x0

    invoke-direct {v3, v4}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v2, v3}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 134
    new-instance v3, Landroid/graphics/RectF;

    # getter for: Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;->mLeft:I
    invoke-static {p1}, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;->access$0(Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;)I

    move-result v4

    int-to-float v4, v4

    # getter for: Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;->mTop:I
    invoke-static {p1}, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;->access$1(Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;)I

    move-result v5

    int-to-float v5, v5

    # getter for: Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;->mLeft:I
    invoke-static {p1}, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;->access$0(Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;)I

    move-result v6

    # getter for: Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;->mWidth:I
    invoke-static {p1}, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;->access$2(Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;)I

    move-result v7

    add-int/2addr v6, v7

    int-to-float v6, v6

    # getter for: Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;->mTop:I
    invoke-static {p1}, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;->access$1(Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;)I

    move-result v7

    # getter for: Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;->mHeight:I
    invoke-static {p1}, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;->access$3(Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;)I

    move-result v8

    add-int/2addr v7, v8

    int-to-float v7, v7

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v3, p0, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;->mDialogRect:Landroid/graphics/RectF;

    .line 135
    # getter for: Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;->mViewRect:Landroid/graphics/RectF;
    invoke-static {p1}, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;->access$7(Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;)Landroid/graphics/RectF;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;->mViewRect:Landroid/graphics/RectF;

    .line 137
    # getter for: Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;->mMediaType:Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowType;
    invoke-static {p1}, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;->access$8(Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;)Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowType;

    move-result-object v3

    sget-object v4, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowType;->IMAGE_HOVER_WINDOW:Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowType;

    if-ne v3, v4, :cond_4

    .line 138
    new-instance v3, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$ImagePreviewController;

    invoke-direct {v3}, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$ImagePreviewController;-><init>()V

    iput-object v3, p0, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;->mController:Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$Controller;

    .line 142
    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;->mController:Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$Controller;

    if-eqz v3, :cond_1

    .line 143
    iget-object v3, p0, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;->mController:Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$Controller;

    iget-object v4, p0, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;->mHoverListener:Landroid/view/View$OnHoverListener;

    invoke-interface {v3, p1, v4}, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$Controller;->doLayout(Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;Landroid/view/View$OnHoverListener;)Landroid/view/View;

    move-result-object v0

    .line 145
    :cond_1
    if-eqz v0, :cond_2

    .line 146
    const/16 v3, 0x400

    invoke-virtual {v2, v3}, Landroid/view/Window;->addFlags(I)V

    .line 147
    invoke-virtual {p0, v0}, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;->setContentView(Landroid/view/View;)V

    .line 148
    invoke-virtual {v2, v9, v9}, Landroid/view/Window;->setLayout(II)V

    .line 149
    iget-object v3, p0, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;->mHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 152
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;->show()V

    .line 154
    sget-object v3, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;->mHandler:Landroid/os/Handler;

    invoke-static {v3, v10, p0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 155
    .local v1, "msg":Landroid/os/Message;
    sget-object v3, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;->mHandler:Landroid/os/Handler;

    const-wide/16 v4, 0x7d0

    invoke-virtual {v3, v1, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 157
    .end local v0    # "layout":Landroid/view/View;
    .end local v1    # "msg":Landroid/os/Message;
    .end local v2    # "window":Landroid/view/Window;
    :cond_3
    return-void

    .line 139
    .restart local v0    # "layout":Landroid/view/View;
    .restart local v2    # "window":Landroid/view/Window;
    :cond_4
    # getter for: Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;->mMediaType:Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowType;
    invoke-static {p1}, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;->access$8(Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowParams;)Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowType;

    move-result-object v3

    sget-object v4, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowType;->VIDEO_HOVER_WINDOW:Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$MediaHoverWindowType;

    if-ne v3, v4, :cond_0

    .line 140
    new-instance v3, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$VideoPreviewController;

    invoke-direct {v3, p0}, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$VideoPreviewController;-><init>(Landroid/app/Dialog;)V

    iput-object v3, p0, Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow;->mController:Lcom/sec/android/app/storycam/view/MediaPreviewHoverWindow$Controller;

    goto :goto_0
.end method
