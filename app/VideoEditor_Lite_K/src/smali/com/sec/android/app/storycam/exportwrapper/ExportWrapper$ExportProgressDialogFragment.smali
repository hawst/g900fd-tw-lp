.class public Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment;
.super Landroid/app/DialogFragment;
.source "ExportWrapper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ExportProgressDialogFragment"
.end annotation


# static fields
.field public static final MAX_PROGRESS:I = 0x64

.field private static mPauseText:Landroid/widget/TextView;

.field private static mProgDlg:Landroid/app/AlertDialog;

.field private static mProgText:Landroid/widget/TextView;

.field private static mProgView:Landroid/widget/ProgressBar;


# instance fields
.field public dialogView:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 145
    sput-object v0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment;->mProgDlg:Landroid/app/AlertDialog;

    .line 146
    sput-object v0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment;->mProgView:Landroid/widget/ProgressBar;

    .line 147
    sput-object v0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment;->mProgText:Landroid/widget/TextView;

    .line 148
    sput-object v0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment;->mPauseText:Landroid/widget/TextView;

    .line 167
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 151
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 152
    return-void
.end method

.method static synthetic access$0()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 148
    sget-object v0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment;->mPauseText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1()Landroid/app/AlertDialog;
    .locals 1

    .prologue
    .line 145
    sget-object v0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment;->mProgDlg:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$2()Landroid/widget/ProgressBar;
    .locals 1

    .prologue
    .line 146
    sget-object v0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment;->mProgView:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic access$3()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 147
    sget-object v0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment;->mProgText:Landroid/widget/TextView;

    return-object v0
.end method

.method static showDialog(Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;)Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment;
    .locals 3
    .param p0, "wrapper"    # Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;

    .prologue
    .line 157
    :try_start_0
    new-instance v0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment;

    invoke-direct {v0}, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment;-><init>()V

    .line 158
    .local v0, "dialog":Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment;
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/android/app/ve/common/AndroidUtils;->showDialog(Landroid/app/DialogFragment;Landroid/app/Activity;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 164
    .end local v0    # "dialog":Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment;
    :goto_0
    return-object v0

    .line 160
    :catch_0
    move-exception v1

    .line 161
    .local v1, "ex":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 164
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x0

    .line 183
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 184
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f030013

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment;->dialogView:Landroid/view/View;

    .line 185
    const v2, 0x7f070074

    invoke-static {v2}, Lcom/sec/android/app/ve/VEApp;->getStringValue(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 186
    iget-object v2, p0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment;->dialogView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 188
    iget-object v2, p0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment;->dialogView:Landroid/view/View;

    const v3, 0x7f0d0049

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    sput-object v2, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment;->mPauseText:Landroid/widget/TextView;

    .line 189
    iget-object v2, p0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment;->dialogView:Landroid/view/View;

    const v3, 0x7f0d004a

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    sput-object v2, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment;->mProgText:Landroid/widget/TextView;

    .line 191
    iget-object v2, p0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment;->dialogView:Landroid/view/View;

    const v3, 0x7f0d004b

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ProgressBar;

    sput-object v2, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment;->mProgView:Landroid/widget/ProgressBar;

    .line 192
    sget-object v2, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment;->mProgView:Landroid/widget/ProgressBar;

    const/16 v3, 0x64

    invoke-virtual {v2, v3}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 193
    const/4 v1, 0x0

    .line 194
    .local v1, "mProgress":I
    sget-object v2, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment;->mProgView:Landroid/widget/ProgressBar;

    invoke-virtual {v2, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 196
    const v2, 0x7f0700ae

    invoke-static {v2}, Lcom/sec/android/app/ve/VEApp;->getStringValue(I)Ljava/lang/String;

    move-result-object v2

    .line 197
    new-instance v3, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment$1;

    invoke-direct {v3, p0}, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment$1;-><init>(Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment;)V

    .line 196
    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 205
    const v2, 0x7f07005e

    invoke-static {v2}, Lcom/sec/android/app/ve/VEApp;->getStringValue(I)Ljava/lang/String;

    move-result-object v2

    .line 206
    new-instance v3, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment$2;

    invoke-direct {v3, p0}, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment$2;-><init>(Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment;)V

    .line 205
    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 213
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    sput-object v2, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment;->mProgDlg:Landroid/app/AlertDialog;

    .line 215
    sget-object v2, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment;->mProgDlg:Landroid/app/AlertDialog;

    new-instance v3, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment$3;

    invoke-direct {v3, p0}, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment$3;-><init>(Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment;)V

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 226
    sget-object v2, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment;->mProgDlg:Landroid/app/AlertDialog;

    new-instance v3, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment$4;

    invoke-direct {v3, p0}, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment$4;-><init>(Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment;)V

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 242
    sget-object v2, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment;->mProgDlg:Landroid/app/AlertDialog;

    invoke-virtual {v2, v5}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 243
    sget-object v2, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment;->mProgDlg:Landroid/app/AlertDialog;

    return-object v2
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 171
    invoke-super {p0}, Landroid/app/DialogFragment;->onDestroyView()V

    .line 172
    iget-object v1, p0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment;->dialogView:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 173
    iget-object v1, p0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportProgressDialogFragment;->dialogView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 174
    .local v0, "parentViewGroup":Landroid/view/ViewGroup;
    if-eqz v0, :cond_0

    .line 175
    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 178
    .end local v0    # "parentViewGroup":Landroid/view/ViewGroup;
    :cond_0
    return-void
.end method
