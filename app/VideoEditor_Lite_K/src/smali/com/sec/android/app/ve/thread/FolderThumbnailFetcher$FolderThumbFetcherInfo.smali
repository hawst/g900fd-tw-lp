.class public Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher$FolderThumbFetcherInfo;
.super Ljava/lang/Object;
.source "FolderThumbnailFetcher.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FolderThumbFetcherInfo"
.end annotation


# instance fields
.field private mBucketId:Ljava/lang/String;

.field private mThumbnailPaths:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 0
    .param p1, "bucketId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 310
    .local p2, "thumbPaths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 311
    iput-object p1, p0, Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher$FolderThumbFetcherInfo;->mBucketId:Ljava/lang/String;

    .line 312
    iput-object p2, p0, Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher$FolderThumbFetcherInfo;->mThumbnailPaths:Ljava/util/ArrayList;

    .line 313
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher$FolderThumbFetcherInfo;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 307
    iget-object v0, p0, Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher$FolderThumbFetcherInfo;->mBucketId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher$FolderThumbFetcherInfo;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 308
    iget-object v0, p0, Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher$FolderThumbFetcherInfo;->mThumbnailPaths:Ljava/util/ArrayList;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 317
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 319
    .local v2, "str":Ljava/lang/StringBuilder;
    const-string v3, "Thumbnails requested:\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 320
    iget-object v3, p0, Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher$FolderThumbFetcherInfo;->mThumbnailPaths:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 321
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v0, :cond_0

    .line 324
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 322
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/sec/android/app/ve/thread/FolderThumbnailFetcher$FolderThumbFetcherInfo;->mThumbnailPaths:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "\n"

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 321
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
