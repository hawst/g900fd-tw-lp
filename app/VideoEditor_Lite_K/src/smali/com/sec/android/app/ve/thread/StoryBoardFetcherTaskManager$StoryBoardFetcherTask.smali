.class public Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;
.super Ljava/lang/Object;
.source "StoryBoardFetcherTaskManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "StoryBoardFetcherTask"
.end annotation


# instance fields
.field callback:Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$FetcherCallBack;

.field elemType:I

.field filePath:Ljava/lang/String;

.field filename_pos:Ljava/lang/String;

.field isAssetResource:Z

.field mAssetManager:Landroid/content/res/AssetManager;

.field mCacheable:Z

.field private mExecutingThread:Ljava/lang/Thread;

.field mMediaEmpty:Z

.field private mRetriever:Landroid/media/MediaMetadataRetriever;

.field thumbPos:I

.field time:J


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 128
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 95
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;->elemType:I

    .line 96
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;->mCacheable:Z

    .line 130
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;JILcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$FetcherCallBack;Ljava/lang/String;Landroid/content/res/AssetManager;ZZ)V
    .locals 2
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "time"    # J
    .param p4, "thumbPos"    # I
    .param p5, "callback"    # Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$FetcherCallBack;
    .param p6, "filename_pos"    # Ljava/lang/String;
    .param p7, "am"    # Landroid/content/res/AssetManager;
    .param p8, "isAssetResource"    # Z
    .param p9, "mMediaEmpty"    # Z

    .prologue
    .line 132
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 95
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;->elemType:I

    .line 96
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;->mCacheable:Z

    .line 133
    iput-object p1, p0, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;->filePath:Ljava/lang/String;

    .line 134
    iput-wide p2, p0, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;->time:J

    .line 135
    iput p4, p0, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;->thumbPos:I

    .line 136
    iput-object p5, p0, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;->callback:Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$FetcherCallBack;

    .line 137
    iput-object p6, p0, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;->filename_pos:Ljava/lang/String;

    .line 138
    iput-boolean p8, p0, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;->isAssetResource:Z

    .line 139
    iput-object p7, p0, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;->mAssetManager:Landroid/content/res/AssetManager;

    .line 140
    iput-boolean p9, p0, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;->mMediaEmpty:Z

    .line 141
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;JILcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$FetcherCallBack;Ljava/lang/String;Landroid/content/res/AssetManager;ZZI)V
    .locals 0
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "time"    # J
    .param p4, "thumbPos"    # I
    .param p5, "callback"    # Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$FetcherCallBack;
    .param p6, "filename_pos"    # Ljava/lang/String;
    .param p7, "am"    # Landroid/content/res/AssetManager;
    .param p8, "isAssetResource"    # Z
    .param p9, "mMediaEmpty"    # Z
    .param p10, "elemType"    # I

    .prologue
    .line 144
    invoke-direct/range {p0 .. p9}, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;-><init>(Ljava/lang/String;JILcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$FetcherCallBack;Ljava/lang/String;Landroid/content/res/AssetManager;ZZ)V

    .line 145
    iput p10, p0, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;->elemType:I

    .line 146
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;JILcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$FetcherCallBack;Ljava/lang/String;Landroid/content/res/AssetManager;ZZIZ)V
    .locals 0
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "time"    # J
    .param p4, "thumbPos"    # I
    .param p5, "callback"    # Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$FetcherCallBack;
    .param p6, "filename_pos"    # Ljava/lang/String;
    .param p7, "am"    # Landroid/content/res/AssetManager;
    .param p8, "isAssetResource"    # Z
    .param p9, "mMediaEmpty"    # Z
    .param p10, "elemType"    # I
    .param p11, "shouldPutIntoCache"    # Z

    .prologue
    .line 150
    invoke-direct/range {p0 .. p10}, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;-><init>(Ljava/lang/String;JILcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$FetcherCallBack;Ljava/lang/String;Landroid/content/res/AssetManager;ZZI)V

    .line 151
    iput-boolean p11, p0, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;->mCacheable:Z

    .line 152
    return-void
.end method

.method public static createDummyTask(Ljava/lang/String;J)Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;
    .locals 1
    .param p0, "filePath"    # Ljava/lang/String;
    .param p1, "time"    # J

    .prologue
    .line 112
    new-instance v0, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;

    invoke-direct {v0}, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;-><init>()V

    .line 113
    .local v0, "task":Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;
    iput-object p0, v0, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;->filePath:Ljava/lang/String;

    .line 114
    iput-wide p1, v0, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;->time:J

    .line 115
    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 120
    const/4 v0, 0x0

    .line 121
    .local v0, "equal":Z
    instance-of v2, p1, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;

    if-eqz v2, :cond_0

    move-object v1, p1

    .line 122
    check-cast v1, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;

    .line 123
    .local v1, "task":Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;
    iget-object v2, p0, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;->filePath:Ljava/lang/String;

    iget-object v3, v1, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;->filePath:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-wide v2, p0, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;->time:J

    iget-wide v4, v1, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;->time:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_1

    const/4 v0, 0x1

    .line 125
    .end local v1    # "task":Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;
    :cond_0
    :goto_0
    return v0

    .line 123
    .restart local v1    # "task":Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getExecutingThread()Ljava/lang/Thread;
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;->mExecutingThread:Ljava/lang/Thread;

    return-object v0
.end method

.method public run()V
    .locals 15

    .prologue
    const/4 v14, 0x0

    const/4 v8, 0x1

    .line 163
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;->mExecutingThread:Ljava/lang/Thread;

    .line 165
    iget-object v1, p0, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;->callback:Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$FetcherCallBack;

    iget-wide v2, p0, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;->time:J

    invoke-interface {v1, v2, v3}, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$FetcherCallBack;->isTargetViewExist(J)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 166
    const/4 v5, 0x0

    .line 167
    .local v5, "bitmap":Landroid/graphics/Bitmap;
    iget v1, p0, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;->elemType:I

    if-ne v1, v8, :cond_5

    .line 169
    iget-object v1, p0, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;->mAssetManager:Landroid/content/res/AssetManager;

    iget-object v2, p0, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;->filePath:Ljava/lang/String;

    iget-boolean v3, p0, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;->isAssetResource:Z

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/ve/util/CommonUtils;->getRetrieverForSource(Landroid/content/res/AssetManager;Ljava/lang/String;Z)Landroid/media/MediaMetadataRetriever;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;->mRetriever:Landroid/media/MediaMetadataRetriever;

    .line 170
    iget-boolean v1, p0, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;->mMediaEmpty:Z

    if-nez v1, :cond_4

    .line 172
    const/4 v0, 0x0

    .line 173
    .local v0, "shouldPutIntoCache":Z
    sget-object v1, Lcom/sec/android/app/ve/VEApp;->gThumbFetcher:Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcher;

    iget-object v2, p0, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;->filePath:Ljava/lang/String;

    new-instance v3, Ljava/lang/Long;

    iget-wide v6, p0, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;->time:J

    invoke-direct {v3, v6, v7}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v3}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v8}, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcher;->getVideoThumbnailForTimeline(Ljava/lang/String;Ljava/lang/String;Z)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 174
    if-nez v5, :cond_1

    .line 175
    iget-object v1, p0, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;->mRetriever:Landroid/media/MediaMetadataRetriever;

    iget-wide v2, p0, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;->time:J

    long-to-float v2, v2

    invoke-static {v1, v2}, Lcom/sec/android/app/ve/common/MediaUtils;->getVideoFrame(Landroid/media/MediaMetadataRetriever;F)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 176
    iget-boolean v1, p0, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;->mCacheable:Z

    if-eqz v1, :cond_0

    .line 177
    iget-object v1, p0, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;->callback:Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$FetcherCallBack;

    invoke-interface {v1, v5}, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$FetcherCallBack;->resizeBitmapToCache(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 178
    :cond_0
    const/4 v0, 0x1

    .line 181
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;->callback:Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$FetcherCallBack;

    iget-wide v2, p0, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;->time:J

    iget v4, p0, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;->thumbPos:I

    iget-object v6, p0, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;->filePath:Ljava/lang/String;

    iget-object v7, p0, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;->filename_pos:Ljava/lang/String;

    move-object v9, p0

    invoke-interface/range {v1 .. v9}, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$FetcherCallBack;->bitmapCreated(JILandroid/graphics/Bitmap;Ljava/lang/String;Ljava/lang/String;ZLcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;)V

    .line 182
    iget-boolean v1, p0, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;->mCacheable:Z

    if-eqz v1, :cond_2

    if-eqz v0, :cond_2

    .line 183
    sget-object v1, Lcom/sec/android/app/ve/VEApp;->gThumbFetcher:Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcher;

    iget-object v2, p0, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;->filePath:Ljava/lang/String;

    new-instance v3, Ljava/lang/Long;

    iget-wide v6, p0, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;->time:J

    invoke-direct {v3, v6, v7}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v3}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v5}, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcher;->putTimelineBitmapToCache(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 191
    .end local v0    # "shouldPutIntoCache":Z
    :cond_2
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;->mRetriever:Landroid/media/MediaMetadataRetriever;

    invoke-static {v1}, Lcom/sec/android/app/ve/util/CommonUtils;->releaseRetriever(Landroid/media/MediaMetadataRetriever;)V

    .line 210
    .end local v5    # "bitmap":Landroid/graphics/Bitmap;
    :cond_3
    :goto_1
    return-void

    .line 187
    .restart local v5    # "bitmap":Landroid/graphics/Bitmap;
    :cond_4
    sget v1, Lcom/samsung/app/video/editor/external/Constants;->THUMBNAIL_IMAGE_SIZE:I

    sget v2, Lcom/samsung/app/video/editor/external/Constants;->THUMBNAIL_IMAGE_HEIGHT:I

    sget-object v3, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 188
    invoke-virtual {v5, v14}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 189
    iget-object v1, p0, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;->callback:Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$FetcherCallBack;

    iget-wide v2, p0, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;->time:J

    iget v4, p0, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;->thumbPos:I

    iget-object v6, p0, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;->filePath:Ljava/lang/String;

    iget-object v7, p0, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;->filename_pos:Ljava/lang/String;

    move-object v9, p0

    invoke-interface/range {v1 .. v9}, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$FetcherCallBack;->bitmapCreated(JILandroid/graphics/Bitmap;Ljava/lang/String;Ljava/lang/String;ZLcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;)V

    goto :goto_0

    .line 193
    :cond_5
    iget v1, p0, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;->elemType:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_3

    .line 194
    const/4 v0, 0x0

    .line 195
    .restart local v0    # "shouldPutIntoCache":Z
    sget-object v1, Lcom/sec/android/app/ve/VEApp;->gThumbFetcher:Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcher;

    iget-object v2, p0, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;->filePath:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;->filename_pos:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v8}, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcher;->getImageThumbnailForTimeline(Ljava/lang/String;Ljava/lang/String;Z)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 196
    if-nez v5, :cond_7

    .line 198
    iget-object v9, p0, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;->filePath:Ljava/lang/String;

    const/16 v10, 0x2e0

    const/16 v11, 0x1a0

    const/high16 v12, -0x1000000

    sget-object v13, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static/range {v9 .. v14}, Lcom/sec/android/app/ve/common/MediaUtils;->getFitCenterBitmap(Ljava/lang/String;IIILandroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 200
    if-nez v5, :cond_6

    .line 201
    iget-object v1, p0, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;->filePath:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/sec/android/app/ve/common/MediaUtils;->getImageBitmap(Ljava/lang/String;I)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 203
    :cond_6
    const/4 v0, 0x1

    .line 205
    :cond_7
    iget-object v1, p0, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;->callback:Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$FetcherCallBack;

    iget-wide v2, p0, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;->time:J

    iget v4, p0, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;->thumbPos:I

    iget-object v6, p0, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;->filePath:Ljava/lang/String;

    iget-object v7, p0, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;->filename_pos:Ljava/lang/String;

    move-object v9, p0

    invoke-interface/range {v1 .. v9}, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$FetcherCallBack;->bitmapCreated(JILandroid/graphics/Bitmap;Ljava/lang/String;Ljava/lang/String;ZLcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;)V

    .line 206
    iget-boolean v1, p0, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;->mCacheable:Z

    if-eqz v1, :cond_3

    if-eqz v0, :cond_3

    .line 207
    sget-object v1, Lcom/sec/android/app/ve/VEApp;->gThumbFetcher:Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcher;

    iget-object v2, p0, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;->filePath:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;->filename_pos:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v5}, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcher;->putTimelineBitmapToCache(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    goto :goto_1
.end method

.method public setRetriever(Landroid/media/MediaMetadataRetriever;)V
    .locals 0
    .param p1, "retriever"    # Landroid/media/MediaMetadataRetriever;

    .prologue
    .line 105
    iput-object p1, p0, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;->mRetriever:Landroid/media/MediaMetadataRetriever;

    .line 106
    return-void
.end method
