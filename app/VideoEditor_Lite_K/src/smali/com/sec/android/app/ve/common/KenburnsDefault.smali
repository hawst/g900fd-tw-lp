.class public Lcom/sec/android/app/ve/common/KenburnsDefault;
.super Ljava/lang/Object;
.source "KenburnsDefault.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/ve/common/KenburnsDefault$ApplyDefaultKenburnsTask;,
        Lcom/sec/android/app/ve/common/KenburnsDefault$KenburnData;
    }
.end annotation


# static fields
.field private static final IMAGE_RESOLUTION_HORIZONTAL_WIDE:I = 0xc

.field private static final IMAGE_RESOLUTION_SQUARE:I = 0xd

.field private static final IMAGE_RESOLUTION_VERTICAL_LONG:I = 0xb

.field public static applyKenburnsDone:Z

.field private static endMatrix:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<[F>;"
        }
    .end annotation
.end field

.field private static endRect:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/RectF;",
            ">;"
        }
    .end annotation
.end field

.field private static startMatrix:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<[F>;"
        }
    .end annotation
.end field

.field private static startRect:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/RectF;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mHandler:Landroid/os/Handler;

.field private final mPoolLstnr:Lcom/sec/android/app/ve/thread/AsyncTaskPool$Listener;

.field private final mTaskPool:Lcom/sec/android/app/ve/thread/AsyncTaskPool;

.field private previewHeight:I

.field private previewWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/app/ve/common/KenburnsDefault;->applyKenburnsDone:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/ve/common/KenburnsDefault;->mHandler:Landroid/os/Handler;

    .line 50
    new-instance v0, Lcom/sec/android/app/ve/thread/AsyncTaskPool;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sec/android/app/ve/thread/AsyncTaskPool;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/ve/common/KenburnsDefault;->mTaskPool:Lcom/sec/android/app/ve/thread/AsyncTaskPool;

    .line 511
    new-instance v0, Lcom/sec/android/app/ve/common/KenburnsDefault$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/ve/common/KenburnsDefault$1;-><init>(Lcom/sec/android/app/ve/common/KenburnsDefault;)V

    iput-object v0, p0, Lcom/sec/android/app/ve/common/KenburnsDefault;->mPoolLstnr:Lcom/sec/android/app/ve/thread/AsyncTaskPool$Listener;

    .line 56
    invoke-direct {p0}, Lcom/sec/android/app/ve/common/KenburnsDefault;->init()V

    .line 57
    return-void
.end method

.method static synthetic access$0()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/sec/android/app/ve/common/KenburnsDefault;->startMatrix:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/sec/android/app/ve/common/KenburnsDefault;->startRect:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$2()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/sec/android/app/ve/common/KenburnsDefault;->endMatrix:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$3()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/sec/android/app/ve/common/KenburnsDefault;->endRect:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$4(Lcom/sec/android/app/ve/common/KenburnsDefault;)Lcom/sec/android/app/ve/thread/AsyncTaskPool;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/app/ve/common/KenburnsDefault;->mTaskPool:Lcom/sec/android/app/ve/thread/AsyncTaskPool;

    return-object v0
.end method

.method private init()V
    .locals 2

    .prologue
    .line 61
    sget v0, Lcom/sec/android/app/ve/common/ConfigUtils;->PREVIEW_WIDTH:I

    iput v0, p0, Lcom/sec/android/app/ve/common/KenburnsDefault;->previewWidth:I

    .line 62
    sget v0, Lcom/sec/android/app/ve/common/ConfigUtils;->PREVIEW_HEIGHT:I

    iput v0, p0, Lcom/sec/android/app/ve/common/KenburnsDefault;->previewHeight:I

    .line 65
    iget v0, p0, Lcom/sec/android/app/ve/common/KenburnsDefault;->previewWidth:I

    iget v1, p0, Lcom/sec/android/app/ve/common/KenburnsDefault;->previewHeight:I

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/ve/common/KenburnsDefault;->initDefaultKenburnDimensions(II)V

    .line 66
    return-void
.end method

.method private initDefaultKenburnDimensions(II)V
    .locals 11
    .param p1, "_Width"    # I
    .param p2, "_Height"    # I

    .prologue
    .line 96
    const v0, 0x3d4ccce8    # 0.0500001f

    .line 100
    .local v0, "percentIncr":F
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    new-array v2, v2, [[F

    const/4 v3, 0x0

    .line 103
    const/16 v4, 0x9

    new-array v4, v4, [F

    fill-array-data v4, :array_0

    .line 104
    aput-object v4, v2, v3

    const/4 v3, 0x1

    .line 106
    const/16 v4, 0x9

    new-array v4, v4, [F

    const/4 v5, 0x0

    const v6, 0x3f866666    # 1.05f

    aput v6, v4, v5

    const/4 v5, 0x1

    const/4 v6, 0x0

    aput v6, v4, v5

    const/4 v5, 0x2

    neg-int v6, p1

    int-to-float v6, v6

    const v7, 0x3d4ccce8    # 0.0500001f

    mul-float/2addr v6, v7

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    aput v6, v4, v5

    const/4 v5, 0x3

    const/4 v6, 0x0

    aput v6, v4, v5

    const/4 v5, 0x4

    const v6, 0x3f866666    # 1.05f

    aput v6, v4, v5

    const/4 v5, 0x5

    neg-int v6, p2

    int-to-float v6, v6

    const v7, 0x3d4ccce8    # 0.0500001f

    mul-float/2addr v6, v7

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    aput v6, v4, v5

    const/4 v5, 0x6

    const/4 v6, 0x0

    aput v6, v4, v5

    const/4 v5, 0x7

    .line 107
    const/4 v6, 0x0

    aput v6, v4, v5

    const/16 v5, 0x8

    const/high16 v6, 0x3f800000    # 1.0f

    aput v6, v4, v5

    aput-object v4, v2, v3

    const/4 v3, 0x2

    .line 110
    const/16 v4, 0x9

    new-array v4, v4, [F

    const/4 v5, 0x0

    const v6, 0x3f866666    # 1.05f

    aput v6, v4, v5

    const/4 v5, 0x1

    const/4 v6, 0x0

    aput v6, v4, v5

    const/4 v5, 0x2

    neg-int v6, p1

    int-to-float v6, v6

    const v7, 0x3d4ccce8    # 0.0500001f

    mul-float/2addr v6, v7

    aput v6, v4, v5

    const/4 v5, 0x3

    const/4 v6, 0x0

    aput v6, v4, v5

    const/4 v5, 0x4

    const v6, 0x3f866666    # 1.05f

    aput v6, v4, v5

    const/4 v5, 0x5

    neg-int v6, p2

    int-to-float v6, v6

    const v7, 0x3d4ccce8    # 0.0500001f

    mul-float/2addr v6, v7

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    aput v6, v4, v5

    const/4 v5, 0x6

    const/4 v6, 0x0

    aput v6, v4, v5

    const/4 v5, 0x7

    const/4 v6, 0x0

    aput v6, v4, v5

    const/16 v5, 0x8

    const/high16 v6, 0x3f800000    # 1.0f

    aput v6, v4, v5

    aput-object v4, v2, v3

    const/4 v3, 0x3

    .line 112
    const/16 v4, 0x9

    new-array v4, v4, [F

    const/4 v5, 0x0

    const v6, 0x3f866666    # 1.05f

    aput v6, v4, v5

    const/4 v5, 0x1

    const/4 v6, 0x0

    aput v6, v4, v5

    const/4 v5, 0x2

    const/4 v6, 0x0

    aput v6, v4, v5

    const/4 v5, 0x3

    const/4 v6, 0x0

    aput v6, v4, v5

    const/4 v5, 0x4

    const v6, 0x3f866666    # 1.05f

    aput v6, v4, v5

    const/4 v5, 0x5

    neg-int v6, p2

    int-to-float v6, v6

    const v7, 0x3d4ccce8    # 0.0500001f

    mul-float/2addr v6, v7

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    aput v6, v4, v5

    const/4 v5, 0x6

    const/4 v6, 0x0

    aput v6, v4, v5

    const/4 v5, 0x7

    const/4 v6, 0x0

    aput v6, v4, v5

    const/16 v5, 0x8

    const/high16 v6, 0x3f800000    # 1.0f

    aput v6, v4, v5

    aput-object v4, v2, v3

    const/4 v3, 0x4

    .line 114
    const/16 v4, 0x9

    new-array v4, v4, [F

    fill-array-data v4, :array_1

    aput-object v4, v2, v3

    const/4 v3, 0x5

    .line 116
    const/16 v4, 0x9

    new-array v4, v4, [F

    const/4 v5, 0x0

    const v6, 0x3f866666    # 1.05f

    aput v6, v4, v5

    const/4 v5, 0x1

    const/4 v6, 0x0

    aput v6, v4, v5

    const/4 v5, 0x2

    neg-int v6, p1

    int-to-float v6, v6

    const v7, 0x3d4ccce8    # 0.0500001f

    mul-float/2addr v6, v7

    aput v6, v4, v5

    const/4 v5, 0x3

    const/4 v6, 0x0

    aput v6, v4, v5

    const/4 v5, 0x4

    const v6, 0x3f866666    # 1.05f

    aput v6, v4, v5

    const/4 v5, 0x5

    neg-int v6, p2

    int-to-float v6, v6

    const v7, 0x3d4ccce8    # 0.0500001f

    mul-float/2addr v6, v7

    aput v6, v4, v5

    const/4 v5, 0x6

    const/4 v6, 0x0

    aput v6, v4, v5

    const/4 v5, 0x7

    const/4 v6, 0x0

    aput v6, v4, v5

    const/16 v5, 0x8

    const/high16 v6, 0x3f800000    # 1.0f

    aput v6, v4, v5

    aput-object v4, v2, v3

    const/4 v3, 0x6

    .line 119
    const/16 v4, 0x9

    new-array v4, v4, [F

    fill-array-data v4, :array_2

    aput-object v4, v2, v3

    const/4 v3, 0x7

    .line 121
    const/16 v4, 0x9

    new-array v4, v4, [F

    const/4 v5, 0x0

    const v6, 0x3f866666    # 1.05f

    aput v6, v4, v5

    const/4 v5, 0x1

    const/4 v6, 0x0

    aput v6, v4, v5

    const/4 v5, 0x2

    const/4 v6, 0x0

    aput v6, v4, v5

    const/4 v5, 0x3

    const/4 v6, 0x0

    aput v6, v4, v5

    const/4 v5, 0x4

    const v6, 0x3f866666    # 1.05f

    aput v6, v4, v5

    const/4 v5, 0x5

    neg-int v6, p2

    int-to-float v6, v6

    const v7, 0x3d4ccce8    # 0.0500001f

    mul-float/2addr v6, v7

    aput v6, v4, v5

    const/4 v5, 0x6

    const/4 v6, 0x0

    aput v6, v4, v5

    const/4 v5, 0x7

    const/4 v6, 0x0

    aput v6, v4, v5

    const/16 v5, 0x8

    const/high16 v6, 0x3f800000    # 1.0f

    aput v6, v4, v5

    aput-object v4, v2, v3

    const/16 v3, 0x8

    .line 123
    const/16 v4, 0x9

    new-array v4, v4, [F

    const/4 v5, 0x0

    const v6, 0x3f866666    # 1.05f

    aput v6, v4, v5

    const/4 v5, 0x1

    const/4 v6, 0x0

    aput v6, v4, v5

    const/4 v5, 0x2

    neg-int v6, p1

    int-to-float v6, v6

    const v7, 0x3d4ccce8    # 0.0500001f

    mul-float/2addr v6, v7

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    aput v6, v4, v5

    const/4 v5, 0x3

    const/4 v6, 0x0

    aput v6, v4, v5

    const/4 v5, 0x4

    const v6, 0x3f866666    # 1.05f

    aput v6, v4, v5

    const/4 v5, 0x5

    neg-int v6, p2

    int-to-float v6, v6

    const v7, 0x3d4ccce8    # 0.0500001f

    mul-float/2addr v6, v7

    aput v6, v4, v5

    const/4 v5, 0x6

    const/4 v6, 0x0

    aput v6, v4, v5

    const/4 v5, 0x7

    const/4 v6, 0x0

    aput v6, v4, v5

    const/16 v5, 0x8

    .line 124
    const/high16 v6, 0x3f800000    # 1.0f

    aput v6, v4, v5

    aput-object v4, v2, v3

    const/16 v3, 0x9

    .line 126
    const/16 v4, 0x9

    new-array v4, v4, [F

    const/4 v5, 0x0

    const v6, 0x3f866666    # 1.05f

    aput v6, v4, v5

    const/4 v5, 0x1

    const/4 v6, 0x0

    aput v6, v4, v5

    const/4 v5, 0x2

    neg-int v6, p1

    int-to-float v6, v6

    const v7, 0x3d4ccce8    # 0.0500001f

    mul-float/2addr v6, v7

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    aput v6, v4, v5

    const/4 v5, 0x3

    const/4 v6, 0x0

    aput v6, v4, v5

    const/4 v5, 0x4

    const v6, 0x3f866666    # 1.05f

    aput v6, v4, v5

    const/4 v5, 0x5

    const/4 v6, 0x0

    aput v6, v4, v5

    const/4 v5, 0x6

    const/4 v6, 0x0

    aput v6, v4, v5

    const/4 v5, 0x7

    .line 127
    const/4 v6, 0x0

    aput v6, v4, v5

    const/16 v5, 0x8

    const/high16 v6, 0x3f800000    # 1.0f

    aput v6, v4, v5

    aput-object v4, v2, v3

    .line 100
    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    sput-object v1, Lcom/sec/android/app/ve/common/KenburnsDefault;->startMatrix:Ljava/util/ArrayList;

    .line 132
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    new-array v2, v2, [[F

    const/4 v3, 0x0

    .line 135
    const/16 v4, 0x9

    new-array v4, v4, [F

    const/4 v5, 0x0

    const v6, 0x3f866666    # 1.05f

    aput v6, v4, v5

    const/4 v5, 0x1

    const/4 v6, 0x0

    aput v6, v4, v5

    const/4 v5, 0x2

    neg-int v6, p1

    int-to-float v6, v6

    const v7, 0x3d4ccce8    # 0.0500001f

    mul-float/2addr v6, v7

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    aput v6, v4, v5

    const/4 v5, 0x3

    const/4 v6, 0x0

    aput v6, v4, v5

    const/4 v5, 0x4

    const v6, 0x3f866666    # 1.05f

    aput v6, v4, v5

    const/4 v5, 0x5

    neg-int v6, p2

    int-to-float v6, v6

    const v7, 0x3d4ccce8    # 0.0500001f

    mul-float/2addr v6, v7

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    aput v6, v4, v5

    const/4 v5, 0x6

    const/4 v6, 0x0

    aput v6, v4, v5

    const/4 v5, 0x7

    .line 136
    const/4 v6, 0x0

    aput v6, v4, v5

    const/16 v5, 0x8

    const/high16 v6, 0x3f800000    # 1.0f

    aput v6, v4, v5

    aput-object v4, v2, v3

    const/4 v3, 0x1

    .line 138
    const/16 v4, 0x9

    new-array v4, v4, [F

    fill-array-data v4, :array_3

    .line 139
    aput-object v4, v2, v3

    const/4 v3, 0x2

    .line 141
    const/16 v4, 0x9

    new-array v4, v4, [F

    const/4 v5, 0x0

    const v6, 0x3f866666    # 1.05f

    aput v6, v4, v5

    const/4 v5, 0x1

    const/4 v6, 0x0

    aput v6, v4, v5

    const/4 v5, 0x2

    const/4 v6, 0x0

    aput v6, v4, v5

    const/4 v5, 0x3

    const/4 v6, 0x0

    aput v6, v4, v5

    const/4 v5, 0x4

    const v6, 0x3f866666    # 1.05f

    aput v6, v4, v5

    const/4 v5, 0x5

    neg-int v6, p2

    int-to-float v6, v6

    const v7, 0x3d4ccce8    # 0.0500001f

    mul-float/2addr v6, v7

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    aput v6, v4, v5

    const/4 v5, 0x6

    const/4 v6, 0x0

    aput v6, v4, v5

    const/4 v5, 0x7

    const/4 v6, 0x0

    aput v6, v4, v5

    const/16 v5, 0x8

    const/high16 v6, 0x3f800000    # 1.0f

    aput v6, v4, v5

    aput-object v4, v2, v3

    const/4 v3, 0x3

    .line 143
    const/16 v4, 0x9

    new-array v4, v4, [F

    const/4 v5, 0x0

    const v6, 0x3f866666    # 1.05f

    aput v6, v4, v5

    const/4 v5, 0x1

    const/4 v6, 0x0

    aput v6, v4, v5

    const/4 v5, 0x2

    neg-int v6, p1

    int-to-float v6, v6

    const v7, 0x3d4ccce8    # 0.0500001f

    mul-float/2addr v6, v7

    aput v6, v4, v5

    const/4 v5, 0x3

    const/4 v6, 0x0

    aput v6, v4, v5

    const/4 v5, 0x4

    const v6, 0x3f866666    # 1.05f

    aput v6, v4, v5

    const/4 v5, 0x5

    neg-int v6, p2

    int-to-float v6, v6

    const v7, 0x3d4ccce8    # 0.0500001f

    mul-float/2addr v6, v7

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    aput v6, v4, v5

    const/4 v5, 0x6

    const/4 v6, 0x0

    aput v6, v4, v5

    const/4 v5, 0x7

    const/4 v6, 0x0

    aput v6, v4, v5

    const/16 v5, 0x8

    const/high16 v6, 0x3f800000    # 1.0f

    aput v6, v4, v5

    aput-object v4, v2, v3

    const/4 v3, 0x4

    .line 145
    const/16 v4, 0x9

    new-array v4, v4, [F

    const/4 v5, 0x0

    const v6, 0x3f866666    # 1.05f

    aput v6, v4, v5

    const/4 v5, 0x1

    const/4 v6, 0x0

    aput v6, v4, v5

    const/4 v5, 0x2

    neg-int v6, p1

    int-to-float v6, v6

    const v7, 0x3d4ccce8    # 0.0500001f

    mul-float/2addr v6, v7

    aput v6, v4, v5

    const/4 v5, 0x3

    const/4 v6, 0x0

    aput v6, v4, v5

    const/4 v5, 0x4

    const v6, 0x3f866666    # 1.05f

    aput v6, v4, v5

    const/4 v5, 0x5

    neg-int v6, p2

    int-to-float v6, v6

    const v7, 0x3d4ccce8    # 0.0500001f

    mul-float/2addr v6, v7

    aput v6, v4, v5

    const/4 v5, 0x6

    const/4 v6, 0x0

    aput v6, v4, v5

    const/4 v5, 0x7

    const/4 v6, 0x0

    aput v6, v4, v5

    const/16 v5, 0x8

    const/high16 v6, 0x3f800000    # 1.0f

    aput v6, v4, v5

    aput-object v4, v2, v3

    const/4 v3, 0x5

    .line 147
    const/16 v4, 0x9

    new-array v4, v4, [F

    fill-array-data v4, :array_4

    aput-object v4, v2, v3

    const/4 v3, 0x6

    .line 149
    const/16 v4, 0x9

    new-array v4, v4, [F

    const/4 v5, 0x0

    const v6, 0x3f866666    # 1.05f

    aput v6, v4, v5

    const/4 v5, 0x1

    const/4 v6, 0x0

    aput v6, v4, v5

    const/4 v5, 0x2

    const/4 v6, 0x0

    aput v6, v4, v5

    const/4 v5, 0x3

    const/4 v6, 0x0

    aput v6, v4, v5

    const/4 v5, 0x4

    const v6, 0x3f866666    # 1.05f

    aput v6, v4, v5

    const/4 v5, 0x5

    neg-int v6, p2

    int-to-float v6, v6

    const v7, 0x3d4ccce8    # 0.0500001f

    mul-float/2addr v6, v7

    aput v6, v4, v5

    const/4 v5, 0x6

    const/4 v6, 0x0

    aput v6, v4, v5

    const/4 v5, 0x7

    const/4 v6, 0x0

    aput v6, v4, v5

    const/16 v5, 0x8

    const/high16 v6, 0x3f800000    # 1.0f

    aput v6, v4, v5

    aput-object v4, v2, v3

    const/4 v3, 0x7

    .line 151
    const/16 v4, 0x9

    new-array v4, v4, [F

    fill-array-data v4, :array_5

    aput-object v4, v2, v3

    const/16 v3, 0x8

    .line 153
    const/16 v4, 0x9

    new-array v4, v4, [F

    const/4 v5, 0x0

    const v6, 0x3f866666    # 1.05f

    aput v6, v4, v5

    const/4 v5, 0x1

    const/4 v6, 0x0

    aput v6, v4, v5

    const/4 v5, 0x2

    neg-int v6, p1

    int-to-float v6, v6

    const v7, 0x3d4ccce8    # 0.0500001f

    mul-float/2addr v6, v7

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    aput v6, v4, v5

    const/4 v5, 0x3

    const/4 v6, 0x0

    aput v6, v4, v5

    const/4 v5, 0x4

    const v6, 0x3f866666    # 1.05f

    aput v6, v4, v5

    const/4 v5, 0x5

    const/4 v6, 0x0

    aput v6, v4, v5

    const/4 v5, 0x6

    const/4 v6, 0x0

    aput v6, v4, v5

    const/4 v5, 0x7

    const/4 v6, 0x0

    aput v6, v4, v5

    const/16 v5, 0x8

    const/high16 v6, 0x3f800000    # 1.0f

    aput v6, v4, v5

    aput-object v4, v2, v3

    const/16 v3, 0x9

    .line 155
    const/16 v4, 0x9

    new-array v4, v4, [F

    const/4 v5, 0x0

    const v6, 0x3f866666    # 1.05f

    aput v6, v4, v5

    const/4 v5, 0x1

    const/4 v6, 0x0

    aput v6, v4, v5

    const/4 v5, 0x2

    neg-int v6, p1

    int-to-float v6, v6

    const v7, 0x3d4ccce8    # 0.0500001f

    mul-float/2addr v6, v7

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    aput v6, v4, v5

    const/4 v5, 0x3

    const/4 v6, 0x0

    aput v6, v4, v5

    const/4 v5, 0x4

    const v6, 0x3f866666    # 1.05f

    aput v6, v4, v5

    const/4 v5, 0x5

    neg-int v6, p2

    int-to-float v6, v6

    const v7, 0x3d4ccce8    # 0.0500001f

    mul-float/2addr v6, v7

    aput v6, v4, v5

    const/4 v5, 0x6

    const/4 v6, 0x0

    aput v6, v4, v5

    const/4 v5, 0x7

    const/4 v6, 0x0

    aput v6, v4, v5

    const/16 v5, 0x8

    .line 156
    const/high16 v6, 0x3f800000    # 1.0f

    aput v6, v4, v5

    aput-object v4, v2, v3

    .line 132
    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    sput-object v1, Lcom/sec/android/app/ve/common/KenburnsDefault;->endMatrix:Ljava/util/ArrayList;

    .line 166
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    new-array v2, v2, [Landroid/graphics/RectF;

    const/4 v3, 0x0

    .line 168
    new-instance v4, Landroid/graphics/RectF;

    const/4 v5, 0x0

    const/4 v6, 0x0

    int-to-float v7, p1

    int-to-float v8, p2

    invoke-direct {v4, v5, v6, v7, v8}, Landroid/graphics/RectF;-><init>(FFFF)V

    aput-object v4, v2, v3

    const/4 v3, 0x1

    .line 170
    new-instance v4, Landroid/graphics/RectF;

    neg-int v5, p1

    int-to-float v5, v5

    const v6, 0x3d4ccce8    # 0.0500001f

    mul-float/2addr v5, v6

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    neg-int v6, p2

    int-to-float v6, v6

    const v7, 0x3d4ccce8    # 0.0500001f

    mul-float/2addr v6, v7

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    int-to-float v7, p1

    int-to-float v8, p1

    const v9, 0x3d4ccce8    # 0.0500001f

    mul-float/2addr v8, v9

    const/high16 v9, 0x40000000    # 2.0f

    div-float/2addr v8, v9

    add-float/2addr v7, v8

    int-to-float v8, p2

    int-to-float v9, p2

    const v10, 0x3d4ccce8    # 0.0500001f

    mul-float/2addr v9, v10

    const/high16 v10, 0x40000000    # 2.0f

    div-float/2addr v9, v10

    add-float/2addr v8, v9

    invoke-direct {v4, v5, v6, v7, v8}, Landroid/graphics/RectF;-><init>(FFFF)V

    aput-object v4, v2, v3

    const/4 v3, 0x2

    .line 172
    new-instance v4, Landroid/graphics/RectF;

    neg-int v5, p1

    int-to-float v5, v5

    const v6, 0x3d4ccce8    # 0.0500001f

    mul-float/2addr v5, v6

    neg-int v6, p2

    int-to-float v6, v6

    const v7, 0x3d4ccce8    # 0.0500001f

    mul-float/2addr v6, v7

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    int-to-float v7, p1

    int-to-float v8, p2

    int-to-float v9, p2

    const v10, 0x3d4ccce8    # 0.0500001f

    mul-float/2addr v9, v10

    const/high16 v10, 0x40000000    # 2.0f

    div-float/2addr v9, v10

    add-float/2addr v8, v9

    invoke-direct {v4, v5, v6, v7, v8}, Landroid/graphics/RectF;-><init>(FFFF)V

    aput-object v4, v2, v3

    const/4 v3, 0x3

    .line 174
    new-instance v4, Landroid/graphics/RectF;

    const/4 v5, 0x0

    neg-int v6, p2

    int-to-float v6, v6

    const v7, 0x3d4ccce8    # 0.0500001f

    mul-float/2addr v6, v7

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    int-to-float v7, p1

    const v8, 0x3d4ccce8    # 0.0500001f

    int-to-float v9, p1

    mul-float/2addr v8, v9

    add-float/2addr v7, v8

    int-to-float v8, p2

    int-to-float v9, p2

    const v10, 0x3d4ccce8    # 0.0500001f

    mul-float/2addr v9, v10

    const/high16 v10, 0x40000000    # 2.0f

    div-float/2addr v9, v10

    add-float/2addr v8, v9

    invoke-direct {v4, v5, v6, v7, v8}, Landroid/graphics/RectF;-><init>(FFFF)V

    aput-object v4, v2, v3

    const/4 v3, 0x4

    .line 176
    new-instance v4, Landroid/graphics/RectF;

    const/4 v5, 0x0

    const/4 v6, 0x0

    int-to-float v7, p1

    int-to-float v8, p2

    invoke-direct {v4, v5, v6, v7, v8}, Landroid/graphics/RectF;-><init>(FFFF)V

    aput-object v4, v2, v3

    const/4 v3, 0x5

    .line 178
    new-instance v4, Landroid/graphics/RectF;

    neg-int v5, p1

    int-to-float v5, v5

    const v6, 0x3d4ccce8    # 0.0500001f

    mul-float/2addr v5, v6

    neg-int v6, p2

    int-to-float v6, v6

    const v7, 0x3d4ccce8    # 0.0500001f

    mul-float/2addr v6, v7

    int-to-float v7, p1

    int-to-float v8, p2

    invoke-direct {v4, v5, v6, v7, v8}, Landroid/graphics/RectF;-><init>(FFFF)V

    aput-object v4, v2, v3

    const/4 v3, 0x6

    .line 180
    new-instance v4, Landroid/graphics/RectF;

    const/4 v5, 0x0

    const/4 v6, 0x0

    int-to-float v7, p1

    int-to-float v8, p2

    invoke-direct {v4, v5, v6, v7, v8}, Landroid/graphics/RectF;-><init>(FFFF)V

    aput-object v4, v2, v3

    const/4 v3, 0x7

    .line 182
    new-instance v4, Landroid/graphics/RectF;

    const/4 v5, 0x0

    neg-int v6, p2

    int-to-float v6, v6

    const v7, 0x3d4ccce8    # 0.0500001f

    mul-float/2addr v6, v7

    int-to-float v7, p1

    int-to-float v8, p1

    const v9, 0x3d4ccce8    # 0.0500001f

    mul-float/2addr v8, v9

    add-float/2addr v7, v8

    int-to-float v8, p2

    invoke-direct {v4, v5, v6, v7, v8}, Landroid/graphics/RectF;-><init>(FFFF)V

    aput-object v4, v2, v3

    const/16 v3, 0x8

    .line 184
    new-instance v4, Landroid/graphics/RectF;

    neg-int v5, p1

    int-to-float v5, v5

    const v6, 0x3d4ccce8    # 0.0500001f

    mul-float/2addr v5, v6

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    neg-int v6, p2

    int-to-float v6, v6

    const v7, 0x3d4ccce8    # 0.0500001f

    mul-float/2addr v6, v7

    int-to-float v7, p1

    int-to-float v8, p1

    const v9, 0x3d4ccce8    # 0.0500001f

    mul-float/2addr v8, v9

    const/high16 v9, 0x40000000    # 2.0f

    div-float/2addr v8, v9

    add-float/2addr v7, v8

    int-to-float v8, p2

    invoke-direct {v4, v5, v6, v7, v8}, Landroid/graphics/RectF;-><init>(FFFF)V

    aput-object v4, v2, v3

    const/16 v3, 0x9

    .line 186
    new-instance v4, Landroid/graphics/RectF;

    neg-int v5, p1

    int-to-float v5, v5

    const v6, 0x3d4ccce8    # 0.0500001f

    mul-float/2addr v5, v6

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    const/4 v6, 0x0

    int-to-float v7, p1

    int-to-float v8, p1

    const v9, 0x3d4ccce8    # 0.0500001f

    mul-float/2addr v8, v9

    const/high16 v9, 0x40000000    # 2.0f

    div-float/2addr v8, v9

    add-float/2addr v7, v8

    int-to-float v8, p2

    int-to-float v9, p2

    const v10, 0x3d4ccce8    # 0.0500001f

    mul-float/2addr v9, v10

    add-float/2addr v8, v9

    invoke-direct {v4, v5, v6, v7, v8}, Landroid/graphics/RectF;-><init>(FFFF)V

    aput-object v4, v2, v3

    .line 166
    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    sput-object v1, Lcom/sec/android/app/ve/common/KenburnsDefault;->startRect:Ljava/util/ArrayList;

    .line 189
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    new-array v2, v2, [Landroid/graphics/RectF;

    const/4 v3, 0x0

    .line 191
    new-instance v4, Landroid/graphics/RectF;

    neg-int v5, p1

    int-to-float v5, v5

    const v6, 0x3d4ccce8    # 0.0500001f

    mul-float/2addr v5, v6

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    neg-int v6, p2

    int-to-float v6, v6

    const v7, 0x3d4ccce8    # 0.0500001f

    mul-float/2addr v6, v7

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    int-to-float v7, p1

    int-to-float v8, p1

    const v9, 0x3d4ccce8    # 0.0500001f

    mul-float/2addr v8, v9

    const/high16 v9, 0x40000000    # 2.0f

    div-float/2addr v8, v9

    add-float/2addr v7, v8

    int-to-float v8, p2

    int-to-float v9, p2

    const v10, 0x3d4ccce8    # 0.0500001f

    mul-float/2addr v9, v10

    const/high16 v10, 0x40000000    # 2.0f

    div-float/2addr v9, v10

    add-float/2addr v8, v9

    invoke-direct {v4, v5, v6, v7, v8}, Landroid/graphics/RectF;-><init>(FFFF)V

    aput-object v4, v2, v3

    const/4 v3, 0x1

    .line 193
    new-instance v4, Landroid/graphics/RectF;

    const/4 v5, 0x0

    const/4 v6, 0x0

    int-to-float v7, p1

    int-to-float v8, p2

    invoke-direct {v4, v5, v6, v7, v8}, Landroid/graphics/RectF;-><init>(FFFF)V

    aput-object v4, v2, v3

    const/4 v3, 0x2

    .line 195
    new-instance v4, Landroid/graphics/RectF;

    const/4 v5, 0x0

    neg-int v6, p2

    int-to-float v6, v6

    const v7, 0x3d4ccce8    # 0.0500001f

    mul-float/2addr v6, v7

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    int-to-float v7, p1

    int-to-float v8, p1

    const v9, 0x3d4ccce8    # 0.0500001f

    mul-float/2addr v8, v9

    add-float/2addr v7, v8

    int-to-float v8, p2

    int-to-float v9, p2

    const v10, 0x3d4ccce8    # 0.0500001f

    mul-float/2addr v9, v10

    const/high16 v10, 0x40000000    # 2.0f

    div-float/2addr v9, v10

    add-float/2addr v8, v9

    invoke-direct {v4, v5, v6, v7, v8}, Landroid/graphics/RectF;-><init>(FFFF)V

    aput-object v4, v2, v3

    const/4 v3, 0x3

    .line 197
    new-instance v4, Landroid/graphics/RectF;

    neg-int v5, p1

    int-to-float v5, v5

    const v6, 0x3d4ccce8    # 0.0500001f

    mul-float/2addr v5, v6

    neg-int v6, p2

    int-to-float v6, v6

    const v7, 0x3d4ccce8    # 0.0500001f

    mul-float/2addr v6, v7

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    int-to-float v7, p1

    int-to-float v8, p2

    int-to-float v9, p2

    const v10, 0x3d4ccce8    # 0.0500001f

    mul-float/2addr v9, v10

    const/high16 v10, 0x40000000    # 2.0f

    div-float/2addr v9, v10

    add-float/2addr v8, v9

    invoke-direct {v4, v5, v6, v7, v8}, Landroid/graphics/RectF;-><init>(FFFF)V

    aput-object v4, v2, v3

    const/4 v3, 0x4

    .line 199
    new-instance v4, Landroid/graphics/RectF;

    neg-int v5, p1

    int-to-float v5, v5

    const v6, 0x3d4ccce8    # 0.0500001f

    mul-float/2addr v5, v6

    neg-int v6, p2

    int-to-float v6, v6

    const v7, 0x3d4ccce8    # 0.0500001f

    mul-float/2addr v6, v7

    int-to-float v7, p1

    int-to-float v8, p2

    invoke-direct {v4, v5, v6, v7, v8}, Landroid/graphics/RectF;-><init>(FFFF)V

    aput-object v4, v2, v3

    const/4 v3, 0x5

    .line 201
    new-instance v4, Landroid/graphics/RectF;

    const/4 v5, 0x0

    const/4 v6, 0x0

    int-to-float v7, p1

    int-to-float v8, p2

    invoke-direct {v4, v5, v6, v7, v8}, Landroid/graphics/RectF;-><init>(FFFF)V

    aput-object v4, v2, v3

    const/4 v3, 0x6

    .line 203
    new-instance v4, Landroid/graphics/RectF;

    const/4 v5, 0x0

    neg-int v6, p2

    int-to-float v6, v6

    const v7, 0x3d4ccce8    # 0.0500001f

    mul-float/2addr v6, v7

    int-to-float v7, p1

    int-to-float v8, p1

    const v9, 0x3d4ccce8    # 0.0500001f

    mul-float/2addr v8, v9

    add-float/2addr v7, v8

    int-to-float v8, p2

    invoke-direct {v4, v5, v6, v7, v8}, Landroid/graphics/RectF;-><init>(FFFF)V

    aput-object v4, v2, v3

    const/4 v3, 0x7

    .line 205
    new-instance v4, Landroid/graphics/RectF;

    const/4 v5, 0x0

    const/4 v6, 0x0

    int-to-float v7, p1

    int-to-float v8, p2

    invoke-direct {v4, v5, v6, v7, v8}, Landroid/graphics/RectF;-><init>(FFFF)V

    aput-object v4, v2, v3

    const/16 v3, 0x8

    .line 207
    new-instance v4, Landroid/graphics/RectF;

    neg-int v5, p1

    int-to-float v5, v5

    const v6, 0x3d4ccce8    # 0.0500001f

    mul-float/2addr v5, v6

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    const/4 v6, 0x0

    int-to-float v7, p1

    int-to-float v8, p1

    const v9, 0x3d4ccce8    # 0.0500001f

    mul-float/2addr v8, v9

    const/high16 v9, 0x40000000    # 2.0f

    div-float/2addr v8, v9

    add-float/2addr v7, v8

    int-to-float v8, p2

    int-to-float v9, p2

    const v10, 0x3d4ccce8    # 0.0500001f

    mul-float/2addr v9, v10

    add-float/2addr v8, v9

    invoke-direct {v4, v5, v6, v7, v8}, Landroid/graphics/RectF;-><init>(FFFF)V

    aput-object v4, v2, v3

    const/16 v3, 0x9

    .line 209
    new-instance v4, Landroid/graphics/RectF;

    neg-int v5, p1

    int-to-float v5, v5

    const v6, 0x3d4ccce8    # 0.0500001f

    mul-float/2addr v5, v6

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    neg-int v6, p2

    int-to-float v6, v6

    const v7, 0x3d4ccce8    # 0.0500001f

    mul-float/2addr v6, v7

    int-to-float v7, p1

    int-to-float v8, p1

    const v9, 0x3d4ccce8    # 0.0500001f

    mul-float/2addr v8, v9

    const/high16 v9, 0x40000000    # 2.0f

    div-float/2addr v8, v9

    add-float/2addr v7, v8

    int-to-float v8, p2

    invoke-direct {v4, v5, v6, v7, v8}, Landroid/graphics/RectF;-><init>(FFFF)V

    aput-object v4, v2, v3

    .line 189
    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    sput-object v1, Lcom/sec/android/app/ve/common/KenburnsDefault;->endRect:Ljava/util/ArrayList;

    .line 211
    return-void

    .line 103
    :array_0
    .array-data 4
        0x3f800008    # 1.000001f
        0x0
        0x0
        0x0
        0x3f800008    # 1.000001f
        0x0
        0x0
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 114
    :array_1
    .array-data 4
        0x3f800008    # 1.000001f
        0x0
        0x0
        0x0
        0x3f800008    # 1.000001f
        0x0
        0x0
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 119
    :array_2
    .array-data 4
        0x3f800008    # 1.000001f
        0x0
        0x0
        0x0
        0x3f800008    # 1.000001f
        0x0
        0x0
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 138
    :array_3
    .array-data 4
        0x3f800008    # 1.000001f
        0x0
        0x0
        0x0
        0x3f800008    # 1.000001f
        0x0
        0x0
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 147
    :array_4
    .array-data 4
        0x3f800008    # 1.000001f
        0x0
        0x0
        0x0
        0x3f800008    # 1.000001f
        0x0
        0x0
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 151
    :array_5
    .array-data 4
        0x3f800008    # 1.000001f
        0x0
        0x0
        0x0
        0x3f800008    # 1.000001f
        0x0
        0x0
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method


# virtual methods
.method public applyDefaultKenburns(Lcom/samsung/app/video/editor/external/Element;)Lcom/samsung/app/video/editor/external/Element;
    .locals 2
    .param p1, "element"    # Lcom/samsung/app/video/editor/external/Element;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/app/ve/common/KenburnsDefault;->mTaskPool:Lcom/sec/android/app/ve/thread/AsyncTaskPool;

    iget-object v1, p0, Lcom/sec/android/app/ve/common/KenburnsDefault;->mPoolLstnr:Lcom/sec/android/app/ve/thread/AsyncTaskPool$Listener;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/ve/thread/AsyncTaskPool;->setListener(Lcom/sec/android/app/ve/thread/AsyncTaskPool$Listener;)V

    .line 75
    iget-object v0, p0, Lcom/sec/android/app/ve/common/KenburnsDefault;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/ve/common/KenburnsDefault$2;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/ve/common/KenburnsDefault$2;-><init>(Lcom/sec/android/app/ve/common/KenburnsDefault;Lcom/samsung/app/video/editor/external/Element;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 83
    return-object p1
.end method
