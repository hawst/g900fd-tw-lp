.class Lcom/sec/android/app/storycam/view/PreviewViewGroup$3;
.super Ljava/lang/Object;
.source "PreviewViewGroup.java"

# interfaces
.implements Lcom/sec/android/app/storycam/view/PreviewViewGroup$PlayerControlInterface;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/storycam/view/PreviewViewGroup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$3;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    .line 512
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public isPlaying()Z
    .locals 1

    .prologue
    .line 562
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$3;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    # getter for: Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mPreview:Lcom/sec/android/app/ve/PreviewPlayerInterface;
    invoke-static {v0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$0(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)Lcom/sec/android/app/ve/PreviewPlayerInterface;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$3;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    # getter for: Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mPreview:Lcom/sec/android/app/ve/PreviewPlayerInterface;
    invoke-static {v0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$0(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)Lcom/sec/android/app/ve/PreviewPlayerInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/ve/PreviewPlayerInterface;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 563
    const/4 v0, 0x1

    .line 565
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public pause_player()V
    .locals 2

    .prologue
    .line 515
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$3;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    # getter for: Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mPreview:Lcom/sec/android/app/ve/PreviewPlayerInterface;
    invoke-static {v0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$0(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)Lcom/sec/android/app/ve/PreviewPlayerInterface;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$3;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    # getter for: Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mPreview:Lcom/sec/android/app/ve/PreviewPlayerInterface;
    invoke-static {v0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$0(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)Lcom/sec/android/app/ve/PreviewPlayerInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/ve/PreviewPlayerInterface;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 517
    const-string v0, "terminating instead of pausing"

    invoke-static {v0}, Lcom/sec/android/app/ve/common/LogUtils;->log(Ljava/lang/String;)V

    .line 519
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$3;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    # getter for: Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$5(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 520
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$3;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    # getter for: Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$5(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 521
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$3;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    # getter for: Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mPreview:Lcom/sec/android/app/ve/PreviewPlayerInterface;
    invoke-static {v0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$0(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)Lcom/sec/android/app/ve/PreviewPlayerInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/ve/PreviewPlayerInterface;->stop()V

    .line 523
    :cond_0
    return-void
.end method

.method public seekTo()I
    .locals 1

    .prologue
    .line 582
    const/4 v0, 0x0

    return v0
.end method

.method public start_play()I
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 527
    iget-object v3, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$3;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    # getter for: Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mCurrTransElement:Lcom/samsung/app/video/editor/external/TranscodeElement;
    invoke-static {v3}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$1(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v3

    if-nez v3, :cond_1

    .line 557
    :cond_0
    :goto_0
    return v1

    .line 531
    :cond_1
    const-string v3, "PVG:start Play"

    invoke-static {v3}, Lcom/sec/android/app/ve/common/LogUtils;->log(Ljava/lang/String;)V

    .line 534
    iget-object v3, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$3;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    # getter for: Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$10(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)Landroid/content/Context;

    move-result-object v3

    const-string v4, "phone"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 535
    .local v0, "telephonyManager":Landroid/telephony/TelephonyManager;
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v3

    if-eqz v3, :cond_2

    .line 536
    iget-object v3, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$3;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    invoke-virtual {v3}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0700f9

    invoke-static {v3, v4, v2}, Lcom/sec/android/app/ve/VEApp;->showToast(Landroid/content/Context;II)Landroid/widget/Toast;

    .line 537
    iget-object v3, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$3;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    invoke-virtual {v3, v2}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->setPlayButtonVisibility(I)V

    goto :goto_0

    .line 542
    :cond_2
    invoke-static {}, Lcom/sec/android/app/ve/util/CommonUtils;->checkStorage()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 543
    iget-object v3, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$3;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    invoke-virtual {v3}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0700da

    invoke-static {v3, v4, v2}, Lcom/sec/android/app/ve/VEApp;->showToast(Landroid/content/Context;II)Landroid/widget/Toast;

    goto :goto_0

    .line 547
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$3;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    # getter for: Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mCurrTransElement:Lcom/samsung/app/video/editor/external/TranscodeElement;
    invoke-static {v3}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$1(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v3

    if-eqz v3, :cond_0

    .line 550
    sget-object v1, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    iget-object v3, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$3;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    # getter for: Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mCurrTransElement:Lcom/samsung/app/video/editor/external/TranscodeElement;
    invoke-static {v3}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$1(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v1, v3, v4}, Lcom/sec/android/app/storycam/AppDataManager;->recheckFilesInTranscodeElement(Lcom/samsung/app/video/editor/external/TranscodeElement;Ljava/lang/String;)Z

    .line 551
    iget-object v1, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$3;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    # getter for: Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mCurrTransElement:Lcom/samsung/app/video/editor/external/TranscodeElement;
    invoke-static {v1}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$1(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v1

    sget v3, Lcom/sec/android/app/ve/common/ConfigUtils;->PREVIEW_WIDTH:I

    invoke-virtual {v1, v3}, Lcom/samsung/app/video/editor/external/TranscodeElement;->setTargetDispWidth(I)V

    .line 552
    iget-object v1, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$3;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    # getter for: Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mCurrTransElement:Lcom/samsung/app/video/editor/external/TranscodeElement;
    invoke-static {v1}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$1(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v1

    sget v3, Lcom/sec/android/app/ve/common/ConfigUtils;->PREVIEW_HEIGHT:I

    invoke-virtual {v1, v3}, Lcom/samsung/app/video/editor/external/TranscodeElement;->setTargetDispHeight(I)V

    .line 554
    iget-object v1, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$3;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    # getter for: Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mCurrTransElement:Lcom/samsung/app/video/editor/external/TranscodeElement;
    invoke-static {v1}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$1(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$3;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    # getter for: Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mCurrTransElement:Lcom/samsung/app/video/editor/external/TranscodeElement;
    invoke-static {v3}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$1(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getTotalDuration()J

    move-result-wide v4

    long-to-int v3, v4

    invoke-virtual {v1, v3}, Lcom/samsung/app/video/editor/external/TranscodeElement;->setFullMovieDuration(I)V

    .line 556
    iget-object v1, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$3;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    # getter for: Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mPreview:Lcom/sec/android/app/ve/PreviewPlayerInterface;
    invoke-static {v1}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$0(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)Lcom/sec/android/app/ve/PreviewPlayerInterface;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$3;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    # invokes: Lcom/sec/android/app/storycam/view/PreviewViewGroup;->getSeekTimeForPlayback()J
    invoke-static {v3}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$11(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Lcom/sec/android/app/ve/PreviewPlayerInterface;->play(J)V

    move v1, v2

    .line 557
    goto/16 :goto_0
.end method

.method public stop_play()V
    .locals 2

    .prologue
    .line 570
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$3;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    # getter for: Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mPreview:Lcom/sec/android/app/ve/PreviewPlayerInterface;
    invoke-static {v0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$0(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)Lcom/sec/android/app/ve/PreviewPlayerInterface;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$3;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    # getter for: Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mPreview:Lcom/sec/android/app/ve/PreviewPlayerInterface;
    invoke-static {v0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$0(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)Lcom/sec/android/app/ve/PreviewPlayerInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/ve/PreviewPlayerInterface;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 571
    const-string v0, "previewStopFromOutside"

    invoke-static {v0}, Lcom/sec/android/app/ve/common/LogUtils;->log(Ljava/lang/String;)V

    .line 573
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$3;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    # getter for: Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$5(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 574
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$3;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    # getter for: Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$5(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 575
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$3;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    # getter for: Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mPreview:Lcom/sec/android/app/ve/PreviewPlayerInterface;
    invoke-static {v0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$0(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)Lcom/sec/android/app/ve/PreviewPlayerInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/ve/PreviewPlayerInterface;->stop()V

    .line 576
    const-string v0, "VideoEditor_AutomationTest"

    const-string v1, "Player Paused"

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 578
    :cond_0
    return-void
.end method
