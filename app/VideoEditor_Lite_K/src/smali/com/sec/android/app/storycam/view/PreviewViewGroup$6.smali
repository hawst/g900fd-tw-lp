.class Lcom/sec/android/app/storycam/view/PreviewViewGroup$6;
.super Ljava/lang/Object;
.source "PreviewViewGroup.java"

# interfaces
.implements Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityStateChangedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/storycam/view/PreviewViewGroup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$6;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    .line 677
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onActivityStateChaged(Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;)V
    .locals 3
    .param p1, "previousState"    # Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;
    .param p2, "newState"    # Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    .prologue
    .line 681
    sget-object v1, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;->STATE_DEFAULT:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    invoke-virtual {v1, p2}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 682
    iget-object v1, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$6;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    # getter for: Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mProgressBar:Landroid/widget/SeekBar;
    invoke-static {v1}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$2(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)Landroid/widget/SeekBar;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 683
    iget-object v1, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$6;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    # getter for: Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mProgressBar:Landroid/widget/SeekBar;
    invoke-static {v1}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$2(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)Landroid/widget/SeekBar;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setEnabled(Z)V

    .line 684
    iget-object v1, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$6;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    # getter for: Lcom/sec/android/app/storycam/view/PreviewViewGroup;->mProgressBar:Landroid/widget/SeekBar;
    invoke-static {v1}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$2(Lcom/sec/android/app/storycam/view/PreviewViewGroup;)Landroid/widget/SeekBar;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    .line 685
    .local v0, "progress":I
    invoke-static {}, Lcom/sec/android/app/storycam/summary/Summary;->isSummaryRunning()Z

    move-result v1

    if-nez v1, :cond_0

    .line 687
    iget-object v1, p0, Lcom/sec/android/app/storycam/view/PreviewViewGroup$6;->this$0:Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    # invokes: Lcom/sec/android/app/storycam/view/PreviewViewGroup;->refreshPreviewForCurrentSeekBarPosition(I)V
    invoke-static {v1, v0}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->access$6(Lcom/sec/android/app/storycam/view/PreviewViewGroup;I)V

    .line 691
    .end local v0    # "progress":I
    :cond_0
    return-void
.end method
