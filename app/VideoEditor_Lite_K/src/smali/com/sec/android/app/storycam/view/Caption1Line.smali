.class public Lcom/sec/android/app/storycam/view/Caption1Line;
.super Landroid/widget/EditText;
.source "Caption1Line.java"

# interfaces
.implements Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ActivityEventsCallback;


# static fields
.field public static final INVALID_CHAR:[Ljava/lang/String;


# instance fields
.field private CANVAS_MAX_WIDTH:I

.field private canvasPaint:Landroid/graphics/Paint;

.field private charLenToast:Landroid/widget/Toast;

.field private emoticonFilter:Landroid/text/InputFilter;

.field private focusChangeListener:Landroid/view/View$OnFocusChangeListener;

.field private invalidCharToast:Landroid/widget/Toast;

.field private invalidInputFilter:Landroid/text/InputFilter;

.field private keyboardActionListener:Landroid/widget/TextView$OnEditorActionListener;

.field private lengthBasedFilter:Landroid/text/InputFilter;

.field private mTranscodeElementListener:Lcom/sec/android/app/storycam/AppDataManager$TranscodeElementChangeListener;

.field private mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

.field private final maxCharacter:I

.field private pixelBasedWidthFilter:Landroid/text/InputFilter;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 83
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    .line 85
    const-string v2, "\\"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "/"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, ":"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "*"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "?"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "\""

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "<"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, ">"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "|"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "\n"

    aput-object v2, v0, v1

    .line 83
    sput-object v0, Lcom/sec/android/app/storycam/view/Caption1Line;->INVALID_CHAR:[Ljava/lang/String;

    .line 87
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/16 v1, 0x50

    .line 443
    invoke-direct {p0, p1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 56
    const/16 v0, 0x44c

    iput v0, p0, Lcom/sec/android/app/storycam/view/Caption1Line;->CANVAS_MAX_WIDTH:I

    .line 81
    iput v1, p0, Lcom/sec/android/app/storycam/view/Caption1Line;->maxCharacter:I

    .line 95
    new-instance v0, Lcom/sec/android/app/storycam/view/Caption1Line$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/Caption1Line$1;-><init>(Lcom/sec/android/app/storycam/view/Caption1Line;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/Caption1Line;->pixelBasedWidthFilter:Landroid/text/InputFilter;

    .line 172
    new-instance v0, Lcom/sec/android/app/storycam/view/Caption1Line$2;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/storycam/view/Caption1Line$2;-><init>(Lcom/sec/android/app/storycam/view/Caption1Line;I)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/Caption1Line;->lengthBasedFilter:Landroid/text/InputFilter;

    .line 187
    new-instance v0, Lcom/sec/android/app/storycam/view/Caption1Line$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/Caption1Line$3;-><init>(Lcom/sec/android/app/storycam/view/Caption1Line;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/Caption1Line;->invalidInputFilter:Landroid/text/InputFilter;

    .line 251
    new-instance v0, Lcom/sec/android/app/storycam/view/Caption1Line$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/Caption1Line$4;-><init>(Lcom/sec/android/app/storycam/view/Caption1Line;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/Caption1Line;->emoticonFilter:Landroid/text/InputFilter;

    .line 390
    new-instance v0, Lcom/sec/android/app/storycam/view/Caption1Line$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/Caption1Line$5;-><init>(Lcom/sec/android/app/storycam/view/Caption1Line;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/Caption1Line;->keyboardActionListener:Landroid/widget/TextView$OnEditorActionListener;

    .line 408
    new-instance v0, Lcom/sec/android/app/storycam/view/Caption1Line$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/Caption1Line$6;-><init>(Lcom/sec/android/app/storycam/view/Caption1Line;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/Caption1Line;->focusChangeListener:Landroid/view/View$OnFocusChangeListener;

    .line 539
    new-instance v0, Lcom/sec/android/app/storycam/view/Caption1Line$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/Caption1Line$7;-><init>(Lcom/sec/android/app/storycam/view/Caption1Line;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/Caption1Line;->mTranscodeElementListener:Lcom/sec/android/app/storycam/AppDataManager$TranscodeElementChangeListener;

    .line 444
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/view/Caption1Line;->init()V

    .line 445
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/16 v1, 0x50

    .line 448
    invoke-direct {p0, p1, p2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 56
    const/16 v0, 0x44c

    iput v0, p0, Lcom/sec/android/app/storycam/view/Caption1Line;->CANVAS_MAX_WIDTH:I

    .line 81
    iput v1, p0, Lcom/sec/android/app/storycam/view/Caption1Line;->maxCharacter:I

    .line 95
    new-instance v0, Lcom/sec/android/app/storycam/view/Caption1Line$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/Caption1Line$1;-><init>(Lcom/sec/android/app/storycam/view/Caption1Line;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/Caption1Line;->pixelBasedWidthFilter:Landroid/text/InputFilter;

    .line 172
    new-instance v0, Lcom/sec/android/app/storycam/view/Caption1Line$2;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/storycam/view/Caption1Line$2;-><init>(Lcom/sec/android/app/storycam/view/Caption1Line;I)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/Caption1Line;->lengthBasedFilter:Landroid/text/InputFilter;

    .line 187
    new-instance v0, Lcom/sec/android/app/storycam/view/Caption1Line$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/Caption1Line$3;-><init>(Lcom/sec/android/app/storycam/view/Caption1Line;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/Caption1Line;->invalidInputFilter:Landroid/text/InputFilter;

    .line 251
    new-instance v0, Lcom/sec/android/app/storycam/view/Caption1Line$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/Caption1Line$4;-><init>(Lcom/sec/android/app/storycam/view/Caption1Line;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/Caption1Line;->emoticonFilter:Landroid/text/InputFilter;

    .line 390
    new-instance v0, Lcom/sec/android/app/storycam/view/Caption1Line$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/Caption1Line$5;-><init>(Lcom/sec/android/app/storycam/view/Caption1Line;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/Caption1Line;->keyboardActionListener:Landroid/widget/TextView$OnEditorActionListener;

    .line 408
    new-instance v0, Lcom/sec/android/app/storycam/view/Caption1Line$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/Caption1Line$6;-><init>(Lcom/sec/android/app/storycam/view/Caption1Line;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/Caption1Line;->focusChangeListener:Landroid/view/View$OnFocusChangeListener;

    .line 539
    new-instance v0, Lcom/sec/android/app/storycam/view/Caption1Line$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/Caption1Line$7;-><init>(Lcom/sec/android/app/storycam/view/Caption1Line;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/Caption1Line;->mTranscodeElementListener:Lcom/sec/android/app/storycam/AppDataManager$TranscodeElementChangeListener;

    .line 449
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/view/Caption1Line;->init()V

    .line 450
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/16 v1, 0x50

    .line 453
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 56
    const/16 v0, 0x44c

    iput v0, p0, Lcom/sec/android/app/storycam/view/Caption1Line;->CANVAS_MAX_WIDTH:I

    .line 81
    iput v1, p0, Lcom/sec/android/app/storycam/view/Caption1Line;->maxCharacter:I

    .line 95
    new-instance v0, Lcom/sec/android/app/storycam/view/Caption1Line$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/Caption1Line$1;-><init>(Lcom/sec/android/app/storycam/view/Caption1Line;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/Caption1Line;->pixelBasedWidthFilter:Landroid/text/InputFilter;

    .line 172
    new-instance v0, Lcom/sec/android/app/storycam/view/Caption1Line$2;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/storycam/view/Caption1Line$2;-><init>(Lcom/sec/android/app/storycam/view/Caption1Line;I)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/Caption1Line;->lengthBasedFilter:Landroid/text/InputFilter;

    .line 187
    new-instance v0, Lcom/sec/android/app/storycam/view/Caption1Line$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/Caption1Line$3;-><init>(Lcom/sec/android/app/storycam/view/Caption1Line;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/Caption1Line;->invalidInputFilter:Landroid/text/InputFilter;

    .line 251
    new-instance v0, Lcom/sec/android/app/storycam/view/Caption1Line$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/Caption1Line$4;-><init>(Lcom/sec/android/app/storycam/view/Caption1Line;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/Caption1Line;->emoticonFilter:Landroid/text/InputFilter;

    .line 390
    new-instance v0, Lcom/sec/android/app/storycam/view/Caption1Line$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/Caption1Line$5;-><init>(Lcom/sec/android/app/storycam/view/Caption1Line;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/Caption1Line;->keyboardActionListener:Landroid/widget/TextView$OnEditorActionListener;

    .line 408
    new-instance v0, Lcom/sec/android/app/storycam/view/Caption1Line$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/Caption1Line$6;-><init>(Lcom/sec/android/app/storycam/view/Caption1Line;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/Caption1Line;->focusChangeListener:Landroid/view/View$OnFocusChangeListener;

    .line 539
    new-instance v0, Lcom/sec/android/app/storycam/view/Caption1Line$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/Caption1Line$7;-><init>(Lcom/sec/android/app/storycam/view/Caption1Line;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/Caption1Line;->mTranscodeElementListener:Lcom/sec/android/app/storycam/AppDataManager$TranscodeElementChangeListener;

    .line 454
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/view/Caption1Line;->init()V

    .line 455
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/app/storycam/view/Caption1Line;)Landroid/graphics/Paint;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/Caption1Line;->canvasPaint:Landroid/graphics/Paint;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/app/storycam/view/Caption1Line;)I
    .locals 1

    .prologue
    .line 56
    iget v0, p0, Lcom/sec/android/app/storycam/view/Caption1Line;->CANVAS_MAX_WIDTH:I

    return v0
.end method

.method static synthetic access$2(Lcom/sec/android/app/storycam/view/Caption1Line;)Landroid/widget/Toast;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/Caption1Line;->charLenToast:Landroid/widget/Toast;

    return-object v0
.end method

.method static synthetic access$3(Lcom/sec/android/app/storycam/view/Caption1Line;)Landroid/widget/Toast;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/Caption1Line;->invalidCharToast:Landroid/widget/Toast;

    return-object v0
.end method

.method static synthetic access$4(Lcom/sec/android/app/storycam/view/Caption1Line;)Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/Caption1Line;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    return-object v0
.end method

.method static synthetic access$5(Lcom/sec/android/app/storycam/view/Caption1Line;I)V
    .locals 0

    .prologue
    .line 56
    iput p1, p0, Lcom/sec/android/app/storycam/view/Caption1Line;->CANVAS_MAX_WIDTH:I

    return-void
.end method

.method static synthetic access$6(Lcom/sec/android/app/storycam/view/Caption1Line;Landroid/graphics/Paint;)V
    .locals 0

    .prologue
    .line 62
    iput-object p1, p0, Lcom/sec/android/app/storycam/view/Caption1Line;->canvasPaint:Landroid/graphics/Paint;

    return-void
.end method


# virtual methods
.method public getCanvasPaint()Landroid/graphics/Paint;
    .locals 1

    .prologue
    .line 697
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/Caption1Line;->canvasPaint:Landroid/graphics/Paint;

    return-object v0
.end method

.method public getToast()Landroid/widget/Toast;
    .locals 1

    .prologue
    .line 694
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/Caption1Line;->charLenToast:Landroid/widget/Toast;

    return-object v0
.end method

.method public init()V
    .locals 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ShowToast"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 463
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/Caption1Line;->canvasPaint:Landroid/graphics/Paint;

    .line 464
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/Caption1Line;->canvasPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 466
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/Caption1Line;->canvasPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x429c0000    # 78.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 467
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/Caption1Line;->canvasPaint:Landroid/graphics/Paint;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 468
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/Caption1Line;->canvasPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 470
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/view/Caption1Line;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0700f3

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/Caption1Line;->charLenToast:Landroid/widget/Toast;

    .line 471
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/view/Caption1Line;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f070088

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/Caption1Line;->invalidCharToast:Landroid/widget/Toast;

    .line 479
    const/4 v0, 0x4

    new-array v0, v0, [Landroid/text/InputFilter;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/android/app/storycam/view/Caption1Line;->lengthBasedFilter:Landroid/text/InputFilter;

    aput-object v2, v0, v1

    iget-object v1, p0, Lcom/sec/android/app/storycam/view/Caption1Line;->emoticonFilter:Landroid/text/InputFilter;

    aput-object v1, v0, v3

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/sec/android/app/storycam/view/Caption1Line;->invalidInputFilter:Landroid/text/InputFilter;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/sec/android/app/storycam/view/Caption1Line;->pixelBasedWidthFilter:Landroid/text/InputFilter;

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/storycam/view/Caption1Line;->setFilters([Landroid/text/InputFilter;)V

    .line 480
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/Caption1Line;->keyboardActionListener:Landroid/widget/TextView$OnEditorActionListener;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/storycam/view/Caption1Line;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 481
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/Caption1Line;->focusChangeListener:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/storycam/view/Caption1Line;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 482
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/view/Caption1Line;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    iput-object v0, p0, Lcom/sec/android/app/storycam/view/Caption1Line;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    .line 484
    return-void
.end method

.method public onActivityActionBarTapped()V
    .locals 0

    .prologue
    .line 725
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/view/Caption1Line;->saveCaption()V

    .line 726
    return-void
.end method

.method public onActivityPaused()V
    .locals 0

    .prologue
    .line 716
    return-void
.end method

.method public onActivityResumed()V
    .locals 0

    .prologue
    .line 721
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 493
    invoke-super {p0}, Landroid/widget/EditText;->onAttachedToWindow()V

    .line 494
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/Caption1Line;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    if-eqz v0, :cond_0

    .line 495
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/Caption1Line;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    invoke-interface {v0, p0}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;->setActivityEventsCallback(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ActivityEventsCallback;)V

    .line 496
    :cond_0
    return-void
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 488
    invoke-super {p0, p1}, Landroid/widget/EditText;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 489
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 500
    invoke-super {p0}, Landroid/widget/EditText;->onDetachedFromWindow()V

    .line 501
    sget-object v0, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    if-eqz v0, :cond_0

    .line 502
    sget-object v0, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    iget-object v1, p0, Lcom/sec/android/app/storycam/view/Caption1Line;->mTranscodeElementListener:Lcom/sec/android/app/storycam/AppDataManager$TranscodeElementChangeListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/storycam/AppDataManager;->removeTranscodeElementChangeListener(Lcom/sec/android/app/storycam/AppDataManager$TranscodeElementChangeListener;)V

    .line 503
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/Caption1Line;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    if-eqz v0, :cond_1

    .line 504
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/Caption1Line;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    invoke-interface {v0, p0}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;->removeActivityEventsCallback(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ActivityEventsCallback;)V

    .line 505
    :cond_1
    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 524
    invoke-super {p0}, Landroid/widget/EditText;->onFinishInflate()V

    .line 525
    sget-object v0, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    iget-object v1, p0, Lcom/sec/android/app/storycam/view/Caption1Line;->mTranscodeElementListener:Lcom/sec/android/app/storycam/AppDataManager$TranscodeElementChangeListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/storycam/AppDataManager;->addTranscodeElementChangeListener(Lcom/sec/android/app/storycam/AppDataManager$TranscodeElementChangeListener;)V

    .line 527
    new-instance v0, Lcom/sec/android/app/storycam/view/Caption1Line$8;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/view/Caption1Line$8;-><init>(Lcom/sec/android/app/storycam/view/Caption1Line;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/storycam/view/Caption1Line;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 537
    return-void
.end method

.method public onKeyPreIme(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 509
    invoke-super {p0, p1, p2}, Landroid/widget/EditText;->onKeyPreIme(ILandroid/view/KeyEvent;)Z

    move-result v0

    .line 511
    .local v0, "handled":Z
    if-nez v0, :cond_0

    const/4 v1, 0x4

    if-ne p1, v1, :cond_0

    .line 512
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/view/Caption1Line;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/storycam/view/Caption1Line;->updateTitle(Ljava/lang/String;)V

    .line 513
    iget-object v1, p0, Lcom/sec/android/app/storycam/view/Caption1Line;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;->requestFocusForMainActivity(Z)V

    .line 514
    invoke-static {p0}, Lcom/sec/android/app/ve/common/AndroidUtils;->hideSoftKeyboard(Landroid/view/View;)V

    .line 517
    :cond_0
    return v0
.end method

.method public saveCaption()V
    .locals 3

    .prologue
    .line 705
    invoke-static {}, Landroid/view/inputmethod/InputMethodManager;->peekInstance()Landroid/view/inputmethod/InputMethodManager;

    move-result-object v0

    .line 706
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    if-eqz v0, :cond_0

    invoke-virtual {v0, p0}, Landroid/view/inputmethod/InputMethodManager;->isActive(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 707
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/view/Caption1Line;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 710
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/view/Caption1Line;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/storycam/view/Caption1Line;->updateTitle(Ljava/lang/String;)V

    .line 711
    return-void
.end method

.method public setCanvasPaint(Landroid/graphics/Paint;)V
    .locals 0
    .param p1, "paint"    # Landroid/graphics/Paint;

    .prologue
    .line 700
    iput-object p1, p0, Lcom/sec/android/app/storycam/view/Caption1Line;->canvasPaint:Landroid/graphics/Paint;

    .line 701
    return-void
.end method

.method public showToastMessage(Landroid/widget/Toast;)V
    .locals 1
    .param p1, "toast"    # Landroid/widget/Toast;

    .prologue
    .line 685
    invoke-virtual {p1}, Landroid/widget/Toast;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getWindowVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    .line 686
    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    .line 688
    :cond_0
    return-void
.end method

.method public updateTitle(Ljava/lang/String;)V
    .locals 17
    .param p1, "newTitle"    # Ljava/lang/String;

    .prologue
    .line 610
    if-nez p1, :cond_1

    .line 678
    :cond_0
    :goto_0
    return-void

    .line 613
    :cond_1
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v16

    .line 614
    .local v16, "trimmedNewTitle":Ljava/lang/String;
    const-string v3, ""

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 615
    move-object/from16 p1, v16

    .line 618
    :cond_2
    sget-object v3, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v3}, Lcom/sec/android/app/storycam/AppDataManager;->getOriginalTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v12

    .line 619
    .local v12, "originalTranscodeElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    const/4 v11, 0x0

    .line 621
    .local v11, "originalCaption":Lcom/samsung/app/video/editor/external/ClipartParams;
    if-eqz v12, :cond_0

    .line 624
    invoke-virtual {v12}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getTextEleListCount()I

    move-result v3

    const/4 v4, 0x1

    if-ge v3, v4, :cond_5

    .line 625
    new-instance v11, Lcom/samsung/app/video/editor/external/ClipartParams;

    .end local v11    # "originalCaption":Lcom/samsung/app/video/editor/external/ClipartParams;
    invoke-direct {v11}, Lcom/samsung/app/video/editor/external/ClipartParams;-><init>()V

    .line 626
    .restart local v11    # "originalCaption":Lcom/samsung/app/video/editor/external/ClipartParams;
    move-object/from16 v0, p1

    iput-object v0, v11, Lcom/samsung/app/video/editor/external/ClipartParams;->data:Ljava/lang/String;

    .line 627
    const/4 v3, 0x0

    invoke-virtual {v11, v3}, Lcom/samsung/app/video/editor/external/ClipartParams;->setElementID(I)V

    .line 628
    const/4 v3, 0x0

    invoke-virtual {v11, v3}, Lcom/samsung/app/video/editor/external/ClipartParams;->setStoryboardStartFrame(I)V

    .line 629
    const-wide/16 v4, 0x0

    invoke-virtual {v11, v4, v5}, Lcom/samsung/app/video/editor/external/ClipartParams;->setStoryBoardEndTime(J)V

    .line 630
    const/4 v3, 0x0

    invoke-virtual {v11, v3}, Lcom/samsung/app/video/editor/external/ClipartParams;->setThemeDefaultText(Z)V

    .line 631
    const/4 v3, 0x0

    invoke-virtual {v12, v11, v3}, Lcom/samsung/app/video/editor/external/TranscodeElement;->addTextEleList(Lcom/samsung/app/video/editor/external/ClipartParams;Z)V

    .line 639
    :goto_1
    sget-object v3, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v3}, Lcom/sec/android/app/storycam/AppDataManager;->getCurrentTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v13

    .line 640
    .local v13, "tElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    if-eqz v13, :cond_0

    .line 644
    invoke-virtual {v13}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getTextEleList()Ljava/util/List;

    move-result-object v10

    .line 645
    .local v10, "list":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    const/4 v2, 0x0

    .line 646
    .local v2, "caption":Lcom/samsung/app/video/editor/external/ClipartParams;
    if-eqz v10, :cond_3

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x1

    if-ge v3, v4, :cond_6

    .line 647
    :cond_3
    new-instance v2, Lcom/samsung/app/video/editor/external/ClipartParams;

    .end local v2    # "caption":Lcom/samsung/app/video/editor/external/ClipartParams;
    invoke-direct {v2}, Lcom/samsung/app/video/editor/external/ClipartParams;-><init>()V

    .line 648
    .restart local v2    # "caption":Lcom/samsung/app/video/editor/external/ClipartParams;
    const/4 v3, 0x0

    invoke-virtual {v13, v3}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElement(I)Lcom/samsung/app/video/editor/external/Element;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/Element;->getGroupID()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/samsung/app/video/editor/external/ClipartParams;->setElementID(I)V

    .line 650
    const-wide/16 v14, 0x0

    .line 651
    .local v14, "startTime":J
    const/4 v3, 0x0

    invoke-virtual {v13, v3}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElement(I)Lcom/samsung/app/video/editor/external/Element;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/Element;->getDuration()J

    move-result-wide v4

    add-long v8, v14, v4

    .line 652
    .local v8, "endTime":J
    long-to-float v3, v14

    const/high16 v4, 0x447a0000    # 1000.0f

    div-float/2addr v3, v4

    const/high16 v4, 0x41f00000    # 30.0f

    mul-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/samsung/app/video/editor/external/ClipartParams;->setStoryboardStartFrame(I)V

    .line 653
    long-to-float v3, v8

    const/high16 v4, 0x447a0000    # 1000.0f

    div-float/2addr v3, v4

    const/high16 v4, 0x41f00000    # 30.0f

    mul-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v3}, Lcom/samsung/app/video/editor/external/ClipartParams;->setStoryboardEndFrame(I)V

    .line 655
    const/4 v3, 0x0

    invoke-virtual {v13, v2, v3}, Lcom/samsung/app/video/editor/external/TranscodeElement;->addTextEleList(Lcom/samsung/app/video/editor/external/ClipartParams;Z)V

    .line 661
    .end local v8    # "endTime":J
    .end local v14    # "startTime":J
    :goto_2
    iget-object v3, v2, Lcom/samsung/app/video/editor/external/ClipartParams;->data:Ljava/lang/String;

    if-nez v3, :cond_4

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    .line 665
    :cond_4
    iget-object v3, v2, Lcom/samsung/app/video/editor/external/ClipartParams;->data:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 666
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/storycam/view/Caption1Line;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f02006c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    check-cast v6, Landroid/graphics/drawable/NinePatchDrawable;

    .line 667
    .local v6, "npd":Landroid/graphics/drawable/NinePatchDrawable;
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/app/video/editor/external/ClipartParams;->setThemeDefaultText(Z)V

    .line 669
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/storycam/view/Caption1Line;->mContext:Landroid/content/Context;

    invoke-virtual {v13}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getAssetManager()Landroid/content/res/AssetManager;

    move-result-object v5

    const v3, 0x7f06037e

    invoke-static {v3}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v7

    move-object/from16 v3, p1

    invoke-virtual/range {v2 .. v7}, Lcom/samsung/app/video/editor/external/ClipartParams;->changeCaptionData(Ljava/lang/String;Landroid/content/Context;Landroid/content/res/AssetManager;Landroid/graphics/drawable/NinePatchDrawable;I)V

    .line 670
    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/ClipartParams;->getCaptionID()I

    move-result v3

    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/ClipartParams;->getElementID()I

    move-result v4

    invoke-static {v10, v3, v2, v4}, Lcom/sec/android/app/storycam/Utils;->updateAllTextsWithSameCaptionID(Ljava/util/List;ILcom/samsung/app/video/editor/external/ClipartParams;I)V

    .line 671
    sget-object v3, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v3}, Lcom/sec/android/app/storycam/AppDataManager;->notifyTranscodeElementChanges()V

    .line 672
    const-string v3, ""

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 673
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/storycam/view/Caption1Line;->mUIMgr:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    invoke-interface {v3}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;->requestForOptionMenuRefresh()V

    goto/16 :goto_0

    .line 634
    .end local v2    # "caption":Lcom/samsung/app/video/editor/external/ClipartParams;
    .end local v6    # "npd":Landroid/graphics/drawable/NinePatchDrawable;
    .end local v10    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    .end local v13    # "tElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    :cond_5
    invoke-virtual {v12}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getTextEleList()Ljava/util/List;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    .end local v11    # "originalCaption":Lcom/samsung/app/video/editor/external/ClipartParams;
    check-cast v11, Lcom/samsung/app/video/editor/external/ClipartParams;

    .line 635
    .restart local v11    # "originalCaption":Lcom/samsung/app/video/editor/external/ClipartParams;
    move-object/from16 v0, p1

    iput-object v0, v11, Lcom/samsung/app/video/editor/external/ClipartParams;->data:Ljava/lang/String;

    goto/16 :goto_1

    .line 658
    .restart local v2    # "caption":Lcom/samsung/app/video/editor/external/ClipartParams;
    .restart local v10    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    .restart local v13    # "tElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    :cond_6
    const/4 v3, 0x0

    invoke-interface {v10, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "caption":Lcom/samsung/app/video/editor/external/ClipartParams;
    check-cast v2, Lcom/samsung/app/video/editor/external/ClipartParams;

    .restart local v2    # "caption":Lcom/samsung/app/video/editor/external/ClipartParams;
    goto :goto_2
.end method
