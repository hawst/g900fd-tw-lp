.class public Lcom/sec/android/app/ve/util/GenresCacheUtil;
.super Ljava/lang/Object;
.source "GenresCacheUtil.java"


# static fields
.field public static genresList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/ve/data/VEAlbum;",
            ">;"
        }
    .end annotation
.end field

.field private static genresThread:Ljava/lang/Thread;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0()Ljava/util/List;
    .locals 1

    .prologue
    .line 39
    invoke-static {}, Lcom/sec/android/app/ve/util/GenresCacheUtil;->readGenresList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static clear()V
    .locals 1

    .prologue
    .line 82
    sget-object v0, Lcom/sec/android/app/ve/util/GenresCacheUtil;->genresList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 83
    return-void
.end method

.method public static getGenresList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/ve/data/VEAlbum;",
            ">;"
        }
    .end annotation

    .prologue
    .line 78
    sget-object v0, Lcom/sec/android/app/ve/util/GenresCacheUtil;->genresList:Ljava/util/List;

    return-object v0
.end method

.method public static loadGenresList()V
    .locals 2

    .prologue
    .line 19
    sget-object v0, Lcom/sec/android/app/ve/util/GenresCacheUtil;->genresThread:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/app/ve/util/GenresCacheUtil;->genresThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 37
    :goto_0
    return-void

    .line 22
    :cond_0
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/app/ve/util/GenresCacheUtil$1;

    invoke-direct {v1}, Lcom/sec/android/app/ve/util/GenresCacheUtil$1;-><init>()V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    sput-object v0, Lcom/sec/android/app/ve/util/GenresCacheUtil;->genresThread:Ljava/lang/Thread;

    .line 35
    sget-object v0, Lcom/sec/android/app/ve/util/GenresCacheUtil;->genresThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 36
    sget-object v0, Lcom/sec/android/app/ve/util/GenresCacheUtil;->genresThread:Ljava/lang/Thread;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setPriority(I)V

    goto :goto_0
.end method

.method private static makeGenreUri(I)Landroid/net/Uri;
    .locals 5
    .param p0, "genreId"    # I

    .prologue
    const/16 v4, 0x2f

    .line 102
    sget-object v1, Landroid/provider/MediaStore$Audio$Genres;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 103
    .local v1, "GENRES_URI":Landroid/net/Uri;
    const-string v0, "members"

    .line 104
    .local v0, "CONTENTDIR":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 105
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 106
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 104
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    return-object v2
.end method

.method private static readGenresList()Ljava/util/List;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/ve/data/VEAlbum;",
            ">;"
        }
    .end annotation

    .prologue
    .line 43
    sget-object v1, Landroid/provider/MediaStore$Audio$Genres;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 44
    .local v1, "GENRES_URI":Landroid/net/Uri;
    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "_id"

    aput-object v3, v2, v0

    const/4 v0, 0x1

    .line 45
    const-string v3, "name"

    aput-object v3, v2, v0

    .line 47
    .local v2, "projection":[Ljava/lang/String;
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 49
    .local v12, "lVEAlbumList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/ve/data/VEAlbum;>;"
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 50
    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 51
    const-string v5, "name"

    .line 50
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 52
    .local v6, "cursor":Landroid/database/Cursor;
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v13

    .line 53
    .local v13, "valid":Z
    :goto_0
    if-nez v13, :cond_0

    .line 69
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 73
    .end local v6    # "cursor":Landroid/database/Cursor;
    .end local v13    # "valid":Z
    :goto_1
    return-object v12

    .line 55
    .restart local v6    # "cursor":Landroid/database/Cursor;
    .restart local v13    # "valid":Z
    :cond_0
    const-string v0, "_id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 54
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 57
    .local v8, "genresId":Ljava/lang/String;
    const-string v0, "name"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 56
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 59
    .local v9, "genresName":Ljava/lang/String;
    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v10

    .line 61
    .local v10, "id":I
    invoke-static {v10}, Lcom/sec/android/app/ve/util/GenresCacheUtil;->readSongsCountOfGenres(I)I

    move-result v0

    if-eqz v0, :cond_1

    .line 62
    new-instance v11, Lcom/sec/android/app/ve/data/VEAlbum;

    invoke-direct {v11}, Lcom/sec/android/app/ve/data/VEAlbum;-><init>()V

    .line 63
    .local v11, "lVEAlbum":Lcom/sec/android/app/ve/data/VEAlbum;
    invoke-virtual {v11, v8}, Lcom/sec/android/app/ve/data/VEAlbum;->setMusicCategoryId(Ljava/lang/String;)V

    .line 64
    invoke-virtual {v11, v9}, Lcom/sec/android/app/ve/data/VEAlbum;->setMusicCategoryName(Ljava/lang/String;)V

    .line 65
    invoke-interface {v12, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 67
    .end local v11    # "lVEAlbum":Lcom/sec/android/app/ve/data/VEAlbum;
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v13

    goto :goto_0

    .line 70
    .end local v6    # "cursor":Landroid/database/Cursor;
    .end local v8    # "genresId":Ljava/lang/String;
    .end local v9    # "genresName":Ljava/lang/String;
    .end local v10    # "id":I
    .end local v13    # "valid":Z
    :catch_0
    move-exception v7

    .line 71
    .local v7, "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public static readSongsCountOfGenres(I)I
    .locals 9
    .param p0, "id"    # I

    .prologue
    .line 86
    invoke-static {p0}, Lcom/sec/android/app/ve/util/GenresCacheUtil;->makeGenreUri(I)Landroid/net/Uri;

    move-result-object v1

    .line 87
    .local v1, "GENRES_URI":Landroid/net/Uri;
    const/4 v6, 0x0

    .line 89
    .local v6, "count":I
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "_id"

    aput-object v3, v2, v0

    .line 91
    .local v2, "projection":[Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 92
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 93
    .local v7, "cursor":Landroid/database/Cursor;
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v6

    .line 94
    invoke-interface {v7}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 98
    .end local v7    # "cursor":Landroid/database/Cursor;
    :goto_0
    return v6

    .line 95
    :catch_0
    move-exception v8

    .line 96
    .local v8, "e":Ljava/lang/Exception;
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
