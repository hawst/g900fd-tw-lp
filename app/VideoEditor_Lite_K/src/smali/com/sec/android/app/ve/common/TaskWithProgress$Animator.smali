.class Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;
.super Ljava/lang/Thread;
.source "TaskWithProgress.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/ve/common/TaskWithProgress;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Animator"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/ve/common/TaskWithProgress$Animator$MyHandler;
    }
.end annotation


# static fields
.field private static final ANIMATE_ONCE:I = 0x64

.field private static final ANIM_DELAY:I = 0x14

.field private static final ANIM_STEP:I = 0x6


# instance fields
.field private mAnimate:Z

.field private mHandler:Lcom/sec/android/app/ve/common/TaskWithProgress$Animator$MyHandler;

.field private mRotateLevel:I

.field private mRotatePivotX:I

.field private mRotatePivotY:I

.field private mSH:Landroid/view/SurfaceHolder;

.field private mTranslationX:I

.field private mTranslationY:I


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 229
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 235
    iput-object v1, p0, Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;->mHandler:Lcom/sec/android/app/ve/common/TaskWithProgress$Animator$MyHandler;

    .line 236
    iput-object v1, p0, Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;->mSH:Landroid/view/SurfaceHolder;

    .line 237
    iput-boolean v0, p0, Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;->mAnimate:Z

    .line 238
    iput v0, p0, Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;->mRotateLevel:I

    .line 239
    iput v0, p0, Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;->mRotatePivotX:I

    .line 240
    iput v0, p0, Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;->mRotatePivotY:I

    .line 241
    iput v0, p0, Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;->mTranslationX:I

    .line 242
    iput v0, p0, Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;->mTranslationY:I

    .line 229
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;)V
    .locals 0

    .prologue
    .line 229
    invoke-direct {p0}, Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;-><init>()V

    return-void
.end method

.method static synthetic access$1(Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;)I
    .locals 1

    .prologue
    .line 238
    iget v0, p0, Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;->mRotateLevel:I

    return v0
.end method

.method static synthetic access$2(Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;)I
    .locals 1

    .prologue
    .line 239
    iget v0, p0, Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;->mRotatePivotX:I

    return v0
.end method

.method static synthetic access$3(Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;)I
    .locals 1

    .prologue
    .line 240
    iget v0, p0, Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;->mRotatePivotY:I

    return v0
.end method

.method static synthetic access$4(Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;)I
    .locals 1

    .prologue
    .line 241
    iget v0, p0, Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;->mTranslationX:I

    return v0
.end method

.method static synthetic access$5(Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;)I
    .locals 1

    .prologue
    .line 242
    iget v0, p0, Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;->mTranslationY:I

    return v0
.end method

.method static synthetic access$6(Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;)Landroid/view/SurfaceHolder;
    .locals 1

    .prologue
    .line 236
    iget-object v0, p0, Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;->mSH:Landroid/view/SurfaceHolder;

    return-object v0
.end method

.method static synthetic access$7(Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;I)V
    .locals 0

    .prologue
    .line 238
    iput p1, p0, Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;->mRotateLevel:I

    return-void
.end method

.method static synthetic access$8(Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;)Z
    .locals 1

    .prologue
    .line 237
    iget-boolean v0, p0, Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;->mAnimate:Z

    return v0
.end method

.method static synthetic access$9(Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;Landroid/view/SurfaceHolder;)V
    .locals 0

    .prologue
    .line 236
    iput-object p1, p0, Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;->mSH:Landroid/view/SurfaceHolder;

    return-void
.end method


# virtual methods
.method public init()V
    .locals 2

    .prologue
    .line 246
    # getter for: Lcom/sec/android/app/ve/common/TaskWithProgress;->mAnimThread:Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;
    invoke-static {}, Lcom/sec/android/app/ve/common/TaskWithProgress;->access$4()Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;->isAlive()Z

    move-result v1

    if-nez v1, :cond_0

    .line 247
    # getter for: Lcom/sec/android/app/ve/common/TaskWithProgress;->mAnimThread:Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;
    invoke-static {}, Lcom/sec/android/app/ve/common/TaskWithProgress;->access$4()Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;->start()V

    .line 250
    :cond_0
    sget v1, Lcom/sec/android/app/ve/R$dimen;->wait_progress_icon_size:I

    invoke-static {v1}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v0

    .line 251
    .local v0, "_viewDiameter":I
    # getter for: Lcom/sec/android/app/ve/common/TaskWithProgress;->mLoadingIcon:Landroid/graphics/Bitmap;
    invoke-static {}, Lcom/sec/android/app/ve/common/TaskWithProgress;->access$5()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    sub-int v1, v0, v1

    div-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;->mTranslationX:I

    .line 252
    # getter for: Lcom/sec/android/app/ve/common/TaskWithProgress;->mLoadingIcon:Landroid/graphics/Bitmap;
    invoke-static {}, Lcom/sec/android/app/ve/common/TaskWithProgress;->access$5()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    sub-int v1, v0, v1

    div-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;->mTranslationY:I

    .line 253
    # getter for: Lcom/sec/android/app/ve/common/TaskWithProgress;->mLoadingIcon:Landroid/graphics/Bitmap;
    invoke-static {}, Lcom/sec/android/app/ve/common/TaskWithProgress;->access$5()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;->mRotatePivotX:I

    .line 254
    # getter for: Lcom/sec/android/app/ve/common/TaskWithProgress;->mLoadingIcon:Landroid/graphics/Bitmap;
    invoke-static {}, Lcom/sec/android/app/ve/common/TaskWithProgress;->access$5()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;->mRotatePivotY:I

    .line 255
    return-void
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 281
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;->mSH:Landroid/view/SurfaceHolder;

    .line 282
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;->mAnimate:Z

    .line 283
    return-void
.end method

.method public run()V
    .locals 2

    .prologue
    .line 258
    invoke-static {}, Landroid/os/Looper;->prepare()V

    .line 260
    monitor-enter p0

    .line 261
    :try_start_0
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    .line 262
    .local v0, "looper":Landroid/os/Looper;
    new-instance v1, Lcom/sec/android/app/ve/common/TaskWithProgress$Animator$MyHandler;

    invoke-direct {v1, p0, v0}, Lcom/sec/android/app/ve/common/TaskWithProgress$Animator$MyHandler;-><init>(Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;->mHandler:Lcom/sec/android/app/ve/common/TaskWithProgress$Animator$MyHandler;

    .line 260
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 265
    invoke-static {}, Landroid/os/Looper;->loop()V

    .line 266
    return-void

    .line 260
    .end local v0    # "looper":Landroid/os/Looper;
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public setSurface(Landroid/view/SurfaceHolder;)V
    .locals 4
    .param p1, "sh"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 270
    iget-object v0, p0, Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;->mHandler:Lcom/sec/android/app/ve/common/TaskWithProgress$Animator$MyHandler;

    if-eqz v0, :cond_0

    .line 271
    iget-object v0, p0, Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;->mHandler:Lcom/sec/android/app/ve/common/TaskWithProgress$Animator$MyHandler;

    new-instance v1, Lcom/sec/android/app/ve/common/TaskWithProgress$Animator$1;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/ve/common/TaskWithProgress$Animator$1;-><init>(Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;Landroid/view/SurfaceHolder;)V

    .line 276
    const-wide/16 v2, 0xc8

    .line 271
    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/ve/common/TaskWithProgress$Animator$MyHandler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 278
    :cond_0
    return-void
.end method

.method public startAnimation()V
    .locals 4

    .prologue
    const/16 v1, 0x64

    .line 287
    iget-object v0, p0, Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;->mHandler:Lcom/sec/android/app/ve/common/TaskWithProgress$Animator$MyHandler;

    if-eqz v0, :cond_0

    .line 288
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;->mAnimate:Z

    .line 289
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;->mRotateLevel:I

    .line 290
    iget-object v0, p0, Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;->mHandler:Lcom/sec/android/app/ve/common/TaskWithProgress$Animator$MyHandler;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/ve/common/TaskWithProgress$Animator$MyHandler;->removeMessages(I)V

    .line 291
    iget-object v0, p0, Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;->mHandler:Lcom/sec/android/app/ve/common/TaskWithProgress$Animator$MyHandler;

    const-wide/16 v2, 0x14

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/ve/common/TaskWithProgress$Animator$MyHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 293
    :cond_0
    return-void
.end method

.method public stopAnimation()V
    .locals 1

    .prologue
    .line 297
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;->mAnimate:Z

    .line 298
    return-void
.end method
