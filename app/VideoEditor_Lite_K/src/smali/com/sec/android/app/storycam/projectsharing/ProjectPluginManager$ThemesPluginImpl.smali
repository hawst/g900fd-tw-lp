.class public Lcom/sec/android/app/storycam/projectsharing/ProjectPluginManager$ThemesPluginImpl;
.super Ljava/lang/Object;
.source "ProjectPluginManager.java"

# interfaces
.implements Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework$ThemesPlugin;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/storycam/projectsharing/ProjectPluginManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ThemesPluginImpl"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 526
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public mapThemeFromGeneric(I)I
    .locals 1
    .param p1, "genericTheme"    # I

    .prologue
    .line 553
    const/4 v0, 0x0

    .line 554
    .local v0, "internalTheme":I
    packed-switch p1, :pswitch_data_0

    .line 571
    :goto_0
    return v0

    .line 556
    :pswitch_0
    const/4 v0, 0x1

    .line 557
    goto :goto_0

    .line 559
    :pswitch_1
    const/4 v0, 0x2

    .line 560
    goto :goto_0

    .line 562
    :pswitch_2
    const/4 v0, 0x3

    .line 563
    goto :goto_0

    .line 565
    :pswitch_3
    const/4 v0, 0x4

    .line 566
    goto :goto_0

    .line 568
    :pswitch_4
    const/4 v0, 0x5

    goto :goto_0

    .line 554
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public mapThemeToGeneric(I)I
    .locals 1
    .param p1, "internalTheme"    # I

    .prologue
    .line 530
    const/4 v0, 0x0

    .line 531
    .local v0, "genericTheme":I
    packed-switch p1, :pswitch_data_0

    .line 548
    :goto_0
    return v0

    .line 533
    :pswitch_0
    const/4 v0, 0x1

    .line 534
    goto :goto_0

    .line 536
    :pswitch_1
    const/4 v0, 0x2

    .line 537
    goto :goto_0

    .line 539
    :pswitch_2
    const/4 v0, 0x3

    .line 540
    goto :goto_0

    .line 542
    :pswitch_3
    const/4 v0, 0x4

    .line 543
    goto :goto_0

    .line 545
    :pswitch_4
    const/4 v0, 0x5

    goto :goto_0

    .line 531
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
