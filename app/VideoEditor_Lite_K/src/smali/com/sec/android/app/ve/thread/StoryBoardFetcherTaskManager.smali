.class public Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager;
.super Ljava/lang/Object;
.source "StoryBoardFetcherTaskManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$FetcherCallBack;,
        Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;,
        Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherThreadPool;
    }
.end annotation


# static fields
.field private static final CORE_POOL_SIZE:I = 0x8

.field private static final KEEP_ALIVE_TIME:I = 0x1

.field private static final KEEP_ALIVE_TIME_UNIT:Ljava/util/concurrent/TimeUnit;

.field private static final MAXIMUM_POOL_SIZE:I = 0x8

.field private static sInstance:Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager;


# instance fields
.field private final mFetchQueue:Ljava/util/concurrent/BlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingQueue",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private final mThreadPool:Ljava/util/concurrent/ThreadPoolExecutor;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    sput-object v0, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager;->KEEP_ALIVE_TIME_UNIT:Ljava/util/concurrent/TimeUnit;

    .line 58
    new-instance v0, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager;

    invoke-direct {v0}, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager;-><init>()V

    sput-object v0, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager;->sInstance:Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager;

    .line 59
    return-void
.end method

.method public constructor <init>()V
    .locals 8

    .prologue
    const/16 v2, 0x8

    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager;->mFetchQueue:Ljava/util/concurrent/BlockingQueue;

    .line 74
    new-instance v0, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherThreadPool;

    .line 75
    const-wide/16 v4, 0x1

    sget-object v6, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager;->KEEP_ALIVE_TIME_UNIT:Ljava/util/concurrent/TimeUnit;

    iget-object v7, p0, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager;->mFetchQueue:Ljava/util/concurrent/BlockingQueue;

    move-object v1, p0

    move v3, v2

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherThreadPool;-><init>(Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager;IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;)V

    .line 74
    iput-object v0, p0, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager;->mThreadPool:Ljava/util/concurrent/ThreadPoolExecutor;

    .line 76
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager;
    .locals 1

    .prologue
    .line 62
    sget-object v0, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager;->sInstance:Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager;

    return-object v0
.end method


# virtual methods
.method public addFetchTask(Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;)V
    .locals 1
    .param p1, "fetchTask"    # Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager;->mThreadPool:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ThreadPoolExecutor;->execute(Ljava/lang/Runnable;)V

    .line 80
    return-void
.end method

.method public removeFetchTask(Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;)V
    .locals 1
    .param p1, "removeTask"    # Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager;->mThreadPool:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ThreadPoolExecutor;->remove(Ljava/lang/Runnable;)Z

    .line 84
    return-void
.end method
