.class Lcom/sec/android/app/storycam/AppDataManager$1;
.super Ljava/lang/Object;
.source "AppDataManager.java"

# interfaces
.implements Lcom/sec/android/app/ve/thread/VideoImageThumbFetcher$VideoImageThumbnaiFetcherCallbak;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/storycam/AppDataManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/storycam/AppDataManager;


# direct methods
.method constructor <init>(Lcom/sec/android/app/storycam/AppDataManager;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/storycam/AppDataManager$1;->this$0:Lcom/sec/android/app/storycam/AppDataManager;

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bitmapCreated(ILandroid/graphics/Bitmap;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 5
    .param p1, "position"    # I
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;
    .param p3, "aFilePath"    # Ljava/lang/String;
    .param p4, "tag"    # Ljava/lang/Object;

    .prologue
    .line 68
    iget-object v3, p0, Lcom/sec/android/app/storycam/AppDataManager$1;->this$0:Lcom/sec/android/app/storycam/AppDataManager;

    # getter for: Lcom/sec/android/app/storycam/AppDataManager;->mThumbnailCache:Ljava/util/LinkedHashMap;
    invoke-static {v3}, Lcom/sec/android/app/storycam/AppDataManager;->access$0(Lcom/sec/android/app/storycam/AppDataManager;)Ljava/util/LinkedHashMap;

    move-result-object v3

    if-eqz v3, :cond_2

    if-eqz p2, :cond_2

    .line 69
    iget-object v3, p0, Lcom/sec/android/app/storycam/AppDataManager$1;->this$0:Lcom/sec/android/app/storycam/AppDataManager;

    # getter for: Lcom/sec/android/app/storycam/AppDataManager;->mThumbnailCacheLock:Ljava/lang/Object;
    invoke-static {v3}, Lcom/sec/android/app/storycam/AppDataManager;->access$1(Lcom/sec/android/app/storycam/AppDataManager;)Ljava/lang/Object;

    move-result-object v4

    monitor-enter v4

    .line 70
    const/4 v1, 0x0

    .line 71
    .local v1, "item":Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/storycam/AppDataManager$1;->this$0:Lcom/sec/android/app/storycam/AppDataManager;

    # getter for: Lcom/sec/android/app/storycam/AppDataManager;->mThumbnailCache:Ljava/util/LinkedHashMap;
    invoke-static {v3}, Lcom/sec/android/app/storycam/AppDataManager;->access$0(Lcom/sec/android/app/storycam/AppDataManager;)Ljava/util/LinkedHashMap;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;

    move-object v1, v0

    .line 72
    if-nez v1, :cond_1

    .line 73
    new-instance v2, Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;

    const/4 v3, 0x0

    invoke-direct {v2, p2, v3}, Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;-><init>(Landroid/graphics/Bitmap;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 74
    .end local v1    # "item":Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;
    .local v2, "item":Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;
    :try_start_1
    iget-object v3, p0, Lcom/sec/android/app/storycam/AppDataManager$1;->this$0:Lcom/sec/android/app/storycam/AppDataManager;

    # getter for: Lcom/sec/android/app/storycam/AppDataManager;->mThumbnailCache:Ljava/util/LinkedHashMap;
    invoke-static {v3}, Lcom/sec/android/app/storycam/AppDataManager;->access$0(Lcom/sec/android/app/storycam/AppDataManager;)Ljava/util/LinkedHashMap;

    move-result-object v3

    invoke-virtual {v3, p3, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-object v1, v2

    .line 69
    .end local v2    # "item":Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;
    .restart local v1    # "item":Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;
    :goto_0
    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 80
    iget-object v3, p0, Lcom/sec/android/app/storycam/AppDataManager$1;->this$0:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v3}, Lcom/sec/android/app/storycam/AppDataManager;->notifyThumbnailCacheUpdated()V

    .line 84
    .end local v1    # "item":Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;
    :cond_0
    :goto_1
    return-void

    .line 77
    .restart local v1    # "item":Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;
    :cond_1
    :try_start_3
    invoke-static {v1, p2}, Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;->access$0(Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 69
    :catchall_0
    move-exception v3

    :goto_2
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v3

    .line 82
    .end local v1    # "item":Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;
    :cond_2
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v3

    if-nez v3, :cond_0

    .line 83
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_1

    .line 69
    .restart local v2    # "item":Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;
    :catchall_1
    move-exception v3

    move-object v1, v2

    .end local v2    # "item":Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;
    .restart local v1    # "item":Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;
    goto :goto_2
.end method

.method public bitmapCreated(ILandroid/graphics/Bitmap;Ljava/lang/String;Ljava/lang/Object;Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix;Landroid/graphics/Matrix;)V
    .locals 10
    .param p1, "position"    # I
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;
    .param p3, "aFilePath"    # Ljava/lang/String;
    .param p4, "tag"    # Ljava/lang/Object;
    .param p5, "refRect"    # Landroid/graphics/RectF;
    .param p6, "startRect"    # Landroid/graphics/RectF;
    .param p7, "endRect"    # Landroid/graphics/RectF;
    .param p8, "startM"    # Landroid/graphics/Matrix;
    .param p9, "endM"    # Landroid/graphics/Matrix;

    .prologue
    .line 88
    iget-object v1, p0, Lcom/sec/android/app/storycam/AppDataManager$1;->this$0:Lcom/sec/android/app/storycam/AppDataManager;

    # getter for: Lcom/sec/android/app/storycam/AppDataManager;->mThumbnailCache:Ljava/util/LinkedHashMap;
    invoke-static {v1}, Lcom/sec/android/app/storycam/AppDataManager;->access$0(Lcom/sec/android/app/storycam/AppDataManager;)Ljava/util/LinkedHashMap;

    move-result-object v1

    if-eqz v1, :cond_2

    if-eqz p2, :cond_2

    .line 89
    iget-object v1, p0, Lcom/sec/android/app/storycam/AppDataManager$1;->this$0:Lcom/sec/android/app/storycam/AppDataManager;

    # getter for: Lcom/sec/android/app/storycam/AppDataManager;->mThumbnailCacheLock:Ljava/lang/Object;
    invoke-static {v1}, Lcom/sec/android/app/storycam/AppDataManager;->access$1(Lcom/sec/android/app/storycam/AppDataManager;)Ljava/lang/Object;

    move-result-object v9

    monitor-enter v9

    .line 90
    const/4 v0, 0x0

    .line 91
    .local v0, "item":Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/storycam/AppDataManager$1;->this$0:Lcom/sec/android/app/storycam/AppDataManager;

    # getter for: Lcom/sec/android/app/storycam/AppDataManager;->mThumbnailCache:Ljava/util/LinkedHashMap;
    invoke-static {v1}, Lcom/sec/android/app/storycam/AppDataManager;->access$0(Lcom/sec/android/app/storycam/AppDataManager;)Ljava/util/LinkedHashMap;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 92
    .end local v0    # "item":Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;
    .local v8, "item":Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;
    if-nez v8, :cond_1

    .line 93
    :try_start_1
    new-instance v0, Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;

    const/4 v2, 0x0

    move-object v1, p2

    move-object v3, p5

    move-object/from16 v4, p6

    move-object/from16 v5, p7

    move-object/from16 v6, p8

    move-object/from16 v7, p9

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;-><init>(Landroid/graphics/Bitmap;ZLandroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix;Landroid/graphics/Matrix;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 94
    .end local v8    # "item":Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;
    .restart local v0    # "item":Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;
    :try_start_2
    iget-object v1, p0, Lcom/sec/android/app/storycam/AppDataManager$1;->this$0:Lcom/sec/android/app/storycam/AppDataManager;

    # getter for: Lcom/sec/android/app/storycam/AppDataManager;->mThumbnailCache:Ljava/util/LinkedHashMap;
    invoke-static {v1}, Lcom/sec/android/app/storycam/AppDataManager;->access$0(Lcom/sec/android/app/storycam/AppDataManager;)Ljava/util/LinkedHashMap;

    move-result-object v1

    invoke-virtual {v1, p3, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    :goto_0
    monitor-exit v9
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 101
    iget-object v1, p0, Lcom/sec/android/app/storycam/AppDataManager$1;->this$0:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v1}, Lcom/sec/android/app/storycam/AppDataManager;->notifyThumbnailCacheUpdated()V

    .line 105
    .end local v0    # "item":Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;
    :cond_0
    :goto_1
    return-void

    .line 98
    .restart local v8    # "item":Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;
    :cond_1
    :try_start_3
    invoke-static {v8, p2}, Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;->access$0(Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;Landroid/graphics/Bitmap;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-object v0, v8

    .end local v8    # "item":Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;
    .restart local v0    # "item":Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;
    goto :goto_0

    .line 89
    :catchall_0
    move-exception v1

    :goto_2
    :try_start_4
    monitor-exit v9
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v1

    .line 103
    .end local v0    # "item":Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;
    :cond_2
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 104
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_1

    .line 89
    .restart local v8    # "item":Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;
    :catchall_1
    move-exception v1

    move-object v0, v8

    .end local v8    # "item":Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;
    .restart local v0    # "item":Lcom/sec/android/app/storycam/AppDataManager$ThumbnailCacheItem;
    goto :goto_2
.end method

.method public isTargetViewExist(ILjava/lang/String;Ljava/lang/Object;)Z
    .locals 1
    .param p1, "position"    # I
    .param p2, "aFilePath"    # Ljava/lang/String;
    .param p3, "tag"    # Ljava/lang/Object;

    .prologue
    .line 63
    const/4 v0, 0x1

    return v0
.end method
