.class public Lcom/sec/android/app/storycam/Utils;
.super Ljava/lang/Object;
.source "Utils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/storycam/Utils$filefilter;
    }
.end annotation


# static fields
.field public static final API_LEVEL_19:I = 0x13

.field public static AUTOSUMMARY_DURATION:I = 0x0

.field public static MAXIMUM_INPUT_DURATION_PER_FILE:I = 0x0

.field public static MINIMAL_INPUT_DURATION_LIMIT:I = 0x0

.field public static MINIMUM_INPUT_DURATION_PER_FILE:I = 0x0

.field public static final PACKAGE_SAMSUNG_APPS:Ljava/lang/String; = "com.sec.android.app.samsungapps"

.field public static final PACKAGE_VIDEO_EDITOR_1:Ljava/lang/String; = "com.sec.android.app.ve"

.field public static final PACKAGE_VIDEO_EDITOR_2:Ljava/lang/String; = "com.sec.android.app.vefull"

.field public static final SAMSUNG_APPS_DOWNLOAD_URL:Ljava/lang/String; = "http://apps.samsung.com/mw/apps311.as"

.field public static final SHOW_THEME_VIDEO_LENGTH_DIALOG:I = 0x64

.field public static final SHOW_VIDEO_EDITOR_FULL_DIALOG:I = 0x65

.field public static final TOTAL_INPUT_DURATION_LIMIT:I

.field public static final TOTAL_INPUT_ELEMENTS_LIMIT:I = 0xf

.field public static final VIDEOEDITOR_CLASS:Ljava/lang/String; = ".activity.ProjectEditActivity"

.field public static final VIDEO_CLIPS_MIN_REQ:I = 0x1


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 29
    const/16 v0, 0x7530

    sput v0, Lcom/sec/android/app/storycam/Utils;->AUTOSUMMARY_DURATION:I

    .line 30
    sput v1, Lcom/sec/android/app/storycam/Utils;->MINIMAL_INPUT_DURATION_LIMIT:I

    .line 31
    sput v1, Lcom/sec/android/app/storycam/Utils;->MINIMUM_INPUT_DURATION_PER_FILE:I

    .line 32
    const v0, 0x493e0

    sput v0, Lcom/sec/android/app/storycam/Utils;->MAXIMUM_INPUT_DURATION_PER_FILE:I

    .line 33
    sget v0, Lcom/sec/android/app/storycam/Utils;->MAXIMUM_INPUT_DURATION_PER_FILE:I

    mul-int/lit8 v0, v0, 0xf

    sput v0, Lcom/sec/android/app/storycam/Utils;->TOTAL_INPUT_DURATION_LIMIT:I

    .line 41
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getNextAvailableDeafultTitle(Ljava/lang/String;)Ljava/lang/String;
    .locals 24
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 63
    const/16 v16, 0x0

    .line 64
    .local v16, "result":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getDateFormat()Ljava/text/DateFormat;

    move-result-object v5

    .line 65
    .local v5, "dateFormat":Ljava/text/DateFormat;
    invoke-virtual {v5}, Ljava/text/DateFormat;->getCalendar()Ljava/util/Calendar;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v5, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v19

    .line 66
    .local v19, "text":Ljava/lang/String;
    const-string v20, "/"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 67
    .local v6, "dates":[Ljava/lang/String;
    const/4 v4, 0x0

    .line 68
    .local v4, "date":Ljava/lang/String;
    if-eqz v6, :cond_1

    array-length v0, v6

    move/from16 v20, v0

    const/16 v21, 0x2

    move/from16 v0, v20

    move/from16 v1, v21

    if-le v0, v1, :cond_1

    .line 69
    const/16 v20, 0x0

    aget-object v20, v6, v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->length()I

    move-result v20

    const/16 v21, 0x2

    move/from16 v0, v20

    move/from16 v1, v21

    if-le v0, v1, :cond_0

    .line 70
    new-instance v20, Ljava/lang/StringBuilder;

    const/16 v21, 0x1

    aget-object v21, v6, v21

    invoke-static/range {v21 .. v21}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v21

    invoke-direct/range {v20 .. v21}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v21, 0x2

    aget-object v21, v6, v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 80
    :goto_0
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-static/range {p0 .. p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v21

    invoke-direct/range {v20 .. v21}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    .line 82
    .local v17, "s":Ljava/lang/String;
    new-instance v9, Ljava/io/File;

    sget-object v20, Lcom/sec/android/app/ve/common/ConfigUtils;->EXPORT_PATH:Ljava/lang/String;

    move-object/from16 v0, v20

    invoke-direct {v9, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 83
    .local v9, "file":Ljava/io/File;
    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v20

    if-nez v20, :cond_2

    .line 116
    .end local v17    # "s":Ljava/lang/String;
    :goto_1
    return-object v17

    .line 72
    .end local v9    # "file":Ljava/io/File;
    :cond_0
    new-instance v20, Ljava/lang/StringBuilder;

    const/16 v21, 0x0

    aget-object v21, v6, v21

    invoke-static/range {v21 .. v21}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v21

    invoke-direct/range {v20 .. v21}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v21, 0x1

    aget-object v21, v6, v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 74
    goto :goto_0

    .line 75
    :cond_1
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    .line 76
    .local v3, "c":Ljava/util/Calendar;
    const/16 v20, 0x5

    move/from16 v0, v20

    invoke-virtual {v3, v0}, Ljava/util/Calendar;->get(I)I

    move-result v7

    .line 77
    .local v7, "dd":I
    const/16 v20, 0x2

    move/from16 v0, v20

    invoke-virtual {v3, v0}, Ljava/util/Calendar;->get(I)I

    move-result v20

    add-int/lit8 v12, v20, 0x1

    .line 78
    .local v12, "mm":I
    const-string v20, "%02d%02d"

    const/16 v21, 0x2

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    aput-object v23, v21, v22

    const/16 v22, 0x1

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    aput-object v23, v21, v22

    invoke-static/range {v20 .. v21}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 86
    .end local v3    # "c":Ljava/util/Calendar;
    .end local v7    # "dd":I
    .end local v12    # "mm":I
    .restart local v9    # "file":Ljava/io/File;
    .restart local v17    # "s":Ljava/lang/String;
    :cond_2
    invoke-virtual {v9}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v2

    .line 90
    .local v2, "allfileName":[Ljava/lang/String;
    const/4 v11, 0x0

    .line 91
    .local v11, "firstFile":Z
    const/4 v15, 0x1

    .line 92
    .local v15, "number":I
    if-eqz v2, :cond_3

    .line 93
    array-length v0, v2

    move/from16 v21, v0

    const/16 v20, 0x0

    :goto_2
    move/from16 v0, v20

    move/from16 v1, v21

    if-lt v0, v1, :cond_4

    .line 109
    :cond_3
    const/16 v20, 0x1

    move/from16 v0, v20

    if-ne v15, v0, :cond_7

    if-nez v11, :cond_7

    .line 110
    move-object/from16 v16, v17

    :goto_3
    move-object/from16 v17, v16

    .line 116
    goto :goto_1

    .line 93
    :cond_4
    aget-object v10, v2, v20

    .line 94
    .local v10, "fileName":Ljava/lang/String;
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-static/range {v17 .. v17}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v23

    invoke-direct/range {v22 .. v23}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v23, ".mp4"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v10, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_6

    .line 95
    const/4 v11, 0x1

    .line 93
    :cond_5
    :goto_4
    add-int/lit8 v20, v20, 0x1

    goto :goto_2

    .line 96
    :cond_6
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-static/range {v17 .. v17}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v23

    invoke-direct/range {v22 .. v23}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v23, "_[\\d]+\\.mp4"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v10, v0}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_5

    .line 97
    const/16 v22, 0x5f

    move/from16 v0, v22

    invoke-virtual {v10, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v18

    .line 98
    .local v18, "start":I
    const/16 v22, 0x2e

    move/from16 v0, v22

    invoke-virtual {v10, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v8

    .line 99
    .local v8, "end":I
    const/16 v22, -0x1

    move/from16 v0, v18

    move/from16 v1, v22

    if-eq v0, v1, :cond_5

    const/16 v22, -0x1

    move/from16 v0, v22

    if-eq v8, v0, :cond_5

    .line 100
    add-int/lit8 v22, v18, 0x1

    move/from16 v0, v22

    invoke-virtual {v10, v0, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v14

    .line 101
    .local v14, "numStr":Ljava/lang/String;
    invoke-static {v14}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v13

    .line 102
    .local v13, "num":I
    if-le v13, v15, :cond_5

    .line 103
    move v15, v13

    goto :goto_4

    .line 113
    .end local v8    # "end":I
    .end local v10    # "fileName":Ljava/lang/String;
    .end local v13    # "num":I
    .end local v14    # "numStr":Ljava/lang/String;
    .end local v18    # "start":I
    :cond_7
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-static/range {v17 .. v17}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v21

    invoke-direct/range {v20 .. v21}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v21, "_"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    add-int/lit8 v21, v15, 0x1

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    goto/16 :goto_3
.end method

.method public static getThemePosition(I)I
    .locals 1
    .param p0, "themeId"    # I

    .prologue
    .line 179
    const/4 v0, 0x0

    .line 180
    .local v0, "themePos":I
    packed-switch p0, :pswitch_data_0

    .line 232
    :goto_0
    :pswitch_0
    return v0

    .line 182
    :pswitch_1
    const/4 v0, 0x0

    .line 183
    goto :goto_0

    .line 185
    :pswitch_2
    const/4 v0, 0x1

    .line 186
    goto :goto_0

    .line 188
    :pswitch_3
    const/4 v0, 0x2

    .line 189
    goto :goto_0

    .line 191
    :pswitch_4
    const/4 v0, 0x3

    .line 192
    goto :goto_0

    .line 194
    :pswitch_5
    const/4 v0, 0x4

    .line 195
    goto :goto_0

    .line 197
    :pswitch_6
    const/4 v0, 0x5

    .line 198
    goto :goto_0

    .line 200
    :pswitch_7
    const/4 v0, 0x6

    .line 201
    goto :goto_0

    .line 203
    :pswitch_8
    const/4 v0, 0x7

    .line 204
    goto :goto_0

    .line 206
    :pswitch_9
    const/16 v0, 0x8

    .line 207
    goto :goto_0

    .line 209
    :pswitch_a
    const/16 v0, 0x9

    .line 210
    goto :goto_0

    .line 212
    :pswitch_b
    const/16 v0, 0xa

    .line 213
    goto :goto_0

    .line 215
    :pswitch_c
    const/16 v0, 0xb

    .line 216
    goto :goto_0

    .line 218
    :pswitch_d
    const/16 v0, 0xc

    .line 219
    goto :goto_0

    .line 221
    :pswitch_e
    const/16 v0, 0xd

    .line 222
    goto :goto_0

    .line 224
    :pswitch_f
    const/16 v0, 0xe

    .line 225
    goto :goto_0

    .line 227
    :pswitch_10
    const/16 v0, 0xf

    .line 228
    goto :goto_0

    .line 180
    nop

    :pswitch_data_0
    .packed-switch 0xd
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_f
        :pswitch_0
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_10
        :pswitch_7
    .end packed-switch
.end method

.method public static initBackgroundImage(Ljava/lang/String;III)I
    .locals 20
    .param p0, "bgFilePath"    # Ljava/lang/String;
    .param p1, "index"    # I
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 311
    sget-object v2, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v2}, Lcom/sec/android/app/storycam/AppDataManager;->getCurrentTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v19

    .line 312
    .local v19, "tElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    if-nez v19, :cond_0

    .line 313
    new-instance v2, Ljava/lang/NullPointerException;

    const-string v3, "Transcode Element is null. Cannot init background image."

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 315
    :cond_0
    if-ltz p1, :cond_1

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move/from16 v0, p1

    if-le v0, v2, :cond_2

    .line 316
    :cond_1
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "Invalid argument passed. Value "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " is not in range 0 to "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ". Cannot init background image."

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 318
    :cond_2
    const/16 v18, -0x1

    .line 319
    .local v18, "result":I
    move-object/from16 v0, v19

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElement(I)Lcom/samsung/app/video/editor/external/Element;

    move-result-object v4

    .line 320
    .local v4, "ele":Lcom/samsung/app/video/editor/external/Element;
    if-nez v4, :cond_3

    .line 321
    new-instance v2, Ljava/lang/NullPointerException;

    const-string v3, "Element is null. Cannot init background image."

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 324
    :cond_3
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v2

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getAssetManager()Landroid/content/res/AssetManager;

    move-result-object v3

    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/Element;->getStartTime()J

    move-result-wide v6

    long-to-int v5, v6

    const/4 v8, 0x0

    const/4 v9, 0x1

    const/4 v10, 0x1

    const/4 v11, 0x1

    move/from16 v6, p2

    move/from16 v7, p3

    invoke-virtual/range {v2 .. v11}, Lcom/samsung/app/video/editor/external/NativeInterface;->_getProjectPreviewImage(Landroid/content/res/AssetManager;Lcom/samsung/app/video/editor/external/Element;IIIIIII)[B

    move-result-object v13

    .line 327
    .local v13, "bytes":[B
    if-eqz v13, :cond_5

    .line 328
    sget-object v2, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    move/from16 v0, p2

    move/from16 v1, p3

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v12

    .line 329
    .local v12, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v12, :cond_5

    .line 330
    invoke-static {v13}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v17

    .line 331
    .local v17, "pixelBuffer":Ljava/nio/ByteBuffer;
    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Landroid/graphics/Bitmap;->copyPixelsFromBuffer(Ljava/nio/Buffer;)V

    .line 332
    const/4 v15, 0x0

    .line 334
    .local v15, "fos":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v16, Ljava/io/FileOutputStream;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 335
    .end local v15    # "fos":Ljava/io/FileOutputStream;
    .local v16, "fos":Ljava/io/FileOutputStream;
    :try_start_1
    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v3, 0x64

    move-object/from16 v0, v16

    invoke-virtual {v12, v2, v3, v0}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 336
    const/16 v18, 0x0

    .line 341
    if-eqz v16, :cond_4

    .line 343
    :try_start_2
    invoke-virtual/range {v16 .. v16}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    .line 348
    :cond_4
    :goto_0
    invoke-virtual {v12}, Landroid/graphics/Bitmap;->recycle()V

    .line 353
    .end local v12    # "bitmap":Landroid/graphics/Bitmap;
    .end local v16    # "fos":Ljava/io/FileOutputStream;
    .end local v17    # "pixelBuffer":Ljava/nio/ByteBuffer;
    :cond_5
    :goto_1
    return v18

    .line 337
    .restart local v12    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v15    # "fos":Ljava/io/FileOutputStream;
    .restart local v17    # "pixelBuffer":Ljava/nio/ByteBuffer;
    :catch_0
    move-exception v14

    .line 338
    .local v14, "e":Ljava/io/FileNotFoundException;
    :goto_2
    :try_start_3
    invoke-virtual {v14}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 341
    if-eqz v15, :cond_6

    .line 343
    :try_start_4
    invoke-virtual {v15}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    .line 348
    .end local v14    # "e":Ljava/io/FileNotFoundException;
    :cond_6
    :goto_3
    invoke-virtual {v12}, Landroid/graphics/Bitmap;->recycle()V

    .line 349
    const/4 v12, 0x0

    goto :goto_1

    .line 344
    .restart local v14    # "e":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v14

    .line 345
    .local v14, "e":Ljava/io/IOException;
    invoke-virtual {v14}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 340
    .end local v14    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v2

    .line 341
    :goto_4
    if-eqz v15, :cond_7

    .line 343
    :try_start_5
    invoke-virtual {v15}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    .line 348
    :cond_7
    :goto_5
    invoke-virtual {v12}, Landroid/graphics/Bitmap;->recycle()V

    .line 349
    const/4 v12, 0x0

    .line 350
    throw v2

    .line 344
    :catch_2
    move-exception v14

    .line 345
    .restart local v14    # "e":Ljava/io/IOException;
    invoke-virtual {v14}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 344
    .end local v14    # "e":Ljava/io/IOException;
    .end local v15    # "fos":Ljava/io/FileOutputStream;
    .restart local v16    # "fos":Ljava/io/FileOutputStream;
    :catch_3
    move-exception v14

    .line 345
    .restart local v14    # "e":Ljava/io/IOException;
    invoke-virtual {v14}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 340
    .end local v14    # "e":Ljava/io/IOException;
    :catchall_1
    move-exception v2

    move-object/from16 v15, v16

    .end local v16    # "fos":Ljava/io/FileOutputStream;
    .restart local v15    # "fos":Ljava/io/FileOutputStream;
    goto :goto_4

    .line 337
    .end local v15    # "fos":Ljava/io/FileOutputStream;
    .restart local v16    # "fos":Ljava/io/FileOutputStream;
    :catch_4
    move-exception v14

    move-object/from16 v15, v16

    .end local v16    # "fos":Ljava/io/FileOutputStream;
    .restart local v15    # "fos":Ljava/io/FileOutputStream;
    goto :goto_2
.end method

.method public static initDimensions()V
    .locals 2

    .prologue
    .line 49
    const v1, 0x7f0602dc

    :try_start_0
    invoke-static {v1}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v1

    sput v1, Lcom/sec/android/app/ve/common/ConfigUtils;->FIXED_PREVIEW_WIDTH:I

    .line 50
    const v1, 0x7f0602db

    invoke-static {v1}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v1

    sput v1, Lcom/sec/android/app/ve/common/ConfigUtils;->FIXED_PREVIEW_HEIGHT:I

    .line 52
    const v1, 0x7f0602dc

    invoke-static {v1}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v1

    sput v1, Lcom/sec/android/app/ve/common/ConfigUtils;->PREVIEW_WIDTH:I

    .line 53
    const v1, 0x7f0602db

    invoke-static {v1}, Lcom/sec/android/app/ve/VEApp;->getPixDimen(I)I

    move-result v1

    sput v1, Lcom/sec/android/app/ve/common/ConfigUtils;->PREVIEW_HEIGHT:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 59
    .local v0, "e":Ljava/lang/Exception;
    :goto_0
    return-void

    .line 56
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_0
    move-exception v0

    .line 57
    .restart local v0    # "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static isImage(Ljava/lang/String;)Z
    .locals 1
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 276
    const-string v0, "jpeg"

    invoke-virtual {p0, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "jpg"

    invoke-virtual {p0, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 277
    const-string v0, "JPEG"

    invoke-virtual {p0, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "JPG"

    invoke-virtual {p0, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 278
    const-string v0, "png"

    invoke-virtual {p0, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "PNG"

    invoke-virtual {p0, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 279
    const-string v0, "bmp"

    invoke-virtual {p0, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "BMP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 280
    const-string v0, "raw"

    invoke-virtual {p0, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "RAW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 281
    const-string v0, "gif"

    invoke-virtual {p0, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "GIF"

    invoke-virtual {p0, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 276
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static isInteger(Ljava/lang/String;)I
    .locals 2
    .param p0, "input"    # Ljava/lang/String;

    .prologue
    .line 268
    :try_start_0
    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 271
    :goto_0
    return v1

    .line 270
    :catch_0
    move-exception v0

    .line 271
    .local v0, "e":Ljava/lang/Exception;
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public static isTranscodeElementValid(Lcom/samsung/app/video/editor/external/TranscodeElement;)Z
    .locals 8
    .param p0, "transcodeElement"    # Lcom/samsung/app/video/editor/external/TranscodeElement;

    .prologue
    .line 360
    const/4 v4, 0x0

    .line 361
    .local v4, "result":Z
    if-eqz p0, :cond_2

    .line 362
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementList()Ljava/util/List;

    move-result-object v2

    .line 363
    .local v2, "eleList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v0

    .line 364
    .local v0, "count":I
    if-eqz v0, :cond_0

    const/16 v6, 0xf

    if-le v0, v6, :cond_1

    :cond_0
    move v5, v4

    .line 378
    .end local v0    # "count":I
    .end local v2    # "eleList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    .end local v4    # "result":Z
    .local v5, "result":I
    :goto_0
    return v5

    .line 366
    .end local v5    # "result":I
    .restart local v0    # "count":I
    .restart local v2    # "eleList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    .restart local v4    # "result":Z
    :cond_1
    const/4 v4, 0x1

    .line 367
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    if-lt v3, v0, :cond_3

    .end local v0    # "count":I
    .end local v2    # "eleList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    .end local v3    # "i":I
    :cond_2
    :goto_2
    move v5, v4

    .line 378
    .restart local v5    # "result":I
    goto :goto_0

    .line 368
    .end local v5    # "result":I
    .restart local v0    # "count":I
    .restart local v2    # "eleList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    .restart local v3    # "i":I
    :cond_3
    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/app/video/editor/external/Element;

    .line 369
    .local v1, "ele":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/Element;->getID()I

    move-result v6

    const/4 v7, -0x1

    if-ne v6, v7, :cond_4

    .line 370
    const/4 v4, 0x0

    .line 371
    goto :goto_2

    .line 367
    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method public static mapExportResolution(I)I
    .locals 1
    .param p0, "index"    # I

    .prologue
    .line 236
    const/4 v0, 0x1

    .line 237
    .local v0, "exportResolution":I
    packed-switch p0, :pswitch_data_0

    .line 251
    :goto_0
    return v0

    .line 239
    :pswitch_0
    const/4 v0, 0x1

    .line 240
    goto :goto_0

    .line 242
    :pswitch_1
    const/4 v0, 0x2

    .line 243
    goto :goto_0

    .line 245
    :pswitch_2
    const/4 v0, 0x4

    .line 246
    goto :goto_0

    .line 237
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static mapThemeIndexToID(I)I
    .locals 1
    .param p0, "index"    # I

    .prologue
    .line 121
    const/4 v0, 0x0

    .line 122
    .local v0, "themeID":I
    packed-switch p0, :pswitch_data_0

    .line 175
    :goto_0
    return v0

    .line 124
    :pswitch_0
    const/16 v0, 0xd

    .line 125
    goto :goto_0

    .line 127
    :pswitch_1
    const/16 v0, 0xe

    .line 128
    goto :goto_0

    .line 130
    :pswitch_2
    const/16 v0, 0xf

    .line 131
    goto :goto_0

    .line 133
    :pswitch_3
    const/16 v0, 0x10

    .line 134
    goto :goto_0

    .line 136
    :pswitch_4
    const/16 v0, 0x11

    .line 137
    goto :goto_0

    .line 139
    :pswitch_5
    const/16 v0, 0x12

    .line 140
    goto :goto_0

    .line 142
    :pswitch_6
    const/16 v0, 0x1d

    .line 143
    goto :goto_0

    .line 145
    :pswitch_7
    const/16 v0, 0x15

    .line 146
    goto :goto_0

    .line 148
    :pswitch_8
    const/16 v0, 0x16

    .line 149
    goto :goto_0

    .line 151
    :pswitch_9
    const/16 v0, 0x17

    .line 152
    goto :goto_0

    .line 154
    :pswitch_a
    const/16 v0, 0x18

    .line 155
    goto :goto_0

    .line 157
    :pswitch_b
    const/16 v0, 0x19

    .line 158
    goto :goto_0

    .line 160
    :pswitch_c
    const/16 v0, 0x1a

    .line 161
    goto :goto_0

    .line 163
    :pswitch_d
    const/16 v0, 0x1b

    .line 164
    goto :goto_0

    .line 166
    :pswitch_e
    const/16 v0, 0x13

    .line 167
    goto :goto_0

    .line 169
    :pswitch_f
    const/16 v0, 0x1c

    .line 170
    goto :goto_0

    .line 122
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
    .end packed-switch
.end method

.method public static updateAllTextsWithSameCaptionID(Ljava/util/List;ILcom/samsung/app/video/editor/external/ClipartParams;I)V
    .locals 5
    .param p1, "captionID"    # I
    .param p2, "referenceCaption"    # Lcom/samsung/app/video/editor/external/ClipartParams;
    .param p3, "elementID"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/ClipartParams;",
            ">;I",
            "Lcom/samsung/app/video/editor/external/ClipartParams;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 285
    .local p0, "list":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    if-eqz p0, :cond_0

    if-eqz p2, :cond_0

    .line 286
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    .line 287
    .local v1, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-lt v2, v1, :cond_1

    .line 297
    .end local v1    # "count":I
    .end local v2    # "i":I
    :cond_0
    return-void

    .line 288
    .restart local v1    # "count":I
    .restart local v2    # "i":I
    :cond_1
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/app/video/editor/external/ClipartParams;

    .line 289
    .local v0, "clipartParam":Lcom/samsung/app/video/editor/external/ClipartParams;
    invoke-virtual {v0, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {v0}, Lcom/samsung/app/video/editor/external/ClipartParams;->getCaptionID()I

    move-result v3

    invoke-virtual {p2}, Lcom/samsung/app/video/editor/external/ClipartParams;->getCaptionID()I

    move-result v4

    if-ne v3, v4, :cond_2

    .line 291
    iget-object v3, p2, Lcom/samsung/app/video/editor/external/ClipartParams;->data:Ljava/lang/String;

    if-eqz v3, :cond_3

    new-instance v3, Ljava/lang/String;

    iget-object v4, p2, Lcom/samsung/app/video/editor/external/ClipartParams;->data:Ljava/lang/String;

    invoke-direct {v3, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    :goto_1
    iput-object v3, v0, Lcom/samsung/app/video/editor/external/ClipartParams;->data:Ljava/lang/String;

    .line 292
    invoke-virtual {p2}, Lcom/samsung/app/video/editor/external/ClipartParams;->getFilePath()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Lcom/samsung/app/video/editor/external/ClipartParams;->setRawFilePath(Ljava/lang/String;Z)V

    .line 293
    invoke-virtual {v0, p3}, Lcom/samsung/app/video/editor/external/ClipartParams;->setElementID(I)V

    .line 287
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 291
    :cond_3
    const/4 v3, 0x0

    goto :goto_1
.end method
