.class Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$6;
.super Landroid/os/Handler;
.source "VideoEditorLiteActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$6;->this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;

    .line 1653
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 1657
    :try_start_0
    iget v4, p1, Landroid/os/Message;->what:I

    const/16 v5, 0x6f

    if-ne v4, v5, :cond_0

    .line 1658
    new-instance v4, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogRemoveClips;

    invoke-direct {v4}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogRemoveClips;-><init>()V

    iget-object v5, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$6;->this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;

    invoke-static {v4, v5}, Lcom/sec/android/app/ve/common/AndroidUtils;->showDialog(Landroid/app/DialogFragment;Landroid/app/Activity;)V

    .line 1660
    :cond_0
    iget v4, p1, Landroid/os/Message;->what:I

    const/16 v5, 0x7c

    if-ne v4, v5, :cond_1

    .line 1662
    invoke-static {}, Lcom/sec/android/app/storycam/summary/Summary;->deinit()V

    .line 1664
    :cond_1
    iget v4, p1, Landroid/os/Message;->what:I

    const/16 v5, 0x7a

    if-ne v4, v5, :cond_5

    .line 1665
    const-string v4, "LIBRARY"

    const-string v5, "MSG_SUMMARY_FAILURE"

    invoke-static {v4, v5}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 1666
    iget v4, p1, Landroid/os/Message;->arg1:I

    const/16 v5, 0x7e

    if-ne v4, v5, :cond_4

    .line 1668
    sget-object v4, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v4}, Lcom/sec/android/app/storycam/AppDataManager;->getOriginalTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v2

    .line 1669
    .local v2, "tElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    if-eqz v2, :cond_3

    iget v4, p1, Landroid/os/Message;->arg2:I

    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v5

    if-ge v4, v5, :cond_3

    .line 1670
    invoke-static {}, Lcom/sec/android/app/storycam/summary/Summary;->stopSummary()V

    .line 1671
    new-instance v3, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$UnsupportedContentsDialog;

    invoke-direct {v3}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$UnsupportedContentsDialog;-><init>()V

    .line 1672
    .local v3, "unsupportedDialog":Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$UnsupportedContentsDialog;
    iget-object v4, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$6;->this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;

    invoke-static {v3, v4}, Lcom/sec/android/app/ve/common/AndroidUtils;->showDialog(Landroid/app/DialogFragment;Landroid/app/Activity;)V

    .line 1673
    iget v4, p1, Landroid/os/Message;->arg2:I

    invoke-virtual {v2, v4}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElement(I)Lcom/samsung/app/video/editor/external/Element;

    move-result-object v4

    const/4 v5, -0x1

    invoke-virtual {v4, v5}, Lcom/samsung/app/video/editor/external/Element;->setID(I)V

    .line 1674
    iget-object v4, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$6;->this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;

    sget-object v5, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;->STATE_EDIT:Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;

    const/4 v6, 0x0

    const/4 v7, 0x1

    invoke-virtual {v4, v5, v6, v7}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->changeActivityState(Lcom/sec/android/app/storycam/interfaces/UIManagerInterface$ActivityState;ZZ)V

    .line 1708
    .end local v2    # "tElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    .end local v3    # "unsupportedDialog":Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$UnsupportedContentsDialog;
    :cond_2
    :goto_0
    return-void

    .line 1677
    .restart local v2    # "tElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    :cond_3
    iget-object v4, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$6;->this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;

    invoke-virtual {v4}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->finish()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1704
    .end local v2    # "tElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    :catch_0
    move-exception v0

    .line 1705
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 1706
    iget-object v4, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$6;->this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;

    invoke-virtual {v4}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->finish()V

    goto :goto_0

    .line 1681
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_4
    :try_start_1
    iget-object v4, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$6;->this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;

    invoke-virtual {v4}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->finish()V

    goto :goto_0

    .line 1683
    :cond_5
    iget v4, p1, Landroid/os/Message;->what:I

    const/16 v5, 0x7b

    if-ne v4, v5, :cond_7

    .line 1684
    iget-object v4, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$6;->this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;

    invoke-static {v4}, Lcom/sec/android/app/storycam/summary/Summary;->showSummary(Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;)V

    .line 1685
    iget-object v4, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$6;->this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;

    # getter for: Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->mRootView:Landroid/widget/RelativeLayout;
    invoke-static {v4}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->access$0(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;)Landroid/widget/RelativeLayout;

    move-result-object v4

    const v5, 0x7f0d0003

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/storycam/view/PreviewViewGroup;

    .line 1688
    .local v1, "pvg":Lcom/sec/android/app/storycam/view/PreviewViewGroup;
    iget-object v4, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 1689
    if-eqz v1, :cond_2

    .line 1690
    invoke-virtual {v1}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->start_play()I

    goto :goto_0

    .line 1694
    :cond_6
    if-eqz v1, :cond_2

    .line 1695
    invoke-virtual {v1}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->refreshPreviewToCurrentSeekBar()V

    .line 1696
    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Lcom/sec/android/app/storycam/view/PreviewViewGroup;->setPlayButtonVisibility(I)V

    goto :goto_0

    .line 1700
    .end local v1    # "pvg":Lcom/sec/android/app/storycam/view/PreviewViewGroup;
    :cond_7
    iget v4, p1, Landroid/os/Message;->what:I

    const/16 v5, 0xff

    if-ne v4, v5, :cond_2

    .line 1701
    const-string v4, "LIBRARY"

    const-string v5, "MSG_SUMMARY_FAILURE"

    invoke-static {v4, v5}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 1702
    iget-object v4, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$6;->this$0:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;

    # invokes: Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->handleBackPressed()V
    invoke-static {v4}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->access$6(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method
