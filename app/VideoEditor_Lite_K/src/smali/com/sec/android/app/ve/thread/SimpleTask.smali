.class public abstract Lcom/sec/android/app/ve/thread/SimpleTask;
.super Landroid/os/AsyncTask;
.source "SimpleTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Object;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/ve/thread/SimpleTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected final varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Object;
    .locals 1
    .param p1, "arg0"    # [Ljava/lang/Void;

    .prologue
    .line 9
    invoke-virtual {p0}, Lcom/sec/android/app/ve/thread/SimpleTask;->getParam()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/ve/thread/SimpleTask;->onBackgroundTask(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected abstract getParam()Ljava/lang/Object;
.end method

.method protected abstract getPool()Lcom/sec/android/app/ve/thread/AsyncTaskPool;
.end method

.method protected abstract onBackgroundTask(Ljava/lang/Object;)Ljava/lang/Object;
.end method

.method protected final onPostExecute(Ljava/lang/Object;)V
    .locals 1
    .param p1, "result"    # Ljava/lang/Object;

    .prologue
    .line 14
    if-eqz p1, :cond_0

    .line 15
    invoke-virtual {p0, p1}, Lcom/sec/android/app/ve/thread/SimpleTask;->onUIThreadFinally(Ljava/lang/Object;)V

    .line 17
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/ve/thread/SimpleTask;->getPool()Lcom/sec/android/app/ve/thread/AsyncTaskPool;

    move-result-object v0

    .line 18
    .local v0, "pool":Lcom/sec/android/app/ve/thread/AsyncTaskPool;
    if-eqz v0, :cond_1

    .line 19
    invoke-virtual {v0, p0}, Lcom/sec/android/app/ve/thread/AsyncTaskPool;->onTaskFinished(Lcom/sec/android/app/ve/thread/SimpleTask;)V

    .line 20
    :cond_1
    return-void
.end method

.method protected abstract onUIThreadFinally(Ljava/lang/Object;)V
.end method
