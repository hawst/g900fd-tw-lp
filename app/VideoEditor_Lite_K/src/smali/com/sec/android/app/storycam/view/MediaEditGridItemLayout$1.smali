.class Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$1;
.super Ljava/lang/Object;
.source "MediaEditGridItemLayout.java"

# interfaces
.implements Lcom/sec/android/app/storycam/view/DeleteOnFlingListener$DeleteCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;


# direct methods
.method constructor <init>(Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$1;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public canDelete()Z
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x1

    return v0
.end method

.method public getConfig()Landroid/content/res/Configuration;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$1;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;

    # getter for: Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mConfig:Landroid/content/res/Configuration;
    invoke-static {v0}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->access$0(Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;)Landroid/content/res/Configuration;

    move-result-object v0

    return-object v0
.end method

.method public onDelete(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$1;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;

    # getter for: Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mMetaData:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;
    invoke-static {v0}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->access$1(Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;)Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 62
    iget-object v0, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$1;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;

    # getter for: Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mMetaData:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;
    invoke-static {v0}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->access$1(Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;)Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;

    move-result-object v0

    # getter for: Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;->callback:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MediaGridItemInterface;
    invoke-static {v0}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;->access$0(Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;)Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MediaGridItemInterface;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$1;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;

    # getter for: Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->mMetaData:Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;
    invoke-static {v1}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->access$1(Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;)Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;

    move-result-object v1

    # getter for: Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;->index:I
    invoke-static {v1}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;->access$1(Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MetaData;)I

    move-result v1

    invoke-interface {v0, v1}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout$MediaGridItemInterface;->onDeleteButtonClicked(I)V

    .line 64
    :cond_0
    return-void
.end method
