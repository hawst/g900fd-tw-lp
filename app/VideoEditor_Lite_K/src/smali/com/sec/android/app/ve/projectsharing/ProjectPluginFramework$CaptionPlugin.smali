.class public interface abstract Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework$CaptionPlugin;
.super Ljava/lang/Object;
.source "ProjectPluginFramework.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/ve/projectsharing/ProjectPluginFramework;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "CaptionPlugin"
.end annotation


# virtual methods
.method public abstract mapCaptionAlignmentFromGeneric(I)I
.end method

.method public abstract mapCaptionAlignmentToGeneric(I)I
.end method

.method public abstract mapCaptionThemeFromGeneric(I)I
.end method

.method public abstract mapCaptionThemeToGeneric(I)I
.end method

.method public abstract mapThemeCaptionAlignmentFromGeneric(I)I
.end method

.method public abstract mapThemeCaptionAlignmentToGeneric(I)I
.end method
