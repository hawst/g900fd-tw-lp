.class Lcom/sec/android/app/ve/common/TaskWithProgress$Animator$MyHandler;
.super Landroid/os/Handler;
.source "TaskWithProgress.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyHandler"
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .prologue
    .line 302
    iput-object p1, p0, Lcom/sec/android/app/ve/common/TaskWithProgress$Animator$MyHandler;->this$1:Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;

    .line 303
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 304
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 9
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v8, 0x1

    const/16 v7, 0x64

    .line 307
    iget v4, p1, Landroid/os/Message;->what:I

    if-ne v7, v4, :cond_1

    .line 310
    new-instance v2, Landroid/graphics/Matrix;

    invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V

    .line 311
    .local v2, "m":Landroid/graphics/Matrix;
    iget-object v4, p0, Lcom/sec/android/app/ve/common/TaskWithProgress$Animator$MyHandler;->this$1:Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;

    # getter for: Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;->mRotateLevel:I
    invoke-static {v4}, Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;->access$1(Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;)I

    move-result v4

    int-to-float v4, v4

    iget-object v5, p0, Lcom/sec/android/app/ve/common/TaskWithProgress$Animator$MyHandler;->this$1:Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;

    # getter for: Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;->mRotatePivotX:I
    invoke-static {v5}, Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;->access$2(Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;)I

    move-result v5

    int-to-float v5, v5

    iget-object v6, p0, Lcom/sec/android/app/ve/common/TaskWithProgress$Animator$MyHandler;->this$1:Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;

    # getter for: Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;->mRotatePivotY:I
    invoke-static {v6}, Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;->access$3(Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;)I

    move-result v6

    int-to-float v6, v6

    invoke-virtual {v2, v4, v5, v6}, Landroid/graphics/Matrix;->preRotate(FFF)Z

    .line 312
    iget-object v4, p0, Lcom/sec/android/app/ve/common/TaskWithProgress$Animator$MyHandler;->this$1:Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;

    # getter for: Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;->mTranslationX:I
    invoke-static {v4}, Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;->access$4(Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;)I

    move-result v4

    int-to-float v4, v4

    iget-object v5, p0, Lcom/sec/android/app/ve/common/TaskWithProgress$Animator$MyHandler;->this$1:Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;

    # getter for: Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;->mTranslationY:I
    invoke-static {v5}, Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;->access$5(Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;)I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 314
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    .line 315
    .local v3, "painter":Landroid/graphics/Paint;
    sget-object v4, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 316
    invoke-virtual {v3, v8}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 317
    invoke-virtual {v3, v8}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 319
    iget-object v4, p0, Lcom/sec/android/app/ve/common/TaskWithProgress$Animator$MyHandler;->this$1:Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;

    # getter for: Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;->mSH:Landroid/view/SurfaceHolder;
    invoke-static {v4}, Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;->access$6(Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;)Landroid/view/SurfaceHolder;

    move-result-object v4

    if-eqz v4, :cond_0

    # getter for: Lcom/sec/android/app/ve/common/TaskWithProgress;->mLoadingIcon:Landroid/graphics/Bitmap;
    invoke-static {}, Lcom/sec/android/app/ve/common/TaskWithProgress;->access$5()Landroid/graphics/Bitmap;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 320
    iget-object v4, p0, Lcom/sec/android/app/ve/common/TaskWithProgress$Animator$MyHandler;->this$1:Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;

    # getter for: Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;->mSH:Landroid/view/SurfaceHolder;
    invoke-static {v4}, Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;->access$6(Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;)Landroid/view/SurfaceHolder;

    move-result-object v4

    invoke-interface {v4}, Landroid/view/SurfaceHolder;->lockCanvas()Landroid/graphics/Canvas;

    move-result-object v0

    .line 321
    .local v0, "cnvs":Landroid/graphics/Canvas;
    if-eqz v0, :cond_0

    .line 322
    const/4 v4, 0x0

    sget-object v5, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 323
    # getter for: Lcom/sec/android/app/ve/common/TaskWithProgress;->mLoadingIcon:Landroid/graphics/Bitmap;
    invoke-static {}, Lcom/sec/android/app/ve/common/TaskWithProgress;->access$5()Landroid/graphics/Bitmap;

    move-result-object v4

    invoke-virtual {v0, v4, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    .line 325
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/app/ve/common/TaskWithProgress$Animator$MyHandler;->this$1:Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;

    # getter for: Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;->mSH:Landroid/view/SurfaceHolder;
    invoke-static {v4}, Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;->access$6(Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;)Landroid/view/SurfaceHolder;

    move-result-object v4

    invoke-interface {v4, v0}, Landroid/view/SurfaceHolder;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 333
    .end local v0    # "cnvs":Landroid/graphics/Canvas;
    :cond_0
    :goto_0
    iget-object v4, p0, Lcom/sec/android/app/ve/common/TaskWithProgress$Animator$MyHandler;->this$1:Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;

    # getter for: Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;->mRotateLevel:I
    invoke-static {v4}, Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;->access$1(Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;)I

    move-result v5

    add-int/lit8 v5, v5, 0x6

    invoke-static {v4, v5}, Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;->access$7(Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;I)V

    .line 334
    iget-object v4, p0, Lcom/sec/android/app/ve/common/TaskWithProgress$Animator$MyHandler;->this$1:Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;

    # getter for: Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;->mRotateLevel:I
    invoke-static {v4}, Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;->access$1(Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;)I

    move-result v5

    rem-int/lit16 v5, v5, 0x168

    invoke-static {v4, v5}, Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;->access$7(Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;I)V

    .line 336
    invoke-virtual {p0, v7}, Lcom/sec/android/app/ve/common/TaskWithProgress$Animator$MyHandler;->removeMessages(I)V

    .line 337
    iget-object v4, p0, Lcom/sec/android/app/ve/common/TaskWithProgress$Animator$MyHandler;->this$1:Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;

    # getter for: Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;->mAnimate:Z
    invoke-static {v4}, Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;->access$8(Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 338
    const-wide/16 v4, 0x14

    invoke-virtual {p0, v7, v4, v5}, Lcom/sec/android/app/ve/common/TaskWithProgress$Animator$MyHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 342
    .end local v2    # "m":Landroid/graphics/Matrix;
    .end local v3    # "painter":Landroid/graphics/Paint;
    :cond_1
    :goto_1
    return-void

    .line 327
    .restart local v0    # "cnvs":Landroid/graphics/Canvas;
    .restart local v2    # "m":Landroid/graphics/Matrix;
    .restart local v3    # "painter":Landroid/graphics/Paint;
    :catch_0
    move-exception v1

    .line 328
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 340
    .end local v0    # "cnvs":Landroid/graphics/Canvas;
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/ve/common/TaskWithProgress$Animator$MyHandler;->this$1:Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;

    const/4 v5, 0x0

    invoke-static {v4, v5}, Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;->access$9(Lcom/sec/android/app/ve/common/TaskWithProgress$Animator;Landroid/view/SurfaceHolder;)V

    goto :goto_1
.end method
