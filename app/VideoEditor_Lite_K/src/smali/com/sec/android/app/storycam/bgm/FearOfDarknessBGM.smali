.class public Lcom/sec/android/app/storycam/bgm/FearOfDarknessBGM;
.super Lcom/sec/android/app/ve/bgm/BGM;
.source "FearOfDarknessBGM.java"


# static fields
.field private static synthetic $SWITCH_TABLE$com$sec$android$app$ve$bgm$BGM$BGMModes:[I


# instance fields
.field private fastModeBGM:[I

.field private mediumModeBGM:[I

.field private slowModeBGM:[I

.field private totalDurationBGM:I


# direct methods
.method static synthetic $SWITCH_TABLE$com$sec$android$app$ve$bgm$BGM$BGMModes()[I
    .locals 3

    .prologue
    .line 5
    sget-object v0, Lcom/sec/android/app/storycam/bgm/FearOfDarknessBGM;->$SWITCH_TABLE$com$sec$android$app$ve$bgm$BGM$BGMModes:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/sec/android/app/ve/bgm/BGM$BGMModes;->values()[Lcom/sec/android/app/ve/bgm/BGM$BGMModes;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/sec/android/app/ve/bgm/BGM$BGMModes;->FAST:Lcom/sec/android/app/ve/bgm/BGM$BGMModes;

    invoke-virtual {v1}, Lcom/sec/android/app/ve/bgm/BGM$BGMModes;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    :try_start_1
    sget-object v1, Lcom/sec/android/app/ve/bgm/BGM$BGMModes;->MEDIUM:Lcom/sec/android/app/ve/bgm/BGM$BGMModes;

    invoke-virtual {v1}, Lcom/sec/android/app/ve/bgm/BGM$BGMModes;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    :try_start_2
    sget-object v1, Lcom/sec/android/app/ve/bgm/BGM$BGMModes;->SLOW:Lcom/sec/android/app/ve/bgm/BGM$BGMModes;

    invoke-virtual {v1}, Lcom/sec/android/app/ve/bgm/BGM$BGMModes;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_3
    sput-object v0, Lcom/sec/android/app/storycam/bgm/FearOfDarknessBGM;->$SWITCH_TABLE$com$sec$android$app$ve$bgm$BGM$BGMModes:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_1
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/sec/android/app/ve/bgm/BGM;-><init>()V

    .line 7
    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    .line 11
    iput-object v0, p0, Lcom/sec/android/app/storycam/bgm/FearOfDarknessBGM;->slowModeBGM:[I

    .line 14
    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    .line 20
    iput-object v0, p0, Lcom/sec/android/app/storycam/bgm/FearOfDarknessBGM;->mediumModeBGM:[I

    .line 23
    const/16 v0, 0xa

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    .line 32
    iput-object v0, p0, Lcom/sec/android/app/storycam/bgm/FearOfDarknessBGM;->fastModeBGM:[I

    .line 35
    const/16 v0, 0x7d00

    iput v0, p0, Lcom/sec/android/app/storycam/bgm/FearOfDarknessBGM;->totalDurationBGM:I

    .line 5
    return-void

    .line 7
    nop

    :array_0
    .array-data 4
        0x13ec
        0x2af8
        0x3e1c
        0x51a4
        0x64c8
    .end array-data

    .line 14
    :array_1
    .array-data 4
        0xed8
        0x1e78
        0x2e18
        0x3e1c
        0x4e20
        0x5dc0
        0x7080
    .end array-data

    .line 23
    :array_2
    .array-data 4
        0x834
        0x13ec
        0x1fa4
        0x2b5c
        0x3714
        0x45ec
        0x51a4
        0x5d5c
        0x6914
        0x74cc
    .end array-data
.end method


# virtual methods
.method protected getBGMFilepath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    const-string v0, "themedata/bgm/E1_Fear of Darkness.ogg"

    return-object v0
.end method

.method protected getBGMName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    const-string v0, "Fear of Darkness"

    return-object v0
.end method

.method protected getTotalDurationBGM()I
    .locals 1

    .prologue
    .line 52
    iget v0, p0, Lcom/sec/android/app/storycam/bgm/FearOfDarknessBGM;->totalDurationBGM:I

    return v0
.end method

.method protected getTransitionTimeStamps(Lcom/sec/android/app/ve/bgm/BGM$BGMModes;)[I
    .locals 2
    .param p1, "mode"    # Lcom/sec/android/app/ve/bgm/BGM$BGMModes;

    .prologue
    .line 38
    invoke-static {}, Lcom/sec/android/app/storycam/bgm/FearOfDarknessBGM;->$SWITCH_TABLE$com$sec$android$app$ve$bgm$BGM$BGMModes()[I

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/android/app/ve/bgm/BGM$BGMModes;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 46
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 40
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/storycam/bgm/FearOfDarknessBGM;->slowModeBGM:[I

    goto :goto_0

    .line 42
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/storycam/bgm/FearOfDarknessBGM;->mediumModeBGM:[I

    goto :goto_0

    .line 44
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/storycam/bgm/FearOfDarknessBGM;->fastModeBGM:[I

    goto :goto_0

    .line 38
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
