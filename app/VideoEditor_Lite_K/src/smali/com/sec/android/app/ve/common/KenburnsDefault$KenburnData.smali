.class public Lcom/sec/android/app/ve/common/KenburnsDefault$KenburnData;
.super Ljava/lang/Object;
.source "KenburnsDefault.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/ve/common/KenburnsDefault;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "KenburnData"
.end annotation


# instance fields
.field private lBitmap:Landroid/graphics/Bitmap;

.field private mEndMatrix:Landroid/graphics/Matrix;

.field private mEndRect:Landroid/graphics/RectF;

.field private mRefRect:Landroid/graphics/RectF;

.field private mStartMatrix:Landroid/graphics/Matrix;

.field private mStartRect:Landroid/graphics/RectF;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 520
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 576
    iget-object v0, p0, Lcom/sec/android/app/ve/common/KenburnsDefault$KenburnData;->lBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getEndMatrix()Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 568
    iget-object v0, p0, Lcom/sec/android/app/ve/common/KenburnsDefault$KenburnData;->mEndMatrix:Landroid/graphics/Matrix;

    return-object v0
.end method

.method public getEndRect()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 552
    iget-object v0, p0, Lcom/sec/android/app/ve/common/KenburnsDefault$KenburnData;->mEndRect:Landroid/graphics/RectF;

    return-object v0
.end method

.method public getRefRect()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 544
    iget-object v0, p0, Lcom/sec/android/app/ve/common/KenburnsDefault$KenburnData;->mRefRect:Landroid/graphics/RectF;

    return-object v0
.end method

.method public getStartMatrix()Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 560
    iget-object v0, p0, Lcom/sec/android/app/ve/common/KenburnsDefault$KenburnData;->mStartMatrix:Landroid/graphics/Matrix;

    return-object v0
.end method

.method public getStartRect()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 536
    iget-object v0, p0, Lcom/sec/android/app/ve/common/KenburnsDefault$KenburnData;->mStartRect:Landroid/graphics/RectF;

    return-object v0
.end method

.method public setBitmap(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "lBitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 572
    iput-object p1, p0, Lcom/sec/android/app/ve/common/KenburnsDefault$KenburnData;->lBitmap:Landroid/graphics/Bitmap;

    .line 573
    return-void
.end method

.method public setEndMatrix(Landroid/graphics/Matrix;)V
    .locals 0
    .param p1, "mEndMatrix"    # Landroid/graphics/Matrix;

    .prologue
    .line 564
    iput-object p1, p0, Lcom/sec/android/app/ve/common/KenburnsDefault$KenburnData;->mEndMatrix:Landroid/graphics/Matrix;

    .line 565
    return-void
.end method

.method public setEndRect(Landroid/graphics/RectF;)V
    .locals 0
    .param p1, "mEndRect"    # Landroid/graphics/RectF;

    .prologue
    .line 548
    iput-object p1, p0, Lcom/sec/android/app/ve/common/KenburnsDefault$KenburnData;->mEndRect:Landroid/graphics/RectF;

    .line 549
    return-void
.end method

.method public setRefRect(Landroid/graphics/RectF;)V
    .locals 0
    .param p1, "mRefRect"    # Landroid/graphics/RectF;

    .prologue
    .line 540
    iput-object p1, p0, Lcom/sec/android/app/ve/common/KenburnsDefault$KenburnData;->mRefRect:Landroid/graphics/RectF;

    .line 541
    return-void
.end method

.method public setStartMatrix(Landroid/graphics/Matrix;)V
    .locals 0
    .param p1, "mStartMatrix"    # Landroid/graphics/Matrix;

    .prologue
    .line 556
    iput-object p1, p0, Lcom/sec/android/app/ve/common/KenburnsDefault$KenburnData;->mStartMatrix:Landroid/graphics/Matrix;

    .line 557
    return-void
.end method

.method public setStartRect(Landroid/graphics/RectF;)V
    .locals 0
    .param p1, "mStartRect"    # Landroid/graphics/RectF;

    .prologue
    .line 532
    iput-object p1, p0, Lcom/sec/android/app/ve/common/KenburnsDefault$KenburnData;->mStartRect:Landroid/graphics/RectF;

    .line 533
    return-void
.end method
