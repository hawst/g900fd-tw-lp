.class public Lcom/sec/android/app/storycam/view/AutoScrollableHorizontalScrollView;
.super Landroid/widget/HorizontalScrollView;
.source "AutoScrollableHorizontalScrollView.java"


# instance fields
.field private mViewStartPos:I

.field private myScroller:Landroid/widget/OverScroller;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;)V

    .line 12
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/storycam/view/AutoScrollableHorizontalScrollView;->mViewStartPos:I

    .line 17
    invoke-direct {p0}, Lcom/sec/android/app/storycam/view/AutoScrollableHorizontalScrollView;->init()V

    .line 18
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 12
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/storycam/view/AutoScrollableHorizontalScrollView;->mViewStartPos:I

    .line 22
    invoke-direct {p0}, Lcom/sec/android/app/storycam/view/AutoScrollableHorizontalScrollView;->init()V

    .line 23
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 26
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 12
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/storycam/view/AutoScrollableHorizontalScrollView;->mViewStartPos:I

    .line 27
    invoke-direct {p0}, Lcom/sec/android/app/storycam/view/AutoScrollableHorizontalScrollView;->init()V

    .line 28
    return-void
.end method

.method private init()V
    .locals 5

    .prologue
    .line 42
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    .line 44
    .local v2, "parent":Ljava/lang/Class;
    :cond_0
    invoke-virtual {v2}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v2

    .line 45
    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "android.widget.HorizontalScrollView"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 47
    const-string v3, "mScroller"

    invoke-virtual {v2, v3}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 48
    .local v1, "field":Ljava/lang/reflect/Field;
    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 49
    invoke-virtual {v1, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/widget/OverScroller;

    iput-object v3, p0, Lcom/sec/android/app/storycam/view/AutoScrollableHorizontalScrollView;->myScroller:Landroid/widget/OverScroller;
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2

    .line 57
    .end local v1    # "field":Ljava/lang/reflect/Field;
    .end local v2    # "parent":Ljava/lang/Class;
    :goto_0
    return-void

    .line 50
    :catch_0
    move-exception v0

    .line 51
    .local v0, "e":Ljava/lang/NoSuchFieldException;
    invoke-virtual {v0}, Ljava/lang/NoSuchFieldException;->printStackTrace()V

    goto :goto_0

    .line 52
    .end local v0    # "e":Ljava/lang/NoSuchFieldException;
    :catch_1
    move-exception v0

    .line 53
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 54
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v0

    .line 55
    .local v0, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public customSmoothScrollBy(II)V
    .locals 7
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    const/4 v6, 0x0

    .line 60
    iget-object v4, p0, Lcom/sec/android/app/storycam/view/AutoScrollableHorizontalScrollView;->myScroller:Landroid/widget/OverScroller;

    if-nez v4, :cond_1

    .line 61
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/storycam/view/AutoScrollableHorizontalScrollView;->smoothScrollBy(II)V

    .line 73
    :cond_0
    :goto_0
    return-void

    .line 64
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/view/AutoScrollableHorizontalScrollView;->getChildCount()I

    move-result v4

    if-eqz v4, :cond_0

    .line 66
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/view/AutoScrollableHorizontalScrollView;->getWidth()I

    move-result v4

    invoke-virtual {p0}, Lcom/sec/android/app/storycam/view/AutoScrollableHorizontalScrollView;->getPaddingRight()I

    move-result v5

    sub-int/2addr v4, v5

    invoke-virtual {p0}, Lcom/sec/android/app/storycam/view/AutoScrollableHorizontalScrollView;->getPaddingLeft()I

    move-result v5

    sub-int v3, v4, v5

    .line 67
    .local v3, "width":I
    invoke-virtual {p0, v6}, Lcom/sec/android/app/storycam/view/AutoScrollableHorizontalScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    move-result v1

    .line 68
    .local v1, "right":I
    sub-int v4, v1, v3

    invoke-static {v6, v4}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 69
    .local v0, "maxX":I
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/view/AutoScrollableHorizontalScrollView;->getScrollX()I

    move-result v2

    .line 70
    .local v2, "scrollX":I
    add-int v4, v2, p1

    invoke-static {v4, v0}, Ljava/lang/Math;->min(II)I

    move-result v4

    invoke-static {v6, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    sub-int p1, v4, v2

    .line 71
    mul-int/lit8 v4, p1, 0x1e

    invoke-virtual {p0, v4, p2}, Lcom/sec/android/app/storycam/view/AutoScrollableHorizontalScrollView;->smoothScrollBy(II)V

    .line 72
    invoke-virtual {p0}, Lcom/sec/android/app/storycam/view/AutoScrollableHorizontalScrollView;->invalidate()V

    goto :goto_0
.end method

.method public getViewStartPosition()I
    .locals 1

    .prologue
    .line 36
    iget v0, p0, Lcom/sec/android/app/storycam/view/AutoScrollableHorizontalScrollView;->mViewStartPos:I

    return v0
.end method

.method protected onScrollChanged(IIII)V
    .locals 0
    .param p1, "l"    # I
    .param p2, "t"    # I
    .param p3, "oldl"    # I
    .param p4, "oldt"    # I

    .prologue
    .line 32
    iput p1, p0, Lcom/sec/android/app/storycam/view/AutoScrollableHorizontalScrollView;->mViewStartPos:I

    .line 33
    return-void
.end method
