.class Lcom/sec/android/app/ve/export/ExportService$1;
.super Lcom/sec/android/app/ve/export/ExportInterface$Adapter;
.source "ExportService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/ve/export/ExportService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/ve/export/ExportService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/ve/export/ExportService;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/ve/export/ExportService$1;->this$0:Lcom/sec/android/app/ve/export/ExportService;

    .line 221
    invoke-direct {p0}, Lcom/sec/android/app/ve/export/ExportInterface$Adapter;-><init>()V

    return-void
.end method


# virtual methods
.method public getCurrentTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;
    .locals 1

    .prologue
    .line 246
    const/4 v0, 0x0

    return-object v0
.end method

.method public onError()V
    .locals 4

    .prologue
    .line 240
    sget-object v0, Lcom/sec/android/app/ve/VEApp;->gExport:Lcom/sec/android/app/ve/export/Export;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/export/Export;->getHandler()Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x0

    const-wide/16 v2, 0xa

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 241
    return-void
.end method

.method public onExportCompleted()V
    .locals 4

    .prologue
    .line 228
    sget-object v0, Lcom/sec/android/app/ve/VEApp;->gExport:Lcom/sec/android/app/ve/export/Export;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/export/Export;->getHandler()Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x1

    const-wide/16 v2, 0xa

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 229
    return-void
.end method

.method public onExportPaused()V
    .locals 0

    .prologue
    .line 233
    return-void
.end method

.method public onExportStarted()V
    .locals 0

    .prologue
    .line 225
    return-void
.end method

.method public onExportStopped()V
    .locals 4

    .prologue
    .line 236
    sget-object v0, Lcom/sec/android/app/ve/VEApp;->gExport:Lcom/sec/android/app/ve/export/Export;

    invoke-virtual {v0}, Lcom/sec/android/app/ve/export/Export;->getHandler()Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x2

    const-wide/16 v2, 0xa

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 237
    return-void
.end method
