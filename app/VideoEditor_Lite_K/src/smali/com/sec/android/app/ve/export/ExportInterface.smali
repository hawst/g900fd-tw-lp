.class public Lcom/sec/android/app/ve/export/ExportInterface;
.super Ljava/lang/Object;
.source "ExportInterface.java"

# interfaces
.implements Lcom/sec/android/app/ve/PlayerInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/ve/export/ExportInterface$Adapter;,
        Lcom/sec/android/app/ve/export/ExportInterface$BackEndHandler;
    }
.end annotation


# static fields
.field private static final EXPORT_STATE_ERROR:I = -0x1

.field private static final EXPORT_STATE_IDLE:I = 0x0

.field private static final EXPORT_STATE_INIT:I = 0x1

.field private static final EXPORT_STATE_PAUSED:I = 0x5

.field private static final EXPORT_STATE_PLAYING:I = 0x2


# instance fields
.field private fileName:Ljava/lang/String;

.field private isCompleted:Z

.field public mAdapter:Lcom/sec/android/app/ve/export/ExportInterface$Adapter;

.field private mBackendHandler:Landroid/os/Handler;

.field private mCurrentState:I

.field private mEngineListener:Lcom/samsung/app/video/editor/external/NativeInterface$previewPlayerStateListener;

.field private mExpParams:Lcom/samsung/app/video/editor/external/TranscodeElement$ExportParameters;

.field private mLastPausedTime:I

.field private mNextState:I

.field private mStblzReqd:Z

.field private mTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

.field private wakelock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method public constructor <init>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput v4, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mCurrentState:I

    .line 49
    iput v4, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mNextState:I

    .line 50
    iput-boolean v4, p0, Lcom/sec/android/app/ve/export/ExportInterface;->isCompleted:Z

    .line 51
    iput v4, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mLastPausedTime:I

    .line 54
    new-instance v1, Lcom/sec/android/app/ve/export/ExportInterface$BackEndHandler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/sec/android/app/ve/export/ExportInterface$BackEndHandler;-><init>(Lcom/sec/android/app/ve/export/ExportInterface;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mBackendHandler:Landroid/os/Handler;

    .line 467
    new-instance v1, Lcom/sec/android/app/ve/export/ExportInterface$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/ve/export/ExportInterface$1;-><init>(Lcom/sec/android/app/ve/export/ExportInterface;)V

    iput-object v1, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mEngineListener:Lcom/samsung/app/video/editor/external/NativeInterface$previewPlayerStateListener;

    .line 61
    sget-object v1, Lcom/sec/android/app/ve/VEApp;->gExport:Lcom/sec/android/app/ve/export/Export;

    invoke-virtual {v1}, Lcom/sec/android/app/ve/export/Export;->getTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    .line 62
    sget-object v1, Lcom/sec/android/app/ve/VEApp;->gExport:Lcom/sec/android/app/ve/export/Export;

    invoke-virtual {v1}, Lcom/sec/android/app/ve/export/Export;->getFileName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/ve/export/ExportInterface;->fileName:Ljava/lang/String;

    .line 63
    sget-object v1, Lcom/sec/android/app/ve/VEApp;->gExport:Lcom/sec/android/app/ve/export/Export;

    invoke-virtual {v1}, Lcom/sec/android/app/ve/export/Export;->stabilizationRequired()Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mStblzReqd:Z

    .line 64
    new-instance v1, Lcom/samsung/app/video/editor/external/TranscodeElement$ExportParameters;

    invoke-direct {v1}, Lcom/samsung/app/video/editor/external/TranscodeElement$ExportParameters;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mExpParams:Lcom/samsung/app/video/editor/external/TranscodeElement$ExportParameters;

    .line 65
    iget-object v1, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mExpParams:Lcom/samsung/app/video/editor/external/TranscodeElement$ExportParameters;

    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/sec/android/app/ve/export/ExportInterface;->fileName:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, ".tmp"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/samsung/app/video/editor/external/TranscodeElement$ExportParameters;->outputFileName:Ljava/lang/String;

    .line 66
    iget-object v1, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mExpParams:Lcom/samsung/app/video/editor/external/TranscodeElement$ExportParameters;

    iget-boolean v2, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mStblzReqd:Z

    iput-boolean v2, v1, Lcom/samsung/app/video/editor/external/TranscodeElement$ExportParameters;->isStabilizeOn:Z

    .line 67
    iget-object v1, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mExpParams:Lcom/samsung/app/video/editor/external/TranscodeElement$ExportParameters;

    sget-object v2, Lcom/sec/android/app/ve/VEApp;->gExport:Lcom/sec/android/app/ve/export/Export;

    invoke-virtual {v2}, Lcom/sec/android/app/ve/export/Export;->isExportAfterAutoEdit()Z

    move-result v2

    iput-boolean v2, v1, Lcom/samsung/app/video/editor/external/TranscodeElement$ExportParameters;->isAfterAutoEdit:Z

    .line 68
    iget-object v1, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getResolutionEnumValue()I

    move-result v0

    .line 69
    .local v0, "expResEnumValue":I
    if-ltz v0, :cond_0

    sget-object v1, Lcom/samsung/app/video/editor/external/Constants;->EXPORT_RESOLUTIONS:[[I

    array-length v1, v1

    if-lt v0, v1, :cond_1

    .line 70
    :cond_0
    const/4 v0, 0x0

    .line 71
    const-string v1, "EXPORT_INTERFACE"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "<<<<<< Wrong Export resolution selected: setting to default resolution: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v3, Lcom/samsung/app/video/editor/external/Constants;->EXPORT_RESOLUTIONS:[[I

    aget-object v3, v3, v4

    aget v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "x"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 72
    sget-object v3, Lcom/samsung/app/video/editor/external/Constants;->EXPORT_RESOLUTIONS:[[I

    aget-object v3, v3, v4

    aget v3, v3, v5

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 71
    invoke-static {v1, v2}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mExpParams:Lcom/samsung/app/video/editor/external/TranscodeElement$ExportParameters;

    sget-object v2, Lcom/samsung/app/video/editor/external/Constants;->EXPORT_RESOLUTIONS:[[I

    aget-object v2, v2, v0

    aget v2, v2, v4

    iput v2, v1, Lcom/samsung/app/video/editor/external/TranscodeElement$ExportParameters;->exportWidth:I

    .line 75
    iget-object v1, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mExpParams:Lcom/samsung/app/video/editor/external/TranscodeElement$ExportParameters;

    sget-object v2, Lcom/samsung/app/video/editor/external/Constants;->EXPORT_RESOLUTIONS:[[I

    aget-object v2, v2, v0

    aget v2, v2, v5

    iput v2, v1, Lcom/samsung/app/video/editor/external/TranscodeElement$ExportParameters;->exportHeight:I

    .line 76
    return-void
.end method

.method private _pause()V
    .locals 3

    .prologue
    .line 236
    const-string v1, "EXPORT_PLAYER_STATE"

    const-string v2, "_pause"

    invoke-static {v1, v2}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    invoke-direct {p0}, Lcom/sec/android/app/ve/export/ExportInterface;->releaseWakeLock()V

    .line 238
    const/4 v1, 0x2

    iget v2, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mCurrentState:I

    if-ne v1, v2, :cond_0

    .line 241
    :try_start_0
    const-string v1, "EXPORT_PLAYER_STATE"

    const-string v2, "calling native _stop"

    invoke-static {v1, v2}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 242
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/NativeInterface;->pause()I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mLastPausedTime:I

    .line 243
    const/4 v1, 0x5

    iput v1, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mNextState:I

    .line 244
    invoke-direct {p0}, Lcom/sec/android/app/ve/export/ExportInterface;->handleExportPauseEventCompletion()V

    .line 245
    const-string v1, "PLAYER_STATE"

    const-string v2, "calling wait done"

    invoke-static {v1, v2}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 250
    :cond_0
    :goto_0
    return-void

    .line 246
    :catch_0
    move-exception v0

    .line 247
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private _play(J)V
    .locals 9
    .param p1, "pos"    # J

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 145
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/sec/android/app/ve/export/ExportInterface;->wakelock:Landroid/os/PowerManager$WakeLock;

    .line 147
    const/4 v5, 0x1

    :try_start_0
    new-array v1, v5, [Ljava/lang/String;

    .line 148
    .local v1, "path":[Ljava/lang/String;
    const/4 v5, 0x0

    new-instance v6, Ljava/lang/StringBuilder;

    iget-object v7, p0, Lcom/sec/android/app/ve/export/ExportInterface;->fileName:Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, ".tmp"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v1, v5

    .line 149
    invoke-direct {p0}, Lcom/sec/android/app/ve/export/ExportInterface;->acquireWakeLock()V

    .line 151
    iget-object v5, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    iget-object v6, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    invoke-virtual {v6}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getTotalDuration()J

    move-result-wide v6

    long-to-int v6, v6

    invoke-virtual {v5, v6}, Lcom/samsung/app/video/editor/external/TranscodeElement;->setFullMovieDuration(I)V

    .line 153
    const/4 v5, 0x1

    iput v5, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mNextState:I

    .line 154
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mEngineListener:Lcom/samsung/app/video/editor/external/NativeInterface$previewPlayerStateListener;

    invoke-virtual {v5, v6}, Lcom/samsung/app/video/editor/external/NativeInterface;->setStackStateListener(Lcom/samsung/app/video/editor/external/NativeInterface$previewPlayerStateListener;)V

    .line 155
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    iget-object v7, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mExpParams:Lcom/samsung/app/video/editor/external/TranscodeElement$ExportParameters;

    invoke-virtual {v5, v6, p1, p2, v7}, Lcom/samsung/app/video/editor/external/NativeInterface;->startVeEngine(Lcom/samsung/app/video/editor/external/TranscodeElement;JLcom/samsung/app/video/editor/external/TranscodeElement$ExportParameters;)I

    move-result v2

    .line 157
    .local v2, "play_started":I
    const-string v5, "SANTHOSH"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "********************Export Started? "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-lez v2, :cond_0

    :goto_0
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v5, v3}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    if-gtz v2, :cond_1

    .line 159
    const/4 v3, -0x1

    iput v3, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mNextState:I

    .line 164
    :goto_1
    invoke-direct {p0}, Lcom/sec/android/app/ve/export/ExportInterface;->handleExportPlayEventCompletion()V

    .line 186
    .end local v1    # "path":[Ljava/lang/String;
    .end local v2    # "play_started":I
    :goto_2
    return-void

    .restart local v1    # "path":[Ljava/lang/String;
    .restart local v2    # "play_started":I
    :cond_0
    move v3, v4

    .line 157
    goto :goto_0

    .line 162
    :cond_1
    const/4 v3, 0x2

    iput v3, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mNextState:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 183
    .end local v1    # "path":[Ljava/lang/String;
    .end local v2    # "play_started":I
    :catch_0
    move-exception v0

    .line 184
    .local v0, "ex":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2
.end method

.method private _resume(J)V
    .locals 5
    .param p1, "pos"    # J

    .prologue
    const/4 v1, 0x1

    .line 281
    const/4 v2, 0x5

    iget v3, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mCurrentState:I

    if-ne v2, v3, :cond_0

    .line 282
    invoke-direct {p0}, Lcom/sec/android/app/ve/export/ExportInterface;->acquireWakeLock()V

    .line 283
    iput v1, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mNextState:I

    .line 284
    const-string v2, "SANTHOSH"

    const-string v3, "*****************going to call resume: "

    invoke-static {v2, v3}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v2

    long-to-int v3, p1

    invoke-virtual {v2, v3}, Lcom/samsung/app/video/editor/external/NativeInterface;->resume(I)I

    move-result v0

    .line 286
    .local v0, "resumed":I
    const-string v2, "SANTHOSH"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "********************Export resume? "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-ltz v0, :cond_1

    :goto_0
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 287
    if-gez v0, :cond_2

    .line 288
    const/4 v1, -0x1

    iput v1, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mNextState:I

    .line 293
    :goto_1
    invoke-direct {p0}, Lcom/sec/android/app/ve/export/ExportInterface;->handleExportResumeEventCompletion()V

    .line 295
    .end local v0    # "resumed":I
    :cond_0
    return-void

    .line 286
    .restart local v0    # "resumed":I
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 291
    :cond_2
    const/4 v1, 0x2

    iput v1, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mNextState:I

    goto :goto_1
.end method

.method private _stop(Z)V
    .locals 3
    .param p1, "force"    # Z

    .prologue
    .line 339
    const-string v1, "EXPORT_PLAYER_STATE"

    const-string v2, "_stop"

    invoke-static {v1, v2}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 340
    invoke-direct {p0}, Lcom/sec/android/app/ve/export/ExportInterface;->releaseWakeLock()V

    .line 341
    if-nez p1, :cond_0

    const/4 v1, 0x2

    iget v2, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mCurrentState:I

    if-eq v1, v2, :cond_0

    const/4 v1, 0x5

    iget v2, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mCurrentState:I

    if-ne v1, v2, :cond_1

    .line 344
    :cond_0
    :try_start_0
    const-string v1, "EXPORT_PLAYER_STATE"

    const-string v2, "calling native _stop"

    invoke-static {v1, v2}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 345
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/NativeInterface;->stopVeEngine()V

    .line 346
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mNextState:I

    .line 347
    invoke-direct {p0}, Lcom/sec/android/app/ve/export/ExportInterface;->handleExportStopEventCompletion()V

    .line 348
    const-string v1, "PLAYER_STATE"

    const-string v2, "calling wait done"

    invoke-static {v1, v2}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 353
    :cond_1
    :goto_0
    return-void

    .line 349
    :catch_0
    move-exception v0

    .line 350
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic access$0(Lcom/sec/android/app/ve/export/ExportInterface;)I
    .locals 1

    .prologue
    .line 49
    iget v0, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mNextState:I

    return v0
.end method

.method static synthetic access$1(Lcom/sec/android/app/ve/export/ExportInterface;)I
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mCurrentState:I

    return v0
.end method

.method static synthetic access$2(Lcom/sec/android/app/ve/export/ExportInterface;I)V
    .locals 0

    .prologue
    .line 49
    iput p1, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mNextState:I

    return-void
.end method

.method static synthetic access$3(Lcom/sec/android/app/ve/export/ExportInterface;)V
    .locals 0

    .prologue
    .line 406
    invoke-direct {p0}, Lcom/sec/android/app/ve/export/ExportInterface;->handleExportTimeError()V

    return-void
.end method

.method static synthetic access$4(Lcom/sec/android/app/ve/export/ExportInterface;Z)V
    .locals 0

    .prologue
    .line 50
    iput-boolean p1, p0, Lcom/sec/android/app/ve/export/ExportInterface;->isCompleted:Z

    return-void
.end method

.method static synthetic access$5(Lcom/sec/android/app/ve/export/ExportInterface;)V
    .locals 0

    .prologue
    .line 394
    invoke-direct {p0}, Lcom/sec/android/app/ve/export/ExportInterface;->handleExportCompletedEvent()V

    return-void
.end method

.method static synthetic access$6(Lcom/sec/android/app/ve/export/ExportInterface;Z)V
    .locals 0

    .prologue
    .line 337
    invoke-direct {p0, p1}, Lcom/sec/android/app/ve/export/ExportInterface;->_stop(Z)V

    return-void
.end method

.method private acquireWakeLock()V
    .locals 3

    .prologue
    .line 110
    iget-object v0, p0, Lcom/sec/android/app/ve/export/ExportInterface;->wakelock:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/ve/export/ExportInterface;->wakelock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-nez v0, :cond_1

    .line 111
    :cond_0
    const-string v0, "power"

    invoke-static {v0}, Lcom/sec/android/app/ve/VEApp;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    const v1, 0x2000000a

    const-string v2, "Video Editor - New Export Interface"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/ve/export/ExportInterface;->wakelock:Landroid/os/PowerManager$WakeLock;

    .line 112
    const-string v0, "PLAYER_STATE"

    const-string v1, "acquiring"

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    iget-object v0, p0, Lcom/sec/android/app/ve/export/ExportInterface;->wakelock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 115
    :cond_1
    return-void
.end method

.method private handleExportCompletedEvent()V
    .locals 2

    .prologue
    .line 395
    iget-object v0, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mBackendHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/ve/export/ExportInterface$9;

    invoke-direct {v1, p0}, Lcom/sec/android/app/ve/export/ExportInterface$9;-><init>(Lcom/sec/android/app/ve/export/ExportInterface;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 401
    return-void
.end method

.method private handleExportPauseEventCompletion()V
    .locals 4

    .prologue
    const/4 v3, 0x5

    .line 253
    const-string v0, "EXPORT_PLAYER_STATE"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "handleExportPauseEventCompletion ==> NextState = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mNextState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " CurrentState = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mCurrentState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 254
    iget v0, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mNextState:I

    if-ne v0, v3, :cond_0

    .line 255
    iget-object v0, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mBackendHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/ve/export/ExportInterface$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/ve/export/ExportInterface$4;-><init>(Lcom/sec/android/app/ve/export/ExportInterface;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 265
    :cond_0
    iput v3, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mCurrentState:I

    iput v3, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mNextState:I

    .line 266
    const-string v0, "EXPORT_PLAYER_STATE"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "handleExportPauseEventCompletion ==> NextState = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mNextState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " CurrentState = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mCurrentState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 267
    return-void
.end method

.method private handleExportPlayEventCompletion()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 192
    const-string v0, "EXPORT_PLAYER_STATE"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "handlePlayEventCompletion ==> NextState = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mNextState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " CurrentState = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mCurrentState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    iget v0, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mCurrentState:I

    if-nez v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mNextState:I

    if-nez v0, :cond_0

    .line 195
    const-string v0, "PLAYER_STATE"

    const-string v1, "handlePlayEventCompletion ==> The error happend on App side before start of playback"

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    :goto_0
    return-void

    .line 198
    :cond_0
    iget v0, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mNextState:I

    if-ne v0, v3, :cond_2

    .line 200
    iput v3, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mCurrentState:I

    .line 201
    iget-object v0, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mBackendHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/ve/export/ExportInterface$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/ve/export/ExportInterface$2;-><init>(Lcom/sec/android/app/ve/export/ExportInterface;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 221
    :cond_1
    :goto_1
    const-string v0, "EXPORT_PLAYER_STATE"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "handlePlayEventCompletion ==> NextState = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mNextState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " CurrentState = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mCurrentState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 210
    :cond_2
    iget v0, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mNextState:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 211
    iget-object v0, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mBackendHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/ve/export/ExportInterface$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/ve/export/ExportInterface$3;-><init>(Lcom/sec/android/app/ve/export/ExportInterface;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 219
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mNextState:I

    iput v0, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mCurrentState:I

    goto :goto_1
.end method

.method private handleExportResumeEventCompletion()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 298
    const-string v0, "EXPORT_PLAYER_STATE"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "handleExportResumeEventCompletion ==> NextState = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mNextState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " CurrentState = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mCurrentState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 299
    iget v0, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mNextState:I

    if-ne v0, v3, :cond_1

    .line 301
    iput v3, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mCurrentState:I

    .line 302
    iget-object v0, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mBackendHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/ve/export/ExportInterface$5;

    invoke-direct {v1, p0}, Lcom/sec/android/app/ve/export/ExportInterface$5;-><init>(Lcom/sec/android/app/ve/export/ExportInterface;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 323
    :cond_0
    :goto_0
    const-string v0, "EXPORT_PLAYER_STATE"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "handleExportResumeEventCompletion ==> NextState = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mNextState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " CurrentState = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mCurrentState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 324
    return-void

    .line 312
    :cond_1
    iget v0, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mNextState:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 313
    iget-object v0, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mBackendHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/ve/export/ExportInterface$6;

    invoke-direct {v1, p0}, Lcom/sec/android/app/ve/export/ExportInterface$6;-><init>(Lcom/sec/android/app/ve/export/ExportInterface;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 321
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mCurrentState:I

    iput v0, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mNextState:I

    goto :goto_0
.end method

.method private handleExportStopEventCompletion()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 359
    const-string v0, "EXPORT_PLAYER_STATE"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "handleStopEventCompletion ==> NextState = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mNextState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " CurrentState = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mCurrentState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 360
    const-string v0, "EXPORT_PLAYER_STATE"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "handleStopEventCompletion ==> isCompleted = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/sec/android/app/ve/export/ExportInterface;->isCompleted:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 361
    iget v0, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mNextState:I

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/ve/export/ExportInterface;->isCompleted:Z

    if-eqz v0, :cond_0

    .line 363
    iput-boolean v3, p0, Lcom/sec/android/app/ve/export/ExportInterface;->isCompleted:Z

    .line 364
    iget-object v0, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mBackendHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/ve/export/ExportInterface$7;

    invoke-direct {v1, p0}, Lcom/sec/android/app/ve/export/ExportInterface$7;-><init>(Lcom/sec/android/app/ve/export/ExportInterface;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 386
    :goto_0
    iput v3, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mCurrentState:I

    iput v3, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mNextState:I

    .line 387
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/app/video/editor/external/NativeInterface;->setStackStateListener(Lcom/samsung/app/video/editor/external/NativeInterface$previewPlayerStateListener;)V

    .line 388
    const-string v0, "EXPORT_PLAYER_STATE"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "handleStopEventCompletion ==> NextState = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mNextState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " CurrentState = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mCurrentState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 389
    return-void

    .line 375
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mBackendHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/ve/export/ExportInterface$8;

    invoke-direct {v1, p0}, Lcom/sec/android/app/ve/export/ExportInterface$8;-><init>(Lcom/sec/android/app/ve/export/ExportInterface;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method private handleExportTimeError()V
    .locals 2

    .prologue
    .line 407
    iget-object v0, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mBackendHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/ve/export/ExportInterface$10;

    invoke-direct {v1, p0}, Lcom/sec/android/app/ve/export/ExportInterface$10;-><init>(Lcom/sec/android/app/ve/export/ExportInterface;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 414
    return-void
.end method

.method private releaseWakeLock()V
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/sec/android/app/ve/export/ExportInterface;->wakelock:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/ve/export/ExportInterface;->wakelock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 119
    iget-object v0, p0, Lcom/sec/android/app/ve/export/ExportInterface;->wakelock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 122
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/ve/export/ExportInterface;->wakelock:Landroid/os/PowerManager$WakeLock;

    .line 123
    return-void
.end method


# virtual methods
.method public getExportProgress()I
    .locals 6

    .prologue
    .line 126
    iget-object v4, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getTotalDuration()J

    move-result-wide v2

    .line 127
    .local v2, "totalDuration":J
    iget v4, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mCurrentState:I

    const/4 v5, 0x5

    if-ne v4, v5, :cond_0

    iget v0, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mLastPausedTime:I

    .line 128
    .local v0, "exportedDuration":I
    :goto_0
    int-to-float v4, v0

    const/high16 v5, 0x42c80000    # 100.0f

    mul-float/2addr v4, v5

    long-to-float v5, v2

    div-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 129
    .local v1, "percentageCompleted":I
    return v1

    .line 127
    .end local v0    # "exportedDuration":I
    .end local v1    # "percentageCompleted":I
    :cond_0
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/NativeInterface;->getCurrentPosition()I

    move-result v0

    goto :goto_0
.end method

.method public getFileName()Ljava/lang/String;
    .locals 4

    .prologue
    .line 463
    iget-object v1, p0, Lcom/sec/android/app/ve/export/ExportInterface;->fileName:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/app/ve/export/ExportInterface;->fileName:Ljava/lang/String;

    const/16 v3, 0x2f

    invoke-virtual {v2, v3}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 464
    .local v0, "fName":Ljava/lang/String;
    return-object v0
.end method

.method public getMaxVolume()I
    .locals 1

    .prologue
    .line 440
    const/4 v0, 0x0

    return v0
.end method

.method public getVolume()I
    .locals 1

    .prologue
    .line 431
    const/4 v0, 0x0

    return v0
.end method

.method public isPlaying()Z
    .locals 2

    .prologue
    .line 458
    iget v0, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mCurrentState:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mCurrentState:I

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public pause()V
    .locals 2

    .prologue
    .line 231
    iget v0, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mCurrentState:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 232
    invoke-direct {p0}, Lcom/sec/android/app/ve/export/ExportInterface;->_pause()V

    .line 233
    :cond_0
    return-void
.end method

.method public play(J)V
    .locals 1
    .param p1, "pos"    # J

    .prologue
    .line 138
    iget v0, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mCurrentState:I

    if-nez v0, :cond_0

    .line 139
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/ve/export/ExportInterface;->_play(J)V

    .line 141
    :cond_0
    return-void
.end method

.method public resume(J)V
    .locals 2
    .param p1, "pos"    # J

    .prologue
    .line 276
    iget v0, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mCurrentState:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    .line 277
    iget v0, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mLastPausedTime:I

    int-to-long v0, v0

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/ve/export/ExportInterface;->_resume(J)V

    .line 278
    :cond_0
    return-void
.end method

.method public seekTo(J)V
    .locals 0
    .param p1, "pos"    # J

    .prologue
    .line 423
    return-void
.end method

.method public setActivity(Landroid/app/Activity;)V
    .locals 0
    .param p1, "actvt"    # Landroid/app/Activity;

    .prologue
    .line 107
    return-void
.end method

.method public setAdapter(Lcom/sec/android/app/ve/export/ExportInterface$Adapter;)V
    .locals 0
    .param p1, "adapter"    # Lcom/sec/android/app/ve/export/ExportInterface$Adapter;

    .prologue
    .line 79
    iput-object p1, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mAdapter:Lcom/sec/android/app/ve/export/ExportInterface$Adapter;

    .line 80
    return-void
.end method

.method public setDataSource(Landroid/net/Uri;)V
    .locals 0
    .param p1, "videoURI"    # Landroid/net/Uri;

    .prologue
    .line 98
    return-void
.end method

.method public setDataSource(Ljava/lang/String;)V
    .locals 0
    .param p1, "videoPath"    # Ljava/lang/String;

    .prologue
    .line 89
    return-void
.end method

.method public setVolume(I)V
    .locals 0
    .param p1, "volume"    # I

    .prologue
    .line 450
    return-void
.end method

.method public stop()V
    .locals 2

    .prologue
    .line 332
    iget v0, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mCurrentState:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/sec/android/app/ve/export/ExportInterface;->mCurrentState:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_1

    .line 333
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/app/ve/export/ExportInterface;->_stop(Z)V

    .line 335
    :cond_1
    return-void
.end method
