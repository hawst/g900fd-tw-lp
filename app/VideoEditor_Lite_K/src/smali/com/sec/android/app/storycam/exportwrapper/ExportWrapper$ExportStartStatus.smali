.class public final enum Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;
.super Ljava/lang/Enum;
.source "ExportWrapper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ExportStartStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum DIRECTORY_CREATION_FAILED:Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;

.field private static final synthetic ENUM$VALUES:[Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;

.field public static final enum EXPORT_ALREADY_RUNNING:Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;

.field public static final enum EXPORT_NO_MEMORY:Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;

.field public static final enum EXPORT_START_SUCCESS:Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;

.field public static final enum FILENAME_EXISTS:Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;

.field public static final enum FILENAME_NULL:Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 40
    new-instance v0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;

    const-string v1, "EXPORT_START_SUCCESS"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;->EXPORT_START_SUCCESS:Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;

    .line 41
    new-instance v0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;

    const-string v1, "EXPORT_NO_MEMORY"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;->EXPORT_NO_MEMORY:Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;

    .line 42
    new-instance v0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;

    const-string v1, "EXPORT_ALREADY_RUNNING"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;->EXPORT_ALREADY_RUNNING:Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;

    .line 43
    new-instance v0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;

    const-string v1, "FILENAME_NULL"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;->FILENAME_NULL:Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;

    .line 44
    new-instance v0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;

    const-string v1, "FILENAME_EXISTS"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;->FILENAME_EXISTS:Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;

    .line 45
    new-instance v0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;

    const-string v1, "DIRECTORY_CREATION_FAILED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;->DIRECTORY_CREATION_FAILED:Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;

    .line 39
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;

    sget-object v1, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;->EXPORT_START_SUCCESS:Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;->EXPORT_NO_MEMORY:Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;->EXPORT_ALREADY_RUNNING:Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;->FILENAME_NULL:Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;->FILENAME_EXISTS:Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;->DIRECTORY_CREATION_FAILED:Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;->ENUM$VALUES:[Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;->ENUM$VALUES:[Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;

    array-length v1, v0

    new-array v2, v1, [Lcom/sec/android/app/storycam/exportwrapper/ExportWrapper$ExportStartStatus;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
