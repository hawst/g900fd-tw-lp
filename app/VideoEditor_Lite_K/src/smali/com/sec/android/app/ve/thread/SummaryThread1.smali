.class public Lcom/sec/android/app/ve/thread/SummaryThread1;
.super Ljava/lang/Thread;
.source "SummaryThread1.java"


# static fields
.field public static final MSG_DELAY_SUMMARY_WAKELOCK:I = 0x7d

.field public static final MSG_SUMMARY_FAILURE:I = 0x7a

.field public static final MSG_SUMMARY_STOPPED:I = 0x7c

.field public static final MSG_SUMMARY_SUCCESS:I = 0x7b

.field public static final SUMMARY_ANALYSIS_SUCCESS:I = 0x1

.field static final SUMMARY_ERROR:I = 0x1

.field static final SUMMARY_SUCCESS:I = 0x0

.field public static final VIDEO_SUMMARY:I = 0x3eb


# instance fields
.field private analysisRequired:Z

.field private handler:Landroid/os/Handler;

.field private mContext:Landroid/content/Context;

.field private mCurrentTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

.field private mForceStop:Z

.field mHandler:Landroid/os/Handler;

.field mSummaryDuration:J

.field private mode:I

.field private path:[Ljava/lang/String;

.field private summaryTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

.field wakelock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method public constructor <init>(Landroid/os/Handler;ILandroid/content/Context;Lcom/samsung/app/video/editor/external/TranscodeElement;JLandroid/os/Bundle;ILcom/samsung/app/video/editor/external/TranscodeElement;Z)V
    .locals 3
    .param p1, "handler"    # Landroid/os/Handler;
    .param p2, "operation"    # I
    .param p3, "context"    # Landroid/content/Context;
    .param p4, "transcodeElement"    # Lcom/samsung/app/video/editor/external/TranscodeElement;
    .param p5, "videoSummaryTime"    # J
    .param p7, "aBundle"    # Landroid/os/Bundle;
    .param p8, "mode"    # I
    .param p9, "output"    # Lcom/samsung/app/video/editor/external/TranscodeElement;
    .param p10, "analysis"    # Z

    .prologue
    const/4 v1, 0x1

    .line 45
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 30
    new-array v0, v1, [Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/ve/thread/SummaryThread1;->path:[Ljava/lang/String;

    .line 36
    iput-boolean v1, p0, Lcom/sec/android/app/ve/thread/SummaryThread1;->analysisRequired:Z

    .line 301
    new-instance v0, Lcom/sec/android/app/ve/thread/SummaryThread1$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/ve/thread/SummaryThread1$1;-><init>(Lcom/sec/android/app/ve/thread/SummaryThread1;)V

    iput-object v0, p0, Lcom/sec/android/app/ve/thread/SummaryThread1;->handler:Landroid/os/Handler;

    .line 48
    iput-object p1, p0, Lcom/sec/android/app/ve/thread/SummaryThread1;->mHandler:Landroid/os/Handler;

    .line 49
    iput-object p3, p0, Lcom/sec/android/app/ve/thread/SummaryThread1;->mContext:Landroid/content/Context;

    .line 50
    iput-object p4, p0, Lcom/sec/android/app/ve/thread/SummaryThread1;->mCurrentTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    .line 51
    iput-wide p5, p0, Lcom/sec/android/app/ve/thread/SummaryThread1;->mSummaryDuration:J

    .line 52
    iput p8, p0, Lcom/sec/android/app/ve/thread/SummaryThread1;->mode:I

    .line 53
    iput-object p9, p0, Lcom/sec/android/app/ve/thread/SummaryThread1;->summaryTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    .line 54
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/ve/thread/SummaryThread1;->mForceStop:Z

    .line 55
    iput-boolean p10, p0, Lcom/sec/android/app/ve/thread/SummaryThread1;->analysisRequired:Z

    .line 56
    return-void
.end method

.method private createNoneThemeSummaryProject()Z
    .locals 20

    .prologue
    .line 124
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/ve/thread/SummaryThread1;->mCurrentTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    .line 125
    .local v4, "oldTranscodeElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    const/4 v11, 0x0

    .line 126
    .local v11, "elem":Lcom/samsung/app/video/editor/external/Element;
    const/4 v9, 0x0

    .line 127
    .local v9, "edit":Lcom/samsung/app/video/editor/external/Edit;
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v3

    .line 128
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/ve/thread/SummaryThread1;->summaryTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    .line 129
    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/sec/android/app/ve/thread/SummaryThread1;->mSummaryDuration:J

    long-to-double v6, v6

    move-object/from16 v0, p0

    iget v8, v0, Lcom/sec/android/app/ve/thread/SummaryThread1;->mode:I

    .line 128
    invoke-virtual/range {v3 .. v8}, Lcom/samsung/app/video/editor/external/NativeInterface;->createSummaryFromEngine(Lcom/samsung/app/video/editor/external/TranscodeElement;Lcom/samsung/app/video/editor/external/TranscodeElement;DI)I

    move-result v16

    .line 131
    .local v16, "outProject":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/ve/thread/SummaryThread1;->summaryTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Lcom/samsung/app/video/editor/external/TranscodeElement;->setSummaryProject(Z)V

    .line 133
    if-nez v16, :cond_0

    .line 134
    const/4 v3, 0x0

    .line 200
    :goto_0
    return v3

    .line 137
    :cond_0
    const/4 v12, 0x0

    .line 141
    .local v12, "i":I
    :goto_1
    new-instance v11, Lcom/samsung/app/video/editor/external/Element;

    .end local v11    # "elem":Lcom/samsung/app/video/editor/external/Element;
    invoke-direct {v11}, Lcom/samsung/app/video/editor/external/Element;-><init>()V

    .line 142
    .restart local v11    # "elem":Lcom/samsung/app/video/editor/external/Element;
    const/4 v3, 0x0

    iput-object v3, v11, Lcom/samsung/app/video/editor/external/Element;->filePath:Ljava/lang/String;

    .line 144
    new-instance v17, Lcom/samsung/app/video/editor/external/Edit;

    invoke-direct/range {v17 .. v17}, Lcom/samsung/app/video/editor/external/Edit;-><init>()V

    .line 145
    .local v17, "volumeEdit":Lcom/samsung/app/video/editor/external/Edit;
    const/4 v3, 0x1

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Lcom/samsung/app/video/editor/external/Edit;->setType(I)V

    .line 146
    const/4 v3, -0x1

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Lcom/samsung/app/video/editor/external/Edit;->setVolumeLevel(I)V

    .line 147
    move-object/from16 v0, v17

    invoke-virtual {v11, v0}, Lcom/samsung/app/video/editor/external/Element;->addEdit(Lcom/samsung/app/video/editor/external/Edit;)V

    .line 150
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v3

    move/from16 v0, v16

    invoke-virtual {v3, v11, v0, v12}, Lcom/samsung/app/video/editor/external/NativeInterface;->_getSummaryElementInElementListAt(Lcom/samsung/app/video/editor/external/Element;II)I

    .line 152
    iget-object v3, v11, Lcom/samsung/app/video/editor/external/Element;->filePath:Ljava/lang/String;

    if-nez v3, :cond_2

    .line 184
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v3

    move/from16 v0, v16

    invoke-virtual {v3, v0}, Lcom/samsung/app/video/editor/external/NativeInterface;->_freeSummaryOutput(I)I

    .line 187
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/ve/thread/SummaryThread1;->summaryTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementList()Ljava/util/List;

    move-result-object v13

    .line 189
    .local v13, "list":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    if-eqz v13, :cond_1

    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_1

    .line 191
    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-interface {v13, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/samsung/app/video/editor/external/Element;

    .line 192
    .local v10, "ele":Lcom/samsung/app/video/editor/external/Element;
    if-eqz v10, :cond_1

    .line 193
    invoke-virtual {v10}, Lcom/samsung/app/video/editor/external/Element;->removetransitionEdit()V

    .line 197
    .end local v10    # "ele":Lcom/samsung/app/video/editor/external/Element;
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/ve/thread/SummaryThread1;->summaryTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getThemeName()I

    move-result v5

    invoke-virtual {v3, v5}, Lcom/samsung/app/video/editor/external/TranscodeElement;->setThemeName(I)V

    .line 198
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/ve/thread/SummaryThread1;->summaryTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/TranscodeElement;->overlapTransitionsHandling()V

    .line 199
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/ve/thread/SummaryThread1;->summaryTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4}, Lcom/sec/android/app/ve/thread/SummaryThread1;->updateTimingInfoForGroup(Lcom/samsung/app/video/editor/external/TranscodeElement;Lcom/samsung/app/video/editor/external/TranscodeElement;)V

    .line 200
    const/4 v3, 0x1

    goto :goto_0

    .line 155
    .end local v13    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    :cond_2
    add-int/lit8 v12, v12, 0x1

    .line 158
    invoke-virtual {v11}, Lcom/samsung/app/video/editor/external/Element;->getEndTime()J

    move-result-wide v6

    invoke-virtual {v11}, Lcom/samsung/app/video/editor/external/Element;->getStartTime()J

    move-result-wide v18

    cmp-long v3, v6, v18

    if-gtz v3, :cond_3

    .line 159
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/ve/thread/SummaryThread1;->summaryTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v11}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 163
    :cond_3
    invoke-virtual {v11}, Lcom/samsung/app/video/editor/external/Element;->getEndTime()J

    move-result-wide v6

    invoke-virtual {v11}, Lcom/samsung/app/video/editor/external/Element;->getStartTime()J

    move-result-wide v18

    sub-long v6, v6, v18

    invoke-virtual {v11, v6, v7}, Lcom/samsung/app/video/editor/external/Element;->setDuration(J)V

    .line 164
    const/4 v3, 0x1

    invoke-virtual {v11, v3}, Lcom/samsung/app/video/editor/external/Element;->setAutoEdited(Z)V

    .line 165
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/ve/thread/SummaryThread1;->summaryTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    const/4 v5, 0x0

    invoke-virtual {v3, v11, v5}, Lcom/samsung/app/video/editor/external/TranscodeElement;->addElement(Lcom/samsung/app/video/editor/external/Element;Z)V

    .line 168
    invoke-virtual {v11}, Lcom/samsung/app/video/editor/external/Element;->getGroupID()I

    move-result v3

    invoke-virtual {v4, v3}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementWithID(I)Lcom/samsung/app/video/editor/external/Element;

    move-result-object v15

    .line 169
    .local v15, "originalEle":Lcom/samsung/app/video/editor/external/Element;
    if-eqz v15, :cond_4

    .line 171
    const/4 v3, 0x1

    invoke-virtual {v15, v3}, Lcom/samsung/app/video/editor/external/Element;->getEdit(I)Lcom/samsung/app/video/editor/external/Edit;

    move-result-object v2

    .line 172
    .local v2, "curEdit":Lcom/samsung/app/video/editor/external/Edit;
    if-eqz v2, :cond_4

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/app/video/editor/external/Edit;->getVolumeLevel()I

    move-result v3

    const/4 v5, -0x1

    if-ne v3, v5, :cond_4

    .line 173
    new-instance v14, Lcom/samsung/app/video/editor/external/Edit;

    const/4 v3, 0x1

    invoke-virtual {v15, v3}, Lcom/samsung/app/video/editor/external/Element;->getEdit(I)Lcom/samsung/app/video/editor/external/Edit;

    move-result-object v3

    invoke-direct {v14, v3}, Lcom/samsung/app/video/editor/external/Edit;-><init>(Lcom/samsung/app/video/editor/external/Edit;)V

    .line 174
    .local v14, "newEdit":Lcom/samsung/app/video/editor/external/Edit;
    invoke-virtual {v11, v14}, Lcom/samsung/app/video/editor/external/Element;->addEdit(Lcom/samsung/app/video/editor/external/Edit;)V

    .line 178
    .end local v2    # "curEdit":Lcom/samsung/app/video/editor/external/Edit;
    .end local v14    # "newEdit":Lcom/samsung/app/video/editor/external/Edit;
    :cond_4
    new-instance v9, Lcom/samsung/app/video/editor/external/Edit;

    .end local v9    # "edit":Lcom/samsung/app/video/editor/external/Edit;
    invoke-direct {v9}, Lcom/samsung/app/video/editor/external/Edit;-><init>()V

    .line 179
    .restart local v9    # "edit":Lcom/samsung/app/video/editor/external/Edit;
    const/4 v3, 0x6

    invoke-virtual {v9, v3}, Lcom/samsung/app/video/editor/external/Edit;->setType(I)V

    .line 180
    const/4 v3, 0x0

    invoke-virtual {v9, v3}, Lcom/samsung/app/video/editor/external/Edit;->setTrans_duration(I)V

    .line 181
    const/4 v3, 0x0

    invoke-virtual {v9, v3}, Lcom/samsung/app/video/editor/external/Edit;->setSubType(I)V

    .line 182
    invoke-virtual {v11, v9}, Lcom/samsung/app/video/editor/external/Element;->addEdit(Lcom/samsung/app/video/editor/external/Edit;)V

    goto/16 :goto_1
.end method

.method private createThemeSummaryProject()Z
    .locals 22

    .prologue
    .line 206
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/ve/thread/SummaryThread1;->mCurrentTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    .line 207
    .local v4, "oldTranscodeElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    const/4 v12, 0x0

    .line 208
    .local v12, "elem":Lcom/samsung/app/video/editor/external/Element;
    const/4 v10, 0x0

    .line 209
    .local v10, "edit":Lcom/samsung/app/video/editor/external/Edit;
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v3

    .line 210
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/ve/thread/SummaryThread1;->summaryTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    .line 211
    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/sec/android/app/ve/thread/SummaryThread1;->mSummaryDuration:J

    long-to-double v6, v6

    move-object/from16 v0, p0

    iget v8, v0, Lcom/sec/android/app/ve/thread/SummaryThread1;->mode:I

    .line 210
    invoke-virtual/range {v3 .. v8}, Lcom/samsung/app/video/editor/external/NativeInterface;->createSummaryFromEngine(Lcom/samsung/app/video/editor/external/TranscodeElement;Lcom/samsung/app/video/editor/external/TranscodeElement;DI)I

    move-result v17

    .line 213
    .local v17, "outProject":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/ve/thread/SummaryThread1;->summaryTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Lcom/samsung/app/video/editor/external/TranscodeElement;->setSummaryProject(Z)V

    .line 215
    if-nez v17, :cond_0

    .line 216
    const/4 v3, 0x0

    .line 266
    :goto_0
    return v3

    .line 219
    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/ve/thread/SummaryThread1;->summaryTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v2

    .line 220
    .local v2, "count":I
    const/4 v13, 0x0

    .local v13, "k":I
    :goto_1
    if-lt v13, v2, :cond_2

    .line 263
    :cond_1
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v3

    move/from16 v0, v17

    invoke-virtual {v3, v0}, Lcom/samsung/app/video/editor/external/NativeInterface;->_freeSummaryOutput(I)I

    .line 264
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/ve/thread/SummaryThread1;->summaryTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4}, Lcom/sec/android/app/ve/thread/SummaryThread1;->updateTimingInfoForGroup(Lcom/samsung/app/video/editor/external/TranscodeElement;Lcom/samsung/app/video/editor/external/TranscodeElement;)V

    .line 265
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/ve/thread/SummaryThread1;->summaryTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/TranscodeElement;->overlapTransitionsHandling()V

    .line 266
    const/4 v3, 0x1

    goto :goto_0

    .line 222
    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/ve/thread/SummaryThread1;->summaryTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    invoke-virtual {v3, v13}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElement(I)Lcom/samsung/app/video/editor/external/Element;

    move-result-object v12

    .line 223
    const/4 v3, 0x0

    iput-object v3, v12, Lcom/samsung/app/video/editor/external/Element;->filePath:Ljava/lang/String;

    .line 225
    new-instance v18, Lcom/samsung/app/video/editor/external/Edit;

    invoke-direct/range {v18 .. v18}, Lcom/samsung/app/video/editor/external/Edit;-><init>()V

    .line 226
    .local v18, "volumeEdit":Lcom/samsung/app/video/editor/external/Edit;
    const/4 v3, 0x1

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Lcom/samsung/app/video/editor/external/Edit;->setType(I)V

    .line 227
    const/4 v3, -0x1

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Lcom/samsung/app/video/editor/external/Edit;->setVolumeLevel(I)V

    .line 228
    move-object/from16 v0, v18

    invoke-virtual {v12, v0}, Lcom/samsung/app/video/editor/external/Element;->addEdit(Lcom/samsung/app/video/editor/external/Edit;)V

    .line 230
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v3

    move/from16 v0, v17

    invoke-virtual {v3, v12, v0, v13}, Lcom/samsung/app/video/editor/external/NativeInterface;->_getSummaryElementInElementListAt(Lcom/samsung/app/video/editor/external/Element;II)I

    .line 232
    iget-object v3, v12, Lcom/samsung/app/video/editor/external/Element;->filePath:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 233
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/ve/thread/SummaryThread1;->summaryTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v11

    .line 234
    .local v11, "eleCount":I
    move v14, v13

    .local v14, "l":I
    :goto_2
    if-ge v13, v11, :cond_1

    .line 235
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/ve/thread/SummaryThread1;->summaryTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    invoke-virtual {v3, v14}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElement(I)Lcom/samsung/app/video/editor/external/Element;

    move-result-object v12

    .line 236
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/ve/thread/SummaryThread1;->summaryTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    invoke-virtual {v3, v12}, Lcom/samsung/app/video/editor/external/TranscodeElement;->removeElement(Lcom/samsung/app/video/editor/external/Element;)Ljava/util/List;

    .line 234
    add-int/lit8 v13, v13, 0x1

    goto :goto_2

    .line 241
    .end local v11    # "eleCount":I
    .end local v14    # "l":I
    :cond_3
    invoke-virtual {v12}, Lcom/samsung/app/video/editor/external/Element;->getEndTime()J

    move-result-wide v6

    invoke-virtual {v12}, Lcom/samsung/app/video/editor/external/Element;->getStartTime()J

    move-result-wide v20

    cmp-long v3, v6, v20

    if-gtz v3, :cond_4

    .line 242
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/ve/thread/SummaryThread1;->summaryTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v12}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 220
    :goto_3
    add-int/lit8 v13, v13, 0x1

    goto/16 :goto_1

    .line 246
    :cond_4
    const/4 v3, 0x1

    invoke-virtual {v12, v3}, Lcom/samsung/app/video/editor/external/Element;->setAutoEdited(Z)V

    .line 248
    invoke-virtual {v12}, Lcom/samsung/app/video/editor/external/Element;->getGroupID()I

    move-result v3

    invoke-virtual {v4, v3}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementWithID(I)Lcom/samsung/app/video/editor/external/Element;

    move-result-object v16

    .line 249
    .local v16, "originalEle":Lcom/samsung/app/video/editor/external/Element;
    if-eqz v16, :cond_5

    .line 251
    const/4 v3, 0x1

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Lcom/samsung/app/video/editor/external/Element;->getEdit(I)Lcom/samsung/app/video/editor/external/Edit;

    move-result-object v9

    .line 252
    .local v9, "curEdit":Lcom/samsung/app/video/editor/external/Edit;
    if-eqz v9, :cond_5

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/app/video/editor/external/Edit;->getVolumeLevel()I

    move-result v3

    const/4 v5, -0x1

    if-ne v3, v5, :cond_5

    .line 253
    new-instance v15, Lcom/samsung/app/video/editor/external/Edit;

    const/4 v3, 0x1

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Lcom/samsung/app/video/editor/external/Element;->getEdit(I)Lcom/samsung/app/video/editor/external/Edit;

    move-result-object v3

    invoke-direct {v15, v3}, Lcom/samsung/app/video/editor/external/Edit;-><init>(Lcom/samsung/app/video/editor/external/Edit;)V

    .line 254
    .local v15, "newEdit":Lcom/samsung/app/video/editor/external/Edit;
    invoke-virtual {v12, v15}, Lcom/samsung/app/video/editor/external/Element;->addEdit(Lcom/samsung/app/video/editor/external/Edit;)V

    .line 258
    .end local v9    # "curEdit":Lcom/samsung/app/video/editor/external/Edit;
    .end local v15    # "newEdit":Lcom/samsung/app/video/editor/external/Edit;
    :cond_5
    new-instance v10, Lcom/samsung/app/video/editor/external/Edit;

    .end local v10    # "edit":Lcom/samsung/app/video/editor/external/Edit;
    invoke-direct {v10}, Lcom/samsung/app/video/editor/external/Edit;-><init>()V

    .line 259
    .restart local v10    # "edit":Lcom/samsung/app/video/editor/external/Edit;
    const/4 v3, 0x1

    invoke-virtual {v10, v3}, Lcom/samsung/app/video/editor/external/Edit;->setType(I)V

    .line 260
    invoke-virtual {v12, v10}, Lcom/samsung/app/video/editor/external/Element;->addEdit(Lcom/samsung/app/video/editor/external/Edit;)V

    goto :goto_3
.end method

.method private updateTimingInfoForGroup(Lcom/samsung/app/video/editor/external/TranscodeElement;Lcom/samsung/app/video/editor/external/TranscodeElement;)V
    .locals 20
    .param p1, "summaryProject"    # Lcom/samsung/app/video/editor/external/TranscodeElement;
    .param p2, "origProject"    # Lcom/samsung/app/video/editor/external/TranscodeElement;

    .prologue
    .line 270
    const/4 v4, 0x0

    .line 271
    .local v4, "element":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementList()Ljava/util/List;

    move-result-object v11

    .line 272
    .local v11, "list":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getMaxGroupID()I

    move-result v2

    .line 273
    .local v2, "count":I
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_0
    if-le v9, v2, :cond_0

    .line 299
    return-void

    .line 274
    :cond_0
    new-instance v7, Ljava/util/Vector;

    invoke-direct {v7}, Ljava/util/Vector;-><init>()V

    .line 275
    .local v7, "groupList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v12

    .line 276
    .local v12, "listSize":I
    const/4 v10, 0x0

    .local v10, "j":I
    :goto_1
    if-lt v10, v12, :cond_1

    .line 282
    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getSummaryGroupTotalTime(Ljava/util/List;)J

    move-result-wide v14

    .line 283
    .local v14, "totalDuration":J
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v8

    .line 284
    .local v8, "groupSize":I
    const/4 v10, 0x0

    :goto_2
    if-lt v10, v8, :cond_3

    .line 273
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 277
    .end local v8    # "groupSize":I
    .end local v14    # "totalDuration":J
    :cond_1
    invoke-interface {v11, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "element":Lcom/samsung/app/video/editor/external/Element;
    check-cast v4, Lcom/samsung/app/video/editor/external/Element;

    .line 278
    .restart local v4    # "element":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/Element;->getGroupID()I

    move-result v16

    move/from16 v0, v16

    if-ne v9, v0, :cond_2

    .line 279
    invoke-interface {v7, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 276
    :cond_2
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    .line 285
    .restart local v8    # "groupSize":I
    .restart local v14    # "totalDuration":J
    :cond_3
    invoke-interface {v7, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "element":Lcom/samsung/app/video/editor/external/Element;
    check-cast v4, Lcom/samsung/app/video/editor/external/Element;

    .line 286
    .restart local v4    # "element":Lcom/samsung/app/video/editor/external/Element;
    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-interface {v7, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/app/video/editor/external/Element;

    .line 287
    .local v5, "groupFirstEle":Lcom/samsung/app/video/editor/external/Element;
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v16

    add-int/lit8 v16, v16, -0x1

    move/from16 v0, v16

    invoke-interface {v7, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/app/video/editor/external/Element;

    .line 288
    .local v6, "groupLastEle":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {v5}, Lcom/samsung/app/video/editor/external/Element;->getStartTime()J

    move-result-wide v16

    move-wide/from16 v0, v16

    invoke-virtual {v4, v0, v1}, Lcom/samsung/app/video/editor/external/Element;->setGroupStartTime(J)V

    .line 289
    invoke-virtual {v4, v14, v15}, Lcom/samsung/app/video/editor/external/Element;->setGroupDuration(J)V

    .line 290
    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/Element;->getGroupID()I

    move-result v16

    move-object/from16 v0, p2

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementWithID(I)Lcom/samsung/app/video/editor/external/Element;

    move-result-object v3

    .line 291
    .local v3, "elem":Lcom/samsung/app/video/editor/external/Element;
    if-eqz v3, :cond_4

    .line 292
    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/Element;->getSplitTime()J

    move-result-wide v16

    move-wide/from16 v0, v16

    invoke-virtual {v4, v0, v1}, Lcom/samsung/app/video/editor/external/Element;->setSplitTime(J)V

    .line 293
    :cond_4
    invoke-virtual {v6}, Lcom/samsung/app/video/editor/external/Element;->getEndTime()J

    move-result-wide v16

    move-wide/from16 v0, v16

    invoke-virtual {v4, v0, v1}, Lcom/samsung/app/video/editor/external/Element;->setGroupEndTime(J)V

    .line 294
    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/Element;->getEndTime()J

    move-result-wide v16

    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/Element;->getStartTime()J

    move-result-wide v18

    sub-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-float v0, v0

    move/from16 v16, v0

    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/Element;->getGroupDuration()J

    move-result-wide v18

    move-wide/from16 v0, v18

    long-to-float v0, v0

    move/from16 v17, v0

    div-float v13, v16, v17

    .line 295
    .local v13, "ratio":F
    invoke-virtual {v4, v13}, Lcom/samsung/app/video/editor/external/Element;->setElementRatioInGroup(F)V

    .line 284
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_2
.end method


# virtual methods
.method public getFinalPath()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/android/app/ve/thread/SummaryThread1;->path:[Ljava/lang/String;

    return-object v0
.end method

.method public getSummaryTranscode()Lcom/samsung/app/video/editor/external/TranscodeElement;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/sec/android/app/ve/thread/SummaryThread1;->summaryTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    return-object v0
.end method

.method public releaseWakelock()V
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/android/app/ve/thread/SummaryThread1;->wakelock:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/ve/thread/SummaryThread1;->wakelock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 117
    iget-object v0, p0, Lcom/sec/android/app/ve/thread/SummaryThread1;->wakelock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 118
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/ve/thread/SummaryThread1;->wakelock:Landroid/os/PowerManager$WakeLock;

    .line 120
    :cond_0
    return-void
.end method

.method public run()V
    .locals 10

    .prologue
    const-wide/16 v8, 0xa

    const/16 v7, 0x7d

    const/4 v6, 0x1

    .line 59
    iget-object v3, p0, Lcom/sec/android/app/ve/thread/SummaryThread1;->mContext:Landroid/content/Context;

    if-eqz v3, :cond_2

    .line 61
    iget-object v3, p0, Lcom/sec/android/app/ve/thread/SummaryThread1;->mContext:Landroid/content/Context;

    .line 62
    const-string v4, "power"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/PowerManager;

    .line 63
    const/16 v4, 0xa

    const-string v5, "VE Export Thread"

    .line 62
    invoke-virtual {v3, v4, v5}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v3

    .line 61
    iput-object v3, p0, Lcom/sec/android/app/ve/thread/SummaryThread1;->wakelock:Landroid/os/PowerManager$WakeLock;

    .line 64
    iget-object v3, p0, Lcom/sec/android/app/ve/thread/SummaryThread1;->wakelock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v3}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 66
    const/4 v0, 0x0

    .line 67
    .local v0, "isSummarySuccess":Z
    const/4 v2, 0x1

    .line 68
    .local v2, "result":I
    iget-boolean v3, p0, Lcom/sec/android/app/ve/thread/SummaryThread1;->analysisRequired:Z

    if-eqz v3, :cond_0

    .line 69
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/ve/thread/SummaryThread1;->mCurrentTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    .line 70
    sget v5, Lcom/samsung/app/video/editor/external/Constants;->AUTOEDIT_MODE_AUTO:I

    .line 69
    invoke-virtual {v3, v4, v5, v6}, Lcom/samsung/app/video/editor/external/NativeInterface;->createSummaryInstance(Lcom/samsung/app/video/editor/external/TranscodeElement;II)I

    move-result v2

    .line 71
    :cond_0
    if-ne v2, v6, :cond_1

    .line 72
    iget-object v3, p0, Lcom/sec/android/app/ve/thread/SummaryThread1;->summaryTranscodeElement:Lcom/samsung/app/video/editor/external/TranscodeElement;

    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getThemeName()I

    move-result v3

    if-nez v3, :cond_3

    .line 73
    invoke-direct {p0}, Lcom/sec/android/app/ve/thread/SummaryThread1;->createNoneThemeSummaryProject()Z

    move-result v0

    .line 77
    :cond_1
    :goto_0
    if-nez v0, :cond_4

    .line 78
    invoke-virtual {p0}, Lcom/sec/android/app/ve/thread/SummaryThread1;->releaseWakelock()V

    .line 85
    :goto_1
    if-nez v0, :cond_6

    .line 86
    iget-boolean v3, p0, Lcom/sec/android/app/ve/thread/SummaryThread1;->mForceStop:Z

    if-eqz v3, :cond_5

    const/16 v1, 0x7c

    .line 87
    .local v1, "msg":I
    :goto_2
    iget-object v3, p0, Lcom/sec/android/app/ve/thread/SummaryThread1;->mHandler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/sec/android/app/ve/thread/SummaryThread1;->mHandler:Landroid/os/Handler;

    invoke-static {v4, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {v3, v4, v8, v9}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 88
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/sec/android/app/ve/thread/SummaryThread1;->mForceStop:Z

    .line 100
    .end local v0    # "isSummarySuccess":Z
    .end local v1    # "msg":I
    .end local v2    # "result":I
    :cond_2
    :goto_3
    return-void

    .line 75
    .restart local v0    # "isSummarySuccess":Z
    .restart local v2    # "result":I
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/app/ve/thread/SummaryThread1;->createThemeSummaryProject()Z

    move-result v0

    goto :goto_0

    .line 80
    :cond_4
    iget-object v3, p0, Lcom/sec/android/app/ve/thread/SummaryThread1;->handler:Landroid/os/Handler;

    invoke-virtual {v3, v7}, Landroid/os/Handler;->removeMessages(I)V

    .line 81
    iget-object v3, p0, Lcom/sec/android/app/ve/thread/SummaryThread1;->handler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/sec/android/app/ve/thread/SummaryThread1;->handler:Landroid/os/Handler;

    invoke-static {v4, v7}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v4

    .line 82
    const-wide/16 v6, 0xbb8

    .line 81
    invoke-virtual {v3, v4, v6, v7}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_1

    .line 86
    :cond_5
    const/16 v1, 0x7a

    goto :goto_2

    .line 91
    :cond_6
    iget-object v3, p0, Lcom/sec/android/app/ve/thread/SummaryThread1;->mContext:Landroid/content/Context;

    if-nez v3, :cond_7

    .line 92
    const-string v3, "context is null"

    invoke-static {v3}, Lcom/sec/android/app/ve/common/LogUtils;->log(Ljava/lang/String;)V

    .line 95
    :cond_7
    iget-object v3, p0, Lcom/sec/android/app/ve/thread/SummaryThread1;->mHandler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/sec/android/app/ve/thread/SummaryThread1;->mHandler:Landroid/os/Handler;

    .line 96
    const/16 v5, 0x7b

    .line 97
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    .line 95
    invoke-static {v4, v5, v6}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {v3, v4, v8, v9}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_3
.end method

.method public stopSummary()V
    .locals 1

    .prologue
    .line 103
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/ve/thread/SummaryThread1;->mForceStop:Z

    .line 104
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/app/video/editor/external/NativeInterface;->stopEngineSummary()V

    .line 105
    return-void
.end method
