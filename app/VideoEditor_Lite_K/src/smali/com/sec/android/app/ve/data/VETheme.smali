.class public Lcom/sec/android/app/ve/data/VETheme;
.super Ljava/lang/Object;
.source "VETheme.java"


# instance fields
.field private lMusicElement:Lcom/samsung/app/video/editor/external/Element;

.field private mAlphaPath:Ljava/lang/String;

.field private mAlphaPath1:Ljava/lang/String;

.field private mAlphaPath2:Ljava/lang/String;

.field private mEffectEdit:Lcom/samsung/app/video/editor/external/Edit;

.field private mTemplatePath:Ljava/lang/String;

.field private mTemplatePath1:Ljava/lang/String;

.field private mTemplatePath2:Ljava/lang/String;

.field private mTransitionEdit:Lcom/samsung/app/video/editor/external/Edit;

.field private maAlphaq:Ljava/lang/String;

.field private maTemepleteq:Ljava/lang/String;

.field private mbAlphaq:Ljava/lang/String;

.field private mbTemepleteq:Ljava/lang/String;

.field private mcAlphaq:Ljava/lang/String;

.field private mcTemepleteq:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 5
    .param p1, "aAlphapath"    # Ljava/lang/String;
    .param p2, "aAlphapath1"    # Ljava/lang/String;
    .param p3, "aAlphapath2"    # Ljava/lang/String;
    .param p4, "aTemplatepath"    # Ljava/lang/String;
    .param p5, "aTemplatepath1"    # Ljava/lang/String;
    .param p6, "aTemplatepath2"    # Ljava/lang/String;
    .param p7, "aAlphaq"    # Ljava/lang/String;
    .param p8, "bAlphaq"    # Ljava/lang/String;
    .param p9, "cAlphaq"    # Ljava/lang/String;
    .param p10, "aTempleteq"    # Ljava/lang/String;
    .param p11, "bTempleteq"    # Ljava/lang/String;
    .param p12, "cTempleteq"    # Ljava/lang/String;
    .param p13, "aBGMPath"    # Ljava/lang/String;
    .param p14, "aBGMName"    # Ljava/lang/String;
    .param p15, "aSubType"    # I
    .param p16, "aBGMDuration"    # I

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/sec/android/app/ve/data/VETheme;->mAlphaPath:Ljava/lang/String;

    .line 20
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/sec/android/app/ve/data/VETheme;->mAlphaPath1:Ljava/lang/String;

    .line 21
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/sec/android/app/ve/data/VETheme;->mAlphaPath2:Ljava/lang/String;

    .line 25
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/sec/android/app/ve/data/VETheme;->mTemplatePath:Ljava/lang/String;

    .line 26
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/sec/android/app/ve/data/VETheme;->mTemplatePath1:Ljava/lang/String;

    .line 27
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/sec/android/app/ve/data/VETheme;->mTemplatePath2:Ljava/lang/String;

    .line 31
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/sec/android/app/ve/data/VETheme;->mTransitionEdit:Lcom/samsung/app/video/editor/external/Edit;

    .line 32
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/sec/android/app/ve/data/VETheme;->mEffectEdit:Lcom/samsung/app/video/editor/external/Edit;

    .line 34
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/sec/android/app/ve/data/VETheme;->lMusicElement:Lcom/samsung/app/video/editor/external/Element;

    .line 41
    iput-object p1, p0, Lcom/sec/android/app/ve/data/VETheme;->mAlphaPath:Ljava/lang/String;

    .line 42
    iput-object p2, p0, Lcom/sec/android/app/ve/data/VETheme;->mAlphaPath1:Ljava/lang/String;

    .line 43
    iput-object p3, p0, Lcom/sec/android/app/ve/data/VETheme;->mAlphaPath2:Ljava/lang/String;

    .line 45
    iput-object p4, p0, Lcom/sec/android/app/ve/data/VETheme;->mTemplatePath:Ljava/lang/String;

    .line 46
    iput-object p5, p0, Lcom/sec/android/app/ve/data/VETheme;->mTemplatePath1:Ljava/lang/String;

    .line 47
    iput-object p6, p0, Lcom/sec/android/app/ve/data/VETheme;->mTemplatePath2:Ljava/lang/String;

    .line 49
    iput-object p7, p0, Lcom/sec/android/app/ve/data/VETheme;->maAlphaq:Ljava/lang/String;

    .line 50
    iput-object p8, p0, Lcom/sec/android/app/ve/data/VETheme;->mbAlphaq:Ljava/lang/String;

    .line 51
    iput-object p9, p0, Lcom/sec/android/app/ve/data/VETheme;->mcAlphaq:Ljava/lang/String;

    .line 53
    iput-object p10, p0, Lcom/sec/android/app/ve/data/VETheme;->maTemepleteq:Ljava/lang/String;

    .line 54
    move-object/from16 v0, p11

    iput-object v0, p0, Lcom/sec/android/app/ve/data/VETheme;->mbTemepleteq:Ljava/lang/String;

    .line 55
    move-object/from16 v0, p12

    iput-object v0, p0, Lcom/sec/android/app/ve/data/VETheme;->mcTemepleteq:Ljava/lang/String;

    .line 57
    new-instance v3, Lcom/samsung/app/video/editor/external/Edit;

    invoke-direct {v3}, Lcom/samsung/app/video/editor/external/Edit;-><init>()V

    iput-object v3, p0, Lcom/sec/android/app/ve/data/VETheme;->mTransitionEdit:Lcom/samsung/app/video/editor/external/Edit;

    .line 58
    iget-object v3, p0, Lcom/sec/android/app/ve/data/VETheme;->mTransitionEdit:Lcom/samsung/app/video/editor/external/Edit;

    const/4 v4, 0x6

    invoke-virtual {v3, v4}, Lcom/samsung/app/video/editor/external/Edit;->setType(I)V

    .line 59
    iget-object v3, p0, Lcom/sec/android/app/ve/data/VETheme;->mTransitionEdit:Lcom/samsung/app/video/editor/external/Edit;

    const/16 v4, 0x3e8

    invoke-virtual {v3, v4}, Lcom/samsung/app/video/editor/external/Edit;->setTrans_duration(I)V

    .line 60
    iget-object v3, p0, Lcom/sec/android/app/ve/data/VETheme;->mTransitionEdit:Lcom/samsung/app/video/editor/external/Edit;

    move/from16 v0, p15

    invoke-virtual {v3, v0}, Lcom/samsung/app/video/editor/external/Edit;->setSubType(I)V

    .line 62
    new-instance v3, Lcom/samsung/app/video/editor/external/Edit;

    invoke-direct {v3}, Lcom/samsung/app/video/editor/external/Edit;-><init>()V

    iput-object v3, p0, Lcom/sec/android/app/ve/data/VETheme;->mEffectEdit:Lcom/samsung/app/video/editor/external/Edit;

    .line 63
    iget-object v3, p0, Lcom/sec/android/app/ve/data/VETheme;->mEffectEdit:Lcom/samsung/app/video/editor/external/Edit;

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Lcom/samsung/app/video/editor/external/Edit;->setType(I)V

    .line 64
    iget-object v3, p0, Lcom/sec/android/app/ve/data/VETheme;->mEffectEdit:Lcom/samsung/app/video/editor/external/Edit;

    const/16 v4, 0x17

    invoke-virtual {v3, v4}, Lcom/samsung/app/video/editor/external/Edit;->setSubType(I)V

    .line 66
    move-object/from16 v0, p13

    move-object/from16 v1, p14

    move/from16 v2, p16

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/app/ve/data/VETheme;->createBGMElement(Ljava/lang/String;Ljava/lang/String;I)V

    .line 67
    return-void
.end method

.method private createBGMElement(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 4
    .param p1, "aFilePath"    # Ljava/lang/String;
    .param p2, "aDisplayName"    # Ljava/lang/String;
    .param p3, "aBGMDuration"    # I

    .prologue
    .line 71
    new-instance v1, Lcom/samsung/app/video/editor/external/Element;

    invoke-direct {v1}, Lcom/samsung/app/video/editor/external/Element;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/ve/data/VETheme;->lMusicElement:Lcom/samsung/app/video/editor/external/Element;

    .line 72
    iget-object v1, p0, Lcom/sec/android/app/ve/data/VETheme;->lMusicElement:Lcom/samsung/app/video/editor/external/Element;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/samsung/app/video/editor/external/Element;->setType(I)V

    .line 73
    iget-object v1, p0, Lcom/sec/android/app/ve/data/VETheme;->lMusicElement:Lcom/samsung/app/video/editor/external/Element;

    invoke-virtual {v1, p1}, Lcom/samsung/app/video/editor/external/Element;->setFilePath(Ljava/lang/String;)V

    .line 75
    iget-object v1, p0, Lcom/sec/android/app/ve/data/VETheme;->lMusicElement:Lcom/samsung/app/video/editor/external/Element;

    int-to-long v2, p3

    invoke-virtual {v1, v2, v3}, Lcom/samsung/app/video/editor/external/Element;->setEndTime(J)V

    .line 77
    new-instance v0, Lcom/samsung/app/video/editor/external/Edit;

    invoke-direct {v0}, Lcom/samsung/app/video/editor/external/Edit;-><init>()V

    .line 78
    .local v0, "edit":Lcom/samsung/app/video/editor/external/Edit;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/app/video/editor/external/Edit;->setType(I)V

    .line 79
    iget-object v1, p0, Lcom/sec/android/app/ve/data/VETheme;->lMusicElement:Lcom/samsung/app/video/editor/external/Element;

    invoke-virtual {v1, v0}, Lcom/samsung/app/video/editor/external/Element;->addEdit(Lcom/samsung/app/video/editor/external/Edit;)V

    .line 80
    iget-object v1, p0, Lcom/sec/android/app/ve/data/VETheme;->lMusicElement:Lcom/samsung/app/video/editor/external/Element;

    invoke-virtual {v1, p2}, Lcom/samsung/app/video/editor/external/Element;->setAudioDisplayName(Ljava/lang/String;)V

    .line 81
    return-void
.end method


# virtual methods
.method public getA_Alpha_q()Ljava/lang/String;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/android/app/ve/data/VETheme;->maAlphaq:Ljava/lang/String;

    return-object v0
.end method

.method public getA_Temeplete_q()Ljava/lang/String;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/sec/android/app/ve/data/VETheme;->maTemepleteq:Ljava/lang/String;

    return-object v0
.end method

.method public getAlphaPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/android/app/ve/data/VETheme;->mAlphaPath:Ljava/lang/String;

    return-object v0
.end method

.method public getAlphaPath1()Ljava/lang/String;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/android/app/ve/data/VETheme;->mAlphaPath1:Ljava/lang/String;

    return-object v0
.end method

.method public getAlphaPath2()Ljava/lang/String;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/android/app/ve/data/VETheme;->mAlphaPath2:Ljava/lang/String;

    return-object v0
.end method

.method public getB_Alpha_q()Ljava/lang/String;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/android/app/ve/data/VETheme;->mbAlphaq:Ljava/lang/String;

    return-object v0
.end method

.method public getB_Temeplete_q()Ljava/lang/String;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/sec/android/app/ve/data/VETheme;->mbTemepleteq:Ljava/lang/String;

    return-object v0
.end method

.method public getC_Alpha_q()Ljava/lang/String;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/sec/android/app/ve/data/VETheme;->mcAlphaq:Ljava/lang/String;

    return-object v0
.end method

.method public getC_Temeplete_q()Ljava/lang/String;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/android/app/ve/data/VETheme;->mcTemepleteq:Ljava/lang/String;

    return-object v0
.end method

.method public getEffectEdit()Lcom/samsung/app/video/editor/external/Edit;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/sec/android/app/ve/data/VETheme;->mEffectEdit:Lcom/samsung/app/video/editor/external/Edit;

    return-object v0
.end method

.method public getTemplatePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/sec/android/app/ve/data/VETheme;->mTemplatePath:Ljava/lang/String;

    return-object v0
.end method

.method public getTemplatePath1()Ljava/lang/String;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/android/app/ve/data/VETheme;->mTemplatePath1:Ljava/lang/String;

    return-object v0
.end method

.method public getTemplatePath2()Ljava/lang/String;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/sec/android/app/ve/data/VETheme;->mTemplatePath2:Ljava/lang/String;

    return-object v0
.end method

.method public getThemeBGM()Lcom/samsung/app/video/editor/external/Element;
    .locals 2

    .prologue
    .line 84
    new-instance v0, Lcom/samsung/app/video/editor/external/Element;

    iget-object v1, p0, Lcom/sec/android/app/ve/data/VETheme;->lMusicElement:Lcom/samsung/app/video/editor/external/Element;

    invoke-direct {v0, v1}, Lcom/samsung/app/video/editor/external/Element;-><init>(Lcom/samsung/app/video/editor/external/Element;)V

    return-object v0
.end method

.method public getTransitionEdit()Lcom/samsung/app/video/editor/external/Edit;
    .locals 2

    .prologue
    .line 136
    new-instance v0, Lcom/samsung/app/video/editor/external/Edit;

    iget-object v1, p0, Lcom/sec/android/app/ve/data/VETheme;->mTransitionEdit:Lcom/samsung/app/video/editor/external/Edit;

    invoke-direct {v0, v1}, Lcom/samsung/app/video/editor/external/Edit;-><init>(Lcom/samsung/app/video/editor/external/Edit;)V

    return-object v0
.end method
