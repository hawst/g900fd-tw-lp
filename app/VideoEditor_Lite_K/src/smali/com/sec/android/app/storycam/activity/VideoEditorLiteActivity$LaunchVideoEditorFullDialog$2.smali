.class Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$LaunchVideoEditorFullDialog$2;
.super Ljava/lang/Object;
.source "VideoEditorLiteActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$LaunchVideoEditorFullDialog;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$LaunchVideoEditorFullDialog;


# direct methods
.method constructor <init>(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$LaunchVideoEditorFullDialog;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$LaunchVideoEditorFullDialog$2;->this$1:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$LaunchVideoEditorFullDialog;

    .line 2489
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "arg1"    # I

    .prologue
    .line 2494
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 2496
    check-cast p1, Landroid/app/AlertDialog;

    .end local p1    # "dialog":Landroid/content/DialogInterface;
    const v3, 0x7f0d0020

    invoke-virtual {p1, v3}, Landroid/app/AlertDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 2497
    .local v0, "cb":Landroid/widget/CheckBox;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2498
    iget-object v3, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$LaunchVideoEditorFullDialog$2;->this$1:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$LaunchVideoEditorFullDialog;

    invoke-virtual {v3}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$LaunchVideoEditorFullDialog;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 2499
    .local v2, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 2500
    .local v1, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v3, "SHOW_LAUNCH_VIDEO_EDITOR"

    const/4 v4, 0x0

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 2501
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2503
    .end local v1    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v2    # "prefs":Landroid/content/SharedPreferences;
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$LaunchVideoEditorFullDialog$2;->this$1:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$LaunchVideoEditorFullDialog;

    invoke-virtual {v3}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$LaunchVideoEditorFullDialog;->getActivity()Landroid/app/Activity;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity;->checkVEFullAndProceed()V

    .line 2506
    return-void
.end method
