.class public Lcom/sec/android/app/storycam/VEMediaSession;
.super Ljava/lang/Object;
.source "VEMediaSession.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/storycam/VEMediaSession$VEMediaSessionCallback;
    }
.end annotation


# instance fields
.field private mediaSessionCallback:Landroid/media/session/MediaSession$Callback;

.field private session:Landroid/media/session/MediaSession;

.field private veCallback:Lcom/sec/android/app/storycam/VEMediaSession$VEMediaSessionCallback;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    new-instance v0, Lcom/sec/android/app/storycam/VEMediaSession$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/storycam/VEMediaSession$1;-><init>(Lcom/sec/android/app/storycam/VEMediaSession;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/VEMediaSession;->mediaSessionCallback:Landroid/media/session/MediaSession$Callback;

    .line 7
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/app/storycam/VEMediaSession;)Lcom/sec/android/app/storycam/VEMediaSession$VEMediaSessionCallback;
    .locals 1

    .prologue
    .line 9
    iget-object v0, p0, Lcom/sec/android/app/storycam/VEMediaSession;->veCallback:Lcom/sec/android/app/storycam/VEMediaSession$VEMediaSessionCallback;

    return-object v0
.end method


# virtual methods
.method public activateSession(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 28
    new-instance v0, Landroid/media/session/PlaybackState$Builder;

    invoke-direct {v0}, Landroid/media/session/PlaybackState$Builder;-><init>()V

    .line 29
    .local v0, "state":Landroid/media/session/PlaybackState$Builder;
    const-wide/16 v2, 0x37f

    invoke-virtual {v0, v2, v3}, Landroid/media/session/PlaybackState$Builder;->setActions(J)Landroid/media/session/PlaybackState$Builder;

    .line 36
    iget-object v1, p0, Lcom/sec/android/app/storycam/VEMediaSession;->session:Landroid/media/session/MediaSession;

    invoke-virtual {v0}, Landroid/media/session/PlaybackState$Builder;->build()Landroid/media/session/PlaybackState;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/media/session/MediaSession;->setPlaybackState(Landroid/media/session/PlaybackState;)V

    .line 37
    check-cast p1, Landroid/app/Activity;

    .end local p1    # "context":Landroid/content/Context;
    iget-object v1, p0, Lcom/sec/android/app/storycam/VEMediaSession;->session:Landroid/media/session/MediaSession;

    .line 38
    invoke-virtual {v1}, Landroid/media/session/MediaSession;->getController()Landroid/media/session/MediaController;

    move-result-object v1

    .line 37
    invoke-virtual {p1, v1}, Landroid/app/Activity;->setMediaController(Landroid/media/session/MediaController;)V

    .line 39
    iget-object v1, p0, Lcom/sec/android/app/storycam/VEMediaSession;->session:Landroid/media/session/MediaSession;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/media/session/MediaSession;->setActive(Z)V

    .line 40
    return-void
.end method

.method public createSession(Landroid/content/Context;Ljava/lang/String;Lcom/sec/android/app/storycam/VEMediaSession$VEMediaSessionCallback;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "className"    # Ljava/lang/String;
    .param p3, "callBacks"    # Lcom/sec/android/app/storycam/VEMediaSession$VEMediaSessionCallback;

    .prologue
    .line 21
    iput-object p3, p0, Lcom/sec/android/app/storycam/VEMediaSession;->veCallback:Lcom/sec/android/app/storycam/VEMediaSession$VEMediaSessionCallback;

    .line 22
    new-instance v0, Landroid/media/session/MediaSession;

    invoke-direct {v0, p1, p2}, Landroid/media/session/MediaSession;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/storycam/VEMediaSession;->session:Landroid/media/session/MediaSession;

    .line 23
    iget-object v0, p0, Lcom/sec/android/app/storycam/VEMediaSession;->session:Landroid/media/session/MediaSession;

    iget-object v1, p0, Lcom/sec/android/app/storycam/VEMediaSession;->mediaSessionCallback:Landroid/media/session/MediaSession$Callback;

    invoke-virtual {v0, v1}, Landroid/media/session/MediaSession;->setCallback(Landroid/media/session/MediaSession$Callback;)V

    .line 24
    iget-object v0, p0, Lcom/sec/android/app/storycam/VEMediaSession;->session:Landroid/media/session/MediaSession;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/media/session/MediaSession;->setFlags(I)V

    .line 26
    return-void
.end method

.method public deactivateSession()V
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/app/storycam/VEMediaSession;->session:Landroid/media/session/MediaSession;

    invoke-virtual {v0}, Landroid/media/session/MediaSession;->release()V

    .line 44
    return-void
.end method
