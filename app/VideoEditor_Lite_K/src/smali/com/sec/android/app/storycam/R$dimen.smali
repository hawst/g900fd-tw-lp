.class public final Lcom/sec/android/app/storycam/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/storycam/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final AddMediaParent_height:I = 0x7f06001a

.field public static final Play_text_height:I = 0x7f060155

.field public static final PreviewGroup_height:I = 0x7f06009a

.field public static final PreviewGroup_marginTop:I = 0x7f06009b

.field public static final PreviewGroup_width:I = 0x7f06009c

.field public static final PreviewGroup_width_extended:I = 0x7f06009d

.field public static final action_menu_height:I = 0x7f060004

.field public static final action_menu_width:I = 0x7f060005

.field public static final actionbar_height:I = 0x7f060001

.field public static final actionbar_undo_paddingBottom:I = 0x7f060006

.field public static final actionbar_undo_paddingRight:I = 0x7f060007

.field public static final actionbar_undo_paddingTop:I = 0x7f060008

.field public static final addMedia_PreviewParent_height:I = 0x7f060018

.field public static final addMedia_animation_y:I = 0x7f060010

.field public static final add_bgm_icon_margin_left:I = 0x7f060368

.field public static final add_bgm_icon_margin_top:I = 0x7f060367

.field public static final add_bgm_icon_width:I = 0x7f06034e

.field public static final add_media_second_text_margintop:I = 0x7f0602ba

.field public static final add_titleImg_Height:I = 0x7f060009

.field public static final add_titleImg_Width:I = 0x7f06000a

.field public static final add_titleText_Height:I = 0x7f06000c

.field public static final add_titleText_Size:I = 0x7f06000e

.field public static final add_titleText_Width:I = 0x7f06000f

.field public static final add_titleText_marginTop:I = 0x7f06000d

.field public static final addmedia_capture_picture_textsize:I = 0x7f060011

.field public static final addmedia_control_layout_height_custom:I = 0x7f06001e

.field public static final addmedia_control_layout_height_default:I = 0x7f06001d

.field public static final addmedia_control_list_imageview_marginleft:I = 0x7f06028a

.field public static final addmedia_controllist_contentbg_width:I = 0x7f060012

.field public static final addmedia_controllist_margintop:I = 0x7f060013

.field public static final addmedia_imagevideo_bg_padding:I = 0x7f0602b5

.field public static final addmedia_imagevideo_corner_radius:I = 0x7f0602b4

.field public static final addmedia_items_height:I = 0x7f06001c

.field public static final addmedia_items_width:I = 0x7f06001b

.field public static final addmedia_margineTop:I = 0x7f060014

.field public static final addmedia_media_picker_height:I = 0x7f0602c4

.field public static final addmedia_media_picker_verticalspacing:I = 0x7f0602c3

.field public static final addmedia_picker_width:I = 0x7f060015

.field public static final addmedia_play_btn_height:I = 0x7f060016

.field public static final addmedia_play_btn_width:I = 0x7f060017

.field public static final addmedia_playbutton_layout_height:I = 0x7f060165

.field public static final addmedia_playbutton_layout_width:I = 0x7f060166

.field public static final addmedia_show_button_limit:I = 0x7f060019

.field public static final addmedia_toggle_pointer_height:I = 0x7f060020

.field public static final addmedia_toggle_pointer_width:I = 0x7f06001f

.field public static final addmediaicon_height:I = 0x7f060161

.field public static final addmediaicon_margintop:I = 0x7f060162

.field public static final addmediaicon_width:I = 0x7f060160

.field public static final addmediatextsize:I = 0x7f060159

.field public static final addtextbox_margintop:I = 0x7f060167

.field public static final album_frame_padding:I = 0x7f0602ab

.field public static final audioBubble_height:I = 0x7f060026

.field public static final audioBubble_text_size:I = 0x7f060027

.field public static final audioBubble_width:I = 0x7f060028

.field public static final audio_images_insetLeft:I = 0x7f060021

.field public static final audio_images_insetRight:I = 0x7f060022

.field public static final audio_name_vol_marginLeft:I = 0x7f060023

.field public static final audio_name_vol_textSize:I = 0x7f060024

.field public static final audio_video_height:I = 0x7f060025

.field public static final auto_mk_big_text:I = 0x7f060029

.field public static final auto_mk_child_left_margin:I = 0x7f06002a

.field public static final auto_mk_child_right_margin:I = 0x7f06002b

.field public static final auto_mk_normal_text:I = 0x7f06002c

.field public static final auto_mk_small_text:I = 0x7f06002d

.field public static final auto_mk_time_bar_margin:I = 0x7f06002e

.field public static final auto_mk_time_bar_margin_right:I = 0x7f06002f

.field public static final auto_mk_time_margin_top:I = 0x7f060030

.field public static final auto_summary_check_height:I = 0x7f06029d

.field public static final auto_summary_check_marginleft:I = 0x7f0602bc

.field public static final auto_summary_check_margintop:I = 0x7f06029c

.field public static final auto_summary_check_textgap:I = 0x7f0602bd

.field public static final auto_summary_checkboxgap:I = 0x7f06029e

.field public static final auto_summary_checkmarginbelow:I = 0x7f06029f

.field public static final auto_summary_text_marginside:I = 0x7f060299

.field public static final auto_summary_text_margintop:I = 0x7f06029a

.field public static final auto_summary_textsize:I = 0x7f06029b

.field public static final autoedit_nevershow_check_marginleft:I = 0x7f06021f

.field public static final baseline_arrowHeight:I = 0x7f06016c

.field public static final baseline_arrowHeight_Split:I = 0x7f06016e

.field public static final baseline_arrowWidth:I = 0x7f06016f

.field public static final baseline_arrowWidth_split:I = 0x7f060170

.field public static final baseline_arrowhead:I = 0x7f06016d

.field public static final baseline_margineTop:I = 0x7f060031

.field public static final baseline_margineTop_split:I = 0x7f060032

.field public static final bgm_edit_actionbar_layout_height:I = 0x7f0603ac

.field public static final bgm_edit_actionbar_marginLR:I = 0x7f0603ab

.field public static final bgm_edit_actionbar_text_size:I = 0x7f0603ad

.field public static final bgm_grid_item_width:I = 0x7f06034d

.field public static final bgm_grid_right_margin:I = 0x7f06034c

.field public static final bgm_grid_width:I = 0x7f06034b

.field public static final bgm_layout_content_marginTop:I = 0x7f06039e

.field public static final bgm_layout_height:I = 0x7f060364

.field public static final bgm_layout_padding_left:I = 0x7f060375

.field public static final bgm_layout_width:I = 0x7f060365

.field public static final bgm_message_content_textSize:I = 0x7f06039f

.field public static final bgm_message_divider_height:I = 0x7f06039d

.field public static final bgm_message_list_margin_left:I = 0x7f06039b

.field public static final bgm_message_list_margin_right:I = 0x7f06039c

.field public static final bgm_message_margin:I = 0x7f060399

.field public static final bgm_message_margin_left:I = 0x7f06039a

.field public static final bgm_text_padding_left:I = 0x7f060366

.field public static final cancel_overlay_help_h:I = 0x7f0603b2

.field public static final cancel_overlay_help_hw:I = 0x7f0603b3

.field public static final cancel_overlay_help_marginR:I = 0x7f0603b8

.field public static final cancel_overlay_help_marginT:I = 0x7f0603ba

.field public static final cancel_overlay_help_w:I = 0x7f0603b1

.field public static final caption_bigfont_size:I = 0x7f0601a1

.field public static final caption_close_icon_height:I = 0x7f0601a4

.field public static final caption_close_icon_width:I = 0x7f0601a3

.field public static final caption_container_width:I = 0x7f060356

.field public static final caption_divider_height:I = 0x7f060354

.field public static final caption_divider_margin_l:I = 0x7f060351

.field public static final caption_divider_margin_r:I = 0x7f060352

.field public static final caption_divider_margin_t:I = 0x7f060353

.field public static final caption_divider_width:I = 0x7f060355

.field public static final caption_drag_bottomlimit:I = 0x7f0601ba

.field public static final caption_drag_holder_height:I = 0x7f0601b6

.field public static final caption_drag_holder_marginTop:I = 0x7f0601b7

.field public static final caption_drag_holder_width:I = 0x7f0601b5

.field public static final caption_drag_toplimit:I = 0x7f0601b9

.field public static final caption_height_limit:I = 0x7f06019f

.field public static final caption_holder_left_margin:I = 0x7f0601a6

.field public static final caption_holder_top_margin:I = 0x7f0601a5

.field public static final caption_padding_sides:I = 0x7f0601b0

.field public static final caption_position_icon:I = 0x7f0601b3

.field public static final caption_position_icon_marginLeft:I = 0x7f0601b4

.field public static final caption_position_layout_height:I = 0x7f0601b2

.field public static final caption_position_layout_width:I = 0x7f0601b1

.field public static final caption_position_top_offset:I = 0x7f0601b8

.field public static final caption_smallfont_size:I = 0x7f0601a2

.field public static final caption_text_height:I = 0x7f0601af

.field public static final caption_text_init_topmargin:I = 0x7f0601ad

.field public static final caption_text_rightmargin:I = 0x7f0601ac

.field public static final caption_text_size:I = 0x7f060350

.field public static final caption_text_topmargin:I = 0x7f0601ab

.field public static final caption_text_width:I = 0x7f0601ae

.field public static final caption_topmargin_bottom:I = 0x7f0601a7

.field public static final caption_topmargin_bottom_chk:I = 0x7f0601a8

.field public static final caption_topmargin_middle:I = 0x7f0601a9

.field public static final caption_topmargin_top:I = 0x7f0601aa

.field public static final caption_width_limit:I = 0x7f0601a0

.field public static final captureimageVideo_marginTop:I = 0x7f060033

.field public static final change_media_size:I = 0x7f060034

.field public static final change_media_top:I = 0x7f060035

.field public static final common_device_height:I = 0x7f060003

.field public static final common_device_width:I = 0x7f060002

.field public static final controller_layout_fulscreen_height:I = 0x7f060036

.field public static final custom_tab_divider_h:I = 0x7f060037

.field public static final custom_tab_divider_margin_t:I = 0x7f060038

.field public static final custom_tab_divider_w:I = 0x7f060039

.field public static final delete_btn_height_withrecord_Sound:I = 0x7f060082

.field public static final delete_icon_width:I = 0x7f060060

.field public static final delete_launcher_height:I = 0x7f060177

.field public static final delete_launcher_leftemptywidth:I = 0x7f060107

.field public static final delete_project_chkbx_height:I = 0x7f060104

.field public static final delete_project_chkbx_marginR:I = 0x7f060102

.field public static final delete_project_chkbx_marginT:I = 0x7f060105

.field public static final delete_project_chkbx_width:I = 0x7f060103

.field public static final delete_show_limit:I = 0x7f06008b

.field public static final device_h:I = 0x7f06003a

.field public static final device_w:I = 0x7f06003b

.field public static final dragTextSize:I = 0x7f06003c

.field public static final drop_icon_size:I = 0x7f060171

.field public static final drop_marginTop:I = 0x7f06003d

.field public static final dropicon_marginLeft:I = 0x7f06003f

.field public static final droptext2_marginLeft:I = 0x7f060041

.field public static final droptext_height:I = 0x7f060040

.field public static final droptext_marginLeft:I = 0x7f060043

.field public static final droptext_marginTop:I = 0x7f06003e

.field public static final droptext_textSize:I = 0x7f060042

.field public static final edit_box_duration_margin_top:I = 0x7f060281

.field public static final edit_box_duration_textsize:I = 0x7f060280

.field public static final effect_picker_grid_verticalSpacing:I = 0x7f060044

.field public static final effect_picker_item_effectThumb_ht:I = 0x7f060046

.field public static final effect_picker_item_effectThumb_wd:I = 0x7f060047

.field public static final effect_picker_item_effectname_text:I = 0x7f060045

.field public static final effect_picker_item_ht:I = 0x7f060048

.field public static final effect_picker_item_text_h:I = 0x7f060049

.field public static final effect_picker_item_wd:I = 0x7f06004a

.field public static final effect_picker_margin_l:I = 0x7f06004b

.field public static final end_time_margin_top:I = 0x7f06004e

.field public static final export_check_radio_button_bkg_circle_diff:I = 0x7f060388

.field public static final export_check_radio_button_innerRadius:I = 0x7f060386

.field public static final export_check_radio_button_marginRight:I = 0x7f060385

.field public static final export_check_radio_button_outerCircle_strokeWidth:I = 0x7f060387

.field public static final export_check_radio_button_view_offset:I = 0x7f060389

.field public static final export_check_radio_button_width:I = 0x7f060384

.field public static final export_filename_change_marginleft:I = 0x7f060220

.field public static final export_filename_change_marginright:I = 0x7f060221

.field public static final export_itemlayout_height:I = 0x7f06004f

.field public static final export_itemlayout_width:I = 0x7f060052

.field public static final export_progressbar_filename_marginL:I = 0x7f060292

.field public static final export_progressbar_percentage_marginR:I = 0x7f0603aa

.field public static final export_resoln_item_height:I = 0x7f06037f

.field public static final export_resoln_option_text_marginLeft:I = 0x7f060380

.field public static final export_resoln_text_desc_size:I = 0x7f060383

.field public static final export_resoln_text_size:I = 0x7f060382

.field public static final export_resoln_text_views_gap:I = 0x7f060381

.field public static final export_username_edit_marginleft:I = 0x7f060223

.field public static final export_username_edit_marginright:I = 0x7f060224

.field public static final export_username_edit_margintop:I = 0x7f060222

.field public static final expsettings_divider1_margintop:I = 0x7f0601e0

.field public static final expsettings_divider2_margintop:I = 0x7f0601ea

.field public static final expsettings_divider_height:I = 0x7f0601df

.field public static final expsettings_layer1_editheight:I = 0x7f0601db

.field public static final expsettings_layer1_editmarginleft:I = 0x7f0601dc

.field public static final expsettings_layer1_editmargintop:I = 0x7f0601dd

.field public static final expsettings_layer1_editsize:I = 0x7f0601de

.field public static final expsettings_layer1_editwidth:I = 0x7f0601da

.field public static final expsettings_layer1_height:I = 0x7f0601d5

.field public static final expsettings_layer1_textheight:I = 0x7f0601d6

.field public static final expsettings_layer1_textmarginleft:I = 0x7f0601d7

.field public static final expsettings_layer1_textmargintop:I = 0x7f0601d8

.field public static final expsettings_layer1_textsize:I = 0x7f0601d9

.field public static final expsettings_layer2_height:I = 0x7f0601e1

.field public static final expsettings_layer2_spinnerheight:I = 0x7f0601e7

.field public static final expsettings_layer2_spinnermarginright:I = 0x7f0601e8

.field public static final expsettings_layer2_spinnermargintop:I = 0x7f0601e9

.field public static final expsettings_layer2_spinnerwidth:I = 0x7f0601e6

.field public static final expsettings_layer2_textheight:I = 0x7f0601e2

.field public static final expsettings_layer2_textmarginleft:I = 0x7f0601e3

.field public static final expsettings_layer2_textmargintop:I = 0x7f0601e4

.field public static final expsettings_layer2_textsize:I = 0x7f0601e5

.field public static final expsettings_layer3_checkboxheight:I = 0x7f0601f5

.field public static final expsettings_layer3_checkboxmarginright:I = 0x7f0601f6

.field public static final expsettings_layer3_checkboxwidth:I = 0x7f0601f4

.field public static final expsettings_layer3_checkmargintop:I = 0x7f0601f7

.field public static final expsettings_layer3_height:I = 0x7f0601eb

.field public static final expsettings_layer3_text1height:I = 0x7f0601ec

.field public static final expsettings_layer3_text1margintop:I = 0x7f0601ee

.field public static final expsettings_layer3_text1margnleft:I = 0x7f0601ed

.field public static final expsettings_layer3_text1size:I = 0x7f0601ef

.field public static final expsettings_layer3_text2height:I = 0x7f0601f0

.field public static final expsettings_layer3_text2marginleft:I = 0x7f0601f1

.field public static final expsettings_layer3_text2margintop:I = 0x7f0601f2

.field public static final expsettings_layer3_text2size:I = 0x7f0601f3

.field public static final expsettings_layout_height:I = 0x7f0601d4

.field public static final expsettings_layout_width:I = 0x7f0601d3

.field public static final fixed_prev_height:I = 0x7f060347

.field public static final fixed_prev_width:I = 0x7f060346

.field public static final folderuplevel_marginTop:I = 0x7f060053

.field public static final full_screen_button_margin_right:I = 0x7f06010d

.field public static final full_screen_button_padding_left:I = 0x7f060110

.field public static final full_screen_button_padding_top:I = 0x7f060111

.field public static final full_screen_seekbar_height:I = 0x7f060057

.field public static final full_screen_title_text_size:I = 0x7f06010e

.field public static final full_screen_titletext_margin_left:I = 0x7f06010f

.field public static final full_screen_volume_btn_padding_top:I = 0x7f06010b

.field public static final full_time_marginRight:I = 0x7f060054

.field public static final full_time_marginRight_fullscreen_mode:I = 0x7f060055

.field public static final full_time_padding_left_full_screen_mode:I = 0x7f0600f8

.field public static final full_time_textSize:I = 0x7f060056

.field public static final fullscreen_record_ht:I = 0x7f060058

.field public static final gesture_overlay_help_height:I = 0x7f0603b4

.field public static final gesture_overlay_help_marginT:I = 0x7f0603b5

.field public static final horizontal_media_edit_margin_top:I = 0x7f060376

.field public static final horizontal_title_layout_TopMargin:I = 0x7f06005a

.field public static final hover_folder_item_padding:I = 0x7f0601d2

.field public static final hover_folder_item_width:I = 0x7f0601d1

.field public static final hover_media_preview_height:I = 0x7f0601cd

.field public static final hover_media_preview_marginLeft:I = 0x7f0601d0

.field public static final hover_media_preview_marginTop:I = 0x7f0601cf

.field public static final hover_media_preview_radius:I = 0x7f0601ce

.field public static final hover_media_preview_width:I = 0x7f0601cc

.field public static final hover_media_preview_width_lite:I = 0x7f06033f

.field public static final hover_media_preview_window_gap:I = 0x7f060340

.field public static final hover_media_preview_window_height:I = 0x7f0601cb

.field public static final hover_media_preview_window_height_gap:I = 0x7f06033e

.field public static final hover_media_preview_window_width:I = 0x7f0601ca

.field public static final hover_media_preview_window_width_lite:I = 0x7f06033d

.field public static final image_folder_bucketname_height:I = 0x7f060226

.field public static final image_folder_bucketname_textsize:I = 0x7f060227

.field public static final image_picker_folder_canvas_height:I = 0x7f0602d4

.field public static final image_picker_folder_canvas_width:I = 0x7f0602d3

.field public static final image_picker_folder_item_width:I = 0x7f0602d2

.field public static final image_picker_folder_rot_thumb_height:I = 0x7f0602d6

.field public static final image_picker_folder_rot_thumb_width:I = 0x7f0602d5

.field public static final image_picker_folder_thumb_height:I = 0x7f0602d7

.field public static final image_picker_folder_thumb_width:I = 0x7f0602d8

.field public static final image_preview_height:I = 0x7f06005c

.field public static final image_preview_left_margin:I = 0x7f06005d

.field public static final image_preview_width:I = 0x7f06005e

.field public static final inProgress_notif_height:I = 0x7f0602ce

.field public static final inProgress_notif_margin_left:I = 0x7f0602cf

.field public static final inProgress_notif_margin_top:I = 0x7f0602d0

.field public static final inProgress_notif_textSize:I = 0x7f0602d1

.field public static final inProgress_notif_width:I = 0x7f0602cd

.field public static final indicator_icon_Width:I = 0x7f06005f

.field public static final item_bkg_endH:I = 0x7f0603cc

.field public static final item_bkg_endW:I = 0x7f0603cf

.field public static final item_bkg_end_marginL:I = 0x7f0603cd

.field public static final item_bkg_end_marginT:I = 0x7f0603ce

.field public static final item_parent_height:I = 0x7f0603bf

.field public static final item_parent_width:I = 0x7f0603be

.field public static final kenburn_actionbar_height:I = 0x7f060076

.field public static final kenburn_begin_thumbnail_marginleft:I = 0x7f060062

.field public static final kenburn_btn_marginLeft:I = 0x7f060063

.field public static final kenburn_btn_marginTop:I = 0x7f060064

.field public static final kenburn_checkbox_leftmargin:I = 0x7f060068

.field public static final kenburn_dilaog_text_size:I = 0x7f060066

.field public static final kenburn_edit_button_width_height:I = 0x7f060065

.field public static final kenburn_nevershow_text_leftmargin:I = 0x7f060067

.field public static final kenburn_nevershowdialog_chkparent_margintop:I = 0x7f060228

.field public static final kenburn_preview_button_height:I = 0x7f06006f

.field public static final kenburn_preview_button_marginBottom:I = 0x7f060073

.field public static final kenburn_preview_button_textSize:I = 0x7f060074

.field public static final kenburn_preview_button_width:I = 0x7f06006e

.field public static final kenburn_softkey_width:I = 0x7f060061

.field public static final kenburns_thumbnail_focus_height:I = 0x7f06006c

.field public static final kenburns_thumbnail_focus_width:I = 0x7f06006d

.field public static final kenburns_thumbnail_height:I = 0x7f06006b

.field public static final kenburns_thumbnail_marginLeft:I = 0x7f060070

.field public static final kenburns_thumbnail_marginTop:I = 0x7f060071

.field public static final kenburns_thumbnail_textSize:I = 0x7f060072

.field public static final kenburns_thumbnail_width:I = 0x7f06006a

.field public static final kenburns_zoom_image_view_padding:I = 0x7f060069

.field public static final launcher_itemdate_margintop:I = 0x7f060190

.field public static final launcher_itemdate_textsize:I = 0x7f060191

.field public static final launcher_itemdecsription_text_width:I = 0x7f060181

.field public static final launcher_itemdescription_margintop:I = 0x7f06017f

.field public static final launcher_itemdescription_textsize:I = 0x7f060180

.field public static final launcher_itemdownload_height:I = 0x7f060186

.field public static final launcher_itemdownload_marginleft:I = 0x7f060184

.field public static final launcher_itemdownload_margintop:I = 0x7f060183

.field public static final launcher_itemdownload_padding:I = 0x7f060187

.field public static final launcher_itemdownload_textSize:I = 0x7f060182

.field public static final launcher_itemdownload_width:I = 0x7f060185

.field public static final launcher_itemgap:I = 0x7f06017a

.field public static final launcher_itemicon_height:I = 0x7f06018b

.field public static final launcher_itemicon_marginleft:I = 0x7f060189

.field public static final launcher_itemicon_margintop:I = 0x7f060188

.field public static final launcher_itemicon_width:I = 0x7f06018a

.field public static final launcher_itemname_textsize:I = 0x7f06017e

.field public static final launcher_itemplay_height:I = 0x7f06018f

.field public static final launcher_itemplay_marginleft:I = 0x7f06018d

.field public static final launcher_itemplay_margintop:I = 0x7f06018c

.field public static final launcher_itemplay_width:I = 0x7f06018e

.field public static final launcher_itemtime_marginleft:I = 0x7f060193

.field public static final launcher_itemtime_margintop:I = 0x7f060192

.field public static final launcher_itemtime_textsize:I = 0x7f060194

.field public static final launcher_leftempty_width:I = 0x7f060178

.field public static final launcher_marginBottom:I = 0x7f060086

.field public static final launcher_margintop:I = 0x7f060085

.field public static final launcher_preview_marginLeft:I = 0x7f060195

.field public static final launcher_proj_name_width:I = 0x7f0602cc

.field public static final launcher_selitem_height:I = 0x7f060176

.field public static final launcher_selitem_width:I = 0x7f060174

.field public static final launcher_selitem_width_PLA:I = 0x7f060175

.field public static final launcher_selitemdetails_marginleft:I = 0x7f06017b

.field public static final launcher_selitemdetails_margintop:I = 0x7f06017c

.field public static final launcher_selitemdetails_width:I = 0x7f06017d

.field public static final launcher_selposareawidth:I = 0x7f060179

.field public static final launcher_unselitem_height:I = 0x7f060173

.field public static final launcher_unselitem_width:I = 0x7f060172

.field public static final layout_image_paddingLeft:I = 0x7f060077

.field public static final layout_image_paddingRight:I = 0x7f060078

.field public static final lite_actionbar_big_ic_w:I = 0x7f06031f

.field public static final lite_actionbar_ic_divider_h:I = 0x7f060323

.field public static final lite_actionbar_ic_divider_w:I = 0x7f060322

.field public static final lite_actionbar_ic_h:I = 0x7f06031e

.field public static final lite_actionbar_ic_pad_l:I = 0x7f060320

.field public static final lite_actionbar_ic_pad_r:I = 0x7f060321

.field public static final lite_actionbar_ic_w:I = 0x7f06031d

.field public static final lite_actionbar_pen_bar_h:I = 0x7f060341

.field public static final lite_actionbar_pen_bar_m_l:I = 0x7f06033c

.field public static final lite_actionbar_pen_bar_m_t:I = 0x7f06033b

.field public static final lite_actionbar_pen_bar_w:I = 0x7f060342

.field public static final lite_actionbar_pen_options_m_b:I = 0x7f060328

.field public static final lite_actionbar_pen_options_m_l:I = 0x7f060326

.field public static final lite_actionbar_pen_options_m_t:I = 0x7f060327

.field public static final lite_actionbar_toast_x:I = 0x7f060329

.field public static final lite_actionbar_toast_y:I = 0x7f06032a

.field public static final lite_actionbar_toast_y_portrait:I = 0x7f06032b

.field public static final lite_actionbar_winset_divider_h:I = 0x7f060325

.field public static final lite_actionbar_winset_divider_w:I = 0x7f060324

.field public static final lite_add_bgm_icon_top_margin:I = 0x7f06034f

.field public static final lite_bgm_add_text_size:I = 0x7f0602e4

.field public static final lite_bgm_gridview_left_margin:I = 0x7f0603d4

.field public static final lite_bgm_gridview_right_margin:I = 0x7f0603d2

.field public static final lite_bgm_left_text_margin_left:I = 0x7f060395

.field public static final lite_bgm_right_text_margin_right:I = 0x7f060396

.field public static final lite_bgm_text_bottom_margin:I = 0x7f060349

.field public static final lite_bgm_text_left_margin:I = 0x7f0603d1

.field public static final lite_bgm_text_margin_top:I = 0x7f060348

.field public static final lite_bgm_text_right_margin:I = 0x7f0603dd

.field public static final lite_bgm_text_size:I = 0x7f0602e3

.field public static final lite_bgm_text_top_margin:I = 0x7f060369

.field public static final lite_caption_height:I = 0x7f060370

.field public static final lite_caption_marginLeft:I = 0x7f060371

.field public static final lite_caption_marginRight:I = 0x7f060372

.field public static final lite_caption_marginTop:I = 0x7f060373

.field public static final lite_caption_rawbuffer_paddingLR:I = 0x7f06037e

.field public static final lite_caption_width:I = 0x7f06036f

.field public static final lite_divider_height:I = 0x7f060308

.field public static final lite_drag_cue_thickness:I = 0x7f060345

.field public static final lite_durationSelector_height:I = 0x7f06030b

.field public static final lite_durationView_marginLeft:I = 0x7f0603da

.field public static final lite_editMediaLayout_marginTop:I = 0x7f0603d5

.field public static final lite_editMedia_duration_marginBottom:I = 0x7f0602fa

.field public static final lite_editMedia_duration_marginLeft:I = 0x7f0602f9

.field public static final lite_editMedia_play_marginBottom:I = 0x7f0602fc

.field public static final lite_editMedia_play_marginRight:I = 0x7f0602fb

.field public static final lite_editMedia_play_width:I = 0x7f0602fd

.field public static final lite_edit_grid_duration_marginTop:I = 0x7f06037d

.field public static final lite_edit_grid_item_marginRight:I = 0x7f060343

.field public static final lite_edit_grid_item_marginTop:I = 0x7f060344

.field public static final lite_edit_grid_item_padding:I = 0x7f0603dc

.field public static final lite_edit_grid_item_padding_add_media:I = 0x7f06037a

.field public static final lite_edit_grid_padding_top:I = 0x7f0603db

.field public static final lite_edit_grid_play_marginTop:I = 0x7f06037c

.field public static final lite_edittextbox_width:I = 0x7f0603c1

.field public static final lite_media_editGrid_add_media_height:I = 0x7f060398

.field public static final lite_media_editGrid_add_media_width:I = 0x7f060397

.field public static final lite_media_editGrid_itemHeight:I = 0x7f0602f5

.field public static final lite_media_editGrid_itemHeight_border:I = 0x7f0602f6

.field public static final lite_media_editGrid_itemWidth:I = 0x7f060377

.field public static final lite_media_editGrid_itemWidth_border:I = 0x7f060378

.field public static final lite_media_editGrid_itemWidth_landscape:I = 0x7f060379

.field public static final lite_media_editGrid_marginLeft:I = 0x7f0602f3

.field public static final lite_media_editGrid_marginLeft_border:I = 0x7f0602f2

.field public static final lite_optionlayout_divider_height:I = 0x7f06030a

.field public static final lite_options_layout_height:I = 0x7f0602e6

.field public static final lite_penButton_Width:I = 0x7f060307

.field public static final lite_penButton_height:I = 0x7f060306

.field public static final lite_penButton_marginRight:I = 0x7f060304

.field public static final lite_penButton_marginTop:I = 0x7f060305

.field public static final lite_pen_canvas_height:I = 0x7f060332

.field public static final lite_pen_canvas_width:I = 0x7f060333

.field public static final lite_penmode_height:I = 0x7f0602e8

.field public static final lite_penmode_marginBottom:I = 0x7f0602ea

.field public static final lite_penmode_width:I = 0x7f0602e7

.field public static final lite_previewViewGroup_height:I = 0x7f0602fe

.field public static final lite_preview_height:I = 0x7f0602db

.field public static final lite_preview_play_left_offset:I = 0x7f0602df

.field public static final lite_preview_play_size:I = 0x7f0602dd

.field public static final lite_preview_play_top_offset:I = 0x7f0602de

.field public static final lite_preview_width:I = 0x7f0602dc

.field public static final lite_previewarea_height:I = 0x7f0602da

.field public static final lite_previewarea_width:I = 0x7f0602d9

.field public static final lite_previewlayout_divider_height:I = 0x7f060309

.field public static final lite_previewlayout_marginLeft:I = 0x7f06032f

.field public static final lite_previewlayout_marginRight:I = 0x7f060330

.field public static final lite_previewlayout_marginTop:I = 0x7f060331

.field public static final lite_rightArrow_icon_marginRight:I = 0x7f06032d

.field public static final lite_seek_view_left:I = 0x7f0602e5

.field public static final lite_seekbar_h:I = 0x7f060339

.field public static final lite_seekbar_left_margin:I = 0x7f060336

.field public static final lite_seekbar_margin_t:I = 0x7f06033a

.field public static final lite_seekbar_parent_h:I = 0x7f060334

.field public static final lite_seekbar_parent_margin_t:I = 0x7f060335

.field public static final lite_seekbar_right_margin:I = 0x7f060337

.field public static final lite_selected_duration_textSize:I = 0x7f06032c

.field public static final lite_storyboard_time_margin_t:I = 0x7f060338

.field public static final lite_themeSelector_horizontalSpacing:I = 0x7f06030c

.field public static final lite_themeSelector_margin_r:I = 0x7f060312

.field public static final lite_themeSelector_paddingRight:I = 0x7f0603d8

.field public static final lite_themeSelector_paddingTop:I = 0x7f0603d9

.field public static final lite_themeSelector_padding_b:I = 0x7f060311

.field public static final lite_themeSelector_padding_l:I = 0x7f06030f

.field public static final lite_themeSelector_padding_r:I = 0x7f060310

.field public static final lite_themeSelector_padding_t:I = 0x7f06030e

.field public static final lite_themeSelector_verticalSpacing:I = 0x7f06030d

.field public static final lite_theme_item_height:I = 0x7f060316

.field public static final lite_theme_item_width:I = 0x7f060314

.field public static final lite_theme_marginL:I = 0x7f0602e9

.field public static final lite_theme_name_height:I = 0x7f060319

.field public static final lite_theme_name_marginL:I = 0x7f0602ee

.field public static final lite_theme_name_marginT:I = 0x7f0602ef

.field public static final lite_theme_name_marginTop:I = 0x7f06031a

.field public static final lite_theme_name_paddingTop:I = 0x7f0602f0

.field public static final lite_theme_name_padding_bottom:I = 0x7f0603c0

.field public static final lite_theme_name_padding_top:I = 0x7f0602f1

.field public static final lite_theme_name_textSize:I = 0x7f06031b

.field public static final lite_theme_thumb_height:I = 0x7f060318

.field public static final lite_theme_thumb_padding:I = 0x7f06031c

.field public static final lite_theme_thumb_width:I = 0x7f060317

.field public static final lite_theme_thumbnail_height:I = 0x7f0602ed

.field public static final lite_theme_thumbnail_padding:I = 0x7f0602eb

.field public static final lite_theme_thumbnail_width:I = 0x7f0602ec

.field public static final lite_themegrid_height:I = 0x7f06032e

.field public static final lite_themelistLayout_height:I = 0x7f0602f7

.field public static final lite_themelist_marginTop:I = 0x7f0602f8

.field public static final lite_themeselector_pddingBottom:I = 0x7f0603c6

.field public static final lite_time_view_left:I = 0x7f0602e1

.field public static final lite_time_view_right:I = 0x7f0602e0

.field public static final lite_time_view_size:I = 0x7f0602e2

.field public static final lite_titleEffect_layoutHeight:I = 0x7f0603c7

.field public static final lite_titleEffect_marginTop:I = 0x7f0603c2

.field public static final lite_titleEffect_paddingLeft:I = 0x7f0603c5

.field public static final lite_titleEffect_paddingTop:I = 0x7f0603c4

.field public static final lite_titleEffect_textSize:I = 0x7f0603c3

.field public static final lite_titlebox_height:I = 0x7f0602ff

.field public static final lite_titlebox_marginLeft:I = 0x7f060301

.field public static final lite_titlebox_marginRight:I = 0x7f060302

.field public static final lite_titlebox_marginTop:I = 0x7f060303

.field public static final lite_titlebox_width:I = 0x7f060300

.field public static final lite_videoeditor_launch_checkbox_margin_b:I = 0x7f06038f

.field public static final lite_videoeditor_launch_checkbox_margin_l:I = 0x7f06038e

.field public static final lite_videoeditor_launch_checkbox_margin_t:I = 0x7f06038d

.field public static final lite_videoeditor_launch_checkbox_padding_l:I = 0x7f060390

.field public static final lite_videoeditor_launch_content_margin_l:I = 0x7f06038a

.field public static final lite_videoeditor_launch_content_margin_r:I = 0x7f06038b

.field public static final lite_videoeditor_launch_content_margin_t:I = 0x7f06038c

.field public static final main_edit_screen_draglayer_height:I = 0x7f060229

.field public static final main_edit_screen_text_paddingTop:I = 0x7f0602b8

.field public static final main_edit_screen_timedisplay_paddingtop:I = 0x7f06022a

.field public static final main_edit_screen_timedisplay_textsize:I = 0x7f06022b

.field public static final main_screen_icons_margintop:I = 0x7f0602b6

.field public static final main_soundeffect_item_height:I = 0x7f0601bc

.field public static final main_soundeffect_item_width:I = 0x7f0601bb

.field public static final mediaParentLayout_height:I = 0x7f06007a

.field public static final mediaParentLayout_width:I = 0x7f060079

.field public static final mediaTypeLayout_width:I = 0x7f06007b

.field public static final media_edit_grid_marginT:I = 0x7f0603ae

.field public static final media_edit_scroll_bottom_limit:I = 0x7f060391

.field public static final media_edit_scroll_left_limit:I = 0x7f060394

.field public static final media_edit_scroll_right_limit:I = 0x7f060393

.field public static final media_edit_scroll_top_limit:I = 0x7f060392

.field public static final mediatextbox_margintop:I = 0x7f060168

.field public static final menuitemwidth:I = 0x7f06028d

.field public static final musicName_marginLeft:I = 0x7f06020d

.field public static final music_album_albumname_textsize:I = 0x7f06022c

.field public static final music_album_artistname_textsize:I = 0x7f06022d

.field public static final music_album_picker_pickerview_divider_height:I = 0x7f06022e

.field public static final music_album_picker_pickerview_paddingleft:I = 0x7f06022f

.field public static final music_album_picker_pickerview_paddingright:I = 0x7f060230

.field public static final music_album_picker_textview_textsize:I = 0x7f060231

.field public static final music_categories_folder_icon_width_height:I = 0x7f060234

.field public static final music_categories_layout_height:I = 0x7f060232

.field public static final music_categories_layout_padding_left:I = 0x7f060233

.field public static final music_categories_parent_divider_height:I = 0x7f06007c

.field public static final music_categories_parent_padding_left_right:I = 0x7f06007d

.field public static final music_categories_parent_padding_top_bottom:I = 0x7f06007e

.field public static final music_categories_textview_marginleft:I = 0x7f060235

.field public static final music_categories_textview_textsize:I = 0x7f060236

.field public static final music_equalizer_marginRight:I = 0x7f0602a6

.field public static final music_option_parent_textview_option_contentcount_textsize:I = 0x7f06023c

.field public static final music_option_parent_textview_text_textsize:I = 0x7f06023b

.field public static final music_options_item_width:I = 0x7f06007f

.field public static final music_options_parent_layout_height:I = 0x7f060237

.field public static final music_options_parent_layout_marginleft:I = 0x7f060239

.field public static final music_options_parent_layout_paddingleft:I = 0x7f060238

.field public static final music_options_parent_textview_margintop:I = 0x7f06023a

.field public static final music_playbtn_marginRight:I = 0x7f0602a5

.field public static final music_songs_picker_pickerview_divider_height:I = 0x7f06023d

.field public static final music_songs_picker_pickerview_paddingleft:I = 0x7f06023e

.field public static final music_text_size:I = 0x7f060080

.field public static final music_title_height:I = 0x7f060363

.field public static final musicbar_delete_btn_height:I = 0x7f060296

.field public static final musicbar_delete_btn_right_margin:I = 0x7f060293

.field public static final musicbar_height:I = 0x7f060081

.field public static final musicbar_height_withrecord:I = 0x7f06008d

.field public static final musicbar_height_withrecord_sound:I = 0x7f06008e

.field public static final musicbar_trackname_Bottom_margin:I = 0x7f060294

.field public static final none_theme_image_height:I = 0x7f060084

.field public static final none_theme_image_mB:I = 0x7f060087

.field public static final none_theme_image_mL:I = 0x7f060088

.field public static final none_theme_image_mR:I = 0x7f060089

.field public static final none_theme_image_mT:I = 0x7f06008a

.field public static final none_theme_image_width:I = 0x7f060083

.field public static final notification_icon_layout_height:I = 0x7f0603a0

.field public static final notification_layout_export_done_textsize:I = 0x7f06024d

.field public static final notification_layout_imageview_layout_marginleft:I = 0x7f060247

.field public static final notification_layout_imageview_layout_margintop:I = 0x7f060248

.field public static final notification_layout_imageview_layout_padding:I = 0x7f060246

.field public static final notification_layout_imageview_layout_width_height:I = 0x7f060245

.field public static final notification_layout_layout_marginleft:I = 0x7f060240

.field public static final notification_layout_layout_marginleft_k:I = 0x7f0603a2

.field public static final notification_layout_layout_marginright:I = 0x7f060241

.field public static final notification_layout_layout_marginright_k:I = 0x7f0603a3

.field public static final notification_layout_layout_margintop:I = 0x7f060242

.field public static final notification_layout_layout_margintop_k:I = 0x7f0603a4

.field public static final notification_layout_layout_padding:I = 0x7f06023f

.field public static final notification_layout_percent_prog_textsize:I = 0x7f06024c

.field public static final notification_layout_progressbar_height:I = 0x7f060244

.field public static final notification_layout_status_text_height:I = 0x7f060243

.field public static final notification_layout_textview_exportfilname_textsize:I = 0x7f060249

.field public static final notification_layout_textview_exportfilname_textsize_k:I = 0x7f0603a5

.field public static final notification_layout_textview_exportfilname_textsize_l:I = 0x7f0603a9

.field public static final notification_layout_textview_statustext_textsize:I = 0x7f06024a

.field public static final notification_layout_textview_statustext_textsize_l:I = 0x7f06024b

.field public static final notification_progressbar_margin_top:I = 0x7f0603a1

.field public static final options_layout_marginTop:I = 0x7f0603d0

.field public static final options_layout_text_item_height:I = 0x7f06008f

.field public static final options_layout_text_item_width:I = 0x7f060090

.field public static final overlay_h:I = 0x7f0603b0

.field public static final overlay_help_content_marginL:I = 0x7f0603b7

.field public static final overlay_help_content_marginT:I = 0x7f0603b9

.field public static final overlay_help_content_textSize:I = 0x7f0603b6

.field public static final overlay_w:I = 0x7f0603af

.field public static final play_confirm_message_textsize:I = 0x7f06024e

.field public static final play_confirm_message_textsize_marginL:I = 0x7f060250

.field public static final play_confirm_textview_message_padding:I = 0x7f06024f

.field public static final play_icon_height_width:I = 0x7f060091

.field public static final play_text_icons_margintop:I = 0x7f0602b7

.field public static final playbutton_height:I = 0x7f06015f

.field public static final playbutton_height_fullscreen_mode:I = 0x7f060092

.field public static final playbutton_margintop:I = 0x7f060163

.field public static final playbutton_width:I = 0x7f06015e

.field public static final playbutton_width_fullscreen_mode:I = 0x7f060093

.field public static final playbuttonlayout_marginleft:I = 0x7f060169

.field public static final playbuttonlayout_margintop:I = 0x7f06016a

.field public static final playbuttontext_margintop:I = 0x7f060164

.field public static final playbuttontextsize:I = 0x7f060156

.field public static final player_control_parent_width:I = 0x7f06005b

.field public static final player_controller_margin_bottom_full_screen_mode:I = 0x7f0600fa

.field public static final player_controller_margin_top_full_screen_mode:I = 0x7f0600f9

.field public static final preview_bottom_divider_margine_r:I = 0x7f060362

.field public static final preview_container_w:I = 0x7f060094

.field public static final preview_layout_margin_l:I = 0x7f060358

.field public static final preview_layout_margin_t:I = 0x7f060357

.field public static final preview_play_img_size:I = 0x7f0601c9

.field public static final preview_play_marginLeft:I = 0x7f060095

.field public static final preview_surfaceview_pad_b:I = 0x7f060096

.field public static final preview_surfaceview_pad_l:I = 0x7f060097

.field public static final preview_surfaceview_pad_r:I = 0x7f060098

.field public static final preview_surfaceview_pad_t:I = 0x7f060099

.field public static final preview_vidoepreview_margin_top_bottom_left_right:I = 0x7f060251

.field public static final previewscreen_record_ht:I = 0x7f06009e

.field public static final progress_bar_max_height:I = 0x7f06035d

.field public static final progress_bar_min_height:I = 0x7f06035c

.field public static final progress_bar_padding_l:I = 0x7f06035a

.field public static final progress_bar_padding_r:I = 0x7f06035b

.field public static final progress_bar_w:I = 0x7f060359

.field public static final progress_popup_layout_height:I = 0x7f060290

.field public static final progress_popup_layout_width:I = 0x7f06028f

.field public static final progress_popup_textsize:I = 0x7f060291

.field public static final project_date_delete_marginL:I = 0x7f060100

.field public static final project_date_delete_marginT:I = 0x7f060101

.field public static final project_date_delete_textsize:I = 0x7f060050

.field public static final project_date_marginTop:I = 0x7f06009f

.field public static final project_delete_launcher_marginT:I = 0x7f0600fe

.field public static final project_delete_layout_height:I = 0x7f0600f3

.field public static final project_delete_selItem_height:I = 0x7f0600f4

.field public static final project_delete_selItem_width:I = 0x7f0600f2

.field public static final project_frame_marginBottom:I = 0x7f0600a0

.field public static final project_frame_marginLeft:I = 0x7f0600a1

.field public static final project_frame_marginRight:I = 0x7f0600a2

.field public static final project_frame_marginTop:I = 0x7f0600a3

.field public static final project_image_h:I = 0x7f0600a4

.field public static final project_image_height:I = 0x7f0600a5

.field public static final project_image_marginBottom:I = 0x7f0600a6

.field public static final project_image_marginLeft:I = 0x7f0600a7

.field public static final project_image_marginRight:I = 0x7f0600a8

.field public static final project_image_marginTop:I = 0x7f0600a9

.field public static final project_image_width:I = 0x7f0600aa

.field public static final project_layout_height:I = 0x7f0600ab

.field public static final project_layout_width:I = 0x7f0600ac

.field public static final project_list_marginTop:I = 0x7f0600ad

.field public static final project_name_change_marginleft_right:I = 0x7f06028b

.field public static final project_name_change_margintop:I = 0x7f06028c

.field public static final project_name_delete_marginL:I = 0x7f060106

.field public static final project_name_delete_marginT:I = 0x7f0600ff

.field public static final project_name_delete_textsize:I = 0x7f060051

.field public static final project_name_marginTop:I = 0x7f0600ae

.field public static final project_time_marginL:I = 0x7f060157

.field public static final project_time_marginT:I = 0x7f060158

.field public static final project_time_marginTop:I = 0x7f0600af

.field public static final project_time_textsize:I = 0x7f06015c

.field public static final recom_new_icon_height:I = 0x7f0602c9

.field public static final recom_new_icon_textsize:I = 0x7f0602cb

.field public static final recom_new_icon_width:I = 0x7f0602ca

.field public static final record_cancel_height:I = 0x7f0600b0

.field public static final record_cancel_width:I = 0x7f0600b1

.field public static final record_height:I = 0x7f0600b2

.field public static final record_indicator_center:I = 0x7f06019e

.field public static final record_layout_imageview_height:I = 0x7f060255

.field public static final record_layout_imageview_margin_left:I = 0x7f060252

.field public static final record_layout_imageview_marginleft:I = 0x7f060256

.field public static final record_layout_textview_recordtime_margin_right:I = 0x7f060253

.field public static final record_layout_textview_recordtime_textsize:I = 0x7f060254

.field public static final record_layout_toggle_button_marginright:I = 0x7f060257

.field public static final record_pause_width:I = 0x7f0600b3

.field public static final record_stop_width:I = 0x7f0600b4

.field public static final record_warning_dialog_height:I = 0x7f0600b5

.field public static final record_warning_dialog_width:I = 0x7f0600b6

.field public static final record_warning_layout_margintop:I = 0x7f060259

.field public static final record_warning_layout_textview_margintop:I = 0x7f060258

.field public static final record_width:I = 0x7f0600b7

.field public static final recordbar_height:I = 0x7f0600b8

.field public static final recordicon_size:I = 0x7f0601c5

.field public static final resol_category_marginLeft:I = 0x7f0600b9

.field public static final resol_category_textSize:I = 0x7f0600ba

.field public static final resol_category_width:I = 0x7f0600bb

.field public static final resol_icon_marginLeft:I = 0x7f0600bc

.field public static final resol_icon_marginbottom:I = 0x7f0600be

.field public static final resol_icon_margintop:I = 0x7f0600bd

.field public static final resol_value_marginRight:I = 0x7f0600bf

.field public static final right_divider_margin_r:I = 0x7f06037b

.field public static final right_margin_trim:I = 0x7f060298

.field public static final sample_video_text_height:I = 0x7f0602bf

.field public static final sample_video_text_margin_r:I = 0x7f0602c2

.field public static final sample_video_text_size:I = 0x7f0602c1

.field public static final sample_video_text_width:I = 0x7f0602c0

.field public static final seek_thumb_height:I = 0x7f0601f8

.field public static final seek_thumb_offset:I = 0x7f06034a

.field public static final seek_thumb_offset_top:I = 0x7f0601f9

.field public static final seek_thumb_width:I = 0x7f0601fa

.field public static final seekbar_top:I = 0x7f0601fb

.field public static final select_all_checkbox_marginLR:I = 0x7f0600ee

.field public static final select_all_checkbox_marginT:I = 0x7f0600ef

.field public static final select_all_chkbx_height:I = 0x7f060112

.field public static final select_all_chkbx_width:I = 0x7f060113

.field public static final select_all_text_marginL:I = 0x7f0600fd

.field public static final select_all_text_marginT:I = 0x7f0600fc

.field public static final select_all_text_size:I = 0x7f060114

.field public static final sharevia_dialog_width:I = 0x7f06021e

.field public static final sharevia_gridview_padding:I = 0x7f06021b

.field public static final sharevia_gridview_paddingbottom:I = 0x7f06021d

.field public static final sharevia_gridview_paddingtop:I = 0x7f06021c

.field public static final sharevia_item_image_height:I = 0x7f060215

.field public static final sharevia_item_image_marginleft:I = 0x7f060216

.field public static final sharevia_item_image_margintop:I = 0x7f060217

.field public static final sharevia_item_image_width:I = 0x7f060214

.field public static final sharevia_item_layout_height:I = 0x7f060213

.field public static final sharevia_item_layout_width:I = 0x7f060212

.field public static final sharevia_item_text_height:I = 0x7f060218

.field public static final sharevia_item_text_margintop:I = 0x7f060219

.field public static final sharevia_item_text_size:I = 0x7f06021a

.field public static final small_folder_thumbnail_marginTop:I = 0x7f0602ac

.field public static final songArtistName_marginBottom:I = 0x7f0600c0

.field public static final songArtistName_textSize:I = 0x7f0600c1

.field public static final songDetails_margintop:I = 0x7f0600c2

.field public static final songDuration_paddingTop:I = 0x7f0600cc

.field public static final songName_marginTop:I = 0x7f0600c3

.field public static final songName_textSize:I = 0x7f0600c4

.field public static final songName_width:I = 0x7f0600c5

.field public static final songRadioBtn_height:I = 0x7f0600c6

.field public static final songRadioBtn_marginLeft:I = 0x7f0600c7

.field public static final songRadioBtn_marginRight:I = 0x7f0600c8

.field public static final songRadioBtn_width:I = 0x7f0600c9

.field public static final songsDuration_marginRight:I = 0x7f0600ca

.field public static final songsDuration_textSize:I = 0x7f0600cb

.field public static final songs_albums_layout_width:I = 0x7f0600ce

.field public static final songs_frame_selected_padding:I = 0x7f0602aa

.field public static final songs_playbtn_width:I = 0x7f0602be

.field public static final songslayout_album_art_height:I = 0x7f0600cf

.field public static final songslayout_album_art_selected_height:I = 0x7f0602bb

.field public static final songslayout_height:I = 0x7f0600cd

.field public static final soundInfo_RightMargin:I = 0x7f060297

.field public static final soundInfo_TopMargin:I = 0x7f06020e

.field public static final sound_effect_item_FrameLayout01_ht:I = 0x7f0600d0

.field public static final sound_effect_item_FrameLayout01_ml:I = 0x7f0600d1

.field public static final sound_effect_item_FrameLayout01_wd:I = 0x7f0600d2

.field public static final sound_effect_item_icon_ht:I = 0x7f0600d3

.field public static final sound_effect_item_icon_ml:I = 0x7f0600d4

.field public static final sound_effect_item_icon_mt:I = 0x7f0600d5

.field public static final sound_effect_item_icon_wd:I = 0x7f0600d6

.field public static final sound_effect_item_minHeight:I = 0x7f0600d7

.field public static final sound_effect_item_musicfileduration_minWidth:I = 0x7f0600d8

.field public static final sound_effect_item_musicfileduration_ml:I = 0x7f0600d9

.field public static final sound_effect_item_musicfileduration_padding_right:I = 0x7f0600da

.field public static final sound_effect_item_musicfileduration_text:I = 0x7f0600db

.field public static final sound_effect_item_musicfilename_text:I = 0x7f0600dc

.field public static final sound_effect_item_musicfilename_wd:I = 0x7f0600dd

.field public static final sound_effect_item_musicinsert_ht:I = 0x7f0600de

.field public static final sound_effect_item_musicinsert_mr:I = 0x7f0600df

.field public static final sound_effect_item_musicinsert_wd:I = 0x7f0600e0

.field public static final sound_effect_width:I = 0x7f0600e1

.field public static final sound_effects_layout_marginLeft:I = 0x7f0600e2

.field public static final sound_effects_layout_padding_left:I = 0x7f06025a

.field public static final sound_effects_layout_relativelayout_height:I = 0x7f06025e

.field public static final sound_effects_subitem_imageview_marginright:I = 0x7f060263

.field public static final sound_effects_subitem_imageview_width_height:I = 0x7f060262

.field public static final sound_effects_subitem_relativelayout_width:I = 0x7f06025f

.field public static final sound_effects_subitem_textview_musicfilename_marginleft:I = 0x7f060260

.field public static final sound_effects_subitem_textview_musicfilename_textsize:I = 0x7f060261

.field public static final sound_effects_subviewgroup_imageview_ivUpFolderSES_marginleft:I = 0x7f060267

.field public static final sound_effects_subviewgroup_imageview_ivUpFolderSES_width_height:I = 0x7f060266

.field public static final sound_effects_subviewgroup_lvSoundEffectsSublistview_dividerheight:I = 0x7f06026c

.field public static final sound_effects_subviewgroup_relativelayout_height:I = 0x7f060264

.field public static final sound_effects_subviewgroup_rlHeader_height:I = 0x7f060265

.field public static final sound_effects_subviewgroup_textview_tvHRSES_height:I = 0x7f06026b

.field public static final sound_effects_subviewgroup_textview_tvSoundEffectsItemNameSES_marginleft:I = 0x7f060269

.field public static final sound_effects_subviewgroup_textview_tvSoundEffectsItemNameSES_textsize:I = 0x7f06026a

.field public static final sound_effects_subviewgroup_textview_tvSoundEffectsItemNameSES_width:I = 0x7f060268

.field public static final sound_effects_textview_soundTrackCount_textsize:I = 0x7f06025d

.field public static final sound_effects_textview_tvMusicItemNameSEI_margintop:I = 0x7f06025b

.field public static final sound_effects_textview_tvMusicItemNameSEI_textsize:I = 0x7f06025c

.field public static final sound_text_size:I = 0x7f0602a0

.field public static final sound_vertical_seekbar1_margin_left:I = 0x7f0600e6

.field public static final sound_vertical_seekbar2_margin_left:I = 0x7f0600e7

.field public static final sound_vertical_seekbar_height:I = 0x7f0600e3

.field public static final sound_vertical_seekbar_margin_top:I = 0x7f0600e4

.field public static final sound_vertical_seekbar_width:I = 0x7f0600e5

.field public static final soundbar_delete_btn_height:I = 0x7f060295

.field public static final soundbar_height:I = 0x7f0600e8

.field public static final soundbar_topmargin:I = 0x7f0600e9

.field public static final soundeffect_item_image_height:I = 0x7f0601c1

.field public static final soundeffect_item_image_width:I = 0x7f0601c0

.field public static final soundeffect_item_text_height:I = 0x7f0601bf

.field public static final soundeffect_item_text_marginleft:I = 0x7f0601bd

.field public static final soundeffect_item_text_width:I = 0x7f0601be

.field public static final soundeffect_layout_divider_height:I = 0x7f0601c3

.field public static final soundeffect_layout_marginleft:I = 0x7f0601c4

.field public static final soundeffectlist_margintop:I = 0x7f0601c2

.field public static final soundeffecttextsize:I = 0x7f06015a

.field public static final split_btn_margin_left:I = 0x7f0600ea

.field public static final split_btn_margin_top:I = 0x7f0600eb

.field public static final split_height:I = 0x7f0600ec

.field public static final split_image_height_width:I = 0x7f0602ad

.field public static final split_image_marginleft:I = 0x7f0602ae

.field public static final split_image_margintop:I = 0x7f0602af

.field public static final split_marker_height:I = 0x7f0601c7

.field public static final split_marker_margin:I = 0x7f0601c8

.field public static final split_marker_width:I = 0x7f0601c6

.field public static final split_text_height:I = 0x7f0602b0

.field public static final split_textsize:I = 0x7f0602b1

.field public static final split_width:I = 0x7f0600ed

.field public static final story_time_fullscreen_marginTop:I = 0x7f0600f0

.field public static final story_time_marginBottom:I = 0x7f0600f1

.field public static final story_time_marginLeft:I = 0x7f0600f5

.field public static final story_time_marginLeft_fullscreen_mode:I = 0x7f0600f6

.field public static final story_time_padding_right_full_screen_mode:I = 0x7f0600f7

.field public static final story_time_textSize:I = 0x7f0600fb

.field public static final storyboard_baseline_paddingLeft:I = 0x7f060108

.field public static final storyboard_baseline_paddingLeft_split:I = 0x7f060109

.field public static final storyboard_fragment_margin_top:I = 0x7f06010a

.field public static final storyboard_height:I = 0x7f060115

.field public static final storyboard_marginTop:I = 0x7f060116

.field public static final storyboard_paddingLeft:I = 0x7f060117

.field public static final storyboard_paddingRight:I = 0x7f060118

.field public static final storyboard_paddingTop:I = 0x7f060119

.field public static final storyboard_paddingleft_trim_play:I = 0x7f0602a8

.field public static final text_effect_none_size:I = 0x7f0602b2

.field public static final text_effect_size:I = 0x7f0602b3

.field public static final textbox_height:I = 0x7f06015d

.field public static final textbox_margintop:I = 0x7f06019d

.field public static final textbox_width:I = 0x7f06015b

.field public static final texttab_item_gap:I = 0x7f06011a

.field public static final themeGallerPaddingLR:I = 0x7f0603bc

.field public static final themeGalleryHeight:I = 0x7f0603bb

.field public static final theme_background_lastEl_widthH:I = 0x7f060315

.field public static final theme_gallery_parent_margin_l:I = 0x7f0603d3

.field public static final theme_gallery_parent_padding_T:I = 0x7f0603bd

.field public static final theme_gallery_parent_padding_l:I = 0x7f06036d

.field public static final theme_gallery_parent_padding_r:I = 0x7f06036e

.field public static final theme_gallery_width:I = 0x7f06036a

.field public static final theme_gallery_width_UHDContent:I = 0x7f06036c

.field public static final theme_gallery_width_m2:I = 0x7f06036b

.field public static final theme_item_marginB:I = 0x7f0603c8

.field public static final theme_item_marginL:I = 0x7f0603c9

.field public static final theme_item_marginR:I = 0x7f0603cb

.field public static final theme_item_marginT:I = 0x7f0603ca

.field public static final theme_list_item_width:I = 0x7f0603d6

.field public static final theme_list_last_item_width:I = 0x7f0603d7

.field public static final theme_name_text_height:I = 0x7f0602f4

.field public static final theme_parent_width:I = 0x7f060374

.field public static final theme_thumbnailview_icon_list_marginleft:I = 0x7f06026e

.field public static final theme_thumbnailview_icon_list_margintop:I = 0x7f06026d

.field public static final theme_vide_length_margin_r:I = 0x7f060360

.field public static final theme_vide_length_margin_t:I = 0x7f060361

.field public static final theme_video_length_height:I = 0x7f06035f

.field public static final theme_video_length_width:I = 0x7f06035e

.field public static final themebased_storyboard_paddingLeft:I = 0x7f06011b

.field public static final themebased_storyboard_paddingRight:I = 0x7f06011c

.field public static final themebased_storyboard_paddingTop:I = 0x7f06011d

.field public static final themebased_thumbnail_viewgroup_height:I = 0x7f06011e

.field public static final themelist_layout_margin_b:I = 0x7f060313

.field public static final threshold_trim_diff:I = 0x7f060125

.field public static final thumbnail_child_paddingBottom:I = 0x7f060126

.field public static final thumbnail_child_paddingTop:I = 0x7f060127

.field public static final thumbnail_viewgroup_height:I = 0x7f060128

.field public static final thumbnailview_icon_list_marginleft:I = 0x7f060270

.field public static final thumbnailview_icon_list_margintop:I = 0x7f06026f

.field public static final thumbnailview_imageview_rightClueView_marginbottom:I = 0x7f060273

.field public static final thumbnailview_imageview_rightClueView_marginleft:I = 0x7f060274

.field public static final thumbnailview_imageview_rightClueView_margintop:I = 0x7f060275

.field public static final thumbnailview_imageview_rightClueView_width:I = 0x7f060272

.field public static final thumbnailview_trimbar_left_marginleft:I = 0x7f060271

.field public static final time_slot_bottommargin:I = 0x7f060210

.field public static final time_slot_rightmargin:I = 0x7f06020f

.field public static final time_slot_textsize:I = 0x7f060211

.field public static final time_text_margin_bottom:I = 0x7f0601fc

.field public static final time_text_margin_right:I = 0x7f0601fd

.field public static final time_text_size:I = 0x7f0601fe

.field public static final timeline_addtitle_marginleft:I = 0x7f060277

.field public static final timeline_addtitle_marginright:I = 0x7f060278

.field public static final timeline_addtitle_margintop:I = 0x7f060276

.field public static final timeline_addtitle_textsize:I = 0x7f060279

.field public static final timeline_shadow_height:I = 0x7f0602a1

.field public static final timeline_shadow_width:I = 0x7f0602a2

.field public static final timeline_thumb_height:I = 0x7f0601ff

.field public static final timeline_thumb_width:I = 0x7f060200

.field public static final timeline_view_parent_audioBars_margintop:I = 0x7f06027a

.field public static final timeline_view_parent_edit_box_width:I = 0x7f06027f

.field public static final timeline_view_parent_paddingtop:I = 0x7f06027b

.field public static final timeline_view_parent_recordindicator_height:I = 0x7f06027e

.field public static final timeline_view_parent_recordindicator_margintop:I = 0x7f06027c

.field public static final timeline_view_parent_recordindicator_width:I = 0x7f06027d

.field public static final timelineprogress_fullscreen_height:I = 0x7f060129

.field public static final timelineprogress_fullscreen_width:I = 0x7f06012a

.field public static final timeview_layout_addicon_size:I = 0x7f06012b

.field public static final timeview_layout_addiconbot:I = 0x7f06012c

.field public static final timeview_layout_addicontop:I = 0x7f06012d

.field public static final timeview_layout_addmedia_text:I = 0x7f06012e

.field public static final title_control_layout_margin_top:I = 0x7f06010c

.field public static final toggle_button_record_height:I = 0x7f06012f

.field public static final toggle_button_record_width:I = 0x7f060130

.field public static final trackIcon_marginLeft:I = 0x7f06020c

.field public static final track_icon_size:I = 0x7f06008c

.field public static final transition_align_x:I = 0x7f060131

.field public static final transition_align_y:I = 0x7f060132

.field public static final transition_align_y_offset:I = 0x7f060075

.field public static final transition_horz_spacing:I = 0x7f060123

.field public static final transition_image_height:I = 0x7f06013a

.field public static final transition_image_width:I = 0x7f06013b

.field public static final transition_left_margin:I = 0x7f060133

.field public static final transition_pop_paddingB:I = 0x7f06011f

.field public static final transition_pop_paddingL:I = 0x7f060121

.field public static final transition_pop_paddingR:I = 0x7f060122

.field public static final transition_pop_paddingT:I = 0x7f060120

.field public static final transition_right_margin:I = 0x7f060134

.field public static final transition_select_grid_height:I = 0x7f060135

.field public static final transition_select_grid_width:I = 0x7f060136

.field public static final transition_size:I = 0x7f060137

.field public static final transition_vert_spacing:I = 0x7f060124

.field public static final transitionview_height:I = 0x7f060138

.field public static final transitionview_width:I = 0x7f060139

.field public static final trim_bar_gap:I = 0x7f060059

.field public static final trim_bar_height:I = 0x7f060201

.field public static final trim_bar_margin_bottom:I = 0x7f060202

.field public static final trim_bar_margin_left:I = 0x7f060203

.field public static final trim_bar_margin_right:I = 0x7f060204

.field public static final trim_bg_height:I = 0x7f060205

.field public static final trim_gap:I = 0x7f060206

.field public static final trim_play_marker_width:I = 0x7f0602a7

.field public static final trim_thumb_height:I = 0x7f060207

.field public static final trim_thumb_offset_top:I = 0x7f060208

.field public static final trim_thumb_width:I = 0x7f060209

.field public static final trimbar_imageWidth:I = 0x7f06013c

.field public static final trimbar_marginTop:I = 0x7f06013d

.field public static final vertical_scroll:I = 0x7f0602a3

.field public static final videoDuration_marginRight:I = 0x7f0602a9

.field public static final video_editor_timeline_bg_line_height:I = 0x7f06016b

.field public static final video_image_picker_hor_space:I = 0x7f06013e

.field public static final video_image_picker_item_height:I = 0x7f06013f

.field public static final video_image_picker_item_height_padding:I = 0x7f060140

.field public static final video_image_picker_item_width:I = 0x7f060141

.field public static final video_image_picker_marginRight:I = 0x7f060142

.field public static final video_image_picker_paddingTop:I = 0x7f060143

.field public static final video_image_picker_vert_space:I = 0x7f060144

.field public static final video_length_item_height:I = 0x7f0603a6

.field public static final video_length_item_padding:I = 0x7f0603a7

.field public static final video_length_item_text_size:I = 0x7f0603a8

.field public static final video_preview_play_margin_top:I = 0x7f06020b

.field public static final video_preview_play_width_height:I = 0x7f06020a

.field public static final video_summary_dialog_optiontext_height:I = 0x7f060284

.field public static final video_summary_dialog_optiontext_marginleft_right:I = 0x7f060286

.field public static final video_summary_dialog_optiontext_margintop:I = 0x7f060285

.field public static final video_summary_dialog_optiontext_width:I = 0x7f060283

.field public static final video_summary_dialog_rllayout_width:I = 0x7f060282

.field public static final video_summary_dialog_textoption_marginbottom:I = 0x7f060288

.field public static final video_summary_dialog_textoption_margintop:I = 0x7f060287

.field public static final video_summary_dialog_textoption_textsize:I = 0x7f060289

.field public static final videoclipviewgroup_addmedia_height:I = 0x7f060145

.field public static final videoclipviewgroup_thumb_image_height:I = 0x7f060146

.field public static final videoclipviewgroup_thumb_image_size:I = 0x7f060148

.field public static final videoclipviewgroup_thumb_internal_padding:I = 0x7f060147

.field public static final videoclipviewgroup_thumb_marginright:I = 0x7f060225

.field public static final videoclipviewgroup_thumb_padding:I = 0x7f060149

.field public static final videoclipviewgroup_thumb_padding_theme:I = 0x7f06014a

.field public static final videoclipviewgroup_trim_width:I = 0x7f0602a4

.field public static final videopicker_margin_left:I = 0x7f06014b

.field public static final videopicker_margin_right:I = 0x7f06014c

.field public static final viewTopMinus:I = 0x7f06014d

.field public static final visualeffecs_divider_height:I = 0x7f0602b9

.field public static final visualeffectlist_margintop:I = 0x7f06004c

.field public static final visualeffectlist_textview_margintop:I = 0x7f06004d

.field public static final volume_thumboffset:I = 0x7f06019c

.field public static final volumeimage_height_width:I = 0x7f06019a

.field public static final volumeimage_margintop:I = 0x7f06019b

.field public static final volumelayout_height:I = 0x7f060197

.field public static final volumelayout_marginbottom:I = 0x7f060198

.field public static final volumelayout_margintop:I = 0x7f060199

.field public static final volumelayout_width:I = 0x7f060196

.field public static final volumepopup_height:I = 0x7f06014e

.field public static final volumepopup_marginRight:I = 0x7f06014f

.field public static final volumepopup_marginTop:I = 0x7f060150

.field public static final volumepopup_seekbar_width:I = 0x7f060151

.field public static final volumepopup_textVolume_size:I = 0x7f060152

.field public static final volumepopup_textvolume_width:I = 0x7f060153

.field public static final volumepopup_width:I = 0x7f060154

.field public static final wait_progress_dialog_height:I = 0x7f0602c8

.field public static final wait_progress_dialog_width:I = 0x7f0602c7

.field public static final wait_progress_icon_size:I = 0x7f0602c6

.field public static final wait_progress_loading:I = 0x7f060000

.field public static final wait_progress_text_marginL:I = 0x7f06000b

.field public static final wait_progress_text_size:I = 0x7f0602c5

.field public static final waveview_margin:I = 0x7f06028e


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 219
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
