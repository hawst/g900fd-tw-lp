.class public Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade$MediaDetail;
.super Ljava/lang/Object;
.source "LibraryInitDeinitFacade.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MediaDetail"
.end annotation


# instance fields
.field final filePath:Ljava/lang/String;

.field final startTime:J


# direct methods
.method public constructor <init>(Lcom/samsung/app/video/editor/external/Element;)V
    .locals 4
    .param p1, "element"    # Lcom/samsung/app/video/editor/external/Element;

    .prologue
    .line 127
    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/Element;->getFilePath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/Element;->getStartTime()J

    move-result-wide v2

    invoke-direct {p0, v0, v2, v3}, Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade$MediaDetail;-><init>(Ljava/lang/String;J)V

    .line 128
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;J)V
    .locals 0
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "startTime"    # J

    .prologue
    .line 122
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 123
    iput-object p1, p0, Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade$MediaDetail;->filePath:Ljava/lang/String;

    .line 124
    iput-wide p2, p0, Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade$MediaDetail;->startTime:J

    .line 125
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 141
    if-ne p0, p1, :cond_1

    .line 156
    :cond_0
    :goto_0
    return v1

    .line 143
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 144
    goto :goto_0

    .line 145
    :cond_2
    instance-of v3, p1, Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade$MediaDetail;

    if-nez v3, :cond_3

    move v1, v2

    .line 146
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 147
    check-cast v0, Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade$MediaDetail;

    .line 149
    .local v0, "other":Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade$MediaDetail;
    iget-object v3, p0, Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade$MediaDetail;->filePath:Ljava/lang/String;

    if-nez v3, :cond_4

    .line 150
    iget-object v3, v0, Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade$MediaDetail;->filePath:Ljava/lang/String;

    if-eqz v3, :cond_5

    move v1, v2

    .line 151
    goto :goto_0

    .line 152
    :cond_4
    iget-object v3, p0, Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade$MediaDetail;->filePath:Ljava/lang/String;

    iget-object v4, v0, Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade$MediaDetail;->filePath:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    move v1, v2

    .line 153
    goto :goto_0

    .line 154
    :cond_5
    iget-wide v4, p0, Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade$MediaDetail;->startTime:J

    iget-wide v6, v0, Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade$MediaDetail;->startTime:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    move v1, v2

    .line 155
    goto :goto_0
.end method

.method public hashCode()I
    .locals 8

    .prologue
    .line 132
    const/16 v0, 0x1f

    .line 133
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 134
    .local v1, "result":I
    iget-object v2, p0, Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade$MediaDetail;->filePath:Ljava/lang/String;

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    add-int/lit8 v1, v2, 0x1f

    .line 135
    mul-int/lit8 v2, v1, 0x1f

    iget-wide v4, p0, Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade$MediaDetail;->startTime:J

    iget-wide v6, p0, Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade$MediaDetail;->startTime:J

    const/16 v3, 0x20

    ushr-long/2addr v6, v3

    xor-long/2addr v4, v6

    long-to-int v3, v4

    add-int v1, v2, v3

    .line 136
    return v1

    .line 134
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/ve/thread/LibraryInitDeinitFacade$MediaDetail;->filePath:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_0
.end method
