.class public Lcom/sec/android/app/ve/data/MusicSubgroupDAO;
.super Ljava/lang/Object;
.source "MusicSubgroupDAO.java"


# instance fields
.field private itemCount:I

.field private musicSubgroupId:Ljava/lang/String;

.field private musicSubgroupName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "genresId"    # Ljava/lang/String;
    .param p2, "genresName"    # Ljava/lang/String;

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput-object p1, p0, Lcom/sec/android/app/ve/data/MusicSubgroupDAO;->musicSubgroupId:Ljava/lang/String;

    .line 11
    iput-object p2, p0, Lcom/sec/android/app/ve/data/MusicSubgroupDAO;->musicSubgroupName:Ljava/lang/String;

    .line 12
    return-void
.end method


# virtual methods
.method public getItemCount()I
    .locals 1

    .prologue
    .line 31
    iget v0, p0, Lcom/sec/android/app/ve/data/MusicSubgroupDAO;->itemCount:I

    return v0
.end method

.method public getMusicSubgroupId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/sec/android/app/ve/data/MusicSubgroupDAO;->musicSubgroupId:Ljava/lang/String;

    return-object v0
.end method

.method public getMusicSubgroupName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/ve/data/MusicSubgroupDAO;->musicSubgroupName:Ljava/lang/String;

    return-object v0
.end method

.method public setItemCount(I)V
    .locals 0
    .param p1, "itemCount"    # I

    .prologue
    .line 35
    iput p1, p0, Lcom/sec/android/app/ve/data/MusicSubgroupDAO;->itemCount:I

    .line 36
    return-void
.end method

.method public setMusicSubgroupId(Ljava/lang/String;)V
    .locals 0
    .param p1, "genresId"    # Ljava/lang/String;

    .prologue
    .line 19
    iput-object p1, p0, Lcom/sec/android/app/ve/data/MusicSubgroupDAO;->musicSubgroupId:Ljava/lang/String;

    .line 20
    return-void
.end method

.method public setMusicSubgroupName(Ljava/lang/String;)V
    .locals 0
    .param p1, "genresName"    # Ljava/lang/String;

    .prologue
    .line 27
    iput-object p1, p0, Lcom/sec/android/app/ve/data/MusicSubgroupDAO;->musicSubgroupName:Ljava/lang/String;

    .line 28
    return-void
.end method
