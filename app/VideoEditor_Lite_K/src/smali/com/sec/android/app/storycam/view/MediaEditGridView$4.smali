.class Lcom/sec/android/app/storycam/view/MediaEditGridView$4;
.super Ljava/lang/Object;
.source "MediaEditGridView.java"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/storycam/view/MediaEditGridView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/storycam/view/MediaEditGridView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView$4;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    .line 126
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLongClick(Landroid/view/View;)Z
    .locals 13
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v12, 0x0

    .line 129
    const/4 v4, 0x0

    .line 130
    .local v4, "handled":Z
    sget-object v9, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v9}, Lcom/sec/android/app/storycam/AppDataManager;->getOriginalTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v8

    .line 131
    .local v8, "trans":Lcom/samsung/app/video/editor/external/TranscodeElement;
    if-eqz v8, :cond_0

    .line 132
    iget-object v9, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView$4;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    invoke-virtual {v9, p1}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->indexOfChild(Landroid/view/View;)I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 133
    .local v2, "dragPosition":Ljava/lang/Integer;
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v9

    invoke-virtual {v8, v9}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElement(I)Lcom/samsung/app/video/editor/external/Element;

    move-result-object v3

    .line 134
    .local v3, "element":Lcom/samsung/app/video/editor/external/Element;
    if-eqz v3, :cond_0

    move-object v6, p1

    .line 135
    check-cast v6, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;

    .line 136
    .local v6, "layout":Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;
    const v9, 0x7f0d002d

    invoke-virtual {v6, v9}, Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 137
    .local v0, "childView":Landroid/view/View;
    new-instance v7, Landroid/graphics/Rect;

    invoke-direct {v7}, Landroid/graphics/Rect;-><init>()V

    .line 138
    .local v7, "r":Landroid/graphics/Rect;
    invoke-virtual {v0, v7}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 139
    iget-object v9, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView$4;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    # getter for: Lcom/sec/android/app/storycam/view/MediaEditGridView;->mTouchX:F
    invoke-static {v9}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->access$4(Lcom/sec/android/app/storycam/view/MediaEditGridView;)F

    move-result v9

    iget v10, v7, Landroid/graphics/Rect;->left:I

    int-to-float v10, v10

    cmpl-float v9, v9, v10

    if-lez v9, :cond_0

    iget-object v9, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView$4;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    # getter for: Lcom/sec/android/app/storycam/view/MediaEditGridView;->mTouchX:F
    invoke-static {v9}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->access$4(Lcom/sec/android/app/storycam/view/MediaEditGridView;)F

    move-result v9

    iget v10, v7, Landroid/graphics/Rect;->right:I

    int-to-float v10, v10

    cmpg-float v9, v9, v10

    if-gez v9, :cond_0

    iget-object v9, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView$4;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    # getter for: Lcom/sec/android/app/storycam/view/MediaEditGridView;->mTouchY:F
    invoke-static {v9}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->access$5(Lcom/sec/android/app/storycam/view/MediaEditGridView;)F

    move-result v9

    iget v10, v7, Landroid/graphics/Rect;->top:I

    int-to-float v10, v10

    cmpl-float v9, v9, v10

    if-lez v9, :cond_0

    iget-object v9, p0, Lcom/sec/android/app/storycam/view/MediaEditGridView$4;->this$0:Lcom/sec/android/app/storycam/view/MediaEditGridView;

    # getter for: Lcom/sec/android/app/storycam/view/MediaEditGridView;->mTouchY:F
    invoke-static {v9}, Lcom/sec/android/app/storycam/view/MediaEditGridView;->access$5(Lcom/sec/android/app/storycam/view/MediaEditGridView;)F

    move-result v9

    iget v10, v7, Landroid/graphics/Rect;->bottom:I

    int-to-float v10, v10

    cmpg-float v9, v9, v10

    if-gez v9, :cond_0

    .line 140
    new-instance v5, Landroid/content/ClipData$Item;

    const-string v9, ""

    invoke-direct {v5, v9}, Landroid/content/ClipData$Item;-><init>(Ljava/lang/CharSequence;)V

    .line 141
    .local v5, "item":Landroid/content/ClipData$Item;
    new-instance v1, Landroid/content/ClipData;

    const/4 v9, 0x0

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/String;

    const-string v11, "image/*"

    aput-object v11, v10, v12

    invoke-direct {v1, v9, v10, v5}, Landroid/content/ClipData;-><init>(Ljava/lang/CharSequence;[Ljava/lang/String;Landroid/content/ClipData$Item;)V

    .line 142
    .local v1, "clipData":Landroid/content/ClipData;
    new-instance v9, Landroid/view/View$DragShadowBuilder;

    invoke-direct {v9, v0}, Landroid/view/View$DragShadowBuilder;-><init>(Landroid/view/View;)V

    invoke-virtual {p1, v1, v9, p1, v12}, Landroid/view/View;->startDrag(Landroid/content/ClipData;Landroid/view/View$DragShadowBuilder;Ljava/lang/Object;I)Z

    .line 143
    const/4 v4, 0x1

    .line 147
    .end local v0    # "childView":Landroid/view/View;
    .end local v1    # "clipData":Landroid/content/ClipData;
    .end local v2    # "dragPosition":Ljava/lang/Integer;
    .end local v3    # "element":Lcom/samsung/app/video/editor/external/Element;
    .end local v5    # "item":Landroid/content/ClipData$Item;
    .end local v6    # "layout":Lcom/sec/android/app/storycam/view/MediaEditGridItemLayout;
    .end local v7    # "r":Landroid/graphics/Rect;
    :cond_0
    return v4
.end method
