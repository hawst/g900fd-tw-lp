.class public final Lcom/sec/android/app/storycam/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/storycam/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final actionbar_bgm:I = 0x7f020000

.field public static final actionbar_cancel:I = 0x7f020001

.field public static final actionbar_custom:I = 0x7f020002

.field public static final actionbar_edit:I = 0x7f020003

.field public static final actionbar_save:I = 0x7f020004

.field public static final add_media_image_selector:I = 0x7f020005

.field public static final bgm_item_selector:I = 0x7f020006

.field public static final bgm_off_selector:I = 0x7f020007

.field public static final bgm_on_selector:I = 0x7f020008

.field public static final caption_text_box_selector:I = 0x7f020009

.field public static final caption_text_box_while_scroll_selector:I = 0x7f02000a

.field public static final cartoon:I = 0x7f02000b

.field public static final clip_help_img_01:I = 0x7f02000c

.field public static final drag_drop_cue:I = 0x7f02000d

.field public static final edit_item_volume_border_selector:I = 0x7f02000e

.field public static final edit_thumbnail_selector:I = 0x7f02000f

.field public static final faded_colors:I = 0x7f020010

.field public static final fairview_popup_image_bg:I = 0x7f020011

.field public static final grey_scale:I = 0x7f020012

.field public static final help_cancel_btn_selector:I = 0x7f020013

.field public static final mirror:I = 0x7f020014

.field public static final mosaic:I = 0x7f020015

.field public static final movie_broken:I = 0x7f020016

.field public static final music_fullplayer_progress_bg:I = 0x7f020017

.field public static final music_fullplayer_progress_primary:I = 0x7f020018

.field public static final music_fullplayer_progress_secondary:I = 0x7f020019

.field public static final music_player_progressive_focus:I = 0x7f02001a

.field public static final music_player_progressive_ing:I = 0x7f02001b

.field public static final music_player_progressive_press:I = 0x7f02001c

.field public static final oil_pastel:I = 0x7f02001d

.field public static final old_film:I = 0x7f02001e

.field public static final overlay_help_button_focused:I = 0x7f02001f

.field public static final overlay_help_button_normal:I = 0x7f020020

.field public static final overlay_help_button_pressed:I = 0x7f020021

.field public static final pause_button_selector:I = 0x7f020022

.field public static final pic_broken:I = 0x7f020023

.field public static final play_button_selector:I = 0x7f020024

.field public static final player_progress_drawable:I = 0x7f020025

.field public static final player_progress_thumb:I = 0x7f020026

.field public static final preview_play_selector:I = 0x7f020027

.field public static final quick_notify_bg_2:I = 0x7f020028

.field public static final ripple_effect:I = 0x7f020029

.field public static final rugged:I = 0x7f02002a

.field public static final s_help_cancel_btn_focus:I = 0x7f02002b

.field public static final s_help_cancel_btn_nor:I = 0x7f02002c

.field public static final s_help_cancel_btn_press:I = 0x7f02002d

.field public static final s_studio:I = 0x7f02002e

.field public static final sepia:I = 0x7f02002f

.field public static final shining:I = 0x7f020030

.field public static final simple:I = 0x7f020031

.field public static final stat_notify_video:I = 0x7f020032

.field public static final theme_grid_selector:I = 0x7f020033

.field public static final theme_thumbnail_border_selector:I = 0x7f020034

.field public static final theme_video_length_selector:I = 0x7f020035

.field public static final thermal:I = 0x7f020036

.field public static final tint:I = 0x7f020037

.field public static final transparent_bg:I = 0x7f02006f

.field public static final turquoise:I = 0x7f020038

.field public static final tw_textfield_activated_holo_dark:I = 0x7f020039

.field public static final tw_textfield_default_holo_dark:I = 0x7f02003a

.field public static final tw_textfield_disabled_focused_holo_dark:I = 0x7f02003b

.field public static final tw_textfield_disabled_holo_dark:I = 0x7f02003c

.field public static final tw_textfield_focused_holo_dark:I = 0x7f02003d

.field public static final tw_textfield_pressed_holo_dark:I = 0x7f02003e

.field public static final tw_textfield_selected_holo_dark:I = 0x7f02003f

.field public static final tw_widget_progressbar_holo_dark:I = 0x7f020040

.field public static final video_clip_background:I = 0x7f020041

.field public static final video_edit_effect_focus:I = 0x7f020042

.field public static final video_edit_effect_press:I = 0x7f020043

.field public static final video_edit_effect_selected:I = 0x7f020044

.field public static final videoclip_action_bar_icon_cancel:I = 0x7f020045

.field public static final videoclip_action_bar_icon_cancel_dim:I = 0x7f020046

.field public static final videoclip_action_bar_icon_edit:I = 0x7f020047

.field public static final videoclip_action_bar_icon_edit_dim:I = 0x7f020048

.field public static final videoclip_action_bar_icon_movieeditor:I = 0x7f020049

.field public static final videoclip_action_bar_icon_movieeditor_dim:I = 0x7f02004a

.field public static final videoclip_action_bar_icon_save:I = 0x7f02004b

.field public static final videoclip_action_bar_icon_save_dim:I = 0x7f02004c

.field public static final videoclip_action_bar_icon_song:I = 0x7f02004d

.field public static final videoclip_action_bar_icon_song_dim:I = 0x7f02004e

.field public static final videoclip_bgm_select:I = 0x7f02004f

.field public static final videoclip_bgm_select_icon:I = 0x7f020050

.field public static final videoclip_bgm_select_icon_h:I = 0x7f020051

.field public static final videoclip_btn_bgm_off:I = 0x7f020052

.field public static final videoclip_btn_bgm_off_focused:I = 0x7f020053

.field public static final videoclip_btn_bgm_on:I = 0x7f020054

.field public static final videoclip_btn_bgm_on_focused:I = 0x7f020055

.field public static final videoclip_btn_plus:I = 0x7f020056

.field public static final videoclip_btn_plus_press:I = 0x7f020057

.field public static final videoclip_edit_bg:I = 0x7f020058

.field public static final videoclip_edit_focus:I = 0x7f020059

.field public static final videoclip_edit_press:I = 0x7f02005a

.field public static final videoclip_edit_selected:I = 0x7f02005b

.field public static final videoclip_edit_selected_check:I = 0x7f02005c

.field public static final videoclip_effect_scroll:I = 0x7f02005d

.field public static final videoclip_icon_mute:I = 0x7f02005e

.field public static final videoclip_icon_not_support:I = 0x7f02005f

.field public static final videoclip_icon_not_uhd:I = 0x7f020060

.field public static final videoclip_icon_not_wqhd:I = 0x7f020061

.field public static final videoclip_icon_volume:I = 0x7f020062

.field public static final videoclip_player_btn_pause:I = 0x7f020063

.field public static final videoclip_player_btn_pause_focus:I = 0x7f020064

.field public static final videoclip_player_btn_pause_press:I = 0x7f020065

.field public static final videoclip_player_btn_play:I = 0x7f020066

.field public static final videoclip_player_btn_play_focus:I = 0x7f020067

.field public static final videoclip_player_btn_play_press:I = 0x7f020068

.field public static final videoclip_player_icon_duration:I = 0x7f020069

.field public static final videoclip_player_icon_duration_focus:I = 0x7f02006a

.field public static final videoclip_player_icon_duration_press:I = 0x7f02006b

.field public static final videoclip_screen_title_bg:I = 0x7f02006c

.field public static final vignette:I = 0x7f02006d

.field public static final vintage:I = 0x7f02006e


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1251
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
