.class public Lcom/sec/android/app/ve/export/ExportService;
.super Landroid/app/Service;
.source "ExportService.java"


# static fields
.field public static EXPORT_COMPLETEION_ID:I

.field public static EXPORT_NOTFICATION_ID:I


# instance fields
.field public volatile defaultPaused:Z

.field private eThread:Lcom/sec/android/app/ve/export/ExportThread;

.field private mAdapter:Lcom/sec/android/app/ve/export/ExportInterface$Adapter;

.field private mExpInterface:Lcom/sec/android/app/ve/export/ExportInterface;

.field private nManager:Landroid/app/NotificationManager;

.field private notification:Landroid/app/Notification;

.field private onGoingNotification:Landroid/app/Notification;

.field private pIntent:Landroid/app/PendingIntent;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const/16 v0, 0xa

    sput v0, Lcom/sec/android/app/ve/export/ExportService;->EXPORT_NOTFICATION_ID:I

    .line 32
    const/4 v0, 0x2

    sput v0, Lcom/sec/android/app/ve/export/ExportService;->EXPORT_COMPLETEION_ID:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 25
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 26
    iput-object v0, p0, Lcom/sec/android/app/ve/export/ExportService;->eThread:Lcom/sec/android/app/ve/export/ExportThread;

    .line 27
    iput-object v0, p0, Lcom/sec/android/app/ve/export/ExportService;->mExpInterface:Lcom/sec/android/app/ve/export/ExportInterface;

    .line 34
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/ve/export/ExportService;->defaultPaused:Z

    .line 221
    new-instance v0, Lcom/sec/android/app/ve/export/ExportService$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/ve/export/ExportService$1;-><init>(Lcom/sec/android/app/ve/export/ExportService;)V

    iput-object v0, p0, Lcom/sec/android/app/ve/export/ExportService;->mAdapter:Lcom/sec/android/app/ve/export/ExportInterface$Adapter;

    .line 25
    return-void
.end method

.method static synthetic access$1(Lcom/sec/android/app/ve/export/ExportService;Lcom/sec/android/app/ve/export/ExportInterface;)V
    .locals 0

    .prologue
    .line 27
    iput-object p1, p0, Lcom/sec/android/app/ve/export/ExportService;->mExpInterface:Lcom/sec/android/app/ve/export/ExportInterface;

    return-void
.end method

.method static synthetic access$10(Lcom/sec/android/app/ve/export/ExportService;)Landroid/app/PendingIntent;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/ve/export/ExportService;->pIntent:Landroid/app/PendingIntent;

    return-object v0
.end method

.method static synthetic access$11(Lcom/sec/android/app/ve/export/ExportService;)Landroid/app/NotificationManager;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/app/ve/export/ExportService;->nManager:Landroid/app/NotificationManager;

    return-object v0
.end method

.method static synthetic access$12(Lcom/sec/android/app/ve/export/ExportService;Landroid/app/Notification;)V
    .locals 0

    .prologue
    .line 30
    iput-object p1, p0, Lcom/sec/android/app/ve/export/ExportService;->notification:Landroid/app/Notification;

    return-void
.end method

.method static synthetic access$13(Lcom/sec/android/app/ve/export/ExportService;)Landroid/app/Notification;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/ve/export/ExportService;->notification:Landroid/app/Notification;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/app/ve/export/ExportService;)Lcom/sec/android/app/ve/export/ExportInterface;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/app/ve/export/ExportService;->mExpInterface:Lcom/sec/android/app/ve/export/ExportInterface;

    return-object v0
.end method

.method static synthetic access$3(Lcom/sec/android/app/ve/export/ExportService;)Lcom/sec/android/app/ve/export/ExportInterface$Adapter;
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Lcom/sec/android/app/ve/export/ExportService;->mAdapter:Lcom/sec/android/app/ve/export/ExportInterface$Adapter;

    return-object v0
.end method

.method static synthetic access$4(Lcom/sec/android/app/ve/export/ExportService;Lcom/sec/android/app/ve/export/ExportThread;)V
    .locals 0

    .prologue
    .line 26
    iput-object p1, p0, Lcom/sec/android/app/ve/export/ExportService;->eThread:Lcom/sec/android/app/ve/export/ExportThread;

    return-void
.end method

.method static synthetic access$5(Lcom/sec/android/app/ve/export/ExportService;)Lcom/sec/android/app/ve/export/ExportThread;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/ve/export/ExportService;->eThread:Lcom/sec/android/app/ve/export/ExportThread;

    return-object v0
.end method

.method static synthetic access$6(Lcom/sec/android/app/ve/export/ExportService;Landroid/app/NotificationManager;)V
    .locals 0

    .prologue
    .line 28
    iput-object p1, p0, Lcom/sec/android/app/ve/export/ExportService;->nManager:Landroid/app/NotificationManager;

    return-void
.end method

.method static synthetic access$7(Lcom/sec/android/app/ve/export/ExportService;Landroid/app/Notification;)V
    .locals 0

    .prologue
    .line 29
    iput-object p1, p0, Lcom/sec/android/app/ve/export/ExportService;->onGoingNotification:Landroid/app/Notification;

    return-void
.end method

.method static synthetic access$8(Lcom/sec/android/app/ve/export/ExportService;Landroid/app/PendingIntent;)V
    .locals 0

    .prologue
    .line 33
    iput-object p1, p0, Lcom/sec/android/app/ve/export/ExportService;->pIntent:Landroid/app/PendingIntent;

    return-void
.end method

.method static synthetic access$9(Lcom/sec/android/app/ve/export/ExportService;)Landroid/app/Notification;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/app/ve/export/ExportService;->onGoingNotification:Landroid/app/Notification;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lcom/sec/android/app/ve/export/ExportService;->onBind(Landroid/content/Intent;)Lcom/sec/android/app/ve/export/IExportService$Stub;

    move-result-object v0

    return-object v0
.end method

.method public onBind(Landroid/content/Intent;)Lcom/sec/android/app/ve/export/IExportService$Stub;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 40
    new-instance v0, Lcom/sec/android/app/ve/export/ExportService$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/ve/export/ExportService$2;-><init>(Lcom/sec/android/app/ve/export/ExportService;)V

    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 216
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 217
    iget-object v0, p0, Lcom/sec/android/app/ve/export/ExportService;->nManager:Landroid/app/NotificationManager;

    if-eqz v0, :cond_0

    .line 218
    iget-object v0, p0, Lcom/sec/android/app/ve/export/ExportService;->nManager:Landroid/app/NotificationManager;

    sget v1, Lcom/sec/android/app/ve/export/ExportService;->EXPORT_NOTFICATION_ID:I

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 219
    :cond_0
    return-void
.end method
