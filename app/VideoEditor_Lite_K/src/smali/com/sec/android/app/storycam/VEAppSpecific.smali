.class public Lcom/sec/android/app/storycam/VEAppSpecific;
.super Lcom/sec/android/app/ve/VEApp;
.source "VEAppSpecific.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/storycam/VEAppSpecific$Adaper;
    }
.end annotation


# static fields
.field public static final EXPORT_PATH_DEFAULT:Ljava/lang/String;

.field public static EXPORT_PATH_PERSONALPAGE:Ljava/lang/String;

.field public static gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

.field private static final mAdapter:Lcom/sec/android/app/storycam/VEAppSpecific$Adaper;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 65
    new-instance v0, Lcom/sec/android/app/storycam/AppDataManager;

    invoke-direct {v0}, Lcom/sec/android/app/storycam/AppDataManager;-><init>()V

    sput-object v0, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    .line 66
    new-instance v0, Lcom/sec/android/app/storycam/VEAppSpecific$Adaper;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/sec/android/app/storycam/VEAppSpecific$Adaper;-><init>(Lcom/sec/android/app/storycam/VEAppSpecific$Adaper;)V

    sput-object v0, Lcom/sec/android/app/storycam/VEAppSpecific;->mAdapter:Lcom/sec/android/app/storycam/VEAppSpecific$Adaper;

    .line 67
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "/Studio/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/storycam/VEAppSpecific;->EXPORT_PATH_DEFAULT:Ljava/lang/String;

    .line 68
    const-string v0, "/storage/Private/"

    sput-object v0, Lcom/sec/android/app/storycam/VEAppSpecific;->EXPORT_PATH_PERSONALPAGE:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/sec/android/app/ve/VEApp;-><init>()V

    return-void
.end method

.method private static copyFileFromAssetsToFilesDir(Ljava/lang/String;)V
    .locals 10
    .param p0, "assetFile"    # Ljava/lang/String;

    .prologue
    .line 185
    new-instance v3, Ljava/io/File;

    invoke-static {}, Lcom/sec/android/app/storycam/VEAppSpecific;->getFilesDirectory()Ljava/io/File;

    move-result-object v8

    invoke-direct {v3, v8, p0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 186
    .local v3, "file":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v8

    if-nez v8, :cond_2

    .line 188
    const/4 v4, 0x0

    .line 189
    .local v4, "in":Ljava/io/InputStream;
    const/4 v5, 0x0

    .line 191
    .local v5, "out":Ljava/io/FileOutputStream;
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/storycam/VEAppSpecific;->getAppAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    .line 192
    .local v0, "assetManager":Landroid/content/res/AssetManager;
    invoke-virtual {v0, p0}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v4

    .line 193
    new-instance v6, Ljava/io/FileOutputStream;

    invoke-direct {v6, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 194
    .end local v5    # "out":Ljava/io/FileOutputStream;
    .local v6, "out":Ljava/io/FileOutputStream;
    const/16 v8, 0x400

    :try_start_1
    new-array v1, v8, [B

    .line 195
    .local v1, "buffer":[B
    invoke-virtual {v4, v1}, Ljava/io/InputStream;->read([B)I

    move-result v7

    .line 196
    .local v7, "read":I
    :goto_0
    const/4 v8, -0x1

    if-ne v7, v8, :cond_3

    .line 200
    const/4 v8, 0x1

    const/4 v9, 0x1

    invoke-virtual {v3, v8, v9}, Ljava/io/File;->setExecutable(ZZ)Z

    .line 201
    const/4 v8, 0x1

    const/4 v9, 0x1

    invoke-virtual {v3, v8, v9}, Ljava/io/File;->setWritable(ZZ)Z

    .line 202
    const/4 v8, 0x1

    const/4 v9, 0x1

    invoke-virtual {v3, v8, v9}, Ljava/io/File;->setReadable(ZZ)Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_a
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 207
    if-eqz v4, :cond_0

    .line 208
    :try_start_2
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_7

    .line 213
    :cond_0
    :goto_1
    const/4 v4, 0x0

    .line 215
    if-eqz v6, :cond_1

    .line 216
    :try_start_3
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->flush()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_8

    .line 222
    :cond_1
    :goto_2
    if-eqz v6, :cond_2

    .line 223
    :try_start_4
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_9

    .line 231
    .end local v0    # "assetManager":Landroid/content/res/AssetManager;
    .end local v1    # "buffer":[B
    .end local v4    # "in":Ljava/io/InputStream;
    .end local v6    # "out":Ljava/io/FileOutputStream;
    .end local v7    # "read":I
    :cond_2
    :goto_3
    return-void

    .line 197
    .restart local v0    # "assetManager":Landroid/content/res/AssetManager;
    .restart local v1    # "buffer":[B
    .restart local v4    # "in":Ljava/io/InputStream;
    .restart local v6    # "out":Ljava/io/FileOutputStream;
    .restart local v7    # "read":I
    :cond_3
    const/4 v8, 0x0

    :try_start_5
    invoke-virtual {v6, v1, v8, v7}, Ljava/io/FileOutputStream;->write([BII)V

    .line 198
    invoke-virtual {v4, v1}, Ljava/io/InputStream;->read([B)I
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_a
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-result v7

    goto :goto_0

    .line 203
    .end local v0    # "assetManager":Landroid/content/res/AssetManager;
    .end local v1    # "buffer":[B
    .end local v6    # "out":Ljava/io/FileOutputStream;
    .end local v7    # "read":I
    .restart local v5    # "out":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v2

    .line 204
    .local v2, "e":Ljava/io/IOException;
    :goto_4
    :try_start_6
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 207
    if-eqz v4, :cond_4

    .line 208
    :try_start_7
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1

    .line 213
    :cond_4
    :goto_5
    const/4 v4, 0x0

    .line 215
    if-eqz v5, :cond_5

    .line 216
    :try_start_8
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->flush()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_2

    .line 222
    :cond_5
    :goto_6
    if-eqz v5, :cond_6

    .line 223
    :try_start_9
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_3

    .line 228
    :cond_6
    :goto_7
    const/4 v5, 0x0

    goto :goto_3

    .line 209
    :catch_1
    move-exception v2

    .line 211
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 217
    :catch_2
    move-exception v2

    .line 219
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 224
    :catch_3
    move-exception v2

    .line 226
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_7

    .line 205
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v8

    .line 207
    :goto_8
    if-eqz v4, :cond_7

    .line 208
    :try_start_a
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_4

    .line 213
    :cond_7
    :goto_9
    const/4 v4, 0x0

    .line 215
    if-eqz v5, :cond_8

    .line 216
    :try_start_b
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->flush()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_5

    .line 222
    :cond_8
    :goto_a
    if-eqz v5, :cond_9

    .line 223
    :try_start_c
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_6

    .line 228
    :cond_9
    :goto_b
    const/4 v5, 0x0

    .line 229
    throw v8

    .line 209
    :catch_4
    move-exception v2

    .line 211
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_9

    .line 217
    .end local v2    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v2

    .line 219
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_a

    .line 224
    .end local v2    # "e":Ljava/io/IOException;
    :catch_6
    move-exception v2

    .line 226
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_b

    .line 209
    .end local v2    # "e":Ljava/io/IOException;
    .end local v5    # "out":Ljava/io/FileOutputStream;
    .restart local v0    # "assetManager":Landroid/content/res/AssetManager;
    .restart local v1    # "buffer":[B
    .restart local v6    # "out":Ljava/io/FileOutputStream;
    .restart local v7    # "read":I
    :catch_7
    move-exception v2

    .line 211
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 217
    .end local v2    # "e":Ljava/io/IOException;
    :catch_8
    move-exception v2

    .line 219
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 224
    .end local v2    # "e":Ljava/io/IOException;
    :catch_9
    move-exception v2

    .line 226
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 205
    .end local v1    # "buffer":[B
    .end local v2    # "e":Ljava/io/IOException;
    .end local v7    # "read":I
    :catchall_1
    move-exception v8

    move-object v5, v6

    .end local v6    # "out":Ljava/io/FileOutputStream;
    .restart local v5    # "out":Ljava/io/FileOutputStream;
    goto :goto_8

    .line 203
    .end local v5    # "out":Ljava/io/FileOutputStream;
    .restart local v6    # "out":Ljava/io/FileOutputStream;
    :catch_a
    move-exception v2

    move-object v5, v6

    .end local v6    # "out":Ljava/io/FileOutputStream;
    .restart local v5    # "out":Ljava/io/FileOutputStream;
    goto :goto_4
.end method

.method public static init()Z
    .locals 8

    .prologue
    .line 75
    :try_start_0
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getInitialApplication()Landroid/app/Application;

    move-result-object v7

    invoke-static {v7}, Lcom/samsung/android/secretmode/SecretModeManager;->getPersonalPageRoot(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v7, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    sput-object v6, Lcom/sec/android/app/storycam/VEAppSpecific;->EXPORT_PATH_PERSONALPAGE:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_1

    .line 84
    :goto_0
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v7

    invoke-virtual {v7}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v7, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ".videocliprawdir"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/app/video/editor/external/ClipartParams;->setRawDirPath(Ljava/lang/String;)V

    .line 85
    const/4 v4, 0x0

    .line 87
    .local v4, "result":Z
    sget-object v6, Lcom/sec/android/app/storycam/VEAppSpecific;->EXPORT_PATH_DEFAULT:Ljava/lang/String;

    sput-object v6, Lcom/sec/android/app/ve/common/ConfigUtils;->EXPORT_PATH:Ljava/lang/String;

    .line 88
    sget-object v6, Lcom/sec/android/app/storycam/VEAppSpecific;->mAdapter:Lcom/sec/android/app/storycam/VEAppSpecific$Adaper;

    invoke-static {v6}, Lcom/sec/android/app/ve/VEApp;->init(Lcom/sec/android/app/ve/VEAdapter;)V

    .line 89
    const/high16 v6, 0x40000000    # 2.0f

    invoke-static {v6}, Lcom/sec/android/app/ve/VEApp;->setJNIVersion(F)V

    .line 90
    invoke-static {}, Lcom/sec/android/app/storycam/Utils;->initDimensions()V

    .line 92
    invoke-static {}, Lcom/sec/android/app/ve/util/CommonUtils;->init()V

    .line 93
    sget-object v6, Lcom/sec/android/app/ve/VEApp;->gThemeMgr:Lcom/sec/android/app/ve/theme/ThemeManager;

    invoke-virtual {v6}, Lcom/sec/android/app/ve/theme/ThemeManager;->getThemeCount()I

    move-result v6

    if-nez v6, :cond_1

    .line 94
    sget-object v6, Lcom/sec/android/app/ve/VEApp;->gThemeMgr:Lcom/sec/android/app/ve/theme/ThemeManager;

    new-instance v7, Lcom/sec/android/app/storycam/theme/OriginalTheme;

    invoke-direct {v7}, Lcom/sec/android/app/storycam/theme/OriginalTheme;-><init>()V

    invoke-virtual {v6, v7}, Lcom/sec/android/app/ve/theme/ThemeManager;->addTheme(Lcom/sec/android/app/ve/theme/Theme;)V

    .line 95
    sget-object v6, Lcom/sec/android/app/ve/VEApp;->gThemeMgr:Lcom/sec/android/app/ve/theme/ThemeManager;

    new-instance v7, Lcom/sec/android/app/storycam/theme/GreyScaleTheme;

    invoke-direct {v7}, Lcom/sec/android/app/storycam/theme/GreyScaleTheme;-><init>()V

    invoke-virtual {v6, v7}, Lcom/sec/android/app/ve/theme/ThemeManager;->addTheme(Lcom/sec/android/app/ve/theme/Theme;)V

    .line 96
    sget-object v6, Lcom/sec/android/app/ve/VEApp;->gThemeMgr:Lcom/sec/android/app/ve/theme/ThemeManager;

    new-instance v7, Lcom/sec/android/app/storycam/theme/SepiaTheme;

    invoke-direct {v7}, Lcom/sec/android/app/storycam/theme/SepiaTheme;-><init>()V

    invoke-virtual {v6, v7}, Lcom/sec/android/app/ve/theme/ThemeManager;->addTheme(Lcom/sec/android/app/ve/theme/Theme;)V

    .line 97
    sget-object v6, Lcom/sec/android/app/ve/VEApp;->gThemeMgr:Lcom/sec/android/app/ve/theme/ThemeManager;

    new-instance v7, Lcom/sec/android/app/storycam/theme/VintageTheme;

    invoke-direct {v7}, Lcom/sec/android/app/storycam/theme/VintageTheme;-><init>()V

    invoke-virtual {v6, v7}, Lcom/sec/android/app/ve/theme/ThemeManager;->addTheme(Lcom/sec/android/app/ve/theme/Theme;)V

    .line 98
    sget-object v6, Lcom/sec/android/app/ve/VEApp;->gThemeMgr:Lcom/sec/android/app/ve/theme/ThemeManager;

    new-instance v7, Lcom/sec/android/app/storycam/theme/CartoonTheme;

    invoke-direct {v7}, Lcom/sec/android/app/storycam/theme/CartoonTheme;-><init>()V

    invoke-virtual {v6, v7}, Lcom/sec/android/app/ve/theme/ThemeManager;->addTheme(Lcom/sec/android/app/ve/theme/Theme;)V

    .line 99
    sget-object v6, Lcom/sec/android/app/ve/VEApp;->gThemeMgr:Lcom/sec/android/app/ve/theme/ThemeManager;

    new-instance v7, Lcom/sec/android/app/storycam/theme/FadedColorsTheme;

    invoke-direct {v7}, Lcom/sec/android/app/storycam/theme/FadedColorsTheme;-><init>()V

    invoke-virtual {v6, v7}, Lcom/sec/android/app/ve/theme/ThemeManager;->addTheme(Lcom/sec/android/app/ve/theme/Theme;)V

    .line 100
    sget-object v6, Lcom/sec/android/app/ve/VEApp;->gThemeMgr:Lcom/sec/android/app/ve/theme/ThemeManager;

    new-instance v7, Lcom/sec/android/app/storycam/theme/TurquoiseTheme;

    invoke-direct {v7}, Lcom/sec/android/app/storycam/theme/TurquoiseTheme;-><init>()V

    invoke-virtual {v6, v7}, Lcom/sec/android/app/ve/theme/ThemeManager;->addTheme(Lcom/sec/android/app/ve/theme/Theme;)V

    .line 101
    sget-object v6, Lcom/sec/android/app/ve/VEApp;->gThemeMgr:Lcom/sec/android/app/ve/theme/ThemeManager;

    new-instance v7, Lcom/sec/android/app/storycam/theme/VignetteTheme;

    invoke-direct {v7}, Lcom/sec/android/app/storycam/theme/VignetteTheme;-><init>()V

    invoke-virtual {v6, v7}, Lcom/sec/android/app/ve/theme/ThemeManager;->addTheme(Lcom/sec/android/app/ve/theme/Theme;)V

    .line 102
    sget-object v6, Lcom/sec/android/app/ve/VEApp;->gThemeMgr:Lcom/sec/android/app/ve/theme/ThemeManager;

    new-instance v7, Lcom/sec/android/app/storycam/theme/OilPastelTheme;

    invoke-direct {v7}, Lcom/sec/android/app/storycam/theme/OilPastelTheme;-><init>()V

    invoke-virtual {v6, v7}, Lcom/sec/android/app/ve/theme/ThemeManager;->addTheme(Lcom/sec/android/app/ve/theme/Theme;)V

    .line 103
    sget-object v6, Lcom/sec/android/app/ve/VEApp;->gThemeMgr:Lcom/sec/android/app/ve/theme/ThemeManager;

    new-instance v7, Lcom/sec/android/app/storycam/theme/MosaicTheme;

    invoke-direct {v7}, Lcom/sec/android/app/storycam/theme/MosaicTheme;-><init>()V

    invoke-virtual {v6, v7}, Lcom/sec/android/app/ve/theme/ThemeManager;->addTheme(Lcom/sec/android/app/ve/theme/Theme;)V

    .line 104
    sget-object v6, Lcom/sec/android/app/ve/VEApp;->gThemeMgr:Lcom/sec/android/app/ve/theme/ThemeManager;

    new-instance v7, Lcom/sec/android/app/storycam/theme/TintTheme;

    invoke-direct {v7}, Lcom/sec/android/app/storycam/theme/TintTheme;-><init>()V

    invoke-virtual {v6, v7}, Lcom/sec/android/app/ve/theme/ThemeManager;->addTheme(Lcom/sec/android/app/ve/theme/Theme;)V

    .line 105
    sget-object v6, Lcom/sec/android/app/ve/VEApp;->gThemeMgr:Lcom/sec/android/app/ve/theme/ThemeManager;

    new-instance v7, Lcom/sec/android/app/storycam/theme/RuggedTheme;

    invoke-direct {v7}, Lcom/sec/android/app/storycam/theme/RuggedTheme;-><init>()V

    invoke-virtual {v6, v7}, Lcom/sec/android/app/ve/theme/ThemeManager;->addTheme(Lcom/sec/android/app/ve/theme/Theme;)V

    .line 106
    sget-object v6, Lcom/sec/android/app/ve/VEApp;->gThemeMgr:Lcom/sec/android/app/ve/theme/ThemeManager;

    new-instance v7, Lcom/sec/android/app/storycam/theme/ThermalTheme;

    invoke-direct {v7}, Lcom/sec/android/app/storycam/theme/ThermalTheme;-><init>()V

    invoke-virtual {v6, v7}, Lcom/sec/android/app/ve/theme/ThemeManager;->addTheme(Lcom/sec/android/app/ve/theme/Theme;)V

    .line 107
    sget-object v6, Lcom/sec/android/app/ve/VEApp;->gThemeMgr:Lcom/sec/android/app/ve/theme/ThemeManager;

    new-instance v7, Lcom/sec/android/app/storycam/theme/MirrorTheme;

    invoke-direct {v7}, Lcom/sec/android/app/storycam/theme/MirrorTheme;-><init>()V

    invoke-virtual {v6, v7}, Lcom/sec/android/app/ve/theme/ThemeManager;->addTheme(Lcom/sec/android/app/ve/theme/Theme;)V

    .line 109
    const-string v6, "ro.product.device"

    invoke-static {v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 110
    .local v0, "modelName":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v6, "m2"

    invoke-virtual {v0, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 111
    :cond_0
    sget-object v6, Lcom/sec/android/app/ve/VEApp;->gThemeMgr:Lcom/sec/android/app/ve/theme/ThemeManager;

    new-instance v7, Lcom/sec/android/app/storycam/theme/ShiningTheme;

    invoke-direct {v7}, Lcom/sec/android/app/storycam/theme/ShiningTheme;-><init>()V

    invoke-virtual {v6, v7}, Lcom/sec/android/app/ve/theme/ThemeManager;->addTheme(Lcom/sec/android/app/ve/theme/Theme;)V

    .line 112
    sget-object v6, Lcom/sec/android/app/ve/VEApp;->gThemeMgr:Lcom/sec/android/app/ve/theme/ThemeManager;

    new-instance v7, Lcom/sec/android/app/storycam/theme/OldFilmTheme;

    invoke-direct {v7}, Lcom/sec/android/app/storycam/theme/OldFilmTheme;-><init>()V

    invoke-virtual {v6, v7}, Lcom/sec/android/app/ve/theme/ThemeManager;->addTheme(Lcom/sec/android/app/ve/theme/Theme;)V

    .line 118
    .end local v0    # "modelName":Ljava/lang/String;
    :cond_1
    sget-object v6, Lcom/sec/android/app/ve/VEApp;->gBGMManager:Lcom/sec/android/app/ve/bgm/BGMManager;

    invoke-virtual {v6}, Lcom/sec/android/app/ve/bgm/BGMManager;->getBGMCount()I

    move-result v6

    if-nez v6, :cond_2

    .line 119
    sget-object v6, Lcom/sec/android/app/ve/VEApp;->gBGMManager:Lcom/sec/android/app/ve/bgm/BGMManager;

    new-instance v7, Lcom/sec/android/app/storycam/bgm/VacationsBGM;

    invoke-direct {v7}, Lcom/sec/android/app/storycam/bgm/VacationsBGM;-><init>()V

    invoke-virtual {v6, v7}, Lcom/sec/android/app/ve/bgm/BGMManager;->addBGM(Lcom/sec/android/app/ve/bgm/BGM;)V

    .line 120
    sget-object v6, Lcom/sec/android/app/ve/VEApp;->gBGMManager:Lcom/sec/android/app/ve/bgm/BGMManager;

    new-instance v7, Lcom/sec/android/app/storycam/bgm/SpecialDayBGM;

    invoke-direct {v7}, Lcom/sec/android/app/storycam/bgm/SpecialDayBGM;-><init>()V

    invoke-virtual {v6, v7}, Lcom/sec/android/app/ve/bgm/BGMManager;->addBGM(Lcom/sec/android/app/ve/bgm/BGM;)V

    .line 121
    sget-object v6, Lcom/sec/android/app/ve/VEApp;->gBGMManager:Lcom/sec/android/app/ve/bgm/BGMManager;

    new-instance v7, Lcom/sec/android/app/storycam/bgm/ActionBGM;

    invoke-direct {v7}, Lcom/sec/android/app/storycam/bgm/ActionBGM;-><init>()V

    invoke-virtual {v6, v7}, Lcom/sec/android/app/ve/bgm/BGMManager;->addBGM(Lcom/sec/android/app/ve/bgm/BGM;)V

    .line 122
    sget-object v6, Lcom/sec/android/app/ve/VEApp;->gBGMManager:Lcom/sec/android/app/ve/bgm/BGMManager;

    new-instance v7, Lcom/sec/android/app/storycam/bgm/InTheClubBGM;

    invoke-direct {v7}, Lcom/sec/android/app/storycam/bgm/InTheClubBGM;-><init>()V

    invoke-virtual {v6, v7}, Lcom/sec/android/app/ve/bgm/BGMManager;->addBGM(Lcom/sec/android/app/ve/bgm/BGM;)V

    .line 123
    sget-object v6, Lcom/sec/android/app/ve/VEApp;->gBGMManager:Lcom/sec/android/app/ve/bgm/BGMManager;

    new-instance v7, Lcom/sec/android/app/storycam/bgm/TonightBGM;

    invoke-direct {v7}, Lcom/sec/android/app/storycam/bgm/TonightBGM;-><init>()V

    invoke-virtual {v6, v7}, Lcom/sec/android/app/ve/bgm/BGMManager;->addBGM(Lcom/sec/android/app/ve/bgm/BGM;)V

    .line 124
    sget-object v6, Lcom/sec/android/app/ve/VEApp;->gBGMManager:Lcom/sec/android/app/ve/bgm/BGMManager;

    new-instance v7, Lcom/sec/android/app/storycam/bgm/DubbyDogBGM;

    invoke-direct {v7}, Lcom/sec/android/app/storycam/bgm/DubbyDogBGM;-><init>()V

    invoke-virtual {v6, v7}, Lcom/sec/android/app/ve/bgm/BGMManager;->addBGM(Lcom/sec/android/app/ve/bgm/BGM;)V

    .line 125
    sget-object v6, Lcom/sec/android/app/ve/VEApp;->gBGMManager:Lcom/sec/android/app/ve/bgm/BGMManager;

    new-instance v7, Lcom/sec/android/app/storycam/bgm/FunkWorksBGM;

    invoke-direct {v7}, Lcom/sec/android/app/storycam/bgm/FunkWorksBGM;-><init>()V

    invoke-virtual {v6, v7}, Lcom/sec/android/app/ve/bgm/BGMManager;->addBGM(Lcom/sec/android/app/ve/bgm/BGM;)V

    .line 126
    sget-object v6, Lcom/sec/android/app/ve/VEApp;->gBGMManager:Lcom/sec/android/app/ve/bgm/BGMManager;

    new-instance v7, Lcom/sec/android/app/storycam/bgm/YoungJayBGM;

    invoke-direct {v7}, Lcom/sec/android/app/storycam/bgm/YoungJayBGM;-><init>()V

    invoke-virtual {v6, v7}, Lcom/sec/android/app/ve/bgm/BGMManager;->addBGM(Lcom/sec/android/app/ve/bgm/BGM;)V

    .line 127
    sget-object v6, Lcom/sec/android/app/ve/VEApp;->gBGMManager:Lcom/sec/android/app/ve/bgm/BGMManager;

    new-instance v7, Lcom/sec/android/app/storycam/bgm/RunBGM;

    invoke-direct {v7}, Lcom/sec/android/app/storycam/bgm/RunBGM;-><init>()V

    invoke-virtual {v6, v7}, Lcom/sec/android/app/ve/bgm/BGMManager;->addBGM(Lcom/sec/android/app/ve/bgm/BGM;)V

    .line 128
    sget-object v6, Lcom/sec/android/app/ve/VEApp;->gBGMManager:Lcom/sec/android/app/ve/bgm/BGMManager;

    new-instance v7, Lcom/sec/android/app/storycam/bgm/BoltToyBGM;

    invoke-direct {v7}, Lcom/sec/android/app/storycam/bgm/BoltToyBGM;-><init>()V

    invoke-virtual {v6, v7}, Lcom/sec/android/app/ve/bgm/BGMManager;->addBGM(Lcom/sec/android/app/ve/bgm/BGM;)V

    .line 129
    sget-object v6, Lcom/sec/android/app/ve/VEApp;->gBGMManager:Lcom/sec/android/app/ve/bgm/BGMManager;

    new-instance v7, Lcom/sec/android/app/storycam/bgm/EgoTripBGM;

    invoke-direct {v7}, Lcom/sec/android/app/storycam/bgm/EgoTripBGM;-><init>()V

    invoke-virtual {v6, v7}, Lcom/sec/android/app/ve/bgm/BGMManager;->addBGM(Lcom/sec/android/app/ve/bgm/BGM;)V

    .line 130
    sget-object v6, Lcom/sec/android/app/ve/VEApp;->gBGMManager:Lcom/sec/android/app/ve/bgm/BGMManager;

    new-instance v7, Lcom/sec/android/app/storycam/bgm/BlueJonesBGM;

    invoke-direct {v7}, Lcom/sec/android/app/storycam/bgm/BlueJonesBGM;-><init>()V

    invoke-virtual {v6, v7}, Lcom/sec/android/app/ve/bgm/BGMManager;->addBGM(Lcom/sec/android/app/ve/bgm/BGM;)V

    .line 131
    sget-object v6, Lcom/sec/android/app/ve/VEApp;->gBGMManager:Lcom/sec/android/app/ve/bgm/BGMManager;

    new-instance v7, Lcom/sec/android/app/storycam/bgm/SightsInTheCityBGM;

    invoke-direct {v7}, Lcom/sec/android/app/storycam/bgm/SightsInTheCityBGM;-><init>()V

    invoke-virtual {v6, v7}, Lcom/sec/android/app/ve/bgm/BGMManager;->addBGM(Lcom/sec/android/app/ve/bgm/BGM;)V

    .line 132
    sget-object v6, Lcom/sec/android/app/ve/VEApp;->gBGMManager:Lcom/sec/android/app/ve/bgm/BGMManager;

    new-instance v7, Lcom/sec/android/app/storycam/bgm/WhistleYourCaresBGM;

    invoke-direct {v7}, Lcom/sec/android/app/storycam/bgm/WhistleYourCaresBGM;-><init>()V

    invoke-virtual {v6, v7}, Lcom/sec/android/app/ve/bgm/BGMManager;->addBGM(Lcom/sec/android/app/ve/bgm/BGM;)V

    .line 133
    sget-object v6, Lcom/sec/android/app/ve/VEApp;->gBGMManager:Lcom/sec/android/app/ve/bgm/BGMManager;

    new-instance v7, Lcom/sec/android/app/storycam/bgm/LittleJoyBGM;

    invoke-direct {v7}, Lcom/sec/android/app/storycam/bgm/LittleJoyBGM;-><init>()V

    invoke-virtual {v6, v7}, Lcom/sec/android/app/ve/bgm/BGMManager;->addBGM(Lcom/sec/android/app/ve/bgm/BGM;)V

    .line 134
    sget-object v6, Lcom/sec/android/app/ve/VEApp;->gBGMManager:Lcom/sec/android/app/ve/bgm/BGMManager;

    new-instance v7, Lcom/sec/android/app/storycam/bgm/FamilyBGM;

    invoke-direct {v7}, Lcom/sec/android/app/storycam/bgm/FamilyBGM;-><init>()V

    invoke-virtual {v6, v7}, Lcom/sec/android/app/ve/bgm/BGMManager;->addBGM(Lcom/sec/android/app/ve/bgm/BGM;)V

    .line 135
    sget-object v6, Lcom/sec/android/app/ve/VEApp;->gBGMManager:Lcom/sec/android/app/ve/bgm/BGMManager;

    new-instance v7, Lcom/sec/android/app/storycam/bgm/TableBGM;

    invoke-direct {v7}, Lcom/sec/android/app/storycam/bgm/TableBGM;-><init>()V

    invoke-virtual {v6, v7}, Lcom/sec/android/app/ve/bgm/BGMManager;->addBGM(Lcom/sec/android/app/ve/bgm/BGM;)V

    .line 136
    sget-object v6, Lcom/sec/android/app/ve/VEApp;->gBGMManager:Lcom/sec/android/app/ve/bgm/BGMManager;

    new-instance v7, Lcom/sec/android/app/storycam/bgm/ADayOnTheTrainsBGM;

    invoke-direct {v7}, Lcom/sec/android/app/storycam/bgm/ADayOnTheTrainsBGM;-><init>()V

    invoke-virtual {v6, v7}, Lcom/sec/android/app/ve/bgm/BGMManager;->addBGM(Lcom/sec/android/app/ve/bgm/BGM;)V

    .line 137
    sget-object v6, Lcom/sec/android/app/ve/VEApp;->gBGMManager:Lcom/sec/android/app/ve/bgm/BGMManager;

    new-instance v7, Lcom/sec/android/app/storycam/bgm/BlueDuckBGM;

    invoke-direct {v7}, Lcom/sec/android/app/storycam/bgm/BlueDuckBGM;-><init>()V

    invoke-virtual {v6, v7}, Lcom/sec/android/app/ve/bgm/BGMManager;->addBGM(Lcom/sec/android/app/ve/bgm/BGM;)V

    .line 138
    sget-object v6, Lcom/sec/android/app/ve/VEApp;->gBGMManager:Lcom/sec/android/app/ve/bgm/BGMManager;

    new-instance v7, Lcom/sec/android/app/storycam/bgm/VintageBGM;

    invoke-direct {v7}, Lcom/sec/android/app/storycam/bgm/VintageBGM;-><init>()V

    invoke-virtual {v6, v7}, Lcom/sec/android/app/ve/bgm/BGMManager;->addBGM(Lcom/sec/android/app/ve/bgm/BGM;)V

    .line 139
    sget-object v6, Lcom/sec/android/app/ve/VEApp;->gBGMManager:Lcom/sec/android/app/ve/bgm/BGMManager;

    new-instance v7, Lcom/sec/android/app/storycam/bgm/FearOfDarknessBGM;

    invoke-direct {v7}, Lcom/sec/android/app/storycam/bgm/FearOfDarknessBGM;-><init>()V

    invoke-virtual {v6, v7}, Lcom/sec/android/app/ve/bgm/BGMManager;->addBGM(Lcom/sec/android/app/ve/bgm/BGM;)V

    .line 140
    sget-object v6, Lcom/sec/android/app/ve/VEApp;->gBGMManager:Lcom/sec/android/app/ve/bgm/BGMManager;

    new-instance v7, Lcom/sec/android/app/storycam/bgm/RainBGM;

    invoke-direct {v7}, Lcom/sec/android/app/storycam/bgm/RainBGM;-><init>()V

    invoke-virtual {v6, v7}, Lcom/sec/android/app/ve/bgm/BGMManager;->addBGM(Lcom/sec/android/app/ve/bgm/BGM;)V

    .line 141
    sget-object v6, Lcom/sec/android/app/ve/VEApp;->gBGMManager:Lcom/sec/android/app/ve/bgm/BGMManager;

    new-instance v7, Lcom/sec/android/app/storycam/bgm/GoodyBGM;

    invoke-direct {v7}, Lcom/sec/android/app/storycam/bgm/GoodyBGM;-><init>()V

    invoke-virtual {v6, v7}, Lcom/sec/android/app/ve/bgm/BGMManager;->addBGM(Lcom/sec/android/app/ve/bgm/BGM;)V

    .line 142
    sget-object v6, Lcom/sec/android/app/ve/VEApp;->gBGMManager:Lcom/sec/android/app/ve/bgm/BGMManager;

    new-instance v7, Lcom/sec/android/app/storycam/bgm/ForChildrenBGM;

    invoke-direct {v7}, Lcom/sec/android/app/storycam/bgm/ForChildrenBGM;-><init>()V

    invoke-virtual {v6, v7}, Lcom/sec/android/app/ve/bgm/BGMManager;->addBGM(Lcom/sec/android/app/ve/bgm/BGM;)V

    .line 143
    sget-object v6, Lcom/sec/android/app/ve/VEApp;->gBGMManager:Lcom/sec/android/app/ve/bgm/BGMManager;

    new-instance v7, Lcom/sec/android/app/storycam/bgm/MildNovaBGM;

    invoke-direct {v7}, Lcom/sec/android/app/storycam/bgm/MildNovaBGM;-><init>()V

    invoke-virtual {v6, v7}, Lcom/sec/android/app/ve/bgm/BGMManager;->addBGM(Lcom/sec/android/app/ve/bgm/BGM;)V

    .line 147
    :cond_2
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/app/video/editor/external/NativeInterface;->applicationEntryPoint()V

    .line 155
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v6

    if-eqz v6, :cond_3

    .line 156
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v6

    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getFilesDirectory()Ljava/io/File;

    move-result-object v7

    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/samsung/app/video/editor/external/NativeInterface;->updatePackageforEngine(Ljava/lang/String;)V

    .line 159
    :cond_3
    new-instance v1, Ljava/io/File;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v7

    invoke-virtual {v7}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, "/MovieMakerLib/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v1, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 160
    .local v1, "movieMakerLib":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v6

    if-nez v6, :cond_6

    .line 161
    :cond_4
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    move-result v6

    if-nez v6, :cond_6

    .line 181
    .end local v4    # "result":Z
    :cond_5
    :goto_1
    return v4

    .line 76
    .end local v1    # "movieMakerLib":Ljava/io/File;
    :catch_0
    move-exception v2

    .line 77
    .local v2, "noClassDefError":Ljava/lang/NoClassDefFoundError;
    invoke-virtual {v2}, Ljava/lang/NoClassDefFoundError;->printStackTrace()V

    goto/16 :goto_0

    .line 80
    .end local v2    # "noClassDefError":Ljava/lang/NoClassDefFoundError;
    :catch_1
    move-exception v3

    .line 81
    .local v3, "noMethodError":Ljava/lang/NoSuchMethodError;
    invoke-virtual {v3}, Ljava/lang/NoSuchMethodError;->printStackTrace()V

    goto/16 :goto_0

    .line 164
    .end local v3    # "noMethodError":Ljava/lang/NoSuchMethodError;
    .restart local v1    # "movieMakerLib":Ljava/io/File;
    .restart local v4    # "result":Z
    :cond_6
    new-instance v5, Ljava/io/File;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v7

    invoke-virtual {v7}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, "/RW_LIB/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 165
    .local v5, "rw_LIB":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->mkdirs()Z

    .line 166
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_7

    invoke-virtual {v5}, Ljava/io/File;->isDirectory()Z

    move-result v6

    if-nez v6, :cond_8

    .line 167
    :cond_7
    invoke-virtual {v5}, Ljava/io/File;->mkdirs()Z

    move-result v6

    if-eqz v6, :cond_5

    .line 172
    :cond_8
    const-string v6, "VsConfig.cfg"

    invoke-static {v6}, Lcom/sec/android/app/storycam/VEAppSpecific;->copyFileFromAssetsToFilesDir(Ljava/lang/String;)V

    .line 173
    const-string v6, "sandstone.jpg"

    invoke-static {v6}, Lcom/sec/android/app/storycam/VEAppSpecific;->copyFileFromAssetsToFilesDir(Ljava/lang/String;)V

    .line 174
    const-string v6, "texture_strokes.jpg"

    invoke-static {v6}, Lcom/sec/android/app/storycam/VEAppSpecific;->copyFileFromAssetsToFilesDir(Ljava/lang/String;)V

    .line 175
    const-string v6, "_1.gmm"

    invoke-static {v6}, Lcom/sec/android/app/storycam/VEAppSpecific;->copyFileFromAssetsToFilesDir(Ljava/lang/String;)V

    .line 176
    const-string v6, "_2.gmm"

    invoke-static {v6}, Lcom/sec/android/app/storycam/VEAppSpecific;->copyFileFromAssetsToFilesDir(Ljava/lang/String;)V

    .line 177
    const-string v6, "_3.gmm"

    invoke-static {v6}, Lcom/sec/android/app/storycam/VEAppSpecific;->copyFileFromAssetsToFilesDir(Ljava/lang/String;)V

    .line 178
    const-string v6, "_4.gmm"

    invoke-static {v6}, Lcom/sec/android/app/storycam/VEAppSpecific;->copyFileFromAssetsToFilesDir(Ljava/lang/String;)V

    .line 179
    const-string v6, "_5.gmm"

    invoke-static {v6}, Lcom/sec/android/app/storycam/VEAppSpecific;->copyFileFromAssetsToFilesDir(Ljava/lang/String;)V

    .line 181
    const/4 v4, 0x1

    goto :goto_1
.end method
