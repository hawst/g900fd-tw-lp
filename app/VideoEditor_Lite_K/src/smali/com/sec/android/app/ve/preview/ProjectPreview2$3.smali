.class Lcom/sec/android/app/ve/preview/ProjectPreview2$3;
.super Ljava/lang/Object;
.source "ProjectPreview2.java"

# interfaces
.implements Lcom/samsung/app/video/editor/external/NativeInterface$previewPlayerStateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/ve/preview/ProjectPreview2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$samsung$app$video$editor$external$NativeInterface$playerState:[I


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/ve/preview/ProjectPreview2;


# direct methods
.method static synthetic $SWITCH_TABLE$com$samsung$app$video$editor$external$NativeInterface$playerState()[I
    .locals 3

    .prologue
    .line 650
    sget-object v0, Lcom/sec/android/app/ve/preview/ProjectPreview2$3;->$SWITCH_TABLE$com$samsung$app$video$editor$external$NativeInterface$playerState:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface$playerState;->values()[Lcom/samsung/app/video/editor/external/NativeInterface$playerState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/samsung/app/video/editor/external/NativeInterface$playerState;->VT_PREVIEW_PLAYER_AUDIO_DECODE_FAIL:Lcom/samsung/app/video/editor/external/NativeInterface$playerState;

    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/NativeInterface$playerState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_d

    :goto_1
    :try_start_1
    sget-object v1, Lcom/samsung/app/video/editor/external/NativeInterface$playerState;->VT_PREVIEW_PLAYER_CLOSED:Lcom/samsung/app/video/editor/external/NativeInterface$playerState;

    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/NativeInterface$playerState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_c

    :goto_2
    :try_start_2
    sget-object v1, Lcom/samsung/app/video/editor/external/NativeInterface$playerState;->VT_PREVIEW_PLAYER_CREATED:Lcom/samsung/app/video/editor/external/NativeInterface$playerState;

    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/NativeInterface$playerState;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_b

    :goto_3
    :try_start_3
    sget-object v1, Lcom/samsung/app/video/editor/external/NativeInterface$playerState;->VT_PREVIEW_PLAYER_DAM_CONFIGURE:Lcom/samsung/app/video/editor/external/NativeInterface$playerState;

    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/NativeInterface$playerState;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_a

    :goto_4
    :try_start_4
    sget-object v1, Lcom/samsung/app/video/editor/external/NativeInterface$playerState;->VT_PREVIEW_PLAYER_DAM_DISPLAY:Lcom/samsung/app/video/editor/external/NativeInterface$playerState;

    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/NativeInterface$playerState;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_9

    :goto_5
    :try_start_5
    sget-object v1, Lcom/samsung/app/video/editor/external/NativeInterface$playerState;->VT_PREVIEW_PLAYER_DAM_UNREGISTER:Lcom/samsung/app/video/editor/external/NativeInterface$playerState;

    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/NativeInterface$playerState;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_8

    :goto_6
    :try_start_6
    sget-object v1, Lcom/samsung/app/video/editor/external/NativeInterface$playerState;->VT_PREVIEW_PLAYER_FILE_OPEN_FAIL:Lcom/samsung/app/video/editor/external/NativeInterface$playerState;

    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/NativeInterface$playerState;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_7

    :goto_7
    :try_start_7
    sget-object v1, Lcom/samsung/app/video/editor/external/NativeInterface$playerState;->VT_PREVIEW_PLAYER_PAUSED:Lcom/samsung/app/video/editor/external/NativeInterface$playerState;

    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/NativeInterface$playerState;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_6

    :goto_8
    :try_start_8
    sget-object v1, Lcom/samsung/app/video/editor/external/NativeInterface$playerState;->VT_PREVIEW_PLAYER_PLAYBACK_COMPLETE:Lcom/samsung/app/video/editor/external/NativeInterface$playerState;

    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/NativeInterface$playerState;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_5

    :goto_9
    :try_start_9
    sget-object v1, Lcom/samsung/app/video/editor/external/NativeInterface$playerState;->VT_PREVIEW_PLAYER_PLAYING:Lcom/samsung/app/video/editor/external/NativeInterface$playerState;

    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/NativeInterface$playerState;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_4

    :goto_a
    :try_start_a
    sget-object v1, Lcom/samsung/app/video/editor/external/NativeInterface$playerState;->VT_PREVIEW_PLAYER_RESUMED:Lcom/samsung/app/video/editor/external/NativeInterface$playerState;

    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/NativeInterface$playerState;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_3

    :goto_b
    :try_start_b
    sget-object v1, Lcom/samsung/app/video/editor/external/NativeInterface$playerState;->VT_PREVIEW_PLAYER_STOPED:Lcom/samsung/app/video/editor/external/NativeInterface$playerState;

    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/NativeInterface$playerState;->ordinal()I

    move-result v1

    const/16 v2, 0xc

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_2

    :goto_c
    :try_start_c
    sget-object v1, Lcom/samsung/app/video/editor/external/NativeInterface$playerState;->VT_PREVIEW_PLAYER_STOPED_ON_ERROR:Lcom/samsung/app/video/editor/external/NativeInterface$playerState;

    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/NativeInterface$playerState;->ordinal()I

    move-result v1

    const/16 v2, 0xd

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_1

    :goto_d
    :try_start_d
    sget-object v1, Lcom/samsung/app/video/editor/external/NativeInterface$playerState;->VT_PREVIEW_PLAYER_VIDEO_DECODE_FAIL:Lcom/samsung/app/video/editor/external/NativeInterface$playerState;

    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/NativeInterface$playerState;->ordinal()I

    move-result v1

    const/16 v2, 0xe

    aput v2, v0, v1
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_0

    :goto_e
    sput-object v0, Lcom/sec/android/app/ve/preview/ProjectPreview2$3;->$SWITCH_TABLE$com$samsung$app$video$editor$external$NativeInterface$playerState:[I

    goto/16 :goto_0

    :catch_0
    move-exception v1

    goto :goto_e

    :catch_1
    move-exception v1

    goto :goto_d

    :catch_2
    move-exception v1

    goto :goto_c

    :catch_3
    move-exception v1

    goto :goto_b

    :catch_4
    move-exception v1

    goto :goto_a

    :catch_5
    move-exception v1

    goto :goto_9

    :catch_6
    move-exception v1

    goto :goto_8

    :catch_7
    move-exception v1

    goto :goto_7

    :catch_8
    move-exception v1

    goto :goto_6

    :catch_9
    move-exception v1

    goto :goto_5

    :catch_a
    move-exception v1

    goto :goto_4

    :catch_b
    move-exception v1

    goto/16 :goto_3

    :catch_c
    move-exception v1

    goto/16 :goto_2

    :catch_d
    move-exception v1

    goto/16 :goto_1
.end method

.method constructor <init>(Lcom/sec/android/app/ve/preview/ProjectPreview2;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2$3;->this$0:Lcom/sec/android/app/ve/preview/ProjectPreview2;

    .line 650
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onStateChanged(Lcom/samsung/app/video/editor/external/NativeInterface$playerState;)V
    .locals 4
    .param p1, "state"    # Lcom/samsung/app/video/editor/external/NativeInterface$playerState;

    .prologue
    const/4 v3, 0x2

    .line 654
    invoke-static {}, Lcom/sec/android/app/ve/preview/ProjectPreview2$3;->$SWITCH_TABLE$com$samsung$app$video$editor$external$NativeInterface$playerState()[I

    move-result-object v0

    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/NativeInterface$playerState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 749
    :goto_0
    :pswitch_0
    return-void

    .line 659
    :pswitch_1
    const-string v0, "PLAYER_STATE"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "VT_PREVIEW_PLAYER_STOPED_ON_ERROR ==> NextState = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2$3;->this$0:Lcom/sec/android/app/ve/preview/ProjectPreview2;

    # getter for: Lcom/sec/android/app/ve/preview/ProjectPreview2;->mNextState:I
    invoke-static {v2}, Lcom/sec/android/app/ve/preview/ProjectPreview2;->access$10(Lcom/sec/android/app/ve/preview/ProjectPreview2;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " CurrentState = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2$3;->this$0:Lcom/sec/android/app/ve/preview/ProjectPreview2;

    # getter for: Lcom/sec/android/app/ve/preview/ProjectPreview2;->mCurrentState:I
    invoke-static {v2}, Lcom/sec/android/app/ve/preview/ProjectPreview2;->access$3(Lcom/sec/android/app/ve/preview/ProjectPreview2;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 666
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2$3;->this$0:Lcom/sec/android/app/ve/preview/ProjectPreview2;

    # getter for: Lcom/sec/android/app/ve/preview/ProjectPreview2;->mCurrentState:I
    invoke-static {v0}, Lcom/sec/android/app/ve/preview/ProjectPreview2;->access$3(Lcom/sec/android/app/ve/preview/ProjectPreview2;)I

    move-result v0

    if-ne v0, v3, :cond_0

    .line 667
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2$3;->this$0:Lcom/sec/android/app/ve/preview/ProjectPreview2;

    const/4 v1, -0x1

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/preview/ProjectPreview2;->access$11(Lcom/sec/android/app/ve/preview/ProjectPreview2;I)V

    .line 668
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2$3;->this$0:Lcom/sec/android/app/ve/preview/ProjectPreview2;

    # invokes: Lcom/sec/android/app/ve/preview/ProjectPreview2;->handlePlayTimeError()V
    invoke-static {v0}, Lcom/sec/android/app/ve/preview/ProjectPreview2;->access$12(Lcom/sec/android/app/ve/preview/ProjectPreview2;)V

    .line 670
    :cond_0
    const-string v0, "PLAYER_STATE"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "VT_PREVIEW_PLAYER_STOPED_ON_ERROR ==> NextState = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2$3;->this$0:Lcom/sec/android/app/ve/preview/ProjectPreview2;

    # getter for: Lcom/sec/android/app/ve/preview/ProjectPreview2;->mNextState:I
    invoke-static {v2}, Lcom/sec/android/app/ve/preview/ProjectPreview2;->access$10(Lcom/sec/android/app/ve/preview/ProjectPreview2;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " CurrentState = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2$3;->this$0:Lcom/sec/android/app/ve/preview/ProjectPreview2;

    # getter for: Lcom/sec/android/app/ve/preview/ProjectPreview2;->mCurrentState:I
    invoke-static {v2}, Lcom/sec/android/app/ve/preview/ProjectPreview2;->access$3(Lcom/sec/android/app/ve/preview/ProjectPreview2;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 695
    :pswitch_2
    const-string v0, "PLAYER_STATE"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "VT_PREVIEW_PLAYER_PLAYBACK_COMPLETE ==> NextState = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2$3;->this$0:Lcom/sec/android/app/ve/preview/ProjectPreview2;

    # getter for: Lcom/sec/android/app/ve/preview/ProjectPreview2;->mNextState:I
    invoke-static {v2}, Lcom/sec/android/app/ve/preview/ProjectPreview2;->access$10(Lcom/sec/android/app/ve/preview/ProjectPreview2;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " CurrentState = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2$3;->this$0:Lcom/sec/android/app/ve/preview/ProjectPreview2;

    # getter for: Lcom/sec/android/app/ve/preview/ProjectPreview2;->mCurrentState:I
    invoke-static {v2}, Lcom/sec/android/app/ve/preview/ProjectPreview2;->access$3(Lcom/sec/android/app/ve/preview/ProjectPreview2;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 697
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2$3;->this$0:Lcom/sec/android/app/ve/preview/ProjectPreview2;

    # getter for: Lcom/sec/android/app/ve/preview/ProjectPreview2;->mCurrentState:I
    invoke-static {v0}, Lcom/sec/android/app/ve/preview/ProjectPreview2;->access$3(Lcom/sec/android/app/ve/preview/ProjectPreview2;)I

    move-result v0

    if-ne v0, v3, :cond_1

    .line 698
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2$3;->this$0:Lcom/sec/android/app/ve/preview/ProjectPreview2;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/preview/ProjectPreview2;->access$13(Lcom/sec/android/app/ve/preview/ProjectPreview2;Z)V

    .line 699
    iget-object v0, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2$3;->this$0:Lcom/sec/android/app/ve/preview/ProjectPreview2;

    # invokes: Lcom/sec/android/app/ve/preview/ProjectPreview2;->handleCompletedEvent()V
    invoke-static {v0}, Lcom/sec/android/app/ve/preview/ProjectPreview2;->access$14(Lcom/sec/android/app/ve/preview/ProjectPreview2;)V

    .line 708
    :cond_1
    const-string v0, "PLAYER_STATE"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "VT_PREVIEW_PLAYER_PLAYBACK_COMPLETE ==> NextState = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2$3;->this$0:Lcom/sec/android/app/ve/preview/ProjectPreview2;

    # getter for: Lcom/sec/android/app/ve/preview/ProjectPreview2;->mNextState:I
    invoke-static {v2}, Lcom/sec/android/app/ve/preview/ProjectPreview2;->access$10(Lcom/sec/android/app/ve/preview/ProjectPreview2;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " CurrentState = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/ve/preview/ProjectPreview2$3;->this$0:Lcom/sec/android/app/ve/preview/ProjectPreview2;

    # getter for: Lcom/sec/android/app/ve/preview/ProjectPreview2;->mCurrentState:I
    invoke-static {v2}, Lcom/sec/android/app/ve/preview/ProjectPreview2;->access$3(Lcom/sec/android/app/ve/preview/ProjectPreview2;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 735
    :pswitch_3
    const-string v0, "PLAYER_STATE"

    const-string v1, "VT_PREVIEW_PLAYER_DAM_CONFIGURE"

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 739
    :pswitch_4
    const-string v0, "PLAYER_STATE"

    const-string v1, "VT_PREVIEW_PLAYER_DAM_DISPLAY"

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 743
    :pswitch_5
    const-string v0, "PLAYER_STATE"

    const-string v1, "VT_PREVIEW_PLAYER_DAM_UNREGISTER"

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 654
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
