.class public Lcom/sec/android/app/storycam/theme/VintageTheme;
.super Lcom/sec/android/app/ve/theme/Theme;
.source "VintageTheme.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/sec/android/app/ve/theme/Theme;-><init>()V

    .line 13
    const v0, 0x7f02006e

    iput v0, p0, Lcom/sec/android/app/storycam/theme/VintageTheme;->mThemeDrawableResId:I

    .line 14
    return-void
.end method


# virtual methods
.method protected getPreviewPathInAssets()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getThemeDataPath()Ljava/lang/String;
    .locals 2

    .prologue
    .line 48
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-super {p0}, Lcom/sec/android/app/ve/theme/Theme;->getThemeDataPath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/storycam/theme/VintageTheme;->getThemeName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getThemeDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    const v0, 0x7f070274

    invoke-static {v0}, Lcom/sec/android/app/ve/VEApp;->getStringValue(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getThemeDisplayName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    const v0, 0x7f070287

    invoke-static {v0}, Lcom/sec/android/app/ve/VEApp;->getStringValue(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getThemeDownloadAlertMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getThemeID()I
    .locals 1

    .prologue
    .line 23
    const/16 v0, 0x10

    return v0
.end method

.method protected getThemeJSONFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    const-string v0, "velite_theme_vintage_k.json"

    return-object v0
.end method

.method protected getThemeName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 18
    const-string v0, "vintage"

    return-object v0
.end method

.method protected getThemePackage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    const-string v0, "com.sec.android.app.storycam"

    return-object v0
.end method

.method protected getThemePathFromRaw()I
    .locals 1

    .prologue
    .line 63
    const/4 v0, 0x0

    return v0
.end method
