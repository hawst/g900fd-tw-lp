.class Lcom/sec/android/app/storycam/VEAppSpecific$Adaper;
.super Ljava/lang/Object;
.source "VEAppSpecific.java"

# interfaces
.implements Lcom/sec/android/app/ve/VEAdapter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/storycam/VEAppSpecific;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Adaper"
.end annotation


# instance fields
.field private mLoadingIcon:Landroid/graphics/Bitmap;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 233
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 235
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/storycam/VEAppSpecific$Adaper;->mLoadingIcon:Landroid/graphics/Bitmap;

    .line 233
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/storycam/VEAppSpecific$Adaper;)V
    .locals 0

    .prologue
    .line 233
    invoke-direct {p0}, Lcom/sec/android/app/storycam/VEAppSpecific$Adaper;-><init>()V

    return-void
.end method


# virtual methods
.method public getAppIconResource()I
    .locals 1

    .prologue
    .line 239
    const v0, 0x7f020032

    return v0
.end method

.method public getLoadingIconBitmap()Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 244
    iget-object v0, p0, Lcom/sec/android/app/storycam/VEAppSpecific$Adaper;->mLoadingIcon:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 245
    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getresource()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020040

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/storycam/VEAppSpecific$Adaper;->mLoadingIcon:Landroid/graphics/Bitmap;

    .line 246
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/storycam/VEAppSpecific$Adaper;->mLoadingIcon:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getProjectShareHandlerActivity()Ljava/lang/String;
    .locals 1

    .prologue
    .line 287
    const/4 v0, 0x0

    return-object v0
.end method

.method public getResolutionValue(I)I
    .locals 0
    .param p1, "pos"    # I

    .prologue
    .line 282
    return p1
.end method

.method public loadNativeLibraries()V
    .locals 3

    .prologue
    .line 251
    sget-object v1, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    const-string v2, "4.4.4"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 252
    const-string v1, "_AnimationEngine_kk443"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 261
    :goto_0
    const-string v1, "ro.board.platform"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 263
    .local v0, "platformName":Ljava/lang/String;
    if-nez v0, :cond_3

    .line 264
    const-string v1, "LIBRARY"

    const-string v2, "platform name cannot be found"

    invoke-static {v1, v2}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    :goto_1
    return-void

    .line 253
    .end local v0    # "platformName":Ljava/lang/String;
    :cond_0
    sget-object v1, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    const-string v2, "4.4.3"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 254
    const-string v1, "_AnimationEngine_kk443"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    goto :goto_0

    .line 255
    :cond_1
    sget-object v1, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    const-string v2, "5"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 256
    const-string v1, "_AnimationEngine_L"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    goto :goto_0

    .line 259
    :cond_2
    const-string v1, "_AnimationEngine_kk44"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    goto :goto_0

    .line 266
    .restart local v0    # "platformName":Ljava/lang/String;
    :cond_3
    const-string v1, "exynos"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 267
    const-string v1, "videoeditorlite_ha"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 268
    const-string v1, "velitejni_ha"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    goto :goto_1

    .line 270
    :cond_4
    const-string v1, "msm"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, "apq8084"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 271
    :cond_5
    const-string v1, "videoeditorlite_hf"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 272
    const-string v1, "velitejni_hf"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    goto :goto_1

    .line 275
    :cond_6
    const-string v1, "LIBRARY"

    const-string v2, "no library matched platform."

    invoke-static {v1, v2}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method
