.class Lcom/sec/android/app/ve/projectsharing/ChordChannelService$4;
.super Ljava/lang/Object;
.source "ChordChannelService.java"

# interfaces
.implements Lcom/samsung/chord/ChordManager$INetworkListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/ve/projectsharing/ChordChannelService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$4;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    .line 403
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onConnected(I)V
    .locals 3
    .param p1, "networkInterface"    # I

    .prologue
    .line 412
    const-string v0, "ProjectSharing"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "inside networkListener.onConnected, networkInterface = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$4;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    # invokes: Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->getInterfaceName(I)Ljava/lang/String;
    invoke-static {v2, p1}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->access$20(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 413
    iget-object v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$4;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    # invokes: Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->isWiFiDirectInterfaceAvailable()Z
    invoke-static {v0}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->access$1(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 414
    iget-object v0, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$4;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    const/4 v1, 0x1

    # invokes: Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->startChord(I)V
    invoke-static {v0, v1}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->access$21(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;I)V

    .line 415
    :cond_0
    return-void
.end method

.method public onDisconnected(I)V
    .locals 3
    .param p1, "networkInterface"    # I

    .prologue
    .line 407
    const-string v0, "ProjectSharing"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "inside networkListener.onDisconnected, networkInterface = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/app/ve/projectsharing/ChordChannelService$4;->this$0:Lcom/sec/android/app/ve/projectsharing/ChordChannelService;

    # invokes: Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->getInterfaceName(I)Ljava/lang/String;
    invoke-static {v2, p1}, Lcom/sec/android/app/ve/projectsharing/ChordChannelService;->access$20(Lcom/sec/android/app/ve/projectsharing/ChordChannelService;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 408
    return-void
.end method
