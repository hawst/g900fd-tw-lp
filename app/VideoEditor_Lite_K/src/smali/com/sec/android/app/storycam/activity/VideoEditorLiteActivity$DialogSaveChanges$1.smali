.class Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogSaveChanges$1;
.super Ljava/lang/Object;
.source "VideoEditorLiteActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogSaveChanges;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogSaveChanges;


# direct methods
.method constructor <init>(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogSaveChanges;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogSaveChanges$1;->this$1:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogSaveChanges;

    .line 977
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "id"    # I

    .prologue
    .line 979
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 980
    sget-object v4, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v4}, Lcom/sec/android/app/storycam/AppDataManager;->getCurrentTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v1

    .line 981
    .local v1, "trans":Lcom/samsung/app/video/editor/external/TranscodeElement;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getTotalDuration()J

    move-result-wide v2

    .line 982
    .local v2, "totalDuration":J
    :goto_0
    invoke-static {v2, v3}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog;->newInstance(J)Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$ExportResDialog;

    move-result-object v0

    .line 983
    .local v0, "exportdialog":Landroid/app/DialogFragment;
    iget-object v4, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogSaveChanges$1;->this$1:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogSaveChanges;

    # getter for: Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogSaveChanges;->activity:Landroid/app/Activity;
    invoke-static {v4}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogSaveChanges;->access$0(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$DialogSaveChanges;)Landroid/app/Activity;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/sec/android/app/ve/common/AndroidUtils;->showDialog(Landroid/app/DialogFragment;Landroid/app/Activity;)V

    .line 985
    return-void

    .line 981
    .end local v0    # "exportdialog":Landroid/app/DialogFragment;
    .end local v2    # "totalDuration":J
    :cond_0
    const-wide/16 v2, 0x0

    goto :goto_0
.end method
