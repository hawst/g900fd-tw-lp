.class public Lcom/sec/android/app/ve/thread/AlbumArtThumbFetcher;
.super Ljava/lang/Thread;
.source "AlbumArtThumbFetcher.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/ve/thread/AlbumArtThumbFetcher$AlbumArtThumbFetcherFetcherCallbak;,
        Lcom/sec/android/app/ve/thread/AlbumArtThumbFetcher$FetcherInfo;
    }
.end annotation


# instance fields
.field private final mJobList:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Lcom/sec/android/app/ve/thread/AlbumArtThumbFetcher$FetcherInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mTerminate:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 24
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/ve/thread/AlbumArtThumbFetcher;->mJobList:Ljava/util/Vector;

    .line 23
    return-void
.end method


# virtual methods
.method public addOperation(Ljava/lang/String;ILandroid/view/View;Lcom/sec/android/app/ve/thread/AlbumArtThumbFetcher$AlbumArtThumbFetcherFetcherCallbak;)V
    .locals 2
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "aPosition"    # I
    .param p3, "targetView"    # Landroid/view/View;
    .param p4, "callback"    # Lcom/sec/android/app/ve/thread/AlbumArtThumbFetcher$AlbumArtThumbFetcherFetcherCallbak;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/app/ve/thread/AlbumArtThumbFetcher;->mJobList:Ljava/util/Vector;

    new-instance v1, Lcom/sec/android/app/ve/thread/AlbumArtThumbFetcher$FetcherInfo;

    invoke-direct {v1, p1, p2, p3, p4}, Lcom/sec/android/app/ve/thread/AlbumArtThumbFetcher$FetcherInfo;-><init>(Ljava/lang/String;ILandroid/view/View;Lcom/sec/android/app/ve/thread/AlbumArtThumbFetcher$AlbumArtThumbFetcherFetcherCallbak;)V

    invoke-virtual {v0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 42
    monitor-enter p0

    .line 43
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V

    .line 42
    monitor-exit p0

    .line 45
    return-void

    .line 42
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public run()V
    .locals 12

    .prologue
    const/4 v11, 0x0

    .line 48
    :goto_0
    iget-boolean v7, p0, Lcom/sec/android/app/ve/thread/AlbumArtThumbFetcher;->mTerminate:Z

    if-eqz v7, :cond_3

    .line 108
    return-void

    .line 51
    :cond_0
    iget-object v7, p0, Lcom/sec/android/app/ve/thread/AlbumArtThumbFetcher;->mJobList:Ljava/util/Vector;

    invoke-virtual {v7, v11}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/ve/thread/AlbumArtThumbFetcher$FetcherInfo;

    .line 53
    .local v2, "info":Lcom/sec/android/app/ve/thread/AlbumArtThumbFetcher$FetcherInfo;
    # getter for: Lcom/sec/android/app/ve/thread/AlbumArtThumbFetcher$FetcherInfo;->callback:Lcom/sec/android/app/ve/thread/AlbumArtThumbFetcher$AlbumArtThumbFetcherFetcherCallbak;
    invoke-static {v2}, Lcom/sec/android/app/ve/thread/AlbumArtThumbFetcher$FetcherInfo;->access$0(Lcom/sec/android/app/ve/thread/AlbumArtThumbFetcher$FetcherInfo;)Lcom/sec/android/app/ve/thread/AlbumArtThumbFetcher$AlbumArtThumbFetcherFetcherCallbak;

    move-result-object v7

    # getter for: Lcom/sec/android/app/ve/thread/AlbumArtThumbFetcher$FetcherInfo;->targetView:Landroid/view/View;
    invoke-static {v2}, Lcom/sec/android/app/ve/thread/AlbumArtThumbFetcher$FetcherInfo;->access$1(Lcom/sec/android/app/ve/thread/AlbumArtThumbFetcher$FetcherInfo;)Landroid/view/View;

    move-result-object v8

    .line 54
    # getter for: Lcom/sec/android/app/ve/thread/AlbumArtThumbFetcher$FetcherInfo;->position:I
    invoke-static {v2}, Lcom/sec/android/app/ve/thread/AlbumArtThumbFetcher$FetcherInfo;->access$2(Lcom/sec/android/app/ve/thread/AlbumArtThumbFetcher$FetcherInfo;)I

    move-result v9

    # getter for: Lcom/sec/android/app/ve/thread/AlbumArtThumbFetcher$FetcherInfo;->filePath:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/ve/thread/AlbumArtThumbFetcher$FetcherInfo;->access$3(Lcom/sec/android/app/ve/thread/AlbumArtThumbFetcher$FetcherInfo;)Ljava/lang/String;

    move-result-object v10

    .line 53
    invoke-interface {v7, v8, v9, v10}, Lcom/sec/android/app/ve/thread/AlbumArtThumbFetcher$AlbumArtThumbFetcherFetcherCallbak;->isTargetViewExist(Landroid/view/View;ILjava/lang/String;)Z

    move-result v7

    .line 54
    if-eqz v7, :cond_2

    .line 57
    :try_start_0
    # getter for: Lcom/sec/android/app/ve/thread/AlbumArtThumbFetcher$FetcherInfo;->filePath:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/ve/thread/AlbumArtThumbFetcher$FetcherInfo;->access$3(Lcom/sec/android/app/ve/thread/AlbumArtThumbFetcher$FetcherInfo;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v6

    .line 60
    .local v6, "uri":Landroid/net/Uri;
    const/4 v4, 0x0

    .line 63
    .local v4, "localBit":Landroid/graphics/Bitmap;
    :try_start_1
    new-instance v5, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v5}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 64
    .local v5, "mBitmapOptions":Landroid/graphics/BitmapFactory$Options;
    new-instance v1, Ljava/io/FileInputStream;

    invoke-virtual {v6}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v1, v7}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 66
    .local v1, "in":Ljava/io/InputStream;
    # getter for: Lcom/sec/android/app/ve/thread/AlbumArtThumbFetcher$FetcherInfo;->filePath:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/ve/thread/AlbumArtThumbFetcher$FetcherInfo;->access$3(Lcom/sec/android/app/ve/thread/AlbumArtThumbFetcher$FetcherInfo;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/app/ve/common/MediaUtils;->getImageSampleSize(Ljava/lang/String;)I

    move-result v7

    iput v7, v5, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 68
    const/16 v7, 0x50

    iput v7, v5, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 69
    const/16 v7, 0x50

    iput v7, v5, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 70
    const/4 v7, 0x0

    invoke-static {v1, v7, v5}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 74
    .local v3, "lBitmap":Landroid/graphics/Bitmap;
    if-eqz v3, :cond_1

    .line 75
    invoke-static {v3}, Lcom/sec/android/app/ve/util/CommonUtils;->copyBitmapLocally(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 77
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 79
    const/4 v3, 0x0

    .line 87
    .end local v1    # "in":Ljava/io/InputStream;
    .end local v3    # "lBitmap":Landroid/graphics/Bitmap;
    .end local v5    # "mBitmapOptions":Landroid/graphics/BitmapFactory$Options;
    :cond_1
    :goto_1
    :try_start_2
    # getter for: Lcom/sec/android/app/ve/thread/AlbumArtThumbFetcher$FetcherInfo;->callback:Lcom/sec/android/app/ve/thread/AlbumArtThumbFetcher$AlbumArtThumbFetcherFetcherCallbak;
    invoke-static {v2}, Lcom/sec/android/app/ve/thread/AlbumArtThumbFetcher$FetcherInfo;->access$0(Lcom/sec/android/app/ve/thread/AlbumArtThumbFetcher$FetcherInfo;)Lcom/sec/android/app/ve/thread/AlbumArtThumbFetcher$AlbumArtThumbFetcherFetcherCallbak;

    move-result-object v7

    # getter for: Lcom/sec/android/app/ve/thread/AlbumArtThumbFetcher$FetcherInfo;->targetView:Landroid/view/View;
    invoke-static {v2}, Lcom/sec/android/app/ve/thread/AlbumArtThumbFetcher$FetcherInfo;->access$1(Lcom/sec/android/app/ve/thread/AlbumArtThumbFetcher$FetcherInfo;)Landroid/view/View;

    move-result-object v8

    # getter for: Lcom/sec/android/app/ve/thread/AlbumArtThumbFetcher$FetcherInfo;->filePath:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/ve/thread/AlbumArtThumbFetcher$FetcherInfo;->access$3(Lcom/sec/android/app/ve/thread/AlbumArtThumbFetcher$FetcherInfo;)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v7, v8, v4, v9}, Lcom/sec/android/app/ve/thread/AlbumArtThumbFetcher$AlbumArtThumbFetcherFetcherCallbak;->bitmapCreated(Landroid/view/View;Landroid/graphics/Bitmap;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 92
    .end local v4    # "localBit":Landroid/graphics/Bitmap;
    .end local v6    # "uri":Landroid/net/Uri;
    :cond_2
    :goto_2
    iget-object v7, p0, Lcom/sec/android/app/ve/thread/AlbumArtThumbFetcher;->mJobList:Ljava/util/Vector;

    invoke-virtual {v7}, Ljava/util/Vector;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_3

    .line 93
    iget-object v7, p0, Lcom/sec/android/app/ve/thread/AlbumArtThumbFetcher;->mJobList:Ljava/util/Vector;

    invoke-virtual {v7, v11}, Ljava/util/Vector;->remove(I)Ljava/lang/Object;

    .line 49
    .end local v2    # "info":Lcom/sec/android/app/ve/thread/AlbumArtThumbFetcher$FetcherInfo;
    :cond_3
    iget-object v7, p0, Lcom/sec/android/app/ve/thread/AlbumArtThumbFetcher;->mJobList:Ljava/util/Vector;

    invoke-virtual {v7}, Ljava/util/Vector;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 97
    monitor-enter p0

    .line 99
    :try_start_3
    iget-object v7, p0, Lcom/sec/android/app/ve/thread/AlbumArtThumbFetcher;->mJobList:Ljava/util/Vector;

    invoke-virtual {v7}, Ljava/util/Vector;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 100
    const-wide/16 v8, 0x7d0

    invoke-virtual {p0, v8, v9}, Ljava/lang/Object;->wait(J)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 97
    :cond_4
    :goto_3
    :try_start_4
    monitor-exit p0

    goto/16 :goto_0

    :catchall_0
    move-exception v7

    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v7

    .line 81
    .restart local v2    # "info":Lcom/sec/android/app/ve/thread/AlbumArtThumbFetcher$FetcherInfo;
    .restart local v4    # "localBit":Landroid/graphics/Bitmap;
    .restart local v6    # "uri":Landroid/net/Uri;
    :catch_0
    move-exception v0

    .line 82
    .local v0, "e":Ljava/lang/OutOfMemoryError;
    :try_start_5
    const-string v7, "Got Outofmemory error while creating Bitmap"

    invoke-static {v7}, Lcom/sec/android/app/ve/common/LogUtils;->log(Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_1

    .line 88
    .end local v0    # "e":Ljava/lang/OutOfMemoryError;
    .end local v4    # "localBit":Landroid/graphics/Bitmap;
    .end local v6    # "uri":Landroid/net/Uri;
    :catch_1
    move-exception v0

    .line 89
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 83
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v4    # "localBit":Landroid/graphics/Bitmap;
    .restart local v6    # "uri":Landroid/net/Uri;
    :catch_2
    move-exception v0

    .line 84
    .restart local v0    # "e":Ljava/lang/Exception;
    :try_start_6
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1

    goto :goto_1

    .line 102
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v2    # "info":Lcom/sec/android/app/ve/thread/AlbumArtThumbFetcher$FetcherInfo;
    .end local v4    # "localBit":Landroid/graphics/Bitmap;
    .end local v6    # "uri":Landroid/net/Uri;
    :catch_3
    move-exception v0

    .line 103
    .restart local v0    # "e":Ljava/lang/Exception;
    :try_start_7
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_3
.end method

.method public terminate()V
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/app/ve/thread/AlbumArtThumbFetcher;->mJobList:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clear()V

    .line 32
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/ve/thread/AlbumArtThumbFetcher;->mTerminate:Z

    .line 34
    monitor-enter p0

    .line 35
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V

    .line 34
    monitor-exit p0

    .line 37
    return-void

    .line 34
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
