.class Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$BGMModesDialog$4;
.super Ljava/lang/Object;
.source "VideoEditorLiteActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$BGMModesDialog;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$BGMModesDialog;

.field private final synthetic val$listView:Landroid/widget/ListView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$BGMModesDialog;Landroid/widget/ListView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$BGMModesDialog$4;->this$1:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$BGMModesDialog;

    iput-object p2, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$BGMModesDialog$4;->val$listView:Landroid/widget/ListView;

    .line 2403
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6
    .param p1, "arg0"    # Landroid/content/DialogInterface;
    .param p2, "arg1"    # I

    .prologue
    .line 2408
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 2409
    iget-object v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$BGMModesDialog$4;->val$listView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getCheckedItemPosition()I

    move-result v0

    invoke-static {v0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$BGMModesDialog;->access$0(I)V

    .line 2410
    sget-object v0, Lcom/sec/android/app/storycam/VEAppSpecific;->gDataMgr:Lcom/sec/android/app/storycam/AppDataManager;

    invoke-virtual {v0}, Lcom/sec/android/app/storycam/AppDataManager;->getCurrentTranscodeElement()Lcom/samsung/app/video/editor/external/TranscodeElement;

    move-result-object v2

    .line 2411
    .local v2, "tElement":Lcom/samsung/app/video/editor/external/TranscodeElement;
    if-eqz v2, :cond_0

    .line 2412
    invoke-static {}, Lcom/sec/android/app/ve/theme/ThemeDataManager;->getInstance()Lcom/sec/android/app/ve/theme/ThemeDataManager;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/ve/VEApp;->gBGMManager:Lcom/sec/android/app/ve/bgm/BGMManager;

    invoke-virtual {v0, v2, v1}, Lcom/sec/android/app/ve/theme/ThemeDataManager;->modifyTranscodeWithBGMTranstionSlots(Lcom/samsung/app/video/editor/external/TranscodeElement;Lcom/sec/android/app/ve/bgm/BGMManager;)V

    .line 2413
    iget-object v0, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$BGMModesDialog$4;->this$1:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$BGMModesDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$BGMModesDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    iget-object v1, p0, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$BGMModesDialog$4;->this$1:Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$BGMModesDialog;

    invoke-virtual {v1}, Lcom/sec/android/app/storycam/activity/VideoEditorLiteActivity$BGMModesDialog;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;

    invoke-interface {v1}, Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;->getSummaryHandler()Landroid/os/Handler;

    move-result-object v1

    .line 2414
    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    .line 2413
    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/storycam/summary/Summary;->summarize(Lcom/sec/android/app/storycam/interfaces/UIManagerInterface;Landroid/os/Handler;Lcom/samsung/app/video/editor/external/TranscodeElement;ZJ)V

    .line 2417
    :cond_0
    return-void
.end method
