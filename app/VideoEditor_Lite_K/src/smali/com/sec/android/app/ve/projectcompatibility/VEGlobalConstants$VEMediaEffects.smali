.class public Lcom/sec/android/app/ve/projectcompatibility/VEGlobalConstants$VEMediaEffects;
.super Ljava/lang/Object;
.source "VEGlobalConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/ve/projectcompatibility/VEGlobalConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "VEMediaEffects"
.end annotation


# static fields
.field public static final VEMEDIAEFFECTS_BLACK_AND_WHITE:I = 0x1

.field public static final VEMEDIAEFFECTS_BLACK_AND_WHITE_K:I = 0x9

.field public static final VEMEDIAEFFECTS_BLACK_VIGNETTE:I = 0x18

.field public static final VEMEDIAEFFECTS_BLUR:I = 0x3

.field public static final VEMEDIAEFFECTS_CARTOON:I = 0x7

.field public static final VEMEDIAEFFECTS_CARTOON_K:I = 0xa

.field public static final VEMEDIAEFFECTS_FADEDCOLOR:I = 0xb

.field public static final VEMEDIAEFFECTS_FADE_FROM_BLACK:I = 0x1a

.field public static final VEMEDIAEFFECTS_FADE_TO_BLACK:I = 0x1b

.field public static final VEMEDIAEFFECTS_GAUSSIAN_BLUR:I = 0xc

.field public static final VEMEDIAEFFECTS_KENBURNS:I = 0x8

.field public static final VEMEDIAEFFECTS_LINEAR_DODGE_BLEND_TEXTURE:I = 0xd

.field public static final VEMEDIAEFFECTS_MIRROR_LEFT:I = 0xe

.field public static final VEMEDIAEFFECTS_MIRROR_RIGHT:I = 0xf

.field public static final VEMEDIAEFFECTS_MOSAIC:I = 0x10

.field public static final VEMEDIAEFFECTS_NATURE_AUTUMN_LEAF:I = 0x1c

.field public static final VEMEDIAEFFECTS_NATURE_CHERRY_BLOSSOM:I = 0x1d

.field public static final VEMEDIAEFFECTS_NATURE_FOG:I = 0x21

.field public static final VEMEDIAEFFECTS_NATURE_RAINBOW:I = 0x20

.field public static final VEMEDIAEFFECTS_NATURE_RAINFALL:I = 0x1e

.field public static final VEMEDIAEFFECTS_NATURE_SNOWFALL:I = 0x1f

.field public static final VEMEDIAEFFECTS_NATURE_THUNDER:I = 0x22

.field public static final VEMEDIAEFFECTS_NEGATIVE:I = 0x5

.field public static final VEMEDIAEFFECTS_NONE:I = 0x0

.field public static final VEMEDIAEFFECTS_PASTEL_SKETCH:I = 0x11

.field public static final VEMEDIAEFFECTS_SAND_STONE:I = 0x12

.field public static final VEMEDIAEFFECTS_SEPIA:I = 0x2

.field public static final VEMEDIAEFFECTS_SEPIA_K:I = 0x13

.field public static final VEMEDIAEFFECTS_SHARPEN:I = 0x4

.field public static final VEMEDIAEFFECTS_THERMAL:I = 0x14

.field public static final VEMEDIAEFFECTS_TINT:I = 0x15

.field public static final VEMEDIAEFFECTS_TURQUOISE:I = 0x16

.field public static final VEMEDIAEFFECTS_VID_MIX_MULTIPLE_TEXTURE:I = 0x19

.field public static final VEMEDIAEFFECTS_VINTAGE:I = 0x6

.field public static final VEMEDIAEFFECTS_VINTAGE_K:I = 0x17


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
