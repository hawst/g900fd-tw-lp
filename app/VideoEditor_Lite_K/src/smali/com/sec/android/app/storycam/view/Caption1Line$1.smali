.class Lcom/sec/android/app/storycam/view/Caption1Line$1;
.super Ljava/lang/Object;
.source "Caption1Line.java"

# interfaces
.implements Landroid/text/InputFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/storycam/view/Caption1Line;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# static fields
.field private static final SPACE_FIX:Ljava/lang/String; = "x"


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/storycam/view/Caption1Line;


# direct methods
.method constructor <init>(Lcom/sec/android/app/storycam/view/Caption1Line;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/storycam/view/Caption1Line$1;->this$0:Lcom/sec/android/app/storycam/view/Caption1Line;

    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;
    .locals 14
    .param p1, "source"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "end"    # I
    .param p4, "dest"    # Landroid/text/Spanned;
    .param p5, "dstart"    # I
    .param p6, "dend"    # I

    .prologue
    .line 111
    move/from16 v0, p2

    move/from16 v1, p3

    if-ne v0, v1, :cond_0

    .line 115
    const/4 v9, 0x0

    .line 165
    :goto_0
    return-object v9

    .line 118
    :cond_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-interface/range {p4 .. p4}, Landroid/text/Spanned;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v5, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 119
    .local v5, "currentString":Ljava/lang/StringBuilder;
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    .line 120
    .local v3, "bounds":Landroid/graphics/Rect;
    new-instance v6, Landroid/graphics/Rect;

    invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V

    .line 121
    .local v6, "spaceFixBounds":Landroid/graphics/Rect;
    new-instance v8, Landroid/graphics/Rect;

    invoke-direct {v8}, Landroid/graphics/Rect;-><init>()V

    .line 122
    .local v8, "temp":Landroid/graphics/Rect;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-interface/range {p1 .. p3}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v9

    invoke-direct {v4, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 125
    .local v4, "changesString":Ljava/lang/StringBuilder;
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    move/from16 v0, p5

    move/from16 v1, p6

    invoke-virtual {v5, v0, v1, v9}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 126
    iget-object v9, p0, Lcom/sec/android/app/storycam/view/Caption1Line$1;->this$0:Lcom/sec/android/app/storycam/view/Caption1Line;

    # getter for: Lcom/sec/android/app/storycam/view/Caption1Line;->canvasPaint:Landroid/graphics/Paint;
    invoke-static {v9}, Lcom/sec/android/app/storycam/view/Caption1Line;->access$0(Lcom/sec/android/app/storycam/view/Caption1Line;)Landroid/graphics/Paint;

    move-result-object v9

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v12

    invoke-virtual {v9, v10, v11, v12, v3}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 128
    iget-object v9, p0, Lcom/sec/android/app/storycam/view/Caption1Line$1;->this$0:Lcom/sec/android/app/storycam/view/Caption1Line;

    # getter for: Lcom/sec/android/app/storycam/view/Caption1Line;->canvasPaint:Landroid/graphics/Paint;
    invoke-static {v9}, Lcom/sec/android/app/storycam/view/Caption1Line;->access$0(Lcom/sec/android/app/storycam/view/Caption1Line;)Landroid/graphics/Paint;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "x"

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "x"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v12

    const-string v13, "x"

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v13

    shl-int/lit8 v13, v13, 0x1

    add-int/2addr v12, v13

    invoke-virtual {v9, v10, v11, v12, v6}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 129
    iget-object v9, p0, Lcom/sec/android/app/storycam/view/Caption1Line$1;->this$0:Lcom/sec/android/app/storycam/view/Caption1Line;

    # getter for: Lcom/sec/android/app/storycam/view/Caption1Line;->canvasPaint:Landroid/graphics/Paint;
    invoke-static {v9}, Lcom/sec/android/app/storycam/view/Caption1Line;->access$0(Lcom/sec/android/app/storycam/view/Caption1Line;)Landroid/graphics/Paint;

    move-result-object v9

    const-string v10, "x"

    const/4 v11, 0x0

    const-string v12, "x"

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v12

    invoke-virtual {v9, v10, v11, v12, v8}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 130
    invoke-virtual {v8}, Landroid/graphics/Rect;->width()I

    move-result v9

    shl-int/lit8 v7, v9, 0x1

    .line 131
    .local v7, "spaceFixWidth":I
    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v9

    sub-int v2, v9, v7

    .line 136
    .local v2, "actualWidth":I
    iget-object v9, p0, Lcom/sec/android/app/storycam/view/Caption1Line$1;->this$0:Lcom/sec/android/app/storycam/view/Caption1Line;

    # getter for: Lcom/sec/android/app/storycam/view/Caption1Line;->CANVAS_MAX_WIDTH:I
    invoke-static {v9}, Lcom/sec/android/app/storycam/view/Caption1Line;->access$1(Lcom/sec/android/app/storycam/view/Caption1Line;)I

    move-result v9

    if-gt v2, v9, :cond_3

    .line 137
    const/4 v9, 0x0

    goto/16 :goto_0

    .line 144
    :cond_1
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v9

    invoke-static {v9}, Ljava/lang/Character;->isLowSurrogate(C)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 145
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    .line 147
    :cond_2
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    .line 148
    const/4 v9, 0x0

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v10

    invoke-interface/range {p4 .. p4}, Landroid/text/Spanned;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v5, v9, v10, v11}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 149
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    move/from16 v0, p5

    move/from16 v1, p6

    invoke-virtual {v5, v0, v1, v9}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 150
    iget-object v9, p0, Lcom/sec/android/app/storycam/view/Caption1Line$1;->this$0:Lcom/sec/android/app/storycam/view/Caption1Line;

    # getter for: Lcom/sec/android/app/storycam/view/Caption1Line;->canvasPaint:Landroid/graphics/Paint;
    invoke-static {v9}, Lcom/sec/android/app/storycam/view/Caption1Line;->access$0(Lcom/sec/android/app/storycam/view/Caption1Line;)Landroid/graphics/Paint;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "x"

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "x"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v12

    const-string v13, "x"

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v13

    shl-int/lit8 v13, v13, 0x1

    add-int/2addr v12, v13

    invoke-virtual {v9, v10, v11, v12, v6}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 151
    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v9

    sub-int v2, v9, v7

    .line 143
    :cond_3
    iget-object v9, p0, Lcom/sec/android/app/storycam/view/Caption1Line$1;->this$0:Lcom/sec/android/app/storycam/view/Caption1Line;

    # getter for: Lcom/sec/android/app/storycam/view/Caption1Line;->CANVAS_MAX_WIDTH:I
    invoke-static {v9}, Lcom/sec/android/app/storycam/view/Caption1Line;->access$1(Lcom/sec/android/app/storycam/view/Caption1Line;)I

    move-result v9

    if-le v2, v9, :cond_4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v9

    if-gtz v9, :cond_1

    .line 156
    :cond_4
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v9

    if-nez v9, :cond_5

    .line 157
    iget-object v9, p0, Lcom/sec/android/app/storycam/view/Caption1Line$1;->this$0:Lcom/sec/android/app/storycam/view/Caption1Line;

    iget-object v10, p0, Lcom/sec/android/app/storycam/view/Caption1Line$1;->this$0:Lcom/sec/android/app/storycam/view/Caption1Line;

    # getter for: Lcom/sec/android/app/storycam/view/Caption1Line;->charLenToast:Landroid/widget/Toast;
    invoke-static {v10}, Lcom/sec/android/app/storycam/view/Caption1Line;->access$2(Lcom/sec/android/app/storycam/view/Caption1Line;)Landroid/widget/Toast;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/sec/android/app/storycam/view/Caption1Line;->showToastMessage(Landroid/widget/Toast;)V

    .line 158
    const-string v9, ""

    goto/16 :goto_0

    .line 161
    :cond_5
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v9

    sub-int v10, p3, p2

    if-eq v9, v10, :cond_6

    .line 162
    iget-object v9, p0, Lcom/sec/android/app/storycam/view/Caption1Line$1;->this$0:Lcom/sec/android/app/storycam/view/Caption1Line;

    iget-object v10, p0, Lcom/sec/android/app/storycam/view/Caption1Line$1;->this$0:Lcom/sec/android/app/storycam/view/Caption1Line;

    # getter for: Lcom/sec/android/app/storycam/view/Caption1Line;->charLenToast:Landroid/widget/Toast;
    invoke-static {v10}, Lcom/sec/android/app/storycam/view/Caption1Line;->access$2(Lcom/sec/android/app/storycam/view/Caption1Line;)Landroid/widget/Toast;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/sec/android/app/storycam/view/Caption1Line;->showToastMessage(Landroid/widget/Toast;)V

    .line 165
    :cond_6
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v9

    add-int v9, v9, p2

    move/from16 v0, p2

    invoke-interface {p1, v0, v9}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v9

    goto/16 :goto_0
.end method
