.class public interface abstract Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$FetcherCallBack;
.super Ljava/lang/Object;
.source "StoryBoardFetcherTaskManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "FetcherCallBack"
.end annotation


# virtual methods
.method public abstract bitmapCreated(JILandroid/graphics/Bitmap;Ljava/lang/String;Ljava/lang/String;ZLcom/sec/android/app/ve/thread/StoryBoardFetcherTaskManager$StoryBoardFetcherTask;)V
.end method

.method public abstract isTargetViewExist(J)Z
.end method

.method public abstract resizeBitmapToCache(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
.end method
