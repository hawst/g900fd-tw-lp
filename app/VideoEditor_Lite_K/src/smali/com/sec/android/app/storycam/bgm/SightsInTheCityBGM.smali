.class public Lcom/sec/android/app/storycam/bgm/SightsInTheCityBGM;
.super Lcom/sec/android/app/ve/bgm/BGM;
.source "SightsInTheCityBGM.java"


# static fields
.field private static synthetic $SWITCH_TABLE$com$sec$android$app$ve$bgm$BGM$BGMModes:[I


# instance fields
.field private fastModeBGM:[I

.field private mediumModeBGM:[I

.field private slowModeBGM:[I

.field private totalDurationBGM:I


# direct methods
.method static synthetic $SWITCH_TABLE$com$sec$android$app$ve$bgm$BGM$BGMModes()[I
    .locals 3

    .prologue
    .line 5
    sget-object v0, Lcom/sec/android/app/storycam/bgm/SightsInTheCityBGM;->$SWITCH_TABLE$com$sec$android$app$ve$bgm$BGM$BGMModes:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/sec/android/app/ve/bgm/BGM$BGMModes;->values()[Lcom/sec/android/app/ve/bgm/BGM$BGMModes;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/sec/android/app/ve/bgm/BGM$BGMModes;->FAST:Lcom/sec/android/app/ve/bgm/BGM$BGMModes;

    invoke-virtual {v1}, Lcom/sec/android/app/ve/bgm/BGM$BGMModes;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    :try_start_1
    sget-object v1, Lcom/sec/android/app/ve/bgm/BGM$BGMModes;->MEDIUM:Lcom/sec/android/app/ve/bgm/BGM$BGMModes;

    invoke-virtual {v1}, Lcom/sec/android/app/ve/bgm/BGM$BGMModes;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    :try_start_2
    sget-object v1, Lcom/sec/android/app/ve/bgm/BGM$BGMModes;->SLOW:Lcom/sec/android/app/ve/bgm/BGM$BGMModes;

    invoke-virtual {v1}, Lcom/sec/android/app/ve/bgm/BGM$BGMModes;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_3
    sput-object v0, Lcom/sec/android/app/storycam/bgm/SightsInTheCityBGM;->$SWITCH_TABLE$com$sec$android$app$ve$bgm$BGM$BGMModes:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_1
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/sec/android/app/ve/bgm/BGM;-><init>()V

    .line 8
    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    .line 13
    iput-object v0, p0, Lcom/sec/android/app/storycam/bgm/SightsInTheCityBGM;->slowModeBGM:[I

    .line 16
    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    .line 23
    iput-object v0, p0, Lcom/sec/android/app/storycam/bgm/SightsInTheCityBGM;->mediumModeBGM:[I

    .line 26
    const/16 v0, 0xb

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    .line 36
    iput-object v0, p0, Lcom/sec/android/app/storycam/bgm/SightsInTheCityBGM;->fastModeBGM:[I

    .line 39
    const v0, 0x91dc

    iput v0, p0, Lcom/sec/android/app/storycam/bgm/SightsInTheCityBGM;->totalDurationBGM:I

    .line 5
    return-void

    .line 8
    nop

    :array_0
    .array-data 4
        0x1450
        0x24b8
        0x3db8
        0x5140
        0x6658
        0x75f8
    .end array-data

    .line 16
    :array_1
    .array-data 4
        0x11f8
        0x2008
        0x3200
        0x3db8
        0x4cf4
        0x5c30
        0x6ef0
        0x80e8
    .end array-data

    .line 26
    :array_2
    .array-data 4
        0x1450
        0x2008
        0x2bc0
        0x3778
        0x4330
        0x4ee8
        0x5aa0
        0x6658
        0x7210
        0x7dc8
        0x88ea
    .end array-data
.end method


# virtual methods
.method protected getBGMFilepath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    const-string v0, "themedata/bgm/C3_Sights in the City.ogg"

    return-object v0
.end method

.method protected getBGMName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    const-string v0, "Sights in the City"

    return-object v0
.end method

.method protected getTotalDurationBGM()I
    .locals 1

    .prologue
    .line 57
    iget v0, p0, Lcom/sec/android/app/storycam/bgm/SightsInTheCityBGM;->totalDurationBGM:I

    return v0
.end method

.method protected getTransitionTimeStamps(Lcom/sec/android/app/ve/bgm/BGM$BGMModes;)[I
    .locals 2
    .param p1, "mode"    # Lcom/sec/android/app/ve/bgm/BGM$BGMModes;

    .prologue
    .line 43
    invoke-static {}, Lcom/sec/android/app/storycam/bgm/SightsInTheCityBGM;->$SWITCH_TABLE$com$sec$android$app$ve$bgm$BGM$BGMModes()[I

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/android/app/ve/bgm/BGM$BGMModes;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 51
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 45
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/storycam/bgm/SightsInTheCityBGM;->slowModeBGM:[I

    goto :goto_0

    .line 47
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/storycam/bgm/SightsInTheCityBGM;->mediumModeBGM:[I

    goto :goto_0

    .line 49
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/storycam/bgm/SightsInTheCityBGM;->fastModeBGM:[I

    goto :goto_0

    .line 43
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
