.class public Lcom/samsung/android/sdk/chord/ChordAgent;
.super Ljava/lang/Object;
.source "ChordAgent.java"


# instance fields
.field private a:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-string v0, "chord-v1.5"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 31
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    return-void
.end method


# virtual methods
.method public native ChordInit(Ljava/lang/String;)I
.end method

.method public synchronized native declared-synchronized ChordRelease()Z
.end method

.method public native ChordStart()Z
.end method

.method public synchronized native declared-synchronized ChordStop()Z
.end method

.method a(I)I
    .locals 1

    .prologue
    .line 607
    sparse-switch p1, :sswitch_data_0

    .line 622
    const/16 v0, 0x7d0

    :goto_0
    return v0

    .line 609
    :sswitch_0
    const/16 v0, 0x7d1

    goto :goto_0

    .line 611
    :sswitch_1
    const/16 v0, 0x7d2

    goto :goto_0

    .line 614
    :sswitch_2
    const/16 v0, 0x7d4

    goto :goto_0

    .line 616
    :sswitch_3
    const/16 v0, 0x7d3

    goto :goto_0

    .line 618
    :sswitch_4
    const/16 v0, 0x7d5

    goto :goto_0

    .line 620
    :sswitch_5
    const/16 v0, 0x7d6

    goto :goto_0

    .line 607
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_5
        0x138a -> :sswitch_2
        0x138b -> :sswitch_3
        0x138e -> :sswitch_4
        0x138f -> :sswitch_2
        0x1393 -> :sswitch_0
        0x1394 -> :sswitch_1
    .end sparse-switch
.end method

.method public native acceptFile(Ljava/lang/String;Ljava/lang/String;JIJ)I
.end method

.method b(I)I
    .locals 3

    .prologue
    .line 634
    packed-switch p1, :pswitch_data_0

    .line 645
    const-string v0, "chord_jni"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "**** convtNodeLeaveReason : invalid reason("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 646
    const/4 v0, -0x1

    :goto_0
    return v0

    .line 636
    :pswitch_0
    const/16 v0, 0x834

    goto :goto_0

    .line 638
    :pswitch_1
    const/16 v0, 0x835

    goto :goto_0

    .line 640
    :pswitch_2
    const/16 v0, 0x836

    goto :goto_0

    .line 634
    :pswitch_data_0
    .packed-switch 0x1b59
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public native cancelFile(Ljava/lang/String;Ljava/lang/String;)I
.end method

.method public native getChordAgentState()I
.end method

.method public native getDiscoveryIP()Ljava/lang/String;
.end method

.method public native getDiscoveryPortNumber()I
.end method

.method public native getNodeIpAddress(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
.end method

.method public native getUsingInterfaceName()Ljava/lang/String;
.end method

.method public native getVersion()Ljava/lang/String;
.end method

.method public native joinChannel(Ljava/lang/String;)I
.end method

.method public native leaveChannelEx(Ljava/lang/String;)I
.end method

.method public native multiacceptFiles(Ljava/lang/String;Ljava/lang/String;JIJ)I
.end method

.method public native multicancelFiles(Ljava/lang/String;Ljava/lang/String;)I
.end method

.method public native multirejectFiles(Ljava/lang/String;Ljava/lang/String;)I
.end method

.method public native multishareFiles(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;J)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation
.end method

.method onChordNoPeersCB()V
    .locals 2

    .prologue
    .line 248
    const-string v0, "chord_jni"

    const-string v1, "**** onChordNoPeersCB"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 249
    return-void
.end method

.method onChordPeersCB()V
    .locals 2

    .prologue
    .line 241
    const-string v0, "chord_jni"

    const-string v1, "**** onChordPeersCB"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 242
    return-void
.end method

.method onChunkReceivedCB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)V
    .locals 19

    .prologue
    .line 323
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    if-nez v2, :cond_0

    .line 336
    :goto_0
    return-void

    .line 325
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    const/16 v18, 0xce5

    new-instance v3, Lcom/samsung/chord/a/c;

    const-wide/16 v14, 0x0

    const/16 v16, -0x1

    const/16 v17, 0x0

    move-object/from16 v4, p2

    move-object/from16 v5, p1

    move-object/from16 v6, p3

    move-object/from16 v7, p4

    move-object/from16 v8, p5

    move-object/from16 v9, p6

    move-wide/from16 v10, p7

    move-wide/from16 v12, p9

    invoke-direct/range {v3 .. v17}, Lcom/samsung/chord/a/c;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJILjava/lang/String;)V

    move/from16 v0, v18

    invoke-virtual {v2, v0, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    .line 335
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    invoke-virtual {v3, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method onChunkSentCB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJ)V
    .locals 19

    .prologue
    .line 381
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    if-nez v2, :cond_0

    .line 395
    :goto_0
    return-void

    .line 383
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    const/16 v18, 0xce8

    new-instance v3, Lcom/samsung/chord/a/c;

    const/16 v16, -0x1

    const/16 v17, 0x0

    move-object/from16 v4, p2

    move-object/from16 v5, p1

    move-object/from16 v6, p3

    move-object/from16 v7, p4

    move-object/from16 v8, p5

    move-object/from16 v9, p6

    move-wide/from16 v10, p7

    move-wide/from16 v12, p9

    move-wide/from16 v14, p11

    invoke-direct/range {v3 .. v17}, Lcom/samsung/chord/a/c;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJILjava/lang/String;)V

    move/from16 v0, v18

    invoke-virtual {v2, v0, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    .line 394
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    invoke-virtual {v3, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method onCoreListeningCB(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 192
    const-string v0, "chord_jni"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "**** onCoreListeningCB : Migration "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/android/sdk/chord/ChordAgent;->getVersion()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 193
    const-string v0, "chord_jni"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "**** onCoreListeningCB : Name["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 194
    const-string v0, "chord_jni"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "**** onCoreListeningCB : IP["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/android/sdk/chord/ChordAgent;->getDiscoveryIP()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/android/sdk/chord/ChordAgent;->getDiscoveryPortNumber()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 202
    :goto_0
    return-void

    .line 200
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    const/16 v1, 0xbb8

    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 201
    iget-object v1, p0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method onCoreStoppedCB()V
    .locals 3

    .prologue
    .line 255
    const-string v0, "chord_jni"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "**** onCoreStoppedCB : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/android/sdk/chord/ChordAgent;->getVersion()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 256
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 257
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    const/16 v1, 0xbb9

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 258
    :cond_0
    return-void
.end method

.method onDataReceivedCB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[[B)V
    .locals 3

    .prologue
    .line 293
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 299
    :goto_0
    return-void

    .line 296
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    const/16 v1, 0xc80

    new-instance v2, Lcom/samsung/chord/a/b;

    invoke-direct {v2, p2, p1, p3, p4}, Lcom/samsung/chord/a/b;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[[B)V

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 298
    iget-object v1, p0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method onDiscoveryFailedCB(Ljava/lang/String;Ljava/lang/String;II)V
    .locals 3

    .prologue
    .line 231
    const-string v0, "chord_jni"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "**** onDiscoveryFailedCB ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 235
    return-void
.end method

.method onFileFailedCB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 19

    .prologue
    .line 362
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    if-nez v2, :cond_0

    .line 375
    :goto_0
    return-void

    .line 364
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    const/16 v18, 0xcea

    new-instance v3, Lcom/samsung/chord/a/c;

    const/4 v8, 0x0

    const-wide/16 v10, 0x0

    const-wide/16 v12, 0x0

    const-wide/16 v14, 0x0

    move-object/from16 v0, p0

    move/from16 v1, p6

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chord/ChordAgent;->a(I)I

    move-result v16

    const/16 v17, 0x0

    move-object/from16 v4, p2

    move-object/from16 v5, p1

    move-object/from16 v6, p3

    move-object/from16 v7, p4

    move-object/from16 v9, p5

    invoke-direct/range {v3 .. v17}, Lcom/samsung/chord/a/c;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJILjava/lang/String;)V

    move/from16 v0, v18

    invoke-virtual {v2, v0, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    .line 374
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    invoke-virtual {v3, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method onFileNotifiedCB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)V
    .locals 19

    .prologue
    .line 305
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    if-nez v2, :cond_0

    .line 317
    :goto_0
    return-void

    .line 307
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    const/16 v18, 0xce4

    new-instance v3, Lcom/samsung/chord/a/c;

    const-wide/16 v12, 0x0

    const-wide/16 v14, 0x0

    const/16 v16, -0x1

    const/16 v17, 0x0

    move-object/from16 v4, p2

    move-object/from16 v5, p1

    move-object/from16 v6, p3

    move-object/from16 v7, p4

    move-object/from16 v8, p5

    move-object/from16 v9, p6

    move-wide/from16 v10, p7

    invoke-direct/range {v3 .. v17}, Lcom/samsung/chord/a/c;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJILjava/lang/String;)V

    move/from16 v0, v18

    invoke-virtual {v2, v0, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    .line 316
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    invoke-virtual {v3, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method onFileReceivedCB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)V
    .locals 19

    .prologue
    .line 342
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    if-nez v2, :cond_0

    .line 355
    :goto_0
    return-void

    .line 344
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    const/16 v18, 0xce6

    new-instance v3, Lcom/samsung/chord/a/c;

    const-wide/16 v12, 0x0

    const-wide/16 v14, 0x0

    const/16 v16, -0x1

    move-object/from16 v4, p2

    move-object/from16 v5, p1

    move-object/from16 v6, p3

    move-object/from16 v7, p4

    move-object/from16 v8, p5

    move-object/from16 v9, p6

    move-wide/from16 v10, p7

    move-object/from16 v17, p9

    invoke-direct/range {v3 .. v17}, Lcom/samsung/chord/a/c;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJILjava/lang/String;)V

    move/from16 v0, v18

    invoke-virtual {v2, v0, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    .line 354
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    invoke-virtual {v3, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method onFileSentCB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 19

    .prologue
    .line 401
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    if-nez v2, :cond_0

    .line 413
    :goto_0
    return-void

    .line 403
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    const/16 v18, 0xce9

    new-instance v3, Lcom/samsung/chord/a/c;

    const-wide/16 v12, 0x0

    const-wide/16 v14, 0x0

    const/16 v16, -0x1

    const/16 v17, 0x0

    move-object/from16 v4, p2

    move-object/from16 v5, p1

    move-object/from16 v6, p3

    move-object/from16 v7, p4

    move-object/from16 v8, p5

    move-object/from16 v9, p6

    move-wide/from16 v10, p7

    invoke-direct/range {v3 .. v17}, Lcom/samsung/chord/a/c;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJILjava/lang/String;)V

    move/from16 v0, v18

    invoke-virtual {v2, v0, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    .line 412
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    invoke-virtual {v3, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method onJoinedEventCB(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 263
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 269
    :goto_0
    return-void

    .line 266
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    const/16 v1, 0xc1d

    new-instance v2, Lcom/samsung/chord/a/a;

    invoke-direct {v2, p2, p1}, Lcom/samsung/chord/a/a;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 268
    iget-object v1, p0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method onLeaveEventCB(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 4

    .prologue
    .line 281
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 287
    :goto_0
    return-void

    .line 284
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    const/16 v1, 0xc1e

    new-instance v2, Lcom/samsung/chord/a/a;

    invoke-virtual {p0, p3}, Lcom/samsung/android/sdk/chord/ChordAgent;->b(I)I

    move-result v3

    invoke-direct {v2, p2, p1, v3}, Lcom/samsung/chord/a/a;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 286
    iget-object v1, p0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method onMultiChunkReceivedCB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJJ)V
    .locals 21

    .prologue
    .line 440
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    if-nez v2, :cond_0

    .line 454
    :goto_0
    return-void

    .line 442
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    const/16 v20, 0xcf9

    new-instance v3, Lcom/samsung/chord/a/c;

    const/4 v7, 0x0

    const/4 v9, 0x0

    const-wide/16 v14, 0x0

    const/16 v16, -0x1

    const/16 v17, 0x0

    move-object/from16 v4, p2

    move-object/from16 v5, p1

    move-object/from16 v6, p3

    move-object/from16 v8, p5

    move-wide/from16 v10, p7

    move-wide/from16 v12, p9

    move-object/from16 v18, p4

    move/from16 v19, p6

    invoke-direct/range {v3 .. v19}, Lcom/samsung/chord/a/c;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJILjava/lang/String;Ljava/lang/String;I)V

    move/from16 v0, v20

    invoke-virtual {v2, v0, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    .line 453
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    invoke-virtual {v3, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method onMultiChunkSentCB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJJJ)V
    .locals 21

    .prologue
    .line 506
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    if-nez v2, :cond_0

    .line 521
    :goto_0
    return-void

    .line 508
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    const/16 v20, 0xcfc

    new-instance v3, Lcom/samsung/chord/a/c;

    const/4 v7, 0x0

    const/4 v9, 0x0

    const/16 v16, -0x1

    const/16 v17, 0x0

    move-object/from16 v4, p2

    move-object/from16 v5, p1

    move-object/from16 v6, p3

    move-object/from16 v8, p5

    move-wide/from16 v10, p7

    move-wide/from16 v12, p9

    move-wide/from16 v14, p11

    move-object/from16 v18, p4

    move/from16 v19, p6

    invoke-direct/range {v3 .. v19}, Lcom/samsung/chord/a/c;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJILjava/lang/String;Ljava/lang/String;I)V

    move/from16 v0, v20

    invoke-virtual {v2, v0, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    .line 520
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    invoke-virtual {v3, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method onMultiFileFailedCB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 21

    .prologue
    .line 484
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    if-nez v2, :cond_0

    .line 498
    :goto_0
    return-void

    .line 486
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    const/16 v20, 0xcfe

    new-instance v3, Lcom/samsung/chord/a/c;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const-wide/16 v10, 0x0

    const-wide/16 v12, 0x0

    const-wide/16 v14, 0x0

    move-object/from16 v0, p0

    move/from16 v1, p6

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chord/ChordAgent;->a(I)I

    move-result v16

    const/16 v17, 0x0

    move-object/from16 v4, p2

    move-object/from16 v5, p1

    move-object/from16 v6, p3

    move-object/from16 v18, p4

    move/from16 v19, p5

    invoke-direct/range {v3 .. v19}, Lcom/samsung/chord/a/c;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJILjava/lang/String;Ljava/lang/String;I)V

    move/from16 v0, v20

    invoke-virtual {v2, v0, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    .line 497
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    invoke-virtual {v3, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method onMultiFileFinishedCB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 21

    .prologue
    .line 550
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    if-nez v2, :cond_0

    .line 564
    :goto_0
    return-void

    .line 552
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    const/16 v20, 0xcff

    new-instance v3, Lcom/samsung/chord/a/c;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const-wide/16 v10, 0x0

    const-wide/16 v12, 0x0

    const-wide/16 v14, 0x0

    move-object/from16 v0, p0

    move/from16 v1, p4

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chord/ChordAgent;->a(I)I

    move-result v16

    const/16 v17, 0x0

    const/16 v19, -0x1

    move-object/from16 v4, p2

    move-object/from16 v5, p1

    move-object/from16 v18, p3

    invoke-direct/range {v3 .. v19}, Lcom/samsung/chord/a/c;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJILjava/lang/String;Ljava/lang/String;I)V

    move/from16 v0, v20

    invoke-virtual {v2, v0, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    .line 563
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    invoke-virtual {v3, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method onMultiFileNotifiedCB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJLjava/lang/String;)V
    .locals 21

    .prologue
    .line 420
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    if-nez v2, :cond_0

    .line 433
    :goto_0
    return-void

    .line 422
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    const/16 v20, 0xcf8

    new-instance v3, Lcom/samsung/chord/a/c;

    const/4 v7, 0x0

    const/4 v9, 0x0

    const-wide/16 v12, 0x0

    const-wide/16 v14, 0x0

    const/16 v16, -0x1

    const/16 v17, 0x0

    move-object/from16 v4, p2

    move-object/from16 v5, p1

    move-object/from16 v6, p3

    move-object/from16 v8, p5

    move-wide/from16 v10, p7

    move-object/from16 v18, p4

    move/from16 v19, p6

    invoke-direct/range {v3 .. v19}, Lcom/samsung/chord/a/c;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJILjava/lang/String;Ljava/lang/String;I)V

    move/from16 v0, v20

    invoke-virtual {v2, v0, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    .line 432
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    invoke-virtual {v3, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method onMultiFileReceivedCB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJLjava/lang/String;)V
    .locals 21

    .prologue
    .line 461
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    if-nez v2, :cond_0

    .line 475
    :goto_0
    return-void

    .line 463
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    const/16 v20, 0xcfa

    new-instance v3, Lcom/samsung/chord/a/c;

    const/4 v7, 0x0

    const/4 v9, 0x0

    const-wide/16 v12, 0x0

    const-wide/16 v14, 0x0

    const/16 v16, -0x1

    move-object/from16 v4, p2

    move-object/from16 v5, p1

    move-object/from16 v6, p3

    move-object/from16 v8, p5

    move-wide/from16 v10, p7

    move-object/from16 v17, p9

    move-object/from16 v18, p4

    move/from16 v19, p6

    invoke-direct/range {v3 .. v19}, Lcom/samsung/chord/a/c;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJILjava/lang/String;Ljava/lang/String;I)V

    move/from16 v0, v20

    invoke-virtual {v2, v0, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    .line 474
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    invoke-virtual {v3, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method onMultiFileSentCB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJ)V
    .locals 21

    .prologue
    .line 529
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    if-nez v2, :cond_0

    .line 542
    :goto_0
    return-void

    .line 531
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    const/16 v20, 0xcfd

    new-instance v3, Lcom/samsung/chord/a/c;

    const/4 v7, 0x0

    const/4 v9, 0x0

    const-wide/16 v12, 0x0

    const-wide/16 v14, 0x0

    const/16 v16, -0x1

    const/16 v17, 0x0

    move-object/from16 v4, p2

    move-object/from16 v5, p1

    move-object/from16 v6, p3

    move-object/from16 v8, p5

    move-wide/from16 v10, p7

    move-object/from16 v18, p4

    move/from16 v19, p6

    invoke-direct/range {v3 .. v19}, Lcom/samsung/chord/a/c;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJILjava/lang/String;Ljava/lang/String;I)V

    move/from16 v0, v20

    invoke-virtual {v2, v0, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    .line 541
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    invoke-virtual {v3, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public native onNodeJoinedSender(Ljava/lang/String;)V
.end method

.method onNotifyChannelErrorCB(Ljava/lang/String;I)V
    .locals 3

    .prologue
    .line 567
    const-string v0, "chord_jni"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "**** onNotifyChannelErrorCB : channel("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") reason("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 581
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 582
    const/16 v1, 0xc1c

    iput v1, v0, Landroid/os/Message;->what:I

    .line 583
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 585
    iget-object v1, p0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 586
    iget-object v1, p0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 588
    :cond_0
    return-void
.end method

.method onNotifyServiceErrorCB(I)V
    .locals 3

    .prologue
    .line 205
    const-string v0, "chord_jni"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "**** onNotifyServiceErrorCB ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 221
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    if-eqz v0, :cond_1

    .line 222
    const/16 v0, 0x3e9

    if-eq p1, v0, :cond_0

    const/16 v0, 0x3ea

    if-eq p1, v0, :cond_0

    const/16 v0, 0x3eb

    if-eq p1, v0, :cond_0

    const/16 v0, 0x3ec

    if-ne p1, v0, :cond_1

    .line 226
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    const/16 v1, 0xbbc

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 228
    :cond_1
    return-void
.end method

.method public native rejectFile(Ljava/lang/String;Ljava/lang/String;)I
.end method

.method public native resetSmartDiscoveryPeriod()V
.end method

.method public native sendData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[[B)I
.end method

.method public native setConnectivityState(I)V
.end method

.method public setHandler(Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 38
    iput-object p1, p0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    .line 39
    return-void
.end method

.method public native setLivenessTimeout(J)V
.end method

.method public native setNodeExpiry(Z)V
.end method

.method public native setSecureMode(Z)Z
.end method

.method public native setSendFileLimit(I)Z
.end method

.method public native setSmartDiscovery(Z)V
.end method

.method public native setUdpDiscover(Z)V
.end method

.method public native setUsingInterface(Ljava/lang/String;)V
.end method

.method public native shareFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation
.end method
