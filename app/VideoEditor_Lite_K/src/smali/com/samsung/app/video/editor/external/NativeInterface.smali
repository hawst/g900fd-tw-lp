.class public Lcom/samsung/app/video/editor/external/NativeInterface;
.super Ljava/lang/Object;
.source "NativeInterface.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/app/video/editor/external/NativeInterface$playerState;,
        Lcom/samsung/app/video/editor/external/NativeInterface$previewPlayerStateListener;
    }
.end annotation


# static fields
.field public static final THUMBINIT_ERROR:I = -0x1

.field private static final THUMBINIT_NULL:I = 0x0

.field private static final VT_PREVIEW_PLAYER_AUDIO_DECODE_FAIL:I = 0x9

.field private static final VT_PREVIEW_PLAYER_CLOSED:I = 0x4

.field private static final VT_PREVIEW_PLAYER_CREATED:I = 0x0

.field private static final VT_PREVIEW_PLAYER_DAM_CONFIGURE:I = 0xb

.field private static final VT_PREVIEW_PLAYER_DAM_DISPLAY:I = 0xc

.field private static final VT_PREVIEW_PLAYER_DAM_UNREGISTER:I = 0xd

.field private static final VT_PREVIEW_PLAYER_FILE_OPEN_FAIL:I = 0xa

.field private static final VT_PREVIEW_PLAYER_PAUSED:I = 0x5

.field private static final VT_PREVIEW_PLAYER_PLAYBACK_COMPLETE:I = 0x2

.field private static final VT_PREVIEW_PLAYER_PLAYING:I = 0x1

.field private static final VT_PREVIEW_PLAYER_RESUMED:I = 0x6

.field private static final VT_PREVIEW_PLAYER_STOPED:I = 0x3

.field private static final VT_PREVIEW_PLAYER_STOPED_ON_ERROR:I = 0x7

.field private static final VT_PREVIEW_PLAYER_VIDEO_DECODE_FAIL:I = 0x8

.field public static volatile isInitFIMC:Z

.field private static volatile mGPUCreated:Z

.field private static mStackStateListener:Lcom/samsung/app/video/editor/external/NativeInterface$previewPlayerStateListener;

.field private static final nativeInterface:Lcom/samsung/app/video/editor/external/NativeInterface;

.field private static volatile playExportHandle:I

.field private static volatile thumbHandle:I


# instance fields
.field private volatile mAppEntryPointRefCount:I

.field private mSurface:Landroid/view/Surface;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 19
    new-instance v0, Lcom/samsung/app/video/editor/external/NativeInterface;

    invoke-direct {v0}, Lcom/samsung/app/video/editor/external/NativeInterface;-><init>()V

    sput-object v0, Lcom/samsung/app/video/editor/external/NativeInterface;->nativeInterface:Lcom/samsung/app/video/editor/external/NativeInterface;

    .line 24
    sput-boolean v1, Lcom/samsung/app/video/editor/external/NativeInterface;->mGPUCreated:Z

    .line 26
    const/4 v0, -0x1

    sput v0, Lcom/samsung/app/video/editor/external/NativeInterface;->thumbHandle:I

    .line 27
    sput v1, Lcom/samsung/app/video/editor/external/NativeInterface;->playExportHandle:I

    .line 133
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 137
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/app/video/editor/external/NativeInterface;->mAppEntryPointRefCount:I

    .line 139
    sget-object v0, Lcom/sec/android/app/ve/VEApp;->gAdaper:Lcom/sec/android/app/ve/VEAdapter;

    if-eqz v0, :cond_0

    .line 140
    sget-object v0, Lcom/sec/android/app/ve/VEApp;->gAdaper:Lcom/sec/android/app/ve/VEAdapter;

    invoke-interface {v0}, Lcom/sec/android/app/ve/VEAdapter;->loadNativeLibraries()V

    .line 161
    :cond_0
    return-void
.end method

.method private synchronized native declared-synchronized CreateGPUEngine()I
.end method

.method private synchronized native declared-synchronized CreateGPUEngine(I)I
.end method

.method private synchronized native declared-synchronized FindIfJPEG(Ljava/lang/String;)I
.end method

.method private synchronized native declared-synchronized GetCodecFileProperties(Ljava/lang/String;)I
.end method

.method private synchronized native declared-synchronized GetEstimatedTime()I
.end method

.method private synchronized native declared-synchronized Height()I
.end method

.method private synchronized native declared-synchronized ImageHeight()I
.end method

.method private synchronized native declared-synchronized ImageWidth()I
.end method

.method private synchronized native declared-synchronized JpegtoRGB565(Ljava/lang/String;II)[B
.end method

.method private synchronized native declared-synchronized NewVideoEditorLaunch()I
.end method

.method private synchronized native declared-synchronized PauseVEEditExportFunction()V
.end method

.method private synchronized native declared-synchronized PauseVeEngine(I)I
.end method

.method private synchronized native declared-synchronized PinchDeInit(Ljava/lang/String;)V
.end method

.method private synchronized native declared-synchronized PinchInit(Ljava/lang/String;)I
.end method

.method private synchronized native declared-synchronized PinchVEGetFrameRGB565Buffer(III)[B
.end method

.method private synchronized native declared-synchronized PlayerDeinitImage(ILjava/lang/String;)V
.end method

.method private synchronized native declared-synchronized PlayerDeinitImage(Ljava/lang/String;)V
.end method

.method private synchronized native declared-synchronized PlayerInitImage(ILjava/lang/String;)V
.end method

.method private synchronized native declared-synchronized PlayerInitImage(Ljava/lang/String;)V
.end method

.method private synchronized native declared-synchronized ReadJpegHeader(Ljava/lang/String;)I
.end method

.method private synchronized native declared-synchronized ResumeVEEditExportFunction()V
.end method

.method private synchronized native declared-synchronized ResumeVeEngine(II)V
.end method

.method private synchronized native declared-synchronized StopVEEditExportFunction()V
.end method

.method private native VEEdit(Lcom/samsung/app/video/editor/external/TranscodeElement;Ljava/lang/String;Z)I
.end method

.method private synchronized native declared-synchronized VEGetFrameRGB565Buffer(Ljava/lang/String;III)[B
.end method

.method private synchronized native declared-synchronized VEGetKenburnsFrame(Ljava/lang/String;Lcom/samsung/app/video/editor/external/Element;Lcom/samsung/app/video/editor/external/Element;IIII)[B
.end method

.method private synchronized native declared-synchronized VideoEditorLaunch()V
.end method

.method private synchronized native declared-synchronized VideoEditorTerminate()V
.end method

.method private synchronized native declared-synchronized VideoEditorTerminate(I)V
.end method

.method private synchronized native declared-synchronized Width()I
.end method

.method private declared-synchronized _FindIfJPEG(Ljava/lang/String;)I
    .locals 3
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 942
    monitor-enter p0

    :try_start_0
    const-string v1, "LIBRARY"

    const-string v2, " <<<<<<<<<<<< FindIfJPEG >>>>>>>>>>  "

    invoke-static {v1, v2}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 943
    invoke-direct {p0, p1}, Lcom/samsung/app/video/editor/external/NativeInterface;->FindIfJPEG(Ljava/lang/String;)I

    move-result v0

    .line 944
    .local v0, "res":I
    const-string v1, "LIBRARY"

    const-string v2, " <<<<<<<<<<<< AFTER FindIfJPEG >>>>>>>>>>  "

    invoke-static {v1, v2}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 946
    monitor-exit p0

    return v0

    .line 942
    .end local v0    # "res":I
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private declared-synchronized _createGPUEngine()I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 778
    monitor-enter p0

    const/4 v0, 0x0

    .line 779
    .local v0, "ret":I
    :try_start_0
    sget-boolean v1, Lcom/samsung/app/video/editor/external/NativeInterface;->isInitFIMC:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_1

    .line 793
    :cond_0
    :goto_0
    monitor-exit p0

    return v3

    .line 781
    :cond_1
    :try_start_1
    sget-boolean v1, Lcom/samsung/app/video/editor/external/NativeInterface;->mGPUCreated:Z

    if-nez v1, :cond_0

    .line 783
    const/4 v1, 0x1

    sput-boolean v1, Lcom/samsung/app/video/editor/external/NativeInterface;->mGPUCreated:Z

    .line 784
    const-string v1, "LIBRARY"

    const-string v2, " <<<<<<<<<<<< CreateGPUEngine >>>>>>>>>>  "

    invoke-static {v1, v2}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 785
    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getJNIVersion()F

    move-result v1

    const/high16 v2, 0x40000000    # 2.0f

    cmpl-float v1, v1, v2

    if-nez v1, :cond_2

    .line 786
    sget v1, Lcom/samsung/app/video/editor/external/NativeInterface;->playExportHandle:I

    invoke-direct {p0, v1}, Lcom/samsung/app/video/editor/external/NativeInterface;->CreateGPUEngine(I)I

    move-result v0

    .line 791
    :goto_1
    const-string v1, "LIBRARY"

    const-string v2, " <<<<<<<<<<<< AFTER CreateGPUEngine >>>>>>>>>>  "

    invoke-static {v1, v2}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 778
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 789
    :cond_2
    :try_start_2
    invoke-direct {p0}, Lcom/samsung/app/video/editor/external/NativeInterface;->CreateGPUEngine()I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method private final declared-synchronized _native_setup()V
    .locals 2

    .prologue
    .line 966
    monitor-enter p0

    :try_start_0
    const-string v0, "LIBRARY"

    const-string v1, " <<<<<<<<<<<< native_setup >>>>>>>>>>  "

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 967
    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getJNIVersion()F

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 968
    sget v0, Lcom/samsung/app/video/editor/external/NativeInterface;->playExportHandle:I

    invoke-direct {p0, v0}, Lcom/samsung/app/video/editor/external/NativeInterface;->native_setup(I)V

    .line 973
    :goto_0
    const-string v0, "LIBRARY"

    const-string v1, " <<<<<<<<<<<< AFTER native_setup >>>>>>>>>>  "

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 974
    monitor-exit p0

    return-void

    .line 971
    :cond_0
    :try_start_1
    invoke-direct {p0}, Lcom/samsung/app/video/editor/external/NativeInterface;->native_setup()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 966
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private synchronized native declared-synchronized applyEffectToVideoThumbnail([BIIII)[B
.end method

.method private native createInstanceSummary(Lcom/samsung/app/video/editor/external/TranscodeElement;II)I
.end method

.method private synchronized native declared-synchronized createPreviewPlayer()V
.end method

.method private synchronized native declared-synchronized createPreviewPlayer(Lcom/samsung/app/video/editor/external/TranscodeElement;)V
.end method

.method private native createSummary(Lcom/samsung/app/video/editor/external/TranscodeElement;Lcom/samsung/app/video/editor/external/TranscodeElement;DI)I
.end method

.method private native createSummary(Lcom/samsung/app/video/editor/external/TranscodeElement;Lcom/samsung/app/video/editor/external/TranscodeElement;DII)I
.end method

.method private synchronized native declared-synchronized currentBinaryType(I)V
.end method

.method private synchronized native declared-synchronized currentBinaryType(II)V
.end method

.method private synchronized native declared-synchronized deleteSummaryInstance()I
.end method

.method private synchronized native declared-synchronized deleteSummaryInstance(I)I
.end method

.method private synchronized native declared-synchronized denit()V
.end method

.method private synchronized native declared-synchronized freeSummaryOutput(I)I
.end method

.method private native getClipArtParamsAtParamsList(Lcom/samsung/app/video/editor/external/ClipartParams;I)I
.end method

.method private native getCurrentPositionOfPreviewPlayer()I
.end method

.method private native getCurrentPositionOfVeEngine(I)I
.end method

.method private native getEditInEditListAt(Lcom/samsung/app/video/editor/external/Edit;II)I
.end method

.method private synchronized native declared-synchronized getEstimatedSize(Lcom/samsung/app/video/editor/external/TranscodeElement;II)J
.end method

.method public static getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;
    .locals 1

    .prologue
    .line 111
    sget-object v0, Lcom/samsung/app/video/editor/external/NativeInterface;->nativeInterface:Lcom/samsung/app/video/editor/external/NativeInterface;

    return-object v0
.end method

.method private static getMessageFromEngine(I)Lcom/samsung/app/video/editor/external/NativeInterface$playerState;
    .locals 1
    .param p0, "msg"    # I

    .prologue
    .line 44
    packed-switch p0, :pswitch_data_0

    .line 86
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 46
    :pswitch_1
    sget-object v0, Lcom/samsung/app/video/editor/external/NativeInterface$playerState;->VT_PREVIEW_PLAYER_CREATED:Lcom/samsung/app/video/editor/external/NativeInterface$playerState;

    goto :goto_0

    .line 49
    :pswitch_2
    sget-object v0, Lcom/samsung/app/video/editor/external/NativeInterface$playerState;->VT_PREVIEW_PLAYER_PLAYING:Lcom/samsung/app/video/editor/external/NativeInterface$playerState;

    goto :goto_0

    .line 52
    :pswitch_3
    sget-object v0, Lcom/samsung/app/video/editor/external/NativeInterface$playerState;->VT_PREVIEW_PLAYER_PLAYBACK_COMPLETE:Lcom/samsung/app/video/editor/external/NativeInterface$playerState;

    goto :goto_0

    .line 55
    :pswitch_4
    sget-object v0, Lcom/samsung/app/video/editor/external/NativeInterface$playerState;->VT_PREVIEW_PLAYER_STOPED:Lcom/samsung/app/video/editor/external/NativeInterface$playerState;

    goto :goto_0

    .line 58
    :pswitch_5
    sget-object v0, Lcom/samsung/app/video/editor/external/NativeInterface$playerState;->VT_PREVIEW_PLAYER_CLOSED:Lcom/samsung/app/video/editor/external/NativeInterface$playerState;

    goto :goto_0

    .line 61
    :pswitch_6
    sget-object v0, Lcom/samsung/app/video/editor/external/NativeInterface$playerState;->VT_PREVIEW_PLAYER_PAUSED:Lcom/samsung/app/video/editor/external/NativeInterface$playerState;

    goto :goto_0

    .line 64
    :pswitch_7
    sget-object v0, Lcom/samsung/app/video/editor/external/NativeInterface$playerState;->VT_PREVIEW_PLAYER_RESUMED:Lcom/samsung/app/video/editor/external/NativeInterface$playerState;

    goto :goto_0

    .line 67
    :pswitch_8
    sget-object v0, Lcom/samsung/app/video/editor/external/NativeInterface$playerState;->VT_PREVIEW_PLAYER_STOPED_ON_ERROR:Lcom/samsung/app/video/editor/external/NativeInterface$playerState;

    goto :goto_0

    .line 70
    :pswitch_9
    sget-object v0, Lcom/samsung/app/video/editor/external/NativeInterface$playerState;->VT_PREVIEW_PLAYER_VIDEO_DECODE_FAIL:Lcom/samsung/app/video/editor/external/NativeInterface$playerState;

    goto :goto_0

    .line 73
    :pswitch_a
    sget-object v0, Lcom/samsung/app/video/editor/external/NativeInterface$playerState;->VT_PREVIEW_PLAYER_AUDIO_DECODE_FAIL:Lcom/samsung/app/video/editor/external/NativeInterface$playerState;

    goto :goto_0

    .line 76
    :pswitch_b
    sget-object v0, Lcom/samsung/app/video/editor/external/NativeInterface$playerState;->VT_PREVIEW_PLAYER_FILE_OPEN_FAIL:Lcom/samsung/app/video/editor/external/NativeInterface$playerState;

    goto :goto_0

    .line 79
    :pswitch_c
    sget-object v0, Lcom/samsung/app/video/editor/external/NativeInterface$playerState;->VT_PREVIEW_PLAYER_DAM_CONFIGURE:Lcom/samsung/app/video/editor/external/NativeInterface$playerState;

    goto :goto_0

    .line 82
    :pswitch_d
    sget-object v0, Lcom/samsung/app/video/editor/external/NativeInterface$playerState;->VT_PREVIEW_PLAYER_DAM_UNREGISTER:Lcom/samsung/app/video/editor/external/NativeInterface$playerState;

    goto :goto_0

    .line 44
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_0
        :pswitch_d
    .end packed-switch
.end method

.method private synchronized native declared-synchronized getMetadataOfPreviewPlayer()I
.end method

.method private synchronized native declared-synchronized getOverlapCondition(Lcom/samsung/app/video/editor/external/Element;Lcom/samsung/app/video/editor/external/Element;)Z
.end method

.method private synchronized native declared-synchronized getProjectPreviewImage(Landroid/content/res/AssetManager;Lcom/samsung/app/video/editor/external/Element;IIIIIII)[B
.end method

.method private native getSummaryElementInElementListAt(Lcom/samsung/app/video/editor/external/Element;II)I
.end method

.method private synchronized native declared-synchronized getSummaryInstance()I
.end method

.method private synchronized native declared-synchronized getThumbnail(Lcom/samsung/app/video/editor/external/Element;Lcom/samsung/app/video/editor/external/Element;IIIIILjava/util/List;F)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/app/video/editor/external/Element;",
            "Lcom/samsung/app/video/editor/external/Element;",
            "IIIII",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/ClipartParams;",
            ">;F)V"
        }
    .end annotation
.end method

.method private synchronized native declared-synchronized getThumbnail(Lcom/samsung/app/video/editor/external/Element;Lcom/samsung/app/video/editor/external/Element;IIIIILjava/util/List;Ljava/util/List;F)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/app/video/editor/external/Element;",
            "Lcom/samsung/app/video/editor/external/Element;",
            "IIIII",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/ClipartParams;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/ClipartParams;",
            ">;F)V"
        }
    .end annotation
.end method

.method private synchronized native declared-synchronized getVideoType()I
.end method

.method private final synchronized native declared-synchronized native_initfimc()V
.end method

.method private final synchronized native declared-synchronized native_initfimc(I)V
.end method

.method private final synchronized native declared-synchronized native_setup()V
.end method

.method private final synchronized native declared-synchronized native_setup(I)V
.end method

.method private final synchronized native declared-synchronized native_terminate()V
.end method

.method private final synchronized native declared-synchronized native_terminate(I)V
.end method

.method private synchronized native declared-synchronized pausePreviewPlayer()V
.end method

.method private synchronized native declared-synchronized pauseVeEngine(I)V
.end method

.method private final synchronized native declared-synchronized playbackCompleted()V
.end method

.method private static declared-synchronized postEventForExport(Ljava/lang/Object;F)Ljava/nio/ByteBuffer;
    .locals 2
    .param p0, "obj"    # Ljava/lang/Object;
    .param p1, "f"    # F

    .prologue
    .line 130
    const-class v0, Lcom/samsung/app/video/editor/external/NativeInterface;

    monitor-enter v0

    const/4 v1, 0x0

    monitor-exit v0

    return-object v1
.end method

.method private static declared-synchronized postEventFromNative(Ljava/lang/Object;IIILjava/lang/Object;)V
    .locals 7
    .param p0, "vt_ref"    # Ljava/lang/Object;
    .param p1, "what"    # I
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I
    .param p4, "obj"    # Ljava/lang/Object;

    .prologue
    .line 116
    const-class v4, Lcom/samsung/app/video/editor/external/NativeInterface;

    monitor-enter v4

    :try_start_0
    move-object v0, p0

    check-cast v0, Lcom/samsung/app/video/editor/external/NativeInterface;

    move-object v1, v0

    .line 118
    .local v1, "c":Lcom/samsung/app/video/editor/external/NativeInterface;
    if-nez v1, :cond_1

    .line 119
    const-string v3, "LIBRARY"

    const-string v5, "Object reference is NULL"

    invoke-static {v3, v5}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 127
    :cond_0
    :goto_0
    monitor-exit v4

    return-void

    .line 123
    :cond_1
    :try_start_1
    const-string v3, "LIBRARY"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "<<<<<<<<<<<<<<<<<< Event from Native : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    invoke-static {p1}, Lcom/samsung/app/video/editor/external/NativeInterface;->getMessageFromEngine(I)Lcom/samsung/app/video/editor/external/NativeInterface$playerState;

    move-result-object v2

    .line 125
    .local v2, "state":Lcom/samsung/app/video/editor/external/NativeInterface$playerState;
    sget-object v3, Lcom/samsung/app/video/editor/external/NativeInterface;->mStackStateListener:Lcom/samsung/app/video/editor/external/NativeInterface$previewPlayerStateListener;

    if-eqz v3, :cond_0

    if-eqz v2, :cond_0

    .line 126
    sget-object v3, Lcom/samsung/app/video/editor/external/NativeInterface;->mStackStateListener:Lcom/samsung/app/video/editor/external/NativeInterface$previewPlayerStateListener;

    invoke-interface {v3, v2}, Lcom/samsung/app/video/editor/external/NativeInterface$previewPlayerStateListener;->onStateChanged(Lcom/samsung/app/video/editor/external/NativeInterface$playerState;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 116
    .end local v1    # "c":Lcom/samsung/app/video/editor/external/NativeInterface;
    .end local v2    # "state":Lcom/samsung/app/video/editor/external/NativeInterface$playerState;
    :catchall_0
    move-exception v3

    monitor-exit v4

    throw v3
.end method

.method private synchronized native declared-synchronized previewFrame(Lcom/samsung/app/video/editor/external/Element;Lcom/samsung/app/video/editor/external/Element;III)V
.end method

.method private synchronized native declared-synchronized release()I
.end method

.method private final declared-synchronized resume()V
    .locals 2

    .prologue
    .line 366
    monitor-enter p0

    :try_start_0
    const-string v0, "LIBRARY"

    const-string v1, " <<<<<<<<<<<< resumePreviewPlayer >>>>>>>>>>  "

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 367
    invoke-direct {p0}, Lcom/samsung/app/video/editor/external/NativeInterface;->resumePreviewPlayer()V

    .line 368
    const-string v0, "LIBRARY"

    const-string v1, " <<<<<<<<<<<< AFTER resumePreviewPlayer >>>>>>>>>>  "

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 369
    monitor-exit p0

    return-void

    .line 366
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private synchronized native declared-synchronized resumePreviewPlayer()V
.end method

.method private synchronized native declared-synchronized resumeVeEngine(I)V
.end method

.method private synchronized native declared-synchronized seekPreviewPlayer(I)V
.end method

.method private synchronized native declared-synchronized setAR(II)V
.end method

.method private synchronized native declared-synchronized setDisplayForPreviewPlayer(Lcom/samsung/app/video/editor/external/TranscodeElement;)V
.end method

.method private final synchronized native declared-synchronized setPreviewDisplay(ILandroid/view/Surface;)V
.end method

.method private final synchronized native declared-synchronized setPreviewDisplay(Landroid/view/Surface;)V
.end method

.method private synchronized native declared-synchronized startPreviewPlayer(Lcom/samsung/app/video/editor/external/TranscodeElement;I)I
.end method

.method private final synchronized native declared-synchronized startPreviewPlayer()V
.end method

.method private synchronized native declared-synchronized startVeEngine(ILcom/samsung/app/video/editor/external/TranscodeElement;ILcom/samsung/app/video/editor/external/TranscodeElement$ExportParameters;)I
.end method

.method private synchronized native declared-synchronized stopPreviewPlayer()V
.end method

.method private synchronized native declared-synchronized stopSummary()V
.end method

.method private synchronized native declared-synchronized stopSummary(I)V
.end method

.method private synchronized native declared-synchronized stopVeEngine(I)V
.end method

.method private synchronized native declared-synchronized surfaceDestroyed()V
.end method

.method private synchronized native declared-synchronized surfaceDestroyed(I)V
.end method

.method private synchronized native declared-synchronized terminatePreviewPlayer()V
.end method

.method private synchronized native declared-synchronized thumbnailDeinit(I)I
.end method

.method private synchronized native declared-synchronized thumbnailInit(Landroid/content/res/AssetManager;Ljava/lang/String;)I
.end method

.method private synchronized native declared-synchronized updatePackagePath(Ljava/lang/String;)V
.end method

.method private synchronized native declared-synchronized updatePreviewDisplay(Lcom/samsung/app/video/editor/external/Element;I)V
.end method

.method private synchronized native declared-synchronized ve_appexitfromJNI()V
.end method

.method private synchronized native declared-synchronized ve_appexitfromJNI(I)V
.end method

.method private synchronized native declared-synchronized waveformDeinit(I)I
.end method

.method private synchronized native declared-synchronized waveformInit(Ljava/lang/String;)I
.end method

.method private synchronized native declared-synchronized waveformProcess(II)I
.end method

.method private synchronized native declared-synchronized waveformSampleCount(I)I
.end method

.method private synchronized native declared-synchronized waveformSetFrequency(II)I
.end method


# virtual methods
.method public final declared-synchronized EditActivityExit()V
    .locals 2

    .prologue
    .line 608
    monitor-enter p0

    :try_start_0
    const-string v0, "LIBRARY"

    const-string v1, " <<<<<<<<<<<< ve_appexitfromJNI >>>>>>>>>>  "

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 609
    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getJNIVersion()F

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 610
    sget v0, Lcom/samsung/app/video/editor/external/NativeInterface;->playExportHandle:I

    invoke-direct {p0, v0}, Lcom/samsung/app/video/editor/external/NativeInterface;->ve_appexitfromJNI(I)V

    .line 615
    :goto_0
    const-string v0, "LIBRARY"

    const-string v1, " <<<<<<<<<<<< AFTER ve_appexitfromJNI >>>>>>>>>>  "

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 616
    monitor-exit p0

    return-void

    .line 613
    :cond_0
    :try_start_1
    invoke-direct {p0}, Lcom/samsung/app/video/editor/external/NativeInterface;->ve_appexitfromJNI()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 608
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized EngineDeinitImage(Ljava/lang/String;)V
    .locals 2
    .param p1, "inputFilePath"    # Ljava/lang/String;

    .prologue
    .line 452
    monitor-enter p0

    :try_start_0
    const-string v0, "LIBRARY"

    const-string v1, " <<<<<<<<<<<< PlayerDeinitImage >>>>>>>>>>  "

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 453
    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getJNIVersion()F

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 454
    sget v0, Lcom/samsung/app/video/editor/external/NativeInterface;->playExportHandle:I

    invoke-direct {p0, v0, p1}, Lcom/samsung/app/video/editor/external/NativeInterface;->PlayerDeinitImage(ILjava/lang/String;)V

    .line 459
    :goto_0
    const-string v0, "LIBRARY"

    const-string v1, " <<<<<<<<<<<< AFTER PlayerDeinitImage >>>>>>>>>>  "

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 460
    monitor-exit p0

    return-void

    .line 457
    :cond_0
    :try_start_1
    invoke-direct {p0, p1}, Lcom/samsung/app/video/editor/external/NativeInterface;->PlayerDeinitImage(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 452
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized EngineInitImage(Ljava/lang/String;)Z
    .locals 3
    .param p1, "inputFilePath"    # Ljava/lang/String;

    .prologue
    .line 463
    monitor-enter p0

    :try_start_0
    sget-boolean v0, Lcom/samsung/app/video/editor/external/NativeInterface;->isInitFIMC:Z

    if-eqz v0, :cond_1

    .line 464
    const-string v0, "LIBRARY"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " <<<<<<<<<<<< PlayerInitImage >>>>>>>>>>  "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 465
    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getJNIVersion()F

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 466
    sget v0, Lcom/samsung/app/video/editor/external/NativeInterface;->playExportHandle:I

    invoke-direct {p0, v0, p1}, Lcom/samsung/app/video/editor/external/NativeInterface;->PlayerInitImage(ILjava/lang/String;)V

    .line 471
    :goto_0
    const-string v0, "LIBRARY"

    const-string v1, " <<<<<<<<<<<< AFTER PlayerInitImage >>>>>>>>>>  "

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 472
    const/4 v0, 0x1

    .line 474
    :goto_1
    monitor-exit p0

    return v0

    .line 469
    :cond_0
    :try_start_1
    invoke-direct {p0, p1}, Lcom/samsung/app/video/editor/external/NativeInterface;->PlayerInitImage(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 463
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 474
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final declared-synchronized GetCodecFilePropertiesFromEngine(Ljava/lang/String;)I
    .locals 4
    .param p1, "filename"    # Ljava/lang/String;

    .prologue
    .line 639
    monitor-enter p0

    :try_start_0
    const-string v1, "LIBRARY"

    const-string v2, " <<<<<<<<<<<< GetCodecFileProperties >>>>>>>>>>  "

    invoke-static {v1, v2}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 640
    invoke-direct {p0, p1}, Lcom/samsung/app/video/editor/external/NativeInterface;->GetCodecFileProperties(Ljava/lang/String;)I

    move-result v0

    .line 641
    .local v0, "res":I
    const-string v1, "LIBRARY"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, " <<<<<<<<<<<< AFTER GetCodecFileProperties >>>>>>>>>>  "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 643
    monitor-exit p0

    return v0

    .line 639
    .end local v0    # "res":I
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public final declared-synchronized VEGetKenburnsFrameFromEngine(Ljava/lang/String;Lcom/samsung/app/video/editor/external/Element;Lcom/samsung/app/video/editor/external/Element;IIII)[B
    .locals 5
    .param p1, "filepath"    # Ljava/lang/String;
    .param p2, "firstElement"    # Lcom/samsung/app/video/editor/external/Element;
    .param p3, "element"    # Lcom/samsung/app/video/editor/external/Element;
    .param p4, "time"    # I
    .param p5, "width"    # I
    .param p6, "height"    # I
    .param p7, "effect"    # I

    .prologue
    .line 545
    monitor-enter p0

    :try_start_0
    const-string v2, "LIBRARY"

    const-string v3, " <<<<<<<<<<<< VEGetKenburnsFrame >>>>>>>>>>  "

    invoke-static {v2, v3}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 546
    invoke-direct/range {p0 .. p7}, Lcom/samsung/app/video/editor/external/NativeInterface;->VEGetKenburnsFrame(Ljava/lang/String;Lcom/samsung/app/video/editor/external/Element;Lcom/samsung/app/video/editor/external/Element;IIII)[B

    move-result-object v0

    .line 547
    .local v0, "res":[B
    if-eqz v0, :cond_0

    const/4 v1, 0x1

    .line 548
    .local v1, "valid":Z
    :goto_0
    const-string v2, "LIBRARY"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, " <<<<<<<<<<<< VEGetKenburnsFrame >>>>>>>>>>  "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 550
    monitor-exit p0

    return-object v0

    .line 547
    .end local v1    # "valid":Z
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 545
    .end local v0    # "res":[B
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized _currentBinaryType(I)V
    .locals 2
    .param p1, "type"    # I

    .prologue
    .line 931
    monitor-enter p0

    :try_start_0
    const-string v0, "LIBRARY"

    const-string v1, " <<<<<<<<<<<< currentBinaryType >>>>>>>>>>  "

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 932
    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getJNIVersion()F

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 933
    sget v0, Lcom/samsung/app/video/editor/external/NativeInterface;->playExportHandle:I

    invoke-direct {p0, v0, p1}, Lcom/samsung/app/video/editor/external/NativeInterface;->currentBinaryType(II)V

    .line 938
    :goto_0
    const-string v0, "LIBRARY"

    const-string v1, " <<<<<<<<<<<< AFTER currentBinaryType >>>>>>>>>>  "

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 939
    monitor-exit p0

    return-void

    .line 936
    :cond_0
    :try_start_1
    invoke-direct {p0, p1}, Lcom/samsung/app/video/editor/external/NativeInterface;->currentBinaryType(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 931
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized _freeSummaryOutput(I)I
    .locals 3
    .param p1, "editPointer"    # I

    .prologue
    .line 950
    monitor-enter p0

    :try_start_0
    const-string v1, "LIBRARY"

    const-string v2, " <<<<<<<<<<<< freeSummaryOutput >>>>>>>>>>  "

    invoke-static {v1, v2}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 951
    invoke-direct {p0, p1}, Lcom/samsung/app/video/editor/external/NativeInterface;->freeSummaryOutput(I)I

    move-result v0

    .line 952
    .local v0, "res":I
    const-string v1, "LIBRARY"

    const-string v2, " <<<<<<<<<<<< AFTER freeSummaryOutput >>>>>>>>>>  "

    invoke-static {v1, v2}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 954
    monitor-exit p0

    return v0

    .line 950
    .end local v0    # "res":I
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized _getProjectPreviewImage(Landroid/content/res/AssetManager;Lcom/samsung/app/video/editor/external/Element;IIIIIII)[B
    .locals 5
    .param p1, "assetManager"    # Landroid/content/res/AssetManager;
    .param p2, "ele"    # Lcom/samsung/app/video/editor/external/Element;
    .param p3, "projectPreviewTime"    # I
    .param p4, "videoWidth"    # I
    .param p5, "videoHeight"    # I
    .param p6, "isThemeRequired"    # I
    .param p7, "isEffectRequired"    # I
    .param p8, "ifCaptionRequired"    # I
    .param p9, "ifDrawingRequired"    # I

    .prologue
    .line 891
    monitor-enter p0

    :try_start_0
    sget-boolean v2, Lcom/samsung/app/video/editor/external/NativeInterface;->isInitFIMC:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_0

    .line 892
    const/4 v0, 0x0

    .line 908
    :goto_0
    monitor-exit p0

    return-object v0

    .line 894
    :cond_0
    :try_start_1
    const-string v2, "LIBRARY"

    const-string v3, " <<<<<<<<<<<< getProjectPreviewImage >>>>>>>>>>  "

    invoke-static {v2, v3}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 895
    invoke-direct/range {p0 .. p9}, Lcom/samsung/app/video/editor/external/NativeInterface;->getProjectPreviewImage(Landroid/content/res/AssetManager;Lcom/samsung/app/video/editor/external/Element;IIIIIII)[B

    move-result-object v0

    .line 905
    .local v0, "res":[B
    if-eqz v0, :cond_1

    const/4 v1, 0x1

    .line 906
    .local v1, "valid":Z
    :goto_1
    const-string v2, "LIBRARY"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, " <<<<<<<<<<<< AFTER getProjectPreviewImage >>>>>>>>>>  "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 891
    .end local v0    # "res":[B
    .end local v1    # "valid":Z
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 905
    .restart local v0    # "res":[B
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public declared-synchronized _getSummaryElementInElementListAt(Lcom/samsung/app/video/editor/external/Element;II)I
    .locals 3
    .param p1, "elem"    # Lcom/samsung/app/video/editor/external/Element;
    .param p2, "tePointer"    # I
    .param p3, "pos"    # I

    .prologue
    .line 958
    monitor-enter p0

    :try_start_0
    const-string v1, "LIBRARY"

    const-string v2, " <<<<<<<<<<<< getSummaryElementInElementListAt >>>>>>>>>>  "

    invoke-static {v1, v2}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 959
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/app/video/editor/external/NativeInterface;->getSummaryElementInElementListAt(Lcom/samsung/app/video/editor/external/Element;II)I

    move-result v0

    .line 960
    .local v0, "res":I
    const-string v1, "LIBRARY"

    const-string v2, " <<<<<<<<<<<< AFTER getSummaryElementInElementListAt >>>>>>>>>>  "

    invoke-static {v1, v2}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 962
    monitor-exit p0

    return v0

    .line 958
    .end local v0    # "res":I
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized _native_initfimc()V
    .locals 3

    .prologue
    .line 164
    monitor-enter p0

    :try_start_0
    sget-boolean v0, Lcom/samsung/app/video/editor/external/NativeInterface;->isInitFIMC:Z

    if-nez v0, :cond_0

    .line 165
    const-string v0, "LIBRARY"

    const-string v1, " <<<<<<<<<<<< INIT FIMC >>>>>>>>>>"

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getJNIVersion()F

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    .line 167
    const-string v0, "LIBRARY"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " <<<<<<<<<<<< Native Handle : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v2, Lcom/samsung/app/video/editor/external/NativeInterface;->playExportHandle:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    sget v0, Lcom/samsung/app/video/editor/external/NativeInterface;->playExportHandle:I

    invoke-direct {p0, v0}, Lcom/samsung/app/video/editor/external/NativeInterface;->native_initfimc(I)V

    .line 173
    :goto_0
    const-string v0, "LIBRARY"

    const-string v1, " <<<<<<<<<<<< AFTER INIT FIMC >>>>>>>>>>"

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    const/4 v0, 0x1

    sput-boolean v0, Lcom/samsung/app/video/editor/external/NativeInterface;->isInitFIMC:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 176
    :cond_0
    monitor-exit p0

    return-void

    .line 171
    :cond_1
    :try_start_1
    invoke-direct {p0}, Lcom/samsung/app/video/editor/external/NativeInterface;->native_initfimc()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 164
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized _native_terminate()V
    .locals 2

    .prologue
    .line 179
    monitor-enter p0

    :try_start_0
    sget-boolean v0, Lcom/samsung/app/video/editor/external/NativeInterface;->isInitFIMC:Z

    if-eqz v0, :cond_0

    .line 181
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/app/video/editor/external/NativeInterface;->deInitDecoderForFile(Ljava/lang/String;)V

    .line 183
    const-string v0, "LIBRARY"

    const-string v1, " <<<<<<<<<<<< DE-INIT FIMC >>>>>>>>>>"

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getJNIVersion()F

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    .line 185
    sget v0, Lcom/samsung/app/video/editor/external/NativeInterface;->playExportHandle:I

    invoke-direct {p0, v0}, Lcom/samsung/app/video/editor/external/NativeInterface;->native_terminate(I)V

    .line 190
    :goto_0
    const-string v0, "LIBRARY"

    const-string v1, " <<<<<<<<<<<< AFTER DE-INIT FIMC >>>>>>>>>>"

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/app/video/editor/external/NativeInterface;->isInitFIMC:Z

    .line 192
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/app/video/editor/external/NativeInterface;->mGPUCreated:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 194
    :cond_0
    monitor-exit p0

    return-void

    .line 188
    :cond_1
    :try_start_1
    invoke-direct {p0}, Lcom/samsung/app/video/editor/external/NativeInterface;->native_terminate()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 179
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized _surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 2
    .param p1, "holder"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 913
    monitor-enter p0

    if-nez p1, :cond_1

    .line 928
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 916
    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/NativeInterface;->mSurface:Landroid/view/Surface;

    invoke-interface {p1}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 918
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/app/video/editor/external/NativeInterface;->mGPUCreated:Z

    .line 919
    const-string v0, "LIBRARY"

    const-string v1, " <<<<<<<<<<<< surfaceDestroyed >>>>>>>>>>  "

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 920
    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getJNIVersion()F

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_2

    .line 921
    sget v0, Lcom/samsung/app/video/editor/external/NativeInterface;->playExportHandle:I

    invoke-direct {p0, v0}, Lcom/samsung/app/video/editor/external/NativeInterface;->surfaceDestroyed(I)V

    .line 926
    :goto_1
    const-string v0, "LIBRARY"

    const-string v1, " <<<<<<<<<<<< AFTER surfaceDestroyed >>>>>>>>>>  "

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 913
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 924
    :cond_2
    :try_start_1
    invoke-direct {p0}, Lcom/samsung/app/video/editor/external/NativeInterface;->surfaceDestroyed()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method public final declared-synchronized applicationEntryPoint()V
    .locals 4

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 554
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/samsung/app/video/editor/external/NativeInterface;->mAppEntryPointRefCount:I

    if-nez v0, :cond_1

    .line 555
    const-string v0, "LIBRARY"

    const-string v1, " <<<<<<<<<<<< VideoEditorLaunch >>>>>>>>>>  "

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 556
    const-string v1, "LIBRARY"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "<<<<<<<<<<<<<<<<Application making Entry point, going to use "

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getJNIVersion()F

    move-result v0

    cmpl-float v0, v0, v3

    if-nez v0, :cond_2

    .line 557
    const-string v0, "New Libs(new player) provided"

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 556
    invoke-static {v1, v0}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 558
    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getJNIVersion()F

    move-result v0

    cmpl-float v0, v0, v3

    if-nez v0, :cond_0

    .line 562
    invoke-direct {p0}, Lcom/samsung/app/video/editor/external/NativeInterface;->NewVideoEditorLaunch()I

    move-result v0

    sput v0, Lcom/samsung/app/video/editor/external/NativeInterface;->playExportHandle:I

    .line 563
    sget v0, Lcom/samsung/app/video/editor/external/NativeInterface;->playExportHandle:I

    if-nez v0, :cond_0

    .line 564
    const-string v0, "LIBRARY"

    const-string v1, "<<<<<<<<<<<< VideoEditorLaunch >>>>>>>>>>  returns null, so failed"

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 579
    :cond_0
    const-string v0, "LIBRARY"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "<<<<<<<<<<<< VideoEditorLaunch >>>>>>>>>>  returns : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v2, Lcom/samsung/app/video/editor/external/NativeInterface;->playExportHandle:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 580
    const-string v0, "LIBRARY"

    const-string v1, " <<<<<<<<<<<< AFTER VideoEditorLaunch >>>>>>>>>>  "

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 581
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/app/video/editor/external/NativeInterface;->_currentBinaryType(I)V

    .line 582
    invoke-direct {p0}, Lcom/samsung/app/video/editor/external/NativeInterface;->_native_setup()V

    .line 584
    :cond_1
    iget v0, p0, Lcom/samsung/app/video/editor/external/NativeInterface;->mAppEntryPointRefCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/app/video/editor/external/NativeInterface;->mAppEntryPointRefCount:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 585
    monitor-exit p0

    return-void

    .line 557
    :cond_2
    :try_start_1
    const-string v0, "old libs only"
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 554
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized applicationExitPoint()V
    .locals 2

    .prologue
    .line 588
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/samsung/app/video/editor/external/NativeInterface;->mAppEntryPointRefCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/samsung/app/video/editor/external/NativeInterface;->mAppEntryPointRefCount:I

    .line 589
    iget v0, p0, Lcom/samsung/app/video/editor/external/NativeInterface;->mAppEntryPointRefCount:I

    if-nez v0, :cond_1

    .line 590
    const-string v0, "LIBRARY"

    const-string v1, " <<<<<<<<<<<< VideoEditorTerminate >>>>>>>>>>  "

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 591
    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getJNIVersion()F

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 592
    sget v0, Lcom/samsung/app/video/editor/external/NativeInterface;->playExportHandle:I

    invoke-direct {p0, v0}, Lcom/samsung/app/video/editor/external/NativeInterface;->VideoEditorTerminate(I)V

    .line 601
    :cond_0
    const-string v0, "LIBRARY"

    const-string v1, " <<<<<<<<<<<< AFTER VideoEditorTerminate >>>>>>>>>>  "

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 603
    :cond_1
    monitor-exit p0

    return-void

    .line 588
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized applyEffectToMediaThumbnail([BIIII)[B
    .locals 5
    .param p1, "videoBuffer"    # [B
    .param p2, "videoWidth"    # I
    .param p3, "videoHeight"    # I
    .param p4, "effectType"    # I
    .param p5, "iseffect"    # I

    .prologue
    .line 480
    monitor-enter p0

    :try_start_0
    const-string v2, "LIBRARY"

    const-string v3, " <<<<<<<<<<<< applyEffectToVideoThumbnail >>>>>>>>>>  "

    invoke-static {v2, v3}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 481
    invoke-direct/range {p0 .. p5}, Lcom/samsung/app/video/editor/external/NativeInterface;->applyEffectToVideoThumbnail([BIIII)[B

    move-result-object v0

    .line 482
    .local v0, "res":[B
    if-eqz v0, :cond_0

    const/4 v1, 0x1

    .line 483
    .local v1, "valid":Z
    :goto_0
    const-string v2, "LIBRARY"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, " <<<<<<<<<<<< AFTER applyEffectToVideoThumbnail >>>>>>>>>>  "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 485
    monitor-exit p0

    return-object v0

    .line 482
    .end local v1    # "valid":Z
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 480
    .end local v0    # "res":[B
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public final declared-synchronized clearSummaryInstance()V
    .locals 2

    .prologue
    .line 999
    monitor-enter p0

    :try_start_0
    const-string v0, "LIBRARY"

    const-string v1, " <<<<<<<<<<<< deleteSummaryInstance >>>>>>>>>>  "

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 1000
    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getJNIVersion()F

    move-result v0

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getJNIVersion()F

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    .line 1001
    :cond_0
    invoke-direct {p0}, Lcom/samsung/app/video/editor/external/NativeInterface;->deleteSummaryInstance()I

    .line 1002
    :cond_1
    const-string v0, "LIBRARY"

    const-string v1, " <<<<<<<<<<<< AFTER deleteSummaryInstance >>>>>>>>>>  "

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1003
    monitor-exit p0

    return-void

    .line 999
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized createMediaPlayer(Lcom/samsung/app/video/editor/external/TranscodeElement;)V
    .locals 2
    .param p1, "transcodeElement"    # Lcom/samsung/app/video/editor/external/TranscodeElement;

    .prologue
    .line 655
    monitor-enter p0

    :try_start_0
    const-string v0, "LIBRARY"

    const-string v1, " <<<<<<<<<<<< createPreviewPlayer >>>>>>>>>>  "

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 656
    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getJNIVersion()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    .line 657
    invoke-direct {p0}, Lcom/samsung/app/video/editor/external/NativeInterface;->createPreviewPlayer()V

    .line 660
    :cond_0
    :goto_0
    const-string v0, "LIBRARY"

    const-string v1, " <<<<<<<<<<<< AFTER createPreviewPlayer >>>>>>>>>>  "

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 661
    monitor-exit p0

    return-void

    .line 658
    :cond_1
    :try_start_1
    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getJNIVersion()F

    move-result v0

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 659
    invoke-direct {p0, p1}, Lcom/samsung/app/video/editor/external/NativeInterface;->createPreviewPlayer(Lcom/samsung/app/video/editor/external/TranscodeElement;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 655
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final createSummaryFromEngine(Lcom/samsung/app/video/editor/external/TranscodeElement;Lcom/samsung/app/video/editor/external/TranscodeElement;DI)I
    .locals 5
    .param p1, "input"    # Lcom/samsung/app/video/editor/external/TranscodeElement;
    .param p2, "output"    # Lcom/samsung/app/video/editor/external/TranscodeElement;
    .param p3, "mSummaryDuration"    # D
    .param p5, "mode"    # I

    .prologue
    .line 535
    const-string v1, "LIBRARY"

    const-string v2, " <<<<<<<<<<<< createSummary >>>>>>>>>>  "

    invoke-static {v1, v2}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 537
    invoke-direct/range {p0 .. p5}, Lcom/samsung/app/video/editor/external/NativeInterface;->createSummary(Lcom/samsung/app/video/editor/external/TranscodeElement;Lcom/samsung/app/video/editor/external/TranscodeElement;DI)I

    move-result v0

    .line 538
    .local v0, "res":I
    const-string v1, "LIBRARY"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, " <<<<<<<<<<<< AFTER createSummary >>>>>>>>>>  "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 539
    return v0
.end method

.method public final createSummaryInstance(Lcom/samsung/app/video/editor/external/TranscodeElement;II)I
    .locals 4
    .param p1, "input"    # Lcom/samsung/app/video/editor/external/TranscodeElement;
    .param p2, "mode"    # I
    .param p3, "summaryVersion"    # I

    .prologue
    .line 986
    const/4 v0, -0x1

    .line 987
    .local v0, "res":I
    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getJNIVersion()F

    move-result v1

    const/high16 v2, 0x3f800000    # 1.0f

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getJNIVersion()F

    move-result v1

    const/high16 v2, 0x40000000    # 2.0f

    cmpl-float v1, v1, v2

    if-nez v1, :cond_1

    :cond_0
    sget-boolean v1, Lcom/samsung/app/video/editor/external/NativeInterface;->isInitFIMC:Z

    if-eqz v1, :cond_1

    .line 988
    const-string v1, "LIBRARY"

    const-string v2, " <<<<<<<<<<<< createInstanceSummary >>>>>>>>>>  "

    invoke-static {v1, v2}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 989
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/app/video/editor/external/NativeInterface;->createInstanceSummary(Lcom/samsung/app/video/editor/external/TranscodeElement;II)I

    move-result v0

    .line 990
    const-string v1, "LIBRARY"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, " <<<<<<<<<<<< AFTER createInstanceSummary >>>>>>>>>>  "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 995
    :goto_0
    return v0

    .line 993
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public declared-synchronized deInitDecoderForFile(Ljava/lang/String;)V
    .locals 3
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    const/4 v1, -0x1

    .line 831
    monitor-enter p0

    :try_start_0
    sget-boolean v0, Lcom/samsung/app/video/editor/external/NativeInterface;->isInitFIMC:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 841
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 834
    :cond_1
    :try_start_1
    sget v0, Lcom/samsung/app/video/editor/external/NativeInterface;->thumbHandle:I

    if-eq v0, v1, :cond_0

    .line 835
    const-string v0, "LIBRARY"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " <<<<<<<<<<<< thumbnailDeinit >>>>>>>>>>  "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v2, Lcom/samsung/app/video/editor/external/NativeInterface;->thumbHandle:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 836
    sget v0, Lcom/samsung/app/video/editor/external/NativeInterface;->thumbHandle:I

    invoke-direct {p0, v0}, Lcom/samsung/app/video/editor/external/NativeInterface;->thumbnailDeinit(I)I

    .line 837
    const-string v0, "LIBRARY"

    const-string v1, " <<<<<<<<<<<< AFTER thumbnailDeinit >>>>>>>>>>  "

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 838
    const/4 v0, -0x1

    sput v0, Lcom/samsung/app/video/editor/external/NativeInterface;->thumbHandle:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 831
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final exportProject(Lcom/samsung/app/video/editor/external/TranscodeElement;Ljava/lang/String;Z)I
    .locals 4
    .param p1, "transcodeElement"    # Lcom/samsung/app/video/editor/external/TranscodeElement;
    .param p2, "outFileName"    # Ljava/lang/String;
    .param p3, "stablize"    # Z

    .prologue
    .line 436
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v2

    monitor-enter v2

    .line 437
    :try_start_0
    const-string v1, "LIBRARY"

    const-string v3, " <<<<<<<<<<<< VEEdit - Wait Done >>>>>>>>>>  "

    invoke-static {v1, v3}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 436
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 441
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/samsung/app/video/editor/external/NativeInterface;->deInitDecoderForFile(Ljava/lang/String;)V

    .line 444
    const-string v1, "LIBRARY"

    const-string v2, " <<<<<<<<<<<< VEEdit >>>>>>>>>>  "

    invoke-static {v1, v2}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 445
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/app/video/editor/external/NativeInterface;->VEEdit(Lcom/samsung/app/video/editor/external/TranscodeElement;Ljava/lang/String;Z)I

    move-result v0

    .line 446
    .local v0, "res":I
    const-string v1, "LIBRARY"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, " <<<<<<<<<<<< AFTER VEEdit >>>>>>>>>>  "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 448
    return v0

    .line 436
    .end local v0    # "res":I
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public final getCurrentPosition()I
    .locals 4

    .prologue
    .line 239
    const-string v1, "LIBRARY"

    const-string v2, " <<<<<<<<<<<< getCurrentPositionOfPreviewPlayer >>>>>>>>>>  "

    invoke-static {v1, v2}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    const/4 v0, 0x0

    .line 242
    .local v0, "res":I
    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getJNIVersion()F

    move-result v1

    const/high16 v2, 0x40000000    # 2.0f

    cmpl-float v1, v1, v2

    if-nez v1, :cond_0

    .line 243
    sget v1, Lcom/samsung/app/video/editor/external/NativeInterface;->playExportHandle:I

    invoke-direct {p0, v1}, Lcom/samsung/app/video/editor/external/NativeInterface;->getCurrentPositionOfVeEngine(I)I

    move-result v0

    .line 248
    :goto_0
    const-string v1, "LIBRARY"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, " <<<<<<<<<<<< AFTER getCurrentPositionOfPreviewPlayer >>>>>>>>>>  "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    return v0

    .line 246
    :cond_0
    invoke-direct {p0}, Lcom/samsung/app/video/editor/external/NativeInterface;->getCurrentPositionOfPreviewPlayer()I

    move-result v0

    goto :goto_0
.end method

.method public declared-synchronized initDecoderForFile(Landroid/content/res/AssetManager;Ljava/lang/String;)Z
    .locals 4
    .param p1, "assetManager"    # Landroid/content/res/AssetManager;
    .param p2, "path"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 797
    monitor-enter p0

    :try_start_0
    sget-boolean v1, Lcom/samsung/app/video/editor/external/NativeInterface;->isInitFIMC:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    .line 811
    :goto_0
    monitor-exit p0

    return v0

    .line 799
    :cond_0
    :try_start_1
    invoke-virtual {p0, p2}, Lcom/samsung/app/video/editor/external/NativeInterface;->deInitDecoderForFile(Ljava/lang/String;)V

    .line 801
    const-string v1, "LIBRARY"

    const-string v2, " <<<<<<<<<<<< thumbnailInit >>>>>>>>>>  "

    invoke-static {v1, v2}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 802
    invoke-direct {p0, p1, p2}, Lcom/samsung/app/video/editor/external/NativeInterface;->thumbnailInit(Landroid/content/res/AssetManager;Ljava/lang/String;)I

    move-result v1

    sput v1, Lcom/samsung/app/video/editor/external/NativeInterface;->thumbHandle:I

    .line 803
    const-string v1, "LIBRARY"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, " <<<<<<<<<<<< AFTER thumbnailInit >>>>>>>>>>  "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v3, Lcom/samsung/app/video/editor/external/NativeInterface;->thumbHandle:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 806
    sget v1, Lcom/samsung/app/video/editor/external/NativeInterface;->thumbHandle:I

    if-nez v1, :cond_1

    .line 807
    const/4 v1, -0x1

    sput v1, Lcom/samsung/app/video/editor/external/NativeInterface;->thumbHandle:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 797
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 811
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isJPG(Ljava/lang/String;)Z
    .locals 4
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 285
    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getJNIVersion()F

    move-result v2

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-nez v2, :cond_3

    .line 286
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "jpeg"

    invoke-virtual {v2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "jpg"

    invoke-virtual {v2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 287
    :cond_0
    invoke-static {p1}, Lcom/sec/android/app/ve/common/MediaUtils;->isImageLarger13MP(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 288
    invoke-direct {p0, p1}, Lcom/samsung/app/video/editor/external/NativeInterface;->_FindIfJPEG(Ljava/lang/String;)I

    move-result v2

    if-ne v2, v0, :cond_2

    .line 297
    :cond_1
    :goto_0
    return v0

    :cond_2
    move v0, v1

    .line 286
    goto :goto_0

    .line 290
    :cond_3
    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getJNIVersion()F

    move-result v2

    const/high16 v3, 0x3f800000    # 1.0f

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_4

    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getJNIVersion()F

    move-result v2

    const/high16 v3, 0x40000000    # 2.0f

    cmpl-float v2, v2, v3

    if-nez v2, :cond_7

    .line 291
    :cond_4
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "jpeg"

    invoke-virtual {v2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_5

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "jpg"

    invoke-virtual {v2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 292
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    const-string v3, ".png"

    invoke-virtual {v2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 293
    :cond_5
    invoke-static {p1}, Lcom/sec/android/app/ve/common/MediaUtils;->isImageLarger13MP(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 294
    invoke-direct {p0, p1}, Lcom/samsung/app/video/editor/external/NativeInterface;->_FindIfJPEG(Ljava/lang/String;)I

    move-result v2

    if-eq v2, v0, :cond_1

    :cond_6
    move v0, v1

    .line 291
    goto :goto_0

    :cond_7
    move v0, v1

    .line 297
    goto :goto_0
.end method

.method public declared-synchronized isOverlapPossible(Lcom/samsung/app/video/editor/external/Element;Lcom/samsung/app/video/editor/external/Element;)Z
    .locals 4
    .param p1, "element"    # Lcom/samsung/app/video/editor/external/Element;
    .param p2, "next"    # Lcom/samsung/app/video/editor/external/Element;

    .prologue
    .line 977
    monitor-enter p0

    :try_start_0
    const-string v1, "LIBRARY"

    const-string v2, " <<<<<<<<<<<< getOverlapCondition >>>>>>>>>>  "

    invoke-static {v1, v2}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 978
    invoke-direct {p0, p1, p2}, Lcom/samsung/app/video/editor/external/NativeInterface;->getOverlapCondition(Lcom/samsung/app/video/editor/external/Element;Lcom/samsung/app/video/editor/external/Element;)Z

    move-result v0

    .line 979
    .local v0, "res":Z
    const-string v1, "LIBRARY"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, " <<<<<<<<<<<< AFTER getOverlapCondition >>>>>>>>>>  "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 981
    monitor-exit p0

    return v0

    .line 977
    .end local v0    # "res":Z
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized makeSurfaceBlack()V
    .locals 1

    .prologue
    .line 884
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, v0}, Lcom/samsung/app/video/editor/external/NativeInterface;->makeSurfaceBlack(Landroid/content/res/AssetManager;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 885
    monitor-exit p0

    return-void

    .line 884
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized makeSurfaceBlack(Landroid/content/res/AssetManager;)V
    .locals 11
    .param p1, "assetManager"    # Landroid/content/res/AssetManager;

    .prologue
    .line 852
    monitor-enter p0

    :try_start_0
    sget-boolean v0, Lcom/samsung/app/video/editor/external/NativeInterface;->isInitFIMC:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 874
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 855
    :cond_1
    :try_start_1
    const-string v0, "LIBRARY"

    const-string v1, " <<<<<<<<<<<< makeSurfaceBlack thumbnailInit >>>>>>>>>>  "

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 856
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/samsung/app/video/editor/external/NativeInterface;->thumbnailInit(Landroid/content/res/AssetManager;Ljava/lang/String;)I

    move-result v3

    .line 857
    .local v3, "handle":I
    const-string v0, "LIBRARY"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " <<<<<<<<<<<< AFTER makeSurfaceBlack thumbnailInit >>>>>>>>>>  "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 860
    if-eqz v3, :cond_0

    .line 864
    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getJNIVersion()F

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_2

    if-nez p1, :cond_2

    .line 865
    const-string v0, "LIBRARY"

    const-string v1, " <<<<<<<<<<<< makeSurfaceBlack getThumbnail >>>>>>>>>>  "

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 866
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v10}, Lcom/samsung/app/video/editor/external/NativeInterface;->getThumbnail(Lcom/samsung/app/video/editor/external/Element;Lcom/samsung/app/video/editor/external/Element;IIIIILjava/util/List;Ljava/util/List;F)V

    .line 867
    const-string v0, "LIBRARY"

    const-string v1, " <<<<<<<<<<<< AFTER makeSurfaceBlack getThumbnail >>>>>>>>>>  "

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 870
    :cond_2
    const-string v0, "LIBRARY"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " <<<<<<<<<<<< makeSurfaceBlack thumbnailDeinit >>>>>>>>>>  "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 871
    invoke-direct {p0, v3}, Lcom/samsung/app/video/editor/external/NativeInterface;->thumbnailDeinit(I)I

    .line 872
    const-string v0, "LIBRARY"

    const-string v1, " <<<<<<<<<<<< AFTER makeSurfaceBlack thumbnailDeinit >>>>>>>>>>  "

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 852
    .end local v3    # "handle":I
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized pause()I
    .locals 3

    .prologue
    .line 321
    monitor-enter p0

    const/4 v0, 0x0

    .line 322
    .local v0, "pausedTime":I
    :try_start_0
    const-string v1, "LIBRARY"

    const-string v2, " <<<<<<<<<<<< pausePreviewPlayer >>>>>>>>>>  "

    invoke-static {v1, v2}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getJNIVersion()F

    move-result v1

    const/high16 v2, 0x40000000    # 2.0f

    cmpl-float v1, v1, v2

    if-nez v1, :cond_0

    .line 324
    sget v1, Lcom/samsung/app/video/editor/external/NativeInterface;->playExportHandle:I

    invoke-direct {p0, v1}, Lcom/samsung/app/video/editor/external/NativeInterface;->PauseVeEngine(I)I

    move-result v0

    .line 329
    :goto_0
    const-string v1, "LIBRARY"

    const-string v2, " <<<<<<<<<<<< AFTER pausePreviewPlayer >>>>>>>>>>  "

    invoke-static {v1, v2}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 330
    monitor-exit p0

    return v0

    .line 327
    :cond_0
    :try_start_1
    invoke-direct {p0}, Lcom/samsung/app/video/editor/external/NativeInterface;->pausePreviewPlayer()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 321
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public final declared-synchronized pauseExport()V
    .locals 2

    .prologue
    .line 626
    monitor-enter p0

    :try_start_0
    const-string v0, "LIBRARY"

    const-string v1, " <<<<<<<<<<<< PauseVEEditExportFunction >>>>>>>>>>  "

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 627
    invoke-direct {p0}, Lcom/samsung/app/video/editor/external/NativeInterface;->PauseVEEditExportFunction()V

    .line 628
    const-string v0, "LIBRARY"

    const-string v1, " <<<<<<<<<<<< AFTER PauseVEEditExportFunction >>>>>>>>>>  "

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 629
    monitor-exit p0

    return-void

    .line 626
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized previewFrame(Lcom/samsung/app/video/editor/external/Element;Lcom/samsung/app/video/editor/external/Element;IIIILjava/util/List;Ljava/util/List;F)V
    .locals 12
    .param p1, "firstElement"    # Lcom/samsung/app/video/editor/external/Element;
    .param p2, "secondElement"    # Lcom/samsung/app/video/editor/external/Element;
    .param p3, "time"    # I
    .param p4, "width"    # I
    .param p5, "height"    # I
    .param p6, "previewType"    # I
    .param p9, "storyTime"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/app/video/editor/external/Element;",
            "Lcom/samsung/app/video/editor/external/Element;",
            "IIII",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/ClipartParams;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/ClipartParams;",
            ">;F)V"
        }
    .end annotation

    .prologue
    .line 815
    .local p7, "clipartList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    .local p8, "drawList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    monitor-enter p0

    :try_start_0
    sget-boolean v1, Lcom/samsung/app/video/editor/external/NativeInterface;->isInitFIMC:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_1

    .line 828
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 818
    :cond_1
    :try_start_1
    sget v1, Lcom/samsung/app/video/editor/external/NativeInterface;->thumbHandle:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 820
    const-string v1, "LIBRARY"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, " <<<<<<<<<<<< getThumbnail : time >>>>>>>>>>  "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "StoryBoardTime = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p9

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 821
    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getJNIVersion()F

    move-result v1

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-nez v1, :cond_3

    .line 822
    sget v4, Lcom/samsung/app/video/editor/external/NativeInterface;->thumbHandle:I

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v5, p3

    move/from16 v6, p4

    move/from16 v7, p5

    move/from16 v8, p6

    move-object/from16 v9, p7

    move/from16 v10, p9

    invoke-direct/range {v1 .. v10}, Lcom/samsung/app/video/editor/external/NativeInterface;->getThumbnail(Lcom/samsung/app/video/editor/external/Element;Lcom/samsung/app/video/editor/external/Element;IIIIILjava/util/List;F)V

    .line 826
    :cond_2
    :goto_1
    const-string v1, "LIBRARY"

    const-string v2, " <<<<<<<<<<<< AFTER getThumbnail >>>>>>>>>>  "

    invoke-static {v1, v2}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 815
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 823
    :cond_3
    :try_start_2
    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getJNIVersion()F

    move-result v1

    const/high16 v2, 0x3f800000    # 1.0f

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_4

    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getJNIVersion()F

    move-result v1

    const/high16 v2, 0x40000000    # 2.0f

    cmpl-float v1, v1, v2

    if-nez v1, :cond_2

    .line 824
    :cond_4
    sget v4, Lcom/samsung/app/video/editor/external/NativeInterface;->thumbHandle:I

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v5, p3

    move/from16 v6, p4

    move/from16 v7, p5

    move/from16 v8, p6

    move-object/from16 v9, p7

    move-object/from16 v10, p8

    move/from16 v11, p9

    invoke-direct/range {v1 .. v11}, Lcom/samsung/app/video/editor/external/NativeInterface;->getThumbnail(Lcom/samsung/app/video/editor/external/Element;Lcom/samsung/app/video/editor/external/Element;IIIIILjava/util/List;Ljava/util/List;F)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method public final declared-synchronized resume(I)I
    .locals 3
    .param p1, "lastPausedTime"    # I

    .prologue
    .line 375
    monitor-enter p0

    const/4 v0, 0x0

    .line 376
    .local v0, "ret":I
    :try_start_0
    const-string v1, "LIBRARY"

    const-string v2, " <<<<<<<<<<<< resumePreviewPlayer >>>>>>>>>>  "

    invoke-static {v1, v2}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 377
    sget v1, Lcom/samsung/app/video/editor/external/NativeInterface;->playExportHandle:I

    invoke-direct {p0, v1, p1}, Lcom/samsung/app/video/editor/external/NativeInterface;->ResumeVeEngine(II)V

    .line 378
    const-string v1, "LIBRARY"

    const-string v2, " <<<<<<<<<<<< AFTER resumePreviewPlayer >>>>>>>>>>  "

    invoke-static {v1, v2}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 379
    monitor-exit p0

    return v0

    .line 375
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public final declared-synchronized resumeExport()V
    .locals 2

    .prologue
    .line 632
    monitor-enter p0

    :try_start_0
    const-string v0, "LIBRARY"

    const-string v1, " <<<<<<<<<<<< ResumeVEEditExportFunction >>>>>>>>>>  "

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 633
    invoke-direct {p0}, Lcom/samsung/app/video/editor/external/NativeInterface;->ResumeVEEditExportFunction()V

    .line 634
    const-string v0, "LIBRARY"

    const-string v1, " <<<<<<<<<<<< AFTER ResumeVEEditExportFunction >>>>>>>>>>  "

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 635
    monitor-exit p0

    return-void

    .line 632
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized seek(I)V
    .locals 3
    .param p1, "seektime"    # I

    .prologue
    .line 385
    monitor-enter p0

    :try_start_0
    const-string v0, "LIBRARY"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " <<<<<<<<<<<< seekPreviewPlayer >>>>>>>>>>  "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 386
    invoke-direct {p0, p1}, Lcom/samsung/app/video/editor/external/NativeInterface;->seekPreviewPlayer(I)V

    .line 387
    const-string v0, "LIBRARY"

    const-string v1, " <<<<<<<<<<<< AFTER seekPreviewPlayer >>>>>>>>>>  "

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 388
    monitor-exit p0

    return-void

    .line 385
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized setDisplayAspectRatio(II)V
    .locals 2
    .param p1, "prevWidth"    # I
    .param p2, "prevHeight"    # I

    .prologue
    .line 680
    monitor-enter p0

    :try_start_0
    const-string v0, "LIBRARY"

    const-string v1, " <<<<<<<<<<<< setAR >>>>>>>>>>  "

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 681
    invoke-direct {p0, p1, p2}, Lcom/samsung/app/video/editor/external/NativeInterface;->setAR(II)V

    .line 682
    const-string v0, "LIBRARY"

    const-string v1, " <<<<<<<<<<<< AFTER setAR >>>>>>>>>>  "

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 683
    monitor-exit p0

    return-void

    .line 680
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setEngineSurface(Landroid/view/SurfaceHolder;)V
    .locals 2
    .param p1, "mSurfaceHolder"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 198
    monitor-enter p0

    :try_start_0
    sget-boolean v0, Lcom/samsung/app/video/editor/external/NativeInterface;->isInitFIMC:Z

    if-eqz v0, :cond_0

    .line 200
    sget v0, Lcom/sec/android/app/ve/common/ConfigUtils;->FIXED_PREVIEW_WIDTH:I

    sget v1, Lcom/sec/android/app/ve/common/ConfigUtils;->FIXED_PREVIEW_HEIGHT:I

    invoke-virtual {p0, v0, v1}, Lcom/samsung/app/video/editor/external/NativeInterface;->setDisplayAspectRatio(II)V

    .line 201
    invoke-virtual {p0, p1}, Lcom/samsung/app/video/editor/external/NativeInterface;->setPreviewDisplay(Landroid/view/SurfaceHolder;)V

    .line 202
    invoke-direct {p0}, Lcom/samsung/app/video/editor/external/NativeInterface;->_createGPUEngine()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 204
    :cond_0
    monitor-exit p0

    return-void

    .line 198
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized setPreviewDisplay(Landroid/view/SurfaceHolder;)V
    .locals 3
    .param p1, "holder"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 405
    monitor-enter p0

    if-eqz p1, :cond_1

    :try_start_0
    invoke-interface {p1}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Surface;->isValid()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 406
    invoke-interface {p1}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/app/video/editor/external/NativeInterface;->mSurface:Landroid/view/Surface;

    .line 407
    const-string v0, "LIBRARY"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " <<<<<<<<<<<< setPreviewDisplay >>>>>>>>>> and is surface Valid? "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/samsung/app/video/editor/external/NativeInterface;->mSurface:Landroid/view/Surface;

    invoke-virtual {v2}, Landroid/view/Surface;->isValid()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 408
    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getJNIVersion()F

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 409
    sget v0, Lcom/samsung/app/video/editor/external/NativeInterface;->playExportHandle:I

    iget-object v1, p0, Lcom/samsung/app/video/editor/external/NativeInterface;->mSurface:Landroid/view/Surface;

    invoke-direct {p0, v0, v1}, Lcom/samsung/app/video/editor/external/NativeInterface;->setPreviewDisplay(ILandroid/view/Surface;)V

    .line 414
    :goto_0
    const-string v0, "LIBRARY"

    const-string v1, " <<<<<<<<<<<< AFTER setPreviewDisplay >>>>>>>>>>"

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 419
    :goto_1
    monitor-exit p0

    return-void

    .line 412
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/NativeInterface;->mSurface:Landroid/view/Surface;

    invoke-direct {p0, v0}, Lcom/samsung/app/video/editor/external/NativeInterface;->setPreviewDisplay(Landroid/view/Surface;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 405
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 417
    :cond_1
    :try_start_2
    const-string v0, "LIBRARY"

    const-string v1, " <<<<<<<<<<<< Ignored setPreviewDisplay since the surface is invalid. Subsequent call should update the surface >>>>>>>>>>"

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method public declared-synchronized setStackStateListener(Lcom/samsung/app/video/editor/external/NativeInterface$previewPlayerStateListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/samsung/app/video/editor/external/NativeInterface$previewPlayerStateListener;

    .prologue
    .line 422
    monitor-enter p0

    :try_start_0
    const-string v0, "LIBRARY"

    const-string v1, "Setting mStackStateListener"

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 423
    sput-object p1, Lcom/samsung/app/video/editor/external/NativeInterface;->mStackStateListener:Lcom/samsung/app/video/editor/external/NativeInterface$previewPlayerStateListener;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 424
    monitor-exit p0

    return-void

    .line 422
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized setTranscodeElementForEngine(Lcom/samsung/app/video/editor/external/TranscodeElement;)V
    .locals 2
    .param p1, "transcodeElement"    # Lcom/samsung/app/video/editor/external/TranscodeElement;

    .prologue
    .line 647
    monitor-enter p0

    :try_start_0
    const-string v0, "LIBRARY"

    const-string v1, " <<<<<<<<<<<< setDisplayForPreviewPlayer >>>>>>>>>>  "

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 648
    invoke-direct {p0, p1}, Lcom/samsung/app/video/editor/external/NativeInterface;->setDisplayForPreviewPlayer(Lcom/samsung/app/video/editor/external/TranscodeElement;)V

    .line 649
    const-string v0, "LIBRARY"

    const-string v1, " <<<<<<<<<<<< AFTER setDisplayForPreviewPlayer >>>>>>>>>>  "

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 650
    monitor-exit p0

    return-void

    .line 647
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized startVeEngine(Lcom/samsung/app/video/editor/external/TranscodeElement;JLcom/samsung/app/video/editor/external/TranscodeElement$ExportParameters;)I
    .locals 4
    .param p1, "transcodeElement"    # Lcom/samsung/app/video/editor/external/TranscodeElement;
    .param p2, "playPos"    # J
    .param p4, "expParams"    # Lcom/samsung/app/video/editor/external/TranscodeElement$ExportParameters;

    .prologue
    .line 664
    monitor-enter p0

    :try_start_0
    sget-boolean v1, Lcom/samsung/app/video/editor/external/NativeInterface;->isInitFIMC:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    .line 665
    const/4 v0, 0x0

    .line 670
    :goto_0
    monitor-exit p0

    return v0

    .line 666
    :cond_0
    :try_start_1
    const-string v1, "LIBRARY"

    const-string v2, " <<<<<<<<<<<< startVEEngine >>>>>>>>>>  "

    invoke-static {v1, v2}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 667
    const/4 v0, 0x0

    .line 668
    .local v0, "ret":I
    sget v1, Lcom/samsung/app/video/editor/external/NativeInterface;->playExportHandle:I

    long-to-int v2, p2

    invoke-direct {p0, v1, p1, v2, p4}, Lcom/samsung/app/video/editor/external/NativeInterface;->startVeEngine(ILcom/samsung/app/video/editor/external/TranscodeElement;ILcom/samsung/app/video/editor/external/TranscodeElement$ExportParameters;)I

    move-result v0

    .line 669
    const-string v1, "LIBRARY"

    const-string v2, " <<<<<<<<<<<< AFTER startVEEngine >>>>>>>>>>  "

    invoke-static {v1, v2}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 664
    .end local v0    # "ret":I
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public final declared-synchronized stop()V
    .locals 2

    .prologue
    .line 713
    monitor-enter p0

    :try_start_0
    const-string v0, "LIBRARY"

    const-string v1, " <<<<<<<<<<<< stopPreviewPlayer >>>>>>>>>>  "

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 714
    invoke-direct {p0}, Lcom/samsung/app/video/editor/external/NativeInterface;->stopPreviewPlayer()V

    .line 715
    const-string v0, "LIBRARY"

    const-string v1, " <<<<<<<<<<<< AFTER stopPreviewPlayer >>>>>>>>>>  "

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 716
    monitor-exit p0

    return-void

    .line 713
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized stopEngineSummary()V
    .locals 2

    .prologue
    .line 528
    monitor-enter p0

    :try_start_0
    const-string v0, "LIBRARY"

    const-string v1, " <<<<<<<<<<<< stopSummary >>>>>>>>>>  "

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 529
    invoke-direct {p0}, Lcom/samsung/app/video/editor/external/NativeInterface;->stopSummary()V

    .line 530
    const-string v0, "LIBRARY"

    const-string v1, " <<<<<<<<<<<< AFTER stopSummary >>>>>>>>>>  "

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 531
    monitor-exit p0

    return-void

    .line 528
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized stopExport()V
    .locals 2

    .prologue
    .line 619
    monitor-enter p0

    :try_start_0
    const-string v0, "LIBRARY"

    const-string v1, " <<<<<<<<<<<< StopVEEditExportFunction >>>>>>>>>>  "

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 620
    invoke-direct {p0}, Lcom/samsung/app/video/editor/external/NativeInterface;->StopVEEditExportFunction()V

    .line 621
    const-string v0, "LIBRARY"

    const-string v1, " <<<<<<<<<<<< AFTER StopVEEditExportFunction >>>>>>>>>>  "

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 622
    monitor-exit p0

    return-void

    .line 619
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized stopVeEngine()V
    .locals 2

    .prologue
    .line 674
    monitor-enter p0

    :try_start_0
    const-string v0, "LIBRARY"

    const-string v1, " <<<<<<<<<<<< stopVEEngine >>>>>>>>>>  "

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 675
    sget v0, Lcom/samsung/app/video/editor/external/NativeInterface;->playExportHandle:I

    invoke-direct {p0, v0}, Lcom/samsung/app/video/editor/external/NativeInterface;->stopVeEngine(I)V

    .line 676
    const-string v0, "LIBRARY"

    const-string v1, " <<<<<<<<<<<< AFTER stopVEEngine >>>>>>>>>>  "

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 677
    monitor-exit p0

    return-void

    .line 674
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized terminate()V
    .locals 2

    .prologue
    .line 429
    monitor-enter p0

    :try_start_0
    const-string v0, "LIBRARY"

    const-string v1, " <<<<<<<<<<<< terminatePreviewPlayer >>>>>>>>>>  "

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 430
    invoke-direct {p0}, Lcom/samsung/app/video/editor/external/NativeInterface;->terminatePreviewPlayer()V

    .line 431
    const-string v0, "LIBRARY"

    const-string v1, " <<<<<<<<<<<< AFTER terminatePreviewPlayer >>>>>>>>>>  "

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 432
    monitor-exit p0

    return-void

    .line 429
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized updatePackageforEngine(Ljava/lang/String;)V
    .locals 2
    .param p1, "packagePath"    # Ljava/lang/String;

    .prologue
    .line 1005
    monitor-enter p0

    :try_start_0
    const-string v0, "LIBRARY"

    const-string v1, " <<<<<<<<<<<< UpdatePackagePath >>>>>>>>>>  "

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 1006
    invoke-direct {p0, p1}, Lcom/samsung/app/video/editor/external/NativeInterface;->updatePackagePath(Ljava/lang/String;)V

    .line 1007
    const-string v0, "LIBRARY"

    const-string v1, " <<<<<<<<<<<< AFTER UpdatePackagePath >>>>>>>>>>  "

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1008
    monitor-exit p0

    return-void

    .line 1005
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized waveformEngineDeinit(I)I
    .locals 4
    .param p1, "handle"    # I

    .prologue
    .line 514
    monitor-enter p0

    :try_start_0
    const-string v1, "LIBRARY"

    const-string v2, " <<<<<<<<<<<< waveformDeinit >>>>>>>>>>  "

    invoke-static {v1, v2}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 515
    invoke-direct {p0, p1}, Lcom/samsung/app/video/editor/external/NativeInterface;->waveformDeinit(I)I

    move-result v0

    .line 516
    .local v0, "res":I
    const-string v1, "LIBRARY"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, " <<<<<<<<<<<< AFTER waveformDeinit >>>>>>>>>>  "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 518
    monitor-exit p0

    return v0

    .line 514
    .end local v0    # "res":I
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public final declared-synchronized waveformEngineInit(Ljava/lang/String;)I
    .locals 4
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 490
    monitor-enter p0

    :try_start_0
    const-string v1, "LIBRARY"

    const-string v2, " <<<<<<<<<<<< waveformInit >>>>>>>>>>  "

    invoke-static {v1, v2}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 491
    invoke-direct {p0, p1}, Lcom/samsung/app/video/editor/external/NativeInterface;->waveformInit(Ljava/lang/String;)I

    move-result v0

    .line 492
    .local v0, "res":I
    const-string v1, "LIBRARY"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, " <<<<<<<<<<<< AFTER waveformInit >>>>>>>>>>  "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 494
    monitor-exit p0

    return v0

    .line 490
    .end local v0    # "res":I
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public final declared-synchronized waveformEngineProcess(II)I
    .locals 2
    .param p1, "handle"    # I
    .param p2, "frameIndex"    # I

    .prologue
    .line 523
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1, p2}, Lcom/samsung/app/video/editor/external/NativeInterface;->waveformProcess(II)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 524
    .local v0, "res":I
    monitor-exit p0

    return v0

    .line 523
    .end local v0    # "res":I
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public final declared-synchronized waveformEngineSampleCount(I)I
    .locals 4
    .param p1, "handle"    # I

    .prologue
    .line 498
    monitor-enter p0

    :try_start_0
    const-string v1, "LIBRARY"

    const-string v2, " <<<<<<<<<<<< waveformEngineSampleCount >>>>>>>>>>  "

    invoke-static {v1, v2}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 499
    invoke-direct {p0, p1}, Lcom/samsung/app/video/editor/external/NativeInterface;->waveformSampleCount(I)I

    move-result v0

    .line 500
    .local v0, "res":I
    const-string v1, "LIBRARY"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, " <<<<<<<<<<<< AFTER waveformEngineSampleCount >>>>>>>>>>  "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 502
    monitor-exit p0

    return v0

    .line 498
    .end local v0    # "res":I
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public final declared-synchronized waveformEngineSetFrequency(II)I
    .locals 4
    .param p1, "handle"    # I
    .param p2, "freq"    # I

    .prologue
    .line 506
    monitor-enter p0

    :try_start_0
    const-string v1, "LIBRARY"

    const-string v2, " <<<<<<<<<<<< waveformSetFrequency >>>>>>>>>>  "

    invoke-static {v1, v2}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 507
    invoke-direct {p0, p1, p2}, Lcom/samsung/app/video/editor/external/NativeInterface;->waveformSetFrequency(II)I

    move-result v0

    .line 508
    .local v0, "res":I
    const-string v1, "LIBRARY"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, " <<<<<<<<<<<< AFTER waveformSetFrequency >>>>>>>>>>  "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 510
    monitor-exit p0

    return v0

    .line 506
    .end local v0    # "res":I
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method
