.class public Lcom/samsung/app/video/editor/external/TranscodeElement$ExportedTranscodeElement;
.super Ljava/lang/Object;
.source "TranscodeElement.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/app/video/editor/external/TranscodeElement;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ExportedTranscodeElement"
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x5d1988080e81b25bL


# instance fields
.field private exportedTE:Lcom/samsung/app/video/editor/external/TranscodeElement;

.field private exportedfilepath:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2933
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getexportedTE()Lcom/samsung/app/video/editor/external/TranscodeElement;
    .locals 1

    .prologue
    .line 2940
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement$ExportedTranscodeElement;->exportedTE:Lcom/samsung/app/video/editor/external/TranscodeElement;

    return-object v0
.end method

.method public getfilepath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2948
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement$ExportedTranscodeElement;->exportedfilepath:Ljava/lang/String;

    return-object v0
.end method

.method public setexportedTE(Lcom/samsung/app/video/editor/external/TranscodeElement;)V
    .locals 0
    .param p1, "exportedTranscode"    # Lcom/samsung/app/video/editor/external/TranscodeElement;

    .prologue
    .line 2944
    iput-object p1, p0, Lcom/samsung/app/video/editor/external/TranscodeElement$ExportedTranscodeElement;->exportedTE:Lcom/samsung/app/video/editor/external/TranscodeElement;

    .line 2945
    return-void
.end method

.method public setfilePath(Ljava/lang/String;)V
    .locals 0
    .param p1, "exportedFilePath"    # Ljava/lang/String;

    .prologue
    .line 2952
    iput-object p1, p0, Lcom/samsung/app/video/editor/external/TranscodeElement$ExportedTranscodeElement;->exportedfilepath:Ljava/lang/String;

    .line 2953
    return-void
.end method
