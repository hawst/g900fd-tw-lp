.class public Lcom/samsung/app/video/editor/external/Constants;
.super Ljava/lang/Object;
.source "Constants.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/app/video/editor/external/Constants$AUDIO_TRACKS;,
        Lcom/samsung/app/video/editor/external/Constants$AudioVideoStrip;,
        Lcom/samsung/app/video/editor/external/Constants$AutoEdit;,
        Lcom/samsung/app/video/editor/external/Constants$CaptionTextAlignment;,
        Lcom/samsung/app/video/editor/external/Constants$CaptionThemes;,
        Lcom/samsung/app/video/editor/external/Constants$ElementType;,
        Lcom/samsung/app/video/editor/external/Constants$ImageFormat;,
        Lcom/samsung/app/video/editor/external/Constants$ImageSize;,
        Lcom/samsung/app/video/editor/external/Constants$OperationMaintype;,
        Lcom/samsung/app/video/editor/external/Constants$OperationSubType;,
        Lcom/samsung/app/video/editor/external/Constants$PreviewType;,
        Lcom/samsung/app/video/editor/external/Constants$Priority;,
        Lcom/samsung/app/video/editor/external/Constants$ROTATION_DEGREES;,
        Lcom/samsung/app/video/editor/external/Constants$SLOW_MOTION_SPEED;,
        Lcom/samsung/app/video/editor/external/Constants$SplitReplace;,
        Lcom/samsung/app/video/editor/external/Constants$ThumbnailIcons;,
        Lcom/samsung/app/video/editor/external/Constants$VIDEO_RECORD_MODE;,
        Lcom/samsung/app/video/editor/external/Constants$VTError;
    }
.end annotation


# static fields
.field public static final ACTION_CLEAR_COVER_OPEN:Ljava/lang/String; = "com.samsung.cover.OPEN"

.field public static AUTOEDIT_MODE_AUTO:I = 0x0

.field public static final AUTO_MAKING_LIMIT:I = 0xb4

.field public static D1_BITRATE:I = 0x0

.field public static DIALOG_TAG:Ljava/lang/String; = null

.field public static final DURATION:Ljava/lang/String; = "duration"

.field public static final EDIT_MODE_THUMB_COUNT:I = 0x6

.field public static final END_POS:Ljava/lang/String; = "end_pos"

.field public static final EXPORT_DURATION_30SECS:I = 0x1e

.field public static final EXPORT_DURATION_60SECS:I = 0x3c

.field public static EXPORT_RESOLUTIONS:[[I = null

.field public static final FILE_PATH:Ljava/lang/String; = "file_path"

.field public static final GREATER_THAN_900:I = 0x4

.field public static HD_BITRATE:I = 0x0

.field public static final KENBURN_ALERT_REQD:Ljava/lang/String; = "KENBURN_ALERT_REQD"

.field public static KENBURN_EFFECTS_COUNT:I = 0x0

.field public static final LESS_THAN_15:I = 0x0

.field public static final LESS_THAN_180:I = 0x2

.field public static final LESS_THAN_60:I = 0x1

.field public static final LESS_THAN_900:I = 0x3

.field public static final MODE_NORMAL:I = 0x0

.field public static final MODE_TRIM:I = 0x1

.field public static final NOT_SUPPORTED_BY_ENGINE:I = 0x0

.field public static ROTATE_LEFT:I = 0x0

.field public static ROTATE_RIGHT:I = 0x0

.field public static final SPEN_FILE_EXTENSION:Ljava/lang/String; = ".ves"

.field public static final START_POS:Ljava/lang/String; = "start_pos"

.field public static final SUPPORTED_BY_ENGINE:I = 0x1

.field public static THUMBNAIL_HALF_IMAGE_SIZE:I = 0x0

.field public static THUMBNAIL_IMAGE_HEIGHT:I = 0x0

.field public static THUMBNAIL_IMAGE_INTERNAL_PADDING:I = 0x0

.field public static THUMBNAIL_IMAGE_SIZE:I = 0x0

.field public static VGA_BITRATE:I = 0x0

.field public static final VOLUME:Ljava/lang/String; = "volume"

.field public static clipLengths:[F

.field public static final mThemeNamePath:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x0

    const/4 v5, 0x5

    const/4 v4, 0x1

    const/4 v3, 0x2

    .line 9
    const/16 v0, 0x32

    sput v0, Lcom/samsung/app/video/editor/external/Constants;->AUTOEDIT_MODE_AUTO:I

    .line 10
    const-string v0, "dialog"

    sput-object v0, Lcom/samsung/app/video/editor/external/Constants;->DIALOG_TAG:Ljava/lang/String;

    .line 11
    const/16 v0, 0xa

    sput v0, Lcom/samsung/app/video/editor/external/Constants;->KENBURN_EFFECTS_COUNT:I

    .line 12
    const/16 v0, 0xb0

    sput v0, Lcom/samsung/app/video/editor/external/Constants;->THUMBNAIL_IMAGE_SIZE:I

    .line 13
    sput v3, Lcom/samsung/app/video/editor/external/Constants;->THUMBNAIL_IMAGE_INTERNAL_PADDING:I

    .line 14
    const/16 v0, 0x63

    sput v0, Lcom/samsung/app/video/editor/external/Constants;->THUMBNAIL_IMAGE_HEIGHT:I

    .line 15
    sget v0, Lcom/samsung/app/video/editor/external/Constants;->THUMBNAIL_IMAGE_SIZE:I

    div-int/lit8 v0, v0, 0x2

    sput v0, Lcom/samsung/app/video/editor/external/Constants;->THUMBNAIL_HALF_IMAGE_SIZE:I

    .line 20
    const/16 v0, 0x1f40

    sput v0, Lcom/samsung/app/video/editor/external/Constants;->HD_BITRATE:I

    .line 21
    const/16 v0, 0x5dc

    sput v0, Lcom/samsung/app/video/editor/external/Constants;->D1_BITRATE:I

    .line 22
    const/16 v0, 0x4b0

    sput v0, Lcom/samsung/app/video/editor/external/Constants;->VGA_BITRATE:I

    .line 24
    sput v4, Lcom/samsung/app/video/editor/external/Constants;->ROTATE_LEFT:I

    .line 25
    sput v3, Lcom/samsung/app/video/editor/external/Constants;->ROTATE_RIGHT:I

    .line 121
    const/16 v0, 0x8

    new-array v0, v0, [[I

    .line 122
    new-array v1, v3, [I

    fill-array-data v1, :array_0

    aput-object v1, v0, v6

    .line 123
    new-array v1, v3, [I

    fill-array-data v1, :array_1

    aput-object v1, v0, v4

    .line 124
    new-array v1, v3, [I

    fill-array-data v1, :array_2

    aput-object v1, v0, v3

    .line 125
    new-array v1, v3, [I

    fill-array-data v1, :array_3

    aput-object v1, v0, v7

    const/4 v1, 0x4

    .line 126
    new-array v2, v3, [I

    fill-array-data v2, :array_4

    aput-object v2, v0, v1

    .line 127
    new-array v1, v3, [I

    fill-array-data v1, :array_5

    aput-object v1, v0, v5

    const/4 v1, 0x6

    .line 128
    new-array v2, v3, [I

    fill-array-data v2, :array_6

    aput-object v2, v0, v1

    const/4 v1, 0x7

    .line 129
    new-array v2, v3, [I

    fill-array-data v2, :array_7

    aput-object v2, v0, v1

    .line 121
    sput-object v0, Lcom/samsung/app/video/editor/external/Constants;->EXPORT_RESOLUTIONS:[[I

    .line 431
    new-array v0, v5, [F

    fill-array-data v0, :array_8

    sput-object v0, Lcom/samsung/app/video/editor/external/Constants;->clipLengths:[F

    .line 441
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, ""

    aput-object v1, v0, v6

    const-string v1, "/homevideo_theme.jpg"

    aput-object v1, v0, v4

    .line 442
    const-string v1, "/party_theme.jpg"

    aput-object v1, v0, v3

    const-string v1, "/travel_theme.jpg"

    aput-object v1, v0, v7

    const/4 v1, 0x4

    const-string v2, "/conference_theme.jpg"

    aput-object v2, v0, v1

    .line 443
    const-string v1, "/movie_theme.jpg"

    aput-object v1, v0, v5

    const/4 v1, 0x6

    const-string v2, "/stageperformance_theme.jpg"

    aput-object v2, v0, v1

    .line 441
    sput-object v0, Lcom/samsung/app/video/editor/external/Constants;->mThemeNamePath:[Ljava/lang/String;

    .line 455
    return-void

    .line 122
    nop

    :array_0
    .array-data 4
        0xb0
        0x90
    .end array-data

    .line 123
    :array_1
    .array-data 4
        0x140
        0xf0
    .end array-data

    .line 124
    :array_2
    .array-data 4
        0x280
        0x1e0
    .end array-data

    .line 125
    :array_3
    .array-data 4
        0x2d0
        0x1e0
    .end array-data

    .line 126
    :array_4
    .array-data 4
        0x500
        0x2d0
    .end array-data

    .line 127
    :array_5
    .array-data 4
        0x780
        0x438
    .end array-data

    .line 128
    :array_6
    .array-data 4
        0xf00
        0x870
    .end array-data

    .line 129
    :array_7
    .array-data 4
        0x280
        0x168
    .end array-data

    .line 431
    :array_8
    .array-data 4
        0x3f800000    # 1.0f
        0x3fc00000    # 1.5f
        0x40000000    # 2.0f
        0x40400000    # 3.0f
        0x40800000    # 4.0f
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getTransPosition(I)I
    .locals 1
    .param p0, "type"    # I

    .prologue
    const/4 v0, 0x0

    .line 472
    sparse-switch p0, :sswitch_data_0

    .line 493
    :goto_0
    :sswitch_0
    return v0

    .line 477
    :sswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 479
    :sswitch_2
    const/4 v0, 0x2

    goto :goto_0

    .line 481
    :sswitch_3
    const/4 v0, 0x3

    goto :goto_0

    .line 483
    :sswitch_4
    const/4 v0, 0x4

    goto :goto_0

    .line 485
    :sswitch_5
    const/4 v0, 0x5

    goto :goto_0

    .line 487
    :sswitch_6
    const/4 v0, 0x6

    goto :goto_0

    .line 489
    :sswitch_7
    const/4 v0, 0x7

    goto :goto_0

    .line 491
    :sswitch_8
    const/16 v0, 0x8

    goto :goto_0

    .line 472
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_3
        0x2 -> :sswitch_2
        0x3 -> :sswitch_1
        0x5 -> :sswitch_4
        0x9 -> :sswitch_5
        0x28 -> :sswitch_6
        0x29 -> :sswitch_7
        0x2a -> :sswitch_8
    .end sparse-switch
.end method

.method public static getTransType(I)I
    .locals 1
    .param p0, "position"    # I

    .prologue
    .line 499
    packed-switch p0, :pswitch_data_0

    .line 526
    const/4 v0, -0x1

    :goto_0
    return v0

    .line 502
    :pswitch_0
    const/4 v0, 0x0

    goto :goto_0

    .line 505
    :pswitch_1
    const/4 v0, 0x3

    goto :goto_0

    .line 508
    :pswitch_2
    const/4 v0, 0x2

    goto :goto_0

    .line 511
    :pswitch_3
    const/4 v0, 0x1

    goto :goto_0

    .line 513
    :pswitch_4
    const/4 v0, 0x5

    goto :goto_0

    .line 516
    :pswitch_5
    const/16 v0, 0x9

    goto :goto_0

    .line 519
    :pswitch_6
    const/16 v0, 0x28

    goto :goto_0

    .line 521
    :pswitch_7
    const/16 v0, 0x29

    goto :goto_0

    .line 524
    :pswitch_8
    const/16 v0, 0x2a

    goto :goto_0

    .line 499
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public static getTransitionSubType(I)I
    .locals 2
    .param p0, "themeid"    # I

    .prologue
    const/16 v1, 0x21

    .line 458
    if-eq p0, v1, :cond_0

    .line 459
    const/16 v0, 0x26

    if-eq p0, v0, :cond_0

    .line 460
    const/16 v0, 0x24

    if-eq p0, v0, :cond_0

    .line 461
    const/16 v0, 0x22

    if-eq p0, v0, :cond_0

    .line 462
    const/16 v0, 0x23

    if-eq p0, v0, :cond_0

    .line 463
    if-ne p0, v1, :cond_1

    .line 464
    :cond_0
    const/4 v0, 0x1

    .line 466
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public static getVideoBitRate(I)I
    .locals 1
    .param p0, "enumVideoResolution"    # I

    .prologue
    .line 535
    const/4 v0, 0x0

    .line 536
    .local v0, "bitRate":I
    packed-switch p0, :pswitch_data_0

    .line 561
    :goto_0
    :pswitch_0
    return v0

    .line 538
    :pswitch_1
    const/16 v0, 0x118

    .line 539
    goto :goto_0

    .line 541
    :pswitch_2
    const/16 v0, 0x200

    .line 542
    goto :goto_0

    .line 544
    :pswitch_3
    sget v0, Lcom/samsung/app/video/editor/external/Constants;->VGA_BITRATE:I

    .line 545
    goto :goto_0

    .line 547
    :pswitch_4
    sget v0, Lcom/samsung/app/video/editor/external/Constants;->D1_BITRATE:I

    .line 548
    goto :goto_0

    .line 550
    :pswitch_5
    sget v0, Lcom/samsung/app/video/editor/external/Constants;->HD_BITRATE:I

    .line 551
    goto :goto_0

    .line 553
    :pswitch_6
    const/16 v0, 0x2710

    .line 554
    goto :goto_0

    .line 556
    :pswitch_7
    const/16 v0, 0x4b0

    .line 557
    goto :goto_0

    .line 536
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_7
    .end packed-switch
.end method
