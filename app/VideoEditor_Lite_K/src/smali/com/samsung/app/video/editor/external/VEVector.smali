.class public Lcom/samsung/app/video/editor/external/VEVector;
.super Ljava/util/Vector;
.source "VEVector.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/Vector",
        "<TE;>;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x1L


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    .local p0, "this":Lcom/samsung/app/video/editor/external/VEVector;, "Lcom/samsung/app/video/editor/external/VEVector<TE;>;"
    invoke-direct {p0}, Ljava/util/Vector;-><init>()V

    .line 22
    return-void
.end method

.method public constructor <init>(I)V
    .locals 0
    .param p1, "i"    # I

    .prologue
    .line 25
    .local p0, "this":Lcom/samsung/app/video/editor/external/VEVector;, "Lcom/samsung/app/video/editor/external/VEVector<TE;>;"
    invoke-direct {p0, p1}, Ljava/util/Vector;-><init>(I)V

    .line 26
    return-void
.end method

.method private doDeleteInternally(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)V"
        }
    .end annotation

    .prologue
    .line 29
    .local p0, "this":Lcom/samsung/app/video/editor/external/VEVector;, "Lcom/samsung/app/video/editor/external/VEVector<TE;>;"
    .local p1, "object":Ljava/lang/Object;, "TE;"
    instance-of v0, p1, Lcom/samsung/app/video/editor/external/TranscodeElement;

    if-eqz v0, :cond_1

    .line 31
    check-cast p1, Lcom/samsung/app/video/editor/external/TranscodeElement;

    .end local p1    # "object":Ljava/lang/Object;, "TE;"
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->deleteAllClipArts(Z)V

    .line 35
    :cond_0
    :goto_0
    return-void

    .line 32
    .restart local p1    # "object":Ljava/lang/Object;, "TE;"
    :cond_1
    instance-of v0, p1, Lcom/samsung/app/video/editor/external/Element;

    if-eqz v0, :cond_0

    .line 33
    check-cast p1, Lcom/samsung/app/video/editor/external/Element;

    .end local p1    # "object":Ljava/lang/Object;, "TE;"
    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/Element;->deleteAllClipArts()V

    goto :goto_0
.end method

.method private doDeleteInternallyForObject(Ljava/lang/Object;)V
    .locals 1
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    .line 38
    .local p0, "this":Lcom/samsung/app/video/editor/external/VEVector;, "Lcom/samsung/app/video/editor/external/VEVector<TE;>;"
    instance-of v0, p1, Lcom/samsung/app/video/editor/external/TranscodeElement;

    if-eqz v0, :cond_1

    .line 40
    check-cast p1, Lcom/samsung/app/video/editor/external/TranscodeElement;

    .end local p1    # "object":Ljava/lang/Object;
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->deleteAllClipArts(Z)V

    .line 44
    :cond_0
    :goto_0
    return-void

    .line 41
    .restart local p1    # "object":Ljava/lang/Object;
    :cond_1
    instance-of v0, p1, Lcom/samsung/app/video/editor/external/Element;

    if-eqz v0, :cond_0

    .line 42
    check-cast p1, Lcom/samsung/app/video/editor/external/Element;

    .end local p1    # "object":Ljava/lang/Object;
    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/Element;->deleteAllClipArts()V

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized remove(I)Ljava/lang/Object;
    .locals 2
    .param p1, "location"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TE;"
        }
    .end annotation

    .prologue
    .line 48
    .local p0, "this":Lcom/samsung/app/video/editor/external/VEVector;, "Lcom/samsung/app/video/editor/external/VEVector<TE;>;"
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/samsung/app/video/editor/external/VEVector;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 49
    .local v0, "object":Ljava/lang/Object;, "TE;"
    invoke-direct {p0, v0}, Lcom/samsung/app/video/editor/external/VEVector;->doDeleteInternally(Ljava/lang/Object;)V

    .line 51
    invoke-super {p0, p1}, Ljava/util/Vector;->remove(I)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    monitor-exit p0

    return-object v1

    .line 48
    .end local v0    # "object":Ljava/lang/Object;, "TE;"
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized removeAll(Ljava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 56
    .local p0, "this":Lcom/samsung/app/video/editor/external/VEVector;, "Lcom/samsung/app/video/editor/external/VEVector<TE;>;"
    .local p1, "collection":Ljava/util/Collection;, "Ljava/util/Collection<*>;"
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Ljava/util/Vector;->removeAll(Ljava/util/Collection;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized removeElement(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    .line 61
    .local p0, "this":Lcom/samsung/app/video/editor/external/VEVector;, "Lcom/samsung/app/video/editor/external/VEVector<TE;>;"
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/samsung/app/video/editor/external/VEVector;->doDeleteInternallyForObject(Ljava/lang/Object;)V

    .line 63
    invoke-super {p0, p1}, Ljava/util/Vector;->removeElement(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    .line 61
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
