.class public Lcom/samsung/app/video/editor/external/BitmapAnimationData;
.super Ljava/lang/Object;
.source "BitmapAnimationData.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = -0x72d6586701816664L


# instance fields
.field public bitmap_animation_end_frame:I

.field public bitmap_animation_start_frame:I

.field public currentIndexInTemplate:I

.field public endIndexInTemplate:I

.field private mAssetResource:Z

.field private mOriginalEndFrameWithinElement:I

.field private mOriginalStartFrameWithinElement:I

.field public pngTemplateFileName:Ljava/lang/String;

.field public startIndexInTemplate:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/app/video/editor/external/BitmapAnimationData;->mAssetResource:Z

    .line 29
    return-void
.end method

.method public constructor <init>(Lcom/samsung/app/video/editor/external/BitmapAnimationData;)V
    .locals 1
    .param p1, "bitmapAnimationsData"    # Lcom/samsung/app/video/editor/external/BitmapAnimationData;

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/app/video/editor/external/BitmapAnimationData;->mAssetResource:Z

    .line 32
    iget v0, p1, Lcom/samsung/app/video/editor/external/BitmapAnimationData;->bitmap_animation_start_frame:I

    iput v0, p0, Lcom/samsung/app/video/editor/external/BitmapAnimationData;->bitmap_animation_start_frame:I

    .line 33
    iget v0, p1, Lcom/samsung/app/video/editor/external/BitmapAnimationData;->bitmap_animation_end_frame:I

    iput v0, p0, Lcom/samsung/app/video/editor/external/BitmapAnimationData;->bitmap_animation_end_frame:I

    .line 34
    iget-object v0, p1, Lcom/samsung/app/video/editor/external/BitmapAnimationData;->pngTemplateFileName:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/app/video/editor/external/BitmapAnimationData;->pngTemplateFileName:Ljava/lang/String;

    .line 35
    iget v0, p1, Lcom/samsung/app/video/editor/external/BitmapAnimationData;->startIndexInTemplate:I

    iput v0, p0, Lcom/samsung/app/video/editor/external/BitmapAnimationData;->startIndexInTemplate:I

    .line 36
    iget v0, p1, Lcom/samsung/app/video/editor/external/BitmapAnimationData;->endIndexInTemplate:I

    iput v0, p0, Lcom/samsung/app/video/editor/external/BitmapAnimationData;->endIndexInTemplate:I

    .line 37
    iget-boolean v0, p1, Lcom/samsung/app/video/editor/external/BitmapAnimationData;->mAssetResource:Z

    iput-boolean v0, p0, Lcom/samsung/app/video/editor/external/BitmapAnimationData;->mAssetResource:Z

    .line 38
    iget v0, p1, Lcom/samsung/app/video/editor/external/BitmapAnimationData;->mOriginalStartFrameWithinElement:I

    iput v0, p0, Lcom/samsung/app/video/editor/external/BitmapAnimationData;->mOriginalStartFrameWithinElement:I

    .line 39
    iget v0, p1, Lcom/samsung/app/video/editor/external/BitmapAnimationData;->mOriginalEndFrameWithinElement:I

    iput v0, p0, Lcom/samsung/app/video/editor/external/BitmapAnimationData;->mOriginalEndFrameWithinElement:I

    .line 40
    return-void
.end method


# virtual methods
.method public getEndFrameWithinElement()I
    .locals 1

    .prologue
    .line 55
    iget v0, p0, Lcom/samsung/app/video/editor/external/BitmapAnimationData;->bitmap_animation_end_frame:I

    return v0
.end method

.method public getOriginalEndFrameWithinElement()I
    .locals 1

    .prologue
    .line 71
    iget v0, p0, Lcom/samsung/app/video/editor/external/BitmapAnimationData;->mOriginalEndFrameWithinElement:I

    return v0
.end method

.method public getOriginalStartFrameWithinElement()I
    .locals 1

    .prologue
    .line 63
    iget v0, p0, Lcom/samsung/app/video/editor/external/BitmapAnimationData;->mOriginalStartFrameWithinElement:I

    return v0
.end method

.method public getStartFrameWithinElement()I
    .locals 1

    .prologue
    .line 47
    iget v0, p0, Lcom/samsung/app/video/editor/external/BitmapAnimationData;->bitmap_animation_start_frame:I

    return v0
.end method

.method public isAssetResource()Z
    .locals 1

    .prologue
    .line 91
    iget-boolean v0, p0, Lcom/samsung/app/video/editor/external/BitmapAnimationData;->mAssetResource:Z

    return v0
.end method

.method public setAssetResource(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 99
    iput-boolean p1, p0, Lcom/samsung/app/video/editor/external/BitmapAnimationData;->mAssetResource:Z

    .line 100
    return-void
.end method

.method public setEndFrameWithinElement(I)V
    .locals 0
    .param p1, "frame"    # I

    .prologue
    .line 51
    iput p1, p0, Lcom/samsung/app/video/editor/external/BitmapAnimationData;->bitmap_animation_end_frame:I

    .line 52
    return-void
.end method

.method public setEndIndexInTemplate(I)V
    .locals 0
    .param p1, "endFrame"    # I

    .prologue
    .line 83
    iput p1, p0, Lcom/samsung/app/video/editor/external/BitmapAnimationData;->endIndexInTemplate:I

    .line 84
    return-void
.end method

.method public setOriginalEndFrameWithinElement(I)V
    .locals 0
    .param p1, "frame"    # I

    .prologue
    .line 67
    iput p1, p0, Lcom/samsung/app/video/editor/external/BitmapAnimationData;->mOriginalEndFrameWithinElement:I

    .line 68
    return-void
.end method

.method public setOriginalStartFrameWithinElement(I)V
    .locals 0
    .param p1, "frame"    # I

    .prologue
    .line 59
    iput p1, p0, Lcom/samsung/app/video/editor/external/BitmapAnimationData;->mOriginalStartFrameWithinElement:I

    .line 60
    return-void
.end method

.method public setPNGTemplate(Ljava/lang/String;)V
    .locals 0
    .param p1, "template"    # Ljava/lang/String;

    .prologue
    .line 75
    iput-object p1, p0, Lcom/samsung/app/video/editor/external/BitmapAnimationData;->pngTemplateFileName:Ljava/lang/String;

    .line 76
    return-void
.end method

.method public setStartFrameWithinElement(I)V
    .locals 0
    .param p1, "frame"    # I

    .prologue
    .line 43
    iput p1, p0, Lcom/samsung/app/video/editor/external/BitmapAnimationData;->bitmap_animation_start_frame:I

    .line 44
    return-void
.end method

.method public setStartIndexInTemplate(I)V
    .locals 0
    .param p1, "startFrame"    # I

    .prologue
    .line 79
    iput p1, p0, Lcom/samsung/app/video/editor/external/BitmapAnimationData;->startIndexInTemplate:I

    .line 80
    return-void
.end method
