.class public Lcom/samsung/app/video/editor/external/Constants$ROTATION_DEGREES;
.super Ljava/lang/Object;
.source "Constants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/app/video/editor/external/Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ROTATION_DEGREES"
.end annotation


# static fields
.field public static NINETY:I

.field public static ONE_EIGHTY:I

.field public static TWO_SEVENTY:I

.field public static ZERO:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 62
    const/4 v0, 0x0

    sput v0, Lcom/samsung/app/video/editor/external/Constants$ROTATION_DEGREES;->ZERO:I

    .line 63
    const/16 v0, 0x5a

    sput v0, Lcom/samsung/app/video/editor/external/Constants$ROTATION_DEGREES;->NINETY:I

    .line 64
    const/16 v0, 0xb4

    sput v0, Lcom/samsung/app/video/editor/external/Constants$ROTATION_DEGREES;->ONE_EIGHTY:I

    .line 65
    const/16 v0, 0x10e

    sput v0, Lcom/samsung/app/video/editor/external/Constants$ROTATION_DEGREES;->TWO_SEVENTY:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
