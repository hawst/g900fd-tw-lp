.class public Lcom/samsung/app/video/editor/external/Element;
.super Ljava/lang/Object;
.source "Element.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static final NON_TIME_LOSS_TRANSITION_PERIOD:I = 0x1f4

.field private static mDrawingListComparator:Lcom/samsung/app/video/editor/external/TranscodeElement$DrawingListComparator; = null

.field private static final serialVersionUID:J = -0x4d17b9cc982558b3L


# instance fields
.field public ID:I

.field public Video_Template:I

.field public alphaImageFileName:Ljava/lang/String;

.field public alphaImageFileName_q:Ljava/lang/String;

.field public audFilePath1:Ljava/lang/String;

.field public audFilePath2:Ljava/lang/String;

.field public audVidStripConstant:I

.field public audVidType:I

.field private audioDisplayName:Ljava/lang/String;

.field public autoEdited:Z

.field private bitmapAnimationsDataList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/BitmapAnimationData;",
            ">;"
        }
    .end annotation
.end field

.field private clipartList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/ClipartParams;",
            ">;"
        }
    .end annotation
.end field

.field private clipartListDrawing:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/ClipartParams;",
            ">;"
        }
    .end annotation
.end field

.field private duration:I

.field public editList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/Edit;",
            ">;"
        }
    .end annotation
.end field

.field public elementScale:F

.field private endTime:I

.field private end_frame_x1:I

.field private end_frame_x2:I

.field private end_frame_y1:I

.field private end_frame_y2:I

.field private end_values:[F

.field public filePath:Ljava/lang/String;

.field private groupDuration:J

.field private groupEndTime:J

.field private groupID:I

.field private groupStartTime:J

.field public isEndElement:Z

.field public isMergeFullFile:Z

.field public isOverLapPossible:Z

.field public isStartElement:Z

.field private isUHD:Z

.field private isWQHD:Z

.field private mAssetResource:Z

.field private nonTimeLossTransitionAtEnd:Z

.field private nonTimeLossTransitionAtStart:Z

.field private orientation:I

.field private overLapAtEnd:I

.field private overLapAtStart:I

.field private ratioInGroup:F

.field private recordSuccess:Z

.field private recordingMode:I

.field public ref_frame_x1:I

.field public ref_frame_x2:I

.field public ref_frame_y1:I

.field public ref_frame_y2:I

.field public speed:I

.field public splitReplaceSeq:I

.field private splitTime:F

.field private startTime:I

.field private start_frame_x1:I

.field private start_frame_x2:I

.field private start_frame_y1:I

.field private start_frame_y2:I

.field private start_values:[F

.field private storyboardEndTime:I

.field private storyboardStartTime:I

.field public subType:I

.field public templateFileName:Ljava/lang/String;

.field public templateFileName_q:Ljava/lang/String;

.field public type:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    new-instance v0, Lcom/samsung/app/video/editor/external/TranscodeElement$DrawingListComparator;

    invoke-direct {v0}, Lcom/samsung/app/video/editor/external/TranscodeElement$DrawingListComparator;-><init>()V

    sput-object v0, Lcom/samsung/app/video/editor/external/Element;->mDrawingListComparator:Lcom/samsung/app/video/editor/external/TranscodeElement$DrawingListComparator;

    .line 27
    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/16 v3, 0x1770

    const/16 v2, 0x9

    const/4 v1, 0x0

    .line 174
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput v1, p0, Lcom/samsung/app/video/editor/external/Element;->overLapAtStart:I

    .line 32
    iput v1, p0, Lcom/samsung/app/video/editor/external/Element;->overLapAtEnd:I

    .line 51
    iput-boolean v1, p0, Lcom/samsung/app/video/editor/external/Element;->nonTimeLossTransitionAtStart:Z

    .line 53
    iput-boolean v1, p0, Lcom/samsung/app/video/editor/external/Element;->nonTimeLossTransitionAtEnd:Z

    .line 56
    iput v1, p0, Lcom/samsung/app/video/editor/external/Element;->ID:I

    .line 57
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/app/video/editor/external/Element;->isOverLapPossible:Z

    .line 61
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/app/video/editor/external/Element;->alphaImageFileName:Ljava/lang/String;

    .line 62
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/app/video/editor/external/Element;->alphaImageFileName_q:Ljava/lang/String;

    .line 76
    const/4 v0, 0x3

    iput v0, p0, Lcom/samsung/app/video/editor/external/Element;->audVidType:I

    .line 78
    iput-boolean v1, p0, Lcom/samsung/app/video/editor/external/Element;->autoEdited:Z

    .line 84
    iput v3, p0, Lcom/samsung/app/video/editor/external/Element;->duration:I

    .line 89
    const v0, 0x426aaaab

    iput v0, p0, Lcom/samsung/app/video/editor/external/Element;->elementScale:F

    .line 92
    iput v1, p0, Lcom/samsung/app/video/editor/external/Element;->end_frame_x1:I

    .line 95
    sget v0, Lcom/sec/android/app/ve/common/ConfigUtils;->KENBURN_WIDTH:I

    iput v0, p0, Lcom/samsung/app/video/editor/external/Element;->end_frame_x2:I

    .line 97
    iput v1, p0, Lcom/samsung/app/video/editor/external/Element;->end_frame_y1:I

    .line 99
    sget v0, Lcom/sec/android/app/ve/common/ConfigUtils;->KENBURN_HEIGHT:I

    iput v0, p0, Lcom/samsung/app/video/editor/external/Element;->end_frame_y2:I

    .line 100
    new-array v0, v2, [F

    fill-array-data v0, :array_0

    .line 101
    iput-object v0, p0, Lcom/samsung/app/video/editor/external/Element;->end_values:[F

    .line 104
    iput v3, p0, Lcom/samsung/app/video/editor/external/Element;->endTime:I

    .line 110
    iput v1, p0, Lcom/samsung/app/video/editor/external/Element;->groupID:I

    .line 117
    iput-boolean v1, p0, Lcom/samsung/app/video/editor/external/Element;->isMergeFullFile:Z

    .line 120
    sget v0, Lcom/samsung/app/video/editor/external/Constants$SLOW_MOTION_SPEED;->SPEED_NORMAL:I

    iput v0, p0, Lcom/samsung/app/video/editor/external/Element;->speed:I

    .line 121
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/samsung/app/video/editor/external/Element;->ratioInGroup:F

    .line 122
    iput v1, p0, Lcom/samsung/app/video/editor/external/Element;->ref_frame_x1:I

    .line 123
    sget v0, Lcom/sec/android/app/ve/common/ConfigUtils;->KENBURN_WIDTH:I

    iput v0, p0, Lcom/samsung/app/video/editor/external/Element;->ref_frame_x2:I

    .line 124
    iput v1, p0, Lcom/samsung/app/video/editor/external/Element;->ref_frame_y1:I

    .line 125
    sget v0, Lcom/sec/android/app/ve/common/ConfigUtils;->KENBURN_HEIGHT:I

    iput v0, p0, Lcom/samsung/app/video/editor/external/Element;->ref_frame_y2:I

    .line 134
    iput v1, p0, Lcom/samsung/app/video/editor/external/Element;->start_frame_x1:I

    .line 137
    sget v0, Lcom/sec/android/app/ve/common/ConfigUtils;->KENBURN_WIDTH:I

    iput v0, p0, Lcom/samsung/app/video/editor/external/Element;->start_frame_x2:I

    .line 139
    iput v1, p0, Lcom/samsung/app/video/editor/external/Element;->start_frame_y1:I

    .line 141
    sget v0, Lcom/sec/android/app/ve/common/ConfigUtils;->KENBURN_HEIGHT:I

    iput v0, p0, Lcom/samsung/app/video/editor/external/Element;->start_frame_y2:I

    .line 142
    new-array v0, v2, [F

    fill-array-data v0, :array_1

    .line 143
    iput-object v0, p0, Lcom/samsung/app/video/editor/external/Element;->start_values:[F

    .line 154
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/app/video/editor/external/Element;->templateFileName:Ljava/lang/String;

    .line 155
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/app/video/editor/external/Element;->templateFileName_q:Ljava/lang/String;

    .line 160
    sget v0, Lcom/samsung/app/video/editor/external/Constants$VIDEO_RECORD_MODE;->NORMAL:I

    iput v0, p0, Lcom/samsung/app/video/editor/external/Element;->recordingMode:I

    .line 164
    iput-boolean v1, p0, Lcom/samsung/app/video/editor/external/Element;->recordSuccess:Z

    .line 169
    iput-boolean v1, p0, Lcom/samsung/app/video/editor/external/Element;->mAssetResource:Z

    .line 170
    iput-boolean v1, p0, Lcom/samsung/app/video/editor/external/Element;->isUHD:Z

    .line 171
    iput-boolean v1, p0, Lcom/samsung/app/video/editor/external/Element;->isWQHD:Z

    .line 173
    iput v1, p0, Lcom/samsung/app/video/editor/external/Element;->orientation:I

    .line 175
    return-void

    .line 100
    :array_0
    .array-data 4
        0x3f800008    # 1.000001f
        0x0
        0x0
        0x0
        0x3f800008    # 1.000001f
        0x0
        0x0
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 142
    :array_1
    .array-data 4
        0x3f800008    # 1.000001f
        0x0
        0x0
        0x0
        0x3f800008    # 1.000001f
        0x0
        0x0
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public constructor <init>(Lcom/samsung/app/video/editor/external/Element;)V
    .locals 6
    .param p1, "element"    # Lcom/samsung/app/video/editor/external/Element;

    .prologue
    const/16 v5, 0x1770

    const/16 v4, 0x9

    const/4 v3, 0x0

    .line 177
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput v3, p0, Lcom/samsung/app/video/editor/external/Element;->overLapAtStart:I

    .line 32
    iput v3, p0, Lcom/samsung/app/video/editor/external/Element;->overLapAtEnd:I

    .line 51
    iput-boolean v3, p0, Lcom/samsung/app/video/editor/external/Element;->nonTimeLossTransitionAtStart:Z

    .line 53
    iput-boolean v3, p0, Lcom/samsung/app/video/editor/external/Element;->nonTimeLossTransitionAtEnd:Z

    .line 56
    iput v3, p0, Lcom/samsung/app/video/editor/external/Element;->ID:I

    .line 57
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/app/video/editor/external/Element;->isOverLapPossible:Z

    .line 61
    const-string v2, ""

    iput-object v2, p0, Lcom/samsung/app/video/editor/external/Element;->alphaImageFileName:Ljava/lang/String;

    .line 62
    const-string v2, ""

    iput-object v2, p0, Lcom/samsung/app/video/editor/external/Element;->alphaImageFileName_q:Ljava/lang/String;

    .line 76
    const/4 v2, 0x3

    iput v2, p0, Lcom/samsung/app/video/editor/external/Element;->audVidType:I

    .line 78
    iput-boolean v3, p0, Lcom/samsung/app/video/editor/external/Element;->autoEdited:Z

    .line 84
    iput v5, p0, Lcom/samsung/app/video/editor/external/Element;->duration:I

    .line 89
    const v2, 0x426aaaab

    iput v2, p0, Lcom/samsung/app/video/editor/external/Element;->elementScale:F

    .line 92
    iput v3, p0, Lcom/samsung/app/video/editor/external/Element;->end_frame_x1:I

    .line 95
    sget v2, Lcom/sec/android/app/ve/common/ConfigUtils;->KENBURN_WIDTH:I

    iput v2, p0, Lcom/samsung/app/video/editor/external/Element;->end_frame_x2:I

    .line 97
    iput v3, p0, Lcom/samsung/app/video/editor/external/Element;->end_frame_y1:I

    .line 99
    sget v2, Lcom/sec/android/app/ve/common/ConfigUtils;->KENBURN_HEIGHT:I

    iput v2, p0, Lcom/samsung/app/video/editor/external/Element;->end_frame_y2:I

    .line 100
    new-array v2, v4, [F

    fill-array-data v2, :array_0

    .line 101
    iput-object v2, p0, Lcom/samsung/app/video/editor/external/Element;->end_values:[F

    .line 104
    iput v5, p0, Lcom/samsung/app/video/editor/external/Element;->endTime:I

    .line 110
    iput v3, p0, Lcom/samsung/app/video/editor/external/Element;->groupID:I

    .line 117
    iput-boolean v3, p0, Lcom/samsung/app/video/editor/external/Element;->isMergeFullFile:Z

    .line 120
    sget v2, Lcom/samsung/app/video/editor/external/Constants$SLOW_MOTION_SPEED;->SPEED_NORMAL:I

    iput v2, p0, Lcom/samsung/app/video/editor/external/Element;->speed:I

    .line 121
    const/high16 v2, 0x3f800000    # 1.0f

    iput v2, p0, Lcom/samsung/app/video/editor/external/Element;->ratioInGroup:F

    .line 122
    iput v3, p0, Lcom/samsung/app/video/editor/external/Element;->ref_frame_x1:I

    .line 123
    sget v2, Lcom/sec/android/app/ve/common/ConfigUtils;->KENBURN_WIDTH:I

    iput v2, p0, Lcom/samsung/app/video/editor/external/Element;->ref_frame_x2:I

    .line 124
    iput v3, p0, Lcom/samsung/app/video/editor/external/Element;->ref_frame_y1:I

    .line 125
    sget v2, Lcom/sec/android/app/ve/common/ConfigUtils;->KENBURN_HEIGHT:I

    iput v2, p0, Lcom/samsung/app/video/editor/external/Element;->ref_frame_y2:I

    .line 134
    iput v3, p0, Lcom/samsung/app/video/editor/external/Element;->start_frame_x1:I

    .line 137
    sget v2, Lcom/sec/android/app/ve/common/ConfigUtils;->KENBURN_WIDTH:I

    iput v2, p0, Lcom/samsung/app/video/editor/external/Element;->start_frame_x2:I

    .line 139
    iput v3, p0, Lcom/samsung/app/video/editor/external/Element;->start_frame_y1:I

    .line 141
    sget v2, Lcom/sec/android/app/ve/common/ConfigUtils;->KENBURN_HEIGHT:I

    iput v2, p0, Lcom/samsung/app/video/editor/external/Element;->start_frame_y2:I

    .line 142
    new-array v2, v4, [F

    fill-array-data v2, :array_1

    .line 143
    iput-object v2, p0, Lcom/samsung/app/video/editor/external/Element;->start_values:[F

    .line 154
    const-string v2, ""

    iput-object v2, p0, Lcom/samsung/app/video/editor/external/Element;->templateFileName:Ljava/lang/String;

    .line 155
    const-string v2, ""

    iput-object v2, p0, Lcom/samsung/app/video/editor/external/Element;->templateFileName_q:Ljava/lang/String;

    .line 160
    sget v2, Lcom/samsung/app/video/editor/external/Constants$VIDEO_RECORD_MODE;->NORMAL:I

    iput v2, p0, Lcom/samsung/app/video/editor/external/Element;->recordingMode:I

    .line 164
    iput-boolean v3, p0, Lcom/samsung/app/video/editor/external/Element;->recordSuccess:Z

    .line 169
    iput-boolean v3, p0, Lcom/samsung/app/video/editor/external/Element;->mAssetResource:Z

    .line 170
    iput-boolean v3, p0, Lcom/samsung/app/video/editor/external/Element;->isUHD:Z

    .line 171
    iput-boolean v3, p0, Lcom/samsung/app/video/editor/external/Element;->isWQHD:Z

    .line 173
    iput v3, p0, Lcom/samsung/app/video/editor/external/Element;->orientation:I

    .line 178
    iget-boolean v2, p1, Lcom/samsung/app/video/editor/external/Element;->isOverLapPossible:Z

    iput-boolean v2, p0, Lcom/samsung/app/video/editor/external/Element;->isOverLapPossible:Z

    .line 179
    iget v2, p1, Lcom/samsung/app/video/editor/external/Element;->overLapAtStart:I

    iput v2, p0, Lcom/samsung/app/video/editor/external/Element;->overLapAtStart:I

    .line 180
    iget v2, p1, Lcom/samsung/app/video/editor/external/Element;->overLapAtEnd:I

    iput v2, p0, Lcom/samsung/app/video/editor/external/Element;->overLapAtEnd:I

    .line 181
    iget-boolean v2, p1, Lcom/samsung/app/video/editor/external/Element;->nonTimeLossTransitionAtStart:Z

    iput-boolean v2, p0, Lcom/samsung/app/video/editor/external/Element;->nonTimeLossTransitionAtStart:Z

    .line 182
    iget-boolean v2, p1, Lcom/samsung/app/video/editor/external/Element;->nonTimeLossTransitionAtEnd:Z

    iput-boolean v2, p0, Lcom/samsung/app/video/editor/external/Element;->nonTimeLossTransitionAtEnd:Z

    .line 183
    iget-object v2, p1, Lcom/samsung/app/video/editor/external/Element;->audFilePath1:Ljava/lang/String;

    iput-object v2, p0, Lcom/samsung/app/video/editor/external/Element;->audFilePath1:Ljava/lang/String;

    .line 184
    iget-object v2, p1, Lcom/samsung/app/video/editor/external/Element;->audFilePath2:Ljava/lang/String;

    iput-object v2, p0, Lcom/samsung/app/video/editor/external/Element;->audFilePath2:Ljava/lang/String;

    .line 185
    iget-object v2, p1, Lcom/samsung/app/video/editor/external/Element;->audioDisplayName:Ljava/lang/String;

    iput-object v2, p0, Lcom/samsung/app/video/editor/external/Element;->audioDisplayName:Ljava/lang/String;

    .line 186
    iget v2, p1, Lcom/samsung/app/video/editor/external/Element;->audVidStripConstant:I

    iput v2, p0, Lcom/samsung/app/video/editor/external/Element;->audVidStripConstant:I

    .line 187
    iget v2, p1, Lcom/samsung/app/video/editor/external/Element;->speed:I

    iput v2, p0, Lcom/samsung/app/video/editor/external/Element;->speed:I

    .line 188
    iget v2, p1, Lcom/samsung/app/video/editor/external/Element;->recordingMode:I

    iput v2, p0, Lcom/samsung/app/video/editor/external/Element;->recordingMode:I

    .line 189
    iget v2, p1, Lcom/samsung/app/video/editor/external/Element;->orientation:I

    iput v2, p0, Lcom/samsung/app/video/editor/external/Element;->orientation:I

    .line 190
    iget v2, p1, Lcom/samsung/app/video/editor/external/Element;->duration:I

    iput v2, p0, Lcom/samsung/app/video/editor/external/Element;->duration:I

    .line 191
    iget v2, p1, Lcom/samsung/app/video/editor/external/Element;->endTime:I

    iput v2, p0, Lcom/samsung/app/video/editor/external/Element;->endTime:I

    .line 192
    iget-object v2, p1, Lcom/samsung/app/video/editor/external/Element;->filePath:Ljava/lang/String;

    iput-object v2, p0, Lcom/samsung/app/video/editor/external/Element;->filePath:Ljava/lang/String;

    .line 193
    iget-boolean v2, p1, Lcom/samsung/app/video/editor/external/Element;->isMergeFullFile:Z

    iput-boolean v2, p0, Lcom/samsung/app/video/editor/external/Element;->isMergeFullFile:Z

    .line 194
    iget v2, p1, Lcom/samsung/app/video/editor/external/Element;->splitReplaceSeq:I

    iput v2, p0, Lcom/samsung/app/video/editor/external/Element;->splitReplaceSeq:I

    .line 195
    iget v2, p1, Lcom/samsung/app/video/editor/external/Element;->startTime:I

    iput v2, p0, Lcom/samsung/app/video/editor/external/Element;->startTime:I

    .line 196
    iget v2, p1, Lcom/samsung/app/video/editor/external/Element;->type:I

    iput v2, p0, Lcom/samsung/app/video/editor/external/Element;->type:I

    .line 197
    iget v2, p1, Lcom/samsung/app/video/editor/external/Element;->splitTime:F

    iput v2, p0, Lcom/samsung/app/video/editor/external/Element;->splitTime:F

    .line 198
    iget v2, p1, Lcom/samsung/app/video/editor/external/Element;->audVidType:I

    iput v2, p0, Lcom/samsung/app/video/editor/external/Element;->audVidType:I

    .line 199
    iget-object v2, p1, Lcom/samsung/app/video/editor/external/Element;->templateFileName:Ljava/lang/String;

    iput-object v2, p0, Lcom/samsung/app/video/editor/external/Element;->templateFileName:Ljava/lang/String;

    .line 200
    iget-object v2, p1, Lcom/samsung/app/video/editor/external/Element;->alphaImageFileName:Ljava/lang/String;

    iput-object v2, p0, Lcom/samsung/app/video/editor/external/Element;->alphaImageFileName:Ljava/lang/String;

    .line 201
    iget v2, p1, Lcom/samsung/app/video/editor/external/Element;->Video_Template:I

    iput v2, p0, Lcom/samsung/app/video/editor/external/Element;->Video_Template:I

    .line 202
    iget-object v2, p1, Lcom/samsung/app/video/editor/external/Element;->alphaImageFileName_q:Ljava/lang/String;

    iput-object v2, p0, Lcom/samsung/app/video/editor/external/Element;->alphaImageFileName_q:Ljava/lang/String;

    .line 203
    iget-object v2, p1, Lcom/samsung/app/video/editor/external/Element;->templateFileName_q:Ljava/lang/String;

    iput-object v2, p0, Lcom/samsung/app/video/editor/external/Element;->templateFileName_q:Ljava/lang/String;

    .line 205
    iget v2, p1, Lcom/samsung/app/video/editor/external/Element;->start_frame_x1:I

    iput v2, p0, Lcom/samsung/app/video/editor/external/Element;->start_frame_x1:I

    .line 206
    iget v2, p1, Lcom/samsung/app/video/editor/external/Element;->start_frame_y1:I

    iput v2, p0, Lcom/samsung/app/video/editor/external/Element;->start_frame_y1:I

    .line 207
    iget v2, p1, Lcom/samsung/app/video/editor/external/Element;->start_frame_x2:I

    iput v2, p0, Lcom/samsung/app/video/editor/external/Element;->start_frame_x2:I

    .line 208
    iget v2, p1, Lcom/samsung/app/video/editor/external/Element;->start_frame_y2:I

    iput v2, p0, Lcom/samsung/app/video/editor/external/Element;->start_frame_y2:I

    .line 210
    iget v2, p1, Lcom/samsung/app/video/editor/external/Element;->end_frame_x1:I

    iput v2, p0, Lcom/samsung/app/video/editor/external/Element;->end_frame_x1:I

    .line 211
    iget v2, p1, Lcom/samsung/app/video/editor/external/Element;->end_frame_y1:I

    iput v2, p0, Lcom/samsung/app/video/editor/external/Element;->end_frame_y1:I

    .line 212
    iget v2, p1, Lcom/samsung/app/video/editor/external/Element;->end_frame_x2:I

    iput v2, p0, Lcom/samsung/app/video/editor/external/Element;->end_frame_x2:I

    .line 213
    iget v2, p1, Lcom/samsung/app/video/editor/external/Element;->end_frame_y2:I

    iput v2, p0, Lcom/samsung/app/video/editor/external/Element;->end_frame_y2:I

    .line 215
    iget v2, p1, Lcom/samsung/app/video/editor/external/Element;->ref_frame_x1:I

    iput v2, p0, Lcom/samsung/app/video/editor/external/Element;->ref_frame_x1:I

    .line 216
    iget v2, p1, Lcom/samsung/app/video/editor/external/Element;->ref_frame_y1:I

    iput v2, p0, Lcom/samsung/app/video/editor/external/Element;->ref_frame_y1:I

    .line 217
    iget v2, p1, Lcom/samsung/app/video/editor/external/Element;->ref_frame_x2:I

    iput v2, p0, Lcom/samsung/app/video/editor/external/Element;->ref_frame_x2:I

    .line 218
    iget v2, p1, Lcom/samsung/app/video/editor/external/Element;->ref_frame_y2:I

    iput v2, p0, Lcom/samsung/app/video/editor/external/Element;->ref_frame_y2:I

    .line 220
    iget-object v2, p1, Lcom/samsung/app/video/editor/external/Element;->start_values:[F

    iput-object v2, p0, Lcom/samsung/app/video/editor/external/Element;->start_values:[F

    .line 221
    iget-object v2, p1, Lcom/samsung/app/video/editor/external/Element;->end_values:[F

    iput-object v2, p0, Lcom/samsung/app/video/editor/external/Element;->end_values:[F

    .line 223
    iget v2, p1, Lcom/samsung/app/video/editor/external/Element;->storyboardStartTime:I

    iput v2, p0, Lcom/samsung/app/video/editor/external/Element;->storyboardStartTime:I

    .line 224
    iget v2, p1, Lcom/samsung/app/video/editor/external/Element;->storyboardEndTime:I

    iput v2, p0, Lcom/samsung/app/video/editor/external/Element;->storyboardEndTime:I

    .line 226
    new-instance v2, Ljava/util/Vector;

    invoke-direct {v2}, Ljava/util/Vector;-><init>()V

    iput-object v2, p0, Lcom/samsung/app/video/editor/external/Element;->editList:Ljava/util/List;

    .line 227
    iget v2, p1, Lcom/samsung/app/video/editor/external/Element;->ID:I

    iput v2, p0, Lcom/samsung/app/video/editor/external/Element;->ID:I

    .line 228
    iget v2, p1, Lcom/samsung/app/video/editor/external/Element;->groupID:I

    iput v2, p0, Lcom/samsung/app/video/editor/external/Element;->groupID:I

    .line 229
    iget-wide v2, p1, Lcom/samsung/app/video/editor/external/Element;->groupStartTime:J

    iput-wide v2, p0, Lcom/samsung/app/video/editor/external/Element;->groupStartTime:J

    .line 230
    iget-wide v2, p1, Lcom/samsung/app/video/editor/external/Element;->groupEndTime:J

    iput-wide v2, p0, Lcom/samsung/app/video/editor/external/Element;->groupEndTime:J

    .line 231
    iget-wide v2, p1, Lcom/samsung/app/video/editor/external/Element;->groupDuration:J

    iput-wide v2, p0, Lcom/samsung/app/video/editor/external/Element;->groupDuration:J

    .line 232
    iget v2, p1, Lcom/samsung/app/video/editor/external/Element;->ratioInGroup:F

    iput v2, p0, Lcom/samsung/app/video/editor/external/Element;->ratioInGroup:F

    .line 233
    iget-boolean v2, p1, Lcom/samsung/app/video/editor/external/Element;->autoEdited:Z

    iput-boolean v2, p0, Lcom/samsung/app/video/editor/external/Element;->autoEdited:Z

    .line 234
    iget-boolean v2, p1, Lcom/samsung/app/video/editor/external/Element;->recordSuccess:Z

    iput-boolean v2, p0, Lcom/samsung/app/video/editor/external/Element;->recordSuccess:Z

    .line 235
    iget v2, p1, Lcom/samsung/app/video/editor/external/Element;->subType:I

    iput v2, p0, Lcom/samsung/app/video/editor/external/Element;->subType:I

    .line 236
    iget-boolean v2, p1, Lcom/samsung/app/video/editor/external/Element;->mAssetResource:Z

    iput-boolean v2, p0, Lcom/samsung/app/video/editor/external/Element;->mAssetResource:Z

    .line 237
    iget-boolean v2, p1, Lcom/samsung/app/video/editor/external/Element;->isUHD:Z

    iput-boolean v2, p0, Lcom/samsung/app/video/editor/external/Element;->isUHD:Z

    .line 238
    iget-boolean v2, p1, Lcom/samsung/app/video/editor/external/Element;->isWQHD:Z

    iput-boolean v2, p0, Lcom/samsung/app/video/editor/external/Element;->isWQHD:Z

    .line 239
    monitor-enter p0

    .line 240
    :try_start_0
    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/Element;->getEditListSize()I

    move-result v0

    .line 241
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v0, :cond_0

    .line 239
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 245
    new-instance v2, Ljava/util/Vector;

    invoke-direct {v2}, Ljava/util/Vector;-><init>()V

    iput-object v2, p0, Lcom/samsung/app/video/editor/external/Element;->clipartList:Ljava/util/List;

    .line 246
    monitor-enter p0

    .line 247
    :try_start_1
    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/Element;->getClipartListCount()I

    move-result v0

    .line 248
    const/4 v1, 0x0

    :goto_1
    if-lt v1, v0, :cond_1

    .line 246
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 253
    new-instance v2, Ljava/util/Vector;

    invoke-direct {v2}, Ljava/util/Vector;-><init>()V

    iput-object v2, p0, Lcom/samsung/app/video/editor/external/Element;->bitmapAnimationsDataList:Ljava/util/List;

    .line 254
    monitor-enter p0

    .line 255
    :try_start_2
    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/Element;->getBitmapAnimationsDataListCount()I

    move-result v0

    .line 256
    const/4 v1, 0x0

    :goto_2
    if-lt v1, v0, :cond_2

    .line 254
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 261
    new-instance v2, Ljava/util/Vector;

    invoke-direct {v2}, Ljava/util/Vector;-><init>()V

    iput-object v2, p0, Lcom/samsung/app/video/editor/external/Element;->clipartListDrawing:Ljava/util/List;

    .line 262
    monitor-enter p0

    .line 263
    :try_start_3
    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/Element;->getDrawingEleListCount()I

    move-result v0

    .line 264
    const/4 v1, 0x0

    :goto_3
    if-lt v1, v0, :cond_3

    .line 262
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 271
    return-void

    .line 242
    :cond_0
    :try_start_4
    iget-object v3, p0, Lcom/samsung/app/video/editor/external/Element;->editList:Ljava/util/List;

    new-instance v4, Lcom/samsung/app/video/editor/external/Edit;

    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/Element;->getEditList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/app/video/editor/external/Edit;

    invoke-direct {v4, v2}, Lcom/samsung/app/video/editor/external/Edit;-><init>(Lcom/samsung/app/video/editor/external/Edit;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 241
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 239
    .end local v0    # "count":I
    .end local v1    # "i":I
    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v2

    .line 249
    .restart local v0    # "count":I
    .restart local v1    # "i":I
    :cond_1
    :try_start_5
    iget-object v3, p0, Lcom/samsung/app/video/editor/external/Element;->clipartList:Ljava/util/List;

    new-instance v4, Lcom/samsung/app/video/editor/external/ClipartParams;

    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/Element;->getClipartList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/app/video/editor/external/ClipartParams;

    invoke-direct {v4, v2}, Lcom/samsung/app/video/editor/external/ClipartParams;-><init>(Lcom/samsung/app/video/editor/external/ClipartParams;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 248
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 246
    :catchall_1
    move-exception v2

    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v2

    .line 257
    :cond_2
    :try_start_6
    iget-object v3, p0, Lcom/samsung/app/video/editor/external/Element;->bitmapAnimationsDataList:Ljava/util/List;

    new-instance v4, Lcom/samsung/app/video/editor/external/BitmapAnimationData;

    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/Element;->getBitmapAnimationsDataList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/app/video/editor/external/BitmapAnimationData;

    invoke-direct {v4, v2}, Lcom/samsung/app/video/editor/external/BitmapAnimationData;-><init>(Lcom/samsung/app/video/editor/external/BitmapAnimationData;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 256
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 254
    :catchall_2
    move-exception v2

    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    throw v2

    .line 265
    :cond_3
    :try_start_7
    iget-object v3, p0, Lcom/samsung/app/video/editor/external/Element;->clipartListDrawing:Ljava/util/List;

    new-instance v4, Lcom/samsung/app/video/editor/external/ClipartParams;

    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/Element;->getDrawingEleList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/app/video/editor/external/ClipartParams;

    invoke-direct {v4, v2}, Lcom/samsung/app/video/editor/external/ClipartParams;-><init>(Lcom/samsung/app/video/editor/external/ClipartParams;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 264
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 262
    :catchall_3
    move-exception v2

    monitor-exit p0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    throw v2

    .line 100
    :array_0
    .array-data 4
        0x3f800008    # 1.000001f
        0x0
        0x0
        0x0
        0x3f800008    # 1.000001f
        0x0
        0x0
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 142
    :array_1
    .array-data 4
        0x3f800008    # 1.000001f
        0x0
        0x0
        0x0
        0x3f800008    # 1.000001f
        0x0
        0x0
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private adjustClipartList()V
    .locals 10

    .prologue
    .line 409
    iget-object v8, p0, Lcom/samsung/app/video/editor/external/Element;->clipartList:Ljava/util/List;

    if-eqz v8, :cond_0

    .line 410
    const-wide/16 v6, 0x0

    .line 411
    .local v6, "preendTime":J
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/Element;->getClipartListCount()I

    move-result v1

    .line 412
    .local v1, "count":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-lt v4, v1, :cond_1

    .line 425
    .end local v1    # "count":I
    .end local v4    # "i":I
    .end local v6    # "preendTime":J
    :cond_0
    return-void

    .line 413
    .restart local v1    # "count":I
    .restart local v4    # "i":I
    .restart local v6    # "preendTime":J
    :cond_1
    iget-object v8, p0, Lcom/samsung/app/video/editor/external/Element;->clipartList:Ljava/util/List;

    invoke-interface {v8, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/app/video/editor/external/ClipartParams;

    .line 414
    .local v0, "clipParams":Lcom/samsung/app/video/editor/external/ClipartParams;
    invoke-virtual {v0}, Lcom/samsung/app/video/editor/external/ClipartParams;->getStartTime()J

    move-result-wide v8

    cmp-long v8, v6, v8

    if-lez v8, :cond_2

    if-lez v4, :cond_2

    .line 415
    iget-object v8, p0, Lcom/samsung/app/video/editor/external/Element;->clipartList:Ljava/util/List;

    add-int/lit8 v9, v4, -0x1

    invoke-interface {v8, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/app/video/editor/external/ClipartParams;

    .line 417
    .local v5, "prevClipParams":Lcom/samsung/app/video/editor/external/ClipartParams;
    invoke-virtual {v0}, Lcom/samsung/app/video/editor/external/ClipartParams;->getStartTime()J

    move-result-wide v8

    .line 416
    sub-long v2, v6, v8

    .line 418
    .local v2, "currentTimeDiffTime":J
    invoke-virtual {v5}, Lcom/samsung/app/video/editor/external/ClipartParams;->getEndTime()J

    move-result-wide v8

    .line 419
    sub-long/2addr v8, v2

    .line 418
    invoke-virtual {v5, v8, v9}, Lcom/samsung/app/video/editor/external/ClipartParams;->setEndTime(J)V

    .line 422
    .end local v2    # "currentTimeDiffTime":J
    .end local v5    # "prevClipParams":Lcom/samsung/app/video/editor/external/ClipartParams;
    :cond_2
    invoke-virtual {v0}, Lcom/samsung/app/video/editor/external/ClipartParams;->getEndTime()J

    move-result-wide v6

    .line 412
    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method

.method private adjustDrawingEleList()V
    .locals 10

    .prologue
    .line 390
    iget-object v8, p0, Lcom/samsung/app/video/editor/external/Element;->clipartListDrawing:Ljava/util/List;

    if-eqz v8, :cond_0

    .line 391
    const-wide/16 v6, 0x0

    .line 392
    .local v6, "preendTime":J
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/Element;->getDrawingEleListCount()I

    move-result v1

    .line 393
    .local v1, "count":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-lt v4, v1, :cond_1

    .line 405
    .end local v1    # "count":I
    .end local v4    # "i":I
    .end local v6    # "preendTime":J
    :cond_0
    return-void

    .line 394
    .restart local v1    # "count":I
    .restart local v4    # "i":I
    .restart local v6    # "preendTime":J
    :cond_1
    iget-object v8, p0, Lcom/samsung/app/video/editor/external/Element;->clipartListDrawing:Ljava/util/List;

    invoke-interface {v8, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/app/video/editor/external/ClipartParams;

    .line 395
    .local v0, "clipParams":Lcom/samsung/app/video/editor/external/ClipartParams;
    invoke-virtual {v0}, Lcom/samsung/app/video/editor/external/ClipartParams;->getStartTime()J

    move-result-wide v8

    cmp-long v8, v6, v8

    if-lez v8, :cond_2

    if-lez v4, :cond_2

    .line 396
    iget-object v8, p0, Lcom/samsung/app/video/editor/external/Element;->clipartListDrawing:Ljava/util/List;

    add-int/lit8 v9, v4, -0x1

    invoke-interface {v8, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/app/video/editor/external/ClipartParams;

    .line 397
    .local v5, "prevClipParams":Lcom/samsung/app/video/editor/external/ClipartParams;
    invoke-virtual {v0}, Lcom/samsung/app/video/editor/external/ClipartParams;->getStartTime()J

    move-result-wide v8

    sub-long v2, v6, v8

    .line 398
    .local v2, "currentTimeDiffTime":J
    invoke-virtual {v5}, Lcom/samsung/app/video/editor/external/ClipartParams;->getEndTime()J

    move-result-wide v8

    .line 399
    sub-long/2addr v8, v2

    .line 398
    invoke-virtual {v5, v8, v9}, Lcom/samsung/app/video/editor/external/ClipartParams;->setEndTime(J)V

    .line 402
    .end local v2    # "currentTimeDiffTime":J
    .end local v5    # "prevClipParams":Lcom/samsung/app/video/editor/external/ClipartParams;
    :cond_2
    invoke-virtual {v0}, Lcom/samsung/app/video/editor/external/ClipartParams;->getEndTime()J

    move-result-wide v6

    .line 393
    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method

.method private moveKenburnsEditToLast()V
    .locals 6

    .prologue
    const/16 v5, 0x27

    .line 358
    const/4 v1, 0x0

    .line 359
    .local v1, "tempEdit":Lcom/samsung/app/video/editor/external/Edit;
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/Element;->editList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_2

    .line 371
    :goto_0
    if-eqz v1, :cond_1

    .line 372
    invoke-virtual {p0, v1}, Lcom/samsung/app/video/editor/external/Element;->addKenburnsEdit(Lcom/samsung/app/video/editor/external/Edit;)V

    .line 373
    :cond_1
    return-void

    .line 359
    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/app/video/editor/external/Edit;

    .line 360
    .local v0, "edit":Lcom/samsung/app/video/editor/external/Edit;
    invoke-virtual {v0}, Lcom/samsung/app/video/editor/external/Edit;->getType()I

    move-result v3

    const/4 v4, 0x4

    if-ne v3, v4, :cond_0

    .line 361
    invoke-virtual {v0}, Lcom/samsung/app/video/editor/external/Edit;->getSubType()I

    move-result v3

    if-ne v3, v5, :cond_1

    .line 363
    invoke-virtual {v0}, Lcom/samsung/app/video/editor/external/Edit;->getSubType()I

    move-result v3

    if-ne v3, v5, :cond_0

    .line 364
    new-instance v1, Lcom/samsung/app/video/editor/external/Edit;

    .end local v1    # "tempEdit":Lcom/samsung/app/video/editor/external/Edit;
    invoke-direct {v1, v0}, Lcom/samsung/app/video/editor/external/Edit;-><init>(Lcom/samsung/app/video/editor/external/Edit;)V

    .line 365
    .restart local v1    # "tempEdit":Lcom/samsung/app/video/editor/external/Edit;
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/Element;->editList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method


# virtual methods
.method public addBitmapAnimationsData(Lcom/samsung/app/video/editor/external/BitmapAnimationData;)V
    .locals 1
    .param p1, "bitmapAnimations"    # Lcom/samsung/app/video/editor/external/BitmapAnimationData;

    .prologue
    .line 1079
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/Element;->bitmapAnimationsDataList:Ljava/util/List;

    if-nez v0, :cond_0

    .line 1080
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/samsung/app/video/editor/external/Element;->bitmapAnimationsDataList:Ljava/util/List;

    .line 1083
    :cond_0
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/Element;->bitmapAnimationsDataList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1084
    return-void
.end method

.method public addClipartList(Lcom/samsung/app/video/editor/external/ClipartParams;)V
    .locals 2
    .param p1, "clipParams"    # Lcom/samsung/app/video/editor/external/ClipartParams;

    .prologue
    .line 310
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/Element;->clipartList:Ljava/util/List;

    if-nez v0, :cond_0

    .line 311
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/samsung/app/video/editor/external/Element;->clipartList:Ljava/util/List;

    .line 312
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/Element;->clipartList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 316
    :goto_0
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/Element;->clipartList:Ljava/util/List;

    sget-object v1, Lcom/samsung/app/video/editor/external/Element;->mDrawingListComparator:Lcom/samsung/app/video/editor/external/TranscodeElement$DrawingListComparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 317
    invoke-direct {p0}, Lcom/samsung/app/video/editor/external/Element;->adjustDrawingEleList()V

    .line 318
    return-void

    .line 314
    :cond_0
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/Element;->clipartList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public addDrawingEleList(Lcom/samsung/app/video/editor/external/ClipartParams;)V
    .locals 2
    .param p1, "clipParams"    # Lcom/samsung/app/video/editor/external/ClipartParams;

    .prologue
    .line 298
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/Element;->clipartListDrawing:Ljava/util/List;

    if-nez v0, :cond_0

    .line 299
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/samsung/app/video/editor/external/Element;->clipartListDrawing:Ljava/util/List;

    .line 300
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/Element;->clipartListDrawing:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 304
    :goto_0
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/Element;->clipartListDrawing:Ljava/util/List;

    sget-object v1, Lcom/samsung/app/video/editor/external/Element;->mDrawingListComparator:Lcom/samsung/app/video/editor/external/TranscodeElement$DrawingListComparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 305
    invoke-direct {p0}, Lcom/samsung/app/video/editor/external/Element;->adjustClipartList()V

    .line 306
    return-void

    .line 302
    :cond_0
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/Element;->clipartListDrawing:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public addEdit(Lcom/samsung/app/video/editor/external/Edit;)V
    .locals 1
    .param p1, "edit"    # Lcom/samsung/app/video/editor/external/Edit;

    .prologue
    .line 321
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/samsung/app/video/editor/external/Element;->addEdit(Lcom/samsung/app/video/editor/external/Edit;Z)V

    .line 322
    return-void
.end method

.method public addEdit(Lcom/samsung/app/video/editor/external/Edit;Z)V
    .locals 5
    .param p1, "edit"    # Lcom/samsung/app/video/editor/external/Edit;
    .param p2, "uniqueEffect"    # Z

    .prologue
    .line 325
    if-nez p1, :cond_1

    .line 355
    :cond_0
    :goto_0
    return-void

    .line 329
    :cond_1
    iget-object v3, p0, Lcom/samsung/app/video/editor/external/Element;->editList:Ljava/util/List;

    if-nez v3, :cond_2

    .line 330
    new-instance v3, Ljava/util/Vector;

    invoke-direct {v3}, Ljava/util/Vector;-><init>()V

    iput-object v3, p0, Lcom/samsung/app/video/editor/external/Element;->editList:Ljava/util/List;

    .line 339
    :cond_2
    if-eqz p2, :cond_3

    .line 340
    iget-object v3, p0, Lcom/samsung/app/video/editor/external/Element;->editList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v1

    .line 341
    .local v1, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-lt v2, v1, :cond_4

    .line 351
    .end local v1    # "count":I
    .end local v2    # "i":I
    :cond_3
    :goto_2
    iget-object v3, p0, Lcom/samsung/app/video/editor/external/Element;->editList:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 352
    iget v3, p0, Lcom/samsung/app/video/editor/external/Element;->type:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_0

    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getJNIVersion()F

    move-result v3

    const/high16 v4, 0x40000000    # 2.0f

    cmpl-float v3, v3, v4

    if-ltz v3, :cond_0

    .line 353
    invoke-direct {p0}, Lcom/samsung/app/video/editor/external/Element;->moveKenburnsEditToLast()V

    goto :goto_0

    .line 342
    .restart local v1    # "count":I
    .restart local v2    # "i":I
    :cond_4
    iget-object v3, p0, Lcom/samsung/app/video/editor/external/Element;->editList:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/app/video/editor/external/Edit;

    .line 344
    .local v0, "cedit":Lcom/samsung/app/video/editor/external/Edit;
    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/Edit;->getType()I

    move-result v3

    invoke-virtual {v0}, Lcom/samsung/app/video/editor/external/Edit;->getType()I

    move-result v4

    if-ne v3, v4, :cond_5

    .line 345
    iget-object v3, p0, Lcom/samsung/app/video/editor/external/Element;->editList:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto :goto_2

    .line 341
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method public addKenburnsEdit(Lcom/samsung/app/video/editor/external/Edit;)V
    .locals 4
    .param p1, "kenBurnsEdit"    # Lcom/samsung/app/video/editor/external/Edit;

    .prologue
    .line 376
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/Element;->editList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 378
    .local v1, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/app/video/editor/external/Edit;>;"
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 385
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/Element;->editList:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 387
    return-void

    .line 379
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/app/video/editor/external/Edit;

    .line 381
    .local v0, "edit":Lcom/samsung/app/video/editor/external/Edit;
    invoke-virtual {v0}, Lcom/samsung/app/video/editor/external/Edit;->getType()I

    move-result v2

    const/4 v3, 0x4

    if-ne v2, v3, :cond_0

    .line 382
    invoke-virtual {v0}, Lcom/samsung/app/video/editor/external/Edit;->getSubType()I

    move-result v2

    const/16 v3, 0x27

    if-ne v2, v3, :cond_0

    .line 383
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/Element;->editList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public deleteAllClipArts()V
    .locals 4

    .prologue
    .line 428
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/Element;->getClipartListCount()I

    move-result v3

    add-int/lit8 v1, v3, -0x1

    .line 429
    .local v1, "count":I
    move v2, v1

    .local v2, "i":I
    :goto_0
    if-gez v2, :cond_0

    .line 433
    return-void

    .line 430
    :cond_0
    iget-object v3, p0, Lcom/samsung/app/video/editor/external/Element;->clipartList:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/app/video/editor/external/ClipartParams;

    .line 431
    .local v0, "cliparts":Lcom/samsung/app/video/editor/external/ClipartParams;
    invoke-virtual {v0}, Lcom/samsung/app/video/editor/external/ClipartParams;->remove()V

    .line 429
    add-int/lit8 v2, v2, -0x1

    goto :goto_0
.end method

.method public deleteAllDrawings()V
    .locals 4

    .prologue
    .line 436
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/Element;->getDrawingEleListCount()I

    move-result v3

    add-int/lit8 v1, v3, -0x1

    .line 437
    .local v1, "count":I
    move v2, v1

    .local v2, "i":I
    :goto_0
    if-gez v2, :cond_0

    .line 441
    return-void

    .line 438
    :cond_0
    iget-object v3, p0, Lcom/samsung/app/video/editor/external/Element;->clipartListDrawing:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/app/video/editor/external/ClipartParams;

    .line 439
    .local v0, "cliparts":Lcom/samsung/app/video/editor/external/ClipartParams;
    invoke-virtual {v0}, Lcom/samsung/app/video/editor/external/ClipartParams;->remove()V

    .line 437
    add-int/lit8 v2, v2, -0x1

    goto :goto_0
.end method

.method public deleteDrawingElement()V
    .locals 2

    .prologue
    .line 1061
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/Element;->clipartListDrawing:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/app/video/editor/external/ClipartParams;

    invoke-virtual {p0, v0}, Lcom/samsung/app/video/editor/external/Element;->removeDrawingEleList(Lcom/samsung/app/video/editor/external/ClipartParams;)V

    .line 1062
    return-void
.end method

.method public getAudFilePath1()Ljava/lang/String;
    .locals 1

    .prologue
    .line 448
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/Element;->audFilePath1:Ljava/lang/String;

    return-object v0
.end method

.method public getAudFilePath2()Ljava/lang/String;
    .locals 1

    .prologue
    .line 455
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/Element;->audFilePath2:Ljava/lang/String;

    return-object v0
.end method

.method public getAudVidStripConstant()I
    .locals 1

    .prologue
    .line 466
    iget v0, p0, Lcom/samsung/app/video/editor/external/Element;->audVidStripConstant:I

    return v0
.end method

.method public getAudioDisplayName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 459
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/Element;->audioDisplayName:Ljava/lang/String;

    return-object v0
.end method

.method public getAudioEffectEdit()Lcom/samsung/app/video/editor/external/Edit;
    .locals 4

    .prologue
    .line 718
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/Element;->editList:Ljava/util/List;

    if-eqz v2, :cond_0

    .line 719
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/Element;->editList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    .line 720
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v0, :cond_1

    .line 727
    .end local v0    # "count":I
    .end local v1    # "i":I
    :cond_0
    const/4 v2, 0x0

    :goto_1
    return-object v2

    .line 721
    .restart local v0    # "count":I
    .restart local v1    # "i":I
    :cond_1
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/Element;->editList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/app/video/editor/external/Edit;

    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/Edit;->getType()I

    move-result v2

    const/16 v3, 0xa

    if-ne v2, v3, :cond_2

    .line 722
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/Element;->editList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/app/video/editor/external/Edit;

    goto :goto_1

    .line 720
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public getBitmapAnimationsDataList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/BitmapAnimationData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1065
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/Element;->bitmapAnimationsDataList:Ljava/util/List;

    if-nez v0, :cond_0

    .line 1066
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/samsung/app/video/editor/external/Element;->bitmapAnimationsDataList:Ljava/util/List;

    .line 1068
    :cond_0
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/Element;->bitmapAnimationsDataList:Ljava/util/List;

    return-object v0
.end method

.method public getBitmapAnimationsDataListCount()I
    .locals 1

    .prologue
    .line 1072
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/Element;->bitmapAnimationsDataList:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 1073
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/Element;->bitmapAnimationsDataList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 1075
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getClipartList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/ClipartParams;",
            ">;"
        }
    .end annotation

    .prologue
    .line 484
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/Element;->clipartList:Ljava/util/List;

    if-nez v0, :cond_0

    .line 485
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/samsung/app/video/editor/external/Element;->clipartList:Ljava/util/List;

    .line 486
    :cond_0
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/Element;->clipartList:Ljava/util/List;

    return-object v0
.end method

.method public getClipartListCount()I
    .locals 1

    .prologue
    .line 490
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/Element;->clipartList:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 491
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/Element;->clipartList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 493
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDrawingEleList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/ClipartParams;",
            ">;"
        }
    .end annotation

    .prologue
    .line 471
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/Element;->clipartListDrawing:Ljava/util/List;

    if-nez v0, :cond_0

    .line 472
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/samsung/app/video/editor/external/Element;->clipartListDrawing:Ljava/util/List;

    .line 473
    :cond_0
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/Element;->clipartListDrawing:Ljava/util/List;

    return-object v0
.end method

.method public getDrawingEleListCount()I
    .locals 1

    .prologue
    .line 477
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/Element;->clipartListDrawing:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 478
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/Element;->clipartListDrawing:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 480
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDuration()J
    .locals 2

    .prologue
    .line 497
    iget v0, p0, Lcom/samsung/app/video/editor/external/Element;->duration:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public getEdit(I)Lcom/samsung/app/video/editor/external/Edit;
    .locals 4
    .param p1, "type"    # I

    .prologue
    .line 501
    iget-object v3, p0, Lcom/samsung/app/video/editor/external/Element;->editList:Ljava/util/List;

    if-eqz v3, :cond_0

    .line 502
    iget-object v3, p0, Lcom/samsung/app/video/editor/external/Element;->editList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    .line 503
    .local v0, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-lt v2, v0, :cond_2

    .line 512
    .end local v0    # "count":I
    .end local v2    # "i":I
    :cond_0
    const/4 v1, 0x0

    :cond_1
    return-object v1

    .line 504
    .restart local v0    # "count":I
    .restart local v2    # "i":I
    :cond_2
    iget-object v3, p0, Lcom/samsung/app/video/editor/external/Element;->editList:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/app/video/editor/external/Edit;

    .line 506
    .local v1, "edit":Lcom/samsung/app/video/editor/external/Edit;
    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/Edit;->getType()I

    move-result v3

    if-eq p1, v3, :cond_1

    .line 503
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public getEditList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/Edit;",
            ">;"
        }
    .end annotation

    .prologue
    .line 519
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/Element;->editList:Ljava/util/List;

    return-object v0
.end method

.method public getEditListSize()I
    .locals 1

    .prologue
    .line 524
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/Element;->editList:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 525
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/Element;->editList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 526
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getElementRatioInGroup()F
    .locals 1

    .prologue
    .line 530
    iget v0, p0, Lcom/samsung/app/video/editor/external/Element;->ratioInGroup:F

    return v0
.end method

.method public getEndRect()Landroid/graphics/RectF;
    .locals 5

    .prologue
    .line 534
    new-instance v0, Landroid/graphics/RectF;

    iget v1, p0, Lcom/samsung/app/video/editor/external/Element;->end_frame_x1:I

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/app/video/editor/external/Element;->end_frame_y1:I

    int-to-float v2, v2

    iget v3, p0, Lcom/samsung/app/video/editor/external/Element;->end_frame_x2:I

    int-to-float v3, v3

    iget v4, p0, Lcom/samsung/app/video/editor/external/Element;->end_frame_y2:I

    int-to-float v4, v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    return-object v0
.end method

.method public getEndTime()J
    .locals 2

    .prologue
    .line 541
    iget v0, p0, Lcom/samsung/app/video/editor/external/Element;->endTime:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public getFadInFadOutEdit()Lcom/samsung/app/video/editor/external/Edit;
    .locals 4

    .prologue
    .line 545
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/Element;->editList:Ljava/util/List;

    if-eqz v2, :cond_0

    .line 546
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/Element;->editList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    .line 547
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v0, :cond_1

    .line 553
    .end local v0    # "count":I
    .end local v1    # "i":I
    :cond_0
    const/4 v2, 0x0

    :goto_1
    return-object v2

    .line 548
    .restart local v0    # "count":I
    .restart local v1    # "i":I
    :cond_1
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/Element;->editList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/app/video/editor/external/Edit;

    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/Edit;->getType()I

    move-result v2

    const/4 v3, 0x7

    if-ne v2, v3, :cond_2

    .line 549
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/Element;->editList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/app/video/editor/external/Edit;

    goto :goto_1

    .line 547
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public getFilePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 561
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/Element;->filePath:Ljava/lang/String;

    return-object v0
.end method

.method public getGroupDuration()J
    .locals 2

    .prologue
    .line 565
    iget-wide v0, p0, Lcom/samsung/app/video/editor/external/Element;->groupDuration:J

    return-wide v0
.end method

.method public getGroupEndTime()J
    .locals 2

    .prologue
    .line 569
    iget-wide v0, p0, Lcom/samsung/app/video/editor/external/Element;->groupEndTime:J

    return-wide v0
.end method

.method public getGroupID()I
    .locals 1

    .prologue
    .line 573
    iget v0, p0, Lcom/samsung/app/video/editor/external/Element;->groupID:I

    return v0
.end method

.method public getGroupStartTime()J
    .locals 2

    .prologue
    .line 581
    iget-wide v0, p0, Lcom/samsung/app/video/editor/external/Element;->groupStartTime:J

    return-wide v0
.end method

.method public getID()I
    .locals 1

    .prologue
    .line 577
    iget v0, p0, Lcom/samsung/app/video/editor/external/Element;->ID:I

    return v0
.end method

.method public getMatrix(Z)Landroid/graphics/Matrix;
    .locals 3
    .param p1, "value"    # Z

    .prologue
    .line 585
    if-eqz p1, :cond_0

    .line 586
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 587
    .local v0, "m":Landroid/graphics/Matrix;
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/Element;->start_values:[F

    invoke-virtual {v0, v2}, Landroid/graphics/Matrix;->setValues([F)V

    move-object v1, v0

    .line 592
    .end local v0    # "m":Landroid/graphics/Matrix;
    .local v1, "m":Ljava/lang/Object;
    :goto_0
    return-object v1

    .line 590
    .end local v1    # "m":Ljava/lang/Object;
    :cond_0
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 591
    .restart local v0    # "m":Landroid/graphics/Matrix;
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/Element;->end_values:[F

    invoke-virtual {v0, v2}, Landroid/graphics/Matrix;->setValues([F)V

    move-object v1, v0

    .line 592
    .restart local v1    # "m":Ljava/lang/Object;
    goto :goto_0
.end method

.method public getOrientation()I
    .locals 1

    .prologue
    .line 286
    iget v0, p0, Lcom/samsung/app/video/editor/external/Element;->orientation:I

    return v0
.end method

.method public getOverLapAtEnd()I
    .locals 1

    .prologue
    .line 1146
    iget-boolean v0, p0, Lcom/samsung/app/video/editor/external/Element;->nonTimeLossTransitionAtEnd:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x1f4

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/samsung/app/video/editor/external/Element;->overLapAtEnd:I

    goto :goto_0
.end method

.method public getOverLapAtStart()I
    .locals 1

    .prologue
    .line 1136
    iget-boolean v0, p0, Lcom/samsung/app/video/editor/external/Element;->nonTimeLossTransitionAtStart:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x1f4

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/samsung/app/video/editor/external/Element;->overLapAtStart:I

    goto :goto_0
.end method

.method public getRecordingMode()I
    .locals 1

    .prologue
    .line 278
    iget v0, p0, Lcom/samsung/app/video/editor/external/Element;->recordingMode:I

    return v0
.end method

.method public getRefRect()Landroid/graphics/RectF;
    .locals 5

    .prologue
    .line 597
    new-instance v0, Landroid/graphics/RectF;

    iget v1, p0, Lcom/samsung/app/video/editor/external/Element;->ref_frame_x1:I

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/app/video/editor/external/Element;->ref_frame_y1:I

    int-to-float v2, v2

    iget v3, p0, Lcom/samsung/app/video/editor/external/Element;->ref_frame_x2:I

    int-to-float v3, v3

    iget v4, p0, Lcom/samsung/app/video/editor/external/Element;->ref_frame_y2:I

    int-to-float v4, v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    return-object v0
.end method

.method public getRetouchEdit()Lcom/samsung/app/video/editor/external/Edit;
    .locals 4

    .prologue
    .line 601
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/Element;->editList:Ljava/util/List;

    if-eqz v2, :cond_0

    .line 602
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/Element;->editList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    .line 603
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v0, :cond_1

    .line 610
    .end local v0    # "count":I
    .end local v1    # "i":I
    :cond_0
    const/4 v2, 0x0

    :goto_1
    return-object v2

    .line 604
    .restart local v0    # "count":I
    .restart local v1    # "i":I
    :cond_1
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/Element;->editList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/app/video/editor/external/Edit;

    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/Edit;->getType()I

    move-result v2

    const/4 v3, 0x4

    if-ne v2, v3, :cond_2

    .line 605
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/Element;->editList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/app/video/editor/external/Edit;

    goto :goto_1

    .line 603
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public getSpeedForElement()I
    .locals 1

    .prologue
    .line 294
    iget v0, p0, Lcom/samsung/app/video/editor/external/Element;->speed:I

    return v0
.end method

.method public getSplitReplaceSeq()J
    .locals 2

    .prologue
    .line 617
    iget v0, p0, Lcom/samsung/app/video/editor/external/Element;->splitReplaceSeq:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public getSplitTime()J
    .locals 2

    .prologue
    .line 623
    iget v0, p0, Lcom/samsung/app/video/editor/external/Element;->splitTime:F

    float-to-long v0, v0

    return-wide v0
.end method

.method public getStartRect()Landroid/graphics/RectF;
    .locals 5

    .prologue
    .line 627
    new-instance v0, Landroid/graphics/RectF;

    iget v1, p0, Lcom/samsung/app/video/editor/external/Element;->start_frame_x1:I

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/app/video/editor/external/Element;->start_frame_y1:I

    int-to-float v2, v2

    iget v3, p0, Lcom/samsung/app/video/editor/external/Element;->start_frame_x2:I

    int-to-float v3, v3

    .line 628
    iget v4, p0, Lcom/samsung/app/video/editor/external/Element;->start_frame_y2:I

    int-to-float v4, v4

    .line 627
    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    return-object v0
.end method

.method public getStartTime()J
    .locals 2

    .prologue
    .line 635
    iget v0, p0, Lcom/samsung/app/video/editor/external/Element;->startTime:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public getStoryBoardEndTime()J
    .locals 2

    .prologue
    .line 642
    iget v0, p0, Lcom/samsung/app/video/editor/external/Element;->storyboardEndTime:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public getStoryBoardStartTime()J
    .locals 2

    .prologue
    .line 649
    iget v0, p0, Lcom/samsung/app/video/editor/external/Element;->storyboardStartTime:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public getSubType()I
    .locals 1

    .prologue
    .line 1018
    iget v0, p0, Lcom/samsung/app/video/editor/external/Element;->subType:I

    return v0
.end method

.method public getTransitionEdit()Lcom/samsung/app/video/editor/external/Edit;
    .locals 4

    .prologue
    .line 653
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/Element;->editList:Ljava/util/List;

    if-eqz v2, :cond_0

    .line 654
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/Element;->editList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    .line 655
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v0, :cond_1

    .line 662
    .end local v0    # "count":I
    .end local v1    # "i":I
    :cond_0
    const/4 v2, 0x0

    :goto_1
    return-object v2

    .line 656
    .restart local v0    # "count":I
    .restart local v1    # "i":I
    :cond_1
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/Element;->editList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/app/video/editor/external/Edit;

    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/Edit;->getType()I

    move-result v2

    const/4 v3, 0x6

    if-ne v2, v3, :cond_2

    .line 657
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/Element;->editList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/app/video/editor/external/Edit;

    goto :goto_1

    .line 655
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public getTransitionTime()I
    .locals 4

    .prologue
    .line 666
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/Element;->editList:Ljava/util/List;

    if-eqz v2, :cond_0

    .line 667
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/Element;->editList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    .line 668
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v0, :cond_1

    .line 675
    .end local v0    # "count":I
    .end local v1    # "i":I
    :cond_0
    const/4 v2, 0x0

    :goto_1
    return v2

    .line 669
    .restart local v0    # "count":I
    .restart local v1    # "i":I
    :cond_1
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/Element;->editList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/app/video/editor/external/Edit;

    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/Edit;->getType()I

    move-result v2

    const/4 v3, 0x6

    if-ne v2, v3, :cond_2

    .line 670
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/Element;->editList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/app/video/editor/external/Edit;

    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/Edit;->getTrans_duration()I

    move-result v2

    goto :goto_1

    .line 668
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 701
    iget v0, p0, Lcom/samsung/app/video/editor/external/Element;->type:I

    return v0
.end method

.method public getVolumeEdit()Lcom/samsung/app/video/editor/external/Edit;
    .locals 4

    .prologue
    .line 705
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/Element;->editList:Ljava/util/List;

    if-eqz v2, :cond_0

    .line 706
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/Element;->editList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    .line 707
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v0, :cond_1

    .line 714
    .end local v0    # "count":I
    .end local v1    # "i":I
    :cond_0
    const/4 v2, 0x0

    :goto_1
    return-object v2

    .line 708
    .restart local v0    # "count":I
    .restart local v1    # "i":I
    :cond_1
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/Element;->editList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/app/video/editor/external/Edit;

    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/Edit;->getType()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    .line 709
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/Element;->editList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/app/video/editor/external/Edit;

    goto :goto_1

    .line 707
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public hasNonTimeLossTransitionAtEnd()Z
    .locals 1

    .prologue
    .line 1157
    iget-boolean v0, p0, Lcom/samsung/app/video/editor/external/Element;->nonTimeLossTransitionAtEnd:Z

    return v0
.end method

.method public hasNonTimeLossTransitionAtStart()Z
    .locals 1

    .prologue
    .line 1153
    iget-boolean v0, p0, Lcom/samsung/app/video/editor/external/Element;->nonTimeLossTransitionAtStart:Z

    return v0
.end method

.method public insertDrawingElement(Ljava/lang/String;)Lcom/samsung/app/video/editor/external/ClipartParams;
    .locals 4
    .param p1, "drawing"    # Ljava/lang/String;

    .prologue
    .line 1049
    new-instance v0, Lcom/samsung/app/video/editor/external/ClipartParams;

    invoke-direct {v0}, Lcom/samsung/app/video/editor/external/ClipartParams;-><init>()V

    .line 1051
    .local v0, "clipParams":Lcom/samsung/app/video/editor/external/ClipartParams;
    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/samsung/app/video/editor/external/ClipartParams;->setStoryBoardStartTime(J)V

    .line 1052
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/Element;->getDuration()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/samsung/app/video/editor/external/ClipartParams;->setStoryBoardEndTime(J)V

    .line 1054
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/Element;->getDrawingEleListCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsung/app/video/editor/external/ClipartParams;->data:Ljava/lang/String;

    .line 1056
    invoke-virtual {p0, v0}, Lcom/samsung/app/video/editor/external/Element;->addDrawingEleList(Lcom/samsung/app/video/editor/external/ClipartParams;)V

    .line 1057
    return-object v0
.end method

.method public isAssetResource()Z
    .locals 1

    .prologue
    .line 1173
    iget-boolean v0, p0, Lcom/samsung/app/video/editor/external/Element;->mAssetResource:Z

    return v0
.end method

.method public isAutoEdited()Z
    .locals 1

    .prologue
    .line 731
    iget-boolean v0, p0, Lcom/samsung/app/video/editor/external/Element;->autoEdited:Z

    return v0
.end method

.method public isEndElement(Lcom/samsung/app/video/editor/external/TranscodeElement;)V
    .locals 2
    .param p1, "trans"    # Lcom/samsung/app/video/editor/external/TranscodeElement;

    .prologue
    .line 736
    invoke-virtual {p1, p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getMyPosition(Lcom/samsung/app/video/editor/external/Element;)I

    move-result v0

    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementList()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_0

    .line 738
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/app/video/editor/external/Element;->isEndElement:Z

    .line 743
    :goto_0
    return-void

    .line 740
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/app/video/editor/external/Element;->isEndElement:Z

    goto :goto_0
.end method

.method public isKenburnsApplied()Lcom/samsung/app/video/editor/external/Edit;
    .locals 4

    .prologue
    .line 746
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/Element;->editList:Ljava/util/List;

    if-eqz v2, :cond_0

    .line 747
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/Element;->editList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    .line 748
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v0, :cond_1

    .line 756
    .end local v0    # "count":I
    .end local v1    # "i":I
    :cond_0
    const/4 v2, 0x0

    :goto_1
    return-object v2

    .line 749
    .restart local v0    # "count":I
    .restart local v1    # "i":I
    :cond_1
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/Element;->editList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/app/video/editor/external/Edit;

    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/Edit;->getType()I

    move-result v2

    const/4 v3, 0x4

    if-ne v2, v3, :cond_2

    .line 750
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/Element;->editList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/app/video/editor/external/Edit;

    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/Edit;->getSubType()I

    move-result v2

    const/16 v3, 0x27

    if-ne v2, v3, :cond_2

    .line 751
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/Element;->editList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/app/video/editor/external/Edit;

    goto :goto_1

    .line 748
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public isMergeFullFile()Z
    .locals 1

    .prologue
    .line 763
    iget-boolean v0, p0, Lcom/samsung/app/video/editor/external/Element;->isMergeFullFile:Z

    return v0
.end method

.method public isRecordSuccess()Z
    .locals 1

    .prologue
    .line 1122
    iget-boolean v0, p0, Lcom/samsung/app/video/editor/external/Element;->recordSuccess:Z

    return v0
.end method

.method public isStartElement(Lcom/samsung/app/video/editor/external/TranscodeElement;)V
    .locals 1
    .param p1, "trans"    # Lcom/samsung/app/video/editor/external/TranscodeElement;

    .prologue
    .line 768
    invoke-virtual {p1, p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getMyPosition(Lcom/samsung/app/video/editor/external/Element;)I

    move-result v0

    if-nez v0, :cond_0

    .line 769
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/app/video/editor/external/Element;->isStartElement:Z

    .line 774
    :goto_0
    return-void

    .line 771
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/app/video/editor/external/Element;->isStartElement:Z

    goto :goto_0
.end method

.method public isUHD()Z
    .locals 1

    .prologue
    .line 1189
    iget-boolean v0, p0, Lcom/samsung/app/video/editor/external/Element;->isUHD:Z

    return v0
.end method

.method public isWQHD()Z
    .locals 1

    .prologue
    .line 1198
    iget-boolean v0, p0, Lcom/samsung/app/video/editor/external/Element;->isWQHD:Z

    return v0
.end method

.method public removeClipartList(Lcom/samsung/app/video/editor/external/ClipartParams;)V
    .locals 2
    .param p1, "clipParams"    # Lcom/samsung/app/video/editor/external/ClipartParams;

    .prologue
    .line 787
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/Element;->clipartList:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 788
    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/ClipartParams;->remove()V

    .line 789
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/Element;->clipartList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 791
    :cond_0
    sget-object v0, Lcom/samsung/app/video/editor/external/Element;->mDrawingListComparator:Lcom/samsung/app/video/editor/external/TranscodeElement$DrawingListComparator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/app/video/editor/external/Element;->clipartList:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 792
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/Element;->clipartList:Ljava/util/List;

    sget-object v1, Lcom/samsung/app/video/editor/external/Element;->mDrawingListComparator:Lcom/samsung/app/video/editor/external/TranscodeElement$DrawingListComparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 793
    :cond_1
    return-void
.end method

.method public removeDrawingEleList(Lcom/samsung/app/video/editor/external/ClipartParams;)V
    .locals 2
    .param p1, "clipParams"    # Lcom/samsung/app/video/editor/external/ClipartParams;

    .prologue
    .line 778
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/Element;->clipartListDrawing:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 779
    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/ClipartParams;->remove()V

    .line 780
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/Element;->clipartListDrawing:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 782
    :cond_0
    sget-object v0, Lcom/samsung/app/video/editor/external/Element;->mDrawingListComparator:Lcom/samsung/app/video/editor/external/TranscodeElement$DrawingListComparator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/app/video/editor/external/Element;->clipartListDrawing:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 783
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/Element;->clipartListDrawing:Ljava/util/List;

    sget-object v1, Lcom/samsung/app/video/editor/external/Element;->mDrawingListComparator:Lcom/samsung/app/video/editor/external/TranscodeElement$DrawingListComparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 784
    :cond_1
    return-void
.end method

.method public declared-synchronized removeEdit(I)V
    .locals 3
    .param p1, "edittype"    # I

    .prologue
    .line 796
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/samsung/app/video/editor/external/Element;->editList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_1

    .line 802
    :goto_0
    monitor-exit p0

    return-void

    .line 796
    :cond_1
    :try_start_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/app/video/editor/external/Edit;

    .line 797
    .local v0, "e":Lcom/samsung/app/video/editor/external/Edit;
    invoke-virtual {v0}, Lcom/samsung/app/video/editor/external/Edit;->getType()I

    move-result v2

    if-ne v2, p1, :cond_0

    .line 798
    iget-object v1, p0, Lcom/samsung/app/video/editor/external/Element;->editList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 796
    .end local v0    # "e":Lcom/samsung/app/video/editor/external/Edit;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized removeRetouchEdit()V
    .locals 4

    .prologue
    .line 805
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/Element;->editList:Ljava/util/List;

    if-eqz v2, :cond_0

    .line 806
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/Element;->editList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    add-int/lit8 v0, v2, -0x1

    .line 807
    .local v0, "count":I
    move v1, v0

    .local v1, "i":I
    :goto_0
    if-gez v1, :cond_1

    .line 815
    .end local v0    # "count":I
    .end local v1    # "i":I
    :cond_0
    :goto_1
    monitor-exit p0

    return-void

    .line 808
    .restart local v0    # "count":I
    .restart local v1    # "i":I
    :cond_1
    :try_start_1
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/Element;->editList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/app/video/editor/external/Edit;

    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/Edit;->getType()I

    move-result v2

    const/4 v3, 0x4

    if-ne v2, v3, :cond_2

    .line 809
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/Element;->editList:Ljava/util/List;

    iget-object v3, p0, Lcom/samsung/app/video/editor/external/Element;->editList:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->remove(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 805
    .end local v0    # "count":I
    .end local v1    # "i":I
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 807
    .restart local v0    # "count":I
    .restart local v1    # "i":I
    :cond_2
    add-int/lit8 v1, v1, -0x1

    goto :goto_0
.end method

.method public declared-synchronized removetransitionEdit()V
    .locals 4

    .prologue
    .line 818
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/Element;->editList:Ljava/util/List;

    if-eqz v2, :cond_0

    .line 819
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/Element;->editList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    add-int/lit8 v0, v2, -0x1

    .line 820
    .local v0, "count":I
    move v1, v0

    .local v1, "i":I
    :goto_0
    if-gez v1, :cond_1

    .line 828
    .end local v0    # "count":I
    .end local v1    # "i":I
    :cond_0
    :goto_1
    monitor-exit p0

    return-void

    .line 821
    .restart local v0    # "count":I
    .restart local v1    # "i":I
    :cond_1
    :try_start_1
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/Element;->editList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/app/video/editor/external/Edit;

    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/Edit;->getType()I

    move-result v2

    const/4 v3, 0x6

    if-ne v2, v3, :cond_2

    .line 822
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/Element;->editList:Ljava/util/List;

    iget-object v3, p0, Lcom/samsung/app/video/editor/external/Element;->editList:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->remove(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 818
    .end local v0    # "count":I
    .end local v1    # "i":I
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 820
    .restart local v0    # "count":I
    .restart local v1    # "i":I
    :cond_2
    add-int/lit8 v1, v1, -0x1

    goto :goto_0
.end method

.method public returnEffectEdit()Lcom/samsung/app/video/editor/external/Edit;
    .locals 4

    .prologue
    .line 831
    iget-object v1, p0, Lcom/samsung/app/video/editor/external/Element;->editList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 837
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 831
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/app/video/editor/external/Edit;

    .line 832
    .local v0, "delEdit":Lcom/samsung/app/video/editor/external/Edit;
    invoke-virtual {v0}, Lcom/samsung/app/video/editor/external/Edit;->getType()I

    move-result v2

    const/4 v3, 0x4

    if-ne v2, v3, :cond_0

    goto :goto_0
.end method

.method public setAssetResource(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 1181
    iput-boolean p1, p0, Lcom/samsung/app/video/editor/external/Element;->mAssetResource:Z

    .line 1182
    return-void
.end method

.method public setAudFilePath1(Ljava/lang/String;)V
    .locals 0
    .param p1, "audFilePath1"    # Ljava/lang/String;

    .prologue
    .line 845
    iput-object p1, p0, Lcom/samsung/app/video/editor/external/Element;->audFilePath1:Ljava/lang/String;

    .line 846
    return-void
.end method

.method public setAudFilePath2(Ljava/lang/String;)V
    .locals 0
    .param p1, "audFilePath2"    # Ljava/lang/String;

    .prologue
    .line 853
    iput-object p1, p0, Lcom/samsung/app/video/editor/external/Element;->audFilePath2:Ljava/lang/String;

    .line 854
    return-void
.end method

.method public setAudVidStripConstant(I)V
    .locals 0
    .param p1, "audVidStripConstant"    # I

    .prologue
    .line 865
    iput p1, p0, Lcom/samsung/app/video/editor/external/Element;->audVidStripConstant:I

    .line 866
    return-void
.end method

.method public setAudioDisplayName(Ljava/lang/String;)V
    .locals 0
    .param p1, "audioDisplayName"    # Ljava/lang/String;

    .prologue
    .line 857
    iput-object p1, p0, Lcom/samsung/app/video/editor/external/Element;->audioDisplayName:Ljava/lang/String;

    .line 858
    return-void
.end method

.method public setAutoEdited(Z)V
    .locals 0
    .param p1, "mAutoEdited"    # Z

    .prologue
    .line 869
    iput-boolean p1, p0, Lcom/samsung/app/video/editor/external/Element;->autoEdited:Z

    .line 870
    return-void
.end method

.method public setDuration(J)V
    .locals 1
    .param p1, "duration"    # J

    .prologue
    .line 873
    long-to-int v0, p1

    iput v0, p0, Lcom/samsung/app/video/editor/external/Element;->duration:I

    .line 874
    return-void
.end method

.method public setEditList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/Edit;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 881
    .local p1, "editList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Edit;>;"
    iput-object p1, p0, Lcom/samsung/app/video/editor/external/Element;->editList:Ljava/util/List;

    .line 882
    return-void
.end method

.method public setEffectEndTime(II)V
    .locals 3
    .param p1, "endTime"    # I
    .param p2, "type"    # I

    .prologue
    .line 685
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/Element;->editList:Ljava/util/List;

    if-eqz v2, :cond_0

    .line 686
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/Element;->editList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    .line 687
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v0, :cond_1

    .line 694
    .end local v0    # "count":I
    .end local v1    # "i":I
    :cond_0
    return-void

    .line 689
    .restart local v0    # "count":I
    .restart local v1    # "i":I
    :cond_1
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/Element;->editList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/app/video/editor/external/Edit;

    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/Edit;->getType()I

    move-result v2

    if-ne v2, p2, :cond_2

    .line 690
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/Element;->editList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/app/video/editor/external/Edit;

    invoke-virtual {v2, p1}, Lcom/samsung/app/video/editor/external/Edit;->setEffectEndTime(I)V

    .line 687
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public setElementRatioInGroup(F)V
    .locals 0
    .param p1, "ratio"    # F

    .prologue
    .line 885
    iput p1, p0, Lcom/samsung/app/video/editor/external/Element;->ratioInGroup:F

    .line 886
    return-void
.end method

.method public setEndRect(Landroid/graphics/RectF;)V
    .locals 1
    .param p1, "end"    # Landroid/graphics/RectF;

    .prologue
    .line 889
    iget v0, p1, Landroid/graphics/RectF;->left:F

    float-to-int v0, v0

    iput v0, p0, Lcom/samsung/app/video/editor/external/Element;->end_frame_x1:I

    .line 890
    iget v0, p1, Landroid/graphics/RectF;->top:F

    float-to-int v0, v0

    iput v0, p0, Lcom/samsung/app/video/editor/external/Element;->end_frame_y1:I

    .line 891
    iget v0, p1, Landroid/graphics/RectF;->right:F

    float-to-int v0, v0

    iput v0, p0, Lcom/samsung/app/video/editor/external/Element;->end_frame_x2:I

    .line 892
    iget v0, p1, Landroid/graphics/RectF;->bottom:F

    float-to-int v0, v0

    iput v0, p0, Lcom/samsung/app/video/editor/external/Element;->end_frame_y2:I

    .line 893
    return-void
.end method

.method public setEndTime(J)V
    .locals 1
    .param p1, "endTime"    # J

    .prologue
    .line 900
    long-to-int v0, p1

    iput v0, p0, Lcom/samsung/app/video/editor/external/Element;->endTime:I

    .line 901
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/Element;->updateBitmapAnimationData()V

    .line 902
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/Element;->updateFadeEffectData()V

    .line 903
    return-void
.end method

.method public setFilePath(Ljava/lang/String;)V
    .locals 0
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 910
    iput-object p1, p0, Lcom/samsung/app/video/editor/external/Element;->filePath:Ljava/lang/String;

    .line 911
    return-void
.end method

.method public setGroupDuration(J)V
    .locals 1
    .param p1, "duration"    # J

    .prologue
    .line 914
    iput-wide p1, p0, Lcom/samsung/app/video/editor/external/Element;->groupDuration:J

    .line 915
    return-void
.end method

.method public setGroupEndTime(J)V
    .locals 1
    .param p1, "endTime"    # J

    .prologue
    .line 918
    iput-wide p1, p0, Lcom/samsung/app/video/editor/external/Element;->groupEndTime:J

    .line 919
    return-void
.end method

.method public setGroupID(I)V
    .locals 0
    .param p1, "value"    # I

    .prologue
    .line 922
    iput p1, p0, Lcom/samsung/app/video/editor/external/Element;->groupID:I

    .line 923
    return-void
.end method

.method public setGroupStartTime(J)V
    .locals 1
    .param p1, "startTime"    # J

    .prologue
    .line 930
    iput-wide p1, p0, Lcom/samsung/app/video/editor/external/Element;->groupStartTime:J

    .line 931
    return-void
.end method

.method public setID(I)V
    .locals 0
    .param p1, "value"    # I

    .prologue
    .line 926
    iput p1, p0, Lcom/samsung/app/video/editor/external/Element;->ID:I

    .line 927
    return-void
.end method

.method public setMatrix(Landroid/graphics/Matrix;Z)V
    .locals 2
    .param p1, "m"    # Landroid/graphics/Matrix;
    .param p2, "value"    # Z

    .prologue
    const/16 v1, 0x9

    .line 934
    if-eqz p2, :cond_1

    .line 935
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/samsung/app/video/editor/external/Element;->start_values:[F

    if-eqz v0, :cond_0

    .line 936
    new-array v0, v1, [F

    iput-object v0, p0, Lcom/samsung/app/video/editor/external/Element;->start_values:[F

    .line 937
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/Element;->start_values:[F

    invoke-virtual {p1, v0}, Landroid/graphics/Matrix;->getValues([F)V

    .line 943
    :cond_0
    :goto_0
    return-void

    .line 940
    :cond_1
    new-array v0, v1, [F

    iput-object v0, p0, Lcom/samsung/app/video/editor/external/Element;->end_values:[F

    .line 941
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/Element;->end_values:[F

    invoke-virtual {p1, v0}, Landroid/graphics/Matrix;->getValues([F)V

    goto :goto_0
.end method

.method public setMergeFullFile(Z)V
    .locals 0
    .param p1, "isMergeFullFile"    # Z

    .prologue
    .line 950
    iput-boolean p1, p0, Lcom/samsung/app/video/editor/external/Element;->isMergeFullFile:Z

    .line 951
    return-void
.end method

.method public setNonTimeLossTransitionAtEnd(Z)V
    .locals 0
    .param p1, "set"    # Z

    .prologue
    .line 1165
    iput-boolean p1, p0, Lcom/samsung/app/video/editor/external/Element;->nonTimeLossTransitionAtEnd:Z

    .line 1166
    return-void
.end method

.method public setNonTimeLossTransitionAtStart(Z)V
    .locals 0
    .param p1, "set"    # Z

    .prologue
    .line 1161
    iput-boolean p1, p0, Lcom/samsung/app/video/editor/external/Element;->nonTimeLossTransitionAtStart:Z

    .line 1162
    return-void
.end method

.method public setOrientation(I)V
    .locals 0
    .param p1, "angle"    # I

    .prologue
    .line 282
    iput p1, p0, Lcom/samsung/app/video/editor/external/Element;->orientation:I

    .line 283
    return-void
.end method

.method public setOverLapAtEnd(I)V
    .locals 0
    .param p1, "flag"    # I

    .prologue
    .line 1149
    iput p1, p0, Lcom/samsung/app/video/editor/external/Element;->overLapAtEnd:I

    .line 1150
    return-void
.end method

.method public setOverLapAtStart(I)V
    .locals 0
    .param p1, "flag"    # I

    .prologue
    .line 1139
    iput p1, p0, Lcom/samsung/app/video/editor/external/Element;->overLapAtStart:I

    .line 1140
    return-void
.end method

.method public setRecordSuccess(Z)V
    .locals 0
    .param p1, "recordSuccess"    # Z

    .prologue
    .line 1129
    iput-boolean p1, p0, Lcom/samsung/app/video/editor/external/Element;->recordSuccess:Z

    .line 1130
    return-void
.end method

.method public setRecordingMode(I)V
    .locals 0
    .param p1, "mode"    # I

    .prologue
    .line 274
    iput p1, p0, Lcom/samsung/app/video/editor/external/Element;->recordingMode:I

    .line 275
    return-void
.end method

.method public setRefRect(Landroid/graphics/RectF;)V
    .locals 1
    .param p1, "ref"    # Landroid/graphics/RectF;

    .prologue
    .line 954
    iget v0, p1, Landroid/graphics/RectF;->left:F

    float-to-int v0, v0

    iput v0, p0, Lcom/samsung/app/video/editor/external/Element;->ref_frame_x1:I

    .line 955
    iget v0, p1, Landroid/graphics/RectF;->top:F

    float-to-int v0, v0

    iput v0, p0, Lcom/samsung/app/video/editor/external/Element;->ref_frame_y1:I

    .line 956
    iget v0, p1, Landroid/graphics/RectF;->right:F

    float-to-int v0, v0

    iput v0, p0, Lcom/samsung/app/video/editor/external/Element;->ref_frame_x2:I

    .line 957
    iget v0, p1, Landroid/graphics/RectF;->bottom:F

    float-to-int v0, v0

    iput v0, p0, Lcom/samsung/app/video/editor/external/Element;->ref_frame_y2:I

    .line 958
    return-void
.end method

.method public setSpeedForElement(I)V
    .locals 0
    .param p1, "speed"    # I

    .prologue
    .line 290
    iput p1, p0, Lcom/samsung/app/video/editor/external/Element;->speed:I

    .line 291
    return-void
.end method

.method public setSplitReplaceSeq(I)V
    .locals 0
    .param p1, "splitReplaceSeq"    # I

    .prologue
    .line 965
    iput p1, p0, Lcom/samsung/app/video/editor/external/Element;->splitReplaceSeq:I

    .line 966
    return-void
.end method

.method public setSplitTime(J)V
    .locals 1
    .param p1, "splitTime"    # J

    .prologue
    .line 969
    long-to-int v0, p1

    int-to-float v0, v0

    iput v0, p0, Lcom/samsung/app/video/editor/external/Element;->splitTime:F

    .line 970
    return-void
.end method

.method public setStartRect(Landroid/graphics/RectF;)V
    .locals 1
    .param p1, "start"    # Landroid/graphics/RectF;

    .prologue
    .line 973
    iget v0, p1, Landroid/graphics/RectF;->left:F

    float-to-int v0, v0

    iput v0, p0, Lcom/samsung/app/video/editor/external/Element;->start_frame_x1:I

    .line 974
    iget v0, p1, Landroid/graphics/RectF;->top:F

    float-to-int v0, v0

    iput v0, p0, Lcom/samsung/app/video/editor/external/Element;->start_frame_y1:I

    .line 975
    iget v0, p1, Landroid/graphics/RectF;->right:F

    float-to-int v0, v0

    iput v0, p0, Lcom/samsung/app/video/editor/external/Element;->start_frame_x2:I

    .line 976
    iget v0, p1, Landroid/graphics/RectF;->bottom:F

    float-to-int v0, v0

    iput v0, p0, Lcom/samsung/app/video/editor/external/Element;->start_frame_y2:I

    .line 977
    return-void
.end method

.method public setStartTime(J)V
    .locals 1
    .param p1, "startTime"    # J

    .prologue
    .line 984
    long-to-int v0, p1

    iput v0, p0, Lcom/samsung/app/video/editor/external/Element;->startTime:I

    .line 985
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/Element;->updateBitmapAnimationData()V

    .line 986
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/Element;->updateFadeEffectData()V

    .line 987
    return-void
.end method

.method public setStoryBoardEndTime(J)V
    .locals 1
    .param p1, "storyBoardEndTime"    # J

    .prologue
    .line 994
    long-to-int v0, p1

    iput v0, p0, Lcom/samsung/app/video/editor/external/Element;->storyboardEndTime:I

    .line 995
    return-void
.end method

.method public setStoryBoardStartTime(J)V
    .locals 1
    .param p1, "storyBoardStartTime"    # J

    .prologue
    .line 1002
    long-to-int v0, p1

    iput v0, p0, Lcom/samsung/app/video/editor/external/Element;->storyboardStartTime:I

    .line 1003
    return-void
.end method

.method public setSubType(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 1026
    iput p1, p0, Lcom/samsung/app/video/editor/external/Element;->subType:I

    .line 1027
    return-void
.end method

.method public setType(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 1010
    iput p1, p0, Lcom/samsung/app/video/editor/external/Element;->type:I

    .line 1011
    return-void
.end method

.method public setUHDinfo(Z)V
    .locals 0
    .param p1, "uhd"    # Z

    .prologue
    .line 1185
    iput-boolean p1, p0, Lcom/samsung/app/video/editor/external/Element;->isUHD:Z

    .line 1186
    return-void
.end method

.method public setWQHDinfo(Z)V
    .locals 0
    .param p1, "wqhd"    # Z

    .prologue
    .line 1194
    iput-boolean p1, p0, Lcom/samsung/app/video/editor/external/Element;->isWQHD:Z

    .line 1195
    return-void
.end method

.method public updateBitmapAnimationData()V
    .locals 8

    .prologue
    .line 1087
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/Element;->getBitmapAnimationsDataListCount()I

    move-result v0

    .line 1088
    .local v0, "count":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-lt v3, v0, :cond_0

    .line 1099
    return-void

    .line 1089
    :cond_0
    iget-object v6, p0, Lcom/samsung/app/video/editor/external/Element;->bitmapAnimationsDataList:Ljava/util/List;

    invoke-interface {v6, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/app/video/editor/external/BitmapAnimationData;

    .line 1090
    .local v1, "data":Lcom/samsung/app/video/editor/external/BitmapAnimationData;
    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/BitmapAnimationData;->getOriginalStartFrameWithinElement()I

    move-result v4

    .line 1091
    .local v4, "startframe":I
    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/BitmapAnimationData;->getOriginalEndFrameWithinElement()I

    move-result v2

    .line 1092
    .local v2, "endFrame":I
    sub-int v5, v2, v4

    .line 1094
    .local v5, "totalFrames":I
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/Element;->getStartTime()J

    move-result-wide v6

    long-to-float v6, v6

    const/high16 v7, 0x447a0000    # 1000.0f

    div-float/2addr v6, v7

    const/high16 v7, 0x41f00000    # 30.0f

    mul-float/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v6

    add-int/2addr v4, v6

    .line 1095
    invoke-virtual {v1, v4}, Lcom/samsung/app/video/editor/external/BitmapAnimationData;->setStartFrameWithinElement(I)V

    .line 1096
    add-int v6, v4, v5

    invoke-virtual {v1, v6}, Lcom/samsung/app/video/editor/external/BitmapAnimationData;->setEndFrameWithinElement(I)V

    .line 1088
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public updateClipart()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1030
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/Element;->getClipartList()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 1031
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/Element;->getClipartList()Ljava/util/List;

    move-result-object v0

    .line 1032
    .local v0, "list":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/app/video/editor/external/ClipartParams;

    const-wide/16 v2, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/samsung/app/video/editor/external/ClipartParams;->setStartTime(J)V

    .line 1033
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/app/video/editor/external/ClipartParams;

    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/Element;->getDuration()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/samsung/app/video/editor/external/ClipartParams;->setEndTime(J)V

    .line 1036
    .end local v0    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    :cond_0
    return-void
.end method

.method public updateDrawingElement()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1039
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/Element;->getDrawingEleList()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 1040
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/Element;->getDrawingEleList()Ljava/util/List;

    move-result-object v0

    .line 1041
    .local v0, "list":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/app/video/editor/external/ClipartParams;

    const-wide/16 v2, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/samsung/app/video/editor/external/ClipartParams;->setStartTime(J)V

    .line 1042
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/app/video/editor/external/ClipartParams;

    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/Element;->getDuration()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/samsung/app/video/editor/external/ClipartParams;->setEndTime(J)V

    .line 1045
    .end local v0    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    :cond_0
    return-void
.end method

.method public updateFadeEffectData()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x3e8

    .line 1102
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/Element;->getEditList()Ljava/util/List;

    move-result-object v1

    .line 1103
    .local v1, "editList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Edit;>;"
    if-eqz v1, :cond_1

    .line 1104
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_2

    .line 1116
    :cond_1
    return-void

    .line 1104
    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/app/video/editor/external/Edit;

    .line 1105
    .local v0, "edit":Lcom/samsung/app/video/editor/external/Edit;
    invoke-virtual {v0}, Lcom/samsung/app/video/editor/external/Edit;->getType()I

    move-result v3

    const/4 v4, 0x4

    if-ne v3, v4, :cond_0

    .line 1106
    invoke-virtual {v0}, Lcom/samsung/app/video/editor/external/Edit;->getSubType()I

    move-result v3

    const/16 v4, 0x35

    if-ne v3, v4, :cond_3

    .line 1107
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/Element;->getEndTime()J

    move-result-wide v4

    sub-long/2addr v4, v6

    long-to-int v3, v4

    invoke-virtual {v0, v3}, Lcom/samsung/app/video/editor/external/Edit;->setEffectStartTime(I)V

    .line 1108
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/Element;->getEndTime()J

    move-result-wide v4

    long-to-int v3, v4

    invoke-virtual {v0, v3}, Lcom/samsung/app/video/editor/external/Edit;->setEffectEndTime(I)V

    goto :goto_0

    .line 1109
    :cond_3
    invoke-virtual {v0}, Lcom/samsung/app/video/editor/external/Edit;->getSubType()I

    move-result v3

    const/16 v4, 0x33

    if-ne v3, v4, :cond_0

    .line 1110
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/Element;->getStartTime()J

    move-result-wide v4

    long-to-int v3, v4

    invoke-virtual {v0, v3}, Lcom/samsung/app/video/editor/external/Edit;->setEffectStartTime(I)V

    .line 1111
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/Element;->getStartTime()J

    move-result-wide v4

    add-long/2addr v4, v6

    long-to-int v3, v4

    invoke-virtual {v0, v3}, Lcom/samsung/app/video/editor/external/Edit;->setEffectEndTime(I)V

    goto :goto_0
.end method
