.class public Lcom/samsung/app/video/editor/external/Constants$OperationSubType;
.super Ljava/lang/Object;
.source "Constants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/app/video/editor/external/Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "OperationSubType"
.end annotation


# static fields
.field public static final EDIT_SUB_TYPE_AUD_DJ_ALIEN:I = 0x43

.field public static final EDIT_SUB_TYPE_AUD_DJ_ECHO:I = 0x3b

.field public static final EDIT_SUB_TYPE_AUD_DJ_MONSTER:I = 0x42

.field public static final EDIT_SUB_TYPE_AUD_DJ_REVERBERATION:I = 0x3f

.field public static final EDIT_SUB_TYPE_AUD_TR_FADE_IN:I = 0x14

.field public static final EDIT_SUB_TYPE_AUD_TR_FADE_IN_OUT_BOTH:I = 0x15

.field public static final EDIT_SUB_TYPE_AUD_TR_FADE_OUT:I = 0x13

.field public static final EDIT_SUB_TYPE_EFFECT_BACKLIGHT:I = 0x20

.field public static final EDIT_SUB_TYPE_EFFECT_BLACK_AND_WHITE:I = 0x17

.field public static final EDIT_SUB_TYPE_EFFECT_BLACK_AND_WHITE_K:I = 0x5d

.field public static final EDIT_SUB_TYPE_EFFECT_BLACK_VIGNETTE:I = 0x6c

.field public static final EDIT_SUB_TYPE_EFFECT_BLUR:I = 0x1b

.field public static final EDIT_SUB_TYPE_EFFECT_CARTOON:I = 0x2c

.field public static final EDIT_SUB_TYPE_EFFECT_CARTOON_K:I = 0x5f

.field public static final EDIT_SUB_TYPE_EFFECT_CIRCLE_ZOOM_IN:I = 0xa0

.field public static final EDIT_SUB_TYPE_EFFECT_CITRUS:I = 0x7f

.field public static final EDIT_SUB_TYPE_EFFECT_DIAGONAL_VIEW:I = 0xa5

.field public static final EDIT_SUB_TYPE_EFFECT_DYNAMIC_RAINBOW:I = 0x8d

.field public static final EDIT_SUB_TYPE_EFFECT_EMBOSS:I = 0x1e

.field public static final EDIT_SUB_TYPE_EFFECT_FADEDCOLOR:I = 0x61

.field public static final EDIT_SUB_TYPE_EFFECT_FISH_EYE:I = 0x8b

.field public static final EDIT_SUB_TYPE_EFFECT_GAUSSIAN_BLUR:I = 0x63

.field public static final EDIT_SUB_TYPE_EFFECT_GRESYSCALE_CONTRAST_VIGNETTE:I = 0x46

.field public static final EDIT_SUB_TYPE_EFFECT_HALF_TONE:I = 0x85

.field public static final EDIT_SUB_TYPE_EFFECT_HEAT:I = 0x7e

.field public static final EDIT_SUB_TYPE_EFFECT_ILLUMINATION:I = 0x8c

.field public static final EDIT_SUB_TYPE_EFFECT_KENBURN:I = 0x22

.field public static final EDIT_SUB_TYPE_EFFECT_LENS_FLARE:I = 0x9a

.field public static final EDIT_SUB_TYPE_EFFECT_LINEAR_DODGE_BLEND_TEXTURE:I = 0x62

.field public static final EDIT_SUB_TYPE_EFFECT_MINT:I = 0x87

.field public static final EDIT_SUB_TYPE_EFFECT_MIRROR_H:I = 0x89

.field public static final EDIT_SUB_TYPE_EFFECT_MIRROR_LEFT:I = 0x73

.field public static final EDIT_SUB_TYPE_EFFECT_MIRROR_RIGHT:I = 0x72

.field public static final EDIT_SUB_TYPE_EFFECT_MOSAIC:I = 0x6e

.field public static final EDIT_SUB_TYPE_EFFECT_NEGATIVE:I = 0x1f

.field public static final EDIT_SUB_TYPE_EFFECT_NE_AUTUMN_LEAF:I = 0x75

.field public static final EDIT_SUB_TYPE_EFFECT_NE_FALLING_STARS:I = 0x99

.field public static final EDIT_SUB_TYPE_EFFECT_NE_FOG:I = 0x7a

.field public static final EDIT_SUB_TYPE_EFFECT_NE_RAINBOW:I = 0x79

.field public static final EDIT_SUB_TYPE_EFFECT_NE_RAINFALL:I = 0x77

.field public static final EDIT_SUB_TYPE_EFFECT_NE_SNOWFALL:I = 0x78

.field public static final EDIT_SUB_TYPE_EFFECT_NE_SPRING_PETAL:I = 0x76

.field public static final EDIT_SUB_TYPE_EFFECT_NE_THUNDER:I = 0x7b

.field public static final EDIT_SUB_TYPE_EFFECT_NOISE:I = 0x1d

.field public static final EDIT_SUB_TYPE_EFFECT_NO_EFFECT:I = 0x16

.field public static final EDIT_SUB_TYPE_EFFECT_OLD_TV:I = 0x86

.field public static final EDIT_SUB_TYPE_EFFECT_PASTEL:I = 0x83

.field public static final EDIT_SUB_TYPE_EFFECT_PASTEL_SKETCH:I = 0x6d

.field public static final EDIT_SUB_TYPE_EFFECT_POSTER:I = 0x84

.field public static final EDIT_SUB_TYPE_EFFECT_POSTERISE:I = 0x19

.field public static final EDIT_SUB_TYPE_EFFECT_PRISM:I = 0x8a

.field public static final EDIT_SUB_TYPE_EFFECT_RAINBOW_H:I = 0x82

.field public static final EDIT_SUB_TYPE_EFFECT_RAINBOW_V:I = 0x81

.field public static final EDIT_SUB_TYPE_EFFECT_RGBSHIFT:I = 0x9b

.field public static final EDIT_SUB_TYPE_EFFECT_ROSE:I = 0x80

.field public static final EDIT_SUB_TYPE_EFFECT_SAND_STONE:I = 0x70

.field public static final EDIT_SUB_TYPE_EFFECT_SEPIA:I = 0x18

.field public static final EDIT_SUB_TYPE_EFFECT_SEPIA_K:I = 0x5e

.field public static final EDIT_SUB_TYPE_EFFECT_SEPIA_VIGNETTE:I = 0x48

.field public static final EDIT_SUB_TYPE_EFFECT_SHARPEN:I = 0x1c

.field public static final EDIT_SUB_TYPE_EFFECT_SNAPSHOT:I = 0x4d

.field public static final EDIT_SUB_TYPE_EFFECT_SOLARISE:I = 0x1a

.field public static final EDIT_SUB_TYPE_EFFECT_SPY_CAM:I = 0x88

.field public static final EDIT_SUB_TYPE_EFFECT_THERMAL:I = 0x71

.field public static final EDIT_SUB_TYPE_EFFECT_TINT:I = 0x6f

.field public static final EDIT_SUB_TYPE_EFFECT_TURQUOISE:I = 0x74

.field public static final EDIT_SUB_TYPE_EFFECT_VINTAGE:I = 0x2b

.field public static final EDIT_SUB_TYPE_EFFECT_VINTAGE_K:I = 0x60

.field public static final EDIT_SUB_TYPE_EFFECT_WHITE_VIGNETTE:I = 0x5c

.field public static final EDIT_SUB_TYPE_EFFECT_ZOOM:I = 0x27

.field public static final EDIT_SUB_TYPE_FADE_IN_BLACK:I = 0x33

.field public static final EDIT_SUB_TYPE_FADE_IN_OUT_BOTH_BLACK:I = 0x37

.field public static final EDIT_SUB_TYPE_FADE_IN_OUT_BOTH_WHITE:I = 0x38

.field public static final EDIT_SUB_TYPE_FADE_IN_WHITE:I = 0x34

.field public static final EDIT_SUB_TYPE_FADE_OUT_BLACK:I = 0x35

.field public static final EDIT_SUB_TYPE_FADE_OUT_WHITE:I = 0x36

.field public static final EDIT_SUB_TYPE_MAX:I = 0xa9

.field public static final EDIT_SUB_TYPE_NONE:I = 0x0

.field public static final EDIT_SUB_TYPE_PNG_TEXTURE_BLEND:I = 0x50

.field public static final EDIT_SUB_TYPE_TR_HOMEVIDEO:I = 0x21

.field public static final EDIT_SUB_TYPE_VID_EFFECT_ANALOGTV_SCRAMBLE:I = 0xa4

.field public static final EDIT_SUB_TYPE_VID_EFFECT_RGB_SLIDING:I = 0xa8

.field public static final EDIT_SUB_TYPE_VID_MIX_MULTIPLE_TEXTURE:I = 0x5b

.field public static final EDIT_SUB_TYPE_VID_TEXTURE_BLEND:I = 0x4e

.field public static final EDIT_SUB_TYPE_VID_TR_2STEP_SLIDE_LEFT:I = 0x51

.field public static final EDIT_SUB_TYPE_VID_TR_ALPHA_BLEND:I = 0x4f

.field public static final EDIT_SUB_TYPE_VID_TR_BARS:I = 0x3

.field public static final EDIT_SUB_TYPE_VID_TR_BLACK_TRANSITION:I = 0x65

.field public static final EDIT_SUB_TYPE_VID_TR_BLEND_IN:I = 0x47

.field public static final EDIT_SUB_TYPE_VID_TR_BLEND_IN_WITH_TEXTURE:I = 0x5a

.field public static final EDIT_SUB_TYPE_VID_TR_BLOCK:I = 0x29

.field public static final EDIT_SUB_TYPE_VID_TR_CHECKER_BOARD:I = 0x4

.field public static final EDIT_SUB_TYPE_VID_TR_CIRCLE:I = 0xb

.field public static final EDIT_SUB_TYPE_VID_TR_CIRCLE_SCALE_DOWN:I = 0x9d

.field public static final EDIT_SUB_TYPE_VID_TR_CLOCK_WIPE:I = 0x93

.field public static final EDIT_SUB_TYPE_VID_TR_COLLAGE1:I = 0x94

.field public static final EDIT_SUB_TYPE_VID_TR_COLLAGE2:I = 0x95

.field public static final EDIT_SUB_TYPE_VID_TR_COLOR_CIRCLE_SCALE:I = 0x9e

.field public static final EDIT_SUB_TYPE_VID_TR_CONFERENCE:I = 0x26

.field public static final EDIT_SUB_TYPE_VID_TR_CORNER:I = 0x90

.field public static final EDIT_SUB_TYPE_VID_TR_CRICLE_SCALE_UP:I = 0x9c

.field public static final EDIT_SUB_TYPE_VID_TR_CUBE_TURN_A:I = 0x6a

.field public static final EDIT_SUB_TYPE_VID_TR_CUBE_TURN_B:I = 0x69

.field public static final EDIT_SUB_TYPE_VID_TR_DECLUTTER:I = 0x2e

.field public static final EDIT_SUB_TYPE_VID_TR_DIAGONALSLIDE_BEGIN:I = 0xa6

.field public static final EDIT_SUB_TYPE_VID_TR_DIAGONALSLIDE_END:I = 0xa7

.field public static final EDIT_SUB_TYPE_VID_TR_DIAGONAL_TRANSLATE_BLEND:I = 0x56

.field public static final EDIT_SUB_TYPE_VID_TR_DIAGONAL_TRANSLATE_BLEND_WITH_TEXTURE:I = 0x59

.field public static final EDIT_SUB_TYPE_VID_TR_DISSOLVE:I = 0x2

.field public static final EDIT_SUB_TYPE_VID_TR_FADE:I = 0x1

.field public static final EDIT_SUB_TYPE_VID_TR_FADE_IN_OUT:I = 0x64

.field public static final EDIT_SUB_TYPE_VID_TR_GAUSSIAN_BLUR:I = 0x68

.field public static final EDIT_SUB_TYPE_VID_TR_HEART:I = 0xc

.field public static final EDIT_SUB_TYPE_VID_TR_INSERT_DOWN_LEFT:I = 0xd

.field public static final EDIT_SUB_TYPE_VID_TR_INSERT_DOWN_RIGHT:I = 0xe

.field public static final EDIT_SUB_TYPE_VID_TR_INSERT_UP_LEFT:I = 0xf

.field public static final EDIT_SUB_TYPE_VID_TR_INSERT_UP_RIGHT:I = 0x10

.field public static final EDIT_SUB_TYPE_VID_TR_KNOCKASIDE:I = 0x8e

.field public static final EDIT_SUB_TYPE_VID_TR_LINEAR_DODGE_BLEND_TEXTURE:I = 0x67

.field public static final EDIT_SUB_TYPE_VID_TR_MOVIE:I = 0x22

.field public static final EDIT_SUB_TYPE_VID_TR_MULTILAYER_FRAME:I = 0x9f

.field public static final EDIT_SUB_TYPE_VID_TR_ORIGAMI:I = 0x2a

.field public static final EDIT_SUB_TYPE_VID_TR_PARTICLE_LIGHTING:I = 0xa2

.field public static final EDIT_SUB_TYPE_VID_TR_PARTY:I = 0x23

.field public static final EDIT_SUB_TYPE_VID_TR_PIXELISE:I = 0x6b

.field public static final EDIT_SUB_TYPE_VID_TR_RECTANGLE:I = 0xa

.field public static final EDIT_SUB_TYPE_VID_TR_REVEAL_DOWN:I = 0x6

.field public static final EDIT_SUB_TYPE_VID_TR_REVEAL_RIGHT:I = 0x7

.field public static final EDIT_SUB_TYPE_VID_TR_RGB_SLIDING:I = 0xa3

.field public static final EDIT_SUB_TYPE_VID_TR_ROTATION_270LEFT_Y:I = 0x45

.field public static final EDIT_SUB_TYPE_VID_TR_ROTATION_270RIGHT_Y:I = 0x44

.field public static final EDIT_SUB_TYPE_VID_TR_ROTATION_AROUND_Y:I = 0x39

.field public static final EDIT_SUB_TYPE_VID_TR_SLIDE_FROM_RIGHT:I = 0x98

.field public static final EDIT_SUB_TYPE_VID_TR_SPIN:I = 0x8f

.field public static final EDIT_SUB_TYPE_VID_TR_SPLIT_HORIZONTAL:I = 0x12

.field public static final EDIT_SUB_TYPE_VID_TR_SPLIT_VERTICAL:I = 0x11

.field public static final EDIT_SUB_TYPE_VID_TR_STAGE:I = 0x25

.field public static final EDIT_SUB_TYPE_VID_TR_SWAP:I = 0x28

.field public static final EDIT_SUB_TYPE_VID_TR_TEXSPLIT:I = 0x97

.field public static final EDIT_SUB_TYPE_VID_TR_TEXTURE_TRANSLATE:I = 0x96

.field public static final EDIT_SUB_TYPE_VID_TR_THREE_BUFFER:I = 0x7d

.field public static final EDIT_SUB_TYPE_VID_TR_TILE_FLIP:I = 0x91

.field public static final EDIT_SUB_TYPE_VID_TR_TRANSLATE_BOTTOM:I = 0x55

.field public static final EDIT_SUB_TYPE_VID_TR_TRANSLATE_BOTTOM_WITH_TEXTURE:I = 0x57

.field public static final EDIT_SUB_TYPE_VID_TR_TRANSLATE_LEFT:I = 0x52

.field public static final EDIT_SUB_TYPE_VID_TR_TRANSLATE_RIGHT:I = 0x53

.field public static final EDIT_SUB_TYPE_VID_TR_TRANSLATE_RIGHT_WITH_TEXTURE:I = 0x58

.field public static final EDIT_SUB_TYPE_VID_TR_TRANSLATE_TOP:I = 0x54

.field public static final EDIT_SUB_TYPE_VID_TR_TRAVEL:I = 0x24

.field public static final EDIT_SUB_TYPE_VID_TR_TWO_BUFFER:I = 0x7c

.field public static final EDIT_SUB_TYPE_VID_TR_WARM_COOL_LIGHTING:I = 0xa1

.field public static final EDIT_SUB_TYPE_VID_TR_WEAVE:I = 0x2d

.field public static final EDIT_SUB_TYPE_VID_TR_WHEEL:I = 0x5

.field public static final EDIT_SUB_TYPE_VID_TR_WHITE_TRANSITION:I = 0x66

.field public static final EDIT_SUB_TYPE_VID_TR_WIPE_DOWN:I = 0x8

.field public static final EDIT_SUB_TYPE_VID_TR_WIPE_RIGHT:I = 0x9

.field public static final EDIT_SUB_TYPE_VID_TR_WIPE_WAVE:I = 0x92

.field public static final EDIT_SUB_TYPE_VID_TR__BLUR_TRANSLATE_BOTTOM:I = 0x4c

.field public static final EDIT_SUB_TYPE_VID_TR__BLUR_TRANSLATE_LEFT:I = 0x49

.field public static final EDIT_SUB_TYPE_VID_TR__BLUR_TRANSLATE_RIGHT:I = 0x4a

.field public static final EDIT_SUB_TYPE_VID_TR__BLUR_TRANSLATE_TOP:I = 0x4b


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 146
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
