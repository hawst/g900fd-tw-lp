.class public Lcom/samsung/app/video/editor/external/EngineFlawCoverUp$ImageReference;
.super Ljava/lang/Object;
.source "EngineFlawCoverUp.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/app/video/editor/external/EngineFlawCoverUp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ImageReference"
.end annotation


# instance fields
.field private mFilePath:Ljava/lang/String;

.field private mRefCount:I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "filepath"    # Ljava/lang/String;

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/app/video/editor/external/EngineFlawCoverUp$ImageReference;->mRefCount:I

    .line 62
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/app/video/editor/external/EngineFlawCoverUp$ImageReference;->mFilePath:Ljava/lang/String;

    .line 65
    iput-object p1, p0, Lcom/samsung/app/video/editor/external/EngineFlawCoverUp$ImageReference;->mFilePath:Ljava/lang/String;

    .line 66
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/app/video/editor/external/EngineFlawCoverUp$ImageReference;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/EngineFlawCoverUp$ImageReference;->mFilePath:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public addRef()I
    .locals 1

    .prologue
    .line 76
    iget v0, p0, Lcom/samsung/app/video/editor/external/EngineFlawCoverUp$ImageReference;->mRefCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/app/video/editor/external/EngineFlawCoverUp$ImageReference;->mRefCount:I

    .line 77
    iget v0, p0, Lcom/samsung/app/video/editor/external/EngineFlawCoverUp$ImageReference;->mRefCount:I

    return v0
.end method

.method public deleteRef()I
    .locals 1

    .prologue
    .line 69
    iget v0, p0, Lcom/samsung/app/video/editor/external/EngineFlawCoverUp$ImageReference;->mRefCount:I

    if-lez v0, :cond_0

    .line 70
    iget v0, p0, Lcom/samsung/app/video/editor/external/EngineFlawCoverUp$ImageReference;->mRefCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/samsung/app/video/editor/external/EngineFlawCoverUp$ImageReference;->mRefCount:I

    .line 72
    :cond_0
    iget v0, p0, Lcom/samsung/app/video/editor/external/EngineFlawCoverUp$ImageReference;->mRefCount:I

    return v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 81
    iget v0, p0, Lcom/samsung/app/video/editor/external/EngineFlawCoverUp$ImageReference;->mRefCount:I

    return v0
.end method
