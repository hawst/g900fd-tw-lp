.class public Lcom/samsung/app/video/editor/external/TranscodeElement;
.super Ljava/lang/Object;
.source "TranscodeElement.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/app/video/editor/external/TranscodeElement$DrawingListComparator;,
        Lcom/samsung/app/video/editor/external/TranscodeElement$ElementListComparator;,
        Lcom/samsung/app/video/editor/external/TranscodeElement$ExportParameters;,
        Lcom/samsung/app/video/editor/external/TranscodeElement$ExportedTranscodeElement;,
        Lcom/samsung/app/video/editor/external/TranscodeElement$GroupData;,
        Lcom/samsung/app/video/editor/external/TranscodeElement$Listener;,
        Lcom/samsung/app/video/editor/external/TranscodeElement$TextListComparator;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/io/Serializable;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/samsung/app/video/editor/external/TranscodeElement;",
        ">;"
    }
.end annotation


# static fields
.field private static mDrawingListComparator:Lcom/samsung/app/video/editor/external/TranscodeElement$DrawingListComparator; = null

.field private static mElementListComparator:Lcom/samsung/app/video/editor/external/TranscodeElement$ElementListComparator; = null

.field private static mTextListComparator:Lcom/samsung/app/video/editor/external/TranscodeElement$TextListComparator; = null

.field private static final serialVersionUID:J = -0x2e064f59557babb6L


# instance fields
.field private TargetDispHeight:I

.field private TargetDispWidth:I

.field private additionalAudioDuration:J

.field private additionlAudioEleList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/Element;",
            ">;"
        }
    .end annotation
.end field

.field private additionlNarrationEleList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/Element;",
            ">;"
        }
    .end annotation
.end field

.field private additionlSoundEleList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/Element;",
            ">;"
        }
    .end annotation
.end field

.field private drawingEleList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/ClipartParams;",
            ">;"
        }
    .end annotation
.end field

.field private elementList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/Element;",
            ">;"
        }
    .end annotation
.end field

.field public exportedVideoPath:Ljava/lang/String;

.field private exporthm:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/samsung/app/video/editor/external/TranscodeElement$ExportedTranscodeElement;",
            ">;"
        }
    .end annotation
.end field

.field private fullMovieDuration:I

.field private isAPreview:Z

.field private isMissingFileProject:Z

.field private isNewRecomProject:Z

.field private isSummaryProject:Z

.field private lastModified:Ljava/lang/String;

.field private transient mAssetManager:Landroid/content/res/AssetManager;

.field private mThemeName:I

.field private transient mThumbnail:Landroid/graphics/Bitmap;

.field private mUniqueProjectID:Ljava/lang/String;

.field private mfilesPathforEngine:Ljava/lang/String;

.field private projectCreationTime:J

.field private projectFileName:Ljava/lang/String;

.field private projectName:Ljava/lang/String;

.field private resolutionEnumValue:I

.field private tempAudioEleList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/Element;",
            ">;"
        }
    .end annotation
.end field

.field private textEleList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/ClipartParams;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 80
    new-instance v0, Lcom/samsung/app/video/editor/external/TranscodeElement$DrawingListComparator;

    invoke-direct {v0}, Lcom/samsung/app/video/editor/external/TranscodeElement$DrawingListComparator;-><init>()V

    sput-object v0, Lcom/samsung/app/video/editor/external/TranscodeElement;->mDrawingListComparator:Lcom/samsung/app/video/editor/external/TranscodeElement$DrawingListComparator;

    .line 82
    new-instance v0, Lcom/samsung/app/video/editor/external/TranscodeElement$ElementListComparator;

    invoke-direct {v0}, Lcom/samsung/app/video/editor/external/TranscodeElement$ElementListComparator;-><init>()V

    sput-object v0, Lcom/samsung/app/video/editor/external/TranscodeElement;->mElementListComparator:Lcom/samsung/app/video/editor/external/TranscodeElement$ElementListComparator;

    .line 84
    new-instance v0, Lcom/samsung/app/video/editor/external/TranscodeElement$TextListComparator;

    invoke-direct {v0}, Lcom/samsung/app/video/editor/external/TranscodeElement$TextListComparator;-><init>()V

    sput-object v0, Lcom/samsung/app/video/editor/external/TranscodeElement;->mTextListComparator:Lcom/samsung/app/video/editor/external/TranscodeElement$TextListComparator;

    .line 85
    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 127
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 101
    iput-boolean v2, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->isMissingFileProject:Z

    .line 102
    iput-boolean v2, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->isSummaryProject:Z

    .line 105
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->projectCreationTime:J

    .line 112
    sget v0, Lcom/sec/android/app/ve/common/ConfigUtils;->PREVIEW_HEIGHT:I

    iput v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->TargetDispHeight:I

    .line 114
    sget v0, Lcom/sec/android/app/ve/common/ConfigUtils;->PREVIEW_WIDTH:I

    iput v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->TargetDispWidth:I

    .line 120
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->mThumbnail:Landroid/graphics/Bitmap;

    .line 121
    iput-boolean v2, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->isNewRecomProject:Z

    .line 122
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->exporthm:Ljava/util/HashMap;

    .line 128
    return-void
.end method

.method public constructor <init>(Lcom/samsung/app/video/editor/external/TranscodeElement;)V
    .locals 5
    .param p1, "tElement"    # Lcom/samsung/app/video/editor/external/TranscodeElement;

    .prologue
    const/4 v4, 0x0

    .line 131
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 101
    iput-boolean v4, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->isMissingFileProject:Z

    .line 102
    iput-boolean v4, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->isSummaryProject:Z

    .line 105
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->projectCreationTime:J

    .line 112
    sget v2, Lcom/sec/android/app/ve/common/ConfigUtils;->PREVIEW_HEIGHT:I

    iput v2, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->TargetDispHeight:I

    .line 114
    sget v2, Lcom/sec/android/app/ve/common/ConfigUtils;->PREVIEW_WIDTH:I

    iput v2, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->TargetDispWidth:I

    .line 120
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->mThumbnail:Landroid/graphics/Bitmap;

    .line 121
    iput-boolean v4, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->isNewRecomProject:Z

    .line 122
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->exporthm:Ljava/util/HashMap;

    .line 132
    if-nez p1, :cond_1

    .line 226
    :cond_0
    :goto_0
    return-void

    .line 135
    :cond_1
    iget-boolean v2, p1, Lcom/samsung/app/video/editor/external/TranscodeElement;->isAPreview:Z

    iput-boolean v2, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->isAPreview:Z

    .line 136
    iget v2, p1, Lcom/samsung/app/video/editor/external/TranscodeElement;->fullMovieDuration:I

    iput v2, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->fullMovieDuration:I

    .line 137
    iget v2, p1, Lcom/samsung/app/video/editor/external/TranscodeElement;->resolutionEnumValue:I

    iput v2, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->resolutionEnumValue:I

    .line 139
    iget v2, p1, Lcom/samsung/app/video/editor/external/TranscodeElement;->mThemeName:I

    iput v2, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->mThemeName:I

    .line 140
    iget-object v2, p1, Lcom/samsung/app/video/editor/external/TranscodeElement;->lastModified:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 141
    new-instance v2, Ljava/lang/String;

    iget-object v3, p1, Lcom/samsung/app/video/editor/external/TranscodeElement;->lastModified:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->lastModified:Ljava/lang/String;

    .line 142
    :cond_2
    iget-object v2, p1, Lcom/samsung/app/video/editor/external/TranscodeElement;->exportedVideoPath:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 143
    new-instance v2, Ljava/lang/String;

    iget-object v3, p1, Lcom/samsung/app/video/editor/external/TranscodeElement;->exportedVideoPath:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->exportedVideoPath:Ljava/lang/String;

    .line 144
    :cond_3
    iget-wide v2, p1, Lcom/samsung/app/video/editor/external/TranscodeElement;->projectCreationTime:J

    iput-wide v2, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->projectCreationTime:J

    .line 145
    iget-object v2, p1, Lcom/samsung/app/video/editor/external/TranscodeElement;->projectFileName:Ljava/lang/String;

    if-eqz v2, :cond_4

    .line 146
    new-instance v2, Ljava/lang/String;

    iget-object v3, p1, Lcom/samsung/app/video/editor/external/TranscodeElement;->projectFileName:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->projectFileName:Ljava/lang/String;

    .line 147
    :cond_4
    iget-object v2, p1, Lcom/samsung/app/video/editor/external/TranscodeElement;->projectName:Ljava/lang/String;

    if-eqz v2, :cond_5

    .line 148
    new-instance v2, Ljava/lang/String;

    iget-object v3, p1, Lcom/samsung/app/video/editor/external/TranscodeElement;->projectName:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->projectName:Ljava/lang/String;

    .line 149
    :cond_5
    iget-wide v2, p1, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionalAudioDuration:J

    iput-wide v2, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionalAudioDuration:J

    .line 151
    iget v2, p1, Lcom/samsung/app/video/editor/external/TranscodeElement;->TargetDispWidth:I

    iput v2, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->TargetDispWidth:I

    .line 152
    iget v2, p1, Lcom/samsung/app/video/editor/external/TranscodeElement;->TargetDispHeight:I

    iput v2, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->TargetDispHeight:I

    .line 153
    iget-boolean v2, p1, Lcom/samsung/app/video/editor/external/TranscodeElement;->isSummaryProject:Z

    iput-boolean v2, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->isSummaryProject:Z

    .line 154
    iget-boolean v2, p1, Lcom/samsung/app/video/editor/external/TranscodeElement;->isMissingFileProject:Z

    iput-boolean v2, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->isMissingFileProject:Z

    .line 155
    iget-object v2, p1, Lcom/samsung/app/video/editor/external/TranscodeElement;->mAssetManager:Landroid/content/res/AssetManager;

    iput-object v2, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->mAssetManager:Landroid/content/res/AssetManager;

    .line 156
    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v0

    .line 157
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-lt v1, v0, :cond_8

    .line 163
    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getAudioListCount()I

    move-result v0

    .line 164
    const/4 v1, 0x0

    :goto_2
    if-lt v1, v0, :cond_a

    .line 172
    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getTempAudioListCount()I

    move-result v0

    .line 173
    const/4 v1, 0x0

    :goto_3
    if-lt v1, v0, :cond_c

    .line 178
    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getSoundListCount()I

    move-result v0

    .line 179
    const/4 v1, 0x0

    :goto_4
    if-lt v1, v0, :cond_e

    .line 187
    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getRecordListCount()I

    move-result v0

    .line 188
    const/4 v1, 0x0

    :goto_5
    if-lt v1, v0, :cond_10

    .line 196
    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getTextEleListCount()I

    move-result v0

    .line 197
    const/4 v1, 0x0

    :goto_6
    if-lt v1, v0, :cond_12

    .line 204
    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getDrawingEleListCount()I

    move-result v0

    .line 205
    const/4 v1, 0x0

    :goto_7
    if-lt v1, v0, :cond_14

    .line 214
    iget-object v2, p1, Lcom/samsung/app/video/editor/external/TranscodeElement;->exporthm:Ljava/util/HashMap;

    if-eqz v2, :cond_7

    .line 216
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->exporthm:Ljava/util/HashMap;

    if-nez v2, :cond_6

    .line 217
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->exporthm:Ljava/util/HashMap;

    .line 220
    :cond_6
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->exporthm:Ljava/util/HashMap;

    iget-object v3, p1, Lcom/samsung/app/video/editor/external/TranscodeElement;->exporthm:Ljava/util/HashMap;

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 222
    :cond_7
    iget-object v2, p1, Lcom/samsung/app/video/editor/external/TranscodeElement;->mfilesPathforEngine:Ljava/lang/String;

    iput-object v2, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->mfilesPathforEngine:Ljava/lang/String;

    .line 223
    iget-object v2, p1, Lcom/samsung/app/video/editor/external/TranscodeElement;->mUniqueProjectID:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 224
    new-instance v2, Ljava/lang/String;

    iget-object v3, p1, Lcom/samsung/app/video/editor/external/TranscodeElement;->mUniqueProjectID:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->mUniqueProjectID:Ljava/lang/String;

    goto/16 :goto_0

    .line 158
    :cond_8
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->elementList:Ljava/util/List;

    if-nez v2, :cond_9

    .line 159
    new-instance v2, Ljava/util/Vector;

    invoke-direct {v2}, Ljava/util/Vector;-><init>()V

    iput-object v2, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->elementList:Ljava/util/List;

    .line 161
    :cond_9
    iget-object v3, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->elementList:Ljava/util/List;

    new-instance v4, Lcom/samsung/app/video/editor/external/Element;

    iget-object v2, p1, Lcom/samsung/app/video/editor/external/TranscodeElement;->elementList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/app/video/editor/external/Element;

    invoke-direct {v4, v2}, Lcom/samsung/app/video/editor/external/Element;-><init>(Lcom/samsung/app/video/editor/external/Element;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 157
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 165
    :cond_a
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlAudioEleList:Ljava/util/List;

    if-nez v2, :cond_b

    .line 166
    new-instance v2, Ljava/util/Vector;

    invoke-direct {v2}, Ljava/util/Vector;-><init>()V

    iput-object v2, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlAudioEleList:Ljava/util/List;

    .line 169
    :cond_b
    iget-object v3, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlAudioEleList:Ljava/util/List;

    new-instance v4, Lcom/samsung/app/video/editor/external/Element;

    .line 170
    iget-object v2, p1, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlAudioEleList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/app/video/editor/external/Element;

    invoke-direct {v4, v2}, Lcom/samsung/app/video/editor/external/Element;-><init>(Lcom/samsung/app/video/editor/external/Element;)V

    .line 169
    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 164
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_2

    .line 174
    :cond_c
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->tempAudioEleList:Ljava/util/List;

    if-nez v2, :cond_d

    .line 175
    new-instance v2, Ljava/util/Vector;

    invoke-direct {v2}, Ljava/util/Vector;-><init>()V

    iput-object v2, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->tempAudioEleList:Ljava/util/List;

    .line 176
    :cond_d
    iget-object v3, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->tempAudioEleList:Ljava/util/List;

    new-instance v4, Lcom/samsung/app/video/editor/external/Element;

    iget-object v2, p1, Lcom/samsung/app/video/editor/external/TranscodeElement;->tempAudioEleList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/app/video/editor/external/Element;

    invoke-direct {v4, v2}, Lcom/samsung/app/video/editor/external/Element;-><init>(Lcom/samsung/app/video/editor/external/Element;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 173
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_3

    .line 180
    :cond_e
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlSoundEleList:Ljava/util/List;

    if-nez v2, :cond_f

    .line 181
    new-instance v2, Ljava/util/Vector;

    invoke-direct {v2}, Ljava/util/Vector;-><init>()V

    iput-object v2, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlSoundEleList:Ljava/util/List;

    .line 184
    :cond_f
    iget-object v3, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlSoundEleList:Ljava/util/List;

    new-instance v4, Lcom/samsung/app/video/editor/external/Element;

    .line 185
    iget-object v2, p1, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlSoundEleList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/app/video/editor/external/Element;

    invoke-direct {v4, v2}, Lcom/samsung/app/video/editor/external/Element;-><init>(Lcom/samsung/app/video/editor/external/Element;)V

    .line 184
    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 179
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_4

    .line 189
    :cond_10
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlNarrationEleList:Ljava/util/List;

    if-nez v2, :cond_11

    .line 190
    new-instance v2, Ljava/util/Vector;

    invoke-direct {v2}, Ljava/util/Vector;-><init>()V

    iput-object v2, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlNarrationEleList:Ljava/util/List;

    .line 193
    :cond_11
    iget-object v3, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlNarrationEleList:Ljava/util/List;

    new-instance v4, Lcom/samsung/app/video/editor/external/Element;

    .line 194
    iget-object v2, p1, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlNarrationEleList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/app/video/editor/external/Element;

    invoke-direct {v4, v2}, Lcom/samsung/app/video/editor/external/Element;-><init>(Lcom/samsung/app/video/editor/external/Element;)V

    .line 193
    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 188
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_5

    .line 198
    :cond_12
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->textEleList:Ljava/util/List;

    if-nez v2, :cond_13

    .line 199
    new-instance v2, Ljava/util/Vector;

    invoke-direct {v2}, Ljava/util/Vector;-><init>()V

    iput-object v2, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->textEleList:Ljava/util/List;

    .line 202
    :cond_13
    iget-object v3, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->textEleList:Ljava/util/List;

    new-instance v4, Lcom/samsung/app/video/editor/external/ClipartParams;

    iget-object v2, p1, Lcom/samsung/app/video/editor/external/TranscodeElement;->textEleList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/app/video/editor/external/ClipartParams;

    invoke-direct {v4, v2}, Lcom/samsung/app/video/editor/external/ClipartParams;-><init>(Lcom/samsung/app/video/editor/external/ClipartParams;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 197
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_6

    .line 206
    :cond_14
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->drawingEleList:Ljava/util/List;

    if-nez v2, :cond_15

    .line 207
    new-instance v2, Ljava/util/Vector;

    invoke-direct {v2}, Ljava/util/Vector;-><init>()V

    iput-object v2, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->drawingEleList:Ljava/util/List;

    .line 210
    :cond_15
    iget-object v3, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->drawingEleList:Ljava/util/List;

    .line 211
    new-instance v4, Lcom/samsung/app/video/editor/external/ClipartParams;

    iget-object v2, p1, Lcom/samsung/app/video/editor/external/TranscodeElement;->drawingEleList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/app/video/editor/external/ClipartParams;

    invoke-direct {v4, v2}, Lcom/samsung/app/video/editor/external/ClipartParams;-><init>(Lcom/samsung/app/video/editor/external/ClipartParams;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 205
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_7
.end method

.method private adjustMusicList(Ljava/util/List;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/Element;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 490
    .local p1, "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    if-eqz p1, :cond_0

    .line 491
    const-wide/16 v6, 0x0

    .line 492
    .local v6, "preendTime":J
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    .line 493
    .local v0, "count":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-lt v4, v0, :cond_1

    .line 507
    .end local v0    # "count":I
    .end local v4    # "i":I
    .end local v6    # "preendTime":J
    :cond_0
    return-void

    .line 494
    .restart local v0    # "count":I
    .restart local v4    # "i":I
    .restart local v6    # "preendTime":J
    :cond_1
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/app/video/editor/external/Element;

    .line 496
    .local v1, "element":Lcom/samsung/app/video/editor/external/Element;
    if-lez v4, :cond_2

    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/Element;->getStoryBoardStartTime()J

    move-result-wide v8

    cmp-long v8, v6, v8

    if-lez v8, :cond_2

    .line 497
    add-int/lit8 v8, v4, -0x1

    invoke-interface {p1, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/app/video/editor/external/Element;

    .line 499
    .local v5, "prevElement":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/Element;->getStoryBoardStartTime()J

    move-result-wide v8

    .line 498
    sub-long v2, v6, v8

    .line 500
    .local v2, "currentTimeDiffTime":J
    invoke-virtual {v5}, Lcom/samsung/app/video/editor/external/Element;->getStoryBoardEndTime()J

    move-result-wide v8

    .line 501
    sub-long/2addr v8, v2

    .line 500
    invoke-virtual {v5, v8, v9}, Lcom/samsung/app/video/editor/external/Element;->setStoryBoardEndTime(J)V

    .line 504
    .end local v2    # "currentTimeDiffTime":J
    .end local v5    # "prevElement":Lcom/samsung/app/video/editor/external/Element;
    :cond_2
    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/Element;->getStoryBoardEndTime()J

    move-result-wide v6

    .line 493
    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method

.method public static getBufferForEngineInitImage(Ljava/lang/String;II)[B
    .locals 9
    .param p0, "filePath"    # Ljava/lang/String;
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 2989
    const/4 v5, 0x1

    .line 2990
    .local v5, "needRotation":Z
    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getJNIVersion()F

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 2991
    const/4 v5, 0x0

    .line 2994
    :cond_0
    const/high16 v3, -0x1000000

    sget-object v4, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    move-object v0, p0

    move v1, p1

    move v2, p2

    .line 2993
    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/ve/common/MediaUtils;->getFitCenterBitmap(Ljava/lang/String;IIILandroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 2995
    .local v6, "bmp":Landroid/graphics/Bitmap;
    if-eqz v6, :cond_2

    .line 2996
    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    mul-int/2addr v0, v1

    mul-int/lit8 v8, v0, 0x2

    .line 2997
    .local v8, "iBytes":I
    invoke-static {v8}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v7

    .line 2998
    .local v7, "buffer":Ljava/nio/ByteBuffer;
    invoke-virtual {v6, v7}, Landroid/graphics/Bitmap;->copyPixelsToBuffer(Ljava/nio/Buffer;)V

    .line 2999
    invoke-virtual {v6}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 3000
    invoke-virtual {v6}, Landroid/graphics/Bitmap;->recycle()V

    .line 3001
    :cond_1
    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    .line 3003
    .end local v7    # "buffer":Ljava/nio/ByteBuffer;
    .end local v8    # "iBytes":I
    :goto_0
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getBufferForEngineInitImageARGB32(Ljava/lang/String;II)[B
    .locals 14
    .param p0, "filePath"    # Ljava/lang/String;
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 3008
    const/4 v5, 0x1

    .line 3009
    .local v5, "needRotation":Z
    const/4 v6, 0x0

    .line 3010
    .local v6, "bmp":Landroid/graphics/Bitmap;
    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getJNIVersion()F

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 3011
    const/4 v5, 0x0

    .line 3014
    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v12

    .line 3015
    .local v12, "path":Ljava/lang/String;
    const-string v0, "/"

    invoke-virtual {v12, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v10

    .line 3016
    .local v10, "k":I
    add-int/lit8 v0, v10, 0x1

    invoke-virtual {v12, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    .line 3019
    .local v8, "filename":Ljava/lang/String;
    const/4 v13, 0x0

    .line 3020
    .local v13, "temp":Landroid/graphics/Bitmap;
    sget-boolean v0, Lcom/sec/android/app/ve/VEApp;->EngineInitImageOnFly:Z

    if-eqz v0, :cond_1

    .line 3021
    sget-object v0, Lcom/sec/android/app/ve/VEApp;->gThumbFetcher:Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcher;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, p0, v1, v2}, Lcom/sec/android/app/ve/thumbcache/ThumbnailFetcher;->getImageThumbnailForTimeline(Ljava/lang/String;Ljava/lang/String;Z)Landroid/graphics/Bitmap;

    move-result-object v13

    .line 3022
    :cond_1
    if-nez v13, :cond_3

    .line 3024
    const/high16 v3, -0x1000000

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move-object v0, p0

    move v1, p1

    move/from16 v2, p2

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/ve/common/MediaUtils;->getFitCenterBitmap(Ljava/lang/String;IIILandroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 3033
    :goto_0
    if-eqz v6, :cond_5

    .line 3034
    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    mul-int/2addr v0, v1

    mul-int/lit8 v9, v0, 0x4

    .line 3035
    .local v9, "iBytes":I
    invoke-static {v9}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v7

    .line 3036
    .local v7, "buffer":Ljava/nio/ByteBuffer;
    invoke-virtual {v6, v7}, Landroid/graphics/Bitmap;->copyPixelsToBuffer(Ljava/nio/Buffer;)V

    .line 3037
    if-eqz v6, :cond_2

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_2

    .line 3038
    invoke-virtual {v6}, Landroid/graphics/Bitmap;->recycle()V

    .line 3039
    :cond_2
    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    .line 3041
    .end local v7    # "buffer":Ljava/nio/ByteBuffer;
    .end local v9    # "iBytes":I
    :goto_1
    return-object v0

    .line 3026
    :cond_3
    if-eqz v5, :cond_4

    .line 3028
    const/4 v0, 0x2

    invoke-static {p0, v0}, Lcom/sec/android/app/ve/common/MediaUtils;->getRotateDegree(Ljava/lang/String;I)I

    move-result v11

    .line 3029
    .local v11, "orientation":I
    invoke-static {v13, v11}, Lcom/sec/android/app/ve/common/MediaUtils;->getRotatedBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 3030
    goto :goto_0

    .line 3032
    .end local v11    # "orientation":I
    :cond_4
    move-object v6, v13

    goto :goto_0

    .line 3041
    :cond_5
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static isCaptionItemSame(Lcom/samsung/app/video/editor/external/ClipartParams;Lcom/samsung/app/video/editor/external/ClipartParams;)Z
    .locals 6
    .param p0, "original"    # Lcom/samsung/app/video/editor/external/ClipartParams;
    .param p1, "clone"    # Lcom/samsung/app/video/editor/external/ClipartParams;

    .prologue
    const/4 v0, 0x0

    .line 2849
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/ClipartParams;->getStoryBoardStartTime()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/ClipartParams;->getStoryBoardStartTime()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 2850
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/ClipartParams;->getStoryBoardEndTime()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/ClipartParams;->getStoryBoardEndTime()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 2851
    iget v1, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->themeId:I

    iget v2, p1, Lcom/samsung/app/video/editor/external/ClipartParams;->themeId:I

    if-ne v1, v2, :cond_0

    .line 2852
    iget-object v1, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->data:Ljava/lang/String;

    iget-object v2, p1, Lcom/samsung/app/video/editor/external/ClipartParams;->data:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->mCaptionTextAlignment:I

    iget v2, p1, Lcom/samsung/app/video/editor/external/ClipartParams;->mCaptionTextAlignment:I

    if-ne v1, v2, :cond_0

    .line 2853
    const/4 v0, 0x1

    .line 2867
    :cond_0
    return v0
.end method

.method public static isClipartParamsSame(Ljava/util/List;Ljava/util/List;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/ClipartParams;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/ClipartParams;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 2907
    .local p0, "origClips":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    .local p1, "newClips":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    const/4 v0, 0x0

    .line 2908
    .local v0, "bComparison":Z
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    .line 2909
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v3

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    if-ne v3, v4, :cond_0

    .line 2910
    const/4 v0, 0x1

    .line 2911
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    .line 2912
    .local v1, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-lt v2, v1, :cond_1

    .line 2920
    .end local v1    # "count":I
    .end local v2    # "i":I
    :cond_0
    :goto_1
    return v0

    .line 2913
    .restart local v1    # "count":I
    .restart local v2    # "i":I
    :cond_1
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/app/video/editor/external/ClipartParams;

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/app/video/editor/external/ClipartParams;

    invoke-static {v3, v4}, Lcom/samsung/app/video/editor/external/TranscodeElement;->isCaptionItemSame(Lcom/samsung/app/video/editor/external/ClipartParams;Lcom/samsung/app/video/editor/external/ClipartParams;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 2914
    const/4 v0, 0x0

    .line 2915
    goto :goto_1

    .line 2912
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private static isDrawingItemSame(Lcom/samsung/app/video/editor/external/ClipartParams;Lcom/samsung/app/video/editor/external/ClipartParams;)Z
    .locals 6
    .param p0, "original"    # Lcom/samsung/app/video/editor/external/ClipartParams;
    .param p1, "clone"    # Lcom/samsung/app/video/editor/external/ClipartParams;

    .prologue
    const/4 v0, 0x0

    .line 2833
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/ClipartParams;->getStoryBoardStartTime()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/ClipartParams;->getStoryBoardStartTime()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 2834
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/ClipartParams;->getStoryBoardEndTime()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/ClipartParams;->getStoryBoardEndTime()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 2835
    const/4 v0, 0x1

    .line 2842
    :cond_0
    return v0
.end method

.method public static isElementEditSame(Lcom/samsung/app/video/editor/external/Edit;Lcom/samsung/app/video/editor/external/Edit;)Z
    .locals 4
    .param p0, "original"    # Lcom/samsung/app/video/editor/external/Edit;
    .param p1, "clone"    # Lcom/samsung/app/video/editor/external/Edit;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2804
    if-eqz p0, :cond_3

    if-eqz p1, :cond_3

    .line 2805
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/Edit;->getType()I

    move-result v2

    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/Edit;->getType()I

    move-result v3

    if-ne v2, v3, :cond_4

    .line 2807
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/Edit;->getSubType()I

    move-result v2

    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/Edit;->getSubType()I

    move-result v3

    if-ne v2, v3, :cond_2

    .line 2809
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/Edit;->getTrans_duration()I

    move-result v2

    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/Edit;->getTrans_duration()I

    move-result v3

    if-ne v2, v3, :cond_1

    .line 2811
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/Edit;->getVolumeLevel()I

    move-result v2

    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/Edit;->getVolumeLevel()I

    move-result v3

    if-ne v2, v3, :cond_4

    .line 2827
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 2816
    goto :goto_0

    :cond_2
    move v0, v1

    .line 2820
    goto :goto_0

    .line 2824
    :cond_3
    if-nez p0, :cond_4

    if-eqz p1, :cond_0

    :cond_4
    move v0, v1

    .line 2827
    goto :goto_0
.end method

.method private static isElementItemSame(Lcom/samsung/app/video/editor/external/Element;Lcom/samsung/app/video/editor/external/Element;)Z
    .locals 8
    .param p0, "original"    # Lcom/samsung/app/video/editor/external/Element;
    .param p1, "clone"    # Lcom/samsung/app/video/editor/external/Element;

    .prologue
    const/4 v4, 0x0

    .line 2873
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/Element;->getStoryBoardStartTime()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/Element;->getStoryBoardStartTime()J

    move-result-wide v6

    cmp-long v2, v2, v6

    if-nez v2, :cond_5

    .line 2874
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/Element;->getStoryBoardEndTime()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/Element;->getStoryBoardEndTime()J

    move-result-wide v6

    cmp-long v2, v2, v6

    if-nez v2, :cond_4

    .line 2875
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/Element;->getFilePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/Element;->getFilePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2876
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/Element;->getEditListSize()I

    move-result v2

    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/Element;->getEditListSize()I

    move-result v3

    if-ne v2, v3, :cond_2

    .line 2877
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/Element;->getEditListSize()I

    move-result v0

    .line 2878
    .local v0, "editCount":I
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_0
    if-lt v1, v0, :cond_0

    .line 2887
    const/4 v2, 0x1

    .line 2900
    .end local v0    # "editCount":I
    .end local v1    # "j":I
    :goto_1
    return v2

    .line 2879
    .restart local v0    # "editCount":I
    .restart local v1    # "j":I
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/Element;->getEditList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/app/video/editor/external/Edit;

    .line 2880
    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/Element;->getEditList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/app/video/editor/external/Edit;

    .line 2879
    invoke-static {v2, v3}, Lcom/samsung/app/video/editor/external/TranscodeElement;->isElementEditSame(Lcom/samsung/app/video/editor/external/Edit;Lcom/samsung/app/video/editor/external/Edit;)Z

    move-result v2

    .line 2880
    if-eqz v2, :cond_1

    .line 2878
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    move v2, v4

    .line 2883
    goto :goto_1

    .end local v0    # "editCount":I
    .end local v1    # "j":I
    :cond_2
    move v2, v4

    .line 2889
    goto :goto_1

    :cond_3
    move v2, v4

    .line 2892
    goto :goto_1

    :cond_4
    move v2, v4

    .line 2896
    goto :goto_1

    :cond_5
    move v2, v4

    .line 2900
    goto :goto_1
.end method

.method public static isElementSame(Lcom/samsung/app/video/editor/external/Element;Lcom/samsung/app/video/editor/external/Element;)Z
    .locals 12
    .param p0, "original"    # Lcom/samsung/app/video/editor/external/Element;
    .param p1, "clone"    # Lcom/samsung/app/video/editor/external/Element;

    .prologue
    const/4 v7, 0x0

    .line 2738
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/Element;->getFilePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/Element;->getFilePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_b

    .line 2740
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/Element;->getEndTime()J

    move-result-wide v8

    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/Element;->getEndTime()J

    move-result-wide v10

    cmp-long v5, v8, v10

    if-nez v5, :cond_a

    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/Element;->getStartTime()J

    move-result-wide v8

    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/Element;->getStartTime()J

    move-result-wide v10

    cmp-long v5, v8, v10

    if-nez v5, :cond_a

    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/Element;->getSpeedForElement()I

    move-result v5

    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/Element;->getSpeedForElement()I

    move-result v6

    if-ne v5, v6, :cond_a

    .line 2742
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/Element;->getStartRect()Landroid/graphics/RectF;

    move-result-object v5

    iget v5, v5, Landroid/graphics/RectF;->left:F

    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/Element;->getStartRect()Landroid/graphics/RectF;

    move-result-object v6

    iget v6, v6, Landroid/graphics/RectF;->left:F

    cmpl-float v5, v5, v6

    if-nez v5, :cond_9

    .line 2743
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/Element;->getStartRect()Landroid/graphics/RectF;

    move-result-object v5

    iget v5, v5, Landroid/graphics/RectF;->top:F

    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/Element;->getStartRect()Landroid/graphics/RectF;

    move-result-object v6

    iget v6, v6, Landroid/graphics/RectF;->top:F

    cmpl-float v5, v5, v6

    if-nez v5, :cond_9

    .line 2744
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/Element;->getStartRect()Landroid/graphics/RectF;

    move-result-object v5

    iget v5, v5, Landroid/graphics/RectF;->right:F

    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/Element;->getStartRect()Landroid/graphics/RectF;

    move-result-object v6

    iget v6, v6, Landroid/graphics/RectF;->right:F

    cmpl-float v5, v5, v6

    if-nez v5, :cond_9

    .line 2745
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/Element;->getStartRect()Landroid/graphics/RectF;

    move-result-object v5

    iget v5, v5, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/Element;->getStartRect()Landroid/graphics/RectF;

    move-result-object v6

    iget v6, v6, Landroid/graphics/RectF;->bottom:F

    cmpl-float v5, v5, v6

    if-nez v5, :cond_9

    .line 2746
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/Element;->getEndRect()Landroid/graphics/RectF;

    move-result-object v5

    iget v5, v5, Landroid/graphics/RectF;->left:F

    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/Element;->getEndRect()Landroid/graphics/RectF;

    move-result-object v6

    iget v6, v6, Landroid/graphics/RectF;->left:F

    cmpl-float v5, v5, v6

    if-nez v5, :cond_9

    .line 2747
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/Element;->getEndRect()Landroid/graphics/RectF;

    move-result-object v5

    iget v5, v5, Landroid/graphics/RectF;->top:F

    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/Element;->getEndRect()Landroid/graphics/RectF;

    move-result-object v6

    iget v6, v6, Landroid/graphics/RectF;->top:F

    cmpl-float v5, v5, v6

    if-nez v5, :cond_9

    .line 2748
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/Element;->getEndRect()Landroid/graphics/RectF;

    move-result-object v5

    iget v5, v5, Landroid/graphics/RectF;->right:F

    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/Element;->getEndRect()Landroid/graphics/RectF;

    move-result-object v6

    iget v6, v6, Landroid/graphics/RectF;->right:F

    cmpl-float v5, v5, v6

    if-nez v5, :cond_9

    .line 2749
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/Element;->getEndRect()Landroid/graphics/RectF;

    move-result-object v5

    iget v5, v5, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/Element;->getEndRect()Landroid/graphics/RectF;

    move-result-object v6

    iget v6, v6, Landroid/graphics/RectF;->bottom:F

    cmpl-float v5, v5, v6

    if-nez v5, :cond_9

    .line 2750
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/Element;->getEditListSize()I

    move-result v5

    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/Element;->getEditListSize()I

    move-result v6

    if-ne v5, v6, :cond_8

    .line 2751
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/Element;->getEditListSize()I

    move-result v1

    .line 2752
    .local v1, "count":I
    const/4 v4, 0x0

    .local v4, "j":I
    :goto_0
    if-lt v4, v1, :cond_2

    .line 2761
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/Element;->getDrawingEleListCount()I

    move-result v5

    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/Element;->getDrawingEleListCount()I

    move-result v6

    if-ne v5, v6, :cond_0

    .line 2762
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/Element;->getDrawingEleListCount()I

    move-result v2

    .line 2763
    .local v2, "drawCount":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    if-lt v3, v2, :cond_4

    .line 2774
    .end local v2    # "drawCount":I
    .end local v3    # "i":I
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/Element;->getClipartListCount()I

    move-result v5

    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/Element;->getClipartListCount()I

    move-result v6

    if-ne v5, v6, :cond_1

    .line 2775
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/Element;->getClipartListCount()I

    move-result v0

    .line 2776
    .local v0, "clipCount":I
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_2
    if-lt v3, v0, :cond_6

    .line 2787
    .end local v0    # "clipCount":I
    .end local v3    # "i":I
    :cond_1
    const/4 v5, 0x1

    .line 2800
    .end local v1    # "count":I
    .end local v4    # "j":I
    :goto_3
    return v5

    .line 2753
    .restart local v1    # "count":I
    .restart local v4    # "j":I
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/Element;->getEditList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/app/video/editor/external/Edit;

    .line 2754
    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/Element;->getEditList()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/app/video/editor/external/Edit;

    .line 2753
    invoke-static {v5, v6}, Lcom/samsung/app/video/editor/external/TranscodeElement;->isElementEditSame(Lcom/samsung/app/video/editor/external/Edit;Lcom/samsung/app/video/editor/external/Edit;)Z

    move-result v5

    .line 2754
    if-eqz v5, :cond_3

    .line 2752
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_3
    move v5, v7

    .line 2757
    goto :goto_3

    .line 2765
    .restart local v2    # "drawCount":I
    .restart local v3    # "i":I
    :cond_4
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/Element;->getDrawingEleList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/app/video/editor/external/ClipartParams;

    .line 2766
    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/Element;->getDrawingEleList()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/app/video/editor/external/ClipartParams;

    .line 2764
    invoke-static {v5, v6}, Lcom/samsung/app/video/editor/external/TranscodeElement;->isDrawingItemSame(Lcom/samsung/app/video/editor/external/ClipartParams;Lcom/samsung/app/video/editor/external/ClipartParams;)Z

    move-result v5

    .line 2766
    if-eqz v5, :cond_5

    .line 2763
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_5
    move v5, v7

    .line 2769
    goto :goto_3

    .line 2778
    .end local v2    # "drawCount":I
    .restart local v0    # "clipCount":I
    :cond_6
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/Element;->getClipartList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/app/video/editor/external/ClipartParams;

    .line 2779
    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/Element;->getClipartList()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/app/video/editor/external/ClipartParams;

    .line 2777
    invoke-static {v5, v6}, Lcom/samsung/app/video/editor/external/TranscodeElement;->isCaptionItemSame(Lcom/samsung/app/video/editor/external/ClipartParams;Lcom/samsung/app/video/editor/external/ClipartParams;)Z

    move-result v5

    .line 2779
    if-eqz v5, :cond_7

    .line 2776
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_7
    move v5, v7

    .line 2782
    goto :goto_3

    .end local v0    # "clipCount":I
    .end local v1    # "count":I
    .end local v3    # "i":I
    .end local v4    # "j":I
    :cond_8
    move v5, v7

    .line 2789
    goto :goto_3

    :cond_9
    move v5, v7

    .line 2793
    goto :goto_3

    :cond_a
    move v5, v7

    .line 2796
    goto :goto_3

    :cond_b
    move v5, v7

    .line 2800
    goto :goto_3
.end method

.method public static isTranscodeContentSame(Lcom/samsung/app/video/editor/external/TranscodeElement;Lcom/samsung/app/video/editor/external/TranscodeElement;)Z
    .locals 10
    .param p0, "original"    # Lcom/samsung/app/video/editor/external/TranscodeElement;
    .param p1, "clone"    # Lcom/samsung/app/video/editor/external/TranscodeElement;

    .prologue
    const/4 v9, 0x0

    .line 2632
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getThemeName()I

    move-result v7

    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getThemeName()I

    move-result v8

    if-ne v7, v8, :cond_f

    .line 2634
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v7

    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v8

    if-ne v7, v8, :cond_e

    .line 2635
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v1

    .line 2636
    .local v1, "count":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-lt v3, v1, :cond_0

    .line 2647
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getTextEleListCount()I

    move-result v7

    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getTextEleListCount()I

    move-result v8

    if-ne v7, v8, :cond_4

    .line 2648
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getTextEleListCount()I

    move-result v6

    .line 2649
    .local v6, "textCount":I
    const/4 v3, 0x0

    :goto_1
    if-lt v3, v6, :cond_2

    .line 2664
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getSoundListCount()I

    move-result v7

    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getSoundListCount()I

    move-result v8

    if-ne v7, v8, :cond_7

    .line 2665
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getSoundListCount()I

    move-result v5

    .line 2666
    .local v5, "soundCount":I
    const/4 v3, 0x0

    :goto_2
    if-lt v3, v5, :cond_5

    .line 2684
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getRecordListCount()I

    move-result v7

    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getRecordListCount()I

    move-result v8

    if-ne v7, v8, :cond_a

    .line 2685
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getRecordListCount()I

    move-result v4

    .line 2686
    .local v4, "recCount":I
    const/4 v3, 0x0

    :goto_3
    if-lt v3, v4, :cond_8

    .line 2704
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getAudioListCount()I

    move-result v7

    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getAudioListCount()I

    move-result v8

    if-ne v7, v8, :cond_d

    .line 2705
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getAudioListCount()I

    move-result v0

    .line 2706
    .local v0, "audioCount":I
    const/4 v3, 0x0

    :goto_4
    if-lt v3, v0, :cond_b

    .line 2721
    const/4 v7, 0x1

    .line 2733
    .end local v0    # "audioCount":I
    .end local v1    # "count":I
    .end local v3    # "i":I
    .end local v4    # "recCount":I
    .end local v5    # "soundCount":I
    .end local v6    # "textCount":I
    :goto_5
    return v7

    .line 2638
    .restart local v1    # "count":I
    .restart local v3    # "i":I
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementList()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/app/video/editor/external/Element;

    .line 2639
    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementList()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/app/video/editor/external/Element;

    .line 2638
    invoke-static {v7, v8}, Lcom/samsung/app/video/editor/external/TranscodeElement;->isElementSame(Lcom/samsung/app/video/editor/external/Element;Lcom/samsung/app/video/editor/external/Element;)Z

    move-result v7

    .line 2639
    if-eqz v7, :cond_1

    .line 2636
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    move v7, v9

    .line 2642
    goto :goto_5

    .line 2651
    .restart local v6    # "textCount":I
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getTextEleList()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/app/video/editor/external/ClipartParams;

    .line 2652
    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getTextEleList()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/app/video/editor/external/ClipartParams;

    .line 2650
    invoke-static {v7, v8}, Lcom/samsung/app/video/editor/external/TranscodeElement;->isCaptionItemSame(Lcom/samsung/app/video/editor/external/ClipartParams;Lcom/samsung/app/video/editor/external/ClipartParams;)Z

    move-result v7

    .line 2652
    if-eqz v7, :cond_3

    .line 2649
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_3
    move v7, v9

    .line 2655
    goto :goto_5

    .end local v6    # "textCount":I
    :cond_4
    move v7, v9

    .line 2660
    goto :goto_5

    .line 2668
    .restart local v5    # "soundCount":I
    .restart local v6    # "textCount":I
    :cond_5
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getAdditionlSoundEleList()Ljava/util/List;

    move-result-object v7

    .line 2669
    invoke-interface {v7, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/app/video/editor/external/Element;

    .line 2670
    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getAdditionlSoundEleList()Ljava/util/List;

    move-result-object v8

    .line 2671
    invoke-interface {v8, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/app/video/editor/external/Element;

    .line 2667
    invoke-static {v7, v8}, Lcom/samsung/app/video/editor/external/TranscodeElement;->isElementItemSame(Lcom/samsung/app/video/editor/external/Element;Lcom/samsung/app/video/editor/external/Element;)Z

    move-result v7

    .line 2671
    if-eqz v7, :cond_6

    .line 2666
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2

    :cond_6
    move v7, v9

    .line 2674
    goto :goto_5

    .end local v5    # "soundCount":I
    :cond_7
    move v7, v9

    .line 2679
    goto :goto_5

    .line 2688
    .restart local v4    # "recCount":I
    .restart local v5    # "soundCount":I
    :cond_8
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getAdditionlRecordEleList()Ljava/util/List;

    move-result-object v7

    .line 2689
    invoke-interface {v7, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/app/video/editor/external/Element;

    .line 2690
    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getAdditionlRecordEleList()Ljava/util/List;

    move-result-object v8

    .line 2691
    invoke-interface {v8, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/app/video/editor/external/Element;

    .line 2687
    invoke-static {v7, v8}, Lcom/samsung/app/video/editor/external/TranscodeElement;->isElementItemSame(Lcom/samsung/app/video/editor/external/Element;Lcom/samsung/app/video/editor/external/Element;)Z

    move-result v7

    .line 2691
    if-eqz v7, :cond_9

    .line 2686
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_3

    :cond_9
    move v7, v9

    .line 2694
    goto/16 :goto_5

    .end local v4    # "recCount":I
    :cond_a
    move v7, v9

    .line 2699
    goto/16 :goto_5

    .line 2708
    .restart local v0    # "audioCount":I
    .restart local v4    # "recCount":I
    :cond_b
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getAdditionlAudioEleList()Ljava/util/List;

    move-result-object v7

    .line 2709
    invoke-interface {v7, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/app/video/editor/external/Element;

    .line 2710
    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getAdditionlAudioEleList()Ljava/util/List;

    move-result-object v8

    .line 2711
    invoke-interface {v8, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/app/video/editor/external/Element;

    .line 2707
    invoke-static {v7, v8}, Lcom/samsung/app/video/editor/external/TranscodeElement;->isElementItemSame(Lcom/samsung/app/video/editor/external/Element;Lcom/samsung/app/video/editor/external/Element;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v7

    .line 2711
    if-eqz v7, :cond_c

    .line 2706
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_4

    :cond_c
    move v7, v9

    .line 2714
    goto/16 :goto_5

    .end local v0    # "audioCount":I
    :cond_d
    move v7, v9

    .line 2719
    goto/16 :goto_5

    .end local v1    # "count":I
    .end local v3    # "i":I
    .end local v4    # "recCount":I
    .end local v5    # "soundCount":I
    .end local v6    # "textCount":I
    :cond_e
    move v7, v9

    .line 2723
    goto/16 :goto_5

    :cond_f
    move v7, v9

    .line 2726
    goto/16 :goto_5

    .line 2730
    :catch_0
    move-exception v2

    .line 2731
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    move v7, v9

    .line 2733
    goto/16 :goto_5
.end method

.method public static isTranscodeSame(Lcom/samsung/app/video/editor/external/TranscodeElement;Lcom/samsung/app/video/editor/external/TranscodeElement;)Z
    .locals 3
    .param p0, "original"    # Lcom/samsung/app/video/editor/external/TranscodeElement;
    .param p1, "clone"    # Lcom/samsung/app/video/editor/external/TranscodeElement;

    .prologue
    .line 2616
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getProjectName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getProjectName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2618
    invoke-static {p0, p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->isTranscodeContentSame(Lcom/samsung/app/video/editor/external/TranscodeElement;Lcom/samsung/app/video/editor/external/TranscodeElement;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2625
    :goto_0
    return v1

    .line 2621
    :catch_0
    move-exception v0

    .line 2622
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 2625
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private recheckTranstionElements()V
    .locals 6

    .prologue
    .line 1749
    const/4 v2, 0x0

    .line 1750
    .local v2, "i":I
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v5

    add-int/lit8 v0, v5, -0x1

    .line 1751
    .local v0, "count":I
    :goto_0
    if-lt v2, v0, :cond_0

    .line 1764
    .end local v0    # "count":I
    :goto_1
    return-void

    .line 1752
    .restart local v0    # "count":I
    :cond_0
    iget-object v5, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->elementList:Ljava/util/List;

    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/app/video/editor/external/Element;

    .line 1753
    .local v3, "lElement":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/Element;->getTransitionEdit()Lcom/samsung/app/video/editor/external/Edit;

    move-result-object v5

    if-nez v5, :cond_1

    .line 1754
    new-instance v4, Lcom/samsung/app/video/editor/external/Edit;

    invoke-direct {v4}, Lcom/samsung/app/video/editor/external/Edit;-><init>()V

    .line 1755
    .local v4, "transEdit":Lcom/samsung/app/video/editor/external/Edit;
    const/4 v5, 0x6

    invoke-virtual {v4, v5}, Lcom/samsung/app/video/editor/external/Edit;->setType(I)V

    .line 1756
    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/samsung/app/video/editor/external/Edit;->setSubType(I)V

    .line 1757
    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/samsung/app/video/editor/external/Edit;->setTrans_duration(I)V

    .line 1758
    invoke-virtual {v3, v4}, Lcom/samsung/app/video/editor/external/Element;->addEdit(Lcom/samsung/app/video/editor/external/Edit;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1751
    .end local v4    # "transEdit":Lcom/samsung/app/video/editor/external/Edit;
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1761
    .end local v0    # "count":I
    .end local v3    # "lElement":Lcom/samsung/app/video/editor/external/Element;
    :catch_0
    move-exception v1

    .line 1762
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method private removeDrawingClipartParamsForElement(I)V
    .locals 4
    .param p1, "id"    # I

    .prologue
    .line 1828
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getDrawingEleListCount()I

    move-result v0

    .line 1829
    .local v0, "count":I
    add-int/lit8 v2, v0, -0x1

    .local v2, "i":I
    :goto_0
    if-gez v2, :cond_0

    .line 1835
    return-void

    .line 1830
    :cond_0
    iget-object v3, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->drawingEleList:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/app/video/editor/external/ClipartParams;

    .line 1831
    .local v1, "draw":Lcom/samsung/app/video/editor/external/ClipartParams;
    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/ClipartParams;->getElementID()I

    move-result v3

    if-ne v3, p1, :cond_1

    .line 1832
    invoke-virtual {p0, v1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->removeDrawingEleList(Lcom/samsung/app/video/editor/external/ClipartParams;)V

    .line 1829
    :cond_1
    add-int/lit8 v2, v2, -0x1

    goto :goto_0
.end method

.method private removeTextClipartParamsForElement(I)V
    .locals 4
    .param p1, "id"    # I

    .prologue
    .line 1819
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getTextEleListCount()I

    move-result v0

    .line 1820
    .local v0, "count":I
    add-int/lit8 v1, v0, -0x1

    .local v1, "i":I
    :goto_0
    if-gez v1, :cond_0

    .line 1825
    return-void

    .line 1821
    :cond_0
    iget-object v3, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->textEleList:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/app/video/editor/external/ClipartParams;

    .line 1822
    .local v2, "text":Lcom/samsung/app/video/editor/external/ClipartParams;
    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/ClipartParams;->getElementID()I

    move-result v3

    if-ne v3, p1, :cond_1

    .line 1823
    invoke-virtual {p0, v2}, Lcom/samsung/app/video/editor/external/TranscodeElement;->removeTextEleList(Lcom/samsung/app/video/editor/external/ClipartParams;)V

    .line 1820
    :cond_1
    add-int/lit8 v1, v1, -0x1

    goto :goto_0
.end method

.method private setOverLapTransitionFlag()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 378
    iget-object v5, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->elementList:Ljava/util/List;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->elementList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    const/4 v6, 0x1

    if-gt v5, v6, :cond_1

    .line 404
    :cond_0
    :goto_0
    return-void

    .line 381
    :cond_1
    const/4 v2, 0x0

    .line 385
    .local v2, "i":I
    iget-object v5, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->elementList:Ljava/util/List;

    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/app/video/editor/external/Element;

    .line 386
    .local v1, "current":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {v1, v7}, Lcom/samsung/app/video/editor/external/Element;->setNonTimeLossTransitionAtStart(Z)V

    .line 387
    invoke-virtual {v1, v7}, Lcom/samsung/app/video/editor/external/Element;->setOverLapAtStart(I)V

    .line 388
    invoke-virtual {p0, v1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->isOverLapTransitionEnabled(Lcom/samsung/app/video/editor/external/Element;)I

    move-result v4

    .line 389
    .local v4, "overLapVal":I
    invoke-virtual {v1, v4}, Lcom/samsung/app/video/editor/external/Element;->setOverLapAtEnd(I)V

    .line 390
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v5

    add-int/lit8 v0, v5, -0x1

    .line 391
    .local v0, "count":I
    const/4 v2, 0x1

    :goto_1
    if-lt v2, v0, :cond_2

    .line 399
    iget-object v5, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->elementList:Ljava/util/List;

    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/app/video/editor/external/Element;

    .line 400
    .local v3, "nextElement":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {v3, v4}, Lcom/samsung/app/video/editor/external/Element;->setOverLapAtStart(I)V

    .line 401
    invoke-virtual {v3, v7}, Lcom/samsung/app/video/editor/external/Element;->setOverLapAtEnd(I)V

    .line 402
    invoke-virtual {v3, v7}, Lcom/samsung/app/video/editor/external/Element;->setNonTimeLossTransitionAtEnd(Z)V

    goto :goto_0

    .line 392
    .end local v3    # "nextElement":Lcom/samsung/app/video/editor/external/Element;
    :cond_2
    iget-object v5, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->elementList:Ljava/util/List;

    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/app/video/editor/external/Element;

    .line 395
    .restart local v3    # "nextElement":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {v3, v4}, Lcom/samsung/app/video/editor/external/Element;->setOverLapAtStart(I)V

    .line 396
    invoke-virtual {p0, v3}, Lcom/samsung/app/video/editor/external/TranscodeElement;->isOverLapTransitionEnabled(Lcom/samsung/app/video/editor/external/Element;)I

    move-result v4

    .line 397
    invoke-virtual {v3, v4}, Lcom/samsung/app/video/editor/external/Element;->setOverLapAtEnd(I)V

    .line 391
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method private setUniqueID(Lcom/samsung/app/video/editor/external/Element;)V
    .locals 2
    .param p1, "element"    # Lcom/samsung/app/video/editor/external/Element;

    .prologue
    .line 314
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getThemeName()I

    move-result v1

    if-nez v1, :cond_0

    .line 315
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getMaxGroupID()I

    move-result v1

    add-int/lit8 v0, v1, 0x1

    .line 316
    .local v0, "id":I
    invoke-virtual {p1, v0}, Lcom/samsung/app/video/editor/external/Element;->setGroupID(I)V

    .line 318
    .end local v0    # "id":I
    :cond_0
    return-void
.end method


# virtual methods
.method public addATempAudioEleList(Lcom/samsung/app/video/editor/external/Element;)V
    .locals 1
    .param p1, "element"    # Lcom/samsung/app/video/editor/external/Element;

    .prologue
    .line 248
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->tempAudioEleList:Ljava/util/List;

    if-nez v0, :cond_0

    .line 249
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->tempAudioEleList:Ljava/util/List;

    .line 250
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->tempAudioEleList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 255
    :goto_0
    return-void

    .line 252
    :cond_0
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->tempAudioEleList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 253
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->tempAudioEleList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public addAdditionlAudioEleList(Lcom/samsung/app/video/editor/external/Element;)V
    .locals 1
    .param p1, "element"    # Lcom/samsung/app/video/editor/external/Element;

    .prologue
    .line 233
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlAudioEleList:Ljava/util/List;

    if-nez v0, :cond_0

    .line 234
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlAudioEleList:Ljava/util/List;

    .line 235
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlAudioEleList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 241
    :goto_0
    return-void

    .line 238
    :cond_0
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlAudioEleList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 239
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlAudioEleList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public addDrawingClipartParamsForElement(Lcom/samsung/app/video/editor/external/Element;Ljava/util/List;)V
    .locals 16
    .param p1, "element"    # Lcom/samsung/app/video/editor/external/Element;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/app/video/editor/external/Element;",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/ClipartParams;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2155
    .local p2, "drawClipart":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/app/video/editor/external/TranscodeElement;->drawingEleList:Ljava/util/List;

    if-eqz v7, :cond_0

    .line 2156
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v2

    .line 2157
    .local v2, "count":I
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    if-lt v6, v2, :cond_1

    .line 2170
    .end local v2    # "count":I
    .end local v6    # "i":I
    :cond_0
    return-void

    .line 2158
    .restart local v2    # "count":I
    .restart local v6    # "i":I
    :cond_1
    new-instance v3, Lcom/samsung/app/video/editor/external/ClipartParams;

    move-object/from16 v0, p2

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/app/video/editor/external/ClipartParams;

    invoke-direct {v3, v7}, Lcom/samsung/app/video/editor/external/ClipartParams;-><init>(Lcom/samsung/app/video/editor/external/ClipartParams;)V

    .line 2159
    .local v3, "draw":Lcom/samsung/app/video/editor/external/ClipartParams;
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getStoryTimeTillElement(Lcom/samsung/app/video/editor/external/Element;)J

    move-result-wide v10

    .line 2161
    .local v10, "startTime":J
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/app/video/editor/external/Element;->getOverLapAtEnd()I

    move-result v7

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/app/video/editor/external/Element;->getOverLapAtStart()I

    move-result v12

    add-int/2addr v7, v12

    int-to-long v8, v7

    .line 2162
    .local v8, "overLapTime":J
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/app/video/editor/external/Element;->getEndTime()J

    move-result-wide v12

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/app/video/editor/external/Element;->getStartTime()J

    move-result-wide v14

    sub-long/2addr v12, v14

    long-to-float v7, v12

    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/ve/util/CommonUtils;->getSpeedFactor(Lcom/samsung/app/video/editor/external/Element;)F

    move-result v12

    mul-float/2addr v7, v12

    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v7

    int-to-long v12, v7

    add-long/2addr v12, v10

    sub-long v4, v12, v8

    .line 2164
    .local v4, "endTime":J
    long-to-float v7, v10

    const/high16 v12, 0x447a0000    # 1000.0f

    div-float/2addr v7, v12

    const/high16 v12, 0x41f00000    # 30.0f

    mul-float/2addr v7, v12

    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v7

    invoke-virtual {v3, v7}, Lcom/samsung/app/video/editor/external/ClipartParams;->setStoryboardStartFrame(I)V

    .line 2165
    long-to-float v7, v4

    const/high16 v12, 0x447a0000    # 1000.0f

    div-float/2addr v7, v12

    const/high16 v12, 0x41f00000    # 30.0f

    mul-float/2addr v7, v12

    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-virtual {v3, v7}, Lcom/samsung/app/video/editor/external/ClipartParams;->setStoryboardEndFrame(I)V

    .line 2166
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/app/video/editor/external/Element;->getGroupID()I

    move-result v7

    invoke-virtual {v3, v7}, Lcom/samsung/app/video/editor/external/ClipartParams;->setElementID(I)V

    .line 2167
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/app/video/editor/external/TranscodeElement;->drawingEleList:Ljava/util/List;

    invoke-interface {v7, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2157
    add-int/lit8 v6, v6, 0x1

    goto :goto_0
.end method

.method public addDrawingEleList(Lcom/samsung/app/video/editor/external/ClipartParams;)V
    .locals 1
    .param p1, "clipParams"    # Lcom/samsung/app/video/editor/external/ClipartParams;

    .prologue
    .line 258
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->addDrawingEleList(Lcom/samsung/app/video/editor/external/ClipartParams;Z)V

    .line 259
    return-void
.end method

.method public addDrawingEleList(Lcom/samsung/app/video/editor/external/ClipartParams;Z)V
    .locals 2
    .param p1, "clipParams"    # Lcom/samsung/app/video/editor/external/ClipartParams;
    .param p2, "updateTimeInfo"    # Z

    .prologue
    .line 263
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->drawingEleList:Ljava/util/List;

    if-nez v0, :cond_1

    .line 264
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->drawingEleList:Ljava/util/List;

    .line 265
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->drawingEleList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 270
    :goto_0
    if-eqz p2, :cond_0

    .line 271
    iget-boolean v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->isSummaryProject:Z

    if-eqz v0, :cond_2

    .line 272
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->updateDrawingTimeinSummary()V

    .line 277
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->drawingEleList:Ljava/util/List;

    sget-object v1, Lcom/samsung/app/video/editor/external/TranscodeElement;->mDrawingListComparator:Lcom/samsung/app/video/editor/external/TranscodeElement$DrawingListComparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 279
    return-void

    .line 267
    :cond_1
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->drawingEleList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 274
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->updateDrawingTime()V

    goto :goto_1
.end method

.method public addElement(Lcom/samsung/app/video/editor/external/Element;)V
    .locals 1
    .param p1, "element"    # Lcom/samsung/app/video/editor/external/Element;

    .prologue
    .line 284
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->addElement(Lcom/samsung/app/video/editor/external/Element;Z)V

    .line 285
    return-void
.end method

.method public addElement(Lcom/samsung/app/video/editor/external/Element;Z)V
    .locals 1
    .param p1, "element"    # Lcom/samsung/app/video/editor/external/Element;
    .param p2, "clearTrans"    # Z

    .prologue
    .line 292
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->elementList:Ljava/util/List;

    if-nez v0, :cond_0

    .line 293
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->elementList:Ljava/util/List;

    .line 296
    :cond_0
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->elementList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 297
    if-eqz p2, :cond_1

    .line 298
    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/Element;->getTransitionEdit()Lcom/samsung/app/video/editor/external/Edit;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 299
    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/Element;->removetransitionEdit()V

    .line 302
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/Element;->getFilePath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/ve/util/CommonUtils;->isUHD(Ljava/lang/String;)Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/samsung/app/video/editor/external/Element;->setUHDinfo(Z)V

    .line 303
    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/Element;->getFilePath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/ve/util/CommonUtils;->isWQHD(Ljava/lang/String;)Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/samsung/app/video/editor/external/Element;->setWQHDinfo(Z)V

    .line 304
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->elementList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 306
    :cond_2
    iget-boolean v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->isSummaryProject:Z

    if-nez v0, :cond_3

    .line 307
    invoke-direct {p0, p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->setUniqueID(Lcom/samsung/app/video/editor/external/Element;)V

    .line 308
    :cond_3
    invoke-direct {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->recheckTranstionElements()V

    .line 309
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->overlapTransitionsHandling()V

    .line 310
    return-void
.end method

.method public addMusicEleList(Lcom/samsung/app/video/editor/external/Element;)V
    .locals 2
    .param p1, "element"    # Lcom/samsung/app/video/editor/external/Element;

    .prologue
    .line 407
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlAudioEleList:Ljava/util/List;

    if-nez v0, :cond_0

    .line 408
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlAudioEleList:Ljava/util/List;

    .line 409
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlAudioEleList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 414
    :goto_0
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlAudioEleList:Ljava/util/List;

    sget-object v1, Lcom/samsung/app/video/editor/external/TranscodeElement;->mElementListComparator:Lcom/samsung/app/video/editor/external/TranscodeElement$ElementListComparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 416
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlAudioEleList:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->adjustMusicList(Ljava/util/List;)V

    .line 417
    return-void

    .line 411
    :cond_0
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlAudioEleList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public addMusicTempEleList(Lcom/samsung/app/video/editor/external/Element;)V
    .locals 2
    .param p1, "element"    # Lcom/samsung/app/video/editor/external/Element;

    .prologue
    .line 420
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->tempAudioEleList:Ljava/util/List;

    if-nez v0, :cond_0

    .line 421
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->tempAudioEleList:Ljava/util/List;

    .line 422
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->tempAudioEleList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 426
    :goto_0
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->tempAudioEleList:Ljava/util/List;

    sget-object v1, Lcom/samsung/app/video/editor/external/TranscodeElement;->mElementListComparator:Lcom/samsung/app/video/editor/external/TranscodeElement$ElementListComparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 428
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->tempAudioEleList:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->adjustMusicList(Ljava/util/List;)V

    .line 429
    return-void

    .line 424
    :cond_0
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->tempAudioEleList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public addRecordEleList(Lcom/samsung/app/video/editor/external/Element;)V
    .locals 2
    .param p1, "element"    # Lcom/samsung/app/video/editor/external/Element;

    .prologue
    .line 432
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlNarrationEleList:Ljava/util/List;

    if-nez v0, :cond_0

    .line 433
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlNarrationEleList:Ljava/util/List;

    .line 434
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlNarrationEleList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 439
    :goto_0
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlNarrationEleList:Ljava/util/List;

    sget-object v1, Lcom/samsung/app/video/editor/external/TranscodeElement;->mElementListComparator:Lcom/samsung/app/video/editor/external/TranscodeElement$ElementListComparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 441
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlNarrationEleList:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->adjustMusicList(Ljava/util/List;)V

    .line 442
    return-void

    .line 436
    :cond_0
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlNarrationEleList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public addSoundEleList(Lcom/samsung/app/video/editor/external/Element;)V
    .locals 2
    .param p1, "element"    # Lcom/samsung/app/video/editor/external/Element;

    .prologue
    .line 445
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlSoundEleList:Ljava/util/List;

    if-nez v0, :cond_0

    .line 446
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlSoundEleList:Ljava/util/List;

    .line 447
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlSoundEleList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 452
    :goto_0
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlSoundEleList:Ljava/util/List;

    sget-object v1, Lcom/samsung/app/video/editor/external/TranscodeElement;->mElementListComparator:Lcom/samsung/app/video/editor/external/TranscodeElement$ElementListComparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 454
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlSoundEleList:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->adjustMusicList(Ljava/util/List;)V

    .line 455
    return-void

    .line 449
    :cond_0
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlSoundEleList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public addTextClipartParamsForElement(Lcom/samsung/app/video/editor/external/Element;Ljava/util/List;)V
    .locals 16
    .param p1, "element"    # Lcom/samsung/app/video/editor/external/Element;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/app/video/editor/external/Element;",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/ClipartParams;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2110
    .local p2, "clipart":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/app/video/editor/external/TranscodeElement;->textEleList:Ljava/util/List;

    if-eqz v11, :cond_0

    .line 2111
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v2

    .line 2112
    .local v2, "count":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-lt v3, v2, :cond_1

    .line 2126
    .end local v2    # "count":I
    .end local v3    # "i":I
    :cond_0
    return-void

    .line 2113
    .restart local v2    # "count":I
    .restart local v3    # "i":I
    :cond_1
    new-instance v10, Lcom/samsung/app/video/editor/external/ClipartParams;

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/samsung/app/video/editor/external/ClipartParams;

    invoke-direct {v10, v11}, Lcom/samsung/app/video/editor/external/ClipartParams;-><init>(Lcom/samsung/app/video/editor/external/ClipartParams;)V

    .line 2114
    .local v10, "text":Lcom/samsung/app/video/editor/external/ClipartParams;
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getStoryTimeTillElement(Lcom/samsung/app/video/editor/external/Element;)J

    move-result-wide v8

    .line 2116
    .local v8, "startTime":J
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/app/video/editor/external/Element;->getOverLapAtEnd()I

    move-result v11

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/app/video/editor/external/Element;->getOverLapAtStart()I

    move-result v12

    add-int/2addr v11, v12

    int-to-long v6, v11

    .line 2117
    .local v6, "overLapTime":J
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/app/video/editor/external/Element;->getEndTime()J

    move-result-wide v12

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/app/video/editor/external/Element;->getStartTime()J

    move-result-wide v14

    sub-long/2addr v12, v14

    long-to-float v11, v12

    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/ve/util/CommonUtils;->getSpeedFactor(Lcom/samsung/app/video/editor/external/Element;)F

    move-result v12

    mul-float/2addr v11, v12

    invoke-static {v11}, Ljava/lang/Math;->round(F)I

    move-result v11

    int-to-long v12, v11

    add-long/2addr v12, v8

    sub-long v4, v12, v6

    .line 2119
    .local v4, "endTime":J
    long-to-float v11, v8

    const/high16 v12, 0x447a0000    # 1000.0f

    div-float/2addr v11, v12

    const/high16 v12, 0x41f00000    # 30.0f

    mul-float/2addr v11, v12

    invoke-static {v11}, Ljava/lang/Math;->round(F)I

    move-result v11

    invoke-virtual {v10, v11}, Lcom/samsung/app/video/editor/external/ClipartParams;->setStoryboardStartFrame(I)V

    .line 2120
    long-to-float v11, v4

    const/high16 v12, 0x447a0000    # 1000.0f

    div-float/2addr v11, v12

    const/high16 v12, 0x41f00000    # 30.0f

    mul-float/2addr v11, v12

    invoke-static {v11}, Ljava/lang/Math;->round(F)I

    move-result v11

    add-int/lit8 v11, v11, -0x1

    invoke-virtual {v10, v11}, Lcom/samsung/app/video/editor/external/ClipartParams;->setStoryboardEndFrame(I)V

    .line 2121
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/app/video/editor/external/Element;->getGroupID()I

    move-result v11

    invoke-virtual {v10, v11}, Lcom/samsung/app/video/editor/external/ClipartParams;->setElementID(I)V

    .line 2122
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v10}, Lcom/samsung/app/video/editor/external/TranscodeElement;->updateAnimationsForCaptions(Lcom/samsung/app/video/editor/external/Element;Lcom/samsung/app/video/editor/external/ClipartParams;)V

    .line 2123
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/app/video/editor/external/TranscodeElement;->textEleList:Ljava/util/List;

    invoke-interface {v11, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2112
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public addTextEleList(Lcom/samsung/app/video/editor/external/ClipartParams;)V
    .locals 1
    .param p1, "clipParams"    # Lcom/samsung/app/video/editor/external/ClipartParams;

    .prologue
    .line 458
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->addTextEleList(Lcom/samsung/app/video/editor/external/ClipartParams;Z)V

    .line 459
    return-void
.end method

.method public addTextEleList(Lcom/samsung/app/video/editor/external/ClipartParams;Z)V
    .locals 2
    .param p1, "clipParams"    # Lcom/samsung/app/video/editor/external/ClipartParams;
    .param p2, "updateTimeInfo"    # Z

    .prologue
    .line 462
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->textEleList:Ljava/util/List;

    if-nez v0, :cond_1

    .line 463
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->textEleList:Ljava/util/List;

    .line 464
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->textEleList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 468
    :goto_0
    if-eqz p2, :cond_0

    .line 469
    iget-boolean v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->isSummaryProject:Z

    if-eqz v0, :cond_2

    .line 470
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->updateCaptionsTimeinSummary()V

    .line 474
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->textEleList:Ljava/util/List;

    sget-object v1, Lcom/samsung/app/video/editor/external/TranscodeElement;->mTextListComparator:Lcom/samsung/app/video/editor/external/TranscodeElement$TextListComparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 475
    return-void

    .line 466
    :cond_1
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->textEleList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 472
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->updateCaptionsTime()V

    goto :goto_1
.end method

.method public calculateMinTimeForAutomaking()F
    .locals 24

    .prologue
    .line 510
    const/4 v12, 0x0

    .line 511
    .local v12, "minTime":F
    const/4 v9, 0x0

    .line 512
    .local v9, "index":I
    const/4 v8, 0x0

    .line 513
    .local v8, "i":I
    const/4 v11, 0x0

    .line 514
    .local v11, "lastRecording":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v2

    .line 515
    .local v2, "count":I
    :goto_0
    if-lt v8, v2, :cond_0

    .line 599
    return v12

    .line 516
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/app/video/editor/external/TranscodeElement;->elementList:Ljava/util/List;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/app/video/editor/external/Element;

    .line 517
    .local v3, "current":Lcom/samsung/app/video/editor/external/Element;
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getStoryTimeTillElement(Lcom/samsung/app/video/editor/external/Element;)J

    move-result-wide v20

    move-wide/from16 v0, v20

    long-to-float v7, v0

    .line 518
    .local v7, "eleStartTime":F
    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/Element;->getEndTime()J

    move-result-wide v20

    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/Element;->getStartTime()J

    move-result-wide v22

    sub-long v20, v20, v22

    move-wide/from16 v0, v20

    long-to-float v0, v0

    move/from16 v19, v0

    invoke-static {v3}, Lcom/sec/android/app/ve/util/CommonUtils;->getSpeedFactor(Lcom/samsung/app/video/editor/external/Element;)F

    move-result v20

    mul-float v19, v19, v20

    invoke-static/range {v19 .. v19}, Ljava/lang/Math;->round(F)I

    move-result v19

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    add-float v19, v19, v7

    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/Element;->getOverLapAtStart()I

    move-result v20

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    sub-float v19, v19, v20

    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/Element;->getOverLapAtEnd()I

    move-result v20

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    sub-float v6, v19, v20

    .line 519
    .local v6, "eleEndTime":F
    move-object/from16 v0, p0

    invoke-virtual {v0, v9, v7, v6}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getRecordsBetween(IFF)Ljava/util/Vector;

    move-result-object v14

    .line 521
    .local v14, "recordings":Ljava/util/Vector;, "Ljava/util/Vector<Lcom/samsung/app/video/editor/external/Element;>;"
    invoke-virtual {v14}, Ljava/util/Vector;->size()I

    move-result v13

    .line 523
    .local v13, "recCount":I
    const/4 v10, 0x0

    .local v10, "j":I
    :goto_1
    if-lt v10, v13, :cond_5

    .line 532
    const/16 v19, 0x1

    move/from16 v0, v19

    if-ne v13, v0, :cond_1

    .line 533
    invoke-virtual {v11}, Lcom/samsung/app/video/editor/external/Element;->getStoryBoardStartTime()J

    move-result-wide v20

    move-wide/from16 v0, v20

    long-to-float v0, v0

    move/from16 v19, v0

    sub-float v19, v6, v19

    const/high16 v20, 0x447a0000    # 1000.0f

    cmpg-float v19, v19, v20

    if-gez v19, :cond_1

    .line 534
    const/high16 v19, 0x447a0000    # 1000.0f

    .line 535
    invoke-virtual {v11}, Lcom/samsung/app/video/editor/external/Element;->getStoryBoardStartTime()J

    move-result-wide v20

    move-wide/from16 v0, v20

    long-to-float v0, v0

    move/from16 v20, v0

    sub-float v20, v6, v20

    sub-float v19, v19, v20

    add-float v12, v12, v19

    .line 540
    :cond_1
    if-nez v13, :cond_6

    if-eqz v11, :cond_6

    .line 541
    invoke-virtual {v11}, Lcom/samsung/app/video/editor/external/Element;->getStoryBoardStartTime()J

    move-result-wide v20

    move-wide/from16 v0, v20

    long-to-float v0, v0

    move/from16 v19, v0

    cmpg-float v19, v19, v7

    if-gez v19, :cond_6

    .line 542
    invoke-virtual {v11}, Lcom/samsung/app/video/editor/external/Element;->getStoryBoardEndTime()J

    move-result-wide v20

    move-wide/from16 v0, v20

    long-to-float v0, v0

    move/from16 v19, v0

    cmpl-float v19, v19, v7

    if-lez v19, :cond_6

    .line 544
    invoke-virtual {v11}, Lcom/samsung/app/video/editor/external/Element;->getStoryBoardEndTime()J

    move-result-wide v20

    move-wide/from16 v0, v20

    long-to-float v0, v0

    move/from16 v19, v0

    sub-float v19, v19, v7

    const/high16 v20, 0x447a0000    # 1000.0f

    cmpg-float v19, v19, v20

    if-gez v19, :cond_2

    .line 545
    const/high16 v19, 0x447a0000    # 1000.0f

    invoke-virtual {v11}, Lcom/samsung/app/video/editor/external/Element;->getStoryBoardEndTime()J

    move-result-wide v20

    move-wide/from16 v0, v20

    long-to-float v0, v0

    move/from16 v20, v0

    sub-float v20, v20, v7

    sub-float v19, v19, v20

    add-float v12, v12, v19

    .line 552
    :cond_2
    :goto_2
    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/Element;->getTransitionEdit()Lcom/samsung/app/video/editor/external/Edit;

    move-result-object v5

    .line 553
    .local v5, "edit":Lcom/samsung/app/video/editor/external/Edit;
    if-eqz v5, :cond_4

    .line 554
    move/from16 v18, v6

    .line 555
    .local v18, "transStartTime":F
    invoke-virtual {v5}, Lcom/samsung/app/video/editor/external/Edit;->getTrans_duration()I

    move-result v19

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    add-float v15, v18, v19

    .line 557
    .local v15, "transEndTime":F
    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v9, v1, v15}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getRecordsBetween(IFF)Ljava/util/Vector;

    move-result-object v17

    .line 559
    .local v17, "transRecordings":Ljava/util/Vector;, "Ljava/util/Vector<Lcom/samsung/app/video/editor/external/Element;>;"
    invoke-virtual/range {v17 .. v17}, Ljava/util/Vector;->size()I

    move-result v16

    .line 560
    .local v16, "transRecordingCount":I
    const/4 v10, 0x0

    :goto_3
    move/from16 v0, v16

    if-lt v10, v0, :cond_7

    .line 576
    if-nez v16, :cond_3

    .line 577
    if-eqz v11, :cond_3

    .line 578
    invoke-virtual {v11}, Lcom/samsung/app/video/editor/external/Element;->getStoryBoardStartTime()J

    move-result-wide v20

    move-wide/from16 v0, v20

    long-to-float v0, v0

    move/from16 v19, v0

    cmpg-float v19, v19, v18

    if-gez v19, :cond_3

    .line 579
    invoke-virtual {v11}, Lcom/samsung/app/video/editor/external/Element;->getStoryBoardEndTime()J

    move-result-wide v20

    move-wide/from16 v0, v20

    long-to-float v0, v0

    move/from16 v19, v0

    cmpl-float v19, v19, v15

    if-gtz v19, :cond_4

    .line 584
    :cond_3
    if-nez v16, :cond_8

    .line 585
    if-eqz v11, :cond_8

    .line 586
    invoke-virtual {v11}, Lcom/samsung/app/video/editor/external/Element;->getStoryBoardStartTime()J

    move-result-wide v20

    move-wide/from16 v0, v20

    long-to-float v0, v0

    move/from16 v19, v0

    cmpg-float v19, v19, v18

    if-gez v19, :cond_8

    .line 587
    invoke-virtual {v11}, Lcom/samsung/app/video/editor/external/Element;->getStoryBoardEndTime()J

    move-result-wide v20

    move-wide/from16 v0, v20

    long-to-float v0, v0

    move/from16 v19, v0

    cmpg-float v19, v19, v15

    if-gez v19, :cond_8

    .line 588
    invoke-virtual {v11}, Lcom/samsung/app/video/editor/external/Element;->getStoryBoardEndTime()J

    move-result-wide v20

    move-wide/from16 v0, v20

    long-to-float v0, v0

    move/from16 v19, v0

    cmpl-float v19, v19, v18

    if-lez v19, :cond_8

    .line 590
    invoke-virtual {v11}, Lcom/samsung/app/video/editor/external/Element;->getStoryBoardEndTime()J

    move-result-wide v20

    move-wide/from16 v0, v20

    long-to-float v0, v0

    move/from16 v19, v0

    sub-float v19, v15, v19

    add-float v12, v12, v19

    .line 515
    .end local v15    # "transEndTime":F
    .end local v16    # "transRecordingCount":I
    .end local v17    # "transRecordings":Ljava/util/Vector;, "Ljava/util/Vector<Lcom/samsung/app/video/editor/external/Element;>;"
    .end local v18    # "transStartTime":F
    :cond_4
    :goto_4
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_0

    .line 524
    .end local v5    # "edit":Lcom/samsung/app/video/editor/external/Edit;
    :cond_5
    invoke-virtual {v14, v10}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v11

    .end local v11    # "lastRecording":Lcom/samsung/app/video/editor/external/Element;
    check-cast v11, Lcom/samsung/app/video/editor/external/Element;

    .line 525
    .restart local v11    # "lastRecording":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {v11}, Lcom/samsung/app/video/editor/external/Element;->getStoryBoardEndTime()J

    move-result-wide v20

    .line 526
    invoke-virtual {v11}, Lcom/samsung/app/video/editor/external/Element;->getStoryBoardStartTime()J

    move-result-wide v22

    sub-long v20, v20, v22

    move-wide/from16 v0, v20

    long-to-float v0, v0

    move/from16 v19, v0

    add-float v12, v12, v19

    .line 527
    add-int/lit8 v9, v9, 0x1

    .line 523
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_1

    .line 549
    :cond_6
    invoke-virtual {v14}, Ljava/util/Vector;->size()I

    move-result v19

    if-nez v19, :cond_2

    .line 550
    const/high16 v19, 0x447a0000    # 1000.0f

    add-float v12, v12, v19

    goto/16 :goto_2

    .line 561
    .restart local v5    # "edit":Lcom/samsung/app/video/editor/external/Edit;
    .restart local v15    # "transEndTime":F
    .restart local v16    # "transRecordingCount":I
    .restart local v17    # "transRecordings":Ljava/util/Vector;, "Ljava/util/Vector<Lcom/samsung/app/video/editor/external/Element;>;"
    .restart local v18    # "transStartTime":F
    :cond_7
    move-object/from16 v0, v17

    invoke-virtual {v0, v10}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v11

    .end local v11    # "lastRecording":Lcom/samsung/app/video/editor/external/Element;
    check-cast v11, Lcom/samsung/app/video/editor/external/Element;

    .line 562
    .restart local v11    # "lastRecording":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {v11}, Lcom/samsung/app/video/editor/external/Element;->getStoryBoardEndTime()J

    move-result-wide v20

    .line 563
    invoke-virtual {v11}, Lcom/samsung/app/video/editor/external/Element;->getStoryBoardStartTime()J

    move-result-wide v22

    .line 562
    sub-long v20, v20, v22

    move-wide/from16 v0, v20

    long-to-float v4, v0

    .line 568
    .local v4, "duration":F
    invoke-virtual {v11}, Lcom/samsung/app/video/editor/external/Element;->getStoryBoardStartTime()J

    move-result-wide v20

    move-wide/from16 v0, v20

    long-to-float v0, v0

    move/from16 v19, v0

    .line 567
    sub-float v19, v15, v19

    sub-float v19, v4, v19

    add-float v12, v12, v19

    .line 570
    invoke-virtual {v5}, Lcom/samsung/app/video/editor/external/Edit;->getTrans_duration()I

    move-result v19

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    add-float v12, v12, v19

    .line 571
    add-int/lit8 v9, v9, 0x1

    .line 560
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_3

    .line 594
    .end local v4    # "duration":F
    :cond_8
    if-nez v16, :cond_4

    .line 595
    invoke-virtual {v5}, Lcom/samsung/app/video/editor/external/Edit;->getTrans_duration()I

    move-result v19

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    add-float v12, v12, v19

    goto :goto_4
.end method

.method public clearBGMMusic()V
    .locals 1

    .prologue
    .line 603
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlAudioEleList:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 604
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlAudioEleList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 606
    :cond_0
    return-void
.end method

.method public closeAssetManager()V
    .locals 1

    .prologue
    .line 2584
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->mAssetManager:Landroid/content/res/AssetManager;

    if-eqz v0, :cond_0

    .line 2585
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->mAssetManager:Landroid/content/res/AssetManager;

    invoke-virtual {v0}, Landroid/content/res/AssetManager;->close()V

    .line 2586
    :cond_0
    return-void
.end method

.method public commitTempAudioFiles()V
    .locals 4

    .prologue
    .line 609
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlAudioEleList:Ljava/util/List;

    if-eqz v2, :cond_0

    .line 610
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlAudioEleList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 611
    :cond_0
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlAudioEleList:Ljava/util/List;

    .line 613
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->tempAudioEleList:Ljava/util/List;

    if-eqz v2, :cond_1

    .line 614
    new-instance v2, Ljava/util/Vector;

    invoke-direct {v2}, Ljava/util/Vector;-><init>()V

    iput-object v2, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlAudioEleList:Ljava/util/List;

    .line 615
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->tempAudioEleList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    .line 616
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v0, :cond_2

    .line 620
    .end local v0    # "count":I
    .end local v1    # "i":I
    :cond_1
    return-void

    .line 617
    .restart local v0    # "count":I
    .restart local v1    # "i":I
    :cond_2
    iget-object v3, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlAudioEleList:Ljava/util/List;

    iget-object v2, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->tempAudioEleList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/app/video/editor/external/Element;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 616
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public compareTo(Lcom/samsung/app/video/editor/external/TranscodeElement;)I
    .locals 1
    .param p1, "another"    # Lcom/samsung/app/video/editor/external/TranscodeElement;

    .prologue
    .line 625
    const/4 v0, 0x0

    return v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lcom/samsung/app/video/editor/external/TranscodeElement;

    invoke-virtual {p0, p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->compareTo(Lcom/samsung/app/video/editor/external/TranscodeElement;)I

    move-result v0

    return v0
.end method

.method public copyVariablesFromTranscode(Lcom/samsung/app/video/editor/external/TranscodeElement;)V
    .locals 2
    .param p1, "tElement"    # Lcom/samsung/app/video/editor/external/TranscodeElement;

    .prologue
    .line 629
    iget-boolean v0, p1, Lcom/samsung/app/video/editor/external/TranscodeElement;->isAPreview:Z

    iput-boolean v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->isAPreview:Z

    .line 630
    iget v0, p1, Lcom/samsung/app/video/editor/external/TranscodeElement;->fullMovieDuration:I

    iput v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->fullMovieDuration:I

    .line 631
    iget v0, p1, Lcom/samsung/app/video/editor/external/TranscodeElement;->resolutionEnumValue:I

    iput v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->resolutionEnumValue:I

    .line 633
    iget v0, p1, Lcom/samsung/app/video/editor/external/TranscodeElement;->mThemeName:I

    iput v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->mThemeName:I

    .line 634
    iget-object v0, p1, Lcom/samsung/app/video/editor/external/TranscodeElement;->lastModified:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 635
    new-instance v0, Ljava/lang/String;

    iget-object v1, p1, Lcom/samsung/app/video/editor/external/TranscodeElement;->lastModified:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->lastModified:Ljava/lang/String;

    .line 636
    :cond_0
    iget-object v0, p1, Lcom/samsung/app/video/editor/external/TranscodeElement;->exportedVideoPath:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 637
    new-instance v0, Ljava/lang/String;

    iget-object v1, p1, Lcom/samsung/app/video/editor/external/TranscodeElement;->exportedVideoPath:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->exportedVideoPath:Ljava/lang/String;

    .line 638
    :cond_1
    iget-wide v0, p1, Lcom/samsung/app/video/editor/external/TranscodeElement;->projectCreationTime:J

    iput-wide v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->projectCreationTime:J

    .line 639
    iget-object v0, p1, Lcom/samsung/app/video/editor/external/TranscodeElement;->projectFileName:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 640
    new-instance v0, Ljava/lang/String;

    iget-object v1, p1, Lcom/samsung/app/video/editor/external/TranscodeElement;->projectFileName:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->projectFileName:Ljava/lang/String;

    .line 641
    :cond_2
    iget-object v0, p1, Lcom/samsung/app/video/editor/external/TranscodeElement;->projectName:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 642
    new-instance v0, Ljava/lang/String;

    iget-object v1, p1, Lcom/samsung/app/video/editor/external/TranscodeElement;->projectName:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->projectName:Ljava/lang/String;

    .line 643
    :cond_3
    iget-wide v0, p1, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionalAudioDuration:J

    iput-wide v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionalAudioDuration:J

    .line 645
    iget v0, p1, Lcom/samsung/app/video/editor/external/TranscodeElement;->TargetDispWidth:I

    iput v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->TargetDispWidth:I

    .line 646
    iget v0, p1, Lcom/samsung/app/video/editor/external/TranscodeElement;->TargetDispHeight:I

    iput v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->TargetDispHeight:I

    .line 648
    iget-boolean v0, p1, Lcom/samsung/app/video/editor/external/TranscodeElement;->isSummaryProject:Z

    iput-boolean v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->isSummaryProject:Z

    .line 649
    iget-boolean v0, p1, Lcom/samsung/app/video/editor/external/TranscodeElement;->isMissingFileProject:Z

    iput-boolean v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->isMissingFileProject:Z

    .line 650
    return-void
.end method

.method public createElement(Ljava/lang/String;IZ)Lcom/samsung/app/video/editor/external/Element;
    .locals 12
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "type"    # I
    .param p3, "kenburnRequired"    # Z

    .prologue
    const/4 v8, 0x0

    const/4 v11, 0x4

    const/4 v10, 0x0

    .line 2362
    new-instance v2, Lcom/samsung/app/video/editor/external/Element;

    invoke-direct {v2}, Lcom/samsung/app/video/editor/external/Element;-><init>()V

    .line 2363
    .local v2, "element":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {v2, p1}, Lcom/samsung/app/video/editor/external/Element;->setFilePath(Ljava/lang/String;)V

    .line 2364
    invoke-virtual {v2, p2}, Lcom/samsung/app/video/editor/external/Element;->setType(I)V

    .line 2365
    new-instance v6, Lcom/samsung/app/video/editor/external/Edit;

    invoke-direct {v6}, Lcom/samsung/app/video/editor/external/Edit;-><init>()V

    .line 2366
    .local v6, "none_effect":Lcom/samsung/app/video/editor/external/Edit;
    invoke-virtual {v6, v11}, Lcom/samsung/app/video/editor/external/Edit;->setType(I)V

    .line 2367
    const/16 v9, 0x16

    invoke-virtual {v6, v9}, Lcom/samsung/app/video/editor/external/Edit;->setSubType(I)V

    .line 2368
    invoke-virtual {v2, v6}, Lcom/samsung/app/video/editor/external/Element;->addEdit(Lcom/samsung/app/video/editor/external/Edit;)V

    .line 2369
    const/4 v9, 0x2

    if-ne p2, v9, :cond_2

    .line 2371
    if-eqz p3, :cond_0

    .line 2373
    new-instance v0, Lcom/sec/android/app/ve/common/KenburnsDefault;

    invoke-direct {v0}, Lcom/sec/android/app/ve/common/KenburnsDefault;-><init>()V

    .line 2374
    .local v0, "defKenburns":Lcom/sec/android/app/ve/common/KenburnsDefault;
    invoke-virtual {v0, v2}, Lcom/sec/android/app/ve/common/KenburnsDefault;->applyDefaultKenburns(Lcom/samsung/app/video/editor/external/Element;)Lcom/samsung/app/video/editor/external/Element;

    move-result-object v2

    .line 2377
    new-instance v3, Lcom/samsung/app/video/editor/external/Edit;

    invoke-direct {v3}, Lcom/samsung/app/video/editor/external/Edit;-><init>()V

    .line 2378
    .local v3, "ken_burn":Lcom/samsung/app/video/editor/external/Edit;
    invoke-virtual {v3, v11}, Lcom/samsung/app/video/editor/external/Edit;->setType(I)V

    .line 2379
    const/16 v8, 0x27

    invoke-virtual {v3, v8}, Lcom/samsung/app/video/editor/external/Edit;->setSubType(I)V

    .line 2380
    invoke-virtual {v2, v3}, Lcom/samsung/app/video/editor/external/Element;->addEdit(Lcom/samsung/app/video/editor/external/Edit;)V

    .line 2394
    .end local v0    # "defKenburns":Lcom/sec/android/app/ve/common/KenburnsDefault;
    .end local v3    # "ken_burn":Lcom/samsung/app/video/editor/external/Edit;
    :cond_0
    :goto_0
    new-instance v1, Lcom/samsung/app/video/editor/external/Edit;

    invoke-direct {v1}, Lcom/samsung/app/video/editor/external/Edit;-><init>()V

    .line 2395
    .local v1, "edit":Lcom/samsung/app/video/editor/external/Edit;
    const/4 v8, 0x1

    invoke-virtual {v1, v8}, Lcom/samsung/app/video/editor/external/Edit;->setType(I)V

    .line 2396
    invoke-virtual {v2, v1}, Lcom/samsung/app/video/editor/external/Element;->addEdit(Lcom/samsung/app/video/editor/external/Edit;)V

    .line 2397
    invoke-virtual {v2, v10}, Lcom/samsung/app/video/editor/external/Element;->setAutoEdited(Z)V

    move-object v8, v2

    .line 2398
    .end local v1    # "edit":Lcom/samsung/app/video/editor/external/Edit;
    :cond_1
    return-object v8

    .line 2383
    :cond_2
    const/4 v7, 0x0

    .line 2384
    .local v7, "retriever":Landroid/media/MediaMetadataRetriever;
    iget-object v9, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->mAssetManager:Landroid/content/res/AssetManager;

    invoke-static {v9, p1, v10}, Lcom/sec/android/app/ve/util/CommonUtils;->getRetrieverForSource(Landroid/content/res/AssetManager;Ljava/lang/String;Z)Landroid/media/MediaMetadataRetriever;

    move-result-object v7

    .line 2385
    if-eqz v7, :cond_1

    .line 2387
    invoke-static {v10, v8, v7, p1}, Lcom/sec/android/app/ve/util/CommonUtils;->getVideoDuration(ZLandroid/content/res/AssetManager;Landroid/media/MediaMetadataRetriever;Ljava/lang/String;)I

    move-result v8

    int-to-long v4, v8

    .line 2388
    .local v4, "elementDur":J
    invoke-virtual {v2, v4, v5}, Lcom/samsung/app/video/editor/external/Element;->setEndTime(J)V

    .line 2389
    invoke-virtual {v2, v4, v5}, Lcom/samsung/app/video/editor/external/Element;->setDuration(J)V

    .line 2390
    invoke-static {v2}, Lcom/sec/android/app/ve/util/CommonUtils;->getRecordedMode(Lcom/samsung/app/video/editor/external/Element;)I

    move-result v8

    invoke-virtual {v2, v8}, Lcom/samsung/app/video/editor/external/Element;->setRecordingMode(I)V

    .line 2391
    invoke-static {v7}, Lcom/sec/android/app/ve/util/CommonUtils;->releaseRetriever(Landroid/media/MediaMetadataRetriever;)V

    goto :goto_0
.end method

.method public createElement(Ljava/lang/String;JJI)Lcom/samsung/app/video/editor/external/Element;
    .locals 8
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "start"    # J
    .param p4, "end"    # J
    .param p6, "volume"    # I

    .prologue
    .line 2334
    new-instance v1, Lcom/samsung/app/video/editor/external/Element;

    invoke-direct {v1}, Lcom/samsung/app/video/editor/external/Element;-><init>()V

    .line 2335
    .local v1, "element":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {v1, p1}, Lcom/samsung/app/video/editor/external/Element;->setFilePath(Ljava/lang/String;)V

    .line 2336
    const/4 v5, 0x0

    .line 2337
    .local v5, "retriever":Landroid/media/MediaMetadataRetriever;
    iget-object v6, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->mAssetManager:Landroid/content/res/AssetManager;

    const/4 v7, 0x0

    invoke-static {v6, p1, v7}, Lcom/sec/android/app/ve/util/CommonUtils;->getRetrieverForSource(Landroid/content/res/AssetManager;Ljava/lang/String;Z)Landroid/media/MediaMetadataRetriever;

    move-result-object v5

    .line 2338
    if-nez v5, :cond_0

    .line 2339
    const/4 v1, 0x0

    .line 2357
    .end local v1    # "element":Lcom/samsung/app/video/editor/external/Element;
    :goto_0
    return-object v1

    .line 2340
    .restart local v1    # "element":Lcom/samsung/app/video/editor/external/Element;
    :cond_0
    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static {v6, v7, v5, p1}, Lcom/sec/android/app/ve/util/CommonUtils;->getVideoDuration(ZLandroid/content/res/AssetManager;Landroid/media/MediaMetadataRetriever;Ljava/lang/String;)I

    move-result v6

    int-to-long v2, v6

    .line 2341
    .local v2, "elementDur":J
    const/4 v6, 0x1

    invoke-virtual {v1, v6}, Lcom/samsung/app/video/editor/external/Element;->setType(I)V

    .line 2342
    invoke-virtual {v1, p2, p3}, Lcom/samsung/app/video/editor/external/Element;->setStartTime(J)V

    .line 2343
    invoke-virtual {v1, p4, p5}, Lcom/samsung/app/video/editor/external/Element;->setEndTime(J)V

    .line 2344
    invoke-virtual {v1, v2, v3}, Lcom/samsung/app/video/editor/external/Element;->setDuration(J)V

    .line 2345
    invoke-static {v1}, Lcom/sec/android/app/ve/util/CommonUtils;->getRecordedMode(Lcom/samsung/app/video/editor/external/Element;)I

    move-result v6

    invoke-virtual {v1, v6}, Lcom/samsung/app/video/editor/external/Element;->setRecordingMode(I)V

    .line 2346
    invoke-static {v5}, Lcom/sec/android/app/ve/util/CommonUtils;->releaseRetriever(Landroid/media/MediaMetadataRetriever;)V

    .line 2348
    new-instance v0, Lcom/samsung/app/video/editor/external/Edit;

    invoke-direct {v0}, Lcom/samsung/app/video/editor/external/Edit;-><init>()V

    .line 2349
    .local v0, "edit":Lcom/samsung/app/video/editor/external/Edit;
    const/4 v6, 0x1

    invoke-virtual {v0, v6}, Lcom/samsung/app/video/editor/external/Edit;->setType(I)V

    .line 2350
    invoke-virtual {v0, p6}, Lcom/samsung/app/video/editor/external/Edit;->setVolumeLevel(I)V

    .line 2351
    invoke-virtual {v1, v0}, Lcom/samsung/app/video/editor/external/Element;->addEdit(Lcom/samsung/app/video/editor/external/Edit;)V

    .line 2352
    const/4 v6, 0x0

    invoke-virtual {v1, v6}, Lcom/samsung/app/video/editor/external/Element;->setAutoEdited(Z)V

    .line 2353
    new-instance v4, Lcom/samsung/app/video/editor/external/Edit;

    invoke-direct {v4}, Lcom/samsung/app/video/editor/external/Edit;-><init>()V

    .line 2354
    .local v4, "none_effect":Lcom/samsung/app/video/editor/external/Edit;
    const/4 v6, 0x4

    invoke-virtual {v4, v6}, Lcom/samsung/app/video/editor/external/Edit;->setType(I)V

    .line 2355
    const/16 v6, 0x16

    invoke-virtual {v4, v6}, Lcom/samsung/app/video/editor/external/Edit;->setSubType(I)V

    .line 2356
    invoke-virtual {v1, v4}, Lcom/samsung/app/video/editor/external/Element;->addEdit(Lcom/samsung/app/video/editor/external/Edit;)V

    goto :goto_0
.end method

.method public deleteAllClipArts()V
    .locals 1

    .prologue
    .line 653
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->deleteAllClipArts(Z)V

    .line 654
    return-void
.end method

.method public deleteAllClipArts(Z)V
    .locals 4
    .param p1, "forceRemoval"    # Z

    .prologue
    .line 657
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v0

    .line 658
    .local v0, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-lt v2, v0, :cond_0

    .line 662
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getTextEleListCount()I

    move-result v0

    .line 663
    const/4 v2, 0x0

    :goto_1
    if-lt v2, v0, :cond_1

    .line 666
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getDrawingEleListCount()I

    move-result v0

    .line 667
    const/4 v2, 0x0

    :goto_2
    if-lt v2, v0, :cond_2

    .line 670
    return-void

    .line 659
    :cond_0
    iget-object v3, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->elementList:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/app/video/editor/external/Element;

    .line 660
    .local v1, "element":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/Element;->deleteAllClipArts()V

    .line 658
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 664
    .end local v1    # "element":Lcom/samsung/app/video/editor/external/Element;
    :cond_1
    iget-object v3, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->textEleList:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/app/video/editor/external/ClipartParams;

    invoke-virtual {v3, p1}, Lcom/samsung/app/video/editor/external/ClipartParams;->remove(Z)V

    .line 663
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 668
    :cond_2
    iget-object v3, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->drawingEleList:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/app/video/editor/external/ClipartParams;

    invoke-virtual {v3, p1}, Lcom/samsung/app/video/editor/external/ClipartParams;->remove(Z)V

    .line 667
    add-int/lit8 v2, v2, 0x1

    goto :goto_2
.end method

.method public deleteTempAudioFiles()V
    .locals 1

    .prologue
    .line 673
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->tempAudioEleList:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 674
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->tempAudioEleList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 675
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->tempAudioEleList:Ljava/util/List;

    .line 676
    return-void
.end method

.method public deleteTextEleList(Lcom/samsung/app/video/editor/external/ClipartParams;)V
    .locals 2
    .param p1, "clipParams"    # Lcom/samsung/app/video/editor/external/ClipartParams;

    .prologue
    .line 479
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->textEleList:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 480
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->textEleList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 481
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->textEleList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 483
    :cond_0
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->textEleList:Ljava/util/List;

    sget-object v1, Lcom/samsung/app/video/editor/external/TranscodeElement;->mTextListComparator:Lcom/samsung/app/video/editor/external/TranscodeElement$TextListComparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 485
    :cond_1
    return-void
.end method

.method public getAdditionlAudioEleList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/Element;",
            ">;"
        }
    .end annotation

    .prologue
    .line 682
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlAudioEleList:Ljava/util/List;

    return-object v0
.end method

.method public getAdditionlRecordEleList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/Element;",
            ">;"
        }
    .end annotation

    .prologue
    .line 686
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlNarrationEleList:Ljava/util/List;

    return-object v0
.end method

.method public getAdditionlSoundEleList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/Element;",
            ">;"
        }
    .end annotation

    .prologue
    .line 690
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlSoundEleList:Ljava/util/List;

    return-object v0
.end method

.method public getAllClipArtParams()Ljava/util/Vector;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Vector",
            "<",
            "Lcom/samsung/app/video/editor/external/ClipartParams;",
            ">;"
        }
    .end annotation

    .prologue
    .line 694
    new-instance v3, Ljava/util/Vector;

    invoke-direct {v3}, Ljava/util/Vector;-><init>()V

    .line 695
    .local v3, "params":Ljava/util/Vector;, "Ljava/util/Vector<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v0

    .line 696
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v0, :cond_0

    .line 705
    return-object v3

    .line 697
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementList()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/app/video/editor/external/Element;

    .line 698
    invoke-virtual {v6}, Lcom/samsung/app/video/editor/external/Element;->getClipartList()Ljava/util/List;

    move-result-object v5

    .line 699
    .local v5, "texts_list":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    if-eqz v5, :cond_1

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v6

    if-lez v6, :cond_1

    .line 700
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v4

    .line 701
    .local v4, "textSize":I
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_1
    if-lt v2, v4, :cond_2

    .line 696
    .end local v2    # "j":I
    .end local v4    # "textSize":I
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 702
    .restart local v2    # "j":I
    .restart local v4    # "textSize":I
    :cond_2
    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/app/video/editor/external/ClipartParams;

    invoke-virtual {v3, v6}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 701
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method public getAllElementsInGroup(I)Ljava/util/List;
    .locals 6
    .param p1, "i"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/Element;",
            ">;"
        }
    .end annotation

    .prologue
    .line 710
    new-instance v1, Ljava/util/Vector;

    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    .line 711
    .local v1, "elemList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    iget-boolean v5, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->isSummaryProject:Z

    if-eqz v5, :cond_3

    .line 714
    iget-object v5, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->elementList:Ljava/util/List;

    if-eqz v5, :cond_0

    .line 715
    move v2, p1

    .line 716
    .local v2, "id":I
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v0

    .line 717
    .local v0, "count":I
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_0
    if-lt v3, v0, :cond_1

    .line 726
    .end local v0    # "count":I
    .end local v1    # "elemList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    .end local v2    # "id":I
    .end local v3    # "j":I
    :cond_0
    :goto_1
    return-object v1

    .line 718
    .restart local v0    # "count":I
    .restart local v1    # "elemList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    .restart local v2    # "id":I
    .restart local v3    # "j":I
    :cond_1
    iget-object v5, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->elementList:Ljava/util/List;

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/app/video/editor/external/Element;

    .line 719
    .local v4, "lElement":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/Element;->getGroupID()I

    move-result v5

    if-ne v2, v5, :cond_2

    .line 720
    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 717
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 726
    .end local v0    # "count":I
    .end local v2    # "id":I
    .end local v3    # "j":I
    .end local v4    # "lElement":Lcom/samsung/app/video/editor/external/Element;
    :cond_3
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public getAssetManager()Landroid/content/res/AssetManager;
    .locals 2

    .prologue
    .line 2578
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->mAssetManager:Landroid/content/res/AssetManager;

    if-nez v0, :cond_0

    .line 2579
    sget-object v0, Lcom/sec/android/app/ve/VEApp;->gThemeMgr:Lcom/sec/android/app/ve/theme/ThemeManager;

    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getThemeName()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/ve/theme/ThemeManager;->getThemeAssetManager(I)Landroid/content/res/AssetManager;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->mAssetManager:Landroid/content/res/AssetManager;

    .line 2581
    :cond_0
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->mAssetManager:Landroid/content/res/AssetManager;

    return-object v0
.end method

.method public getAudioListCount()I
    .locals 1

    .prologue
    .line 730
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlAudioEleList:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 731
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlAudioEleList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 733
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCaptionsAtTime(F)Ljava/util/List;
    .locals 6
    .param p1, "currentStoryboardTime"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(F)",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/ClipartParams;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2192
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2193
    .local v2, "currentClipartList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getTextEleListCount()I

    move-result v1

    .line 2194
    .local v1, "count":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-lt v3, v1, :cond_0

    .line 2201
    return-object v2

    .line 2195
    :cond_0
    iget-object v4, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->textEleList:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/app/video/editor/external/ClipartParams;

    .line 2196
    .local v0, "caption":Lcom/samsung/app/video/editor/external/ClipartParams;
    invoke-virtual {v0}, Lcom/samsung/app/video/editor/external/ClipartParams;->getStoryBoardStartTime()J

    move-result-wide v4

    long-to-float v4, v4

    cmpl-float v4, p1, v4

    if-ltz v4, :cond_1

    .line 2197
    invoke-virtual {v0}, Lcom/samsung/app/video/editor/external/ClipartParams;->getStoryBoardEndTime()J

    move-result-wide v4

    long-to-float v4, v4

    cmpg-float v4, p1, v4

    if-gtz v4, :cond_1

    .line 2198
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2194
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public getCaptionsAtTimeFrame(F)Ljava/util/List;
    .locals 6
    .param p1, "currentStoryboardTime"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(F)",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/ClipartParams;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2174
    const/high16 v5, 0x41f00000    # 30.0f

    mul-float/2addr v5, p1

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v3

    .line 2175
    .local v3, "frameNumber":I
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2177
    .local v2, "currentClipartList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getTextEleListCount()I

    move-result v1

    .line 2178
    .local v1, "count":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-lt v4, v1, :cond_0

    .line 2186
    return-object v2

    .line 2179
    :cond_0
    iget-object v5, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->textEleList:Ljava/util/List;

    invoke-interface {v5, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/app/video/editor/external/ClipartParams;

    .line 2180
    .local v0, "caption":Lcom/samsung/app/video/editor/external/ClipartParams;
    invoke-virtual {v0}, Lcom/samsung/app/video/editor/external/ClipartParams;->getStoryboardStartFrame()I

    move-result v5

    if-lt v3, v5, :cond_1

    .line 2181
    invoke-virtual {v0}, Lcom/samsung/app/video/editor/external/ClipartParams;->getStoryboardEndFrame()I

    move-result v5

    if-gt v3, v5, :cond_1

    .line 2182
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2178
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method

.method public getCopiedDrawClipartParamsListForElement(Lcom/samsung/app/video/editor/external/Element;)Ljava/util/List;
    .locals 6
    .param p1, "element"    # Lcom/samsung/app/video/editor/external/Element;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/app/video/editor/external/Element;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/ClipartParams;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2246
    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/Element;->getGroupID()I

    move-result v4

    .line 2247
    .local v4, "id":I
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2248
    .local v1, "currentDrawList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getDrawingEleListCount()I

    move-result v0

    .line 2249
    .local v0, "count":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-lt v3, v0, :cond_0

    .line 2255
    return-object v1

    .line 2250
    :cond_0
    iget-object v5, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->drawingEleList:Ljava/util/List;

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/app/video/editor/external/ClipartParams;

    .line 2251
    .local v2, "draw":Lcom/samsung/app/video/editor/external/ClipartParams;
    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/ClipartParams;->getElementID()I

    move-result v5

    if-ne v5, v4, :cond_1

    .line 2252
    new-instance v5, Lcom/samsung/app/video/editor/external/ClipartParams;

    invoke-direct {v5, v2}, Lcom/samsung/app/video/editor/external/ClipartParams;-><init>(Lcom/samsung/app/video/editor/external/ClipartParams;)V

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2249
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public getCopiedTextClipartParamsListForElement(Lcom/samsung/app/video/editor/external/Element;)Ljava/util/List;
    .locals 6
    .param p1, "element"    # Lcom/samsung/app/video/editor/external/Element;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/app/video/editor/external/Element;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/ClipartParams;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2233
    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/Element;->getGroupID()I

    move-result v3

    .line 2234
    .local v3, "id":I
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2235
    .local v1, "currentClipartList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getTextEleListCount()I

    move-result v0

    .line 2236
    .local v0, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-lt v2, v0, :cond_0

    .line 2242
    return-object v1

    .line 2237
    :cond_0
    iget-object v5, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->textEleList:Ljava/util/List;

    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/app/video/editor/external/ClipartParams;

    .line 2238
    .local v4, "text":Lcom/samsung/app/video/editor/external/ClipartParams;
    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/ClipartParams;->getElementID()I

    move-result v5

    if-ne v5, v3, :cond_1

    .line 2239
    new-instance v5, Lcom/samsung/app/video/editor/external/ClipartParams;

    invoke-direct {v5, v4}, Lcom/samsung/app/video/editor/external/ClipartParams;-><init>(Lcom/samsung/app/video/editor/external/ClipartParams;)V

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2236
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public getDrawClipartParamsListForElement(Lcom/samsung/app/video/editor/external/Element;)Ljava/util/List;
    .locals 6
    .param p1, "element"    # Lcom/samsung/app/video/editor/external/Element;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/app/video/editor/external/Element;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/ClipartParams;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2219
    if-nez p1, :cond_1

    .line 2220
    const/4 v1, 0x0

    .line 2230
    :cond_0
    return-object v1

    .line 2221
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/Element;->getGroupID()I

    move-result v4

    .line 2222
    .local v4, "id":I
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2223
    .local v1, "currentDrawList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getDrawingEleListCount()I

    move-result v0

    .line 2224
    .local v0, "count":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v0, :cond_0

    .line 2225
    iget-object v5, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->drawingEleList:Ljava/util/List;

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/app/video/editor/external/ClipartParams;

    .line 2226
    .local v2, "draw":Lcom/samsung/app/video/editor/external/ClipartParams;
    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/ClipartParams;->getElementID()I

    move-result v5

    if-ne v5, v4, :cond_2

    .line 2227
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2224
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public getDrawEleList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/ClipartParams;",
            ">;"
        }
    .end annotation

    .prologue
    .line 737
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->drawingEleList:Ljava/util/List;

    return-object v0
.end method

.method public getDrawingAt(F)Lcom/samsung/app/video/editor/external/ClipartParams;
    .locals 8
    .param p1, "storyBoardTime"    # F

    .prologue
    .line 741
    iget-object v5, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->drawingEleList:Ljava/util/List;

    if-eqz v5, :cond_0

    .line 742
    iget-object v5, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->drawingEleList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    .line 743
    .local v0, "count":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-lt v4, v0, :cond_2

    .line 756
    .end local v0    # "count":I
    .end local v4    # "i":I
    :cond_0
    const/4 v1, 0x0

    :cond_1
    return-object v1

    .line 744
    .restart local v0    # "count":I
    .restart local v4    # "i":I
    :cond_2
    iget-object v5, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->drawingEleList:Ljava/util/List;

    invoke-interface {v5, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/app/video/editor/external/ClipartParams;

    .line 746
    .local v1, "drawing":Lcom/samsung/app/video/editor/external/ClipartParams;
    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/ClipartParams;->getStoryBoardStartTime()J

    move-result-wide v6

    long-to-float v3, v6

    .line 747
    .local v3, "drawingStartBoardStartTime":F
    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/ClipartParams;->getStoryBoardEndTime()J

    move-result-wide v6

    long-to-float v2, v6

    .line 749
    .local v2, "drawingStartBoardEndTime":F
    cmpl-float v5, p1, v3

    if-ltz v5, :cond_3

    .line 750
    cmpg-float v5, p1, v2

    if-lez v5, :cond_1

    .line 743
    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method

.method public getDrawingBetween(FF)Ljava/util/Vector;
    .locals 8
    .param p1, "storyBoardStartTime"    # F
    .param p2, "storyBoardEndTime"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(FF)",
            "Ljava/util/Vector",
            "<",
            "Lcom/samsung/app/video/editor/external/ClipartParams;",
            ">;"
        }
    .end annotation

    .prologue
    .line 761
    new-instance v4, Ljava/util/Vector;

    invoke-direct {v4}, Ljava/util/Vector;-><init>()V

    .line 763
    .local v4, "drawings":Ljava/util/Vector;, "Ljava/util/Vector<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    iget-object v6, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->drawingEleList:Ljava/util/List;

    if-eqz v6, :cond_0

    .line 764
    iget-object v6, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->drawingEleList:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    .line 765
    .local v0, "count":I
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    if-lt v5, v0, :cond_1

    .line 779
    .end local v0    # "count":I
    .end local v5    # "i":I
    :cond_0
    return-object v4

    .line 766
    .restart local v0    # "count":I
    .restart local v5    # "i":I
    :cond_1
    iget-object v6, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->drawingEleList:Ljava/util/List;

    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/app/video/editor/external/ClipartParams;

    .line 768
    .local v1, "drawing":Lcom/samsung/app/video/editor/external/ClipartParams;
    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/ClipartParams;->getStoryBoardStartTime()J

    move-result-wide v6

    long-to-float v3, v6

    .line 769
    .local v3, "drawingStartBoardStartTime":F
    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/ClipartParams;->getStoryBoardEndTime()J

    move-result-wide v6

    long-to-float v2, v6

    .line 771
    .local v2, "drawingStartBoardEndTime":F
    cmpl-float v6, p1, v3

    if-ltz v6, :cond_2

    cmpg-float v6, p1, v2

    if-lez v6, :cond_4

    .line 772
    :cond_2
    cmpl-float v6, p2, v3

    if-ltz v6, :cond_3

    cmpg-float v6, p2, v2

    if-lez v6, :cond_4

    .line 773
    :cond_3
    cmpg-float v6, p1, v3

    if-gtz v6, :cond_5

    cmpl-float v6, p2, v2

    if-ltz v6, :cond_5

    .line 774
    :cond_4
    invoke-virtual {v4, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 765
    :cond_5
    add-int/lit8 v5, v5, 0x1

    goto :goto_0
.end method

.method public getDrawingEleListCount()I
    .locals 1

    .prologue
    .line 783
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->drawingEleList:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 784
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->drawingEleList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 786
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getElement(I)Lcom/samsung/app/video/editor/external/Element;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 791
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v0

    if-ge p1, v0, :cond_0

    if-ltz p1, :cond_0

    .line 792
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->elementList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/app/video/editor/external/Element;

    .line 794
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getElementAt(J)Lcom/samsung/app/video/editor/external/Element;
    .locals 13
    .param p1, "storyBoardTime"    # J

    .prologue
    .line 798
    const/4 v6, 0x0

    .line 800
    .local v6, "time":F
    iget-object v7, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->elementList:Ljava/util/List;

    if-eqz v7, :cond_0

    .line 801
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v0

    .line 802
    .local v0, "count":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-lt v3, v0, :cond_2

    .line 816
    .end local v0    # "count":I
    .end local v3    # "i":I
    :cond_0
    const/4 v2, 0x0

    :cond_1
    return-object v2

    .line 803
    .restart local v0    # "count":I
    .restart local v3    # "i":I
    :cond_2
    iget-object v7, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->elementList:Ljava/util/List;

    invoke-interface {v7, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/app/video/editor/external/Element;

    .line 804
    .local v2, "element":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/Element;->getOverLapAtStart()I

    move-result v7

    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/Element;->getOverLapAtEnd()I

    move-result v8

    add-int/2addr v7, v8

    int-to-long v4, v7

    .line 805
    .local v4, "overLapTime":J
    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/Element;->getEndTime()J

    move-result-wide v8

    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/Element;->getStartTime()J

    move-result-wide v10

    sub-long/2addr v8, v10

    long-to-float v7, v8

    invoke-static {v2}, Lcom/sec/android/app/ve/util/CommonUtils;->getSpeedFactor(Lcom/samsung/app/video/editor/external/Element;)F

    move-result v8

    mul-float/2addr v7, v8

    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v7

    int-to-float v7, v7

    add-float/2addr v7, v6

    long-to-float v8, v4

    sub-float v6, v7, v8

    .line 806
    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/Element;->getTransitionEdit()Lcom/samsung/app/video/editor/external/Edit;

    move-result-object v1

    .line 807
    .local v1, "edit":Lcom/samsung/app/video/editor/external/Edit;
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/Edit;->getSubType()I

    move-result v7

    if-eqz v7, :cond_3

    .line 808
    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/Edit;->getTrans_duration()I

    move-result v7

    int-to-float v7, v7

    add-float/2addr v6, v7

    .line 809
    :cond_3
    long-to-float v7, p1

    cmpl-float v7, v6, v7

    if-gez v7, :cond_1

    .line 802
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public getElementCount()I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 820
    iget-object v1, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->elementList:Ljava/util/List;

    if-eqz v1, :cond_1

    .line 821
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->elementList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 826
    :cond_0
    :goto_0
    return v0

    .line 822
    :cond_1
    iget-object v1, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->elementList:Ljava/util/List;

    if-nez v1, :cond_0

    goto :goto_0
.end method

.method public getElementCountInSummary()I
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 830
    const/4 v3, 0x1

    .line 831
    .local v3, "j":I
    iget-object v6, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->elementList:Ljava/util/List;

    if-eqz v6, :cond_3

    .line 832
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v5

    add-int/lit8 v0, v5, -0x1

    .line 833
    .local v0, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-lt v2, v0, :cond_1

    move v5, v3

    .line 844
    .end local v0    # "count":I
    .end local v2    # "i":I
    :cond_0
    :goto_1
    return v5

    .line 834
    .restart local v0    # "count":I
    .restart local v2    # "i":I
    :cond_1
    iget-object v5, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->elementList:Ljava/util/List;

    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/app/video/editor/external/Element;

    invoke-virtual {v5}, Lcom/samsung/app/video/editor/external/Element;->getGroupID()I

    move-result v1

    .line 835
    .local v1, "groupID":I
    iget-object v5, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->elementList:Ljava/util/List;

    add-int/lit8 v6, v2, 0x1

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/app/video/editor/external/Element;

    invoke-virtual {v5}, Lcom/samsung/app/video/editor/external/Element;->getGroupID()I

    move-result v4

    .line 837
    .local v4, "nextGroupID":I
    if-eq v1, v4, :cond_2

    .line 838
    add-int/lit8 v3, v3, 0x1

    .line 833
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 842
    .end local v0    # "count":I
    .end local v1    # "groupID":I
    .end local v2    # "i":I
    .end local v4    # "nextGroupID":I
    :cond_3
    iget-object v6, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->elementList:Ljava/util/List;

    if-nez v6, :cond_0

    goto :goto_1
.end method

.method public getElementList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/Element;",
            ">;"
        }
    .end annotation

    .prologue
    .line 851
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->elementList:Ljava/util/List;

    if-nez v0, :cond_0

    .line 852
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->elementList:Ljava/util/List;

    .line 855
    :cond_0
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->elementList:Ljava/util/List;

    return-object v0
.end method

.method public getElementWithID(I)Lcom/samsung/app/video/editor/external/Element;
    .locals 4
    .param p1, "groupID"    # I

    .prologue
    .line 939
    iget-boolean v3, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->isSummaryProject:Z

    if-nez v3, :cond_0

    .line 940
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v0

    .line 941
    .local v0, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-lt v2, v0, :cond_2

    .line 948
    .end local v0    # "count":I
    .end local v2    # "i":I
    :cond_0
    const/4 v1, 0x0

    :cond_1
    return-object v1

    .line 942
    .restart local v0    # "count":I
    .restart local v2    # "i":I
    :cond_2
    iget-object v3, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->elementList:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/app/video/editor/external/Element;

    .line 943
    .local v1, "element":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/Element;->getGroupID()I

    move-result v3

    if-eq v3, p1, :cond_1

    .line 941
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public getElementsBetween(ILjava/util/List;FF)Ljava/util/Vector;
    .locals 8
    .param p1, "index"    # I
    .param p3, "storyBoardStartTime"    # F
    .param p4, "storyBoardEndTime"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/Element;",
            ">;FF)",
            "Ljava/util/Vector",
            "<",
            "Lcom/samsung/app/video/editor/external/Element;",
            ">;"
        }
    .end annotation

    .prologue
    .line 874
    .local p2, "elementsList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    new-instance v5, Ljava/util/Vector;

    invoke-direct {v5}, Ljava/util/Vector;-><init>()V

    .line 876
    .local v5, "recordings":Ljava/util/Vector;, "Ljava/util/Vector<Lcom/samsung/app/video/editor/external/Element;>;"
    float-to-int v6, p3

    if-eqz v6, :cond_0

    .line 878
    const/high16 v6, 0x3f800000    # 1.0f

    sub-float/2addr p3, v6

    .line 881
    :cond_0
    if-eqz p2, :cond_1

    .line 882
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v4

    .line 883
    .local v4, "listSize":I
    const/4 v0, 0x0

    .line 888
    .local v0, "childElement":Lcom/samsung/app/video/editor/external/Element;
    move v3, p1

    .local v3, "i":I
    :goto_0
    if-lt v3, v4, :cond_2

    .line 904
    .end local v0    # "childElement":Lcom/samsung/app/video/editor/external/Element;
    .end local v3    # "i":I
    .end local v4    # "listSize":I
    :cond_1
    return-object v5

    .line 889
    .restart local v0    # "childElement":Lcom/samsung/app/video/editor/external/Element;
    .restart local v3    # "i":I
    .restart local v4    # "listSize":I
    :cond_2
    invoke-interface {p2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "childElement":Lcom/samsung/app/video/editor/external/Element;
    check-cast v0, Lcom/samsung/app/video/editor/external/Element;

    .line 890
    .restart local v0    # "childElement":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {v0}, Lcom/samsung/app/video/editor/external/Element;->getStoryBoardStartTime()J

    move-result-wide v6

    long-to-float v2, v6

    .line 891
    .local v2, "childElementStartTime":F
    invoke-virtual {v0}, Lcom/samsung/app/video/editor/external/Element;->getStoryBoardEndTime()J

    move-result-wide v6

    long-to-float v1, v6

    .line 897
    .local v1, "childElementEndTime":F
    cmpl-float v6, p4, v2

    if-ltz v6, :cond_3

    cmpg-float v6, p4, v1

    if-lez v6, :cond_4

    .line 898
    :cond_3
    cmpg-float v6, p3, v2

    if-gtz v6, :cond_5

    cmpl-float v6, p4, v1

    if-ltz v6, :cond_5

    .line 899
    :cond_4
    invoke-virtual {v5, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 888
    :cond_5
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public getElementsBetween(Ljava/util/List;FF)Ljava/util/Vector;
    .locals 8
    .param p2, "storyBoardStartTime"    # F
    .param p3, "storyBoardEndTime"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/Element;",
            ">;FF)",
            "Ljava/util/Vector",
            "<",
            "Lcom/samsung/app/video/editor/external/Element;",
            ">;"
        }
    .end annotation

    .prologue
    .line 909
    .local p1, "elementsList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    new-instance v5, Ljava/util/Vector;

    invoke-direct {v5}, Ljava/util/Vector;-><init>()V

    .line 911
    .local v5, "recordings":Ljava/util/Vector;, "Ljava/util/Vector<Lcom/samsung/app/video/editor/external/Element;>;"
    if-eqz p1, :cond_0

    .line 912
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    .line 913
    .local v4, "listSize":I
    const/4 v0, 0x0

    .line 918
    .local v0, "childElement":Lcom/samsung/app/video/editor/external/Element;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-lt v3, v4, :cond_1

    .line 935
    .end local v0    # "childElement":Lcom/samsung/app/video/editor/external/Element;
    .end local v3    # "i":I
    .end local v4    # "listSize":I
    :cond_0
    return-object v5

    .line 919
    .restart local v0    # "childElement":Lcom/samsung/app/video/editor/external/Element;
    .restart local v3    # "i":I
    .restart local v4    # "listSize":I
    :cond_1
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "childElement":Lcom/samsung/app/video/editor/external/Element;
    check-cast v0, Lcom/samsung/app/video/editor/external/Element;

    .line 920
    .restart local v0    # "childElement":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {v0}, Lcom/samsung/app/video/editor/external/Element;->getStoryBoardStartTime()J

    move-result-wide v6

    long-to-float v2, v6

    .line 921
    .local v2, "childElementStartTime":F
    invoke-virtual {v0}, Lcom/samsung/app/video/editor/external/Element;->getStoryBoardEndTime()J

    move-result-wide v6

    long-to-float v1, v6

    .line 927
    .local v1, "childElementEndTime":F
    cmpl-float v6, p2, v2

    if-ltz v6, :cond_2

    cmpg-float v6, p2, v1

    if-lez v6, :cond_4

    .line 928
    :cond_2
    cmpl-float v6, p3, v2

    if-ltz v6, :cond_3

    cmpg-float v6, p3, v1

    if-lez v6, :cond_4

    .line 929
    :cond_3
    cmpg-float v6, p2, v2

    if-gtz v6, :cond_5

    cmpl-float v6, p3, v1

    if-ltz v6, :cond_5

    .line 930
    :cond_4
    invoke-virtual {v5, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 918
    :cond_5
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public getEndTImeTillElement(I)J
    .locals 14
    .param p1, "index"    # I

    .prologue
    .line 952
    const-wide/16 v0, 0x0

    .line 953
    .local v0, "duration":J
    iget-object v9, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->elementList:Ljava/util/List;

    if-eqz v9, :cond_0

    .line 954
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    if-le v5, p1, :cond_1

    .line 966
    .end local v5    # "i":I
    :cond_0
    return-wide v0

    .line 955
    .restart local v5    # "i":I
    :cond_1
    iget-object v9, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->elementList:Ljava/util/List;

    invoke-interface {v9, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/app/video/editor/external/Element;

    .line 956
    .local v4, "element":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/Element;->getEndTime()J

    move-result-wide v10

    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/Element;->getStartTime()J

    move-result-wide v12

    sub-long v2, v10, v12

    .line 957
    .local v2, "eleDuration":J
    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/Element;->getOverLapAtEnd()I

    move-result v9

    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/Element;->getOverLapAtStart()I

    move-result v10

    add-int/2addr v9, v10

    int-to-long v6, v9

    .line 958
    .local v6, "overLapTime":J
    long-to-float v9, v2

    invoke-static {v4}, Lcom/sec/android/app/ve/util/CommonUtils;->getSpeedFactor(Lcom/samsung/app/video/editor/external/Element;)F

    move-result v10

    mul-float/2addr v9, v10

    invoke-static {v9}, Ljava/lang/Math;->round(F)I

    move-result v9

    int-to-long v2, v9

    .line 959
    sub-long/2addr v2, v6

    .line 960
    add-long/2addr v0, v2

    .line 961
    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/Element;->getTransitionEdit()Lcom/samsung/app/video/editor/external/Edit;

    move-result-object v8

    .line 962
    .local v8, "transitionEdit":Lcom/samsung/app/video/editor/external/Edit;
    if-eqz v8, :cond_2

    if-eq v5, p1, :cond_2

    .line 963
    invoke-virtual {v8}, Lcom/samsung/app/video/editor/external/Edit;->getTrans_duration()I

    move-result v9

    int-to-long v10, v9

    add-long/2addr v0, v10

    .line 954
    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_0
.end method

.method public getExportedVideoPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 974
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->exportedVideoPath:Ljava/lang/String;

    return-object v0
.end method

.method public getFirstElementInGroup(I)Lcom/samsung/app/video/editor/external/Element;
    .locals 4
    .param p1, "id"    # I

    .prologue
    .line 978
    iget-boolean v3, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->isSummaryProject:Z

    if-eqz v3, :cond_2

    .line 981
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v0

    .line 982
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_0
    if-lt v1, v0, :cond_1

    .line 988
    const/4 v2, 0x0

    .line 990
    .end local v0    # "count":I
    .end local v1    # "j":I
    :cond_0
    :goto_1
    return-object v2

    .line 983
    .restart local v0    # "count":I
    .restart local v1    # "j":I
    :cond_1
    iget-object v3, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->elementList:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/app/video/editor/external/Element;

    .line 984
    .local v2, "lElement":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/Element;->getGroupID()I

    move-result v3

    if-eq p1, v3, :cond_0

    .line 982
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 990
    .end local v0    # "count":I
    .end local v1    # "j":I
    .end local v2    # "lElement":Lcom/samsung/app/video/editor/external/Element;
    :cond_2
    iget-object v3, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->elementList:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/app/video/editor/external/Element;

    move-object v2, v3

    goto :goto_1
.end method

.method public getFullMovieDuration()I
    .locals 1

    .prologue
    .line 996
    iget v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->fullMovieDuration:I

    return v0
.end method

.method public getLastElementInGroup(I)Lcom/samsung/app/video/editor/external/Element;
    .locals 5
    .param p1, "groupID"    # I

    .prologue
    .line 1016
    iget-boolean v4, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->isSummaryProject:Z

    if-eqz v4, :cond_0

    .line 1019
    move v1, p1

    .line 1020
    .local v1, "id":I
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v4

    add-int/lit8 v0, v4, -0x1

    .line 1021
    .local v0, "count":I
    move v2, v0

    .local v2, "j":I
    :goto_0
    if-gez v2, :cond_2

    .line 1028
    .end local v0    # "count":I
    .end local v1    # "id":I
    .end local v2    # "j":I
    :cond_0
    const/4 v3, 0x0

    :cond_1
    return-object v3

    .line 1022
    .restart local v0    # "count":I
    .restart local v1    # "id":I
    .restart local v2    # "j":I
    :cond_2
    iget-object v4, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->elementList:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/app/video/editor/external/Element;

    .line 1023
    .local v3, "lElement":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/Element;->getGroupID()I

    move-result v4

    if-eq v1, v4, :cond_1

    .line 1021
    add-int/lit8 v2, v2, -0x1

    goto :goto_0
.end method

.method public getLastElementInGroup(Lcom/samsung/app/video/editor/external/Element;)Lcom/samsung/app/video/editor/external/Element;
    .locals 5
    .param p1, "e"    # Lcom/samsung/app/video/editor/external/Element;

    .prologue
    .line 1000
    iget-boolean v4, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->isSummaryProject:Z

    if-eqz v4, :cond_0

    .line 1003
    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/Element;->getGroupID()I

    move-result v1

    .line 1004
    .local v1, "id":I
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v4

    add-int/lit8 v0, v4, -0x1

    .line 1005
    .local v0, "count":I
    move v2, v0

    .local v2, "j":I
    :goto_0
    if-gez v2, :cond_2

    .end local v0    # "count":I
    .end local v1    # "id":I
    .end local v2    # "j":I
    :cond_0
    move-object v3, p1

    .line 1012
    :cond_1
    return-object v3

    .line 1006
    .restart local v0    # "count":I
    .restart local v1    # "id":I
    .restart local v2    # "j":I
    :cond_2
    iget-object v4, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->elementList:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/app/video/editor/external/Element;

    .line 1007
    .local v3, "lElement":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/Element;->getGroupID()I

    move-result v4

    if-eq v1, v4, :cond_1

    .line 1005
    add-int/lit8 v2, v2, -0x1

    goto :goto_0
.end method

.method public getMaxGroupID()I
    .locals 5

    .prologue
    .line 1033
    const/4 v2, 0x0

    .line 1034
    .local v2, "groupID":I
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v0

    .line 1035
    .local v0, "count":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-lt v3, v0, :cond_0

    .line 1041
    return v2

    .line 1036
    :cond_0
    iget-object v4, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->elementList:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/app/video/editor/external/Element;

    .line 1037
    .local v1, "element":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/Element;->getGroupID()I

    move-result v4

    if-le v4, v2, :cond_1

    .line 1038
    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/Element;->getGroupID()I

    move-result v2

    .line 1035
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public getMusicAt(F)Lcom/samsung/app/video/editor/external/Element;
    .locals 8
    .param p1, "storyBoardTime"    # F

    .prologue
    .line 1045
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getAudioListCount()I

    move-result v0

    .line 1046
    .local v0, "count":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-lt v3, v0, :cond_1

    .line 1057
    const/4 v4, 0x0

    :cond_0
    return-object v4

    .line 1047
    :cond_1
    iget-object v5, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlAudioEleList:Ljava/util/List;

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/app/video/editor/external/Element;

    .line 1049
    .local v4, "music":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/Element;->getStoryBoardStartTime()J

    move-result-wide v6

    long-to-float v2, v6

    .line 1050
    .local v2, "drawingStartBoardStartTime":F
    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/Element;->getStoryBoardEndTime()J

    move-result-wide v6

    long-to-float v1, v6

    .line 1052
    .local v1, "drawingStartBoardEndTime":F
    cmpl-float v5, p1, v2

    if-ltz v5, :cond_2

    .line 1053
    cmpg-float v5, p1, v1

    if-lez v5, :cond_0

    .line 1046
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public getMusicInTimeRange(FF)Ljava/util/ArrayList;
    .locals 8
    .param p1, "storyBoardStartTime"    # F
    .param p2, "storyBoardEndTime"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(FF)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/app/video/editor/external/Element;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1061
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1062
    .local v2, "elementList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/app/video/editor/external/Element;>;"
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getAudioListCount()I

    move-result v1

    .line 1063
    .local v1, "count":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-lt v3, v1, :cond_0

    .line 1077
    return-object v2

    .line 1064
    :cond_0
    iget-object v5, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlAudioEleList:Ljava/util/List;

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/app/video/editor/external/Element;

    .line 1065
    .local v4, "music":Lcom/samsung/app/video/editor/external/Element;
    const/4 v0, 0x1

    .line 1066
    .local v0, "add":Z
    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/Element;->getStoryBoardEndTime()J

    move-result-wide v6

    long-to-float v5, v6

    cmpg-float v5, v5, p1

    if-gez v5, :cond_3

    .line 1067
    const/4 v0, 0x0

    .line 1073
    :cond_1
    :goto_1
    if-eqz v0, :cond_2

    .line 1074
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1063
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1069
    :cond_3
    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/Element;->getStoryBoardStartTime()J

    move-result-wide v6

    long-to-float v5, v6

    cmpl-float v5, v5, p2

    if-lez v5, :cond_1

    .line 1070
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public getMusicListCount()I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1251
    iget-object v1, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlAudioEleList:Ljava/util/List;

    if-eqz v1, :cond_1

    .line 1252
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlAudioEleList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 1257
    :cond_0
    :goto_0
    return v0

    .line 1253
    :cond_1
    iget-object v1, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlAudioEleList:Ljava/util/List;

    if-nez v1, :cond_0

    goto :goto_0
.end method

.method public getMyPosition(Lcom/samsung/app/video/editor/external/ClipartParams;)I
    .locals 3
    .param p1, "textClipartParam"    # Lcom/samsung/app/video/editor/external/ClipartParams;

    .prologue
    .line 1081
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getTextEleListCount()I

    move-result v0

    .line 1082
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v0, :cond_1

    .line 1087
    const/4 v1, -0x1

    .end local v1    # "i":I
    :cond_0
    return v1

    .line 1083
    .restart local v1    # "i":I
    :cond_1
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->textEleList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/app/video/editor/external/ClipartParams;

    invoke-virtual {v2, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1082
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public getMyPosition(Lcom/samsung/app/video/editor/external/Element;)I
    .locals 3
    .param p1, "element"    # Lcom/samsung/app/video/editor/external/Element;

    .prologue
    .line 1091
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v0

    .line 1092
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v0, :cond_1

    .line 1098
    const/4 v1, -0x1

    .end local v1    # "i":I
    :cond_0
    return v1

    .line 1093
    .restart local v1    # "i":I
    :cond_1
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->elementList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/app/video/editor/external/Element;

    invoke-virtual {v2, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1092
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public getMyPositionInDrawing(Lcom/samsung/app/video/editor/external/ClipartParams;)I
    .locals 3
    .param p1, "drawClipartParam"    # Lcom/samsung/app/video/editor/external/ClipartParams;

    .prologue
    .line 1102
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getDrawingEleListCount()I

    move-result v0

    .line 1103
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v0, :cond_1

    .line 1108
    const/4 v1, -0x1

    .end local v1    # "i":I
    :cond_0
    return v1

    .line 1104
    .restart local v1    # "i":I
    :cond_1
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->drawingEleList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/app/video/editor/external/ClipartParams;

    invoke-virtual {v2, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1103
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public getMyPositionInMusicList(Lcom/samsung/app/video/editor/external/Element;)I
    .locals 3
    .param p1, "element"    # Lcom/samsung/app/video/editor/external/Element;

    .prologue
    .line 1112
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getAudioListCount()I

    move-result v0

    .line 1113
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v0, :cond_1

    .line 1118
    const/4 v1, -0x1

    .end local v1    # "i":I
    :cond_0
    return v1

    .line 1114
    .restart local v1    # "i":I
    :cond_1
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlAudioEleList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/app/video/editor/external/Element;

    invoke-virtual {v2, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1113
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public getMyPositionInRecordList(Lcom/samsung/app/video/editor/external/Element;)I
    .locals 3
    .param p1, "element"    # Lcom/samsung/app/video/editor/external/Element;

    .prologue
    .line 1122
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getRecordListCount()I

    move-result v0

    .line 1123
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v0, :cond_1

    .line 1130
    const/4 v1, -0x1

    .end local v1    # "i":I
    :cond_0
    return v1

    .line 1124
    .restart local v1    # "i":I
    :cond_1
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlNarrationEleList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/app/video/editor/external/Element;

    invoke-virtual {v2, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1123
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public getMyPositionInSoundList(Lcom/samsung/app/video/editor/external/Element;)I
    .locals 3
    .param p1, "element"    # Lcom/samsung/app/video/editor/external/Element;

    .prologue
    .line 1134
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getSoundListCount()I

    move-result v0

    .line 1135
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v0, :cond_1

    .line 1142
    const/4 v1, -0x1

    .end local v1    # "i":I
    :cond_0
    return v1

    .line 1136
    .restart local v1    # "i":I
    :cond_1
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlSoundEleList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/app/video/editor/external/Element;

    invoke-virtual {v2, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1135
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public getMyPositionInTempMusicList(Lcom/samsung/app/video/editor/external/Element;)I
    .locals 3
    .param p1, "element"    # Lcom/samsung/app/video/editor/external/Element;

    .prologue
    .line 1146
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getTempAudioListCount()I

    move-result v0

    .line 1147
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v0, :cond_1

    .line 1152
    const/4 v1, -0x1

    .end local v1    # "i":I
    :cond_0
    return v1

    .line 1148
    .restart local v1    # "i":I
    :cond_1
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->tempAudioEleList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/app/video/editor/external/Element;

    invoke-virtual {v2, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1147
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public getNewRecomProject()Z
    .locals 1

    .prologue
    .line 2006
    iget-boolean v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->isNewRecomProject:Z

    return v0
.end method

.method public getProjCreationTime()J
    .locals 2

    .prologue
    .line 1156
    iget-wide v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->projectCreationTime:J

    return-wide v0
.end method

.method public getProjectFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1163
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->projectFileName:Ljava/lang/String;

    return-object v0
.end method

.method public getProjectModifiedDate()Ljava/lang/String;
    .locals 5

    .prologue
    .line 1168
    new-instance v1, Ljava/io/File;

    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getProjectFileName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1171
    .local v1, "file":Ljava/io/File;
    :try_start_0
    invoke-virtual {v1}, Ljava/io/File;->lastModified()J

    move-result-wide v2

    .line 1172
    .local v2, "value":J
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/ve/util/CommonUtils;->getDate(Ljava/lang/Long;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->lastModified:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1177
    .end local v2    # "value":J
    :goto_0
    iget-object v4, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->lastModified:Ljava/lang/String;

    return-object v4

    .line 1173
    :catch_0
    move-exception v0

    .line 1174
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public getProjectModifiedTime()J
    .locals 4

    .prologue
    .line 1182
    const-wide/16 v2, 0x0

    .line 1183
    .local v2, "val":J
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getProjectFileName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1185
    .local v0, "file":Ljava/io/File;
    :try_start_0
    invoke-virtual {v0}, Ljava/io/File;->lastModified()J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 1189
    :goto_0
    return-wide v2

    .line 1186
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public getProjectName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2098
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->projectName:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 2099
    invoke-static {}, Lcom/sec/android/app/ve/pm/ProjectManager;->getInstance()Lcom/sec/android/app/ve/pm/ProjectManager;

    move-result-object v1

    iget v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->mThemeName:I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/sec/android/app/ve/pm/ProjectManager;->getProjectFileNameAvailableNext(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2101
    :goto_1
    return-object v0

    .line 2099
    :cond_0
    invoke-static {p0}, Lcom/sec/android/app/ve/util/CommonUtils;->getThemeTitle(Lcom/samsung/app/video/editor/external/TranscodeElement;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2101
    :cond_1
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->projectName:Ljava/lang/String;

    goto :goto_1
.end method

.method public getRecordAt(F)Lcom/samsung/app/video/editor/external/Element;
    .locals 8
    .param p1, "storyBoardTime"    # F

    .prologue
    .line 1193
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getRecordListCount()I

    move-result v0

    .line 1194
    .local v0, "count":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-lt v3, v0, :cond_1

    .line 1205
    const/4 v4, 0x0

    :cond_0
    return-object v4

    .line 1195
    :cond_1
    iget-object v5, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlNarrationEleList:Ljava/util/List;

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/app/video/editor/external/Element;

    .line 1197
    .local v4, "music":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/Element;->getStoryBoardStartTime()J

    move-result-wide v6

    long-to-float v2, v6

    .line 1198
    .local v2, "drawingStartBoardStartTime":F
    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/Element;->getStoryBoardEndTime()J

    move-result-wide v6

    long-to-float v1, v6

    .line 1200
    .local v1, "drawingStartBoardEndTime":F
    cmpl-float v5, p1, v2

    if-ltz v5, :cond_2

    .line 1201
    cmpg-float v5, p1, v1

    if-lez v5, :cond_0

    .line 1194
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public getRecordListCount()I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1261
    iget-object v1, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlNarrationEleList:Ljava/util/List;

    if-eqz v1, :cond_1

    .line 1262
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlNarrationEleList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 1267
    :cond_0
    :goto_0
    return v0

    .line 1263
    :cond_1
    iget-object v1, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlNarrationEleList:Ljava/util/List;

    if-nez v1, :cond_0

    goto :goto_0
.end method

.method public getRecordingElementsBetween(Ljava/util/List;FF)Ljava/util/Vector;
    .locals 8
    .param p2, "storyBoardStartTime"    # F
    .param p3, "storyBoardEndTime"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/Element;",
            ">;FF)",
            "Ljava/util/Vector",
            "<",
            "Lcom/samsung/app/video/editor/external/Element;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1224
    .local p1, "elementsList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    new-instance v4, Ljava/util/Vector;

    invoke-direct {v4}, Ljava/util/Vector;-><init>()V

    .line 1226
    .local v4, "recordings":Ljava/util/Vector;, "Ljava/util/Vector<Lcom/samsung/app/video/editor/external/Element;>;"
    if-eqz p1, :cond_0

    .line 1227
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    .line 1228
    .local v3, "listSize":I
    const/4 v0, 0x0

    .line 1229
    .local v0, "childElement":Lcom/samsung/app/video/editor/external/Element;
    const/4 v1, 0x0

    .line 1232
    .local v1, "childElementStartTime":F
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-lt v2, v3, :cond_1

    .line 1247
    .end local v0    # "childElement":Lcom/samsung/app/video/editor/external/Element;
    .end local v1    # "childElementStartTime":F
    .end local v2    # "i":I
    .end local v3    # "listSize":I
    :cond_0
    return-object v4

    .line 1233
    .restart local v0    # "childElement":Lcom/samsung/app/video/editor/external/Element;
    .restart local v1    # "childElementStartTime":F
    .restart local v2    # "i":I
    .restart local v3    # "listSize":I
    :cond_1
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "childElement":Lcom/samsung/app/video/editor/external/Element;
    check-cast v0, Lcom/samsung/app/video/editor/external/Element;

    .line 1234
    .restart local v0    # "childElement":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {v0}, Lcom/samsung/app/video/editor/external/Element;->getStoryBoardStartTime()J

    move-result-wide v6

    long-to-float v1, v6

    .line 1240
    cmpl-float v5, v1, p2

    if-ltz v5, :cond_2

    .line 1241
    cmpg-float v5, v1, p2

    if-gtz v5, :cond_2

    .line 1242
    invoke-virtual {v4, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 1232
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public getRecordsBetween(FF)Ljava/util/Vector;
    .locals 8
    .param p1, "storyBoardStartTime"    # F
    .param p2, "storyBoardEndTime"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(FF)",
            "Ljava/util/Vector",
            "<",
            "Lcom/samsung/app/video/editor/external/Element;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1272
    new-instance v5, Ljava/util/Vector;

    invoke-direct {v5}, Ljava/util/Vector;-><init>()V

    .line 1274
    .local v5, "records":Ljava/util/Vector;, "Ljava/util/Vector<Lcom/samsung/app/video/editor/external/Element;>;"
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getRecordListCount()I

    move-result v0

    .line 1275
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v0, :cond_0

    .line 1287
    return-object v5

    .line 1276
    :cond_0
    iget-object v6, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlNarrationEleList:Ljava/util/List;

    invoke-interface {v6, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/app/video/editor/external/Element;

    .line 1278
    .local v2, "record":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/Element;->getStoryBoardStartTime()J

    move-result-wide v6

    long-to-float v4, v6

    .line 1279
    .local v4, "recordStartBoardStartTime":F
    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/Element;->getStoryBoardEndTime()J

    move-result-wide v6

    long-to-float v3, v6

    .line 1281
    .local v3, "recordStartBoardEndTime":F
    cmpl-float v6, p1, v4

    if-ltz v6, :cond_1

    cmpg-float v6, p1, v3

    if-lez v6, :cond_3

    .line 1282
    :cond_1
    cmpl-float v6, p2, v4

    if-ltz v6, :cond_2

    cmpg-float v6, p2, v3

    if-lez v6, :cond_3

    .line 1283
    :cond_2
    cmpg-float v6, p1, v4

    if-gtz v6, :cond_4

    cmpl-float v6, p2, v3

    if-ltz v6, :cond_4

    .line 1284
    :cond_3
    invoke-virtual {v5, v2}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 1275
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public getRecordsBetween(IFF)Ljava/util/Vector;
    .locals 8
    .param p1, "index"    # I
    .param p2, "storyBoardStartTime"    # F
    .param p3, "storyBoardEndTime"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IFF)",
            "Ljava/util/Vector",
            "<",
            "Lcom/samsung/app/video/editor/external/Element;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1292
    new-instance v5, Ljava/util/Vector;

    invoke-direct {v5}, Ljava/util/Vector;-><init>()V

    .line 1294
    .local v5, "records":Ljava/util/Vector;, "Ljava/util/Vector<Lcom/samsung/app/video/editor/external/Element;>;"
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getRecordListCount()I

    move-result v0

    .line 1295
    .local v0, "count":I
    move v1, p1

    .local v1, "i":I
    :goto_0
    if-lt v1, v0, :cond_0

    .line 1306
    return-object v5

    .line 1296
    :cond_0
    iget-object v6, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlNarrationEleList:Ljava/util/List;

    invoke-interface {v6, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/app/video/editor/external/Element;

    .line 1298
    .local v2, "record":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/Element;->getStoryBoardStartTime()J

    move-result-wide v6

    long-to-float v4, v6

    .line 1299
    .local v4, "recordStartBoardStartTime":F
    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/Element;->getStoryBoardEndTime()J

    move-result-wide v6

    long-to-float v3, v6

    .line 1301
    .local v3, "recordStartBoardEndTime":F
    cmpl-float v6, p3, v4

    if-ltz v6, :cond_1

    cmpg-float v6, p3, v3

    if-lez v6, :cond_2

    .line 1302
    :cond_1
    cmpg-float v6, p2, v4

    if-gtz v6, :cond_3

    cmpl-float v6, p3, v3

    if-ltz v6, :cond_3

    .line 1303
    :cond_2
    invoke-virtual {v5, v2}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 1295
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public getResolutionEnumValue()I
    .locals 1

    .prologue
    .line 1310
    iget v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->resolutionEnumValue:I

    return v0
.end method

.method public getSoundAt(F)Lcom/samsung/app/video/editor/external/Element;
    .locals 8
    .param p1, "storyBoardTime"    # F

    .prologue
    .line 1314
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getSoundListCount()I

    move-result v0

    .line 1315
    .local v0, "count":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-lt v3, v0, :cond_1

    .line 1326
    const/4 v4, 0x0

    :cond_0
    return-object v4

    .line 1316
    :cond_1
    iget-object v5, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlSoundEleList:Ljava/util/List;

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/app/video/editor/external/Element;

    .line 1318
    .local v4, "music":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/Element;->getStoryBoardStartTime()J

    move-result-wide v6

    long-to-float v2, v6

    .line 1319
    .local v2, "drawingStartBoardStartTime":F
    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/Element;->getStoryBoardEndTime()J

    move-result-wide v6

    long-to-float v1, v6

    .line 1321
    .local v1, "drawingStartBoardEndTime":F
    cmpl-float v5, p1, v2

    if-ltz v5, :cond_2

    .line 1322
    cmpg-float v5, p1, v1

    if-lez v5, :cond_0

    .line 1315
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public getSoundListCount()I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1330
    iget-object v1, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlSoundEleList:Ljava/util/List;

    if-eqz v1, :cond_1

    .line 1331
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlSoundEleList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 1336
    :cond_0
    :goto_0
    return v0

    .line 1332
    :cond_1
    iget-object v1, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlSoundEleList:Ljava/util/List;

    if-nez v1, :cond_0

    goto :goto_0
.end method

.method public getStartPostTillPixelInGroup(FLcom/samsung/app/video/editor/external/Element;I)Lcom/samsung/app/video/editor/external/TranscodeElement$GroupData;
    .locals 20
    .param p1, "pix"    # F
    .param p2, "e"    # Lcom/samsung/app/video/editor/external/Element;
    .param p3, "width"    # I

    .prologue
    .line 1341
    const/4 v4, 0x0

    .line 1342
    .local v4, "element":Lcom/samsung/app/video/editor/external/Element;
    const/4 v7, 0x0

    .line 1343
    .local v7, "inElePix":F
    const/4 v3, 0x0

    .line 1344
    .local v3, "currentPix":F
    const/4 v8, 0x0

    .line 1345
    .local v8, "inEleTime":F
    new-instance v9, Lcom/samsung/app/video/editor/external/TranscodeElement$GroupData;

    invoke-direct {v9}, Lcom/samsung/app/video/editor/external/TranscodeElement$GroupData;-><init>()V

    .line 1346
    .local v9, "inElementData":Lcom/samsung/app/video/editor/external/TranscodeElement$GroupData;
    new-instance v6, Ljava/util/Vector;

    invoke-direct {v6}, Ljava/util/Vector;-><init>()V

    .line 1348
    .local v6, "groupList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v2

    .line 1349
    .local v2, "count":I
    const/4 v10, 0x0

    .local v10, "j":I
    :goto_0
    if-lt v10, v2, :cond_1

    .line 1356
    move/from16 v0, p3

    int-to-float v13, v0

    cmpl-float v13, p1, v13

    if-nez v13, :cond_3

    .line 1357
    if-eqz v4, :cond_0

    .line 1358
    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/Element;->getEndTime()J

    move-result-wide v16

    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/Element;->getStartTime()J

    move-result-wide v18

    sub-long v14, v16, v18

    .line 1359
    .local v14, "time":J
    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/Element;->getOverLapAtEnd()I

    move-result v13

    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/Element;->getOverLapAtStart()I

    move-result v16

    add-int v12, v13, v16

    .line 1360
    .local v12, "overLapTime":I
    int-to-long v0, v12

    move-wide/from16 v16, v0

    sub-long v14, v14, v16

    .line 1361
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v13

    add-int/lit8 v13, v13, -0x1

    invoke-interface {v6, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/samsung/app/video/editor/external/Element;

    iput-object v13, v9, Lcom/samsung/app/video/editor/external/TranscodeElement$GroupData;->element:Lcom/samsung/app/video/editor/external/Element;

    .line 1362
    long-to-int v13, v14

    iput v13, v9, Lcom/samsung/app/video/editor/external/TranscodeElement$GroupData;->inEleTime:I

    .line 1382
    .end local v12    # "overLapTime":I
    .end local v14    # "time":J
    :cond_0
    :goto_1
    return-object v9

    .line 1350
    :cond_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/app/video/editor/external/TranscodeElement;->elementList:Ljava/util/List;

    invoke-interface {v13, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "element":Lcom/samsung/app/video/editor/external/Element;
    check-cast v4, Lcom/samsung/app/video/editor/external/Element;

    .line 1351
    .restart local v4    # "element":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/app/video/editor/external/Element;->getGroupID()I

    move-result v13

    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/Element;->getGroupID()I

    move-result v16

    move/from16 v0, v16

    if-ne v13, v0, :cond_2

    .line 1352
    invoke-interface {v6, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1349
    :cond_2
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 1365
    :cond_3
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v11

    .line 1366
    .local v11, "listCount":I
    const/4 v10, 0x0

    :goto_2
    if-ge v10, v11, :cond_0

    .line 1367
    invoke-interface {v6, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "element":Lcom/samsung/app/video/editor/external/Element;
    check-cast v4, Lcom/samsung/app/video/editor/external/Element;

    .line 1368
    .restart local v4    # "element":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/Element;->getElementRatioInGroup()F

    move-result v13

    move/from16 v0, p3

    int-to-float v0, v0

    move/from16 v16, v0

    mul-float v5, v13, v16

    .line 1369
    .local v5, "elementWidth":F
    add-float/2addr v3, v5

    .line 1370
    cmpg-float v13, p1, v3

    if-gtz v13, :cond_4

    .line 1371
    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/Element;->getEndTime()J

    move-result-wide v16

    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/Element;->getStartTime()J

    move-result-wide v18

    sub-long v14, v16, v18

    .line 1372
    .restart local v14    # "time":J
    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/Element;->getOverLapAtEnd()I

    move-result v13

    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/Element;->getOverLapAtStart()I

    move-result v16

    add-int v12, v13, v16

    .line 1373
    .restart local v12    # "overLapTime":I
    int-to-long v0, v12

    move-wide/from16 v16, v0

    sub-long v14, v14, v16

    .line 1374
    sub-float v13, v3, p1

    sub-float v7, v5, v13

    .line 1375
    long-to-float v13, v14

    mul-float/2addr v13, v7

    div-float v8, v13, v5

    .line 1376
    iput-object v4, v9, Lcom/samsung/app/video/editor/external/TranscodeElement$GroupData;->element:Lcom/samsung/app/video/editor/external/Element;

    .line 1377
    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v13

    iput v13, v9, Lcom/samsung/app/video/editor/external/TranscodeElement$GroupData;->inEleTime:I

    goto :goto_1

    .line 1366
    .end local v12    # "overLapTime":I
    .end local v14    # "time":J
    :cond_4
    add-int/lit8 v10, v10, 0x1

    goto :goto_2
.end method

.method public getStoryTimeTillElement(I)J
    .locals 14
    .param p1, "position"    # I

    .prologue
    .line 1424
    const-wide/16 v8, 0x0

    .line 1425
    .local v8, "time":J
    iget-object v5, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->elementList:Ljava/util/List;

    if-eqz v5, :cond_0

    .line 1426
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-lt v4, p1, :cond_1

    .line 1440
    .end local v4    # "i":I
    :cond_0
    return-wide v8

    .line 1427
    .restart local v4    # "i":I
    :cond_1
    iget-object v5, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->elementList:Ljava/util/List;

    invoke-interface {v5, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/app/video/editor/external/Element;

    .line 1428
    .local v1, "element":Lcom/samsung/app/video/editor/external/Element;
    const-wide/16 v2, 0x0

    .line 1429
    .local v2, "eleTime":J
    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/Element;->getOverLapAtEnd()I

    move-result v5

    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/Element;->getOverLapAtStart()I

    move-result v10

    add-int/2addr v5, v10

    int-to-long v6, v5

    .line 1430
    .local v6, "overLapTime":J
    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/Element;->getEndTime()J

    move-result-wide v10

    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/Element;->getStartTime()J

    move-result-wide v12

    sub-long/2addr v10, v12

    add-long/2addr v2, v10

    .line 1431
    long-to-float v5, v2

    invoke-static {v1}, Lcom/sec/android/app/ve/util/CommonUtils;->getSpeedFactor(Lcom/samsung/app/video/editor/external/Element;)F

    move-result v10

    mul-float/2addr v5, v10

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    int-to-long v2, v5

    .line 1432
    sub-long/2addr v2, v6

    .line 1433
    add-long/2addr v8, v2

    .line 1434
    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/Element;->getTransitionEdit()Lcom/samsung/app/video/editor/external/Edit;

    move-result-object v0

    .line 1435
    .local v0, "edit":Lcom/samsung/app/video/editor/external/Edit;
    if-eqz v0, :cond_2

    .line 1436
    invoke-virtual {v0}, Lcom/samsung/app/video/editor/external/Edit;->getTrans_duration()I

    move-result v5

    int-to-long v10, v5

    add-long/2addr v8, v10

    .line 1426
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method

.method public getStoryTimeTillElement(Lcom/samsung/app/video/editor/external/Element;)J
    .locals 2
    .param p1, "telement"    # Lcom/samsung/app/video/editor/external/Element;

    .prologue
    .line 1395
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getStoryTimeTillElement(Lcom/samsung/app/video/editor/external/Element;Z)J

    move-result-wide v0

    return-wide v0
.end method

.method public getStoryTimeTillElement(Lcom/samsung/app/video/editor/external/Element;Z)J
    .locals 14
    .param p1, "telement"    # Lcom/samsung/app/video/editor/external/Element;
    .param p2, "hasTransitionView"    # Z

    .prologue
    .line 1398
    const-wide/16 v8, 0x0

    .line 1399
    .local v8, "time":J
    iget-object v10, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->elementList:Ljava/util/List;

    if-eqz v10, :cond_0

    .line 1400
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v0

    .line 1401
    .local v0, "count":I
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    if-lt v5, v0, :cond_1

    .line 1419
    .end local v0    # "count":I
    .end local v5    # "i":I
    :cond_0
    return-wide v8

    .line 1402
    .restart local v0    # "count":I
    .restart local v5    # "i":I
    :cond_1
    iget-object v10, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->elementList:Ljava/util/List;

    invoke-interface {v10, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/app/video/editor/external/Element;

    .line 1403
    .local v4, "element":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {v4, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_0

    .line 1405
    const-wide/16 v2, 0x0

    .line 1406
    .local v2, "eleTime":J
    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/Element;->getEndTime()J

    move-result-wide v10

    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/Element;->getStartTime()J

    move-result-wide v12

    sub-long/2addr v10, v12

    add-long/2addr v2, v10

    .line 1407
    if-eqz p2, :cond_2

    .line 1408
    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/Element;->getOverLapAtEnd()I

    move-result v10

    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/Element;->getOverLapAtStart()I

    move-result v11

    add-int/2addr v10, v11

    int-to-long v6, v10

    .line 1409
    .local v6, "overLapTime":J
    long-to-float v10, v2

    invoke-static {v4}, Lcom/sec/android/app/ve/util/CommonUtils;->getSpeedFactor(Lcom/samsung/app/video/editor/external/Element;)F

    move-result v11

    mul-float/2addr v10, v11

    invoke-static {v10}, Ljava/lang/Math;->round(F)I

    move-result v10

    int-to-long v2, v10

    .line 1410
    sub-long/2addr v2, v6

    .line 1411
    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/Element;->getTransitionEdit()Lcom/samsung/app/video/editor/external/Edit;

    move-result-object v1

    .line 1412
    .local v1, "edit":Lcom/samsung/app/video/editor/external/Edit;
    if-eqz v1, :cond_2

    .line 1413
    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/Edit;->getTrans_duration()I

    move-result v10

    int-to-long v10, v10

    add-long/2addr v8, v10

    .line 1415
    .end local v1    # "edit":Lcom/samsung/app/video/editor/external/Edit;
    .end local v6    # "overLapTime":J
    :cond_2
    add-long/2addr v8, v2

    .line 1401
    add-int/lit8 v5, v5, 0x1

    goto :goto_0
.end method

.method public getSummaryGroupTotalTime(Ljava/util/List;)J
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/Element;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 1444
    .local p1, "groupList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    const-wide/16 v6, 0x0

    .line 1445
    .local v6, "totalDuration":J
    iget-object v5, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->elementList:Ljava/util/List;

    if-eqz v5, :cond_0

    .line 1446
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_0

    .line 1447
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v5

    add-int/lit8 v3, v5, -0x1

    .line 1448
    .local v3, "listSize":I
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_0
    if-lt v2, v3, :cond_1

    .line 1458
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/app/video/editor/external/Element;

    .line 1459
    .local v1, "element":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/Element;->getEndTime()J

    move-result-wide v8

    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/Element;->getStartTime()J

    move-result-wide v10

    sub-long/2addr v8, v10

    add-long/2addr v6, v8

    .line 1460
    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/Element;->getOverLapAtEnd()I

    move-result v5

    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/Element;->getOverLapAtStart()I

    move-result v8

    add-int v4, v5, v8

    .line 1461
    .local v4, "overLapTime":I
    int-to-long v8, v4

    sub-long/2addr v6, v8

    .line 1464
    .end local v1    # "element":Lcom/samsung/app/video/editor/external/Element;
    .end local v2    # "j":I
    .end local v3    # "listSize":I
    .end local v4    # "overLapTime":I
    :cond_0
    return-wide v6

    .line 1449
    .restart local v2    # "j":I
    .restart local v3    # "listSize":I
    :cond_1
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/app/video/editor/external/Element;

    .line 1450
    .restart local v1    # "element":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/Element;->getEndTime()J

    move-result-wide v8

    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/Element;->getStartTime()J

    move-result-wide v10

    sub-long/2addr v8, v10

    add-long/2addr v6, v8

    .line 1451
    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/Element;->getOverLapAtEnd()I

    move-result v5

    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/Element;->getOverLapAtStart()I

    move-result v8

    add-int v4, v5, v8

    .line 1452
    .restart local v4    # "overLapTime":I
    int-to-long v8, v4

    sub-long/2addr v6, v8

    .line 1453
    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/Element;->getTransitionEdit()Lcom/samsung/app/video/editor/external/Edit;

    move-result-object v0

    .line 1454
    .local v0, "edit":Lcom/samsung/app/video/editor/external/Edit;
    if-eqz v0, :cond_2

    .line 1455
    invoke-virtual {v0}, Lcom/samsung/app/video/editor/external/Edit;->getTrans_duration()I

    move-result v5

    int-to-long v8, v5

    add-long/2addr v6, v8

    .line 1448
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public getTargetDispHeight()I
    .locals 1

    .prologue
    .line 1468
    iget v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->TargetDispHeight:I

    return v0
.end method

.method public getTargetDispWidth()I
    .locals 1

    .prologue
    .line 1472
    iget v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->TargetDispWidth:I

    return v0
.end method

.method public getTempAudioEleList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/Element;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1479
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->tempAudioEleList:Ljava/util/List;

    return-object v0
.end method

.method public getTempAudioListCount()I
    .locals 1

    .prologue
    .line 1483
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->tempAudioEleList:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 1484
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->tempAudioEleList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 1486
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getTempMusicAt(F)Lcom/samsung/app/video/editor/external/Element;
    .locals 8
    .param p1, "storyBoardTime"    # F

    .prologue
    .line 1490
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getTempAudioListCount()I

    move-result v0

    .line 1491
    .local v0, "count":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-lt v3, v0, :cond_1

    .line 1502
    const/4 v4, 0x0

    :cond_0
    return-object v4

    .line 1492
    :cond_1
    iget-object v5, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->tempAudioEleList:Ljava/util/List;

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/app/video/editor/external/Element;

    .line 1494
    .local v4, "music":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/Element;->getStoryBoardStartTime()J

    move-result-wide v6

    long-to-float v2, v6

    .line 1495
    .local v2, "drawingStartBoardStartTime":F
    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/Element;->getStoryBoardEndTime()J

    move-result-wide v6

    long-to-float v1, v6

    .line 1496
    .local v1, "drawingStartBoardEndTime":F
    cmpl-float v5, p1, v2

    if-ltz v5, :cond_2

    .line 1497
    cmpg-float v5, p1, v1

    if-lez v5, :cond_0

    .line 1491
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public getTempMusicInTimeRange(FF)Ljava/util/ArrayList;
    .locals 8
    .param p1, "storyBoardStartTime"    # F
    .param p2, "storyBoardEndTime"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(FF)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/app/video/editor/external/Element;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1506
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1507
    .local v2, "elementList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/app/video/editor/external/Element;>;"
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getTempAudioListCount()I

    move-result v1

    .line 1508
    .local v1, "count":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-lt v3, v1, :cond_0

    .line 1523
    return-object v2

    .line 1509
    :cond_0
    iget-object v5, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->tempAudioEleList:Ljava/util/List;

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/app/video/editor/external/Element;

    .line 1510
    .local v4, "music":Lcom/samsung/app/video/editor/external/Element;
    const/4 v0, 0x1

    .line 1512
    .local v0, "add":Z
    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/Element;->getStoryBoardEndTime()J

    move-result-wide v6

    long-to-float v5, v6

    cmpg-float v5, v5, p1

    if-gez v5, :cond_3

    .line 1513
    const/4 v0, 0x0

    .line 1519
    :cond_1
    :goto_1
    if-eqz v0, :cond_2

    .line 1520
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1508
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1515
    :cond_3
    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/Element;->getStoryBoardStartTime()J

    move-result-wide v6

    long-to-float v5, v6

    cmpl-float v5, v5, p2

    if-lez v5, :cond_1

    .line 1516
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public getTextAt(F)Lcom/samsung/app/video/editor/external/ClipartParams;
    .locals 8
    .param p1, "storyBoardTime"    # F

    .prologue
    .line 1528
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getTextEleListCount()I

    move-result v0

    .line 1529
    .local v0, "count":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-lt v4, v0, :cond_1

    .line 1542
    const/4 v1, 0x0

    :cond_0
    return-object v1

    .line 1530
    :cond_1
    iget-object v5, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->textEleList:Ljava/util/List;

    invoke-interface {v5, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/app/video/editor/external/ClipartParams;

    .line 1532
    .local v1, "drawing":Lcom/samsung/app/video/editor/external/ClipartParams;
    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/ClipartParams;->getStoryBoardStartTime()J

    move-result-wide v6

    long-to-float v3, v6

    .line 1533
    .local v3, "drawingStartBoardStartTime":F
    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/ClipartParams;->getStoryBoardEndTime()J

    move-result-wide v6

    long-to-float v2, v6

    .line 1535
    .local v2, "drawingStartBoardEndTime":F
    cmpl-float v5, p1, v3

    if-ltz v5, :cond_2

    .line 1536
    cmpg-float v5, p1, v2

    if-lez v5, :cond_0

    .line 1529
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method

.method public getTextClipartParamsListForElement(Lcom/samsung/app/video/editor/external/Element;)Ljava/util/List;
    .locals 6
    .param p1, "element"    # Lcom/samsung/app/video/editor/external/Element;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/app/video/editor/external/Element;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/ClipartParams;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2205
    if-nez p1, :cond_1

    .line 2206
    const/4 v1, 0x0

    .line 2216
    :cond_0
    return-object v1

    .line 2207
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/Element;->getGroupID()I

    move-result v3

    .line 2208
    .local v3, "id":I
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2209
    .local v1, "currentClipartList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getTextEleListCount()I

    move-result v0

    .line 2210
    .local v0, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v0, :cond_0

    .line 2211
    iget-object v5, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->textEleList:Ljava/util/List;

    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/app/video/editor/external/ClipartParams;

    .line 2212
    .local v4, "text":Lcom/samsung/app/video/editor/external/ClipartParams;
    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/ClipartParams;->getElementID()I

    move-result v5

    if-ne v5, v3, :cond_2

    .line 2213
    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2210
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public getTextEleList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/ClipartParams;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1546
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->textEleList:Ljava/util/List;

    return-object v0
.end method

.method public getTextEleListCount()I
    .locals 1

    .prologue
    .line 1550
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->textEleList:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 1551
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->textEleList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 1553
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getTextsBetween(FF)Ljava/util/Vector;
    .locals 8
    .param p1, "storyBoardStartTime"    # F
    .param p2, "storyBoardEndTime"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(FF)",
            "Ljava/util/Vector",
            "<",
            "Lcom/samsung/app/video/editor/external/ClipartParams;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1559
    new-instance v5, Ljava/util/Vector;

    invoke-direct {v5}, Ljava/util/Vector;-><init>()V

    .line 1561
    .local v5, "texts":Ljava/util/Vector;, "Ljava/util/Vector<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getTextEleListCount()I

    move-result v0

    .line 1562
    .local v0, "count":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-lt v3, v0, :cond_0

    .line 1576
    return-object v5

    .line 1563
    :cond_0
    iget-object v6, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->textEleList:Ljava/util/List;

    invoke-interface {v6, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/app/video/editor/external/ClipartParams;

    .line 1565
    .local v4, "text":Lcom/samsung/app/video/editor/external/ClipartParams;
    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/ClipartParams;->getStoryBoardStartTime()J

    move-result-wide v6

    long-to-float v2, v6

    .line 1566
    .local v2, "drawingStartBoardStartTime":F
    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/ClipartParams;->getStoryBoardEndTime()J

    move-result-wide v6

    long-to-float v1, v6

    .line 1568
    .local v1, "drawingStartBoardEndTime":F
    cmpl-float v6, p1, v2

    if-ltz v6, :cond_1

    cmpg-float v6, p1, v1

    if-lez v6, :cond_3

    .line 1569
    :cond_1
    cmpl-float v6, p2, v2

    if-ltz v6, :cond_2

    cmpg-float v6, p2, v1

    if-lez v6, :cond_3

    .line 1570
    :cond_2
    cmpg-float v6, p1, v2

    if-gtz v6, :cond_4

    cmpl-float v6, p2, v1

    if-ltz v6, :cond_4

    .line 1571
    :cond_3
    invoke-virtual {v5, v4}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 1562
    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public getThemeName()I
    .locals 1

    .prologue
    .line 1580
    iget v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->mThemeName:I

    return v0
.end method

.method public getThumbnail(Z)Landroid/graphics/Bitmap;
    .locals 2
    .param p1, "createIfRequired"    # Z

    .prologue
    .line 2589
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->mThumbnail:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 2590
    const-string v0, "VIDEO EDITOR"

    const-string v1, " Bitmap need to be created , call getBitmapFromEngine"

    invoke-static {v0, v1}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 2591
    const-wide/16 v0, 0x3a98

    invoke-static {p0, v0, v1}, Lcom/sec/android/app/ve/util/CommonUtils;->getBitmapFromEngine(Lcom/samsung/app/video/editor/external/TranscodeElement;J)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->mThumbnail:Landroid/graphics/Bitmap;

    .line 2594
    :cond_0
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->mThumbnail:Landroid/graphics/Bitmap;

    if-nez v0, :cond_1

    .line 2595
    const-string v0, "getThumbnail returned null"

    invoke-static {v0}, Lcom/sec/android/app/ve/common/LogUtils;->log(Ljava/lang/String;)V

    .line 2597
    :cond_1
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->mThumbnail:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getThumbnail(ZI)Landroid/graphics/Bitmap;
    .locals 2
    .param p1, "createIfRequired"    # Z
    .param p2, "time"    # I

    .prologue
    .line 2602
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->mThumbnail:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 2603
    int-to-long v0, p2

    invoke-static {p0, v0, v1}, Lcom/sec/android/app/ve/util/CommonUtils;->getBitmapFromEngine(Lcom/samsung/app/video/editor/external/TranscodeElement;J)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->mThumbnail:Landroid/graphics/Bitmap;

    .line 2604
    :cond_0
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->mThumbnail:Landroid/graphics/Bitmap;

    if-nez v0, :cond_1

    .line 2605
    const-string v0, "getThumbnail returned null"

    invoke-static {v0}, Lcom/sec/android/app/ve/common/LogUtils;->log(Ljava/lang/String;)V

    .line 2606
    :cond_1
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->mThumbnail:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getTotalDuration()J
    .locals 20

    .prologue
    .line 1584
    const-wide/16 v14, 0x0

    .line 1585
    .local v14, "totalDuration":J
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/samsung/app/video/editor/external/TranscodeElement;->isSummaryProject:Z

    if-eqz v13, :cond_3

    .line 1586
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v2

    .line 1587
    .local v2, "count":I
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_0
    if-lt v10, v2, :cond_1

    .line 1623
    :cond_0
    return-wide v14

    .line 1588
    :cond_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/app/video/editor/external/TranscodeElement;->elementList:Ljava/util/List;

    invoke-interface {v13, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/app/video/editor/external/Element;

    .line 1589
    .local v6, "ele":Lcom/samsung/app/video/editor/external/Element;
    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getLastElementInGroup(Lcom/samsung/app/video/editor/external/Element;)Lcom/samsung/app/video/editor/external/Element;

    move-result-object v7

    .line 1590
    .local v7, "element":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {v7, v6}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    .line 1591
    invoke-virtual {v7}, Lcom/samsung/app/video/editor/external/Element;->getGroupDuration()J

    move-result-wide v16

    add-long v14, v14, v16

    .line 1592
    invoke-virtual {v7}, Lcom/samsung/app/video/editor/external/Element;->getTransitionEdit()Lcom/samsung/app/video/editor/external/Edit;

    move-result-object v3

    .line 1593
    .local v3, "edit":Lcom/samsung/app/video/editor/external/Edit;
    if-eqz v3, :cond_2

    .line 1594
    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/Edit;->getTrans_duration()I

    move-result v13

    int-to-long v0, v13

    move-wide/from16 v16, v0

    add-long v14, v14, v16

    .line 1587
    .end local v3    # "edit":Lcom/samsung/app/video/editor/external/Edit;
    :cond_2
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 1599
    .end local v2    # "count":I
    .end local v6    # "ele":Lcom/samsung/app/video/editor/external/Element;
    .end local v7    # "element":Lcom/samsung/app/video/editor/external/Element;
    .end local v10    # "i":I
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v2

    .line 1600
    .restart local v2    # "count":I
    const/4 v10, 0x0

    .restart local v10    # "i":I
    :goto_1
    if-ge v10, v2, :cond_0

    .line 1601
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/app/video/editor/external/TranscodeElement;->elementList:Ljava/util/List;

    invoke-interface {v13, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/app/video/editor/external/Element;

    .line 1602
    .restart local v7    # "element":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {v7}, Lcom/samsung/app/video/editor/external/Element;->getEndTime()J

    move-result-wide v16

    invoke-virtual {v7}, Lcom/samsung/app/video/editor/external/Element;->getStartTime()J

    move-result-wide v18

    sub-long v8, v16, v18

    .line 1603
    .local v8, "eleDuration":J
    invoke-virtual {v7}, Lcom/samsung/app/video/editor/external/Element;->getOverLapAtEnd()I

    move-result v13

    invoke-virtual {v7}, Lcom/samsung/app/video/editor/external/Element;->getOverLapAtStart()I

    move-result v16

    add-int v12, v13, v16

    .line 1604
    .local v12, "overLapTime":I
    long-to-float v13, v8

    invoke-static {v7}, Lcom/sec/android/app/ve/util/CommonUtils;->getSpeedFactor(Lcom/samsung/app/video/editor/external/Element;)F

    move-result v16

    mul-float v13, v13, v16

    invoke-static {v13}, Ljava/lang/Math;->round(F)I

    move-result v13

    int-to-long v8, v13

    .line 1605
    int-to-long v0, v12

    move-wide/from16 v16, v0

    sub-long v8, v8, v16

    .line 1606
    add-long/2addr v14, v8

    .line 1608
    invoke-virtual {v7}, Lcom/samsung/app/video/editor/external/Element;->getEditList()Ljava/util/List;

    move-result-object v13

    if-eqz v13, :cond_4

    .line 1609
    invoke-virtual {v7}, Lcom/samsung/app/video/editor/external/Element;->getEditListSize()I

    move-result v13

    if-eqz v13, :cond_4

    .line 1610
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v13

    add-int/lit8 v13, v13, -0x1

    if-eq v10, v13, :cond_4

    .line 1611
    invoke-virtual {v7}, Lcom/samsung/app/video/editor/external/Element;->getEditList()Ljava/util/List;

    move-result-object v4

    .line 1612
    .local v4, "editList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Edit;>;"
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    .line 1613
    .local v5, "editSize":I
    const/4 v11, 0x0

    .local v11, "j":I
    :goto_2
    if-lt v11, v5, :cond_5

    .line 1600
    .end local v4    # "editList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Edit;>;"
    .end local v5    # "editSize":I
    .end local v11    # "j":I
    :cond_4
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    .line 1614
    .restart local v4    # "editList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Edit;>;"
    .restart local v5    # "editSize":I
    .restart local v11    # "j":I
    :cond_5
    invoke-interface {v4, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/samsung/app/video/editor/external/Edit;

    invoke-virtual {v13}, Lcom/samsung/app/video/editor/external/Edit;->getType()I

    move-result v13

    const/16 v16, 0x6

    move/from16 v0, v16

    if-ne v13, v0, :cond_6

    .line 1615
    invoke-interface {v4, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/samsung/app/video/editor/external/Edit;

    invoke-virtual {v13}, Lcom/samsung/app/video/editor/external/Edit;->getSubType()I

    move-result v13

    if-eqz v13, :cond_6

    .line 1617
    invoke-interface {v4, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/samsung/app/video/editor/external/Edit;

    invoke-virtual {v13}, Lcom/samsung/app/video/editor/external/Edit;->getTrans_duration()I

    move-result v13

    int-to-long v0, v13

    move-wide/from16 v16, v0

    add-long v14, v14, v16

    .line 1613
    :cond_6
    add-int/lit8 v11, v11, 0x1

    goto :goto_2
.end method

.method public getTotalDurationForSummary()J
    .locals 10

    .prologue
    .line 2924
    const-wide/16 v4, 0x0

    .line 2925
    .local v4, "totalDuration":J
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v0

    .line 2926
    .local v0, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-lt v2, v0, :cond_0

    .line 2930
    return-wide v4

    .line 2927
    :cond_0
    iget-object v3, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->elementList:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/app/video/editor/external/Element;

    .line 2928
    .local v1, "element":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/Element;->getEndTime()J

    move-result-wide v6

    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/Element;->getStartTime()J

    move-result-wide v8

    sub-long/2addr v6, v8

    add-long/2addr v4, v6

    .line 2926
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public getTotalDurationWithoutTransition()J
    .locals 12

    .prologue
    .line 1632
    const-wide/16 v6, 0x0

    .line 1633
    .local v6, "totalTime":J
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v0

    .line 1634
    .local v0, "count":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-lt v4, v0, :cond_0

    .line 1641
    return-wide v6

    .line 1635
    :cond_0
    iget-object v5, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->elementList:Ljava/util/List;

    invoke-interface {v5, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/app/video/editor/external/Element;

    .line 1636
    .local v1, "element":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/Element;->getEndTime()J

    move-result-wide v8

    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/Element;->getStartTime()J

    move-result-wide v10

    sub-long v2, v8, v10

    .line 1637
    .local v2, "eleTime":J
    long-to-float v5, v2

    invoke-static {v1}, Lcom/sec/android/app/ve/util/CommonUtils;->getSpeedFactor(Lcom/samsung/app/video/editor/external/Element;)F

    move-result v8

    mul-float/2addr v5, v8

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    int-to-long v2, v5

    .line 1638
    add-long/2addr v6, v2

    .line 1634
    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method

.method public getTranstionsTimeBetweenRecordings()F
    .locals 10

    .prologue
    .line 1645
    const/4 v4, 0x0

    .line 1647
    .local v4, "time":F
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v6

    add-int/lit8 v0, v6, -0x1

    .line 1648
    .local v0, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-lt v2, v0, :cond_0

    .line 1662
    return v4

    .line 1649
    :cond_0
    iget-object v6, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->elementList:Ljava/util/List;

    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/app/video/editor/external/Element;

    .line 1650
    .local v1, "element":Lcom/samsung/app/video/editor/external/Element;
    iget-object v6, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->elementList:Ljava/util/List;

    add-int/lit8 v7, v2, 0x1

    invoke-interface {v6, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/app/video/editor/external/Element;

    .line 1652
    .local v3, "nxtElement":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/Element;->getEndTime()J

    move-result-wide v6

    long-to-float v6, v6

    .line 1653
    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/Element;->getStartTime()J

    move-result-wide v8

    long-to-float v7, v8

    .line 1652
    invoke-virtual {p0, v6, v7}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getRecordsBetween(FF)Ljava/util/Vector;

    move-result-object v6

    .line 1653
    invoke-virtual {v6}, Ljava/util/Vector;->size()I

    move-result v6

    if-lez v6, :cond_1

    .line 1654
    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/Element;->getTransitionEdit()Lcom/samsung/app/video/editor/external/Edit;

    move-result-object v5

    .line 1655
    .local v5, "transitionEdit":Lcom/samsung/app/video/editor/external/Edit;
    if-eqz v5, :cond_1

    .line 1656
    invoke-virtual {v5}, Lcom/samsung/app/video/editor/external/Edit;->getTrans_duration()I

    move-result v6

    int-to-float v6, v6

    add-float/2addr v4, v6

    .line 1648
    .end local v5    # "transitionEdit":Lcom/samsung/app/video/editor/external/Edit;
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public getUniqueProjectID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2022
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->mUniqueProjectID:Ljava/lang/String;

    return-object v0
.end method

.method public getexportHM()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/samsung/app/video/editor/external/TranscodeElement$ExportedTranscodeElement;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2958
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->exporthm:Ljava/util/HashMap;

    return-object v0
.end method

.method public getfilePathforEngine()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2014
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->mfilesPathforEngine:Ljava/lang/String;

    return-object v0
.end method

.method public insertElement(Lcom/samsung/app/video/editor/external/Element;I)V
    .locals 1
    .param p1, "element"    # Lcom/samsung/app/video/editor/external/Element;
    .param p2, "position"    # I

    .prologue
    .line 1666
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->elementList:Ljava/util/List;

    if-nez v0, :cond_0

    .line 1667
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->elementList:Ljava/util/List;

    .line 1668
    :cond_0
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->elementList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1669
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->elementList:Ljava/util/List;

    invoke-interface {v0, p2, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 1671
    :cond_1
    invoke-direct {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->recheckTranstionElements()V

    .line 1674
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->overlapTransitionsHandling()V

    .line 1675
    iget-boolean v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->isSummaryProject:Z

    if-nez v0, :cond_2

    .line 1676
    invoke-direct {p0, p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->setUniqueID(Lcom/samsung/app/video/editor/external/Element;)V

    .line 1677
    :cond_2
    return-void
.end method

.method public isAPreview()Z
    .locals 1

    .prologue
    .line 1683
    iget-boolean v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->isAPreview:Z

    return v0
.end method

.method public isElementUsed(Lcom/samsung/app/video/editor/external/Element;)Z
    .locals 4
    .param p1, "element"    # Lcom/samsung/app/video/editor/external/Element;

    .prologue
    .line 1687
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v0

    .line 1688
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v0, :cond_0

    .line 1697
    const/4 v3, 0x0

    :goto_1
    return v3

    .line 1689
    :cond_0
    iget-object v3, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->elementList:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/app/video/editor/external/Element;

    .line 1691
    .local v2, "mainElement":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {v2, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1692
    const/4 v3, 0x1

    goto :goto_1

    .line 1688
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public isItGotThemeMusicOnly()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1701
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getAudioListCount()I

    move-result v3

    if-ne v3, v1, :cond_1

    .line 1702
    const/4 v0, 0x0

    .line 1703
    .local v0, "element":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getAdditionlAudioEleList()Ljava/util/List;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 1704
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getAdditionlAudioEleList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "element":Lcom/samsung/app/video/editor/external/Element;
    check-cast v0, Lcom/samsung/app/video/editor/external/Element;

    .line 1706
    .restart local v0    # "element":Lcom/samsung/app/video/editor/external/Element;
    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/samsung/app/video/editor/external/Element;->getFilePath()Ljava/lang/String;

    move-result-object v3

    .line 1707
    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getFilesDirectory()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    .line 1706
    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    .line 1707
    if-eqz v3, :cond_1

    .line 1712
    .end local v0    # "element":Lcom/samsung/app/video/editor/external/Element;
    :goto_0
    return v1

    :cond_1
    move v1, v2

    goto :goto_0
.end method

.method public isMissingFileProject()Z
    .locals 1

    .prologue
    .line 1715
    iget-boolean v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->isMissingFileProject:Z

    return v0
.end method

.method public isOverLapTransitionEnabled(Lcom/samsung/app/video/editor/external/Element;)I
    .locals 6
    .param p1, "element"    # Lcom/samsung/app/video/editor/external/Element;

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 2402
    invoke-static {}, Lcom/sec/android/app/ve/VEApp;->getJNIVersion()F

    move-result v3

    const/high16 v4, 0x40000000    # 2.0f

    cmpl-float v3, v3, v4

    if-ltz v3, :cond_2

    .line 2403
    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/Element;->getTransitionEdit()Lcom/samsung/app/video/editor/external/Edit;

    move-result-object v1

    .line 2404
    .local v1, "transitionEdit":Lcom/samsung/app/video/editor/external/Edit;
    iput-boolean v2, p1, Lcom/samsung/app/video/editor/external/Element;->isOverLapPossible:Z

    .line 2405
    invoke-virtual {p0, p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getMyPosition(Lcom/samsung/app/video/editor/external/Element;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {p0, v3}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElement(I)Lcom/samsung/app/video/editor/external/Element;

    move-result-object v0

    .line 2406
    .local v0, "next":Lcom/samsung/app/video/editor/external/Element;
    if-eqz v1, :cond_1

    .line 2407
    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/Edit;->getSubType()I

    move-result v3

    if-eqz v3, :cond_1

    .line 2408
    invoke-virtual {p1, v5}, Lcom/samsung/app/video/editor/external/Element;->setNonTimeLossTransitionAtEnd(Z)V

    .line 2409
    invoke-virtual {v0, v5}, Lcom/samsung/app/video/editor/external/Element;->setNonTimeLossTransitionAtStart(Z)V

    .line 2410
    iput-boolean v5, p1, Lcom/samsung/app/video/editor/external/Element;->isOverLapPossible:Z

    .line 2433
    .end local v0    # "next":Lcom/samsung/app/video/editor/external/Element;
    :cond_0
    :goto_0
    return v2

    .line 2414
    .restart local v0    # "next":Lcom/samsung/app/video/editor/external/Element;
    :cond_1
    invoke-virtual {p1, v2}, Lcom/samsung/app/video/editor/external/Element;->setNonTimeLossTransitionAtEnd(Z)V

    .line 2415
    invoke-virtual {v0, v2}, Lcom/samsung/app/video/editor/external/Element;->setNonTimeLossTransitionAtStart(Z)V

    goto :goto_0

    .line 2419
    .end local v0    # "next":Lcom/samsung/app/video/editor/external/Element;
    .end local v1    # "transitionEdit":Lcom/samsung/app/video/editor/external/Edit;
    :cond_2
    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/Element;->getTransitionEdit()Lcom/samsung/app/video/editor/external/Edit;

    move-result-object v1

    .line 2420
    .restart local v1    # "transitionEdit":Lcom/samsung/app/video/editor/external/Edit;
    if-eqz v1, :cond_0

    .line 2421
    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/Edit;->getSubType()I

    move-result v3

    if-eqz v3, :cond_0

    .line 2422
    invoke-virtual {p0, p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getMyPosition(Lcom/samsung/app/video/editor/external/Element;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {p0, v3}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElement(I)Lcom/samsung/app/video/editor/external/Element;

    move-result-object v0

    .line 2423
    .restart local v0    # "next":Lcom/samsung/app/video/editor/external/Element;
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v3

    invoke-virtual {v3, p1, v0}, Lcom/samsung/app/video/editor/external/NativeInterface;->isOverlapPossible(Lcom/samsung/app/video/editor/external/Element;Lcom/samsung/app/video/editor/external/Element;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2425
    iput-boolean v5, p1, Lcom/samsung/app/video/editor/external/Element;->isOverLapPossible:Z

    .line 2430
    :goto_1
    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/Edit;->getTrans_duration()I

    move-result v2

    goto :goto_0

    .line 2428
    :cond_3
    iput-boolean v2, p1, Lcom/samsung/app/video/editor/external/Element;->isOverLapPossible:Z

    goto :goto_1
.end method

.method public isSummaryProject()Z
    .locals 1

    .prologue
    .line 1719
    iget-boolean v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->isSummaryProject:Z

    return v0
.end method

.method public isTempGotThemeMusicOnly(Landroid/content/Context;)Z
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1723
    if-nez p1, :cond_1

    .line 1733
    :cond_0
    :goto_0
    return v1

    .line 1725
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getTempAudioListCount()I

    move-result v3

    if-ne v3, v2, :cond_0

    .line 1726
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getTempAudioEleList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/app/video/editor/external/Element;

    .line 1727
    .local v0, "element":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {v0}, Lcom/samsung/app/video/editor/external/Element;->getFilePath()Ljava/lang/String;

    move-result-object v3

    .line 1728
    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v4

    .line 1729
    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    .line 1727
    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    .line 1729
    if-eqz v3, :cond_0

    move v1, v2

    .line 1730
    goto :goto_0
.end method

.method public isVideoFilePresentInStory()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1737
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v0

    .line 1738
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v0, :cond_0

    .line 1744
    const/4 v2, 0x0

    :goto_1
    return v2

    .line 1739
    :cond_0
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->elementList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/app/video/editor/external/Element;

    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/Element;->getType()I

    move-result v2

    if-ne v2, v3, :cond_1

    move v2, v3

    .line 1740
    goto :goto_1

    .line 1738
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public overlapTransitionsHandling()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 322
    iget-boolean v1, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->isSummaryProject:Z

    if-eqz v1, :cond_1

    .line 340
    :cond_0
    :goto_0
    return-void

    .line 327
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v1

    if-le v1, v3, :cond_2

    .line 329
    invoke-direct {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->setOverLapTransitionFlag()V

    .line 331
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->verifyOverlapAreaDuration()V

    goto :goto_0

    .line 332
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v1

    if-ne v1, v3, :cond_0

    .line 333
    iget-object v1, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->elementList:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/app/video/editor/external/Element;

    .line 334
    .local v0, "current":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {v0, v2}, Lcom/samsung/app/video/editor/external/Element;->setOverLapAtStart(I)V

    .line 335
    invoke-virtual {v0, v2}, Lcom/samsung/app/video/editor/external/Element;->setOverLapAtEnd(I)V

    .line 336
    invoke-virtual {v0, v2}, Lcom/samsung/app/video/editor/external/Element;->setNonTimeLossTransitionAtStart(Z)V

    .line 337
    invoke-virtual {v0, v2}, Lcom/samsung/app/video/editor/external/Element;->setNonTimeLossTransitionAtEnd(Z)V

    goto :goto_0
.end method

.method public removeAdditionlAudioEleList(Lcom/samsung/app/video/editor/external/Element;)V
    .locals 1
    .param p1, "element"    # Lcom/samsung/app/video/editor/external/Element;

    .prologue
    .line 1767
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlAudioEleList:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 1768
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlAudioEleList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1770
    :cond_0
    return-void
.end method

.method public removeBGMMusic()V
    .locals 1

    .prologue
    .line 1773
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlAudioEleList:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 1774
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlAudioEleList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1776
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlAudioEleList:Ljava/util/List;

    .line 1777
    return-void
.end method

.method public removeDrawingEleList(Lcom/samsung/app/video/editor/external/ClipartParams;)V
    .locals 2
    .param p1, "clipParams"    # Lcom/samsung/app/video/editor/external/ClipartParams;

    .prologue
    .line 1780
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->drawingEleList:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 1781
    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/ClipartParams;->remove()V

    .line 1782
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->drawingEleList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1785
    :cond_0
    sget-object v0, Lcom/samsung/app/video/editor/external/TranscodeElement;->mDrawingListComparator:Lcom/samsung/app/video/editor/external/TranscodeElement$DrawingListComparator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->drawingEleList:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 1786
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->drawingEleList:Ljava/util/List;

    sget-object v1, Lcom/samsung/app/video/editor/external/TranscodeElement;->mDrawingListComparator:Lcom/samsung/app/video/editor/external/TranscodeElement$DrawingListComparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1788
    :cond_1
    return-void
.end method

.method public removeDrawingList()V
    .locals 3

    .prologue
    .line 1791
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getDrawingEleListCount()I

    move-result v2

    add-int/lit8 v0, v2, -0x1

    .line 1792
    .local v0, "count":I
    move v1, v0

    .local v1, "i":I
    :goto_0
    if-gez v1, :cond_0

    .line 1796
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->drawingEleList:Ljava/util/List;

    .line 1797
    return-void

    .line 1793
    :cond_0
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->drawingEleList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/app/video/editor/external/ClipartParams;

    invoke-virtual {p0, v2}, Lcom/samsung/app/video/editor/external/TranscodeElement;->removeDrawingEleList(Lcom/samsung/app/video/editor/external/ClipartParams;)V

    .line 1792
    add-int/lit8 v1, v1, -0x1

    goto :goto_0
.end method

.method public removeElement(Lcom/samsung/app/video/editor/external/Element;)Ljava/util/List;
    .locals 2
    .param p1, "element"    # Lcom/samsung/app/video/editor/external/Element;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/app/video/editor/external/Element;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/ClipartParams;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1800
    iget-object v1, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->elementList:Ljava/util/List;

    if-eqz v1, :cond_2

    .line 1801
    const/4 v0, 0x0

    .line 1802
    .local v0, "list":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    iget-object v1, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->elementList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1804
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v1

    if-eqz v1, :cond_0

    .line 1805
    invoke-direct {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->recheckTranstionElements()V

    .line 1808
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getThemeName()I

    move-result v1

    if-nez v1, :cond_1

    .line 1809
    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/Element;->getGroupID()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->removeTextClipartParamsForElement(I)V

    .line 1810
    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/Element;->getGroupID()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->removeDrawingClipartParamsForElement(I)V

    .line 1812
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->overlapTransitionsHandling()V

    .line 1815
    .end local v0    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    :goto_0
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public removeElementInSummary(Lcom/samsung/app/video/editor/external/Element;)Ljava/util/List;
    .locals 6
    .param p1, "element"    # Lcom/samsung/app/video/editor/external/Element;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/app/video/editor/external/Element;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/ClipartParams;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1838
    iget-object v5, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->elementList:Ljava/util/List;

    if-eqz v5, :cond_2

    .line 1839
    const/4 v4, 0x0

    .line 1840
    .local v4, "list":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/Element;->getGroupID()I

    move-result v1

    .line 1841
    .local v1, "id":I
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v0

    .line 1842
    .local v0, "count":I
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_0
    if-lt v2, v0, :cond_0

    .line 1849
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->overlapTransitionsHandling()V

    .line 1853
    .end local v0    # "count":I
    .end local v1    # "id":I
    .end local v2    # "j":I
    .end local v4    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    :goto_1
    return-object v4

    .line 1843
    .restart local v0    # "count":I
    .restart local v1    # "id":I
    .restart local v2    # "j":I
    .restart local v4    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    :cond_0
    iget-object v5, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->elementList:Ljava/util/List;

    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/app/video/editor/external/Element;

    .line 1844
    .local v3, "lElement":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/Element;->getGroupID()I

    move-result v5

    if-ne v1, v5, :cond_1

    .line 1845
    iget-object v5, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->elementList:Ljava/util/List;

    invoke-interface {v5, v3}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1846
    const/4 v2, -0x1

    .line 1842
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1853
    .end local v0    # "count":I
    .end local v1    # "id":I
    .end local v2    # "j":I
    .end local v3    # "lElement":Lcom/samsung/app/video/editor/external/Element;
    .end local v4    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    :cond_2
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public removeMusicEleList(Lcom/samsung/app/video/editor/external/Element;)V
    .locals 2
    .param p1, "element"    # Lcom/samsung/app/video/editor/external/Element;

    .prologue
    .line 1857
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlAudioEleList:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 1858
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlAudioEleList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1861
    :cond_0
    sget-object v0, Lcom/samsung/app/video/editor/external/TranscodeElement;->mElementListComparator:Lcom/samsung/app/video/editor/external/TranscodeElement$ElementListComparator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlAudioEleList:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 1862
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlAudioEleList:Ljava/util/List;

    sget-object v1, Lcom/samsung/app/video/editor/external/TranscodeElement;->mElementListComparator:Lcom/samsung/app/video/editor/external/TranscodeElement$ElementListComparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1864
    :cond_1
    return-void
.end method

.method public removeRecordEleList()V
    .locals 1

    .prologue
    .line 1893
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlNarrationEleList:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 1894
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlNarrationEleList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1895
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlNarrationEleList:Ljava/util/List;

    .line 1897
    :cond_0
    return-void
.end method

.method public removeRecordEleList(Lcom/samsung/app/video/editor/external/Element;)V
    .locals 2
    .param p1, "element"    # Lcom/samsung/app/video/editor/external/Element;

    .prologue
    .line 1867
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlNarrationEleList:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 1868
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlNarrationEleList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1871
    :cond_0
    sget-object v0, Lcom/samsung/app/video/editor/external/TranscodeElement;->mElementListComparator:Lcom/samsung/app/video/editor/external/TranscodeElement$ElementListComparator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlNarrationEleList:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 1872
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlNarrationEleList:Ljava/util/List;

    sget-object v1, Lcom/samsung/app/video/editor/external/TranscodeElement;->mElementListComparator:Lcom/samsung/app/video/editor/external/TranscodeElement$ElementListComparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1874
    :cond_1
    return-void
.end method

.method public removeSoundEleList()V
    .locals 1

    .prologue
    .line 1887
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlSoundEleList:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 1888
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlSoundEleList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1889
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlSoundEleList:Ljava/util/List;

    .line 1891
    :cond_0
    return-void
.end method

.method public removeSoundEleList(Lcom/samsung/app/video/editor/external/Element;)V
    .locals 2
    .param p1, "element"    # Lcom/samsung/app/video/editor/external/Element;

    .prologue
    .line 1877
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlSoundEleList:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 1878
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlSoundEleList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1881
    :cond_0
    sget-object v0, Lcom/samsung/app/video/editor/external/TranscodeElement;->mElementListComparator:Lcom/samsung/app/video/editor/external/TranscodeElement$ElementListComparator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlSoundEleList:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 1882
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlSoundEleList:Ljava/util/List;

    sget-object v1, Lcom/samsung/app/video/editor/external/TranscodeElement;->mElementListComparator:Lcom/samsung/app/video/editor/external/TranscodeElement$ElementListComparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1884
    :cond_1
    return-void
.end method

.method public removeTempAudioEleList(Lcom/samsung/app/video/editor/external/Element;)V
    .locals 1
    .param p1, "element"    # Lcom/samsung/app/video/editor/external/Element;

    .prologue
    .line 1901
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->tempAudioEleList:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 1902
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->tempAudioEleList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1904
    :cond_0
    return-void
.end method

.method public removeTempBGMMusic()V
    .locals 1

    .prologue
    .line 1907
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->tempAudioEleList:Ljava/util/List;

    .line 1908
    return-void
.end method

.method public removeTextEleList(Lcom/samsung/app/video/editor/external/ClipartParams;)V
    .locals 2
    .param p1, "clipParams"    # Lcom/samsung/app/video/editor/external/ClipartParams;

    .prologue
    .line 1911
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->textEleList:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 1912
    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/ClipartParams;->remove()V

    .line 1913
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->textEleList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1916
    :cond_0
    sget-object v0, Lcom/samsung/app/video/editor/external/TranscodeElement;->mTextListComparator:Lcom/samsung/app/video/editor/external/TranscodeElement$TextListComparator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->textEleList:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 1917
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->textEleList:Ljava/util/List;

    sget-object v1, Lcom/samsung/app/video/editor/external/TranscodeElement;->mTextListComparator:Lcom/samsung/app/video/editor/external/TranscodeElement$TextListComparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1919
    :cond_1
    return-void
.end method

.method public removeTextList()V
    .locals 3

    .prologue
    .line 1922
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getTextEleListCount()I

    move-result v2

    add-int/lit8 v0, v2, -0x1

    .line 1923
    .local v0, "count":I
    move v1, v0

    .local v1, "i":I
    :goto_0
    if-gez v1, :cond_0

    .line 1927
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->textEleList:Ljava/util/List;

    .line 1928
    return-void

    .line 1924
    :cond_0
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->textEleList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/app/video/editor/external/ClipartParams;

    invoke-virtual {p0, v2}, Lcom/samsung/app/video/editor/external/TranscodeElement;->removeTextEleList(Lcom/samsung/app/video/editor/external/ClipartParams;)V

    .line 1923
    add-int/lit8 v1, v1, -0x1

    goto :goto_0
.end method

.method public resetElement(Ljava/lang/String;IZLcom/samsung/app/video/editor/external/Element;)Lcom/samsung/app/video/editor/external/Element;
    .locals 10
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "type"    # I
    .param p3, "kenburnRequired"    # Z
    .param p4, "origEle"    # Lcom/samsung/app/video/editor/external/Element;

    .prologue
    .line 2289
    new-instance v2, Lcom/samsung/app/video/editor/external/Element;

    invoke-direct {v2, p4}, Lcom/samsung/app/video/editor/external/Element;-><init>(Lcom/samsung/app/video/editor/external/Element;)V

    .line 2290
    .local v2, "element":Lcom/samsung/app/video/editor/external/Element;
    const/4 v8, 0x0

    invoke-virtual {v2, v8}, Lcom/samsung/app/video/editor/external/Element;->setAssetResource(Z)V

    .line 2291
    invoke-virtual {v2, p1}, Lcom/samsung/app/video/editor/external/Element;->setFilePath(Ljava/lang/String;)V

    .line 2292
    new-instance v6, Lcom/samsung/app/video/editor/external/Edit;

    invoke-direct {v6}, Lcom/samsung/app/video/editor/external/Edit;-><init>()V

    .line 2293
    .local v6, "none_effect":Lcom/samsung/app/video/editor/external/Edit;
    const/4 v8, 0x4

    invoke-virtual {v6, v8}, Lcom/samsung/app/video/editor/external/Edit;->setType(I)V

    .line 2294
    const/16 v8, 0x16

    invoke-virtual {v6, v8}, Lcom/samsung/app/video/editor/external/Edit;->setSubType(I)V

    .line 2295
    invoke-virtual {v2, v6}, Lcom/samsung/app/video/editor/external/Element;->addEdit(Lcom/samsung/app/video/editor/external/Edit;)V

    .line 2296
    const/4 v8, 0x2

    if-ne p2, v8, :cond_1

    .line 2298
    if-eqz p3, :cond_0

    .line 2300
    new-instance v0, Lcom/sec/android/app/ve/common/KenburnsDefault;

    invoke-direct {v0}, Lcom/sec/android/app/ve/common/KenburnsDefault;-><init>()V

    .line 2301
    .local v0, "defKenburns":Lcom/sec/android/app/ve/common/KenburnsDefault;
    invoke-virtual {v0, v2}, Lcom/sec/android/app/ve/common/KenburnsDefault;->applyDefaultKenburns(Lcom/samsung/app/video/editor/external/Element;)Lcom/samsung/app/video/editor/external/Element;

    move-result-object v2

    .line 2304
    new-instance v3, Lcom/samsung/app/video/editor/external/Edit;

    invoke-direct {v3}, Lcom/samsung/app/video/editor/external/Edit;-><init>()V

    .line 2305
    .local v3, "ken_burn":Lcom/samsung/app/video/editor/external/Edit;
    const/4 v8, 0x4

    invoke-virtual {v3, v8}, Lcom/samsung/app/video/editor/external/Edit;->setType(I)V

    .line 2306
    const/16 v8, 0x27

    invoke-virtual {v3, v8}, Lcom/samsung/app/video/editor/external/Edit;->setSubType(I)V

    .line 2307
    invoke-virtual {v2, v3}, Lcom/samsung/app/video/editor/external/Element;->addEdit(Lcom/samsung/app/video/editor/external/Edit;)V

    .line 2309
    .end local v0    # "defKenburns":Lcom/sec/android/app/ve/common/KenburnsDefault;
    .end local v3    # "ken_burn":Lcom/samsung/app/video/editor/external/Edit;
    :cond_0
    const-wide/16 v8, 0x1770

    invoke-virtual {v2, v8, v9}, Lcom/samsung/app/video/editor/external/Element;->setEndTime(J)V

    .line 2310
    const-wide/16 v8, 0x1770

    invoke-virtual {v2, v8, v9}, Lcom/samsung/app/video/editor/external/Element;->setDuration(J)V

    .line 2321
    :goto_0
    invoke-virtual {v2, p2}, Lcom/samsung/app/video/editor/external/Element;->setType(I)V

    .line 2322
    new-instance v1, Lcom/samsung/app/video/editor/external/Edit;

    invoke-direct {v1}, Lcom/samsung/app/video/editor/external/Edit;-><init>()V

    .line 2323
    .local v1, "edit":Lcom/samsung/app/video/editor/external/Edit;
    const/4 v8, 0x1

    invoke-virtual {v1, v8}, Lcom/samsung/app/video/editor/external/Edit;->setType(I)V

    .line 2324
    const/4 v8, 0x0

    invoke-virtual {v1, v8}, Lcom/samsung/app/video/editor/external/Edit;->setVolumeLevel(I)V

    .line 2325
    invoke-virtual {v2, v1}, Lcom/samsung/app/video/editor/external/Element;->addEdit(Lcom/samsung/app/video/editor/external/Edit;)V

    .line 2326
    const/4 v8, 0x0

    invoke-virtual {v2, v8}, Lcom/samsung/app/video/editor/external/Element;->setAutoEdited(Z)V

    move-object v8, v2

    .line 2327
    .end local v1    # "edit":Lcom/samsung/app/video/editor/external/Edit;
    :goto_1
    return-object v8

    .line 2312
    :cond_1
    const/4 v7, 0x0

    .line 2313
    .local v7, "retriever":Landroid/media/MediaMetadataRetriever;
    iget-object v8, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->mAssetManager:Landroid/content/res/AssetManager;

    const/4 v9, 0x0

    invoke-static {v8, p1, v9}, Lcom/sec/android/app/ve/util/CommonUtils;->getRetrieverForSource(Landroid/content/res/AssetManager;Ljava/lang/String;Z)Landroid/media/MediaMetadataRetriever;

    move-result-object v7

    .line 2314
    if-nez v7, :cond_2

    .line 2315
    const/4 v8, 0x0

    goto :goto_1

    .line 2316
    :cond_2
    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-static {v8, v9, v7, p1}, Lcom/sec/android/app/ve/util/CommonUtils;->getVideoDuration(ZLandroid/content/res/AssetManager;Landroid/media/MediaMetadataRetriever;Ljava/lang/String;)I

    move-result v8

    int-to-long v4, v8

    .line 2317
    .local v4, "elementDur":J
    invoke-virtual {v2, v4, v5}, Lcom/samsung/app/video/editor/external/Element;->setEndTime(J)V

    .line 2318
    invoke-virtual {v2, v4, v5}, Lcom/samsung/app/video/editor/external/Element;->setDuration(J)V

    .line 2319
    invoke-static {v7}, Lcom/sec/android/app/ve/util/CommonUtils;->releaseRetriever(Landroid/media/MediaMetadataRetriever;)V

    goto :goto_0
.end method

.method public resetElement(Ljava/lang/String;JJILcom/samsung/app/video/editor/external/Element;)Lcom/samsung/app/video/editor/external/Element;
    .locals 8
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "start"    # J
    .param p4, "end"    # J
    .param p6, "volume"    # I
    .param p7, "origEle"    # Lcom/samsung/app/video/editor/external/Element;

    .prologue
    .line 2261
    new-instance v1, Lcom/samsung/app/video/editor/external/Element;

    invoke-direct {v1, p7}, Lcom/samsung/app/video/editor/external/Element;-><init>(Lcom/samsung/app/video/editor/external/Element;)V

    .line 2262
    .local v1, "element":Lcom/samsung/app/video/editor/external/Element;
    const/4 v6, 0x0

    invoke-virtual {v1, v6}, Lcom/samsung/app/video/editor/external/Element;->setAssetResource(Z)V

    .line 2263
    invoke-virtual {v1, p1}, Lcom/samsung/app/video/editor/external/Element;->setFilePath(Ljava/lang/String;)V

    .line 2264
    const/4 v5, 0x0

    .line 2265
    .local v5, "retriever":Landroid/media/MediaMetadataRetriever;
    iget-object v6, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->mAssetManager:Landroid/content/res/AssetManager;

    const/4 v7, 0x0

    invoke-static {v6, p1, v7}, Lcom/sec/android/app/ve/util/CommonUtils;->getRetrieverForSource(Landroid/content/res/AssetManager;Ljava/lang/String;Z)Landroid/media/MediaMetadataRetriever;

    move-result-object v5

    .line 2266
    if-nez v5, :cond_0

    .line 2267
    const/4 v1, 0x0

    .line 2284
    .end local v1    # "element":Lcom/samsung/app/video/editor/external/Element;
    :goto_0
    return-object v1

    .line 2268
    .restart local v1    # "element":Lcom/samsung/app/video/editor/external/Element;
    :cond_0
    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static {v6, v7, v5, p1}, Lcom/sec/android/app/ve/util/CommonUtils;->getVideoDuration(ZLandroid/content/res/AssetManager;Landroid/media/MediaMetadataRetriever;Ljava/lang/String;)I

    move-result v6

    int-to-long v2, v6

    .line 2269
    .local v2, "elementDur":J
    const/4 v6, 0x1

    invoke-virtual {v1, v6}, Lcom/samsung/app/video/editor/external/Element;->setType(I)V

    .line 2270
    invoke-virtual {v1, p2, p3}, Lcom/samsung/app/video/editor/external/Element;->setStartTime(J)V

    .line 2271
    invoke-virtual {v1, p4, p5}, Lcom/samsung/app/video/editor/external/Element;->setEndTime(J)V

    .line 2272
    invoke-virtual {v1, v2, v3}, Lcom/samsung/app/video/editor/external/Element;->setDuration(J)V

    .line 2273
    invoke-static {v5}, Lcom/sec/android/app/ve/util/CommonUtils;->releaseRetriever(Landroid/media/MediaMetadataRetriever;)V

    .line 2275
    new-instance v0, Lcom/samsung/app/video/editor/external/Edit;

    invoke-direct {v0}, Lcom/samsung/app/video/editor/external/Edit;-><init>()V

    .line 2276
    .local v0, "edit":Lcom/samsung/app/video/editor/external/Edit;
    const/4 v6, 0x1

    invoke-virtual {v0, v6}, Lcom/samsung/app/video/editor/external/Edit;->setType(I)V

    .line 2277
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/samsung/app/video/editor/external/Edit;->setVolumeLevel(I)V

    .line 2278
    invoke-virtual {v1, v0}, Lcom/samsung/app/video/editor/external/Element;->addEdit(Lcom/samsung/app/video/editor/external/Edit;)V

    .line 2279
    const/4 v6, 0x0

    invoke-virtual {v1, v6}, Lcom/samsung/app/video/editor/external/Element;->setAutoEdited(Z)V

    .line 2280
    new-instance v4, Lcom/samsung/app/video/editor/external/Edit;

    invoke-direct {v4}, Lcom/samsung/app/video/editor/external/Edit;-><init>()V

    .line 2281
    .local v4, "none_effect":Lcom/samsung/app/video/editor/external/Edit;
    const/4 v6, 0x4

    invoke-virtual {v4, v6}, Lcom/samsung/app/video/editor/external/Edit;->setType(I)V

    .line 2282
    const/16 v6, 0x16

    invoke-virtual {v4, v6}, Lcom/samsung/app/video/editor/external/Edit;->setSubType(I)V

    .line 2283
    invoke-virtual {v1, v4}, Lcom/samsung/app/video/editor/external/Element;->addEdit(Lcom/samsung/app/video/editor/external/Edit;)V

    goto :goto_0
.end method

.method public setAPreview(Z)V
    .locals 0
    .param p1, "isAPreview"    # Z

    .prologue
    .line 1939
    iput-boolean p1, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->isAPreview:Z

    .line 1940
    return-void
.end method

.method public setAdditionlAudioEleList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/Element;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1931
    .local p1, "additionlAudioEleList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    iput-object p1, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlAudioEleList:Ljava/util/List;

    .line 1932
    return-void
.end method

.method public setAssetManager(Landroid/content/res/AssetManager;)V
    .locals 0
    .param p1, "assetManager"    # Landroid/content/res/AssetManager;

    .prologue
    .line 2575
    iput-object p1, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->mAssetManager:Landroid/content/res/AssetManager;

    .line 2576
    return-void
.end method

.method public setAutoEditedToAllElements(Z)V
    .locals 5
    .param p1, "value"    # Z

    .prologue
    .line 1943
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementList()Ljava/util/List;

    move-result-object v1

    .line 1944
    .local v1, "elementList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v0

    .line 1945
    .local v0, "count":I
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_0
    if-lt v2, v0, :cond_0

    .line 1948
    return-void

    .line 1946
    :cond_0
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/app/video/editor/external/Element;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/samsung/app/video/editor/external/Element;->setAutoEdited(Z)V

    .line 1945
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public setExportedVideoPath(Ljava/lang/String;)V
    .locals 1
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 1951
    if-eqz p1, :cond_0

    .line 1952
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->exportedVideoPath:Ljava/lang/String;

    .line 1953
    :cond_0
    return-void
.end method

.method public setFullMovieDuration(I)V
    .locals 0
    .param p1, "fullMovieDuration"    # I

    .prologue
    .line 1960
    iput p1, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->fullMovieDuration:I

    .line 1961
    return-void
.end method

.method public setMissingFileProject(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 1965
    iput-boolean p1, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->isMissingFileProject:Z

    .line 1966
    return-void
.end method

.method public setNewRecomProject(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 2002
    iput-boolean p1, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->isNewRecomProject:Z

    .line 2003
    return-void
.end method

.method public setProjCreationTime(J)V
    .locals 1
    .param p1, "value"    # J

    .prologue
    .line 1969
    iput-wide p1, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->projectCreationTime:J

    .line 1970
    return-void
.end method

.method public setProjectFileName(Ljava/lang/String;)V
    .locals 1
    .param p1, "projectFileName"    # Ljava/lang/String;

    .prologue
    .line 1977
    if-eqz p1, :cond_0

    .line 1978
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->projectFileName:Ljava/lang/String;

    .line 1979
    :cond_0
    return-void
.end method

.method public setProjectName(Ljava/lang/String;)V
    .locals 1
    .param p1, "projectName"    # Ljava/lang/String;

    .prologue
    .line 2093
    if-eqz p1, :cond_0

    .line 2094
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->projectName:Ljava/lang/String;

    .line 2095
    :cond_0
    return-void
.end method

.method public setResolutionEnumValue(I)V
    .locals 1
    .param p1, "resolutionEnumValue"    # I

    .prologue
    .line 1982
    if-ltz p1, :cond_0

    const/4 v0, 0x7

    if-le p1, v0, :cond_1

    .line 1983
    :cond_0
    const/4 p1, 0x0

    .line 1986
    :cond_1
    iput p1, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->resolutionEnumValue:I

    .line 1987
    return-void
.end method

.method public setSummaryProject(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 1990
    iput-boolean p1, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->isSummaryProject:Z

    .line 1991
    return-void
.end method

.method public setTargetDispHeight(I)V
    .locals 0
    .param p1, "aHeight"    # I

    .prologue
    .line 1994
    iput p1, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->TargetDispHeight:I

    .line 1995
    return-void
.end method

.method public setTargetDispWidth(I)V
    .locals 0
    .param p1, "aWidth"    # I

    .prologue
    .line 1998
    iput p1, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->TargetDispWidth:I

    .line 1999
    return-void
.end method

.method public setTempAudioEleList(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/Element;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2026
    .local p1, "audioList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->tempAudioEleList:Ljava/util/List;

    if-eqz v2, :cond_0

    .line 2027
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->tempAudioEleList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 2028
    :cond_0
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->tempAudioEleList:Ljava/util/List;

    .line 2030
    if-eqz p1, :cond_1

    .line 2031
    new-instance v2, Ljava/util/Vector;

    invoke-direct {v2}, Ljava/util/Vector;-><init>()V

    iput-object v2, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->tempAudioEleList:Ljava/util/List;

    .line 2032
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    .line 2033
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v0, :cond_2

    .line 2038
    .end local v0    # "count":I
    .end local v1    # "i":I
    :cond_1
    return-void

    .line 2034
    .restart local v0    # "count":I
    .restart local v1    # "i":I
    :cond_2
    iget-object v3, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->tempAudioEleList:Ljava/util/List;

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/app/video/editor/external/Element;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2033
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public setTextList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/ClipartParams;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2041
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    iput-object p1, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->textEleList:Ljava/util/List;

    .line 2042
    return-void
.end method

.method public setThemeName(I)V
    .locals 0
    .param p1, "aThemeName"    # I

    .prologue
    .line 2048
    iput p1, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->mThemeName:I

    .line 2049
    return-void
.end method

.method public setThumbnail(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "bmp"    # Landroid/graphics/Bitmap;

    .prologue
    .line 2611
    iput-object p1, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->mThumbnail:Landroid/graphics/Bitmap;

    .line 2612
    return-void
.end method

.method public setUniqueProjectID(Ljava/lang/String;)V
    .locals 0
    .param p1, "projectID"    # Ljava/lang/String;

    .prologue
    .line 2018
    iput-object p1, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->mUniqueProjectID:Ljava/lang/String;

    .line 2019
    return-void
.end method

.method public setfilePathforEngine(Ljava/lang/String;)V
    .locals 0
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 2010
    iput-object p1, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->mfilesPathforEngine:Ljava/lang/String;

    .line 2011
    return-void
.end method

.method public updateAnimationsForCaptions(Lcom/samsung/app/video/editor/external/Element;Lcom/samsung/app/video/editor/external/ClipartParams;)V
    .locals 18
    .param p1, "element"    # Lcom/samsung/app/video/editor/external/Element;
    .param p2, "clipart"    # Lcom/samsung/app/video/editor/external/ClipartParams;

    .prologue
    .line 2129
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getThemeName()I

    move-result v14

    if-nez v14, :cond_0

    move-object/from16 v0, p2

    iget-object v14, v0, Lcom/samsung/app/video/editor/external/ClipartParams;->mAnimationList:Ljava/util/List;

    if-eqz v14, :cond_0

    .line 2130
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getStoryTimeTillElement(Lcom/samsung/app/video/editor/external/Element;)J

    move-result-wide v12

    .line 2131
    .local v12, "startTime":J
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/app/video/editor/external/Element;->getOverLapAtEnd()I

    move-result v14

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/app/video/editor/external/Element;->getOverLapAtStart()I

    move-result v15

    add-int/2addr v14, v15

    int-to-long v8, v14

    .line 2132
    .local v8, "overLapTime":J
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/app/video/editor/external/Element;->getEndTime()J

    move-result-wide v14

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/app/video/editor/external/Element;->getStartTime()J

    move-result-wide v16

    sub-long v14, v14, v16

    long-to-float v14, v14

    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/ve/util/CommonUtils;->getSpeedFactor(Lcom/samsung/app/video/editor/external/Element;)F

    move-result v15

    mul-float/2addr v14, v15

    invoke-static {v14}, Ljava/lang/Math;->round(F)I

    move-result v14

    int-to-long v14, v14

    add-long/2addr v14, v12

    sub-long v4, v14, v8

    .line 2133
    .local v4, "endTime":J
    long-to-float v14, v12

    const/high16 v15, 0x447a0000    # 1000.0f

    div-float/2addr v14, v15

    const/high16 v15, 0x41f00000    # 30.0f

    mul-float/2addr v14, v15

    invoke-static {v14}, Ljava/lang/Math;->round(F)I

    move-result v7

    .line 2134
    .local v7, "firstAnimationStartFrame":I
    const-wide/16 v14, 0x3e8

    add-long/2addr v14, v12

    long-to-float v14, v14

    const/high16 v15, 0x447a0000    # 1000.0f

    div-float/2addr v14, v15

    const/high16 v15, 0x41f00000    # 30.0f

    mul-float/2addr v14, v15

    invoke-static {v14}, Ljava/lang/Math;->round(F)I

    move-result v6

    .line 2135
    .local v6, "firstAnimationEndFrame":I
    const-wide/16 v14, 0x3e8

    sub-long v14, v4, v14

    long-to-float v14, v14

    const/high16 v15, 0x447a0000    # 1000.0f

    div-float/2addr v14, v15

    const/high16 v15, 0x41f00000    # 30.0f

    mul-float/2addr v14, v15

    invoke-static {v14}, Ljava/lang/Math;->round(F)I

    move-result v11

    .line 2136
    .local v11, "secondAnimationStartFrame":I
    long-to-float v14, v4

    const/high16 v15, 0x447a0000    # 1000.0f

    div-float/2addr v14, v15

    const/high16 v15, 0x41f00000    # 30.0f

    mul-float/2addr v14, v15

    invoke-static {v14}, Ljava/lang/Math;->round(F)I

    move-result v10

    .line 2138
    .local v10, "secondAnimationEndFrame":I
    new-instance v3, Lcom/samsung/app/video/editor/external/TextAnimationData;

    invoke-direct {v3}, Lcom/samsung/app/video/editor/external/TextAnimationData;-><init>()V

    .line 2139
    .local v3, "animationDatabegin":Lcom/samsung/app/video/editor/external/TextAnimationData;
    new-instance v2, Lcom/samsung/app/video/editor/external/TextAnimationData;

    invoke-direct {v2}, Lcom/samsung/app/video/editor/external/TextAnimationData;-><init>()V

    .line 2141
    .local v2, "animationDataEnd":Lcom/samsung/app/video/editor/external/TextAnimationData;
    const/4 v14, 0x1

    const/4 v15, 0x0

    .line 2140
    invoke-virtual {v3, v7, v6, v14, v15}, Lcom/samsung/app/video/editor/external/TextAnimationData;->setAnimationType(IIII)V

    .line 2142
    const/4 v14, 0x0

    const/high16 v15, 0x3f800000    # 1.0f

    invoke-virtual {v3, v14, v15}, Lcom/samsung/app/video/editor/external/TextAnimationData;->setAlphaRange(FF)V

    .line 2145
    const/4 v14, 0x1

    const/4 v15, 0x0

    .line 2144
    invoke-virtual {v2, v11, v10, v14, v15}, Lcom/samsung/app/video/editor/external/TextAnimationData;->setAnimationType(IIII)V

    .line 2146
    const/high16 v14, 0x3f800000    # 1.0f

    const/4 v15, 0x0

    invoke-virtual {v2, v14, v15}, Lcom/samsung/app/video/editor/external/TextAnimationData;->setAlphaRange(FF)V

    .line 2147
    move-object/from16 v0, p2

    iget-object v14, v0, Lcom/samsung/app/video/editor/external/ClipartParams;->mAnimationList:Ljava/util/List;

    invoke-interface {v14}, Ljava/util/List;->clear()V

    .line 2148
    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Lcom/samsung/app/video/editor/external/ClipartParams;->addAnimationToList(Lcom/samsung/app/video/editor/external/TextAnimationData;)V

    .line 2149
    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Lcom/samsung/app/video/editor/external/ClipartParams;->addAnimationToList(Lcom/samsung/app/video/editor/external/TextAnimationData;)V

    .line 2152
    .end local v2    # "animationDataEnd":Lcom/samsung/app/video/editor/external/TextAnimationData;
    .end local v3    # "animationDatabegin":Lcom/samsung/app/video/editor/external/TextAnimationData;
    .end local v4    # "endTime":J
    .end local v6    # "firstAnimationEndFrame":I
    .end local v7    # "firstAnimationStartFrame":I
    .end local v8    # "overLapTime":J
    .end local v10    # "secondAnimationEndFrame":I
    .end local v11    # "secondAnimationStartFrame":I
    .end local v12    # "startTime":J
    :cond_0
    return-void
.end method

.method public updateCaptionsTime()V
    .locals 18

    .prologue
    .line 2465
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getThemeName()I

    move-result v14

    if-nez v14, :cond_0

    .line 2467
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v14

    if-lez v14, :cond_0

    .line 2468
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v14

    add-int/lit8 v14, v14, -0x1

    if-lt v6, v14, :cond_1

    .line 2485
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/app/video/editor/external/TranscodeElement;->elementList:Ljava/util/List;

    invoke-interface {v14, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/app/video/editor/external/Element;

    .line 2486
    .local v3, "element":Lcom/samsung/app/video/editor/external/Element;
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getTextClipartParamsListForElement(Lcom/samsung/app/video/editor/external/Element;)Ljava/util/List;

    move-result-object v13

    .line 2487
    .local v13, "textList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    if-eqz v13, :cond_0

    .line 2488
    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v2

    .line 2489
    .local v2, "count":I
    const/4 v7, 0x0

    .local v7, "j":I
    :goto_1
    if-lt v7, v2, :cond_4

    .line 2503
    .end local v2    # "count":I
    .end local v3    # "element":Lcom/samsung/app/video/editor/external/Element;
    .end local v6    # "i":I
    .end local v7    # "j":I
    .end local v13    # "textList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    :cond_0
    return-void

    .line 2469
    .restart local v6    # "i":I
    :cond_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/app/video/editor/external/TranscodeElement;->elementList:Ljava/util/List;

    invoke-interface {v14, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/app/video/editor/external/Element;

    .line 2470
    .restart local v3    # "element":Lcom/samsung/app/video/editor/external/Element;
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getTextClipartParamsListForElement(Lcom/samsung/app/video/editor/external/Element;)Ljava/util/List;

    move-result-object v13

    .line 2471
    .restart local v13    # "textList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    if-eqz v13, :cond_2

    .line 2472
    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v2

    .line 2473
    .restart local v2    # "count":I
    const/4 v7, 0x0

    .restart local v7    # "j":I
    :goto_2
    if-lt v7, v2, :cond_3

    .line 2468
    .end local v2    # "count":I
    .end local v7    # "j":I
    :cond_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 2474
    .restart local v2    # "count":I
    .restart local v7    # "j":I
    :cond_3
    invoke-interface {v13, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/samsung/app/video/editor/external/ClipartParams;

    .line 2475
    .local v12, "text":Lcom/samsung/app/video/editor/external/ClipartParams;
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getStoryTimeTillElement(Lcom/samsung/app/video/editor/external/Element;)J

    move-result-wide v10

    .line 2476
    .local v10, "startTime":J
    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/Element;->getOverLapAtEnd()I

    move-result v14

    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/Element;->getOverLapAtStart()I

    move-result v15

    add-int/2addr v14, v15

    int-to-long v8, v14

    .line 2477
    .local v8, "overLapTime":J
    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/Element;->getEndTime()J

    move-result-wide v14

    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/Element;->getStartTime()J

    move-result-wide v16

    sub-long v14, v14, v16

    long-to-float v14, v14

    .line 2478
    invoke-static {v3}, Lcom/sec/android/app/ve/util/CommonUtils;->getSpeedFactor(Lcom/samsung/app/video/editor/external/Element;)F

    move-result v15

    mul-float/2addr v14, v15

    .line 2477
    invoke-static {v14}, Ljava/lang/Math;->round(F)I

    move-result v14

    int-to-long v14, v14

    add-long/2addr v14, v10

    sub-long v4, v14, v8

    .line 2479
    .local v4, "endTime":J
    long-to-float v14, v10

    const/high16 v15, 0x447a0000    # 1000.0f

    div-float/2addr v14, v15

    const/high16 v15, 0x41f00000    # 30.0f

    mul-float/2addr v14, v15

    invoke-static {v14}, Ljava/lang/Math;->round(F)I

    move-result v14

    invoke-virtual {v12, v14}, Lcom/samsung/app/video/editor/external/ClipartParams;->setStoryboardStartFrame(I)V

    .line 2480
    long-to-float v14, v4

    const/high16 v15, 0x447a0000    # 1000.0f

    div-float/2addr v14, v15

    const/high16 v15, 0x41f00000    # 30.0f

    mul-float/2addr v14, v15

    invoke-static {v14}, Ljava/lang/Math;->round(F)I

    move-result v14

    add-int/lit8 v14, v14, -0x1

    invoke-virtual {v12, v14}, Lcom/samsung/app/video/editor/external/ClipartParams;->setStoryboardEndFrame(I)V

    .line 2481
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v12}, Lcom/samsung/app/video/editor/external/TranscodeElement;->updateAnimationsForCaptions(Lcom/samsung/app/video/editor/external/Element;Lcom/samsung/app/video/editor/external/ClipartParams;)V

    .line 2473
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 2490
    .end local v4    # "endTime":J
    .end local v8    # "overLapTime":J
    .end local v10    # "startTime":J
    .end local v12    # "text":Lcom/samsung/app/video/editor/external/ClipartParams;
    :cond_4
    invoke-interface {v13, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/samsung/app/video/editor/external/ClipartParams;

    .line 2491
    .restart local v12    # "text":Lcom/samsung/app/video/editor/external/ClipartParams;
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getStoryTimeTillElement(Lcom/samsung/app/video/editor/external/Element;)J

    move-result-wide v10

    .line 2492
    .restart local v10    # "startTime":J
    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/Element;->getOverLapAtEnd()I

    move-result v14

    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/Element;->getOverLapAtStart()I

    move-result v15

    add-int/2addr v14, v15

    int-to-long v8, v14

    .line 2493
    .restart local v8    # "overLapTime":J
    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/Element;->getEndTime()J

    move-result-wide v14

    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/Element;->getStartTime()J

    move-result-wide v16

    sub-long v14, v14, v16

    long-to-float v14, v14

    .line 2494
    invoke-static {v3}, Lcom/sec/android/app/ve/util/CommonUtils;->getSpeedFactor(Lcom/samsung/app/video/editor/external/Element;)F

    move-result v15

    mul-float/2addr v14, v15

    .line 2493
    invoke-static {v14}, Ljava/lang/Math;->round(F)I

    move-result v14

    int-to-long v14, v14

    add-long/2addr v14, v10

    sub-long v4, v14, v8

    .line 2496
    .restart local v4    # "endTime":J
    long-to-float v14, v10

    const/high16 v15, 0x447a0000    # 1000.0f

    div-float/2addr v14, v15

    const/high16 v15, 0x41f00000    # 30.0f

    mul-float/2addr v14, v15

    invoke-static {v14}, Ljava/lang/Math;->round(F)I

    move-result v14

    invoke-virtual {v12, v14}, Lcom/samsung/app/video/editor/external/ClipartParams;->setStoryboardStartFrame(I)V

    .line 2497
    long-to-float v14, v4

    const/high16 v15, 0x447a0000    # 1000.0f

    div-float/2addr v14, v15

    const/high16 v15, 0x41f00000    # 30.0f

    mul-float/2addr v14, v15

    invoke-static {v14}, Ljava/lang/Math;->round(F)I

    move-result v14

    invoke-virtual {v12, v14}, Lcom/samsung/app/video/editor/external/ClipartParams;->setStoryboardEndFrame(I)V

    .line 2498
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v12}, Lcom/samsung/app/video/editor/external/TranscodeElement;->updateAnimationsForCaptions(Lcom/samsung/app/video/editor/external/Element;Lcom/samsung/app/video/editor/external/ClipartParams;)V

    .line 2489
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_1
.end method

.method public updateCaptionsTimeinSummary()V
    .locals 14

    .prologue
    .line 2438
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getThemeName()I

    move-result v12

    if-nez v12, :cond_0

    .line 2440
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v12

    if-lez v12, :cond_0

    .line 2441
    const/4 v5, -0x1

    .line 2442
    .local v5, "groupID":I
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v0

    .line 2443
    .local v0, "count":I
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    if-lt v6, v0, :cond_1

    .line 2462
    .end local v0    # "count":I
    .end local v5    # "groupID":I
    .end local v6    # "i":I
    :cond_0
    return-void

    .line 2444
    .restart local v0    # "count":I
    .restart local v5    # "groupID":I
    .restart local v6    # "i":I
    :cond_1
    iget-object v12, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->elementList:Ljava/util/List;

    invoke-interface {v12, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/app/video/editor/external/Element;

    .line 2445
    .local v1, "element":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/Element;->getGroupID()I

    move-result v12

    if-eq v5, v12, :cond_2

    .line 2446
    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/Element;->getGroupID()I

    move-result v5

    .line 2447
    invoke-virtual {p0, v5}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getFirstElementInGroup(I)Lcom/samsung/app/video/editor/external/Element;

    move-result-object v4

    .line 2448
    .local v4, "groupFirstEle":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {p0, v4}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getTextClipartParamsListForElement(Lcom/samsung/app/video/editor/external/Element;)Ljava/util/List;

    move-result-object v11

    .line 2449
    .local v11, "textList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    if-eqz v11, :cond_2

    .line 2450
    const/4 v7, 0x0

    .local v7, "j":I
    :goto_1
    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v12

    if-lt v7, v12, :cond_3

    .line 2443
    .end local v4    # "groupFirstEle":Lcom/samsung/app/video/editor/external/Element;
    .end local v7    # "j":I
    .end local v11    # "textList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    :cond_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 2451
    .restart local v4    # "groupFirstEle":Lcom/samsung/app/video/editor/external/Element;
    .restart local v7    # "j":I
    .restart local v11    # "textList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    :cond_3
    invoke-interface {v11, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/samsung/app/video/editor/external/ClipartParams;

    .line 2452
    .local v10, "text":Lcom/samsung/app/video/editor/external/ClipartParams;
    invoke-virtual {p0, v4}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getStoryTimeTillElement(Lcom/samsung/app/video/editor/external/Element;)J

    move-result-wide v8

    .line 2453
    .local v8, "startTime":J
    invoke-virtual {v4}, Lcom/samsung/app/video/editor/external/Element;->getGroupDuration()J

    move-result-wide v12

    add-long v2, v8, v12

    .line 2454
    .local v2, "endTime":J
    long-to-float v12, v8

    const/high16 v13, 0x447a0000    # 1000.0f

    div-float/2addr v12, v13

    const/high16 v13, 0x41f00000    # 30.0f

    mul-float/2addr v12, v13

    invoke-static {v12}, Ljava/lang/Math;->round(F)I

    move-result v12

    invoke-virtual {v10, v12}, Lcom/samsung/app/video/editor/external/ClipartParams;->setStoryboardStartFrame(I)V

    .line 2455
    long-to-float v12, v2

    const/high16 v13, 0x447a0000    # 1000.0f

    div-float/2addr v12, v13

    const/high16 v13, 0x41f00000    # 30.0f

    mul-float/2addr v12, v13

    invoke-static {v12}, Ljava/lang/Math;->round(F)I

    move-result v12

    add-int/lit8 v12, v12, -0x1

    invoke-virtual {v10, v12}, Lcom/samsung/app/video/editor/external/ClipartParams;->setStoryboardEndFrame(I)V

    .line 2450
    add-int/lit8 v7, v7, 0x1

    goto :goto_1
.end method

.method public updateDrawingTime()V
    .locals 20

    .prologue
    .line 2534
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getThemeName()I

    move-result v11

    if-nez v11, :cond_0

    .line 2536
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v2

    .line 2537
    .local v2, "count":I
    if-lez v2, :cond_0

    .line 2538
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    add-int/lit8 v11, v2, -0x1

    if-lt v8, v11, :cond_1

    .line 2554
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/app/video/editor/external/TranscodeElement;->elementList:Ljava/util/List;

    invoke-interface {v11, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/app/video/editor/external/Element;

    .line 2555
    .local v5, "element":Lcom/samsung/app/video/editor/external/Element;
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getDrawClipartParamsListForElement(Lcom/samsung/app/video/editor/external/Element;)Ljava/util/List;

    move-result-object v4

    .line 2556
    .local v4, "drawList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    if-eqz v4, :cond_0

    .line 2557
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v10

    .line 2558
    .local v10, "listSize":I
    const/4 v9, 0x0

    .local v9, "j":I
    :goto_1
    if-lt v9, v10, :cond_4

    .line 2572
    .end local v2    # "count":I
    .end local v4    # "drawList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    .end local v5    # "element":Lcom/samsung/app/video/editor/external/Element;
    .end local v8    # "i":I
    .end local v9    # "j":I
    .end local v10    # "listSize":I
    :cond_0
    return-void

    .line 2539
    .restart local v2    # "count":I
    .restart local v8    # "i":I
    :cond_1
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/app/video/editor/external/TranscodeElement;->elementList:Ljava/util/List;

    invoke-interface {v11, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/app/video/editor/external/Element;

    .line 2540
    .restart local v5    # "element":Lcom/samsung/app/video/editor/external/Element;
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getDrawClipartParamsListForElement(Lcom/samsung/app/video/editor/external/Element;)Ljava/util/List;

    move-result-object v4

    .line 2541
    .restart local v4    # "drawList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    if-eqz v4, :cond_2

    .line 2542
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v10

    .line 2543
    .restart local v10    # "listSize":I
    const/4 v9, 0x0

    .restart local v9    # "j":I
    :goto_2
    if-lt v9, v10, :cond_3

    .line 2538
    .end local v9    # "j":I
    .end local v10    # "listSize":I
    :cond_2
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 2544
    .restart local v9    # "j":I
    .restart local v10    # "listSize":I
    :cond_3
    invoke-interface {v4, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/app/video/editor/external/ClipartParams;

    .line 2545
    .local v3, "draw":Lcom/samsung/app/video/editor/external/ClipartParams;
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getStoryTimeTillElement(Lcom/samsung/app/video/editor/external/Element;)J

    move-result-wide v14

    .line 2546
    .local v14, "startTime":J
    invoke-virtual {v5}, Lcom/samsung/app/video/editor/external/Element;->getOverLapAtEnd()I

    move-result v11

    invoke-virtual {v5}, Lcom/samsung/app/video/editor/external/Element;->getOverLapAtStart()I

    move-result v16

    add-int v11, v11, v16

    int-to-long v12, v11

    .line 2547
    .local v12, "overLapTime":J
    invoke-virtual {v5}, Lcom/samsung/app/video/editor/external/Element;->getEndTime()J

    move-result-wide v16

    invoke-virtual {v5}, Lcom/samsung/app/video/editor/external/Element;->getStartTime()J

    move-result-wide v18

    sub-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-float v11, v0

    .line 2548
    invoke-static {v5}, Lcom/sec/android/app/ve/util/CommonUtils;->getSpeedFactor(Lcom/samsung/app/video/editor/external/Element;)F

    move-result v16

    mul-float v11, v11, v16

    .line 2547
    invoke-static {v11}, Ljava/lang/Math;->round(F)I

    move-result v11

    int-to-long v0, v11

    move-wide/from16 v16, v0

    add-long v16, v16, v14

    sub-long v6, v16, v12

    .line 2549
    .local v6, "endTime":J
    long-to-float v11, v14

    const/high16 v16, 0x447a0000    # 1000.0f

    div-float v11, v11, v16

    const/high16 v16, 0x41f00000    # 30.0f

    mul-float v11, v11, v16

    invoke-static {v11}, Ljava/lang/Math;->round(F)I

    move-result v11

    invoke-virtual {v3, v11}, Lcom/samsung/app/video/editor/external/ClipartParams;->setStoryboardStartFrame(I)V

    .line 2550
    long-to-float v11, v6

    const/high16 v16, 0x447a0000    # 1000.0f

    div-float v11, v11, v16

    const/high16 v16, 0x41f00000    # 30.0f

    mul-float v11, v11, v16

    invoke-static {v11}, Ljava/lang/Math;->round(F)I

    move-result v11

    add-int/lit8 v11, v11, -0x1

    invoke-virtual {v3, v11}, Lcom/samsung/app/video/editor/external/ClipartParams;->setStoryboardEndFrame(I)V

    .line 2543
    add-int/lit8 v9, v9, 0x1

    goto :goto_2

    .line 2559
    .end local v3    # "draw":Lcom/samsung/app/video/editor/external/ClipartParams;
    .end local v6    # "endTime":J
    .end local v12    # "overLapTime":J
    .end local v14    # "startTime":J
    :cond_4
    invoke-interface {v4, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/app/video/editor/external/ClipartParams;

    .line 2560
    .restart local v3    # "draw":Lcom/samsung/app/video/editor/external/ClipartParams;
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getStoryTimeTillElement(Lcom/samsung/app/video/editor/external/Element;)J

    move-result-wide v14

    .line 2561
    .restart local v14    # "startTime":J
    invoke-virtual {v5}, Lcom/samsung/app/video/editor/external/Element;->getOverLapAtEnd()I

    move-result v11

    invoke-virtual {v5}, Lcom/samsung/app/video/editor/external/Element;->getOverLapAtStart()I

    move-result v16

    add-int v11, v11, v16

    int-to-long v12, v11

    .line 2562
    .restart local v12    # "overLapTime":J
    invoke-virtual {v5}, Lcom/samsung/app/video/editor/external/Element;->getEndTime()J

    move-result-wide v16

    invoke-virtual {v5}, Lcom/samsung/app/video/editor/external/Element;->getStartTime()J

    move-result-wide v18

    sub-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-float v11, v0

    .line 2563
    invoke-static {v5}, Lcom/sec/android/app/ve/util/CommonUtils;->getSpeedFactor(Lcom/samsung/app/video/editor/external/Element;)F

    move-result v16

    mul-float v11, v11, v16

    .line 2562
    invoke-static {v11}, Ljava/lang/Math;->round(F)I

    move-result v11

    int-to-long v0, v11

    move-wide/from16 v16, v0

    add-long v16, v16, v14

    sub-long v6, v16, v12

    .line 2565
    .restart local v6    # "endTime":J
    long-to-float v11, v14

    const/high16 v16, 0x447a0000    # 1000.0f

    div-float v11, v11, v16

    const/high16 v16, 0x41f00000    # 30.0f

    mul-float v11, v11, v16

    invoke-static {v11}, Ljava/lang/Math;->round(F)I

    move-result v11

    invoke-virtual {v3, v11}, Lcom/samsung/app/video/editor/external/ClipartParams;->setStoryboardStartFrame(I)V

    .line 2566
    long-to-float v11, v6

    const/high16 v16, 0x447a0000    # 1000.0f

    div-float v11, v11, v16

    const/high16 v16, 0x41f00000    # 30.0f

    mul-float v11, v11, v16

    invoke-static {v11}, Ljava/lang/Math;->round(F)I

    move-result v11

    invoke-virtual {v3, v11}, Lcom/samsung/app/video/editor/external/ClipartParams;->setStoryboardEndFrame(I)V

    .line 2558
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_1
.end method

.method public updateDrawingTimeinSummary()V
    .locals 18

    .prologue
    .line 2506
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getThemeName()I

    move-result v13

    if-nez v13, :cond_0

    .line 2508
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v13

    if-lez v13, :cond_0

    .line 2509
    const/4 v9, -0x1

    .line 2510
    .local v9, "groupID":I
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v2

    .line 2511
    .local v2, "count":I
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_0
    if-lt v10, v2, :cond_1

    .line 2531
    .end local v2    # "count":I
    .end local v9    # "groupID":I
    .end local v10    # "i":I
    :cond_0
    return-void

    .line 2512
    .restart local v2    # "count":I
    .restart local v9    # "groupID":I
    .restart local v10    # "i":I
    :cond_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/app/video/editor/external/TranscodeElement;->elementList:Ljava/util/List;

    invoke-interface {v13, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/app/video/editor/external/Element;

    .line 2513
    .local v5, "element":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {v5}, Lcom/samsung/app/video/editor/external/Element;->getGroupID()I

    move-result v13

    if-eq v9, v13, :cond_2

    .line 2514
    invoke-virtual {v5}, Lcom/samsung/app/video/editor/external/Element;->getGroupID()I

    move-result v9

    .line 2515
    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getFirstElementInGroup(I)Lcom/samsung/app/video/editor/external/Element;

    move-result-object v8

    .line 2516
    .local v8, "groupFirstEle":Lcom/samsung/app/video/editor/external/Element;
    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getDrawClipartParamsListForElement(Lcom/samsung/app/video/editor/external/Element;)Ljava/util/List;

    move-result-object v4

    .line 2517
    .local v4, "drawList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    if-eqz v4, :cond_2

    .line 2518
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v12

    .line 2519
    .local v12, "listSize":I
    const/4 v11, 0x0

    .local v11, "j":I
    :goto_1
    if-lt v11, v12, :cond_3

    .line 2511
    .end local v4    # "drawList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    .end local v8    # "groupFirstEle":Lcom/samsung/app/video/editor/external/Element;
    .end local v11    # "j":I
    .end local v12    # "listSize":I
    :cond_2
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 2520
    .restart local v4    # "drawList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/ClipartParams;>;"
    .restart local v8    # "groupFirstEle":Lcom/samsung/app/video/editor/external/Element;
    .restart local v11    # "j":I
    .restart local v12    # "listSize":I
    :cond_3
    invoke-interface {v4, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/app/video/editor/external/ClipartParams;

    .line 2521
    .local v3, "draw":Lcom/samsung/app/video/editor/external/ClipartParams;
    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getStoryTimeTillElement(Lcom/samsung/app/video/editor/external/Element;)J

    move-result-wide v14

    .line 2522
    .local v14, "startTime":J
    invoke-virtual {v8}, Lcom/samsung/app/video/editor/external/Element;->getGroupDuration()J

    move-result-wide v16

    add-long v6, v14, v16

    .line 2523
    .local v6, "endTime":J
    long-to-float v13, v14

    const/high16 v16, 0x447a0000    # 1000.0f

    div-float v13, v13, v16

    const/high16 v16, 0x41f00000    # 30.0f

    mul-float v13, v13, v16

    invoke-static {v13}, Ljava/lang/Math;->round(F)I

    move-result v13

    invoke-virtual {v3, v13}, Lcom/samsung/app/video/editor/external/ClipartParams;->setStoryboardStartFrame(I)V

    .line 2524
    long-to-float v13, v6

    const/high16 v16, 0x447a0000    # 1000.0f

    div-float v13, v13, v16

    const/high16 v16, 0x41f00000    # 30.0f

    mul-float v13, v13, v16

    invoke-static {v13}, Ljava/lang/Math;->round(F)I

    move-result v13

    add-int/lit8 v13, v13, -0x1

    invoke-virtual {v3, v13}, Lcom/samsung/app/video/editor/external/ClipartParams;->setStoryboardEndFrame(I)V

    .line 2519
    add-int/lit8 v11, v11, 0x1

    goto :goto_1
.end method

.method public updateMusicEleList()V
    .locals 2

    .prologue
    .line 2052
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlAudioEleList:Ljava/util/List;

    sget-object v1, Lcom/samsung/app/video/editor/external/TranscodeElement;->mElementListComparator:Lcom/samsung/app/video/editor/external/TranscodeElement$ElementListComparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 2053
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlAudioEleList:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->adjustMusicList(Ljava/util/List;)V

    .line 2054
    return-void
.end method

.method public updateRecordEleList()V
    .locals 2

    .prologue
    .line 2058
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlNarrationEleList:Ljava/util/List;

    sget-object v1, Lcom/samsung/app/video/editor/external/TranscodeElement;->mElementListComparator:Lcom/samsung/app/video/editor/external/TranscodeElement$ElementListComparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 2059
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlNarrationEleList:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->adjustMusicList(Ljava/util/List;)V

    .line 2060
    return-void
.end method

.method public updateSoundEleList()V
    .locals 2

    .prologue
    .line 2063
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlSoundEleList:Ljava/util/List;

    sget-object v1, Lcom/samsung/app/video/editor/external/TranscodeElement;->mElementListComparator:Lcom/samsung/app/video/editor/external/TranscodeElement$ElementListComparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 2064
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->additionlAudioEleList:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->adjustMusicList(Ljava/util/List;)V

    .line 2065
    return-void
.end method

.method public updateTempMusicEleList()V
    .locals 2

    .prologue
    .line 2068
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->tempAudioEleList:Ljava/util/List;

    sget-object v1, Lcom/samsung/app/video/editor/external/TranscodeElement;->mElementListComparator:Lcom/samsung/app/video/editor/external/TranscodeElement$ElementListComparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 2069
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->tempAudioEleList:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->adjustMusicList(Ljava/util/List;)V

    .line 2070
    return-void
.end method

.method public updateTextElementAfterSplit(Lcom/samsung/app/video/editor/external/ClipartParams;I)V
    .locals 8
    .param p1, "currentClipart"    # Lcom/samsung/app/video/editor/external/ClipartParams;
    .param p2, "diff"    # I

    .prologue
    .line 2075
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->textEleList:Ljava/util/List;

    if-eqz v2, :cond_0

    if-eqz p1, :cond_0

    .line 2076
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getTextEleListCount()I

    move-result v0

    .line 2077
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v0, :cond_1

    .line 2090
    .end local v0    # "count":I
    .end local v1    # "i":I
    :cond_0
    return-void

    .line 2079
    .restart local v0    # "count":I
    .restart local v1    # "i":I
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/ClipartParams;->getStoryBoardStartTime()J

    move-result-wide v4

    iget-object v2, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->textEleList:Ljava/util/List;

    .line 2080
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/app/video/editor/external/ClipartParams;

    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/ClipartParams;->getStoryBoardStartTime()J

    move-result-wide v2

    cmp-long v2, v4, v2

    if-gez v2, :cond_2

    .line 2081
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->textEleList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/app/video/editor/external/ClipartParams;

    .line 2082
    iget-object v3, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->textEleList:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/app/video/editor/external/ClipartParams;

    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/ClipartParams;->getStoryBoardStartTime()J

    move-result-wide v4

    int-to-long v6, p2

    add-long/2addr v4, v6

    .line 2081
    invoke-virtual {v2, v4, v5}, Lcom/samsung/app/video/editor/external/ClipartParams;->setStoryBoardStartTime(J)V

    .line 2083
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->textEleList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/app/video/editor/external/ClipartParams;

    .line 2084
    iget-object v3, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->textEleList:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/app/video/editor/external/ClipartParams;

    invoke-virtual {v3}, Lcom/samsung/app/video/editor/external/ClipartParams;->getStoryBoardEndTime()J

    move-result-wide v4

    int-to-long v6, p2

    add-long/2addr v4, v6

    .line 2083
    invoke-virtual {v2, v4, v5}, Lcom/samsung/app/video/editor/external/ClipartParams;->setStoryBoardEndTime(J)V

    .line 2077
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public updateTotalDurationForGroup()V
    .locals 14

    .prologue
    .line 2962
    const/4 v1, 0x0

    .line 2963
    .local v1, "element":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getMaxGroupID()I

    move-result v0

    .line 2964
    .local v0, "count":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-le v4, v0, :cond_0

    .line 2983
    return-void

    .line 2965
    :cond_0
    new-instance v2, Ljava/util/Vector;

    invoke-direct {v2}, Ljava/util/Vector;-><init>()V

    .line 2966
    .local v2, "groupList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/app/video/editor/external/Element;>;"
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v6

    .line 2967
    .local v6, "listCount":I
    const/4 v5, 0x0

    .local v5, "j":I
    :goto_1
    if-lt v5, v6, :cond_1

    .line 2973
    invoke-virtual {p0, v2}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getSummaryGroupTotalTime(Ljava/util/List;)J

    move-result-wide v8

    .line 2974
    .local v8, "totalDuration":J
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    .line 2975
    .local v3, "groupSize":I
    const/4 v5, 0x0

    :goto_2
    if-lt v5, v3, :cond_3

    .line 2964
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 2968
    .end local v3    # "groupSize":I
    .end local v8    # "totalDuration":J
    :cond_1
    iget-object v10, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->elementList:Ljava/util/List;

    invoke-interface {v10, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "element":Lcom/samsung/app/video/editor/external/Element;
    check-cast v1, Lcom/samsung/app/video/editor/external/Element;

    .line 2969
    .restart local v1    # "element":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/Element;->getGroupID()I

    move-result v10

    if-ne v4, v10, :cond_2

    .line 2970
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2967
    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 2976
    .restart local v3    # "groupSize":I
    .restart local v8    # "totalDuration":J
    :cond_3
    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "element":Lcom/samsung/app/video/editor/external/Element;
    check-cast v1, Lcom/samsung/app/video/editor/external/Element;

    .line 2977
    .restart local v1    # "element":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {v1, v8, v9}, Lcom/samsung/app/video/editor/external/Element;->setGroupDuration(J)V

    .line 2978
    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/Element;->getEndTime()J

    move-result-wide v10

    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/Element;->getStartTime()J

    move-result-wide v12

    sub-long/2addr v10, v12

    long-to-float v10, v10

    invoke-virtual {v1}, Lcom/samsung/app/video/editor/external/Element;->getGroupDuration()J

    move-result-wide v12

    long-to-float v11, v12

    div-float v7, v10, v11

    .line 2979
    .local v7, "ratio":F
    invoke-virtual {v1, v7}, Lcom/samsung/app/video/editor/external/Element;->setElementRatioInGroup(F)V

    .line 2975
    add-int/lit8 v5, v5, 0x1

    goto :goto_2
.end method

.method public verifyOverlapAreaDuration()V
    .locals 13

    .prologue
    const/4 v12, 0x0

    .line 344
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v7

    const/4 v8, 0x2

    if-ge v7, v8, :cond_1

    .line 374
    :cond_0
    return-void

    .line 350
    :cond_1
    const/4 v3, 0x0

    .line 351
    .local v3, "i":I
    iget-object v7, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->elementList:Ljava/util/List;

    invoke-interface {v7, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/app/video/editor/external/Element;

    .line 352
    .local v2, "current":Lcom/samsung/app/video/editor/external/Element;
    iget-object v7, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->elementList:Ljava/util/List;

    const/4 v8, 0x1

    invoke-interface {v7, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/app/video/editor/external/Element;

    .line 353
    .local v5, "nextElement":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/Element;->getOverLapAtEnd()I

    move-result v7

    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/Element;->getOverLapAtStart()I

    move-result v8

    add-int v6, v7, v8

    .line 354
    .local v6, "overLapTime":I
    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/Element;->getEndTime()J

    move-result-wide v8

    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/Element;->getStartTime()J

    move-result-wide v10

    sub-long/2addr v8, v10

    long-to-float v7, v8

    invoke-static {v2}, Lcom/sec/android/app/ve/util/CommonUtils;->getSpeedFactor(Lcom/samsung/app/video/editor/external/Element;)F

    move-result v8

    mul-float/2addr v7, v8

    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v7

    int-to-float v1, v7

    .line 355
    .local v1, "currDuartion":F
    invoke-virtual {v5}, Lcom/samsung/app/video/editor/external/Element;->getEndTime()J

    move-result-wide v8

    invoke-virtual {v5}, Lcom/samsung/app/video/editor/external/Element;->getStartTime()J

    move-result-wide v10

    sub-long/2addr v8, v10

    long-to-float v7, v8

    invoke-static {v5}, Lcom/sec/android/app/ve/util/CommonUtils;->getSpeedFactor(Lcom/samsung/app/video/editor/external/Element;)F

    move-result v8

    mul-float/2addr v7, v8

    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v7

    int-to-float v4, v7

    .line 356
    .local v4, "nextDuration":F
    int-to-float v7, v6

    cmpg-float v7, v1, v7

    if-ltz v7, :cond_2

    int-to-float v7, v6

    cmpg-float v7, v4, v7

    if-gez v7, :cond_3

    .line 357
    :cond_2
    invoke-virtual {v2, v12}, Lcom/samsung/app/video/editor/external/Element;->setOverLapAtEnd(I)V

    .line 358
    invoke-virtual {v5, v12}, Lcom/samsung/app/video/editor/external/Element;->setOverLapAtStart(I)V

    .line 359
    iput-boolean v12, v2, Lcom/samsung/app/video/editor/external/Element;->isOverLapPossible:Z

    .line 361
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/TranscodeElement;->getElementCount()I

    move-result v7

    add-int/lit8 v0, v7, -0x1

    .line 362
    .local v0, "count":I
    const/4 v3, 0x1

    :goto_0
    if-ge v3, v0, :cond_0

    .line 363
    iget-object v7, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->elementList:Ljava/util/List;

    invoke-interface {v7, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "current":Lcom/samsung/app/video/editor/external/Element;
    check-cast v2, Lcom/samsung/app/video/editor/external/Element;

    .line 364
    .restart local v2    # "current":Lcom/samsung/app/video/editor/external/Element;
    iget-object v7, p0, Lcom/samsung/app/video/editor/external/TranscodeElement;->elementList:Ljava/util/List;

    add-int/lit8 v8, v3, 0x1

    invoke-interface {v7, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    .end local v5    # "nextElement":Lcom/samsung/app/video/editor/external/Element;
    check-cast v5, Lcom/samsung/app/video/editor/external/Element;

    .line 365
    .restart local v5    # "nextElement":Lcom/samsung/app/video/editor/external/Element;
    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/Element;->getOverLapAtEnd()I

    move-result v7

    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/Element;->getOverLapAtStart()I

    move-result v8

    add-int v6, v7, v8

    .line 366
    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/Element;->getEndTime()J

    move-result-wide v8

    invoke-virtual {v2}, Lcom/samsung/app/video/editor/external/Element;->getStartTime()J

    move-result-wide v10

    sub-long/2addr v8, v10

    long-to-float v7, v8

    invoke-static {v2}, Lcom/sec/android/app/ve/util/CommonUtils;->getSpeedFactor(Lcom/samsung/app/video/editor/external/Element;)F

    move-result v8

    mul-float/2addr v7, v8

    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v7

    int-to-float v1, v7

    .line 367
    invoke-virtual {v5}, Lcom/samsung/app/video/editor/external/Element;->getEndTime()J

    move-result-wide v8

    invoke-virtual {v5}, Lcom/samsung/app/video/editor/external/Element;->getStartTime()J

    move-result-wide v10

    sub-long/2addr v8, v10

    long-to-float v7, v8

    invoke-static {v5}, Lcom/sec/android/app/ve/util/CommonUtils;->getSpeedFactor(Lcom/samsung/app/video/editor/external/Element;)F

    move-result v8

    mul-float/2addr v7, v8

    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v7

    int-to-float v4, v7

    .line 368
    int-to-float v7, v6

    cmpg-float v7, v1, v7

    if-ltz v7, :cond_4

    invoke-virtual {v5}, Lcom/samsung/app/video/editor/external/Element;->getOverLapAtStart()I

    move-result v7

    int-to-float v7, v7

    cmpg-float v7, v4, v7

    if-gez v7, :cond_5

    .line 369
    :cond_4
    invoke-virtual {v2, v12}, Lcom/samsung/app/video/editor/external/Element;->setOverLapAtEnd(I)V

    .line 370
    invoke-virtual {v5, v12}, Lcom/samsung/app/video/editor/external/Element;->setOverLapAtStart(I)V

    .line 371
    iput-boolean v12, v2, Lcom/samsung/app/video/editor/external/Element;->isOverLapPossible:Z

    .line 362
    :cond_5
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method
