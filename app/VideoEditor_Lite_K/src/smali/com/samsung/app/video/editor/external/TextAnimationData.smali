.class public Lcom/samsung/app/video/editor/external/TextAnimationData;
.super Ljava/lang/Object;
.source "TextAnimationData.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = -0x22963d13f54526daL


# instance fields
.field public mAnimationType:I

.field public mEndAlpha:F

.field public mEndFrame:I

.field public mEndRotate:F

.field public mEndScaleX:F

.field public mEndScaleY:F

.field public mEndTranslateX:F

.field public mEndTranslateY:F

.field public mInterpolation:I

.field public mPivotX:F

.field public mPivotY:F

.field public mStartAlpha:F

.field public mStartFrame:I

.field public mStartRotate:F

.field public mStartScaleX:F

.field public mStartScaleY:F

.field public mStartTranslateX:F

.field public mStartTranslateY:F


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/app/video/editor/external/TextAnimationData;->mAnimationType:I

    .line 42
    iput v1, p0, Lcom/samsung/app/video/editor/external/TextAnimationData;->mStartAlpha:F

    .line 43
    iput v1, p0, Lcom/samsung/app/video/editor/external/TextAnimationData;->mEndAlpha:F

    .line 44
    iput v1, p0, Lcom/samsung/app/video/editor/external/TextAnimationData;->mStartScaleX:F

    .line 45
    iput v1, p0, Lcom/samsung/app/video/editor/external/TextAnimationData;->mEndScaleX:F

    .line 46
    iput v1, p0, Lcom/samsung/app/video/editor/external/TextAnimationData;->mStartScaleY:F

    .line 47
    iput v1, p0, Lcom/samsung/app/video/editor/external/TextAnimationData;->mEndScaleY:F

    .line 48
    return-void
.end method

.method public constructor <init>(Lcom/samsung/app/video/editor/external/TextAnimationData;)V
    .locals 1
    .param p1, "animationData"    # Lcom/samsung/app/video/editor/external/TextAnimationData;

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iget v0, p1, Lcom/samsung/app/video/editor/external/TextAnimationData;->mStartFrame:I

    iput v0, p0, Lcom/samsung/app/video/editor/external/TextAnimationData;->mStartFrame:I

    .line 52
    iget v0, p1, Lcom/samsung/app/video/editor/external/TextAnimationData;->mEndFrame:I

    iput v0, p0, Lcom/samsung/app/video/editor/external/TextAnimationData;->mEndFrame:I

    .line 53
    iget v0, p1, Lcom/samsung/app/video/editor/external/TextAnimationData;->mAnimationType:I

    iput v0, p0, Lcom/samsung/app/video/editor/external/TextAnimationData;->mAnimationType:I

    .line 54
    iget v0, p1, Lcom/samsung/app/video/editor/external/TextAnimationData;->mInterpolation:I

    iput v0, p0, Lcom/samsung/app/video/editor/external/TextAnimationData;->mInterpolation:I

    .line 55
    iget v0, p1, Lcom/samsung/app/video/editor/external/TextAnimationData;->mStartAlpha:F

    iput v0, p0, Lcom/samsung/app/video/editor/external/TextAnimationData;->mStartAlpha:F

    .line 56
    iget v0, p1, Lcom/samsung/app/video/editor/external/TextAnimationData;->mEndAlpha:F

    iput v0, p0, Lcom/samsung/app/video/editor/external/TextAnimationData;->mEndAlpha:F

    .line 57
    iget v0, p1, Lcom/samsung/app/video/editor/external/TextAnimationData;->mStartTranslateX:F

    iput v0, p0, Lcom/samsung/app/video/editor/external/TextAnimationData;->mStartTranslateX:F

    .line 58
    iget v0, p1, Lcom/samsung/app/video/editor/external/TextAnimationData;->mEndTranslateX:F

    iput v0, p0, Lcom/samsung/app/video/editor/external/TextAnimationData;->mEndTranslateX:F

    .line 59
    iget v0, p1, Lcom/samsung/app/video/editor/external/TextAnimationData;->mStartTranslateY:F

    iput v0, p0, Lcom/samsung/app/video/editor/external/TextAnimationData;->mStartTranslateY:F

    .line 60
    iget v0, p1, Lcom/samsung/app/video/editor/external/TextAnimationData;->mEndTranslateY:F

    iput v0, p0, Lcom/samsung/app/video/editor/external/TextAnimationData;->mEndTranslateY:F

    .line 61
    iget v0, p1, Lcom/samsung/app/video/editor/external/TextAnimationData;->mStartScaleX:F

    iput v0, p0, Lcom/samsung/app/video/editor/external/TextAnimationData;->mStartScaleX:F

    .line 62
    iget v0, p1, Lcom/samsung/app/video/editor/external/TextAnimationData;->mEndScaleX:F

    iput v0, p0, Lcom/samsung/app/video/editor/external/TextAnimationData;->mEndScaleX:F

    .line 63
    iget v0, p1, Lcom/samsung/app/video/editor/external/TextAnimationData;->mStartScaleY:F

    iput v0, p0, Lcom/samsung/app/video/editor/external/TextAnimationData;->mStartScaleY:F

    .line 64
    iget v0, p1, Lcom/samsung/app/video/editor/external/TextAnimationData;->mEndScaleY:F

    iput v0, p0, Lcom/samsung/app/video/editor/external/TextAnimationData;->mEndScaleY:F

    .line 65
    iget v0, p1, Lcom/samsung/app/video/editor/external/TextAnimationData;->mStartRotate:F

    iput v0, p0, Lcom/samsung/app/video/editor/external/TextAnimationData;->mStartRotate:F

    .line 66
    iget v0, p1, Lcom/samsung/app/video/editor/external/TextAnimationData;->mEndRotate:F

    iput v0, p0, Lcom/samsung/app/video/editor/external/TextAnimationData;->mEndRotate:F

    .line 67
    iget v0, p1, Lcom/samsung/app/video/editor/external/TextAnimationData;->mPivotX:F

    iput v0, p0, Lcom/samsung/app/video/editor/external/TextAnimationData;->mPivotX:F

    .line 68
    iget v0, p1, Lcom/samsung/app/video/editor/external/TextAnimationData;->mPivotY:F

    iput v0, p0, Lcom/samsung/app/video/editor/external/TextAnimationData;->mPivotY:F

    .line 69
    return-void
.end method


# virtual methods
.method public setAlphaRange(FF)V
    .locals 1
    .param p1, "startAlpha"    # F
    .param p2, "endAlpha"    # F

    .prologue
    .line 81
    const/4 v0, 0x0

    invoke-static {p1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, Lcom/samsung/app/video/editor/external/TextAnimationData;->mStartAlpha:F

    .line 82
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-static {p2, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iput v0, p0, Lcom/samsung/app/video/editor/external/TextAnimationData;->mEndAlpha:F

    .line 83
    return-void
.end method

.method public setAnimationType(IIII)V
    .locals 1
    .param p1, "startFrame"    # I
    .param p2, "endFrame"    # I
    .param p3, "animationType"    # I
    .param p4, "interpolation"    # I

    .prologue
    .line 72
    if-ltz p3, :cond_0

    const/4 v0, 0x5

    if-lt p3, v0, :cond_1

    .line 73
    :cond_0
    const/4 p3, 0x0

    .line 74
    :cond_1
    iput p1, p0, Lcom/samsung/app/video/editor/external/TextAnimationData;->mStartFrame:I

    .line 75
    iput p2, p0, Lcom/samsung/app/video/editor/external/TextAnimationData;->mEndFrame:I

    .line 76
    iput p3, p0, Lcom/samsung/app/video/editor/external/TextAnimationData;->mAnimationType:I

    .line 77
    iput p4, p0, Lcom/samsung/app/video/editor/external/TextAnimationData;->mInterpolation:I

    .line 78
    return-void
.end method

.method public setRotationRange(FFFF)V
    .locals 0
    .param p1, "startAngle"    # F
    .param p2, "endAngle"    # F
    .param p3, "pivotX"    # F
    .param p4, "pivotY"    # F

    .prologue
    .line 102
    iput p1, p0, Lcom/samsung/app/video/editor/external/TextAnimationData;->mStartRotate:F

    .line 103
    iput p2, p0, Lcom/samsung/app/video/editor/external/TextAnimationData;->mEndRotate:F

    .line 104
    iput p3, p0, Lcom/samsung/app/video/editor/external/TextAnimationData;->mPivotX:F

    .line 105
    iput p4, p0, Lcom/samsung/app/video/editor/external/TextAnimationData;->mPivotY:F

    .line 106
    return-void
.end method

.method public setScaleRange(FFFFFF)V
    .locals 0
    .param p1, "startScaleX"    # F
    .param p2, "endScaleX"    # F
    .param p3, "startScaleY"    # F
    .param p4, "EndScaleY"    # F
    .param p5, "pivotX"    # F
    .param p6, "pivotY"    # F

    .prologue
    .line 93
    iput p1, p0, Lcom/samsung/app/video/editor/external/TextAnimationData;->mStartScaleX:F

    .line 94
    iput p2, p0, Lcom/samsung/app/video/editor/external/TextAnimationData;->mEndScaleX:F

    .line 95
    iput p3, p0, Lcom/samsung/app/video/editor/external/TextAnimationData;->mStartScaleY:F

    .line 96
    iput p4, p0, Lcom/samsung/app/video/editor/external/TextAnimationData;->mEndScaleY:F

    .line 97
    iput p5, p0, Lcom/samsung/app/video/editor/external/TextAnimationData;->mPivotX:F

    .line 98
    iput p6, p0, Lcom/samsung/app/video/editor/external/TextAnimationData;->mPivotY:F

    .line 99
    return-void
.end method

.method public setTranslateRange(FFFF)V
    .locals 0
    .param p1, "startX"    # F
    .param p2, "endX"    # F
    .param p3, "startY"    # F
    .param p4, "endY"    # F

    .prologue
    .line 86
    iput p1, p0, Lcom/samsung/app/video/editor/external/TextAnimationData;->mStartTranslateX:F

    .line 87
    iput p2, p0, Lcom/samsung/app/video/editor/external/TextAnimationData;->mEndTranslateX:F

    .line 88
    iput p3, p0, Lcom/samsung/app/video/editor/external/TextAnimationData;->mStartTranslateY:F

    .line 89
    iput p4, p0, Lcom/samsung/app/video/editor/external/TextAnimationData;->mEndTranslateY:F

    .line 90
    return-void
.end method
