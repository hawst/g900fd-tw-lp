.class public Lcom/samsung/app/video/editor/external/ClipartParams;
.super Ljava/lang/Object;
.source "ClipartParams.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;
    }
.end annotation


# static fields
.field private static RAW_FILES_SD_PATH:Ljava/lang/String; = null

.field private static final RAW_FILE_PREFIX:Ljava/lang/String; = "/draw_"

.field private static final RAW_FILE_SUFFIX:Ljava/lang/String; = ".raw"

.field private static final serialVersionUID:J = -0x2e6f991fc05e975L


# instance fields
.field private Filepath:Ljava/lang/String;

.field public Style:I

.field public bigSize:Z

.field public clipart_height:I

.field public clipart_width:I

.field public clipart_x_pos:I

.field public clipart_y_pos:I

.field public data:Ljava/lang/String;

.field private elementID:I

.field private isThemeDefaultText:Z

.field public itext_end_time:I

.field public itext_start_time:I

.field public mAnimationList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/TextAnimationData;",
            ">;"
        }
    .end annotation
.end field

.field private mAssetResource:Z

.field private mCaptionID:I

.field public mCaptionTextAlignment:I

.field public mDefaultAlpha:F

.field public mDefaultRotate:F

.field public mDefaultScale:F

.field public mDefaultTranslateX:F

.field public mDefaultTranslateY:F

.field public mFontparams:Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;

.field public mIsEditable:Z

.field public mPivotX:F

.field public mPivotY:F

.field private mStoryboardEndFrame:I

.field private mStoryboardStartFrame:I

.field private maxCharLength:I

.field public offsetX:I

.field public offsetY:I

.field public pbBuf:[B

.field private storyboardEndTime:I

.field private storyboardStartTime:I

.field public text_title_id:I

.field public tf:I

.field public themeId:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 33
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".vefullrawdir"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/app/video/editor/external/ClipartParams;->RAW_FILES_SD_PATH:Ljava/lang/String;

    .line 37
    return-void
.end method

.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v0, 0x0

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-boolean v0, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->isThemeDefaultText:Z

    .line 70
    iput v0, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->text_title_id:I

    .line 76
    iput v2, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->elementID:I

    .line 99
    iput-boolean v0, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->mAssetResource:Z

    .line 103
    const/16 v0, 0x3e7

    iput v0, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->maxCharLength:I

    .line 106
    iput v4, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->mCaptionTextAlignment:I

    .line 113
    iput v2, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->mCaptionID:I

    .line 116
    invoke-direct {p0}, Lcom/samsung/app/video/editor/external/ClipartParams;->checkRAWFilesDir()V

    .line 117
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->mAnimationList:Ljava/util/List;

    .line 118
    iput v1, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->mDefaultTranslateX:F

    .line 119
    iput v1, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->mDefaultTranslateY:F

    .line 120
    iput v3, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->mDefaultAlpha:F

    .line 121
    iput v3, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->mDefaultScale:F

    .line 122
    iput v1, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->mDefaultRotate:F

    .line 123
    iput-boolean v4, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->mIsEditable:Z

    .line 124
    iput v2, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->mCaptionID:I

    .line 125
    return-void
.end method

.method public constructor <init>(Lcom/samsung/app/video/editor/external/ClipartParams;)V
    .locals 1
    .param p1, "clipartParams"    # Lcom/samsung/app/video/editor/external/ClipartParams;

    .prologue
    .line 128
    invoke-direct {p0}, Lcom/samsung/app/video/editor/external/ClipartParams;-><init>()V

    .line 129
    const/4 v0, 0x0

    invoke-static {p1, p0, v0}, Lcom/samsung/app/video/editor/external/ClipartParams;->makeCopy(Lcom/samsung/app/video/editor/external/ClipartParams;Lcom/samsung/app/video/editor/external/ClipartParams;Z)V

    .line 130
    return-void
.end method

.method private checkRAWFilesDir()V
    .locals 2

    .prologue
    .line 214
    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/samsung/app/video/editor/external/ClipartParams;->RAW_FILES_SD_PATH:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 215
    .local v0, "lFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 216
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 218
    :cond_0
    return-void
.end method

.method public static getDeepCopyOf(Lcom/samsung/app/video/editor/external/ClipartParams;)Lcom/samsung/app/video/editor/external/ClipartParams;
    .locals 2
    .param p0, "orgParams"    # Lcom/samsung/app/video/editor/external/ClipartParams;

    .prologue
    .line 208
    new-instance v0, Lcom/samsung/app/video/editor/external/ClipartParams;

    invoke-direct {v0}, Lcom/samsung/app/video/editor/external/ClipartParams;-><init>()V

    .line 209
    .local v0, "newParams":Lcom/samsung/app/video/editor/external/ClipartParams;
    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Lcom/samsung/app/video/editor/external/ClipartParams;->makeCopy(Lcom/samsung/app/video/editor/external/ClipartParams;Lcom/samsung/app/video/editor/external/ClipartParams;Z)V

    .line 210
    return-object v0
.end method

.method public static getRawDirPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 136
    sget-object v0, Lcom/samsung/app/video/editor/external/ClipartParams;->RAW_FILES_SD_PATH:Ljava/lang/String;

    return-object v0
.end method

.method private static makeCopy(Lcom/samsung/app/video/editor/external/ClipartParams;Lcom/samsung/app/video/editor/external/ClipartParams;Z)V
    .locals 6
    .param p0, "orgParams"    # Lcom/samsung/app/video/editor/external/ClipartParams;
    .param p1, "newParams"    # Lcom/samsung/app/video/editor/external/ClipartParams;
    .param p2, "deepCopy"    # Z

    .prologue
    const/4 v5, 0x0

    .line 140
    iget v2, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->clipart_height:I

    iput v2, p1, Lcom/samsung/app/video/editor/external/ClipartParams;->clipart_height:I

    .line 141
    iget v2, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->clipart_width:I

    iput v2, p1, Lcom/samsung/app/video/editor/external/ClipartParams;->clipart_width:I

    .line 142
    iget v2, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->clipart_x_pos:I

    iput v2, p1, Lcom/samsung/app/video/editor/external/ClipartParams;->clipart_x_pos:I

    .line 143
    iget v2, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->clipart_y_pos:I

    iput v2, p1, Lcom/samsung/app/video/editor/external/ClipartParams;->clipart_y_pos:I

    .line 144
    iget v2, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->itext_end_time:I

    iput v2, p1, Lcom/samsung/app/video/editor/external/ClipartParams;->itext_end_time:I

    .line 145
    iget v2, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->itext_start_time:I

    iput v2, p1, Lcom/samsung/app/video/editor/external/ClipartParams;->itext_start_time:I

    .line 146
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->data:Ljava/lang/String;

    iput-object v2, p1, Lcom/samsung/app/video/editor/external/ClipartParams;->data:Ljava/lang/String;

    .line 147
    iget v2, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->storyboardStartTime:I

    iput v2, p1, Lcom/samsung/app/video/editor/external/ClipartParams;->storyboardStartTime:I

    .line 148
    iget v2, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->storyboardEndTime:I

    iput v2, p1, Lcom/samsung/app/video/editor/external/ClipartParams;->storyboardEndTime:I

    .line 149
    iget v2, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->tf:I

    iput v2, p1, Lcom/samsung/app/video/editor/external/ClipartParams;->tf:I

    .line 150
    iget v2, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->themeId:I

    iput v2, p1, Lcom/samsung/app/video/editor/external/ClipartParams;->themeId:I

    .line 151
    iget v2, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->Style:I

    iput v2, p1, Lcom/samsung/app/video/editor/external/ClipartParams;->Style:I

    .line 152
    iget v2, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->offsetX:I

    iput v2, p1, Lcom/samsung/app/video/editor/external/ClipartParams;->offsetX:I

    .line 153
    iget v2, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->offsetY:I

    iput v2, p1, Lcom/samsung/app/video/editor/external/ClipartParams;->offsetY:I

    .line 155
    iget-boolean v2, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->bigSize:Z

    iput-boolean v2, p1, Lcom/samsung/app/video/editor/external/ClipartParams;->bigSize:Z

    .line 156
    iget v2, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->text_title_id:I

    iput v2, p1, Lcom/samsung/app/video/editor/external/ClipartParams;->text_title_id:I

    .line 157
    iget v2, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->elementID:I

    iput v2, p1, Lcom/samsung/app/video/editor/external/ClipartParams;->elementID:I

    .line 158
    iget-boolean v2, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->isThemeDefaultText:Z

    iput-boolean v2, p1, Lcom/samsung/app/video/editor/external/ClipartParams;->isThemeDefaultText:Z

    .line 160
    new-instance v2, Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;

    iget-object v3, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->mFontparams:Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;

    invoke-direct {v2, v3}, Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;-><init>(Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;)V

    iput-object v2, p1, Lcom/samsung/app/video/editor/external/ClipartParams;->mFontparams:Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;

    .line 162
    iget v2, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->mDefaultTranslateX:F

    iput v2, p1, Lcom/samsung/app/video/editor/external/ClipartParams;->mDefaultTranslateX:F

    .line 163
    iget v2, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->mDefaultTranslateY:F

    iput v2, p1, Lcom/samsung/app/video/editor/external/ClipartParams;->mDefaultTranslateY:F

    .line 164
    iget v2, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->mDefaultAlpha:F

    iput v2, p1, Lcom/samsung/app/video/editor/external/ClipartParams;->mDefaultAlpha:F

    .line 165
    iget v2, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->mDefaultScale:F

    iput v2, p1, Lcom/samsung/app/video/editor/external/ClipartParams;->mDefaultScale:F

    .line 166
    iget v2, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->mDefaultRotate:F

    iput v2, p1, Lcom/samsung/app/video/editor/external/ClipartParams;->mDefaultRotate:F

    .line 167
    iget v2, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->mPivotX:F

    iput v2, p1, Lcom/samsung/app/video/editor/external/ClipartParams;->mPivotX:F

    .line 168
    iget v2, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->mPivotY:F

    iput v2, p1, Lcom/samsung/app/video/editor/external/ClipartParams;->mPivotY:F

    .line 169
    iget-boolean v2, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->mIsEditable:Z

    iput-boolean v2, p1, Lcom/samsung/app/video/editor/external/ClipartParams;->mIsEditable:Z

    .line 170
    iget v2, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->mCaptionID:I

    iput v2, p1, Lcom/samsung/app/video/editor/external/ClipartParams;->mCaptionID:I

    .line 172
    iget v2, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->mStoryboardStartFrame:I

    iput v2, p1, Lcom/samsung/app/video/editor/external/ClipartParams;->mStoryboardStartFrame:I

    .line 173
    iget v2, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->mStoryboardEndFrame:I

    iput v2, p1, Lcom/samsung/app/video/editor/external/ClipartParams;->mStoryboardEndFrame:I

    .line 175
    iget-boolean v2, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->mAssetResource:Z

    iput-boolean v2, p1, Lcom/samsung/app/video/editor/external/ClipartParams;->mAssetResource:Z

    .line 177
    iget v2, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->maxCharLength:I

    iput v2, p1, Lcom/samsung/app/video/editor/external/ClipartParams;->maxCharLength:I

    .line 178
    iget v2, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->mCaptionTextAlignment:I

    iput v2, p1, Lcom/samsung/app/video/editor/external/ClipartParams;->mCaptionTextAlignment:I

    .line 180
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->mAnimationList:Ljava/util/List;

    if-eqz v2, :cond_0

    .line 181
    new-instance v2, Ljava/util/Vector;

    invoke-direct {v2}, Ljava/util/Vector;-><init>()V

    iput-object v2, p1, Lcom/samsung/app/video/editor/external/ClipartParams;->mAnimationList:Ljava/util/List;

    .line 182
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->mAnimationList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    .line 183
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v0, :cond_3

    .line 187
    .end local v0    # "count":I
    .end local v1    # "i":I
    :cond_0
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->pbBuf:[B

    if-eqz v2, :cond_1

    .line 188
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->pbBuf:[B

    array-length v2, v2

    new-array v2, v2, [B

    iput-object v2, p1, Lcom/samsung/app/video/editor/external/ClipartParams;->pbBuf:[B

    .line 189
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->pbBuf:[B

    iget-object v3, p1, Lcom/samsung/app/video/editor/external/ClipartParams;->pbBuf:[B

    .line 190
    iget-object v4, p1, Lcom/samsung/app/video/editor/external/ClipartParams;->pbBuf:[B

    array-length v4, v4

    .line 189
    invoke-static {v2, v5, v3, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 193
    :cond_1
    if-nez p2, :cond_4

    .line 194
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->Filepath:Ljava/lang/String;

    iput-object v2, p1, Lcom/samsung/app/video/editor/external/ClipartParams;->Filepath:Ljava/lang/String;

    .line 205
    :cond_2
    :goto_1
    return-void

    .line 184
    .restart local v0    # "count":I
    .restart local v1    # "i":I
    :cond_3
    iget-object v3, p1, Lcom/samsung/app/video/editor/external/ClipartParams;->mAnimationList:Ljava/util/List;

    new-instance v4, Lcom/samsung/app/video/editor/external/TextAnimationData;

    iget-object v2, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->mAnimationList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/app/video/editor/external/TextAnimationData;

    invoke-direct {v4, v2}, Lcom/samsung/app/video/editor/external/TextAnimationData;-><init>(Lcom/samsung/app/video/editor/external/TextAnimationData;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 183
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 197
    .end local v0    # "count":I
    .end local v1    # "i":I
    :cond_4
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->Filepath:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 198
    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/ClipartParams;->getNewRawFilePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2, v5}, Lcom/samsung/app/video/editor/external/ClipartParams;->setRawFilePath(Ljava/lang/String;Z)V

    .line 200
    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/ClipartParams;->getFilePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/ClipartParams;->getFilePath()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/ve/util/CommonUtils;->copyFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/samsung/app/video/editor/external/ClipartParams;->getFilePath()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, ".ves"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/ClipartParams;->getFilePath()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, ".ves"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/ve/util/CommonUtils;->copyFile(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static setRawDirPath(Ljava/lang/String;)V
    .locals 0
    .param p0, "rawDir"    # Ljava/lang/String;

    .prologue
    .line 133
    sput-object p0, Lcom/samsung/app/video/editor/external/ClipartParams;->RAW_FILES_SD_PATH:Ljava/lang/String;

    .line 134
    return-void
.end method


# virtual methods
.method public addAnimationToList(Lcom/samsung/app/video/editor/external/TextAnimationData;)V
    .locals 1
    .param p1, "animationData"    # Lcom/samsung/app/video/editor/external/TextAnimationData;

    .prologue
    .line 358
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->mAnimationList:Ljava/util/List;

    if-nez v0, :cond_0

    .line 359
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->mAnimationList:Ljava/util/List;

    .line 360
    :cond_0
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->mAnimationList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 361
    return-void
.end method

.method public changeCaptionData(Ljava/lang/String;Landroid/content/Context;Landroid/content/res/AssetManager;)V
    .locals 6
    .param p1, "newText"    # Ljava/lang/String;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "assetMgr"    # Landroid/content/res/AssetManager;

    .prologue
    .line 442
    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/app/video/editor/external/ClipartParams;->changeCaptionData(Ljava/lang/String;Landroid/content/Context;Landroid/content/res/AssetManager;Landroid/graphics/drawable/NinePatchDrawable;I)V

    .line 443
    return-void
.end method

.method public changeCaptionData(Ljava/lang/String;Landroid/content/Context;Landroid/content/res/AssetManager;Landroid/graphics/drawable/NinePatchDrawable;I)V
    .locals 28
    .param p1, "newText"    # Ljava/lang/String;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "assetMgr"    # Landroid/content/res/AssetManager;
    .param p4, "bg"    # Landroid/graphics/drawable/NinePatchDrawable;
    .param p5, "paddingLR"    # I

    .prologue
    .line 446
    sget-object v15, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    .line 447
    .local v15, "paintAlign":Landroid/graphics/Paint$Align;
    const/16 v22, 0x0

    .line 448
    .local v22, "fontTranslateX":F
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/app/video/editor/external/ClipartParams;->mFontparams:Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;

    if-eqz v5, :cond_4

    .line 449
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/app/video/editor/external/ClipartParams;->mFontparams:Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;

    iget v5, v5, Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;->mAlignFlag:I

    const/16 v6, 0x11

    if-ne v5, v6, :cond_5

    .line 450
    sget-object v15, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    .line 451
    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/app/video/editor/external/ClipartParams;->clipart_width:I

    int-to-float v5, v5

    const/high16 v6, 0x40000000    # 2.0f

    div-float v22, v5, v6

    .line 457
    :cond_0
    :goto_0
    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/app/video/editor/external/ClipartParams;->clipart_width:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/app/video/editor/external/ClipartParams;->clipart_height:I

    sget-object v7, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v5, v6, v7}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v19

    .line 458
    .local v19, "bitmap":Landroid/graphics/Bitmap;
    new-instance v21, Landroid/graphics/Canvas;

    move-object/from16 v0, v21

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 460
    .local v21, "canvas":Landroid/graphics/Canvas;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/app/video/editor/external/ClipartParams;->mFontparams:Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;

    iget-object v6, v5, Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;->mFontAssetFilePath:Ljava/lang/String;

    .line 461
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/app/video/editor/external/ClipartParams;->mFontparams:Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;

    iget-boolean v7, v5, Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;->mIsPlatformFont:Z

    .line 462
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/app/video/editor/external/ClipartParams;->mFontparams:Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;

    iget v8, v5, Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;->mFontTextStyle:I

    .line 463
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/app/video/editor/external/ClipartParams;->mFontparams:Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;

    iget v9, v5, Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;->mFontSize:I

    .line 464
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/app/video/editor/external/ClipartParams;->mFontparams:Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;

    iget v10, v5, Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;->mFontColor:I

    .line 465
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/app/video/editor/external/ClipartParams;->mFontparams:Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;

    iget v11, v5, Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;->mShadowR:I

    .line 466
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/app/video/editor/external/ClipartParams;->mFontparams:Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;

    iget v12, v5, Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;->mShadowX:I

    .line 467
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/app/video/editor/external/ClipartParams;->mFontparams:Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;

    iget v13, v5, Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;->mShadowY:I

    .line 468
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/app/video/editor/external/ClipartParams;->mFontparams:Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;

    iget v14, v5, Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;->mShadowColor:I

    .line 471
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/app/video/editor/external/ClipartParams;->mFontparams:Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;

    iget v0, v5, Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;->mPaintFlags:I

    move/from16 v17, v0

    move-object/from16 v5, p2

    move-object/from16 v16, p3

    .line 459
    invoke-static/range {v5 .. v17}, Lcom/sec/android/app/ve/util/CommonUtils;->createTextPaint(Landroid/content/Context;Ljava/lang/String;ZIIIIIIILandroid/graphics/Paint$Align;Landroid/content/res/AssetManager;I)Landroid/graphics/Paint;

    move-result-object v24

    .line 472
    .local v24, "paint":Landroid/graphics/Paint;
    if-eqz p1, :cond_3

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_3

    .line 473
    new-instance v20, Landroid/graphics/Rect;

    invoke-direct/range {v20 .. v20}, Landroid/graphics/Rect;-><init>()V

    .line 474
    .local v20, "bounds":Landroid/graphics/Rect;
    const-string v26, "x"

    .line 475
    .local v26, "spaceWidthFixString":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static/range {v26 .. v26}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v26

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual/range {v26 .. v26}, Ljava/lang/String;->length()I

    move-result v8

    shl-int/lit8 v8, v8, 0x1

    add-int/2addr v7, v8

    move-object/from16 v0, v24

    move-object/from16 v1, v20

    invoke-virtual {v0, v5, v6, v7, v1}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 477
    new-instance v25, Landroid/graphics/Rect;

    invoke-direct/range {v25 .. v25}, Landroid/graphics/Rect;-><init>()V

    .line 478
    .local v25, "spaceWidthFixRect":Landroid/graphics/Rect;
    const/4 v5, 0x0

    invoke-virtual/range {v26 .. v26}, Ljava/lang/String;->length()I

    move-result v6

    move-object/from16 v0, v24

    move-object/from16 v1, v26

    move-object/from16 v2, v25

    invoke-virtual {v0, v1, v5, v6, v2}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 480
    invoke-virtual/range {v20 .. v20}, Landroid/graphics/Rect;->width()I

    move-result v5

    invoke-virtual/range {v25 .. v25}, Landroid/graphics/Rect;->width()I

    move-result v6

    shl-int/lit8 v6, v6, 0x1

    sub-int v18, v5, v6

    .line 481
    .local v18, "actualWidthIncludeSpace":I
    shl-int/lit8 v5, p5, 0x1

    add-int v5, v5, v18

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/app/video/editor/external/ClipartParams;->clipart_width:I

    if-le v5, v6, :cond_1

    .line 484
    const/high16 v5, 0x429c0000    # 78.0f

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 485
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static/range {v26 .. v26}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v26

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual/range {v26 .. v26}, Ljava/lang/String;->length()I

    move-result v8

    shl-int/lit8 v8, v8, 0x1

    add-int/2addr v7, v8

    move-object/from16 v0, v24

    move-object/from16 v1, v20

    invoke-virtual {v0, v5, v6, v7, v1}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 486
    const/4 v5, 0x0

    invoke-virtual/range {v26 .. v26}, Ljava/lang/String;->length()I

    move-result v6

    move-object/from16 v0, v24

    move-object/from16 v1, v26

    move-object/from16 v2, v25

    invoke-virtual {v0, v1, v5, v6, v2}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 487
    invoke-virtual/range {v20 .. v20}, Landroid/graphics/Rect;->width()I

    move-result v5

    invoke-virtual/range {v25 .. v25}, Landroid/graphics/Rect;->width()I

    move-result v6

    shl-int/lit8 v6, v6, 0x1

    sub-int v18, v5, v6

    .line 490
    :cond_1
    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/app/video/editor/external/ClipartParams;->clipart_height:I

    int-to-float v5, v5

    invoke-virtual/range {v24 .. v24}, Landroid/graphics/Paint;->ascent()F

    move-result v6

    invoke-virtual/range {v24 .. v24}, Landroid/graphics/Paint;->descent()F

    move-result v7

    add-float/2addr v6, v7

    sub-float/2addr v5, v6

    const/high16 v6, 0x40000000    # 2.0f

    div-float v23, v5, v6

    .line 493
    .local v23, "fontTranslateY":F
    if-eqz p4, :cond_2

    .line 494
    move/from16 v0, v22

    float-to-int v5, v0

    shr-int/lit8 v6, v18, 0x1

    sub-int/2addr v5, v6

    sub-int v5, v5, p5

    const/4 v6, 0x0

    move/from16 v0, v22

    float-to-int v7, v0

    shr-int/lit8 v8, v18, 0x1

    add-int/2addr v7, v8

    add-int v7, v7, p5

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/app/video/editor/external/ClipartParams;->clipart_height:I

    move-object/from16 v0, p4

    invoke-virtual {v0, v5, v6, v7, v8}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 495
    move-object/from16 v0, p4

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 498
    :cond_2
    const-string v5, "VE_LITE"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "drawing title = "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/app/ve/common/LogUtils;->criticalLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 500
    move-object/from16 v0, v21

    move-object/from16 v1, p1

    move/from16 v2, v22

    move/from16 v3, v23

    move-object/from16 v4, v24

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 502
    .end local v18    # "actualWidthIncludeSpace":I
    .end local v20    # "bounds":Landroid/graphics/Rect;
    .end local v23    # "fontTranslateY":F
    .end local v25    # "spaceWidthFixRect":Landroid/graphics/Rect;
    .end local v26    # "spaceWidthFixString":Ljava/lang/String;
    :cond_3
    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/app/video/editor/external/ClipartParams;->clipart_width:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/app/video/editor/external/ClipartParams;->clipart_height:I

    mul-int/2addr v5, v6

    mul-int/lit8 v5, v5, 0x4

    invoke-static {v5}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v27

    .line 503
    .local v27, "tempBuffer":Ljava/nio/ByteBuffer;
    move-object/from16 v0, v19

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Landroid/graphics/Bitmap;->copyPixelsToBuffer(Ljava/nio/Buffer;)V

    .line 504
    invoke-virtual/range {v19 .. v19}, Landroid/graphics/Bitmap;->recycle()V

    .line 505
    const/16 v19, 0x0

    .line 506
    if-eqz p1, :cond_6

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_6

    .line 507
    new-instance v5, Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-direct {v5, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/samsung/app/video/editor/external/ClipartParams;->data:Ljava/lang/String;

    .line 510
    :goto_1
    invoke-virtual/range {v27 .. v27}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/samsung/app/video/editor/external/ClipartParams;->saveDataInRAWFile([B)V

    .line 512
    .end local v19    # "bitmap":Landroid/graphics/Bitmap;
    .end local v21    # "canvas":Landroid/graphics/Canvas;
    .end local v24    # "paint":Landroid/graphics/Paint;
    .end local v27    # "tempBuffer":Ljava/nio/ByteBuffer;
    :cond_4
    return-void

    .line 453
    :cond_5
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/app/video/editor/external/ClipartParams;->mFontparams:Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;

    iget v5, v5, Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;->mAlignFlag:I

    const/4 v6, 0x5

    if-ne v5, v6, :cond_0

    .line 454
    sget-object v15, Landroid/graphics/Paint$Align;->RIGHT:Landroid/graphics/Paint$Align;

    .line 455
    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/app/video/editor/external/ClipartParams;->clipart_width:I

    int-to-float v0, v5

    move/from16 v22, v0

    goto/16 :goto_0

    .line 509
    .restart local v19    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v21    # "canvas":Landroid/graphics/Canvas;
    .restart local v24    # "paint":Landroid/graphics/Paint;
    .restart local v27    # "tempBuffer":Ljava/nio/ByteBuffer;
    :cond_6
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/samsung/app/video/editor/external/ClipartParams;->data:Ljava/lang/String;

    goto :goto_1
.end method

.method public getCaptionID()I
    .locals 1

    .prologue
    .line 599
    iget v0, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->mCaptionID:I

    return v0
.end method

.method public getCaptionTextAlignment()I
    .locals 1

    .prologue
    .line 584
    iget v0, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->mCaptionTextAlignment:I

    return v0
.end method

.method public getElementID()I
    .locals 1

    .prologue
    .line 375
    iget v0, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->elementID:I

    return v0
.end method

.method public getEndTime()J
    .locals 2

    .prologue
    .line 224
    iget v0, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->itext_end_time:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public getFilePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 591
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->Filepath:Ljava/lang/String;

    return-object v0
.end method

.method public getFontParams()Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;
    .locals 1

    .prologue
    .line 321
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->mFontparams:Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;

    return-object v0
.end method

.method public getMaxCharLength()I
    .locals 1

    .prologue
    .line 396
    iget v0, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->maxCharLength:I

    return v0
.end method

.method public getNewRawFilePath()Ljava/lang/String;
    .locals 4

    .prologue
    .line 588
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/app/video/editor/external/ClipartParams;->RAW_FILES_SD_PATH:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "/draw_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".raw"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getStartTime()J
    .locals 2

    .prologue
    .line 231
    iget v0, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->itext_start_time:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public getStoryBoardEndTime()J
    .locals 2

    .prologue
    .line 238
    iget v0, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->storyboardEndTime:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public getStoryBoardStartTime()J
    .locals 2

    .prologue
    .line 245
    iget v0, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->storyboardStartTime:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public getStoryboardEndFrame()I
    .locals 1

    .prologue
    .line 354
    iget v0, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->mStoryboardEndFrame:I

    return v0
.end method

.method public getStoryboardStartFrame()I
    .locals 1

    .prologue
    .line 346
    iget v0, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->mStoryboardStartFrame:I

    return v0
.end method

.method public isAssetResource()Z
    .locals 1

    .prologue
    .line 568
    iget-boolean v0, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->mAssetResource:Z

    return v0
.end method

.method public isEditable()Z
    .locals 1

    .prologue
    .line 364
    iget-boolean v0, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->mIsEditable:Z

    return v0
.end method

.method public isThemeDefaultText()Z
    .locals 1

    .prologue
    .line 249
    iget-boolean v0, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->isThemeDefaultText:Z

    return v0
.end method

.method public remove()V
    .locals 1

    .prologue
    .line 253
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/app/video/editor/external/ClipartParams;->remove(Z)V

    .line 254
    return-void
.end method

.method public remove(Z)V
    .locals 4
    .param p1, "forceDelete"    # Z

    .prologue
    .line 257
    iget-object v2, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->Filepath:Ljava/lang/String;

    if-eqz v2, :cond_1

    if-eqz p1, :cond_1

    .line 258
    new-instance v0, Ljava/io/File;

    iget-object v2, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->Filepath:Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 259
    .local v0, "rawFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 260
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 261
    :cond_0
    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->Filepath:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, ".ves"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 262
    .local v1, "spenFile":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 263
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 266
    .end local v0    # "rawFile":Ljava/io/File;
    .end local v1    # "spenFile":Ljava/io/File;
    :cond_1
    return-void
.end method

.method public saveDataInRAWFile([B)V
    .locals 8
    .param p1, "rawData"    # [B

    .prologue
    .line 415
    if-eqz p1, :cond_0

    .line 417
    new-instance v5, Ljava/lang/StringBuilder;

    sget-object v6, Lcom/samsung/app/video/editor/external/ClipartParams;->RAW_FILES_SD_PATH:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "/draw_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ".raw"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->Filepath:Ljava/lang/String;

    .line 418
    const/4 v5, 0x0

    invoke-virtual {p0, v5}, Lcom/samsung/app/video/editor/external/ClipartParams;->setAssetResource(Z)V

    .line 419
    new-instance v4, Ljava/io/File;

    iget-object v5, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->Filepath:Ljava/lang/String;

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 420
    .local v4, "rawFile":Ljava/io/File;
    const/4 v2, 0x0

    .line 423
    .local v2, "fosdata":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 424
    .end local v2    # "fosdata":Ljava/io/FileOutputStream;
    .local v3, "fosdata":Ljava/io/FileOutputStream;
    :try_start_1
    invoke-virtual {v3, p1}, Ljava/io/FileOutputStream;->write([B)V

    .line 425
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->flush()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 430
    if-eqz v3, :cond_0

    .line 432
    :try_start_2
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    .line 439
    .end local v3    # "fosdata":Ljava/io/FileOutputStream;
    .end local v4    # "rawFile":Ljava/io/File;
    :cond_0
    :goto_0
    return-void

    .line 426
    .restart local v2    # "fosdata":Ljava/io/FileOutputStream;
    .restart local v4    # "rawFile":Ljava/io/File;
    :catch_0
    move-exception v1

    .line 427
    .local v1, "ex":Ljava/lang/Exception;
    :goto_1
    :try_start_3
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 430
    if-eqz v2, :cond_0

    .line 432
    :try_start_4
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_0

    .line 433
    :catch_1
    move-exception v0

    .line 434
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 429
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v5

    .line 430
    :goto_2
    if-eqz v2, :cond_1

    .line 432
    :try_start_5
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    .line 437
    :cond_1
    :goto_3
    throw v5

    .line 433
    :catch_2
    move-exception v0

    .line 434
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 433
    .end local v0    # "e":Ljava/io/IOException;
    .end local v2    # "fosdata":Ljava/io/FileOutputStream;
    .restart local v3    # "fosdata":Ljava/io/FileOutputStream;
    :catch_3
    move-exception v0

    .line 434
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 429
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_1
    move-exception v5

    move-object v2, v3

    .end local v3    # "fosdata":Ljava/io/FileOutputStream;
    .restart local v2    # "fosdata":Ljava/io/FileOutputStream;
    goto :goto_2

    .line 426
    .end local v2    # "fosdata":Ljava/io/FileOutputStream;
    .restart local v3    # "fosdata":Ljava/io/FileOutputStream;
    :catch_4
    move-exception v1

    move-object v2, v3

    .end local v3    # "fosdata":Ljava/io/FileOutputStream;
    .restart local v2    # "fosdata":Ljava/io/FileOutputStream;
    goto :goto_1
.end method

.method public setAssetResource(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 576
    iput-boolean p1, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->mAssetResource:Z

    .line 577
    return-void
.end method

.method public setCaptionID(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 595
    iput p1, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->mCaptionID:I

    .line 596
    return-void
.end method

.method public setDefaultTransformations(FFFFFFF)V
    .locals 0
    .param p1, "alpha"    # F
    .param p2, "scale"    # F
    .param p3, "translateX"    # F
    .param p4, "translateY"    # F
    .param p5, "rotate"    # F
    .param p6, "pivotX"    # F
    .param p7, "pivotY"    # F

    .prologue
    .line 332
    iput p1, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->mDefaultAlpha:F

    .line 333
    iput p5, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->mDefaultRotate:F

    .line 334
    iput p2, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->mDefaultScale:F

    .line 335
    iput p3, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->mDefaultTranslateX:F

    .line 336
    iput p4, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->mDefaultTranslateY:F

    .line 337
    iput p6, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->mPivotX:F

    .line 338
    iput p7, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->mPivotY:F

    .line 339
    return-void
.end method

.method public setEditable(Z)V
    .locals 0
    .param p1, "editable"    # Z

    .prologue
    .line 368
    iput-boolean p1, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->mIsEditable:Z

    .line 369
    return-void
.end method

.method public setElementID(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 372
    iput p1, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->elementID:I

    .line 373
    return-void
.end method

.method public setEndTime(J)V
    .locals 1
    .param p1, "storyBoardEndTime"    # J

    .prologue
    .line 273
    long-to-int v0, p1

    iput v0, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->itext_end_time:I

    .line 274
    return-void
.end method

.method public setFontParams(Ljava/lang/String;IIIIIII)V
    .locals 12
    .param p1, "fontFile"    # Ljava/lang/String;
    .param p2, "align"    # I
    .param p3, "fontSize"    # I
    .param p4, "fontColor"    # I
    .param p5, "shadowColor"    # I
    .param p6, "shadowX"    # I
    .param p7, "shadowY"    # I
    .param p8, "shadowR"    # I

    .prologue
    .line 324
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v11, 0x1

    move-object v0, p0

    move-object v1, p1

    move v4, p2

    move v5, p3

    move/from16 v6, p4

    move/from16 v7, p5

    move/from16 v8, p6

    move/from16 v9, p7

    move/from16 v10, p8

    invoke-virtual/range {v0 .. v11}, Lcom/samsung/app/video/editor/external/ClipartParams;->setFontParams(Ljava/lang/String;ZIIIIIIIII)V

    .line 325
    return-void
.end method

.method public setFontParams(Ljava/lang/String;ZIIIIIIIII)V
    .locals 12
    .param p1, "fontFile"    # Ljava/lang/String;
    .param p2, "isPlatformFont"    # Z
    .param p3, "fontTextStyle"    # I
    .param p4, "align"    # I
    .param p5, "fontSize"    # I
    .param p6, "fontColor"    # I
    .param p7, "shadowColor"    # I
    .param p8, "shadowX"    # I
    .param p9, "shadowY"    # I
    .param p10, "shadowR"    # I
    .param p11, "paintFlags"    # I

    .prologue
    .line 328
    new-instance v0, Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move/from16 v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    move/from16 v9, p9

    move/from16 v10, p10

    move/from16 v11, p11

    invoke-direct/range {v0 .. v11}, Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;-><init>(Ljava/lang/String;ZIIIIIIIII)V

    iput-object v0, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->mFontparams:Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;

    .line 329
    return-void
.end method

.method public setMaxCharLength(I)V
    .locals 0
    .param p1, "length"    # I

    .prologue
    .line 405
    iput p1, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->maxCharLength:I

    .line 406
    return-void
.end method

.method public setRawFilePath(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "preventRawFileFromDeletion"    # Z

    .prologue
    .line 387
    invoke-direct {p0}, Lcom/samsung/app/video/editor/external/ClipartParams;->checkRAWFilesDir()V

    .line 388
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->Filepath:Ljava/lang/String;

    .line 389
    return-void
.end method

.method public setStartTime(J)V
    .locals 1
    .param p1, "storyBoardStartTime"    # J

    .prologue
    .line 281
    long-to-int v0, p1

    iput v0, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->itext_start_time:I

    .line 282
    return-void
.end method

.method public setStoryBoardEndTime(J)V
    .locals 1
    .param p1, "storyBoardEndTime"    # J

    .prologue
    .line 289
    long-to-int v0, p1

    iput v0, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->storyboardEndTime:I

    .line 290
    return-void
.end method

.method public setStoryBoardStartTime(J)V
    .locals 1
    .param p1, "storyBoardStartTime"    # J

    .prologue
    .line 297
    long-to-int v0, p1

    iput v0, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->storyboardStartTime:I

    .line 298
    return-void
.end method

.method public setStoryboardEndFrame(I)V
    .locals 0
    .param p1, "endFrame"    # I

    .prologue
    .line 350
    iput p1, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->mStoryboardEndFrame:I

    .line 351
    return-void
.end method

.method public setStoryboardStartFrame(I)V
    .locals 0
    .param p1, "startFrame"    # I

    .prologue
    .line 342
    iput p1, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->mStoryboardStartFrame:I

    .line 343
    return-void
.end method

.method public setThemeDefaultText(Z)V
    .locals 0
    .param p1, "isThemeElement"    # Z

    .prologue
    .line 301
    iput-boolean p1, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->isThemeDefaultText:Z

    .line 302
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 306
    new-instance v0, Ljava/lang/StringBuffer;

    const-string v1, "ClipartParams: ----->\n"

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 307
    .local v0, "strBuf":Ljava/lang/StringBuffer;
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "clipart_width: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->clipart_width:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 308
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "clipart_height: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->clipart_height:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 309
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "itext_start_time: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->itext_start_time:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 310
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "itext_end_time: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->itext_end_time:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 311
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "storyboardStartTime: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->storyboardStartTime:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 312
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "storyboardEndTime: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->storyboardEndTime:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 313
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Filepath: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/samsung/app/video/editor/external/ClipartParams;->Filepath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 315
    const-string v1, "<---------\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 317
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
