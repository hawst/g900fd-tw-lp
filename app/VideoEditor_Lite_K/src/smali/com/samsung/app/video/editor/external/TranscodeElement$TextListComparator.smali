.class Lcom/samsung/app/video/editor/external/TranscodeElement$TextListComparator;
.super Ljava/lang/Object;
.source "TranscodeElement.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/app/video/editor/external/TranscodeElement;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "TextListComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/samsung/app/video/editor/external/ClipartParams;",
        ">;",
        "Ljava/io/Serializable;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x1L


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/samsung/app/video/editor/external/ClipartParams;Lcom/samsung/app/video/editor/external/ClipartParams;)I
    .locals 4
    .param p1, "object1"    # Lcom/samsung/app/video/editor/external/ClipartParams;
    .param p2, "object2"    # Lcom/samsung/app/video/editor/external/ClipartParams;

    .prologue
    .line 76
    invoke-virtual {p1}, Lcom/samsung/app/video/editor/external/ClipartParams;->getStoryBoardStartTime()J

    move-result-wide v0

    long-to-float v0, v0

    .line 77
    invoke-virtual {p2}, Lcom/samsung/app/video/editor/external/ClipartParams;->getStoryBoardStartTime()J

    move-result-wide v2

    long-to-float v1, v2

    .line 76
    invoke-static {v0, v1}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    return v0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lcom/samsung/app/video/editor/external/ClipartParams;

    check-cast p2, Lcom/samsung/app/video/editor/external/ClipartParams;

    invoke-virtual {p0, p1, p2}, Lcom/samsung/app/video/editor/external/TranscodeElement$TextListComparator;->compare(Lcom/samsung/app/video/editor/external/ClipartParams;Lcom/samsung/app/video/editor/external/ClipartParams;)I

    move-result v0

    return v0
.end method
