.class public Lcom/samsung/app/video/editor/external/Constants$SLOW_MOTION_SPEED;
.super Ljava/lang/Object;
.source "Constants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/app/video/editor/external/Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SLOW_MOTION_SPEED"
.end annotation


# static fields
.field public static SPEED_NORMAL:I

.field public static SPEED_X1:I

.field public static SPEED_X2:I

.field public static SPEED_X_1_BY_2:I

.field public static SPEED_X_1_BY_4:I

.field public static SPEED_X_1_BY_8:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x0

    sput v0, Lcom/samsung/app/video/editor/external/Constants$SLOW_MOTION_SPEED;->SPEED_NORMAL:I

    .line 42
    const/4 v0, 0x1

    sput v0, Lcom/samsung/app/video/editor/external/Constants$SLOW_MOTION_SPEED;->SPEED_X1:I

    .line 43
    const/4 v0, 0x2

    sput v0, Lcom/samsung/app/video/editor/external/Constants$SLOW_MOTION_SPEED;->SPEED_X2:I

    .line 44
    const/4 v0, 0x3

    sput v0, Lcom/samsung/app/video/editor/external/Constants$SLOW_MOTION_SPEED;->SPEED_X_1_BY_2:I

    .line 45
    const/4 v0, 0x4

    sput v0, Lcom/samsung/app/video/editor/external/Constants$SLOW_MOTION_SPEED;->SPEED_X_1_BY_4:I

    .line 46
    const/4 v0, 0x5

    sput v0, Lcom/samsung/app/video/editor/external/Constants$SLOW_MOTION_SPEED;->SPEED_X_1_BY_8:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
