.class public Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;
.super Ljava/lang/Object;
.source "ClipartParams.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/app/video/editor/external/ClipartParams;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FontParams"
.end annotation


# static fields
.field private static final serialVersionUID:J = -0x61a6c421e9cc7853L


# instance fields
.field public mAlignFlag:I

.field public mFontAssetFilePath:Ljava/lang/String;

.field public mFontColor:I

.field public mFontSize:I

.field public mFontTextStyle:I

.field public mIsPlatformFont:Z

.field public mPaintFlags:I

.field public mShadowColor:I

.field public mShadowR:I

.field public mShadowX:I

.field public mShadowY:I


# direct methods
.method public constructor <init>(Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;)V
    .locals 1
    .param p1, "fontParams"    # Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;

    .prologue
    .line 547
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 548
    if-eqz p1, :cond_0

    .line 549
    iget-object v0, p1, Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;->mFontAssetFilePath:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;->mFontAssetFilePath:Ljava/lang/String;

    .line 550
    iget-boolean v0, p1, Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;->mIsPlatformFont:Z

    iput-boolean v0, p0, Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;->mIsPlatformFont:Z

    .line 551
    iget v0, p1, Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;->mFontTextStyle:I

    iput v0, p0, Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;->mFontTextStyle:I

    .line 552
    iget v0, p1, Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;->mAlignFlag:I

    iput v0, p0, Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;->mAlignFlag:I

    .line 553
    iget v0, p1, Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;->mFontSize:I

    iput v0, p0, Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;->mFontSize:I

    .line 554
    iget v0, p1, Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;->mFontColor:I

    iput v0, p0, Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;->mFontColor:I

    .line 555
    iget v0, p1, Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;->mShadowColor:I

    iput v0, p0, Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;->mShadowColor:I

    .line 556
    iget v0, p1, Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;->mShadowX:I

    iput v0, p0, Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;->mShadowX:I

    .line 557
    iget v0, p1, Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;->mShadowY:I

    iput v0, p0, Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;->mShadowY:I

    .line 558
    iget v0, p1, Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;->mShadowR:I

    iput v0, p0, Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;->mShadowR:I

    .line 559
    iget v0, p1, Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;->mPaintFlags:I

    iput v0, p0, Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;->mPaintFlags:I

    .line 561
    :cond_0
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IIIIIII)V
    .locals 12
    .param p1, "fontFile"    # Ljava/lang/String;
    .param p2, "align"    # I
    .param p3, "fontSize"    # I
    .param p4, "fontColor"    # I
    .param p5, "shadowColor"    # I
    .param p6, "shadowX"    # I
    .param p7, "shadowY"    # I
    .param p8, "shadowR"    # I

    .prologue
    .line 530
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v11, 0x1

    move-object v0, p0

    move-object v1, p1

    move v4, p2

    move v5, p3

    move/from16 v6, p4

    move/from16 v7, p5

    move/from16 v8, p6

    move/from16 v9, p7

    move/from16 v10, p8

    invoke-direct/range {v0 .. v11}, Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;-><init>(Ljava/lang/String;ZIIIIIIIII)V

    .line 531
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ZIIIIIIIII)V
    .locals 0
    .param p1, "fontFile"    # Ljava/lang/String;
    .param p2, "isPlatformFont"    # Z
    .param p3, "fontTextStyle"    # I
    .param p4, "align"    # I
    .param p5, "fontSize"    # I
    .param p6, "fontColor"    # I
    .param p7, "shadowColor"    # I
    .param p8, "shadowX"    # I
    .param p9, "shadowY"    # I
    .param p10, "shadowR"    # I
    .param p11, "paintFlags"    # I

    .prologue
    .line 533
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 534
    iput-object p1, p0, Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;->mFontAssetFilePath:Ljava/lang/String;

    .line 535
    iput-boolean p2, p0, Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;->mIsPlatformFont:Z

    .line 536
    iput p3, p0, Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;->mFontTextStyle:I

    .line 537
    iput p4, p0, Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;->mAlignFlag:I

    .line 538
    iput p5, p0, Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;->mFontSize:I

    .line 539
    iput p6, p0, Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;->mFontColor:I

    .line 540
    iput p7, p0, Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;->mShadowColor:I

    .line 541
    iput p8, p0, Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;->mShadowX:I

    .line 542
    iput p9, p0, Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;->mShadowY:I

    .line 543
    iput p10, p0, Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;->mShadowR:I

    .line 544
    iput p11, p0, Lcom/samsung/app/video/editor/external/ClipartParams$FontParams;->mPaintFlags:I

    .line 545
    return-void
.end method
