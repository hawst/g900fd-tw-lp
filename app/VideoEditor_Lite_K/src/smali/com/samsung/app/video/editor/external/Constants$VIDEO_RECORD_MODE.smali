.class public Lcom/samsung/app/video/editor/external/Constants$VIDEO_RECORD_MODE;
.super Ljava/lang/Object;
.source "Constants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/app/video/editor/external/Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "VIDEO_RECORD_MODE"
.end annotation


# static fields
.field public static NORMAL:I

.field public static SLOW:I

.field public static SMOOTH:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    const/4 v0, 0x1

    sput v0, Lcom/samsung/app/video/editor/external/Constants$VIDEO_RECORD_MODE;->NORMAL:I

    .line 51
    const/4 v0, 0x2

    sput v0, Lcom/samsung/app/video/editor/external/Constants$VIDEO_RECORD_MODE;->SLOW:I

    .line 52
    const/4 v0, 0x3

    sput v0, Lcom/samsung/app/video/editor/external/Constants$VIDEO_RECORD_MODE;->SMOOTH:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
