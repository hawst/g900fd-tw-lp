.class public Lcom/samsung/app/video/editor/external/EngineFlawCoverUp;
.super Ljava/lang/Object;
.source "EngineFlawCoverUp.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/app/video/editor/external/EngineFlawCoverUp$ImageReference;
    }
.end annotation


# static fields
.field static mImageList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/app/video/editor/external/EngineFlawCoverUp$ImageReference;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    sput-object v0, Lcom/samsung/app/video/editor/external/EngineFlawCoverUp;->mImageList:Ljava/util/List;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static addNewImageRefernce(Ljava/lang/String;)V
    .locals 3
    .param p0, "filepath"    # Ljava/lang/String;

    .prologue
    .line 10
    invoke-static {p0}, Lcom/samsung/app/video/editor/external/EngineFlawCoverUp;->checkNames(Ljava/lang/String;)Lcom/samsung/app/video/editor/external/EngineFlawCoverUp$ImageReference;

    move-result-object v0

    .line 11
    .local v0, "current":Lcom/samsung/app/video/editor/external/EngineFlawCoverUp$ImageReference;
    if-nez v0, :cond_0

    .line 12
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/samsung/app/video/editor/external/NativeInterface;->EngineInitImage(Ljava/lang/String;)Z

    .line 13
    sget-object v1, Lcom/samsung/app/video/editor/external/EngineFlawCoverUp;->mImageList:Ljava/util/List;

    new-instance v2, Lcom/samsung/app/video/editor/external/EngineFlawCoverUp$ImageReference;

    invoke-direct {v2, p0}, Lcom/samsung/app/video/editor/external/EngineFlawCoverUp$ImageReference;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 17
    :goto_0
    return-void

    .line 15
    :cond_0
    invoke-virtual {v0}, Lcom/samsung/app/video/editor/external/EngineFlawCoverUp$ImageReference;->addRef()I

    goto :goto_0
.end method

.method private static checkNames(Ljava/lang/String;)Lcom/samsung/app/video/editor/external/EngineFlawCoverUp$ImageReference;
    .locals 3
    .param p0, "filepath"    # Ljava/lang/String;

    .prologue
    .line 19
    sget-object v2, Lcom/samsung/app/video/editor/external/EngineFlawCoverUp;->mImageList:Ljava/util/List;

    if-eqz v2, :cond_0

    .line 20
    sget-object v2, Lcom/samsung/app/video/editor/external/EngineFlawCoverUp;->mImageList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    .line 21
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v0, :cond_1

    .line 27
    .end local v0    # "count":I
    .end local v1    # "i":I
    :cond_0
    const/4 v2, 0x0

    :goto_1
    return-object v2

    .line 22
    .restart local v0    # "count":I
    .restart local v1    # "i":I
    :cond_1
    sget-object v2, Lcom/samsung/app/video/editor/external/EngineFlawCoverUp;->mImageList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/app/video/editor/external/EngineFlawCoverUp$ImageReference;

    # getter for: Lcom/samsung/app/video/editor/external/EngineFlawCoverUp$ImageReference;->mFilePath:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/app/video/editor/external/EngineFlawCoverUp$ImageReference;->access$0(Lcom/samsung/app/video/editor/external/EngineFlawCoverUp$ImageReference;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 23
    sget-object v2, Lcom/samsung/app/video/editor/external/EngineFlawCoverUp;->mImageList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/app/video/editor/external/EngineFlawCoverUp$ImageReference;

    goto :goto_1

    .line 21
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static clearImageReferences()V
    .locals 3

    .prologue
    .line 51
    sget-object v1, Lcom/samsung/app/video/editor/external/EngineFlawCoverUp;->mImageList:Ljava/util/List;

    if-eqz v1, :cond_0

    .line 52
    .local v0, "ref":Lcom/samsung/app/video/editor/external/EngineFlawCoverUp$ImageReference;
    :goto_0
    sget-object v1, Lcom/samsung/app/video/editor/external/EngineFlawCoverUp;->mImageList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 56
    sget-object v1, Lcom/samsung/app/video/editor/external/EngineFlawCoverUp;->mImageList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 58
    :cond_0
    return-void

    .line 53
    :cond_1
    sget-object v1, Lcom/samsung/app/video/editor/external/EngineFlawCoverUp;->mImageList:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "ref":Lcom/samsung/app/video/editor/external/EngineFlawCoverUp$ImageReference;
    check-cast v0, Lcom/samsung/app/video/editor/external/EngineFlawCoverUp$ImageReference;

    .line 54
    .restart local v0    # "ref":Lcom/samsung/app/video/editor/external/EngineFlawCoverUp$ImageReference;
    # getter for: Lcom/samsung/app/video/editor/external/EngineFlawCoverUp$ImageReference;->mFilePath:Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/app/video/editor/external/EngineFlawCoverUp$ImageReference;->access$0(Lcom/samsung/app/video/editor/external/EngineFlawCoverUp$ImageReference;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/app/video/editor/external/EngineFlawCoverUp;->removeImageReference(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static removeImageReference(Ljava/lang/String;)V
    .locals 2
    .param p0, "filepath"    # Ljava/lang/String;

    .prologue
    .line 38
    invoke-static {p0}, Lcom/samsung/app/video/editor/external/EngineFlawCoverUp;->checkNames(Ljava/lang/String;)Lcom/samsung/app/video/editor/external/EngineFlawCoverUp$ImageReference;

    move-result-object v0

    .line 39
    .local v0, "current":Lcom/samsung/app/video/editor/external/EngineFlawCoverUp$ImageReference;
    if-eqz v0, :cond_0

    .line 40
    invoke-virtual {v0}, Lcom/samsung/app/video/editor/external/EngineFlawCoverUp$ImageReference;->getCount()I

    move-result v1

    if-lez v1, :cond_1

    .line 41
    invoke-virtual {v0}, Lcom/samsung/app/video/editor/external/EngineFlawCoverUp$ImageReference;->deleteRef()I

    .line 48
    :cond_0
    :goto_0
    return-void

    .line 44
    :cond_1
    invoke-static {}, Lcom/samsung/app/video/editor/external/NativeInterface;->getInstance()Lcom/samsung/app/video/editor/external/NativeInterface;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/samsung/app/video/editor/external/NativeInterface;->EngineDeinitImage(Ljava/lang/String;)V

    .line 45
    sget-object v1, Lcom/samsung/app/video/editor/external/EngineFlawCoverUp;->mImageList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static removeWeakImageReference(Ljava/lang/String;)V
    .locals 2
    .param p0, "filepath"    # Ljava/lang/String;

    .prologue
    .line 31
    invoke-static {p0}, Lcom/samsung/app/video/editor/external/EngineFlawCoverUp;->checkNames(Ljava/lang/String;)Lcom/samsung/app/video/editor/external/EngineFlawCoverUp$ImageReference;

    move-result-object v0

    .line 32
    .local v0, "current":Lcom/samsung/app/video/editor/external/EngineFlawCoverUp$ImageReference;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/samsung/app/video/editor/external/EngineFlawCoverUp$ImageReference;->getCount()I

    move-result v1

    if-ltz v1, :cond_0

    .line 33
    invoke-virtual {v0}, Lcom/samsung/app/video/editor/external/EngineFlawCoverUp$ImageReference;->deleteRef()I

    .line 35
    :cond_0
    return-void
.end method
