.class public Lcom/samsung/app/video/editor/external/Edit;
.super Ljava/lang/Object;
.source "Edit.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static final DEFAULT_VOLUME:I = 0x64

.field private static final serialVersionUID:J = 0x5e7164b734f8bfdL


# instance fields
.field public addcaption:I

.field private editParams:Lcom/samsung/app/video/editor/external/ClipartParams;

.field private effectEndTime:I

.field private effectStartTime:I

.field private opacityFrom:F

.field private opacityTo:F

.field private resourceFileName:Ljava/lang/String;

.field private subType:I

.field private trans_duration:I

.field private type:I

.field private volumeLevel:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/app/video/editor/external/Edit;->addcaption:I

    .line 35
    const/16 v0, 0x64

    iput v0, p0, Lcom/samsung/app/video/editor/external/Edit;->volumeLevel:I

    .line 48
    return-void
.end method

.method public constructor <init>(Lcom/samsung/app/video/editor/external/Edit;)V
    .locals 2
    .param p1, "edit"    # Lcom/samsung/app/video/editor/external/Edit;

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/app/video/editor/external/Edit;->addcaption:I

    .line 35
    const/16 v0, 0x64

    iput v0, p0, Lcom/samsung/app/video/editor/external/Edit;->volumeLevel:I

    .line 51
    iget v0, p1, Lcom/samsung/app/video/editor/external/Edit;->subType:I

    iput v0, p0, Lcom/samsung/app/video/editor/external/Edit;->subType:I

    .line 52
    iget v0, p1, Lcom/samsung/app/video/editor/external/Edit;->trans_duration:I

    iput v0, p0, Lcom/samsung/app/video/editor/external/Edit;->trans_duration:I

    .line 53
    iget v0, p1, Lcom/samsung/app/video/editor/external/Edit;->type:I

    iput v0, p0, Lcom/samsung/app/video/editor/external/Edit;->type:I

    .line 54
    iget v0, p1, Lcom/samsung/app/video/editor/external/Edit;->volumeLevel:I

    iput v0, p0, Lcom/samsung/app/video/editor/external/Edit;->volumeLevel:I

    .line 55
    iget-object v0, p1, Lcom/samsung/app/video/editor/external/Edit;->editParams:Lcom/samsung/app/video/editor/external/ClipartParams;

    if-eqz v0, :cond_0

    .line 56
    new-instance v0, Lcom/samsung/app/video/editor/external/ClipartParams;

    iget-object v1, p1, Lcom/samsung/app/video/editor/external/Edit;->editParams:Lcom/samsung/app/video/editor/external/ClipartParams;

    invoke-direct {v0, v1}, Lcom/samsung/app/video/editor/external/ClipartParams;-><init>(Lcom/samsung/app/video/editor/external/ClipartParams;)V

    iput-object v0, p0, Lcom/samsung/app/video/editor/external/Edit;->editParams:Lcom/samsung/app/video/editor/external/ClipartParams;

    .line 57
    :cond_0
    iget v0, p1, Lcom/samsung/app/video/editor/external/Edit;->effectStartTime:I

    iput v0, p0, Lcom/samsung/app/video/editor/external/Edit;->effectStartTime:I

    .line 58
    iget v0, p1, Lcom/samsung/app/video/editor/external/Edit;->effectEndTime:I

    iput v0, p0, Lcom/samsung/app/video/editor/external/Edit;->effectEndTime:I

    .line 59
    iget-object v0, p1, Lcom/samsung/app/video/editor/external/Edit;->resourceFileName:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 60
    new-instance v0, Ljava/lang/String;

    iget-object v1, p1, Lcom/samsung/app/video/editor/external/Edit;->resourceFileName:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/app/video/editor/external/Edit;->resourceFileName:Ljava/lang/String;

    .line 61
    :cond_1
    iget v0, p1, Lcom/samsung/app/video/editor/external/Edit;->opacityFrom:F

    iput v0, p0, Lcom/samsung/app/video/editor/external/Edit;->opacityFrom:F

    .line 62
    iget v0, p1, Lcom/samsung/app/video/editor/external/Edit;->opacityTo:F

    iput v0, p0, Lcom/samsung/app/video/editor/external/Edit;->opacityTo:F

    .line 63
    return-void
.end method


# virtual methods
.method public getAddcaption()I
    .locals 1

    .prologue
    .line 66
    iget v0, p0, Lcom/samsung/app/video/editor/external/Edit;->addcaption:I

    return v0
.end method

.method public getEditParams()Lcom/samsung/app/video/editor/external/ClipartParams;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/Edit;->editParams:Lcom/samsung/app/video/editor/external/ClipartParams;

    return-object v0
.end method

.method public getEffectEndTime()I
    .locals 1

    .prologue
    .line 138
    iget v0, p0, Lcom/samsung/app/video/editor/external/Edit;->effectEndTime:I

    return v0
.end method

.method public getEffectResourceFile()Ljava/lang/String;
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/samsung/app/video/editor/external/Edit;->resourceFileName:Ljava/lang/String;

    return-object v0
.end method

.method public getEffectStartTime()I
    .locals 1

    .prologue
    .line 118
    iget v0, p0, Lcom/samsung/app/video/editor/external/Edit;->effectStartTime:I

    return v0
.end method

.method public getSubType()I
    .locals 1

    .prologue
    .line 74
    iget v0, p0, Lcom/samsung/app/video/editor/external/Edit;->subType:I

    return v0
.end method

.method public getTrans_duration()I
    .locals 1

    .prologue
    .line 78
    iget v0, p0, Lcom/samsung/app/video/editor/external/Edit;->trans_duration:I

    return v0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 82
    iget v0, p0, Lcom/samsung/app/video/editor/external/Edit;->type:I

    return v0
.end method

.method public getVolumeLevel()I
    .locals 1

    .prologue
    .line 86
    iget v0, p0, Lcom/samsung/app/video/editor/external/Edit;->volumeLevel:I

    return v0
.end method

.method public setAddcaption(I)V
    .locals 0
    .param p1, "addcaption"    # I

    .prologue
    .line 90
    iput p1, p0, Lcom/samsung/app/video/editor/external/Edit;->addcaption:I

    .line 91
    return-void
.end method

.method public setEditParams(Lcom/samsung/app/video/editor/external/ClipartParams;)V
    .locals 0
    .param p1, "editParams"    # Lcom/samsung/app/video/editor/external/ClipartParams;

    .prologue
    .line 94
    iput-object p1, p0, Lcom/samsung/app/video/editor/external/Edit;->editParams:Lcom/samsung/app/video/editor/external/ClipartParams;

    .line 95
    return-void
.end method

.method public setEffectAlpha(FF)V
    .locals 0
    .param p1, "startAlpha"    # F
    .param p2, "endAlpha"    # F

    .prologue
    .line 133
    iput p1, p0, Lcom/samsung/app/video/editor/external/Edit;->opacityFrom:F

    .line 134
    iput p2, p0, Lcom/samsung/app/video/editor/external/Edit;->opacityTo:F

    .line 135
    return-void
.end method

.method public setEffectEndTime(I)V
    .locals 0
    .param p1, "endTime"    # I

    .prologue
    .line 122
    iput p1, p0, Lcom/samsung/app/video/editor/external/Edit;->effectEndTime:I

    .line 123
    return-void
.end method

.method public setEffectResourceFile(Ljava/lang/String;)V
    .locals 0
    .param p1, "filepath"    # Ljava/lang/String;

    .prologue
    .line 126
    iput-object p1, p0, Lcom/samsung/app/video/editor/external/Edit;->resourceFileName:Ljava/lang/String;

    .line 127
    return-void
.end method

.method public setEffectStartTime(I)V
    .locals 0
    .param p1, "startTime"    # I

    .prologue
    .line 114
    iput p1, p0, Lcom/samsung/app/video/editor/external/Edit;->effectStartTime:I

    .line 115
    return-void
.end method

.method public setSubType(I)V
    .locals 0
    .param p1, "subType"    # I

    .prologue
    .line 98
    iput p1, p0, Lcom/samsung/app/video/editor/external/Edit;->subType:I

    .line 99
    return-void
.end method

.method public setTrans_duration(I)V
    .locals 0
    .param p1, "transDuration"    # I

    .prologue
    .line 102
    iput p1, p0, Lcom/samsung/app/video/editor/external/Edit;->trans_duration:I

    .line 103
    return-void
.end method

.method public setType(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 106
    iput p1, p0, Lcom/samsung/app/video/editor/external/Edit;->type:I

    .line 107
    return-void
.end method

.method public setVolumeLevel(I)V
    .locals 0
    .param p1, "volumeLevel"    # I

    .prologue
    .line 110
    iput p1, p0, Lcom/samsung/app/video/editor/external/Edit;->volumeLevel:I

    .line 111
    return-void
.end method
