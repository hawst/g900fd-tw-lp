.class Lcom/samsung/chord/b$1;
.super Landroid/content/BroadcastReceiver;
.source "ConnectivityUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/chord/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/samsung/chord/b;


# direct methods
.method constructor <init>(Lcom/samsung/chord/b;)V
    .locals 0

    .prologue
    .line 427
    iput-object p1, p0, Lcom/samsung/chord/b$1;->a:Lcom/samsung/chord/b;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 431
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 434
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 533
    :cond_0
    :goto_0
    return-void

    .line 438
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/chord/b$1;->isInitialStickyBroadcast()Z

    move-result v0

    if-nez v0, :cond_0

    .line 442
    const-string v0, "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 443
    const-string v0, "networkInfo"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/NetworkInfo;

    .line 445
    if-nez v0, :cond_2

    .line 446
    const-string v0, "ConnectivityUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "fail to extrainfo : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 450
    :cond_2
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 451
    iget-object v0, p0, Lcom/samsung/chord/b$1;->a:Lcom/samsung/chord/b;

    invoke-static {v0, v3, v2}, Lcom/samsung/chord/b;->a(Lcom/samsung/chord/b;II)V

    goto :goto_0

    .line 454
    :cond_3
    iget-object v0, p0, Lcom/samsung/chord/b$1;->a:Lcom/samsung/chord/b;

    invoke-static {v0, v3, v3}, Lcom/samsung/chord/b;->a(Lcom/samsung/chord/b;II)V

    goto :goto_0

    .line 457
    :cond_4
    const-string v0, "android.net.wifi.STATE_CHANGE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 458
    const-string v0, "networkInfo"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/NetworkInfo;

    .line 459
    if-nez v0, :cond_5

    .line 460
    const-string v0, "ConnectivityUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "fail to extrainfo : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 464
    :cond_5
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v1

    if-ne v3, v1, :cond_0

    .line 465
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v0

    .line 467
    sget-object v1, Landroid/net/NetworkInfo$State;->CONNECTED:Landroid/net/NetworkInfo$State;

    if-ne v1, v0, :cond_6

    .line 468
    iget-object v0, p0, Lcom/samsung/chord/b$1;->a:Lcom/samsung/chord/b;

    invoke-static {v0, v2, v2}, Lcom/samsung/chord/b;->a(Lcom/samsung/chord/b;II)V

    goto/16 :goto_0

    .line 470
    :cond_6
    sget-object v1, Landroid/net/NetworkInfo$State;->DISCONNECTED:Landroid/net/NetworkInfo$State;

    if-ne v1, v0, :cond_0

    .line 471
    iget-object v0, p0, Lcom/samsung/chord/b$1;->a:Lcom/samsung/chord/b;

    invoke-static {v0, v2, v3}, Lcom/samsung/chord/b;->a(Lcom/samsung/chord/b;II)V

    goto/16 :goto_0

    .line 476
    :cond_7
    const-string v0, "android.net.wifi.WIFI_AP_STATE_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 477
    const-string v0, "wifi_state"

    const/4 v1, -0x1

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 479
    const/4 v1, 0x3

    if-eq v0, v1, :cond_8

    const/16 v1, 0xd

    if-ne v0, v1, :cond_9

    .line 480
    :cond_8
    iget-object v0, p0, Lcom/samsung/chord/b$1;->a:Lcom/samsung/chord/b;

    invoke-static {v0, v4, v2}, Lcom/samsung/chord/b;->a(Lcom/samsung/chord/b;II)V

    goto/16 :goto_0

    .line 482
    :cond_9
    if-eq v0, v3, :cond_a

    const/16 v1, 0xb

    if-ne v0, v1, :cond_0

    .line 483
    :cond_a
    iget-object v0, p0, Lcom/samsung/chord/b$1;->a:Lcom/samsung/chord/b;

    invoke-static {v0, v4, v3}, Lcom/samsung/chord/b;->a(Lcom/samsung/chord/b;II)V

    goto/16 :goto_0

    .line 490
    :cond_b
    const-string v0, "android.net.wifi.LINK_CONFIGURATION_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 491
    iget-object v0, p0, Lcom/samsung/chord/b$1;->a:Lcom/samsung/chord/b;

    invoke-static {v0}, Lcom/samsung/chord/b;->a(Lcom/samsung/chord/b;)Lcom/samsung/chord/b$a;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 492
    iget-object v0, p0, Lcom/samsung/chord/b$1;->a:Lcom/samsung/chord/b;

    invoke-static {v0}, Lcom/samsung/chord/b;->a(Lcom/samsung/chord/b;)Lcom/samsung/chord/b$a;

    move-result-object v0

    invoke-interface {v0}, Lcom/samsung/chord/b$a;->b()V

    goto/16 :goto_0

    .line 494
    :cond_c
    const-string v0, "android.intent.action.SCREEN_ON"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 496
    iget-object v0, p0, Lcom/samsung/chord/b$1;->a:Lcom/samsung/chord/b;

    invoke-static {v0}, Lcom/samsung/chord/b;->b(Lcom/samsung/chord/b;)V

    goto/16 :goto_0

    .line 497
    :cond_d
    const-string v0, "android.net.wifi.NETWORK_OXYGEN_STATE_CHANGE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 498
    const-string v0, "networkInfo"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/NetworkInfo;

    .line 499
    if-nez v0, :cond_e

    .line 500
    const-string v0, "ConnectivityUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "fail to extrainfo : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 504
    :cond_e
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 505
    const-string v0, "ConnectivityUtils"

    const-string v1, "OXYGEN is connected!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 506
    iget-object v0, p0, Lcom/samsung/chord/b$1;->a:Lcom/samsung/chord/b;

    invoke-static {v0, v2, v2}, Lcom/samsung/chord/b;->a(Lcom/samsung/chord/b;II)V

    goto/16 :goto_0

    .line 509
    :cond_f
    const-string v0, "ConnectivityUtils"

    const-string v1, "OXYGEN is disconnected!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 510
    iget-object v0, p0, Lcom/samsung/chord/b$1;->a:Lcom/samsung/chord/b;

    invoke-static {v0, v2, v3}, Lcom/samsung/chord/b;->a(Lcom/samsung/chord/b;II)V

    goto/16 :goto_0
.end method
