.class public Lcom/samsung/chord/ChordManager;
.super Ljava/lang/Object;
.source "ChordManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/chord/ChordManager$INetworkListener;
    }
.end annotation


# static fields
.field public static final ERROR_FAILED:I = 0x1

.field public static final ERROR_INVALID_INTERFACE:I = 0x4

.field public static final ERROR_INVALID_PARAM:I = 0x2

.field public static final ERROR_INVALID_STATE:I = 0x3

.field public static final ERROR_NONE:I = 0x0

.field public static final INTERFACE_TYPE_WIFI:I = 0x0

.field public static final INTERFACE_TYPE_WIFIAP:I = 0x2

.field public static final INTERFACE_TYPE_WIFIP2P:I = 0x1

.field public static final PUBLIC_CHANNEL:Ljava/lang/String; = "Chord"

.field public static final SECURE_PREFIX:Ljava/lang/String; = "#"

.field private static a:Lcom/samsung/chord/ChordManager;

.field private static d:Landroid/os/Handler;


# instance fields
.field private b:Lcom/samsung/android/sdk/chord/ChordAgent;

.field private c:Landroid/content/Context;

.field private e:Ljava/lang/String;

.field private f:Lcom/samsung/chord/IChordManagerListener;

.field private g:I

.field private h:Lcom/samsung/chord/b;

.field private i:I

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/chord/IChordChannel;",
            ">;"
        }
    .end annotation
.end field

.field private m:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private n:Lcom/samsung/chord/ChordManager$INetworkListener;

.field private o:[Ljava/lang/Integer;

.field private p:J

.field private q:Z

.field private r:Z

.field private s:Z

.field private t:Z

.field private u:I

.field private v:Ljava/lang/String;

.field private w:Lcom/samsung/chord/b/a;

.field private x:Lcom/samsung/chord/b$a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 188
    sput-object v0, Lcom/samsung/chord/ChordManager;->a:Lcom/samsung/chord/ChordManager;

    .line 194
    sput-object v0, Lcom/samsung/chord/ChordManager;->d:Landroid/os/Handler;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 6

    .prologue
    const/16 v5, 0xbb9

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 719
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 190
    iput-object v2, p0, Lcom/samsung/chord/ChordManager;->b:Lcom/samsung/android/sdk/chord/ChordAgent;

    .line 192
    iput-object v2, p0, Lcom/samsung/chord/ChordManager;->c:Landroid/content/Context;

    .line 196
    iput-object v2, p0, Lcom/samsung/chord/ChordManager;->e:Ljava/lang/String;

    .line 198
    iput-object v2, p0, Lcom/samsung/chord/ChordManager;->f:Lcom/samsung/chord/IChordManagerListener;

    .line 200
    iput v3, p0, Lcom/samsung/chord/ChordManager;->g:I

    .line 202
    iput-object v2, p0, Lcom/samsung/chord/ChordManager;->h:Lcom/samsung/chord/b;

    .line 204
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/chord/ChordManager;->i:I

    .line 206
    iput-object v2, p0, Lcom/samsung/chord/ChordManager;->j:Ljava/lang/String;

    .line 208
    iput-object v2, p0, Lcom/samsung/chord/ChordManager;->k:Ljava/lang/String;

    .line 210
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/chord/ChordManager;->l:Ljava/util/ArrayList;

    .line 212
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/chord/ChordManager;->m:Ljava/util/ArrayList;

    .line 214
    iput-object v2, p0, Lcom/samsung/chord/ChordManager;->n:Lcom/samsung/chord/ChordManager$INetworkListener;

    .line 215
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Integer;

    iput-object v0, p0, Lcom/samsung/chord/ChordManager;->o:[Ljava/lang/Integer;

    .line 217
    const-wide/32 v0, 0x84d0

    iput-wide v0, p0, Lcom/samsung/chord/ChordManager;->p:J

    .line 218
    iput-boolean v3, p0, Lcom/samsung/chord/ChordManager;->q:Z

    .line 219
    iput-boolean v3, p0, Lcom/samsung/chord/ChordManager;->r:Z

    .line 221
    iput-boolean v3, p0, Lcom/samsung/chord/ChordManager;->s:Z

    .line 222
    iput-boolean v4, p0, Lcom/samsung/chord/ChordManager;->t:Z

    .line 224
    iput v4, p0, Lcom/samsung/chord/ChordManager;->u:I

    .line 225
    iput-object v2, p0, Lcom/samsung/chord/ChordManager;->v:Ljava/lang/String;

    .line 227
    iput-object v2, p0, Lcom/samsung/chord/ChordManager;->w:Lcom/samsung/chord/b/a;

    .line 1187
    new-instance v0, Lcom/samsung/chord/ChordManager$2;

    invoke-direct {v0, p0}, Lcom/samsung/chord/ChordManager$2;-><init>(Lcom/samsung/chord/ChordManager;)V

    iput-object v0, p0, Lcom/samsung/chord/ChordManager;->x:Lcom/samsung/chord/b$a;

    .line 720
    iput-object p1, p0, Lcom/samsung/chord/ChordManager;->c:Landroid/content/Context;

    .line 721
    const-string v0, "1.5.0_20130828_296808"

    iput-object v0, p0, Lcom/samsung/chord/ChordManager;->v:Ljava/lang/String;

    .line 722
    new-instance v0, Lcom/samsung/android/sdk/chord/ChordAgent;

    invoke-direct {v0}, Lcom/samsung/android/sdk/chord/ChordAgent;-><init>()V

    iput-object v0, p0, Lcom/samsung/chord/ChordManager;->b:Lcom/samsung/android/sdk/chord/ChordAgent;

    .line 723
    new-instance v0, Lcom/samsung/chord/b;

    iget-object v1, p0, Lcom/samsung/chord/ChordManager;->c:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/chord/b;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/chord/ChordManager;->h:Lcom/samsung/chord/b;

    .line 724
    iput v3, p0, Lcom/samsung/chord/ChordManager;->g:I

    .line 726
    sget-object v0, Lcom/samsung/chord/ChordManager;->d:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 727
    sget-object v0, Lcom/samsung/chord/ChordManager;->d:Landroid/os/Handler;

    invoke-virtual {v0, v5}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 728
    const-string v0, "chord_api"

    const-string v1, "ChordManager : remove stopped"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 729
    sget-object v0, Lcom/samsung/chord/ChordManager;->d:Landroid/os/Handler;

    invoke-virtual {v0, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 733
    :cond_0
    sput-object v2, Lcom/samsung/chord/ChordManager;->d:Landroid/os/Handler;

    .line 735
    const-string v0, "chord_api"

    const-string v1, "***********************************************"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 736
    const-string v0, "chord_api"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "* Hi Samsung Chord "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/chord/ChordManager;->v:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 737
    const-string v0, "chord_api"

    const-string v1, "***********************************************"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 738
    return-void
.end method

.method private a(IZLcom/samsung/chord/IChordManagerListener;)I
    .locals 7

    .prologue
    const/4 v1, 0x2

    const/4 v0, 0x4

    const/4 v6, 0x0

    const/4 v2, 0x1

    .line 742
    iget v3, p0, Lcom/samsung/chord/ChordManager;->g:I

    if-eqz v3, :cond_0

    iget v3, p0, Lcom/samsung/chord/ChordManager;->g:I

    if-eq v0, v3, :cond_0

    .line 743
    const-string v0, "chord_api"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ChordManager : start : ERROR_INVALID_STATE - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/chord/ChordManager;->g:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 744
    const/4 v0, 0x3

    .line 841
    :goto_0
    return v0

    .line 747
    :cond_0
    iget-object v3, p0, Lcom/samsung/chord/ChordManager;->h:Lcom/samsung/chord/b;

    invoke-virtual {v3, p1}, Lcom/samsung/chord/b;->b(I)Z

    move-result v3

    if-nez v3, :cond_1

    .line 748
    const-string v1, "chord_api"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ChordManager : start : ERROR_INVALID_INTERFACE - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-direct {p0, p1}, Lcom/samsung/chord/ChordManager;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 752
    :cond_1
    if-nez p3, :cond_2

    .line 753
    const-string v0, "chord_api"

    const-string v2, "ChordManager : start : ERROR_INVALID_PARAM - listener is required "

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 754
    goto :goto_0

    .line 758
    :cond_2
    iget-object v3, p0, Lcom/samsung/chord/ChordManager;->e:Ljava/lang/String;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/samsung/chord/ChordManager;->e:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 759
    :cond_3
    const-string v3, "chord_api"

    const-string v4, "ChordManager : start : Please call setTempDirectory before start!"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 762
    :cond_4
    iget-boolean v3, p0, Lcom/samsung/chord/ChordManager;->s:Z

    if-nez v3, :cond_5

    if-eqz p2, :cond_5

    .line 763
    const-string v0, "chord_api"

    const-string v2, "ChordManager : start : ERROR_INVALID_PARAM - you should call getInstanceWithSecurity before start"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 764
    goto :goto_0

    .line 767
    :cond_5
    const-string v1, "chord_api"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ChordManager : start : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/chord/ChordManager;->b:Lcom/samsung/android/sdk/chord/ChordAgent;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/chord/ChordAgent;->getChordAgentState()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 768
    iget-object v1, p0, Lcom/samsung/chord/ChordManager;->b:Lcom/samsung/android/sdk/chord/ChordAgent;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/chord/ChordAgent;->ChordRelease()Z

    .line 769
    iget-object v1, p0, Lcom/samsung/chord/ChordManager;->b:Lcom/samsung/android/sdk/chord/ChordAgent;

    iget-object v3, p0, Lcom/samsung/chord/ChordManager;->e:Ljava/lang/String;

    invoke-virtual {v1, v3}, Lcom/samsung/android/sdk/chord/ChordAgent;->ChordInit(Ljava/lang/String;)I

    move-result v1

    .line 771
    if-eqz v1, :cond_6

    .line 772
    const-string v0, "chord_api"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ChordManager : start : ERROR_FAILED - internal error "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v2

    .line 773
    goto/16 :goto_0

    .line 777
    :cond_6
    iget-object v1, p0, Lcom/samsung/chord/ChordManager;->b:Lcom/samsung/android/sdk/chord/ChordAgent;

    iget-boolean v3, p0, Lcom/samsung/chord/ChordManager;->s:Z

    invoke-virtual {v1, v3}, Lcom/samsung/android/sdk/chord/ChordAgent;->setSecureMode(Z)Z

    .line 779
    iput-object p3, p0, Lcom/samsung/chord/ChordManager;->f:Lcom/samsung/chord/IChordManagerListener;

    .line 781
    iput p1, p0, Lcom/samsung/chord/ChordManager;->i:I

    .line 783
    iget-object v1, p0, Lcom/samsung/chord/ChordManager;->h:Lcom/samsung/chord/b;

    iget-object v3, p0, Lcom/samsung/chord/ChordManager;->x:Lcom/samsung/chord/b$a;

    invoke-virtual {v1, v3}, Lcom/samsung/chord/b;->a(Lcom/samsung/chord/b$a;)V

    .line 785
    sget-object v1, Lcom/samsung/chord/ChordManager;->d:Landroid/os/Handler;

    if-nez v1, :cond_7

    .line 786
    iget-object v1, p0, Lcom/samsung/chord/ChordManager;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/samsung/chord/ChordManager;->setHandleEventLooper(Landroid/os/Looper;)V

    .line 789
    :cond_7
    iget-object v1, p0, Lcom/samsung/chord/ChordManager;->b:Lcom/samsung/android/sdk/chord/ChordAgent;

    sget-object v3, Lcom/samsung/chord/ChordManager;->d:Landroid/os/Handler;

    invoke-virtual {v1, v3}, Lcom/samsung/android/sdk/chord/ChordAgent;->setHandler(Landroid/os/Handler;)V

    .line 791
    iget-object v1, p0, Lcom/samsung/chord/ChordManager;->b:Lcom/samsung/android/sdk/chord/ChordAgent;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/chord/ChordAgent;->setUdpDiscover(Z)V

    .line 793
    iget-object v1, p0, Lcom/samsung/chord/ChordManager;->b:Lcom/samsung/android/sdk/chord/ChordAgent;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/chord/ChordAgent;->setNodeExpiry(Z)V

    .line 794
    iget-object v1, p0, Lcom/samsung/chord/ChordManager;->b:Lcom/samsung/android/sdk/chord/ChordAgent;

    iget-boolean v3, p0, Lcom/samsung/chord/ChordManager;->t:Z

    invoke-virtual {v1, v3}, Lcom/samsung/android/sdk/chord/ChordAgent;->setSmartDiscovery(Z)V

    .line 795
    iget-object v1, p0, Lcom/samsung/chord/ChordManager;->b:Lcom/samsung/android/sdk/chord/ChordAgent;

    iget v3, p0, Lcom/samsung/chord/ChordManager;->u:I

    invoke-virtual {v1, v3}, Lcom/samsung/android/sdk/chord/ChordAgent;->setSendFileLimit(I)Z

    .line 796
    iget-object v1, p0, Lcom/samsung/chord/ChordManager;->b:Lcom/samsung/android/sdk/chord/ChordAgent;

    iget-wide v4, p0, Lcom/samsung/chord/ChordManager;->p:J

    invoke-virtual {v1, v4, v5}, Lcom/samsung/android/sdk/chord/ChordAgent;->setLivenessTimeout(J)V

    .line 800
    iget-object v1, p0, Lcom/samsung/chord/ChordManager;->h:Lcom/samsung/chord/b;

    iget v3, p0, Lcom/samsung/chord/ChordManager;->i:I

    invoke-virtual {v1, v3}, Lcom/samsung/chord/b;->a(I)Ljava/lang/String;

    move-result-object v1

    .line 802
    const-string v3, "chord_api"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ChordManager : interface("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 803
    const-string v3, "chord_api"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ChordManager : smart discover("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/samsung/chord/ChordManager;->t:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 807
    iget-object v3, p0, Lcom/samsung/chord/ChordManager;->h:Lcom/samsung/chord/b;

    iget v4, p0, Lcom/samsung/chord/ChordManager;->i:I

    invoke-virtual {v3, v4}, Lcom/samsung/chord/b;->c(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/chord/ChordManager;->k:Ljava/lang/String;

    .line 812
    iget-object v3, p0, Lcom/samsung/chord/ChordManager;->b:Lcom/samsung/android/sdk/chord/ChordAgent;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ":*"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Lcom/samsung/android/sdk/chord/ChordAgent;->setUsingInterface(Ljava/lang/String;)V

    .line 813
    iput v2, p0, Lcom/samsung/chord/ChordManager;->g:I

    .line 814
    iget-object v1, p0, Lcom/samsung/chord/ChordManager;->b:Lcom/samsung/android/sdk/chord/ChordAgent;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/chord/ChordAgent;->ChordStart()Z

    move-result v1

    if-nez v1, :cond_8

    .line 816
    const-string v1, "chord_api"

    const-string v3, "start : ERROR_FAILED - internal error (fail to start)"

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 818
    iput-object v6, p0, Lcom/samsung/chord/ChordManager;->f:Lcom/samsung/chord/IChordManagerListener;

    .line 820
    const/4 v1, -0x1

    iput v1, p0, Lcom/samsung/chord/ChordManager;->i:I

    .line 822
    iput-object v6, p0, Lcom/samsung/chord/ChordManager;->k:Ljava/lang/String;

    .line 824
    iget-object v1, p0, Lcom/samsung/chord/ChordManager;->h:Lcom/samsung/chord/b;

    invoke-virtual {v1, v6}, Lcom/samsung/chord/b;->a(Lcom/samsung/chord/b$a;)V

    .line 826
    iget-object v1, p0, Lcom/samsung/chord/ChordManager;->b:Lcom/samsung/android/sdk/chord/ChordAgent;

    invoke-virtual {v1, v6}, Lcom/samsung/android/sdk/chord/ChordAgent;->setHandler(Landroid/os/Handler;)V

    .line 828
    iget-object v1, p0, Lcom/samsung/chord/ChordManager;->b:Lcom/samsung/android/sdk/chord/ChordAgent;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/chord/ChordAgent;->ChordRelease()Z

    .line 829
    iput v0, p0, Lcom/samsung/chord/ChordManager;->g:I

    move v0, v2

    .line 830
    goto/16 :goto_0

    .line 835
    :cond_8
    iget-object v0, p0, Lcom/samsung/chord/ChordManager;->h:Lcom/samsung/chord/b;

    invoke-virtual {v0}, Lcom/samsung/chord/b;->a()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 836
    const-string v0, "chord_api"

    const-string v1, "Call Emulator_translator while running in AVD"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 837
    new-instance v0, Lcom/samsung/chord/b/a;

    iget-object v1, p0, Lcom/samsung/chord/ChordManager;->c:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/chord/b/a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/chord/ChordManager;->w:Lcom/samsung/chord/b/a;

    .line 838
    iget-object v0, p0, Lcom/samsung/chord/ChordManager;->w:Lcom/samsung/chord/b/a;

    invoke-virtual {v0}, Lcom/samsung/chord/b/a;->start()V

    .line 841
    :cond_9
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method static synthetic a()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 55
    sget-object v0, Lcom/samsung/chord/ChordManager;->d:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic a(Lcom/samsung/chord/ChordManager;)Lcom/samsung/chord/ChordManager$INetworkListener;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/samsung/chord/ChordManager;->n:Lcom/samsung/chord/ChordManager$INetworkListener;

    return-object v0
.end method

.method private a(Ljava/lang/String;ZLcom/samsung/chord/IChordChannelListener;)Lcom/samsung/chord/IChordChannel;
    .locals 7

    .prologue
    const/4 v6, 0x6

    const/4 v5, 0x1

    const/4 v3, 0x0

    .line 847
    if-eqz p2, :cond_1

    const-string v0, "joinSecureChannel : "

    move-object v2, v0

    .line 848
    :goto_0
    const-string v0, "chord_api"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ChordManager : "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "<"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ">"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 850
    const/4 v0, 0x2

    iget v1, p0, Lcom/samsung/chord/ChordManager;->g:I

    if-eq v0, v1, :cond_2

    iget v0, p0, Lcom/samsung/chord/ChordManager;->g:I

    if-eq v6, v0, :cond_2

    .line 851
    const-string v0, "chord_api"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ChordManager : "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "ERROR_INVALID_STATE - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/chord/ChordManager;->g:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v3

    .line 925
    :cond_0
    :goto_1
    return-object v1

    .line 847
    :cond_1
    const-string v0, "joinChannel : "

    move-object v2, v0

    goto :goto_0

    .line 855
    :cond_2
    if-nez p3, :cond_3

    .line 856
    const-string v0, "chord_api"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ChordManager : "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "ERROR_INVALID_PARAM - listener is required "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v3

    .line 857
    goto :goto_1

    .line 860
    :cond_3
    if-eqz p1, :cond_4

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 861
    :cond_4
    const-string v0, "chord_api"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ChordManager : "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "ERROR_INVALID_PARAM - invalid channel name"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v3

    .line 862
    goto :goto_1

    .line 865
    :cond_5
    iget-boolean v0, p0, Lcom/samsung/chord/ChordManager;->s:Z

    if-nez v0, :cond_6

    if-eqz p2, :cond_6

    .line 866
    const-string v0, "chord_api"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ChordManager : "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "ERROR_INVALID_PARAM - invalid secure mode"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v3

    .line 867
    goto :goto_1

    .line 870
    :cond_6
    invoke-virtual {p0, p1}, Lcom/samsung/chord/ChordManager;->getJoinedChannel(Ljava/lang/String;)Lcom/samsung/chord/IChordChannel;

    move-result-object v1

    .line 872
    if-eqz v1, :cond_9

    move-object v0, v1

    .line 874
    check-cast v0, Lcom/samsung/chord/a;

    .line 876
    invoke-virtual {v0}, Lcom/samsung/chord/a;->a()I

    move-result v4

    if-nez v4, :cond_7

    .line 878
    const-string v3, "chord_api"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "Already joined!"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 880
    invoke-virtual {v0, p3}, Lcom/samsung/chord/a;->a(Lcom/samsung/chord/IChordChannelListener;)V

    goto/16 :goto_1

    .line 884
    :cond_7
    invoke-virtual {v0}, Lcom/samsung/chord/a;->a()I

    move-result v4

    if-ne v5, v4, :cond_8

    .line 886
    const-string v3, "chord_api"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "re-joined!"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 888
    invoke-virtual {v0, p3}, Lcom/samsung/chord/a;->a(Lcom/samsung/chord/IChordChannelListener;)V

    goto/16 :goto_1

    .line 894
    :cond_8
    iget-object v0, p0, Lcom/samsung/chord/ChordManager;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 898
    :cond_9
    new-instance v1, Lcom/samsung/chord/a;

    iget-object v0, p0, Lcom/samsung/chord/ChordManager;->b:Lcom/samsung/android/sdk/chord/ChordAgent;

    invoke-direct {v1, v0, p1, p2, p3}, Lcom/samsung/chord/a;-><init>(Lcom/samsung/android/sdk/chord/ChordAgent;Ljava/lang/String;ZLcom/samsung/chord/IChordChannelListener;)V

    .line 900
    iget v0, p0, Lcom/samsung/chord/ChordManager;->g:I

    if-ne v6, v0, :cond_a

    move-object v0, v1

    .line 901
    check-cast v0, Lcom/samsung/chord/a;

    invoke-virtual {v0, v5}, Lcom/samsung/chord/a;->a(I)V

    .line 904
    :cond_a
    iget-object v0, p0, Lcom/samsung/chord/ChordManager;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 906
    const-string v0, "Chord"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    move-object v0, v1

    .line 908
    check-cast v0, Lcom/samsung/chord/a;

    iget-object v2, p0, Lcom/samsung/chord/ChordManager;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Lcom/samsung/chord/a;->a(Ljava/util/List;)V

    goto/16 :goto_1

    .line 914
    :cond_b
    iget-object v0, p0, Lcom/samsung/chord/ChordManager;->b:Lcom/samsung/android/sdk/chord/ChordAgent;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/chord/ChordAgent;->joinChannel(Ljava/lang/String;)I

    move-result v0

    .line 916
    if-eqz v0, :cond_0

    .line 918
    const-string v4, "chord_api"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "ChordManager : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "ERROR_FAILED - internal error "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 920
    iget-object v0, p0, Lcom/samsung/chord/ChordManager;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-object v1, v3

    .line 921
    goto/16 :goto_1
.end method

.method private a(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 940
    if-nez p1, :cond_0

    .line 941
    const-string v0, "INTERFACE_TYPE_WIFI"

    .line 949
    :goto_0
    return-object v0

    .line 943
    :cond_0
    const/4 v0, 0x1

    if-ne v0, p1, :cond_1

    .line 944
    const-string v0, "INTERFACE_TYPE_WIFIP2P"

    goto :goto_0

    .line 946
    :cond_1
    const/4 v0, 0x2

    if-ne v0, p1, :cond_2

    .line 947
    const-string v0, "INTERFACE_TYPE_WIFIAP"

    goto :goto_0

    .line 949
    :cond_2
    const-string v0, "INTERFACE_TYPE_UNKNOWN"

    goto :goto_0
.end method

.method private a(Landroid/os/Message;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/16 v6, 0x3e8

    const/4 v5, 0x4

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 974
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 1180
    :cond_0
    :goto_0
    :sswitch_0
    return-void

    .line 976
    :sswitch_1
    iget-object v0, p0, Lcom/samsung/chord/ChordManager;->n:Lcom/samsung/chord/ChordManager$INetworkListener;

    if-eqz v0, :cond_0

    .line 979
    iget-object v0, p0, Lcom/samsung/chord/ChordManager;->o:[Ljava/lang/Integer;

    iget v1, p1, Landroid/os/Message;->arg1:I

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget v1, p1, Landroid/os/Message;->arg2:I

    if-ne v0, v1, :cond_1

    .line 980
    const-string v0, "chord_api"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ChordManager : Net Type :  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " CUR: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->arg2:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 984
    :cond_1
    iget-object v0, p0, Lcom/samsung/chord/ChordManager;->o:[Ljava/lang/Integer;

    iget v1, p1, Landroid/os/Message;->arg1:I

    iget v2, p1, Landroid/os/Message;->arg2:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 986
    iget v0, p1, Landroid/os/Message;->arg2:I

    if-nez v0, :cond_2

    .line 987
    iget-object v0, p0, Lcom/samsung/chord/ChordManager;->n:Lcom/samsung/chord/ChordManager$INetworkListener;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-interface {v0, v1}, Lcom/samsung/chord/ChordManager$INetworkListener;->onConnected(I)V

    goto :goto_0

    .line 989
    :cond_2
    iget-object v0, p0, Lcom/samsung/chord/ChordManager;->n:Lcom/samsung/chord/ChordManager$INetworkListener;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-interface {v0, v1}, Lcom/samsung/chord/ChordManager$INetworkListener;->onDisconnected(I)V

    goto :goto_0

    .line 995
    :sswitch_2
    const/4 v0, 0x3

    iget v3, p0, Lcom/samsung/chord/ChordManager;->g:I

    if-ne v0, v3, :cond_3

    .line 996
    const/4 v0, 0x2

    iput v0, p0, Lcom/samsung/chord/ChordManager;->g:I

    .line 997
    iput-boolean v2, p0, Lcom/samsung/chord/ChordManager;->r:Z

    .line 998
    invoke-virtual {p0}, Lcom/samsung/chord/ChordManager;->stop()V

    goto :goto_0

    .line 1000
    :cond_3
    const/4 v0, 0x5

    iget v3, p0, Lcom/samsung/chord/ChordManager;->g:I

    if-ne v0, v3, :cond_4

    .line 1001
    invoke-virtual {p0}, Lcom/samsung/chord/ChordManager;->close()V

    goto :goto_0

    .line 1007
    :cond_4
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/chord/ChordManager;->j:Ljava/lang/String;

    .line 1010
    const/4 v0, 0x6

    iget v3, p0, Lcom/samsung/chord/ChordManager;->g:I

    if-ne v0, v3, :cond_f

    .line 1013
    iget-object v0, p0, Lcom/samsung/chord/ChordManager;->h:Lcom/samsung/chord/b;

    iget v3, p0, Lcom/samsung/chord/ChordManager;->i:I

    invoke-virtual {v0, v3}, Lcom/samsung/chord/b;->c(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/chord/ChordManager;->k:Ljava/lang/String;

    .line 1014
    iput-boolean v2, p0, Lcom/samsung/chord/ChordManager;->r:Z

    .line 1016
    iget-object v0, p0, Lcom/samsung/chord/ChordManager;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/chord/IChordChannel;

    .line 1018
    check-cast v0, Lcom/samsung/chord/a;

    invoke-virtual {v0, v2}, Lcom/samsung/chord/a;->a(I)V

    goto :goto_1

    :cond_5
    move v0, v1

    .line 1023
    :goto_2
    const/4 v1, 0x2

    iput v1, p0, Lcom/samsung/chord/ChordManager;->g:I

    .line 1024
    iget-object v1, p0, Lcom/samsung/chord/ChordManager;->f:Lcom/samsung/chord/IChordManagerListener;

    if-eqz v1, :cond_0

    .line 1025
    const-string v1, "chord_api"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ChordManager : ****onStarted("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/chord/ChordManager;->j:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1026
    iget-object v1, p0, Lcom/samsung/chord/ChordManager;->f:Lcom/samsung/chord/IChordManagerListener;

    iget-object v2, p0, Lcom/samsung/chord/ChordManager;->j:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Lcom/samsung/chord/IChordManagerListener;->onStarted(Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 1033
    :sswitch_3
    const-string v0, "chord_api"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ChordManager : handleMessages : state is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/samsung/chord/ChordManager;->g:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1034
    iget v0, p0, Lcom/samsung/chord/ChordManager;->g:I

    if-ne v1, v0, :cond_7

    .line 1035
    iput v5, p0, Lcom/samsung/chord/ChordManager;->g:I

    .line 1037
    iget-object v0, p0, Lcom/samsung/chord/ChordManager;->f:Lcom/samsung/chord/IChordManagerListener;

    if-eqz v0, :cond_6

    .line 1038
    iget-object v0, p0, Lcom/samsung/chord/ChordManager;->f:Lcom/samsung/chord/IChordManagerListener;

    invoke-interface {v0, v6}, Lcom/samsung/chord/IChordManagerListener;->onError(I)V

    .line 1039
    const-string v0, "chord_api"

    const-string v1, "ChordManager : ****onStopped(IChordManagerListener.ERROR_START_FAIL)"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1040
    iget-object v0, p0, Lcom/samsung/chord/ChordManager;->f:Lcom/samsung/chord/IChordManagerListener;

    invoke-interface {v0, v6}, Lcom/samsung/chord/IChordManagerListener;->onStopped(I)V

    .line 1043
    :cond_6
    invoke-virtual {p0}, Lcom/samsung/chord/ChordManager;->stop()V

    goto/16 :goto_0

    .line 1046
    :cond_7
    const/4 v0, 0x3

    iget v3, p0, Lcom/samsung/chord/ChordManager;->g:I

    if-ne v0, v3, :cond_8

    .line 1047
    iget-object v0, p0, Lcom/samsung/chord/ChordManager;->b:Lcom/samsung/android/sdk/chord/ChordAgent;

    invoke-virtual {v0, v7}, Lcom/samsung/android/sdk/chord/ChordAgent;->setHandler(Landroid/os/Handler;)V

    .line 1048
    iget-object v0, p0, Lcom/samsung/chord/ChordManager;->b:Lcom/samsung/android/sdk/chord/ChordAgent;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chord/ChordAgent;->ChordRelease()Z

    .line 1049
    iput v5, p0, Lcom/samsung/chord/ChordManager;->g:I

    .line 1051
    iget-object v0, p0, Lcom/samsung/chord/ChordManager;->f:Lcom/samsung/chord/IChordManagerListener;

    if-eqz v0, :cond_0

    .line 1052
    const-string v0, "chord_api"

    const-string v1, "ChordManager : ****onStopped(IChordManagerListener.STOPPED_BY_USER)"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1053
    iget-object v0, p0, Lcom/samsung/chord/ChordManager;->f:Lcom/samsung/chord/IChordManagerListener;

    const/16 v1, 0x3ea

    invoke-interface {v0, v1}, Lcom/samsung/chord/IChordManagerListener;->onStopped(I)V

    .line 1056
    iget v0, p0, Lcom/samsung/chord/ChordManager;->g:I

    if-ne v0, v5, :cond_0

    .line 1057
    iput-object v7, p0, Lcom/samsung/chord/ChordManager;->f:Lcom/samsung/chord/IChordManagerListener;

    goto/16 :goto_0

    .line 1061
    :cond_8
    const/4 v0, 0x5

    iget v3, p0, Lcom/samsung/chord/ChordManager;->g:I

    if-ne v0, v3, :cond_9

    .line 1066
    iput v5, p0, Lcom/samsung/chord/ChordManager;->g:I

    .line 1067
    invoke-virtual {p0}, Lcom/samsung/chord/ChordManager;->close()V

    goto/16 :goto_0

    .line 1071
    :cond_9
    const/4 v0, 0x6

    iput v0, p0, Lcom/samsung/chord/ChordManager;->g:I

    .line 1072
    iget-object v0, p0, Lcom/samsung/chord/ChordManager;->f:Lcom/samsung/chord/IChordManagerListener;

    if-eqz v0, :cond_b

    .line 1073
    iget-object v0, p0, Lcom/samsung/chord/ChordManager;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/chord/IChordChannel;

    .line 1074
    check-cast v0, Lcom/samsung/chord/a;

    invoke-virtual {v0, v1}, Lcom/samsung/chord/a;->a(I)V

    goto :goto_3

    .line 1076
    :cond_a
    iput-object v7, p0, Lcom/samsung/chord/ChordManager;->k:Ljava/lang/String;

    .line 1077
    iget-object v0, p0, Lcom/samsung/chord/ChordManager;->f:Lcom/samsung/chord/IChordManagerListener;

    invoke-interface {v0}, Lcom/samsung/chord/IChordManagerListener;->onNetworkDisconnected()V

    .line 1078
    const-string v0, "chord_api"

    const-string v1, "ChordManager : ****onStopped(IChordManagerListener.NETWORK_DISCONNECTED)"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1079
    iget-object v0, p0, Lcom/samsung/chord/ChordManager;->f:Lcom/samsung/chord/IChordManagerListener;

    const/16 v1, 0x3eb

    invoke-interface {v0, v1}, Lcom/samsung/chord/IChordManagerListener;->onStopped(I)V

    .line 1082
    :cond_b
    iget-boolean v0, p0, Lcom/samsung/chord/ChordManager;->q:Z

    if-eqz v0, :cond_0

    .line 1083
    iget-object v0, p0, Lcom/samsung/chord/ChordManager;->b:Lcom/samsung/android/sdk/chord/ChordAgent;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/chord/ChordAgent;->setConnectivityState(I)V

    .line 1084
    iput-boolean v2, p0, Lcom/samsung/chord/ChordManager;->q:Z

    goto/16 :goto_0

    .line 1097
    :sswitch_4
    iget-object v0, p0, Lcom/samsung/chord/ChordManager;->f:Lcom/samsung/chord/IChordManagerListener;

    .line 1098
    iput v5, p0, Lcom/samsung/chord/ChordManager;->g:I

    .line 1099
    invoke-virtual {p0}, Lcom/samsung/chord/ChordManager;->stop()V

    .line 1101
    if-eqz v0, :cond_0

    .line 1102
    const/16 v1, 0xbbc

    iget v2, p1, Landroid/os/Message;->what:I

    if-ne v1, v2, :cond_c

    .line 1103
    const-string v1, "chord_api"

    const-string v2, "ChordManager : ****onStopped(IChordManagerListener.ERROR_START_FAIL)"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1104
    invoke-interface {v0, v6}, Lcom/samsung/chord/IChordManagerListener;->onError(I)V

    .line 1105
    invoke-interface {v0, v6}, Lcom/samsung/chord/IChordManagerListener;->onStopped(I)V

    goto/16 :goto_0

    .line 1106
    :cond_c
    const/16 v1, 0xbbc

    iget v2, p1, Landroid/os/Message;->what:I

    if-ne v1, v2, :cond_0

    .line 1107
    const-string v1, "chord_api"

    const-string v2, "ChordManager : ****onStopped(IChordManagerListener.ERROR_UNEXPECTED_STOP)"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1108
    const/16 v1, 0x3e9

    invoke-interface {v0, v1}, Lcom/samsung/chord/IChordManagerListener;->onError(I)V

    .line 1109
    const/16 v1, 0x3e9

    invoke-interface {v0, v1}, Lcom/samsung/chord/IChordManagerListener;->onStopped(I)V

    goto/16 :goto_0

    .line 1134
    :sswitch_5
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/samsung/chord/a/d;

    invoke-interface {v0}, Lcom/samsung/chord/a/d;->a()Ljava/lang/String;

    move-result-object v1

    .line 1137
    const-string v0, "Chord"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 1138
    const-string v0, ""

    .line 1139
    const/16 v0, 0xc1d

    iget v2, p1, Landroid/os/Message;->what:I

    if-ne v0, v2, :cond_e

    .line 1140
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/samsung/chord/a/a;

    iget-object v0, v0, Lcom/samsung/chord/a/a;->b:Ljava/lang/String;

    .line 1141
    iget-object v2, p0, Lcom/samsung/chord/ChordManager;->m:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    .line 1142
    iget-object v2, p0, Lcom/samsung/chord/ChordManager;->m:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1149
    :cond_d
    :goto_4
    invoke-virtual {p0, v1}, Lcom/samsung/chord/ChordManager;->getJoinedChannel(Ljava/lang/String;)Lcom/samsung/chord/IChordChannel;

    move-result-object v0

    check-cast v0, Lcom/samsung/chord/a;

    .line 1150
    if-eqz v0, :cond_0

    .line 1151
    iget v1, p1, Landroid/os/Message;->what:I

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/chord/a;->a(ILjava/lang/Object;)V

    goto/16 :goto_0

    .line 1143
    :cond_e
    const/16 v0, 0xc1e

    iget v2, p1, Landroid/os/Message;->what:I

    if-ne v0, v2, :cond_d

    .line 1144
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/samsung/chord/a/a;

    iget-object v0, v0, Lcom/samsung/chord/a/a;->b:Ljava/lang/String;

    .line 1145
    iget-object v2, p0, Lcom/samsung/chord/ChordManager;->m:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_4

    .line 1158
    :sswitch_6
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    .line 1161
    invoke-virtual {p0, v0}, Lcom/samsung/chord/ChordManager;->getJoinedChannel(Ljava/lang/String;)Lcom/samsung/chord/IChordChannel;

    move-result-object v0

    check-cast v0, Lcom/samsung/chord/a;

    .line 1162
    if-eqz v0, :cond_0

    .line 1163
    iget v1, p1, Landroid/os/Message;->what:I

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/chord/a;->a(ILjava/lang/Object;)V

    goto/16 :goto_0

    .line 1169
    :sswitch_7
    iget-object v0, p0, Lcom/samsung/chord/ChordManager;->b:Lcom/samsung/android/sdk/chord/ChordAgent;

    if-eqz v0, :cond_0

    .line 1170
    const-string v0, "chord_api"

    const-string v2, "ChordManager : NETWORK_DHCP_IP_RENEW"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1171
    iget-object v0, p0, Lcom/samsung/chord/ChordManager;->b:Lcom/samsung/android/sdk/chord/ChordAgent;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chord/ChordAgent;->setConnectivityState(I)V

    .line 1172
    iput-boolean v1, p0, Lcom/samsung/chord/ChordManager;->q:Z

    goto/16 :goto_0

    :cond_f
    move v0, v2

    goto/16 :goto_2

    .line 974
    nop

    :sswitch_data_0
    .sparse-switch
        0xbb8 -> :sswitch_2
        0xbb9 -> :sswitch_3
        0xbba -> :sswitch_4
        0xbbb -> :sswitch_0
        0xbbc -> :sswitch_4
        0xc1c -> :sswitch_6
        0xc1d -> :sswitch_5
        0xc1e -> :sswitch_5
        0xc80 -> :sswitch_5
        0xce4 -> :sswitch_5
        0xce5 -> :sswitch_5
        0xce6 -> :sswitch_5
        0xce8 -> :sswitch_5
        0xce9 -> :sswitch_5
        0xcea -> :sswitch_5
        0xcf8 -> :sswitch_5
        0xcf9 -> :sswitch_5
        0xcfa -> :sswitch_5
        0xcfc -> :sswitch_5
        0xcfd -> :sswitch_5
        0xcfe -> :sswitch_5
        0xcff -> :sswitch_5
        0xd48 -> :sswitch_1
        0xd49 -> :sswitch_7
    .end sparse-switch
.end method

.method static synthetic a(Lcom/samsung/chord/ChordManager;Landroid/os/Message;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0, p1}, Lcom/samsung/chord/ChordManager;->a(Landroid/os/Message;)V

    return-void
.end method

.method private a(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 930
    iget-object v0, p0, Lcom/samsung/chord/ChordManager;->c:Landroid/content/Context;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 931
    const/4 v0, 0x1

    .line 932
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/samsung/chord/ChordManager;Z)Z
    .locals 0

    .prologue
    .line 55
    iput-boolean p1, p0, Lcom/samsung/chord/ChordManager;->r:Z

    return p1
.end method

.method static synthetic b(Lcom/samsung/chord/ChordManager;)I
    .locals 1

    .prologue
    .line 55
    iget v0, p0, Lcom/samsung/chord/ChordManager;->i:I

    return v0
.end method

.method static synthetic c(Lcom/samsung/chord/ChordManager;)Lcom/samsung/chord/b;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/samsung/chord/ChordManager;->h:Lcom/samsung/chord/b;

    return-object v0
.end method

.method static synthetic d(Lcom/samsung/chord/ChordManager;)Lcom/samsung/android/sdk/chord/ChordAgent;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/samsung/chord/ChordManager;->b:Lcom/samsung/android/sdk/chord/ChordAgent;

    return-object v0
.end method

.method static synthetic e(Lcom/samsung/chord/ChordManager;)I
    .locals 1

    .prologue
    .line 55
    iget v0, p0, Lcom/samsung/chord/ChordManager;->g:I

    return v0
.end method

.method static synthetic f(Lcom/samsung/chord/ChordManager;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/samsung/chord/ChordManager;->k:Ljava/lang/String;

    return-object v0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/samsung/chord/ChordManager;
    .locals 2

    .prologue
    .line 242
    sget-object v0, Lcom/samsung/chord/ChordManager;->a:Lcom/samsung/chord/ChordManager;

    if-eqz v0, :cond_1

    .line 243
    sget-object v0, Lcom/samsung/chord/ChordManager;->a:Lcom/samsung/chord/ChordManager;

    invoke-direct {v0, p0}, Lcom/samsung/chord/ChordManager;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 244
    sget-object v0, Lcom/samsung/chord/ChordManager;->a:Lcom/samsung/chord/ChordManager;

    .line 252
    :goto_0
    return-object v0

    .line 246
    :cond_0
    sget-object v0, Lcom/samsung/chord/ChordManager;->a:Lcom/samsung/chord/ChordManager;

    invoke-virtual {v0}, Lcom/samsung/chord/ChordManager;->close()V

    .line 249
    :cond_1
    const-string v0, "chord_api"

    const-string v1, "ChordManager : getInstance"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 250
    new-instance v0, Lcom/samsung/chord/ChordManager;

    invoke-direct {v0, p0}, Lcom/samsung/chord/ChordManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/chord/ChordManager;->a:Lcom/samsung/chord/ChordManager;

    .line 252
    sget-object v0, Lcom/samsung/chord/ChordManager;->a:Lcom/samsung/chord/ChordManager;

    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 5

    .prologue
    const/4 v4, 0x5

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 262
    const-string v0, "chord_api"

    const-string v1, "ChordManager : close"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 264
    sput-object v2, Lcom/samsung/chord/ChordManager;->a:Lcom/samsung/chord/ChordManager;

    .line 265
    iput-object v2, p0, Lcom/samsung/chord/ChordManager;->c:Landroid/content/Context;

    .line 266
    iput-object v2, p0, Lcom/samsung/chord/ChordManager;->e:Ljava/lang/String;

    .line 267
    iput-boolean v3, p0, Lcom/samsung/chord/ChordManager;->s:Z

    .line 269
    invoke-virtual {p0}, Lcom/samsung/chord/ChordManager;->stop()V

    .line 271
    iget-object v0, p0, Lcom/samsung/chord/ChordManager;->h:Lcom/samsung/chord/b;

    if-eqz v0, :cond_0

    .line 272
    iget-object v0, p0, Lcom/samsung/chord/ChordManager;->h:Lcom/samsung/chord/b;

    invoke-virtual {v0, v2}, Lcom/samsung/chord/b;->a(Lcom/samsung/chord/b$a;)V

    .line 273
    :cond_0
    iput-object v2, p0, Lcom/samsung/chord/ChordManager;->h:Lcom/samsung/chord/b;

    .line 275
    const/4 v0, 0x3

    iget v1, p0, Lcom/samsung/chord/ChordManager;->g:I

    if-ne v0, v1, :cond_1

    .line 276
    iput v4, p0, Lcom/samsung/chord/ChordManager;->g:I

    .line 279
    :cond_1
    iget v0, p0, Lcom/samsung/chord/ChordManager;->g:I

    if-ne v4, v0, :cond_2

    .line 289
    :goto_0
    return-void

    .line 283
    :cond_2
    iput-object v2, p0, Lcom/samsung/chord/ChordManager;->b:Lcom/samsung/android/sdk/chord/ChordAgent;

    .line 284
    iput v3, p0, Lcom/samsung/chord/ChordManager;->g:I

    .line 286
    const-string v0, "chord_api"

    const-string v1, "***********************************************"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 287
    const-string v0, "chord_api"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "* Bye Samsung Chord "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/chord/ChordManager;->v:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 288
    const-string v0, "chord_api"

    const-string v1, "***********************************************"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getAvailableInterfaceTypes()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 302
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 303
    iget-object v1, p0, Lcom/samsung/chord/ChordManager;->h:Lcom/samsung/chord/b;

    if-nez v1, :cond_1

    .line 304
    const-string v1, "chord_api"

    const-string v2, "ChordManager : getAvailableInterfaceTypes : Invalid condition. Call this after initialize"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 319
    :cond_0
    :goto_0
    return-object v0

    .line 310
    :cond_1
    iget-object v1, p0, Lcom/samsung/chord/ChordManager;->h:Lcom/samsung/chord/b;

    invoke-virtual {v1, v4}, Lcom/samsung/chord/b;->b(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 311
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 313
    :cond_2
    iget-object v1, p0, Lcom/samsung/chord/ChordManager;->h:Lcom/samsung/chord/b;

    invoke-virtual {v1, v2}, Lcom/samsung/chord/b;->b(I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 314
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 316
    :cond_3
    iget-object v1, p0, Lcom/samsung/chord/ChordManager;->h:Lcom/samsung/chord/b;

    invoke-virtual {v1, v3}, Lcom/samsung/chord/b;->b(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 317
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public getIp()Ljava/lang/String;
    .locals 1

    .prologue
    .line 618
    iget-object v0, p0, Lcom/samsung/chord/ChordManager;->k:Ljava/lang/String;

    return-object v0
.end method

.method public declared-synchronized getJoinedChannel(Ljava/lang/String;)Lcom/samsung/chord/IChordChannel;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 567
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/chord/ChordManager;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 579
    :goto_0
    monitor-exit p0

    return-object v0

    .line 571
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/samsung/chord/ChordManager;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/chord/IChordChannel;

    .line 573
    invoke-interface {v0, p1}, Lcom/samsung/chord/IChordChannel;->isName(Ljava/lang/String;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v3

    if-eqz v3, :cond_1

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 579
    goto :goto_0

    .line 567
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getJoinedChannelList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/chord/IChordChannel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 591
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/chord/ChordManager;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 592
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 591
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 605
    iget-object v0, p0, Lcom/samsung/chord/ChordManager;->j:Ljava/lang/String;

    return-object v0
.end method

.method public isSmartDiscoveryEnabled()Z
    .locals 1

    .prologue
    .line 701
    iget-boolean v0, p0, Lcom/samsung/chord/ChordManager;->t:Z

    return v0
.end method

.method public joinChannel(Ljava/lang/String;Lcom/samsung/chord/IChordChannelListener;)Lcom/samsung/chord/IChordChannel;
    .locals 1

    .prologue
    .line 507
    const-string v0, "#"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 508
    :goto_0
    invoke-direct {p0, p1, v0, p2}, Lcom/samsung/chord/ChordManager;->a(Ljava/lang/String;ZLcom/samsung/chord/IChordChannelListener;)Lcom/samsung/chord/IChordChannel;

    move-result-object v0

    return-object v0

    .line 507
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public leaveChannel(Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v2, 0x2

    .line 519
    iget v0, p0, Lcom/samsung/chord/ChordManager;->g:I

    if-eq v2, v0, :cond_1

    const/4 v0, 0x4

    iget v1, p0, Lcom/samsung/chord/ChordManager;->g:I

    if-eq v0, v1, :cond_1

    .line 521
    const-string v0, "chord_api"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ChordManager : leaveChannel : ERROR_INVALID_STATE - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/chord/ChordManager;->g:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 556
    :cond_0
    :goto_0
    return-void

    .line 527
    :cond_1
    invoke-virtual {p0, p1}, Lcom/samsung/chord/ChordManager;->getJoinedChannel(Ljava/lang/String;)Lcom/samsung/chord/IChordChannel;

    move-result-object v1

    .line 529
    if-nez v1, :cond_2

    .line 531
    const-string v0, "chord_api"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ChordManager : leaveChannel : ERROR_INVALID_PARAM - Have not joined @"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 538
    check-cast v0, Lcom/samsung/chord/a;

    invoke-virtual {v0, v2}, Lcom/samsung/chord/a;->a(I)V

    .line 540
    iget-object v0, p0, Lcom/samsung/chord/ChordManager;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 542
    const-string v0, "Chord"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 548
    iget-object v0, p0, Lcom/samsung/chord/ChordManager;->b:Lcom/samsung/android/sdk/chord/ChordAgent;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/chord/ChordAgent;->leaveChannelEx(Ljava/lang/String;)I

    move-result v0

    .line 550
    if-eqz v0, :cond_0

    .line 552
    const-string v1, "chord_api"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ChordManager : leaveChannel : ERROR_FAILED - internal error "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public resetSmartDiscoveryPeriod()V
    .locals 2

    .prologue
    .line 711
    const/4 v0, 0x1

    iget v1, p0, Lcom/samsung/chord/ChordManager;->g:I

    if-eq v0, v1, :cond_0

    const/4 v0, 0x2

    iget v1, p0, Lcom/samsung/chord/ChordManager;->g:I

    if-ne v0, v1, :cond_1

    .line 712
    :cond_0
    iget-object v0, p0, Lcom/samsung/chord/ChordManager;->b:Lcom/samsung/android/sdk/chord/ChordAgent;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chord/ChordAgent;->resetSmartDiscoveryPeriod()V

    .line 714
    :cond_1
    return-void
.end method

.method public setHandleEventLooper(Landroid/os/Looper;)V
    .locals 2

    .prologue
    .line 358
    if-nez p1, :cond_0

    .line 359
    const-string v0, "chord_api"

    const-string v1, "ChordManager : setHandleEventLooper : ERROR_INVALID_PARAM"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 376
    :goto_0
    return-void

    .line 363
    :cond_0
    new-instance v0, Lcom/samsung/chord/ChordManager$1;

    invoke-direct {v0, p0, p1}, Lcom/samsung/chord/ChordManager$1;-><init>(Lcom/samsung/chord/ChordManager;Landroid/os/Looper;)V

    sput-object v0, Lcom/samsung/chord/ChordManager;->d:Landroid/os/Handler;

    goto :goto_0
.end method

.method public setNetworkListener(Lcom/samsung/chord/ChordManager$INetworkListener;)Z
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 389
    sget-object v0, Lcom/samsung/chord/ChordManager;->d:Landroid/os/Handler;

    if-nez v0, :cond_1

    .line 390
    iput-object v5, p0, Lcom/samsung/chord/ChordManager;->n:Lcom/samsung/chord/ChordManager$INetworkListener;

    move v2, v1

    .line 408
    :cond_0
    :goto_0
    return v2

    :cond_1
    move v3, v1

    .line 394
    :goto_1
    const/4 v0, 0x3

    if-ge v3, v0, :cond_3

    .line 395
    iget-object v4, p0, Lcom/samsung/chord/ChordManager;->o:[Ljava/lang/Integer;

    iget-object v0, p0, Lcom/samsung/chord/ChordManager;->h:Lcom/samsung/chord/b;

    invoke-virtual {v0, v3}, Lcom/samsung/chord/b;->b(I)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v3

    .line 394
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_2
    move v0, v2

    .line 395
    goto :goto_2

    .line 398
    :cond_3
    iput-object p1, p0, Lcom/samsung/chord/ChordManager;->n:Lcom/samsung/chord/ChordManager$INetworkListener;

    .line 400
    iget-object v0, p0, Lcom/samsung/chord/ChordManager;->n:Lcom/samsung/chord/ChordManager$INetworkListener;

    if-eqz v0, :cond_4

    .line 401
    iget-object v0, p0, Lcom/samsung/chord/ChordManager;->h:Lcom/samsung/chord/b;

    iget-object v1, p0, Lcom/samsung/chord/ChordManager;->x:Lcom/samsung/chord/b$a;

    invoke-virtual {v0, v1}, Lcom/samsung/chord/b;->a(Lcom/samsung/chord/b$a;)V

    goto :goto_0

    .line 405
    :cond_4
    iget v0, p0, Lcom/samsung/chord/ChordManager;->g:I

    if-nez v0, :cond_0

    .line 406
    iget-object v0, p0, Lcom/samsung/chord/ChordManager;->h:Lcom/samsung/chord/b;

    invoke-virtual {v0, v5}, Lcom/samsung/chord/b;->a(Lcom/samsung/chord/b$a;)V

    goto :goto_0
.end method

.method public setNodeKeepAliveTimeout(J)V
    .locals 5

    .prologue
    .line 632
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-lez v0, :cond_2

    .line 633
    iput-wide p1, p0, Lcom/samsung/chord/ChordManager;->p:J

    .line 638
    :goto_0
    const/4 v0, 0x1

    iget v1, p0, Lcom/samsung/chord/ChordManager;->g:I

    if-eq v0, v1, :cond_0

    const/4 v0, 0x2

    iget v1, p0, Lcom/samsung/chord/ChordManager;->g:I

    if-ne v0, v1, :cond_1

    .line 639
    :cond_0
    iget-object v0, p0, Lcom/samsung/chord/ChordManager;->b:Lcom/samsung/android/sdk/chord/ChordAgent;

    iget-wide v2, p0, Lcom/samsung/chord/ChordManager;->p:J

    invoke-virtual {v0, v2, v3}, Lcom/samsung/android/sdk/chord/ChordAgent;->setLivenessTimeout(J)V

    .line 642
    :cond_1
    return-void

    .line 635
    :cond_2
    const-wide/32 v0, 0x84d0

    iput-wide v0, p0, Lcom/samsung/chord/ChordManager;->p:J

    goto :goto_0
.end method

.method public setSecureModeEnabled(Z)V
    .locals 0

    .prologue
    .line 669
    iput-boolean p1, p0, Lcom/samsung/chord/ChordManager;->s:Z

    .line 670
    return-void
.end method

.method public setSendMultiFilesLimitCount(I)V
    .locals 2

    .prologue
    .line 652
    iput p1, p0, Lcom/samsung/chord/ChordManager;->u:I

    .line 654
    const/4 v0, 0x1

    iget v1, p0, Lcom/samsung/chord/ChordManager;->g:I

    if-eq v0, v1, :cond_0

    const/4 v0, 0x2

    iget v1, p0, Lcom/samsung/chord/ChordManager;->g:I

    if-ne v0, v1, :cond_1

    .line 655
    :cond_0
    iget-object v0, p0, Lcom/samsung/chord/ChordManager;->b:Lcom/samsung/android/sdk/chord/ChordAgent;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/chord/ChordAgent;->setSendFileLimit(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 656
    const-string v0, "chord_api"

    const-string v1, "ChordManager : fail to setSendFileLimit"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 658
    :cond_1
    return-void
.end method

.method public setSmartDiscoveryEnabled(Z)V
    .locals 2

    .prologue
    .line 687
    iput-boolean p1, p0, Lcom/samsung/chord/ChordManager;->t:Z

    .line 688
    const/4 v0, 0x1

    iget v1, p0, Lcom/samsung/chord/ChordManager;->g:I

    if-eq v0, v1, :cond_0

    const/4 v0, 0x2

    iget v1, p0, Lcom/samsung/chord/ChordManager;->g:I

    if-ne v0, v1, :cond_1

    .line 689
    :cond_0
    iget-object v0, p0, Lcom/samsung/chord/ChordManager;->b:Lcom/samsung/android/sdk/chord/ChordAgent;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/chord/ChordAgent;->setSmartDiscovery(Z)V

    .line 691
    :cond_1
    return-void
.end method

.method public setTempDirectory(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 333
    iput-object p1, p0, Lcom/samsung/chord/ChordManager;->e:Ljava/lang/String;

    .line 335
    iget-object v0, p0, Lcom/samsung/chord/ChordManager;->e:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/chord/ChordManager;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 336
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/samsung/chord/ChordManager;->e:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 337
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 338
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result v0

    if-nez v0, :cond_0

    .line 339
    const-string v0, "chord_api"

    const-string v1, "ChordManager : setTempDirectory : fail to create dir"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 345
    :cond_0
    :goto_0
    return-void

    .line 343
    :cond_1
    const-string v0, "chord_api"

    const-string v1, "ChordManager : setTempDirectory : ERROR_INVALID_PARAM"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public start(ILcom/samsung/chord/IChordManagerListener;)I
    .locals 1

    .prologue
    .line 429
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Lcom/samsung/chord/ChordManager;->a(IZLcom/samsung/chord/IChordManagerListener;)I

    move-result v0

    return v0
.end method

.method public stop()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 438
    const-string v0, "chord_api"

    const-string v1, "ChordManager : stop"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 441
    iget-object v0, p0, Lcom/samsung/chord/ChordManager;->h:Lcom/samsung/chord/b;

    if-eqz v0, :cond_1

    .line 442
    iget-object v0, p0, Lcom/samsung/chord/ChordManager;->h:Lcom/samsung/chord/b;

    invoke-virtual {v0}, Lcom/samsung/chord/b;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 443
    const-string v0, "chord_api"

    const-string v1, "Stop  Emulator_translator while running in AVD"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 445
    iget-object v0, p0, Lcom/samsung/chord/ChordManager;->w:Lcom/samsung/chord/b/a;

    if-eqz v0, :cond_0

    .line 447
    const-string v0, "chord_api"

    const-string v1, "UDPUnicastReceiver is closed "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 448
    iget-object v0, p0, Lcom/samsung/chord/ChordManager;->w:Lcom/samsung/chord/b/a;

    invoke-virtual {v0}, Lcom/samsung/chord/b/a;->a()V

    .line 449
    iget-object v0, p0, Lcom/samsung/chord/ChordManager;->w:Lcom/samsung/chord/b/a;

    invoke-virtual {v0}, Lcom/samsung/chord/b/a;->interrupt()V

    .line 450
    iput-object v2, p0, Lcom/samsung/chord/ChordManager;->w:Lcom/samsung/chord/b/a;

    .line 454
    :cond_0
    iget-object v0, p0, Lcom/samsung/chord/ChordManager;->n:Lcom/samsung/chord/ChordManager$INetworkListener;

    if-nez v0, :cond_1

    .line 455
    iget-object v0, p0, Lcom/samsung/chord/ChordManager;->h:Lcom/samsung/chord/b;

    invoke-virtual {v0, v2}, Lcom/samsung/chord/b;->a(Lcom/samsung/chord/b$a;)V

    .line 459
    :cond_1
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/chord/ChordManager;->i:I

    .line 460
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/chord/ChordManager;->q:Z

    .line 462
    iput-object v2, p0, Lcom/samsung/chord/ChordManager;->k:Ljava/lang/String;

    .line 464
    iget-object v0, p0, Lcom/samsung/chord/ChordManager;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 466
    iget-object v0, p0, Lcom/samsung/chord/ChordManager;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/chord/IChordChannel;

    .line 467
    check-cast v0, Lcom/samsung/chord/a;

    invoke-virtual {v0, v4}, Lcom/samsung/chord/a;->a(I)V

    goto :goto_0

    .line 471
    :cond_2
    iget-object v0, p0, Lcom/samsung/chord/ChordManager;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 472
    iget-object v0, p0, Lcom/samsung/chord/ChordManager;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 473
    iput-object v2, p0, Lcom/samsung/chord/ChordManager;->j:Ljava/lang/String;

    .line 475
    iget v0, p0, Lcom/samsung/chord/ChordManager;->g:I

    if-eq v3, v0, :cond_3

    iget-boolean v0, p0, Lcom/samsung/chord/ChordManager;->r:Z

    if-ne v0, v3, :cond_5

    .line 476
    :cond_3
    iput v5, p0, Lcom/samsung/chord/ChordManager;->g:I

    .line 477
    const-string v0, "chord_api"

    const-string v1, "ChordManager : stop : STATE_REQ_STOP"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 493
    :cond_4
    :goto_1
    return-void

    .line 482
    :cond_5
    iget v0, p0, Lcom/samsung/chord/ChordManager;->g:I

    if-ne v4, v0, :cond_6

    .line 483
    iput v5, p0, Lcom/samsung/chord/ChordManager;->g:I

    .line 484
    iget-object v0, p0, Lcom/samsung/chord/ChordManager;->b:Lcom/samsung/android/sdk/chord/ChordAgent;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chord/ChordAgent;->ChordStop()Z

    goto :goto_1

    .line 486
    :cond_6
    iget v0, p0, Lcom/samsung/chord/ChordManager;->g:I

    if-eq v6, v0, :cond_7

    const/4 v0, 0x6

    iget v1, p0, Lcom/samsung/chord/ChordManager;->g:I

    if-ne v0, v1, :cond_4

    .line 487
    :cond_7
    iget-object v0, p0, Lcom/samsung/chord/ChordManager;->b:Lcom/samsung/android/sdk/chord/ChordAgent;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/chord/ChordAgent;->setHandler(Landroid/os/Handler;)V

    .line 488
    iget-object v0, p0, Lcom/samsung/chord/ChordManager;->b:Lcom/samsung/android/sdk/chord/ChordAgent;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chord/ChordAgent;->ChordRelease()Z

    .line 489
    iput-object v2, p0, Lcom/samsung/chord/ChordManager;->f:Lcom/samsung/chord/IChordManagerListener;

    .line 490
    iput v6, p0, Lcom/samsung/chord/ChordManager;->g:I

    goto :goto_1
.end method
