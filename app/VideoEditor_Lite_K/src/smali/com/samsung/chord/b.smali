.class public Lcom/samsung/chord/b;
.super Ljava/lang/Object;
.source "ConnectivityUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/chord/b$a;
    }
.end annotation


# static fields
.field private static f:Z


# instance fields
.field private a:Landroid/content/Context;

.field private b:Z

.field private c:Lcom/samsung/chord/b$a;

.field private d:Z

.field private e:Z

.field private g:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 83
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/chord/b;->f:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    iput-object v1, p0, Lcom/samsung/chord/b;->a:Landroid/content/Context;

    .line 74
    iput-boolean v0, p0, Lcom/samsung/chord/b;->b:Z

    .line 76
    iput-object v1, p0, Lcom/samsung/chord/b;->c:Lcom/samsung/chord/b$a;

    .line 78
    iput-boolean v0, p0, Lcom/samsung/chord/b;->d:Z

    .line 79
    iput-boolean v0, p0, Lcom/samsung/chord/b;->e:Z

    .line 427
    new-instance v0, Lcom/samsung/chord/b$1;

    invoke-direct {v0, p0}, Lcom/samsung/chord/b$1;-><init>(Lcom/samsung/chord/b;)V

    iput-object v0, p0, Lcom/samsung/chord/b;->g:Landroid/content/BroadcastReceiver;

    .line 87
    iput-object p1, p0, Lcom/samsung/chord/b;->a:Landroid/content/Context;

    .line 89
    sget-object v0, Landroid/os/Build;->BRAND:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "generic"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/chord/b;->f:Z

    .line 90
    invoke-direct {p0}, Lcom/samsung/chord/b;->f()Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/chord/b;->d:Z

    .line 91
    invoke-direct {p0}, Lcom/samsung/chord/b;->h()Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/chord/b;->e:Z

    .line 92
    const-string v0, "ConnectivityUtils"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ConnectivityUtils : E("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/samsung/chord/b;->f:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") O("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/samsung/chord/b;->d:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") S("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/samsung/chord/b;->e:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    return-void
.end method

.method static synthetic a(Lcom/samsung/chord/b;)Lcom/samsung/chord/b$a;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/chord/b;->c:Lcom/samsung/chord/b$a;

    return-object v0
.end method

.method private a(II)V
    .locals 1

    .prologue
    .line 538
    iget-object v0, p0, Lcom/samsung/chord/b;->c:Lcom/samsung/chord/b$a;

    if-eqz v0, :cond_0

    .line 539
    iget-object v0, p0, Lcom/samsung/chord/b;->c:Lcom/samsung/chord/b$a;

    invoke-interface {v0, p1, p2}, Lcom/samsung/chord/b$a;->a(II)V

    .line 541
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/samsung/chord/b;II)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0, p1, p2}, Lcom/samsung/chord/b;->a(II)V

    return-void
.end method

.method static synthetic b(Lcom/samsung/chord/b;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/samsung/chord/b;->j()V

    return-void
.end method

.method private b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 257
    invoke-direct {p0}, Lcom/samsung/chord/b;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 269
    :cond_0
    :goto_0
    return v0

    .line 261
    :cond_1
    iget-boolean v1, p0, Lcom/samsung/chord/b;->d:Z

    if-eqz v1, :cond_2

    invoke-direct {p0}, Lcom/samsung/chord/b;->g()Z

    move-result v1

    if-nez v1, :cond_0

    .line 265
    :cond_2
    iget-boolean v1, p0, Lcom/samsung/chord/b;->e:Z

    if-eqz v1, :cond_3

    invoke-direct {p0}, Lcom/samsung/chord/b;->i()Z

    move-result v1

    if-nez v1, :cond_0

    .line 269
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 273
    iget-object v0, p0, Lcom/samsung/chord/b;->a:Landroid/content/Context;

    const-string v3, "connectivity"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 274
    invoke-virtual {v0, v2}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v0

    .line 275
    if-nez v0, :cond_0

    .line 276
    const-string v0, "ConnectivityUtils"

    const-string v2, "Fail to get Wi-Fi info"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 285
    :goto_0
    return v0

    .line 280
    :cond_0
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v2

    .line 282
    goto :goto_0

    :cond_1
    move v0, v1

    .line 285
    goto :goto_0
.end method

.method private d()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 289
    iget-object v0, p0, Lcom/samsung/chord/b;->a:Landroid/content/Context;

    const-string v2, "connectivity"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 290
    const/16 v2, 0xd

    invoke-virtual {v0, v2}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v0

    .line 291
    if-nez v0, :cond_0

    .line 292
    const-string v0, "ConnectivityUtils"

    const-string v2, "Fail to get Wi-Fi Direct info"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 300
    :goto_0
    return v0

    .line 296
    :cond_0
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 298
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 300
    goto :goto_0
.end method

.method private e()Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 304
    iget-object v0, p0, Lcom/samsung/chord/b;->a:Landroid/content/Context;

    const-string v1, "wifi"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    .line 307
    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    const-string v5, "isWifiApEnabled"

    const/4 v1, 0x0

    check-cast v1, [Ljava/lang/Class;

    invoke-virtual {v4, v5, v1}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 314
    const/4 v1, 0x0

    :try_start_1
    check-cast v1, [Ljava/lang/Object;

    invoke-virtual {v4, v0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-ne v2, v0, :cond_0

    .line 315
    const-string v0, "ConnectivityUtils"

    const-string v1, "ConnectivityUtils.isWifiApEnabled"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_3

    move v0, v2

    .line 326
    :goto_0
    return v0

    .line 308
    :catch_0
    move-exception v0

    .line 309
    const-string v1, "ConnectivityUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ConnectivityUtils.isWifiApEnabled : "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v3

    .line 310
    goto :goto_0

    .line 318
    :catch_1
    move-exception v0

    .line 319
    const-string v1, "ConnectivityUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ConnectivityUtils.isWifiApEnabled : "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_1
    move v0, v3

    .line 326
    goto :goto_0

    .line 320
    :catch_2
    move-exception v0

    .line 321
    const-string v1, "ConnectivityUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ConnectivityUtils.isWifiApEnabled : "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 322
    :catch_3
    move-exception v0

    .line 323
    const-string v1, "ConnectivityUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ConnectivityUtils.isWifiApEnabled : "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private f()Z
    .locals 3

    .prologue
    .line 330
    iget-object v0, p0, Lcom/samsung/chord/b;->a:Landroid/content/Context;

    const-string v1, "wifi"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    .line 332
    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "getWifiIBSSState"

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Class;

    invoke-virtual {v1, v2, v0}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    .line 338
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 333
    :catch_0
    move-exception v0

    .line 334
    const-string v0, "ConnectivityUtils"

    const-string v1, "ConnectivityUtils : It is not supported"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 335
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private g()Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 343
    iget-object v0, p0, Lcom/samsung/chord/b;->a:Landroid/content/Context;

    const-string v1, "wifi"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    .line 346
    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string v4, "getWifiIBSSState"

    const/4 v1, 0x0

    check-cast v1, [Ljava/lang/Class;

    invoke-virtual {v3, v4, v1}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 353
    const/4 v4, 0x3

    const/4 v1, 0x0

    :try_start_1
    check-cast v1, [Ljava/lang/Object;

    invoke-virtual {v3, v0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v4, v1, :cond_1

    .line 354
    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v0

    .line 355
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getBSSID()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 356
    const-string v0, "ConnectivityUtils"

    const-string v1, "ConnectivityUtils.isSecFeature: enabled"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_3

    .line 357
    const/4 v0, 0x1

    .line 369
    :goto_0
    return v0

    .line 347
    :catch_0
    move-exception v0

    .line 348
    const-string v0, "ConnectivityUtils"

    const-string v1, "ConnectivityUtils : It is not supported!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v2

    .line 349
    goto :goto_0

    :cond_0
    move v0, v2

    .line 359
    goto :goto_0

    .line 361
    :catch_1
    move-exception v0

    .line 362
    const-string v0, "ConnectivityUtils"

    const-string v1, "ConnectivityUtils.isSecFeature : IllegalArgumentException"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_1
    move v0, v2

    .line 369
    goto :goto_0

    .line 363
    :catch_2
    move-exception v0

    .line 364
    const-string v0, "ConnectivityUtils"

    const-string v1, "ConnectivityUtils.isSecFeature : IllegalAccessException"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 365
    :catch_3
    move-exception v0

    .line 366
    const-string v0, "ConnectivityUtils"

    const-string v1, "ConnectivityUtils.isSecFeature : InvocationTargetException"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private h()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 375
    iget-object v0, p0, Lcom/samsung/chord/b;->a:Landroid/content/Context;

    const-string v2, "wifi"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    .line 377
    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v2, "getNetworkInfo"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Class;

    invoke-virtual {v0, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    .line 383
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 378
    :catch_0
    move-exception v0

    .line 379
    const-string v0, "ConnectivityUtils"

    const-string v2, "ConnectivityUtils : It is not supported2"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 380
    goto :goto_0
.end method

.method private i()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 387
    iget-object v0, p0, Lcom/samsung/chord/b;->a:Landroid/content/Context;

    const-string v2, "wifi"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    .line 390
    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-string v3, "getNetworkInfo"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Class;

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 397
    const/4 v3, 0x0

    :try_start_1
    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v2, v0, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/NetworkInfo;

    .line 398
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 399
    const-string v0, "ConnectivityUtils"

    const-string v2, "ConnectivityUtils.isSecFeature: connected"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_3

    .line 400
    const/4 v0, 0x1

    .line 412
    :goto_0
    return v0

    .line 391
    :catch_0
    move-exception v0

    .line 392
    const-string v0, "ConnectivityUtils"

    const-string v2, "ConnectivityUtils : It is not supported2!"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 393
    goto :goto_0

    :cond_0
    move v0, v1

    .line 403
    goto :goto_0

    .line 404
    :catch_1
    move-exception v0

    .line 405
    const-string v0, "ConnectivityUtils"

    const-string v2, "ConnectivityUtils.isSecFeature2 : IllegalArgumentException"

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    move v0, v1

    .line 412
    goto :goto_0

    .line 406
    :catch_2
    move-exception v0

    .line 407
    const-string v0, "ConnectivityUtils"

    const-string v2, "ConnectivityUtils.isSecFeature2 : IllegalAccessException"

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 408
    :catch_3
    move-exception v0

    .line 409
    const-string v0, "ConnectivityUtils"

    const-string v2, "ConnectivityUtils.isSecFeature2 : InvocationTargetException"

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private j()V
    .locals 2

    .prologue
    .line 544
    iget-object v0, p0, Lcom/samsung/chord/b;->c:Lcom/samsung/chord/b$a;

    if-eqz v0, :cond_0

    .line 545
    const-string v0, "ConnectivityUtils"

    const-string v1, "notifyScreenOn if"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 546
    iget-object v0, p0, Lcom/samsung/chord/b;->c:Lcom/samsung/chord/b$a;

    invoke-interface {v0}, Lcom/samsung/chord/b$a;->a()V

    .line 548
    :cond_0
    return-void
.end method


# virtual methods
.method public a(I)Ljava/lang/String;
    .locals 3

    .prologue
    .line 129
    invoke-virtual {p0}, Lcom/samsung/chord/b;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 130
    const-string v0, "eth0"

    .line 140
    :goto_0
    return-object v0

    .line 131
    :cond_0
    if-nez p1, :cond_1

    .line 132
    const-string v0, "wlan0"

    goto :goto_0

    .line 133
    :cond_1
    const/4 v0, 0x1

    if-ne v0, p1, :cond_2

    .line 134
    const-string v0, "p2p-wlan0-0"

    goto :goto_0

    .line 135
    :cond_2
    const/4 v0, 0x2

    if-ne v0, p1, :cond_3

    .line 136
    const-string v0, "wlan0"

    goto :goto_0

    .line 139
    :cond_3
    const-string v0, "ConnectivityUtils"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ConnectivityUtils.getInterfaceName : invalid interface type ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized a(Lcom/samsung/chord/b$a;)V
    .locals 3

    .prologue
    .line 97
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/samsung/chord/b;->c:Lcom/samsung/chord/b$a;

    .line 99
    iget-object v0, p0, Lcom/samsung/chord/b;->c:Lcom/samsung/chord/b$a;

    if-nez v0, :cond_2

    .line 100
    iget-boolean v0, p0, Lcom/samsung/chord/b;->b:Z

    if-eqz v0, :cond_0

    .line 101
    iget-object v0, p0, Lcom/samsung/chord/b;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/chord/b;->g:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 102
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/chord/b;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 126
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    .line 107
    :cond_2
    :try_start_1
    iget-boolean v0, p0, Lcom/samsung/chord/b;->b:Z

    if-nez v0, :cond_1

    .line 110
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 111
    const-string v1, "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 112
    const-string v1, "android.net.wifi.STATE_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 113
    const-string v1, "android.net.wifi.WIFI_AP_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 114
    const-string v1, "android.net.wifi.LINK_CONFIGURATION_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 115
    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 117
    iget-boolean v1, p0, Lcom/samsung/chord/b;->d:Z

    if-eqz v1, :cond_3

    .line 118
    const-string v1, "android.net.wifi.NETWORK_OXYGEN_STATE_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 123
    :cond_3
    iget-object v1, p0, Lcom/samsung/chord/b;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/chord/b;->g:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 124
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/chord/b;->b:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 97
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 228
    sget-boolean v0, Lcom/samsung/chord/b;->f:Z

    return v0
.end method

.method public b(I)Z
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 144
    invoke-virtual {p0}, Lcom/samsung/chord/b;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 145
    if-ltz p1, :cond_0

    if-le p1, v3, :cond_1

    .line 146
    :cond_0
    const-string v0, "ConnectivityUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ConnectivityUtils.isAvailableInterfaceType : invalid interface type ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 160
    :cond_1
    :goto_0
    return v0

    .line 151
    :cond_2
    if-nez p1, :cond_3

    .line 152
    invoke-direct {p0}, Lcom/samsung/chord/b;->b()Z

    move-result v0

    goto :goto_0

    .line 153
    :cond_3
    if-ne v0, p1, :cond_4

    .line 154
    invoke-direct {p0}, Lcom/samsung/chord/b;->d()Z

    move-result v0

    goto :goto_0

    .line 155
    :cond_4
    if-ne v3, p1, :cond_5

    .line 156
    invoke-direct {p0}, Lcom/samsung/chord/b;->e()Z

    move-result v0

    goto :goto_0

    .line 159
    :cond_5
    const-string v0, "ConnectivityUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ConnectivityUtils.isAvailableInterfaceType : invalid interface type ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 160
    goto :goto_0
.end method

.method public c(I)Ljava/lang/String;
    .locals 4

    .prologue
    .line 165
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/samsung/chord/b;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/net/NetworkInterface;->getByName(Ljava/lang/String;)Ljava/net/NetworkInterface;

    move-result-object v0

    .line 167
    invoke-virtual {v0}, Ljava/net/NetworkInterface;->getInetAddresses()Ljava/util/Enumeration;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 168
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/InetAddress;

    .line 169
    invoke-virtual {v0}, Ljava/net/InetAddress;->isLoopbackAddress()Z

    move-result v2

    if-nez v2, :cond_0

    .line 171
    instance-of v2, v0, Ljava/net/Inet4Address;

    if-eqz v2, :cond_0

    .line 172
    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 180
    :goto_0
    return-object v0

    .line 176
    :catch_0
    move-exception v0

    .line 177
    const-string v1, "ConnectivityUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ConnectivityUtils.getIp4Address : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 180
    :cond_1
    const-string v0, ""

    goto :goto_0
.end method
