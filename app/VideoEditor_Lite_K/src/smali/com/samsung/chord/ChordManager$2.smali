.class Lcom/samsung/chord/ChordManager$2;
.super Ljava/lang/Object;
.source "ChordManager.java"

# interfaces
.implements Lcom/samsung/chord/b$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/chord/ChordManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/samsung/chord/ChordManager;


# direct methods
.method constructor <init>(Lcom/samsung/chord/ChordManager;)V
    .locals 0

    .prologue
    .line 1187
    iput-object p1, p0, Lcom/samsung/chord/ChordManager$2;->a:Lcom/samsung/chord/ChordManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 1216
    const-string v0, "chord_api"

    const-string v1, "ChordManager : onScreenOn"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1217
    iget-object v0, p0, Lcom/samsung/chord/ChordManager$2;->a:Lcom/samsung/chord/ChordManager;

    invoke-virtual {v0}, Lcom/samsung/chord/ChordManager;->resetSmartDiscoveryPeriod()V

    .line 1218
    return-void
.end method

.method public a(II)V
    .locals 3

    .prologue
    .line 1191
    iget-object v0, p0, Lcom/samsung/chord/ChordManager$2;->a:Lcom/samsung/chord/ChordManager;

    invoke-static {v0}, Lcom/samsung/chord/ChordManager;->a(Lcom/samsung/chord/ChordManager;)Lcom/samsung/chord/ChordManager$INetworkListener;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/samsung/chord/ChordManager;->a()Landroid/os/Handler;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1192
    invoke-static {}, Lcom/samsung/chord/ChordManager;->a()Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0xd48

    invoke-virtual {v0, v1, p1, p2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    .line 1193
    invoke-static {}, Lcom/samsung/chord/ChordManager;->a()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1196
    :cond_0
    iget-object v0, p0, Lcom/samsung/chord/ChordManager$2;->a:Lcom/samsung/chord/ChordManager;

    invoke-static {v0}, Lcom/samsung/chord/ChordManager;->b(Lcom/samsung/chord/ChordManager;)I

    move-result v0

    if-eq v0, p1, :cond_2

    .line 1213
    :cond_1
    :goto_0
    return-void

    .line 1202
    :cond_2
    const-string v0, "chord_api"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ChordManager : onChangedConnectivity : interface["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/chord/ChordManager$2;->a:Lcom/samsung/chord/ChordManager;

    invoke-static {v2}, Lcom/samsung/chord/ChordManager;->c(Lcom/samsung/chord/ChordManager;)Lcom/samsung/chord/b;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/samsung/chord/b;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] status["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1207
    iget-object v0, p0, Lcom/samsung/chord/ChordManager$2;->a:Lcom/samsung/chord/ChordManager;

    invoke-static {v0}, Lcom/samsung/chord/ChordManager;->d(Lcom/samsung/chord/ChordManager;)Lcom/samsung/android/sdk/chord/ChordAgent;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/samsung/android/sdk/chord/ChordAgent;->setConnectivityState(I)V

    .line 1209
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/samsung/chord/ChordManager$2;->a:Lcom/samsung/chord/ChordManager;

    invoke-static {v1}, Lcom/samsung/chord/ChordManager;->e(Lcom/samsung/chord/ChordManager;)I

    move-result v1

    if-ne v0, v1, :cond_1

    if-nez p2, :cond_1

    .line 1211
    iget-object v0, p0, Lcom/samsung/chord/ChordManager$2;->a:Lcom/samsung/chord/ChordManager;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/samsung/chord/ChordManager;->a(Lcom/samsung/chord/ChordManager;Z)Z

    goto :goto_0
.end method

.method public b()V
    .locals 4

    .prologue
    .line 1222
    const-string v0, "chord_api"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ChordManager : onUpdateWifiLinkProperties : interface["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/chord/ChordManager$2;->a:Lcom/samsung/chord/ChordManager;

    invoke-static {v2}, Lcom/samsung/chord/ChordManager;->c(Lcom/samsung/chord/ChordManager;)Lcom/samsung/chord/b;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/chord/ChordManager$2;->a:Lcom/samsung/chord/ChordManager;

    invoke-static {v3}, Lcom/samsung/chord/ChordManager;->b(Lcom/samsung/chord/ChordManager;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/samsung/chord/b;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] status["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/chord/ChordManager$2;->a:Lcom/samsung/chord/ChordManager;

    invoke-static {v2}, Lcom/samsung/chord/ChordManager;->e(Lcom/samsung/chord/ChordManager;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1226
    iget-object v0, p0, Lcom/samsung/chord/ChordManager$2;->a:Lcom/samsung/chord/ChordManager;

    invoke-static {v0}, Lcom/samsung/chord/ChordManager;->b(Lcom/samsung/chord/ChordManager;)I

    move-result v0

    if-eqz v0, :cond_1

    .line 1242
    :cond_0
    :goto_0
    return-void

    .line 1230
    :cond_1
    iget-object v0, p0, Lcom/samsung/chord/ChordManager$2;->a:Lcom/samsung/chord/ChordManager;

    invoke-static {v0}, Lcom/samsung/chord/ChordManager;->e(Lcom/samsung/chord/ChordManager;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 1234
    iget-object v0, p0, Lcom/samsung/chord/ChordManager$2;->a:Lcom/samsung/chord/ChordManager;

    invoke-static {v0}, Lcom/samsung/chord/ChordManager;->c(Lcom/samsung/chord/ChordManager;)Lcom/samsung/chord/b;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/chord/b;->c(I)Ljava/lang/String;

    move-result-object v0

    .line 1235
    const-string v1, "chord_api"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ChordManager : onUpdateWifiLinkProperties : old["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/chord/ChordManager$2;->a:Lcom/samsung/chord/ChordManager;

    invoke-static {v3}, Lcom/samsung/chord/ChordManager;->f(Lcom/samsung/chord/ChordManager;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] => new["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1237
    iget-object v1, p0, Lcom/samsung/chord/ChordManager$2;->a:Lcom/samsung/chord/ChordManager;

    invoke-static {v1}, Lcom/samsung/chord/ChordManager;->f(Lcom/samsung/chord/ChordManager;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1238
    invoke-static {}, Lcom/samsung/chord/ChordManager;->a()Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0xd49

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 1239
    invoke-static {}, Lcom/samsung/chord/ChordManager;->a()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method
