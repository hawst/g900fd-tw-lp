.class public interface abstract Lcom/samsung/chord/IChordManagerListener;
.super Ljava/lang/Object;
.source "IChordManagerListener.java"


# static fields
.field public static final ERROR_START_FAIL:I = 0x3e8

.field public static final ERROR_UNEXPECTED_STOP:I = 0x3e9

.field public static final NETWORK_DISCONNECTED:I = 0x3eb

.field public static final STARTED_BY_RECONNECTION:I = 0x1

.field public static final STARTED_BY_USER:I = 0x0

.field public static final STOPPED_BY_USER:I = 0x3ea


# virtual methods
.method public abstract onError(I)V
.end method

.method public abstract onNetworkDisconnected()V
.end method

.method public abstract onStarted(Ljava/lang/String;I)V
.end method

.method public abstract onStopped(I)V
.end method
