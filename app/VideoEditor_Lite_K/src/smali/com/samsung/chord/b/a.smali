.class public Lcom/samsung/chord/b/a;
.super Ljava/lang/Thread;
.source "UDPUnicastReceiver.java"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Landroid/content/Context;

.field private c:Landroid/os/PowerManager$WakeLock;

.field private d:Ljava/net/DatagramSocket;

.field private e:Ljava/net/DatagramSocket;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 37
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 25
    const-string v0, "10.0.2.255"

    iput-object v0, p0, Lcom/samsung/chord/b/a;->a:Ljava/lang/String;

    .line 27
    iput-object v1, p0, Lcom/samsung/chord/b/a;->b:Landroid/content/Context;

    .line 29
    iput-object v1, p0, Lcom/samsung/chord/b/a;->c:Landroid/os/PowerManager$WakeLock;

    .line 32
    iput-object v1, p0, Lcom/samsung/chord/b/a;->d:Ljava/net/DatagramSocket;

    .line 35
    iput-object v1, p0, Lcom/samsung/chord/b/a;->e:Ljava/net/DatagramSocket;

    .line 38
    iput-object p1, p0, Lcom/samsung/chord/b/a;->b:Landroid/content/Context;

    .line 41
    :try_start_0
    new-instance v0, Ljava/net/DatagramSocket;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/net/DatagramSocket;-><init>(Ljava/net/SocketAddress;)V

    iput-object v0, p0, Lcom/samsung/chord/b/a;->e:Ljava/net/DatagramSocket;

    .line 42
    iget-object v0, p0, Lcom/samsung/chord/b/a;->e:Ljava/net/DatagramSocket;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/net/DatagramSocket;->setReuseAddress(Z)V

    .line 43
    iget-object v0, p0, Lcom/samsung/chord/b/a;->e:Ljava/net/DatagramSocket;

    new-instance v1, Ljava/net/InetSocketAddress;

    const/16 v2, 0x1c20

    invoke-direct {v1, v2}, Ljava/net/InetSocketAddress;-><init>(I)V

    invoke-virtual {v0, v1}, Ljava/net/DatagramSocket;->bind(Ljava/net/SocketAddress;)V

    .line 46
    new-instance v0, Ljava/net/DatagramSocket;

    invoke-direct {v0}, Ljava/net/DatagramSocket;-><init>()V

    iput-object v0, p0, Lcom/samsung/chord/b/a;->d:Ljava/net/DatagramSocket;

    .line 47
    iget-object v0, p0, Lcom/samsung/chord/b/a;->d:Ljava/net/DatagramSocket;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/net/DatagramSocket;->setReuseAddress(Z)V

    .line 48
    iget-object v0, p0, Lcom/samsung/chord/b/a;->d:Ljava/net/DatagramSocket;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/net/DatagramSocket;->setBroadcast(Z)V
    :try_end_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_0

    .line 58
    :goto_0
    return-void

    .line 54
    :catch_0
    move-exception v0

    .line 55
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception in UDPUnicastReceiver():"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/net/SocketException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private b()V
    .locals 3

    .prologue
    .line 79
    iget-object v0, p0, Lcom/samsung/chord/b/a;->c:Landroid/os/PowerManager$WakeLock;

    if-nez v0, :cond_0

    .line 82
    iget-object v0, p0, Lcom/samsung/chord/b/a;->b:Landroid/content/Context;

    const-string v1, "power"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 84
    const/16 v1, 0x1a

    const-string v2, "Chord Wake Lock"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/chord/b/a;->c:Landroid/os/PowerManager$WakeLock;

    .line 86
    const-string v0, "UDPUnicastReceiver"

    const-string v1, "acqureWakeLock : new"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    :cond_0
    iget-object v0, p0, Lcom/samsung/chord/b/a;->c:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 90
    const-string v0, "UDPUnicastReceiver"

    const-string v1, "acqureWakeLock : already acquire"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    iget-object v0, p0, Lcom/samsung/chord/b/a;->c:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 94
    :cond_1
    const-string v0, "UDPUnicastReceiver"

    const-string v1, "acqureWakeLock : acquire"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    iget-object v0, p0, Lcom/samsung/chord/b/a;->c:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 96
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 99
    iget-object v0, p0, Lcom/samsung/chord/b/a;->c:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/chord/b/a;->c:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 100
    const-string v0, "UDPUnicastReceiver"

    const-string v1, "releaseWakeLock"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    iget-object v0, p0, Lcom/samsung/chord/b/a;->c:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 103
    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 61
    const-string v0, "UDPUnicastReceiver"

    const-string v1, "stopThread for UnicastReceiver"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 63
    :try_start_0
    iget-object v0, p0, Lcom/samsung/chord/b/a;->d:Ljava/net/DatagramSocket;

    if-eqz v0, :cond_0

    .line 64
    iget-object v0, p0, Lcom/samsung/chord/b/a;->d:Ljava/net/DatagramSocket;

    invoke-virtual {v0}, Ljava/net/DatagramSocket;->close()V

    .line 65
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/chord/b/a;->d:Ljava/net/DatagramSocket;

    .line 68
    :cond_0
    iget-object v0, p0, Lcom/samsung/chord/b/a;->e:Ljava/net/DatagramSocket;

    if-eqz v0, :cond_1

    .line 69
    iget-object v0, p0, Lcom/samsung/chord/b/a;->e:Ljava/net/DatagramSocket;

    invoke-virtual {v0}, Ljava/net/DatagramSocket;->close()V

    .line 70
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/chord/b/a;->e:Ljava/net/DatagramSocket;

    .line 72
    :cond_1
    const-string v0, "UDPUnicastReceiver"

    const-string v1, "UDPUnicastReceiver ::mServerSocket and Client Socket  closed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 76
    :goto_0
    return-void

    .line 73
    :catch_0
    move-exception v0

    .line 74
    const-string v0, "UDPUnicastReceiver"

    const-string v1, "Exception while closing UDP mServerSocket and Client Socket "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public run()V
    .locals 5

    .prologue
    .line 107
    invoke-direct {p0}, Lcom/samsung/chord/b/a;->b()V

    .line 108
    const/16 v0, 0x5dc

    new-array v0, v0, [B

    .line 109
    new-instance v1, Ljava/net/DatagramPacket;

    array-length v2, v0

    invoke-direct {v1, v0, v2}, Ljava/net/DatagramPacket;-><init>([BI)V

    .line 111
    :try_start_0
    iget-object v2, p0, Lcom/samsung/chord/b/a;->a:Ljava/lang/String;

    invoke-static {v2}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v2

    .line 113
    const-string v3, "UDPUnicastReceiver"

    const-string v4, "started UDP server to listen discovery packets from HOST_RELAY_SERVER"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    :goto_0
    iget-object v3, p0, Lcom/samsung/chord/b/a;->e:Ljava/net/DatagramSocket;

    invoke-virtual {v3, v1}, Ljava/net/DatagramSocket;->receive(Ljava/net/DatagramPacket;)V

    .line 119
    invoke-virtual {v1, v2}, Ljava/net/DatagramPacket;->setAddress(Ljava/net/InetAddress;)V

    .line 120
    const/16 v3, 0x232e

    invoke-virtual {v1, v3}, Ljava/net/DatagramPacket;->setPort(I)V

    .line 122
    const-string v3, "UDPUnicastReceiver"

    const-string v4, "Broadcast discovery packet inside AVD network at port: 9006"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    iget-object v3, p0, Lcom/samsung/chord/b/a;->d:Ljava/net/DatagramSocket;

    invoke-virtual {v3, v1}, Ljava/net/DatagramSocket;->send(Ljava/net/DatagramPacket;)V

    .line 128
    const/4 v3, 0x0

    invoke-static {v0, v3}, Ljava/util/Arrays;->fill([BB)V

    .line 129
    invoke-virtual {v1}, Ljava/net/DatagramPacket;->getData()[B

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v3, v4}, Ljava/util/Arrays;->fill([BB)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 132
    :catch_0
    move-exception v0

    .line 133
    :try_start_1
    const-string v1, "-----------------EXCEPTION--------------"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 137
    const-string v0, "UDPUnicastReceiver"

    const-string v1, "UDPUnicastREceiver::socket closer & clenaup for UDP Server and Client port"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    :try_start_2
    iget-object v0, p0, Lcom/samsung/chord/b/a;->d:Ljava/net/DatagramSocket;

    if-eqz v0, :cond_0

    .line 140
    iget-object v0, p0, Lcom/samsung/chord/b/a;->d:Ljava/net/DatagramSocket;

    invoke-virtual {v0}, Ljava/net/DatagramSocket;->close()V

    .line 141
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/chord/b/a;->d:Ljava/net/DatagramSocket;

    .line 143
    :cond_0
    iget-object v0, p0, Lcom/samsung/chord/b/a;->e:Ljava/net/DatagramSocket;

    if-eqz v0, :cond_1

    .line 144
    iget-object v0, p0, Lcom/samsung/chord/b/a;->e:Ljava/net/DatagramSocket;

    invoke-virtual {v0}, Ljava/net/DatagramSocket;->close()V

    .line 145
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/chord/b/a;->e:Ljava/net/DatagramSocket;

    .line 150
    :cond_1
    invoke-direct {p0}, Lcom/samsung/chord/b/a;->c()V

    .line 151
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/chord/b/a;->c:Landroid/os/PowerManager$WakeLock;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 160
    :goto_1
    return-void

    .line 137
    :catchall_0
    move-exception v0

    const-string v1, "UDPUnicastReceiver"

    const-string v2, "UDPUnicastREceiver::socket closer & clenaup for UDP Server and Client port"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    :try_start_3
    iget-object v1, p0, Lcom/samsung/chord/b/a;->d:Ljava/net/DatagramSocket;

    if-eqz v1, :cond_2

    .line 140
    iget-object v1, p0, Lcom/samsung/chord/b/a;->d:Ljava/net/DatagramSocket;

    invoke-virtual {v1}, Ljava/net/DatagramSocket;->close()V

    .line 141
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/chord/b/a;->d:Ljava/net/DatagramSocket;

    .line 143
    :cond_2
    iget-object v1, p0, Lcom/samsung/chord/b/a;->e:Ljava/net/DatagramSocket;

    if-eqz v1, :cond_3

    .line 144
    iget-object v1, p0, Lcom/samsung/chord/b/a;->e:Ljava/net/DatagramSocket;

    invoke-virtual {v1}, Ljava/net/DatagramSocket;->close()V

    .line 145
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/chord/b/a;->e:Ljava/net/DatagramSocket;

    .line 150
    :cond_3
    invoke-direct {p0}, Lcom/samsung/chord/b/a;->c()V

    .line 151
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/chord/b/a;->c:Landroid/os/PowerManager$WakeLock;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    .line 137
    :goto_2
    throw v0

    .line 152
    :catch_1
    move-exception v1

    .line 153
    const-string v2, "UDPUnicastReceiver"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "UDPUnicastREceiver::Exception while closing socket for or UDP Server and Client port"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 152
    :catch_2
    move-exception v0

    .line 153
    const-string v1, "UDPUnicastReceiver"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "UDPUnicastREceiver::Exception while closing socket for or UDP Server and Client port"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
