.class public Lcom/sec/tcpdumpservice/TcpDumpService;
.super Landroid/app/Service;
.source "TcpDumpService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/tcpdumpservice/TcpDumpService$OemCommands;
    }
.end annotation


# static fields
.field private static LOG_TAG:Ljava/lang/String;

.field private static bindservice_flag:I


# instance fields
.field private TCPDUMP_INTERFACE:Ljava/lang/String;

.field public mHandler:Landroid/os/Handler;

.field private mOem:Lcom/sec/tcpdumpservice/TcpDumpService$OemCommands;

.field private mSecPhoneServiceConnection:Landroid/content/ServiceConnection;

.field private mServiceMessenger:Landroid/os/Messenger;

.field private mSvcModeMessenger:Landroid/os/Messenger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-string v0, "TcpDumpService"

    sput-object v0, Lcom/sec/tcpdumpservice/TcpDumpService;->LOG_TAG:Ljava/lang/String;

    .line 25
    const/4 v0, 0x0

    sput v0, Lcom/sec/tcpdumpservice/TcpDumpService;->bindservice_flag:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 21
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 24
    iput-object v0, p0, Lcom/sec/tcpdumpservice/TcpDumpService;->mServiceMessenger:Landroid/os/Messenger;

    .line 27
    iput-object v0, p0, Lcom/sec/tcpdumpservice/TcpDumpService;->mOem:Lcom/sec/tcpdumpservice/TcpDumpService$OemCommands;

    .line 32
    const-string v0, "any"

    iput-object v0, p0, Lcom/sec/tcpdumpservice/TcpDumpService;->TCPDUMP_INTERFACE:Ljava/lang/String;

    .line 34
    new-instance v0, Lcom/sec/tcpdumpservice/TcpDumpService$1;

    invoke-direct {v0, p0}, Lcom/sec/tcpdumpservice/TcpDumpService$1;-><init>(Lcom/sec/tcpdumpservice/TcpDumpService;)V

    iput-object v0, p0, Lcom/sec/tcpdumpservice/TcpDumpService;->mHandler:Landroid/os/Handler;

    .line 57
    new-instance v0, Landroid/os/Messenger;

    iget-object v1, p0, Lcom/sec/tcpdumpservice/TcpDumpService;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/tcpdumpservice/TcpDumpService;->mSvcModeMessenger:Landroid/os/Messenger;

    .line 114
    new-instance v0, Lcom/sec/tcpdumpservice/TcpDumpService$2;

    invoke-direct {v0, p0}, Lcom/sec/tcpdumpservice/TcpDumpService$2;-><init>(Lcom/sec/tcpdumpservice/TcpDumpService;)V

    iput-object v0, p0, Lcom/sec/tcpdumpservice/TcpDumpService;->mSecPhoneServiceConnection:Landroid/content/ServiceConnection;

    return-void
.end method

.method private SendData(I)V
    .locals 3
    .param p1, "cmd"    # I

    .prologue
    .line 96
    const/4 v0, 0x0

    .line 98
    .local v0, "data":[B
    iget-object v1, p0, Lcom/sec/tcpdumpservice/TcpDumpService;->mOem:Lcom/sec/tcpdumpservice/TcpDumpService$OemCommands;

    invoke-virtual {v1, p1}, Lcom/sec/tcpdumpservice/TcpDumpService$OemCommands;->StartSysDumpData(I)[B

    move-result-object v0

    .line 100
    if-nez v0, :cond_1

    .line 101
    sget-object v1, Lcom/sec/tcpdumpservice/TcpDumpService;->LOG_TAG:Ljava/lang/String;

    const-string v2, " err - data is NULL"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    :cond_0
    :goto_0
    return-void

    .line 104
    :cond_1
    sget-object v1, Lcom/sec/tcpdumpservice/TcpDumpService;->LOG_TAG:Ljava/lang/String;

    const-string v2, "in SendData()"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    iget-object v1, p0, Lcom/sec/tcpdumpservice/TcpDumpService;->mOem:Lcom/sec/tcpdumpservice/TcpDumpService$OemCommands;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v1, 0x19

    if-ne p1, v1, :cond_2

    .line 106
    iget-object v1, p0, Lcom/sec/tcpdumpservice/TcpDumpService;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x3f1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/sec/tcpdumpservice/TcpDumpService;->invokeOemRilRequestRaw([BLandroid/os/Message;)V

    goto :goto_0

    .line 107
    :cond_2
    iget-object v1, p0, Lcom/sec/tcpdumpservice/TcpDumpService;->mOem:Lcom/sec/tcpdumpservice/TcpDumpService$OemCommands;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v1, 0x18

    if-ne p1, v1, :cond_0

    .line 108
    iget-object v1, p0, Lcom/sec/tcpdumpservice/TcpDumpService;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x3f9

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/sec/tcpdumpservice/TcpDumpService;->invokeOemRilRequestRaw([BLandroid/os/Message;)V

    .line 109
    sget-object v1, Lcom/sec/tcpdumpservice/TcpDumpService;->LOG_TAG:Ljava/lang/String;

    const-string v2, "SendData() after invokeOemRilRequestRaw"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/sec/tcpdumpservice/TcpDumpService;->LOG_TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/tcpdumpservice/TcpDumpService;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/tcpdumpservice/TcpDumpService;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/tcpdumpservice/TcpDumpService;->TCPDUMP_INTERFACE:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/tcpdumpservice/TcpDumpService;Landroid/os/Messenger;)Landroid/os/Messenger;
    .locals 0
    .param p0, "x0"    # Lcom/sec/tcpdumpservice/TcpDumpService;
    .param p1, "x1"    # Landroid/os/Messenger;

    .prologue
    .line 21
    iput-object p1, p0, Lcom/sec/tcpdumpservice/TcpDumpService;->mServiceMessenger:Landroid/os/Messenger;

    return-object p1
.end method

.method static synthetic access$302(I)I
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 21
    sput p0, Lcom/sec/tcpdumpservice/TcpDumpService;->bindservice_flag:I

    return p0
.end method

.method static synthetic access$400(Lcom/sec/tcpdumpservice/TcpDumpService;)Lcom/sec/tcpdumpservice/TcpDumpService$OemCommands;
    .locals 1
    .param p0, "x0"    # Lcom/sec/tcpdumpservice/TcpDumpService;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/tcpdumpservice/TcpDumpService;->mOem:Lcom/sec/tcpdumpservice/TcpDumpService$OemCommands;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/tcpdumpservice/TcpDumpService;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/tcpdumpservice/TcpDumpService;
    .param p1, "x1"    # I

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcom/sec/tcpdumpservice/TcpDumpService;->SendData(I)V

    return-void
.end method

.method private connectToRilService()V
    .locals 3

    .prologue
    .line 129
    sget-object v1, Lcom/sec/tcpdumpservice/TcpDumpService;->LOG_TAG:Ljava/lang/String;

    const-string v2, "connect To Ril service"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 130
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 131
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.sec.phone"

    const-string v2, "com.sec.phone.SecPhoneService"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 132
    iget-object v1, p0, Lcom/sec/tcpdumpservice/TcpDumpService;->mSecPhoneServiceConnection:Landroid/content/ServiceConnection;

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/tcpdumpservice/TcpDumpService;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 133
    return-void
.end method

.method private invokeOemRilRequestRaw([BLandroid/os/Message;)V
    .locals 6
    .param p1, "data"    # [B
    .param p2, "response"    # Landroid/os/Message;

    .prologue
    .line 136
    invoke-virtual {p2}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    .line 137
    .local v2, "req":Landroid/os/Bundle;
    const-string v3, "request"

    invoke-virtual {v2, v3, p1}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 139
    invoke-virtual {p2, v2}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 140
    iget-object v3, p0, Lcom/sec/tcpdumpservice/TcpDumpService;->mSvcModeMessenger:Landroid/os/Messenger;

    iput-object v3, p2, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    .line 142
    const/4 v0, 0x0

    .line 144
    .local v0, "cnt":I
    const/4 v0, 0x0

    :goto_0
    const/16 v3, 0xa

    if-ge v0, v3, :cond_0

    .line 145
    :try_start_0
    sget v3, Lcom/sec/tcpdumpservice/TcpDumpService;->bindservice_flag:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_1

    .line 146
    iget-object v3, p0, Lcom/sec/tcpdumpservice/TcpDumpService;->mServiceMessenger:Landroid/os/Messenger;

    if-eqz v3, :cond_0

    .line 147
    iget-object v3, p0, Lcom/sec/tcpdumpservice/TcpDumpService;->mServiceMessenger:Landroid/os/Messenger;

    invoke-virtual {v3, p2}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 161
    :cond_0
    :goto_1
    return-void

    .line 152
    :cond_1
    :try_start_1
    sget-object v3, Lcom/sec/tcpdumpservice/TcpDumpService;->LOG_TAG:Ljava/lang/String;

    const-string v4, "mServiceMessenger is NULL"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    const-wide/16 v4, 0xc8

    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    .line 144
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 158
    :catch_0
    move-exception v1

    .line 159
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v3, Lcom/sec/tcpdumpservice/TcpDumpService;->LOG_TAG:Ljava/lang/String;

    const-string v4, "invokeOemRilRequestRaw() exception"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 154
    .end local v1    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v3

    goto :goto_2
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 196
    sget-object v0, Lcom/sec/tcpdumpservice/TcpDumpService;->LOG_TAG:Ljava/lang/String;

    const-string v1, "onBind()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 165
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 166
    sget-object v0, Lcom/sec/tcpdumpservice/TcpDumpService;->LOG_TAG:Ljava/lang/String;

    const-string v1, "onCreate()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 168
    const/4 v0, 0x0

    sput v0, Lcom/sec/tcpdumpservice/TcpDumpService;->bindservice_flag:I

    .line 169
    new-instance v0, Lcom/sec/tcpdumpservice/TcpDumpService$OemCommands;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/tcpdumpservice/TcpDumpService$OemCommands;-><init>(Lcom/sec/tcpdumpservice/TcpDumpService;Lcom/sec/tcpdumpservice/TcpDumpService$1;)V

    iput-object v0, p0, Lcom/sec/tcpdumpservice/TcpDumpService;->mOem:Lcom/sec/tcpdumpservice/TcpDumpService$OemCommands;

    .line 170
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/tcpdumpservice/TcpDumpService;->mOem:Lcom/sec/tcpdumpservice/TcpDumpService$OemCommands;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v0, 0x19

    invoke-direct {p0, v0}, Lcom/sec/tcpdumpservice/TcpDumpService;->SendData(I)V

    .line 175
    iget-object v0, p0, Lcom/sec/tcpdumpservice/TcpDumpService;->mServiceMessenger:Landroid/os/Messenger;

    if-eqz v0, :cond_0

    .line 176
    iget-object v0, p0, Lcom/sec/tcpdumpservice/TcpDumpService;->mSecPhoneServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0}, Lcom/sec/tcpdumpservice/TcpDumpService;->unbindService(Landroid/content/ServiceConnection;)V

    .line 180
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/tcpdumpservice/TcpDumpService;->mSecPhoneServiceConnection:Landroid/content/ServiceConnection;

    .line 181
    const/4 v0, 0x0

    sput v0, Lcom/sec/tcpdumpservice/TcpDumpService;->bindservice_flag:I

    .line 182
    sget-object v0, Lcom/sec/tcpdumpservice/TcpDumpService;->LOG_TAG:Ljava/lang/String;

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 184
    return-void

    .line 178
    :cond_0
    sget-object v0, Lcom/sec/tcpdumpservice/TcpDumpService;->LOG_TAG:Ljava/lang/String;

    const-string v1, "mSecPhoneServiceConnection is disconnected already."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 188
    invoke-static {}, Lcom/sec/tcpdumpservice/TcpDumpReceiver;->isNotUserScenario()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 189
    invoke-direct {p0}, Lcom/sec/tcpdumpservice/TcpDumpService;->connectToRilService()V

    .line 191
    :cond_0
    const/4 v0, 0x2

    return v0
.end method
