.class Lcom/sec/tcpdumpservice/TcpDumpService$1;
.super Landroid/os/Handler;
.source "TcpDumpService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/tcpdumpservice/TcpDumpService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/tcpdumpservice/TcpDumpService;


# direct methods
.method constructor <init>(Lcom/sec/tcpdumpservice/TcpDumpService;)V
    .locals 0

    .prologue
    .line 34
    iput-object p1, p0, Lcom/sec/tcpdumpservice/TcpDumpService$1;->this$0:Lcom/sec/tcpdumpservice/TcpDumpService;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 36
    iget v1, p1, Landroid/os/Message;->what:I

    sparse-switch v1, :sswitch_data_0

    .line 53
    :goto_0
    # getter for: Lcom/sec/tcpdumpservice/TcpDumpService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/tcpdumpservice/TcpDumpService;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleMessage() : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Landroid/os/Message;->what:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 54
    return-void

    .line 38
    :sswitch_0
    # getter for: Lcom/sec/tcpdumpservice/TcpDumpService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/tcpdumpservice/TcpDumpService;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Auto tcpdump Success"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 42
    :sswitch_1
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "error"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 43
    .local v0, "error":I
    if-nez v0, :cond_0

    .line 44
    # getter for: Lcom/sec/tcpdumpservice/TcpDumpService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/tcpdumpservice/TcpDumpService;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "QUERY_TCP_DUMP_DONE Success"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 46
    :cond_0
    # getter for: Lcom/sec/tcpdumpservice/TcpDumpService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/tcpdumpservice/TcpDumpService;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "QUERY_TCP_DUMP_DONE fail"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 36
    :sswitch_data_0
    .sparse-switch
        0x3f1 -> :sswitch_0
        0x3f9 -> :sswitch_1
    .end sparse-switch
.end method
