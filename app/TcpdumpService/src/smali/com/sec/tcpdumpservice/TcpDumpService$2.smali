.class Lcom/sec/tcpdumpservice/TcpDumpService$2;
.super Ljava/lang/Object;
.source "TcpDumpService.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/tcpdumpservice/TcpDumpService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/tcpdumpservice/TcpDumpService;


# direct methods
.method constructor <init>(Lcom/sec/tcpdumpservice/TcpDumpService;)V
    .locals 0

    .prologue
    .line 114
    iput-object p1, p0, Lcom/sec/tcpdumpservice/TcpDumpService$2;->this$0:Lcom/sec/tcpdumpservice/TcpDumpService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1, "className"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 116
    # getter for: Lcom/sec/tcpdumpservice/TcpDumpService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/tcpdumpservice/TcpDumpService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onServiceConnected()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    iget-object v0, p0, Lcom/sec/tcpdumpservice/TcpDumpService$2;->this$0:Lcom/sec/tcpdumpservice/TcpDumpService;

    new-instance v1, Landroid/os/Messenger;

    invoke-direct {v1, p2}, Landroid/os/Messenger;-><init>(Landroid/os/IBinder;)V

    # setter for: Lcom/sec/tcpdumpservice/TcpDumpService;->mServiceMessenger:Landroid/os/Messenger;
    invoke-static {v0, v1}, Lcom/sec/tcpdumpservice/TcpDumpService;->access$202(Lcom/sec/tcpdumpservice/TcpDumpService;Landroid/os/Messenger;)Landroid/os/Messenger;

    .line 118
    const/4 v0, 0x1

    # setter for: Lcom/sec/tcpdumpservice/TcpDumpService;->bindservice_flag:I
    invoke-static {v0}, Lcom/sec/tcpdumpservice/TcpDumpService;->access$302(I)I

    .line 119
    iget-object v0, p0, Lcom/sec/tcpdumpservice/TcpDumpService$2;->this$0:Lcom/sec/tcpdumpservice/TcpDumpService;

    iget-object v1, p0, Lcom/sec/tcpdumpservice/TcpDumpService$2;->this$0:Lcom/sec/tcpdumpservice/TcpDumpService;

    # getter for: Lcom/sec/tcpdumpservice/TcpDumpService;->mOem:Lcom/sec/tcpdumpservice/TcpDumpService$OemCommands;
    invoke-static {v1}, Lcom/sec/tcpdumpservice/TcpDumpService;->access$400(Lcom/sec/tcpdumpservice/TcpDumpService;)Lcom/sec/tcpdumpservice/TcpDumpService$OemCommands;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v1, 0x18

    # invokes: Lcom/sec/tcpdumpservice/TcpDumpService;->SendData(I)V
    invoke-static {v0, v1}, Lcom/sec/tcpdumpservice/TcpDumpService;->access$500(Lcom/sec/tcpdumpservice/TcpDumpService;I)V

    .line 120
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "className"    # Landroid/content/ComponentName;

    .prologue
    .line 123
    # getter for: Lcom/sec/tcpdumpservice/TcpDumpService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/tcpdumpservice/TcpDumpService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onServiceDisconnected()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    iget-object v0, p0, Lcom/sec/tcpdumpservice/TcpDumpService$2;->this$0:Lcom/sec/tcpdumpservice/TcpDumpService;

    const/4 v1, 0x0

    # setter for: Lcom/sec/tcpdumpservice/TcpDumpService;->mServiceMessenger:Landroid/os/Messenger;
    invoke-static {v0, v1}, Lcom/sec/tcpdumpservice/TcpDumpService;->access$202(Lcom/sec/tcpdumpservice/TcpDumpService;Landroid/os/Messenger;)Landroid/os/Messenger;

    .line 125
    return-void
.end method
