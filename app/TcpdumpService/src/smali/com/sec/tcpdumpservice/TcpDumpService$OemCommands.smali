.class Lcom/sec/tcpdumpservice/TcpDumpService$OemCommands;
.super Ljava/lang/Object;
.source "TcpDumpService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/tcpdumpservice/TcpDumpService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OemCommands"
.end annotation


# instance fields
.field final OEM_AUTO_TCPDUMP_START:I

.field final OEM_AUTO_TCPDUMP_STOP:I

.field final OEM_SYSDUMP_FUNCTAG:I

.field final synthetic this$0:Lcom/sec/tcpdumpservice/TcpDumpService;


# direct methods
.method private constructor <init>(Lcom/sec/tcpdumpservice/TcpDumpService;)V
    .locals 1

    .prologue
    .line 59
    iput-object p1, p0, Lcom/sec/tcpdumpservice/TcpDumpService$OemCommands;->this$0:Lcom/sec/tcpdumpservice/TcpDumpService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    const/4 v0, 0x7

    iput v0, p0, Lcom/sec/tcpdumpservice/TcpDumpService$OemCommands;->OEM_SYSDUMP_FUNCTAG:I

    .line 61
    const/16 v0, 0x18

    iput v0, p0, Lcom/sec/tcpdumpservice/TcpDumpService$OemCommands;->OEM_AUTO_TCPDUMP_START:I

    .line 62
    const/16 v0, 0x19

    iput v0, p0, Lcom/sec/tcpdumpservice/TcpDumpService$OemCommands;->OEM_AUTO_TCPDUMP_STOP:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/tcpdumpservice/TcpDumpService;Lcom/sec/tcpdumpservice/TcpDumpService$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/tcpdumpservice/TcpDumpService;
    .param p2, "x1"    # Lcom/sec/tcpdumpservice/TcpDumpService$1;

    .prologue
    .line 59
    invoke-direct {p0, p1}, Lcom/sec/tcpdumpservice/TcpDumpService$OemCommands;-><init>(Lcom/sec/tcpdumpservice/TcpDumpService;)V

    return-void
.end method


# virtual methods
.method StartSysDumpData(I)[B
    .locals 8
    .param p1, "cmd"    # I

    .prologue
    .line 65
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 66
    .local v0, "bos":Ljava/io/ByteArrayOutputStream;
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 69
    .local v1, "dos":Ljava/io/DataOutputStream;
    const/4 v5, 0x7

    :try_start_0
    invoke-virtual {v1, v5}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 70
    invoke-virtual {v1, p1}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 72
    # getter for: Lcom/sec/tcpdumpservice/TcpDumpService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/tcpdumpservice/TcpDumpService;->access$000()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "cmd : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 74
    const/16 v5, 0x18

    if-ne p1, v5, :cond_0

    .line 76
    iget-object v5, p0, Lcom/sec/tcpdumpservice/TcpDumpService$OemCommands;->this$0:Lcom/sec/tcpdumpservice/TcpDumpService;

    # getter for: Lcom/sec/tcpdumpservice/TcpDumpService;->TCPDUMP_INTERFACE:Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/tcpdumpservice/TcpDumpService;->access$100(Lcom/sec/tcpdumpservice/TcpDumpService;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    .line 77
    .local v4, "tcpdump_interface_byte":[B
    # getter for: Lcom/sec/tcpdumpservice/TcpDumpService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/tcpdumpservice/TcpDumpService;->access$000()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "dos.writeByte length: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    array-length v7, v4

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    array-length v5, v4

    add-int/lit8 v5, v5, 0x4

    invoke-virtual {v1, v5}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 81
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v5, v4

    if-ge v3, v5, :cond_1

    .line 82
    aget-byte v5, v4, v3

    invoke-virtual {v1, v5}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 81
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 84
    .end local v3    # "i":I
    .end local v4    # "tcpdump_interface_byte":[B
    :cond_0
    # getter for: Lcom/sec/tcpdumpservice/TcpDumpService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/tcpdumpservice/TcpDumpService;->access$000()Ljava/lang/String;

    move-result-object v5

    const-string v6, "dos.writeByte(4)"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    const/4 v5, 0x4

    invoke-virtual {v1, v5}, Ljava/io/DataOutputStream;->writeShort(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 91
    :cond_1
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v5

    :goto_1
    return-object v5

    .line 87
    :catch_0
    move-exception v2

    .line 88
    .local v2, "e":Ljava/io/IOException;
    # getter for: Lcom/sec/tcpdumpservice/TcpDumpService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/tcpdumpservice/TcpDumpService;->access$000()Ljava/lang/String;

    move-result-object v5

    const-string v6, "IOException in getServMQueryData!!!"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    const/4 v5, 0x0

    goto :goto_1
.end method
