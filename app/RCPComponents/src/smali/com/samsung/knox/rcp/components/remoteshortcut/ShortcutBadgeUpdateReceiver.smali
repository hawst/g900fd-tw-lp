.class public Lcom/samsung/knox/rcp/components/remoteshortcut/ShortcutBadgeUpdateReceiver;
.super Landroid/content/BroadcastReceiver;
.source "ShortcutBadgeUpdateReceiver.java"


# instance fields
.field private final ACTION_BADGE_COUNT_UPDATE:Ljava/lang/String;

.field private TAG:Ljava/lang/String;

.field aliasActivityPrefix:Ljava/lang/String;

.field private aliasPackageName:Ljava/lang/String;

.field private badgeCount:[I

.field private final bundleToFetch:Ljava/lang/String;

.field private clsName:[Ljava/lang/String;

.field private pkgname:[Ljava/lang/String;

.field private userId:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 12
    const-string v0, "com.sec.knox.action.badge_update"

    iput-object v0, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/ShortcutBadgeUpdateReceiver;->ACTION_BADGE_COUNT_UPDATE:Ljava/lang/String;

    .line 13
    const-string v0, "com.sec.knox.bridge.BadgeData"

    iput-object v0, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/ShortcutBadgeUpdateReceiver;->bundleToFetch:Ljava/lang/String;

    .line 14
    iput-object v1, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/ShortcutBadgeUpdateReceiver;->pkgname:[Ljava/lang/String;

    .line 15
    iput-object v1, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/ShortcutBadgeUpdateReceiver;->clsName:[Ljava/lang/String;

    .line 16
    iput-object v1, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/ShortcutBadgeUpdateReceiver;->badgeCount:[I

    .line 17
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/ShortcutBadgeUpdateReceiver;->userId:I

    .line 18
    iput-object v1, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/ShortcutBadgeUpdateReceiver;->aliasPackageName:Ljava/lang/String;

    .line 19
    const-string v0, "Activity"

    iput-object v0, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/ShortcutBadgeUpdateReceiver;->aliasActivityPrefix:Ljava/lang/String;

    .line 21
    const-string v0, "ShortcutBadgeUpdateReceiver"

    iput-object v0, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/ShortcutBadgeUpdateReceiver;->TAG:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 26
    const/4 v0, 0x0

    .line 28
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v6, "com.sec.knox.action.badge_update"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 29
    iget-object v6, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/ShortcutBadgeUpdateReceiver;->TAG:Ljava/lang/String;

    const-string v7, "onReceive"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 31
    const-string v6, "com.sec.knox.bridge.BadgeData"

    invoke-virtual {p2, v6}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 33
    if-eqz v0, :cond_3

    .line 34
    iget-object v6, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/ShortcutBadgeUpdateReceiver;->TAG:Ljava/lang/String;

    const-string v7, "Bundle not null"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 36
    const-string v6, "pkg_names"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/ShortcutBadgeUpdateReceiver;->pkgname:[Ljava/lang/String;

    .line 37
    const-string v6, "class_names"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/ShortcutBadgeUpdateReceiver;->clsName:[Ljava/lang/String;

    .line 38
    const-string v6, "counts"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/ShortcutBadgeUpdateReceiver;->badgeCount:[I

    .line 39
    const-string v6, "UserId"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    iput v6, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/ShortcutBadgeUpdateReceiver;->userId:I

    .line 41
    iget-object v6, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/ShortcutBadgeUpdateReceiver;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "userID received "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/ShortcutBadgeUpdateReceiver;->userId:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 43
    iget-object v6, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/ShortcutBadgeUpdateReceiver;->pkgname:[Ljava/lang/String;

    array-length v1, v6

    .line 44
    .local v1, "count":I
    const/4 v2, 0x0

    .line 45
    .local v2, "dummyActivity":Ljava/lang/String;
    invoke-static {p1}, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;->getInstance(Landroid/content/Context;)Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;

    move-result-object v4

    .line 47
    .local v4, "mActivityMap":Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;
    iget-object v6, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/ShortcutBadgeUpdateReceiver;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, " count of shortcut badges"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 48
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/ShortcutBadgeUpdateReceiver;->aliasPackageName:Ljava/lang/String;

    .line 49
    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    .line 51
    .local v5, "updateBadgeShortcutIntent":Landroid/content/Intent;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v1, :cond_3

    .line 53
    if-eqz v4, :cond_0

    .line 54
    iget-object v6, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/ShortcutBadgeUpdateReceiver;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Real class name "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/ShortcutBadgeUpdateReceiver;->clsName:[Ljava/lang/String;

    aget-object v8, v8, v3

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    iget v6, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/ShortcutBadgeUpdateReceiver;->userId:I

    iget-object v7, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/ShortcutBadgeUpdateReceiver;->clsName:[Ljava/lang/String;

    aget-object v7, v7, v3

    invoke-virtual {v4, v6, v7}, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;->getVirtualActivityName(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 57
    :cond_0
    if-eqz v2, :cond_1

    .line 58
    iget-object v6, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/ShortcutBadgeUpdateReceiver;->aliasActivityPrefix:Ljava/lang/String;

    invoke-virtual {v2, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 59
    iget-object v6, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/ShortcutBadgeUpdateReceiver;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, " Dummy activity retreived from map "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    iget-object v6, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/ShortcutBadgeUpdateReceiver;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Badge count "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/ShortcutBadgeUpdateReceiver;->badgeCount:[I

    aget v8, v8, v3

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " its alias package "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/ShortcutBadgeUpdateReceiver;->aliasPackageName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " its alias class "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/ShortcutBadgeUpdateReceiver;->aliasPackageName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 64
    const-string v6, "badge_count"

    iget-object v7, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/ShortcutBadgeUpdateReceiver;->badgeCount:[I

    aget v7, v7, v3

    invoke-virtual {v5, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 65
    const-string v6, "badge_count_package_name"

    iget-object v7, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/ShortcutBadgeUpdateReceiver;->aliasPackageName:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 67
    const-string v6, "badge_count_class_name"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/ShortcutBadgeUpdateReceiver;->aliasPackageName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 69
    const-string v6, "android.intent.action.BADGE_COUNT_UPDATE"

    invoke-virtual {v5, v6}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 71
    invoke-virtual {p1, v5}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 51
    :cond_1
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    .line 73
    :cond_2
    iget-object v6, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/ShortcutBadgeUpdateReceiver;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, " Dummy activity retreived from map "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 82
    .end local v1    # "count":I
    .end local v2    # "dummyActivity":Ljava/lang/String;
    .end local v3    # "i":I
    .end local v4    # "mActivityMap":Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;
    .end local v5    # "updateBadgeShortcutIntent":Landroid/content/Intent;
    :cond_3
    return-void
.end method
