.class public Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;
.super Ljava/lang/Object;
.source "ActivityMap.java"


# static fields
.field private static mActivityMap:Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;

.field private static mContext:Landroid/content/Context;


# instance fields
.field private mAvailList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    invoke-direct {p0}, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;->loadInitPool()V

    .line 37
    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 41
    sput-object p0, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;->mContext:Landroid/content/Context;

    .line 42
    sget-object v0, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;->mActivityMap:Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;

    if-nez v0, :cond_0

    .line 43
    new-instance v0, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;

    invoke-direct {v0}, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;-><init>()V

    sput-object v0, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;->mActivityMap:Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;

    .line 44
    :cond_0
    sget-object v0, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;->mActivityMap:Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;

    return-object v0
.end method

.method private getMappedVirtualActivity()Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 169
    const/4 v0, 0x0

    .line 170
    .local v0, "virtualActivityName":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;->mAvailList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 171
    iget-object v1, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;->mAvailList:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "virtualActivityName":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .line 172
    .restart local v0    # "virtualActivityName":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;->mAvailList:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 173
    invoke-direct {p0}, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;->saveAvailableActivitys()V

    .line 176
    :cond_0
    return-object v0
.end method

.method private getPreference(Ljava/lang/String;)Landroid/content/SharedPreferences;
    .locals 3
    .param p1, "preferenceType"    # Ljava/lang/String;

    .prologue
    .line 146
    sget-object v1, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;->mContext:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 147
    .local v0, "prefs":Landroid/content/SharedPreferences;
    return-object v0
.end method

.method private loadInitPool()V
    .locals 5

    .prologue
    .line 155
    const-string v2, "ActivityPool"

    invoke-direct {p0, v2}, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;->getPreference(Ljava/lang/String;)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v3, "ActivityList"

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 156
    .local v0, "availActivities":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 157
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;->mAvailList:Ljava/util/ArrayList;

    .line 158
    const/4 v1, 0x1

    .local v1, "index":I
    :goto_0
    const/16 v2, 0x32

    if-gt v1, v2, :cond_0

    .line 159
    iget-object v2, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;->mAvailList:Ljava/util/ArrayList;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Activity"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 158
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 161
    :cond_0
    invoke-direct {p0}, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;->saveAvailableActivitys()V

    .line 166
    .end local v1    # "index":I
    :goto_1
    return-void

    .line 164
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    const-string v3, ";"

    invoke-static {v0, v3}, Landroid/text/TextUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;->mAvailList:Ljava/util/ArrayList;

    goto :goto_1
.end method

.method private saveAvailableActivitys()V
    .locals 4

    .prologue
    .line 151
    const-string v0, "ActivityPool"

    invoke-direct {p0, v0}, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;->getPreference(Ljava/lang/String;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "ActivityList"

    const-string v2, ";"

    iget-object v3, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;->mAvailList:Ljava/util/ArrayList;

    invoke-static {v2, v3}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 152
    return-void
.end method


# virtual methods
.method public checkIfShortcutIsMade(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 5
    .param p1, "personaId"    # I
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "realActivityName"    # Ljava/lang/String;
    .param p4, "lable"    # Ljava/lang/String;

    .prologue
    .line 217
    const-string v4, "ShortcutMap"

    invoke-direct {p0, v4}, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;->getPreference(Ljava/lang/String;)Landroid/content/SharedPreferences;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v0

    .line 218
    .local v0, "activityMaps":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;*>;"
    if-eqz v0, :cond_1

    .line 219
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 220
    .local v1, "entry":Ljava/util/Map$Entry;
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;->getVirtualActivityInfoFromString(Ljava/lang/String;)Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;

    move-result-object v3

    .line 221
    .local v3, "virtualActivityMap":Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;
    invoke-virtual {v3}, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;->getClassName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3}, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;->getPersonaId()I

    move-result v4

    if-ne v4, p1, :cond_0

    .line 223
    const/4 v4, 0x1

    .line 226
    .end local v1    # "entry":Ljava/util/Map$Entry;
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "virtualActivityMap":Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;
    :goto_0
    return v4

    :cond_1
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public getRealActivity(Ljava/lang/String;)Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;
    .locals 3
    .param p1, "virtualActivityName"    # Ljava/lang/String;

    .prologue
    .line 48
    const-string v1, "ShortcutMap"

    invoke-direct {p0, v1}, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;->getPreference(Ljava/lang/String;)Landroid/content/SharedPreferences;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, p1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 49
    .local v0, "activityMapName":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;->getVirtualActivityInfoFromString(Ljava/lang/String;)Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;

    move-result-object v1

    return-object v1
.end method

.method public getStringFromVirtualActivityInfo(Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;)Ljava/lang/String;
    .locals 5
    .param p1, "activityInfo"    # Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;

    .prologue
    .line 195
    const/4 v0, 0x0

    .line 196
    .local v0, "activityInfoString":Ljava/lang/String;
    if-nez p1, :cond_0

    .line 197
    const/4 v3, 0x0

    .line 210
    :goto_0
    return-object v3

    .line 200
    :cond_0
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 201
    .local v2, "object":Lorg/json/JSONObject;
    const-string v3, "personaId"

    invoke-virtual {p1}, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;->getPersonaId()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 202
    const-string v3, "packageName"

    invoke-virtual {p1}, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 203
    const-string v3, "className"

    invoke-virtual {p1}, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;->getClassName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 204
    const-string v3, "label"

    invoke-virtual {p1}, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;->getLable()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 205
    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .end local v2    # "object":Lorg/json/JSONObject;
    :goto_1
    move-object v3, v0

    .line 210
    goto :goto_0

    .line 206
    :catch_0
    move-exception v1

    .line 207
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_1
.end method

.method public getVirtualActivityInfoFromString(Ljava/lang/String;)Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;
    .locals 8
    .param p1, "activityInfoAsString"    # Ljava/lang/String;

    .prologue
    .line 180
    const/4 v0, 0x0

    .line 181
    .local v0, "activityInfo":Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;
    if-nez p1, :cond_0

    .line 182
    const/4 v4, 0x0

    .line 191
    :goto_0
    return-object v4

    .line 185
    :cond_0
    :try_start_0
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 186
    .local v3, "object":Lorg/json/JSONObject;
    new-instance v1, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;

    const-string v4, "personaId"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v4

    const-string v5, "packageName"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "className"

    invoke-virtual {v3, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "label"

    invoke-virtual {v3, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v1, v4, v5, v6, v7}, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v0    # "activityInfo":Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;
    .local v1, "activityInfo":Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;
    move-object v0, v1

    .end local v1    # "activityInfo":Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;
    .end local v3    # "object":Lorg/json/JSONObject;
    .restart local v0    # "activityInfo":Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;
    :goto_1
    move-object v4, v0

    .line 191
    goto :goto_0

    .line 187
    :catch_0
    move-exception v2

    .line 188
    .local v2, "e":Lorg/json/JSONException;
    invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_1
.end method

.method public getVirtualActivityName(ILjava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "personaId"    # I
    .param p2, "realActivityName"    # Ljava/lang/String;

    .prologue
    .line 54
    const/4 v0, 0x0

    .line 55
    .local v0, "activityMapName":Ljava/lang/String;
    const-string v6, "ShortcutMap"

    invoke-direct {p0, v6}, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;->getPreference(Ljava/lang/String;)Landroid/content/SharedPreferences;

    move-result-object v6

    invoke-interface {v6}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v2

    .line 56
    .local v2, "activityMaps":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;*>;"
    if-eqz v2, :cond_1

    .line 57
    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 58
    .local v3, "entry":Ljava/util/Map$Entry;
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;->getVirtualActivityInfoFromString(Ljava/lang/String;)Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;

    move-result-object v5

    .line 59
    .local v5, "virtualActivityMap":Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;
    invoke-virtual {v5}, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;->getClassName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {v5}, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;->getPersonaId()I

    move-result v6

    if-ne v6, p1, :cond_0

    .line 60
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "activityMapName":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .restart local v0    # "activityMapName":Ljava/lang/String;
    move-object v1, v0

    .line 64
    .end local v0    # "activityMapName":Ljava/lang/String;
    .end local v3    # "entry":Ljava/util/Map$Entry;
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v5    # "virtualActivityMap":Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;
    .local v1, "activityMapName":Ljava/lang/String;
    :goto_0
    return-object v1

    .end local v1    # "activityMapName":Ljava/lang/String;
    .restart local v0    # "activityMapName":Ljava/lang/String;
    :cond_1
    move-object v1, v0

    .end local v0    # "activityMapName":Ljava/lang/String;
    .restart local v1    # "activityMapName":Ljava/lang/String;
    goto :goto_0
.end method

.method public releaseVirtualActivity(I)Ljava/util/ArrayList;
    .locals 7
    .param p1, "personaId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 92
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 93
    .local v4, "releasedActivities":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;>;"
    const/4 v3, 0x0

    .line 94
    .local v3, "key_value":Ljava/lang/String;
    const-string v6, "ShortcutMap"

    invoke-direct {p0, v6}, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;->getPreference(Ljava/lang/String;)Landroid/content/SharedPreferences;

    move-result-object v6

    invoke-interface {v6}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v0

    .line 95
    .local v0, "activityMaps":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;*>;"
    if-eqz v0, :cond_2

    .line 96
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 97
    .local v1, "entry":Ljava/util/Map$Entry;
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;->getVirtualActivityInfoFromString(Ljava/lang/String;)Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;

    move-result-object v5

    .line 98
    .local v5, "virtualActivityMap":Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;
    invoke-virtual {v5}, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;->getPersonaId()I

    move-result v6

    if-ne v6, p1, :cond_0

    .line 99
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "key_value":Ljava/lang/String;
    check-cast v3, Ljava/lang/String;

    .line 100
    .restart local v3    # "key_value":Ljava/lang/String;
    const-string v6, "ShortcutMap"

    invoke-direct {p0, v6}, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;->getPreference(Ljava/lang/String;)Landroid/content/SharedPreferences;

    move-result-object v6

    invoke-interface {v6}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    invoke-interface {v6, v3}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    invoke-interface {v6}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 105
    const-string v6, "Activity"

    invoke-virtual {v3, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 106
    iget-object v6, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;->mAvailList:Ljava/util/ArrayList;

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 108
    :cond_1
    invoke-virtual {v5, v3}, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;->setClassName(Ljava/lang/String;)V

    .line 109
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 112
    .end local v1    # "entry":Ljava/util/Map$Entry;
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v5    # "virtualActivityMap":Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;
    :cond_2
    invoke-direct {p0}, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;->saveAvailableActivitys()V

    .line 113
    return-object v4
.end method

.method public releaseVirtualActivity(ILjava/lang/String;)Ljava/util/ArrayList;
    .locals 7
    .param p1, "personaId"    # I
    .param p2, "packageName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 119
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 120
    .local v4, "releasedActivities":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;>;"
    const/4 v3, 0x0

    .line 121
    .local v3, "key_value":Ljava/lang/String;
    const-string v6, "ShortcutMap"

    invoke-direct {p0, v6}, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;->getPreference(Ljava/lang/String;)Landroid/content/SharedPreferences;

    move-result-object v6

    invoke-interface {v6}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v0

    .line 122
    .local v0, "activityMaps":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;*>;"
    if-eqz v0, :cond_2

    .line 123
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 124
    .local v1, "entry":Ljava/util/Map$Entry;
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;->getVirtualActivityInfoFromString(Ljava/lang/String;)Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;

    move-result-object v5

    .line 125
    .local v5, "virtualActivityMap":Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;
    invoke-virtual {v5}, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {v5}, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;->getPersonaId()I

    move-result v6

    if-ne v6, p1, :cond_0

    .line 127
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "key_value":Ljava/lang/String;
    check-cast v3, Ljava/lang/String;

    .line 128
    .restart local v3    # "key_value":Ljava/lang/String;
    const-string v6, "ShortcutMap"

    invoke-direct {p0, v6}, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;->getPreference(Ljava/lang/String;)Landroid/content/SharedPreferences;

    move-result-object v6

    invoke-interface {v6}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    invoke-interface {v6, v3}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    invoke-interface {v6}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 133
    const-string v6, "Activity"

    invoke-virtual {v3, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 134
    iget-object v6, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;->mAvailList:Ljava/util/ArrayList;

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 136
    :cond_1
    invoke-virtual {v5, v3}, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;->setClassName(Ljava/lang/String;)V

    .line 138
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 141
    .end local v1    # "entry":Ljava/util/Map$Entry;
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v5    # "virtualActivityMap":Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;
    :cond_2
    invoke-direct {p0}, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;->saveAvailableActivitys()V

    .line 142
    return-object v4
.end method

.method public setRealActivityName(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p1, "personaId"    # I
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "realActivityName"    # Ljava/lang/String;
    .param p4, "lable"    # Ljava/lang/String;

    .prologue
    .line 70
    const-string v6, "ShortcutMap"

    invoke-direct {p0, v6}, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;->getPreference(Ljava/lang/String;)Landroid/content/SharedPreferences;

    move-result-object v6

    invoke-interface {v6}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v2

    .line 71
    .local v2, "activityMaps":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;*>;"
    if-eqz v2, :cond_1

    .line 72
    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 73
    .local v3, "entry":Ljava/util/Map$Entry;
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;->getVirtualActivityInfoFromString(Ljava/lang/String;)Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;

    move-result-object v5

    .line 74
    .local v5, "virtualActivityMap":Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;
    invoke-virtual {v5}, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;->getClassName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {v5}, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;->getPersonaId()I

    move-result v6

    if-ne v6, p1, :cond_0

    .line 75
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .local v0, "activityMapName":Ljava/lang/String;
    move-object v1, v0

    .line 86
    .end local v0    # "activityMapName":Ljava/lang/String;
    .end local v3    # "entry":Ljava/util/Map$Entry;
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v5    # "virtualActivityMap":Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;
    .local v1, "activityMapName":Ljava/lang/String;
    :goto_0
    return-object v1

    .line 80
    .end local v1    # "activityMapName":Ljava/lang/String;
    :cond_1
    invoke-direct {p0}, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;->getMappedVirtualActivity()Ljava/lang/String;

    move-result-object v0

    .line 81
    .restart local v0    # "activityMapName":Ljava/lang/String;
    if-eqz v0, :cond_2

    .line 82
    new-instance v5, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;

    invoke-direct {v5, p1, p2, p3, p4}, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    .restart local v5    # "virtualActivityMap":Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;
    const-string v6, "ShortcutMap"

    invoke-direct {p0, v6}, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;->getPreference(Ljava/lang/String;)Landroid/content/SharedPreferences;

    move-result-object v6

    invoke-interface {v6}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    invoke-virtual {p0, v5}, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;->getStringFromVirtualActivityInfo(Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v0, v7}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    invoke-interface {v6}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .end local v5    # "virtualActivityMap":Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;
    :cond_2
    move-object v1, v0

    .line 86
    .end local v0    # "activityMapName":Ljava/lang/String;
    .restart local v1    # "activityMapName":Ljava/lang/String;
    goto :goto_0
.end method
