.class Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$2;
.super Ljava/lang/Thread;
.source "RCPContactsSyncer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->startSyncingProcess()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;


# direct methods
.method constructor <init>(Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;)V
    .locals 0

    .prologue
    .line 296
    iput-object p1, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$2;->this$0:Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 298
    const/4 v0, 0x1

    sput-boolean v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->isSyncState:Z

    .line 299
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$2;->this$0:Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;

    # getter for: Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z
    invoke-static {v0}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->access$300(Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 300
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$2;->this$0:Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;

    iget-object v0, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v1, "Running sync thread..."

    invoke-static {v0, v1}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    :cond_0
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$2;->this$0:Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;

    # invokes: Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->doSync()V
    invoke-static {v0}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->access$400(Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;)V

    .line 302
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$2;->this$0:Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;

    const-string v1, "DO_SYNC"

    # invokes: Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->stopMySelf(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->access$200(Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;Ljava/lang/String;)V

    .line 303
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->isSyncState:Z

    .line 304
    return-void
.end method
