.class public Lcom/samsung/knox/rcp/components/remoteshortcut/SpecialActivityMap;
.super Ljava/lang/Object;
.source "SpecialActivityMap.java"


# static fields
.field private static mActivityMap:Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;

.field private static mContext:Landroid/content/Context;

.field private static mSpecialActivity:Lcom/samsung/knox/rcp/components/remoteshortcut/SpecialActivityMap;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/knox/rcp/components/remoteshortcut/SpecialActivityMap;->mActivityMap:Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/samsung/knox/rcp/components/remoteshortcut/SpecialActivityMap;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 16
    sput-object p0, Lcom/samsung/knox/rcp/components/remoteshortcut/SpecialActivityMap;->mContext:Landroid/content/Context;

    .line 17
    sget-object v0, Lcom/samsung/knox/rcp/components/remoteshortcut/SpecialActivityMap;->mSpecialActivity:Lcom/samsung/knox/rcp/components/remoteshortcut/SpecialActivityMap;

    if-nez v0, :cond_0

    .line 18
    new-instance v0, Lcom/samsung/knox/rcp/components/remoteshortcut/SpecialActivityMap;

    invoke-direct {v0}, Lcom/samsung/knox/rcp/components/remoteshortcut/SpecialActivityMap;-><init>()V

    sput-object v0, Lcom/samsung/knox/rcp/components/remoteshortcut/SpecialActivityMap;->mSpecialActivity:Lcom/samsung/knox/rcp/components/remoteshortcut/SpecialActivityMap;

    .line 20
    :cond_0
    invoke-static {p0}, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;->getInstance(Landroid/content/Context;)Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;

    move-result-object v0

    sput-object v0, Lcom/samsung/knox/rcp/components/remoteshortcut/SpecialActivityMap;->mActivityMap:Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;

    .line 21
    sget-object v0, Lcom/samsung/knox/rcp/components/remoteshortcut/SpecialActivityMap;->mSpecialActivity:Lcom/samsung/knox/rcp/components/remoteshortcut/SpecialActivityMap;

    return-object v0
.end method

.method private getPreference(Ljava/lang/String;)Landroid/content/SharedPreferences;
    .locals 3
    .param p1, "preferenceType"    # Ljava/lang/String;

    .prologue
    .line 53
    sget-object v1, Lcom/samsung/knox/rcp/components/remoteshortcut/SpecialActivityMap;->mContext:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 55
    .local v0, "prefs":Landroid/content/SharedPreferences;
    return-object v0
.end method


# virtual methods
.method public getRealActivity(J)Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;
    .locals 5
    .param p1, "id"    # J

    .prologue
    .line 59
    const-string v1, "ShortcutMap"

    invoke-direct {p0, v1}, Lcom/samsung/knox/rcp/components/remoteshortcut/SpecialActivityMap;->getPreference(Ljava/lang/String;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 62
    .local v0, "timeStamp":Ljava/lang/String;
    sget-object v1, Lcom/samsung/knox/rcp/components/remoteshortcut/SpecialActivityMap;->mActivityMap:Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;

    invoke-virtual {v1, v0}, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;->getVirtualActivityInfoFromString(Ljava/lang/String;)Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;

    move-result-object v1

    return-object v1
.end method

.method public setRealActivityName(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)J
    .locals 9
    .param p1, "personaId"    # I
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "realActivityName"    # Ljava/lang/String;
    .param p4, "lable"    # Ljava/lang/String;

    .prologue
    .line 31
    sget-object v7, Lcom/samsung/knox/rcp/components/remoteshortcut/SpecialActivityMap;->mActivityMap:Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;

    const-string v7, "ShortcutMap"

    invoke-direct {p0, v7}, Lcom/samsung/knox/rcp/components/remoteshortcut/SpecialActivityMap;->getPreference(Ljava/lang/String;)Landroid/content/SharedPreferences;

    move-result-object v7

    invoke-interface {v7}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v0

    .line 32
    .local v0, "activityMaps":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;*>;"
    if-eqz v0, :cond_1

    .line 33
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 34
    .local v1, "entry":Ljava/util/Map$Entry;
    sget-object v7, Lcom/samsung/knox/rcp/components/remoteshortcut/SpecialActivityMap;->mActivityMap:Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;->getVirtualActivityInfoFromString(Ljava/lang/String;)Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;

    move-result-object v6

    .line 35
    .local v6, "virtualActivityMap":Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;
    invoke-virtual {v6}, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;->getClassName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-virtual {v6}, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;->getPersonaId()I

    move-result v7

    if-ne v7, p1, :cond_0

    .line 37
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 38
    .local v3, "timestamp":Ljava/lang/String;
    invoke-static {v3}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 49
    .end local v1    # "entry":Ljava/util/Map$Entry;
    .end local v2    # "i$":Ljava/util/Iterator;
    :goto_0
    return-wide v4

    .line 42
    .end local v3    # "timestamp":Ljava/lang/String;
    .end local v6    # "virtualActivityMap":Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;
    :cond_1
    new-instance v6, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;

    invoke-direct {v6, p1, p2, p3, p4}, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    .restart local v6    # "virtualActivityMap":Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 44
    .local v4, "id_timestamp":J
    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    .line 45
    .restart local v3    # "timestamp":Ljava/lang/String;
    const-string v7, "ShortcutMap"

    invoke-direct {p0, v7}, Lcom/samsung/knox/rcp/components/remoteshortcut/SpecialActivityMap;->getPreference(Ljava/lang/String;)Landroid/content/SharedPreferences;

    move-result-object v7

    invoke-interface {v7}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    sget-object v8, Lcom/samsung/knox/rcp/components/remoteshortcut/SpecialActivityMap;->mActivityMap:Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;

    invoke-virtual {v8, v6}, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;->getStringFromVirtualActivityInfo(Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v3, v8}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    invoke-interface {v7}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method
