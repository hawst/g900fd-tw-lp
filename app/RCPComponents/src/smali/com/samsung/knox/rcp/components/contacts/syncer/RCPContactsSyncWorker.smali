.class public Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncWorker;
.super Ljava/lang/Object;
.source "RCPContactsSyncWorker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncWorker$1;
    }
.end annotation


# static fields
.field static mCtx:Landroid/content/Context;

.field static sInstance:Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncWorker;


# instance fields
.field mGroupSyncMaps:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field

.field mStarredInfoMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 46
    sput-object v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncWorker;->sInstance:Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncWorker;

    .line 48
    sput-object v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncWorker;->mCtx:Landroid/content/Context;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncWorker;->mStarredInfoMap:Ljava/util/HashMap;

    .line 52
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncWorker;->mGroupSyncMaps:Ljava/util/HashMap;

    .line 56
    return-void
.end method

.method public static getInstance()Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncWorker;
    .locals 1

    .prologue
    .line 74
    sget-object v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncWorker;->sInstance:Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncWorker;

    return-object v0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncWorker;
    .locals 1
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    .line 60
    sget-object v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncWorker;->sInstance:Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncWorker;

    if-nez v0, :cond_0

    .line 62
    new-instance v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncWorker;

    invoke-direct {v0}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncWorker;-><init>()V

    sput-object v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncWorker;->sInstance:Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncWorker;

    .line 64
    sput-object p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncWorker;->mCtx:Landroid/content/Context;

    .line 68
    :cond_0
    sget-object v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncWorker;->sInstance:Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncWorker;

    return-object v0
.end method

.method public static insert(Landroid/content/Context;Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;Lcom/sec/knox/bridge/IBridgeService;II)V
    .locals 51
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "syncer"    # Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;
    .param p2, "bridgeService"    # Lcom/sec/knox/bridge/IBridgeService;
    .param p3, "userid"    # I
    .param p4, "min_id"    # I

    .prologue
    .line 169
    const-string v6, "RCPContactsSyncWorker"

    const-string v7, "syncContacts() START"

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    const/16 v22, 0x0

    .line 173
    .local v22, "cntOriRaw":I
    const/16 v21, 0x0

    .line 175
    .local v21, "cntConRaw":I
    const/16 v33, 0x0

    .line 177
    .local v33, "inserts":I
    const/16 v30, 0x0

    .line 179
    .local v30, "deletes":I
    const/16 v50, 0x0

    .line 181
    .local v50, "updates":I
    const/16 v49, 0x0

    .line 183
    .local v49, "synced":I
    const/16 v34, 0x0

    .line 185
    .local v34, "joined":I
    const/16 v14, 0x64

    .line 189
    .local v14, "OPS_SIZE":I
    const/16 v38, 0x0

    .line 195
    .local v38, "orig_raw_cur":Landroid/content/CustomCursor;
    :try_start_0
    const-string v7, "Contacts"

    const-string v8, "RAW_CONTACTS"

    sget-object v10, Lcom/samsung/knox/rcp/components/contacts/syncer/util/DataSyncConstants;->ORI_RAWCONTACTS_PROJECTION:[Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, " _id > "

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, p4

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    const-string v13, "_id ASC"

    move-object/from16 v6, p2

    move/from16 v9, p3

    invoke-interface/range {v6 .. v13}, Lcom/sec/knox/bridge/IBridgeService;->queryProvider(Ljava/lang/String;Ljava/lang/String;I[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/content/CustomCursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v38

    .line 211
    if-eqz v38, :cond_13

    .line 213
    const/16 v24, 0x0

    .line 215
    .local v24, "con_raw_cur":Landroid/database/Cursor;
    const/16 v27, 0x0

    .line 217
    .local v27, "dbContacts":Landroid/database/Cursor;
    const/16 v18, 0x0

    .line 219
    .local v18, "accountType":Ljava/lang/String;
    const/4 v15, 0x0

    .line 223
    .local v15, "accountName":Ljava/lang/String;
    const/16 v6, 0x64

    move/from16 v0, p3

    if-lt v0, v6, :cond_3

    .line 225
    :try_start_1
    move-object/from16 v0, p1

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->createOrGetAccountTypeForKnox(I)Ljava/lang/String;

    move-result-object v18

    .line 227
    const-string v6, "RCPContactsSyncWorker"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, " syncContacts() knoxAccountType = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " for user = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move/from16 v0, p3

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "vnd.sec.contact.phone_knox"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, p3

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    .line 242
    :goto_0
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    sget-object v7, Lcom/samsung/knox/rcp/components/contacts/syncer/util/DataSyncConstants;->CON_RAW_CONTACTS_URI:Landroid/net/Uri;

    sget-object v8, Lcom/samsung/knox/rcp/components/contacts/syncer/util/DataSyncConstants;->CON_RAWCONTACTS_PROJECTION:[Ljava/lang/String;

    const-string v9, "account_name=? AND account_type=? AND deleted=?"

    const/4 v10, 0x3

    new-array v10, v10, [Ljava/lang/String;

    const/4 v11, 0x0

    aput-object v15, v10, v11

    const/4 v11, 0x1

    aput-object v18, v10, v11

    const/4 v11, 0x2

    const-string v12, "0"

    aput-object v12, v10, v11

    const-string v11, "_id ASC"

    invoke-virtual/range {v6 .. v11}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v24

    .line 253
    if-nez v24, :cond_4

    .line 255
    const-string v6, "RCPContactsSyncWorker"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "syncContacts con_raw_cur == null. Skipping ... for userOrPersonaId="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move/from16 v0, p3

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 259
    invoke-virtual/range {v38 .. v38}, Landroid/content/CustomCursor;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 261
    const/16 v38, 0x0

    .line 721
    if-eqz v38, :cond_0

    .line 723
    invoke-virtual/range {v38 .. v38}, Landroid/content/CustomCursor;->close()V

    .line 725
    const/16 v38, 0x0

    .line 729
    :cond_0
    if-eqz v24, :cond_1

    .line 731
    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->close()V

    .line 734
    const/16 v24, 0x0

    .line 738
    :cond_1
    if-eqz v27, :cond_2

    .line 740
    invoke-interface/range {v27 .. v27}, Landroid/database/Cursor;->close()V

    .line 742
    const/16 v27, 0x0

    .line 746
    :cond_2
    const-string v6, "RCPContactsSyncWorker"

    const-string v7, "syncContacts() completed"

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 754
    .end local v15    # "accountName":Ljava/lang/String;
    .end local v18    # "accountType":Ljava/lang/String;
    .end local v24    # "con_raw_cur":Landroid/database/Cursor;
    .end local v27    # "dbContacts":Landroid/database/Cursor;
    :goto_1
    return-void

    .line 203
    :catch_0
    move-exception v31

    .line 205
    .local v31, "e":Ljava/lang/Exception;
    invoke-virtual/range {v31 .. v31}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 234
    .end local v31    # "e":Ljava/lang/Exception;
    .restart local v15    # "accountName":Ljava/lang/String;
    .restart local v18    # "accountType":Ljava/lang/String;
    .restart local v24    # "con_raw_cur":Landroid/database/Cursor;
    .restart local v27    # "dbContacts":Landroid/database/Cursor;
    :cond_3
    :try_start_2
    const-string v18, "vnd.sec.contact.phone_personal"

    .line 236
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "vnd.sec.contact.phone_personal"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, p3

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    goto :goto_0

    .line 269
    :cond_4
    invoke-virtual/range {v38 .. v38}, Landroid/content/CustomCursor;->getCount()I

    move-result v22

    .line 271
    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->getCount()I

    move-result v21

    .line 273
    const-string v6, "RCPContactsSyncWorker"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "syncContacts cntOriRaw "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move/from16 v0, v22

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 275
    const-string v6, "RCPContactsSyncWorker"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "syncContacts cntConRaw  "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move/from16 v0, v21

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 279
    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->contactsRcpDb:Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;

    invoke-virtual {v6}, Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "select * from RAW_CONTACTS_SYNC_TABLE where _id_raw_contact_id_original>"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move/from16 v0, p4

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " and raw_contacts_persona_id="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move/from16 v0, p3

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " order by _id_raw_original"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v27

    .line 284
    if-nez v27, :cond_8

    .line 286
    const-string v6, "RCPContactsSyncWorker"

    const-string v7, "local db returns null cursor"

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 288
    invoke-virtual/range {v38 .. v38}, Landroid/content/CustomCursor;->close()V

    .line 290
    const/16 v38, 0x0

    .line 292
    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 294
    const/16 v24, 0x0

    .line 721
    if-eqz v38, :cond_5

    .line 723
    invoke-virtual/range {v38 .. v38}, Landroid/content/CustomCursor;->close()V

    .line 725
    const/16 v38, 0x0

    .line 729
    :cond_5
    if-eqz v24, :cond_6

    .line 731
    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->close()V

    .line 734
    const/16 v24, 0x0

    .line 738
    :cond_6
    if-eqz v27, :cond_7

    .line 740
    invoke-interface/range {v27 .. v27}, Landroid/database/Cursor;->close()V

    .line 742
    const/16 v27, 0x0

    .line 746
    :cond_7
    const-string v6, "RCPContactsSyncWorker"

    const-string v7, "syncContacts() completed"

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 302
    :cond_8
    :try_start_3
    const-string v6, "RCPContactsSyncWorker"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Before Join : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual/range {v38 .. v38}, Landroid/content/CustomCursor;->getCount()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-interface/range {v27 .. v27}, Landroid/database/Cursor;->getCount()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 304
    new-instance v35, Landroid/database/CursorJoiner;

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "_id"

    aput-object v8, v6, v7

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    const-string v9, "_id_raw_original"

    aput-object v9, v7, v8

    move-object/from16 v0, v35

    move-object/from16 v1, v38

    move-object/from16 v2, v27

    invoke-direct {v0, v1, v6, v2, v7}, Landroid/database/CursorJoiner;-><init>(Landroid/database/Cursor;[Ljava/lang/String;Landroid/database/Cursor;[Ljava/lang/String;)V

    .line 316
    .local v35, "joiner":Landroid/database/CursorJoiner;
    const-string v6, "RCPContactsSyncWorker"

    const-string v7, "After Join"

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 318
    new-instance v37, Ljava/util/ArrayList;

    invoke-direct/range {v37 .. v37}, Ljava/util/ArrayList;-><init>()V

    .line 320
    .local v37, "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    new-instance v45, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;

    invoke-direct/range {v45 .. v45}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;-><init>()V

    .line 322
    .local v45, "rci":Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;
    invoke-virtual/range {v35 .. v35}, Landroid/database/CursorJoiner;->iterator()Ljava/util/Iterator;

    move-result-object v32

    .local v32, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface/range {v32 .. v32}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_d

    invoke-interface/range {v32 .. v32}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v46

    check-cast v46, Landroid/database/CursorJoiner$Result;

    .line 326
    .local v46, "result":Landroid/database/CursorJoiner$Result;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->isBridgeServiceValid()Z

    move-result v6

    if-nez v6, :cond_c

    .line 328
    const-string v6, "RCPContactsSyncWorker"

    const-string v7, "syncContacts(): isBridgeServiceValid() == false; stopping syncing...."

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 721
    if-eqz v38, :cond_9

    .line 723
    invoke-virtual/range {v38 .. v38}, Landroid/content/CustomCursor;->close()V

    .line 725
    const/16 v38, 0x0

    .line 729
    :cond_9
    if-eqz v24, :cond_a

    .line 731
    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->close()V

    .line 734
    const/16 v24, 0x0

    .line 738
    :cond_a
    if-eqz v27, :cond_b

    .line 740
    invoke-interface/range {v27 .. v27}, Landroid/database/Cursor;->close()V

    .line 742
    const/16 v27, 0x0

    .line 746
    :cond_b
    const-string v6, "RCPContactsSyncWorker"

    const-string v7, "syncContacts() completed"

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 335
    :cond_c
    :try_start_4
    sget-boolean v6, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->isSyncState:Z

    if-nez v6, :cond_14

    .line 337
    const-string v6, "RCPContactsSyncWorker"

    const-string v7, "mSyncThread interrupt "

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 700
    .end local v46    # "result":Landroid/database/CursorJoiner$Result;
    :cond_d
    invoke-virtual/range {v37 .. v37}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_e

    .line 704
    move-object/from16 v0, p1

    move-object/from16 v1, v37

    move/from16 v2, p3

    move-object/from16 v3, v45

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->applyBatchRawContacts(Ljava/util/ArrayList;ILcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;)V

    .line 708
    :cond_e
    if-lez v33, :cond_f

    .line 710
    move-object/from16 v0, p1

    move/from16 v1, v33

    move-object/from16 v2, v45

    move/from16 v3, p3

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->insertLocalDB(ILcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;I)V

    .line 712
    :cond_f
    const-string v6, "RCPContactsSyncWorker"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "inserts("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move/from16 v0, v33

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "), deletes("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move/from16 v0, v30

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "), updates("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move/from16 v0, v50

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "), synced("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move/from16 v0, v49

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ") joined("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move/from16 v0, v34

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 721
    if-eqz v38, :cond_10

    .line 723
    invoke-virtual/range {v38 .. v38}, Landroid/content/CustomCursor;->close()V

    .line 725
    const/16 v38, 0x0

    .line 729
    :cond_10
    if-eqz v24, :cond_11

    .line 731
    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->close()V

    .line 734
    const/16 v24, 0x0

    .line 738
    :cond_11
    if-eqz v27, :cond_12

    .line 740
    invoke-interface/range {v27 .. v27}, Landroid/database/Cursor;->close()V

    .line 742
    const/16 v27, 0x0

    .line 746
    :cond_12
    const-string v6, "RCPContactsSyncWorker"

    const-string v7, "syncContacts() completed"

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 752
    .end local v15    # "accountName":Ljava/lang/String;
    .end local v18    # "accountType":Ljava/lang/String;
    .end local v24    # "con_raw_cur":Landroid/database/Cursor;
    .end local v27    # "dbContacts":Landroid/database/Cursor;
    .end local v32    # "i$":Ljava/util/Iterator;
    .end local v35    # "joiner":Landroid/database/CursorJoiner;
    .end local v37    # "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .end local v45    # "rci":Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;
    :cond_13
    :goto_3
    const-string v6, "RCPContactsSyncWorker"

    const-string v7, "syncContacts() END"

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 343
    .restart local v15    # "accountName":Ljava/lang/String;
    .restart local v18    # "accountType":Ljava/lang/String;
    .restart local v24    # "con_raw_cur":Landroid/database/Cursor;
    .restart local v27    # "dbContacts":Landroid/database/Cursor;
    .restart local v32    # "i$":Ljava/util/Iterator;
    .restart local v35    # "joiner":Landroid/database/CursorJoiner;
    .restart local v37    # "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .restart local v45    # "rci":Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;
    .restart local v46    # "result":Landroid/database/CursorJoiner$Result;
    :cond_14
    add-int/lit8 v34, v34, 0x1

    .line 345
    :try_start_5
    sget-object v6, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncWorker$1;->$SwitchMap$android$database$CursorJoiner$Result:[I

    invoke-virtual/range {v46 .. v46}, Landroid/database/CursorJoiner$Result;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    goto/16 :goto_2

    .line 349
    :pswitch_0
    move-object/from16 v0, p1

    move-object/from16 v1, v37

    move-object/from16 v2, v38

    move/from16 v3, p3

    move-object/from16 v4, v45

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->insertRawContact(Ljava/util/ArrayList;Landroid/content/CustomCursor;ILcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;)V

    .line 351
    invoke-virtual/range {v37 .. v37}, Ljava/util/ArrayList;->size()I

    move-result v6

    rem-int/2addr v6, v14

    if-nez v6, :cond_15

    .line 353
    move-object/from16 v0, p1

    move-object/from16 v1, v37

    move/from16 v2, p3

    move-object/from16 v3, v45

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->applyBatchRawContacts(Ljava/util/ArrayList;ILcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;)V

    .line 357
    :cond_15
    add-int/lit8 v33, v33, 0x1

    .line 359
    goto/16 :goto_2

    .line 363
    :pswitch_1
    const-string v39, ""

    .line 365
    .local v39, "rawIdContainer":Ljava/lang/String;
    const-string v6, "_id_raw_container"

    move-object/from16 v0, v27

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v44

    .line 368
    .local v44, "raw_id_container_Index":I
    const/4 v6, -0x1

    move/from16 v0, v44

    if-ne v0, v6, :cond_19

    .line 370
    const-string v6, "RCPContactsSyncWorker"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "syncContacts() Rorre.. Skipping case RIGHT delete contact... Raw Id Container Index  "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move/from16 v0, v44

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_2

    .line 715
    .end local v32    # "i$":Ljava/util/Iterator;
    .end local v35    # "joiner":Landroid/database/CursorJoiner;
    .end local v37    # "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .end local v39    # "rawIdContainer":Ljava/lang/String;
    .end local v44    # "raw_id_container_Index":I
    .end local v45    # "rci":Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;
    .end local v46    # "result":Landroid/database/CursorJoiner$Result;
    :catch_1
    move-exception v31

    .line 717
    .restart local v31    # "e":Ljava/lang/Exception;
    :try_start_6
    invoke-virtual/range {v31 .. v31}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 721
    if-eqz v38, :cond_16

    .line 723
    invoke-virtual/range {v38 .. v38}, Landroid/content/CustomCursor;->close()V

    .line 725
    const/16 v38, 0x0

    .line 729
    :cond_16
    if-eqz v24, :cond_17

    .line 731
    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->close()V

    .line 734
    const/16 v24, 0x0

    .line 738
    :cond_17
    if-eqz v27, :cond_18

    .line 740
    invoke-interface/range {v27 .. v27}, Landroid/database/Cursor;->close()V

    .line 742
    const/16 v27, 0x0

    .line 746
    :cond_18
    const-string v6, "RCPContactsSyncWorker"

    const-string v7, "syncContacts() completed"

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 382
    .end local v31    # "e":Ljava/lang/Exception;
    .restart local v32    # "i$":Ljava/util/Iterator;
    .restart local v35    # "joiner":Landroid/database/CursorJoiner;
    .restart local v37    # "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .restart local v39    # "rawIdContainer":Ljava/lang/String;
    .restart local v44    # "raw_id_container_Index":I
    .restart local v45    # "rci":Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;
    .restart local v46    # "result":Landroid/database/CursorJoiner$Result;
    :cond_19
    :try_start_7
    move-object/from16 v0, v27

    move/from16 v1, v44

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v39

    .line 384
    if-eqz v39, :cond_1a

    .line 388
    const-string v6, "RCPContactsSyncWorker"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "syncContacts Raw Id Container is  "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v39

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 402
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    sget-object v7, Lcom/samsung/knox/rcp/components/contacts/syncer/util/DataSyncConstants;->CON_RAW_CONTACTS_URI:Landroid/net/Uri;

    const-string v8, "_id=?"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    aput-object v39, v9, v10

    invoke-virtual {v6, v7, v8, v9}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v36

    .line 409
    .local v36, "noOfDelete":I
    const/4 v6, 0x1

    move/from16 v0, v36

    if-ne v0, v6, :cond_1e

    .line 411
    const-string v6, "RCPContactsSyncWorker"

    const-string v7, "syncContacts Entry deleted form CP "

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 443
    :goto_4
    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->contactsRcpDb:Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "_id_raw_container="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v39

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;->deleteContact(Ljava/lang/String;)I

    move-result v29

    .line 447
    .local v29, "deleteLocalDB":I
    const/4 v6, 0x1

    move/from16 v0, v29

    if-ge v0, v6, :cond_21

    .line 449
    const-string v6, "RCPContactsSyncWorker"

    const-string v7, "syncContacts Failed to delete form local DB "

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 455
    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->contactsRcpDb:Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "_id_raw_container="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v39

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;->deleteContact(Ljava/lang/String;)I

    move-result v29

    .line 459
    const/4 v6, 0x1

    move/from16 v0, v29

    if-ge v0, v6, :cond_20

    .line 461
    const-string v6, "RCPContactsSyncWorker"

    const-string v7, "syncContacts Failed to delete form local DB "

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 473
    :goto_5
    add-int/lit8 v30, v30, 0x1

    .line 475
    goto/16 :goto_2

    .line 392
    .end local v29    # "deleteLocalDB":I
    .end local v36    # "noOfDelete":I
    :cond_1a
    const-string v6, "RCPContactsSyncWorker"

    const-string v7, "syncContacts Raw Id Container is Null "

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_2

    .line 721
    .end local v32    # "i$":Ljava/util/Iterator;
    .end local v35    # "joiner":Landroid/database/CursorJoiner;
    .end local v37    # "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .end local v39    # "rawIdContainer":Ljava/lang/String;
    .end local v44    # "raw_id_container_Index":I
    .end local v45    # "rci":Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;
    .end local v46    # "result":Landroid/database/CursorJoiner$Result;
    :catchall_0
    move-exception v6

    if-eqz v38, :cond_1b

    .line 723
    invoke-virtual/range {v38 .. v38}, Landroid/content/CustomCursor;->close()V

    .line 725
    const/16 v38, 0x0

    .line 729
    :cond_1b
    if-eqz v24, :cond_1c

    .line 731
    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->close()V

    .line 734
    const/16 v24, 0x0

    .line 738
    :cond_1c
    if-eqz v27, :cond_1d

    .line 740
    invoke-interface/range {v27 .. v27}, Landroid/database/Cursor;->close()V

    .line 742
    const/16 v27, 0x0

    .line 746
    :cond_1d
    const-string v7, "RCPContactsSyncWorker"

    const-string v8, "syncContacts() completed"

    invoke-static {v7, v8}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    throw v6

    .line 417
    .restart local v32    # "i$":Ljava/util/Iterator;
    .restart local v35    # "joiner":Landroid/database/CursorJoiner;
    .restart local v36    # "noOfDelete":I
    .restart local v37    # "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .restart local v39    # "rawIdContainer":Ljava/lang/String;
    .restart local v44    # "raw_id_container_Index":I
    .restart local v45    # "rci":Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;
    .restart local v46    # "result":Landroid/database/CursorJoiner$Result;
    :cond_1e
    :try_start_8
    const-string v6, "RCPContactsSyncWorker"

    const-string v7, "syncContacts Failed to delete from CP "

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 421
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    sget-object v7, Lcom/samsung/knox/rcp/components/contacts/syncer/util/DataSyncConstants;->CON_RAW_CONTACTS_URI:Landroid/net/Uri;

    const-string v8, "_id=?"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    aput-object v39, v9, v10

    invoke-virtual {v6, v7, v8, v9}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v36

    .line 428
    const/4 v6, 0x1

    move/from16 v0, v36

    if-ne v0, v6, :cond_1f

    .line 430
    const-string v6, "RCPContactsSyncWorker"

    const-string v7, "syncContacts Entry deleted form CP on second attempt"

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 435
    :cond_1f
    const-string v6, "RCPContactsSyncWorker"

    const-string v7, "syncContacts Failed to Failed to  delete form CP "

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 467
    .restart local v29    # "deleteLocalDB":I
    :cond_20
    const-string v6, "RCPContactsSyncWorker"

    const-string v7, "syncContacts Entry deleted form local DB "

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    .line 471
    :cond_21
    const-string v6, "RCPContactsSyncWorker"

    const-string v7, "syncContacts Entry deleted form local DB "

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    .line 491
    .end local v29    # "deleteLocalDB":I
    .end local v36    # "noOfDelete":I
    .end local v39    # "rawIdContainer":Ljava/lang/String;
    .end local v44    # "raw_id_container_Index":I
    :pswitch_2
    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->contactsRcpDb:Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;

    const-string v6, "_id_raw_container"

    move-object/from16 v0, v27

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, v27

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v25

    .line 494
    .local v25, "contactId":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    sget-object v7, Lcom/samsung/knox/rcp/components/contacts/syncer/util/DataSyncConstants;->CON_RAW_CONTACTS_URI:Landroid/net/Uri;

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    const-string v10, "starred"

    aput-object v10, v8, v9

    const-string v9, "_id=? AND deleted=?"

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/String;

    const/4 v11, 0x0

    aput-object v25, v10, v11

    const/4 v11, 0x1

    const-string v12, "0"

    aput-object v12, v10, v11

    const/4 v11, 0x0

    invoke-virtual/range {v6 .. v11}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v48

    .line 507
    .local v48, "starred_con":Landroid/database/Cursor;
    if-nez v48, :cond_22

    .line 509
    const-string v6, "RCPContactsSyncWorker"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "syncContacts(): starred_con is null, Skipping... no changes made for a record for the userOrPersonaId="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move/from16 v0, p3

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 515
    :cond_22
    invoke-interface/range {v48 .. v48}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v6

    if-nez v6, :cond_23

    .line 517
    const-string v6, "RCPContactsSyncWorker"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "syncContacts(): starred_con is empty or record with id ("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v25

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ") is deleted, Skipping... no changes made for a record for the userOrPersonaId="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move/from16 v0, p3

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 523
    invoke-interface/range {v48 .. v48}, Landroid/database/Cursor;->close()V

    .line 525
    invoke-interface/range {v48 .. v48}, Landroid/database/Cursor;->close()V

    goto/16 :goto_2

    .line 533
    :cond_23
    const-string v6, "raw_contacts_version"

    move-object/from16 v0, v27

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, v27

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    const-string v7, "version"

    move-object/from16 v0, v38

    invoke-virtual {v0, v7}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    move-object/from16 v0, v38

    invoke-virtual {v0, v7}, Landroid/content/CustomCursor;->getInt(I)I

    move-result v7

    if-ne v6, v7, :cond_25

    const-string v6, "_id_raw_contact_id_original"

    move-object/from16 v0, v27

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, v27

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    const-string v7, "contact_id"

    move-object/from16 v0, v38

    invoke-virtual {v0, v7}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    move-object/from16 v0, v38

    invoke-virtual {v0, v7}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_25

    const-string v6, "raw_name_verified"

    move-object/from16 v0, v27

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, v27

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    const-string v7, "name_verified"

    move-object/from16 v0, v38

    invoke-virtual {v0, v7}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    move-object/from16 v0, v38

    invoke-virtual {v0, v7}, Landroid/content/CustomCursor;->getInt(I)I

    move-result v7

    if-ne v6, v7, :cond_25

    const-string v6, "starred"

    move-object/from16 v0, v48

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, v48

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    const-string v7, "starred"

    move-object/from16 v0, v38

    invoke-virtual {v0, v7}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    move-object/from16 v0, v38

    invoke-virtual {v0, v7}, Landroid/content/CustomCursor;->getInt(I)I

    move-result v7

    if-ne v6, v7, :cond_25

    .line 559
    const-string v6, "RCPContactsSyncWorker"

    const-string v7, "syncContacts  Nochange case "

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 561
    add-int/lit8 v49, v49, 0x1

    .line 692
    :cond_24
    :goto_6
    invoke-interface/range {v48 .. v48}, Landroid/database/Cursor;->close()V

    goto/16 :goto_2

    .line 569
    :cond_25
    const-string v6, "RCPContactsSyncWorker"

    const-string v7, "syncContacts  Update case "

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 571
    const-string v6, "_id_raw_container"

    move-object/from16 v0, v27

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, v27

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v16

    .line 577
    .local v16, "_idToDelete":J
    sget-object v6, Lcom/samsung/knox/rcp/components/contacts/syncer/util/DataSyncConstants;->CON_RAW_CONTACTS_URI:Landroid/net/Uri;

    invoke-virtual {v6}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v6

    const-string v7, "caller_is_syncadapter"

    const-string v8, "true"

    invoke-virtual {v6, v7, v8}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v23

    .line 585
    .local v23, "conUri":Landroid/net/Uri;
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "_id=?"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    invoke-static/range {v16 .. v17}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    move-object/from16 v0, v23

    invoke-virtual {v6, v0, v7, v8}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v47

    .line 590
    .local v47, "rowsDeleted":I
    const/4 v6, 0x1

    move/from16 v0, v47

    if-ne v0, v6, :cond_26

    .line 592
    const-string v6, "RCPContactsSyncWorker"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    move/from16 v0, v47

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " rows have been deleted from raw contacts (should be 1) with raw_contacts _id of: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-wide/from16 v0, v16

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 611
    :goto_7
    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->contactsRcpDb:Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "_id_raw_container="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static/range {v16 .. v17}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;->deleteContact(Ljava/lang/String;)I

    move-result v28

    .line 615
    .local v28, "dbRowsDeleted":I
    const-string v6, "RCPContactsSyncWorker"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    move/from16 v0, v28

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " rows have been deleted from local db (should also be 1) with "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 622
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    sget-object v7, Landroid/provider/ContactsContract$AggregationExceptions;->CONTENT_URI:Landroid/net/Uri;

    const/4 v8, 0x0

    const-string v9, "raw_contact_id1=? OR raw_contact_id2=?"

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/String;

    const/4 v11, 0x0

    invoke-static/range {v16 .. v17}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    invoke-static/range {v16 .. v17}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    const-string v11, "_id ASC"

    invoke-virtual/range {v6 .. v11}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v19

    .line 634
    .local v19, "aggCheck":Landroid/database/Cursor;
    if-eqz v19, :cond_27

    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->getCount()I

    move-result v6

    if-lez v6, :cond_27

    .line 636
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->moveToFirst()Z

    .line 638
    :goto_8
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v6

    if-nez v6, :cond_27

    .line 640
    const-string v6, "raw_contact_id1"

    move-object/from16 v0, v19

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, v19

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v40

    .line 644
    .local v40, "rawContact1":J
    const-string v6, "raw_contact_id2"

    move-object/from16 v0, v19

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, v19

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v42

    .line 648
    .local v42, "rawContact2":J
    new-instance v26, Landroid/content/ContentValues;

    invoke-direct/range {v26 .. v26}, Landroid/content/ContentValues;-><init>()V

    .line 650
    .local v26, "cv":Landroid/content/ContentValues;
    const-string v6, "type"

    const/4 v7, 0x2

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object/from16 v0, v26

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 653
    const-string v6, "raw_contact_id1"

    invoke-static/range {v40 .. v41}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    move-object/from16 v0, v26

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 655
    const-string v6, "raw_contact_id2"

    invoke-static/range {v42 .. v43}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    move-object/from16 v0, v26

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 657
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    sget-object v7, Landroid/provider/ContactsContract$AggregationExceptions;->CONTENT_URI:Landroid/net/Uri;

    const-string v8, "raw_contact_id1=? AND raw_contact_id2=?"

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    invoke-static/range {v40 .. v41}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    invoke-static/range {v42 .. v43}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    move-object/from16 v0, v26

    invoke-virtual {v6, v7, v0, v8, v9}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v20

    .line 670
    .local v20, "aggUpdate":I
    const-string v6, "RCPContactsSyncWorker"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Aggregation exceptions updated for joined contacts: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move/from16 v0, v20

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 674
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->moveToNext()Z

    goto/16 :goto_8

    .line 601
    .end local v19    # "aggCheck":Landroid/database/Cursor;
    .end local v20    # "aggUpdate":I
    .end local v26    # "cv":Landroid/content/ContentValues;
    .end local v28    # "dbRowsDeleted":I
    .end local v40    # "rawContact1":J
    .end local v42    # "rawContact2":J
    :cond_26
    const-string v6, "RCPContactsSyncWorker"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    move/from16 v0, v47

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " rows have been deleted from raw contacts (should be 1) with raw_contacts _id of: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-wide/from16 v0, v16

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_7

    .line 680
    .restart local v19    # "aggCheck":Landroid/database/Cursor;
    .restart local v28    # "dbRowsDeleted":I
    :cond_27
    move-object/from16 v0, p1

    move-object/from16 v1, v38

    move/from16 v2, p3

    invoke-virtual {v0, v1, v2}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->insertContacts(Landroid/content/CustomCursor;I)V

    .line 682
    add-int/lit8 v50, v50, 0x1

    .line 684
    if-eqz v19, :cond_24

    .line 686
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto/16 :goto_6

    .line 345
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public checkNewContacts(Landroid/content/Context;Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;)V
    .locals 9
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "syncer"    # Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;

    .prologue
    const/4 v8, 0x0

    .line 82
    const/4 v4, 0x0

    .line 84
    .local v4, "personas":[I
    const/4 v5, -0x1

    .line 86
    .local v5, "recentRawcontactId":I
    iget-object v6, p2, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->contactsRcpDb:Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;

    invoke-virtual {v6}, Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 88
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    const-string v6, "RCPContactsSyncWorker"

    const-string v7, "checkNewContacts START"

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    iget-object v6, p2, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mPm:Landroid/os/PersonaManager;

    if-nez v6, :cond_0

    .line 92
    const-string v6, "RCPContactsSyncWorker"

    const-string v7, "PersonaManager is null"

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    :goto_0
    return-void

    .line 98
    :cond_0
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v6

    if-eqz v6, :cond_4

    .line 100
    const/4 v6, 0x1

    new-array v4, v6, [I

    .end local v4    # "personas":[I
    aput v8, v4, v8

    .line 110
    .restart local v4    # "personas":[I
    :goto_1
    if-eqz v4, :cond_6

    .line 112
    const/4 v0, 0x0

    .line 114
    .local v0, "cursor":Landroid/database/Cursor;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_2
    array-length v6, v4

    if-ge v3, v6, :cond_6

    .line 116
    const/4 v5, -0x1

    .line 120
    :try_start_0
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "select max(_id_raw_contact_id_original) from RAW_CONTACTS_SYNC_TABLE where raw_contacts_persona_id="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget v7, v4, v3

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v1, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 128
    if-eqz v0, :cond_1

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 130
    const/4 v6, 0x0

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v5

    .line 140
    :cond_1
    if-eqz v0, :cond_2

    .line 142
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 144
    const/4 v0, 0x0

    .line 150
    :cond_2
    :goto_3
    const-string v6, "RCPContactsSyncWorker"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "checkNewContacts "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    const/4 v6, -0x1

    if-eq v5, v6, :cond_3

    .line 154
    iget-object v6, p2, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mBridgeService:Lcom/sec/knox/bridge/IBridgeService;

    aget v7, v4, v3

    invoke-static {p1, p2, v6, v7, v5}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncWorker;->insert(Landroid/content/Context;Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;Lcom/sec/knox/bridge/IBridgeService;II)V

    .line 114
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 106
    .end local v0    # "cursor":Landroid/database/Cursor;
    .end local v3    # "i":I
    :cond_4
    iget-object v6, p2, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mPm:Landroid/os/PersonaManager;

    invoke-virtual {v6}, Landroid/os/PersonaManager;->getPersonaIds()[I

    move-result-object v4

    goto :goto_1

    .line 134
    .restart local v0    # "cursor":Landroid/database/Cursor;
    .restart local v3    # "i":I
    :catch_0
    move-exception v2

    .line 136
    .local v2, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 140
    if-eqz v0, :cond_2

    .line 142
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 144
    const/4 v0, 0x0

    goto :goto_3

    .line 140
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v6

    if-eqz v0, :cond_5

    .line 142
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 144
    const/4 v0, 0x0

    :cond_5
    throw v6

    .line 162
    .end local v0    # "cursor":Landroid/database/Cursor;
    .end local v3    # "i":I
    :cond_6
    const-string v6, "RCPContactsSyncWorker"

    const-string v7, "checkNewContacts END"

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public getStarredInfoMap()Ljava/util/HashMap;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 766
    const/4 v6, 0x0

    .line 768
    .local v6, "starred_con":Landroid/database/Cursor;
    const-string v0, "RCPContactsSyncWorker"

    const-string v1, "getStarredInfoMap START"

    invoke-static {v0, v1}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 770
    sget-object v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncWorker;->mCtx:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/knox/rcp/components/contacts/syncer/util/DataSyncConstants;->CON_RAW_CONTACTS_URI:Landroid/net/Uri;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "_id"

    aput-object v3, v2, v7

    const-string v3, "starred"

    aput-object v3, v2, v8

    const-string v3, "deleted=0"

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 777
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncWorker;->mStarredInfoMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 779
    :goto_0
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 781
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncWorker;->mStarredInfoMap:Ljava/util/HashMap;

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v6, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 785
    :cond_0
    const-string v0, "RCPContactsSyncWorker"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getStarredInfoMap END "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncWorker;->mStarredInfoMap:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 787
    if-eqz v6, :cond_1

    .line 789
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 791
    const/4 v6, 0x0

    .line 795
    :cond_1
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncWorker;->mStarredInfoMap:Ljava/util/HashMap;

    return-object v0
.end method

.method public refreshGroupSyncMap(ILcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;)Ljava/util/HashMap;
    .locals 6
    .param p1, "userid"    # I
    .param p2, "syncer"    # Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 801
    iget-object v3, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncWorker;->mGroupSyncMaps:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/HashMap;

    .line 803
    .local v2, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    if-nez v2, :cond_0

    .line 805
    new-instance v2, Ljava/util/HashMap;

    .end local v2    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 807
    .restart local v2    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    iget-object v3, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncWorker;->mGroupSyncMaps:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 811
    :cond_0
    const/4 v1, 0x0

    .line 815
    .local v1, "group":Landroid/database/Cursor;
    :try_start_0
    iget-object v3, p2, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->contactsRcpDb:Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;

    invoke-virtual {v3}, Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "select _id_original, _id_container from CONTACTS_GROUP_SYNC_TABLE where groups_persona_id="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 819
    invoke-virtual {v2}, Ljava/util/HashMap;->clear()V

    .line 821
    :goto_0
    if-eqz v1, :cond_2

    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 823
    const/4 v3, 0x0

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x1

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 827
    :catch_0
    move-exception v0

    .line 829
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 833
    if-eqz v1, :cond_1

    .line 835
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 837
    const/4 v1, 0x0

    .line 843
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    :goto_1
    return-object v2

    .line 833
    :cond_2
    if-eqz v1, :cond_1

    .line 835
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 837
    const/4 v1, 0x0

    goto :goto_1

    .line 833
    :catchall_0
    move-exception v3

    if-eqz v1, :cond_3

    .line 835
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 837
    const/4 v1, 0x0

    :cond_3
    throw v3
.end method
