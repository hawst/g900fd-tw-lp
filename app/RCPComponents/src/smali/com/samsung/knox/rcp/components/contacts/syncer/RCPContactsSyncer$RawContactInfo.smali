.class public Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;
.super Ljava/lang/Object;
.source "RCPContactsSyncer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "RawContactInfo"
.end annotation


# instance fields
.field private _id:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private con_contactid:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private con_id:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private contactid:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private index:I

.field private name_verified:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field rawContactMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private version:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 390
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 391
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;->_id:Ljava/util/ArrayList;

    .line 392
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;->contactid:Ljava/util/ArrayList;

    .line 393
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;->version:Ljava/util/ArrayList;

    .line 394
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;->name_verified:Ljava/util/ArrayList;

    .line 396
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;->con_id:Ljava/util/ArrayList;

    .line 397
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;->con_contactid:Ljava/util/ArrayList;

    .line 399
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;->rawContactMap:Ljava/util/HashMap;

    .line 400
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;->index:I

    .line 401
    return-void
.end method


# virtual methods
.method public getIndex()I
    .locals 1

    .prologue
    .line 449
    iget v0, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;->index:I

    return v0
.end method

.method public getRawContactMap()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 422
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;->rawContactMap:Ljava/util/HashMap;

    return-object v0
.end method

.method public get_con_id()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 442
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;->con_id:Ljava/util/ArrayList;

    return-object v0
.end method

.method public get_contactid()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 430
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;->contactid:Ljava/util/ArrayList;

    return-object v0
.end method

.method public get_id()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 426
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;->_id:Ljava/util/ArrayList;

    return-object v0
.end method

.method public get_name_verified()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 438
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;->name_verified:Ljava/util/ArrayList;

    return-object v0
.end method

.method public get_version()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 434
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;->version:Ljava/util/ArrayList;

    return-object v0
.end method

.method public insertHashMap(JJ)V
    .locals 3
    .param p1, "id"    # J
    .param p3, "cId"    # J

    .prologue
    .line 418
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;->rawContactMap:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 419
    return-void
.end method

.method public insert_RawContact(JJII)V
    .locals 3
    .param p1, "id"    # J
    .param p3, "contact_id"    # J
    .param p5, "ver"    # I
    .param p6, "verified"    # I

    .prologue
    .line 404
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;->_id:Ljava/util/ArrayList;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 405
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;->contactid:Ljava/util/ArrayList;

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 406
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;->version:Ljava/util/ArrayList;

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 407
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;->name_verified:Ljava/util/ArrayList;

    invoke-static {p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 408
    return-void
.end method

.method public insert_con_id(J)V
    .locals 3
    .param p1, "id"    # J

    .prologue
    .line 411
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;->con_id:Ljava/util/ArrayList;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 412
    return-void
.end method

.method public setIndex(I)V
    .locals 0
    .param p1, "index"    # I

    .prologue
    .line 453
    iput p1, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;->index:I

    .line 454
    return-void
.end method
