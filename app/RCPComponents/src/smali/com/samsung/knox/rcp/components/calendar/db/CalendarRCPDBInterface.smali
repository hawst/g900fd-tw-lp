.class public Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;
.super Ljava/lang/Object;
.source "CalendarRCPDBInterface.java"


# instance fields
.field private final TAG:Ljava/lang/String;

.field private allGroupsColumns:[Ljava/lang/String;

.field private database:Landroid/database/sqlite/SQLiteDatabase;

.field private dbHelper:Lcom/samsung/knox/rcp/components/calendar/db/DataBaseHelper;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    const-string v0, "CalendarRCPDBInterface"

    iput-object v0, p0, Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;->TAG:Ljava/lang/String;

    .line 26
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "_id_original"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "_id_container"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "groups_version"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;->allGroupsColumns:[Ljava/lang/String;

    .line 34
    new-instance v0, Lcom/samsung/knox/rcp/components/calendar/db/DataBaseHelper;

    invoke-direct {v0, p1}, Lcom/samsung/knox/rcp/components/calendar/db/DataBaseHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;->dbHelper:Lcom/samsung/knox/rcp/components/calendar/db/DataBaseHelper;

    .line 36
    return-void
.end method


# virtual methods
.method public beginTransaction()V
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;->database:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 207
    return-void
.end method

.method public close()V
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;->dbHelper:Lcom/samsung/knox/rcp/components/calendar/db/DataBaseHelper;

    invoke-virtual {v0}, Lcom/samsung/knox/rcp/components/calendar/db/DataBaseHelper;->close()V

    .line 48
    return-void
.end method

.method public deleteEvent(Ljava/lang/String;)I
    .locals 3
    .param p1, "whereClause"    # Ljava/lang/String;

    .prologue
    .line 86
    const-string v0, "DataBaseInterface"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " deleteEvent whereClause "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;->database:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "EVENTS_SYNC_TABLE"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public deleteTask(Ljava/lang/String;)I
    .locals 3
    .param p1, "whereClause"    # Ljava/lang/String;

    .prologue
    .line 100
    const-string v0, "DataBaseInterface"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " deleteEvent whereClause "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 104
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;->database:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "TASKS_SYNC_TABLE"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 106
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public endTransaction()V
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;->database:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 211
    return-void
.end method

.method public getAllEventsForPersona(ILjava/lang/String;)Landroid/database/Cursor;
    .locals 8
    .param p1, "personaId"    # I
    .param p2, "sortBy"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 58
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;->database:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "EVENTS_SYNC_TABLE"

    const-string v3, "_id_persona=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    move-object v5, v2

    move-object v6, v2

    move-object v7, p2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public getAllTasksForPersona(ILjava/lang/String;)Landroid/database/Cursor;
    .locals 8
    .param p1, "personaId"    # I
    .param p2, "sortBy"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 72
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;->database:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "TASKS_SYNC_TABLE"

    const-string v3, "personaId=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    move-object v5, v2

    move-object v6, v2

    move-object v7, p2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public getConEventIdForOriEventId(JI)J
    .locals 13
    .param p1, "oriId"    # J
    .param p3, "personaId"    # I

    .prologue
    const-wide/16 v10, -0x1

    const/4 v2, 0x0

    .line 140
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;->database:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "EVENTS_SYNC_TABLE"

    const-string v3, "_id_original=? AND _id_persona=?"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-static/range {p3 .. p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 145
    .local v8, "c":Landroid/database/Cursor;
    if-eqz v8, :cond_2

    .line 148
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 149
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 150
    const-string v0, "_id_container"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-gez v0, :cond_0

    .line 151
    const-string v0, "CalendarRCPDBInterface"

    const-string v1, "ColumnIndex is negative"

    invoke-static {v0, v1}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 157
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    move-wide v0, v10

    .line 162
    :goto_0
    return-wide v0

    .line 154
    :cond_0
    :try_start_1
    const-string v0, "_id_container"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v0

    .line 157
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_1
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_2
    move-wide v0, v10

    .line 162
    goto :goto_0

    .line 157
    :catchall_0
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public getConTaskIdForOriTaskId(JI)J
    .locals 13
    .param p1, "oriId"    # J
    .param p3, "personaId"    # I

    .prologue
    const-wide/16 v10, -0x1

    const/4 v2, 0x0

    .line 168
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;->database:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "TASKS_SYNC_TABLE"

    const-string v3, "_id_original=? AND personaId=?"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-static/range {p3 .. p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 173
    .local v8, "c":Landroid/database/Cursor;
    if-eqz v8, :cond_2

    .line 175
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 176
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 177
    const-string v0, "_id_container"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-gez v0, :cond_0

    .line 178
    const-string v0, "CalendarRCPDBInterface"

    const-string v1, "ColumnIndex is negative"

    invoke-static {v0, v1}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 184
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    move-wide v0, v10

    .line 189
    :goto_0
    return-wide v0

    .line 181
    :cond_0
    :try_start_1
    const-string v0, "_id_container"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v0

    .line 184
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_1
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_2
    move-wide v0, v10

    .line 189
    goto :goto_0

    .line 184
    :catchall_0
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public insertEvent(JJJ)J
    .locals 5
    .param p1, "id_original"    # J
    .param p3, "id_container"    # J
    .param p5, "id_persona"    # J

    .prologue
    .line 112
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 114
    .local v0, "cv":Landroid/content/ContentValues;
    const-string v1, "_id_original"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 116
    const-string v1, "_id_container"

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 118
    const-string v1, "_id_persona"

    invoke-static {p5, p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 120
    iget-object v1, p0, Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;->database:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "EVENTS_SYNC_TABLE"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    return-wide v2
.end method

.method public insertTask(JJI)J
    .locals 5
    .param p1, "id_original"    # J
    .param p3, "id_container"    # J
    .param p5, "personaId"    # I

    .prologue
    .line 126
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 128
    .local v0, "cv":Landroid/content/ContentValues;
    const-string v1, "_id_original"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 130
    const-string v1, "_id_container"

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 132
    const-string v1, "personaId"

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 134
    iget-object v1, p0, Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;->database:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "TASKS_SYNC_TABLE"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    return-wide v2
.end method

.method public open()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/database/SQLException;
        }
    .end annotation

    .prologue
    .line 40
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;->dbHelper:Lcom/samsung/knox/rcp/components/calendar/db/DataBaseHelper;

    invoke-virtual {v0}, Lcom/samsung/knox/rcp/components/calendar/db/DataBaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;->database:Landroid/database/sqlite/SQLiteDatabase;

    .line 42
    return-void
.end method

.method public setTransactionSuccessful()V
    .locals 1

    .prologue
    .line 214
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;->database:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 215
    return-void
.end method
