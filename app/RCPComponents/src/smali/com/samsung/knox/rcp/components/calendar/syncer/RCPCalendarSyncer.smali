.class public Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;
.super Landroid/app/Service;
.source "RCPCalendarSyncer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer$3;
    }
.end annotation


# static fields
.field private static accountToKnoxId:J

.field private static isSyncState:Z

.field private static mSetLunarField:Z


# instance fields
.field private KNOX_DEBUG:Z

.field private PERFORMANCE_DEBUG:Z

.field TAG:Ljava/lang/String;

.field private calendarRcpDb:Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;

.field private mBinderProxy:Landroid/os/IBinder;

.field mBridgeService:Lcom/sec/knox/bridge/IBridgeService;

.field private mCalendarSharedPreferences:Landroid/content/SharedPreferences;

.field private mCalendarSharedPrefsEditor:Landroid/content/SharedPreferences$Editor;

.field mContext:Landroid/content/Context;

.field private mSetStartServiceCallers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mSyncThread:Ljava/lang/Thread;

.field mTaskAccountDisplayNameMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field mTaskAccountMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final name_verified:Ljava/lang/String;

.field private projMatchesEvent:I

.field private projMatchesTask:I

.field private final syncIdTag:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 71
    const-wide/16 v0, -0x1

    sput-wide v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->accountToKnoxId:J

    .line 100
    sput-boolean v2, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mSetLunarField:Z

    .line 101
    sput-boolean v2, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->isSyncState:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 62
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 64
    const-string v0, "CalendarDataSyncService"

    iput-object v0, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    .line 66
    iput-object v1, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mBridgeService:Lcom/sec/knox/bridge/IBridgeService;

    .line 67
    const-string v0, "name_verified"

    iput-object v0, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->name_verified:Ljava/lang/String;

    .line 68
    const-string v0, "local"

    iput-object v0, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->syncIdTag:Ljava/lang/String;

    .line 75
    iput v2, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->projMatchesEvent:I

    .line 76
    iput v2, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->projMatchesTask:I

    .line 82
    iput-boolean v2, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->KNOX_DEBUG:Z

    .line 83
    iput-boolean v2, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->PERFORMANCE_DEBUG:Z

    .line 85
    iput-object v1, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mCalendarSharedPreferences:Landroid/content/SharedPreferences;

    .line 86
    iput-object v1, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mCalendarSharedPrefsEditor:Landroid/content/SharedPreferences$Editor;

    .line 90
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mTaskAccountMap:Ljava/util/HashMap;

    .line 91
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mTaskAccountDisplayNameMap:Ljava/util/HashMap;

    .line 93
    iput-object v1, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mSetStartServiceCallers:Ljava/util/List;

    .line 96
    iput-object v1, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mSyncThread:Ljava/lang/Thread;

    .line 97
    iput-object v1, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mBinderProxy:Landroid/os/IBinder;

    .line 99
    iput-object v1, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->calendarRcpDb:Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mSetStartServiceCallers:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;
    .param p1, "x1"    # I

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->deletePersonaData(I)V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->stopMySelf(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$302(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 62
    sput-boolean p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->isSyncState:Z

    return p0
.end method

.method static synthetic access$400(Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;

    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->KNOX_DEBUG:Z

    return v0
.end method

.method static synthetic access$500(Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->doSync()V

    return-void
.end method

.method private addNewCalendar(I)Landroid/net/Uri;
    .locals 16
    .param p1, "userPersonaId"    # I

    .prologue
    .line 1839
    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->KNOX_DEBUG:Z

    if-eqz v12, :cond_0

    .line 1840
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    const-string v13, "addNewCalendar() called"

    invoke-static {v12, v13}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1841
    :cond_0
    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->PERFORMANCE_DEBUG:Z

    if-eqz v12, :cond_1

    .line 1842
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    const-string v13, "addNewCalendar() called"

    invoke-static {v12, v13}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1843
    :cond_1
    const/4 v9, 0x0

    .line 1844
    .local v9, "result":Landroid/net/Uri;
    const/4 v5, 0x0

    .line 1845
    .local v5, "mCalendarAccountName":Ljava/lang/String;
    if-nez p1, :cond_3

    .line 1846
    const-string v5, "My calendars (personal)"

    .line 1864
    :goto_0
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 1865
    .local v3, "calValues":Landroid/content/ContentValues;
    const-string v12, "account_name"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "calendar_personal"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v3, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1871
    const-string v12, "account_type"

    const-string v13, "LOCAL"

    invoke-virtual {v3, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1872
    const-string v12, "name"

    invoke-virtual {v3, v12, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1873
    const-string v12, "calendar_displayName"

    invoke-virtual {v3, v12, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1874
    const-string v12, "My calendars (personal)"

    invoke-virtual {v5, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 1875
    const-string v12, "calendar_color"

    const v13, -0xff0100

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v3, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1881
    :goto_1
    const-string v12, "calendar_access_level"

    const/16 v13, 0xc8

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v3, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1883
    const-string v12, "sync_events"

    const-string v13, "1"

    invoke-virtual {v3, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1884
    const-string v12, "ownerAccount"

    const/4 v13, 0x1

    invoke-static {v13}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v13

    invoke-virtual {v3, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1885
    const-string v12, "visible"

    const/4 v13, 0x1

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v3, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1889
    sget-object v2, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    .line 1890
    .local v2, "calUri":Landroid/net/Uri;
    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v12

    const-string v13, "caller_is_syncadapter"

    const-string v14, "true"

    invoke-virtual {v12, v13, v14}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v12

    const-string v13, "account_name"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "calendar_personal"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move/from16 v0, p1

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v12

    const-string v13, "account_type"

    const-string v14, "LOCAL"

    invoke-virtual {v12, v13, v14}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v12

    invoke-virtual {v12}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    .line 1901
    :try_start_0
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mContext:Landroid/content/Context;

    invoke-virtual {v12}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v12

    invoke-virtual {v12, v2, v3}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v1

    .line 1902
    .local v1, "calInsertedUri":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->KNOX_DEBUG:Z

    if-eqz v12, :cond_2

    .line 1903
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, " calInsertedUri: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 1910
    .end local v1    # "calInsertedUri":Landroid/net/Uri;
    :cond_2
    :goto_2
    return-object v1

    .line 1849
    .end local v2    # "calUri":Landroid/net/Uri;
    .end local v3    # "calValues":Landroid/content/ContentValues;
    :cond_3
    :try_start_1
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mContext:Landroid/content/Context;

    const-string v13, "persona"

    invoke-virtual {v12, v13}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/os/PersonaManager;

    .line 1851
    .local v6, "mPersona":Landroid/os/PersonaManager;
    move/from16 v0, p1

    invoke-virtual {v6, v0}, Landroid/os/PersonaManager;->getPersonaInfo(I)Landroid/content/pm/PersonaInfo;

    move-result-object v8

    .line 1854
    .local v8, "personaInfo":Landroid/content/pm/PersonaInfo;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mContext:Landroid/content/Context;

    const-string v13, "user"

    invoke-virtual {v12, v13}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/os/UserManager;

    .line 1855
    .local v11, "um":Landroid/os/UserManager;
    iget v12, v8, Landroid/content/pm/PersonaInfo;->id:I

    invoke-virtual {v11, v12}, Landroid/os/UserManager;->getUserInfo(I)Landroid/content/pm/UserInfo;

    move-result-object v10

    .line 1856
    .local v10, "ui":Landroid/content/pm/UserInfo;
    iget-object v7, v10, Landroid/content/pm/UserInfo;->name:Ljava/lang/String;

    .line 1858
    .local v7, "name":Ljava/lang/String;
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "My calendars ("

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ")"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v5

    goto/16 :goto_0

    .line 1859
    .end local v6    # "mPersona":Landroid/os/PersonaManager;
    .end local v7    # "name":Ljava/lang/String;
    .end local v8    # "personaInfo":Landroid/content/pm/PersonaInfo;
    .end local v10    # "ui":Landroid/content/pm/UserInfo;
    .end local v11    # "um":Landroid/os/UserManager;
    :catch_0
    move-exception v4

    .line 1860
    .local v4, "e":Ljava/lang/Exception;
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 1876
    .end local v4    # "e":Ljava/lang/Exception;
    .restart local v3    # "calValues":Landroid/content/ContentValues;
    :cond_4
    const-string v12, "My calendars (KNOX)"

    invoke-virtual {v5, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 1877
    const-string v12, "calendar_color"

    const v13, -0x333334

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v3, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_1

    .line 1879
    :cond_5
    const-string v12, "calendar_color"

    const v13, -0xff01

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v3, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_1

    .line 1905
    .restart local v2    # "calUri":Landroid/net/Uri;
    :catch_1
    move-exception v4

    .line 1907
    .restart local v4    # "e":Ljava/lang/Exception;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Exception while adding account to Calendar Provider Please re initialize returning false from init_makeCalendar "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-static {v4}, Lcom/samsung/knox/rcp/components/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/samsung/knox/rcp/components/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1910
    const/4 v1, 0x0

    goto/16 :goto_2
.end method

.method private declared-synchronized addNewCalendarIfRequired(I)Z
    .locals 8
    .param p1, "userOrPersonaId"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1799
    monitor-enter p0

    :try_start_0
    iget-object v5, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mCalendarSharedPreferences:Landroid/content/SharedPreferences;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mCalendarSharedPrefsEditor:Landroid/content/SharedPreferences$Editor;

    if-nez v5, :cond_1

    .line 1800
    :cond_0
    iget-object v5, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mContext:Landroid/content/Context;

    const-string v6, "created_calendars_uris_per_persona_shared_prefs"

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mCalendarSharedPreferences:Landroid/content/SharedPreferences;

    .line 1802
    iget-object v5, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mCalendarSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mCalendarSharedPrefsEditor:Landroid/content/SharedPreferences$Editor;

    .line 1804
    :cond_1
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    .line 1805
    .local v2, "userOrPersonaIdStr":Ljava/lang/String;
    iget-object v5, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mCalendarSharedPreferences:Landroid/content/SharedPreferences;

    const/4 v6, 0x0

    invoke-interface {v5, v2, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1806
    .local v1, "uriStr":Ljava/lang/String;
    if-eqz v1, :cond_3

    .line 1807
    iget-boolean v4, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->KNOX_DEBUG:Z

    if-eqz v4, :cond_2

    .line 1808
    iget-object v4, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "DataSyncService.addNewCalendarIfRequired(): calendar found for the userOrPersonaId: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "; uri = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1834
    :cond_2
    :goto_0
    monitor-exit p0

    return v3

    .line 1815
    :cond_3
    :try_start_1
    invoke-direct {p0, p1}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->addNewCalendar(I)Landroid/net/Uri;

    move-result-object v0

    .line 1817
    .local v0, "calendarUri":Landroid/net/Uri;
    if-eqz v0, :cond_5

    .line 1818
    iget-object v4, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mCalendarSharedPrefsEditor:Landroid/content/SharedPreferences$Editor;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v2, v5}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1819
    iget-object v4, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mCalendarSharedPrefsEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1821
    iget-boolean v4, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->KNOX_DEBUG:Z

    if-eqz v4, :cond_4

    .line 1822
    iget-object v4, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "DataSyncService new Calendar Added during addNewCalendarIfRequired(): "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1824
    :cond_4
    iget-boolean v4, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->KNOX_DEBUG:Z

    if-eqz v4, :cond_2

    .line 1825
    iget-object v4, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "DataSyncService new Calendar Added during addNewCalendarIfRequired() Id is  = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v0}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1799
    .end local v0    # "calendarUri":Landroid/net/Uri;
    .end local v1    # "uriStr":Ljava/lang/String;
    .end local v2    # "userOrPersonaIdStr":Ljava/lang/String;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 1830
    .restart local v0    # "calendarUri":Landroid/net/Uri;
    .restart local v1    # "uriStr":Ljava/lang/String;
    .restart local v2    # "userOrPersonaIdStr":Ljava/lang/String;
    :cond_5
    :try_start_2
    iget-boolean v3, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->KNOX_DEBUG:Z

    if-eqz v3, :cond_6

    .line 1831
    iget-object v3, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "DataSyncService.addNewCalendarIfRequired(): calendar not exists; uri = null; cannot create calendar for the userOrPersonaId: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_6
    move v3, v4

    .line 1834
    goto :goto_0
.end method

.method private addNewTask(I)Landroid/net/Uri;
    .locals 18
    .param p1, "userPersonaId"    # I

    .prologue
    .line 1982
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->KNOX_DEBUG:Z

    if-eqz v1, :cond_0

    .line 1983
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    const-string v2, "addNewTask() called"

    invoke-static {v1, v2}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1984
    :cond_0
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->PERFORMANCE_DEBUG:Z

    if-eqz v1, :cond_1

    .line 1985
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    const-string v2, "addNewTask() called"

    invoke-static {v1, v2}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1987
    :cond_1
    const/4 v8, 0x0

    .line 1988
    .local v8, "c":Landroid/database/Cursor;
    const/4 v7, 0x0

    .line 1990
    .local v7, "accountKey":I
    :try_start_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/samsung/knox/rcp/components/calendar/syncer/util/DataSyncConstants;->TASK_ACCOUNT_URI:Landroid/net/Uri;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "max(_sync_account_key)"

    aput-object v5, v3, v4

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 1994
    if-eqz v8, :cond_2

    .line 1995
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    .line 1997
    const/4 v1, 0x0

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v7

    .line 2000
    :cond_2
    if-eqz v8, :cond_3

    .line 2001
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 2002
    const/4 v8, 0x0

    .line 2005
    :cond_3
    const/4 v12, 0x0

    .line 2006
    .local v12, "mTaskAccountName":Ljava/lang/String;
    if-nez p1, :cond_6

    .line 2007
    const-string v12, "My task (personal)"

    .line 2025
    :goto_0
    new-instance v15, Landroid/content/ContentValues;

    invoke-direct {v15}, Landroid/content/ContentValues;-><init>()V

    .line 2026
    .local v15, "taskValues":Landroid/content/ContentValues;
    const-string v1, "_sync_account"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "task_personal_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v15, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2028
    const-string v1, "_sync_account_key"

    add-int/lit8 v2, v7, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v15, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2029
    const-string v1, "_sync_account_type"

    const-string v2, "LOCAL"

    invoke-virtual {v15, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2030
    const-string v1, "selected"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v15, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2031
    const-string v1, "displayName"

    invoke-virtual {v15, v1, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2035
    :try_start_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/samsung/knox/rcp/components/calendar/syncer/util/DataSyncConstants;->TASK_ACCOUNT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v2, v15}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v9

    .line 2037
    .local v9, "calInsertedUri":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mTaskAccountMap:Ljava/util/HashMap;

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    add-int/lit8 v3, v7, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2038
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mTaskAccountDisplayNameMap:Ljava/util/HashMap;

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v12}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2039
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->KNOX_DEBUG:Z

    if-eqz v1, :cond_4

    .line 2040
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " calInsertedUri: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 2047
    .end local v9    # "calInsertedUri":Landroid/net/Uri;
    :cond_4
    :goto_1
    return-object v9

    .line 2000
    .end local v12    # "mTaskAccountName":Ljava/lang/String;
    .end local v15    # "taskValues":Landroid/content/ContentValues;
    :catchall_0
    move-exception v1

    if-eqz v8, :cond_5

    .line 2001
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 2002
    const/4 v8, 0x0

    :cond_5
    throw v1

    .line 2010
    .restart local v12    # "mTaskAccountName":Ljava/lang/String;
    :cond_6
    :try_start_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mContext:Landroid/content/Context;

    const-string v2, "persona"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/os/PersonaManager;

    .line 2012
    .local v11, "mPersona":Landroid/os/PersonaManager;
    move/from16 v0, p1

    invoke-virtual {v11, v0}, Landroid/os/PersonaManager;->getPersonaInfo(I)Landroid/content/pm/PersonaInfo;

    move-result-object v14

    .line 2015
    .local v14, "personaInfo":Landroid/content/pm/PersonaInfo;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mContext:Landroid/content/Context;

    const-string v2, "user"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Landroid/os/UserManager;

    .line 2016
    .local v17, "um":Landroid/os/UserManager;
    iget v1, v14, Landroid/content/pm/PersonaInfo;->id:I

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Landroid/os/UserManager;->getUserInfo(I)Landroid/content/pm/UserInfo;

    move-result-object v16

    .line 2017
    .local v16, "ui":Landroid/content/pm/UserInfo;
    move-object/from16 v0, v16

    iget-object v13, v0, Landroid/content/pm/UserInfo;->name:Ljava/lang/String;

    .line 2019
    .local v13, "name":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "My task ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v12

    goto/16 :goto_0

    .line 2020
    .end local v11    # "mPersona":Landroid/os/PersonaManager;
    .end local v13    # "name":Ljava/lang/String;
    .end local v14    # "personaInfo":Landroid/content/pm/PersonaInfo;
    .end local v16    # "ui":Landroid/content/pm/UserInfo;
    .end local v17    # "um":Landroid/os/UserManager;
    :catch_0
    move-exception v10

    .line 2021
    .local v10, "e":Ljava/lang/Exception;
    invoke-virtual {v10}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 2042
    .end local v10    # "e":Ljava/lang/Exception;
    .restart local v15    # "taskValues":Landroid/content/ContentValues;
    :catch_1
    move-exception v10

    .line 2044
    .restart local v10    # "e":Ljava/lang/Exception;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception while adding account to Calendar Provider Please re initialize returning false from addNewTask "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v10}, Lcom/samsung/knox/rcp/components/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/knox/rcp/components/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2047
    const/4 v9, 0x0

    goto :goto_1
.end method

.method private declared-synchronized addNewTaskIfRequired(I)Z
    .locals 6
    .param p1, "userOrPersonaId"    # I

    .prologue
    .line 1964
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mTaskAccountMap:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->size()I

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mTaskAccountDisplayNameMap:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->size()I

    move-result v1

    if-nez v1, :cond_1

    .line 1966
    :cond_0
    invoke-direct {p0}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->initializeTaskAccountMap()V

    .line 1969
    :cond_1
    iget-object v1, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mTaskAccountMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1970
    const/4 v1, 0x0

    .line 1977
    :goto_0
    monitor-exit p0

    return v1

    .line 1974
    :cond_2
    :try_start_1
    invoke-direct {p0, p1}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->addNewTask(I)Landroid/net/Uri;

    move-result-object v0

    .line 1976
    .local v0, "taskUri":Landroid/net/Uri;
    iget-object v1, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "addNewTaskIfRequired() added TaskAccount _id : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1977
    const/4 v1, 0x1

    goto :goto_0

    .line 1964
    .end local v0    # "taskUri":Landroid/net/Uri;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private closeCursor(Landroid/database/Cursor;)V
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 2052
    if-nez p1, :cond_0

    .line 2059
    :goto_0
    return-void

    .line 2055
    :cond_0
    :try_start_0
    invoke-interface {p1}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2056
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private deleteCalForPersona(I)Z
    .locals 11
    .param p1, "personaID"    # I

    .prologue
    const/4 v10, 0x1

    .line 2096
    const/4 v3, 0x0

    .line 2099
    .local v3, "result":Z
    :try_start_0
    iget-boolean v4, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->KNOX_DEBUG:Z

    if-eqz v4, :cond_0

    .line 2100
    iget-object v4, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " deleteCalForPersona personaID : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2102
    :cond_0
    sget-object v0, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    .line 2103
    .local v0, "calUri":Landroid/net/Uri;
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v4

    const-string v5, "caller_is_syncadapter"

    const-string v6, "true"

    invoke-virtual {v4, v5, v6}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    const-string v5, "account_name"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "calendar_personal"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    const-string v5, "account_type"

    const-string v6, "LOCAL"

    invoke-virtual {v4, v5, v6}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 2112
    iget-object v4, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "account_name=?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "calendar_personal"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v4, v0, v5, v6}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 2117
    .local v2, "noOfDeletes":I
    iget-boolean v4, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->KNOX_DEBUG:Z

    if-eqz v4, :cond_1

    .line 2118
    iget-object v4, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "deleteCalForPersona: Number of Cal deleted (Should be 1): "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2120
    :cond_1
    if-lt v2, v10, :cond_2

    .line 2121
    const/4 v3, 0x1

    .line 2128
    .end local v0    # "calUri":Landroid/net/Uri;
    .end local v2    # "noOfDeletes":I
    :cond_2
    :goto_0
    return v3

    .line 2123
    :catch_0
    move-exception v1

    .line 2125
    .local v1, "e":Ljava/lang/Exception;
    iget-object v4, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " deleteCalForPersona : Exception while deleting Calendar "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/knox/rcp/components/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private deleteEvent(Landroid/database/Cursor;)V
    .locals 14
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    .line 1764
    iget-boolean v8, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->KNOX_DEBUG:Z

    if-eqz v8, :cond_0

    .line 1765
    iget-object v8, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    const-string v9, "deleteEvent(Cursor c) called"

    invoke-static {v8, v9}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1766
    :cond_0
    iget-object v8, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->calendarRcpDb:Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;

    const-string v8, "_id"

    invoke-interface {p1, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    invoke-interface {p1, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 1767
    .local v2, "db_id":J
    iget-object v8, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->calendarRcpDb:Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;

    const-string v8, "_id_container"

    invoke-interface {p1, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    invoke-interface {p1, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 1768
    .local v0, "con_event_id":J
    iget-object v8, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->calendarRcpDb:Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;

    const-string v8, "_id_original"

    invoke-interface {p1, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    invoke-interface {p1, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 1769
    .local v6, "orig_event_id":J
    iget-object v8, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    sget-object v9, Lcom/samsung/knox/rcp/components/calendar/syncer/util/DataSyncConstants;->CON_EVENT_URI:Landroid/net/Uri;

    const-string v10, "_id=?"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/String;

    const/4 v12, 0x0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-virtual {v8, v9, v10, v11}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v4

    .line 1773
    .local v4, "noOfDeletes":I
    iget-boolean v8, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->KNOX_DEBUG:Z

    if-eqz v8, :cond_1

    .line 1774
    iget-object v8, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "deleteEvent: Number of events deleted (should always be 1): "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1775
    :cond_1
    iget-object v8, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->calendarRcpDb:Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "_id="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;->deleteEvent(Ljava/lang/String;)I

    move-result v5

    .line 1776
    .local v5, "noOfDeletesDB":I
    iget-boolean v8, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->KNOX_DEBUG:Z

    if-eqz v8, :cond_2

    .line 1777
    iget-object v8, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "deleteEvent: Number of events deleted in local DB (should also be 1): "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1779
    :cond_2
    return-void
.end method

.method private deleteEventForPersona(I)Z
    .locals 12
    .param p1, "personaID"    # I

    .prologue
    const/4 v11, 0x1

    .line 2063
    const/4 v3, 0x0

    .line 2066
    .local v3, "result":Z
    :try_start_0
    iget-boolean v4, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->KNOX_DEBUG:Z

    if-eqz v4, :cond_0

    .line 2067
    iget-object v4, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " deleteEventForPersona personaID : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2068
    :cond_0
    iget-object v4, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    const-string v6, "account_name=?"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "calendar_personal"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {v4, v5, v6, v7}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 2074
    .local v1, "noOfDeletes":I
    iget-boolean v4, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->KNOX_DEBUG:Z

    if-eqz v4, :cond_1

    .line 2075
    iget-object v4, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "deleteEventForPersona: Number of events deleted : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2076
    :cond_1
    if-lt v1, v11, :cond_3

    .line 2077
    iget-object v4, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->calendarRcpDb:Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "_id_persona="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;->deleteEvent(Ljava/lang/String;)I

    move-result v2

    .line 2079
    .local v2, "noOfDeletesDB":I
    iget-boolean v4, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->KNOX_DEBUG:Z

    if-eqz v4, :cond_2

    .line 2080
    iget-object v4, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "deleteEventForPersona: Number of events deleted in local DB : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2082
    :cond_2
    if-ne v2, v11, :cond_3

    .line 2083
    const/4 v3, 0x1

    .line 2090
    .end local v1    # "noOfDeletes":I
    .end local v2    # "noOfDeletesDB":I
    :cond_3
    :goto_0
    return v3

    .line 2085
    :catch_0
    move-exception v0

    .line 2087
    .local v0, "e":Ljava/lang/Exception;
    iget-object v4, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " deleteEventForPersona : Exception while deleting Event "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/knox/rcp/components/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private deletePersonaData(I)V
    .locals 4
    .param p1, "personaID"    # I

    .prologue
    .line 209
    const/4 v0, 0x0

    .line 210
    .local v0, "res":Z
    iget-object v1, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " CalendarDataSyncService deletePersonaData personaID : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    invoke-direct {p0, p1}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->deleteEventForPersona(I)Z

    move-result v0

    .line 212
    iget-object v1, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " CalendarDataSyncService deletePersonaData deleteEventForPersona res : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    invoke-direct {p0, p1}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->deleteCalForPersona(I)Z

    move-result v0

    .line 214
    iget-object v1, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " CalendarDataSyncService deletePersonaData deleteCalForPersona res : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    invoke-virtual {p0, p1}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->deleteSharedPrefsForPersona(I)Z

    move-result v0

    .line 216
    iget-object v1, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " CalendarDataSyncService deletePersonaData deleteSharedPrefsForPersona res : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    invoke-direct {p0, p1}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->deleteTaskForPersona(I)Z

    move-result v0

    .line 219
    iget-object v1, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " CalendarDataSyncService deletePersonaData deleteTaskForPersona res : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    invoke-direct {p0, p1}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->deleteTaskAccountForPersona(I)Z

    move-result v0

    .line 221
    iget-object v1, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " CalendarDataSyncService deletePersonaData deleteTaskAccountForPersona res : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    return-void
.end method

.method private deleteTask(Landroid/database/Cursor;)V
    .locals 12
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    .line 1783
    iget-object v6, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->calendarRcpDb:Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;

    const-string v6, "_id"

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 1784
    .local v2, "db_id":J
    iget-object v6, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->calendarRcpDb:Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;

    const-string v6, "_id_container"

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 1785
    .local v0, "con_event_id":J
    iget-object v6, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    sget-object v7, Lcom/samsung/knox/rcp/components/calendar/syncer/util/DataSyncConstants;->TASK_URI:Landroid/net/Uri;

    const-string v8, "_id=?"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {v6, v7, v8, v9}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v4

    .line 1789
    .local v4, "noOfDeletes":I
    iget-boolean v6, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->KNOX_DEBUG:Z

    if-eqz v6, :cond_0

    .line 1790
    iget-object v6, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "deleteTask: Number of tasks deleted (should always be 1): "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1791
    :cond_0
    iget-object v6, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->calendarRcpDb:Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "_id="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;->deleteTask(Ljava/lang/String;)I

    move-result v5

    .line 1792
    .local v5, "noOfDeletesDB":I
    iget-boolean v6, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->KNOX_DEBUG:Z

    if-eqz v6, :cond_1

    .line 1793
    iget-object v6, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "deleteTask: Number of tasks deleted in local DB (should also be 1): "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1796
    :cond_1
    return-void
.end method

.method private deleteTaskAccountForPersona(I)Z
    .locals 14
    .param p1, "personaID"    # I

    .prologue
    .line 2190
    const/4 v11, 0x0

    .line 2191
    .local v11, "result":Z
    const/4 v6, 0x0

    .line 2192
    .local v6, "c":Landroid/database/Cursor;
    const/4 v9, 0x0

    .line 2196
    .local v9, "id":I
    :try_start_0
    iget-boolean v0, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->KNOX_DEBUG:Z

    if-eqz v0, :cond_0

    .line 2197
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " deleteTaskAccountForPersona personaID : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2200
    :cond_0
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/knox/rcp/components/calendar/syncer/util/DataSyncConstants;->TASK_ACCOUNT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const-string v3, "_sync_account=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "task_personal_"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 2211
    if-eqz v6, :cond_4

    .line 2213
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    .line 2214
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 2216
    sget-object v0, Lcom/samsung/knox/rcp/components/calendar/syncer/util/DataSyncConstants;->TASK_ACCOUNT_URI:Landroid/net/Uri;

    int-to-long v2, v9

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v7

    .line 2219
    .local v7, "deleteUri":Landroid/net/Uri;
    if-eqz v7, :cond_3

    .line 2220
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v7, v1, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v10

    .line 2223
    .local v10, "noOfDeletes":I
    const/4 v0, 0x1

    if-lt v10, v0, :cond_1

    .line 2224
    const/4 v11, 0x1

    .line 2241
    .end local v7    # "deleteUri":Landroid/net/Uri;
    .end local v10    # "noOfDeletes":I
    :cond_1
    :goto_0
    if-eqz v6, :cond_2

    .line 2242
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 2243
    const/4 v6, 0x0

    .line 2246
    :cond_2
    :goto_1
    return v11

    .line 2228
    .restart local v7    # "deleteUri":Landroid/net/Uri;
    :cond_3
    :try_start_1
    iget-boolean v0, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->KNOX_DEBUG:Z

    if-eqz v0, :cond_1

    .line 2229
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    const-string v1, " deleteTaskAccountForPersona deleteUri == null"

    invoke-static {v0, v1}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2237
    .end local v7    # "deleteUri":Landroid/net/Uri;
    :catch_0
    move-exception v8

    .line 2239
    .local v8, "e":Ljava/lang/Exception;
    :try_start_2
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " deleteTaskAccountForPersona : Exception while deleting Calendar "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/knox/rcp/components/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2241
    if-eqz v6, :cond_2

    .line 2242
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 2243
    const/4 v6, 0x0

    goto :goto_1

    .line 2233
    .end local v8    # "e":Ljava/lang/Exception;
    :cond_4
    :try_start_3
    iget-boolean v0, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->KNOX_DEBUG:Z

    if-eqz v0, :cond_1

    .line 2234
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    const-string v1, " deleteTaskAccountForPersona c == null"

    invoke-static {v0, v1}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 2241
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_5

    .line 2242
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 2243
    const/4 v6, 0x0

    :cond_5
    throw v0
.end method

.method private deleteTaskForPersona(I)Z
    .locals 12
    .param p1, "personaID"    # I

    .prologue
    const/4 v11, 0x1

    .line 2158
    const/4 v3, 0x0

    .line 2160
    .local v3, "result":Z
    :try_start_0
    iget-object v4, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " deleteTaskForPersona personaID : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2162
    iget-object v4, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mTaskAccountMap:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->size()I

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mTaskAccountDisplayNameMap:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->size()I

    move-result v4

    if-nez v4, :cond_1

    .line 2164
    :cond_0
    invoke-direct {p0}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->initializeTaskAccountMap()V

    .line 2167
    :cond_1
    iget-object v4, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mTaskAccountMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 2168
    iget-object v4, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Lcom/samsung/knox/rcp/components/calendar/syncer/util/DataSyncConstants;->TASK_URI:Landroid/net/Uri;

    const-string v6, "accountKey=?"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    iget-object v9, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mTaskAccountMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {v4, v5, v6, v7}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 2175
    .local v1, "noOfDeletes":I
    if-lt v1, v11, :cond_2

    .line 2176
    iget-object v4, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->calendarRcpDb:Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "personaId="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;->deleteTask(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 2179
    .local v2, "noOfDeletesDB":I
    const/4 v3, 0x1

    .line 2186
    .end local v1    # "noOfDeletes":I
    .end local v2    # "noOfDeletesDB":I
    :cond_2
    :goto_0
    return v3

    .line 2182
    :catch_0
    move-exception v0

    .line 2184
    .local v0, "e":Ljava/lang/Exception;
    iget-object v4, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " deleteTaskForPersona : Exception while deleting task "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/knox/rcp/components/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private doSync()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 265
    iget-object v3, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    const-string v4, "Bridge service bound, calling doSync()"

    invoke-static {v3, v4}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    iget-boolean v3, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->PERFORMANCE_DEBUG:Z

    if-eqz v3, :cond_0

    .line 267
    iget-object v3, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    const-string v4, "doSync() called"

    invoke-static {v3, v4}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    :cond_0
    iget-object v3, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mContext:Landroid/content/Context;

    const-string v4, "firsttimepref"

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 270
    .local v2, "firstTimePreferences":Landroid/content/SharedPreferences;
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 273
    .local v1, "firstTimeEditor":Landroid/content/SharedPreferences$Editor;
    :try_start_0
    invoke-direct {p0}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->syncTask()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 278
    :goto_0
    const-string v3, "firsttime"

    invoke-interface {v2, v3, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_2

    .line 280
    iget-object v3, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    const-string v4, "doSync() called for the first time for event"

    invoke-static {v3, v4}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    invoke-direct {p0}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->isExistLunarField()Z

    .line 282
    const-string v3, "firsttime_lunar_field"

    sget-boolean v4, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mSetLunarField:Z

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 283
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 285
    invoke-direct {p0}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->init_syncEvent()V

    .line 286
    const-string v3, "firsttime"

    const/4 v4, 0x1

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 287
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 289
    iget-object v3, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    const-string v4, "CalendarDataSyncService completed init syncProvider "

    invoke-static {v3, v4}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 294
    :goto_1
    iget-object v3, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    const-string v4, "doSync() completed"

    invoke-static {v3, v4}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 295
    iget-boolean v3, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->PERFORMANCE_DEBUG:Z

    if-eqz v3, :cond_1

    .line 296
    iget-object v3, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    const-string v4, "doSync() completed"

    invoke-static {v3, v4}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 297
    :cond_1
    return-void

    .line 274
    :catch_0
    move-exception v0

    .line 275
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 291
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_2
    invoke-direct {p0}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->syncCalendarK()V

    goto :goto_1
.end method

.method private getSetLunarField()Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 300
    iget-object v1, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mContext:Landroid/content/Context;

    const-string v2, "firsttimepref"

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 304
    .local v0, "firstTimePreferences":Landroid/content/SharedPreferences;
    const-string v1, "firsttime_lunar_field"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method

.method private init_syncEvent()V
    .locals 31

    .prologue
    .line 321
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    const-string v3, "init_syncEvent() called"

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 322
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->PERFORMANCE_DEBUG:Z

    if-eqz v2, :cond_0

    .line 323
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    const-string v3, "init_syncEvent() called"

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 324
    :cond_0
    const/4 v15, 0x0

    .line 325
    .local v15, "eventCount":I
    const/16 v30, -0x1

    .line 326
    .local v30, "userOrPersonaId":I
    const/16 v28, -0x1

    .line 327
    .local v28, "syncedCalId":I
    const/16 v27, 0x0

    .line 328
    .local v27, "syncEvent_cur":Landroid/content/CustomCursor;
    const/4 v11, 0x0

    .line 332
    .local v11, "customCursorsList":Ljava/util/List;, "Ljava/util/List<Landroid/content/CustomCursor;>;"
    :try_start_0
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->KNOX_DEBUG:Z

    if-eqz v2, :cond_1

    .line 333
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    const-string v3, "DataSyncService.init_syncEvent() changes for syncing from multiple personas "

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 335
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DataSyncService.init_syncEvent() mSetLunarField is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-boolean v4, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mSetLunarField:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 339
    :cond_1
    :try_start_1
    invoke-direct/range {p0 .. p0}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->getSetLunarField()Z

    move-result v2

    if-nez v2, :cond_5

    .line 340
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mBridgeService:Lcom/sec/knox/bridge/IBridgeService;

    const-string v3, "Calendar"

    const-string v4, "CALENDAR_EVENTS"

    const/4 v5, 0x0

    sget-object v6, Lcom/samsung/knox/rcp/components/calendar/syncer/util/DataSyncConstants;->ORI_CALENDAR_PROJECTION:[Ljava/lang/String;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const-string v9, "_id ASC"

    invoke-interface/range {v2 .. v9}, Lcom/sec/knox/bridge/IBridgeService;->queryAllProviders(Ljava/lang/String;Ljava/lang/String;I[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v11

    .line 352
    :goto_0
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->KNOX_DEBUG:Z

    if-eqz v2, :cond_2

    .line 353
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DataSyncService.init_syncEvent() mBridgeService.queryAllProviders() size =  "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 361
    :cond_2
    if-eqz v11, :cond_3

    :try_start_2
    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_6

    .line 362
    :cond_3
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->KNOX_DEBUG:Z

    if-eqz v2, :cond_4

    .line 363
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DataSyncService.init_syncEvent(): customCursorsList is null or empty: customCursorsList="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 683
    :cond_4
    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->closeCursor(Landroid/database/Cursor;)V

    .line 684
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    const-string v3, "init_syncEvent() completed"

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 686
    :goto_1
    return-void

    .line 346
    :cond_5
    :try_start_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mBridgeService:Lcom/sec/knox/bridge/IBridgeService;

    const-string v3, "Calendar"

    const-string v4, "CALENDAR_EVENTS"

    const/4 v5, 0x0

    sget-object v6, Lcom/samsung/knox/rcp/components/calendar/syncer/util/DataSyncConstants;->ORI_CALENDAR_PROJECTION_LOCAL:[Ljava/lang/String;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const-string v9, "_id ASC"

    invoke-interface/range {v2 .. v9}, Lcom/sec/knox/bridge/IBridgeService;->queryAllProviders(Ljava/lang/String;Ljava/lang/String;I[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v11

    goto :goto_0

    .line 356
    :catch_0
    move-exception v13

    .line 357
    .local v13, "e":Ljava/lang/Exception;
    :try_start_4
    invoke-virtual {v13}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 683
    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->closeCursor(Landroid/database/Cursor;)V

    .line 684
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    const-string v3, "init_syncEvent() completed"

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 369
    .end local v13    # "e":Ljava/lang/Exception;
    :cond_6
    :try_start_5
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->KNOX_DEBUG:Z

    if-eqz v2, :cond_7

    .line 370
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    const-string v3, " Iterating through customCursorsList"

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 371
    :cond_7
    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v19

    .local v19, "iterator":Ljava/util/Iterator;
    :goto_2
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 373
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Landroid/content/CustomCursor;

    move-object/from16 v27, v0

    .line 375
    if-eqz v27, :cond_28

    .line 376
    sget-boolean v2, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->isSyncState:Z

    if-nez v2, :cond_9

    .line 377
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    const-string v3, "mSyncThread interrupt "

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 683
    :cond_8
    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->closeCursor(Landroid/database/Cursor;)V

    .line 684
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    const-string v3, "init_syncEvent() completed"

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 380
    :cond_9
    :try_start_6
    invoke-virtual/range {v27 .. v27}, Landroid/content/CustomCursor;->getCount()I

    move-result v15

    .line 381
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->KNOX_DEBUG:Z

    if-eqz v2, :cond_a

    .line 382
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Number of events in syncEvent_cur: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 384
    :cond_a
    if-lez v15, :cond_27

    .line 386
    invoke-virtual/range {v27 .. v27}, Landroid/content/CustomCursor;->getCursorOwnerId()I

    move-result v30

    .line 387
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->KNOX_DEBUG:Z

    if-eqz v2, :cond_b

    .line 388
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " calling addNewCalendarIfRequired userOrPersonaId : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v30

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 390
    :cond_b
    move-object/from16 v0, p0

    move/from16 v1, v30

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->addNewCalendarIfRequired(I)Z

    move-result v25

    .line 391
    .local v25, "resAddCall":Z
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->KNOX_DEBUG:Z

    if-eqz v2, :cond_c

    .line 392
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " calling addNewCalendarIfRequired for userOrPersonaId "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v30

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " result : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v25

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 395
    :cond_c
    invoke-virtual/range {v27 .. v27}, Landroid/content/CustomCursor;->moveToFirst()Z

    .line 397
    new-instance v23, Ljava/util/ArrayList;

    invoke-direct/range {v23 .. v23}, Ljava/util/ArrayList;-><init>()V

    .line 398
    .local v23, "opsEvents":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    invoke-virtual/range {v23 .. v23}, Ljava/util/ArrayList;->clear()V

    .line 402
    sget-object v10, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    .line 403
    .local v10, "calUri":Landroid/net/Uri;
    invoke-virtual {v10}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "caller_is_syncadapter"

    const-string v4, "true"

    invoke-virtual {v2, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "account_name"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "calendar_personal"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v30

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "account_type"

    const-string v4, "LOCAL"

    invoke-virtual {v2, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v10

    .line 413
    const/16 v22, 0x0

    .line 414
    .local v22, "opsBuilderEvent":Landroid/content/ContentProviderOperation$Builder;
    new-instance v24, Ljava/util/ArrayList;

    invoke-direct/range {v24 .. v24}, Ljava/util/ArrayList;-><init>()V

    .line 415
    .local v24, "origEventIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    const/16 v17, 0x0

    .local v17, "i":I
    :goto_3
    move/from16 v0, v17

    if-ge v0, v15, :cond_d

    .line 417
    sget-boolean v2, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->isSyncState:Z

    if-nez v2, :cond_e

    .line 418
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    const-string v3, "mSyncThread interrupt "

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 676
    .end local v10    # "calUri":Landroid/net/Uri;
    .end local v17    # "i":I
    .end local v22    # "opsBuilderEvent":Landroid/content/ContentProviderOperation$Builder;
    .end local v23    # "opsEvents":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .end local v24    # "origEventIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    .end local v25    # "resAddCall":Z
    :cond_d
    :goto_4
    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->closeCursor(Landroid/database/Cursor;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_2

    .line 678
    .end local v19    # "iterator":Ljava/util/Iterator;
    :catch_1
    move-exception v13

    .line 680
    .restart local v13    # "e":Ljava/lang/Exception;
    :try_start_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception during init_syncEvent()="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v13}, Lcom/samsung/knox/rcp/components/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 683
    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->closeCursor(Landroid/database/Cursor;)V

    .line 684
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    const-string v3, "init_syncEvent() completed"

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 421
    .end local v13    # "e":Ljava/lang/Exception;
    .restart local v10    # "calUri":Landroid/net/Uri;
    .restart local v17    # "i":I
    .restart local v19    # "iterator":Ljava/util/Iterator;
    .restart local v22    # "opsBuilderEvent":Landroid/content/ContentProviderOperation$Builder;
    .restart local v23    # "opsEvents":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .restart local v24    # "origEventIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    .restart local v25    # "resAddCall":Z
    :cond_e
    :try_start_8
    invoke-static {v10}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v22

    .line 424
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mContext:Landroid/content/Context;

    const-string v3, "created_calendars_uris_per_persona_shared_prefs"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mCalendarSharedPreferences:Landroid/content/SharedPreferences;

    .line 427
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mCalendarSharedPreferences:Landroid/content/SharedPreferences;

    if-eqz v2, :cond_1d

    .line 428
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mCalendarSharedPreferences:Landroid/content/SharedPreferences;

    invoke-static/range {v30 .. v30}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    .line 430
    .local v26, "str":Ljava/lang/String;
    invoke-static/range {v26 .. v26}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v29

    .line 431
    .local v29, "uriStr":Landroid/net/Uri;
    if-eqz v29, :cond_1b

    .line 432
    invoke-static/range {v29 .. v29}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v2

    long-to-int v0, v2

    move/from16 v28, v0

    .line 433
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->KNOX_DEBUG:Z

    if-eqz v2, :cond_f

    .line 434
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " syncedCalId : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v28

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 445
    :cond_f
    new-instance v12, Landroid/content/ContentValues;

    invoke-direct {v12}, Landroid/content/ContentValues;-><init>()V

    .line 448
    .local v12, "cv":Landroid/content/ContentValues;
    const-string v2, "customAppPackage"

    const-string v3, "_id"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Landroid/content/CustomCursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v12, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 451
    const-string v2, "customAppUri"

    const-string v3, "Personal_DB"

    invoke-virtual {v12, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 452
    const-string v2, "title"

    const-string v3, "title"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v12, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 455
    const-string v2, "dtstart"

    const-string v3, "dtstart"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Landroid/content/CustomCursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v12, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 458
    const-string v2, "eventTimezone"

    const-string v3, "eventTimezone"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v12, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 461
    const-string v2, "allDay"

    const-string v3, "allDay"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Landroid/content/CustomCursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v12, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 464
    const-string v2, "rrule"

    const-string v3, "rrule"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v12, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 469
    const-string v2, "lastDate"

    move-object/from16 v0, v27

    invoke-virtual {v0, v2}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v27

    invoke-virtual {v0, v2}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_10

    .line 472
    const-string v2, "lastDate"

    const-string v3, "lastDate"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Landroid/content/CustomCursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v12, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 477
    :cond_10
    const-string v2, "calendar_id"

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v12, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 480
    const-string v2, "duration"

    move-object/from16 v0, v27

    invoke-virtual {v0, v2}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v27

    invoke-virtual {v0, v2}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1e

    .line 483
    const-string v2, "duration"

    const-string v3, "duration"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v12, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 486
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->KNOX_DEBUG:Z

    if-eqz v2, :cond_11

    .line 487
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    const-string v3, "Event has a duration"

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 497
    :cond_11
    :goto_5
    invoke-direct/range {p0 .. p0}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->getSetLunarField()Z

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_12

    .line 498
    const-string v2, "setLunar"

    const-string v3, "setLunar"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Landroid/content/CustomCursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v12, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 501
    :cond_12
    const-string v2, "selfAttendeeStatus"

    move-object/from16 v0, v27

    invoke-virtual {v0, v2}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v27

    invoke-virtual {v0, v2}, Landroid/content/CustomCursor;->getInt(I)I

    move-result v2

    if-eqz v2, :cond_13

    .line 503
    const-string v2, "selfAttendeeStatus"

    const-string v3, "selfAttendeeStatus"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Landroid/content/CustomCursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v12, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 507
    :cond_13
    const-string v2, "description"

    const-string v3, "description"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v12, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 510
    const-string v2, "eventLocation"

    const-string v3, "eventLocation"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v12, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 516
    const-string v2, "_sync_id"

    move-object/from16 v0, v27

    invoke-virtual {v0, v2}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v27

    invoke-virtual {v0, v2}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_14

    .line 518
    const-string v2, "_sync_id"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "local"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_sync_id"

    move-object/from16 v0, v27

    invoke-virtual {v0, v4}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v27

    invoke-virtual {v0, v4}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v12, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 523
    :cond_14
    const-string v2, "eventStatus"

    move-object/from16 v0, v27

    invoke-virtual {v0, v2}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v27

    invoke-virtual {v0, v2}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_15

    .line 525
    const-string v2, "eventStatus"

    const-string v3, "eventStatus"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Landroid/content/CustomCursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v12, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 529
    :cond_15
    const-string v2, "original_id"

    move-object/from16 v0, v27

    invoke-virtual {v0, v2}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v27

    invoke-virtual {v0, v2}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_19

    const-string v2, "original_sync_id"

    move-object/from16 v0, v27

    invoke-virtual {v0, v2}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v27

    invoke-virtual {v0, v2}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_19

    .line 534
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->KNOX_DEBUG:Z

    if-eqz v2, :cond_16

    .line 535
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "original_id of event titled "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "title"

    move-object/from16 v0, v27

    invoke-virtual {v0, v4}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v27

    invoke-virtual {v0, v4}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "original_id"

    move-object/from16 v0, v27

    invoke-virtual {v0, v4}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v27

    invoke-virtual {v0, v4}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " and original_sync_id is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "original_sync_id"

    move-object/from16 v0, v27

    invoke-virtual {v0, v4}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v27

    invoke-virtual {v0, v4}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 549
    :cond_16
    const/16 v16, 0x0

    .line 551
    .local v16, "find_cur":Landroid/database/Cursor;
    :try_start_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/knox/rcp/components/calendar/syncer/util/DataSyncConstants;->CON_EVENT_URI:Landroid/net/Uri;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "_id"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "_sync_id"

    aput-object v6, v4, v5

    const-string v5, "customAppPackage=?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "original_id"

    move-object/from16 v0, v27

    invoke-virtual {v0, v8}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    move-object/from16 v0, v27

    invoke-virtual {v0, v8}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const-string v7, "_id ASC"

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v16

    .line 564
    if-eqz v16, :cond_1f

    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_1f

    .line 565
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->moveToFirst()Z

    .line 566
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->KNOX_DEBUG:Z

    if-eqz v2, :cond_17

    .line 567
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "find_cur found with _id of "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_id"

    move-object/from16 v0, v16

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v16

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 571
    :cond_17
    const-string v2, "original_id"

    const-string v3, "_id"

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v12, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 574
    const-string v2, "original_sync_id"

    const-string v3, "_sync_id"

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v12, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 577
    const-string v2, "originalInstanceTime"

    const-string v3, "originalInstanceTime"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Landroid/content/CustomCursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v12, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 580
    const-string v2, "originalAllDay"

    const-string v3, "originalAllDay"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Landroid/content/CustomCursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v12, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_2
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 597
    :cond_18
    :goto_6
    if-eqz v16, :cond_19

    .line 598
    :try_start_a
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    .line 602
    .end local v16    # "find_cur":Landroid/database/Cursor;
    :cond_19
    move-object/from16 v0, v22

    invoke-virtual {v0, v12}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    .line 603
    invoke-virtual/range {v22 .. v22}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 604
    const/16 v22, 0x0

    .line 605
    const-string v2, "_id"

    move-object/from16 v0, v27

    invoke-virtual {v0, v2}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v27

    invoke-virtual {v0, v2}, Landroid/content/CustomCursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object/from16 v0, v24

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 607
    add-int/lit8 v2, v17, 0x1

    rem-int/lit8 v2, v2, 0x14
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_1
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    if-eqz v2, :cond_1a

    add-int/lit8 v2, v17, 0x1

    if-ne v2, v15, :cond_23

    .line 608
    :cond_1a
    const/16 v21, 0x0

    .line 609
    .local v21, "newBatchLimit":I
    const/16 v18, 0x0

    .line 611
    .local v18, "insertedIds":[Landroid/content/ContentProviderResult;
    :try_start_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "com.android.calendar"

    move-object/from16 v0, v23

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    move-result-object v18

    .line 613
    move-object/from16 v0, v18

    array-length v0, v0

    move/from16 v21, v0

    .line 614
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->calendarRcpDb:Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;

    invoke-virtual {v2}, Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;->beginTransaction()V

    .line 615
    const/16 v20, 0x0

    .local v20, "j":I
    :goto_7
    move/from16 v0, v20

    move/from16 v1, v21

    if-ge v0, v1, :cond_22

    .line 616
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->calendarRcpDb:Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;

    move-object/from16 v0, v24

    move/from16 v1, v20

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    aget-object v2, v18, v20

    iget-object v2, v2, Landroid/content/ContentProviderResult;->uri:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v6

    move/from16 v0, v30

    int-to-long v8, v0

    invoke-virtual/range {v3 .. v9}, Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;->insertEvent(JJJ)J
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_3
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    .line 615
    add-int/lit8 v20, v20, 0x1

    goto :goto_7

    .line 436
    .end local v12    # "cv":Landroid/content/ContentValues;
    .end local v18    # "insertedIds":[Landroid/content/ContentProviderResult;
    .end local v20    # "j":I
    .end local v21    # "newBatchLimit":I
    :cond_1b
    :try_start_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    const-string v3, " uriStr null .. skipping "

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 415
    .end local v26    # "str":Ljava/lang/String;
    .end local v29    # "uriStr":Landroid/net/Uri;
    :cond_1c
    :goto_8
    add-int/lit8 v17, v17, 0x1

    goto/16 :goto_3

    .line 440
    :cond_1d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    const-string v3, " mCalendarSharedPreferences null .. skipping "

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_1
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    goto :goto_8

    .line 683
    .end local v10    # "calUri":Landroid/net/Uri;
    .end local v17    # "i":I
    .end local v19    # "iterator":Ljava/util/Iterator;
    .end local v22    # "opsBuilderEvent":Landroid/content/ContentProviderOperation$Builder;
    .end local v23    # "opsEvents":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .end local v24    # "origEventIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    .end local v25    # "resAddCall":Z
    :catchall_0
    move-exception v2

    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->closeCursor(Landroid/database/Cursor;)V

    .line 684
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    const-string v4, "init_syncEvent() completed"

    invoke-static {v3, v4}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    throw v2

    .line 491
    .restart local v10    # "calUri":Landroid/net/Uri;
    .restart local v12    # "cv":Landroid/content/ContentValues;
    .restart local v17    # "i":I
    .restart local v19    # "iterator":Ljava/util/Iterator;
    .restart local v22    # "opsBuilderEvent":Landroid/content/ContentProviderOperation$Builder;
    .restart local v23    # "opsEvents":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .restart local v24    # "origEventIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    .restart local v25    # "resAddCall":Z
    .restart local v26    # "str":Ljava/lang/String;
    .restart local v29    # "uriStr":Landroid/net/Uri;
    :cond_1e
    :try_start_d
    const-string v2, "dtend"

    const-string v3, "dtend"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Landroid/content/CustomCursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v12, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 494
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->KNOX_DEBUG:Z

    if-eqz v2, :cond_11

    .line 495
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    const-string v3, "Event has a dtend"

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_1
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    goto/16 :goto_5

    .line 585
    .restart local v16    # "find_cur":Landroid/database/Cursor;
    :cond_1f
    :try_start_e
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->KNOX_DEBUG:Z

    if-eqz v2, :cond_18

    .line 586
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    if-nez v16, :cond_20

    const-string v2, "find_cur is null"

    :goto_9
    invoke-static {v3, v2}, Lcom/samsung/knox/rcp/components/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_2
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    goto/16 :goto_6

    .line 589
    :catch_2
    move-exception v13

    .line 591
    .restart local v13    # "e":Ljava/lang/Exception;
    :try_start_f
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception during init_syncEvent(), find_cur="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v13}, Lcom/samsung/knox/rcp/components/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_1

    .line 597
    if-eqz v16, :cond_1c

    .line 598
    :try_start_10
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_1
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    goto :goto_8

    .line 586
    .end local v13    # "e":Ljava/lang/Exception;
    :cond_20
    :try_start_11
    const-string v2, "find_cur has count 0"
    :try_end_11
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_2
    .catchall {:try_start_11 .. :try_end_11} :catchall_1

    goto :goto_9

    .line 597
    :catchall_1
    move-exception v2

    if-eqz v16, :cond_21

    .line 598
    :try_start_12
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    :cond_21
    throw v2
    :try_end_12
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_12} :catch_1
    .catchall {:try_start_12 .. :try_end_12} :catchall_0

    .line 629
    .end local v16    # "find_cur":Landroid/database/Cursor;
    .restart local v18    # "insertedIds":[Landroid/content/ContentProviderResult;
    .restart local v20    # "j":I
    .restart local v21    # "newBatchLimit":I
    :cond_22
    :try_start_13
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->calendarRcpDb:Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;

    invoke-virtual {v2}, Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;->setTransactionSuccessful()V
    :try_end_13
    .catch Ljava/lang/Exception; {:try_start_13 .. :try_end_13} :catch_3
    .catchall {:try_start_13 .. :try_end_13} :catchall_2

    .line 662
    :try_start_14
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->calendarRcpDb:Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;

    invoke-virtual {v2}, Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;->endTransaction()V

    .line 663
    new-instance v24, Ljava/util/ArrayList;

    .end local v24    # "origEventIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    invoke-direct/range {v24 .. v24}, Ljava/util/ArrayList;-><init>()V

    .line 664
    .restart local v24    # "origEventIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    if-eqz v23, :cond_23

    .line 665
    invoke-virtual/range {v23 .. v23}, Ljava/util/ArrayList;->clear()V

    .line 668
    .end local v18    # "insertedIds":[Landroid/content/ContentProviderResult;
    .end local v20    # "j":I
    .end local v21    # "newBatchLimit":I
    :cond_23
    :goto_a
    invoke-virtual/range {v27 .. v27}, Landroid/content/CustomCursor;->moveToNext()Z
    :try_end_14
    .catch Ljava/lang/Exception; {:try_start_14 .. :try_end_14} :catch_1
    .catchall {:try_start_14 .. :try_end_14} :catchall_0

    goto/16 :goto_8

    .line 630
    .restart local v18    # "insertedIds":[Landroid/content/ContentProviderResult;
    .restart local v21    # "newBatchLimit":I
    :catch_3
    move-exception v13

    .line 632
    .restart local v13    # "e":Ljava/lang/Exception;
    :try_start_15
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception during inserting event, (original _id:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_id"

    move-object/from16 v0, v27

    invoke-virtual {v0, v4}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v27

    invoke-virtual {v0, v4}, Landroid/content/CustomCursor;->getLong(I)J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " exp = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v13}, Lcom/samsung/knox/rcp/components/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 638
    if-eqz v23, :cond_24

    .line 639
    invoke-virtual/range {v23 .. v23}, Ljava/util/ArrayList;->clear()V
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_2

    .line 642
    :cond_24
    const/16 v20, 0x0

    .restart local v20    # "j":I
    :goto_b
    move/from16 v0, v20

    move/from16 v1, v21

    if-ge v0, v1, :cond_25

    .line 651
    :try_start_16
    aget-object v2, v18, v20

    iget-object v2, v2, Landroid/content/ContentProviderResult;->uri:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 642
    add-int/lit8 v20, v20, 0x1

    goto :goto_b

    .line 654
    :cond_25
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "com.android.calendar"

    move-object/from16 v0, v23

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_16
    .catch Ljava/lang/Exception; {:try_start_16 .. :try_end_16} :catch_4
    .catchall {:try_start_16 .. :try_end_16} :catchall_2

    .line 662
    :goto_c
    :try_start_17
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->calendarRcpDb:Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;

    invoke-virtual {v2}, Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;->endTransaction()V

    .line 663
    new-instance v24, Ljava/util/ArrayList;

    .end local v24    # "origEventIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    invoke-direct/range {v24 .. v24}, Ljava/util/ArrayList;-><init>()V

    .line 664
    .restart local v24    # "origEventIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    if-eqz v23, :cond_23

    .line 665
    invoke-virtual/range {v23 .. v23}, Ljava/util/ArrayList;->clear()V
    :try_end_17
    .catch Ljava/lang/Exception; {:try_start_17 .. :try_end_17} :catch_1
    .catchall {:try_start_17 .. :try_end_17} :catchall_0

    goto/16 :goto_a

    .line 656
    :catch_4
    move-exception v14

    .line 657
    .local v14, "e1":Ljava/lang/Exception;
    :try_start_18
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " Exception during deleteing inserted event, exp = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v13}, Lcom/samsung/knox/rcp/components/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_18
    .catchall {:try_start_18 .. :try_end_18} :catchall_2

    goto :goto_c

    .line 662
    .end local v13    # "e":Ljava/lang/Exception;
    .end local v14    # "e1":Ljava/lang/Exception;
    .end local v20    # "j":I
    :catchall_2
    move-exception v2

    :try_start_19
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->calendarRcpDb:Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;

    invoke-virtual {v3}, Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;->endTransaction()V

    .line 663
    new-instance v24, Ljava/util/ArrayList;

    .end local v24    # "origEventIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    invoke-direct/range {v24 .. v24}, Ljava/util/ArrayList;-><init>()V

    .line 664
    .restart local v24    # "origEventIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    if-eqz v23, :cond_26

    .line 665
    invoke-virtual/range {v23 .. v23}, Ljava/util/ArrayList;->clear()V

    :cond_26
    throw v2

    .line 671
    .end local v10    # "calUri":Landroid/net/Uri;
    .end local v12    # "cv":Landroid/content/ContentValues;
    .end local v17    # "i":I
    .end local v18    # "insertedIds":[Landroid/content/ContentProviderResult;
    .end local v21    # "newBatchLimit":I
    .end local v22    # "opsBuilderEvent":Landroid/content/ContentProviderOperation$Builder;
    .end local v23    # "opsEvents":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .end local v24    # "origEventIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    .end local v25    # "resAddCall":Z
    .end local v26    # "str":Ljava/lang/String;
    .end local v29    # "uriStr":Landroid/net/Uri;
    :cond_27
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    const-string v3, " count <= 0"

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 674
    :cond_28
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    const-string v3, "null syncEvent_cur"

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_19
    .catch Ljava/lang/Exception; {:try_start_19 .. :try_end_19} :catch_1
    .catchall {:try_start_19 .. :try_end_19} :catchall_0

    goto/16 :goto_4
.end method

.method private initializeTaskAccountMap()V
    .locals 10

    .prologue
    .line 1926
    const/4 v7, 0x0

    .line 1928
    .local v7, "c":Landroid/database/Cursor;
    const/4 v6, 0x0

    .line 1929
    .local v6, "accountKey":I
    const/4 v9, 0x0

    .line 1930
    .local v9, "personaId":I
    const/4 v8, 0x0

    .line 1933
    .local v8, "displayName":Ljava/lang/String;
    :try_start_0
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/knox/rcp/components/calendar/syncer/util/DataSyncConstants;->TASK_ACCOUNT_URI:Landroid/net/Uri;

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "_sync_account_key"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "_sync_account"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "displayName"

    aput-object v4, v2, v3

    const-string v3, "_sync_account like \'task_personal_%\'"

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 1941
    :goto_0
    if-eqz v7, :cond_2

    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1942
    const/4 v0, 0x1

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 1943
    const/4 v0, 0x2

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->parseAccountKey(Ljava/lang/String;)I

    move-result v9

    .line 1944
    const/4 v0, 0x3

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 1946
    const/4 v0, -0x1

    if-eq v9, v0, :cond_0

    if-eqz v8, :cond_0

    .line 1947
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mTaskAccountMap:Ljava/util/HashMap;

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1948
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mTaskAccountDisplayNameMap:Ljava/util/HashMap;

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1950
    :cond_0
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "TaskAccountMap : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mTaskAccountMap:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1951
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mTaskAccountDisplayNameMap : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mTaskAccountDisplayNameMap:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1954
    :catchall_0
    move-exception v0

    if-eqz v7, :cond_1

    .line 1955
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 1956
    const/4 v7, 0x0

    :cond_1
    throw v0

    .line 1954
    :cond_2
    if-eqz v7, :cond_3

    .line 1955
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 1956
    const/4 v7, 0x0

    .line 1959
    :cond_3
    return-void
.end method

.method private insertEvent(Ljava/util/ArrayList;Ljava/util/ArrayList;Landroid/content/CustomCursor;II)V
    .locals 14
    .param p3, "origEventCur"    # Landroid/content/CustomCursor;
    .param p4, "calId"    # I
    .param p5, "personaId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Landroid/content/CustomCursor;",
            "II)V"
        }
    .end annotation

    .prologue
    .line 1391
    .local p1, "opsEvents":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .local p2, "origEventIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    iget-boolean v2, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->KNOX_DEBUG:Z

    if-eqz v2, :cond_0

    .line 1392
    iget-object v2, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "insertEvent(CustomCursor "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") called"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1393
    :cond_0
    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    .line 1394
    .local v9, "cv":Landroid/content/ContentValues;
    const/4 v12, 0x0

    .line 1395
    .local v12, "opsBuilderEvent":Landroid/content/ContentProviderOperation$Builder;
    const-string v2, "customAppPackage"

    const-string v3, "_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v9, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1397
    const-string v2, "customAppUri"

    const-string v3, "Personal_DB"

    invoke-virtual {v9, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1398
    const-string v2, "title"

    const-string v3, "title"

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v9, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1400
    const-string v2, "dtstart"

    const-string v3, "dtstart"

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/content/CustomCursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v9, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1402
    const-string v2, "eventTimezone"

    const-string v3, "eventTimezone"

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v9, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1404
    const-string v2, "allDay"

    const-string v3, "allDay"

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/content/CustomCursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v9, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1406
    const-string v2, "rrule"

    const-string v3, "rrule"

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v9, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1409
    const-string v2, "lastDate"

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 1410
    const-string v2, "lastDate"

    const-string v3, "lastDate"

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/content/CustomCursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v9, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1412
    iget-boolean v2, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->KNOX_DEBUG:Z

    if-eqz v2, :cond_1

    .line 1413
    iget-object v2, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    const-string v3, "insertEvent: Event has a last date"

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1415
    :cond_1
    const-string v2, "calendar_id"

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v9, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1416
    iget-boolean v2, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->KNOX_DEBUG:Z

    if-eqz v2, :cond_2

    .line 1417
    iget-object v2, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "inserting event to calendar id : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p4

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1418
    :cond_2
    const-string v2, "duration"

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_e

    .line 1419
    const-string v2, "duration"

    const-string v3, "duration"

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v9, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1421
    iget-boolean v2, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->KNOX_DEBUG:Z

    if-eqz v2, :cond_3

    .line 1422
    iget-object v2, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    const-string v3, "insertEvent: Event has duration"

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1430
    :cond_3
    :goto_0
    const-string v2, "description"

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 1432
    const-string v2, "description"

    const-string v3, "description"

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v9, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1435
    :cond_4
    const-string v2, "eventLocation"

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_5

    .line 1437
    const-string v2, "eventLocation"

    const-string v3, "eventLocation"

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v9, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1440
    :cond_5
    const-string v2, "_sync_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_6

    .line 1441
    const-string v2, "_sync_id"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "local"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_sync_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v9, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1446
    :cond_6
    const-string v2, "eventStatus"

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_7

    .line 1447
    const-string v2, "eventStatus"

    const-string v3, "eventStatus"

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/content/CustomCursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v9, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1450
    :cond_7
    invoke-direct {p0}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->getSetLunarField()Z

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_8

    .line 1451
    const-string v2, "setLunar"

    const-string v3, "setLunar"

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/content/CustomCursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v9, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1453
    :cond_8
    const-string v2, "selfAttendeeStatus"

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/content/CustomCursor;->getInt(I)I

    move-result v2

    if-eqz v2, :cond_9

    .line 1455
    const-string v2, "selfAttendeeStatus"

    const-string v3, "selfAttendeeStatus"

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/content/CustomCursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v9, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1458
    :cond_9
    const-string v2, "original_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_c

    const-string v2, "original_sync_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_c

    .line 1462
    iget-boolean v2, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->KNOX_DEBUG:Z

    if-eqz v2, :cond_a

    .line 1463
    iget-object v2, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "original_id of event titled "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "title"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "original_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " and original_sync_id is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "original_sync_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1474
    :cond_a
    const/4 v11, 0x0

    .line 1476
    .local v11, "find_cur":Landroid/database/Cursor;
    :try_start_0
    iget-object v2, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/knox/rcp/components/calendar/syncer/util/DataSyncConstants;->CON_EVENT_URI:Landroid/net/Uri;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "_id"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "_sync_id"

    aput-object v6, v4, v5

    const-string v5, "customAppPackage=?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v13, "original_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v13}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v13

    move-object/from16 v0, p3

    invoke-virtual {v0, v13}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    aput-object v13, v6, v7

    const-string v7, "_id ASC"

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 1486
    if-eqz v11, :cond_b

    invoke-interface {v11}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_b

    .line 1487
    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1488
    const-string v2, "original_id"

    const-string v3, "_id"

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v9, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1490
    const-string v2, "original_sync_id"

    const-string v3, "_sync_id"

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v9, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1492
    const-string v2, "originalInstanceTime"

    const-string v3, "originalInstanceTime"

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/content/CustomCursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v9, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1495
    const-string v2, "originalAllDay"

    const-string v3, "originalAllDay"

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/content/CustomCursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v9, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1504
    :cond_b
    if-eqz v11, :cond_c

    .line 1505
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 1508
    .end local v11    # "find_cur":Landroid/database/Cursor;
    :cond_c
    sget-object v8, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    .line 1509
    .local v8, "calUri":Landroid/net/Uri;
    invoke-virtual {v8}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "caller_is_syncadapter"

    const-string v4, "true"

    invoke-virtual {v2, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "account_name"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "calendar_personal"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, p5

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "account_type"

    const-string v4, "LOCAL"

    invoke-virtual {v2, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v8

    .line 1517
    invoke-static {v8}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    .line 1518
    invoke-virtual {v12, v9}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    .line 1519
    invoke-virtual {v12}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1520
    const-string v2, "_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/content/CustomCursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1522
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/16 v3, 0x14

    if-ne v2, v3, :cond_d

    .line 1523
    move-object/from16 v0, p2

    move/from16 v1, p5

    invoke-virtual {p0, p1, v0, v1}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->insertEventBatch(Ljava/util/ArrayList;Ljava/util/ArrayList;I)V

    .line 1524
    invoke-virtual {p1}, Ljava/util/ArrayList;->clear()V

    .line 1525
    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->clear()V

    .line 1527
    .end local v8    # "calUri":Landroid/net/Uri;
    :cond_d
    :goto_1
    return-void

    .line 1425
    :cond_e
    const-string v2, "dtend"

    const-string v3, "dtend"

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/content/CustomCursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v9, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1427
    iget-boolean v2, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->KNOX_DEBUG:Z

    if-eqz v2, :cond_3

    .line 1428
    iget-object v2, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    const-string v3, "insertEvent: Event has a dtend"

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1499
    .restart local v11    # "find_cur":Landroid/database/Cursor;
    :catch_0
    move-exception v10

    .line 1501
    .local v10, "e":Ljava/lang/Exception;
    :try_start_1
    iget-object v2, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception during init_syncEvent()="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v10}, Lcom/samsung/knox/rcp/components/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1504
    if-eqz v11, :cond_d

    .line 1505
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 1504
    .end local v10    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    if-eqz v11, :cond_f

    .line 1505
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    :cond_f
    throw v2
.end method

.method private insertTask(Ljava/util/ArrayList;Ljava/util/ArrayList;Landroid/content/CustomCursor;I)V
    .locals 8
    .param p3, "c"    # Landroid/content/CustomCursor;
    .param p4, "userOrPersonaId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Landroid/content/CustomCursor;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 1624
    .local p1, "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .local p2, "oriId":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 1626
    .local v4, "val":Landroid/content/ContentValues;
    if-eqz p3, :cond_7

    .line 1630
    invoke-virtual {p3}, Landroid/content/CustomCursor;->getColumnCount()I

    move-result v0

    .line 1631
    .local v0, "columncnt":I
    invoke-virtual {p3}, Landroid/content/CustomCursor;->getColumnNames()[Ljava/lang/String;

    move-result-object v1

    .line 1633
    .local v1, "columnnames":[Ljava/lang/String;
    const/4 v2, 0x1

    .local v2, "i":I
    :goto_0
    if-ge v2, v0, :cond_5

    .line 1635
    const-string v5, "due_date"

    aget-object v6, v1, v2

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1636
    invoke-virtual {p3, v2}, Landroid/content/CustomCursor;->getType(I)I

    move-result v5

    if-eqz v5, :cond_0

    .line 1637
    const-string v5, "due_date"

    invoke-virtual {p3, v2}, Landroid/content/CustomCursor;->getLong(I)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1633
    :cond_0
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1640
    :cond_1
    const-string v5, "utc_due_date"

    aget-object v6, v1, v2

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1641
    invoke-virtual {p3, v2}, Landroid/content/CustomCursor;->getType(I)I

    move-result v5

    if-eqz v5, :cond_0

    .line 1642
    const-string v5, "utc_due_date"

    invoke-virtual {p3, v2}, Landroid/content/CustomCursor;->getLong(I)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    goto :goto_1

    .line 1645
    :cond_2
    const-string v5, "task_order"

    aget-object v6, v1, v2

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1647
    const-string v5, "previousId"

    aget-object v6, v1, v2

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1649
    const-string v5, "accountKey"

    aget-object v6, v1, v2

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1650
    const-string v6, "accountKey"

    iget-object v5, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mTaskAccountMap:Ljava/util/HashMap;

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v4, v6, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_1

    .line 1652
    :cond_3
    const-string v5, "accountName"

    aget-object v6, v1, v2

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1653
    const-string v6, "accountName"

    iget-object v5, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mTaskAccountDisplayNameMap:Ljava/util/HashMap;

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v4, v6, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1657
    :cond_4
    aget-object v5, v1, v2

    invoke-direct {p0, v4, p3, v2, v5}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->setContentValue(Landroid/content/ContentValues;Landroid/database/Cursor;ILjava/lang/String;)V

    goto :goto_1

    .line 1661
    :cond_5
    sget-object v5, Lcom/samsung/knox/rcp/components/calendar/syncer/util/DataSyncConstants;->TASK_URI:Landroid/net/Uri;

    invoke-static {v5}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1663
    const/4 v5, 0x0

    invoke-virtual {p3, v5}, Landroid/content/CustomCursor;->getLong(I)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {p2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1664
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 1665
    .local v3, "opscnt":I
    const/16 v5, 0x14

    if-le v3, v5, :cond_6

    .line 1666
    invoke-direct {p0, p1, p2, p4}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->insertTasktoDB(Ljava/util/ArrayList;Ljava/util/ArrayList;I)V

    .line 1672
    :cond_6
    invoke-virtual {v4}, Landroid/content/ContentValues;->clear()V

    .line 1675
    .end local v0    # "columncnt":I
    .end local v1    # "columnnames":[Ljava/lang/String;
    .end local v2    # "i":I
    .end local v3    # "opscnt":I
    :cond_7
    const/4 v4, 0x0

    .line 1676
    return-void
.end method

.method private insertTasktoDB(Ljava/util/ArrayList;Ljava/util/ArrayList;I)V
    .locals 10
    .param p3, "userOrPersonaId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 1680
    .local p1, "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .local p2, "oriId":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1682
    :try_start_0
    iget-object v1, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "com.android.calendar"

    invoke-virtual {v1, v2, p1}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    move-result-object v7

    .line 1685
    .local v7, "cpr":[Landroid/content/ContentProviderResult;
    array-length v0, v7

    .line 1687
    .local v0, "count":I
    iget-object v1, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->calendarRcpDb:Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;

    invoke-virtual {v1}, Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;->beginTransaction()V

    .line 1688
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_0
    if-ge v9, v0, :cond_0

    .line 1689
    iget-object v1, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->calendarRcpDb:Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;

    invoke-virtual {p2, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    aget-object v4, v7, v9

    iget-object v4, v4, Landroid/content/ContentProviderResult;->uri:Landroid/net/Uri;

    invoke-static {v4}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v4

    move v6, p3

    invoke-virtual/range {v1 .. v6}, Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;->insertTask(JJI)J

    .line 1688
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 1693
    :cond_0
    iget-object v1, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->calendarRcpDb:Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;

    invoke-virtual {v1}, Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;->setTransactionSuccessful()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1697
    iget-object v1, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->calendarRcpDb:Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;

    invoke-virtual {v1}, Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;->endTransaction()V

    .line 1698
    invoke-virtual {p1}, Ljava/util/ArrayList;->clear()V

    .line 1699
    invoke-virtual {p2}, Ljava/util/ArrayList;->clear()V

    .line 1702
    .end local v0    # "count":I
    .end local v7    # "cpr":[Landroid/content/ContentProviderResult;
    .end local v9    # "i":I
    :cond_1
    :goto_1
    return-void

    .line 1694
    :catch_0
    move-exception v8

    .line 1695
    .local v8, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1697
    iget-object v1, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->calendarRcpDb:Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;

    invoke-virtual {v1}, Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;->endTransaction()V

    .line 1698
    invoke-virtual {p1}, Ljava/util/ArrayList;->clear()V

    .line 1699
    invoke-virtual {p2}, Ljava/util/ArrayList;->clear()V

    goto :goto_1

    .line 1697
    .end local v8    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    iget-object v2, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->calendarRcpDb:Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;

    invoke-virtual {v2}, Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;->endTransaction()V

    .line 1698
    invoke-virtual {p1}, Ljava/util/ArrayList;->clear()V

    .line 1699
    invoke-virtual {p2}, Ljava/util/ArrayList;->clear()V

    throw v1
.end method

.method private isBridgeServiceValid()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 245
    iget-object v1, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mBridgeService:Lcom/sec/knox/bridge/IBridgeService;

    if-nez v1, :cond_0

    .line 246
    iget-object v1, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    const-string v2, "isBridgeServiceValid(): mBridgeService == null"

    invoke-static {v1, v2}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    :goto_0
    return v0

    .line 248
    :cond_0
    iget-object v1, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mBinderProxy:Landroid/os/IBinder;

    if-nez v1, :cond_1

    .line 249
    iget-object v1, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    const-string v2, "isBridgeServiceValid(): mBinderProxy == null"

    invoke-static {v1, v2}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 251
    :cond_1
    iget-object v1, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mBinderProxy:Landroid/os/IBinder;

    invoke-interface {v1}, Landroid/os/IBinder;->pingBinder()Z

    move-result v1

    if-nez v1, :cond_2

    .line 252
    iget-object v1, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    const-string v2, "isBridgeServiceValid(): mBinderProxy.pingBinder() == false"

    invoke-static {v1, v2}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 254
    :cond_2
    iget-object v1, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mBinderProxy:Landroid/os/IBinder;

    invoke-interface {v1}, Landroid/os/IBinder;->isBinderAlive()Z

    move-result v1

    if-nez v1, :cond_3

    .line 255
    iget-object v1, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    const-string v2, "isBridgeServiceValid(): mBinderProxy.isBinderAlive() == false"

    invoke-static {v1, v2}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 258
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private isExistLunarField()Z
    .locals 9

    .prologue
    .line 2296
    const/4 v6, 0x0

    .line 2298
    .local v6, "cv":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 2301
    const-string v0, "setLunar"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    .line 2303
    .local v8, "num":I
    const/4 v0, -0x1

    if-ne v8, v0, :cond_1

    .line 2304
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mSetLunarField:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2312
    :goto_0
    if-eqz v6, :cond_0

    .line 2313
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 2315
    :cond_0
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isExistLunarField() == "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mSetLunarField:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2317
    .end local v8    # "num":I
    :goto_1
    sget-boolean v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mSetLunarField:Z

    return v0

    .line 2306
    .restart local v8    # "num":I
    :cond_1
    const/4 v0, 0x1

    :try_start_1
    sput-boolean v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mSetLunarField:Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2308
    .end local v8    # "num":I
    :catch_0
    move-exception v7

    .line 2309
    .local v7, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    .line 2310
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mSetLunarField:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2312
    if-eqz v6, :cond_2

    .line 2313
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 2315
    :cond_2
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isExistLunarField() == "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mSetLunarField:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 2312
    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    .line 2313
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 2315
    :cond_3
    iget-object v1, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isExistLunarField() == "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-boolean v3, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mSetLunarField:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    throw v0
.end method

.method private parseAccountKey(Ljava/lang/String;)I
    .locals 4
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 1916
    const/4 v0, 0x0

    .line 1918
    .local v0, "ret":I
    const-string v1, "_"

    invoke-virtual {p1, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 1920
    iget-object v1, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "parseAccountKey "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1922
    return v0
.end method

.method private declared-synchronized registerRequestStartService(Ljava/lang/String;)V
    .locals 3
    .param p1, "whoWantToStart"    # Ljava/lang/String;

    .prologue
    .line 2251
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->KNOX_DEBUG:Z

    if-eqz v0, :cond_0

    .line 2252
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "registerRequestStartService():  whoWantToStart="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2254
    :cond_0
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mSetStartServiceCallers:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 2255
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mSetStartServiceCallers:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2257
    :cond_1
    monitor-exit p0

    return-void

    .line 2251
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private setContentValue(Landroid/content/ContentValues;Landroid/database/Cursor;ILjava/lang/String;)V
    .locals 1
    .param p1, "val"    # Landroid/content/ContentValues;
    .param p2, "c"    # Landroid/database/Cursor;
    .param p3, "index"    # I
    .param p4, "name"    # Ljava/lang/String;

    .prologue
    .line 1604
    invoke-interface {p2, p3}, Landroid/database/Cursor;->getType(I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1620
    :goto_0
    return-void

    .line 1606
    :pswitch_0
    invoke-interface {p2, p3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {p1, p4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    goto :goto_0

    .line 1609
    :pswitch_1
    invoke-interface {p2, p3}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, p4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_0

    .line 1612
    :pswitch_2
    invoke-virtual {p1, p4}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto :goto_0

    .line 1615
    :pswitch_3
    invoke-interface {p2, p3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, p4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1604
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method private startSyncingProcess()V
    .locals 2

    .prologue
    .line 226
    invoke-direct {p0}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->isBridgeServiceValid()Z

    move-result v0

    if-nez v0, :cond_0

    .line 227
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    const-string v1, "startSyncingProcess() failed; mBridgeService is not valid."

    invoke-static {v0, v1}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    :cond_0
    const-string v0, "DO_SYNC"

    invoke-direct {p0, v0}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->registerRequestStartService(Ljava/lang/String;)V

    .line 231
    new-instance v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer$2;

    invoke-direct {v0, p0}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer$2;-><init>(Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;)V

    iput-object v0, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mSyncThread:Ljava/lang/Thread;

    .line 241
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mSyncThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 242
    return-void
.end method

.method private declared-synchronized stopMySelf(Ljava/lang/String;)V
    .locals 3
    .param p1, "whoWantToStop"    # Ljava/lang/String;

    .prologue
    .line 2261
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->KNOX_DEBUG:Z

    if-eqz v0, :cond_0

    .line 2262
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "stopMySelf():  whoWantToStop="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2264
    :cond_0
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mSetStartServiceCallers:Ljava/util/List;

    if-nez v0, :cond_2

    .line 2265
    invoke-direct {p0}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->stopSyncThread()V

    .line 2266
    invoke-virtual {p0}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->stopSelf()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2282
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    .line 2268
    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mSetStartServiceCallers:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 2269
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mSetStartServiceCallers:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2270
    iget-boolean v0, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->KNOX_DEBUG:Z

    if-eqz v0, :cond_3

    .line 2271
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    const-string v1, "stopMySelf() called: mSetStartServiceCallers is Empty; stopping...."

    invoke-static {v0, v1}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2273
    :cond_3
    invoke-direct {p0}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->stopSyncThread()V

    .line 2274
    invoke-virtual {p0}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->stopSelf()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2261
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2276
    :cond_4
    :try_start_2
    iget-boolean v0, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->KNOX_DEBUG:Z

    if-eqz v0, :cond_1

    .line 2277
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "stopMySelf() called: mSetStartServiceCallers is NOT Empty(); not stopping....mSetStartServiceCallers="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mSetStartServiceCallers:Ljava/util/List;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method private stopSyncThread()V
    .locals 1

    .prologue
    .line 2286
    :try_start_0
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mSyncThread:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mSyncThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2287
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mSyncThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 2288
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mSyncThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->stop()V

    .line 2289
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mSyncThread:Ljava/lang/Thread;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2293
    :cond_0
    :goto_0
    return-void

    .line 2291
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private syncCalendarK()V
    .locals 2

    .prologue
    .line 712
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    const-string v1, "syncCalendarK() called"

    invoke-static {v0, v1}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 713
    invoke-direct {p0}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->syncEvent()V

    .line 714
    return-void
.end method

.method private syncEvent()V
    .locals 42

    .prologue
    .line 718
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    const-string v8, "syncEvent() called"

    invoke-static {v2, v8}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 719
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->PERFORMANCE_DEBUG:Z

    if-eqz v2, :cond_0

    .line 720
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    const-string v8, "syncEvent() called"

    invoke-static {v2, v8}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 721
    :cond_0
    const/16 v32, 0x0

    .line 722
    .local v32, "orig_event_cur":Landroid/content/CustomCursor;
    const/16 v21, 0x0

    .line 723
    .local v21, "customCursorsList":Ljava/util/List;, "Ljava/util/List<Landroid/content/CustomCursor;>;"
    const/16 v22, 0x0

    .line 724
    .local v22, "dbEvents":Landroid/database/Cursor;
    const/4 v14, 0x0

    .line 725
    .local v14, "alreadySynced":I
    const/16 v38, 0x0

    .line 726
    .local v38, "updateNeeded":I
    const/16 v29, 0x0

    .line 727
    .local v29, "insertNeeded":I
    const/16 v23, 0x0

    .line 729
    .local v23, "deleteNeeded":I
    const/16 v41, -0x1

    .line 730
    .local v41, "userOrPersonaId":I
    const/16 v36, -0x1

    .line 733
    .local v36, "syncedCalId":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mCalendarSharedPreferences:Landroid/content/SharedPreferences;

    if-nez v2, :cond_1

    .line 734
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mContext:Landroid/content/Context;

    const-string v8, "created_calendars_uris_per_persona_shared_prefs"

    const/4 v9, 0x0

    invoke-virtual {v2, v8, v9}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mCalendarSharedPreferences:Landroid/content/SharedPreferences;

    .line 737
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mCalendarSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v37

    .line 739
    .local v37, "syncedUsers":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;*>;"
    :try_start_0
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->KNOX_DEBUG:Z

    if-eqz v2, :cond_2

    .line 740
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    const-string v8, "DataSyncService.syncEvent() changes for syncing from multiple personas "

    invoke-static {v2, v8}, Lcom/samsung/knox/rcp/components/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 742
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "DataSyncService.syncEvent() mSetLunarField is "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-boolean v9, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mSetLunarField:Z

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v2, v8}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 748
    :cond_2
    :try_start_1
    invoke-direct/range {p0 .. p0}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->getSetLunarField()Z

    move-result v2

    if-nez v2, :cond_6

    .line 749
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mBridgeService:Lcom/sec/knox/bridge/IBridgeService;

    const-string v3, "Calendar"

    const-string v4, "CALENDAR_EVENTS"

    const/4 v5, 0x0

    sget-object v6, Lcom/samsung/knox/rcp/components/calendar/syncer/util/DataSyncConstants;->ORI_CALENDAR_PROJECTION:[Ljava/lang/String;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const-string v9, "_id ASC"

    invoke-interface/range {v2 .. v9}, Lcom/sec/knox/bridge/IBridgeService;->queryAllProviders(Ljava/lang/String;Ljava/lang/String;I[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v21

    .line 761
    :goto_0
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->KNOX_DEBUG:Z

    if-eqz v2, :cond_3

    .line 762
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "DataSyncService.syncEvent() mBridgeService.queryAllProviders() size =  "

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    if-eqz v21, :cond_7

    invoke-interface/range {v21 .. v21}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    :goto_1
    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v8, v2}, Lcom/samsung/knox/rcp/components/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 775
    :cond_3
    if-eqz v21, :cond_4

    :try_start_2
    invoke-interface/range {v21 .. v21}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_b

    .line 777
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "DataSyncService.syncEvent(): customCursorsList is null or empty: customCursorsList="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, v21

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v2, v8}, Lcom/samsung/knox/rcp/components/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 784
    invoke-interface/range {v37 .. v37}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .local v28, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Ljava/util/Map$Entry;

    .line 785
    .local v25, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;*>;"
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "syncEvent() user whose synced data will be deleted = "

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-interface/range {v25 .. v25}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v8, v2}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 789
    :try_start_3
    invoke-interface/range {v25 .. v25}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v40

    .line 790
    .local v40, "userId":I
    move-object/from16 v0, p0

    move/from16 v1, v40

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->deletePersonaData(I)V
    :try_end_3
    .catch Ljava/lang/NumberFormatException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_2

    .line 791
    .end local v40    # "userId":I
    :catch_0
    move-exception v31

    .line 792
    .local v31, "nfe":Ljava/lang/NumberFormatException;
    :try_start_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "syncEvent(): NumberFormatException  = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, v31

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v2, v8}, Lcom/samsung/knox/rcp/components/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_2

    .line 1172
    .end local v25    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;*>;"
    .end local v28    # "i$":Ljava/util/Iterator;
    .end local v31    # "nfe":Ljava/lang/NumberFormatException;
    :catch_1
    move-exception v24

    move/from16 v6, v36

    .end local v36    # "syncedCalId":I
    .local v6, "syncedCalId":I
    move/from16 v7, v41

    .end local v41    # "userOrPersonaId":I
    .local v7, "userOrPersonaId":I
    move-object/from16 v5, v32

    .line 1174
    .end local v32    # "orig_event_cur":Landroid/content/CustomCursor;
    .local v5, "orig_event_cur":Landroid/content/CustomCursor;
    .local v24, "e":Ljava/lang/Exception;
    :goto_3
    :try_start_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Exception during syncEvent()="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static/range {v24 .. v24}, Lcom/samsung/knox/rcp/components/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v2, v8}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 1177
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->closeCursor(Landroid/database/Cursor;)V

    .line 1178
    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->closeCursor(Landroid/database/Cursor;)V

    .line 1180
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->PERFORMANCE_DEBUG:Z

    if-eqz v2, :cond_5

    .line 1181
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    const-string v8, "syncEvent() completed"

    invoke-static {v2, v8}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1184
    .end local v24    # "e":Ljava/lang/Exception;
    :cond_5
    :goto_4
    return-void

    .line 755
    .end local v5    # "orig_event_cur":Landroid/content/CustomCursor;
    .end local v6    # "syncedCalId":I
    .end local v7    # "userOrPersonaId":I
    .restart local v32    # "orig_event_cur":Landroid/content/CustomCursor;
    .restart local v36    # "syncedCalId":I
    .restart local v41    # "userOrPersonaId":I
    :cond_6
    :try_start_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mBridgeService:Lcom/sec/knox/bridge/IBridgeService;

    const-string v3, "Calendar"

    const-string v4, "CALENDAR_EVENTS"

    const/4 v5, 0x0

    sget-object v6, Lcom/samsung/knox/rcp/components/calendar/syncer/util/DataSyncConstants;->ORI_CALENDAR_PROJECTION_LOCAL:[Ljava/lang/String;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const-string v9, "_id ASC"

    invoke-interface/range {v2 .. v9}, Lcom/sec/knox/bridge/IBridgeService;->queryAllProviders(Ljava/lang/String;Ljava/lang/String;I[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v21

    goto/16 :goto_0

    .line 762
    :cond_7
    const-string v2, " customCursorsList is null"
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto/16 :goto_1

    .line 767
    :catch_2
    move-exception v24

    .line 769
    .restart local v24    # "e":Ljava/lang/Exception;
    :try_start_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "DataSyncService.syncEvent() Exception during mBridgeService.queryAllProviders() = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static/range {v24 .. v24}, Lcom/samsung/knox/rcp/components/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v2, v8}, Lcom/samsung/knox/rcp/components/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 1177
    move-object/from16 v0, p0

    move-object/from16 v1, v32

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->closeCursor(Landroid/database/Cursor;)V

    .line 1178
    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->closeCursor(Landroid/database/Cursor;)V

    .line 1180
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->PERFORMANCE_DEBUG:Z

    if-eqz v2, :cond_8

    .line 1181
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    const-string v8, "syncEvent() completed"

    invoke-static {v2, v8}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_8
    move/from16 v6, v36

    .end local v36    # "syncedCalId":I
    .restart local v6    # "syncedCalId":I
    move/from16 v7, v41

    .end local v41    # "userOrPersonaId":I
    .restart local v7    # "userOrPersonaId":I
    move-object/from16 v5, v32

    .end local v32    # "orig_event_cur":Landroid/content/CustomCursor;
    .restart local v5    # "orig_event_cur":Landroid/content/CustomCursor;
    goto :goto_4

    .line 1177
    .end local v5    # "orig_event_cur":Landroid/content/CustomCursor;
    .end local v6    # "syncedCalId":I
    .end local v7    # "userOrPersonaId":I
    .end local v24    # "e":Ljava/lang/Exception;
    .restart local v28    # "i$":Ljava/util/Iterator;
    .restart local v32    # "orig_event_cur":Landroid/content/CustomCursor;
    .restart local v36    # "syncedCalId":I
    .restart local v41    # "userOrPersonaId":I
    :cond_9
    move-object/from16 v0, p0

    move-object/from16 v1, v32

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->closeCursor(Landroid/database/Cursor;)V

    .line 1178
    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->closeCursor(Landroid/database/Cursor;)V

    .line 1180
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->PERFORMANCE_DEBUG:Z

    if-eqz v2, :cond_a

    .line 1181
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    const-string v8, "syncEvent() completed"

    invoke-static {v2, v8}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_a
    move/from16 v6, v36

    .end local v36    # "syncedCalId":I
    .restart local v6    # "syncedCalId":I
    move/from16 v7, v41

    .end local v41    # "userOrPersonaId":I
    .restart local v7    # "userOrPersonaId":I
    move-object/from16 v5, v32

    .end local v32    # "orig_event_cur":Landroid/content/CustomCursor;
    .restart local v5    # "orig_event_cur":Landroid/content/CustomCursor;
    goto :goto_4

    .line 798
    .end local v5    # "orig_event_cur":Landroid/content/CustomCursor;
    .end local v6    # "syncedCalId":I
    .end local v7    # "userOrPersonaId":I
    .end local v28    # "i$":Ljava/util/Iterator;
    .restart local v32    # "orig_event_cur":Landroid/content/CustomCursor;
    .restart local v36    # "syncedCalId":I
    .restart local v41    # "userOrPersonaId":I
    :cond_b
    :try_start_8
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 799
    .local v3, "opsEvents":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 801
    .local v4, "origEventIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->KNOX_DEBUG:Z

    if-eqz v2, :cond_c

    .line 802
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    const-string v8, " Iterating through customCursorsList in "

    invoke-static {v2, v8}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 803
    :cond_c
    invoke-interface/range {v21 .. v21}, Ljava/util/List;->iterator()Ljava/util/Iterator;
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    move-result-object v30

    .local v30, "iterator":Ljava/util/Iterator;
    move/from16 v6, v36

    .end local v36    # "syncedCalId":I
    .restart local v6    # "syncedCalId":I
    move/from16 v7, v41

    .end local v41    # "userOrPersonaId":I
    .restart local v7    # "userOrPersonaId":I
    move-object/from16 v5, v32

    .end local v32    # "orig_event_cur":Landroid/content/CustomCursor;
    .restart local v5    # "orig_event_cur":Landroid/content/CustomCursor;
    :goto_5
    :try_start_9
    invoke-interface/range {v30 .. v30}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_d

    .line 805
    invoke-interface/range {v30 .. v30}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Landroid/content/CustomCursor;

    move-object v5, v0

    .line 807
    if-eqz v5, :cond_32

    .line 808
    sget-boolean v2, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->isSyncState:Z

    if-nez v2, :cond_e

    .line 809
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    const-string v8, "mSyncThread interrupt "

    invoke-static {v2, v8}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1161
    :cond_d
    invoke-interface/range {v37 .. v37}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28    # "i$":Ljava/util/Iterator;
    :goto_6
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_33

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Ljava/util/Map$Entry;

    .line 1162
    .restart local v25    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;*>;"
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "syncEvent()2 user whose synced data will be deleted = "

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-interface/range {v25 .. v25}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v8, v2}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_4
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 1165
    :try_start_a
    invoke-interface/range {v25 .. v25}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v40

    .line 1166
    .restart local v40    # "userId":I
    move-object/from16 v0, p0

    move/from16 v1, v40

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->deletePersonaData(I)V
    :try_end_a
    .catch Ljava/lang/NumberFormatException; {:try_start_a .. :try_end_a} :catch_3
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_4
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto :goto_6

    .line 1167
    .end local v40    # "userId":I
    :catch_3
    move-exception v31

    .line 1168
    .restart local v31    # "nfe":Ljava/lang/NumberFormatException;
    :try_start_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "syncEvent()2: NumberFormatException  = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, v31

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v2, v8}, Lcom/samsung/knox/rcp/components/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_6

    .line 1172
    .end local v25    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;*>;"
    .end local v28    # "i$":Ljava/util/Iterator;
    .end local v31    # "nfe":Ljava/lang/NumberFormatException;
    :catch_4
    move-exception v24

    goto/16 :goto_3

    .line 812
    :cond_e
    invoke-virtual {v5}, Landroid/content/CustomCursor;->getColumnCount()I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->projMatchesEvent:I

    .line 813
    invoke-virtual {v5}, Landroid/content/CustomCursor;->getCursorOwnerId()I

    move-result v7

    .line 814
    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->addNewCalendarIfRequired(I)Z

    .line 815
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->KNOX_DEBUG:Z

    if-eqz v2, :cond_f

    .line 816
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, " syncEvent userOrPersonaId : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v2, v8}, Lcom/samsung/knox/rcp/components/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 818
    :cond_f
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Size of orig_event_cur: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v5}, Landroid/content/CustomCursor;->getCount()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", with column count of: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v5}, Landroid/content/CustomCursor;->getColumnCount()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v2, v8}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 825
    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v37

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 827
    if-nez v5, :cond_11

    .line 829
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, " orig_event_cur is null "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v2, v8}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_4
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    goto/16 :goto_5

    .line 1177
    .end local v3    # "opsEvents":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .end local v4    # "origEventIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .end local v30    # "iterator":Ljava/util/Iterator;
    :catchall_0
    move-exception v2

    :goto_7
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->closeCursor(Landroid/database/Cursor;)V

    .line 1178
    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->closeCursor(Landroid/database/Cursor;)V

    .line 1180
    move-object/from16 v0, p0

    iget-boolean v8, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->PERFORMANCE_DEBUG:Z

    if-eqz v8, :cond_10

    .line 1181
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    const-string v9, "syncEvent() completed"

    invoke-static {v8, v9}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_10
    throw v2

    .line 833
    .restart local v3    # "opsEvents":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .restart local v4    # "origEventIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v30    # "iterator":Ljava/util/Iterator;
    :cond_11
    :try_start_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->calendarRcpDb:Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;

    const-string v8, "_id_original ASC"

    invoke-virtual {v2, v7, v8}, Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;->getAllEventsForPersona(ILjava/lang/String;)Landroid/database/Cursor;

    move-result-object v22

    .line 836
    if-nez v22, :cond_12

    .line 838
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    const-string v8, "Null dbEvents"

    invoke-static {v2, v8}, Lcom/samsung/knox/rcp/components/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    .line 842
    :cond_12
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, " dbEvents : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, v22

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " dbEvents.getCount() : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->getCount()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v2, v8}, Lcom/samsung/knox/rcp/components/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 847
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mContext:Landroid/content/Context;

    const-string v8, "created_calendars_uris_per_persona_shared_prefs"

    const/4 v9, 0x0

    invoke-virtual {v2, v8, v9}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mCalendarSharedPreferences:Landroid/content/SharedPreferences;

    .line 851
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mCalendarSharedPreferences:Landroid/content/SharedPreferences;

    if-eqz v2, :cond_1b

    .line 852
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mCalendarSharedPreferences:Landroid/content/SharedPreferences;

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-interface {v2, v8, v9}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v34

    .line 854
    .local v34, "str":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->KNOX_DEBUG:Z

    if-eqz v2, :cond_13

    .line 855
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, " mCalendarSharedPreferences.getString "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, v22

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " str : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, v34

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v2, v8}, Lcom/samsung/knox/rcp/components/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 857
    :cond_13
    if-eqz v34, :cond_19

    .line 858
    invoke-static/range {v34 .. v34}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v39

    .line 859
    .local v39, "uriStr":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->KNOX_DEBUG:Z

    if-eqz v2, :cond_14

    .line 860
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, " Uri.parse(str) = uriStr = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, v39

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v2, v8}, Lcom/samsung/knox/rcp/components/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 861
    :cond_14
    if-nez v39, :cond_15

    .line 862
    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->closeCursor(Landroid/database/Cursor;)V

    goto/16 :goto_5

    .line 865
    :cond_15
    invoke-static/range {v39 .. v39}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v8

    long-to-int v6, v8

    .line 866
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->KNOX_DEBUG:Z

    if-eqz v2, :cond_16

    .line 867
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, " syncedCalId : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v2, v8}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 882
    :cond_16
    new-instance v16, Landroid/database/CursorJoiner;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v8, 0x0

    const-string v9, "_id"

    aput-object v9, v2, v8

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->calendarRcpDb:Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;

    const-string v10, "_id_original"

    aput-object v10, v8, v9

    move-object/from16 v0, v16

    move-object/from16 v1, v22

    invoke-direct {v0, v5, v2, v1, v8}, Landroid/database/CursorJoiner;-><init>(Landroid/database/Cursor;[Ljava/lang/String;Landroid/database/Cursor;[Ljava/lang/String;)V

    .line 891
    .local v16, "cj":Landroid/database/CursorJoiner;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "CursorJoiner : orig_event_cur size: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v5}, Landroid/content/CustomCursor;->getCount()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", dbEvents size: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->getCount()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v2, v8}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 893
    invoke-virtual/range {v16 .. v16}, Landroid/database/CursorJoiner;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28    # "i$":Ljava/util/Iterator;
    :cond_17
    :goto_8
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_18

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v33

    check-cast v33, Landroid/database/CursorJoiner$Result;

    .line 895
    .local v33, "result":Landroid/database/CursorJoiner$Result;
    sget-boolean v2, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->isSyncState:Z

    if-nez v2, :cond_1d

    .line 896
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    const-string v8, "mSyncThread interrupt "

    invoke-static {v2, v8}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1145
    .end local v33    # "result":Landroid/database/CursorJoiner$Result;
    :cond_18
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4, v7}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->insertEventBatch(Ljava/util/ArrayList;Ljava/util/ArrayList;I)V

    .line 1147
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "insertions: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move/from16 v0, v29

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", deletions: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move/from16 v0, v23

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", updates: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move/from16 v0, v38

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", none: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v2, v8}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1149
    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->closeCursor(Landroid/database/Cursor;)V

    .line 1154
    .end local v16    # "cj":Landroid/database/CursorJoiner;
    .end local v28    # "i$":Ljava/util/Iterator;
    .end local v34    # "str":Ljava/lang/String;
    .end local v39    # "uriStr":Landroid/net/Uri;
    :goto_9
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->closeCursor(Landroid/database/Cursor;)V

    goto/16 :goto_5

    .line 869
    .restart local v34    # "str":Ljava/lang/String;
    :cond_19
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->KNOX_DEBUG:Z

    if-eqz v2, :cond_1a

    .line 870
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    const-string v8, " uriStr null .. skipping "

    invoke-static {v2, v8}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 871
    :cond_1a
    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->closeCursor(Landroid/database/Cursor;)V

    goto/16 :goto_5

    .line 875
    .end local v34    # "str":Ljava/lang/String;
    :cond_1b
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->KNOX_DEBUG:Z

    if-eqz v2, :cond_1c

    .line 876
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    const-string v8, " mCalendarSharedPreferences null .. skipping "

    invoke-static {v2, v8}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 877
    :cond_1c
    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->closeCursor(Landroid/database/Cursor;)V

    goto/16 :goto_5

    .line 899
    .restart local v16    # "cj":Landroid/database/CursorJoiner;
    .restart local v28    # "i$":Ljava/util/Iterator;
    .restart local v33    # "result":Landroid/database/CursorJoiner$Result;
    .restart local v34    # "str":Ljava/lang/String;
    .restart local v39    # "uriStr":Landroid/net/Uri;
    :cond_1d
    sget-object v2, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer$3;->$SwitchMap$android$database$CursorJoiner$Result:[I

    invoke-virtual/range {v33 .. v33}, Landroid/database/CursorJoiner$Result;->ordinal()I

    move-result v8

    aget v2, v2, v8

    packed-switch v2, :pswitch_data_0

    goto/16 :goto_8

    :pswitch_0
    move-object/from16 v2, p0

    .line 903
    invoke-direct/range {v2 .. v7}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->insertEvent(Ljava/util/ArrayList;Ljava/util/ArrayList;Landroid/content/CustomCursor;II)V

    .line 905
    add-int/lit8 v29, v29, 0x1

    .line 906
    goto/16 :goto_8

    .line 909
    :pswitch_1
    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->deleteEvent(Landroid/database/Cursor;)V

    .line 910
    add-int/lit8 v23, v23, 0x1

    .line 911
    goto/16 :goto_8

    .line 921
    :pswitch_2
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->KNOX_DEBUG:Z

    if-eqz v2, :cond_1e

    .line 922
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    const-string v8, "Event exists on both sides, check for update"

    invoke-static {v2, v8}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 923
    :cond_1e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->calendarRcpDb:Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;

    const-string v8, "_id"

    invoke-virtual {v5, v8}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    invoke-virtual {v5, v8}, Landroid/content/CustomCursor;->getLong(I)J

    move-result-wide v8

    invoke-virtual {v2, v8, v9, v7}, Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;->getConEventIdForOriEventId(JI)J

    move-result-wide v18

    .line 927
    .local v18, "conEventId":J
    const/16 v17, 0x0

    .line 928
    .local v17, "con_event_cur":Landroid/database/Cursor;
    invoke-direct/range {p0 .. p0}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->getSetLunarField()Z

    move-result v2

    if-nez v2, :cond_21

    .line 929
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    sget-object v9, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    sget-object v10, Lcom/samsung/knox/rcp/components/calendar/syncer/util/DataSyncConstants;->CON_CALENDAR_PROJECTION:[Ljava/lang/String;

    const-string v11, "_id=?"

    const/4 v2, 0x1

    new-array v12, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static/range {v18 .. v19}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v13

    aput-object v13, v12, v2

    const-string v13, "_id ASC"

    invoke-virtual/range {v8 .. v13}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v17

    .line 951
    :goto_a
    if-eqz v17, :cond_17

    .line 952
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->KNOX_DEBUG:Z

    if-eqz v2, :cond_1f

    .line 953
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "orig_event_cur(size,col):("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v5}, Landroid/content/CustomCursor;->getCount()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ","

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v5}, Landroid/content/CustomCursor;->getColumnCount()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "), "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "con_event_cur(size,col):("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->getCount()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ","

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->getColumnCount()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ")"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v2, v8}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 967
    :cond_1f
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->moveToFirst()Z

    .line 968
    const/16 v20, 0x1

    .line 969
    .local v20, "cursorEquals":Z
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->KNOX_DEBUG:Z

    if-eqz v2, :cond_20

    .line 970
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "event cursorEquals check: projMatchesEvent = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->projMatchesEvent:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v2, v8}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 973
    :cond_20
    const/16 v2, 0x14

    invoke-virtual {v5, v2}, Landroid/content/CustomCursor;->getInt(I)I

    move-result v2

    const/16 v8, 0x14

    move-object/from16 v0, v17

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    if-eq v2, v8, :cond_22

    .line 975
    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->deleteEvent(Landroid/database/Cursor;)V

    move-object/from16 v2, p0

    .line 976
    invoke-direct/range {v2 .. v7}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->insertEvent(Ljava/util/ArrayList;Ljava/util/ArrayList;Landroid/content/CustomCursor;II)V

    .line 978
    add-int/lit8 v38, v38, 0x1

    .line 1140
    :goto_b
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    goto/16 :goto_8

    .line 940
    .end local v20    # "cursorEquals":Z
    :cond_21
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    sget-object v9, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    sget-object v10, Lcom/samsung/knox/rcp/components/calendar/syncer/util/DataSyncConstants;->CON_CALENDAR_PROJECTION_LOCAL:[Ljava/lang/String;

    const-string v11, "_id=?"

    const/4 v2, 0x1

    new-array v12, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static/range {v18 .. v19}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v13

    aput-object v13, v12, v2

    const-string v13, "_id ASC"

    invoke-virtual/range {v8 .. v13}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v17

    goto/16 :goto_a

    .line 980
    .restart local v20    # "cursorEquals":Z
    :cond_22
    const/16 v27, 0x1

    .local v27, "i":I
    :goto_c
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->projMatchesEvent:I

    add-int/lit8 v2, v2, -0x1

    move/from16 v0, v27

    if-ge v0, v2, :cond_28

    .line 981
    move/from16 v0, v27

    invoke-virtual {v5, v0}, Landroid/content/CustomCursor;->getColumnName(I)Ljava/lang/String;

    move-result-object v2

    const-string v8, "calendar_id"

    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_23

    move/from16 v0, v27

    invoke-virtual {v5, v0}, Landroid/content/CustomCursor;->getColumnName(I)Ljava/lang/String;

    move-result-object v2

    const-string v8, "latitude"

    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_23

    move/from16 v0, v27

    invoke-virtual {v5, v0}, Landroid/content/CustomCursor;->getColumnName(I)Ljava/lang/String;

    move-result-object v2

    const-string v8, "longitude"

    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_23

    move/from16 v0, v27

    invoke-virtual {v5, v0}, Landroid/content/CustomCursor;->getColumnName(I)Ljava/lang/String;

    move-result-object v2

    const-string v8, "sticker_type"

    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_23

    move/from16 v0, v27

    invoke-virtual {v5, v0}, Landroid/content/CustomCursor;->getColumnName(I)Ljava/lang/String;

    move-result-object v2

    const-string v8, "original_id"

    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_23

    move/from16 v0, v27

    invoke-virtual {v5, v0}, Landroid/content/CustomCursor;->getColumnName(I)Ljava/lang/String;

    move-result-object v2

    const-string v8, "original_sync_id"

    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_24

    .line 980
    :cond_23
    add-int/lit8 v27, v27, 0x1

    goto :goto_c

    .line 995
    :cond_24
    move/from16 v0, v27

    invoke-virtual {v5, v0}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_25

    move-object/from16 v0, v17

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_23

    .line 998
    :cond_25
    move/from16 v0, v27

    invoke-virtual {v5, v0}, Landroid/content/CustomCursor;->getColumnName(I)Ljava/lang/String;

    move-result-object v2

    const-string v8, "_sync_id"

    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2b

    .line 1000
    move-object/from16 v0, v17

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v35

    .line 1001
    .local v35, "syncId":Ljava/lang/String;
    if-eqz v35, :cond_26

    const-string v2, "local"

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual/range {v35 .. v35}, Ljava/lang/String;->length()I

    move-result v8

    move-object/from16 v0, v35

    invoke-virtual {v0, v2, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    move/from16 v0, v27

    invoke-virtual {v5, v0}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_23

    .line 1004
    :cond_26
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->KNOX_DEBUG:Z

    if-eqz v2, :cond_27

    .line 1005
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "event cursorEquals check: _sync_id discrepancy"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move/from16 v0, v27

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v2, v8}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1008
    :cond_27
    const/16 v20, 0x0

    .line 1044
    .end local v35    # "syncId":Ljava/lang/String;
    :cond_28
    :goto_d
    if-nez v20, :cond_31

    .line 1045
    new-instance v26, Landroid/content/ContentValues;

    invoke-direct/range {v26 .. v26}, Landroid/content/ContentValues;-><init>()V

    .line 1046
    .local v26, "eventUpdate":Landroid/content/ContentValues;
    const-string v2, "title"

    const-string v8, "title"

    invoke-virtual {v5, v8}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    invoke-virtual {v5, v8}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, v26

    invoke-virtual {v0, v2, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1050
    const-string v2, "eventStatus"

    invoke-virtual {v5, v2}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v5, v2}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_29

    .line 1053
    const-string v2, "eventStatus"

    const-string v8, "eventStatus"

    invoke-virtual {v5, v8}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    invoke-virtual {v5, v8}, Landroid/content/CustomCursor;->getInt(I)I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    move-object/from16 v0, v26

    invoke-virtual {v0, v2, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1058
    :cond_29
    const-string v2, "dtstart"

    const-string v8, "dtstart"

    invoke-virtual {v5, v8}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    invoke-virtual {v5, v8}, Landroid/content/CustomCursor;->getLong(I)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    move-object/from16 v0, v26

    invoke-virtual {v0, v2, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1062
    const-string v2, "eventTimezone"

    const-string v8, "eventTimezone"

    invoke-virtual {v5, v8}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    invoke-virtual {v5, v8}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, v26

    invoke-virtual {v0, v2, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1066
    const-string v2, "allDay"

    const-string v8, "allDay"

    invoke-virtual {v5, v8}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    invoke-virtual {v5, v8}, Landroid/content/CustomCursor;->getInt(I)I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    move-object/from16 v0, v26

    invoke-virtual {v0, v2, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1070
    const-string v2, "rrule"

    const-string v8, "rrule"

    invoke-virtual {v5, v8}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    invoke-virtual {v5, v8}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, v26

    invoke-virtual {v0, v2, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1074
    const-string v2, "lastDate"

    const-string v8, "lastDate"

    invoke-virtual {v5, v8}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    invoke-virtual {v5, v8}, Landroid/content/CustomCursor;->getLong(I)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    move-object/from16 v0, v26

    invoke-virtual {v0, v2, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1078
    const-string v2, "duration"

    invoke-virtual {v5, v2}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v5, v2}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_30

    .line 1082
    const-string v2, "duration"

    const-string v8, "duration"

    invoke-virtual {v5, v8}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    invoke-virtual {v5, v8}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, v26

    invoke-virtual {v0, v2, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1094
    :goto_e
    invoke-direct/range {p0 .. p0}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->getSetLunarField()Z

    move-result v2

    const/4 v8, 0x1

    if-ne v2, v8, :cond_2a

    .line 1095
    const-string v2, "setLunar"

    const-string v8, "setLunar"

    invoke-virtual {v5, v8}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    invoke-virtual {v5, v8}, Landroid/content/CustomCursor;->getLong(I)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    move-object/from16 v0, v26

    invoke-virtual {v0, v2, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1099
    :cond_2a
    const-string v2, "description"

    const-string v8, "description"

    invoke-virtual {v5, v8}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    invoke-virtual {v5, v8}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, v26

    invoke-virtual {v0, v2, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1103
    const-string v2, "eventLocation"

    const-string v8, "eventLocation"

    invoke-virtual {v5, v8}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    invoke-virtual {v5, v8}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, v26

    invoke-virtual {v0, v2, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1107
    const-string v2, "_sync_id"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "local"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "_sync_id"

    invoke-virtual {v5, v9}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    invoke-virtual {v5, v9}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, v26

    invoke-virtual {v0, v2, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1113
    sget-object v15, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    .line 1114
    .local v15, "calUri":Landroid/net/Uri;
    invoke-virtual {v15}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v8, "caller_is_syncadapter"

    const-string v9, "true"

    invoke-virtual {v2, v8, v9}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v8, "account_name"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "calendar_personal"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v8, v9}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v8, "account_type"

    const-string v9, "LOCAL"

    invoke-virtual {v2, v8, v9}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v15

    .line 1127
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v8, "_id=?"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    const-string v11, "_id"

    move-object/from16 v0, v17

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    move-object/from16 v0, v17

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    move-object/from16 v0, v26

    invoke-virtual {v2, v15, v0, v8, v9}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1135
    add-int/lit8 v38, v38, 0x1

    .line 1136
    goto/16 :goto_b

    .line 1011
    .end local v15    # "calUri":Landroid/net/Uri;
    .end local v26    # "eventUpdate":Landroid/content/ContentValues;
    :cond_2b
    move/from16 v0, v27

    invoke-virtual {v5, v0}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2c

    move-object/from16 v0, v17

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2e

    :cond_2c
    move/from16 v0, v27

    invoke-virtual {v5, v0}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2d

    move-object/from16 v0, v17

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2e

    :cond_2d
    move/from16 v0, v27

    invoke-virtual {v5, v0}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v17

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_23

    .line 1033
    :cond_2e
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->KNOX_DEBUG:Z

    if-eqz v2, :cond_2f

    .line 1034
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "event cursorEquals check: Cursor discrepancy found for "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move/from16 v0, v27

    invoke-virtual {v5, v0}, Landroid/content/CustomCursor;->getColumnName(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " breaking out of loop"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move/from16 v0, v27

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v2, v8}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1039
    :cond_2f
    const/16 v20, 0x0

    .line 1040
    goto/16 :goto_d

    .line 1089
    .restart local v26    # "eventUpdate":Landroid/content/ContentValues;
    :cond_30
    const-string v2, "dtend"

    const-string v8, "dtend"

    invoke-virtual {v5, v8}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    invoke-virtual {v5, v8}, Landroid/content/CustomCursor;->getLong(I)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    move-object/from16 v0, v26

    invoke-virtual {v0, v2, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    goto/16 :goto_e

    .line 1137
    .end local v26    # "eventUpdate":Landroid/content/ContentValues;
    :cond_31
    add-int/lit8 v14, v14, 0x1

    goto/16 :goto_b

    .line 1152
    .end local v16    # "cj":Landroid/database/CursorJoiner;
    .end local v17    # "con_event_cur":Landroid/database/Cursor;
    .end local v18    # "conEventId":J
    .end local v20    # "cursorEquals":Z
    .end local v27    # "i":I
    .end local v28    # "i$":Ljava/util/Iterator;
    .end local v33    # "result":Landroid/database/CursorJoiner$Result;
    .end local v34    # "str":Ljava/lang/String;
    .end local v39    # "uriStr":Landroid/net/Uri;
    :cond_32
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    const-string v8, "Null syncEvent_cur"

    invoke-static {v2, v8}, Lcom/samsung/knox/rcp/components/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_4
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    goto/16 :goto_9

    .line 1177
    .restart local v28    # "i$":Ljava/util/Iterator;
    :cond_33
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->closeCursor(Landroid/database/Cursor;)V

    .line 1178
    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->closeCursor(Landroid/database/Cursor;)V

    .line 1180
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->PERFORMANCE_DEBUG:Z

    if-eqz v2, :cond_5

    .line 1181
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    const-string v8, "syncEvent() completed"

    invoke-static {v2, v8}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 1177
    .end local v3    # "opsEvents":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .end local v4    # "origEventIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .end local v5    # "orig_event_cur":Landroid/content/CustomCursor;
    .end local v6    # "syncedCalId":I
    .end local v7    # "userOrPersonaId":I
    .end local v28    # "i$":Ljava/util/Iterator;
    .end local v30    # "iterator":Ljava/util/Iterator;
    .restart local v32    # "orig_event_cur":Landroid/content/CustomCursor;
    .restart local v36    # "syncedCalId":I
    .restart local v41    # "userOrPersonaId":I
    :catchall_1
    move-exception v2

    move/from16 v6, v36

    .end local v36    # "syncedCalId":I
    .restart local v6    # "syncedCalId":I
    move/from16 v7, v41

    .end local v41    # "userOrPersonaId":I
    .restart local v7    # "userOrPersonaId":I
    move-object/from16 v5, v32

    .end local v32    # "orig_event_cur":Landroid/content/CustomCursor;
    .restart local v5    # "orig_event_cur":Landroid/content/CustomCursor;
    goto/16 :goto_7

    .line 899
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private syncTask()V
    .locals 37

    .prologue
    .line 1187
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    const-string v7, "syncTask() START"

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1189
    const/16 v30, 0x0

    .line 1190
    .local v30, "orig_task_cur":Landroid/content/CustomCursor;
    const/16 v20, 0x0

    .line 1192
    .local v20, "customCursorsList":Ljava/util/List;, "Ljava/util/List<Landroid/content/CustomCursor;>;"
    const/16 v21, 0x0

    .line 1194
    .local v21, "dbTasks":Landroid/database/Cursor;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mBridgeService:Lcom/sec/knox/bridge/IBridgeService;

    const-string v7, "Calendar"

    const-string v8, "CALENDAR_TASKS"

    const/4 v9, 0x0

    sget-object v10, Lcom/samsung/knox/rcp/components/calendar/syncer/util/DataSyncConstants;->TASK_PROJECTION:[Ljava/lang/String;

    const/4 v11, 0x0

    const/4 v12, 0x0

    const-string v13, "_id ASC"

    invoke-interface/range {v6 .. v13}, Lcom/sec/knox/bridge/IBridgeService;->queryAllProviders(Ljava/lang/String;Ljava/lang/String;I[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v20

    .line 1199
    if-nez v20, :cond_0

    .line 1200
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    const-string v7, "syncTask() queryAllProviders() returned null"

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1387
    :goto_0
    return-void

    .line 1203
    :cond_0
    new-instance v28, Ljava/util/ArrayList;

    invoke-direct/range {v28 .. v28}, Ljava/util/ArrayList;-><init>()V

    .line 1204
    .local v28, "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    new-instance v29, Ljava/util/ArrayList;

    invoke-direct/range {v29 .. v29}, Ljava/util/ArrayList;-><init>()V

    .line 1205
    .local v29, "oriId":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    invoke-interface/range {v20 .. v20}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v27

    .local v27, "iterator":Ljava/util/Iterator;
    :goto_1
    invoke-interface/range {v27 .. v27}, Ljava/util/Iterator;->hasNext()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v6

    if-eqz v6, :cond_2

    .line 1209
    const/4 v14, 0x0

    .line 1210
    .local v14, "alreadySynced":I
    const/16 v33, 0x0

    .line 1211
    .local v33, "updateNeeded":I
    const/16 v26, 0x0

    .line 1212
    .local v26, "insertNeeded":I
    const/16 v22, 0x0

    .line 1213
    .local v22, "deleteNeeded":I
    const/16 v35, -0x1

    .line 1215
    .local v35, "userOrPersonaId":I
    :try_start_1
    invoke-interface/range {v27 .. v27}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    move-object v0, v6

    check-cast v0, Landroid/content/CustomCursor;

    move-object/from16 v30, v0

    .line 1217
    if-eqz v30, :cond_13

    .line 1218
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Size of orig_task_cur: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual/range {v30 .. v30}, Landroid/content/CustomCursor;->getCount()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", with column count of: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual/range {v30 .. v30}, Landroid/content/CustomCursor;->getColumnCount()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1223
    invoke-virtual/range {v30 .. v30}, Landroid/content/CustomCursor;->getColumnCount()I

    move-result v6

    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->projMatchesTask:I

    .line 1224
    invoke-virtual/range {v30 .. v30}, Landroid/content/CustomCursor;->getCursorOwnerId()I

    move-result v35

    .line 1226
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->calendarRcpDb:Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;

    const-string v7, "_id_original ASC"

    move/from16 v0, v35

    invoke-virtual {v6, v0, v7}, Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;->getAllTasksForPersona(ILjava/lang/String;)Landroid/database/Cursor;

    move-result-object v21

    .line 1230
    if-nez v21, :cond_1

    .line 1231
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    const-string v7, "Null dbtasks"

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1377
    :try_start_2
    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->closeCursor(Landroid/database/Cursor;)V

    .line 1378
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->closeCursor(Landroid/database/Cursor;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_0

    .line 1384
    .end local v14    # "alreadySynced":I
    .end local v22    # "deleteNeeded":I
    .end local v26    # "insertNeeded":I
    .end local v27    # "iterator":Ljava/util/Iterator;
    .end local v28    # "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .end local v29    # "oriId":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .end local v33    # "updateNeeded":I
    .end local v35    # "userOrPersonaId":I
    :catch_0
    move-exception v23

    .line 1385
    .local v23, "e":Ljava/lang/Exception;
    invoke-virtual/range {v23 .. v23}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 1234
    .end local v23    # "e":Ljava/lang/Exception;
    .restart local v14    # "alreadySynced":I
    .restart local v22    # "deleteNeeded":I
    .restart local v26    # "insertNeeded":I
    .restart local v27    # "iterator":Ljava/util/Iterator;
    .restart local v28    # "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .restart local v29    # "oriId":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v33    # "updateNeeded":I
    .restart local v35    # "userOrPersonaId":I
    :cond_1
    :try_start_3
    sget-boolean v6, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->isSyncState:Z

    if-nez v6, :cond_3

    .line 1235
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    const-string v7, "mSyncThread interrupt "

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1377
    :try_start_4
    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->closeCursor(Landroid/database/Cursor;)V

    .line 1378
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->closeCursor(Landroid/database/Cursor;)V

    .line 1381
    .end local v14    # "alreadySynced":I
    .end local v22    # "deleteNeeded":I
    .end local v26    # "insertNeeded":I
    .end local v33    # "updateNeeded":I
    .end local v35    # "userOrPersonaId":I
    :cond_2
    invoke-interface/range {v20 .. v20}, Ljava/util/List;->clear()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    .line 1382
    const/16 v20, 0x0

    goto/16 :goto_0

    .line 1238
    .restart local v14    # "alreadySynced":I
    .restart local v22    # "deleteNeeded":I
    .restart local v26    # "insertNeeded":I
    .restart local v33    # "updateNeeded":I
    .restart local v35    # "userOrPersonaId":I
    :cond_3
    :try_start_5
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "orig_task_cur size: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual/range {v30 .. v30}, Landroid/content/CustomCursor;->getCount()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", dbTasks size: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->getCount()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1243
    move-object/from16 v0, p0

    move/from16 v1, v35

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->addNewTaskIfRequired(I)Z

    .line 1245
    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->getCount()I

    move-result v6

    if-nez v6, :cond_6

    .line 1246
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    const-string v7, "syncTask() call init_sync_task"

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1247
    invoke-virtual/range {v30 .. v30}, Landroid/content/CustomCursor;->getCount()I

    move-result v32

    .line 1248
    .local v32, "taskcnt":I
    invoke-virtual/range {v30 .. v30}, Landroid/content/CustomCursor;->moveToFirst()Z

    .line 1249
    const/16 v24, 0x0

    .local v24, "i":I
    :goto_2
    move/from16 v0, v24

    move/from16 v1, v32

    if-ge v0, v1, :cond_4

    .line 1250
    sget-boolean v6, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->isSyncState:Z

    if-nez v6, :cond_5

    .line 1251
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    const-string v7, "mSyncThread interrupt "

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1362
    .end local v24    # "i":I
    .end local v32    # "taskcnt":I
    :cond_4
    :goto_3
    move-object/from16 v0, p0

    move-object/from16 v1, v28

    move-object/from16 v2, v29

    move/from16 v3, v35

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->insertTasktoDB(Ljava/util/ArrayList;Ljava/util/ArrayList;I)V

    .line 1363
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "syncTask() completed from userId: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move/from16 v0, v35

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " - insertions: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move/from16 v0, v26

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", deletions: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move/from16 v0, v22

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", updates: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move/from16 v0, v33

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", none: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 1377
    :goto_4
    :try_start_6
    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->closeCursor(Landroid/database/Cursor;)V

    .line 1378
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->closeCursor(Landroid/database/Cursor;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0

    goto/16 :goto_1

    .line 1254
    .restart local v24    # "i":I
    .restart local v32    # "taskcnt":I
    :cond_5
    :try_start_7
    move-object/from16 v0, p0

    move-object/from16 v1, v28

    move-object/from16 v2, v29

    move-object/from16 v3, v30

    move/from16 v4, v35

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->insertTask(Ljava/util/ArrayList;Ljava/util/ArrayList;Landroid/content/CustomCursor;I)V

    .line 1255
    add-int/lit8 v26, v26, 0x1

    .line 1256
    invoke-virtual/range {v30 .. v30}, Landroid/content/CustomCursor;->moveToNext()Z

    .line 1249
    add-int/lit8 v24, v24, 0x1

    goto/16 :goto_2

    .line 1260
    .end local v24    # "i":I
    .end local v32    # "taskcnt":I
    :cond_6
    new-instance v15, Landroid/database/CursorJoiner;

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "_id"

    aput-object v8, v6, v7

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->calendarRcpDb:Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;

    const-string v9, "_id_original"

    aput-object v9, v7, v8

    move-object/from16 v0, v30

    move-object/from16 v1, v21

    invoke-direct {v15, v0, v6, v1, v7}, Landroid/database/CursorJoiner;-><init>(Landroid/database/Cursor;[Ljava/lang/String;Landroid/database/Cursor;[Ljava/lang/String;)V

    .line 1270
    .local v15, "cj":Landroid/database/CursorJoiner;
    invoke-virtual {v15}, Landroid/database/CursorJoiner;->iterator()Ljava/util/Iterator;

    move-result-object v25

    .local v25, "i$":Ljava/util/Iterator;
    :cond_7
    :goto_5
    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v31

    check-cast v31, Landroid/database/CursorJoiner$Result;

    .line 1271
    .local v31, "result":Landroid/database/CursorJoiner$Result;
    sget-boolean v6, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->isSyncState:Z

    if-nez v6, :cond_8

    .line 1272
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    const-string v7, "mSyncThread interrupt "

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_3

    .line 1371
    .end local v15    # "cj":Landroid/database/CursorJoiner;
    .end local v25    # "i$":Ljava/util/Iterator;
    .end local v31    # "result":Landroid/database/CursorJoiner$Result;
    :catch_1
    move-exception v23

    .line 1372
    .restart local v23    # "e":Ljava/lang/Exception;
    :try_start_8
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Exception during syncTask()="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static/range {v23 .. v23}, Lcom/samsung/knox/rcp/components/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 1377
    :try_start_9
    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->closeCursor(Landroid/database/Cursor;)V

    .line 1378
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->closeCursor(Landroid/database/Cursor;)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_0

    goto/16 :goto_0

    .line 1275
    .end local v23    # "e":Ljava/lang/Exception;
    .restart local v15    # "cj":Landroid/database/CursorJoiner;
    .restart local v25    # "i$":Ljava/util/Iterator;
    .restart local v31    # "result":Landroid/database/CursorJoiner$Result;
    :cond_8
    :try_start_a
    sget-object v6, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer$3;->$SwitchMap$android$database$CursorJoiner$Result:[I

    invoke-virtual/range {v31 .. v31}, Landroid/database/CursorJoiner$Result;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    goto :goto_5

    .line 1278
    :pswitch_0
    move-object/from16 v0, p0

    move/from16 v1, v35

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->addNewTaskIfRequired(I)Z

    .line 1279
    move-object/from16 v0, p0

    move-object/from16 v1, v28

    move-object/from16 v2, v29

    move-object/from16 v3, v30

    move/from16 v4, v35

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->insertTask(Ljava/util/ArrayList;Ljava/util/ArrayList;Landroid/content/CustomCursor;I)V

    .line 1280
    add-int/lit8 v26, v26, 0x1

    .line 1281
    goto :goto_5

    .line 1285
    :pswitch_1
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->deleteTask(Landroid/database/Cursor;)V

    .line 1286
    add-int/lit8 v22, v22, 0x1

    .line 1287
    goto :goto_5

    .line 1291
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->calendarRcpDb:Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;

    const-string v7, "_id"

    move-object/from16 v0, v30

    invoke-virtual {v0, v7}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    move-object/from16 v0, v30

    invoke-virtual {v0, v7}, Landroid/content/CustomCursor;->getLong(I)J

    move-result-wide v8

    move/from16 v0, v35

    invoke-virtual {v6, v8, v9, v0}, Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;->getConTaskIdForOriTaskId(JI)J

    move-result-wide v16

    .line 1294
    .local v16, "conTaskId":J
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    sget-object v7, Lcom/samsung/knox/rcp/components/calendar/syncer/util/DataSyncConstants;->TASK_URI:Landroid/net/Uri;

    sget-object v8, Lcom/samsung/knox/rcp/components/calendar/syncer/util/DataSyncConstants;->TASK_PROJECTION:[Ljava/lang/String;

    const-string v9, "_id=?"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/String;

    const/4 v11, 0x0

    invoke-static/range {v16 .. v17}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    const-string v11, "_id ASC"

    invoke-virtual/range {v6 .. v11}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v18

    .line 1305
    .local v18, "con_task_cur":Landroid/database/Cursor;
    if-eqz v18, :cond_11

    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->moveToNext()Z

    move-result v6

    if-eqz v6, :cond_11

    .line 1308
    const/16 v19, 0x1

    .line 1309
    .local v19, "cursorEquals":Z
    const/16 v24, 0x1

    .restart local v24    # "i":I
    :goto_6
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->projMatchesTask:I

    add-int/lit8 v6, v6, -0x1

    move/from16 v0, v24

    if-ge v0, v6, :cond_f

    .line 1312
    const/4 v6, 0x4

    move/from16 v0, v24

    if-eq v0, v6, :cond_9

    const/16 v6, 0x23

    move/from16 v0, v24

    if-eq v0, v6, :cond_9

    const/16 v6, 0x24

    move/from16 v0, v24

    if-eq v0, v6, :cond_9

    const/16 v6, 0x26

    move/from16 v0, v24

    if-ne v0, v6, :cond_a

    .line 1309
    :cond_9
    add-int/lit8 v24, v24, 0x1

    goto :goto_6

    .line 1318
    :cond_a
    move-object/from16 v0, v30

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_b

    move-object/from16 v0, v18

    move/from16 v1, v24

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_9

    .line 1323
    :cond_b
    move-object/from16 v0, v30

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_c

    move-object/from16 v0, v18

    move/from16 v1, v24

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_e

    :cond_c
    move-object/from16 v0, v30

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_d

    move-object/from16 v0, v18

    move/from16 v1, v24

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_e

    :cond_d
    move-object/from16 v0, v30

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v18

    move/from16 v1, v24

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_9

    .line 1337
    :cond_e
    const/16 v19, 0x0

    .line 1342
    :cond_f
    if-nez v19, :cond_12

    .line 1343
    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->updateTask(Landroid/content/CustomCursor;)Landroid/content/ContentValues;

    move-result-object v36

    .line 1344
    .local v36, "val":Landroid/content/ContentValues;
    sget-object v6, Lcom/samsung/knox/rcp/components/calendar/syncer/util/DataSyncConstants;->TASK_URI:Landroid/net/Uri;

    move-wide/from16 v0, v16

    invoke-static {v6, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v34

    .line 1346
    .local v34, "updateUri":Landroid/net/Uri;
    if-eqz v36, :cond_10

    if-eqz v34, :cond_10

    .line 1347
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, v34

    move-object/from16 v1, v36

    invoke-virtual {v6, v0, v1, v7, v8}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1349
    :cond_10
    add-int/lit8 v33, v33, 0x1

    .line 1356
    .end local v19    # "cursorEquals":Z
    .end local v24    # "i":I
    .end local v34    # "updateUri":Landroid/net/Uri;
    .end local v36    # "val":Landroid/content/ContentValues;
    :cond_11
    :goto_7
    if-eqz v18, :cond_7

    .line 1357
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_1
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto/16 :goto_5

    .line 1377
    .end local v15    # "cj":Landroid/database/CursorJoiner;
    .end local v16    # "conTaskId":J
    .end local v18    # "con_task_cur":Landroid/database/Cursor;
    .end local v25    # "i$":Ljava/util/Iterator;
    .end local v31    # "result":Landroid/database/CursorJoiner$Result;
    :catchall_0
    move-exception v6

    :try_start_b
    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->closeCursor(Landroid/database/Cursor;)V

    .line 1378
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->closeCursor(Landroid/database/Cursor;)V

    throw v6
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_0

    .line 1352
    .restart local v15    # "cj":Landroid/database/CursorJoiner;
    .restart local v16    # "conTaskId":J
    .restart local v18    # "con_task_cur":Landroid/database/Cursor;
    .restart local v19    # "cursorEquals":Z
    .restart local v24    # "i":I
    .restart local v25    # "i$":Ljava/util/Iterator;
    .restart local v31    # "result":Landroid/database/CursorJoiner$Result;
    :cond_12
    add-int/lit8 v14, v14, 0x1

    goto :goto_7

    .line 1369
    .end local v15    # "cj":Landroid/database/CursorJoiner;
    .end local v16    # "conTaskId":J
    .end local v18    # "con_task_cur":Landroid/database/Cursor;
    .end local v19    # "cursorEquals":Z
    .end local v24    # "i":I
    .end local v25    # "i$":Ljava/util/Iterator;
    .end local v31    # "result":Landroid/database/CursorJoiner$Result;
    :cond_13
    :try_start_c
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    const-string v7, "Null syncTask_cur"

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_1
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    goto/16 :goto_4

    .line 1275
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private updateTask(Landroid/content/CustomCursor;)Landroid/content/ContentValues;
    .locals 8
    .param p1, "c"    # Landroid/content/CustomCursor;

    .prologue
    .line 1706
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 1708
    .local v3, "val":Landroid/content/ContentValues;
    if-eqz p1, :cond_7

    .line 1715
    invoke-virtual {p1}, Landroid/content/CustomCursor;->getColumnCount()I

    move-result v0

    .line 1716
    .local v0, "columncnt":I
    invoke-virtual {p1}, Landroid/content/CustomCursor;->getColumnNames()[Ljava/lang/String;

    move-result-object v1

    .line 1718
    .local v1, "columnnames":[Ljava/lang/String;
    const/4 v2, 0x1

    .local v2, "i":I
    :goto_0
    if-ge v2, v0, :cond_8

    .line 1720
    const-string v4, "due_date"

    aget-object v5, v1, v2

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1721
    invoke-virtual {p1, v2}, Landroid/content/CustomCursor;->getType(I)I

    move-result v4

    if-eqz v4, :cond_1

    .line 1722
    const-string v4, "due_date"

    invoke-virtual {p1, v2}, Landroid/content/CustomCursor;->getLong(I)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1718
    :cond_0
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1724
    :cond_1
    const-string v4, "due_date"

    invoke-virtual {v3, v4}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto :goto_1

    .line 1727
    :cond_2
    const-string v4, "utc_due_date"

    aget-object v5, v1, v2

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1728
    invoke-virtual {p1, v2}, Landroid/content/CustomCursor;->getType(I)I

    move-result v4

    if-eqz v4, :cond_3

    .line 1729
    const-string v4, "utc_due_date"

    invoke-virtual {p1, v2}, Landroid/content/CustomCursor;->getLong(I)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    goto :goto_1

    .line 1731
    :cond_3
    const-string v4, "utc_due_date"

    invoke-virtual {v3, v4}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto :goto_1

    .line 1734
    :cond_4
    const-string v4, "task_order"

    aget-object v5, v1, v2

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1736
    const-string v4, "previousId"

    aget-object v5, v1, v2

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1738
    const-string v4, "accountKey"

    aget-object v5, v1, v2

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1739
    const-string v5, "accountKey"

    iget-object v4, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mTaskAccountMap:Ljava/util/HashMap;

    invoke-virtual {p1}, Landroid/content/CustomCursor;->getCursorOwnerId()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v3, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_1

    .line 1742
    :cond_5
    const-string v4, "accountName"

    aget-object v5, v1, v2

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 1743
    const-string v5, "accountName"

    iget-object v4, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mTaskAccountDisplayNameMap:Ljava/util/HashMap;

    invoke-virtual {p1}, Landroid/content/CustomCursor;->getCursorOwnerId()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v3, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1748
    :cond_6
    aget-object v4, v1, v2

    invoke-direct {p0, v3, p1, v2, v4}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->setContentValue(Landroid/content/ContentValues;Landroid/database/Cursor;ILjava/lang/String;)V

    goto/16 :goto_1

    .line 1759
    .end local v0    # "columncnt":I
    .end local v1    # "columnnames":[Ljava/lang/String;
    .end local v2    # "i":I
    :cond_7
    const/4 v3, 0x0

    .end local v3    # "val":Landroid/content/ContentValues;
    :cond_8
    return-object v3
.end method


# virtual methods
.method deleteSharedPrefsForPersona(I)Z
    .locals 5
    .param p1, "personaID"    # I

    .prologue
    .line 2134
    const/4 v1, 0x0

    .line 2135
    .local v1, "result":Z
    iget-boolean v2, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->KNOX_DEBUG:Z

    if-eqz v2, :cond_0

    .line 2136
    iget-object v2, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " deleteSharedPrefsForPersona : Need to delete SP for deleted personaID : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2142
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mContext:Landroid/content/Context;

    const-string v3, "created_calendars_uris_per_persona_shared_prefs"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mCalendarSharedPreferences:Landroid/content/SharedPreferences;

    .line 2144
    iget-object v2, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mCalendarSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mCalendarSharedPrefsEditor:Landroid/content/SharedPreferences$Editor;

    .line 2145
    iget-object v2, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mCalendarSharedPrefsEditor:Landroid/content/SharedPreferences$Editor;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 2146
    iget-object v2, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mCalendarSharedPrefsEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2147
    const/4 v1, 0x1

    .line 2153
    :goto_0
    return v1

    .line 2148
    :catch_0
    move-exception v0

    .line 2149
    .local v0, "e":Ljava/lang/Exception;
    const/4 v1, 0x0

    .line 2151
    iget-object v2, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " deleteSharedPrefsForPersona : Exception while deleting SP  "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method insertEventBatch(Ljava/util/ArrayList;Ljava/util/ArrayList;I)V
    .locals 15
    .param p3, "personaId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 1531
    .local p1, "opsEvents":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .local p2, "origEventIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    iget-boolean v3, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->KNOX_DEBUG:Z

    if-eqz v3, :cond_0

    .line 1532
    iget-object v3, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " insertEventBatch called "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " -- "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1534
    :cond_0
    const/4 v14, 0x0

    .line 1535
    .local v14, "newBatchLimit":I
    const/4 v12, 0x0

    .line 1536
    .local v12, "insertedIds":[Landroid/content/ContentProviderResult;
    const/4 v2, 0x0

    .line 1539
    .local v2, "beginTrans":Z
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1541
    iget-object v3, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    const-string v4, " insertEventBatch opsEvents is Empty "

    invoke-static {v3, v4}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1596
    if-eqz v2, :cond_1

    .line 1597
    iget-object v3, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->calendarRcpDb:Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;

    invoke-virtual {v3}, Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;->endTransaction()V

    .line 1598
    :cond_1
    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->clear()V

    .line 1599
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->clear()V

    .line 1601
    :goto_0
    return-void

    .line 1544
    :cond_2
    :try_start_1
    iget-object v3, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "com.android.calendar"

    move-object/from16 v0, p1

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    move-result-object v12

    .line 1546
    if-eqz v12, :cond_3

    array-length v3, v12

    if-nez v3, :cond_6

    .line 1547
    :cond_3
    iget-boolean v3, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->KNOX_DEBUG:Z

    if-eqz v3, :cond_4

    .line 1548
    iget-object v3, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    const-string v4, " insertEventBatch insertedIds is null or zero "

    invoke-static {v3, v4}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1596
    :cond_4
    if-eqz v2, :cond_5

    .line 1597
    iget-object v3, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->calendarRcpDb:Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;

    invoke-virtual {v3}, Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;->endTransaction()V

    .line 1598
    :cond_5
    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->clear()V

    .line 1599
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->clear()V

    goto :goto_0

    .line 1551
    :cond_6
    :try_start_2
    array-length v14, v12

    .line 1552
    iget-object v3, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->calendarRcpDb:Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;

    invoke-virtual {v3}, Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;->beginTransaction()V

    .line 1553
    const/4 v2, 0x1

    .line 1554
    const/4 v13, 0x0

    .local v13, "j":I
    :goto_1
    if-ge v13, v14, :cond_7

    .line 1555
    iget-object v3, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->calendarRcpDb:Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    aget-object v6, v12, v13

    iget-object v6, v6, Landroid/content/ContentProviderResult;->uri:Landroid/net/Uri;

    invoke-static {v6}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v6

    move/from16 v0, p3

    int-to-long v8, v0

    invoke-virtual/range {v3 .. v9}, Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;->insertEvent(JJJ)J

    .line 1554
    add-int/lit8 v13, v13, 0x1

    goto :goto_1

    .line 1563
    :cond_7
    iget-object v3, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->calendarRcpDb:Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;

    invoke-virtual {v3}, Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;->setTransactionSuccessful()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1596
    if-eqz v2, :cond_8

    .line 1597
    iget-object v3, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->calendarRcpDb:Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;

    invoke-virtual {v3}, Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;->endTransaction()V

    .line 1598
    :cond_8
    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->clear()V

    .line 1599
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->clear()V

    goto :goto_0

    .line 1577
    .end local v13    # "j":I
    :catch_0
    move-exception v10

    .line 1579
    .local v10, "e":Ljava/lang/Exception;
    :try_start_3
    iget-object v3, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Exception during inserting event, exp = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v10}, Lcom/samsung/knox/rcp/components/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/knox/rcp/components/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1581
    const/4 v13, 0x0

    .restart local v13    # "j":I
    :goto_2
    if-ge v13, v14, :cond_9

    .line 1587
    :try_start_4
    aget-object v3, v12, v13

    iget-object v3, v3, Landroid/content/ContentProviderResult;->uri:Landroid/net/Uri;

    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1581
    add-int/lit8 v13, v13, 0x1

    goto :goto_2

    .line 1589
    :cond_9
    iget-object v3, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "com.android.calendar"

    move-object/from16 v0, p1

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1596
    :goto_3
    if-eqz v2, :cond_a

    .line 1597
    iget-object v3, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->calendarRcpDb:Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;

    invoke-virtual {v3}, Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;->endTransaction()V

    .line 1598
    :cond_a
    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->clear()V

    .line 1599
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->clear()V

    goto/16 :goto_0

    .line 1590
    :catch_1
    move-exception v11

    .line 1591
    .local v11, "e1":Ljava/lang/Exception;
    :try_start_5
    iget-object v3, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Exception during deleteing inserted event, exp = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v10}, Lcom/samsung/knox/rcp/components/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/knox/rcp/components/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_3

    .line 1596
    .end local v10    # "e":Ljava/lang/Exception;
    .end local v11    # "e1":Ljava/lang/Exception;
    .end local v13    # "j":I
    :catchall_0
    move-exception v3

    if-eqz v2, :cond_b

    .line 1597
    iget-object v4, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->calendarRcpDb:Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;

    invoke-virtual {v4}, Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;->endTransaction()V

    .line 1598
    :cond_b
    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->clear()V

    .line 1599
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->clear()V

    throw v3
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 105
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 111
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 113
    invoke-virtual {p0}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mContext:Landroid/content/Context;

    .line 115
    iget-boolean v0, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->PERFORMANCE_DEBUG:Z

    if-eqz v0, :cond_0

    .line 116
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    const-string v1, "onCreate() called"

    invoke-static {v0, v1}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    :cond_0
    iget-boolean v0, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->KNOX_DEBUG:Z

    if-eqz v0, :cond_1

    .line 118
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    const-string v1, "CalendarDataSyncService onCreate()"

    invoke-static {v0, v1}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    :cond_1
    iget-boolean v0, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->PERFORMANCE_DEBUG:Z

    if-eqz v0, :cond_2

    .line 120
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    const-string v1, "onCreate() completed"

    invoke-static {v0, v1}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    :cond_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mSetStartServiceCallers:Ljava/util/List;

    .line 123
    new-instance v0, Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;

    invoke-direct {v0, p0}, Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->calendarRcpDb:Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;

    .line 124
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->calendarRcpDb:Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;

    if-eqz v0, :cond_3

    .line 125
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->calendarRcpDb:Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;

    invoke-virtual {v0}, Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;->open()V

    .line 126
    :cond_3
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 309
    iget-boolean v0, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->PERFORMANCE_DEBUG:Z

    if-eqz v0, :cond_0

    .line 310
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    const-string v1, " ********onDestroy*********"

    invoke-static {v0, v1}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 311
    :cond_0
    iget-boolean v0, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->KNOX_DEBUG:Z

    if-eqz v0, :cond_1

    .line 312
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    const-string v1, " ********onDestroy********* "

    invoke-static {v0, v1}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 313
    :cond_1
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 315
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->calendarRcpDb:Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;

    if-eqz v0, :cond_2

    .line 316
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->calendarRcpDb:Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;

    invoke-virtual {v0}, Lcom/samsung/knox/rcp/components/calendar/db/CalendarRCPDBInterface;->close()V

    .line 317
    :cond_2
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 12
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    const/4 v11, 0x3

    const/4 v10, -0x1

    .line 130
    iget-object v7, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    const-string v8, " onStartCommand START "

    invoke-static {v7, v8}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    iget-boolean v7, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->PERFORMANCE_DEBUG:Z

    if-eqz v7, :cond_0

    .line 132
    iget-object v7, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    const-string v8, "onStartCommand() called"

    invoke-static {v7, v8}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    :cond_0
    if-nez p1, :cond_2

    .line 135
    iget-object v7, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    const-string v8, "onStartCommand(): incoming intent is null."

    invoke-static {v7, v8}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    const-string v7, "<DUMMY>"

    invoke-direct {p0, v7}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->stopMySelf(Ljava/lang/String;)V

    .line 204
    :cond_1
    :goto_0
    return v11

    .line 140
    :cond_2
    const/4 v4, 0x0

    .line 141
    .local v4, "proxyMessenger":Landroid/os/Messenger;
    const/4 v0, 0x0

    .line 142
    .local v0, "bundle":Landroid/os/Bundle;
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 143
    .local v2, "extras":Landroid/os/Bundle;
    if-eqz v2, :cond_7

    .line 144
    iget-object v7, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    const-string v8, "onStartCommand(): extras != null "

    invoke-static {v7, v8}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    const-string v7, "binderBundle"

    invoke-virtual {v2, v7}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "bundle":Landroid/os/Bundle;
    check-cast v0, Landroid/os/Bundle;

    .line 150
    .restart local v0    # "bundle":Landroid/os/Bundle;
    :goto_1
    if-eqz v0, :cond_3

    .line 151
    const-string v7, "proxy"

    invoke-virtual {v0, v7}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "proxyMessenger":Landroid/os/Messenger;
    check-cast v4, Landroid/os/Messenger;

    .line 154
    .restart local v4    # "proxyMessenger":Landroid/os/Messenger;
    :cond_3
    if-eqz v4, :cond_4

    .line 155
    invoke-virtual {v4}, Landroid/os/Messenger;->getBinder()Landroid/os/IBinder;

    move-result-object v7

    iput-object v7, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mBinderProxy:Landroid/os/IBinder;

    .line 156
    iget-object v7, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mBinderProxy:Landroid/os/IBinder;

    if-eqz v7, :cond_8

    .line 157
    iget-object v7, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mBinderProxy:Landroid/os/IBinder;

    invoke-static {v7}, Lcom/sec/knox/bridge/IBridgeService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/knox/bridge/IBridgeService;

    move-result-object v7

    iput-object v7, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mBridgeService:Lcom/sec/knox/bridge/IBridgeService;

    .line 164
    :cond_4
    :goto_2
    const-string v1, ""

    .line 166
    .local v1, "doWhat":Ljava/lang/String;
    const-string v7, "dowhat"

    invoke-virtual {p1, v7}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 167
    const-string v7, "dowhat"

    invoke-virtual {p1, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 170
    :cond_5
    iget-object v7, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "CalendarDataSyncService onStartCommand() doWhat : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    const-string v7, "DELETE"

    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 173
    iget-boolean v7, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->KNOX_DEBUG:Z

    if-eqz v7, :cond_6

    .line 174
    iget-object v7, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    const-string v8, "CalendarDataSyncService onStartCommand() DELETE"

    invoke-static {v7, v8}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    :cond_6
    const-string v7, "personaid"

    invoke-virtual {p1, v7, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 176
    .local v3, "personaID":I
    iget-object v7, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "CalendarDataSyncService onStartCommand() DELETE personaID "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    if-eq v3, v10, :cond_1

    .line 178
    const/4 v7, 0x0

    sput-boolean v7, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->isSyncState:Z

    .line 179
    const-string v7, "DELETE_PERSONA"

    invoke-direct {p0, v7}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->registerRequestStartService(Ljava/lang/String;)V

    .line 180
    new-instance v5, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer$1;

    invoke-direct {v5, p0, v3}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer$1;-><init>(Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;I)V

    .line 192
    .local v5, "r":Ljava/lang/Runnable;
    new-instance v6, Ljava/lang/Thread;

    invoke-direct {v6, v5}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 193
    .local v6, "t":Ljava/lang/Thread;
    invoke-virtual {v6}, Ljava/lang/Thread;->start()V

    goto/16 :goto_0

    .line 147
    .end local v1    # "doWhat":Ljava/lang/String;
    .end local v3    # "personaID":I
    .end local v5    # "r":Ljava/lang/Runnable;
    .end local v6    # "t":Ljava/lang/Thread;
    :cond_7
    iget-object v7, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    const-string v8, "onStartCommand(): extras == null "

    invoke-static {v7, v8}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 159
    :cond_8
    iget-object v7, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    const-string v8, "onStartCommand(): mBinderProxy == null"

    invoke-static {v7, v8}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    const-string v7, "<DUMMY>"

    invoke-direct {p0, v7}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->stopMySelf(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 196
    .restart local v1    # "doWhat":Ljava/lang/String;
    :cond_9
    iget-object v7, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mSyncThread:Ljava/lang/Thread;

    if-eqz v7, :cond_a

    iget-object v7, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->mSyncThread:Ljava/lang/Thread;

    invoke-virtual {v7}, Ljava/lang/Thread;->isAlive()Z

    move-result v7

    if-nez v7, :cond_b

    .line 197
    :cond_a
    invoke-direct {p0}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->startSyncingProcess()V

    goto/16 :goto_0

    .line 199
    :cond_b
    iget-object v7, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    const-string v8, "onStartCommand(); mSyncThread == null || !mSyncThread.isAlive()... "

    invoke-static {v7, v8}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    const-string v7, "<DUMMY>"

    invoke-direct {p0, v7}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->stopMySelf(Ljava/lang/String;)V

    goto/16 :goto_0
.end method
