.class public Lcom/samsung/knox/rcp/components/contacts/syncer/util/DataSyncConstants;
.super Ljava/lang/Object;
.source "DataSyncConstants.java"


# static fields
.field public static CON_ACCOUNTS_URI:Landroid/net/Uri;

.field public static CON_DATA_URI:Landroid/net/Uri;

.field public static final CON_GROUPS_PROJECTION:[Ljava/lang/String;

.field public static CON_GROUP_URI:Landroid/net/Uri;

.field public static CON_LOGS_URI:Landroid/net/Uri;

.field public static final CON_RAWCONTACTS_PROJECTION:[Ljava/lang/String;

.field public static CON_RAW_CONTACTS_URI:Landroid/net/Uri;

.field public static final ORI_CALLLOG_PROJECTION:[Ljava/lang/String;

.field public static final ORI_DATA_PROJECTION:[Ljava/lang/String;

.field public static final ORI_GROUPS_PROJECTION:[Ljava/lang/String;

.field public static final ORI_RAWCONTACTS_PROJECTION:[Ljava/lang/String;

.field protected static PREF_NAME:Ljava/lang/String;

.field static initCalendarFlag:Z

.field static initCalendarStartFlag:Z

.field static initCallLogFlag:Z

.field static initCallLogStartFlag:Z

.field static initContactsFlag:Z

.field static initContactsStartFlag:Z

.field static isRunningCalendarFlag:Z

.field static isRunningCallLogFlag:Z

.field static isRunningContactsFlag:Z

.field static isRunningServiceCheckFlag:Z

.field static isServiceActive:Z


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 30
    sput-boolean v3, Lcom/samsung/knox/rcp/components/contacts/syncer/util/DataSyncConstants;->initCallLogFlag:Z

    .line 32
    sput-boolean v3, Lcom/samsung/knox/rcp/components/contacts/syncer/util/DataSyncConstants;->initCalendarFlag:Z

    .line 34
    sput-boolean v3, Lcom/samsung/knox/rcp/components/contacts/syncer/util/DataSyncConstants;->initContactsFlag:Z

    .line 36
    sput-boolean v3, Lcom/samsung/knox/rcp/components/contacts/syncer/util/DataSyncConstants;->initCallLogStartFlag:Z

    .line 38
    sput-boolean v3, Lcom/samsung/knox/rcp/components/contacts/syncer/util/DataSyncConstants;->initCalendarStartFlag:Z

    .line 40
    sput-boolean v3, Lcom/samsung/knox/rcp/components/contacts/syncer/util/DataSyncConstants;->initContactsStartFlag:Z

    .line 44
    sput-boolean v3, Lcom/samsung/knox/rcp/components/contacts/syncer/util/DataSyncConstants;->isRunningCallLogFlag:Z

    .line 46
    sput-boolean v3, Lcom/samsung/knox/rcp/components/contacts/syncer/util/DataSyncConstants;->isRunningCalendarFlag:Z

    .line 48
    sput-boolean v3, Lcom/samsung/knox/rcp/components/contacts/syncer/util/DataSyncConstants;->isRunningContactsFlag:Z

    .line 50
    sput-boolean v3, Lcom/samsung/knox/rcp/components/contacts/syncer/util/DataSyncConstants;->isRunningServiceCheckFlag:Z

    .line 52
    sput-boolean v3, Lcom/samsung/knox/rcp/components/contacts/syncer/util/DataSyncConstants;->isServiceActive:Z

    .line 54
    const-string v0, "DB_BRIDGE_INIT_SYNC"

    sput-object v0, Lcom/samsung/knox/rcp/components/contacts/syncer/util/DataSyncConstants;->PREF_NAME:Ljava/lang/String;

    .line 56
    sget-object v0, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/samsung/knox/rcp/components/contacts/syncer/util/DataSyncConstants;->CON_RAW_CONTACTS_URI:Landroid/net/Uri;

    .line 58
    sget-object v0, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/samsung/knox/rcp/components/contacts/syncer/util/DataSyncConstants;->CON_DATA_URI:Landroid/net/Uri;

    .line 60
    sget-object v0, Landroid/provider/ContactsContract$Groups;->CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/samsung/knox/rcp/components/contacts/syncer/util/DataSyncConstants;->CON_GROUP_URI:Landroid/net/Uri;

    .line 62
    const-string v0, "content://sec_container_1.logs/historys"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/knox/rcp/components/contacts/syncer/util/DataSyncConstants;->CON_LOGS_URI:Landroid/net/Uri;

    .line 64
    const-string v0, "content://com.android.contacts/settings"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/knox/rcp/components/contacts/syncer/util/DataSyncConstants;->CON_ACCOUNTS_URI:Landroid/net/Uri;

    .line 66
    const/16 v0, 0x1a

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "number"

    aput-object v1, v0, v4

    const-string v1, "date"

    aput-object v1, v0, v5

    const-string v1, "duration"

    aput-object v1, v0, v6

    const-string v1, "type"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "new"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "name"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "numbertype"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "numberlabel"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "countryiso"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "geocoded_location"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "lookup_uri"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "matched_number"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "normalized_number"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "photo_id"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "formatted_number"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "frequent"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "e164_number"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "cnap_name"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "cdnip_number"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "country_code"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "cityid"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "fname"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "lname"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "bname"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "contactid"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/knox/rcp/components/contacts/syncer/util/DataSyncConstants;->ORI_CALLLOG_PROJECTION:[Ljava/lang/String;

    .line 122
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "sync3"

    aput-object v1, v0, v3

    const-string v1, "sync4"

    aput-object v1, v0, v4

    const-string v1, "sync2"

    aput-object v1, v0, v5

    const-string v1, "contact_id"

    aput-object v1, v0, v6

    const-string v1, "_id"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "display_name"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "name_verified"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/knox/rcp/components/contacts/syncer/util/DataSyncConstants;->CON_RAWCONTACTS_PROJECTION:[Ljava/lang/String;

    .line 140
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "version"

    aput-object v1, v0, v3

    const-string v1, "contact_id"

    aput-object v1, v0, v4

    const-string v1, "_id"

    aput-object v1, v0, v5

    const-string v1, "starred"

    aput-object v1, v0, v6

    const-string v1, "name_verified"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "display_name"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "display_name_source"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/knox/rcp/components/contacts/syncer/util/DataSyncConstants;->ORI_RAWCONTACTS_PROJECTION:[Ljava/lang/String;

    .line 160
    const/16 v0, 0x13

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "mimetype"

    aput-object v1, v0, v3

    const-string v1, "is_primary"

    aput-object v1, v0, v4

    const-string v1, "data1"

    aput-object v1, v0, v5

    const-string v1, "data2"

    aput-object v1, v0, v6

    const-string v1, "data3"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "data4"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "data5"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "data6"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "data7"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "data8"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "data9"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "data10"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "data11"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "data12"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "data13"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "data14"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "data15"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "raw_contact_id"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "is_super_primary"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/knox/rcp/components/contacts/syncer/util/DataSyncConstants;->ORI_DATA_PROJECTION:[Ljava/lang/String;

    .line 202
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "version"

    aput-object v1, v0, v4

    const-string v1, "title"

    aput-object v1, v0, v5

    const-string v1, "favorites"

    aput-object v1, v0, v6

    const-string v1, "system_id"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "auto_add"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/knox/rcp/components/contacts/syncer/util/DataSyncConstants;->ORI_GROUPS_PROJECTION:[Ljava/lang/String;

    .line 218
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "sync2"

    aput-object v1, v0, v3

    const-string v1, "sync3"

    aput-object v1, v0, v4

    const-string v1, "_id"

    aput-object v1, v0, v5

    sput-object v0, Lcom/samsung/knox/rcp/components/contacts/syncer/util/DataSyncConstants;->CON_GROUPS_PROJECTION:[Ljava/lang/String;

    return-void
.end method
