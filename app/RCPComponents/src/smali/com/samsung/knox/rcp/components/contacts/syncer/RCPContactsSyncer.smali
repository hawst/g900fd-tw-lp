.class public Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;
.super Landroid/app/Service;
.source "RCPContactsSyncer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$3;,
        Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$JoinContactQuery;,
        Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;
    }
.end annotation


# static fields
.field static isSyncState:Z


# instance fields
.field private final CUSTOM_CURSORS:Ljava/lang/String;

.field private KNOX_DEBUG:Z

.field private PERFORMANCE_DEBUG:Z

.field TAG:Ljava/lang/String;

.field contactsRcpDb:Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;

.field private mAccountTypeSP:Landroid/content/SharedPreferences;

.field private mAccountTypeSPEditor:Landroid/content/SharedPreferences$Editor;

.field private mAccountsSharedPreferences:Landroid/content/SharedPreferences;

.field private mAccountsSharedPrefsEditor:Landroid/content/SharedPreferences$Editor;

.field mBinderProxy:Landroid/os/IBinder;

.field mBridgeService:Lcom/sec/knox/bridge/IBridgeService;

.field mContext:Landroid/content/Context;

.field mPm:Landroid/os/PersonaManager;

.field private mSetStartServiceCallers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field mSyncThread:Ljava/lang/Thread;

.field mUm:Landroid/os/UserManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 111
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->isSyncState:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 78
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 80
    const-string v0, "Contacts_DataSyncService"

    iput-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    .line 82
    iput-object v1, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mBridgeService:Lcom/sec/knox/bridge/IBridgeService;

    .line 87
    iput-boolean v2, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    .line 88
    iput-boolean v2, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->PERFORMANCE_DEBUG:Z

    .line 90
    const-string v0, "CUSTOM_CURSORS"

    iput-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->CUSTOM_CURSORS:Ljava/lang/String;

    .line 91
    iput-object v1, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mAccountsSharedPreferences:Landroid/content/SharedPreferences;

    .line 92
    iput-object v1, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mAccountsSharedPrefsEditor:Landroid/content/SharedPreferences$Editor;

    .line 93
    iput-object v1, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mAccountTypeSP:Landroid/content/SharedPreferences;

    .line 94
    iput-object v1, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mAccountTypeSPEditor:Landroid/content/SharedPreferences$Editor;

    .line 103
    iput-object v1, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mSetStartServiceCallers:Ljava/util/List;

    .line 106
    iput-object v1, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mSyncThread:Ljava/lang/Thread;

    .line 107
    iput-object v1, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mBinderProxy:Landroid/os/IBinder;

    .line 108
    iput-object v1, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->contactsRcpDb:Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;

    .line 109
    iput-object v1, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mPm:Landroid/os/PersonaManager;

    .line 110
    iput-object v1, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mUm:Landroid/os/UserManager;

    .line 1959
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;

    .prologue
    .line 78
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mSetStartServiceCallers:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;
    .param p1, "x1"    # I

    .prologue
    .line 78
    invoke-direct {p0, p1}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->deletePersonaData(I)V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 78
    invoke-direct {p0, p1}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->stopMySelf(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$300(Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;

    .prologue
    .line 78
    iget-boolean v0, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    return v0
.end method

.method static synthetic access$400(Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;

    .prologue
    .line 78
    invoke-direct {p0}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->doSync()V

    return-void
.end method

.method private addNewAccount(Ljava/lang/String;)Landroid/net/Uri;
    .locals 10
    .param p1, "userPersonaId"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 2124
    iget-boolean v7, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v7, :cond_0

    .line 2125
    iget-object v7, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v8, "addNewAccount() called"

    invoke-static {v7, v8}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2126
    :cond_0
    iget-boolean v7, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->PERFORMANCE_DEBUG:Z

    if-eqz v7, :cond_1

    .line 2127
    iget-object v7, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v8, "addNewAccount() called"

    invoke-static {v7, v8}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2128
    :cond_1
    const/4 v4, -0x1

    .line 2130
    .local v4, "userPersonaIdInt":I
    :try_start_0
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    .line 2135
    iget-object v7, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mPm:Landroid/os/PersonaManager;

    invoke-virtual {v7, v4}, Landroid/os/PersonaManager;->exists(I)Z

    move-result v1

    .line 2136
    .local v1, "isPersona":Z
    const/4 v3, 0x0

    .line 2137
    .local v3, "result":Landroid/net/Uri;
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 2138
    .local v5, "values":Landroid/content/ContentValues;
    invoke-virtual {v5}, Landroid/content/ContentValues;->clear()V

    .line 2139
    if-eqz v1, :cond_5

    .line 2140
    invoke-virtual {p0, v4}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->createOrGetAccountTypeForKnox(I)Ljava/lang/String;

    move-result-object v2

    .line 2141
    .local v2, "knoxAccountType":Ljava/lang/String;
    iget-boolean v7, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v7, :cond_2

    .line 2142
    iget-object v7, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, " addNewAccount() knoxAccountType = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " for user = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2144
    :cond_2
    const-string v7, "account_name"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "vnd.sec.contact.phone_knox"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2146
    const-string v7, "account_type"

    invoke-virtual {v5, v7, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2153
    .end local v2    # "knoxAccountType":Ljava/lang/String;
    :goto_0
    :try_start_1
    iget-object v7, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v8, "content://com.android.contacts/settings"

    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    invoke-virtual {v7, v8, v5}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v3

    .line 2155
    iget-boolean v7, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v7, :cond_3

    .line 2156
    iget-object v7, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v8, "init_AddAccount Completed"

    invoke-static {v7, v8}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2157
    :cond_3
    iget-boolean v7, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->PERFORMANCE_DEBUG:Z

    if-eqz v7, :cond_4

    .line 2158
    iget-object v7, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v8, "init_AddAccount() completed"

    invoke-static {v7, v8}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2160
    :cond_4
    const/4 v7, 0x1

    invoke-direct {p0, v4, v7}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->sendPolicyChangeToContact(II)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 2167
    .end local v1    # "isPersona":Z
    .end local v3    # "result":Landroid/net/Uri;
    .end local v5    # "values":Landroid/content/ContentValues;
    :goto_1
    return-object v3

    .line 2131
    :catch_0
    move-exception v0

    .line 2132
    .local v0, "e":Ljava/lang/Exception;
    iget-object v7, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "addNewAccount() NumberFormatException"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object v3, v6

    .line 2133
    goto :goto_1

    .line 2148
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "isPersona":Z
    .restart local v3    # "result":Landroid/net/Uri;
    .restart local v5    # "values":Landroid/content/ContentValues;
    :cond_5
    const-string v7, "account_name"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "vnd.sec.contact.phone_personal"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2149
    const-string v7, "account_type"

    const-string v8, "vnd.sec.contact.phone_personal"

    invoke-virtual {v5, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2162
    :catch_1
    move-exception v0

    .line 2163
    .restart local v0    # "e":Ljava/lang/Exception;
    iget-object v7, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "DataSyncService Exception during addNewAccount()="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {v0}, Lcom/samsung/knox/rcp/components/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object v3, v6

    .line 2167
    goto :goto_1
.end method

.method private declared-synchronized addNewAccountIfRequired(I)Z
    .locals 8
    .param p1, "userOrPersonaId"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 2081
    monitor-enter p0

    :try_start_0
    iget-object v5, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mAccountsSharedPreferences:Landroid/content/SharedPreferences;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mAccountsSharedPrefsEditor:Landroid/content/SharedPreferences$Editor;

    if-eqz v5, :cond_1

    .line 2082
    :cond_0
    iget-object v5, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mContext:Landroid/content/Context;

    const-string v6, "created_accounts_uris_per_persona_shared_prefs"

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mAccountsSharedPreferences:Landroid/content/SharedPreferences;

    .line 2084
    iget-object v5, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mAccountsSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mAccountsSharedPrefsEditor:Landroid/content/SharedPreferences$Editor;

    .line 2086
    :cond_1
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    .line 2087
    .local v2, "userOrPersonaIdStr":Ljava/lang/String;
    iget-object v5, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mAccountsSharedPreferences:Landroid/content/SharedPreferences;

    const/4 v6, 0x0

    invoke-interface {v5, v2, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2088
    .local v1, "uriStr":Ljava/lang/String;
    if-eqz v1, :cond_3

    .line 2089
    iget-boolean v4, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v4, :cond_2

    .line 2090
    iget-object v4, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "DataSyncService.addNewAccountIfRequired(): account found for the userOrPersonaId: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "; uri = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2119
    :cond_2
    :goto_0
    monitor-exit p0

    return v3

    .line 2097
    :cond_3
    :try_start_1
    invoke-direct {p0, v2}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->addNewAccount(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2099
    .local v0, "accountUri":Landroid/net/Uri;
    if-eqz v0, :cond_5

    .line 2100
    iget-object v4, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mAccountsSharedPrefsEditor:Landroid/content/SharedPreferences$Editor;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v2, v5}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 2101
    iget-object v4, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mAccountsSharedPrefsEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2102
    iget-object v4, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mAccountsSharedPrefsEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2103
    iget-object v4, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mAccountsSharedPrefsEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2105
    iget-boolean v4, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v4, :cond_4

    .line 2106
    iget-object v4, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "DataSyncService new Account Added during addNewAccountIfRequired(): "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2108
    :cond_4
    iget-boolean v4, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v4, :cond_2

    .line 2109
    iget-object v4, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "DataSyncService new Account Added during addNewAccountIfRequired() Id is  = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v0}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2081
    .end local v0    # "accountUri":Landroid/net/Uri;
    .end local v1    # "uriStr":Ljava/lang/String;
    .end local v2    # "userOrPersonaIdStr":Ljava/lang/String;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 2115
    .restart local v0    # "accountUri":Landroid/net/Uri;
    .restart local v1    # "uriStr":Ljava/lang/String;
    .restart local v2    # "userOrPersonaIdStr":Ljava/lang/String;
    :cond_5
    :try_start_2
    iget-boolean v3, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v3, :cond_6

    .line 2116
    iget-object v3, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "DataSyncService.addNewAccountIfRequired(): account not exists; uri = null; cannot create account for the userOrPersonaId: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_6
    move v3, v4

    .line 2119
    goto/16 :goto_0
.end method

.method private argsArrayToString(Ljava/util/List;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 2503
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 2504
    .local v2, "sBuilder":Ljava/lang/StringBuilder;
    const-string v3, "("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2505
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    .line 2506
    .local v0, "Count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 2507
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2508
    add-int/lit8 v3, v0, -0x1

    if-ge v1, v3, :cond_0

    .line 2509
    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2506
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2512
    :cond_1
    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2514
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method private buildJoinContactDiff(Ljava/util/ArrayList;JJ)V
    .locals 4
    .param p2, "rawContactId1"    # J
    .param p4, "rawContactId2"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;JJ)V"
        }
    .end annotation

    .prologue
    .line 2069
    .local p1, "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    iget-boolean v1, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v1, :cond_0

    .line 2070
    iget-object v1, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "buildJoinContactDiff with: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2071
    :cond_0
    sget-object v1, Landroid/provider/ContactsContract$AggregationExceptions;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    .line 2073
    .local v0, "builder":Landroid/content/ContentProviderOperation$Builder;
    const-string v1, "type"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 2074
    const-string v1, "raw_contact_id1"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 2075
    const-string v1, "raw_contact_id2"

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 2076
    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2077
    return-void
.end method

.method private closeCursor(Landroid/database/Cursor;)V
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 2236
    if-nez p1, :cond_0

    .line 2243
    :goto_0
    return-void

    .line 2239
    :cond_0
    :try_start_0
    invoke-interface {p1}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2240
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private deletePersonaData(I)V
    .locals 4
    .param p1, "personaID"    # I

    .prologue
    .line 267
    const/4 v1, 0x0

    invoke-direct {p0, p1, v1}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->sendPolicyChangeToContact(II)V

    .line 269
    iget-boolean v1, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v1, :cond_0

    .line 270
    iget-object v1, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " DataSyncService Contact deletePersonaData logic : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 272
    :cond_0
    invoke-virtual {p0, p1}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->deleteContactForPersona(I)Z

    move-result v0

    .line 273
    .local v0, "result":Z
    iget-boolean v1, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v1, :cond_1

    .line 274
    iget-object v1, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " deletePersonaData deleteContactForPersona : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    :cond_1
    invoke-virtual {p0, p1}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->deleteGroupForPersona(I)Z

    move-result v0

    .line 277
    iget-boolean v1, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v1, :cond_2

    .line 278
    iget-object v1, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " deletePersonaData deleteGroupForPersona : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 280
    :cond_2
    invoke-virtual {p0, p1}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->deleteAccountForPersona(I)Z

    move-result v0

    .line 281
    iget-boolean v1, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v1, :cond_3

    .line 282
    iget-object v1, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " deletePersonaData deleteAccountForPersona : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 284
    :cond_3
    invoke-virtual {p0, p1}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->deleteSharedPrefsForPersona(I)Z

    move-result v0

    .line 285
    iget-boolean v1, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v1, :cond_4

    .line 286
    iget-object v1, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " deletePersonaData deleteSharedPrefsForPersona : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 287
    :cond_4
    return-void
.end method

.method private doSync()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 332
    iget-object v2, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v3, "doSync() START"

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 334
    iget-object v2, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mContext:Landroid/content/Context;

    const-string v3, "firsttimepref"

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 336
    .local v1, "firstTimePreferences":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 338
    .local v0, "firstTimeEditor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "firsttime"

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_1

    .line 339
    iget-boolean v2, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v2, :cond_0

    .line 340
    iget-object v2, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v3, "doSync() called for the first time"

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 342
    :cond_0
    invoke-direct {p0}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->init_syncGroup()V

    .line 344
    iget-object v2, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v3, "Init Contact Group Sync Complete"

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 346
    invoke-direct {p0}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->init_syncRawContacts()V

    .line 348
    const-string v2, "firsttime"

    const/4 v3, 0x1

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 349
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 358
    :goto_0
    iget-object v2, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v3, "doSync() END"

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 359
    return-void

    .line 351
    :cond_1
    invoke-direct {p0}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->syncGroup()V

    .line 353
    iget-object v2, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v3, "DataSyncService syncProvider completed syncGroup, starting syncContacts "

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 355
    invoke-direct {p0}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->syncContacts()V

    goto :goto_0
.end method

.method private getAvailableAccountType(I)Ljava/lang/String;
    .locals 5
    .param p1, "personaId"    # I

    .prologue
    .line 2220
    iget-object v3, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mPm:Landroid/os/PersonaManager;

    invoke-virtual {v3, p1}, Landroid/os/PersonaManager;->getPersonaInfo(I)Landroid/content/pm/PersonaInfo;

    move-result-object v0

    .line 2222
    .local v0, "pInfo":Landroid/content/pm/PersonaInfo;
    iget-object v3, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mUm:Landroid/os/UserManager;

    iget v4, v0, Landroid/content/pm/PersonaInfo;->id:I

    invoke-virtual {v3, v4}, Landroid/os/UserManager;->getUserInfo(I)Landroid/content/pm/UserInfo;

    move-result-object v2

    .line 2223
    .local v2, "ui":Landroid/content/pm/UserInfo;
    iget-object v1, v2, Landroid/content/pm/UserInfo;->name:Ljava/lang/String;

    .line 2225
    .local v1, "personaName":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 2226
    const-string v3, "<DUMMY>"

    .line 2231
    :goto_0
    return-object v3

    .line 2228
    :cond_0
    const-string v3, "KNOX"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2229
    const-string v3, "vnd.sec.contact.phone_knox"

    goto :goto_0

    .line 2231
    :cond_1
    const-string v3, "vnd.sec.contact.phone_knox2"

    goto :goto_0
.end method

.method private init_syncGroup()V
    .locals 25

    .prologue
    .line 459
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v2, :cond_0

    .line 460
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v9, "init_syncGroup called"

    invoke-static {v2, v9}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 461
    :cond_0
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->PERFORMANCE_DEBUG:Z

    if-eqz v2, :cond_1

    .line 462
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v9, "init_syncGroup called"

    invoke-static {v2, v9}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 467
    :cond_1
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->contactsRcpDb:Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;

    invoke-virtual {v2}, Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;->deleteAllGroups()V

    .line 468
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v9, Landroid/provider/ContactsContract$Groups;->CONTENT_URI:Landroid/net/Uri;

    const-string v21, "account_type = ? OR account_type LIKE ? "

    const/16 v22, 0x2

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    const-string v24, "vnd.sec.contact.phone_personal"

    aput-object v24, v22, v23

    const/16 v23, 0x1

    const-string v24, "vnd.sec.contact.phone_knox%"

    aput-object v24, v22, v23

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v2, v9, v0, v1}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 480
    :goto_0
    const/4 v11, 0x0

    .line 482
    .local v11, "customCursorsList":Ljava/util/List;, "Ljava/util/List<Landroid/content/CustomCursor;>;"
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mBridgeService:Lcom/sec/knox/bridge/IBridgeService;

    const-string v3, "Contacts"

    const-string v4, "GROUPS_CONTACTS"

    const/4 v5, 0x0

    sget-object v6, Lcom/samsung/knox/rcp/components/contacts/syncer/util/DataSyncConstants;->ORI_GROUPS_PROJECTION:[Ljava/lang/String;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const-string v9, "_id ASC"

    invoke-interface/range {v2 .. v9}, Lcom/sec/knox/bridge/IBridgeService;->queryAllProviders(Ljava/lang/String;Ljava/lang/String;I[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v11

    .line 492
    if-eqz v11, :cond_2

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_4

    .line 493
    :cond_2
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v2, :cond_3

    .line 494
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "DataSyncService.init_syncGroup(): customCursorsList is null or empty: customCursorsList="

    move-object/from16 v0, v21

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v2, v9}, Lcom/samsung/knox/rcp/components/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 623
    :cond_3
    :goto_1
    return-void

    .line 474
    .end local v11    # "customCursorsList":Ljava/util/List;, "Ljava/util/List<Landroid/content/CustomCursor;>;"
    :catch_0
    move-exception v12

    .line 475
    .local v12, "e":Ljava/lang/Exception;
    invoke-virtual {v12}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 486
    .end local v12    # "e":Ljava/lang/Exception;
    .restart local v11    # "customCursorsList":Ljava/util/List;, "Ljava/util/List<Landroid/content/CustomCursor;>;"
    :catch_1
    move-exception v12

    .line 487
    .restart local v12    # "e":Ljava/lang/Exception;
    invoke-virtual {v12}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 500
    .end local v12    # "e":Ljava/lang/Exception;
    :cond_4
    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    .local v15, "i$":Ljava/util/Iterator;
    :cond_5
    :goto_2
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_11

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Landroid/content/CustomCursor;

    .line 502
    .local v19, "orig_group_cur":Landroid/content/CustomCursor;
    if-nez v19, :cond_6

    .line 503
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v2, :cond_5

    .line 504
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v9, "init_syncGroup: orig_group_cur is null; skipping..."

    invoke-static {v2, v9}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 508
    :cond_6
    const/4 v13, 0x0

    .line 509
    .local v13, "groupCount":I
    const/16 v8, -0x457

    .line 512
    .local v8, "userOrPersonaId":I
    :try_start_2
    invoke-virtual/range {v19 .. v19}, Landroid/content/CustomCursor;->getCount()I

    move-result v13

    .line 513
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v2, :cond_7

    .line 514
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "DataSyncService init_syncGroup Original cursor Count is : "

    move-object/from16 v0, v21

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v2, v9}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 517
    :cond_7
    if-lez v13, :cond_10

    .line 518
    invoke-virtual/range {v19 .. v19}, Landroid/content/CustomCursor;->moveToFirst()Z

    .line 520
    invoke-virtual/range {v19 .. v19}, Landroid/content/CustomCursor;->getCursorOwnerId()I

    move-result v8

    .line 521
    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->addNewAccountIfRequired(I)Z

    .line 522
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mPm:Landroid/os/PersonaManager;

    invoke-virtual {v2, v8}, Landroid/os/PersonaManager;->exists(I)Z

    move-result v16

    .line 525
    .local v16, "isPersona":Z
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 527
    .local v18, "ops_group":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    const/4 v14, 0x0

    .local v14, "i":I
    :goto_3
    if-ge v14, v13, :cond_8

    .line 530
    sget-boolean v2, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->isSyncState:Z

    if-nez v2, :cond_a

    .line 531
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v9, "mSyncThread interrupt "

    invoke-static {v2, v9}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 611
    .end local v14    # "i":I
    .end local v16    # "isPersona":Z
    .end local v18    # "ops_group":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    :cond_8
    :goto_4
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->closeCursor(Landroid/database/Cursor;)V

    .line 613
    :goto_5
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v2, :cond_9

    .line 614
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "DataSyncService init_syncGroup completed for cursorOwnerId="

    move-object/from16 v0, v21

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v2, v9}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 616
    :cond_9
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->PERFORMANCE_DEBUG:Z

    if-eqz v2, :cond_5

    .line 617
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "init_syncGroup completed for cursorOwnerId="

    move-object/from16 v0, v21

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v2, v9}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 534
    .restart local v14    # "i":I
    .restart local v16    # "isPersona":Z
    .restart local v18    # "ops_group":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    :cond_a
    :try_start_3
    sget-object v2, Lcom/samsung/knox/rcp/components/contacts/syncer/util/DataSyncConstants;->CON_GROUP_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v10

    .line 536
    .local v10, "builder_group":Landroid/content/ContentProviderOperation$Builder;
    const-string v2, "title"

    const-string v9, "title"

    move-object/from16 v0, v19

    invoke-virtual {v0, v9}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    move-object/from16 v0, v19

    invoke-virtual {v0, v9}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v10, v2, v9}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 539
    const-string v2, "system_id"

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_e

    .line 541
    const-string v2, "system_id"

    const-string v9, "PersonalGroup"

    invoke-virtual {v10, v2, v9}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 548
    :goto_6
    const-string v2, "group_is_read_only"

    const/4 v9, 0x1

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v10, v2, v9}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 549
    if-eqz v16, :cond_f

    .line 550
    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->createOrGetAccountTypeForKnox(I)Ljava/lang/String;

    move-result-object v17

    .line 551
    .local v17, "knoxAccountType":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v2, :cond_b

    .line 552
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, " init_syncGroup() knoxAccountType = "

    move-object/from16 v0, v21

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move-object/from16 v0, v17

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v21, " for user = "

    move-object/from16 v0, v21

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v2, v9}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 554
    :cond_b
    const-string v2, "account_name"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "vnd.sec.contact.phone_knox"

    move-object/from16 v0, v21

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v10, v2, v9}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 556
    const-string v2, "account_type"

    move-object/from16 v0, v17

    invoke-virtual {v10, v2, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 564
    .end local v17    # "knoxAccountType":Ljava/lang/String;
    :goto_7
    const-string v2, "favorites"

    const-string v9, "favorites"

    move-object/from16 v0, v19

    invoke-virtual {v0, v9}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    move-object/from16 v0, v19

    invoke-virtual {v0, v9}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v10, v2, v9}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 567
    const-string v2, "auto_add"

    const-string v9, "auto_add"

    move-object/from16 v0, v19

    invoke-virtual {v0, v9}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    move-object/from16 v0, v19

    invoke-virtual {v0, v9}, Landroid/content/CustomCursor;->getInt(I)I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v10, v2, v9}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 570
    const-string v2, "group_visible"

    const/4 v9, 0x1

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v10, v2, v9}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 572
    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->clear()V

    .line 573
    invoke-virtual {v10}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 576
    :try_start_4
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v2, :cond_c

    .line 577
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "DataSyncService init_syncGroup adding new group "

    move-object/from16 v0, v21

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v21, "title"

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v21

    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v2, v9}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 581
    :cond_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v9, "com.android.contacts"

    move-object/from16 v0, v18

    invoke-virtual {v2, v9, v0}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    move-result-object v20

    .line 585
    .local v20, "rowIds":[Landroid/content/ContentProviderResult;
    const/4 v2, 0x0

    aget-object v2, v20, v2

    iget-object v2, v2, Landroid/content/ContentProviderResult;->uri:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v6

    .line 586
    .local v6, "id_container":J
    const-string v2, "_id"

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Landroid/content/CustomCursor;->getLong(I)J

    move-result-wide v3

    .line 588
    .local v3, "id_original":J
    const-string v2, "version"

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Landroid/content/CustomCursor;->getInt(I)I

    move-result v5

    .line 591
    .local v5, "version":I
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v2, :cond_d

    .line 592
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "DataSyncService init_syncGroup inserting to local db new group "

    move-object/from16 v0, v21

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v21, " "

    move-object/from16 v0, v21

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v21, " "

    move-object/from16 v0, v21

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v21, " "

    move-object/from16 v0, v21

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v2, v9}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 596
    :cond_d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->contactsRcpDb:Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;

    invoke-virtual/range {v2 .. v8}, Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;->insertGroup(JIJI)J
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_3
    .catch Landroid/content/OperationApplicationException; {:try_start_4 .. :try_end_4} :catch_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 604
    .end local v3    # "id_original":J
    .end local v5    # "version":I
    .end local v6    # "id_container":J
    .end local v20    # "rowIds":[Landroid/content/ContentProviderResult;
    :goto_8
    :try_start_5
    invoke-virtual/range {v19 .. v19}, Landroid/content/CustomCursor;->moveToNext()Z

    .line 527
    add-int/lit8 v14, v14, 0x1

    goto/16 :goto_3

    .line 544
    :cond_e
    const-string v2, "system_id"

    const-string v9, "system_id"

    move-object/from16 v0, v19

    invoke-virtual {v0, v9}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    move-object/from16 v0, v19

    invoke-virtual {v0, v9}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v10, v2, v9}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_6

    .line 608
    .end local v10    # "builder_group":Landroid/content/ContentProviderOperation$Builder;
    .end local v14    # "i":I
    .end local v16    # "isPersona":Z
    .end local v18    # "ops_group":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    :catch_2
    move-exception v12

    .line 609
    .restart local v12    # "e":Ljava/lang/Exception;
    :try_start_6
    invoke-virtual {v12}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 611
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->closeCursor(Landroid/database/Cursor;)V

    goto/16 :goto_5

    .line 559
    .end local v12    # "e":Ljava/lang/Exception;
    .restart local v10    # "builder_group":Landroid/content/ContentProviderOperation$Builder;
    .restart local v14    # "i":I
    .restart local v16    # "isPersona":Z
    .restart local v18    # "ops_group":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    :cond_f
    :try_start_7
    const-string v2, "account_name"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "vnd.sec.contact.phone_personal"

    move-object/from16 v0, v21

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v10, v2, v9}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 561
    const-string v2, "account_type"

    const-string v9, "vnd.sec.contact.phone_personal"

    invoke-virtual {v10, v2, v9}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_7

    .line 611
    .end local v10    # "builder_group":Landroid/content/ContentProviderOperation$Builder;
    .end local v14    # "i":I
    .end local v16    # "isPersona":Z
    .end local v18    # "ops_group":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    :catchall_0
    move-exception v2

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->closeCursor(Landroid/database/Cursor;)V

    throw v2

    .line 598
    .restart local v10    # "builder_group":Landroid/content/ContentProviderOperation$Builder;
    .restart local v14    # "i":I
    .restart local v16    # "isPersona":Z
    .restart local v18    # "ops_group":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    :catch_3
    move-exception v12

    .line 599
    .local v12, "e":Landroid/os/RemoteException;
    :try_start_8
    invoke-virtual {v12}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_8

    .line 600
    .end local v12    # "e":Landroid/os/RemoteException;
    :catch_4
    move-exception v12

    .line 601
    .local v12, "e":Landroid/content/OperationApplicationException;
    invoke-virtual {v12}, Landroid/content/OperationApplicationException;->printStackTrace()V

    goto :goto_8

    .line 606
    .end local v10    # "builder_group":Landroid/content/ContentProviderOperation$Builder;
    .end local v12    # "e":Landroid/content/OperationApplicationException;
    .end local v14    # "i":I
    .end local v16    # "isPersona":Z
    .end local v18    # "ops_group":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    :cond_10
    if-nez v13, :cond_8

    .line 607
    invoke-virtual/range {v19 .. v19}, Landroid/content/CustomCursor;->getCursorOwnerId()I

    move-result v2

    const/4 v9, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v9}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->sendPolicyChangeToContact(II)V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_2
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto/16 :goto_4

    .line 619
    .end local v8    # "userOrPersonaId":I
    .end local v13    # "groupCount":I
    .end local v19    # "orig_group_cur":Landroid/content/CustomCursor;
    :cond_11
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v2, :cond_12

    .line 620
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v9, "DataSyncService init_syncGroup completed"

    invoke-static {v2, v9}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 621
    :cond_12
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->PERFORMANCE_DEBUG:Z

    if-eqz v2, :cond_3

    .line 622
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v9, "init_syncGroup completed"

    invoke-static {v2, v9}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method private init_syncRawContacts()V
    .locals 23

    .prologue
    .line 626
    const/4 v14, 0x0

    .line 627
    .local v14, "contactsCount":I
    const/16 v22, 0x0

    .line 628
    .local v22, "userOrPersonaId":I
    const/16 v13, 0x64

    .line 629
    .local v13, "OPS_SIZE":I
    const/4 v15, 0x0

    .line 631
    .local v15, "customCursorsList":Ljava/util/List;, "Ljava/util/List<Landroid/content/CustomCursor;>;"
    :try_start_0
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->contactsRcpDb:Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;

    invoke-virtual {v5}, Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;->deleteAllContacts()V

    .line 632
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    const-string v7, "account_type = ? OR account_type LIKE ? "

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    const-string v10, "vnd.sec.contact.phone_personal"

    aput-object v10, v8, v9

    const/4 v9, 0x1

    const-string v10, "vnd.sec.contact.phone_knox%"

    aput-object v10, v8, v9

    invoke-virtual {v5, v6, v7, v8}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 644
    :goto_0
    :try_start_1
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mBridgeService:Lcom/sec/knox/bridge/IBridgeService;

    const-string v6, "Contacts"

    const-string v7, "RAW_CONTACTS"

    const/4 v8, 0x0

    sget-object v9, Lcom/samsung/knox/rcp/components/contacts/syncer/util/DataSyncConstants;->ORI_RAWCONTACTS_PROJECTION:[Ljava/lang/String;

    const/4 v10, 0x0

    const/4 v11, 0x0

    const-string v12, "_id ASC"

    invoke-interface/range {v5 .. v12}, Lcom/sec/knox/bridge/IBridgeService;->queryAllProviders(Ljava/lang/String;Ljava/lang/String;I[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v15

    .line 658
    if-eqz v15, :cond_0

    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v5

    if-nez v5, :cond_1

    .line 707
    :cond_0
    :goto_1
    return-void

    .line 638
    :catch_0
    move-exception v16

    .line 639
    .local v16, "e":Ljava/lang/Exception;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Re init deleting all raw contacts throws exeception "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v16

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 653
    .end local v16    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v16

    .line 654
    .restart local v16    # "e":Ljava/lang/Exception;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Exception during init_syncContacts() mBridgeService.queryAllProviders();  "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static/range {v16 .. v16}, Lcom/samsung/knox/rcp/components/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/knox/rcp/components/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 662
    .end local v16    # "e":Ljava/lang/Exception;
    :cond_1
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    .line 664
    .local v19, "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    invoke-interface {v15}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v18

    .local v18, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_2
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Landroid/content/CustomCursor;

    .line 665
    .local v20, "orig_raw_cur":Landroid/content/CustomCursor;
    if-eqz v20, :cond_2

    .line 668
    sget-boolean v5, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->isSyncState:Z

    if-nez v5, :cond_3

    .line 669
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v6, "mSyncThread interrupt "

    invoke-static {v5, v6}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 672
    :cond_3
    invoke-virtual/range {v20 .. v20}, Landroid/content/CustomCursor;->getCursorOwnerId()I

    move-result v22

    .line 674
    invoke-virtual/range {v20 .. v20}, Landroid/content/CustomCursor;->moveToFirst()Z

    move-result v5

    if-nez v5, :cond_4

    .line 675
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "init_syncContacts: orig_raw_cur is empty for userOrPersonaId = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v22

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "; skipping..."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 677
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->closeCursor(Landroid/database/Cursor;)V

    goto :goto_2

    .line 681
    :cond_4
    invoke-virtual/range {v20 .. v20}, Landroid/content/CustomCursor;->getCount()I

    move-result v14

    .line 682
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "contactsCount size "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 683
    new-instance v21, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;

    invoke-direct/range {v21 .. v21}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;-><init>()V

    .line 685
    .local v21, "rawContactInfo":Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;
    const/16 v17, 0x0

    .local v17, "i":I
    :goto_3
    move/from16 v0, v17

    if-ge v0, v14, :cond_6

    .line 686
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->isBridgeServiceValid()Z

    move-result v5

    if-nez v5, :cond_5

    .line 687
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v5, :cond_0

    .line 688
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v6, "init_syncContacts(): isBridgeServiceValid() == false; stopping syncing...."

    invoke-static {v5, v6}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 692
    :cond_5
    sget-boolean v5, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->isSyncState:Z

    if-nez v5, :cond_7

    .line 693
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v6, "mSyncThread interrupt "

    invoke-static {v5, v6}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 704
    :cond_6
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v14, v1, v2}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->insertLocalDB(ILcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;I)V

    .line 705
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->closeCursor(Landroid/database/Cursor;)V

    goto/16 :goto_2

    .line 697
    :cond_7
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    move/from16 v3, v22

    move-object/from16 v4, v21

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->insertRawContact(Ljava/util/ArrayList;Landroid/content/CustomCursor;ILcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;)V

    .line 699
    add-int/lit8 v5, v17, 0x1

    rem-int/lit8 v5, v5, 0x64

    if-eqz v5, :cond_8

    add-int/lit8 v5, v17, 0x1

    if-ne v5, v14, :cond_9

    .line 700
    :cond_8
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move/from16 v2, v22

    move-object/from16 v3, v21

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->applyBatchRawContacts(Ljava/util/ArrayList;ILcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;)V

    .line 702
    :cond_9
    invoke-virtual/range {v20 .. v20}, Landroid/content/CustomCursor;->moveToNext()Z

    .line 685
    add-int/lit8 v17, v17, 0x1

    goto :goto_3
.end method

.method private insertDatas(Landroid/content/CustomCursor;ILcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;)V
    .locals 18
    .param p1, "c"    # Landroid/content/CustomCursor;
    .param p2, "userOrPersonaId"    # I
    .param p3, "rawContactInfo"    # Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;

    .prologue
    .line 796
    const/4 v4, 0x0

    .line 797
    .local v4, "contactsCount":I
    const/16 v2, 0xc8

    .line 798
    .local v2, "OPS_SIZE":I
    const/4 v9, 0x0

    .line 799
    .local v9, "isSystem_group":Z
    const/4 v7, 0x0

    .line 800
    .local v7, "groupSyncMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    if-eqz p1, :cond_0

    invoke-virtual/range {p1 .. p1}, Landroid/content/CustomCursor;->moveToFirst()Z

    move-result v14

    if-nez v14, :cond_1

    .line 889
    :cond_0
    :goto_0
    return-void

    .line 802
    :cond_1
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 804
    .local v5, "dataOps":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    invoke-virtual/range {p1 .. p1}, Landroid/content/CustomCursor;->getCount()I

    move-result v4

    .line 805
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "data count : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 807
    invoke-static/range {p0 .. p0}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncWorker;->getInstance(Landroid/content/Context;)Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncWorker;

    move-result-object v14

    move/from16 v0, p2

    move-object/from16 v1, p0

    invoke-virtual {v14, v0, v1}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncWorker;->refreshGroupSyncMap(ILcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;)Ljava/util/HashMap;

    move-result-object v7

    .line 810
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_1
    if-ge v8, v4, :cond_2

    .line 812
    sget-boolean v14, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->isSyncState:Z

    if-nez v14, :cond_3

    .line 813
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v15, "mSyncThread interrupt "

    invoke-static {v14, v15}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 888
    :cond_2
    invoke-direct/range {p0 .. p1}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->closeCursor(Landroid/database/Cursor;)V

    goto :goto_0

    .line 816
    :cond_3
    sget-object v14, Lcom/samsung/knox/rcp/components/contacts/syncer/util/DataSyncConstants;->CON_DATA_URI:Landroid/net/Uri;

    invoke-static {v14}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    .line 819
    .local v3, "builder_data":Landroid/content/ContentProviderOperation$Builder;
    const-string v14, "raw_contact_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Landroid/content/CustomCursor;->getLong(I)J

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    .line 821
    .local v13, "raw_contact_id":Ljava/lang/Long;
    invoke-virtual {v13}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    const-wide/16 v16, -0x1

    cmp-long v14, v14, v16

    if-eqz v14, :cond_4

    invoke-virtual/range {p3 .. p3}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;->getRawContactMap()Ljava/util/HashMap;

    move-result-object v14

    invoke-virtual {v14, v13}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    if-nez v14, :cond_6

    .line 823
    :cond_4
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "don\'t add ops because of id is null "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 810
    :cond_5
    :goto_2
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 827
    :cond_6
    sget-object v14, Lcom/samsung/knox/rcp/components/contacts/syncer/util/DataSyncConstants;->ORI_DATA_PROJECTION:[Ljava/lang/String;

    const/4 v15, 0x0

    aget-object v14, v14, v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 830
    .local v11, "mimetype":Ljava/lang/String;
    const-string v14, "vnd.android.cursor.item/photo"

    invoke-virtual {v14, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_9

    .line 831
    const-string v14, "data14"

    sget-object v15, Lcom/samsung/knox/rcp/components/contacts/syncer/util/DataSyncConstants;->ORI_DATA_PROJECTION:[Ljava/lang/String;

    const/16 v16, 0xf

    aget-object v15, v15, v16

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v3, v14, v15}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 833
    const-string v14, "data15"

    sget-object v15, Lcom/samsung/knox/rcp/components/contacts/syncer/util/DataSyncConstants;->ORI_DATA_PROJECTION:[Ljava/lang/String;

    const/16 v16, 0x10

    aget-object v15, v15, v16

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Landroid/content/CustomCursor;->getBlob(I)[B

    move-result-object v15

    invoke-virtual {v3, v14, v15}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 862
    :cond_7
    :goto_3
    if-eqz v9, :cond_d

    .line 863
    const/4 v9, 0x0

    .line 875
    :goto_4
    invoke-virtual/range {p1 .. p1}, Landroid/content/CustomCursor;->moveToNext()Z

    .line 877
    add-int/lit8 v14, v8, 0x1

    rem-int/lit16 v14, v14, 0xc8

    if-eqz v14, :cond_8

    add-int/lit8 v14, v8, 0x1

    if-ne v14, v4, :cond_5

    .line 879
    :cond_8
    :try_start_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mContext:Landroid/content/Context;

    invoke-virtual {v14}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v14

    const-string v15, "com.android.contacts"

    invoke-virtual {v14, v15, v5}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 885
    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    goto :goto_2

    .line 835
    :cond_9
    const-string v14, "vnd.android.cursor.item/group_membership"

    invoke-virtual {v14, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_b

    .line 843
    const-string v14, "data1"

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v12

    .line 846
    .local v12, "org_group_id":I
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v7, v14}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    if-nez v14, :cond_a

    .line 847
    const/4 v9, 0x1

    goto :goto_3

    .line 849
    :cond_a
    const/4 v9, 0x0

    .line 850
    const-string v14, "data1"

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-virtual {v7, v15}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v15

    invoke-virtual {v3, v14, v15}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    goto :goto_3

    .line 854
    .end local v12    # "org_group_id":I
    :cond_b
    const/4 v10, 0x1

    .local v10, "k":I
    :goto_5
    const/16 v14, 0x10

    if-ge v10, v14, :cond_7

    .line 856
    sget-object v14, Lcom/samsung/knox/rcp/components/contacts/syncer/util/DataSyncConstants;->ORI_DATA_PROJECTION:[Ljava/lang/String;

    add-int/lit8 v15, v10, 0x1

    aget-object v14, v14, v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    if-eqz v14, :cond_c

    .line 857
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "data"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    sget-object v15, Lcom/samsung/knox/rcp/components/contacts/syncer/util/DataSyncConstants;->ORI_DATA_PROJECTION:[Ljava/lang/String;

    add-int/lit8 v16, v10, 0x1

    aget-object v15, v15, v16

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v3, v14, v15}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 854
    :cond_c
    add-int/lit8 v10, v10, 0x1

    goto :goto_5

    .line 865
    .end local v10    # "k":I
    :cond_d
    const-string v14, "mimetype"

    invoke-virtual {v3, v14, v11}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 866
    const-string v14, "is_primary"

    sget-object v15, Lcom/samsung/knox/rcp/components/contacts/syncer/util/DataSyncConstants;->ORI_DATA_PROJECTION:[Ljava/lang/String;

    const/16 v16, 0x1

    aget-object v15, v15, v16

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Landroid/content/CustomCursor;->getInt(I)I

    move-result v15

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-virtual {v3, v14, v15}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 868
    const-string v14, "is_super_primary"

    sget-object v15, Lcom/samsung/knox/rcp/components/contacts/syncer/util/DataSyncConstants;->ORI_DATA_PROJECTION:[Ljava/lang/String;

    const/16 v16, 0x12

    aget-object v15, v15, v16

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Landroid/content/CustomCursor;->getInt(I)I

    move-result v15

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-virtual {v3, v14, v15}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 870
    const-string v14, "is_read_only"

    const/4 v15, 0x1

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-virtual {v3, v14, v15}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 871
    const-string v14, "raw_contact_id"

    invoke-virtual/range {p3 .. p3}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;->getRawContactMap()Ljava/util/HashMap;

    move-result-object v15

    invoke-virtual {v15, v13}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v15

    invoke-virtual {v3, v14, v15}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 873
    invoke-virtual {v3}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v14

    invoke-virtual {v5, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_4

    .line 880
    :catch_0
    move-exception v6

    .line 881
    .local v6, "e":Ljava/lang/Exception;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "applyBatch exception "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v6}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 882
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_2
.end method

.method private isPersonaExists(I)Z
    .locals 7
    .param p1, "personaID"    # I

    .prologue
    const/4 v3, 0x0

    .line 2519
    iget-object v4, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mPm:Landroid/os/PersonaManager;

    invoke-virtual {v4, v3}, Landroid/os/PersonaManager;->getPersonas(Z)Ljava/util/List;

    move-result-object v2

    .line 2520
    .local v2, "personaInfos":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PersonaInfo;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/pm/PersonaInfo;

    .line 2521
    .local v1, "personaInfo":Landroid/content/pm/PersonaInfo;
    iget-object v4, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "----- Found persona - "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v1, Landroid/content/pm/PersonaInfo;->id:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2522
    iget v4, v1, Landroid/content/pm/PersonaInfo;->id:I

    if-ne v4, p1, :cond_0

    .line 2523
    const/4 v3, 0x1

    .line 2525
    .end local v1    # "personaInfo":Landroid/content/pm/PersonaInfo;
    :cond_1
    return v3
.end method

.method private declared-synchronized registerRequestStartService(Ljava/lang/String;)V
    .locals 3
    .param p1, "whoWantToStart"    # Ljava/lang/String;

    .prologue
    .line 2456
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v0, :cond_0

    .line 2457
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "registerRequestStartService():  whoWantToStart="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2459
    :cond_0
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mSetStartServiceCallers:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 2460
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mSetStartServiceCallers:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2462
    :cond_1
    monitor-exit p0

    return-void

    .line 2456
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private sendPolicyChangeToContact(II)V
    .locals 11
    .param p1, "personaID"    # I
    .param p2, "msg"    # I

    .prologue
    const/4 v7, 0x0

    .line 228
    const-string v5, "com.sec.knox.containeragent.bridge.2wayflag"

    .line 229
    .local v5, "twoWayAction":Ljava/lang/String;
    const-string v4, "com.sec.knox.containeragent.bridge.device_personal"

    .line 231
    .local v4, "personalAction":Ljava/lang/String;
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    .line 232
    .local v0, "id":I
    if-nez p2, :cond_4

    move v6, v7

    .line 234
    .local v6, "value":Z
    :goto_0
    iget-object v8, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "sendPolicyChangeToContact Current mode : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " sync from : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " status : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    if-nez v0, :cond_6

    .line 238
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 239
    .local v1, "intent":Landroid/content/Intent;
    const/4 v8, -0x1

    if-eq p1, v8, :cond_2

    .line 241
    iget-object v8, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mAccountTypeSP:Landroid/content/SharedPreferences;

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mAccountTypeSPEditor:Landroid/content/SharedPreferences$Editor;

    if-eqz v8, :cond_1

    .line 242
    :cond_0
    iget-object v8, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mContext:Landroid/content/Context;

    const-string v9, "created_accounts_type_per_persona_shared_prefs"

    invoke-virtual {v8, v9, v7}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v7

    iput-object v7, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mAccountTypeSP:Landroid/content/SharedPreferences;

    .line 245
    :cond_1
    iget-object v7, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mAccountTypeSP:Landroid/content/SharedPreferences;

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-interface {v7, v8, v9}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 247
    .local v2, "knoxAccountType":Ljava/lang/String;
    iget-object v7, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "sendPolicyChangeToContact name : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    const-string v7, "vnd.sec.contact.phone_knox"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 249
    const-string v3, "KNOX"

    .line 253
    .local v3, "name":Ljava/lang/String;
    :goto_1
    const-string v7, "name"

    invoke-virtual {v1, v7, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 255
    .end local v2    # "knoxAccountType":Ljava/lang/String;
    .end local v3    # "name":Ljava/lang/String;
    :cond_2
    const-string v7, "TwoWayFlag"

    invoke-virtual {v1, v7, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 256
    invoke-virtual {p0, v1}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->sendBroadcast(Landroid/content/Intent;)V

    .line 263
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_3
    :goto_2
    return-void

    .line 232
    .end local v6    # "value":Z
    :cond_4
    const/4 v6, 0x1

    goto/16 :goto_0

    .line 251
    .restart local v1    # "intent":Landroid/content/Intent;
    .restart local v2    # "knoxAccountType":Ljava/lang/String;
    .restart local v6    # "value":Z
    :cond_5
    const-string v3, "KNOX II"

    .restart local v3    # "name":Ljava/lang/String;
    goto :goto_1

    .line 257
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "knoxAccountType":Ljava/lang/String;
    .end local v3    # "name":Ljava/lang/String;
    :cond_6
    const/16 v7, 0x64

    if-lt v0, v7, :cond_3

    .line 258
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 260
    .restart local v1    # "intent":Landroid/content/Intent;
    const-string v7, "device_personal"

    invoke-virtual {v1, v7, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 261
    invoke-virtual {p0, v1}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_2
.end method

.method private startSyncingProcess()V
    .locals 2

    .prologue
    .line 290
    invoke-virtual {p0}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->isBridgeServiceValid()Z

    move-result v0

    if-nez v0, :cond_0

    .line 291
    iget-boolean v0, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v0, :cond_0

    .line 292
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v1, "startSyncingProcess() failed; mBridgeService is not valid."

    invoke-static {v0, v1}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 295
    :cond_0
    const-string v0, "DO_SYNC"

    invoke-direct {p0, v0}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->registerRequestStartService(Ljava/lang/String;)V

    .line 296
    new-instance v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$2;

    invoke-direct {v0, p0}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$2;-><init>(Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;)V

    iput-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mSyncThread:Ljava/lang/Thread;

    .line 306
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mSyncThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 307
    return-void
.end method

.method private declared-synchronized stopMySelf(Ljava/lang/String;)V
    .locals 3
    .param p1, "whoWantToStop"    # Ljava/lang/String;

    .prologue
    .line 2466
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v0, :cond_0

    .line 2467
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "stopMySelf():  whoWantToStop="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2469
    :cond_0
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mSetStartServiceCallers:Ljava/util/List;

    if-nez v0, :cond_1

    .line 2470
    invoke-direct {p0}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->stopSyncThread()V

    .line 2471
    invoke-virtual {p0}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->stopSelf()V

    .line 2474
    :cond_1
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mSetStartServiceCallers:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 2475
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "stopMySelf(): remove whoWantToStop="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2477
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mSetStartServiceCallers:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2478
    iget-boolean v0, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v0, :cond_2

    .line 2479
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v1, "stopMySelf() called: mSetStartServiceCallers is Empty; stopping...."

    invoke-static {v0, v1}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2480
    :cond_2
    invoke-direct {p0}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->stopSyncThread()V

    .line 2481
    invoke-virtual {p0}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->stopSelf()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2488
    :cond_3
    :goto_0
    monitor-exit p0

    return-void

    .line 2483
    :cond_4
    :try_start_1
    iget-boolean v0, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v0, :cond_3

    .line 2484
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "stopMySelf() called: mSetStartServiceCallers is NOT Empty(); not stopping....mSetStartServiceCallers="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mSetStartServiceCallers:Ljava/util/List;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2466
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private stopSyncThread()V
    .locals 2

    .prologue
    .line 2492
    :try_start_0
    iget-object v1, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mSyncThread:Ljava/lang/Thread;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mSyncThread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->isAlive()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2493
    iget-object v1, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mSyncThread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    .line 2495
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mSyncThread:Ljava/lang/Thread;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2500
    :cond_0
    :goto_0
    return-void

    .line 2497
    :catch_0
    move-exception v0

    .line 2498
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private syncContacts()V
    .locals 48

    .prologue
    .line 1223
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v6, :cond_0

    .line 1224
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v7, "syncContacts() has been called"

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1225
    :cond_0
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->PERFORMANCE_DEBUG:Z

    if-eqz v6, :cond_1

    .line 1226
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v7, "syncContacts() called"

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1227
    :cond_1
    const/16 v18, 0x0

    .line 1228
    .local v18, "cntOriRaw":I
    const/16 v17, 0x0

    .line 1229
    .local v17, "cntConRaw":I
    const/16 v30, 0x0

    .line 1230
    .local v30, "inserts":I
    const/16 v25, 0x0

    .line 1231
    .local v25, "deletes":I
    const/16 v45, 0x0

    .line 1232
    .local v45, "updates":I
    const/16 v43, 0x0

    .line 1233
    .local v43, "synced":I
    const/16 v32, 0x0

    .line 1234
    .local v32, "joined":I
    const/16 v14, 0x64

    .line 1235
    .local v14, "OPS_SIZE":I
    const/16 v41, 0x0

    .line 1240
    .local v41, "starredMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v7, "checking for new contacts first START"

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1242
    invoke-static/range {p0 .. p0}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncWorker;->getInstance(Landroid/content/Context;)Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncWorker;

    move-result-object v42

    .line 1243
    .local v42, "syncWorker":Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncWorker;
    move-object/from16 v0, v42

    move-object/from16 v1, p0

    move-object/from16 v2, p0

    invoke-virtual {v0, v1, v2}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncWorker;->checkNewContacts(Landroid/content/Context;Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;)V

    .line 1245
    invoke-static {}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncWorker;->getInstance()Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncWorker;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncWorker;->getStarredInfoMap()Ljava/util/HashMap;

    move-result-object v41

    .line 1247
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v7, "checking for new contacts first END"

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1251
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mAccountsSharedPreferences:Landroid/content/SharedPreferences;

    if-nez v6, :cond_2

    .line 1252
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mContext:Landroid/content/Context;

    const-string v7, "created_accounts_uris_per_persona_shared_prefs"

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v6

    move-object/from16 v0, p0

    iput-object v6, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mAccountsSharedPreferences:Landroid/content/SharedPreferences;

    .line 1255
    :cond_2
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mAccountsSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v6}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v44

    .line 1257
    .local v44, "syncedUsers":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;*>;"
    const/16 v21, 0x0

    .line 1260
    .local v21, "customCursorsList":Ljava/util/List;, "Ljava/util/List<Landroid/content/CustomCursor;>;"
    :try_start_0
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mBridgeService:Lcom/sec/knox/bridge/IBridgeService;

    const-string v7, "Contacts"

    const-string v8, "RAW_CONTACTS"

    const/4 v9, 0x0

    sget-object v10, Lcom/samsung/knox/rcp/components/contacts/syncer/util/DataSyncConstants;->ORI_RAWCONTACTS_PROJECTION:[Ljava/lang/String;

    const/4 v11, 0x0

    const/4 v12, 0x0

    const-string v13, "_id ASC"

    invoke-interface/range {v6 .. v13}, Lcom/sec/knox/bridge/IBridgeService;->queryAllProviders(Ljava/lang/String;Ljava/lang/String;I[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v21

    .line 1271
    if-eqz v21, :cond_3

    invoke-interface/range {v21 .. v21}, Ljava/util/List;->size()I

    move-result v6

    if-nez v6, :cond_6

    .line 1272
    :cond_3
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v6, :cond_4

    .line 1273
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Call to mBridgeService.queryAllProviders();  customCursorsList is null or empty; customCursorsList="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v21

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1280
    :cond_4
    invoke-interface/range {v44 .. v44}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .local v28, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Ljava/util/Map$Entry;

    .line 1281
    .local v27, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;*>;"
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "syncContacts() user whose synced data will be deleted = "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface/range {v27 .. v27}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v7, v6}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1284
    :try_start_1
    invoke-interface/range {v27 .. v27}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v46

    .line 1285
    .local v46, "userId":I
    move-object/from16 v0, p0

    move/from16 v1, v46

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->deletePersonaData(I)V
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 1286
    .end local v46    # "userId":I
    :catch_0
    move-exception v34

    .line 1287
    .local v34, "nfe":Ljava/lang/NumberFormatException;
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "syncContacts(): NumberFormatException  = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v34

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1265
    .end local v27    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;*>;"
    .end local v28    # "i$":Ljava/util/Iterator;
    .end local v34    # "nfe":Ljava/lang/NumberFormatException;
    :catch_1
    move-exception v26

    .line 1266
    .local v26, "e":Ljava/lang/Exception;
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Exception during syncContacts() mBridgeService.queryAllProviders();  "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static/range {v26 .. v26}, Lcom/samsung/knox/rcp/components/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1549
    .end local v26    # "e":Ljava/lang/Exception;
    :cond_5
    :goto_1
    return-void

    .line 1293
    :cond_6
    invoke-interface/range {v21 .. v21}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    :cond_7
    :goto_2
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_9

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v36

    check-cast v36, Landroid/content/CustomCursor;

    .line 1295
    .local v36, "orig_raw_cur":Landroid/content/CustomCursor;
    if-nez v36, :cond_8

    .line 1296
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v6, :cond_7

    .line 1297
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v7, "syncContacts: orig_raw_cur is null; skipping..."

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 1300
    :cond_8
    sget-boolean v6, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->isSyncState:Z

    if-nez v6, :cond_a

    .line 1301
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v7, "mSyncThread interrupt "

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1539
    .end local v36    # "orig_raw_cur":Landroid/content/CustomCursor;
    :cond_9
    invoke-interface/range {v44 .. v44}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28    # "i$":Ljava/util/Iterator;
    :goto_3
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Ljava/util/Map$Entry;

    .line 1540
    .restart local v27    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;*>;"
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "syncContacts():2 user whose synced data will be deleted = "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface/range {v27 .. v27}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v7, v6}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1543
    :try_start_2
    invoke-interface/range {v27 .. v27}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v46

    .line 1544
    .restart local v46    # "userId":I
    move-object/from16 v0, p0

    move/from16 v1, v46

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->deletePersonaData(I)V
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_3

    .line 1545
    .end local v46    # "userId":I
    :catch_2
    move-exception v34

    .line 1546
    .restart local v34    # "nfe":Ljava/lang/NumberFormatException;
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "syncContacts():2 NumberFormatException  = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v34

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 1304
    .end local v27    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;*>;"
    .end local v28    # "i$":Ljava/util/Iterator;
    .end local v34    # "nfe":Ljava/lang/NumberFormatException;
    .restart local v36    # "orig_raw_cur":Landroid/content/CustomCursor;
    :cond_a
    const/16 v19, 0x0

    .line 1305
    .local v19, "con_raw_cur":Landroid/database/Cursor;
    const/16 v22, 0x0

    .line 1307
    .local v22, "dbContacts":Landroid/database/Cursor;
    invoke-virtual/range {v36 .. v36}, Landroid/content/CustomCursor;->getCursorOwnerId()I

    move-result v47

    .line 1308
    .local v47, "userOrPersonaId":I
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v6, :cond_b

    .line 1309
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "syncContacts Querying data from bridge raw table for userOrPersonaId="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move/from16 v0, v47

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1311
    :cond_b
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mPm:Landroid/os/PersonaManager;

    move/from16 v0, v47

    invoke-virtual {v6, v0}, Landroid/os/PersonaManager;->exists(I)Z

    move-result v31

    .line 1312
    .local v31, "isPersona":Z
    const/16 v16, 0x0

    .line 1313
    .local v16, "accountType":Ljava/lang/String;
    const/4 v15, 0x0

    .line 1316
    .local v15, "accountName":Ljava/lang/String;
    invoke-static/range {v47 .. v47}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v44

    invoke-interface {v0, v6}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1320
    if-eqz v31, :cond_e

    .line 1321
    :try_start_3
    move-object/from16 v0, p0

    move/from16 v1, v47

    invoke-virtual {v0, v1}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->createOrGetAccountTypeForKnox(I)Ljava/lang/String;

    move-result-object v16

    .line 1322
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v6, :cond_c

    .line 1323
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, " syncContacts() knoxAccountType = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v16

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " for user = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move/from16 v0, v47

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1325
    :cond_c
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "vnd.sec.contact.phone_knox"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v47

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    .line 1332
    :goto_4
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    sget-object v7, Lcom/samsung/knox/rcp/components/contacts/syncer/util/DataSyncConstants;->CON_RAW_CONTACTS_URI:Landroid/net/Uri;

    sget-object v8, Lcom/samsung/knox/rcp/components/contacts/syncer/util/DataSyncConstants;->CON_RAWCONTACTS_PROJECTION:[Ljava/lang/String;

    const-string v9, "account_name=? AND account_type=? AND deleted=?"

    const/4 v10, 0x3

    new-array v10, v10, [Ljava/lang/String;

    const/4 v11, 0x0

    aput-object v15, v10, v11

    const/4 v11, 0x1

    aput-object v16, v10, v11

    const/4 v11, 0x2

    const-string v12, "0"

    aput-object v12, v10, v11

    const-string v11, "_id ASC"

    invoke-virtual/range {v6 .. v11}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v19

    .line 1341
    if-nez v19, :cond_f

    .line 1342
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v6, :cond_d

    .line 1343
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "syncContacts con_raw_cur == null. Skipping ... for userOrPersonaId="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move/from16 v0, v47

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1346
    :cond_d
    move-object/from16 v0, p0

    move-object/from16 v1, v36

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->closeCursor(Landroid/database/Cursor;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1526
    move-object/from16 v0, p0

    move-object/from16 v1, v36

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->closeCursor(Landroid/database/Cursor;)V

    .line 1527
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->closeCursor(Landroid/database/Cursor;)V

    .line 1528
    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->closeCursor(Landroid/database/Cursor;)V

    .line 1530
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->PERFORMANCE_DEBUG:Z

    if-eqz v6, :cond_7

    .line 1531
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v7, "syncContacts() completed"

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 1327
    :cond_e
    :try_start_4
    const-string v16, "vnd.sec.contact.phone_personal"

    .line 1328
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "vnd.sec.contact.phone_personal"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v47

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    goto/16 :goto_4

    .line 1351
    :cond_f
    invoke-virtual/range {v36 .. v36}, Landroid/content/CustomCursor;->getCount()I

    move-result v18

    .line 1352
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->getCount()I

    move-result v17

    .line 1354
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "syncContacts cntOriRaw "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move/from16 v0, v18

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1355
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "syncContacts cntConRaw  "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move/from16 v0, v17

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1358
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->contactsRcpDb:Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->contactsRcpDb:Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;

    const-string v8, "_id_raw_original"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " ASC"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    move/from16 v0, v47

    invoke-virtual {v6, v0, v7}, Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;->getAllContacts(ILjava/lang/String;)Landroid/database/Cursor;

    move-result-object v22

    .line 1361
    if-nez v22, :cond_11

    .line 1362
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v6, :cond_10

    .line 1363
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "syncContacts dbContacts  == null, Skipping ... for userOrPersonaId="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move/from16 v0, v47

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1366
    :cond_10
    move-object/from16 v0, p0

    move-object/from16 v1, v36

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->closeCursor(Landroid/database/Cursor;)V

    .line 1367
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->closeCursor(Landroid/database/Cursor;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1526
    move-object/from16 v0, p0

    move-object/from16 v1, v36

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->closeCursor(Landroid/database/Cursor;)V

    .line 1527
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->closeCursor(Landroid/database/Cursor;)V

    .line 1528
    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->closeCursor(Landroid/database/Cursor;)V

    .line 1530
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->PERFORMANCE_DEBUG:Z

    if-eqz v6, :cond_7

    .line 1531
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v7, "syncContacts() completed"

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 1372
    :cond_11
    :try_start_5
    new-instance v35, Ljava/util/ArrayList;

    invoke-direct/range {v35 .. v35}, Ljava/util/ArrayList;-><init>()V

    .line 1373
    .local v35, "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    new-instance v23, Ljava/util/ArrayList;

    invoke-direct/range {v23 .. v23}, Ljava/util/ArrayList;-><init>()V

    .line 1374
    .local v23, "delIdList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    new-instance v37, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;

    invoke-direct/range {v37 .. v37}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;-><init>()V

    .line 1375
    .local v37, "rawContactInfo":Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;
    new-instance v33, Landroid/database/CursorJoiner;

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "_id"

    aput-object v8, v6, v7

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->contactsRcpDb:Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;

    const-string v9, "_id_raw_original"

    aput-object v9, v7, v8

    move-object/from16 v0, v33

    move-object/from16 v1, v36

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v6, v2, v7}, Landroid/database/CursorJoiner;-><init>(Landroid/database/Cursor;[Ljava/lang/String;Landroid/database/Cursor;[Ljava/lang/String;)V

    .line 1383
    .local v33, "joiner":Landroid/database/CursorJoiner;
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v6, :cond_12

    .line 1384
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v7, "Begin comparing CursorJoiner results from orig_raw_cur(LEFT) and db(RIGHT)"

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1386
    :cond_12
    invoke-virtual/range {v33 .. v33}, Landroid/database/CursorJoiner;->iterator()Ljava/util/Iterator;

    move-result-object v29

    .local v29, "i$":Ljava/util/Iterator;
    :cond_13
    :goto_5
    invoke-interface/range {v29 .. v29}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_16

    invoke-interface/range {v29 .. v29}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v39

    check-cast v39, Landroid/database/CursorJoiner$Result;

    .line 1388
    .local v39, "result":Landroid/database/CursorJoiner$Result;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->isBridgeServiceValid()Z

    move-result v6

    if-nez v6, :cond_15

    .line 1389
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v6, :cond_14

    .line 1390
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v7, "syncContacts(): isBridgeServiceValid() == false; stopping syncing...."

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 1526
    :cond_14
    move-object/from16 v0, p0

    move-object/from16 v1, v36

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->closeCursor(Landroid/database/Cursor;)V

    .line 1527
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->closeCursor(Landroid/database/Cursor;)V

    .line 1528
    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->closeCursor(Landroid/database/Cursor;)V

    .line 1530
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->PERFORMANCE_DEBUG:Z

    if-eqz v6, :cond_5

    .line 1531
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v7, "syncContacts() completed"

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1394
    :cond_15
    :try_start_6
    sget-boolean v6, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->isSyncState:Z

    if-nez v6, :cond_1a

    .line 1395
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v7, "mSyncThread interrupt "

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1508
    .end local v39    # "result":Landroid/database/CursorJoiner$Result;
    :cond_16
    invoke-virtual/range {v23 .. v23}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lez v6, :cond_17

    .line 1510
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->deleteContacts(Ljava/util/ArrayList;)V

    .line 1511
    invoke-virtual/range {v23 .. v23}, Ljava/util/ArrayList;->clear()V

    .line 1513
    :cond_17
    invoke-virtual/range {v35 .. v35}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_18

    .line 1515
    move-object/from16 v0, p0

    move-object/from16 v1, v35

    move/from16 v2, v47

    move-object/from16 v3, v37

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->applyBatchRawContacts(Ljava/util/ArrayList;ILcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;)V

    .line 1517
    :cond_18
    if-lez v30, :cond_19

    .line 1518
    move-object/from16 v0, p0

    move/from16 v1, v30

    move-object/from16 v2, v37

    move/from16 v3, v47

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->insertLocalDB(ILcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;I)V

    .line 1519
    :cond_19
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "inserts("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move/from16 v0, v30

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "), deletes("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move/from16 v0, v25

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "), updates("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move/from16 v0, v45

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "), synced("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move/from16 v0, v43

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "), joined("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move/from16 v0, v32

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 1526
    move-object/from16 v0, p0

    move-object/from16 v1, v36

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->closeCursor(Landroid/database/Cursor;)V

    .line 1527
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->closeCursor(Landroid/database/Cursor;)V

    .line 1528
    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->closeCursor(Landroid/database/Cursor;)V

    .line 1530
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->PERFORMANCE_DEBUG:Z

    if-eqz v6, :cond_7

    .line 1531
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v7, "syncContacts() completed"

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 1399
    .restart local v39    # "result":Landroid/database/CursorJoiner$Result;
    :cond_1a
    add-int/lit8 v32, v32, 0x1

    .line 1401
    :try_start_7
    sget-object v6, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$3;->$SwitchMap$android$database$CursorJoiner$Result:[I

    invoke-virtual/range {v39 .. v39}, Landroid/database/CursorJoiner$Result;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    goto/16 :goto_5

    .line 1404
    :pswitch_0
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v6, :cond_1b

    .line 1405
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "syncContacts LEFT Case about to add, original raw _id: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "_id"

    move-object/from16 v0, v36

    invoke-virtual {v0, v8}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    move-object/from16 v0, v36

    invoke-virtual {v0, v8}, Landroid/content/CustomCursor;->getLong(I)J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1410
    :cond_1b
    move-object/from16 v0, p0

    move-object/from16 v1, v35

    move-object/from16 v2, v36

    move/from16 v3, v47

    move-object/from16 v4, v37

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->insertRawContact(Ljava/util/ArrayList;Landroid/content/CustomCursor;ILcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;)V

    .line 1412
    invoke-virtual/range {v35 .. v35}, Ljava/util/ArrayList;->size()I

    move-result v6

    rem-int/2addr v6, v14

    if-nez v6, :cond_1c

    .line 1413
    move-object/from16 v0, p0

    move-object/from16 v1, v35

    move/from16 v2, v47

    move-object/from16 v3, v37

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->applyBatchRawContacts(Ljava/util/ArrayList;ILcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;)V

    .line 1415
    :cond_1c
    add-int/lit8 v30, v30, 0x1

    .line 1416
    goto/16 :goto_5

    .line 1419
    :pswitch_1
    const/16 v24, 0x12c

    .line 1420
    .local v24, "del_max_size":I
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v6, :cond_1d

    .line 1421
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "syncContacts Right Case about to delete from CP/DB with original _id: ("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "_id_raw_original"

    move-object/from16 v0, v22

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    move-object/from16 v0, v22

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1428
    :cond_1d
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->contactsRcpDb:Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;

    const-string v6, "_id_raw_container"

    move-object/from16 v0, v22

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v38

    .line 1430
    .local v38, "raw_id_container_Index":I
    const/4 v6, -0x1

    move/from16 v0, v38

    if-ne v0, v6, :cond_1e

    .line 1431
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v6, :cond_13

    .line 1432
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "syncContacts() Rorre.. Skipping case RIGHT delete contact... Raw Id Container Index  "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move/from16 v0, v38

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_5

    .line 1522
    .end local v23    # "delIdList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .end local v24    # "del_max_size":I
    .end local v29    # "i$":Ljava/util/Iterator;
    .end local v33    # "joiner":Landroid/database/CursorJoiner;
    .end local v35    # "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .end local v37    # "rawContactInfo":Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;
    .end local v38    # "raw_id_container_Index":I
    .end local v39    # "result":Landroid/database/CursorJoiner$Result;
    :catch_3
    move-exception v26

    .line 1523
    .restart local v26    # "e":Ljava/lang/Exception;
    :try_start_8
    invoke-virtual/range {v26 .. v26}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 1526
    move-object/from16 v0, p0

    move-object/from16 v1, v36

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->closeCursor(Landroid/database/Cursor;)V

    .line 1527
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->closeCursor(Landroid/database/Cursor;)V

    .line 1528
    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->closeCursor(Landroid/database/Cursor;)V

    .line 1530
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->PERFORMANCE_DEBUG:Z

    if-eqz v6, :cond_7

    .line 1531
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v7, "syncContacts() completed"

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 1441
    .end local v26    # "e":Ljava/lang/Exception;
    .restart local v23    # "delIdList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v24    # "del_max_size":I
    .restart local v29    # "i$":Ljava/util/Iterator;
    .restart local v33    # "joiner":Landroid/database/CursorJoiner;
    .restart local v35    # "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .restart local v37    # "rawContactInfo":Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;
    .restart local v38    # "raw_id_container_Index":I
    .restart local v39    # "result":Landroid/database/CursorJoiner$Result;
    :cond_1e
    :try_start_9
    move-object/from16 v0, v22

    move/from16 v1, v38

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1443
    invoke-virtual/range {v23 .. v23}, Ljava/util/ArrayList;->size()I

    move-result v6

    rem-int/lit16 v6, v6, 0x12c

    if-nez v6, :cond_20

    .line 1444
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->deleteContacts(Ljava/util/ArrayList;)V

    .line 1445
    invoke-virtual/range {v23 .. v23}, Ljava/util/ArrayList;->size()I

    move-result v6

    const/16 v7, 0x12c

    if-eq v6, v7, :cond_1f

    .line 1446
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v7, "delIdList size is not same"

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1448
    :cond_1f
    invoke-virtual/range {v23 .. v23}, Ljava/util/ArrayList;->clear()V

    .line 1450
    :cond_20
    add-int/lit8 v25, v25, 0x1

    .line 1451
    goto/16 :goto_5

    .line 1465
    .end local v24    # "del_max_size":I
    .end local v38    # "raw_id_container_Index":I
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->contactsRcpDb:Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;

    const-string v6, "_id_raw_container"

    move-object/from16 v0, v22

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, v22

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v20

    .line 1468
    .local v20, "contactId":I
    const/16 v40, 0x0

    .line 1470
    .local v40, "starred":I
    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, v41

    invoke-virtual {v0, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    if-eqz v6, :cond_21

    .line 1471
    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, v41

    invoke-virtual {v0, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v40

    .line 1475
    :cond_21
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->contactsRcpDb:Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;

    const-string v6, "raw_contacts_version"

    move-object/from16 v0, v22

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, v22

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    const-string v7, "version"

    move-object/from16 v0, v36

    invoke-virtual {v0, v7}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    move-object/from16 v0, v36

    invoke-virtual {v0, v7}, Landroid/content/CustomCursor;->getInt(I)I

    move-result v7

    if-ne v6, v7, :cond_23

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->contactsRcpDb:Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;

    const-string v6, "_id_raw_contact_id_original"

    move-object/from16 v0, v22

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, v22

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    const-string v7, "contact_id"

    move-object/from16 v0, v36

    invoke-virtual {v0, v7}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    move-object/from16 v0, v36

    invoke-virtual {v0, v7}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_23

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->contactsRcpDb:Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;

    const-string v6, "raw_name_verified"

    move-object/from16 v0, v22

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, v22

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    const-string v7, "name_verified"

    move-object/from16 v0, v36

    invoke-virtual {v0, v7}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    move-object/from16 v0, v36

    invoke-virtual {v0, v7}, Landroid/content/CustomCursor;->getInt(I)I

    move-result v7

    if-ne v6, v7, :cond_23

    const-string v6, "starred"

    move-object/from16 v0, v36

    invoke-virtual {v0, v6}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, v36

    invoke-virtual {v0, v6}, Landroid/content/CustomCursor;->getInt(I)I

    move-result v6

    move/from16 v0, v40

    if-ne v0, v6, :cond_23

    .line 1496
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v6, :cond_22

    .line 1497
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v7, "syncContacts  Nochange case "

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1498
    :cond_22
    add-int/lit8 v43, v43, 0x1

    goto/16 :goto_5

    .line 1500
    :cond_23
    move-object/from16 v0, p0

    move-object/from16 v1, v36

    move/from16 v2, v47

    move/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->updateContact(Landroid/content/CustomCursor;II)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_3
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 1501
    add-int/lit8 v45, v45, 0x1

    goto/16 :goto_5

    .line 1526
    .end local v20    # "contactId":I
    .end local v23    # "delIdList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .end local v29    # "i$":Ljava/util/Iterator;
    .end local v33    # "joiner":Landroid/database/CursorJoiner;
    .end local v35    # "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .end local v37    # "rawContactInfo":Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;
    .end local v39    # "result":Landroid/database/CursorJoiner$Result;
    .end local v40    # "starred":I
    :catchall_0
    move-exception v6

    move-object/from16 v0, p0

    move-object/from16 v1, v36

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->closeCursor(Landroid/database/Cursor;)V

    .line 1527
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->closeCursor(Landroid/database/Cursor;)V

    .line 1528
    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->closeCursor(Landroid/database/Cursor;)V

    .line 1530
    move-object/from16 v0, p0

    iget-boolean v7, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->PERFORMANCE_DEBUG:Z

    if-eqz v7, :cond_24

    .line 1531
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v8, "syncContacts() completed"

    invoke-static {v7, v8}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_24
    throw v6

    .line 1401
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private syncGroup()V
    .locals 30

    .prologue
    .line 933
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v2, :cond_0

    .line 934
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v3, "syncGroup() called"

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 935
    :cond_0
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->PERFORMANCE_DEBUG:Z

    if-eqz v2, :cond_1

    .line 936
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v3, "syncGroup called"

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 937
    :cond_1
    new-instance v27, Landroid/content/ContentValues;

    invoke-direct/range {v27 .. v27}, Landroid/content/ContentValues;-><init>()V

    .line 941
    .local v27, "values":Landroid/content/ContentValues;
    const/4 v11, 0x0

    .line 943
    .local v11, "customCursorsList":Ljava/util/List;, "Ljava/util/List<Landroid/content/CustomCursor;>;"
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mBridgeService:Lcom/sec/knox/bridge/IBridgeService;

    const-string v3, "Contacts"

    const-string v4, "GROUPS_CONTACTS"

    const/4 v5, 0x0

    sget-object v6, Lcom/samsung/knox/rcp/components/contacts/syncer/util/DataSyncConstants;->ORI_GROUPS_PROJECTION:[Ljava/lang/String;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const-string v9, "_id ASC"

    invoke-interface/range {v2 .. v9}, Lcom/sec/knox/bridge/IBridgeService;->queryAllProviders(Ljava/lang/String;Ljava/lang/String;I[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v11

    .line 954
    if-eqz v11, :cond_2

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_4

    .line 955
    :cond_2
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v2, :cond_3

    .line 956
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DataSyncService.syncGroup(): customCursorsList is null or empty: customCursorsList="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1220
    :cond_3
    :goto_0
    return-void

    .line 947
    :catch_0
    move-exception v13

    .line 948
    .local v13, "e":Ljava/lang/Exception;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DataSyncService.syncGroup() Exception during mBridgeService.queryAllProviders() = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v13}, Lcom/samsung/knox/rcp/components/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 963
    .end local v13    # "e":Ljava/lang/Exception;
    :cond_4
    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :cond_5
    :goto_1
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/content/CustomCursor;

    .line 965
    .local v26, "orig_group_cur":Landroid/content/CustomCursor;
    if-nez v26, :cond_6

    .line 966
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v2, :cond_5

    .line 967
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v3, "syncGroup(): orig_group_cur is null; skipping..."

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 971
    :cond_6
    sget-boolean v2, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->isSyncState:Z

    if-nez v2, :cond_7

    .line 972
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v3, "mSyncThread interrupt "

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 975
    :cond_7
    const/4 v10, 0x0

    .line 976
    .local v10, "con_group_cur":Landroid/database/Cursor;
    const/4 v12, 0x0

    .line 979
    .local v12, "datasync_group_cur":Landroid/database/Cursor;
    :try_start_1
    invoke-virtual/range {v26 .. v26}, Landroid/content/CustomCursor;->getCursorOwnerId()I

    move-result v8

    .line 980
    .local v8, "userOrPersonaId":I
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v2, :cond_8

    .line 981
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DataSyncService syncGroup() started for userOrPersonaId="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 984
    :cond_8
    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->addNewAccountIfRequired(I)Z

    .line 985
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mPm:Landroid/os/PersonaManager;

    invoke-virtual {v2, v8}, Landroid/os/PersonaManager;->exists(I)Z

    move-result v20

    .line 987
    .local v20, "isPersona":Z
    if-nez v26, :cond_a

    .line 988
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v2, :cond_9

    .line 989
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "syncGroup: no groups to sync for userOrPersonaId="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 991
    :cond_9
    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->closeCursor(Landroid/database/Cursor;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1213
    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->closeCursor(Landroid/database/Cursor;)V

    .line 1214
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->closeCursor(Landroid/database/Cursor;)V

    .line 1215
    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->closeCursor(Landroid/database/Cursor;)V

    .line 1216
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->PERFORMANCE_DEBUG:Z

    if-eqz v2, :cond_5

    .line 1217
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v3, "syncGroup completed"

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 996
    :cond_a
    :try_start_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->contactsRcpDb:Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;

    invoke-virtual {v2, v8}, Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;->getAllGroups(I)Landroid/database/Cursor;

    move-result-object v12

    .line 997
    if-nez v12, :cond_c

    .line 998
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v2, :cond_b

    .line 999
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Rorre... DataSyncService syncGroup() failed to get data from the local db... Skipping  for userOrPersonaId="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1002
    :cond_b
    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->closeCursor(Landroid/database/Cursor;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1213
    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->closeCursor(Landroid/database/Cursor;)V

    .line 1214
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->closeCursor(Landroid/database/Cursor;)V

    .line 1215
    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->closeCursor(Landroid/database/Cursor;)V

    .line 1216
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->PERFORMANCE_DEBUG:Z

    if-eqz v2, :cond_5

    .line 1217
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v3, "syncGroup completed"

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1004
    :cond_c
    :try_start_3
    invoke-interface {v12}, Landroid/database/Cursor;->getCount()I

    move-result v2

    const/4 v3, 0x1

    if-ge v2, v3, :cond_d

    .line 1005
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v2, :cond_d

    .line 1006
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v3, "syncGroup: reinitializing local DB because of size 0"

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1011
    :cond_d
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v2, :cond_e

    .line 1012
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DataSyncService syncGroup datasync_group_cur cursor Count is : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v12}, Landroid/database/Cursor;->getCount()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1015
    :cond_e
    new-instance v21, Landroid/database/CursorJoiner;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "version"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->contactsRcpDb:Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;

    const-string v9, "_id_original"

    aput-object v9, v3, v4

    const/4 v4, 0x1

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->contactsRcpDb:Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;

    const-string v9, "groups_version"

    aput-object v9, v3, v4

    move-object/from16 v0, v21

    move-object/from16 v1, v26

    invoke-direct {v0, v1, v2, v12, v3}, Landroid/database/CursorJoiner;-><init>(Landroid/database/Cursor;[Ljava/lang/String;Landroid/database/Cursor;[Ljava/lang/String;)V

    .line 1025
    .local v21, "joiner":Landroid/database/CursorJoiner;
    invoke-virtual/range {v26 .. v26}, Landroid/content/CustomCursor;->moveToFirst()Z

    .line 1027
    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1029
    invoke-virtual/range {v21 .. v21}, Landroid/database/CursorJoiner;->iterator()Ljava/util/Iterator;

    move-result-object v16

    .local v16, "i$":Ljava/util/Iterator;
    :cond_f
    :goto_2
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_10

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Landroid/database/CursorJoiner$Result;

    .line 1031
    .local v22, "joinerResult":Landroid/database/CursorJoiner$Result;
    sget-boolean v2, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->isSyncState:Z

    if-nez v2, :cond_11

    .line 1032
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v3, "mSyncThread interrupt "

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1213
    .end local v22    # "joinerResult":Landroid/database/CursorJoiner$Result;
    :cond_10
    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->closeCursor(Landroid/database/Cursor;)V

    .line 1214
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->closeCursor(Landroid/database/Cursor;)V

    .line 1215
    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->closeCursor(Landroid/database/Cursor;)V

    .line 1216
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->PERFORMANCE_DEBUG:Z

    if-eqz v2, :cond_5

    .line 1217
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v3, "syncGroup completed"

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1035
    .restart local v22    # "joinerResult":Landroid/database/CursorJoiner$Result;
    :cond_11
    :try_start_4
    sget-object v2, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$3;->$SwitchMap$android$database$CursorJoiner$Result:[I

    invoke-virtual/range {v22 .. v22}, Landroid/database/CursorJoiner$Result;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 1201
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v2, :cond_f

    .line 1202
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v3, "Something is wrong"

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2

    .line 1206
    .end local v8    # "userOrPersonaId":I
    .end local v16    # "i$":Ljava/util/Iterator;
    .end local v20    # "isPersona":Z
    .end local v21    # "joiner":Landroid/database/CursorJoiner;
    .end local v22    # "joinerResult":Landroid/database/CursorJoiner$Result;
    :catch_1
    move-exception v13

    .line 1207
    .restart local v13    # "e":Ljava/lang/Exception;
    :try_start_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception during syncGroup() for the current userOrPersonaId: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v13}, Lcom/samsung/knox/rcp/components/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 1213
    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->closeCursor(Landroid/database/Cursor;)V

    .line 1214
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->closeCursor(Landroid/database/Cursor;)V

    .line 1215
    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->closeCursor(Landroid/database/Cursor;)V

    .line 1216
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->PERFORMANCE_DEBUG:Z

    if-eqz v2, :cond_5

    .line 1217
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v3, "syncGroup completed"

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1038
    .end local v13    # "e":Ljava/lang/Exception;
    .restart local v8    # "userOrPersonaId":I
    .restart local v16    # "i$":Ljava/util/Iterator;
    .restart local v20    # "isPersona":Z
    .restart local v21    # "joiner":Landroid/database/CursorJoiner;
    .restart local v22    # "joinerResult":Landroid/database/CursorJoiner$Result;
    :pswitch_0
    :try_start_6
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v2, :cond_12

    .line 1039
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v3, "New Group to ADD"

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1040
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v3, "This is LEFT CASE "

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1041
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Original Id of group added "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_id"

    move-object/from16 v0, v26

    invoke-virtual {v0, v4}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v26

    invoke-virtual {v0, v4}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1045
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Original group title "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "title"

    move-object/from16 v0, v26

    invoke-virtual {v0, v4}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v26

    invoke-virtual {v0, v4}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1050
    :cond_12
    invoke-virtual/range {v27 .. v27}, Landroid/content/ContentValues;->clear()V

    .line 1051
    const-string v2, "title"

    const-string v3, "title"

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v27

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1054
    const-string v2, "system_id"

    move-object/from16 v0, v26

    invoke-virtual {v0, v2}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v26

    invoke-virtual {v0, v2}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_17

    .line 1056
    const-string v2, "system_id"

    const-string v3, "PersonalGroup"

    move-object/from16 v0, v27

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1063
    :goto_3
    const-string v2, "group_is_read_only"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v27

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1064
    const-string v2, "favorites"

    const-string v3, "favorites"

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v27

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1067
    const-string v2, "group_visible"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v27

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1068
    const-string v2, "auto_add"

    const-string v3, "auto_add"

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Landroid/content/CustomCursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v27

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1071
    if-eqz v20, :cond_19

    .line 1072
    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->createOrGetAccountTypeForKnox(I)Ljava/lang/String;

    move-result-object v23

    .line 1073
    .local v23, "knoxAccountType":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v2, :cond_13

    .line 1074
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " addNewAccount() knoxAccountType = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v23

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " for user = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1076
    :cond_13
    const-string v2, "account_name"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "vnd.sec.contact.phone_knox"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v27

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1078
    const-string v2, "account_type"

    move-object/from16 v0, v27

    move-object/from16 v1, v23

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1085
    .end local v23    # "knoxAccountType":Ljava/lang/String;
    :goto_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/knox/rcp/components/contacts/syncer/util/DataSyncConstants;->CON_GROUP_URI:Landroid/net/Uri;

    move-object/from16 v0, v27

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v24

    .line 1089
    .local v24, "newRow":Landroid/net/Uri;
    invoke-static/range {v24 .. v24}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v6

    .line 1091
    .local v6, "id_container":J
    const-string v2, "_id"

    move-object/from16 v0, v26

    invoke-virtual {v0, v2}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v26

    invoke-virtual {v0, v2}, Landroid/content/CustomCursor;->getInt(I)I

    move-result v17

    .line 1093
    .local v17, "id_original":I
    const-string v2, "version"

    move-object/from16 v0, v26

    invoke-virtual {v0, v2}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v26

    invoke-virtual {v0, v2}, Landroid/content/CustomCursor;->getInt(I)I

    move-result v5

    .line 1095
    .local v5, "version":I
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v2, :cond_14

    .line 1096
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DataSyncService syncGroup inserting to local db new group "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1101
    :cond_14
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->contactsRcpDb:Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;

    move/from16 v0, v17

    int-to-long v3, v0

    invoke-virtual/range {v2 .. v8}, Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;->insertGroup(JIJI)J

    move-result-wide v18

    .line 1103
    .local v18, "id_db":J
    const-wide/16 v2, -0x1

    cmp-long v2, v18, v2

    if-nez v2, :cond_f

    .line 1104
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v2, :cond_15

    .line 1105
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v3, "Error inserting group to local DB"

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1106
    :cond_15
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->contactsRcpDb:Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;

    move/from16 v0, v17

    int-to-long v3, v0

    invoke-virtual/range {v2 .. v8}, Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;->insertGroup(JIJI)J

    move-result-wide v18

    .line 1108
    const-wide/16 v2, -0x1

    cmp-long v2, v18, v2

    if-nez v2, :cond_f

    .line 1111
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, v24

    invoke-virtual {v2, v0, v3, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v25

    .line 1113
    .local v25, "noDeleted":I
    if-gtz v25, :cond_f

    .line 1114
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v2, :cond_16

    .line 1115
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v3, "Failed to delete row "

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1116
    :cond_16
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, v24

    invoke-virtual {v2, v0, v3, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v25

    .line 1118
    if-gtz v25, :cond_f

    goto/16 :goto_2

    .line 1058
    .end local v5    # "version":I
    .end local v6    # "id_container":J
    .end local v17    # "id_original":I
    .end local v18    # "id_db":J
    .end local v24    # "newRow":Landroid/net/Uri;
    .end local v25    # "noDeleted":I
    :cond_17
    const-string v2, "system_id"

    const-string v3, "system_id"

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v27

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_3

    .line 1213
    .end local v8    # "userOrPersonaId":I
    .end local v16    # "i$":Ljava/util/Iterator;
    .end local v20    # "isPersona":Z
    .end local v21    # "joiner":Landroid/database/CursorJoiner;
    .end local v22    # "joinerResult":Landroid/database/CursorJoiner$Result;
    :catchall_0
    move-exception v2

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->closeCursor(Landroid/database/Cursor;)V

    .line 1214
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->closeCursor(Landroid/database/Cursor;)V

    .line 1215
    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->closeCursor(Landroid/database/Cursor;)V

    .line 1216
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->PERFORMANCE_DEBUG:Z

    if-eqz v3, :cond_18

    .line 1217
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v4, "syncGroup completed"

    invoke-static {v3, v4}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_18
    throw v2

    .line 1080
    .restart local v8    # "userOrPersonaId":I
    .restart local v16    # "i$":Ljava/util/Iterator;
    .restart local v20    # "isPersona":Z
    .restart local v21    # "joiner":Landroid/database/CursorJoiner;
    .restart local v22    # "joinerResult":Landroid/database/CursorJoiner$Result;
    :cond_19
    :try_start_7
    const-string v2, "account_name"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "vnd.sec.contact.phone_personal"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v27

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1082
    const-string v2, "account_type"

    const-string v3, "vnd.sec.contact.phone_personal"

    move-object/from16 v0, v27

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 1130
    :pswitch_1
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v2, :cond_1a

    .line 1131
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v3, "Need to delete old Group"

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1132
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Delete DB  id ORI + id CON :: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->contactsRcpDb:Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;

    const-string v4, "_id_original"

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->contactsRcpDb:Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;

    const-string v4, "_id_container"

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1142
    :cond_1a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/knox/rcp/components/contacts/syncer/util/DataSyncConstants;->CON_GROUP_URI:Landroid/net/Uri;

    const-string v4, "_id=?"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/16 v28, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->contactsRcpDb:Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;

    move-object/from16 v29, v0

    const-string v29, "_id_container"

    move-object/from16 v0, v29

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v29

    move/from16 v0, v29

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v29

    invoke-static/range {v29 .. v29}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v29

    aput-object v29, v9, v28

    invoke-virtual {v2, v3, v4, v9}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v25

    .line 1151
    .restart local v25    # "noDeleted":I
    const/4 v14, 0x0

    .line 1152
    .local v14, "groupId":Ljava/lang/String;
    const/4 v2, 0x1

    move/from16 v0, v25

    if-lt v0, v2, :cond_1f

    .line 1153
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v2, :cond_1b

    .line 1154
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v3, "Group delete successful from Contacts cp "

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1155
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DataSyncService syncGroup deleting to local db  group with id "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->contactsRcpDb:Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;

    const-string v4, "_id_container"

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1161
    :cond_1b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->contactsRcpDb:Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;

    const-string v2, "_id"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 1167
    if-eqz v14, :cond_1c

    .line 1168
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->contactsRcpDb:Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;->deleteGroup(Ljava/lang/String;)I

    move-result v25

    .line 1172
    :cond_1c
    if-gtz v25, :cond_1e

    .line 1173
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v2, :cond_1d

    .line 1174
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v3, "Deleting group failed for local db "

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1178
    :cond_1d
    if-eqz v14, :cond_f

    .line 1179
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->contactsRcpDb:Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;->deleteGroup(Ljava/lang/String;)I

    move-result v25

    goto/16 :goto_2

    .line 1183
    :cond_1e
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v2, :cond_f

    .line 1184
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v3, "Deleted group successfully from local db "

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 1186
    :cond_1f
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v2, :cond_f

    .line 1187
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v3, "Failed to delete group from Contact Provider"

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 1195
    .end local v14    # "groupId":Ljava/lang/String;
    .end local v25    # "noDeleted":I
    :pswitch_2
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v2, :cond_f

    .line 1196
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v3, "Both Group are same"

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_2

    .line 1035
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public applyBatchRawContacts(Ljava/util/ArrayList;ILcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;)V
    .locals 19
    .param p2, "userOrPersonaId"    # I
    .param p3, "rawContactInfo"    # Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;I",
            "Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;",
            ")V"
        }
    .end annotation

    .prologue
    .line 714
    .local p1, "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    const/4 v12, 0x0

    .line 718
    .local v12, "c":Landroid/content/CustomCursor;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "com.android.contacts"

    move-object/from16 v0, p1

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    move-result-object v13

    .line 719
    .local v13, "cpr":[Landroid/content/ContentProviderResult;
    const/16 v17, 0x0

    .local v17, "k":I
    :goto_0
    array-length v4, v13

    move/from16 v0, v17

    if-ge v0, v4, :cond_0

    .line 720
    aget-object v4, v13, v17

    iget-object v4, v4, Landroid/content/ContentProviderResult;->uri:Landroid/net/Uri;

    invoke-static {v4}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v14

    .line 721
    .local v14, "c_id":J
    move-object/from16 v0, p3

    invoke-virtual {v0, v14, v15}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;->insert_con_id(J)V

    .line 722
    invoke-virtual/range {p3 .. p3}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;->get_id()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual/range {p3 .. p3}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;->getIndex()I

    move-result v5

    add-int v5, v5, v17

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    move-object/from16 v0, p3

    invoke-virtual {v0, v4, v5, v14, v15}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;->insertHashMap(JJ)V

    .line 719
    add-int/lit8 v17, v17, 0x1

    goto :goto_0

    .line 726
    .end local v14    # "c_id":J
    :cond_0
    invoke-virtual/range {p3 .. p3}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;->get_id()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual/range {p3 .. p3}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;->get_con_id()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ne v4, v5, :cond_1

    invoke-virtual/range {p3 .. p3}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;->get_con_id()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual/range {p3 .. p3}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;->getRawContactMap()Ljava/util/HashMap;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/HashMap;->size()I

    move-result v5

    if-eq v4, v5, :cond_2

    .line 728
    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "rawContactInfo size error id size: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {p3 .. p3}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;->get_id()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " con_id size: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {p3 .. p3}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;->get_con_id()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " id_Map size: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {p3 .. p3}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;->getRawContactMap()Ljava/util/HashMap;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/HashMap;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/knox/rcp/components/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 733
    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mBridgeService:Lcom/sec/knox/bridge/IBridgeService;

    const-string v5, "Contacts"

    const-string v6, "DATA_CONTACTS"

    sget-object v8, Lcom/samsung/knox/rcp/components/contacts/syncer/util/DataSyncConstants;->ORI_DATA_PROJECTION:[Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "raw_contact_id IN "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual/range {p3 .. p3}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;->get_id()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual/range {p3 .. p3}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;->getIndex()I

    move-result v10

    invoke-virtual/range {p3 .. p3}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;->getIndex()I

    move-result v11

    array-length v0, v13

    move/from16 v18, v0

    add-int v11, v11, v18

    invoke-virtual {v9, v10, v11}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v9

    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->argsArrayToString(Ljava/util/List;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    const-string v11, "raw_contact_id ASC"

    move/from16 v7, p2

    invoke-interface/range {v4 .. v11}, Lcom/sec/knox/bridge/IBridgeService;->queryProvider(Ljava/lang/String;Ljava/lang/String;I[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/content/CustomCursor;

    move-result-object v12

    .line 745
    move-object/from16 v0, p0

    move/from16 v1, p2

    move-object/from16 v2, p3

    invoke-direct {v0, v12, v1, v2}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->insertDatas(Landroid/content/CustomCursor;ILcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 749
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->clear()V

    .line 750
    invoke-virtual/range {p3 .. p3}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;->get_id()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;->setIndex(I)V

    .line 752
    .end local v13    # "cpr":[Landroid/content/ContentProviderResult;
    .end local v17    # "k":I
    :goto_1
    return-void

    .line 746
    :catch_0
    move-exception v16

    .line 747
    .local v16, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual/range {v16 .. v16}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 749
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->clear()V

    .line 750
    invoke-virtual/range {p3 .. p3}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;->get_id()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;->setIndex(I)V

    goto :goto_1

    .line 749
    .end local v16    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->clear()V

    .line 750
    invoke-virtual/range {p3 .. p3}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;->get_id()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    move-object/from16 v0, p3

    invoke-virtual {v0, v5}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;->setIndex(I)V

    throw v4
.end method

.method public contactsJoin(JJ)V
    .locals 25
    .param p1, "contactId1"    # J
    .param p3, "contactId2"    # J

    .prologue
    .line 1977
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v3, :cond_0

    .line 1978
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "contactsJoin(contactId1:"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, p1

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ", contactId2:"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, p3

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " called"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1980
    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 1981
    .local v2, "resolver":Landroid/content/ContentResolver;
    const/16 v16, 0x0

    .line 1985
    .local v16, "joinedContact_cur":Landroid/database/Cursor;
    sget-object v3, Lcom/samsung/knox/rcp/components/contacts/syncer/util/DataSyncConstants;->CON_RAW_CONTACTS_URI:Landroid/net/Uri;

    sget-object v4, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$JoinContactQuery;->PROJECTION:[Ljava/lang/String;

    const-string v5, "contact_id=? OR contact_id=?"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    invoke-static/range {p1 .. p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    invoke-static/range {p3 .. p4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 1991
    .local v12, "c":Landroid/database/Cursor;
    if-nez v12, :cond_2

    .line 1992
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v3, :cond_1

    .line 1993
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v4, "Null cursor, failed to load the number of joined contacts"

    invoke-static {v3, v4}, Lcom/samsung/knox/rcp/components/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2064
    :cond_1
    :goto_0
    return-void

    .line 1996
    :cond_2
    invoke-interface {v12}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-nez v3, :cond_4

    .line 1997
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v3, :cond_3

    .line 1998
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v4, "contactsJoin cursor count is 0"

    invoke-static {v3, v4}, Lcom/samsung/knox/rcp/components/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1999
    :cond_3
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 2005
    :cond_4
    invoke-interface {v12}, Landroid/database/Cursor;->getCount()I

    move-result v3

    new-array v0, v3, [J

    move-object/from16 v20, v0

    .line 2006
    .local v20, "rawContactIds":[J
    const/4 v14, 0x0

    .local v14, "i":I
    :goto_1
    move-object/from16 v0, v20

    array-length v3, v0

    if-ge v14, v3, :cond_5

    .line 2007
    invoke-interface {v12, v14}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 2008
    const/4 v3, 0x0

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v18

    .line 2009
    .local v18, "rawContactId":J
    aput-wide v18, v20, v14

    .line 2006
    add-int/lit8 v14, v14, 0x1

    goto :goto_1

    .line 2011
    .end local v18    # "rawContactId":J
    :cond_5
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 2014
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 2015
    .local v5, "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    const/4 v14, 0x0

    :goto_2
    move-object/from16 v0, v20

    array-length v3, v0

    if-ge v14, v3, :cond_8

    .line 2016
    const/4 v15, 0x0

    .local v15, "j":I
    :goto_3
    move-object/from16 v0, v20

    array-length v3, v0

    if-ge v15, v3, :cond_7

    .line 2017
    if-eq v14, v15, :cond_6

    .line 2018
    aget-wide v6, v20, v14

    aget-wide v8, v20, v15

    move-object/from16 v4, p0

    invoke-direct/range {v4 .. v9}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->buildJoinContactDiff(Ljava/util/ArrayList;JJ)V

    .line 2016
    :cond_6
    add-int/lit8 v15, v15, 0x1

    goto :goto_3

    .line 2015
    :cond_7
    add-int/lit8 v14, v14, 0x1

    goto :goto_2

    .line 2025
    .end local v15    # "j":I
    :cond_8
    :try_start_0
    const-string v3, "com.android.contacts"

    invoke-virtual {v2, v3, v5}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    .line 2026
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    sget-object v7, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x1

    new-array v8, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "contact_id"

    aput-object v4, v8, v3

    const-string v9, "_id=?"

    const/4 v3, 0x1

    new-array v10, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    aget-wide v22, v20, v4

    invoke-static/range {v22 .. v23}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v10, v3

    const-string v11, "_id ASC"

    invoke-virtual/range {v6 .. v11}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v16

    .line 2033
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->moveToFirst()Z

    .line 2034
    new-instance v21, Landroid/content/ContentValues;

    invoke-direct/range {v21 .. v21}, Landroid/content/ContentValues;-><init>()V

    .line 2035
    .local v21, "values":Landroid/content/ContentValues;
    const-string v3, "_id_raw_contact_id_container"

    const-string v4, "contact_id"

    move-object/from16 v0, v16

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v16

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2041
    const/16 v17, 0x0

    .local v17, "k":I
    :goto_4
    move-object/from16 v0, v20

    array-length v3, v0

    move/from16 v0, v17

    if-ge v0, v3, :cond_9

    .line 2042
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->contactsRcpDb:Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;

    const-string v4, "_id_raw_container=?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aget-wide v8, v20, v17

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    move-object/from16 v0, v21

    invoke-virtual {v3, v0, v4, v6}, Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;->updateContact(Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2041
    add-int/lit8 v17, v17, 0x1

    goto :goto_4

    .line 2056
    :cond_9
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v3, :cond_a

    .line 2057
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v4, "join complete"

    invoke-static {v3, v4}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2061
    :cond_a
    if-eqz v16, :cond_1

    .line 2062
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 2058
    .end local v17    # "k":I
    .end local v21    # "values":Landroid/content/ContentValues;
    :catch_0
    move-exception v13

    .line 2059
    .local v13, "e":Ljava/lang/Exception;
    :try_start_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed to apply aggregation exception batch "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/knox/rcp/components/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2061
    if-eqz v16, :cond_1

    .line 2062
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 2061
    .end local v13    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    if-eqz v16, :cond_b

    .line 2062
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    :cond_b
    throw v3
.end method

.method public createOrGetAccountTypeForKnox(I)Ljava/lang/String;
    .locals 7
    .param p1, "userPersonaIdInt"    # I

    .prologue
    const/4 v3, 0x0

    .line 2172
    const/4 v1, 0x0

    .line 2173
    .local v1, "isPersona":Z
    iget-object v4, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mPm:Landroid/os/PersonaManager;

    if-eqz v4, :cond_2

    .line 2174
    iget-object v4, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mPm:Landroid/os/PersonaManager;

    invoke-virtual {v4, p1}, Landroid/os/PersonaManager;->exists(I)Z

    move-result v1

    .line 2175
    if-nez v1, :cond_4

    .line 2176
    iget-boolean v4, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v4, :cond_0

    .line 2177
    iget-object v4, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " createOrGetAccountTypeForKnox isPersona = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " returning ..."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    move-object v2, v3

    .line 2214
    :cond_1
    :goto_0
    return-object v2

    .line 2182
    :cond_2
    iget-boolean v4, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v4, :cond_3

    .line 2183
    iget-object v4, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v5, " createOrGetAccountTypeForKnox mPm is null returning ..."

    invoke-static {v4, v5}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    move-object v2, v3

    .line 2184
    goto :goto_0

    .line 2187
    :cond_4
    iget-object v4, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mAccountTypeSP:Landroid/content/SharedPreferences;

    if-nez v4, :cond_5

    .line 2188
    iget-object v4, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mContext:Landroid/content/Context;

    const-string v5, "created_accounts_type_per_persona_shared_prefs"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mAccountTypeSP:Landroid/content/SharedPreferences;

    .line 2191
    :cond_5
    iget-object v4, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mAccountTypeSP:Landroid/content/SharedPreferences;

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mAccountTypeSPEditor:Landroid/content/SharedPreferences$Editor;

    .line 2192
    iget-object v4, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mAccountTypeSP:Landroid/content/SharedPreferences;

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2193
    .local v2, "knoxAccountType":Ljava/lang/String;
    if-eqz v2, :cond_6

    .line 2194
    iget-boolean v3, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v3, :cond_1

    .line 2195
    iget-object v3, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "DataSyncService.createOrGetAccountTypeForKnox(): account type found for the userOrPersonaId: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "; uri = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2201
    :cond_6
    invoke-direct {p0, p1}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->getAvailableAccountType(I)Ljava/lang/String;

    move-result-object v0

    .line 2202
    .local v0, "accTypeStr":Ljava/lang/String;
    iget-boolean v4, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v4, :cond_7

    .line 2203
    iget-object v4, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " accTypeStr returned  = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " for persona = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2205
    :cond_7
    if-eqz v0, :cond_8

    .line 2207
    iget-object v3, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mAccountTypeSPEditor:Landroid/content/SharedPreferences$Editor;

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 2208
    iget-object v3, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mAccountTypeSPEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-object v2, v0

    .line 2209
    goto/16 :goto_0

    .line 2212
    :cond_8
    iget-boolean v4, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v4, :cond_9

    .line 2213
    iget-object v4, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v5, " createOrGetAccountTypeForKnox accTypeStr is null "

    invoke-static {v4, v5}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_9
    move-object v2, v3

    .line 2214
    goto/16 :goto_0
.end method

.method deleteAccountForPersona(I)Z
    .locals 11
    .param p1, "personaID"    # I

    .prologue
    const/4 v10, 0x1

    const/4 v4, 0x0

    .line 2370
    const/4 v2, 0x0

    .line 2371
    .local v2, "isPersona":Z
    const/4 v0, 0x0

    .line 2372
    .local v0, "accountName":Ljava/lang/String;
    iget-object v5, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mPm:Landroid/os/PersonaManager;

    if-eqz v5, :cond_5

    .line 2373
    invoke-direct {p0, p1}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->isPersonaExists(I)Z

    move-result v2

    .line 2374
    if-eqz v2, :cond_4

    .line 2375
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "vnd.sec.contact.phone_knox"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2385
    :goto_0
    const/4 v4, 0x0

    .line 2386
    .local v4, "result":Z
    iget-boolean v5, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v5, :cond_0

    .line 2387
    iget-object v5, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " deleteAccountForPersona : Need to delete Account for deleted personaID : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2393
    :cond_0
    :try_start_0
    iget-object v5, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "content://com.android.contacts/settings"

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    const-string v7, "account_name=?"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    aput-object v0, v8, v9

    invoke-virtual {v5, v6, v7, v8}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    .line 2398
    .local v3, "noOfDelete":I
    iget-boolean v5, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v5, :cond_1

    .line 2399
    iget-object v5, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " deleteAccountForPersona noOfDelete = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " accountName = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2402
    :cond_1
    if-lt v3, v10, :cond_6

    .line 2403
    iget-boolean v5, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v5, :cond_2

    .line 2404
    iget-object v5, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v6, " deleteAccountForPersona Account deleted form CP "

    invoke-static {v5, v6}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2405
    :cond_2
    const/4 v4, 0x1

    .line 2417
    .end local v3    # "noOfDelete":I
    .end local v4    # "result":Z
    :cond_3
    :goto_1
    return v4

    .line 2377
    :cond_4
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "vnd.sec.contact.phone_personal"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2380
    :cond_5
    iget-boolean v5, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v5, :cond_3

    .line 2381
    iget-object v5, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v6, " deleteGroupForPersona mPm is null returning ..."

    invoke-static {v5, v6}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 2407
    .restart local v3    # "noOfDelete":I
    .restart local v4    # "result":Z
    :cond_6
    :try_start_1
    iget-boolean v5, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v5, :cond_7

    .line 2408
    iget-object v5, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " deleteAccountForPersona noOfDelete = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2409
    :cond_7
    iget-boolean v5, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v5, :cond_3

    .line 2410
    iget-object v5, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v6, "deleteAccountForPersona Failed to delete Account form CP "

    invoke-static {v5, v6}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 2413
    .end local v3    # "noOfDelete":I
    :catch_0
    move-exception v1

    .line 2414
    .local v1, "e":Ljava/lang/Exception;
    iget-object v5, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " deleteAccountForPersona : Exception while deleting Account "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " :: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " :: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/knox/rcp/components/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method deleteContactForPersona(I)Z
    .locals 12
    .param p1, "personaID"    # I

    .prologue
    const/4 v6, 0x0

    const/4 v11, 0x1

    .line 2309
    const/4 v4, 0x0

    .line 2310
    .local v4, "isPersona":Z
    const/4 v0, 0x0

    .line 2311
    .local v0, "accountName":Ljava/lang/String;
    iget-object v7, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mPm:Landroid/os/PersonaManager;

    if-eqz v7, :cond_4

    .line 2312
    invoke-direct {p0, p1}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->isPersonaExists(I)Z

    move-result v4

    .line 2313
    if-eqz v4, :cond_3

    .line 2314
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "vnd.sec.contact.phone_knox"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2324
    :goto_0
    const/4 v6, 0x0

    .line 2325
    .local v6, "result":Z
    iget-boolean v7, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v7, :cond_0

    .line 2326
    iget-object v7, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, " deleteContactForPersona : Need to delete Contact for deleted personaID : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2332
    :cond_0
    :try_start_0
    sget-object v7, Lcom/samsung/knox/rcp/components/contacts/syncer/util/DataSyncConstants;->CON_RAW_CONTACTS_URI:Landroid/net/Uri;

    invoke-virtual {v7}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v7

    const-string v8, "caller_is_syncadapter"

    const-string v9, "true"

    invoke-virtual {v7, v8, v9}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v7

    invoke-virtual {v7}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 2336
    .local v1, "conUri":Landroid/net/Uri;
    iget-object v7, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v8, "account_name=?"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    aput-object v0, v9, v10

    invoke-virtual {v7, v1, v8, v9}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v5

    .line 2341
    .local v5, "noOfDelete":I
    if-lt v5, v11, :cond_6

    .line 2342
    iget-boolean v7, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v7, :cond_1

    .line 2343
    iget-object v7, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v8, "deleteContactForPersona Entry deleted form CP, now deleteing local db entry "

    invoke-static {v7, v8}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2346
    :cond_1
    iget-object v7, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->contactsRcpDb:Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "raw_contacts_persona_id="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;->deleteContact(Ljava/lang/String;)I

    move-result v2

    .line 2348
    .local v2, "deleteLocalDB":I
    if-ge v2, v11, :cond_5

    .line 2349
    iget-boolean v7, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v7, :cond_2

    .line 2350
    iget-object v7, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v8, "deleteContactForPersona Failed to delete form local DB "

    invoke-static {v7, v8}, Lcom/samsung/knox/rcp/components/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2365
    .end local v1    # "conUri":Landroid/net/Uri;
    .end local v2    # "deleteLocalDB":I
    .end local v5    # "noOfDelete":I
    .end local v6    # "result":Z
    :cond_2
    :goto_1
    return v6

    .line 2316
    :cond_3
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "vnd.sec.contact.phone_personal"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 2319
    :cond_4
    iget-boolean v7, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v7, :cond_2

    .line 2320
    iget-object v7, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v8, " deleteGroupForPersona mPm is null returning ..."

    invoke-static {v7, v8}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 2352
    .restart local v1    # "conUri":Landroid/net/Uri;
    .restart local v2    # "deleteLocalDB":I
    .restart local v5    # "noOfDelete":I
    .restart local v6    # "result":Z
    :cond_5
    const/4 v6, 0x1

    .line 2353
    :try_start_1
    iget-boolean v7, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v7, :cond_2

    .line 2354
    iget-object v7, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v8, "deleteContactForPersona Entry deleted form local DB "

    invoke-static {v7, v8}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 2362
    .end local v1    # "conUri":Landroid/net/Uri;
    .end local v2    # "deleteLocalDB":I
    .end local v5    # "noOfDelete":I
    :catch_0
    move-exception v3

    .line 2363
    .local v3, "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 2358
    .end local v3    # "e":Ljava/lang/Exception;
    .restart local v1    # "conUri":Landroid/net/Uri;
    .restart local v5    # "noOfDelete":I
    :cond_6
    :try_start_2
    iget-boolean v7, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v7, :cond_2

    .line 2359
    iget-object v7, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v8, "deleteContactForPersona Failed to delete from CP "

    invoke-static {v7, v8}, Lcom/samsung/knox/rcp/components/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1
.end method

.method public deleteContacts(Ljava/util/ArrayList;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 1553
    iget-object v2, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/knox/rcp/components/contacts/syncer/util/DataSyncConstants;->CON_RAW_CONTACTS_URI:Landroid/net/Uri;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "_id IN "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-direct {p0, p1}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->argsArrayToString(Ljava/util/List;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4, v7}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 1556
    .local v1, "noOfDelete":I
    if-ge v1, v6, :cond_0

    .line 1557
    iget-object v2, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v3, "syncContacts Failed to delete from CP "

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1559
    iget-object v2, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/knox/rcp/components/contacts/syncer/util/DataSyncConstants;->CON_RAW_CONTACTS_URI:Landroid/net/Uri;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "_id IN "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-direct {p0, p1}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->argsArrayToString(Ljava/util/List;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4, v7}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 1562
    if-lez v1, :cond_4

    .line 1563
    iget-object v2, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "syncContacts Entry deleted form CP on second attempt"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1570
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->contactsRcpDb:Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_id_raw_container IN "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-direct {p0, p1}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->argsArrayToString(Ljava/util/List;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;->deleteContact(Ljava/lang/String;)I

    move-result v0

    .line 1572
    .local v0, "delLocalDB":I
    if-ge v0, v6, :cond_1

    .line 1573
    iget-object v2, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v3, "syncContacts Failed to delete form local DB "

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1575
    iget-object v2, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->contactsRcpDb:Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_id_raw_container IN "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-direct {p0, p1}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->argsArrayToString(Ljava/util/List;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;->deleteContact(Ljava/lang/String;)I

    move-result v0

    .line 1577
    if-ge v0, v6, :cond_5

    .line 1578
    iget-object v2, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v3, "syncContacts Failed to delete form local DB "

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1582
    :cond_1
    :goto_1
    if-ne v0, v1, :cond_2

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-eq v1, v2, :cond_3

    .line 1583
    :cond_2
    iget-object v2, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "deleteContacts size is unnormal"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1586
    :cond_3
    return-void

    .line 1566
    .end local v0    # "delLocalDB":I
    :cond_4
    iget-object v2, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v3, "syncContacts Failed to Failed to  delete form CP "

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1580
    .restart local v0    # "delLocalDB":I
    :cond_5
    iget-object v2, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v3, "syncContacts Entry deleted form local DB "

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method deleteGroupForPersona(I)Z
    .locals 11
    .param p1, "personaID"    # I

    .prologue
    const/4 v10, 0x1

    const/4 v4, 0x0

    .line 2247
    const/4 v2, 0x0

    .line 2248
    .local v2, "isPersona":Z
    const/4 v0, 0x0

    .line 2249
    .local v0, "accountName":Ljava/lang/String;
    iget-object v5, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mPm:Landroid/os/PersonaManager;

    if-eqz v5, :cond_4

    .line 2250
    invoke-direct {p0, p1}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->isPersonaExists(I)Z

    move-result v2

    .line 2251
    if-eqz v2, :cond_3

    .line 2252
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "vnd.sec.contact.phone_knox"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2262
    :goto_0
    const/4 v4, 0x0

    .line 2263
    .local v4, "result":Z
    iget-boolean v5, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v5, :cond_0

    .line 2264
    iget-object v5, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " deleteGroupForPersona : Need to delete Group for deleted personaID : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2270
    :cond_0
    :try_start_0
    iget-object v5, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/knox/rcp/components/contacts/syncer/util/DataSyncConstants;->CON_GROUP_URI:Landroid/net/Uri;

    const-string v7, "account_name=?"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    aput-object v0, v8, v9

    invoke-virtual {v5, v6, v7, v8}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    .line 2276
    .local v3, "noDeleted":I
    if-lt v3, v10, :cond_7

    .line 2277
    iget-boolean v5, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v5, :cond_1

    .line 2278
    iget-object v5, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v6, " deleteGroupForPersona Group delete successful from Contacts cp "

    invoke-static {v5, v6}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2279
    iget-object v5, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "deleteGroupForPersona deleting to local db  group with personaID "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2283
    :cond_1
    iget-object v5, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->contactsRcpDb:Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "groups_persona_id="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;->deleteGroup(Ljava/lang/String;)I

    move-result v3

    .line 2286
    if-gtz v3, :cond_5

    .line 2287
    iget-boolean v5, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v5, :cond_2

    .line 2288
    iget-object v5, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v6, " deleteGroupForPersona Deleting group failed for local db "

    invoke-static {v5, v6}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2304
    .end local v3    # "noDeleted":I
    .end local v4    # "result":Z
    :cond_2
    :goto_1
    return v4

    .line 2254
    :cond_3
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "vnd.sec.contact.phone_personal"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 2257
    :cond_4
    iget-boolean v5, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v5, :cond_2

    .line 2258
    iget-object v5, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v6, " deleteGroupForPersona mPm is null returning ..."

    invoke-static {v5, v6}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 2290
    .restart local v3    # "noDeleted":I
    .restart local v4    # "result":Z
    :cond_5
    :try_start_1
    iget-boolean v5, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v5, :cond_6

    .line 2291
    iget-object v5, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v6, " deleteGroupForPersona Deleted group successfully from local db "

    invoke-static {v5, v6}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2293
    :cond_6
    const/4 v4, 0x1

    goto :goto_1

    .line 2297
    :cond_7
    iget-boolean v5, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v5, :cond_2

    .line 2298
    iget-object v5, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v6, " deleteGroupForPersona Failed to delete group from Contact Provider"

    invoke-static {v5, v6}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 2301
    .end local v3    # "noDeleted":I
    :catch_0
    move-exception v1

    .line 2302
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method deleteSharedPrefsForPersona(I)Z
    .locals 5
    .param p1, "personaID"    # I

    .prologue
    .line 2422
    const/4 v1, 0x0

    .line 2423
    .local v1, "result":Z
    iget-boolean v2, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v2, :cond_0

    .line 2424
    iget-object v2, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " deleteSharedPrefsForPersona : Need to delete SP for deleted personaID : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2430
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mAccountsSharedPreferences:Landroid/content/SharedPreferences;

    if-nez v2, :cond_1

    .line 2431
    iget-object v2, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mContext:Landroid/content/Context;

    const-string v3, "created_accounts_uris_per_persona_shared_prefs"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mAccountsSharedPreferences:Landroid/content/SharedPreferences;

    .line 2434
    :cond_1
    iget-object v2, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mAccountsSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mAccountsSharedPrefsEditor:Landroid/content/SharedPreferences$Editor;

    .line 2435
    iget-object v2, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mAccountsSharedPrefsEditor:Landroid/content/SharedPreferences$Editor;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 2436
    iget-object v2, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mAccountsSharedPrefsEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2438
    iget-object v2, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mAccountTypeSP:Landroid/content/SharedPreferences;

    if-nez v2, :cond_2

    .line 2439
    iget-object v2, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mContext:Landroid/content/Context;

    const-string v3, "created_accounts_type_per_persona_shared_prefs"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mAccountTypeSP:Landroid/content/SharedPreferences;

    .line 2442
    :cond_2
    iget-object v2, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mAccountTypeSP:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mAccountTypeSPEditor:Landroid/content/SharedPreferences$Editor;

    .line 2443
    iget-object v2, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mAccountTypeSPEditor:Landroid/content/SharedPreferences$Editor;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 2444
    iget-object v2, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mAccountTypeSPEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2446
    const/4 v1, 0x1

    .line 2451
    :goto_0
    return v1

    .line 2447
    :catch_0
    move-exception v0

    .line 2448
    .local v0, "e":Ljava/lang/Exception;
    const/4 v1, 0x0

    .line 2449
    iget-object v2, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " deleteSharedPrefsForPersona : Exception while deleting SP  "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method insertContacts(Landroid/content/CustomCursor;I)V
    .locals 41
    .param p1, "c"    # Landroid/content/CustomCursor;
    .param p2, "userOrPersonaId"    # I

    .prologue
    .line 1646
    const-wide/16 v38, 0x0

    .line 1647
    .local v38, "raw_id_container":J
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v4, :cond_0

    .line 1648
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "insertContacts(CustomCursor:"

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v16, ") called"

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1650
    :cond_0
    new-instance v35, Ljava/util/ArrayList;

    invoke-direct/range {v35 .. v35}, Ljava/util/ArrayList;-><init>()V

    .line 1651
    .local v35, "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    const/16 v18, 0x0

    .line 1652
    .local v18, "accountType":Ljava/lang/String;
    const/16 v17, 0x0

    .line 1653
    .local v17, "accountName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mPm:Landroid/os/PersonaManager;

    move/from16 v0, p2

    invoke-virtual {v4, v0}, Landroid/os/PersonaManager;->exists(I)Z

    move-result v32

    .line 1655
    .local v32, "isPersona":Z
    if-eqz v32, :cond_5

    .line 1656
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->createOrGetAccountTypeForKnox(I)Ljava/lang/String;

    move-result-object v18

    .line 1657
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v4, :cond_1

    .line 1658
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, " insertContacts() knoxAccountType = "

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v16, " for user = "

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, p2

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1660
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "vnd.sec.contact.phone_knox"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, p2

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    .line 1668
    :goto_0
    sget-object v4, Lcom/samsung/knox/rcp/components/contacts/syncer/util/DataSyncConstants;->CON_RAW_CONTACTS_URI:Landroid/net/Uri;

    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v5, "account_type"

    move-object/from16 v0, v18

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v5, "account_name"

    move-object/from16 v0, v17

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v5, "starred"

    sget-object v16, Lcom/samsung/knox/rcp/components/contacts/syncer/util/DataSyncConstants;->ORI_RAWCONTACTS_PROJECTION:[Ljava/lang/String;

    const/16 v40, 0x3

    aget-object v16, v16, v40

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v16

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v5, "name_verified"

    const-string v16, "name_verified"

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v16

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/content/CustomCursor;->getInt(I)I

    move-result v16

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v5, "display_name_source"

    const-string v16, "display_name_source"

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v16

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/content/CustomCursor;->getInt(I)I

    move-result v16

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v5, "raw_contact_is_read_only"

    const/16 v16, 0x1

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    move-object/from16 v0, v35

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1685
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v4, :cond_2

    .line 1686
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Add to batch, raw contact insert name="

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v16, Lcom/samsung/knox/rcp/components/contacts/syncer/util/DataSyncConstants;->ORI_RAWCONTACTS_PROJECTION:[Ljava/lang/String;

    const/16 v40, 0x5

    aget-object v16, v16, v40

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v16

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1691
    :cond_2
    const/16 v36, 0x0

    .line 1696
    .local v36, "orig_data_cur":Landroid/content/CustomCursor;
    :try_start_0
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v4, :cond_3

    .line 1697
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v5, "INSERT Querying data from Outside CONTACTS_PROVIDER Data table  "

    invoke-static {v4, v5}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1698
    :cond_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mBridgeService:Lcom/sec/knox/bridge/IBridgeService;

    const-string v5, "Contacts"

    const-string v6, "DATA_CONTACTS"

    sget-object v8, Lcom/samsung/knox/rcp/components/contacts/syncer/util/DataSyncConstants;->ORI_DATA_PROJECTION:[Ljava/lang/String;

    const-string v9, "raw_contact_id=?"

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v10, v0, [Ljava/lang/String;

    const/16 v16, 0x0

    const-string v40, "_id"

    move-object/from16 v0, p1

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v40

    move-object/from16 v0, p1

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v40

    aput-object v40, v10, v16

    const/4 v11, 0x0

    move/from16 v7, p2

    invoke-interface/range {v4 .. v11}, Lcom/sec/knox/bridge/IBridgeService;->queryProvider(Ljava/lang/String;Ljava/lang/String;I[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/content/CustomCursor;

    move-result-object v36

    .line 1704
    if-nez v36, :cond_6

    .line 1708
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v4, :cond_4

    .line 1709
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "insertContacts(): orig_data_cur is null, do not insert anything for userOrPersonaId="

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, p2

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/knox/rcp/components/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1885
    :cond_4
    move-object/from16 v0, p0

    move-object/from16 v1, v36

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->closeCursor(Landroid/database/Cursor;)V

    move-wide/from16 v8, v38

    .line 1957
    .end local v38    # "raw_id_container":J
    .local v8, "raw_id_container":J
    :goto_1
    return-void

    .line 1662
    .end local v8    # "raw_id_container":J
    .end local v36    # "orig_data_cur":Landroid/content/CustomCursor;
    .restart local v38    # "raw_id_container":J
    :cond_5
    const-string v18, "vnd.sec.contact.phone_personal"

    .line 1663
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "vnd.sec.contact.phone_personal"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, p2

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    goto/16 :goto_0

    .line 1715
    .restart local v36    # "orig_data_cur":Landroid/content/CustomCursor;
    :cond_6
    :try_start_1
    invoke-virtual/range {v36 .. v36}, Landroid/content/CustomCursor;->getCount()I

    move-result v26

    .line 1716
    .local v26, "curCount":I
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v4, :cond_7

    .line 1717
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "INSERT orig_data_cur count is  "

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v26

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1720
    :cond_7
    if-lez v26, :cond_d

    .line 1721
    const/16 v21, 0x0

    .line 1723
    .local v21, "con_group_cur":Landroid/database/Cursor;
    :try_start_2
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v4, :cond_8

    .line 1724
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "INSERT quering group cursor  "

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v26

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1726
    :cond_8
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Landroid/provider/ContactsContract$Groups;->CONTENT_URI:Landroid/net/Uri;

    const/16 v16, 0x2

    move/from16 v0, v16

    new-array v6, v0, [Ljava/lang/String;

    const/16 v16, 0x0

    const-string v40, "_id"

    aput-object v40, v6, v16

    const/16 v16, 0x1

    const-string v40, "title"

    aput-object v40, v6, v16

    const-string v7, "account_name=? AND account_type=? AND deleted=?"

    const/16 v16, 0x3

    move/from16 v0, v16

    new-array v8, v0, [Ljava/lang/String;

    const/16 v16, 0x0

    aput-object v17, v8, v16

    const/16 v16, 0x1

    aput-object v18, v8, v16

    const/16 v16, 0x2

    const-string v40, "0"

    aput-object v40, v8, v16

    const-string v9, "_id ASC"

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v21

    .line 1737
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v4, :cond_9

    .line 1738
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, " insertContacts con_group_cur "

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v21

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1739
    :cond_9
    if-eqz v21, :cond_20

    .line 1740
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v4, :cond_a

    .line 1741
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v5, "About to insert data rows"

    invoke-static {v4, v5}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1742
    :cond_a
    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->getCount()I

    move-result v20

    .line 1743
    .local v20, "conGroupCount":I
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v4, :cond_b

    .line 1744
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, " insertContacts conGroupCount "

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v20

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1745
    :cond_b
    invoke-virtual/range {v36 .. v36}, Landroid/content/CustomCursor;->moveToFirst()Z

    .line 1748
    const/16 v28, 0x0

    .local v28, "i":I
    :goto_2
    move/from16 v0, v28

    move/from16 v1, v26

    if-ge v0, v1, :cond_c

    .line 1750
    sget-boolean v4, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->isSyncState:Z

    if-nez v4, :cond_11

    .line 1751
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v5, "mSyncThread interrupt "

    invoke-static {v4, v5}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1877
    .end local v20    # "conGroupCount":I
    .end local v28    # "i":I
    :cond_c
    :goto_3
    :try_start_3
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->closeCursor(Landroid/database/Cursor;)V

    .line 1878
    move-object/from16 v0, p0

    move-object/from16 v1, v36

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->closeCursor(Landroid/database/Cursor;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1885
    .end local v21    # "con_group_cur":Landroid/database/Cursor;
    :cond_d
    move-object/from16 v0, p0

    move-object/from16 v1, v36

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->closeCursor(Landroid/database/Cursor;)V

    .line 1890
    :try_start_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "com.android.contacts"

    move-object/from16 v0, v35

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    move-result-object v23

    .line 1892
    .local v23, "cpr":[Landroid/content/ContentProviderResult;
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v4, :cond_e

    .line 1893
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v5, "Batch operation applied to insert to contacts"

    invoke-static {v4, v5}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4

    .line 1895
    :cond_e
    const/16 v22, 0x0

    .line 1897
    .local v22, "con_raw_cur":Landroid/database/Cursor;
    :try_start_5
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const/4 v5, 0x0

    aget-object v5, v23, v5

    iget-object v5, v5, Landroid/content/ContentProviderResult;->uri:Landroid/net/Uri;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v22

    .line 1900
    if-eqz v22, :cond_f

    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-nez v4, :cond_21

    .line 1901
    :cond_f
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v4, :cond_10

    .line 1902
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Rorre during local DB insert/join check for userOrPersonaId="

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, p2

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v16, "; con_raw_cur is null or empty. Cannot record for uri: "

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/16 v16, 0x0

    aget-object v16, v23, v16

    move-object/from16 v0, v16

    iget-object v0, v0, Landroid/content/ContentProviderResult;->uri:Landroid/net/Uri;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/knox/rcp/components/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 1950
    :cond_10
    :try_start_6
    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->closeCursor(Landroid/database/Cursor;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_4

    move-wide/from16 v8, v38

    .end local v38    # "raw_id_container":J
    .restart local v8    # "raw_id_container":J
    goto/16 :goto_1

    .line 1755
    .end local v8    # "raw_id_container":J
    .end local v22    # "con_raw_cur":Landroid/database/Cursor;
    .end local v23    # "cpr":[Landroid/content/ContentProviderResult;
    .restart local v20    # "conGroupCount":I
    .restart local v21    # "con_group_cur":Landroid/database/Cursor;
    .restart local v28    # "i":I
    .restart local v38    # "raw_id_container":J
    :cond_11
    :try_start_7
    sget-object v4, Lcom/samsung/knox/rcp/components/contacts/syncer/util/DataSyncConstants;->CON_DATA_URI:Landroid/net/Uri;

    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v19

    .line 1757
    .local v19, "builder_data":Landroid/content/ContentProviderOperation$Builder;
    const-string v4, "mimetype"

    sget-object v5, Lcom/samsung/knox/rcp/components/contacts/syncer/util/DataSyncConstants;->ORI_DATA_PROJECTION:[Ljava/lang/String;

    const/16 v16, 0x0

    aget-object v5, v5, v16

    move-object/from16 v0, v36

    invoke-virtual {v0, v5}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v36

    invoke-virtual {v0, v5}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 1762
    const-string v4, "is_primary"

    sget-object v5, Lcom/samsung/knox/rcp/components/contacts/syncer/util/DataSyncConstants;->ORI_DATA_PROJECTION:[Ljava/lang/String;

    const/16 v16, 0x1

    aget-object v5, v5, v16

    move-object/from16 v0, v36

    invoke-virtual {v0, v5}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v36

    invoke-virtual {v0, v5}, Landroid/content/CustomCursor;->getInt(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 1767
    const-string v4, "is_super_primary"

    sget-object v5, Lcom/samsung/knox/rcp/components/contacts/syncer/util/DataSyncConstants;->ORI_DATA_PROJECTION:[Ljava/lang/String;

    const/16 v16, 0x12

    aget-object v5, v5, v16

    move-object/from16 v0, v36

    invoke-virtual {v0, v5}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v36

    invoke-virtual {v0, v5}, Landroid/content/CustomCursor;->getInt(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 1773
    const-string v4, "vnd.android.cursor.item/photo"

    sget-object v5, Lcom/samsung/knox/rcp/components/contacts/syncer/util/DataSyncConstants;->ORI_DATA_PROJECTION:[Ljava/lang/String;

    const/16 v16, 0x0

    aget-object v5, v5, v16

    move-object/from16 v0, v36

    invoke-virtual {v0, v5}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v36

    invoke-virtual {v0, v5}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_14

    .line 1776
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v4, :cond_12

    .line 1777
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v5, "This data row contains a photo"

    invoke-static {v4, v5}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1778
    :cond_12
    const-string v4, "data14"

    sget-object v5, Lcom/samsung/knox/rcp/components/contacts/syncer/util/DataSyncConstants;->ORI_DATA_PROJECTION:[Ljava/lang/String;

    const/16 v16, 0xf

    aget-object v5, v5, v16

    move-object/from16 v0, v36

    invoke-virtual {v0, v5}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v36

    invoke-virtual {v0, v5}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 1783
    const-string v4, "data15"

    sget-object v5, Lcom/samsung/knox/rcp/components/contacts/syncer/util/DataSyncConstants;->ORI_DATA_PROJECTION:[Ljava/lang/String;

    const/16 v16, 0x10

    aget-object v5, v5, v16

    move-object/from16 v0, v36

    invoke-virtual {v0, v5}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v36

    invoke-virtual {v0, v5}, Landroid/content/CustomCursor;->getBlob(I)[B

    move-result-object v5

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 1860
    :cond_13
    const-string v4, "is_read_only"

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 1861
    const-string v4, "raw_contact_id"

    const/4 v5, 0x0

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    .line 1863
    invoke-virtual/range {v19 .. v19}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    move-object/from16 v0, v35

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1864
    invoke-virtual/range {v36 .. v36}, Landroid/content/CustomCursor;->moveToNext()Z

    .line 1748
    :goto_4
    add-int/lit8 v28, v28, 0x1

    goto/16 :goto_2

    .line 1788
    :cond_14
    const-string v4, "vnd.android.cursor.item/group_membership"

    sget-object v5, Lcom/samsung/knox/rcp/components/contacts/syncer/util/DataSyncConstants;->ORI_DATA_PROJECTION:[Ljava/lang/String;

    const/16 v16, 0x0

    aget-object v5, v5, v16

    move-object/from16 v0, v36

    invoke-virtual {v0, v5}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v36

    invoke-virtual {v0, v5}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1e

    .line 1791
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v4, :cond_15

    .line 1792
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v5, "This data row contains group membership"

    invoke-static {v4, v5}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1795
    :cond_15
    if-lez v20, :cond_1d

    .line 1796
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v4, :cond_16

    .line 1797
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v5, "groups exist in cursor"

    invoke-static {v4, v5}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1798
    :cond_16
    const/16 v34, 0x0

    .line 1799
    .local v34, "matched":Z
    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1800
    const/16 v29, 0x0

    .local v29, "iGrp":I
    :goto_5
    move/from16 v0, v29

    move/from16 v1, v20

    if-ge v0, v1, :cond_19

    .line 1807
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->contactsRcpDb:Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;

    const-string v5, "_id"

    move-object/from16 v0, v21

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v21

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;->getGroupOriIdFromConId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v37

    .line 1810
    .local v37, "orig_group_id":Ljava/lang/String;
    sget-object v4, Lcom/samsung/knox/rcp/components/contacts/syncer/util/DataSyncConstants;->ORI_DATA_PROJECTION:[Ljava/lang/String;

    const/4 v5, 0x2

    aget-object v4, v4, v5

    move-object/from16 v0, v36

    invoke-virtual {v0, v4}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v36

    invoke-virtual {v0, v4}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v37

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1c

    .line 1816
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v4, :cond_17

    .line 1817
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "orig group _id ("

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v37

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v16, ") = data1"

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1819
    :cond_17
    const-string v4, "Starred in Android"

    const/4 v5, 0x1

    move-object/from16 v0, v21

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1b

    .line 1821
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v4, :cond_18

    .line 1822
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v5, "group_membership-group title is not \'Starred in Android\', adds data1 to builder_data"

    invoke-static {v4, v5}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1824
    :cond_18
    const/16 v34, 0x1

    .line 1825
    const-string v4, "data1"

    const/4 v5, 0x0

    move-object/from16 v0, v21

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 1837
    .end local v37    # "orig_group_id":Ljava/lang/String;
    :cond_19
    :goto_6
    if-nez v34, :cond_13

    .line 1838
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v4, :cond_1a

    .line 1839
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v5, "group_membership-starred in android group"

    invoke-static {v4, v5}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1840
    :cond_1a
    invoke-virtual/range {v36 .. v36}, Landroid/content/CustomCursor;->moveToNext()Z
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_4

    .line 1872
    .end local v19    # "builder_data":Landroid/content/ContentProviderOperation$Builder;
    .end local v20    # "conGroupCount":I
    .end local v28    # "i":I
    .end local v29    # "iGrp":I
    .end local v34    # "matched":Z
    :catch_0
    move-exception v27

    .line 1873
    .local v27, "e":Ljava/lang/Exception;
    :try_start_8
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v5, "Exception during insertContacts() add data="

    invoke-static {v4, v5}, Lcom/samsung/knox/rcp/components/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1874
    invoke-virtual/range {v27 .. v27}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 1877
    :try_start_9
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->closeCursor(Landroid/database/Cursor;)V

    .line 1878
    move-object/from16 v0, p0

    move-object/from16 v1, v36

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->closeCursor(Landroid/database/Cursor;)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 1885
    move-object/from16 v0, p0

    move-object/from16 v1, v36

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->closeCursor(Landroid/database/Cursor;)V

    move-wide/from16 v8, v38

    .end local v38    # "raw_id_container":J
    .restart local v8    # "raw_id_container":J
    goto/16 :goto_1

    .line 1829
    .end local v8    # "raw_id_container":J
    .end local v27    # "e":Ljava/lang/Exception;
    .restart local v19    # "builder_data":Landroid/content/ContentProviderOperation$Builder;
    .restart local v20    # "conGroupCount":I
    .restart local v28    # "i":I
    .restart local v29    # "iGrp":I
    .restart local v34    # "matched":Z
    .restart local v37    # "orig_group_id":Ljava/lang/String;
    .restart local v38    # "raw_id_container":J
    :cond_1b
    :try_start_a
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v4, :cond_19

    .line 1830
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v5, "group_membership-no value added to builder_data"

    invoke-static {v4, v5}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_0
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto :goto_6

    .line 1877
    .end local v19    # "builder_data":Landroid/content/ContentProviderOperation$Builder;
    .end local v20    # "conGroupCount":I
    .end local v28    # "i":I
    .end local v29    # "iGrp":I
    .end local v34    # "matched":Z
    .end local v37    # "orig_group_id":Ljava/lang/String;
    :catchall_0
    move-exception v4

    :try_start_b
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->closeCursor(Landroid/database/Cursor;)V

    .line 1878
    move-object/from16 v0, p0

    move-object/from16 v1, v36

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->closeCursor(Landroid/database/Cursor;)V

    throw v4
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_1
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    .line 1881
    .end local v21    # "con_group_cur":Landroid/database/Cursor;
    .end local v26    # "curCount":I
    :catch_1
    move-exception v27

    .line 1882
    .restart local v27    # "e":Ljava/lang/Exception;
    :try_start_c
    invoke-virtual/range {v27 .. v27}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    .line 1885
    move-object/from16 v0, p0

    move-object/from16 v1, v36

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->closeCursor(Landroid/database/Cursor;)V

    move-wide/from16 v8, v38

    .end local v38    # "raw_id_container":J
    .restart local v8    # "raw_id_container":J
    goto/16 :goto_1

    .line 1835
    .end local v8    # "raw_id_container":J
    .end local v27    # "e":Ljava/lang/Exception;
    .restart local v19    # "builder_data":Landroid/content/ContentProviderOperation$Builder;
    .restart local v20    # "conGroupCount":I
    .restart local v21    # "con_group_cur":Landroid/database/Cursor;
    .restart local v26    # "curCount":I
    .restart local v28    # "i":I
    .restart local v29    # "iGrp":I
    .restart local v34    # "matched":Z
    .restart local v37    # "orig_group_id":Ljava/lang/String;
    .restart local v38    # "raw_id_container":J
    :cond_1c
    :try_start_d
    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->moveToNext()Z

    .line 1800
    add-int/lit8 v29, v29, 0x1

    goto/16 :goto_5

    .line 1844
    .end local v29    # "iGrp":I
    .end local v34    # "matched":Z
    .end local v37    # "orig_group_id":Ljava/lang/String;
    :cond_1d
    invoke-virtual/range {v36 .. v36}, Landroid/content/CustomCursor;->moveToNext()Z

    goto/16 :goto_4

    .line 1848
    :cond_1e
    const/16 v33, 0x1

    .local v33, "k":I
    :goto_7
    const/16 v4, 0x10

    move/from16 v0, v33

    if-ge v0, v4, :cond_13

    .line 1849
    sget-object v4, Lcom/samsung/knox/rcp/components/contacts/syncer/util/DataSyncConstants;->ORI_DATA_PROJECTION:[Ljava/lang/String;

    add-int/lit8 v5, v33, 0x1

    aget-object v4, v4, v5

    move-object/from16 v0, v36

    invoke-virtual {v0, v4}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v36

    invoke-virtual {v0, v4}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1f

    .line 1852
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "data"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v33

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/samsung/knox/rcp/components/contacts/syncer/util/DataSyncConstants;->ORI_DATA_PROJECTION:[Ljava/lang/String;

    add-int/lit8 v16, v33, 0x1

    aget-object v5, v5, v16

    move-object/from16 v0, v36

    invoke-virtual {v0, v5}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v36

    invoke-virtual {v0, v5}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 1848
    :cond_1f
    add-int/lit8 v33, v33, 0x1

    goto :goto_7

    .line 1868
    .end local v19    # "builder_data":Landroid/content/ContentProviderOperation$Builder;
    .end local v20    # "conGroupCount":I
    .end local v28    # "i":I
    .end local v33    # "k":I
    :cond_20
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v4, :cond_c

    .line 1869
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v5, "con_group_cur is null, new group not created and nothing inserted to data"

    invoke-static {v4, v5}, Lcom/samsung/knox/rcp/components/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_0
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    goto/16 :goto_3

    .line 1885
    .end local v21    # "con_group_cur":Landroid/database/Cursor;
    .end local v26    # "curCount":I
    :catchall_1
    move-exception v4

    move-object/from16 v0, p0

    move-object/from16 v1, v36

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->closeCursor(Landroid/database/Cursor;)V

    throw v4

    .line 1910
    .restart local v22    # "con_raw_cur":Landroid/database/Cursor;
    .restart local v23    # "cpr":[Landroid/content/ContentProviderResult;
    .restart local v26    # "curCount":I
    :cond_21
    :try_start_e
    const-string v4, "contact_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/CustomCursor;->getLong(I)J

    move-result-wide v10

    .line 1912
    .local v10, "raw_contact_id_original":J
    const-string v4, "contact_id"

    move-object/from16 v0, v22

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v22

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    .line 1914
    .local v12, "raw_contact_id_container":J
    const-string v4, "_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/CustomCursor;->getLong(I)J

    move-result-wide v6

    .line 1916
    .local v6, "raw_id_original":J
    const/4 v4, 0x0

    aget-object v4, v23, v4

    iget-object v4, v4, Landroid/content/ContentProviderResult;->uri:Landroid/net/Uri;

    invoke-static {v4}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_3
    .catchall {:try_start_e .. :try_end_e} :catchall_2

    move-result-wide v8

    .line 1917
    .end local v38    # "raw_id_container":J
    .restart local v8    # "raw_id_container":J
    :try_start_f
    const-string v4, "version"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/CustomCursor;->getInt(I)I

    move-result v14

    .line 1919
    .local v14, "raw_contact_version":I
    const-string v4, "name_verified"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/CustomCursor;->getInt(I)I

    move-result v15

    .line 1922
    .local v15, "raw_name_verified":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->contactsRcpDb:Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;

    move/from16 v0, p2

    invoke-virtual {v4, v10, v11, v0}, Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;->getContactIdForOriRawConID(JI)J

    move-result-wide v24

    .line 1924
    .local v24, "contactToJoin":J
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v4, :cond_22

    .line 1925
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "contactToJoin value: "

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, v24

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1926
    :cond_22
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v4, :cond_23

    .line 1927
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "INSERT raw_id_original,raw_Id,raw_contact_id_original,raw_contact_Id,raw_contact_version,raw_name_verified"

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v16, " "

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v16, " "

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v16, " "

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v16, " "

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v16, " "

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1938
    :cond_23
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->contactsRcpDb:Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;

    move/from16 v16, p2

    invoke-virtual/range {v5 .. v16}, Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;->insertContact(JJJJIII)J

    move-result-wide v30

    .line 1942
    .local v30, "insertResult":J
    const-wide/16 v4, -0x1

    cmp-long v4, v24, v4

    if-eqz v4, :cond_24

    .line 1944
    move-object/from16 v0, p0

    move-wide/from16 v1, v24

    invoke-virtual {v0, v12, v13, v1, v2}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->contactsJoin(JJ)V
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_5
    .catchall {:try_start_f .. :try_end_f} :catchall_3

    .line 1950
    :cond_24
    :try_start_10
    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->closeCursor(Landroid/database/Cursor;)V
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_2

    goto/16 :goto_1

    .line 1953
    .end local v6    # "raw_id_original":J
    .end local v10    # "raw_contact_id_original":J
    .end local v12    # "raw_contact_id_container":J
    .end local v14    # "raw_contact_version":I
    .end local v15    # "raw_name_verified":I
    .end local v24    # "contactToJoin":J
    .end local v30    # "insertResult":J
    :catch_2
    move-exception v27

    .line 1954
    .end local v22    # "con_raw_cur":Landroid/database/Cursor;
    .end local v23    # "cpr":[Landroid/content/ContentProviderResult;
    .restart local v27    # "e":Ljava/lang/Exception;
    :goto_8
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "INSERT Exception during  .applyBatch for userOrPersonaId="

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, p2

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v16, "; log: "

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {v27 .. v27}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/knox/rcp/components/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1946
    .end local v8    # "raw_id_container":J
    .end local v27    # "e":Ljava/lang/Exception;
    .restart local v22    # "con_raw_cur":Landroid/database/Cursor;
    .restart local v23    # "cpr":[Landroid/content/ContentProviderResult;
    .restart local v38    # "raw_id_container":J
    :catch_3
    move-exception v27

    move-wide/from16 v8, v38

    .line 1947
    .end local v38    # "raw_id_container":J
    .restart local v8    # "raw_id_container":J
    .restart local v27    # "e":Ljava/lang/Exception;
    :goto_9
    :try_start_11
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Exception during local DB insert/join check for userOrPersonaId="

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, p2

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v16, "; log: "

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {v27 .. v27}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/knox/rcp/components/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_3

    .line 1950
    :try_start_12
    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->closeCursor(Landroid/database/Cursor;)V

    goto/16 :goto_1

    .end local v8    # "raw_id_container":J
    .end local v27    # "e":Ljava/lang/Exception;
    .restart local v38    # "raw_id_container":J
    :catchall_2
    move-exception v4

    move-wide/from16 v8, v38

    .end local v38    # "raw_id_container":J
    .restart local v8    # "raw_id_container":J
    :goto_a
    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->closeCursor(Landroid/database/Cursor;)V

    throw v4
    :try_end_12
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_12} :catch_2

    .line 1953
    .end local v8    # "raw_id_container":J
    .end local v22    # "con_raw_cur":Landroid/database/Cursor;
    .end local v23    # "cpr":[Landroid/content/ContentProviderResult;
    .restart local v38    # "raw_id_container":J
    :catch_4
    move-exception v27

    move-wide/from16 v8, v38

    .end local v38    # "raw_id_container":J
    .restart local v8    # "raw_id_container":J
    goto :goto_8

    .line 1950
    .restart local v22    # "con_raw_cur":Landroid/database/Cursor;
    .restart local v23    # "cpr":[Landroid/content/ContentProviderResult;
    :catchall_3
    move-exception v4

    goto :goto_a

    .line 1946
    .restart local v6    # "raw_id_original":J
    .restart local v10    # "raw_contact_id_original":J
    .restart local v12    # "raw_contact_id_container":J
    :catch_5
    move-exception v27

    goto :goto_9
.end method

.method public insertLocalDB(ILcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;I)V
    .locals 18
    .param p1, "Count"    # I
    .param p2, "rawContactInfo"    # Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;
    .param p3, "userOrPersonaId"    # I

    .prologue
    .line 892
    const/4 v15, 0x0

    .line 894
    .local v15, "con_raw_cur":Landroid/database/Cursor;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/knox/rcp/components/contacts/syncer/util/DataSyncConstants;->CON_RAW_CONTACTS_URI:Landroid/net/Uri;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "contact_id"

    aput-object v6, v4, v5

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "_id IN "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;->get_con_id()Ljava/util/ArrayList;

    move-result-object v6

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->argsArrayToString(Ljava/util/List;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const-string v7, "_id ASC"

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v15

    .line 902
    invoke-interface {v15}, Landroid/database/Cursor;->moveToFirst()Z

    .line 904
    invoke-interface {v15}, Landroid/database/Cursor;->getCount()I

    move-result v2

    move/from16 v0, p1

    if-eq v2, v0, :cond_0

    .line 905
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "con_raw_cur.getCount() != Count "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v15}, Landroid/database/Cursor;->getCount()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 907
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "insertLocalDB Count "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 909
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->contactsRcpDb:Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;

    invoke-virtual {v2}, Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;->beginTransaction()V

    .line 911
    const/16 v17, 0x0

    .local v17, "j":I
    :goto_0
    move/from16 v0, v17

    move/from16 v1, p1

    if-ge v0, v1, :cond_1

    .line 912
    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->contactsRcpDb:Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;->get_id()Ljava/util/ArrayList;

    move-result-object v2

    move/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;->get_con_id()Ljava/util/ArrayList;

    move-result-object v2

    move/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;->get_contactid()Ljava/util/ArrayList;

    move-result-object v2

    move/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    const/4 v2, 0x0

    invoke-interface {v15, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;->get_version()Ljava/util/ArrayList;

    move-result-object v2

    move/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v12

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;->get_name_verified()Ljava/util/ArrayList;

    move-result-object v2

    move/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v13

    move/from16 v14, p3

    invoke-virtual/range {v3 .. v14}, Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;->insertContact(JJJJIII)J

    .line 917
    invoke-interface {v15}, Landroid/database/Cursor;->moveToNext()Z

    .line 918
    sget-boolean v2, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->isSyncState:Z

    if-nez v2, :cond_2

    .line 919
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v3, "mSyncThread interrupt "

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 923
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->contactsRcpDb:Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;

    invoke-virtual {v2}, Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;->setTransactionSuccessful()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 927
    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->closeCursor(Landroid/database/Cursor;)V

    .line 928
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->contactsRcpDb:Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;

    invoke-virtual {v2}, Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;->endTransaction()V

    .line 930
    :goto_1
    return-void

    .line 911
    :cond_2
    add-int/lit8 v17, v17, 0x1

    goto/16 :goto_0

    .line 924
    :catch_0
    move-exception v16

    .line 925
    .local v16, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual/range {v16 .. v16}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 927
    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->closeCursor(Landroid/database/Cursor;)V

    .line 928
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->contactsRcpDb:Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;

    invoke-virtual {v2}, Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;->endTransaction()V

    goto :goto_1

    .line 927
    .end local v16    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->closeCursor(Landroid/database/Cursor;)V

    .line 928
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->contactsRcpDb:Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;

    invoke-virtual {v3}, Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;->endTransaction()V

    throw v2
.end method

.method public insertRawContact(Ljava/util/ArrayList;Landroid/content/CustomCursor;ILcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;)V
    .locals 10
    .param p2, "orig_raw_cur"    # Landroid/content/CustomCursor;
    .param p3, "userOrPersonaId"    # I
    .param p4, "rawContactInfo"    # Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;",
            "Landroid/content/CustomCursor;",
            "I",
            "Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;",
            ")V"
        }
    .end annotation

    .prologue
    .line 756
    .local p1, "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    const/4 v8, 0x0

    .line 757
    .local v8, "accountType":Ljava/lang/String;
    const/4 v0, 0x0

    .line 758
    .local v0, "accountName":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mPm:Landroid/os/PersonaManager;

    invoke-virtual {v1, p3}, Landroid/os/PersonaManager;->exists(I)Z

    move-result v9

    .line 760
    .local v9, "isPersona":Z
    if-eqz v9, :cond_0

    .line 761
    invoke-virtual {p0, p3}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->createOrGetAccountTypeForKnox(I)Ljava/lang/String;

    move-result-object v8

    .line 762
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "vnd.sec.contact.phone_knox"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 768
    :goto_0
    const-string v1, "_id"

    invoke-virtual {p2, v1}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p2, v1}, Landroid/content/CustomCursor;->getLong(I)J

    move-result-wide v2

    .line 770
    .local v2, "s_id":J
    sget-object v1, Lcom/samsung/knox/rcp/components/contacts/syncer/util/DataSyncConstants;->CON_RAW_CONTACTS_URI:Landroid/net/Uri;

    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const-string v4, "account_type"

    invoke-virtual {v1, v4, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const-string v4, "account_name"

    invoke-virtual {v1, v4, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const-string v4, "starred"

    const-string v5, "starred"

    invoke-virtual {p2, v5}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {p2, v5}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const-string v4, "display_name_source"

    const-string v5, "display_name_source"

    invoke-virtual {p2, v5}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {p2, v5}, Landroid/content/CustomCursor;->getInt(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const-string v4, "name_verified"

    const-string v5, "name_verified"

    invoke-virtual {p2, v5}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {p2, v5}, Landroid/content/CustomCursor;->getInt(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const-string v4, "raw_contact_is_read_only"

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 787
    const-string v1, "contact_id"

    invoke-virtual {p2, v1}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p2, v1}, Landroid/content/CustomCursor;->getLong(I)J

    move-result-wide v4

    const-string v1, "version"

    invoke-virtual {p2, v1}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p2, v1}, Landroid/content/CustomCursor;->getInt(I)I

    move-result v6

    const-string v1, "name_verified"

    invoke-virtual {p2, v1}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p2, v1}, Landroid/content/CustomCursor;->getInt(I)I

    move-result v7

    move-object v1, p4

    invoke-virtual/range {v1 .. v7}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;->insert_RawContact(JJII)V

    .line 793
    return-void

    .line 764
    .end local v2    # "s_id":J
    :cond_0
    const-string v8, "vnd.sec.contact.phone_personal"

    .line 765
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "vnd.sec.contact.phone_personal"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method

.method isBridgeServiceValid()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 310
    iget-object v1, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mBridgeService:Lcom/sec/knox/bridge/IBridgeService;

    if-nez v1, :cond_1

    .line 311
    iget-boolean v1, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v1, :cond_0

    .line 312
    iget-object v1, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v2, "isBridgeServiceValid(): mBridgeService == null"

    invoke-static {v1, v2}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 327
    :cond_0
    :goto_0
    return v0

    .line 314
    :cond_1
    iget-object v1, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mBinderProxy:Landroid/os/IBinder;

    if-nez v1, :cond_2

    .line 315
    iget-boolean v1, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v1, :cond_0

    .line 316
    iget-object v1, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v2, "isBridgeServiceValid(): mBinderProxy == null"

    invoke-static {v1, v2}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 318
    :cond_2
    iget-object v1, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mBinderProxy:Landroid/os/IBinder;

    invoke-interface {v1}, Landroid/os/IBinder;->pingBinder()Z

    move-result v1

    if-nez v1, :cond_3

    .line 319
    iget-boolean v1, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v1, :cond_0

    .line 320
    iget-object v1, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v2, "isBridgeServiceValid(): mBinderProxy.pingBinder() == false"

    invoke-static {v1, v2}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 322
    :cond_3
    iget-object v1, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mBinderProxy:Landroid/os/IBinder;

    invoke-interface {v1}, Landroid/os/IBinder;->isBinderAlive()Z

    move-result v1

    if-nez v1, :cond_4

    .line 323
    iget-boolean v1, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v1, :cond_0

    .line 324
    iget-object v1, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v2, "isBridgeServiceValid(): mBinderProxy.isBinderAlive() == false"

    invoke-static {v1, v2}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 327
    :cond_4
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 115
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 120
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 123
    invoke-virtual {p0}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mContext:Landroid/content/Context;

    .line 124
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mContext:Landroid/content/Context;

    const-string v1, "persona"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PersonaManager;

    iput-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mPm:Landroid/os/PersonaManager;

    .line 125
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mContext:Landroid/content/Context;

    const-string v1, "user"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    iput-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mUm:Landroid/os/UserManager;

    .line 127
    iget-boolean v0, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->PERFORMANCE_DEBUG:Z

    if-eqz v0, :cond_0

    .line 128
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v1, "onCreate() called"

    invoke-static {v0, v1}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    :cond_0
    iget-boolean v0, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v0, :cond_1

    .line 131
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v1, "DataSyncService onCreate()"

    invoke-static {v0, v1}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    :cond_1
    iget-boolean v0, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->PERFORMANCE_DEBUG:Z

    if-eqz v0, :cond_2

    .line 134
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v1, "onCreate() completed"

    invoke-static {v0, v1}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    :cond_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mSetStartServiceCallers:Ljava/util/List;

    .line 138
    new-instance v0, Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;

    invoke-direct {v0, p0}, Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->contactsRcpDb:Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;

    .line 139
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->contactsRcpDb:Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;

    if-eqz v0, :cond_3

    .line 140
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->contactsRcpDb:Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;

    invoke-virtual {v0}, Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;->open()V

    .line 141
    :cond_3
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 363
    iget-boolean v0, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->PERFORMANCE_DEBUG:Z

    if-eqz v0, :cond_0

    .line 364
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v1, " ********onDestroy*********"

    invoke-static {v0, v1}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 365
    :cond_0
    iget-boolean v0, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v0, :cond_1

    .line 366
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v1, " ********onDestroy*********"

    invoke-static {v0, v1}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 367
    :cond_1
    iget-boolean v0, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v0, :cond_2

    .line 368
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v1, "onDestroy(). will unbind our SC"

    invoke-static {v0, v1}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 369
    :cond_2
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 371
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->contactsRcpDb:Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;

    if-eqz v0, :cond_3

    .line 372
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->contactsRcpDb:Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;

    invoke-virtual {v0}, Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;->close()V

    .line 373
    :cond_3
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 11
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    const/4 v10, 0x3

    const/4 v9, -0x1

    .line 145
    iget-boolean v6, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->PERFORMANCE_DEBUG:Z

    if-eqz v6, :cond_0

    .line 146
    iget-object v6, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v7, "onStartCommand() called"

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    :cond_0
    iget-boolean v6, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v6, :cond_1

    .line 148
    iget-object v6, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v7, "onStartCommand called"

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    :cond_1
    if-nez p1, :cond_4

    .line 152
    iget-boolean v6, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v6, :cond_2

    .line 153
    iget-object v6, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v7, "onStartCommand(): incoming intent is null."

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    :cond_2
    const-string v6, "<DUMMY>"

    invoke-direct {p0, v6}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->stopMySelf(Ljava/lang/String;)V

    .line 222
    :cond_3
    :goto_0
    return v10

    .line 158
    :cond_4
    const/4 v3, 0x0

    .line 159
    .local v3, "proxyMessenger":Landroid/os/Messenger;
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v6

    const-string v7, "binderBundle"

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 161
    .local v0, "bundle":Landroid/os/Bundle;
    if-eqz v0, :cond_5

    .line 162
    const-string v6, "proxy"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "proxyMessenger":Landroid/os/Messenger;
    check-cast v3, Landroid/os/Messenger;

    .line 165
    .restart local v3    # "proxyMessenger":Landroid/os/Messenger;
    :cond_5
    if-eqz v3, :cond_6

    .line 166
    invoke-virtual {v3}, Landroid/os/Messenger;->getBinder()Landroid/os/IBinder;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mBinderProxy:Landroid/os/IBinder;

    .line 167
    iget-object v6, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mBinderProxy:Landroid/os/IBinder;

    if-eqz v6, :cond_9

    .line 168
    iget-object v6, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mBinderProxy:Landroid/os/IBinder;

    invoke-static {v6}, Lcom/sec/knox/bridge/IBridgeService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/knox/bridge/IBridgeService;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mBridgeService:Lcom/sec/knox/bridge/IBridgeService;

    .line 176
    :cond_6
    :goto_1
    const-string v1, ""

    .line 178
    .local v1, "doWhat":Ljava/lang/String;
    const-string v6, "dowhat"

    invoke-virtual {p1, v6}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 179
    const-string v6, "dowhat"

    invoke-virtual {p1, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 182
    :cond_7
    iget-object v6, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "DataSyncService Contact onStartCommand() doWhat : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    const-string v6, "DELETE"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 185
    const-string v6, "personaid"

    invoke-virtual {p1, v6, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 186
    .local v2, "personaID":I
    iget-boolean v6, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v6, :cond_8

    .line 187
    iget-object v6, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v7, "DataSyncService Contact onStartCommand() DELETE"

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    :cond_8
    if-eq v2, v9, :cond_3

    .line 190
    const/4 v6, 0x0

    sput-boolean v6, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->isSyncState:Z

    .line 191
    const-string v6, "DELETE_PERSONA"

    invoke-direct {p0, v6}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->registerRequestStartService(Ljava/lang/String;)V

    .line 192
    new-instance v4, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$1;

    invoke-direct {v4, p0, v2}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$1;-><init>(Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;I)V

    .line 206
    .local v4, "r":Ljava/lang/Runnable;
    new-instance v5, Ljava/lang/Thread;

    invoke-direct {v5, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 207
    .local v5, "t":Ljava/lang/Thread;
    invoke-virtual {v5}, Ljava/lang/Thread;->start()V

    goto/16 :goto_0

    .line 170
    .end local v1    # "doWhat":Ljava/lang/String;
    .end local v2    # "personaID":I
    .end local v4    # "r":Ljava/lang/Runnable;
    .end local v5    # "t":Ljava/lang/Thread;
    :cond_9
    iget-boolean v6, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v6, :cond_a

    .line 171
    iget-object v6, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v7, "onStartCommand(): mBinderProxy == null"

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    :cond_a
    const-string v6, "<DUMMY>"

    invoke-direct {p0, v6}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->stopMySelf(Ljava/lang/String;)V

    goto :goto_1

    .line 212
    .restart local v1    # "doWhat":Ljava/lang/String;
    :cond_b
    iget-object v6, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mSyncThread:Ljava/lang/Thread;

    if-eqz v6, :cond_c

    iget-object v6, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mSyncThread:Ljava/lang/Thread;

    invoke-virtual {v6}, Ljava/lang/Thread;->isAlive()Z

    move-result v6

    if-nez v6, :cond_d

    .line 213
    :cond_c
    invoke-direct {p0}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->startSyncingProcess()V

    goto/16 :goto_0

    .line 215
    :cond_d
    iget-boolean v6, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->KNOX_DEBUG:Z

    if-eqz v6, :cond_e

    .line 216
    iget-object v6, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v7, "onStartCommand(); mSyncThread == null || !mSyncThread.isAlive()... "

    invoke-static {v6, v7}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    :cond_e
    const-string v6, "<DUMMY>"

    invoke-direct {p0, v6}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->stopMySelf(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public updateContact(Landroid/content/CustomCursor;II)V
    .locals 20
    .param p1, "orig_raw_cur"    # Landroid/content/CustomCursor;
    .param p2, "userOrPersonaId"    # I
    .param p3, "contactId"    # I

    .prologue
    .line 1589
    sget-object v4, Lcom/samsung/knox/rcp/components/contacts/syncer/util/DataSyncConstants;->CON_RAW_CONTACTS_URI:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v4

    const-string v5, "caller_is_syncadapter"

    const-string v6, "true"

    invoke-virtual {v4, v5, v6}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v13

    .line 1592
    .local v13, "conUri":Landroid/net/Uri;
    new-instance v19, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;

    invoke-direct/range {v19 .. v19}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;-><init>()V

    .line 1593
    .local v19, "temp":Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;
    const-string v4, "_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/CustomCursor;->getInt(I)I

    move-result v4

    int-to-long v4, v4

    move/from16 v0, p3

    int-to-long v6, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5, v6, v7}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;->insertHashMap(JJ)V

    .line 1596
    new-instance v14, Landroid/content/ContentValues;

    invoke-direct {v14}, Landroid/content/ContentValues;-><init>()V

    .line 1597
    .local v14, "cv":Landroid/content/ContentValues;
    const-string v4, "version"

    const-string v5, "version"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/content/CustomCursor;->getInt(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v14, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1599
    const-string v4, "name_verified"

    const-string v5, "name_verified"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/content/CustomCursor;->getInt(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v14, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1600
    const-string v4, "starred"

    const-string v5, "starred"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/content/CustomCursor;->getInt(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v14, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1603
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "_id=?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    invoke-static/range {p3 .. p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v4, v13, v14, v5, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v18

    .line 1606
    .local v18, "result":I
    new-instance v14, Landroid/content/ContentValues;

    .end local v14    # "cv":Landroid/content/ContentValues;
    invoke-direct {v14}, Landroid/content/ContentValues;-><init>()V

    .line 1607
    .restart local v14    # "cv":Landroid/content/ContentValues;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->contactsRcpDb:Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;

    const-string v4, "raw_contacts_version"

    const-string v5, "version"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/content/CustomCursor;->getInt(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v14, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1609
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->contactsRcpDb:Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;

    const-string v4, "raw_name_verified"

    const-string v5, "name_verified"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/content/CustomCursor;->getInt(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v14, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1611
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->contactsRcpDb:Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;

    const-string v4, "_id_raw_contact_id_original"

    const-string v5, "contact_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/content/CustomCursor;->getLong(I)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v14, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1613
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->contactsRcpDb:Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;

    const-string v5, "_id_raw_container =?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    invoke-static/range {p3 .. p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v4, v14, v5, v6}, Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;->updateContact(Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v17

    .line 1617
    .local v17, "localResult":I
    const/4 v12, 0x0

    .line 1618
    .local v12, "c":Landroid/content/CustomCursor;
    const/4 v15, -0x1

    .line 1620
    .local v15, "delResult":I
    :try_start_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mBridgeService:Lcom/sec/knox/bridge/IBridgeService;

    const-string v5, "Contacts"

    const-string v6, "DATA_CONTACTS"

    sget-object v8, Lcom/samsung/knox/rcp/components/contacts/syncer/util/DataSyncConstants;->ORI_DATA_PROJECTION:[Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "raw_contact_id IN ("

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v9, "_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v9, ")"

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    const-string v11, "raw_contact_id ASC"

    move/from16 v7, p2

    invoke-interface/range {v4 .. v11}, Lcom/sec/knox/bridge/IBridgeService;->queryProvider(Ljava/lang/String;Ljava/lang/String;I[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/content/CustomCursor;

    move-result-object v12

    .line 1631
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Lcom/samsung/knox/rcp/components/contacts/syncer/util/DataSyncConstants;->CON_DATA_URI:Landroid/net/Uri;

    const-string v6, "raw_contact_id=?"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    invoke-static/range {p3 .. p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {v4, v5, v6, v7}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v15

    .line 1635
    move-object/from16 v0, p0

    move/from16 v1, p2

    move-object/from16 v2, v19

    invoke-direct {v0, v12, v1, v2}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->insertDatas(Landroid/content/CustomCursor;ILcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$RawContactInfo;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1641
    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->closeCursor(Landroid/database/Cursor;)V

    .line 1643
    :goto_0
    return-void

    .line 1636
    :catch_0
    move-exception v16

    .line 1637
    .local v16, "e":Ljava/lang/Exception;
    :try_start_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "update, local update, data del result : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v18

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v17

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1639
    invoke-virtual/range {v16 .. v16}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1641
    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->closeCursor(Landroid/database/Cursor;)V

    goto :goto_0

    .end local v16    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->closeCursor(Landroid/database/Cursor;)V

    throw v4
.end method
