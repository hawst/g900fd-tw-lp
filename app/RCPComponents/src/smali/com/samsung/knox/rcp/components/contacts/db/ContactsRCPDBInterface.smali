.class public Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;
.super Ljava/lang/Object;
.source "ContactsRCPDBInterface.java"


# instance fields
.field private database:Landroid/database/sqlite/SQLiteDatabase;

.field private dbHelper:Lcom/samsung/knox/rcp/components/contacts/db/DataBaseHelper;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    new-instance v0, Lcom/samsung/knox/rcp/components/contacts/db/DataBaseHelper;

    invoke-direct {v0, p1}, Lcom/samsung/knox/rcp/components/contacts/db/DataBaseHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;->dbHelper:Lcom/samsung/knox/rcp/components/contacts/db/DataBaseHelper;

    .line 33
    return-void
.end method


# virtual methods
.method public beginTransaction()V
    .locals 1

    .prologue
    .line 243
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;->database:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 245
    return-void
.end method

.method public close()V
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;->dbHelper:Lcom/samsung/knox/rcp/components/contacts/db/DataBaseHelper;

    invoke-virtual {v0}, Lcom/samsung/knox/rcp/components/contacts/db/DataBaseHelper;->close()V

    .line 45
    return-void
.end method

.method public deleteAllContacts()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 237
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;->database:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "RAW_CONTACTS_SYNC_TABLE"

    invoke-virtual {v0, v1, v2, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 239
    return-void
.end method

.method public deleteAllGroups()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 231
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;->database:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "CONTACTS_GROUP_SYNC_TABLE"

    invoke-virtual {v0, v1, v2, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 233
    return-void
.end method

.method public deleteContact(Ljava/lang/String;)I
    .locals 3
    .param p1, "whereClause"    # Ljava/lang/String;

    .prologue
    .line 217
    const-string v0, "DataBaseInterface"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " deleteContact  whereClause : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;->database:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "RAW_CONTACTS_SYNC_TABLE"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public deleteGroup(Ljava/lang/String;)I
    .locals 3
    .param p1, "whereClause"    # Ljava/lang/String;

    .prologue
    .line 90
    const-string v0, "DataBaseInterface"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " deleteGroup  whereClause : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;->database:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "CONTACTS_GROUP_SYNC_TABLE"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public endTransaction()V
    .locals 1

    .prologue
    .line 249
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;->database:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 251
    return-void
.end method

.method public getAllContacts(ILjava/lang/String;)Landroid/database/Cursor;
    .locals 9
    .param p1, "personaId"    # I
    .param p2, "sortBy"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 114
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    .line 116
    .local v8, "strPersonaId":Ljava/lang/String;
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;->database:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "RAW_CONTACTS_SYNC_TABLE"

    const-string v3, "raw_contacts_persona_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v8, v4, v5

    move-object v5, v2

    move-object v6, v2

    move-object v7, p2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public getAllGroups(I)Landroid/database/Cursor;
    .locals 9
    .param p1, "personaId"    # I

    .prologue
    const/4 v2, 0x0

    .line 49
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    .line 51
    .local v8, "strPersonaId":Ljava/lang/String;
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;->database:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "CONTACTS_GROUP_SYNC_TABLE"

    const-string v3, "groups_persona_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v8, v4, v5

    const-string v7, "_id_original ASC,groups_version ASC"

    move-object v5, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public getContactIdForOriRawConID(JI)J
    .locals 13
    .param p1, "conId"    # J
    .param p3, "personaId"    # I

    .prologue
    const-wide/16 v10, -0x1

    const/4 v2, 0x0

    .line 128
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;->database:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "RAW_CONTACTS_SYNC_TABLE"

    const-string v3, "_id_raw_contact_id_original=? AND raw_contacts_persona_id=?"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-static/range {p3 .. p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 133
    .local v8, "c":Landroid/database/Cursor;
    if-eqz v8, :cond_2

    .line 136
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 137
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 138
    const-string v0, "_id_raw_contact_id_container"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-gez v0, :cond_0

    .line 139
    const-string v0, "DataBaseInterface"

    const-string v1, "CoumnIndex is negative"

    invoke-static {v0, v1}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 145
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    move-wide v0, v10

    .line 150
    :goto_0
    return-wide v0

    .line 142
    :cond_0
    :try_start_1
    const-string v0, "_id_raw_contact_id_container"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v0

    .line 145
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_1
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_2
    move-wide v0, v10

    .line 150
    goto :goto_0

    .line 145
    :catchall_0
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public getDatabase()Landroid/database/sqlite/SQLiteDatabase;
    .locals 1

    .prologue
    .line 261
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;->database:Landroid/database/sqlite/SQLiteDatabase;

    return-object v0
.end method

.method public getGroupOriIdFromConId(Ljava/lang/String;)Ljava/lang/String;
    .locals 10
    .param p1, "_id"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 62
    const/4 v9, 0x0

    .line 64
    .local v9, "id":Ljava/lang/String;
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;->database:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "CONTACTS_GROUP_SYNC_TABLE"

    const-string v3, "_id_container=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 69
    .local v8, "c":Landroid/database/Cursor;
    if-eqz v8, :cond_0

    .line 71
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 73
    const-string v0, "_id_original"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 75
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 79
    :cond_0
    return-object v9
.end method

.method public insertContact(JJJJIII)J
    .locals 5
    .param p1, "raw_id_original"    # J
    .param p3, "raw_id_container"    # J
    .param p5, "raw_contact_id_original"    # J
    .param p7, "raw_contact_id_container"    # J
    .param p9, "raw_contact_version"    # I
    .param p10, "raw_name_verified"    # I
    .param p11, "personaId"    # I

    .prologue
    .line 190
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 192
    .local v0, "cv":Landroid/content/ContentValues;
    const-string v1, "_id_raw_contact_id_original"

    invoke-static {p5, p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 194
    const-string v1, "_id_raw_contact_id_container"

    invoke-static {p7, p8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 196
    const-string v1, "_id_raw_original"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 198
    const-string v1, "_id_raw_container"

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 200
    const-string v1, "raw_contacts_version"

    invoke-static {p9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 202
    const-string v1, "raw_name_verified"

    invoke-static {p10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 204
    const-string v1, "raw_contacts_persona_id"

    invoke-static/range {p11 .. p11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 206
    iget-object v1, p0, Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;->database:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "RAW_CONTACTS_SYNC_TABLE"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    return-wide v2
.end method

.method public insertGroup(JIJI)J
    .locals 4
    .param p1, "id_original"    # J
    .param p3, "version"    # I
    .param p4, "id_container"    # J
    .param p6, "personaId"    # I

    .prologue
    .line 98
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 100
    .local v0, "cv":Landroid/content/ContentValues;
    const-string v1, "_id_original"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 102
    const-string v1, "_id_container"

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 104
    const-string v1, "groups_version"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 106
    const-string v1, "groups_persona_id"

    invoke-static {p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 108
    iget-object v1, p0, Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;->database:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "CONTACTS_GROUP_SYNC_TABLE"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    return-wide v2
.end method

.method public open()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/database/SQLException;
        }
    .end annotation

    .prologue
    .line 37
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;->dbHelper:Lcom/samsung/knox/rcp/components/contacts/db/DataBaseHelper;

    invoke-virtual {v0}, Lcom/samsung/knox/rcp/components/contacts/db/DataBaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;->database:Landroid/database/sqlite/SQLiteDatabase;

    .line 39
    return-void
.end method

.method public setTransactionSuccessful()V
    .locals 1

    .prologue
    .line 255
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;->database:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 257
    return-void
.end method

.method public updateContact(Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 2
    .param p1, "cv"    # Landroid/content/ContentValues;
    .param p2, "whereClause"    # Ljava/lang/String;
    .param p3, "whereArgs"    # [Ljava/lang/String;

    .prologue
    .line 225
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/db/ContactsRCPDBInterface;->database:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "RAW_CONTACTS_SYNC_TABLE"

    invoke-virtual {v0, v1, p1, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    return v0
.end method
