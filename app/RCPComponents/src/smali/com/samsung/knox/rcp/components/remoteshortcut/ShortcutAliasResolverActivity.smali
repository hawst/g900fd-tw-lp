.class public Lcom/samsung/knox/rcp/components/remoteshortcut/ShortcutAliasResolverActivity;
.super Landroid/app/Activity;
.source "ShortcutAliasResolverActivity.java"


# instance fields
.field private TAG:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 16
    const-string v0, "ShortcutAliasResolverActivity"

    iput-object v0, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/ShortcutAliasResolverActivity;->TAG:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 11
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v9, 0x400

    .line 20
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 22
    const/4 v8, 0x1

    invoke-virtual {p0, v8}, Lcom/samsung/knox/rcp/components/remoteshortcut/ShortcutAliasResolverActivity;->requestWindowFeature(I)Z

    .line 23
    invoke-virtual {p0}, Lcom/samsung/knox/rcp/components/remoteshortcut/ShortcutAliasResolverActivity;->getWindow()Landroid/view/Window;

    move-result-object v8

    invoke-virtual {v8, v9, v9}, Landroid/view/Window;->setFlags(II)V

    .line 24
    invoke-virtual {p0}, Lcom/samsung/knox/rcp/components/remoteshortcut/ShortcutAliasResolverActivity;->getIntent()Landroid/content/Intent;

    move-result-object v7

    .line 25
    .local v7, "receivedIntent":Landroid/content/Intent;
    if-eqz v7, :cond_3

    invoke-virtual {v7}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v8

    if-eqz v8, :cond_3

    .line 27
    const/4 v0, 0x0

    .line 28
    .local v0, "activityInfo":Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 29
    .local v3, "launchShortcutIntent":Landroid/content/Intent;
    invoke-virtual {v7}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v2

    .line 30
    .local v2, "dummyAliasActivity":Ljava/lang/String;
    iget-object v8, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/ShortcutAliasResolverActivity;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "full dummy activity "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 31
    invoke-virtual {v7}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v8

    invoke-static {v8}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v4

    .line 32
    .local v4, "id":J
    iget-object v8, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/ShortcutAliasResolverActivity;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Id in the Activity "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 34
    if-eqz v2, :cond_0

    .line 35
    const-string v8, "."

    invoke-virtual {v2, v8}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v8

    add-int/lit8 v8, v8, 0x1

    invoke-virtual {v2, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 37
    iget-object v8, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/ShortcutAliasResolverActivity;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "dummy activity"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 38
    const-wide/16 v8, 0x0

    cmp-long v8, v4, v8

    if-nez v8, :cond_1

    .line 39
    invoke-virtual {p0}, Lcom/samsung/knox/rcp/components/remoteshortcut/ShortcutAliasResolverActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;->getInstance(Landroid/content/Context;)Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;

    move-result-object v8

    invoke-virtual {v8, v2}, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;->getRealActivity(Ljava/lang/String;)Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;

    move-result-object v0

    .line 46
    :cond_0
    :goto_0
    new-instance v1, Landroid/app/Command;

    invoke-direct {v1}, Landroid/app/Command;-><init>()V

    .line 47
    .local v1, "command":Landroid/app/Command;
    if-eqz v0, :cond_2

    .line 49
    iget-object v8, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/ShortcutAliasResolverActivity;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, " activityinfo personaID "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v0}, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;->getPersonaId()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " activityinfo packageName "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v0}, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;->getPackageName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " activityinfo classname "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v0}, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;->getClassName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 52
    invoke-virtual {v0}, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;->getPackageName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0}, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;->getClassName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v8, v9}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 54
    const/high16 v8, 0x10000000

    invoke-virtual {v3, v8}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 55
    const-string v8, "android.intent.action.MAIN"

    invoke-virtual {v3, v8}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 56
    const-string v8, "android.intent.category.LAUNCHER"

    invoke-virtual {v3, v8}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 58
    invoke-virtual {v0}, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;->getPersonaId()I

    move-result v8

    iput v8, v1, Landroid/app/Command;->personaId:I

    .line 59
    invoke-virtual {v0}, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;->getPackageName()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v1, Landroid/app/Command;->packageToLaunch:Ljava/lang/String;

    .line 60
    iput-object v3, v1, Landroid/app/Command;->intent:Landroid/content/Intent;

    .line 61
    const-string v8, "launchIntent"

    iput-object v8, v1, Landroid/app/Command;->type:Ljava/lang/String;

    .line 63
    invoke-virtual {p0}, Lcom/samsung/knox/rcp/components/remoteshortcut/ShortcutAliasResolverActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    const-string v9, "rcp"

    invoke-virtual {v8, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/os/RCPManager;

    .line 64
    .local v6, "mBridge":Landroid/os/RCPManager;
    invoke-virtual {v6, v1}, Landroid/os/RCPManager;->executeCommandForPersona(Landroid/app/Command;)V

    .line 65
    iget-object v8, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/ShortcutAliasResolverActivity;->TAG:Ljava/lang/String;

    const-string v9, " finishing target activity  "

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 66
    invoke-virtual {p0}, Lcom/samsung/knox/rcp/components/remoteshortcut/ShortcutAliasResolverActivity;->finish()V

    .line 77
    .end local v0    # "activityInfo":Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;
    .end local v1    # "command":Landroid/app/Command;
    .end local v2    # "dummyAliasActivity":Ljava/lang/String;
    .end local v3    # "launchShortcutIntent":Landroid/content/Intent;
    .end local v4    # "id":J
    .end local v6    # "mBridge":Landroid/os/RCPManager;
    :goto_1
    return-void

    .line 42
    .restart local v0    # "activityInfo":Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;
    .restart local v2    # "dummyAliasActivity":Ljava/lang/String;
    .restart local v3    # "launchShortcutIntent":Landroid/content/Intent;
    .restart local v4    # "id":J
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/knox/rcp/components/remoteshortcut/ShortcutAliasResolverActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Lcom/samsung/knox/rcp/components/remoteshortcut/SpecialActivityMap;->getInstance(Landroid/content/Context;)Lcom/samsung/knox/rcp/components/remoteshortcut/SpecialActivityMap;

    move-result-object v8

    invoke-virtual {v8, v4, v5}, Lcom/samsung/knox/rcp/components/remoteshortcut/SpecialActivityMap;->getRealActivity(J)Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;

    move-result-object v0

    goto/16 :goto_0

    .line 68
    .restart local v1    # "command":Landroid/app/Command;
    :cond_2
    iget-object v8, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/ShortcutAliasResolverActivity;->TAG:Ljava/lang/String;

    const-string v9, " activity info is nulll"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    invoke-virtual {p0}, Lcom/samsung/knox/rcp/components/remoteshortcut/ShortcutAliasResolverActivity;->finish()V

    goto :goto_1

    .line 73
    .end local v0    # "activityInfo":Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;
    .end local v1    # "command":Landroid/app/Command;
    .end local v2    # "dummyAliasActivity":Ljava/lang/String;
    .end local v3    # "launchShortcutIntent":Landroid/content/Intent;
    .end local v4    # "id":J
    :cond_3
    iget-object v8, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/ShortcutAliasResolverActivity;->TAG:Ljava/lang/String;

    const-string v9, " Intent is null "

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 75
    invoke-virtual {p0}, Lcom/samsung/knox/rcp/components/remoteshortcut/ShortcutAliasResolverActivity;->finish()V

    goto :goto_1
.end method
