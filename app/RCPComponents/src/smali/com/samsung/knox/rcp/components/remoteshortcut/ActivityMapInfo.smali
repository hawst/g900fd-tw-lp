.class public Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;
.super Ljava/lang/Object;
.source "ActivityMapInfo.java"


# instance fields
.field private mClassName:Ljava/lang/String;

.field private mLable:Ljava/lang/String;

.field private mPackageName:Ljava/lang/String;

.field private mPersonaId:I


# direct methods
.method constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "personaId"    # I
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "className"    # Ljava/lang/String;
    .param p4, "lable"    # Ljava/lang/String;

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput p1, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;->mPersonaId:I

    .line 41
    iput-object p2, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;->mPackageName:Ljava/lang/String;

    .line 42
    iput-object p3, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;->mClassName:Ljava/lang/String;

    .line 43
    iput-object p4, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;->mLable:Ljava/lang/String;

    .line 44
    return-void
.end method


# virtual methods
.method public getClassName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;->mClassName:Ljava/lang/String;

    return-object v0
.end method

.method public getLable()Ljava/lang/String;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;->mLable:Ljava/lang/String;

    return-object v0
.end method

.method public getPackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;->mPackageName:Ljava/lang/String;

    return-object v0
.end method

.method public getPersonaId()I
    .locals 1

    .prologue
    .line 21
    iget v0, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;->mPersonaId:I

    return v0
.end method

.method public setClassName(Ljava/lang/String;)V
    .locals 0
    .param p1, "mClassName"    # Ljava/lang/String;

    .prologue
    .line 36
    iput-object p1, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;->mClassName:Ljava/lang/String;

    .line 37
    return-void
.end method
