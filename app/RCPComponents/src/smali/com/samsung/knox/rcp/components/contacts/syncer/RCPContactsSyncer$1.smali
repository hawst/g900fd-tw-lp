.class Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$1;
.super Ljava/lang/Object;
.source "RCPContactsSyncer.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->onStartCommand(Landroid/content/Intent;II)I
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;

.field final synthetic val$personaID:I


# direct methods
.method constructor <init>(Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;I)V
    .locals 0

    .prologue
    .line 192
    iput-object p1, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$1;->this$0:Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;

    iput p2, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$1;->val$personaID:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 195
    :cond_0
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$1;->this$0:Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;

    # getter for: Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mSetStartServiceCallers:Ljava/util/List;
    invoke-static {v0}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->access$000(Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$1;->this$0:Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;

    # getter for: Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->mSetStartServiceCallers:Ljava/util/List;
    invoke-static {v0}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->access$000(Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;)Ljava/util/List;

    move-result-object v0

    const-string v1, "DO_SYNC"

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 197
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$1;->this$0:Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;

    iget v1, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$1;->val$personaID:I

    # invokes: Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->deletePersonaData(I)V
    invoke-static {v0, v1}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->access$100(Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;I)V

    .line 201
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$1;->this$0:Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;

    iget-object v0, v0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->TAG:Ljava/lang/String;

    const-string v1, "deletePersonaData() END"

    invoke-static {v0, v1}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer$1;->this$0:Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;

    const-string v1, "DELETE_PERSONA"

    # invokes: Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->stopMySelf(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;->access$200(Lcom/samsung/knox/rcp/components/contacts/syncer/RCPContactsSyncer;Ljava/lang/String;)V

    .line 203
    return-void
.end method
