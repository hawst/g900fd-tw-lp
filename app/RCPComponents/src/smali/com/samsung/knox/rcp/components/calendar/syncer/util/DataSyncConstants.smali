.class public Lcom/samsung/knox/rcp/components/calendar/syncer/util/DataSyncConstants;
.super Ljava/lang/Object;
.source "DataSyncConstants.java"


# static fields
.field public static final CON_CALENDAR_FOR_P_PROJECTION:[Ljava/lang/String;

.field public static final CON_CALENDAR_PROJECTION:[Ljava/lang/String;

.field public static final CON_CALENDAR_PROJECTION_LOCAL:[Ljava/lang/String;

.field public static CON_CALENDAR_URI:Landroid/net/Uri;

.field public static CON_EVENT_URI:Landroid/net/Uri;

.field public static CON_LOGS_URI:Landroid/net/Uri;

.field public static final ORI_CALENDAR_FOR_P_PROJECTION:[Ljava/lang/String;

.field public static final ORI_CALENDAR_PROJECTION:[Ljava/lang/String;

.field public static final ORI_CALENDAR_PROJECTION_LOCAL:[Ljava/lang/String;

.field public static final ORI_CALLLOG_PROJECTION:[Ljava/lang/String;

.field protected static PREF_NAME:Ljava/lang/String;

.field public static final TASK_ACCOUNT_URI:Landroid/net/Uri;

.field public static final TASK_PROJECTION:[Ljava/lang/String;

.field public static TASK_URI:Landroid/net/Uri;

.field static initCalendarFlag:Z

.field static initCalendarStartFlag:Z

.field static initCallLogFlag:Z

.field static initCallLogStartFlag:Z

.field static initContactsFlag:Z

.field static initContactsStartFlag:Z

.field static isRunningCalendarFlag:Z

.field static isRunningCallLogFlag:Z

.field static isRunningContactsFlag:Z

.field static isRunningServiceCheckFlag:Z

.field static isServiceActive:Z


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 36
    sput-boolean v3, Lcom/samsung/knox/rcp/components/calendar/syncer/util/DataSyncConstants;->initCallLogFlag:Z

    .line 38
    sput-boolean v3, Lcom/samsung/knox/rcp/components/calendar/syncer/util/DataSyncConstants;->initCalendarFlag:Z

    .line 40
    sput-boolean v3, Lcom/samsung/knox/rcp/components/calendar/syncer/util/DataSyncConstants;->initContactsFlag:Z

    .line 42
    sput-boolean v3, Lcom/samsung/knox/rcp/components/calendar/syncer/util/DataSyncConstants;->initCallLogStartFlag:Z

    .line 44
    sput-boolean v3, Lcom/samsung/knox/rcp/components/calendar/syncer/util/DataSyncConstants;->initCalendarStartFlag:Z

    .line 46
    sput-boolean v3, Lcom/samsung/knox/rcp/components/calendar/syncer/util/DataSyncConstants;->initContactsStartFlag:Z

    .line 50
    sput-boolean v3, Lcom/samsung/knox/rcp/components/calendar/syncer/util/DataSyncConstants;->isRunningCallLogFlag:Z

    .line 52
    sput-boolean v3, Lcom/samsung/knox/rcp/components/calendar/syncer/util/DataSyncConstants;->isRunningCalendarFlag:Z

    .line 54
    sput-boolean v3, Lcom/samsung/knox/rcp/components/calendar/syncer/util/DataSyncConstants;->isRunningContactsFlag:Z

    .line 56
    sput-boolean v3, Lcom/samsung/knox/rcp/components/calendar/syncer/util/DataSyncConstants;->isRunningServiceCheckFlag:Z

    .line 58
    sput-boolean v3, Lcom/samsung/knox/rcp/components/calendar/syncer/util/DataSyncConstants;->isServiceActive:Z

    .line 60
    const-string v0, "DB_BRIDGE_INIT_SYNC"

    sput-object v0, Lcom/samsung/knox/rcp/components/calendar/syncer/util/DataSyncConstants;->PREF_NAME:Ljava/lang/String;

    .line 62
    sget-object v0, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/samsung/knox/rcp/components/calendar/syncer/util/DataSyncConstants;->CON_CALENDAR_URI:Landroid/net/Uri;

    .line 64
    sget-object v0, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/samsung/knox/rcp/components/calendar/syncer/util/DataSyncConstants;->CON_EVENT_URI:Landroid/net/Uri;

    .line 66
    const-string v0, "content://sec_container_1.logs/historys"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/knox/rcp/components/calendar/syncer/util/DataSyncConstants;->CON_LOGS_URI:Landroid/net/Uri;

    .line 68
    const-string v0, "content://com.android.calendar/syncTasks"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/knox/rcp/components/calendar/syncer/util/DataSyncConstants;->TASK_URI:Landroid/net/Uri;

    .line 70
    const-string v0, "content://com.android.calendar/TasksAccounts"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/knox/rcp/components/calendar/syncer/util/DataSyncConstants;->TASK_ACCOUNT_URI:Landroid/net/Uri;

    .line 95
    const/16 v0, 0x1a

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "number"

    aput-object v1, v0, v4

    const-string v1, "date"

    aput-object v1, v0, v5

    const-string v1, "duration"

    aput-object v1, v0, v6

    const-string v1, "type"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "new"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "name"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "numbertype"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "numberlabel"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "countryiso"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "geocoded_location"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "lookup_uri"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "matched_number"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "normalized_number"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "photo_id"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "formatted_number"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "frequent"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "e164_number"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "cnap_name"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "cdnip_number"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "country_code"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "cityid"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "fname"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "lname"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "bname"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "contactid"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/knox/rcp/components/calendar/syncer/util/DataSyncConstants;->ORI_CALLLOG_PROJECTION:[Ljava/lang/String;

    .line 151
    const/16 v0, 0x15

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "title"

    aput-object v1, v0, v4

    const-string v1, "dtstart"

    aput-object v1, v0, v5

    const-string v1, "eventTimezone"

    aput-object v1, v0, v6

    const-string v1, "allDay"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "rrule"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "lastDate"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "duration"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "dtend"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "description"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "eventLocation"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "latitude"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "longitude"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "sticker_type"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "_sync_id"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "eventStatus"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "original_id"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "original_sync_id"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "originalInstanceTime"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "originalAllDay"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "selfAttendeeStatus"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/knox/rcp/components/calendar/syncer/util/DataSyncConstants;->ORI_CALENDAR_PROJECTION:[Ljava/lang/String;

    .line 194
    const/16 v0, 0x15

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "customAppPackage"

    aput-object v1, v0, v3

    const-string v1, "title"

    aput-object v1, v0, v4

    const-string v1, "dtstart"

    aput-object v1, v0, v5

    const-string v1, "eventTimezone"

    aput-object v1, v0, v6

    const-string v1, "allDay"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "rrule"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "lastDate"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "duration"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "dtend"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "description"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "eventLocation"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "latitude"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "longitude"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "sticker_type"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "_sync_id"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "eventStatus"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "original_sync_id"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "originalInstanceTime"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "originalAllDay"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "selfAttendeeStatus"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/knox/rcp/components/calendar/syncer/util/DataSyncConstants;->CON_CALENDAR_PROJECTION:[Ljava/lang/String;

    .line 232
    const/16 v0, 0x16

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "title"

    aput-object v1, v0, v4

    const-string v1, "dtstart"

    aput-object v1, v0, v5

    const-string v1, "eventTimezone"

    aput-object v1, v0, v6

    const-string v1, "allDay"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "rrule"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "lastDate"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "duration"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "dtend"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "description"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "eventLocation"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "latitude"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "longitude"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "sticker_type"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "_sync_id"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "eventStatus"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "original_id"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "original_sync_id"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "originalInstanceTime"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "originalAllDay"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "selfAttendeeStatus"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "setLunar"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/knox/rcp/components/calendar/syncer/util/DataSyncConstants;->ORI_CALENDAR_PROJECTION_LOCAL:[Ljava/lang/String;

    .line 257
    const/16 v0, 0x16

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "customAppPackage"

    aput-object v1, v0, v3

    const-string v1, "title"

    aput-object v1, v0, v4

    const-string v1, "dtstart"

    aput-object v1, v0, v5

    const-string v1, "eventTimezone"

    aput-object v1, v0, v6

    const-string v1, "allDay"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "rrule"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "lastDate"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "duration"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "dtend"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "description"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "eventLocation"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "latitude"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "longitude"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "sticker_type"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "_sync_id"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "eventStatus"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "original_sync_id"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "originalInstanceTime"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "originalAllDay"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "selfAttendeeStatus"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "setLunar"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/knox/rcp/components/calendar/syncer/util/DataSyncConstants;->CON_CALENDAR_PROJECTION_LOCAL:[Ljava/lang/String;

    .line 282
    const/16 v0, 0xe

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "title"

    aput-object v1, v0, v4

    const-string v1, "dtstart"

    aput-object v1, v0, v5

    const-string v1, "eventTimezone"

    aput-object v1, v0, v6

    const-string v1, "allDay"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "rrule"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "lastDate"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "duration"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "dtend"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "eventStatus"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "original_id"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "original_sync_id"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "originalInstanceTime"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "originalAllDay"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/knox/rcp/components/calendar/syncer/util/DataSyncConstants;->CON_CALENDAR_FOR_P_PROJECTION:[Ljava/lang/String;

    .line 314
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "customAppPackage"

    aput-object v1, v0, v3

    const-string v1, "title"

    aput-object v1, v0, v4

    const-string v1, "dtstart"

    aput-object v1, v0, v5

    const-string v1, "eventTimezone"

    aput-object v1, v0, v6

    const-string v1, "allDay"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "rrule"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "lastDate"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "duration"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "dtend"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "_id"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/knox/rcp/components/calendar/syncer/util/DataSyncConstants;->ORI_CALENDAR_FOR_P_PROJECTION:[Ljava/lang/String;

    .line 338
    const/16 v0, 0x2a

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "syncServerId"

    aput-object v1, v0, v4

    const-string v1, "displayName"

    aput-object v1, v0, v5

    const-string v1, "_sync_dirty"

    aput-object v1, v0, v6

    const-string v1, "clientId"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "sourceid"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "bodyType"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "body_size"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "body_truncated"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "due_date"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "utc_due_date"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "importance"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "date_completed"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "complete"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "recurrence_type"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "recurrence_start"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "recurrence_until"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "recurrence_occurrences"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "recurrence_interval"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "recurrence_day_of_month"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "recurrence_day_of_week"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "recurrence_week_of_month"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "recurrence_month_of_year"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "recurrence_regenerate"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "recurrence_dead_occur"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "sensitivity"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "start_date"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "utc_start_date"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "parentId"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "subject"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "body"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "category1"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "category2"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, "category3"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, "mailboxKey"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string v2, "accountKey"

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string v2, "accountName"

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string v2, "groupId"

    aput-object v2, v0, v1

    const/16 v1, 0x26

    const-string v2, "previousId"

    aput-object v2, v0, v1

    const/16 v1, 0x27

    const-string v2, "syncTime"

    aput-object v2, v0, v1

    const/16 v1, 0x28

    const-string v2, "deleted"

    aput-object v2, v0, v1

    const/16 v1, 0x29

    const-string v2, "task_order"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/knox/rcp/components/calendar/syncer/util/DataSyncConstants;->TASK_PROJECTION:[Ljava/lang/String;

    return-void
.end method
