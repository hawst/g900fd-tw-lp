.class public Lcom/samsung/knox/rcp/components/contacts/provider/RCPContactsProvider;
.super Landroid/app/IntentService;
.source "RCPContactsProvider.java"


# instance fields
.field private TAG:Ljava/lang/String;

.field private containerId:I

.field private projection:[Ljava/lang/String;

.field private providerName:Ljava/lang/String;

.field private resourceName:Ljava/lang/String;

.field private selection:Ljava/lang/String;

.field private selectionArgs:[Ljava/lang/String;

.field private sortOrder:Ljava/lang/String;

.field private syncOnlyFavoritesContactsPolicy:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 44
    const-string v0, "IntentAsyncService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 36
    const-string v0, "Contacts_IntentAsyncService"

    iput-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/provider/RCPContactsProvider;->TAG:Ljava/lang/String;

    .line 56
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/knox/rcp/components/contacts/provider/RCPContactsProvider;->syncOnlyFavoritesContactsPolicy:Z

    .line 48
    return-void
.end method

.method private getCallerInfoDetails(Landroid/os/Bundle;)Landroid/content/CustomCursor;
    .locals 11
    .param p1, "bundleParams"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x0

    .line 472
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/provider/RCPContactsProvider;->TAG:Ljava/lang/String;

    const-string v3, "getCallerInfoDetails():  called "

    invoke-static {v0, v3}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 474
    const-string v0, "contactRefUriAsString"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 476
    .local v6, "contactRefUriAsString":Ljava/lang/String;
    if-nez v6, :cond_0

    .line 478
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/provider/RCPContactsProvider;->TAG:Ljava/lang/String;

    const-string v3, "Rorre: getCallerInfoDetails():  contactRefUriAsString is null"

    invoke-static {v0, v3}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 527
    :goto_0
    return-object v2

    .line 484
    :cond_0
    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 486
    .local v1, "contactRefUri":Landroid/net/Uri;
    invoke-virtual {p0}, Lcom/samsung/knox/rcp/components/contacts/provider/RCPContactsProvider;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 488
    .local v9, "result":Landroid/database/Cursor;
    if-nez v9, :cond_1

    .line 490
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/provider/RCPContactsProvider;->TAG:Ljava/lang/String;

    const-string v3, "Warning: from getCallerInfoDetailsByPhoneNumber(): cursor == null  or empty :-("

    invoke-static {v0, v3}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 495
    :cond_1
    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_2

    .line 496
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/provider/RCPContactsProvider;->TAG:Ljava/lang/String;

    const-string v3, "Warning: from getCallerInfoDetailsByPhoneNumber(): cursor is empty"

    invoke-static {v0, v3}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 497
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 504
    :cond_2
    new-instance v10, Landroid/database/CursorWindow;

    const-string v0, "MyCursorWindow"

    invoke-direct {v10, v0}, Landroid/database/CursorWindow;-><init>(Ljava/lang/String;)V

    .line 506
    .local v10, "window":Landroid/database/CursorWindow;
    new-instance v7, Landroid/database/CrossProcessCursorWrapper;

    invoke-direct {v7, v9}, Landroid/database/CrossProcessCursorWrapper;-><init>(Landroid/database/Cursor;)V

    .line 508
    .local v7, "crossProcessCursor":Landroid/database/CrossProcessCursorWrapper;
    const/4 v0, 0x0

    invoke-virtual {v7, v0, v10}, Landroid/database/CrossProcessCursorWrapper;->fillWindow(ILandroid/database/CursorWindow;)V

    .line 510
    new-instance v8, Landroid/content/CustomCursor;

    invoke-direct {v8, v10}, Landroid/content/CustomCursor;-><init>(Landroid/database/CursorWindow;)V

    .line 512
    .local v8, "remoteCursor":Landroid/content/CustomCursor;
    invoke-interface {v9}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Landroid/content/CustomCursor;->setColumnNames([Ljava/lang/String;)V

    .line 519
    if-eqz v7, :cond_3

    .line 521
    invoke-virtual {v7}, Landroid/database/CrossProcessCursorWrapper;->close()V

    .line 525
    :cond_3
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/provider/RCPContactsProvider;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "remoteCursor has records count: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v8}, Landroid/content/CustomCursor;->getCount()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v8

    .line 527
    goto :goto_0
.end method


# virtual methods
.method getContacts()Landroid/content/CustomCursor;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 157
    iget-object v2, p0, Lcom/samsung/knox/rcp/components/contacts/provider/RCPContactsProvider;->providerName:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/knox/rcp/components/contacts/provider/RCPContactsProvider;->resourceName:Ljava/lang/String;

    if-nez v2, :cond_2

    .line 159
    :cond_0
    iget-object v2, p0, Lcom/samsung/knox/rcp/components/contacts/provider/RCPContactsProvider;->TAG:Ljava/lang/String;

    const-string v3, "getContacts. providerName or resourceNmae is null. Do nothing"

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 215
    :cond_1
    :goto_0
    return-object v0

    .line 165
    :cond_2
    const/4 v0, 0x0

    .line 167
    .local v0, "result":Landroid/content/CustomCursor;
    iget-object v2, p0, Lcom/samsung/knox/rcp/components/contacts/provider/RCPContactsProvider;->providerName:Ljava/lang/String;

    const-string v3, "Contacts"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 169
    iget-object v2, p0, Lcom/samsung/knox/rcp/components/contacts/provider/RCPContactsProvider;->resourceName:Ljava/lang/String;

    const-string v3, "RAW_CONTACTS"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 171
    iget-object v2, p0, Lcom/samsung/knox/rcp/components/contacts/provider/RCPContactsProvider;->projection:[Ljava/lang/String;

    iget-object v3, p0, Lcom/samsung/knox/rcp/components/contacts/provider/RCPContactsProvider;->selection:Ljava/lang/String;

    iget-object v4, p0, Lcom/samsung/knox/rcp/components/contacts/provider/RCPContactsProvider;->selectionArgs:[Ljava/lang/String;

    iget-object v5, p0, Lcom/samsung/knox/rcp/components/contacts/provider/RCPContactsProvider;->sortOrder:Ljava/lang/String;

    invoke-virtual {p0, v2, v3, v4, v5}, Lcom/samsung/knox/rcp/components/contacts/provider/RCPContactsProvider;->getRawContacts([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/content/CustomCursor;

    move-result-object v0

    .line 175
    if-eqz v0, :cond_5

    .line 177
    iget-object v1, p0, Lcom/samsung/knox/rcp/components/contacts/provider/RCPContactsProvider;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Count returned is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/content/CustomCursor;->getCount()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    invoke-virtual {v0}, Landroid/content/CustomCursor;->getCount()I

    goto :goto_0

    .line 185
    :cond_3
    iget-object v2, p0, Lcom/samsung/knox/rcp/components/contacts/provider/RCPContactsProvider;->resourceName:Ljava/lang/String;

    const-string v3, "GROUPS_CONTACTS"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 187
    iget-object v1, p0, Lcom/samsung/knox/rcp/components/contacts/provider/RCPContactsProvider;->projection:[Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/knox/rcp/components/contacts/provider/RCPContactsProvider;->selection:Ljava/lang/String;

    iget-object v3, p0, Lcom/samsung/knox/rcp/components/contacts/provider/RCPContactsProvider;->selectionArgs:[Ljava/lang/String;

    iget-object v4, p0, Lcom/samsung/knox/rcp/components/contacts/provider/RCPContactsProvider;->sortOrder:Ljava/lang/String;

    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/samsung/knox/rcp/components/contacts/provider/RCPContactsProvider;->getGroupsData([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/content/CustomCursor;

    move-result-object v0

    .line 191
    if-eqz v0, :cond_1

    .line 193
    iget-object v1, p0, Lcom/samsung/knox/rcp/components/contacts/provider/RCPContactsProvider;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Count for groups returned is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/content/CustomCursor;->getCount()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 197
    :cond_4
    iget-object v2, p0, Lcom/samsung/knox/rcp/components/contacts/provider/RCPContactsProvider;->resourceName:Ljava/lang/String;

    const-string v3, "DATA_CONTACTS"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 199
    iget-object v1, p0, Lcom/samsung/knox/rcp/components/contacts/provider/RCPContactsProvider;->TAG:Ljava/lang/String;

    const-string v2, "Request received for DataSyncConstants.CONTACTS_DATA_TABLE"

    invoke-static {v1, v2}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    iget-object v1, p0, Lcom/samsung/knox/rcp/components/contacts/provider/RCPContactsProvider;->projection:[Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/knox/rcp/components/contacts/provider/RCPContactsProvider;->selection:Ljava/lang/String;

    iget-object v3, p0, Lcom/samsung/knox/rcp/components/contacts/provider/RCPContactsProvider;->selectionArgs:[Ljava/lang/String;

    iget-object v4, p0, Lcom/samsung/knox/rcp/components/contacts/provider/RCPContactsProvider;->sortOrder:Ljava/lang/String;

    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/samsung/knox/rcp/components/contacts/provider/RCPContactsProvider;->getDataTableData([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/content/CustomCursor;

    move-result-object v0

    .line 205
    if-eqz v0, :cond_1

    .line 207
    iget-object v1, p0, Lcom/samsung/knox/rcp/components/contacts/provider/RCPContactsProvider;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Count for CONTACTS_DATA_TABLE returned is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/content/CustomCursor;->getCount()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_5
    move-object v0, v1

    .line 215
    goto/16 :goto_0
.end method

.method getContacts(Landroid/os/Bundle;)Landroid/content/CustomCursor;
    .locals 4
    .param p1, "bd"    # Landroid/os/Bundle;

    .prologue
    const/4 v1, 0x0

    .line 125
    const-string v2, "providerName"

    invoke-virtual {p1, v2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/knox/rcp/components/contacts/provider/RCPContactsProvider;->providerName:Ljava/lang/String;

    .line 127
    const-string v2, "resourceName"

    invoke-virtual {p1, v2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/knox/rcp/components/contacts/provider/RCPContactsProvider;->resourceName:Ljava/lang/String;

    .line 129
    const-string v2, "containerId"

    iget v3, p0, Lcom/samsung/knox/rcp/components/contacts/provider/RCPContactsProvider;->containerId:I

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/samsung/knox/rcp/components/contacts/provider/RCPContactsProvider;->containerId:I

    .line 131
    const-string v2, "projection"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/knox/rcp/components/contacts/provider/RCPContactsProvider;->projection:[Ljava/lang/String;

    .line 133
    const-string v2, "selection"

    invoke-virtual {p1, v2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/knox/rcp/components/contacts/provider/RCPContactsProvider;->selection:Ljava/lang/String;

    .line 135
    const-string v2, "selectionArgs"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/knox/rcp/components/contacts/provider/RCPContactsProvider;->selectionArgs:[Ljava/lang/String;

    .line 137
    const-string v2, "sortOrder"

    iget-object v3, p0, Lcom/samsung/knox/rcp/components/contacts/provider/RCPContactsProvider;->sortOrder:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/knox/rcp/components/contacts/provider/RCPContactsProvider;->sortOrder:Ljava/lang/String;

    .line 139
    const-string v2, "syncOnlyFavoritesContactsPolicy"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/samsung/knox/rcp/components/contacts/provider/RCPContactsProvider;->syncOnlyFavoritesContactsPolicy:Z

    .line 143
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/knox/rcp/components/contacts/provider/RCPContactsProvider;->getContacts()Landroid/content/CustomCursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 151
    :goto_0
    return-object v1

    .line 145
    :catch_0
    move-exception v0

    .line 147
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method getDataTableData([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/content/CustomCursor;
    .locals 10
    .param p1, "projection"    # [Ljava/lang/String;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;
    .param p4, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 223
    const/4 v8, 0x0

    .line 225
    .local v8, "result":Landroid/database/Cursor;
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/provider/RCPContactsProvider;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getDataTableData "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    invoke-virtual {p0}, Lcom/samsung/knox/rcp/components/contacts/provider/RCPContactsProvider;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 234
    if-eqz v8, :cond_2

    .line 244
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 248
    new-instance v9, Landroid/database/CursorWindow;

    const-string v0, "MyCursorWindow"

    invoke-direct {v9, v0}, Landroid/database/CursorWindow;-><init>(Ljava/lang/String;)V

    .line 250
    .local v9, "window":Landroid/database/CursorWindow;
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 252
    new-instance v6, Landroid/database/CrossProcessCursorWrapper;

    invoke-direct {v6, v8}, Landroid/database/CrossProcessCursorWrapper;-><init>(Landroid/database/Cursor;)V

    .line 254
    .local v6, "crossProcessCursor":Landroid/database/CrossProcessCursorWrapper;
    const/4 v0, 0x0

    invoke-virtual {v6, v0, v9}, Landroid/database/CrossProcessCursorWrapper;->fillWindow(ILandroid/database/CursorWindow;)V

    .line 256
    new-instance v7, Landroid/content/CustomCursor;

    invoke-direct {v7, v9}, Landroid/content/CustomCursor;-><init>(Landroid/database/CursorWindow;)V

    .line 258
    .local v7, "remoteCursor":Landroid/content/CustomCursor;
    invoke-interface {v8}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/content/CustomCursor;->setColumnNames([Ljava/lang/String;)V

    .line 265
    if-eqz v6, :cond_0

    .line 267
    invoke-virtual {v6}, Landroid/database/CrossProcessCursorWrapper;->close()V

    .line 271
    :cond_0
    if-eqz v7, :cond_1

    .line 273
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/provider/RCPContactsProvider;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getDataTableData returns value "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v7}, Landroid/content/CustomCursor;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    .end local v6    # "crossProcessCursor":Landroid/database/CrossProcessCursorWrapper;
    .end local v7    # "remoteCursor":Landroid/content/CustomCursor;
    .end local v9    # "window":Landroid/database/CursorWindow;
    :goto_0
    return-object v7

    .line 277
    .restart local v6    # "crossProcessCursor":Landroid/database/CrossProcessCursorWrapper;
    .restart local v7    # "remoteCursor":Landroid/content/CustomCursor;
    .restart local v9    # "window":Landroid/database/CursorWindow;
    :cond_1
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/provider/RCPContactsProvider;->TAG:Ljava/lang/String;

    const-string v1, "getDataTableData returns null "

    invoke-static {v0, v1}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 283
    .end local v6    # "crossProcessCursor":Landroid/database/CrossProcessCursorWrapper;
    .end local v7    # "remoteCursor":Landroid/content/CustomCursor;
    .end local v9    # "window":Landroid/database/CursorWindow;
    :cond_2
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/provider/RCPContactsProvider;->TAG:Ljava/lang/String;

    const-string v1, "getDataTableData returns null "

    invoke-static {v0, v1}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    const/4 v7, 0x0

    goto :goto_0
.end method

.method getGroupsData([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/content/CustomCursor;
    .locals 11
    .param p1, "projection"    # [Ljava/lang/String;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;
    .param p4, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 293
    const/4 v8, 0x0

    .line 295
    .local v8, "result":Landroid/database/Cursor;
    new-instance v9, Landroid/telephony/TelephonyManager;

    invoke-direct {v9, p0}, Landroid/telephony/TelephonyManager;-><init>(Landroid/content/Context;)V

    .line 297
    .local v9, "telephonyManager":Landroid/telephony/TelephonyManager;
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/provider/RCPContactsProvider;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getGroupsData (values from syncer)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 300
    const-string p2, "account_type<>? AND account_type NOT LIKE ? AND _id NOT IN (?,?,?)  AND deleted=?"

    .line 307
    const/4 v0, 0x6

    new-array p3, v0, [Ljava/lang/String;

    .end local p3    # "selectionArgs":[Ljava/lang/String;
    const/4 v0, 0x0

    const-string v1, "vnd.sec.contact.phone_personal"

    aput-object v1, p3, v0

    const/4 v0, 0x1

    const-string v1, "vnd.sec.contact.phone_knox%"

    aput-object v1, p3, v0

    const/4 v0, 0x2

    const-string v1, "1"

    aput-object v1, p3, v0

    const/4 v0, 0x3

    const-string v1, "2"

    aput-object v1, p3, v0

    const/4 v0, 0x4

    const-string v1, "3"

    aput-object v1, p3, v0

    const/4 v0, 0x5

    const-string v1, "0"

    aput-object v1, p3, v0

    .line 313
    .restart local p3    # "selectionArgs":[Ljava/lang/String;
    invoke-virtual {v9}, Landroid/telephony/TelephonyManager;->isVoiceCapable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 315
    const-string p2, "account_type<>? AND account_type NOT LIKE ? AND _id NOT IN (?,?,?,?)  AND deleted=?"

    .line 321
    const/4 v0, 0x7

    new-array p3, v0, [Ljava/lang/String;

    .end local p3    # "selectionArgs":[Ljava/lang/String;
    const/4 v0, 0x0

    const-string v1, "vnd.sec.contact.phone_personal"

    aput-object v1, p3, v0

    const/4 v0, 0x1

    const-string v1, "vnd.sec.contact.phone_knox%"

    aput-object v1, p3, v0

    const/4 v0, 0x2

    const-string v1, "1"

    aput-object v1, p3, v0

    const/4 v0, 0x3

    const-string v1, "2"

    aput-object v1, p3, v0

    const/4 v0, 0x4

    const-string v1, "3"

    aput-object v1, p3, v0

    const/4 v0, 0x5

    const-string v1, "4"

    aput-object v1, p3, v0

    const/4 v0, 0x6

    const-string v1, "0"

    aput-object v1, p3, v0

    .line 327
    .restart local p3    # "selectionArgs":[Ljava/lang/String;
    :cond_0
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/provider/RCPContactsProvider;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getGroupsData (values after adding filters)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 330
    invoke-virtual {p0}, Lcom/samsung/knox/rcp/components/contacts/provider/RCPContactsProvider;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/ContactsContract$Groups;->CONTENT_URI:Landroid/net/Uri;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 337
    if-eqz v8, :cond_3

    .line 341
    new-instance v10, Landroid/database/CursorWindow;

    const-string v0, "MyCursorWindow"

    invoke-direct {v10, v0}, Landroid/database/CursorWindow;-><init>(Ljava/lang/String;)V

    .line 343
    .local v10, "window":Landroid/database/CursorWindow;
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 345
    new-instance v6, Landroid/database/CrossProcessCursorWrapper;

    invoke-direct {v6, v8}, Landroid/database/CrossProcessCursorWrapper;-><init>(Landroid/database/Cursor;)V

    .line 347
    .local v6, "crossProcessCursor":Landroid/database/CrossProcessCursorWrapper;
    const/4 v0, 0x0

    invoke-virtual {v6, v0, v10}, Landroid/database/CrossProcessCursorWrapper;->fillWindow(ILandroid/database/CursorWindow;)V

    .line 349
    new-instance v7, Landroid/content/CustomCursor;

    invoke-direct {v7, v10}, Landroid/content/CustomCursor;-><init>(Landroid/database/CursorWindow;)V

    .line 351
    .local v7, "remoteCursor":Landroid/content/CustomCursor;
    invoke-virtual {v7, p1}, Landroid/content/CustomCursor;->setColumnNames([Ljava/lang/String;)V

    .line 353
    if-eqz v6, :cond_1

    .line 355
    invoke-virtual {v6}, Landroid/database/CrossProcessCursorWrapper;->close()V

    .line 359
    :cond_1
    if-eqz v7, :cond_2

    .line 361
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/provider/RCPContactsProvider;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getGroups returns value "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v7}, Landroid/content/CustomCursor;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 373
    .end local v6    # "crossProcessCursor":Landroid/database/CrossProcessCursorWrapper;
    .end local v7    # "remoteCursor":Landroid/content/CustomCursor;
    .end local v10    # "window":Landroid/database/CursorWindow;
    :goto_0
    return-object v7

    .line 365
    .restart local v6    # "crossProcessCursor":Landroid/database/CrossProcessCursorWrapper;
    .restart local v7    # "remoteCursor":Landroid/content/CustomCursor;
    .restart local v10    # "window":Landroid/database/CursorWindow;
    :cond_2
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/provider/RCPContactsProvider;->TAG:Ljava/lang/String;

    const-string v1, "getGroups returns null "

    invoke-static {v0, v1}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 371
    .end local v6    # "crossProcessCursor":Landroid/database/CrossProcessCursorWrapper;
    .end local v7    # "remoteCursor":Landroid/content/CustomCursor;
    .end local v10    # "window":Landroid/database/CursorWindow;
    :cond_3
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/provider/RCPContactsProvider;->TAG:Ljava/lang/String;

    const-string v1, "getGroups returns null "

    invoke-static {v0, v1}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 373
    const/4 v7, 0x0

    goto :goto_0
.end method

.method getRawContacts([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/content/CustomCursor;
    .locals 11
    .param p1, "projection"    # [Ljava/lang/String;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;
    .param p4, "sortOrder"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v10, 0x0

    .line 381
    const/4 v8, 0x0

    .line 383
    .local v8, "result":Landroid/database/Cursor;
    if-nez p2, :cond_1

    .line 385
    const-string p2, "account_type<>? AND account_type NOT LIKE ? AND deleted=?"

    .line 397
    :goto_0
    iget-boolean v0, p0, Lcom/samsung/knox/rcp/components/contacts/provider/RCPContactsProvider;->syncOnlyFavoritesContactsPolicy:Z

    if-eqz v0, :cond_2

    .line 399
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND starred=?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 401
    const/4 v0, 0x4

    new-array p3, v0, [Ljava/lang/String;

    .end local p3    # "selectionArgs":[Ljava/lang/String;
    const-string v0, "vnd.sec.contact.phone_personal"

    aput-object v0, p3, v10

    const-string v0, "vnd.sec.contact.phone_knox%"

    aput-object v0, p3, v2

    const-string v0, "0"

    aput-object v0, p3, v3

    const-string v0, "1"

    aput-object v0, p3, v4

    .line 419
    .restart local p3    # "selectionArgs":[Ljava/lang/String;
    :goto_1
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/provider/RCPContactsProvider;->TAG:Ljava/lang/String;

    const-string v1, " getRawContacts() START"

    invoke-static {v0, v1}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 421
    invoke-virtual {p0}, Lcom/samsung/knox/rcp/components/contacts/provider/RCPContactsProvider;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 427
    if-eqz v8, :cond_4

    .line 431
    new-instance v9, Landroid/database/CursorWindow;

    const-string v0, "MyCursorWindow"

    invoke-direct {v9, v0}, Landroid/database/CursorWindow;-><init>(Ljava/lang/String;)V

    .line 433
    .local v9, "window":Landroid/database/CursorWindow;
    new-instance v6, Landroid/database/CrossProcessCursorWrapper;

    invoke-direct {v6, v8}, Landroid/database/CrossProcessCursorWrapper;-><init>(Landroid/database/Cursor;)V

    .line 435
    .local v6, "crossProcessCursor":Landroid/database/CrossProcessCursorWrapper;
    invoke-virtual {v6, v10, v9}, Landroid/database/CrossProcessCursorWrapper;->fillWindow(ILandroid/database/CursorWindow;)V

    .line 437
    new-instance v7, Landroid/content/CustomCursor;

    invoke-direct {v7, v9}, Landroid/content/CustomCursor;-><init>(Landroid/database/CursorWindow;)V

    .line 439
    .local v7, "remoteCursor":Landroid/content/CustomCursor;
    invoke-virtual {v7, p1}, Landroid/content/CustomCursor;->setColumnNames([Ljava/lang/String;)V

    .line 446
    if-eqz v6, :cond_0

    .line 448
    invoke-virtual {v6}, Landroid/database/CrossProcessCursorWrapper;->close()V

    .line 452
    :cond_0
    if-eqz v7, :cond_3

    .line 454
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/provider/RCPContactsProvider;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getRawContacts returns value "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v7}, Landroid/content/CustomCursor;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 466
    .end local v6    # "crossProcessCursor":Landroid/database/CrossProcessCursorWrapper;
    .end local v7    # "remoteCursor":Landroid/content/CustomCursor;
    .end local v9    # "window":Landroid/database/CursorWindow;
    :goto_2
    return-object v7

    .line 390
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "account_type"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "<>? AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "account_type"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " NOT LIKE ? AND deleted=?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_0

    .line 407
    :cond_2
    new-array p3, v4, [Ljava/lang/String;

    .end local p3    # "selectionArgs":[Ljava/lang/String;
    const-string v0, "vnd.sec.contact.phone_personal"

    aput-object v0, p3, v10

    const-string v0, "vnd.sec.contact.phone_knox%"

    aput-object v0, p3, v2

    const-string v0, "0"

    aput-object v0, p3, v3

    .restart local p3    # "selectionArgs":[Ljava/lang/String;
    goto/16 :goto_1

    .line 458
    .restart local v6    # "crossProcessCursor":Landroid/database/CrossProcessCursorWrapper;
    .restart local v7    # "remoteCursor":Landroid/content/CustomCursor;
    .restart local v9    # "window":Landroid/database/CursorWindow;
    :cond_3
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/provider/RCPContactsProvider;->TAG:Ljava/lang/String;

    const-string v1, "getRawContacts returns null "

    invoke-static {v0, v1}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 464
    .end local v6    # "crossProcessCursor":Landroid/database/CrossProcessCursorWrapper;
    .end local v7    # "remoteCursor":Landroid/content/CustomCursor;
    .end local v9    # "window":Landroid/database/CursorWindow;
    :cond_4
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/contacts/provider/RCPContactsProvider;->TAG:Ljava/lang/String;

    const-string v1, "getRawContacts returns null "

    invoke-static {v0, v1}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 466
    const/4 v7, 0x0

    goto :goto_2
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 9
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 67
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 69
    .local v6, "result":Landroid/os/Bundle;
    const/4 v2, 0x0

    .line 73
    .local v2, "cursor":Landroid/content/CustomCursor;
    :try_start_0
    iget-object v7, p0, Lcom/samsung/knox/rcp/components/contacts/provider/RCPContactsProvider;->TAG:Ljava/lang/String;

    const-string v8, " IntentAsyncService onHandleIntent()"

    invoke-static {v7, v8}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v7

    const-string v8, "binderBundle"

    invoke-virtual {v7, v8}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Bundle;

    .line 77
    .local v1, "bd":Landroid/os/Bundle;
    const-string v7, "messenger"

    invoke-virtual {v1, v7}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/os/Messenger;

    .line 79
    .local v5, "msger":Landroid/os/Messenger;
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v4

    .line 81
    .local v4, "msg":Landroid/os/Message;
    const-string v7, "action"

    const/4 v8, 0x0

    invoke-virtual {v1, v7, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 83
    .local v0, "action":Ljava/lang/String;
    const-string v7, "CALLER_INFO"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 87
    const/4 v7, 0x2

    iput v7, v4, Landroid/os/Message;->what:I

    .line 89
    invoke-direct {p0, v1}, Lcom/samsung/knox/rcp/components/contacts/provider/RCPContactsProvider;->getCallerInfoDetails(Landroid/os/Bundle;)Landroid/content/CustomCursor;

    move-result-object v2

    .line 91
    const-string v7, "RESULT"

    invoke-virtual {v6, v7, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 105
    :goto_0
    iput-object v6, v4, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 107
    invoke-virtual {v5, v4}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 115
    .end local v0    # "action":Ljava/lang/String;
    .end local v1    # "bd":Landroid/os/Bundle;
    .end local v4    # "msg":Landroid/os/Message;
    .end local v5    # "msger":Landroid/os/Messenger;
    :goto_1
    if-eqz v2, :cond_0

    .line 117
    invoke-virtual {v2}, Landroid/content/CustomCursor;->close()V

    .line 119
    :cond_0
    iget-object v7, p0, Lcom/samsung/knox/rcp/components/contacts/provider/RCPContactsProvider;->TAG:Ljava/lang/String;

    const-string v8, " IntentAsyncService My Job is done"

    invoke-static {v7, v8}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    return-void

    .line 97
    .restart local v0    # "action":Ljava/lang/String;
    .restart local v1    # "bd":Landroid/os/Bundle;
    .restart local v4    # "msg":Landroid/os/Message;
    .restart local v5    # "msger":Landroid/os/Messenger;
    :cond_1
    const/4 v7, 0x1

    :try_start_1
    iput v7, v4, Landroid/os/Message;->what:I

    .line 99
    invoke-virtual {p0, v1}, Lcom/samsung/knox/rcp/components/contacts/provider/RCPContactsProvider;->getContacts(Landroid/os/Bundle;)Landroid/content/CustomCursor;

    move-result-object v2

    .line 101
    const-string v7, "RESULT"

    invoke-virtual {v6, v7, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 109
    .end local v0    # "action":Ljava/lang/String;
    .end local v1    # "bd":Landroid/os/Bundle;
    .end local v4    # "msg":Landroid/os/Message;
    .end local v5    # "msger":Landroid/os/Messenger;
    :catch_0
    move-exception v3

    .line 111
    .local v3, "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method
