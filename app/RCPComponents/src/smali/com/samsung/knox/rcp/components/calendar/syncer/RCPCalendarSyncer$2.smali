.class Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer$2;
.super Ljava/lang/Thread;
.source "RCPCalendarSyncer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->startSyncingProcess()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;


# direct methods
.method constructor <init>(Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;)V
    .locals 0

    .prologue
    .line 231
    iput-object p1, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer$2;->this$0:Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 233
    const/4 v0, 0x1

    # setter for: Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->isSyncState:Z
    invoke-static {v0}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->access$302(Z)Z

    .line 234
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer$2;->this$0:Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;

    # getter for: Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->KNOX_DEBUG:Z
    invoke-static {v0}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->access$400(Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 235
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer$2;->this$0:Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;

    iget-object v0, v0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->TAG:Ljava/lang/String;

    const-string v1, "Running sync thread..."

    invoke-static {v0, v1}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    :cond_0
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer$2;->this$0:Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;

    # invokes: Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->doSync()V
    invoke-static {v0}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->access$500(Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;)V

    .line 237
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer$2;->this$0:Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;

    const-string v1, "DO_SYNC"

    # invokes: Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->stopMySelf(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->access$200(Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;Ljava/lang/String;)V

    .line 238
    const/4 v0, 0x0

    # setter for: Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->isSyncState:Z
    invoke-static {v0}, Lcom/samsung/knox/rcp/components/calendar/syncer/RCPCalendarSyncer;->access$302(Z)Z

    .line 239
    return-void
.end method
