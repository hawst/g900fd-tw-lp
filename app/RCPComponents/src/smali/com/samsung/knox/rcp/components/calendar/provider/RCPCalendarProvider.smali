.class public Lcom/samsung/knox/rcp/components/calendar/provider/RCPCalendarProvider;
.super Landroid/app/IntentService;
.source "RCPCalendarProvider.java"


# instance fields
.field private KelvinTAG:Ljava/lang/String;

.field private final SANITIZED_DESCRIPTION:Ljava/lang/String;

.field private final SANITIZED_EVENT_LOCATION:Ljava/lang/String;

.field private final SANITIZED_TITLE:Ljava/lang/String;

.field private TAG:Ljava/lang/String;

.field private containerId:I

.field private projection:[Ljava/lang/String;

.field private providerName:Ljava/lang/String;

.field private resourceName:Ljava/lang/String;

.field private sanitizeData:Ljava/lang/String;

.field private selection:Ljava/lang/String;

.field private selectionArgs:[Ljava/lang/String;

.field private sortOrder:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 52
    const-string v0, "IntentCalendarAsyncService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 36
    const-string v0, "IntentCalendarAsyncService"

    iput-object v0, p0, Lcom/samsung/knox/rcp/components/calendar/provider/RCPCalendarProvider;->TAG:Ljava/lang/String;

    .line 38
    const-string v0, "Kelvin"

    iput-object v0, p0, Lcom/samsung/knox/rcp/components/calendar/provider/RCPCalendarProvider;->KelvinTAG:Ljava/lang/String;

    .line 40
    const-string v0, "-"

    iput-object v0, p0, Lcom/samsung/knox/rcp/components/calendar/provider/RCPCalendarProvider;->SANITIZED_TITLE:Ljava/lang/String;

    .line 42
    const-string v0, "-"

    iput-object v0, p0, Lcom/samsung/knox/rcp/components/calendar/provider/RCPCalendarProvider;->SANITIZED_EVENT_LOCATION:Ljava/lang/String;

    .line 44
    const-string v0, "-"

    iput-object v0, p0, Lcom/samsung/knox/rcp/components/calendar/provider/RCPCalendarProvider;->SANITIZED_DESCRIPTION:Ljava/lang/String;

    .line 56
    return-void
.end method

.method private get_exCalendarId()Ljava/lang/String;
    .locals 9

    .prologue
    .line 526
    const-string v7, ""

    .line 528
    .local v7, "calendar":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v0

    const-string v1, "CscFeature_Calendar_EnableLocalHolidayDisplay"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 532
    .local v6, "CSC":Ljava/lang/String;
    const/4 v8, 0x0

    .line 535
    .local v8, "mCursor":Landroid/database/Cursor;
    :try_start_0
    const-string v0, "JAPAN"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 539
    const-string v3, "name IS NULL OR name NOT IN(select name from Calendars where account_type is \'LOCAL\' AND name is \'legalHoliday\')"

    .line 540
    .local v3, "selection":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/knox/rcp/components/calendar/provider/RCPCalendarProvider;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "_id"

    aput-object v5, v2, v4

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 585
    .end local v3    # "selection":Ljava/lang/String;
    :cond_0
    :goto_0
    if-eqz v8, :cond_2

    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 587
    const-string v7, "AND calendar_id IN ("

    .line 588
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/calendar/provider/RCPCalendarProvider;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " id count fetched "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 590
    :cond_1
    invoke-interface {v8}, Landroid/database/Cursor;->isLast()Z

    move-result v0

    if-nez v0, :cond_8

    .line 591
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v1, 0x0

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 594
    :goto_1
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/calendar/provider/RCPCalendarProvider;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " calendar "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 595
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_1

    .line 597
    const-string v0, ")"

    invoke-virtual {v7, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 599
    :cond_2
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/calendar/provider/RCPCalendarProvider;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " final calendar "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 601
    if-eqz v8, :cond_3

    .line 602
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 604
    :cond_3
    return-object v7

    .line 545
    :cond_4
    :try_start_1
    const-string v0, "TAIWAN"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 549
    const-string v3, "name IS NULL OR name NOT IN(select name from Calendars where account_type is \'LOCAL\' AND name is \'legalHoliday\')"

    .line 550
    .restart local v3    # "selection":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/knox/rcp/components/calendar/provider/RCPCalendarProvider;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "_id"

    aput-object v5, v2, v4

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 555
    goto/16 :goto_0

    .end local v3    # "selection":Ljava/lang/String;
    :cond_5
    const-string v0, "HONGKONG"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 559
    const-string v3, "name IS NULL OR name NOT IN(select name from Calendars where account_type is \'LOCAL\' AND name is \'legalHoliday\')"

    .line 560
    .restart local v3    # "selection":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/knox/rcp/components/calendar/provider/RCPCalendarProvider;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "_id"

    aput-object v5, v2, v4

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 565
    goto/16 :goto_0

    .end local v3    # "selection":Ljava/lang/String;
    :cond_6
    const-string v0, "CHINA"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 569
    const-string v3, "name IS NULL OR name NOT IN(select name from Calendars where account_type is \'LOCAL\' AND name IN (\'legalHoliday\',\'24SolarTerms\'))"

    .line 570
    .restart local v3    # "selection":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/knox/rcp/components/calendar/provider/RCPCalendarProvider;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "_id"

    aput-object v5, v2, v4

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 575
    goto/16 :goto_0

    .end local v3    # "selection":Ljava/lang/String;
    :cond_7
    const-string v0, "KOREA"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 579
    const-string v3, "name IS NULL OR name NOT IN(select name from Calendars where account_type is \'LOCAL\' AND name IN (\'legalHoliday\',\'legalSubstHoliday\'))"

    .line 580
    .restart local v3    # "selection":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/knox/rcp/components/calendar/provider/RCPCalendarProvider;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "_id"

    aput-object v5, v2, v4

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    goto/16 :goto_0

    .line 593
    .end local v3    # "selection":Ljava/lang/String;
    :cond_8
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v7

    goto/16 :goto_1

    .line 601
    :catchall_0
    move-exception v0

    if-eqz v8, :cond_9

    .line 602
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_9
    throw v0
.end method


# virtual methods
.method getCalendars()Landroid/content/CustomCursor;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 160
    iget-object v2, p0, Lcom/samsung/knox/rcp/components/calendar/provider/RCPCalendarProvider;->providerName:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/knox/rcp/components/calendar/provider/RCPCalendarProvider;->resourceName:Ljava/lang/String;

    if-nez v2, :cond_2

    .line 162
    :cond_0
    iget-object v2, p0, Lcom/samsung/knox/rcp/components/calendar/provider/RCPCalendarProvider;->TAG:Ljava/lang/String;

    const-string v3, "getContacts. providerName or resourceNmae is null. Do nothing"

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 232
    :cond_1
    :goto_0
    return-object v0

    .line 168
    :cond_2
    const/4 v0, 0x0

    .line 170
    .local v0, "result":Landroid/content/CustomCursor;
    iget-object v2, p0, Lcom/samsung/knox/rcp/components/calendar/provider/RCPCalendarProvider;->KelvinTAG:Ljava/lang/String;

    const-string v3, "getCalendars() called"

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    iget-object v2, p0, Lcom/samsung/knox/rcp/components/calendar/provider/RCPCalendarProvider;->providerName:Ljava/lang/String;

    const-string v3, "Calendar"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 174
    iget-object v2, p0, Lcom/samsung/knox/rcp/components/calendar/provider/RCPCalendarProvider;->KelvinTAG:Ljava/lang/String;

    const-string v3, "Provider is a Calendar provider"

    invoke-static {v2, v3}, Lcom/samsung/knox/rcp/components/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    iget-object v2, p0, Lcom/samsung/knox/rcp/components/calendar/provider/RCPCalendarProvider;->resourceName:Ljava/lang/String;

    const-string v3, "CALENDAR_EVENTS"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 178
    iget-object v1, p0, Lcom/samsung/knox/rcp/components/calendar/provider/RCPCalendarProvider;->projection:[Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/knox/rcp/components/calendar/provider/RCPCalendarProvider;->selection:Ljava/lang/String;

    iget-object v3, p0, Lcom/samsung/knox/rcp/components/calendar/provider/RCPCalendarProvider;->selectionArgs:[Ljava/lang/String;

    iget-object v4, p0, Lcom/samsung/knox/rcp/components/calendar/provider/RCPCalendarProvider;->sortOrder:Ljava/lang/String;

    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/samsung/knox/rcp/components/calendar/provider/RCPCalendarProvider;->getEvents([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/content/CustomCursor;

    move-result-object v0

    .line 180
    if-eqz v0, :cond_1

    .line 182
    iget-object v1, p0, Lcom/samsung/knox/rcp/components/calendar/provider/RCPCalendarProvider;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Count returned is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/content/CustomCursor;->getCount()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 190
    :cond_3
    iget-object v2, p0, Lcom/samsung/knox/rcp/components/calendar/provider/RCPCalendarProvider;->resourceName:Ljava/lang/String;

    const-string v3, "CALENDAR_TASKS"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 192
    iget-object v1, p0, Lcom/samsung/knox/rcp/components/calendar/provider/RCPCalendarProvider;->projection:[Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/knox/rcp/components/calendar/provider/RCPCalendarProvider;->selection:Ljava/lang/String;

    iget-object v3, p0, Lcom/samsung/knox/rcp/components/calendar/provider/RCPCalendarProvider;->selectionArgs:[Ljava/lang/String;

    iget-object v4, p0, Lcom/samsung/knox/rcp/components/calendar/provider/RCPCalendarProvider;->sortOrder:Ljava/lang/String;

    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/samsung/knox/rcp/components/calendar/provider/RCPCalendarProvider;->getTasks([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/content/CustomCursor;

    move-result-object v0

    .line 194
    if-eqz v0, :cond_1

    .line 196
    iget-object v1, p0, Lcom/samsung/knox/rcp/components/calendar/provider/RCPCalendarProvider;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Count returned is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/content/CustomCursor;->getCount()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 204
    :cond_4
    iget-object v2, p0, Lcom/samsung/knox/rcp/components/calendar/provider/RCPCalendarProvider;->resourceName:Ljava/lang/String;

    const-string v3, "CALENDAR_ATTENDEES"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    :cond_5
    :goto_1
    move-object v0, v1

    .line 232
    goto/16 :goto_0

    .line 208
    :cond_6
    iget-object v2, p0, Lcom/samsung/knox/rcp/components/calendar/provider/RCPCalendarProvider;->resourceName:Ljava/lang/String;

    const-string v3, "CALENDAR_REMINDERS"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 212
    iget-object v2, p0, Lcom/samsung/knox/rcp/components/calendar/provider/RCPCalendarProvider;->resourceName:Ljava/lang/String;

    const-string v3, "CALENDAR_INSTANCES"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    goto :goto_1
.end method

.method getEvents([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/content/CustomCursor;
    .locals 14
    .param p1, "projection"    # [Ljava/lang/String;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;
    .param p4, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 239
    const/4 v12, 0x0

    .line 241
    .local v12, "result":Landroid/database/Cursor;
    iget-object v1, p0, Lcom/samsung/knox/rcp/components/calendar/provider/RCPCalendarProvider;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getEvents (values from syncer)"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p4

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "account_name NOT LIKE ? AND deleted=? "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/samsung/knox/rcp/components/calendar/provider/RCPCalendarProvider;->get_exCalendarId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 246
    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/String;

    move-object/from16 p3, v0

    .end local p3    # "selectionArgs":[Ljava/lang/String;
    const/4 v1, 0x0

    const-string v2, "calendar_personal%"

    aput-object v2, p3, v1

    const/4 v1, 0x1

    const-string v2, "0"

    aput-object v2, p3, v1

    .line 250
    .restart local p3    # "selectionArgs":[Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/knox/rcp/components/calendar/provider/RCPCalendarProvider;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getEvents (values after adding filters)"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p4

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 253
    invoke-virtual {p0}, Lcom/samsung/knox/rcp/components/calendar/provider/RCPCalendarProvider;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    move-object v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 258
    if-eqz v12, :cond_8

    .line 260
    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    .line 262
    const/4 v10, 0x0

    .local v10, "k":I
    :goto_0
    invoke-interface {v12}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-ge v10, v1, :cond_1

    .line 264
    invoke-interface {v12}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v1

    if-nez v1, :cond_0

    .line 271
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    .line 262
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 275
    :cond_0
    iget-object v1, p0, Lcom/samsung/knox/rcp/components/calendar/provider/RCPCalendarProvider;->TAG:Ljava/lang/String;

    const-string v2, "getEvents After Last"

    invoke-static {v1, v2}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    :cond_1
    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    .line 287
    new-instance v13, Landroid/database/CursorWindow;

    const-string v1, "MyCursorWindow"

    invoke-direct {v13, v1}, Landroid/database/CursorWindow;-><init>(Ljava/lang/String;)V

    .line 289
    .local v13, "window":Landroid/database/CursorWindow;
    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    .line 291
    new-instance v7, Landroid/database/CrossProcessCursorWrapper;

    invoke-direct {v7, v12}, Landroid/database/CrossProcessCursorWrapper;-><init>(Landroid/database/Cursor;)V

    .line 293
    .local v7, "crossProcessCursor":Landroid/database/CrossProcessCursorWrapper;
    const/4 v1, 0x0

    invoke-virtual {v7, v1, v13}, Landroid/database/CrossProcessCursorWrapper;->fillWindow(ILandroid/database/CursorWindow;)V

    .line 295
    iget-object v1, p0, Lcom/samsung/knox/rcp/components/calendar/provider/RCPCalendarProvider;->sanitizeData:Ljava/lang/String;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/samsung/knox/rcp/components/calendar/provider/RCPCalendarProvider;->sanitizeData:Ljava/lang/String;

    const-string v2, "true"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 297
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_1
    invoke-virtual {v13}, Landroid/database/CursorWindow;->getNumRows()I

    move-result v1

    if-ge v9, v1, :cond_3

    .line 309
    const-string v1, "description"

    invoke-interface {v12, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v13, v9, v1}, Landroid/database/CursorWindow;->getString(II)Ljava/lang/String;

    move-result-object v8

    .line 311
    .local v8, "description":Ljava/lang/String;
    if-eqz v8, :cond_2

    .line 312
    const-string v1, "-"

    const-string v2, "description"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v13, v1, v9, v2}, Landroid/database/CursorWindow;->putString(Ljava/lang/String;II)Z

    .line 297
    :cond_2
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 319
    .end local v8    # "description":Ljava/lang/String;
    .end local v9    # "i":I
    :cond_3
    new-instance v11, Landroid/content/CustomCursor;

    invoke-direct {v11, v13}, Landroid/content/CustomCursor;-><init>(Landroid/database/CursorWindow;)V

    .line 325
    .local v11, "remoteCursor":Landroid/content/CustomCursor;
    invoke-interface {v12}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v11, v1}, Landroid/content/CustomCursor;->setColumnNames([Ljava/lang/String;)V

    .line 327
    invoke-virtual {v11}, Landroid/content/CustomCursor;->moveToFirst()Z

    .line 329
    const/4 v10, 0x0

    :goto_2
    invoke-virtual {v11}, Landroid/content/CustomCursor;->getCount()I

    move-result v1

    if-ge v10, v1, :cond_5

    .line 331
    invoke-virtual {v11}, Landroid/content/CustomCursor;->isAfterLast()Z

    move-result v1

    if-nez v1, :cond_4

    .line 339
    invoke-virtual {v11}, Landroid/content/CustomCursor;->moveToNext()Z

    .line 329
    add-int/lit8 v10, v10, 0x1

    goto :goto_2

    .line 343
    :cond_4
    iget-object v1, p0, Lcom/samsung/knox/rcp/components/calendar/provider/RCPCalendarProvider;->TAG:Ljava/lang/String;

    const-string v2, "getEvents After Last"

    invoke-static {v1, v2}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 351
    :cond_5
    invoke-virtual {v11}, Landroid/content/CustomCursor;->moveToFirst()Z

    .line 358
    if-eqz v7, :cond_6

    .line 360
    invoke-virtual {v7}, Landroid/database/CrossProcessCursorWrapper;->close()V

    .line 364
    :cond_6
    if-eqz v11, :cond_7

    .line 366
    iget-object v1, p0, Lcom/samsung/knox/rcp/components/calendar/provider/RCPCalendarProvider;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getEvents returns value "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v11}, Landroid/content/CustomCursor;->getCount()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 380
    .end local v7    # "crossProcessCursor":Landroid/database/CrossProcessCursorWrapper;
    .end local v10    # "k":I
    .end local v11    # "remoteCursor":Landroid/content/CustomCursor;
    .end local v13    # "window":Landroid/database/CursorWindow;
    :goto_3
    return-object v11

    .line 370
    .restart local v7    # "crossProcessCursor":Landroid/database/CrossProcessCursorWrapper;
    .restart local v10    # "k":I
    .restart local v11    # "remoteCursor":Landroid/content/CustomCursor;
    .restart local v13    # "window":Landroid/database/CursorWindow;
    :cond_7
    iget-object v1, p0, Lcom/samsung/knox/rcp/components/calendar/provider/RCPCalendarProvider;->TAG:Ljava/lang/String;

    const-string v2, "getEvents returns null "

    invoke-static {v1, v2}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 378
    .end local v7    # "crossProcessCursor":Landroid/database/CrossProcessCursorWrapper;
    .end local v10    # "k":I
    .end local v11    # "remoteCursor":Landroid/content/CustomCursor;
    .end local v13    # "window":Landroid/database/CursorWindow;
    :cond_8
    iget-object v1, p0, Lcom/samsung/knox/rcp/components/calendar/provider/RCPCalendarProvider;->TAG:Ljava/lang/String;

    const-string v2, "getEvents returns null "

    invoke-static {v1, v2}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 380
    const/4 v11, 0x0

    goto :goto_3
.end method

.method getTasks([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/content/CustomCursor;
    .locals 14
    .param p1, "projection"    # [Ljava/lang/String;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;
    .param p4, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 387
    const/4 v12, 0x0

    .line 389
    .local v12, "result":Landroid/database/Cursor;
    iget-object v1, p0, Lcom/samsung/knox/rcp/components/calendar/provider/RCPCalendarProvider;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getTasks (values from syncer)"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p4

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 391
    const-string p2, "accountName NOT LIKE ? AND deleted=?"

    .line 393
    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/String;

    move-object/from16 p3, v0

    .end local p3    # "selectionArgs":[Ljava/lang/String;
    const/4 v1, 0x0

    const-string v2, "My task (%"

    aput-object v2, p3, v1

    const/4 v1, 0x1

    const-string v2, "0"

    aput-object v2, p3, v1

    .line 397
    .restart local p3    # "selectionArgs":[Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/knox/rcp/components/calendar/provider/RCPCalendarProvider;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getTasks (values after adding filters)"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p4

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 400
    invoke-virtual {p0}, Lcom/samsung/knox/rcp/components/calendar/provider/RCPCalendarProvider;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "content://com.android.calendar/syncTasks"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    move-object v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 405
    if-nez v12, :cond_0

    .line 407
    iget-object v1, p0, Lcom/samsung/knox/rcp/components/calendar/provider/RCPCalendarProvider;->TAG:Ljava/lang/String;

    const-string v2, "query result is null"

    invoke-static {v1, v2}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 411
    :cond_0
    if-eqz v12, :cond_9

    .line 413
    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    .line 415
    const/4 v10, 0x0

    .local v10, "k":I
    :goto_0
    invoke-interface {v12}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-ge v10, v1, :cond_2

    .line 417
    invoke-interface {v12}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v1

    if-nez v1, :cond_1

    .line 424
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    .line 415
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 428
    :cond_1
    iget-object v1, p0, Lcom/samsung/knox/rcp/components/calendar/provider/RCPCalendarProvider;->TAG:Ljava/lang/String;

    const-string v2, "getTasks After Last"

    invoke-static {v1, v2}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 436
    :cond_2
    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    .line 440
    new-instance v13, Landroid/database/CursorWindow;

    const-string v1, "MyCursorWindow"

    invoke-direct {v13, v1}, Landroid/database/CursorWindow;-><init>(Ljava/lang/String;)V

    .line 442
    .local v13, "window":Landroid/database/CursorWindow;
    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    .line 444
    new-instance v8, Landroid/database/CrossProcessCursorWrapper;

    invoke-direct {v8, v12}, Landroid/database/CrossProcessCursorWrapper;-><init>(Landroid/database/Cursor;)V

    .line 446
    .local v8, "crossProcessCursor":Landroid/database/CrossProcessCursorWrapper;
    const/4 v1, 0x0

    invoke-virtual {v8, v1, v13}, Landroid/database/CrossProcessCursorWrapper;->fillWindow(ILandroid/database/CursorWindow;)V

    .line 448
    iget-object v1, p0, Lcom/samsung/knox/rcp/components/calendar/provider/RCPCalendarProvider;->sanitizeData:Ljava/lang/String;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/samsung/knox/rcp/components/calendar/provider/RCPCalendarProvider;->sanitizeData:Ljava/lang/String;

    const-string v2, "true"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 450
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_1
    invoke-virtual {v13}, Landroid/database/CursorWindow;->getNumRows()I

    move-result v1

    if-ge v9, v1, :cond_4

    .line 455
    const-string v1, "body"

    invoke-interface {v12, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v13, v9, v1}, Landroid/database/CursorWindow;->getString(II)Ljava/lang/String;

    move-result-object v7

    .line 457
    .local v7, "body":Ljava/lang/String;
    if-eqz v7, :cond_3

    invoke-virtual {v7}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    .line 458
    const-string v1, "-"

    const-string v2, "body"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v13, v1, v9, v2}, Landroid/database/CursorWindow;->putString(Ljava/lang/String;II)Z

    .line 450
    :cond_3
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 465
    .end local v7    # "body":Ljava/lang/String;
    .end local v9    # "i":I
    :cond_4
    new-instance v11, Landroid/content/CustomCursor;

    invoke-direct {v11, v13}, Landroid/content/CustomCursor;-><init>(Landroid/database/CursorWindow;)V

    .line 467
    .local v11, "remoteCursor":Landroid/content/CustomCursor;
    invoke-interface {v12}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v11, v1}, Landroid/content/CustomCursor;->setColumnNames([Ljava/lang/String;)V

    .line 469
    invoke-virtual {v11}, Landroid/content/CustomCursor;->moveToFirst()Z

    .line 471
    const/4 v10, 0x0

    :goto_2
    invoke-virtual {v11}, Landroid/content/CustomCursor;->getCount()I

    move-result v1

    if-ge v10, v1, :cond_6

    .line 473
    invoke-virtual {v11}, Landroid/content/CustomCursor;->isAfterLast()Z

    move-result v1

    if-nez v1, :cond_5

    .line 479
    invoke-virtual {v11}, Landroid/content/CustomCursor;->moveToNext()Z

    .line 471
    add-int/lit8 v10, v10, 0x1

    goto :goto_2

    .line 483
    :cond_5
    iget-object v1, p0, Lcom/samsung/knox/rcp/components/calendar/provider/RCPCalendarProvider;->TAG:Ljava/lang/String;

    const-string v2, "getTasks After Last"

    invoke-static {v1, v2}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 491
    :cond_6
    invoke-virtual {v11}, Landroid/content/CustomCursor;->moveToFirst()Z

    .line 498
    if-eqz v8, :cond_7

    .line 500
    invoke-virtual {v8}, Landroid/database/CrossProcessCursorWrapper;->close()V

    .line 504
    :cond_7
    if-eqz v11, :cond_8

    .line 506
    iget-object v1, p0, Lcom/samsung/knox/rcp/components/calendar/provider/RCPCalendarProvider;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getTasks returns value "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v11}, Landroid/content/CustomCursor;->getCount()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 520
    .end local v8    # "crossProcessCursor":Landroid/database/CrossProcessCursorWrapper;
    .end local v10    # "k":I
    .end local v11    # "remoteCursor":Landroid/content/CustomCursor;
    .end local v13    # "window":Landroid/database/CursorWindow;
    :goto_3
    return-object v11

    .line 510
    .restart local v8    # "crossProcessCursor":Landroid/database/CrossProcessCursorWrapper;
    .restart local v10    # "k":I
    .restart local v11    # "remoteCursor":Landroid/content/CustomCursor;
    .restart local v13    # "window":Landroid/database/CursorWindow;
    :cond_8
    iget-object v1, p0, Lcom/samsung/knox/rcp/components/calendar/provider/RCPCalendarProvider;->TAG:Ljava/lang/String;

    const-string v2, "getTasks returns null "

    invoke-static {v1, v2}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 518
    .end local v8    # "crossProcessCursor":Landroid/database/CrossProcessCursorWrapper;
    .end local v10    # "k":I
    .end local v11    # "remoteCursor":Landroid/content/CustomCursor;
    .end local v13    # "window":Landroid/database/CursorWindow;
    :cond_9
    iget-object v1, p0, Lcom/samsung/knox/rcp/components/calendar/provider/RCPCalendarProvider;->TAG:Ljava/lang/String;

    const-string v2, "getTasks returns null "

    invoke-static {v1, v2}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 520
    const/4 v11, 0x0

    goto :goto_3
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 12
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 89
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    const-wide/16 v10, 0x3a98

    add-long v4, v8, v10

    .line 91
    .local v4, "endTime":J
    const/4 v1, 0x0

    .line 97
    .local v1, "cursor":Landroid/content/CustomCursor;
    :try_start_0
    iget-object v8, p0, Lcom/samsung/knox/rcp/components/calendar/provider/RCPCalendarProvider;->TAG:Ljava/lang/String;

    const-string v9, " IntentCalendarAsyncService onHandleIntent() - will get contacts"

    invoke-static {v8, v9}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v8

    const-string v9, "binderBundle"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 103
    .local v0, "bd":Landroid/os/Bundle;
    const-string v8, "messenger"

    invoke-virtual {v0, v8}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/os/Messenger;

    .line 105
    .local v6, "msger":Landroid/os/Messenger;
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v3

    .line 107
    .local v3, "msg":Landroid/os/Message;
    const/4 v8, 0x1

    iput v8, v3, Landroid/os/Message;->what:I

    .line 109
    const-string v8, "providerName"

    const/4 v9, 0x0

    invoke-virtual {v0, v8, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/samsung/knox/rcp/components/calendar/provider/RCPCalendarProvider;->providerName:Ljava/lang/String;

    .line 111
    const-string v8, "resourceName"

    const/4 v9, 0x0

    invoke-virtual {v0, v8, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/samsung/knox/rcp/components/calendar/provider/RCPCalendarProvider;->resourceName:Ljava/lang/String;

    .line 113
    const-string v8, "containerId"

    iget v9, p0, Lcom/samsung/knox/rcp/components/calendar/provider/RCPCalendarProvider;->containerId:I

    invoke-virtual {v0, v8, v9}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v8

    iput v8, p0, Lcom/samsung/knox/rcp/components/calendar/provider/RCPCalendarProvider;->containerId:I

    .line 115
    const-string v8, "projection"

    invoke-virtual {v0, v8}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/samsung/knox/rcp/components/calendar/provider/RCPCalendarProvider;->projection:[Ljava/lang/String;

    .line 117
    const-string v8, "selection"

    const/4 v9, 0x0

    invoke-virtual {v0, v8, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/samsung/knox/rcp/components/calendar/provider/RCPCalendarProvider;->selection:Ljava/lang/String;

    .line 119
    const-string v8, "selectionArgs"

    invoke-virtual {v0, v8}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/samsung/knox/rcp/components/calendar/provider/RCPCalendarProvider;->selectionArgs:[Ljava/lang/String;

    .line 121
    const-string v8, "sortOrder"

    iget-object v9, p0, Lcom/samsung/knox/rcp/components/calendar/provider/RCPCalendarProvider;->sortOrder:Ljava/lang/String;

    invoke-virtual {v0, v8, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/samsung/knox/rcp/components/calendar/provider/RCPCalendarProvider;->sortOrder:Ljava/lang/String;

    .line 123
    const-string v8, "sanitizeData"

    iget-object v9, p0, Lcom/samsung/knox/rcp/components/calendar/provider/RCPCalendarProvider;->sanitizeData:Ljava/lang/String;

    invoke-virtual {v0, v8, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/samsung/knox/rcp/components/calendar/provider/RCPCalendarProvider;->sanitizeData:Ljava/lang/String;

    .line 125
    iget-object v8, p0, Lcom/samsung/knox/rcp/components/calendar/provider/RCPCalendarProvider;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, " query("

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/samsung/knox/rcp/components/calendar/provider/RCPCalendarProvider;->providerName:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/samsung/knox/rcp/components/calendar/provider/RCPCalendarProvider;->resourceName:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/samsung/knox/rcp/components/calendar/provider/RCPCalendarProvider;->selection:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/samsung/knox/rcp/components/calendar/provider/RCPCalendarProvider;->sortOrder:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", sanitizeData = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/samsung/knox/rcp/components/calendar/provider/RCPCalendarProvider;->sanitizeData:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ")"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    .line 134
    .local v7, "result":Landroid/os/Bundle;
    invoke-virtual {p0}, Lcom/samsung/knox/rcp/components/calendar/provider/RCPCalendarProvider;->getCalendars()Landroid/content/CustomCursor;

    move-result-object v1

    .line 136
    const-string v8, "RESULT"

    invoke-virtual {v7, v8, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 138
    iput-object v7, v3, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 140
    invoke-virtual {v6, v3}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 148
    .end local v0    # "bd":Landroid/os/Bundle;
    .end local v3    # "msg":Landroid/os/Message;
    .end local v6    # "msger":Landroid/os/Messenger;
    .end local v7    # "result":Landroid/os/Bundle;
    :goto_0
    const-string v8, "IntentAsyncService "

    const-string v9, " IntentAsyncService My Job is done"

    invoke-static {v8, v9}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    if-eqz v1, :cond_0

    .line 152
    invoke-virtual {v1}, Landroid/content/CustomCursor;->close()V

    .line 156
    :cond_0
    return-void

    .line 144
    :catch_0
    move-exception v2

    .line 145
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
