.class public Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;
.super Landroid/app/IntentService;
.source "RCPRemoteShortcutProvider.java"


# static fields
.field private static ACTION_INSTALL_SHORTCUT:Ljava/lang/String;

.field private static ACTION_KNOX_INSTALL_SHORTCUT:Ljava/lang/String;

.field private static ACTION_KNOX_UNINSTALL_SHORTCUT:Ljava/lang/String;

.field private static ACTION_UNINSTALL_SHORTCUT:Ljava/lang/String;

.field private static FIRST_PERSONA_ACTIVITY:Ljava/lang/String;

.field private static SECOND_PERSONA_ACTIVITY:Ljava/lang/String;


# instance fields
.field private ACTION_SHORTCUTS_MIGRATION_DONE:Ljava/lang/String;

.field private ACTION_UPDATE_SHORTCUT_BADGECOUNT:Ljava/lang/String;

.field private CONTENT_URI:Landroid/net/Uri;

.field private final SPECIAL_ACTIVITY:Ljava/lang/String;

.field private TAG:Ljava/lang/String;

.field private preInstalledAppsMinusEmail:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const-string v0, "com.android.launcher.action.UNINSTALL_SHORTCUT"

    sput-object v0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->ACTION_UNINSTALL_SHORTCUT:Ljava/lang/String;

    .line 42
    const-string v0, "com.android.launcher.action.INSTALL_SHORTCUT"

    sput-object v0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->ACTION_INSTALL_SHORTCUT:Ljava/lang/String;

    .line 44
    const-string v0, "KnoxShortcutActivity"

    sput-object v0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->FIRST_PERSONA_ACTIVITY:Ljava/lang/String;

    .line 46
    const-string v0, "KnoxIIShortcutActivity"

    sput-object v0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->SECOND_PERSONA_ACTIVITY:Ljava/lang/String;

    .line 48
    const-string v0, "com.sec.knox.action.UNINSTALL_SHORTCUT"

    sput-object v0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->ACTION_KNOX_UNINSTALL_SHORTCUT:Ljava/lang/String;

    .line 50
    const-string v0, "com.sec.knox.action.INSTALL_SHORTCUT"

    sput-object v0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->ACTION_KNOX_INSTALL_SHORTCUT:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 60
    const-string v0, "RemoteShortcutService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 38
    const-string v0, "RemoteShortcutService"

    iput-object v0, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->TAG:Ljava/lang/String;

    .line 52
    const-string v0, "android.intent.action.BADGE_COUNT_UPDATE"

    iput-object v0, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->ACTION_UPDATE_SHORTCUT_BADGECOUNT:Ljava/lang/String;

    .line 53
    const-string v0, "com.sec.knox.action.SHORTCUT_MIGRATION_DONE"

    iput-object v0, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->ACTION_SHORTCUTS_MIGRATION_DONE:Ljava/lang/String;

    .line 54
    const-string v0, "content://"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->CONTENT_URI:Landroid/net/Uri;

    .line 55
    const-string v0, "SpecialActivity"

    iput-object v0, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->SPECIAL_ACTIVITY:Ljava/lang/String;

    .line 62
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->TAG:Ljava/lang/String;

    const-string v1, " RemoteShortcutService  constructor"

    invoke-static {v0, v1}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    return-void
.end method

.method private addBadgeToIcon(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    .locals 9
    .param p1, "intentBitmap"    # Landroid/graphics/Bitmap;
    .param p2, "personaId"    # I

    .prologue
    const/4 v8, 0x0

    .line 364
    iget-object v5, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->TAG:Ljava/lang/String;

    const-string v6, "addBadgeToIcon start"

    invoke-static {v5, v6}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 365
    invoke-virtual {p0}, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Landroid/os/UserManager;->get(Landroid/content/Context;)Landroid/os/UserManager;

    move-result-object v4

    .line 366
    .local v4, "usermgr":Landroid/os/UserManager;
    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-direct {v2, v5, p1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 368
    .local v2, "d":Landroid/graphics/drawable/Drawable;
    new-instance v5, Landroid/os/UserHandle;

    invoke-direct {v5, p2}, Landroid/os/UserHandle;-><init>(I)V

    invoke-virtual {v4, v2, v5}, Landroid/os/UserManager;->getBadgedIconForUser(Landroid/graphics/drawable/Drawable;Landroid/os/UserHandle;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 369
    .local v0, "badged":Landroid/graphics/drawable/Drawable;
    const/4 v3, 0x0

    .line 370
    .local v3, "iconBitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_0

    .line 372
    instance-of v5, v0, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v5, :cond_1

    .line 373
    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    .end local v0    # "badged":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v3

    .line 382
    :cond_0
    :goto_0
    iget-object v5, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->TAG:Ljava/lang/String;

    const-string v6, "addBadgeToIcon end"

    invoke-static {v5, v6}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 383
    return-object v3

    .line 375
    .restart local v0    # "badged":Landroid/graphics/drawable/Drawable;
    :cond_1
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v5

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v6

    sget-object v7, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v5, v6, v7}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 377
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 378
    .local v1, "canvas":Landroid/graphics/Canvas;
    invoke-virtual {v1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v5

    invoke-virtual {v1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v6

    invoke-virtual {v0, v8, v8, v5, v6}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 379
    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method private createApplicationShortcut(Ljava/lang/String;Landroid/graphics/Bitmap;Ljava/lang/String;IZ)V
    .locals 30
    .param p1, "packageLabel"    # Ljava/lang/String;
    .param p2, "shortcutIcon"    # Landroid/graphics/Bitmap;
    .param p3, "shortcutIntentUri"    # Ljava/lang/String;
    .param p4, "badgeCount"    # I
    .param p5, "isPreInstalledPackageMinusEmail"    # Z

    .prologue
    .line 162
    const/16 v22, 0x0

    .line 163
    .local v22, "personaId":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->TAG:Ljava/lang/String;

    move-object/from16 v27, v0

    const-string v28, "in create shortcut"

    invoke-static/range {v27 .. v28}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    const/16 v23, 0x0

    .local v23, "shortcutClassName":Ljava/lang/String;
    const/16 v26, 0x0

    .line 167
    .local v26, "shortcutPkgName":Ljava/lang/String;
    const/4 v7, 0x0

    .line 168
    .local v7, "aliasClassName":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->getPackageName()Ljava/lang/String;

    move-result-object v8

    .line 169
    .local v8, "aliasPackageName":Ljava/lang/String;
    new-instance v24, Landroid/content/Intent;

    invoke-direct/range {v24 .. v24}, Landroid/content/Intent;-><init>()V

    .line 170
    .local v24, "shortcutIntent":Landroid/content/Intent;
    new-instance v6, Landroid/content/Intent;

    invoke-direct {v6}, Landroid/content/Intent;-><init>()V

    .line 171
    .local v6, "addIntent":Landroid/content/Intent;
    const/4 v12, 0x0

    .line 173
    .local v12, "data":Landroid/net/Uri;
    const/16 v19, 0x0

    .line 175
    .local v19, "isDummyActivityExhausted":Z
    const/4 v10, 0x0

    .line 178
    .local v10, "bundle":Landroid/os/Bundle;
    const/16 v27, 0x0

    :try_start_0
    move-object/from16 v0, p3

    move/from16 v1, v27

    invoke-static {v0, v1}, Landroid/content/Intent;->parseUri(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v18

    .line 179
    .local v18, "intentForUri":Landroid/content/Intent;
    invoke-virtual/range {v18 .. v18}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v10

    .line 180
    const-string v27, "personaId"

    move-object/from16 v0, v27

    invoke-virtual {v10, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v22

    .line 181
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->TAG:Ljava/lang/String;

    move-object/from16 v27, v0

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "intentForUri : "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v27 .. v28}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->TAG:Ljava/lang/String;

    move-object/from16 v27, v0

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "bundle : "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v27 .. v28}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    const-string v27, "uri"

    move-object/from16 v0, v27

    invoke-virtual {v10, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    .line 185
    .local v25, "shortcutIntentUriString":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->TAG:Ljava/lang/String;

    move-object/from16 v27, v0

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "shortcutIntentUriString : "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v27 .. v28}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    const/16 v27, 0x0

    move-object/from16 v0, v25

    move/from16 v1, v27

    invoke-static {v0, v1}, Landroid/content/Intent;->parseUri(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v15

    .line 188
    .local v15, "intentForComponent":Landroid/content/Intent;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->TAG:Ljava/lang/String;

    move-object/from16 v27, v0

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "intentForComponent : "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v27 .. v28}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    invoke-virtual {v15}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v11

    .line 191
    .local v11, "cn":Landroid/content/ComponentName;
    if-nez v11, :cond_2

    .line 192
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->TAG:Ljava/lang/String;

    move-object/from16 v27, v0

    const-string v28, "ComponentName is null"

    invoke-static/range {v27 .. v28}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 203
    .end local v11    # "cn":Landroid/content/ComponentName;
    .end local v15    # "intentForComponent":Landroid/content/Intent;
    .end local v18    # "intentForUri":Landroid/content/Intent;
    .end local v25    # "shortcutIntentUriString":Ljava/lang/String;
    :goto_0
    if-eqz p5, :cond_3

    .line 204
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->getApplicationContext()Landroid/content/Context;

    move-result-object v27

    invoke-static/range {v27 .. v27}, Lcom/samsung/knox/rcp/components/remoteshortcut/SpecialActivityMap;->getInstance(Landroid/content/Context;)Lcom/samsung/knox/rcp/components/remoteshortcut/SpecialActivityMap;

    move-result-object v21

    .line 205
    .local v21, "mSpecialActivityMap":Lcom/samsung/knox/rcp/components/remoteshortcut/SpecialActivityMap;
    if-eqz v21, :cond_0

    .line 206
    move-object/from16 v0, v21

    move/from16 v1, v22

    move-object/from16 v2, v26

    move-object/from16 v3, v23

    move-object/from16 v4, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/samsung/knox/rcp/components/remoteshortcut/SpecialActivityMap;->setRealActivityName(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v16

    .line 208
    .local v16, "id":J
    const-string v7, "SpecialActivity"

    .line 210
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    move-wide/from16 v1, v16

    invoke-static {v0, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v12

    .line 211
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->TAG:Ljava/lang/String;

    move-object/from16 v27, v0

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "data added "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual {v12}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v27 .. v28}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    .end local v16    # "id":J
    .end local v21    # "mSpecialActivityMap":Lcom/samsung/knox/rcp/components/remoteshortcut/SpecialActivityMap;
    :cond_0
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->TAG:Ljava/lang/String;

    move-object/from16 v27, v0

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "alias className : "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v29, " package name "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v27 .. v28}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 239
    if-eqz v7, :cond_6

    .line 240
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->addBadgeToIcon(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object p2

    .line 241
    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v27

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, "."

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, v24

    move-object/from16 v1, v27

    invoke-virtual {v0, v8, v1}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 242
    const-string v27, "android.intent.action.MAIN"

    move-object/from16 v0, v24

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 243
    move-object/from16 v0, v24

    invoke-virtual {v0, v12}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 245
    const-string v27, "android.intent.extra.shortcut.NAME"

    move-object/from16 v0, v27

    move-object/from16 v1, p1

    invoke-virtual {v6, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 246
    const-string v27, "android.intent.extra.shortcut.ICON"

    move-object/from16 v0, v27

    move-object/from16 v1, p2

    invoke-virtual {v6, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 247
    sget-object v27, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->ACTION_INSTALL_SHORTCUT:Ljava/lang/String;

    move-object/from16 v0, v27

    invoke-virtual {v6, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 250
    :try_start_1
    const-string v27, "android.intent.extra.shortcut.INTENT"

    const/16 v28, 0x0

    move-object/from16 v0, v24

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v28

    const/16 v29, 0x0

    invoke-static/range {v28 .. v29}, Landroid/content/Intent;->parseUri(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v28

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    invoke-virtual {v6, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;
    :try_end_1
    .catch Ljava/net/URISyntaxException; {:try_start_1 .. :try_end_1} :catch_1

    .line 259
    :goto_2
    if-nez p5, :cond_1

    if-eqz v19, :cond_5

    .line 260
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->TAG:Ljava/lang/String;

    move-object/from16 v27, v0

    const-string v28, " package needs special activity since there is no badge"

    invoke-static/range {v27 .. v28}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 261
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->TAG:Ljava/lang/String;

    move-object/from16 v27, v0

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, " isDummyActivityExhausted "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v27 .. v28}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->TAG:Ljava/lang/String;

    move-object/from16 v27, v0

    const-string v28, "sending broascast to install shortcut only"

    invoke-static/range {v27 .. v28}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 265
    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->sendBroadcast(Landroid/content/Intent;)V

    .line 285
    :goto_3
    return-void

    .line 195
    .restart local v11    # "cn":Landroid/content/ComponentName;
    .restart local v15    # "intentForComponent":Landroid/content/Intent;
    .restart local v18    # "intentForUri":Landroid/content/Intent;
    .restart local v25    # "shortcutIntentUriString":Ljava/lang/String;
    :cond_2
    :try_start_2
    invoke-virtual {v11}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v23

    .line 196
    invoke-virtual {v11}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v26

    .line 197
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->TAG:Ljava/lang/String;

    move-object/from16 v27, v0

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "className : "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v27 .. v28}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_0

    .line 199
    .end local v11    # "cn":Landroid/content/ComponentName;
    .end local v15    # "intentForComponent":Landroid/content/Intent;
    .end local v18    # "intentForUri":Landroid/content/Intent;
    .end local v25    # "shortcutIntentUriString":Ljava/lang/String;
    :catch_0
    move-exception v13

    .line 200
    .local v13, "e":Ljava/lang/Exception;
    invoke-virtual {v13}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 214
    .end local v13    # "e":Ljava/lang/Exception;
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->getApplicationContext()Landroid/content/Context;

    move-result-object v27

    invoke-static/range {v27 .. v27}, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;->getInstance(Landroid/content/Context;)Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;

    move-result-object v20

    .line 215
    .local v20, "mActivityMap":Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;
    if-eqz v20, :cond_4

    .line 216
    move-object/from16 v0, v20

    move/from16 v1, v22

    move-object/from16 v2, v26

    move-object/from16 v3, v23

    move-object/from16 v4, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;->setRealActivityName(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 218
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v27, v0

    const-wide/16 v28, 0x0

    invoke-static/range {v27 .. v29}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v12

    .line 219
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->TAG:Ljava/lang/String;

    move-object/from16 v27, v0

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "data added "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual {v12}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v27 .. v28}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    :cond_4
    if-nez v7, :cond_0

    .line 225
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->TAG:Ljava/lang/String;

    move-object/from16 v27, v0

    const-string v28, " dummy activities exhausted"

    invoke-static/range {v27 .. v28}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    const/16 v19, 0x1

    .line 227
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->getApplicationContext()Landroid/content/Context;

    move-result-object v27

    invoke-static/range {v27 .. v27}, Lcom/samsung/knox/rcp/components/remoteshortcut/SpecialActivityMap;->getInstance(Landroid/content/Context;)Lcom/samsung/knox/rcp/components/remoteshortcut/SpecialActivityMap;

    move-result-object v21

    .line 228
    .restart local v21    # "mSpecialActivityMap":Lcom/samsung/knox/rcp/components/remoteshortcut/SpecialActivityMap;
    if-eqz v21, :cond_0

    .line 229
    move-object/from16 v0, v21

    move/from16 v1, v22

    move-object/from16 v2, v26

    move-object/from16 v3, v23

    move-object/from16 v4, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/samsung/knox/rcp/components/remoteshortcut/SpecialActivityMap;->setRealActivityName(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v16

    .line 231
    .restart local v16    # "id":J
    const-string v7, "SpecialActivity"

    .line 232
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    move-wide/from16 v1, v16

    invoke-static {v0, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v12

    .line 233
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->TAG:Ljava/lang/String;

    move-object/from16 v27, v0

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "data added because dummy exhausted "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual {v12}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v27 .. v28}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 252
    .end local v16    # "id":J
    .end local v20    # "mActivityMap":Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;
    .end local v21    # "mSpecialActivityMap":Lcom/samsung/knox/rcp/components/remoteshortcut/SpecialActivityMap;
    :catch_1
    move-exception v14

    .line 253
    .local v14, "e1":Ljava/net/URISyntaxException;
    invoke-virtual {v14}, Ljava/net/URISyntaxException;->printStackTrace()V

    goto/16 :goto_2

    .line 267
    .end local v14    # "e1":Ljava/net/URISyntaxException;
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->TAG:Ljava/lang/String;

    move-object/from16 v27, v0

    const-string v28, " package needs normal alias activity "

    invoke-static/range {v27 .. v28}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 269
    new-instance v9, Landroid/content/Intent;

    invoke-direct {v9}, Landroid/content/Intent;-><init>()V

    .line 270
    .local v9, "badgeShortcutIntent":Landroid/content/Intent;
    const-string v27, "badge_count"

    move-object/from16 v0, v27

    move/from16 v1, p4

    invoke-virtual {v9, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 271
    const-string v27, "badge_count_package_name"

    move-object/from16 v0, v27

    invoke-virtual {v9, v0, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 272
    const-string v27, "badge_count_class_name"

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v28

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v29, "."

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    invoke-virtual {v9, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 274
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->ACTION_UPDATE_SHORTCUT_BADGECOUNT:Ljava/lang/String;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-virtual {v9, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 276
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->TAG:Ljava/lang/String;

    move-object/from16 v27, v0

    const-string v28, "sending broascast"

    invoke-static/range {v27 .. v28}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->sendBroadcast(Landroid/content/Intent;)V

    .line 280
    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_3

    .line 283
    .end local v9    # "badgeShortcutIntent":Landroid/content/Intent;
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->TAG:Ljava/lang/String;

    move-object/from16 v27, v0

    const-string v28, "alias class name is null"

    invoke-static/range {v27 .. v28}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3
.end method

.method private fillPreInstalledAppsMap()V
    .locals 2

    .prologue
    .line 445
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->TAG:Ljava/lang/String;

    const-string v1, "fill preinstalled apps start"

    invoke-static {v0, v1}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 447
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.samsung.android.app.memo"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 449
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.infraware.polarisviewer5"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 451
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.android.browser.provider"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 453
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.google.android.marvin.talkback"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 455
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.google.android.gm"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 457
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.android.chrome"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 459
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.google.android.apps.maps"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 461
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.google.android.gms"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 463
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.google.android.gsf"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 465
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.google.android.setupwizard"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 467
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.google.android.gsf.login"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 469
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.google.android.feedback"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 471
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.google.android.partnersetup"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 473
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.android.vending"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 475
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.google.android.street"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 477
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.google.android.backuptransport"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 479
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.google.android.configupdater"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 481
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.google.android.syncadapters.contacts"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 483
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.google.android.syncadapters.calendar"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 485
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.google.android.tts"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 487
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.gd.mobicore.pa"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 489
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.baidu.map.location"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 491
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.amap.android.location"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 493
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.samsung.locationhistory"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 495
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.android.calendar"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 497
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.android.contacts"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 499
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.sec.android.app.camera"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 501
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.sec.android.widgetapp.SPlannerAppWidget"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 503
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.sec.android.widgetapp.digitalclock"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 505
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.sec.android.app.music"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 507
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.sec.android.app.soundalive"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 509
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.sec.android.app.myfiles"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 511
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.sec.android.app.samsungapps"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 513
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.osp.app.signin"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 515
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.sec.android.app.billing"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 517
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.sec.android.app.sbrowser"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 519
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.sec.sprextension"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 521
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.android.browser"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 523
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.sec.android.gallery3d"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 525
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.samsung.android.mdm"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 527
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.samsung.android.fingerprint.service"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 529
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.android.externalstorage"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 531
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.samsung.everglades.video"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 533
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.sec.android.app.videoplayer"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 535
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.sec.android.app.SamsungContentsAgent"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 537
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.samsung.android.app.pinboard"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 539
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.sec.enterprise.mdm.vpn"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 541
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.samsung.helphub"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 542
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->TAG:Ljava/lang/String;

    const-string v1, "fill preinstall apps end"

    invoke-static {v0, v1}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 543
    return-void
.end method

.method private getApplicationShortcutIcon(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 9
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 136
    invoke-virtual {p0}, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    .line 138
    .local v4, "pm":Landroid/content/pm/PackageManager;
    :try_start_0
    invoke-virtual {v4, p1}, Landroid/content/pm/PackageManager;->getApplicationIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 139
    .local v3, "iconDrawable":Landroid/graphics/drawable/Drawable;
    const/4 v2, 0x0

    .line 140
    .local v2, "iconBitmap":Landroid/graphics/Bitmap;
    if-eqz v3, :cond_0

    .line 142
    instance-of v5, v3, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v5, :cond_1

    .line 143
    check-cast v3, Landroid/graphics/drawable/BitmapDrawable;

    .end local v3    # "iconDrawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v3}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    .line 156
    .end local v2    # "iconBitmap":Landroid/graphics/Bitmap;
    :cond_0
    :goto_0
    return-object v2

    .line 145
    .restart local v2    # "iconBitmap":Landroid/graphics/Bitmap;
    .restart local v3    # "iconDrawable":Landroid/graphics/drawable/Drawable;
    :cond_1
    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v5

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v6

    sget-object v7, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v5, v6, v7}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 147
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 148
    .local v0, "canvas":Landroid/graphics/Canvas;
    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v0}, Landroid/graphics/Canvas;->getWidth()I

    move-result v7

    invoke-virtual {v0}, Landroid/graphics/Canvas;->getHeight()I

    move-result v8

    invoke-virtual {v3, v5, v6, v7, v8}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 149
    invoke-virtual {v3, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 153
    .end local v0    # "canvas":Landroid/graphics/Canvas;
    .end local v2    # "iconBitmap":Landroid/graphics/Bitmap;
    .end local v3    # "iconDrawable":Landroid/graphics/drawable/Drawable;
    :catch_0
    move-exception v1

    .line 154
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 156
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private moveShortcutsFromSetupWizardFolderToLauncher(Landroid/os/Bundle;)V
    .locals 24
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 289
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 295
    .local v18, "shortcutInfoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Bundle;>;"
    const/4 v7, 0x0

    .line 296
    .local v7, "isPreInstalledPackageMinusEmail":Z
    const/16 v19, 0x0

    .line 298
    .local v19, "shortcutIntent":Landroid/content/Intent;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;->getInstance(Landroid/content/Context;)Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;

    move-result-object v14

    .line 301
    .local v14, "mactivityMap":Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;
    const/4 v8, 0x0

    .line 304
    .local v8, "className":Ljava/lang/String;
    const-string v2, "shortcutInfo"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v18

    .line 305
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->TAG:Ljava/lang/String;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, " number if shortcuts to be moved "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-static {v2, v0}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 306
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_0
    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v11, v2, :cond_4

    .line 307
    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Landroid/os/Bundle;

    .line 308
    .local v17, "shortcutInfo":Landroid/os/Bundle;
    const-string v2, "shortcutName"

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 309
    .local v3, "packageLabel":Ljava/lang/String;
    const-string v2, "uri"

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 310
    .local v5, "shortcutIntentUri":Ljava/lang/String;
    const-string v2, "badgeCount"

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    .line 313
    .local v6, "badgeCount":I
    const/4 v2, 0x0

    :try_start_0
    invoke-static {v5, v2}, Landroid/content/Intent;->parseUri(Ljava/lang/String;I)Landroid/content/Intent;
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v19

    .line 317
    :goto_1
    invoke-virtual/range {v19 .. v19}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v12

    .line 318
    .local v12, "intentBundle":Landroid/os/Bundle;
    const-string v2, "package"

    invoke-virtual {v12, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 319
    .local v15, "packageName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->preInstalledAppsMinusEmail:Ljava/util/Set;

    if-eqz v2, :cond_1

    .line 320
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->preInstalledAppsMinusEmail:Ljava/util/Set;

    invoke-interface {v2, v15}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 321
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->TAG:Ljava/lang/String;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, " preinstalled Package name is "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-static {v2, v0}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 322
    const/4 v7, 0x1

    .line 328
    :goto_2
    const-string v2, "personaId"

    invoke-virtual {v12, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v16

    .line 329
    .local v16, "personaId":I
    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->getApplicationShortcutIcon(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v2

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v2, v1}, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->addBadgeToIcon(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 331
    .local v4, "shortcutIcon":Landroid/graphics/Bitmap;
    const-string v2, "uri"

    invoke-virtual {v12, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 332
    .local v20, "shortcutIntentUriString":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->TAG:Ljava/lang/String;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "shortcutIntentUriString : "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-static {v2, v0}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 334
    const/4 v13, 0x0

    .line 336
    .local v13, "intentForComponent":Landroid/content/Intent;
    const/4 v2, 0x0

    :try_start_1
    move-object/from16 v0, v20

    invoke-static {v0, v2}, Landroid/content/Intent;->parseUri(Ljava/lang/String;I)Landroid/content/Intent;
    :try_end_1
    .catch Ljava/net/URISyntaxException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v13

    .line 340
    :goto_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->TAG:Ljava/lang/String;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "intentForComponent : "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-static {v2, v0}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 341
    invoke-virtual {v13}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v9

    .line 342
    .local v9, "cn":Landroid/content/ComponentName;
    if-nez v9, :cond_2

    .line 343
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->TAG:Ljava/lang/String;

    const-string v22, "ComponentName is null"

    move-object/from16 v0, v22

    invoke-static {v2, v0}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 348
    :goto_4
    const-string v2, "personaId"

    invoke-virtual {v12, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v14, v2, v15, v8, v3}, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;->checkIfShortcutIsMade(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    move-object/from16 v2, p0

    .line 350
    invoke-direct/range {v2 .. v7}, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->createApplicationShortcut(Ljava/lang/String;Landroid/graphics/Bitmap;Ljava/lang/String;IZ)V

    .line 355
    :goto_5
    const/4 v7, 0x0

    .line 306
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_0

    .line 314
    .end local v4    # "shortcutIcon":Landroid/graphics/Bitmap;
    .end local v9    # "cn":Landroid/content/ComponentName;
    .end local v12    # "intentBundle":Landroid/os/Bundle;
    .end local v13    # "intentForComponent":Landroid/content/Intent;
    .end local v15    # "packageName":Ljava/lang/String;
    .end local v16    # "personaId":I
    .end local v20    # "shortcutIntentUriString":Ljava/lang/String;
    :catch_0
    move-exception v10

    .line 315
    .local v10, "e":Ljava/net/URISyntaxException;
    invoke-virtual {v10}, Ljava/net/URISyntaxException;->printStackTrace()V

    goto/16 :goto_1

    .line 324
    .end local v10    # "e":Ljava/net/URISyntaxException;
    .restart local v12    # "intentBundle":Landroid/os/Bundle;
    .restart local v15    # "packageName":Ljava/lang/String;
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->TAG:Ljava/lang/String;

    const-string v22, " set does not contain package"

    move-object/from16 v0, v22

    invoke-static {v2, v0}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 326
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->TAG:Ljava/lang/String;

    const-string v22, " set is null "

    move-object/from16 v0, v22

    invoke-static {v2, v0}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 337
    .restart local v4    # "shortcutIcon":Landroid/graphics/Bitmap;
    .restart local v13    # "intentForComponent":Landroid/content/Intent;
    .restart local v16    # "personaId":I
    .restart local v20    # "shortcutIntentUriString":Ljava/lang/String;
    :catch_1
    move-exception v10

    .line 338
    .restart local v10    # "e":Ljava/net/URISyntaxException;
    invoke-virtual {v10}, Ljava/net/URISyntaxException;->printStackTrace()V

    goto :goto_3

    .line 345
    .end local v10    # "e":Ljava/net/URISyntaxException;
    .restart local v9    # "cn":Landroid/content/ComponentName;
    :cond_2
    invoke-virtual {v9}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v8

    .line 346
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->TAG:Ljava/lang/String;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "className : "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-static {v2, v0}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 353
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->TAG:Ljava/lang/String;

    const-string v22, " SHORTCUT ALREADY MADE NO NEED TO MAKE AGAIN "

    move-object/from16 v0, v22

    invoke-static {v2, v0}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    .line 358
    .end local v3    # "packageLabel":Ljava/lang/String;
    .end local v4    # "shortcutIcon":Landroid/graphics/Bitmap;
    .end local v5    # "shortcutIntentUri":Ljava/lang/String;
    .end local v6    # "badgeCount":I
    .end local v9    # "cn":Landroid/content/ComponentName;
    .end local v12    # "intentBundle":Landroid/os/Bundle;
    .end local v13    # "intentForComponent":Landroid/content/Intent;
    .end local v15    # "packageName":Ljava/lang/String;
    .end local v16    # "personaId":I
    .end local v17    # "shortcutInfo":Landroid/os/Bundle;
    .end local v20    # "shortcutIntentUriString":Ljava/lang/String;
    :cond_4
    new-instance v21, Landroid/content/Intent;

    invoke-direct/range {v21 .. v21}, Landroid/content/Intent;-><init>()V

    .line 359
    .local v21, "shortcutMigrationDone":Landroid/content/Intent;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->ACTION_SHORTCUTS_MIGRATION_DONE:Ljava/lang/String;

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 360
    sget-object v2, Landroid/os/UserHandle;->OWNER:Landroid/os/UserHandle;

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-virtual {v0, v1, v2}, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 361
    return-void
.end method

.method private removeApplicationShortcut(ILjava/lang/String;Ljava/lang/String;)V
    .locals 12
    .param p1, "userId"    # I
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "action"    # Ljava/lang/String;

    .prologue
    .line 389
    iget-object v9, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->TAG:Ljava/lang/String;

    const-string v10, "On remove shortcut"

    invoke-static {v9, v10}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 390
    invoke-virtual {p0}, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 391
    .local v1, "aliasPackageName":Ljava/lang/String;
    const/4 v2, 0x0

    .line 393
    .local v2, "dummyActivty":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;>;"
    new-instance v7, Landroid/content/Intent;

    invoke-direct {v7}, Landroid/content/Intent;-><init>()V

    .line 394
    .local v7, "removeShortcutIntent":Landroid/content/Intent;
    new-instance v8, Landroid/content/Intent;

    invoke-direct {v8}, Landroid/content/Intent;-><init>()V

    .line 396
    .local v8, "shortcutIntent":Landroid/content/Intent;
    const-string v0, "Activity"

    .line 398
    .local v0, "aliasActivityPrefix":Ljava/lang/String;
    const-string v9, "remove"

    invoke-virtual {p3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 399
    iget-object v9, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "action is "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 400
    iget-object v9, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "package is "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 401
    invoke-virtual {p0}, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    invoke-static {v9}, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;->getInstance(Landroid/content/Context;)Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;

    move-result-object v9

    invoke-virtual {v9, p1, p2}, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;->releaseVirtualActivity(ILjava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 410
    :cond_0
    :goto_0
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 411
    .local v6, "noOfShortcutToRemove":I
    iget-object v9, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "number of shortcuts to removed "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 412
    iget-object v9, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "alias packagne name to be removed "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 414
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    if-ge v4, v6, :cond_3

    .line 415
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;

    invoke-virtual {v9}, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;->getClassName()Ljava/lang/String;

    move-result-object v5

    .line 416
    .local v5, "infoFromSharedPref":Ljava/lang/String;
    iget-object v9, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Value from shared Pref: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 417
    iget-object v10, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Label name "

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;

    invoke-virtual {v9}, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;->getLable()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v10, v9}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 419
    invoke-virtual {v5, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 420
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "."

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v1, v9}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 422
    iget-object v9, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->CONTENT_URI:Landroid/net/Uri;

    const-wide/16 v10, 0x0

    invoke-static {v9, v10, v11}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 430
    :goto_2
    const-string v9, "android.intent.action.MAIN"

    invoke-virtual {v8, v9}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 432
    :try_start_0
    const-string v9, "android.intent.extra.shortcut.INTENT"

    const/4 v10, 0x0

    invoke-virtual {v8, v10}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    invoke-static {v10, v11}, Landroid/content/Intent;->parseUri(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v10

    invoke-virtual {v7, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    .line 437
    :goto_3
    const-string v10, "android.intent.extra.shortcut.NAME"

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;

    invoke-virtual {v9}, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMapInfo;->getLable()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v10, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 439
    sget-object v9, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->ACTION_UNINSTALL_SHORTCUT:Ljava/lang/String;

    invoke-virtual {v7, v9}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 440
    invoke-virtual {p0, v7}, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->sendBroadcast(Landroid/content/Intent;)V

    .line 414
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_1

    .line 403
    .end local v4    # "i":I
    .end local v5    # "infoFromSharedPref":Ljava/lang/String;
    .end local v6    # "noOfShortcutToRemove":I
    :cond_1
    const-string v9, "removeAllForUser"

    invoke-virtual {p3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 404
    iget-object v9, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "action is "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 405
    iget-object v9, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "package is "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 406
    invoke-virtual {p0}, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    invoke-static {v9}, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;->getInstance(Landroid/content/Context;)Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;

    move-result-object v9

    invoke-virtual {v9, p1}, Lcom/samsung/knox/rcp/components/remoteshortcut/ActivityMap;->releaseVirtualActivity(I)Ljava/util/ArrayList;

    move-result-object v2

    goto/16 :goto_0

    .line 424
    .restart local v4    # "i":I
    .restart local v5    # "infoFromSharedPref":Ljava/lang/String;
    .restart local v6    # "noOfShortcutToRemove":I
    :cond_2
    iget-object v9, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->TAG:Ljava/lang/String;

    const-string v10, "Special activity shortcut being removed"

    invoke-static {v9, v10}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 425
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "."

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "SpecialActivity"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v1, v9}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 427
    iget-object v9, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v5}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    invoke-static {v9, v10, v11}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    goto/16 :goto_2

    .line 434
    :catch_0
    move-exception v3

    .line 435
    .local v3, "e1":Ljava/net/URISyntaxException;
    invoke-virtual {v3}, Ljava/net/URISyntaxException;->printStackTrace()V

    goto/16 :goto_3

    .line 442
    .end local v3    # "e1":Ljava/net/URISyntaxException;
    .end local v5    # "infoFromSharedPref":Ljava/lang/String;
    :cond_3
    return-void
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 9
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 78
    :try_start_0
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " RemoteShortcutService onHandleIntent(): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    if-eqz p1, :cond_1

    .line 82
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v6

    .line 84
    .local v6, "bundle":Landroid/os/Bundle;
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " RemoteShortcutService onHandleIntent() UserHandle.myUserId:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "packageName"

    const/4 v3, 0x0

    invoke-virtual {v6, v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "packageLabel"

    const/4 v3, 0x0

    invoke-virtual {v6, v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "createOrRemove"

    const/4 v3, 0x0

    invoke-virtual {v6, v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    const-string v0, "userId"

    const/4 v1, -0x1

    invoke-virtual {v6, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v8

    .line 95
    .local v8, "userId":I
    if-gez v8, :cond_0

    .line 133
    .end local v6    # "bundle":Landroid/os/Bundle;
    .end local v8    # "userId":I
    :goto_0
    return-void

    .line 99
    .restart local v6    # "bundle":Landroid/os/Bundle;
    .restart local v8    # "userId":I
    :cond_0
    const-string v0, "create"

    const-string v1, "createOrRemove"

    const/4 v2, 0x0

    invoke-virtual {v6, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 101
    const-string v0, "packageLabel"

    const/4 v1, 0x0

    invoke-virtual {v6, v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v0, "shortcutIcon"

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Bitmap;

    const-string v0, "shortcutIntentUri"

    const/4 v3, 0x0

    invoke-virtual {v6, v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v0, "counts"

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    const-string v0, "isPreInstalledPackage"

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->createApplicationShortcut(Ljava/lang/String;Landroid/graphics/Bitmap;Ljava/lang/String;IZ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 131
    .end local v6    # "bundle":Landroid/os/Bundle;
    .end local v8    # "userId":I
    :cond_1
    :goto_1
    const-string v0, "RemoteShortcutService "

    const-string v1, " RemoteShortcutService My Job is done"

    invoke-static {v0, v1}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 106
    .restart local v6    # "bundle":Landroid/os/Bundle;
    .restart local v8    # "userId":I
    :cond_2
    :try_start_1
    const-string v0, "remove"

    const-string v1, "createOrRemove"

    const/4 v2, 0x0

    invoke-virtual {v6, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 108
    const-string v0, "packageName"

    const/4 v1, 0x0

    invoke-virtual {v6, v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "remove"

    invoke-direct {p0, v8, v0, v1}, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->removeApplicationShortcut(ILjava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 125
    .end local v6    # "bundle":Landroid/os/Bundle;
    .end local v8    # "userId":I
    :catch_0
    move-exception v7

    .line 127
    .local v7, "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 111
    .end local v7    # "e":Ljava/lang/Exception;
    .restart local v6    # "bundle":Landroid/os/Bundle;
    .restart local v8    # "userId":I
    :cond_3
    :try_start_2
    const-string v0, "removeAllForUser"

    const-string v1, "createOrRemove"

    const/4 v2, 0x0

    invoke-virtual {v6, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 112
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "USER REMOVED "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    const/4 v0, 0x0

    const-string v1, "removeAllForUser"

    invoke-direct {p0, v8, v0, v1}, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->removeApplicationShortcut(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 116
    :cond_4
    const-string v0, "move"

    const-string v1, "movefromfolder"

    invoke-virtual {v6, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 117
    iget-object v0, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " userid obtained "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/knox/rcp/components/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->preInstalledAppsMinusEmail:Ljava/util/Set;

    .line 119
    invoke-direct {p0}, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->fillPreInstalledAppsMap()V

    .line 120
    invoke-direct {p0, v6}, Lcom/samsung/knox/rcp/components/remoteshortcut/RCPRemoteShortcutProvider;->moveShortcutsFromSetupWizardFolderToLauncher(Landroid/os/Bundle;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_1
.end method
