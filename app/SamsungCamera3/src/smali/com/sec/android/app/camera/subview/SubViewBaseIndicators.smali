.class public Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;
.super Lcom/sec/android/app/camera/subview/SubViewBase;
.source "SubViewBaseIndicators.java"


# static fields
.field private static final INDICATOR_ORDER:[I

.field private static final INDICATOR_REMAINCOUNT_INDEX:I = 0x2

.field private static final TAG:Ljava/lang/String; = "SubViewBaseInicators"

.field private static final VISIBLE_REMAIN_COUNT:I = 0x12c

.field private static mIndicatorMarginBottom:I

.field private static mIndicatorMarginLeft:I

.field private static mIndicatorSpace:I

.field private static mIndicatorWidth:I


# instance fields
.field private mAutoNightDetectionIndicator:Landroid/widget/ImageView;

.field private mBatteryIndicator:Landroid/widget/ImageView;

.field private mFlashIndicator:Landroid/widget/ImageView;

.field private mGPSIndicator:Landroid/widget/ImageView;

.field private mIndexResetOrder:I

.field private mRecordingModeIndicator:Landroid/widget/ImageView;

.field private mRemainCountIndicator:Landroid/widget/TextView;

.field private mStorageIndicator:Landroid/widget/ImageView;

.field private mTimerIndicator:Landroid/widget/ImageView;

.field private mVoiceIndicator:Landroid/widget/ImageView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 48
    sput v0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mIndicatorMarginLeft:I

    .line 49
    sput v0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mIndicatorMarginBottom:I

    .line 50
    sput v0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mIndicatorWidth:I

    .line 51
    sput v0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mIndicatorSpace:I

    .line 54
    const/16 v0, 0x9

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->INDICATOR_ORDER:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x7f0e0057
        0x7f0e0058
        0x7f0e0059
        0x7f0e005a
        0x7f0e005b
        0x7f0e005c
        0x7f0e005d
        0x7f0e005e
        0x7f0e005f
    .end array-data
.end method

.method public constructor <init>(Lcom/sec/android/app/camera/Camera;Lcom/sec/android/app/camera/subview/SubViewManager;)V
    .locals 1
    .param p1, "activitycontext"    # Lcom/sec/android/app/camera/Camera;
    .param p2, "subviewmanager"    # Lcom/sec/android/app/camera/subview/SubViewManager;

    .prologue
    const/4 v0, 0x0

    .line 70
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/camera/subview/SubViewBase;-><init>(Lcom/sec/android/app/camera/Camera;Lcom/sec/android/app/camera/subview/SubViewManager;)V

    .line 37
    iput-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mBatteryIndicator:Landroid/widget/ImageView;

    .line 38
    iput-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mTimerIndicator:Landroid/widget/ImageView;

    .line 39
    iput-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mFlashIndicator:Landroid/widget/ImageView;

    .line 40
    iput-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mAutoNightDetectionIndicator:Landroid/widget/ImageView;

    .line 41
    iput-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mVoiceIndicator:Landroid/widget/ImageView;

    .line 42
    iput-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mStorageIndicator:Landroid/widget/ImageView;

    .line 43
    iput-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mGPSIndicator:Landroid/widget/ImageView;

    .line 44
    iput-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mRecordingModeIndicator:Landroid/widget/ImageView;

    .line 45
    iput-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mRemainCountIndicator:Landroid/widget/TextView;

    .line 46
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mIndexResetOrder:I

    .line 72
    invoke-virtual {p0}, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->initialize()V

    .line 73
    return-void
.end method

.method private reOrder()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 156
    iput v4, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mIndexResetOrder:I

    .line 157
    iget-object v2, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    if-nez v2, :cond_1

    .line 169
    :cond_0
    return-void

    .line 159
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v2, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->INDICATOR_ORDER:[I

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 160
    iget-object v2, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    sget-object v3, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->INDICATOR_ORDER:[I

    aget v3, v3, v0

    invoke-virtual {v2, v3}, Lcom/sec/android/app/camera/Camera;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 161
    .local v1, "view":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-nez v2, :cond_2

    .line 162
    const/4 v2, 0x2

    if-le v0, v2, :cond_3

    iget-object v2, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mRemainCountIndicator:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getVisibility()I

    move-result v2

    if-nez v2, :cond_3

    .line 163
    const/4 v2, 0x1

    invoke-direct {p0, v1, v2}, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->setOrder(Landroid/view/View;Z)V

    .line 159
    :cond_2
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 165
    :cond_3
    invoke-direct {p0, v1, v4}, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->setOrder(Landroid/view/View;Z)V

    goto :goto_1
.end method

.method private setOrder(Landroid/view/View;Z)V
    .locals 6
    .param p1, "view"    # Landroid/view/View;
    .param p2, "isRemainCountShown"    # Z

    .prologue
    .line 172
    const/4 v0, 0x0

    .line 173
    .local v0, "posX":I
    const/4 v1, 0x0

    .line 175
    .local v1, "posY":I
    iget-object v2, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mSubViewManager:Lcom/sec/android/app/camera/subview/SubViewManager;

    invoke-virtual {v2}, Lcom/sec/android/app/camera/subview/SubViewManager;->getDisplayOrientation()I

    move-result v2

    const/4 v3, 0x1

    if-eq v2, v3, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mSubViewManager:Lcom/sec/android/app/camera/subview/SubViewManager;

    invoke-virtual {v2}, Lcom/sec/android/app/camera/subview/SubViewManager;->getDisplayOrientation()I

    move-result v2

    const/4 v3, 0x3

    if-ne v2, v3, :cond_2

    .line 176
    :cond_0
    sget v2, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mIndicatorMarginLeft:I

    iget v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mIndexResetOrder:I

    sget v4, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mIndicatorWidth:I

    sget v5, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mIndicatorSpace:I

    add-int/2addr v4, v5

    mul-int/2addr v3, v4

    add-int v0, v2, v3

    .line 177
    if-eqz p2, :cond_1

    .line 178
    add-int/lit8 v0, v0, 0x3

    .line 180
    :cond_1
    int-to-float v2, v0

    invoke-virtual {p1, v2}, Landroid/view/View;->setX(F)V

    .line 188
    :goto_0
    iget v2, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mIndexResetOrder:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mIndexResetOrder:I

    .line 189
    return-void

    .line 182
    :cond_2
    sget v2, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mIndicatorMarginBottom:I

    rsub-int v2, v2, 0x438

    iget v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mIndexResetOrder:I

    add-int/lit8 v3, v3, 0x1

    sget v4, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mIndicatorWidth:I

    mul-int/2addr v3, v4

    iget v4, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mIndexResetOrder:I

    sget v5, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mIndicatorSpace:I

    mul-int/2addr v4, v5

    add-int/2addr v3, v4

    sub-int v1, v2, v3

    .line 183
    if-eqz p2, :cond_3

    .line 184
    add-int/lit8 v1, v1, -0x3

    .line 186
    :cond_3
    int-to-float v2, v1

    invoke-virtual {p1, v2}, Landroid/view/View;->setY(F)V

    goto :goto_0
.end method


# virtual methods
.method public clear()V
    .locals 0

    .prologue
    .line 101
    return-void
.end method

.method public handleDimButtons()V
    .locals 0

    .prologue
    .line 110
    return-void
.end method

.method public hideCameraBaseIndicator()V
    .locals 4

    .prologue
    .line 143
    const/4 v2, 0x0

    iput v2, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mIndexResetOrder:I

    .line 144
    iget-object v2, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    if-nez v2, :cond_0

    .line 153
    :goto_0
    return-void

    .line 146
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    sget-object v2, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->INDICATOR_ORDER:[I

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 147
    iget-object v2, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    sget-object v3, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->INDICATOR_ORDER:[I

    aget v3, v3, v0

    invoke-virtual {v2, v3}, Lcom/sec/android/app/camera/Camera;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 148
    .local v1, "view":Landroid/view/View;
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-nez v2, :cond_1

    .line 149
    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 146
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 152
    .end local v1    # "view":Landroid/view/View;
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->reOrder()V

    goto :goto_0
.end method

.method public initLayout()V
    .locals 2

    .prologue
    .line 81
    invoke-super {p0}, Lcom/sec/android/app/camera/subview/SubViewBase;->initLayout()V

    .line 83
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    const v1, 0x7f0e0057

    invoke-virtual {v0, v1}, Lcom/sec/android/app/camera/Camera;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mBatteryIndicator:Landroid/widget/ImageView;

    .line 84
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    const v1, 0x7f0e005a

    invoke-virtual {v0, v1}, Lcom/sec/android/app/camera/Camera;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mFlashIndicator:Landroid/widget/ImageView;

    .line 85
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    const v1, 0x7f0e005b

    invoke-virtual {v0, v1}, Lcom/sec/android/app/camera/Camera;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mTimerIndicator:Landroid/widget/ImageView;

    .line 86
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    const v1, 0x7f0e0058

    invoke-virtual {v0, v1}, Lcom/sec/android/app/camera/Camera;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mStorageIndicator:Landroid/widget/ImageView;

    .line 87
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    const v1, 0x7f0e005d

    invoke-virtual {v0, v1}, Lcom/sec/android/app/camera/Camera;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mGPSIndicator:Landroid/widget/ImageView;

    .line 88
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    const v1, 0x7f0e005e

    invoke-virtual {v0, v1}, Lcom/sec/android/app/camera/Camera;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mRecordingModeIndicator:Landroid/widget/ImageView;

    .line 89
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    const v1, 0x7f0e005c

    invoke-virtual {v0, v1}, Lcom/sec/android/app/camera/Camera;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mVoiceIndicator:Landroid/widget/ImageView;

    .line 90
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    const v1, 0x7f0e005f

    invoke-virtual {v0, v1}, Lcom/sec/android/app/camera/Camera;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mAutoNightDetectionIndicator:Landroid/widget/ImageView;

    .line 91
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    const v1, 0x7f0e0059

    invoke-virtual {v0, v1}, Lcom/sec/android/app/camera/Camera;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mRemainCountIndicator:Landroid/widget/TextView;

    .line 93
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0408

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mIndicatorMarginLeft:I

    .line 94
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0409

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mIndicatorMarginBottom:I

    .line 95
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a040c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mIndicatorWidth:I

    .line 96
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a040d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mIndicatorSpace:I

    .line 97
    return-void
.end method

.method public initialize()V
    .locals 2

    .prologue
    .line 76
    const-string v0, "SubViewBaseInicators"

    const-string v1, "initialize"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 77
    invoke-virtual {p0}, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->initLayout()V

    .line 78
    return-void
.end method

.method public onCameraSettingsChanged(II)V
    .locals 4
    .param p1, "menuid"    # I
    .param p2, "modeid"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 386
    sparse-switch p1, :sswitch_data_0

    .line 424
    :goto_0
    return-void

    .line 388
    :sswitch_0
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->getFlashMode()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->setFlashIndicator(I)V

    goto :goto_0

    .line 391
    :sswitch_1
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->getTimer()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->setTimerIndicator(I)V

    goto :goto_0

    .line 394
    :sswitch_2
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->getStorage()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->setStorageIndicator(I)V

    goto :goto_0

    .line 397
    :sswitch_3
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->getGPS()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->setGPSIndicator(I)V

    goto :goto_0

    .line 400
    :sswitch_4
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->getCamcorderRecordingMode()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->setRecordingModeIndicator(I)V

    goto :goto_0

    .line 403
    :sswitch_5
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->getCameraVoiceCommand()I

    move-result v0

    if-ne v0, v3, :cond_0

    .line 404
    invoke-virtual {p0, v1}, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->showVoiceInputIndicator(I)V

    .line 405
    invoke-virtual {p0, v1}, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->setVoiceStatus(I)V

    goto :goto_0

    .line 407
    :cond_0
    invoke-virtual {p0, v2}, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->showVoiceInputIndicator(I)V

    goto :goto_0

    .line 411
    :sswitch_6
    if-ne p2, v3, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->getShootingMode()I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->isDualFrontCamera()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->isDualBackCamera()Z

    move-result v0

    if-nez v0, :cond_2

    .line 413
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mAutoNightDetectionIndicator:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 414
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->isLowLightDetected()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 415
    invoke-virtual {p0, v1}, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->setLowlightIndicator(I)V

    goto/16 :goto_0

    .line 417
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mAutoNightDetectionIndicator:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0

    .line 420
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mAutoNightDetectionIndicator:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0

    .line 386
    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_0
        0x6 -> :sswitch_1
        0x14 -> :sswitch_3
        0x16 -> :sswitch_2
        0x47 -> :sswitch_5
        0x57 -> :sswitch_6
        0xbb8 -> :sswitch_4
    .end sparse-switch
.end method

.method public rotateLayout()V
    .locals 0

    .prologue
    .line 104
    invoke-virtual {p0}, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->initLayout()V

    .line 105
    invoke-direct {p0}, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->reOrder()V

    .line 106
    return-void
.end method

.method public setBatteryLevel(IZ)V
    .locals 4
    .param p1, "level"    # I
    .param p2, "isCharging"    # Z

    .prologue
    const/16 v3, 0x10

    const/4 v1, 0x5

    const/4 v2, 0x0

    .line 192
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mBatteryIndicator:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    .line 214
    :goto_0
    return-void

    .line 195
    :cond_0
    if-eqz p2, :cond_1

    .line 196
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mBatteryIndicator:Landroid/widget/ImageView;

    const v1, 0x7f0204fd

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 197
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mBatteryIndicator:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 198
    invoke-direct {p0}, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->reOrder()V

    goto :goto_0

    .line 201
    :cond_1
    if-ltz p1, :cond_2

    if-ge p1, v1, :cond_2

    .line 202
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mBatteryIndicator:Landroid/widget/ImageView;

    const v1, 0x7f0204fa

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 203
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mBatteryIndicator:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 213
    :goto_1
    invoke-direct {p0}, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->reOrder()V

    goto :goto_0

    .line 204
    :cond_2
    if-lt p1, v1, :cond_3

    if-ge p1, v3, :cond_3

    .line 205
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mBatteryIndicator:Landroid/widget/ImageView;

    const v1, 0x7f0204fb

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 206
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mBatteryIndicator:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    .line 207
    :cond_3
    if-lt p1, v3, :cond_4

    const/16 v0, 0x1d

    if-ge p1, v0, :cond_4

    .line 208
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mBatteryIndicator:Landroid/widget/ImageView;

    const v1, 0x7f0204fc

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 209
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mBatteryIndicator:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    .line 211
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mBatteryIndicator:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1
.end method

.method public setFlashIndicator(I)V
    .locals 3
    .param p1, "mode"    # I

    .prologue
    const/4 v2, 0x0

    .line 254
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mFlashIndicator:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    .line 275
    :goto_0
    return-void

    .line 257
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 271
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mFlashIndicator:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 274
    :goto_1
    invoke-direct {p0}, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->reOrder()V

    goto :goto_0

    .line 259
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mFlashIndicator:Landroid/widget/ImageView;

    const v1, 0x7f02043e

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 260
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mFlashIndicator:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    .line 263
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mFlashIndicator:Landroid/widget/ImageView;

    const v1, 0x7f02043d

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 264
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mFlashIndicator:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    .line 267
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mFlashIndicator:Landroid/widget/ImageView;

    const v1, 0x7f02043f

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 268
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mFlashIndicator:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    .line 257
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setGPSIndicator(I)V
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 313
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mGPSIndicator:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    .line 326
    :goto_0
    return-void

    .line 316
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 322
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mGPSIndicator:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 325
    :goto_1
    invoke-direct {p0}, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->reOrder()V

    goto :goto_0

    .line 318
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mGPSIndicator:Landroid/widget/ImageView;

    const v1, 0x7f0200fc

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 319
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mGPSIndicator:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    .line 316
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public setLowlightIndicator(I)V
    .locals 2
    .param p1, "type"    # I

    .prologue
    const/4 v1, 0x4

    .line 278
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mAutoNightDetectionIndicator:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    .line 294
    :goto_0
    return-void

    .line 281
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 290
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mAutoNightDetectionIndicator:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 293
    :goto_1
    invoke-direct {p0}, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->reOrder()V

    goto :goto_0

    .line 283
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mAutoNightDetectionIndicator:Landroid/widget/ImageView;

    const v1, 0x7f020441

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 284
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mAutoNightDetectionIndicator:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    .line 287
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mAutoNightDetectionIndicator:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    .line 281
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setRecordingModeIndicator(I)V
    .locals 3
    .param p1, "mode"    # I

    .prologue
    const/4 v2, 0x0

    .line 329
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mRecordingModeIndicator:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    .line 355
    :goto_0
    return-void

    .line 332
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 351
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mRecordingModeIndicator:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 354
    :goto_1
    invoke-direct {p0}, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->reOrder()V

    goto :goto_0

    .line 335
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mRecordingModeIndicator:Landroid/widget/ImageView;

    const v1, 0x7f020444

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 336
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mRecordingModeIndicator:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    .line 339
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mRecordingModeIndicator:Landroid/widget/ImageView;

    const v1, 0x7f020443

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 340
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mRecordingModeIndicator:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    .line 343
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mRecordingModeIndicator:Landroid/widget/ImageView;

    const v1, 0x7f020445

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 344
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mRecordingModeIndicator:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    .line 347
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mRecordingModeIndicator:Landroid/widget/ImageView;

    const v1, 0x7f020442

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 348
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mRecordingModeIndicator:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    .line 332
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setRemainCountIndicator(I)V
    .locals 2
    .param p1, "count"    # I

    .prologue
    .line 217
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mRemainCountIndicator:Landroid/widget/TextView;

    if-nez v0, :cond_0

    .line 227
    :goto_0
    return-void

    .line 220
    :cond_0
    const/16 v0, 0x12c

    if-gt p1, v0, :cond_1

    .line 221
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mRemainCountIndicator:Landroid/widget/TextView;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 222
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mRemainCountIndicator:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 226
    :goto_1
    invoke-direct {p0}, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->reOrder()V

    goto :goto_0

    .line 224
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mRemainCountIndicator:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method

.method public setStorageIndicator(I)V
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 297
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mStorageIndicator:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    .line 310
    :goto_0
    return-void

    .line 300
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 306
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mStorageIndicator:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 309
    :goto_1
    invoke-direct {p0}, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->reOrder()V

    goto :goto_0

    .line 302
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mStorageIndicator:Landroid/widget/ImageView;

    const v1, 0x7f020448

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 303
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mStorageIndicator:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    .line 300
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public setTimerIndicator(I)V
    .locals 3
    .param p1, "mode"    # I

    .prologue
    const/4 v2, 0x0

    .line 230
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mTimerIndicator:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    .line 251
    :goto_0
    return-void

    .line 233
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 247
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mTimerIndicator:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 250
    :goto_1
    invoke-direct {p0}, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->reOrder()V

    goto :goto_0

    .line 235
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mTimerIndicator:Landroid/widget/ImageView;

    const v1, 0x7f02044b

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 236
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mTimerIndicator:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    .line 239
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mTimerIndicator:Landroid/widget/ImageView;

    const v1, 0x7f02044c

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 240
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mTimerIndicator:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    .line 243
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mTimerIndicator:Landroid/widget/ImageView;

    const v1, 0x7f02044a

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 244
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mTimerIndicator:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    .line 233
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setVoiceStatus(I)V
    .locals 3
    .param p1, "status"    # I

    .prologue
    const/4 v2, 0x0

    .line 368
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mVoiceIndicator:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    .line 383
    :goto_0
    return-void

    .line 370
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 380
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mVoiceIndicator:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 382
    :goto_1
    invoke-direct {p0}, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->reOrder()V

    goto :goto_0

    .line 372
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mVoiceIndicator:Landroid/widget/ImageView;

    const v1, 0x7f02044d

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 373
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mVoiceIndicator:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    .line 376
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mVoiceIndicator:Landroid/widget/ImageView;

    const v1, 0x7f02044f

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 377
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mVoiceIndicator:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    .line 370
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public showCameraBaseIndicator()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 113
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    if-eqz v0, :cond_0

    .line 114
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getBatteryLevel()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/Camera;->isBatteryCharging()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->setBatteryLevel(IZ)V

    .line 115
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->getFlashMode()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->setFlashIndicator(I)V

    .line 116
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->getTimer()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->setTimerIndicator(I)V

    .line 117
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->getStorage()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->setStorageIndicator(I)V

    .line 118
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->getGPS()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->setGPSIndicator(I)V

    .line 119
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->getCamcorderRecordingMode()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->setRecordingModeIndicator(I)V

    .line 120
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-static {v0}, Lcom/sec/android/app/camera/Util;->isKNOXMode(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 121
    invoke-virtual {p0, v2}, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->showVoiceInputIndicator(I)V

    .line 122
    invoke-virtual {p0, v2}, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->setVoiceStatus(I)V

    .line 126
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->getAutoNightDetectionMode()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->getShootingMode()I

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->isDualFrontCamera()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->isDualBackCamera()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mAutoNightDetectionIndicator:Landroid/widget/ImageView;

    if-eqz v0, :cond_3

    .line 130
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->isLowLightDetected()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 131
    invoke-virtual {p0, v2}, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->setLowlightIndicator(I)V

    .line 138
    :goto_1
    invoke-direct {p0}, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->reOrder()V

    .line 140
    :cond_0
    return-void

    .line 124
    :cond_1
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->showVoiceInputIndicator(I)V

    goto :goto_0

    .line 133
    :cond_2
    invoke-virtual {p0, v3}, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->setLowlightIndicator(I)V

    goto :goto_1

    .line 136
    :cond_3
    invoke-virtual {p0, v3}, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->setLowlightIndicator(I)V

    goto :goto_1
.end method

.method public showVoiceInputIndicator(I)V
    .locals 2
    .param p1, "visible"    # I

    .prologue
    .line 358
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mVoiceIndicator:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    .line 365
    :goto_0
    return-void

    .line 360
    :cond_0
    if-nez p1, :cond_1

    .line 361
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mVoiceIndicator:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 364
    :goto_1
    invoke-direct {p0}, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->reOrder()V

    goto :goto_0

    .line 363
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->mVoiceIndicator:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1
.end method
