.class public final Lcom/sec/android/app/camera/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/camera/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final EasyConnect:I = 0x7f0e0063

.field public static final GLSurfaceLayout:I = 0x7f0e002c

.field public static final ProgressBar01:I = 0x7f0e0062

.field public static final ScrollView:I = 0x7f0e0004

.field public static final WifiDirectCancel:I = 0x7f0e0065

.field public static final WifiDirectSettings:I = 0x7f0e0064

.field public static final base_layout:I = 0x7f0e0029

.field public static final btnCamera:I = 0x7f0e0006

.field public static final btnCancel:I = 0x7f0e0008

.field public static final btnRecord:I = 0x7f0e0007

.field public static final camera_preview:I = 0x7f0e002b

.field public static final camera_preview_layout:I = 0x7f0e002a

.field public static final cancel_button:I = 0x7f0e0011

.field public static final checkbox:I = 0x7f0e0014

.field public static final checkbox_id:I = 0x7f0e0039

.field public static final crop_button_control:I = 0x7f0e000a

.field public static final crop_discard:I = 0x7f0e000b

.field public static final crop_save:I = 0x7f0e000d

.field public static final cursorview:I = 0x7f0e0001

.field public static final custom_actionbar:I = 0x7f0e0010

.field public static final description1:I = 0x7f0e0016

.field public static final description2:I = 0x7f0e0018

.field public static final description3:I = 0x7f0e001a

.field public static final description4:I = 0x7f0e001c

.field public static final description5:I = 0x7f0e001e

.field public static final description6:I = 0x7f0e0020

.field public static final download_id:I = 0x7f0e003a

.field public static final dragview:I = 0x7f0e0002

.field public static final empty_layout:I = 0x7f0e0013

.field public static final framelayout_id:I = 0x7f0e0036

.field public static final gridview:I = 0x7f0e0000

.field public static final guide_fullscreen:I = 0x7f0e0022

.field public static final hover_popup_text:I = 0x7f0e0021

.field public static final image:I = 0x7f0e0009

.field public static final imageview_id:I = 0x7f0e0037

.field public static final message:I = 0x7f0e0005

.field public static final mode_description:I = 0x7f0e002f

.field public static final mode_image:I = 0x7f0e002d

.field public static final mode_info_back_button:I = 0x7f0e003c

.field public static final mode_info_list:I = 0x7f0e003d

.field public static final mode_info_title:I = 0x7f0e003e

.field public static final mode_title:I = 0x7f0e002e

.field public static final okay:I = 0x7f0e000e

.field public static final playAttachVideo:I = 0x7f0e000f

.field public static final quickaccess_close_button:I = 0x7f0e0030

.field public static final quickaccess_help_image:I = 0x7f0e0031

.field public static final quickaccess_help_msg:I = 0x7f0e0032

.field public static final retry:I = 0x7f0e000c

.field public static final save_button:I = 0x7f0e0012

.field public static final sensorcaptrue_close_button:I = 0x7f0e0025

.field public static final sensorcaptrue_help_image:I = 0x7f0e0023

.field public static final sensorcapture_help_msg:I = 0x7f0e0024

.field public static final setting_help_close_button:I = 0x7f0e0035

.field public static final setting_help_image:I = 0x7f0e0033

.field public static final setting_help_msg:I = 0x7f0e0034

.field public static final shadowimage_id:I = 0x7f0e0003

.field public static final shutterLayout:I = 0x7f0e0048

.field public static final sub_camera_ind_battery:I = 0x7f0e0057

.field public static final sub_camera_ind_flash:I = 0x7f0e005a

.field public static final sub_camera_ind_gps:I = 0x7f0e005d

.field public static final sub_camera_ind_lowlight:I = 0x7f0e005f

.field public static final sub_camera_ind_recmode:I = 0x7f0e005e

.field public static final sub_camera_ind_remaincount:I = 0x7f0e0059

.field public static final sub_camera_ind_storage:I = 0x7f0e0058

.field public static final sub_camera_ind_timer:I = 0x7f0e005b

.field public static final sub_camera_ind_voice:I = 0x7f0e005c

.field public static final sub_camera_indicators:I = 0x7f0e0056

.field public static final subtitle1:I = 0x7f0e0015

.field public static final subtitle2:I = 0x7f0e0017

.field public static final subtitle3:I = 0x7f0e0019

.field public static final subtitle4:I = 0x7f0e001b

.field public static final subtitle5:I = 0x7f0e001d

.field public static final subtitle6:I = 0x7f0e001f

.field public static final subview_b_layout:I = 0x7f0e0055

.field public static final subview_base_layout:I = 0x7f0e0040

.field public static final subview_cameramenu:I = 0x7f0e004d

.field public static final subview_easymode_layout:I = 0x7f0e0060

.field public static final subview_focus:I = 0x7f0e0041

.field public static final subview_help_capture:I = 0x7f0e0052

.field public static final subview_help_layout:I = 0x7f0e0051

.field public static final subview_help_mode:I = 0x7f0e0054

.field public static final subview_help_record:I = 0x7f0e0053

.field public static final subview_layout:I = 0x7f0e003f

.field public static final subview_mode:I = 0x7f0e0050

.field public static final subview_pip:I = 0x7f0e0042

.field public static final subview_recording:I = 0x7f0e004f

.field public static final subview_recording_pause:I = 0x7f0e004c

.field public static final subview_recording_shutter:I = 0x7f0e004a

.field public static final subview_recording_shutter_bg:I = 0x7f0e0049

.field public static final subview_recording_stop:I = 0x7f0e004b

.field public static final subview_recordingmenu:I = 0x7f0e0047

.field public static final subview_settings:I = 0x7f0e0043

.field public static final subview_shortcut1:I = 0x7f0e0045

.field public static final subview_shortcut2:I = 0x7f0e0046

.field public static final subview_shutter:I = 0x7f0e004e

.field public static final subview_switch:I = 0x7f0e0044

.field public static final text:I = 0x7f0e003b

.field public static final textview_id:I = 0x7f0e0038

.field public static final toast_image:I = 0x7f0e0027

.field public static final toast_layout_root:I = 0x7f0e0026

.field public static final toast_text:I = 0x7f0e0028

.field public static final watingview_layout:I = 0x7f0e0061


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3265
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
