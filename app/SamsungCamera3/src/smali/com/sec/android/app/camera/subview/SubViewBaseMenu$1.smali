.class Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;
.super Ljava/lang/Object;
.source "SubViewBaseMenu.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/camera/subview/SubViewBaseMenu;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;


# direct methods
.method constructor <init>(Lcom/sec/android/app/camera/subview/SubViewBaseMenu;)V
    .locals 0

    .prologue
    .line 178
    iput-object p1, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 10
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/16 v9, 0x1b

    const/high16 v8, 0x42c80000    # 100.0f

    const/4 v7, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 181
    iget-object v5, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    invoke-virtual {v5}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->onUserInteraction()V

    .line 182
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v5

    const v6, 0x7f0e004e

    if-ne v5, v6, :cond_b

    .line 183
    const-string v5, "SubViewBaseMenu"

    const-string v6, "onTouch : shutter button"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 185
    iget-object v5, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    iget-object v5, v5, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    if-eqz v5, :cond_2

    .line 186
    iget-object v5, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    iget-object v5, v5, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v5}, Lcom/sec/android/app/camera/Camera;->getEngine()Lcom/sec/android/app/camera/CommonEngine;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/camera/CommonEngine;->isPrepareRecording()Z

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    iget-object v5, v5, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v5}, Lcom/sec/android/app/camera/Camera;->getEngine()Lcom/sec/android/app/camera/CommonEngine;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/camera/CommonEngine;->isRecorderStarting()Z

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    iget-object v5, v5, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v5}, Lcom/sec/android/app/camera/Camera;->getEngine()Lcom/sec/android/app/camera/CommonEngine;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/camera/CommonEngine;->isRecording()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 189
    :cond_0
    const-string v4, "SubViewBaseMenu"

    const-string v5, "isRecording, ignore onTouchEvent"

    invoke-static {v4, v5}, Landroid/util/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 350
    :cond_1
    :goto_0
    return v3

    .line 194
    :cond_2
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v5

    packed-switch v5, :pswitch_data_0

    goto :goto_0

    .line 196
    :pswitch_0
    const-string v4, "SubViewBaseMenu"

    const-string v5, "onTouch : ACTION_DOWN"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    iget-object v4, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    # getter for: Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mInitTouchPoint:Landroid/graphics/PointF;
    invoke-static {v4}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->access$000(Lcom/sec/android/app/camera/subview/SubViewBaseMenu;)Landroid/graphics/PointF;

    move-result-object v4

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/graphics/PointF;->set(FF)V

    .line 198
    iget-object v4, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    # getter for: Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mShutterButton:Landroid/widget/ImageView;
    invoke-static {v4}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->access$100(Lcom/sec/android/app/camera/subview/SubViewBaseMenu;)Landroid/widget/ImageView;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 199
    iget-object v4, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    # getter for: Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mShutterButton:Landroid/widget/ImageView;
    invoke-static {v4}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->access$100(Lcom/sec/android/app/camera/subview/SubViewBaseMenu;)Landroid/widget/ImageView;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/widget/ImageView;->setPressed(Z)V

    .line 201
    :cond_3
    iget-object v4, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    iget-object v4, v4, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    if-eqz v4, :cond_1

    .line 202
    iget-object v4, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    iget-object v4, v4, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v4, v3}, Lcom/sec/android/app/camera/Camera;->setLongPressEnabled(Z)V

    .line 203
    iget-object v4, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    iget-object v4, v4, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v4, v9, v7}, Lcom/sec/android/app/camera/Camera;->onKeyDown(ILandroid/view/KeyEvent;)Z

    .line 205
    iget-object v4, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    iget-object v4, v4, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v4}, Lcom/sec/android/app/camera/Camera;->hideZoomMenu()V

    .line 206
    iget-object v4, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    iget-object v4, v4, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v4}, Lcom/sec/android/app/camera/Camera;->hideExposureValueMenu()V

    .line 207
    iget-object v4, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    iget-object v4, v4, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v4}, Lcom/sec/android/app/camera/Camera;->hideBeautyFaceLevelMenu()V

    goto :goto_0

    .line 211
    :pswitch_1
    const-string v5, "SubViewBaseMenu"

    const-string v6, "onTouch : ACTION_CANCEL"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 214
    :pswitch_2
    const-string v5, "SubViewBaseMenu"

    const-string v6, "onTouch : ACTION_UP"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 215
    iget-object v5, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    iget-object v5, v5, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    if-eqz v5, :cond_1

    .line 216
    iget-object v5, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    iget-object v5, v5, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v5, v4}, Lcom/sec/android/app/camera/Camera;->setLongPressEnabled(Z)V

    .line 217
    iget-object v5, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    invoke-virtual {v5}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->isShowSettingMenu()Z

    move-result v5

    if-nez v5, :cond_4

    iget-object v5, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    invoke-virtual {v5}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->isShowShootingModeMenu()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 218
    :cond_4
    iget-object v5, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    iget-object v5, v5, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v5}, Lcom/sec/android/app/camera/Camera;->processBack()V

    .line 219
    iget-object v5, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    # getter for: Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mShutterButton:Landroid/widget/ImageView;
    invoke-static {v5}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->access$100(Lcom/sec/android/app/camera/subview/SubViewBaseMenu;)Landroid/widget/ImageView;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/widget/ImageView;->setPressed(Z)V

    goto/16 :goto_0

    .line 222
    :cond_5
    iget-object v5, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    iget-object v5, v5, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v5}, Lcom/sec/android/app/camera/Camera;->isCaptureEnabled()Z

    move-result v5

    if-nez v5, :cond_7

    iget-object v5, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    iget-object v5, v5, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v5}, Lcom/sec/android/app/camera/Camera;->getEngine()Lcom/sec/android/app/camera/CommonEngine;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/camera/CommonEngine;->isTimerCounting()Z

    move-result v5

    if-nez v5, :cond_7

    .line 223
    :goto_1
    iget-object v5, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    iget-object v5, v5, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v5}, Lcom/sec/android/app/camera/Camera;->isCaptureEnabled()Z

    move-result v5

    if-nez v5, :cond_6

    .line 224
    iget-object v5, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    iget-object v5, v5, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v5}, Lcom/sec/android/app/camera/Camera;->processBack()V

    goto :goto_1

    .line 226
    :cond_6
    iget-object v5, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    # getter for: Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mShutterButton:Landroid/widget/ImageView;
    invoke-static {v5}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->access$100(Lcom/sec/android/app/camera/subview/SubViewBaseMenu;)Landroid/widget/ImageView;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/widget/ImageView;->setPressed(Z)V

    goto/16 :goto_0

    .line 229
    :cond_7
    iget-object v5, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    invoke-virtual {v5}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->isShowThumbnailListMenu()Z

    move-result v5

    if-eqz v5, :cond_8

    .line 230
    iget-object v5, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    iget-object v5, v5, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v5}, Lcom/sec/android/app/camera/Camera;->processBack()V

    .line 232
    :cond_8
    iget-object v5, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    # getter for: Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mShutterButton:Landroid/widget/ImageView;
    invoke-static {v5}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->access$100(Lcom/sec/android/app/camera/subview/SubViewBaseMenu;)Landroid/widget/ImageView;

    move-result-object v5

    if-eqz v5, :cond_9

    iget-object v5, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    # getter for: Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mShutterButton:Landroid/widget/ImageView;
    invoke-static {v5}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->access$100(Lcom/sec/android/app/camera/subview/SubViewBaseMenu;)Landroid/widget/ImageView;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/ImageView;->isPressed()Z

    move-result v5

    if-eqz v5, :cond_9

    .line 233
    iget-object v5, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    # getter for: Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mShutterButton:Landroid/widget/ImageView;
    invoke-static {v5}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->access$100(Lcom/sec/android/app/camera/subview/SubViewBaseMenu;)Landroid/widget/ImageView;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/widget/ImageView;->setPressed(Z)V

    .line 234
    iget-object v4, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    iget-object v4, v4, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v4, v9, v7}, Lcom/sec/android/app/camera/Camera;->onKeyUp(ILandroid/view/KeyEvent;)Z

    .line 236
    :cond_9
    iget-object v4, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    iget-object v4, v4, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    iget-object v4, v4, Lcom/sec/android/app/camera/Camera;->mTutorialCaptureMode:Lcom/sec/android/app/camera/glwidget/TwGLTutorialCaptureMode;

    if-eqz v4, :cond_1

    .line 237
    iget-object v4, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    iget-object v4, v4, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v4}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/camera/CameraSettings;->getHelpMode()I

    move-result v4

    const/16 v5, 0xc9

    if-ne v4, v5, :cond_1

    iget-object v4, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    iget-object v4, v4, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    iget-object v4, v4, Lcom/sec/android/app/camera/Camera;->mTutorialCaptureMode:Lcom/sec/android/app/camera/glwidget/TwGLTutorialCaptureMode;

    iget v4, v4, Lcom/sec/android/app/camera/glwidget/TwGLTutorialCaptureMode;->mTutorialStep:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_1

    .line 238
    iget-object v4, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    iget-object v4, v4, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    iget-object v4, v4, Lcom/sec/android/app/camera/Camera;->mTutorialCaptureMode:Lcom/sec/android/app/camera/glwidget/TwGLTutorialCaptureMode;

    invoke-virtual {v4}, Lcom/sec/android/app/camera/glwidget/TwGLTutorialCaptureMode;->setStepHide()V

    goto/16 :goto_0

    .line 244
    :pswitch_3
    iget-object v5, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    # getter for: Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mShutterButton:Landroid/widget/ImageView;
    invoke-static {v5}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->access$100(Lcom/sec/android/app/camera/subview/SubViewBaseMenu;)Landroid/widget/ImageView;

    move-result-object v5

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    # getter for: Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mShutterButton:Landroid/widget/ImageView;
    invoke-static {v5}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->access$100(Lcom/sec/android/app/camera/subview/SubViewBaseMenu;)Landroid/widget/ImageView;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/ImageView;->isPressed()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 245
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    # getter for: Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mInitTouchPoint:Landroid/graphics/PointF;
    invoke-static {v6}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->access$000(Lcom/sec/android/app/camera/subview/SubViewBaseMenu;)Landroid/graphics/PointF;

    move-result-object v6

    iget v6, v6, Landroid/graphics/PointF;->x:F

    sub-float v1, v5, v6

    .line 246
    .local v1, "deltaX":F
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    # getter for: Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mInitTouchPoint:Landroid/graphics/PointF;
    invoke-static {v6}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->access$000(Lcom/sec/android/app/camera/subview/SubViewBaseMenu;)Landroid/graphics/PointF;

    move-result-object v6

    iget v6, v6, Landroid/graphics/PointF;->y:F

    sub-float v2, v5, v6

    .line 247
    .local v2, "deltaY":F
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v5

    cmpl-float v5, v5, v8

    if-gtz v5, :cond_a

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v5

    cmpl-float v5, v5, v8

    if-lez v5, :cond_1

    .line 248
    :cond_a
    iget-object v5, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    # getter for: Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mShutterButton:Landroid/widget/ImageView;
    invoke-static {v5}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->access$100(Lcom/sec/android/app/camera/subview/SubViewBaseMenu;)Landroid/widget/ImageView;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/widget/ImageView;->setPressed(Z)V

    .line 249
    iget-object v4, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    iget-object v4, v4, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    if-eqz v4, :cond_1

    .line 250
    iget-object v4, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    iget-object v4, v4, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v4}, Lcom/sec/android/app/camera/Camera;->shutterButtonCancelAction()V

    goto/16 :goto_0

    .line 257
    .end local v1    # "deltaX":F
    .end local v2    # "deltaY":F
    :cond_b
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v5

    const v6, 0x7f0e004f

    if-ne v5, v6, :cond_12

    .line 258
    iget-object v5, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    iget-object v5, v5, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    if-eqz v5, :cond_d

    .line 259
    iget-object v5, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    iget-object v5, v5, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v5}, Lcom/sec/android/app/camera/Camera;->getEngine()Lcom/sec/android/app/camera/CommonEngine;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/camera/CommonEngine;->isPrepareRecording()Z

    move-result v5

    if-nez v5, :cond_c

    iget-object v5, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    iget-object v5, v5, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v5}, Lcom/sec/android/app/camera/Camera;->getEngine()Lcom/sec/android/app/camera/CommonEngine;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/camera/CommonEngine;->isRecorderStarting()Z

    move-result v5

    if-nez v5, :cond_c

    iget-object v5, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    iget-object v5, v5, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v5}, Lcom/sec/android/app/camera/Camera;->getEngine()Lcom/sec/android/app/camera/CommonEngine;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/camera/CommonEngine;->isRecording()Z

    move-result v5

    if-eqz v5, :cond_d

    .line 262
    :cond_c
    const-string v4, "SubViewBaseMenu"

    const-string v5, "isRecording, ignore onTouchEvent"

    invoke-static {v4, v5}, Landroid/util/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 266
    :cond_d
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v5

    packed-switch v5, :pswitch_data_1

    .line 307
    iget-object v5, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    # getter for: Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mRecordingButton:Landroid/widget/ImageView;
    invoke-static {v5}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->access$200(Lcom/sec/android/app/camera/subview/SubViewBaseMenu;)Landroid/widget/ImageView;

    move-result-object v5

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    # getter for: Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mRecordingButton:Landroid/widget/ImageView;
    invoke-static {v5}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->access$200(Lcom/sec/android/app/camera/subview/SubViewBaseMenu;)Landroid/widget/ImageView;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/widget/ImageView;->setPressed(Z)V

    goto/16 :goto_0

    .line 268
    :pswitch_4
    iget-object v4, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    # getter for: Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mRecordingButton:Landroid/widget/ImageView;
    invoke-static {v4}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->access$200(Lcom/sec/android/app/camera/subview/SubViewBaseMenu;)Landroid/widget/ImageView;

    move-result-object v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    # getter for: Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mRecordingButton:Landroid/widget/ImageView;
    invoke-static {v4}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->access$200(Lcom/sec/android/app/camera/subview/SubViewBaseMenu;)Landroid/widget/ImageView;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/widget/ImageView;->setPressed(Z)V

    goto/16 :goto_0

    .line 272
    :pswitch_5
    iget-object v5, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    # getter for: Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mRecordingButton:Landroid/widget/ImageView;
    invoke-static {v5}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->access$200(Lcom/sec/android/app/camera/subview/SubViewBaseMenu;)Landroid/widget/ImageView;

    move-result-object v5

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    # getter for: Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mRecordingButton:Landroid/widget/ImageView;
    invoke-static {v5}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->access$200(Lcom/sec/android/app/camera/subview/SubViewBaseMenu;)Landroid/widget/ImageView;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/ImageView;->isPressed()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 275
    iget-object v5, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    # getter for: Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mRecordingButton:Landroid/widget/ImageView;
    invoke-static {v5}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->access$200(Lcom/sec/android/app/camera/subview/SubViewBaseMenu;)Landroid/widget/ImageView;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/widget/ImageView;->setPressed(Z)V

    .line 276
    iget-object v4, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    invoke-virtual {v4}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->onUserInteraction()V

    .line 277
    iget-object v4, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    iget-object v4, v4, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v4}, Lcom/sec/android/app/camera/Camera;->isCaptureEnabled()Z

    move-result v4

    if-nez v4, :cond_e

    iget-object v4, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    iget-object v4, v4, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v4}, Lcom/sec/android/app/camera/Camera;->getEngine()Lcom/sec/android/app/camera/CommonEngine;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/camera/CommonEngine;->isTimerCounting()Z

    move-result v4

    if-nez v4, :cond_e

    .line 278
    :goto_2
    iget-object v4, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    iget-object v4, v4, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v4}, Lcom/sec/android/app/camera/Camera;->isCaptureEnabled()Z

    move-result v4

    if-nez v4, :cond_1

    .line 279
    iget-object v4, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    iget-object v4, v4, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v4}, Lcom/sec/android/app/camera/Camera;->processBack()V

    goto :goto_2

    .line 283
    :cond_e
    iget-object v4, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    invoke-virtual {v4}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->isFullScreenMenuShown()Z

    move-result v4

    if-eqz v4, :cond_f

    .line 284
    iget-object v4, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    iget-object v4, v4, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    if-eqz v4, :cond_1

    .line 285
    iget-object v4, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    iget-object v4, v4, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v4}, Lcom/sec/android/app/camera/Camera;->processBack()V

    goto/16 :goto_0

    .line 289
    :cond_f
    iget-object v4, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    iget-object v4, v4, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    if-eqz v4, :cond_1

    .line 290
    iget-object v4, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    iget-object v4, v4, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v4}, Lcom/sec/android/app/camera/Camera;->cancelTouchAE()V

    .line 291
    iget-object v4, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    iget-object v4, v4, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    const/16 v5, 0x82

    invoke-virtual {v4, v5, v7}, Lcom/sec/android/app/camera/Camera;->onKeyDown(ILandroid/view/KeyEvent;)Z

    .line 293
    iget-object v4, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    invoke-virtual {v4}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->isShowThumbnailListMenu()Z

    move-result v4

    if-eqz v4, :cond_10

    .line 294
    iget-object v4, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    iget-object v4, v4, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v4}, Lcom/sec/android/app/camera/Camera;->processBack()V

    .line 296
    :cond_10
    iget-object v4, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    iget-object v4, v4, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v4}, Lcom/sec/android/app/camera/Camera;->hideZoomMenu()V

    .line 297
    iget-object v4, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    iget-object v4, v4, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v4}, Lcom/sec/android/app/camera/Camera;->hideExposureValueMenu()V

    .line 298
    iget-object v4, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    iget-object v4, v4, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v4}, Lcom/sec/android/app/camera/Camera;->hideBeautyFaceLevelMenu()V

    .line 300
    iget-object v4, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    iget-object v4, v4, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    const/16 v5, 0x82

    invoke-virtual {v4, v5, v7}, Lcom/sec/android/app/camera/Camera;->onKeyUp(ILandroid/view/KeyEvent;)Z

    goto/16 :goto_0

    .line 304
    :pswitch_6
    iget-object v5, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    # getter for: Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mRecordingButton:Landroid/widget/ImageView;
    invoke-static {v5}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->access$200(Lcom/sec/android/app/camera/subview/SubViewBaseMenu;)Landroid/widget/ImageView;

    move-result-object v5

    if-eqz v5, :cond_11

    iget-object v5, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    # getter for: Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mRecordingButton:Landroid/widget/ImageView;
    invoke-static {v5}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->access$200(Lcom/sec/android/app/camera/subview/SubViewBaseMenu;)Landroid/widget/ImageView;

    move-result-object v5

    invoke-virtual {v5, v3}, Landroid/widget/ImageView;->setPressed(Z)V

    :cond_11
    move v3, v4

    .line 305
    goto/16 :goto_0

    .line 311
    :cond_12
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v5

    const v6, 0x7f0e0050

    if-ne v5, v6, :cond_17

    .line 312
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v5

    packed-switch v5, :pswitch_data_2

    .line 345
    iget-object v5, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    # getter for: Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeButton:Landroid/widget/ImageView;
    invoke-static {v5}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->access$300(Lcom/sec/android/app/camera/subview/SubViewBaseMenu;)Landroid/widget/ImageView;

    move-result-object v5

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    # getter for: Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeButton:Landroid/widget/ImageView;
    invoke-static {v5}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->access$300(Lcom/sec/android/app/camera/subview/SubViewBaseMenu;)Landroid/widget/ImageView;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/widget/ImageView;->setPressed(Z)V

    goto/16 :goto_0

    .line 314
    :pswitch_7
    iget-object v4, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    # getter for: Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeButton:Landroid/widget/ImageView;
    invoke-static {v4}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->access$300(Lcom/sec/android/app/camera/subview/SubViewBaseMenu;)Landroid/widget/ImageView;

    move-result-object v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    # getter for: Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeButton:Landroid/widget/ImageView;
    invoke-static {v4}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->access$300(Lcom/sec/android/app/camera/subview/SubViewBaseMenu;)Landroid/widget/ImageView;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/widget/ImageView;->setPressed(Z)V

    goto/16 :goto_0

    .line 318
    :pswitch_8
    iget-object v5, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    # getter for: Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeButton:Landroid/widget/ImageView;
    invoke-static {v5}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->access$300(Lcom/sec/android/app/camera/subview/SubViewBaseMenu;)Landroid/widget/ImageView;

    move-result-object v5

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    # getter for: Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeButton:Landroid/widget/ImageView;
    invoke-static {v5}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->access$300(Lcom/sec/android/app/camera/subview/SubViewBaseMenu;)Landroid/widget/ImageView;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/ImageView;->isPressed()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 321
    iget-object v5, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    # getter for: Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeButton:Landroid/widget/ImageView;
    invoke-static {v5}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->access$300(Lcom/sec/android/app/camera/subview/SubViewBaseMenu;)Landroid/widget/ImageView;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/widget/ImageView;->setPressed(Z)V

    .line 322
    iget-object v5, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    iget-object v5, v5, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    if-eqz v5, :cond_1

    .line 323
    iget-object v5, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    iget-object v5, v5, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v5}, Lcom/sec/android/app/camera/Camera;->getGLContext()Lcom/sec/android/glview/TwGLContext;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/glview/TwGLContext;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    const-string v6, "audio"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 324
    .local v0, "am":Landroid/media/AudioManager;
    invoke-virtual {v0, v4}, Landroid/media/AudioManager;->playSoundEffect(I)V

    .line 325
    iget-object v5, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    invoke-virtual {v5}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->isShowSettingMenu()Z

    move-result v5

    if-eqz v5, :cond_13

    .line 326
    iget-object v5, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    iget-object v5, v5, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v5}, Lcom/sec/android/app/camera/Camera;->processBack()V

    .line 328
    :cond_13
    iget-object v5, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    invoke-virtual {v5}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->isShowShootingModeMenu()Z

    move-result v5

    if-eqz v5, :cond_14

    .line 329
    iget-object v5, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    iget-object v5, v5, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v5}, Lcom/sec/android/app/camera/Camera;->processBack()V

    .line 330
    iget-object v5, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    # getter for: Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeButton:Landroid/widget/ImageView;
    invoke-static {v5}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->access$300(Lcom/sec/android/app/camera/subview/SubViewBaseMenu;)Landroid/widget/ImageView;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/widget/ImageView;->setPressed(Z)V

    goto/16 :goto_0

    .line 333
    :cond_14
    iget-object v4, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    invoke-virtual {v4}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->isShowThumbnailListMenu()Z

    move-result v4

    if-eqz v4, :cond_15

    .line 334
    iget-object v4, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    iget-object v4, v4, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v4}, Lcom/sec/android/app/camera/Camera;->hideEffectMenu()V

    .line 336
    :cond_15
    iget-object v4, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    iget-object v4, v4, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v4}, Lcom/sec/android/app/camera/Camera;->getCameraBaseMenu()Lcom/sec/android/app/camera/glwidget/TwGLCameraBaseMenu;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 337
    iget-object v4, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    iget-object v4, v4, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v4}, Lcom/sec/android/app/camera/Camera;->getCameraBaseMenu()Lcom/sec/android/app/camera/glwidget/TwGLCameraBaseMenu;

    move-result-object v4

    iget-object v4, v4, Lcom/sec/android/app/camera/glwidget/TwGLCameraBaseMenu;->mShootingModeButton:Lcom/sec/android/glview/TwGLButton;

    invoke-virtual {v4}, Lcom/sec/android/glview/TwGLButton;->getOnClickListener()Lcom/sec/android/glview/TwGLView$OnClickListener;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    iget-object v5, v5, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v5}, Lcom/sec/android/app/camera/Camera;->getGLContext()Lcom/sec/android/glview/TwGLContext;

    move-result-object v5

    invoke-virtual {v5, v3}, Lcom/sec/android/glview/TwGLContext;->findViewByTag(I)Lcom/sec/android/glview/TwGLView;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/sec/android/glview/TwGLView$OnClickListener;->onClick(Lcom/sec/android/glview/TwGLView;)Z

    goto/16 :goto_0

    .line 342
    .end local v0    # "am":Landroid/media/AudioManager;
    :pswitch_9
    iget-object v5, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    # getter for: Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeButton:Landroid/widget/ImageView;
    invoke-static {v5}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->access$300(Lcom/sec/android/app/camera/subview/SubViewBaseMenu;)Landroid/widget/ImageView;

    move-result-object v5

    if-eqz v5, :cond_16

    iget-object v5, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;->this$0:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    # getter for: Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeButton:Landroid/widget/ImageView;
    invoke-static {v5}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->access$300(Lcom/sec/android/app/camera/subview/SubViewBaseMenu;)Landroid/widget/ImageView;

    move-result-object v5

    invoke-virtual {v5, v3}, Landroid/widget/ImageView;->setPressed(Z)V

    :cond_16
    move v3, v4

    .line 343
    goto/16 :goto_0

    :cond_17
    move v3, v4

    .line 350
    goto/16 :goto_0

    .line 194
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_1
    .end packed-switch

    .line 266
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_5
    .end packed-switch

    .line 312
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_8
    .end packed-switch
.end method
