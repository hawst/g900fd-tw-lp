.class public Lcom/sec/android/app/camera/subview/SubViewBaseMenu;
.super Lcom/sec/android/app/camera/subview/SubViewBase;
.source "SubViewBaseMenu.java"


# static fields
.field private static final BASE_BUTTONS_POS:[Landroid/graphics/PointF;

.field private static final BENDEDUI_SHUTTER_BUTTON_MOVING_THRESHOLD:F = 100.0f

.field private static final MODE_BUTTON:I = 0x2

.field private static final RECORDING_BUTTON:I = 0x1

.field private static final SHUTTER_BUTTON:I = 0x0

.field private static final TAG:Ljava/lang/String; = "SubViewBaseMenu"

.field private static mButtonVisibility:[I


# instance fields
.field private mCameraBaseMenu:Landroid/view/ViewGroup;

.field private mEasyModeFrontSideBar:Lcom/sec/android/app/camera/subview/SubViewSideBar;

.field private mEasyModeSideBar:Lcom/sec/android/app/camera/subview/SubViewSideBar;

.field private mFrontSideBar:Lcom/sec/android/app/camera/subview/SubViewSideBar;

.field private mIndicators:Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;

.field private mInitTouchPoint:Landroid/graphics/PointF;

.field private mModeBtnResIds:[I

.field private mModeButton:Landroid/widget/ImageView;

.field private mModeTextDimImageResId:I

.field private mModeTextImageResId:I

.field private mModeTextPressImageResId:I

.field private final mRecordingBtnResIds:[I

.field private mRecordingButton:Landroid/widget/ImageView;

.field private final mShutterBtnResIds:[I

.field private mShutterButton:Landroid/widget/ImageView;

.field private mSideBar:Lcom/sec/android/app/camera/subview/SubViewSideBar;

.field mSubViewOnTouchListener:Landroid/view/View$OnTouchListener;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const v5, 0x44bc8000    # 1508.0f

    const/high16 v4, 0x448c0000    # 1120.0f

    const/4 v3, 0x0

    .line 54
    const/4 v0, 0x4

    new-array v0, v0, [Landroid/graphics/PointF;

    const/4 v1, 0x0

    new-instance v2, Landroid/graphics/PointF;

    invoke-direct {v2, v4, v3}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Landroid/graphics/PointF;

    invoke-direct {v2, v5, v3}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v2, v0, v1

    const/4 v1, 0x2

    new-instance v2, Landroid/graphics/PointF;

    invoke-direct {v2, v4, v3}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v2, v0, v1

    new-instance v1, Landroid/graphics/PointF;

    invoke-direct {v1, v5, v3}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v1, v0, v6

    sput-object v0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->BASE_BUTTONS_POS:[Landroid/graphics/PointF;

    .line 62
    new-array v0, v6, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mButtonVisibility:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x4
        0x4
        0x4
    .end array-data
.end method

.method public constructor <init>(Lcom/sec/android/app/camera/Camera;Lcom/sec/android/app/camera/subview/SubViewManager;)V
    .locals 2
    .param p1, "activitycontext"    # Lcom/sec/android/app/camera/Camera;
    .param p2, "subviewmanager"    # Lcom/sec/android/app/camera/subview/SubViewManager;

    .prologue
    const/4 v1, 0x3

    const/4 v0, 0x0

    .line 81
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/camera/subview/SubViewBase;-><init>(Lcom/sec/android/app/camera/Camera;Lcom/sec/android/app/camera/subview/SubViewManager;)V

    .line 69
    iput v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextImageResId:I

    .line 70
    iput v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextPressImageResId:I

    .line 71
    iput v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextDimImageResId:I

    .line 73
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mInitTouchPoint:Landroid/graphics/PointF;

    .line 76
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mShutterBtnResIds:[I

    .line 77
    new-array v0, v1, [I

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mRecordingBtnResIds:[I

    .line 78
    new-array v0, v1, [I

    fill-array-data v0, :array_2

    iput-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeBtnResIds:[I

    .line 178
    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu$1;-><init>(Lcom/sec/android/app/camera/subview/SubViewBaseMenu;)V

    iput-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mSubViewOnTouchListener:Landroid/view/View$OnTouchListener;

    .line 99
    invoke-virtual {p0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->initialize()V

    .line 100
    return-void

    .line 76
    :array_0
    .array-data 4
        0x7f020271
        0x7f020348
        0x7f020347
    .end array-data

    .line 77
    :array_1
    .array-data 4
        0x7f020276
        0x7f020298
        0x7f020297
    .end array-data

    .line 78
    :array_2
    .array-data 4
        0x0
        0x0
        0x0
    .end array-data
.end method

.method static synthetic access$000(Lcom/sec/android/app/camera/subview/SubViewBaseMenu;)Landroid/graphics/PointF;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mInitTouchPoint:Landroid/graphics/PointF;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/camera/subview/SubViewBaseMenu;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mShutterButton:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/camera/subview/SubViewBaseMenu;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mRecordingButton:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/camera/subview/SubViewBaseMenu;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeButton:Landroid/widget/ImageView;

    return-object v0
.end method

.method private getCurrentLocaleModeText()V
    .locals 10

    .prologue
    const v4, 0x7f02014f

    const v9, 0x10100a7

    const v8, 0x101009e

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 633
    new-instance v2, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v2}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 634
    .local v2, "states":Landroid/graphics/drawable/StateListDrawable;
    new-instance v0, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 635
    .local v0, "bgstates":Landroid/graphics/drawable/StateListDrawable;
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    .line 637
    .local v1, "currentLanguage":Ljava/lang/String;
    const v3, 0x7f020299

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextImageResId:I

    .line 638
    const v3, 0x7f02029b

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextPressImageResId:I

    .line 639
    const v3, 0x7f02029a

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextDimImageResId:I

    .line 641
    const-string v3, "as"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 642
    const v3, 0x7f02013d

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextImageResId:I

    .line 643
    const v3, 0x7f02013f

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextPressImageResId:I

    .line 644
    const v3, 0x7f02013e

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextDimImageResId:I

    .line 855
    :cond_0
    :goto_0
    iget v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextPressImageResId:I

    if-eqz v3, :cond_1

    .line 856
    new-array v3, v7, [I

    aput v9, v3, v6

    iget-object v4, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v4}, Lcom/sec/android/app/camera/Camera;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextPressImageResId:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 857
    new-array v3, v7, [I

    aput v9, v3, v6

    iget-object v4, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v4}, Lcom/sec/android/app/camera/Camera;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f02028d

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 859
    :cond_1
    iget v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextImageResId:I

    if-eqz v3, :cond_2

    .line 860
    new-array v3, v7, [I

    aput v8, v3, v6

    iget-object v4, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v4}, Lcom/sec/android/app/camera/Camera;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextImageResId:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 861
    new-array v3, v7, [I

    aput v8, v3, v6

    iget-object v4, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v4}, Lcom/sec/android/app/camera/Camera;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f02028b

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 863
    :cond_2
    iget v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextDimImageResId:I

    if-eqz v3, :cond_3

    .line 864
    new-array v3, v6, [I

    iget-object v4, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v4}, Lcom/sec/android/app/camera/Camera;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextDimImageResId:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 865
    new-array v3, v6, [I

    iget-object v4, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v4}, Lcom/sec/android/app/camera/Camera;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f02028c

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 868
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeButton:Landroid/widget/ImageView;

    if-eqz v3, :cond_4

    .line 869
    iget-object v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeButton:Landroid/widget/ImageView;

    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 870
    iget-object v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeButton:Landroid/widget/ImageView;

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 873
    :cond_4
    iget-object v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeBtnResIds:[I

    iget v4, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextImageResId:I

    aput v4, v3, v6

    .line 874
    iget-object v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeBtnResIds:[I

    iget v4, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextPressImageResId:I

    aput v4, v3, v7

    .line 875
    iget-object v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeBtnResIds:[I

    const/4 v4, 0x2

    iget v5, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextDimImageResId:I

    aput v5, v3, v4

    .line 876
    return-void

    .line 645
    :cond_5
    const-string v3, "bn"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 646
    const v3, 0x7f020146

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextImageResId:I

    .line 647
    const v3, 0x7f020148

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextPressImageResId:I

    .line 648
    const v3, 0x7f020147

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextDimImageResId:I

    goto/16 :goto_0

    .line 649
    :cond_6
    const-string v3, "az"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    const-string v3, "sq"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    const-string v3, "uz"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 650
    :cond_7
    const v3, 0x7f020140

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextImageResId:I

    .line 651
    const v3, 0x7f020142

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextPressImageResId:I

    .line 652
    const v3, 0x7f020141

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextDimImageResId:I

    goto/16 :goto_0

    .line 653
    :cond_8
    const-string v3, "bg"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_9

    const-string v3, "ru"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_9

    const-string v3, "ky"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 654
    :cond_9
    const v3, 0x7f020149

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextImageResId:I

    .line 655
    const v3, 0x7f02014b

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextPressImageResId:I

    .line 656
    const v3, 0x7f02014a

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextDimImageResId:I

    goto/16 :goto_0

    .line 657
    :cond_a
    const-string v3, "cs"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_b

    const-string v3, "sr"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_b

    const-string v3, "sk"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 658
    :cond_b
    iput v4, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextImageResId:I

    .line 659
    const v3, 0x7f020151

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextPressImageResId:I

    .line 660
    const v3, 0x7f020150

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextDimImageResId:I

    goto/16 :goto_0

    .line 661
    :cond_c
    const-string v3, "da"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_d

    .line 662
    const v3, 0x7f020152

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextImageResId:I

    .line 663
    const v3, 0x7f020154

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextPressImageResId:I

    .line 664
    const v3, 0x7f020153

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextDimImageResId:I

    goto/16 :goto_0

    .line 665
    :cond_d
    const-string v3, "de"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_e

    const-string v3, "no"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_e

    const-string v3, "nb"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_f

    .line 666
    :cond_e
    const v3, 0x7f02016a

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextImageResId:I

    .line 667
    const v3, 0x7f02016c

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextPressImageResId:I

    .line 668
    const v3, 0x7f02016b

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextDimImageResId:I

    goto/16 :goto_0

    .line 669
    :cond_f
    const-string v3, "en"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_10

    .line 670
    const v3, 0x7f020158

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextImageResId:I

    .line 671
    const v3, 0x7f02015a

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextPressImageResId:I

    .line 672
    const v3, 0x7f020159

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextDimImageResId:I

    goto/16 :goto_0

    .line 673
    :cond_10
    const-string v3, "et"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_11

    .line 674
    const v3, 0x7f02015b

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextImageResId:I

    .line 675
    const v3, 0x7f02015d

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextPressImageResId:I

    .line 676
    const v3, 0x7f02015c

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextDimImageResId:I

    goto/16 :goto_0

    .line 677
    :cond_11
    const-string v3, "eu"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_12

    .line 678
    const v3, 0x7f020143

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextImageResId:I

    .line 679
    const v3, 0x7f020145

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextPressImageResId:I

    .line 680
    const v3, 0x7f020144

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextDimImageResId:I

    goto/16 :goto_0

    .line 681
    :cond_12
    const-string v3, "fa"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_13

    .line 682
    const v3, 0x7f02015e

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextImageResId:I

    .line 683
    const v3, 0x7f020160

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextPressImageResId:I

    .line 684
    const v3, 0x7f02015f

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextDimImageResId:I

    goto/16 :goto_0

    .line 685
    :cond_13
    const-string v3, "fi"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_14

    .line 686
    const v3, 0x7f020161

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextImageResId:I

    .line 687
    const v3, 0x7f020163

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextPressImageResId:I

    .line 688
    const v3, 0x7f020162

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextDimImageResId:I

    goto/16 :goto_0

    .line 689
    :cond_14
    const-string v3, "ga"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_15

    .line 690
    const v3, 0x7f020185

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextImageResId:I

    .line 691
    const v3, 0x7f020187

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextPressImageResId:I

    .line 692
    const v3, 0x7f020186

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextDimImageResId:I

    goto/16 :goto_0

    .line 693
    :cond_15
    const-string v3, "gl"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_16

    const-string v3, "pt"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_16

    const-string v3, "it"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_16

    const-string v3, "es"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_17

    .line 694
    :cond_16
    const v3, 0x7f020164

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextImageResId:I

    .line 695
    const v3, 0x7f020166

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextPressImageResId:I

    .line 696
    const v3, 0x7f020165

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextDimImageResId:I

    goto/16 :goto_0

    .line 697
    :cond_17
    const-string v3, "gu"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_18

    .line 698
    const v3, 0x7f020170

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextImageResId:I

    .line 699
    const v3, 0x7f020172

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextPressImageResId:I

    .line 700
    const v3, 0x7f020171

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextDimImageResId:I

    goto/16 :goto_0

    .line 701
    :cond_18
    const-string v3, "he"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_19

    const-string v3, "iw"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1a

    .line 702
    :cond_19
    const v3, 0x7f020176

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextImageResId:I

    .line 703
    const v3, 0x7f020178

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextPressImageResId:I

    .line 704
    const v3, 0x7f020177

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextDimImageResId:I

    goto/16 :goto_0

    .line 705
    :cond_1a
    const-string v3, "hi"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1b

    const-string v3, "mr"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1b

    const-string v3, "ne"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1c

    .line 706
    :cond_1b
    const v3, 0x7f020179

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextImageResId:I

    .line 707
    const v3, 0x7f02017b

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextPressImageResId:I

    .line 708
    const v3, 0x7f02017a

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextDimImageResId:I

    goto/16 :goto_0

    .line 709
    :cond_1c
    const-string v3, "hr"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1d

    .line 710
    const v3, 0x7f0201b8

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextImageResId:I

    .line 711
    const v3, 0x7f0201ba

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextPressImageResId:I

    .line 712
    const v3, 0x7f0201b9

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextDimImageResId:I

    goto/16 :goto_0

    .line 713
    :cond_1d
    const-string v3, "hy"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1e

    .line 714
    const v3, 0x7f02013a

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextImageResId:I

    .line 715
    const v3, 0x7f02013c

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextPressImageResId:I

    .line 716
    const v3, 0x7f02013b

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextDimImageResId:I

    goto/16 :goto_0

    .line 717
    :cond_1e
    const-string v3, "ja"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1f

    .line 718
    const v3, 0x7f020188

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextImageResId:I

    .line 719
    const v3, 0x7f02018a

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextPressImageResId:I

    .line 720
    const v3, 0x7f020189

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextDimImageResId:I

    goto/16 :goto_0

    .line 721
    :cond_1f
    const-string v3, "ka"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_20

    .line 722
    const v3, 0x7f020167

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextImageResId:I

    .line 723
    const v3, 0x7f020169

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextPressImageResId:I

    .line 724
    const v3, 0x7f020168

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextDimImageResId:I

    goto/16 :goto_0

    .line 725
    :cond_20
    const-string v3, "km"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_21

    .line 726
    const v3, 0x7f020191

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextImageResId:I

    .line 727
    const v3, 0x7f020193

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextPressImageResId:I

    .line 728
    const v3, 0x7f020192

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextDimImageResId:I

    goto/16 :goto_0

    .line 729
    :cond_21
    const-string v3, "kn"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_22

    .line 730
    const v3, 0x7f02018b

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextImageResId:I

    .line 731
    const v3, 0x7f02018d

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextPressImageResId:I

    .line 732
    const v3, 0x7f02018c

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextDimImageResId:I

    goto/16 :goto_0

    .line 733
    :cond_22
    const-string v3, "ko"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_23

    .line 734
    const v3, 0x7f020194

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextImageResId:I

    .line 735
    const v3, 0x7f020196

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextPressImageResId:I

    .line 736
    const v3, 0x7f020195

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextDimImageResId:I

    goto/16 :goto_0

    .line 737
    :cond_23
    const-string v3, "lo"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_24

    .line 738
    const v3, 0x7f020197

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextImageResId:I

    .line 739
    const v3, 0x7f020199

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextPressImageResId:I

    .line 740
    const v3, 0x7f020198

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextDimImageResId:I

    goto/16 :goto_0

    .line 741
    :cond_24
    const-string v3, "lt"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_25

    .line 742
    const v3, 0x7f02019a

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextImageResId:I

    .line 743
    const v3, 0x7f02019c

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextPressImageResId:I

    .line 744
    const v3, 0x7f02019b

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextDimImageResId:I

    goto/16 :goto_0

    .line 745
    :cond_25
    const-string v3, "lv"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_26

    .line 746
    iput v4, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextImageResId:I

    .line 747
    const v3, 0x7f020151

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextPressImageResId:I

    .line 748
    const v3, 0x7f020150

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextDimImageResId:I

    goto/16 :goto_0

    .line 749
    :cond_26
    const-string v3, "mk"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_27

    .line 750
    const v3, 0x7f02019d

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextImageResId:I

    .line 751
    const v3, 0x7f02019f

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextPressImageResId:I

    .line 752
    const v3, 0x7f02019e

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextDimImageResId:I

    goto/16 :goto_0

    .line 753
    :cond_27
    const-string v3, "ml"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_28

    .line 754
    const v3, 0x7f0201a3

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextImageResId:I

    .line 755
    const v3, 0x7f0201a5

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextPressImageResId:I

    .line 756
    const v3, 0x7f0201a4

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextDimImageResId:I

    goto/16 :goto_0

    .line 757
    :cond_28
    const-string v3, "ms"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_29

    const-string v3, "tr"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_29

    const-string v3, "ro"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2a

    .line 758
    :cond_29
    const v3, 0x7f0201a0

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextImageResId:I

    .line 759
    const v3, 0x7f0201a2

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextPressImageResId:I

    .line 760
    const v3, 0x7f0201a1

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextDimImageResId:I

    goto/16 :goto_0

    .line 761
    :cond_2a
    const-string v3, "my"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2b

    .line 762
    const v3, 0x7f0201a9

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextImageResId:I

    .line 763
    const v3, 0x7f0201ab

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextPressImageResId:I

    .line 764
    const v3, 0x7f0201aa

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextDimImageResId:I

    goto/16 :goto_0

    .line 765
    :cond_2b
    const-string v3, "nl"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2c

    .line 766
    const v3, 0x7f020155

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextImageResId:I

    .line 767
    const v3, 0x7f020157

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextPressImageResId:I

    .line 768
    const v3, 0x7f020156

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextDimImageResId:I

    goto/16 :goto_0

    .line 769
    :cond_2c
    const-string v3, "or"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2d

    .line 770
    const v3, 0x7f0201ac

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextImageResId:I

    .line 771
    const v3, 0x7f0201ae

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextPressImageResId:I

    .line 772
    const v3, 0x7f0201ad

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextDimImageResId:I

    goto/16 :goto_0

    .line 773
    :cond_2d
    const-string v3, "pa"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2e

    .line 774
    const v3, 0x7f0201b2

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextImageResId:I

    .line 775
    const v3, 0x7f0201b4

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextPressImageResId:I

    .line 776
    const v3, 0x7f0201b3

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextDimImageResId:I

    goto/16 :goto_0

    .line 777
    :cond_2e
    const-string v3, "pl"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2f

    .line 778
    const v3, 0x7f0201af

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextImageResId:I

    .line 779
    const v3, 0x7f0201b1

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextPressImageResId:I

    .line 780
    const v3, 0x7f0201b0

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextDimImageResId:I

    goto/16 :goto_0

    .line 781
    :cond_2f
    const-string v3, "sl"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_30

    .line 782
    const v3, 0x7f0201b8

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextImageResId:I

    .line 783
    const v3, 0x7f0201ba

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextPressImageResId:I

    .line 784
    const v3, 0x7f0201b9

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextDimImageResId:I

    goto/16 :goto_0

    .line 785
    :cond_30
    const-string v3, "sv"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_31

    .line 786
    const v3, 0x7f0201bb

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextImageResId:I

    .line 787
    const v3, 0x7f0201bd

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextPressImageResId:I

    .line 788
    const v3, 0x7f0201bc

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextDimImageResId:I

    goto/16 :goto_0

    .line 789
    :cond_31
    const-string v3, "ta"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_32

    .line 790
    const v3, 0x7f0201c4

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextImageResId:I

    .line 791
    const v3, 0x7f0201c6

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextPressImageResId:I

    .line 792
    const v3, 0x7f0201c5

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextDimImageResId:I

    goto/16 :goto_0

    .line 793
    :cond_32
    const-string v3, "te"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_33

    .line 794
    const v3, 0x7f0201c7

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextImageResId:I

    .line 795
    const v3, 0x7f0201c9

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextPressImageResId:I

    .line 796
    const v3, 0x7f0201c8

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextDimImageResId:I

    goto/16 :goto_0

    .line 797
    :cond_33
    const-string v3, "th"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_34

    .line 798
    const v3, 0x7f0201ca

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextImageResId:I

    .line 799
    const v3, 0x7f0201cc

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextPressImageResId:I

    .line 800
    const v3, 0x7f0201cb

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextDimImageResId:I

    goto/16 :goto_0

    .line 801
    :cond_34
    const-string v3, "vi"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_35

    .line 802
    const v3, 0x7f0201d0

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextImageResId:I

    .line 803
    const v3, 0x7f0201d2

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextPressImageResId:I

    .line 804
    const v3, 0x7f0201d1

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextDimImageResId:I

    goto/16 :goto_0

    .line 805
    :cond_35
    const-string v3, "zh"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_36

    .line 806
    const v3, 0x7f02014c

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextImageResId:I

    .line 807
    const v3, 0x7f02014e

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextPressImageResId:I

    .line 808
    const v3, 0x7f02014d

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextDimImageResId:I

    goto/16 :goto_0

    .line 809
    :cond_36
    const-string v3, "ar"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_37

    const-string v3, "ur"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_38

    .line 810
    :cond_37
    const v3, 0x7f020137

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextImageResId:I

    .line 811
    const v3, 0x7f020139

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextPressImageResId:I

    .line 812
    const v3, 0x7f020138

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextDimImageResId:I

    goto/16 :goto_0

    .line 813
    :cond_38
    const-string v3, "el"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_39

    .line 814
    const v3, 0x7f02016d

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextImageResId:I

    .line 815
    const v3, 0x7f02016f

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextPressImageResId:I

    .line 816
    const v3, 0x7f02016e

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextDimImageResId:I

    goto/16 :goto_0

    .line 817
    :cond_39
    const-string v3, "ha"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3a

    .line 818
    const v3, 0x7f020173

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextImageResId:I

    .line 819
    const v3, 0x7f020175

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextPressImageResId:I

    .line 820
    const v3, 0x7f020174

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextDimImageResId:I

    goto/16 :goto_0

    .line 821
    :cond_3a
    const-string v3, "hu"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3b

    .line 822
    const v3, 0x7f02017c

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextImageResId:I

    .line 823
    const v3, 0x7f02017e

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextPressImageResId:I

    .line 824
    const v3, 0x7f02017d

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextDimImageResId:I

    goto/16 :goto_0

    .line 825
    :cond_3b
    const-string v3, "is"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3c

    .line 826
    const v3, 0x7f02017f

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextImageResId:I

    .line 827
    const v3, 0x7f020181

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextPressImageResId:I

    .line 828
    const v3, 0x7f020180

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextDimImageResId:I

    goto/16 :goto_0

    .line 829
    :cond_3c
    const-string v3, "ig"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3d

    .line 830
    const v3, 0x7f020182

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextImageResId:I

    .line 831
    const v3, 0x7f020184

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextPressImageResId:I

    .line 832
    const v3, 0x7f020183

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextDimImageResId:I

    goto/16 :goto_0

    .line 833
    :cond_3d
    const-string v3, "kk"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3e

    .line 834
    const v3, 0x7f02018e

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextImageResId:I

    .line 835
    const v3, 0x7f020190

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextPressImageResId:I

    .line 836
    const v3, 0x7f02018f

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextDimImageResId:I

    goto/16 :goto_0

    .line 837
    :cond_3e
    const-string v3, "si"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3f

    .line 838
    const v3, 0x7f0201b5

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextImageResId:I

    .line 839
    const v3, 0x7f0201b7

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextPressImageResId:I

    .line 840
    const v3, 0x7f0201b6

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextDimImageResId:I

    goto/16 :goto_0

    .line 841
    :cond_3f
    const-string v3, "sw"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_40

    .line 842
    const v3, 0x7f0201be

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextImageResId:I

    .line 843
    const v3, 0x7f0201c0

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextPressImageResId:I

    .line 844
    const v3, 0x7f0201bf

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextDimImageResId:I

    goto/16 :goto_0

    .line 845
    :cond_40
    const-string v3, "uk"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_41

    .line 846
    const v3, 0x7f0201cd

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextImageResId:I

    .line 847
    const v3, 0x7f0201cf

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextPressImageResId:I

    .line 848
    const v3, 0x7f0201ce

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextDimImageResId:I

    goto/16 :goto_0

    .line 849
    :cond_41
    const-string v3, "yo"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 850
    const v3, 0x7f0201d3

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextImageResId:I

    .line 851
    const v3, 0x7f0201d5

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextPressImageResId:I

    .line 852
    const v3, 0x7f0201d4

    iput v3, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeTextDimImageResId:I

    goto/16 :goto_0
.end method

.method private translateMenu()V
    .locals 3

    .prologue
    .line 369
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mCameraBaseMenu:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 370
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mCameraBaseMenu:Landroid/view/ViewGroup;

    sget-object v1, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->BASE_BUTTONS_POS:[Landroid/graphics/PointF;

    iget v2, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mLastOrientation:I

    div-int/lit8 v2, v2, 0x5a

    aget-object v1, v1, v2

    iget v1, v1, Landroid/graphics/PointF;->x:F

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setX(F)V

    .line 371
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mCameraBaseMenu:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->invalidate()V

    .line 373
    :cond_0
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 133
    iput-object v1, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mShutterButton:Landroid/widget/ImageView;

    .line 134
    iput-object v1, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mRecordingButton:Landroid/widget/ImageView;

    .line 135
    iput-object v1, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeButton:Landroid/widget/ImageView;

    .line 136
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mSideBar:Lcom/sec/android/app/camera/subview/SubViewSideBar;

    if-eqz v0, :cond_0

    .line 137
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mSideBar:Lcom/sec/android/app/camera/subview/SubViewSideBar;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewSideBar;->clear()V

    .line 138
    iput-object v1, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mSideBar:Lcom/sec/android/app/camera/subview/SubViewSideBar;

    .line 140
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mFrontSideBar:Lcom/sec/android/app/camera/subview/SubViewSideBar;

    if-eqz v0, :cond_1

    .line 141
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mFrontSideBar:Lcom/sec/android/app/camera/subview/SubViewSideBar;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewSideBar;->clear()V

    .line 142
    iput-object v1, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mFrontSideBar:Lcom/sec/android/app/camera/subview/SubViewSideBar;

    .line 144
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mIndicators:Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;

    if-eqz v0, :cond_2

    .line 145
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mIndicators:Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->clear()V

    .line 146
    iput-object v1, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mIndicators:Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;

    .line 148
    :cond_2
    invoke-super {p0}, Lcom/sec/android/app/camera/subview/SubViewBase;->clear()V

    .line 149
    return-void
.end method

.method public getBaseIndicators()Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mIndicators:Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;

    return-object v0
.end method

.method public getEditableSideBar()Lcom/sec/android/app/camera/subview/SubViewEditableSideBar;
    .locals 1

    .prologue
    .line 156
    invoke-virtual {p0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->getSideBar()Lcom/sec/android/app/camera/subview/SubViewSideBar;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 157
    invoke-virtual {p0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->getSideBar()Lcom/sec/android/app/camera/subview/SubViewSideBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewSideBar;->getEditableSideBar()Lcom/sec/android/app/camera/subview/SubViewEditableSideBar;

    move-result-object v0

    .line 159
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getEditableSideBar(I)Lcom/sec/android/app/camera/subview/SubViewEditableSideBar;
    .locals 1
    .param p1, "command"    # I

    .prologue
    .line 163
    const/16 v0, 0x42

    if-ne p1, v0, :cond_0

    .line 164
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mSideBar:Lcom/sec/android/app/camera/subview/SubViewSideBar;

    if-eqz v0, :cond_1

    .line 165
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mSideBar:Lcom/sec/android/app/camera/subview/SubViewSideBar;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewSideBar;->getEditableSideBar()Lcom/sec/android/app/camera/subview/SubViewEditableSideBar;

    move-result-object v0

    .line 175
    :goto_0
    return-object v0

    .line 167
    :cond_0
    const/16 v0, 0x75

    if-ne p1, v0, :cond_1

    .line 168
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mFrontSideBar:Lcom/sec/android/app/camera/subview/SubViewSideBar;

    if-eqz v0, :cond_1

    .line 169
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mFrontSideBar:Lcom/sec/android/app/camera/subview/SubViewSideBar;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewSideBar;->getEditableSideBar()Lcom/sec/android/app/camera/subview/SubViewEditableSideBar;

    move-result-object v0

    goto :goto_0

    .line 172
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->getSideBar()Lcom/sec/android/app/camera/subview/SubViewSideBar;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 173
    invoke-virtual {p0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->getSideBar()Lcom/sec/android/app/camera/subview/SubViewSideBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewSideBar;->getEditableSideBar()Lcom/sec/android/app/camera/subview/SubViewEditableSideBar;

    move-result-object v0

    goto :goto_0

    .line 175
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSideBar()Lcom/sec/android/app/camera/subview/SubViewSideBar;
    .locals 1

    .prologue
    .line 919
    iget-boolean v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mEasyMode:Z

    if-eqz v0, :cond_0

    .line 920
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mEasyModeSideBar:Lcom/sec/android/app/camera/subview/SubViewSideBar;

    .line 922
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mSideBar:Lcom/sec/android/app/camera/subview/SubViewSideBar;

    goto :goto_0
.end method

.method public handleDimButtons()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 425
    const-string v0, "SubViewBaseMenu"

    const-string v1, "handleDimButtons"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 426
    iget-boolean v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mbFocused:Z

    if-eqz v0, :cond_1

    .line 476
    :cond_0
    :goto_0
    return-void

    .line 429
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mShutterButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    .line 430
    const/16 v0, 0x20

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->getDim(I)Z

    move-result v0

    if-ne v0, v2, :cond_7

    .line 431
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mShutterButton:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mShutterBtnResIds:[I

    invoke-virtual {p0, v0, v3, v1}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->setImageresource(Landroid/widget/ImageView;Z[I)V

    .line 432
    invoke-virtual {p0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->isLandscape()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 433
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mShutterButton:Landroid/widget/ImageView;

    const v1, 0x7f020341

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 436
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mShutterButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 437
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mShutterButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setClickable(Z)V

    .line 448
    :cond_2
    :goto_2
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mRecordingButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_3

    .line 449
    const/16 v0, 0x41

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->getDim(I)Z

    move-result v0

    if-ne v0, v2, :cond_9

    .line 450
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mRecordingButton:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mRecordingBtnResIds:[I

    invoke-virtual {p0, v0, v3, v1}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->setImageresource(Landroid/widget/ImageView;Z[I)V

    .line 451
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mRecordingButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 452
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mRecordingButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setClickable(Z)V

    .line 459
    :cond_3
    :goto_3
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_4

    .line 460
    invoke-virtual {p0, v2}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->getDim(I)Z

    move-result v0

    if-ne v0, v2, :cond_a

    .line 461
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeButton:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeBtnResIds:[I

    invoke-virtual {p0, v0, v3, v1}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->setImageresource(Landroid/widget/ImageView;Z[I)V

    .line 462
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 463
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setClickable(Z)V

    .line 470
    :cond_4
    :goto_4
    invoke-virtual {p0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->getSideBar()Lcom/sec/android/app/camera/subview/SubViewSideBar;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 471
    invoke-virtual {p0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->getSideBar()Lcom/sec/android/app/camera/subview/SubViewSideBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewSideBar;->handleDimButtons()V

    .line 473
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mIndicators:Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;

    if-eqz v0, :cond_0

    .line 474
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mIndicators:Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->handleDimButtons()V

    goto :goto_0

    .line 435
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mShutterButton:Landroid/widget/ImageView;

    const v1, 0x7f020345

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_1

    .line 438
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mShutterButton:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->isClickable()Z

    move-result v0

    if-nez v0, :cond_2

    .line 439
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mShutterButton:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mShutterBtnResIds:[I

    invoke-virtual {p0, v0, v2, v1}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->setImageresource(Landroid/widget/ImageView;Z[I)V

    .line 440
    invoke-virtual {p0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->isLandscape()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 441
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mShutterButton:Landroid/widget/ImageView;

    const v1, 0x7f020340

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 444
    :goto_5
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mShutterButton:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mSubViewOnTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 445
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mShutterButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setClickable(Z)V

    goto/16 :goto_2

    .line 443
    :cond_8
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mShutterButton:Landroid/widget/ImageView;

    const v1, 0x7f020344

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_5

    .line 453
    :cond_9
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mRecordingButton:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->isClickable()Z

    move-result v0

    if-nez v0, :cond_3

    .line 454
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mRecordingButton:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mRecordingBtnResIds:[I

    invoke-virtual {p0, v0, v2, v1}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->setImageresource(Landroid/widget/ImageView;Z[I)V

    .line 455
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mRecordingButton:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mSubViewOnTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 456
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mRecordingButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setClickable(Z)V

    goto/16 :goto_3

    .line 464
    :cond_a
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeButton:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->isClickable()Z

    move-result v0

    if-nez v0, :cond_4

    .line 465
    invoke-direct {p0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->getCurrentLocaleModeText()V

    .line 466
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeButton:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mSubViewOnTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 467
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setClickable(Z)V

    goto/16 :goto_4
.end method

.method public handleDimButtons(Z)V
    .locals 4
    .param p1, "bFocus"    # Z

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 480
    iput-boolean p1, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mbFocused:Z

    .line 481
    if-eqz p1, :cond_4

    .line 482
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mShutterButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 483
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mShutterButton:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mShutterBtnResIds:[I

    invoke-virtual {p0, v0, v2, v1}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->setImageresource(Landroid/widget/ImageView;Z[I)V

    .line 484
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mShutterButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 485
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mShutterButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setClickable(Z)V

    .line 487
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mRecordingButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 488
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mRecordingButton:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mRecordingBtnResIds:[I

    invoke-virtual {p0, v0, v2, v1}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->setImageresource(Landroid/widget/ImageView;Z[I)V

    .line 489
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mRecordingButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 490
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mRecordingButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setClickable(Z)V

    .line 492
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    .line 493
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeButton:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeBtnResIds:[I

    invoke-virtual {p0, v0, v2, v1}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->setImageresource(Landroid/widget/ImageView;Z[I)V

    .line 494
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 495
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setClickable(Z)V

    .line 501
    :cond_2
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->getSideBar()Lcom/sec/android/app/camera/subview/SubViewSideBar;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 502
    invoke-virtual {p0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->getSideBar()Lcom/sec/android/app/camera/subview/SubViewSideBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/camera/subview/SubViewSideBar;->handleDimButtons(Z)V

    .line 504
    :cond_3
    return-void

    .line 498
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->handleDimButtons()V

    goto :goto_0
.end method

.method public hideBaseMenu()V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 559
    invoke-virtual {p0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->getSideBar()Lcom/sec/android/app/camera/subview/SubViewSideBar;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 560
    invoke-virtual {p0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->getSideBar()Lcom/sec/android/app/camera/subview/SubViewSideBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewSideBar;->hideSideBar()V

    .line 562
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mShutterButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 563
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mShutterButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 565
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mRecordingButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    .line 566
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mRecordingButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 568
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_3

    .line 569
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 571
    :cond_3
    invoke-super {p0}, Lcom/sec/android/app/camera/subview/SubViewBase;->onHide()V

    .line 572
    return-void
.end method

.method public hideBaseMenuForBestShot()V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 575
    invoke-virtual {p0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->getSideBar()Lcom/sec/android/app/camera/subview/SubViewSideBar;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 576
    invoke-virtual {p0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->getSideBar()Lcom/sec/android/app/camera/subview/SubViewSideBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewSideBar;->hideSideBar()V

    .line 578
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mRecordingButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 579
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mRecordingButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 581
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    .line 582
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 584
    :cond_2
    invoke-super {p0}, Lcom/sec/android/app/camera/subview/SubViewBase;->onHide()V

    .line 585
    return-void
.end method

.method public hideBaseMenuForEditQuickSettings()V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 617
    invoke-virtual {p0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->getSideBar()Lcom/sec/android/app/camera/subview/SubViewSideBar;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 618
    invoke-virtual {p0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->getSideBar()Lcom/sec/android/app/camera/subview/SubViewSideBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewSideBar;->hideSideBarForEditQuickSettings()V

    .line 620
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mShutterButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 621
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mShutterButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 623
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mRecordingButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    .line 624
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mRecordingButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 626
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_3

    .line 627
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 629
    :cond_3
    invoke-super {p0}, Lcom/sec/android/app/camera/subview/SubViewBase;->onHide()V

    .line 630
    return-void
.end method

.method public hideBaseMenuForRecording()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 588
    invoke-virtual {p0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->getSideBar()Lcom/sec/android/app/camera/subview/SubViewSideBar;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 589
    invoke-virtual {p0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->getSideBar()Lcom/sec/android/app/camera/subview/SubViewSideBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewSideBar;->hideSideBar()V

    .line 591
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mShutterButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 592
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mShutterButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 594
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mRecordingButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    .line 595
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mRecordingButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 597
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_3

    .line 598
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 600
    :cond_3
    invoke-super {p0}, Lcom/sec/android/app/camera/subview/SubViewBase;->onHide()V

    .line 601
    return-void
.end method

.method public hideBaseMenuForShootingMode()V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 604
    invoke-virtual {p0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->getSideBar()Lcom/sec/android/app/camera/subview/SubViewSideBar;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 605
    invoke-virtual {p0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->getSideBar()Lcom/sec/android/app/camera/subview/SubViewSideBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewSideBar;->hideSideBar()V

    .line 607
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mShutterButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 608
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mShutterButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 610
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mRecordingButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    .line 611
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mRecordingButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 613
    :cond_2
    invoke-super {p0}, Lcom/sec/android/app/camera/subview/SubViewBase;->onHide()V

    .line 614
    return-void
.end method

.method public initLayout()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 108
    invoke-super {p0}, Lcom/sec/android/app/camera/subview/SubViewBase;->initLayout()V

    .line 109
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mBaseLayout:Landroid/view/ViewGroup;

    const v1, 0x7f0e004d

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mCameraBaseMenu:Landroid/view/ViewGroup;

    .line 110
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mCameraBaseMenu:Landroid/view/ViewGroup;

    if-eqz v0, :cond_3

    .line 111
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mCameraBaseMenu:Landroid/view/ViewGroup;

    const v1, 0x7f0e004e

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mShutterButton:Landroid/widget/ImageView;

    .line 112
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mShutterButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 113
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mShutterButton:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mSubViewOnTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 114
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mShutterButton:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mSubViewOnHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 115
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mShutterButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setSoundEffectsEnabled(Z)V

    .line 117
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mCameraBaseMenu:Landroid/view/ViewGroup;

    const v1, 0x7f0e004f

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mRecordingButton:Landroid/widget/ImageView;

    .line 118
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mRecordingButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 119
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mRecordingButton:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mSubViewOnTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 120
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mRecordingButton:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mSubViewOnHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 121
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mRecordingButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setSoundEffectsEnabled(Z)V

    .line 123
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mCameraBaseMenu:Landroid/view/ViewGroup;

    const v1, 0x7f0e0050

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeButton:Landroid/widget/ImageView;

    .line 124
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    .line 125
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeButton:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mSubViewOnTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 126
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeButton:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mSubViewOnHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 128
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->getCurrentLocaleModeText()V

    .line 130
    :cond_3
    return-void
.end method

.method public initialize()V
    .locals 2

    .prologue
    .line 103
    const-string v0, "SubViewBaseMenu"

    const-string v1, "initialize"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    invoke-virtual {p0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->initLayout()V

    .line 105
    return-void
.end method

.method public isShutterPressed()Z
    .locals 1

    .prologue
    .line 879
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mShutterButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 880
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mShutterButton:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->isPressed()Z

    move-result v0

    .line 882
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCameraSettingsChanged(II)V
    .locals 0
    .param p1, "menuid"    # I
    .param p2, "modeid"    # I

    .prologue
    .line 1003
    return-void
.end method

.method public onHide()V
    .locals 0

    .prologue
    .line 518
    invoke-virtual {p0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->hideBaseMenu()V

    .line 519
    invoke-super {p0}, Lcom/sec/android/app/camera/subview/SubViewBase;->onHide()V

    .line 520
    return-void
.end method

.method public onRestoreLayoutStatus()V
    .locals 3

    .prologue
    .line 412
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mShutterButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 413
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mShutterButton:Landroid/widget/ImageView;

    sget-object v1, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mButtonVisibility:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 415
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mRecordingButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 416
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mRecordingButton:Landroid/widget/ImageView;

    sget-object v1, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mButtonVisibility:[I

    const/4 v2, 0x1

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 418
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    .line 419
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeButton:Landroid/widget/ImageView;

    sget-object v1, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mButtonVisibility:[I

    const/4 v2, 0x2

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 421
    :cond_2
    return-void
.end method

.method public onSaveLayoutStatus()V
    .locals 3

    .prologue
    .line 400
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mShutterButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 401
    sget-object v0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mButtonVisibility:[I

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mShutterButton:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getVisibility()I

    move-result v2

    aput v2, v0, v1

    .line 403
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mRecordingButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 404
    sget-object v0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mButtonVisibility:[I

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mRecordingButton:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getVisibility()I

    move-result v2

    aput v2, v0, v1

    .line 406
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    .line 407
    sget-object v0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mButtonVisibility:[I

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeButton:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getVisibility()I

    move-result v2

    aput v2, v0, v1

    .line 409
    :cond_2
    return-void
.end method

.method public onShow()V
    .locals 0

    .prologue
    .line 513
    invoke-virtual {p0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->showBaseMenu()V

    .line 514
    invoke-super {p0}, Lcom/sec/android/app/camera/subview/SubViewBase;->onShow()V

    .line 515
    return-void
.end method

.method public refreshShortcutMenu()V
    .locals 1

    .prologue
    .line 507
    invoke-virtual {p0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->getSideBar()Lcom/sec/android/app/camera/subview/SubViewSideBar;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 508
    invoke-virtual {p0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->getSideBar()Lcom/sec/android/app/camera/subview/SubViewSideBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewSideBar;->refreshShortcutMenu()V

    .line 510
    :cond_0
    return-void
.end method

.method public resetCameraSideBar()V
    .locals 2

    .prologue
    .line 926
    iget-boolean v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mEasyMode:Z

    if-eqz v0, :cond_2

    .line 950
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mEasyModeSideBar:Lcom/sec/android/app/camera/subview/SubViewSideBar;

    if-eqz v0, :cond_0

    .line 951
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mEasyModeSideBar:Lcom/sec/android/app/camera/subview/SubViewSideBar;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewSideBar;->refreshShortcutMenu()V

    .line 952
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mEasyModeSideBar:Lcom/sec/android/app/camera/subview/SubViewSideBar;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewSideBar;->onShow()V

    .line 983
    :cond_0
    :goto_0
    iget-boolean v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mbFocused:Z

    if-eqz v0, :cond_1

    .line 984
    invoke-virtual {p0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->getSideBar()Lcom/sec/android/app/camera/subview/SubViewSideBar;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 985
    invoke-virtual {p0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->getSideBar()Lcom/sec/android/app/camera/subview/SubViewSideBar;

    move-result-object v0

    iget-boolean v1, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mbFocused:Z

    invoke-virtual {v0, v1}, Lcom/sec/android/app/camera/subview/SubViewSideBar;->handleDimButtons(Z)V

    .line 988
    :cond_1
    return-void

    .line 978
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mSideBar:Lcom/sec/android/app/camera/subview/SubViewSideBar;

    if-eqz v0, :cond_0

    .line 979
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mSideBar:Lcom/sec/android/app/camera/subview/SubViewSideBar;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewSideBar;->refreshShortcutMenu()V

    .line 980
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mSideBar:Lcom/sec/android/app/camera/subview/SubViewSideBar;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewSideBar;->onShow()V

    goto :goto_0
.end method

.method public rotateLayout()V
    .locals 2

    .prologue
    .line 384
    invoke-virtual {p0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->onSaveLayoutStatus()V

    .line 385
    invoke-virtual {p0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->initLayout()V

    .line 386
    invoke-virtual {p0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->setBackGround()V

    .line 387
    invoke-virtual {p0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->getSideBar()Lcom/sec/android/app/camera/subview/SubViewSideBar;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 388
    invoke-virtual {p0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->getSideBar()Lcom/sec/android/app/camera/subview/SubViewSideBar;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mDisplayOrientation:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/camera/subview/SubViewSideBar;->setOrientation(I)V

    .line 389
    invoke-virtual {p0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->getSideBar()Lcom/sec/android/app/camera/subview/SubViewSideBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewSideBar;->rotateLayout()V

    .line 391
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mIndicators:Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;

    if-eqz v0, :cond_1

    .line 392
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mIndicators:Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;

    iget v1, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mDisplayOrientation:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->setOrientation(I)V

    .line 393
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mIndicators:Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->rotateLayout()V

    .line 395
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->handleDimButtons()V

    .line 396
    invoke-virtual {p0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->onRestoreLayoutStatus()V

    .line 397
    return-void
.end method

.method public rotateMenu()V
    .locals 2

    .prologue
    .line 355
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getEngine()Lcom/sec/android/app/camera/CommonEngine;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CommonEngine;->isRecording()Z

    move-result v0

    if-nez v0, :cond_0

    .line 356
    invoke-direct {p0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->translateMenu()V

    .line 357
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mShutterButton:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->rotateImagesOnLandscape(Landroid/widget/ImageView;)V

    .line 358
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mRecordingButton:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->rotateImagesOnLandscape(Landroid/widget/ImageView;)V

    .line 359
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeButton:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->rotateImagesOnLandscape(Landroid/widget/ImageView;)V

    .line 360
    invoke-virtual {p0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->getSideBar()Lcom/sec/android/app/camera/subview/SubViewSideBar;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 361
    invoke-virtual {p0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->getSideBar()Lcom/sec/android/app/camera/subview/SubViewSideBar;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mOrientationMainLCD:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/camera/subview/SubViewSideBar;->setFixedOrientation(I)V

    .line 362
    invoke-virtual {p0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->getSideBar()Lcom/sec/android/app/camera/subview/SubViewSideBar;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mLastOrientation:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/camera/subview/SubViewSideBar;->setLastOrientation(I)V

    .line 363
    invoke-virtual {p0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->getSideBar()Lcom/sec/android/app/camera/subview/SubViewSideBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewSideBar;->rotateMenu()V

    .line 366
    :cond_0
    return-void
.end method

.method public setShutterBtnPressed(Z)V
    .locals 1
    .param p1, "pressed"    # Z

    .prologue
    .line 886
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mShutterButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 887
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mShutterButton:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setPressed(Z)V

    .line 889
    :cond_0
    return-void
.end method

.method public setShutterButtonDimmed(Z)V
    .locals 3
    .param p1, "dim"    # Z

    .prologue
    .line 891
    const-string v0, "SubViewBaseMenu"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setShutterButtonDimmed:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 892
    if-eqz p1, :cond_1

    .line 893
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mShutterButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 894
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mShutterButton:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 901
    :cond_0
    :goto_0
    return-void

    .line 897
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mShutterButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 898
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mShutterButton:Landroid/widget/ImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    goto :goto_0
.end method

.method public showBaseMenu()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 523
    invoke-virtual {p0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->setBackGround()V

    .line 524
    invoke-virtual {p0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->getSideBar()Lcom/sec/android/app/camera/subview/SubViewSideBar;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 525
    invoke-virtual {p0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->getSideBar()Lcom/sec/android/app/camera/subview/SubViewSideBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewSideBar;->showSideBar()V

    .line 527
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mShutterButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 528
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mShutterButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 530
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mRecordingButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    .line 531
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mRecordingButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 533
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_3

    .line 534
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 536
    :cond_3
    iget-boolean v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mbLandscapeMode:Z

    if-eqz v0, :cond_4

    .line 537
    invoke-virtual {p0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->rotateMenu()V

    .line 539
    :cond_4
    invoke-super {p0}, Lcom/sec/android/app/camera/subview/SubViewBase;->onShow()V

    .line 540
    return-void
.end method

.method public showBaseMenuForEditQuickSettings()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 543
    invoke-virtual {p0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->getSideBar()Lcom/sec/android/app/camera/subview/SubViewSideBar;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 544
    invoke-virtual {p0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->getSideBar()Lcom/sec/android/app/camera/subview/SubViewSideBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewSideBar;->showSideBarForEditQuickSettings()V

    .line 546
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mShutterButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 547
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mShutterButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 549
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mRecordingButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    .line 550
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mRecordingButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 552
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_3

    .line 553
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mModeButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 555
    :cond_3
    invoke-super {p0}, Lcom/sec/android/app/camera/subview/SubViewBase;->onShow()V

    .line 556
    return-void
.end method

.method public translateMenu2()V
    .locals 3

    .prologue
    .line 377
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mCameraBaseMenu:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 378
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mCameraBaseMenu:Landroid/view/ViewGroup;

    sget-object v1, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->BASE_BUTTONS_POS:[Landroid/graphics/PointF;

    iget v2, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mLastOrientation:I

    div-int/lit8 v2, v2, 0x5a

    aget-object v1, v1, v2

    iget v1, v1, Landroid/graphics/PointF;->x:F

    const/high16 v2, 0x44be0000    # 1520.0f

    add-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setX(F)V

    .line 379
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->mCameraBaseMenu:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->invalidate()V

    .line 381
    :cond_0
    return-void
.end method
