.class public Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;
.super Ljava/lang/Object;
.source "SubViewResourceIDMap.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;
    }
.end annotation


# instance fields
.field protected mResourceIDs:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 15

    .prologue
    const v10, 0x7f0200ab

    const v14, 0x7f0c00f0

    const v13, 0x7f0c00f1

    const v12, 0x7f0203eb

    const/4 v7, 0x0

    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    .line 70
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x24

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f02029f

    const v3, 0x7f0202a1

    const v4, 0x7f0202a0

    const v5, 0x7f0c0274

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x6f

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f0200f9

    const v3, 0x7f0200fb

    const v4, 0x7f0200fa

    const v5, 0x7f0c0122

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x1b

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f02029c

    const v3, 0x7f02029e

    const v4, 0x7f02029d

    const v5, 0x7f0c0020

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    iget-object v8, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x8

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f020123

    const v3, 0x7f020125

    const v4, 0x7f020124

    const v5, 0x7f0c00f7

    const v6, 0x7f0c0291

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIIIII)V

    invoke-virtual {v8, v9, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 82
    iget-object v8, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x1e

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f02031c

    const v3, 0x7f02031e

    const v4, 0x7f02031d

    const v5, 0x7f0c010c

    const v6, 0x7f0c02a8

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIIIII)V

    invoke-virtual {v8, v9, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 86
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0xc8

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f02029f

    const v3, 0x7f0202a1

    const v4, 0x7f0202a0

    const v5, 0x7f0c0274

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 87
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0xc9

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f02029f

    const v3, 0x7f0202a1

    const v4, 0x7f0202a0

    const v5, 0x7f0c0274

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x1a2c

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f020354

    const v3, 0x7f020356

    const v4, 0x7f020355

    move-object v1, p0

    move v5, v13

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x1a2d

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f020357

    const v3, 0x7f020359

    const v4, 0x7f020358

    move-object v1, p0

    move v5, v14

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 94
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x1bbc

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f0202a3

    const v3, 0x7f0202a5

    const v4, 0x7f0202a4

    move-object v1, p0

    move v5, v13

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x1bbd

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f0202a6

    const v3, 0x7f0202a8

    const v4, 0x7f0202a7

    move-object v1, p0

    move v5, v14

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    const/16 v0, 0x12c

    invoke-static {v0}, Lcom/sec/android/app/camera/PlugInShootingModesLoader;->isShootingModeExternal(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 99
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x12c

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v4, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v6, 0x7f02045a

    const v9, 0x7f0c0035

    move-object v5, p0

    move v8, v7

    invoke-direct/range {v4 .. v9}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 101
    :cond_0
    const/16 v0, 0x12d

    invoke-static {v0}, Lcom/sec/android/app/camera/PlugInShootingModesLoader;->isShootingModeExternal(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 102
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x12d

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v4, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v6, 0x7f02045d

    const v9, 0x7f0c0028

    move-object v5, p0

    move v8, v7

    invoke-direct/range {v4 .. v9}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 104
    :cond_1
    const/16 v0, 0x12e

    invoke-static {v0}, Lcom/sec/android/app/camera/PlugInShootingModesLoader;->isShootingModeExternal(I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 105
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x12e

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v4, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v6, 0x7f02046c

    const v9, 0x7f0c0029

    move-object v5, p0

    move v8, v7

    invoke-direct/range {v4 .. v9}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    :cond_2
    const/16 v0, 0x12f

    invoke-static {v0}, Lcom/sec/android/app/camera/PlugInShootingModesLoader;->isShootingModeExternal(I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 108
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x12f

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v4, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v6, 0x7f020133

    const v9, 0x7f0c002a

    move-object v5, p0

    move v8, v7

    invoke-direct/range {v4 .. v9}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 110
    :cond_3
    const/16 v0, 0x130

    invoke-static {v0}, Lcom/sec/android/app/camera/PlugInShootingModesLoader;->isShootingModeExternal(I)Z

    move-result v0

    if-nez v0, :cond_4

    .line 111
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x130

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v4, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const/high16 v9, 0x7f0c0000

    move-object v5, p0

    move v6, v12

    move v8, v7

    invoke-direct/range {v4 .. v9}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 113
    :cond_4
    const/16 v0, 0x131

    invoke-static {v0}, Lcom/sec/android/app/camera/PlugInShootingModesLoader;->isShootingModeExternal(I)Z

    move-result v0

    if-nez v0, :cond_5

    .line 114
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x131

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v4, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v9, 0x7f0c002b

    move-object v5, p0

    move v6, v12

    move v8, v7

    invoke-direct/range {v4 .. v9}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    :cond_5
    const/16 v0, 0x133

    invoke-static {v0}, Lcom/sec/android/app/camera/PlugInShootingModesLoader;->isShootingModeExternal(I)Z

    move-result v0

    if-nez v0, :cond_6

    .line 117
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x133

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v4, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v6, 0x7f02045b

    const v9, 0x7f0c002c

    move-object v5, p0

    move v8, v7

    invoke-direct/range {v4 .. v9}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 119
    :cond_6
    const/16 v0, 0x139

    invoke-static {v0}, Lcom/sec/android/app/camera/PlugInShootingModesLoader;->isShootingModeExternal(I)Z

    move-result v0

    if-nez v0, :cond_7

    .line 120
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x139

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v4, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v6, 0x7f020132

    const v9, 0x7f0c0030

    move-object v5, p0

    move v8, v7

    invoke-direct/range {v4 .. v9}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 122
    :cond_7
    const/16 v0, 0x13a

    invoke-static {v0}, Lcom/sec/android/app/camera/PlugInShootingModesLoader;->isShootingModeExternal(I)Z

    move-result v0

    if-nez v0, :cond_8

    .line 123
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x13a

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v4, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v6, 0x7f02046d

    const v9, 0x7f0c0036

    move-object v5, p0

    move v8, v7

    invoke-direct/range {v4 .. v9}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 125
    :cond_8
    const/16 v0, 0x13c

    invoke-static {v0}, Lcom/sec/android/app/camera/PlugInShootingModesLoader;->isShootingModeExternal(I)Z

    move-result v0

    if-nez v0, :cond_9

    .line 129
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x13c

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v4, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v6, 0x7f02045d

    const v9, 0x7f0c0120

    move-object v5, p0

    move v8, v7

    invoke-direct/range {v4 .. v9}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 132
    :cond_9
    const/16 v0, 0x13f

    invoke-static {v0}, Lcom/sec/android/app/camera/PlugInShootingModesLoader;->isShootingModeExternal(I)Z

    move-result v0

    if-nez v0, :cond_a

    .line 133
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x13f

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v4, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v9, 0x7f0c0033

    move-object v5, p0

    move v6, v12

    move v8, v7

    invoke-direct/range {v4 .. v9}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 135
    :cond_a
    const/16 v0, 0x13e

    invoke-static {v0}, Lcom/sec/android/app/camera/PlugInShootingModesLoader;->isShootingModeExternal(I)Z

    move-result v0

    if-nez v0, :cond_b

    .line 136
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x13e

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v4, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v9, 0x7f0c00db

    move-object v5, p0

    move v6, v12

    move v8, v7

    invoke-direct/range {v4 .. v9}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 138
    :cond_b
    const/16 v0, 0x140

    invoke-static {v0}, Lcom/sec/android/app/camera/PlugInShootingModesLoader;->isShootingModeExternal(I)Z

    move-result v0

    if-nez v0, :cond_c

    .line 139
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x140

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v4, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v9, 0x7f0c0031

    move-object v5, p0

    move v6, v12

    move v8, v7

    invoke-direct/range {v4 .. v9}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 141
    :cond_c
    const/16 v0, 0x141

    invoke-static {v0}, Lcom/sec/android/app/camera/PlugInShootingModesLoader;->isShootingModeExternal(I)Z

    move-result v0

    if-nez v0, :cond_d

    .line 145
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x141

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v4, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v9, 0x7f0c0032

    move-object v5, p0

    move v6, v12

    move v8, v7

    invoke-direct/range {v4 .. v9}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 148
    :cond_d
    const/16 v0, 0x143

    invoke-static {v0}, Lcom/sec/android/app/camera/PlugInShootingModesLoader;->isShootingModeExternal(I)Z

    move-result v0

    if-nez v0, :cond_e

    .line 149
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x143

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v4, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v9, 0x7f0c0034

    move-object v5, p0

    move v6, v12

    move v8, v7

    invoke-direct/range {v4 .. v9}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 151
    :cond_e
    const/16 v0, 0x148

    invoke-static {v0}, Lcom/sec/android/app/camera/PlugInShootingModesLoader;->isShootingModeExternal(I)Z

    move-result v0

    if-nez v0, :cond_f

    .line 152
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x148

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v4, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v9, 0x7f0c0038

    move-object v5, p0

    move v6, v12

    move v8, v7

    invoke-direct/range {v4 .. v9}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 154
    :cond_f
    const/16 v0, 0x149

    invoke-static {v0}, Lcom/sec/android/app/camera/PlugInShootingModesLoader;->isShootingModeExternal(I)Z

    move-result v0

    if-nez v0, :cond_10

    .line 155
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x149

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v4, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v6, 0x7f020472

    const v9, 0x7f0c003b

    move-object v5, p0

    move v8, v7

    invoke-direct/range {v4 .. v9}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 157
    :cond_10
    const/16 v0, 0x14b

    invoke-static {v0}, Lcom/sec/android/app/camera/PlugInShootingModesLoader;->isShootingModeExternal(I)Z

    move-result v0

    if-nez v0, :cond_11

    .line 158
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x14b

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v4, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v6, 0x7f020475

    const v9, 0x7f0c003c

    move-object v5, p0

    move v8, v7

    invoke-direct/range {v4 .. v9}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 160
    :cond_11
    const/16 v0, 0x14c

    invoke-static {v0}, Lcom/sec/android/app/camera/PlugInShootingModesLoader;->isShootingModeExternal(I)Z

    move-result v0

    if-nez v0, :cond_12

    .line 161
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x14c

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v4, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v6, 0x7f020458

    const v9, 0x7f0c003d

    move-object v5, p0

    move v8, v7

    invoke-direct/range {v4 .. v9}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 163
    :cond_12
    const/16 v0, 0x14d

    invoke-static {v0}, Lcom/sec/android/app/camera/PlugInShootingModesLoader;->isShootingModeExternal(I)Z

    move-result v0

    if-nez v0, :cond_13

    .line 164
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x14d

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v4, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v6, 0x7f02046a

    const v9, 0x7f0c0033

    move-object v5, p0

    move v8, v7

    invoke-direct/range {v4 .. v9}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 166
    :cond_13
    const/16 v0, 0x14e

    invoke-static {v0}, Lcom/sec/android/app/camera/PlugInShootingModesLoader;->isShootingModeExternal(I)Z

    move-result v0

    if-nez v0, :cond_14

    .line 167
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x14e

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v4, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v6, 0x7f020473

    const v9, 0x7f0c00ef

    move-object v5, p0

    move v8, v7

    invoke-direct/range {v4 .. v9}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 169
    :cond_14
    const/16 v0, 0x14f

    invoke-static {v0}, Lcom/sec/android/app/camera/PlugInShootingModesLoader;->isShootingModeExternal(I)Z

    move-result v0

    if-nez v0, :cond_15

    .line 170
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x14f

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v4, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v6, 0x7f020459

    const v9, 0x7f0c003e

    move-object v5, p0

    move v8, v7

    invoke-direct/range {v4 .. v9}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 172
    :cond_15
    const/16 v0, 0x150

    invoke-static {v0}, Lcom/sec/android/app/camera/PlugInShootingModesLoader;->isShootingModeExternal(I)Z

    move-result v0

    if-nez v0, :cond_16

    .line 173
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x150

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v4, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v6, 0x7f020460

    const v9, 0x7f0c003f

    move-object v5, p0

    move v8, v7

    invoke-direct/range {v4 .. v9}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 175
    :cond_16
    const/16 v0, 0x151

    invoke-static {v0}, Lcom/sec/android/app/camera/PlugInShootingModesLoader;->isShootingModeExternal(I)Z

    move-result v0

    if-nez v0, :cond_17

    .line 176
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x151

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v4, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v6, 0x7f02046b

    const v9, 0x7f0c011f

    move-object v5, p0

    move v8, v7

    invoke-direct/range {v4 .. v9}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 178
    :cond_17
    const/16 v0, 0x152

    invoke-static {v0}, Lcom/sec/android/app/camera/PlugInShootingModesLoader;->isShootingModeExternal(I)Z

    move-result v0

    if-nez v0, :cond_18

    .line 179
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x152

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v4, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v6, 0x7f020468

    const v9, 0x7f0c0039

    move-object v5, p0

    move v8, v7

    invoke-direct/range {v4 .. v9}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 181
    :cond_18
    const/16 v0, 0x153

    invoke-static {v0}, Lcom/sec/android/app/camera/PlugInShootingModesLoader;->isShootingModeExternal(I)Z

    move-result v0

    if-nez v0, :cond_19

    .line 182
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x153

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v4, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v6, 0x7f020453

    const v9, 0x7f0c0041

    move-object v5, p0

    move v8, v7

    invoke-direct/range {v4 .. v9}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 184
    :cond_19
    const/16 v0, 0x154

    invoke-static {v0}, Lcom/sec/android/app/camera/PlugInShootingModesLoader;->isShootingModeExternal(I)Z

    move-result v0

    if-nez v0, :cond_1a

    .line 185
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x154

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v4, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v6, 0x7f020470

    const v9, 0x7f0c0042

    move-object v5, p0

    move v8, v7

    invoke-direct/range {v4 .. v9}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 187
    :cond_1a
    const/16 v0, 0x156

    invoke-static {v0}, Lcom/sec/android/app/camera/PlugInShootingModesLoader;->isShootingModeExternal(I)Z

    move-result v0

    if-nez v0, :cond_1b

    .line 188
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x156

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v4, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v6, 0x7f020476

    const v9, 0x7f0c0048

    move-object v5, p0

    move v8, v7

    invoke-direct/range {v4 .. v9}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 190
    :cond_1b
    const/16 v0, 0x157

    invoke-static {v0}, Lcom/sec/android/app/camera/PlugInShootingModesLoader;->isShootingModeExternal(I)Z

    move-result v0

    if-nez v0, :cond_1c

    .line 191
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x157

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v4, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v6, 0x7f02046f

    const v9, 0x7f0c0043

    move-object v5, p0

    move v8, v7

    invoke-direct/range {v4 .. v9}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 193
    :cond_1c
    const/16 v0, 0x158

    invoke-static {v0}, Lcom/sec/android/app/camera/PlugInShootingModesLoader;->isShootingModeExternal(I)Z

    move-result v0

    if-nez v0, :cond_1d

    .line 194
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x158

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v4, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v6, 0x7f020463

    const v9, 0x7f0c0044

    move-object v5, p0

    move v8, v7

    invoke-direct/range {v4 .. v9}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 197
    :cond_1d
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x18f

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v4, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v6, 0x7f02045e

    const v9, 0x7f0c0090

    move-object v5, p0

    move v8, v7

    invoke-direct/range {v4 .. v9}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 198
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x232f

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v4, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v6, 0x7f020469

    const v9, 0x7f0c008f

    move-object v5, p0

    move v8, v7

    invoke-direct/range {v4 .. v9}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 202
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x258

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f0200c4

    const v3, 0x7f0200c6

    const v4, 0x7f0200c5

    move-object v1, p0

    move v5, v13

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 203
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x259

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f0200c1

    const v3, 0x7f0200c3

    const v4, 0x7f0200c2

    move-object v1, p0

    move v5, v14

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 204
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x25a

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f0200be

    const v3, 0x7f0200c0

    const v4, 0x7f0200bf

    const v5, 0x7f0c00d8

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 205
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x25b

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v3, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v5, 0x7f0200c7

    const v6, 0x7f0200c8

    const/high16 v8, 0x7f0c0000

    move-object v4, p0

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x2bc

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f02012b

    const v3, 0x7f02012c

    const v4, 0x7f02012a

    move-object v1, p0

    move v5, v13

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 209
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x2bd

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f02012b

    const v3, 0x7f02012c

    const v4, 0x7f02012a

    const v5, 0x7f0c00d9

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 210
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x2be

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f02012e

    const v3, 0x7f02012f

    const v4, 0x7f02012d

    const v5, 0x7f0c00da

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 211
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x2bf

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v3, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v5, 0x7f02012b

    const v6, 0x7f02012c

    const v8, 0x7f0c00db

    move-object v4, p0

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 214
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x320

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f020377

    const v3, 0x7f020379

    const v4, 0x7f020378

    move-object v1, p0

    move v5, v13

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 215
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x321

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f020367

    const v3, 0x7f020369

    const v4, 0x7f020368

    const v5, 0x7f0c00df

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 216
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x322

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f02036a

    const v3, 0x7f02036c

    const v4, 0x7f02036b

    const v5, 0x7f0c00e0

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 217
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x323

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f020364

    const v3, 0x7f020366

    const v4, 0x7f020365

    const v5, 0x7f0c00e1

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 220
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x384

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f0203ab

    const v3, 0x7f0203ad

    const v4, 0x7f0203ac

    const v5, 0x7f0c0066

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 221
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x385

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f0203b1

    const v3, 0x7f0203b3

    const v4, 0x7f0203b2

    const v5, 0x7f0c0068

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 222
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x386

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f0203ae

    const v3, 0x7f0203b0

    const v4, 0x7f0203af

    const v5, 0x7f0c0067

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 223
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x387

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f0203b7

    const v3, 0x7f0203b9

    const v4, 0x7f0203b8

    const v5, 0x7f0c006c

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 224
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x388

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f0203b4

    const v3, 0x7f0203b6

    const v4, 0x7f0203b5

    const v5, 0x7f0c0069

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 225
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x389

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v4, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v9, 0x7f0c006b

    move-object v5, p0

    move v6, v12

    move v8, v7

    invoke-direct/range {v4 .. v9}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 226
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x38a

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v4, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v9, 0x7f0c006a

    move-object v5, p0

    move v6, v12

    move v8, v7

    invoke-direct/range {v4 .. v9}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 229
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x44c

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f02010b

    const v3, 0x7f02010d

    const v4, 0x7f02010c

    const v5, 0x7f0c0066

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 230
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x44d

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v4, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v9, 0x7f0c006d

    move-object v5, p0

    move v6, v12

    move v8, v7

    invoke-direct/range {v4 .. v9}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 231
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x44e

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f0200ff

    const v3, 0x7f020101

    const v4, 0x7f020100

    const v5, 0x7f0c006e

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 232
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x44f

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f020102

    const v3, 0x7f020104

    const v4, 0x7f020103

    const v5, 0x7f0c006f

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 233
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x450

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f020105

    const v3, 0x7f020107

    const v4, 0x7f020106

    const v5, 0x7f0c0070

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 234
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x451

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f020108

    const v3, 0x7f02010a

    const v4, 0x7f020109

    const v5, 0x7f0c0071

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 235
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x452

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v4, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const/high16 v9, 0x7f0c0000

    move-object v5, p0

    move v6, v12

    move v8, v7

    invoke-direct/range {v4 .. v9}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 236
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x453

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v4, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v9, 0x7f0c0072

    move-object v5, p0

    move v6, v12

    move v8, v7

    invoke-direct/range {v4 .. v9}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 237
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x454

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v4, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const/high16 v9, 0x7f0c0000

    move-object v5, p0

    move v6, v12

    move v8, v7

    invoke-direct/range {v4 .. v9}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 238
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x455

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v4, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v9, 0x7f0c0073

    move-object v5, p0

    move v6, v12

    move v8, v7

    invoke-direct/range {v4 .. v9}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 239
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x456

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v4, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const/high16 v9, 0x7f0c0000

    move-object v5, p0

    move v6, v12

    move v8, v7

    invoke-direct/range {v4 .. v9}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 240
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x457

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v4, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const/high16 v9, 0x7f0c0000

    move-object v5, p0

    move v6, v12

    move v8, v7

    invoke-direct/range {v4 .. v9}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 241
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x458

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v4, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const/high16 v9, 0x7f0c0000

    move-object v5, p0

    move v6, v12

    move v8, v7

    invoke-direct/range {v4 .. v9}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 244
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x4b0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f02011a

    const v3, 0x7f02011c

    const v4, 0x7f02011b

    const v5, 0x7f0c00e2

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 245
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x4b1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f020120

    const v3, 0x7f020122

    const v4, 0x7f020121

    const v5, 0x7f0c00e3

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 246
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x4b2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f02011d

    const v3, 0x7f02011f

    const v4, 0x7f02011e

    const v5, 0x7f0c00e4

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 249
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x514

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f0200f3

    const v3, 0x7f0200f5

    const v4, 0x7f0200f4

    move-object v1, p0

    move v5, v13

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 250
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x515

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f0200f6

    const v3, 0x7f0200f8

    const v4, 0x7f0200f7

    move-object v1, p0

    move v5, v14

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 253
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x578

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f02037e

    const v3, 0x7f020380

    const v4, 0x7f02037f

    move-object v1, p0

    move v5, v13

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 254
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x579

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f020381

    const v3, 0x7f020383

    const v4, 0x7f020382

    move-object v1, p0

    move v5, v14

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 257
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x57a

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f02052b

    const v3, 0x7f02052d

    const v4, 0x7f02052c

    move-object v1, p0

    move v5, v13

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 258
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x57b

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f020529

    const v3, 0x7f02052e

    const v4, 0x7f02052a

    move-object v1, p0

    move v5, v14

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 262
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x6a4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v4, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v9, 0x7f0c00dd

    move-object v5, p0

    move v6, v12

    move v8, v7

    invoke-direct/range {v4 .. v9}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 263
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x6a5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v4, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v9, 0x7f0c00de

    move-object v5, p0

    move v6, v12

    move v8, v7

    invoke-direct/range {v4 .. v9}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 264
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x6a6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v4, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v9, 0x7f0c0065

    move-object v5, p0

    move v6, v12

    move v8, v7

    invoke-direct/range {v4 .. v9}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 267
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x6a7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v4, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v9, 0x7f0c00dd

    move-object v5, p0

    move v6, v12

    move v8, v7

    invoke-direct/range {v4 .. v9}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 268
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x6a8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v4, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v9, 0x7f0c00de

    move-object v5, p0

    move v6, v12

    move v8, v7

    invoke-direct/range {v4 .. v9}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 269
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x6a9

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v4, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v9, 0x7f0c0065

    move-object v5, p0

    move v6, v12

    move v8, v7

    invoke-direct/range {v4 .. v9}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 272
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x708

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f0200ed

    const v3, 0x7f0200ef

    const v4, 0x7f0200ee

    move-object v1, p0

    move v5, v13

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 273
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x709

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f0200f0

    const v3, 0x7f0200f2

    const v4, 0x7f0200f1

    move-object v1, p0

    move v5, v14

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 276
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x640

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f0202af

    const v3, 0x7f0202b1

    const v4, 0x7f0202b0

    const v5, 0x7f0c0221

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 277
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x641

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f0202ad

    const v3, 0x7f0202b2

    const v4, 0x7f0202ae

    const v5, 0x7f0c0220

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 280
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x76c

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f0202ff

    const v3, 0x7f020301

    const v4, 0x7f020300

    move-object v1, p0

    move v5, v13

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 281
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x76d

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f0202f9

    const v3, 0x7f020302

    const v4, 0x7f0202fe

    move-object v1, p0

    move v5, v14

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 284
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x7d0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f0200e7

    const v3, 0x7f0200e9

    const v4, 0x7f0200e8

    move-object v1, p0

    move v5, v13

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 285
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x7d1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f0200ea

    const v3, 0x7f0200ec

    const v4, 0x7f0200eb

    move-object v1, p0

    move v5, v14

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 288
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x834

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f02034e

    const v3, 0x7f020350

    const v4, 0x7f02034f

    move-object v1, p0

    move v5, v13

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 289
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x835

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f020351

    const v3, 0x7f020353

    const v4, 0x7f020352

    move-object v1, p0

    move v5, v14

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 292
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x898

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f020360

    const v3, 0x7f020362

    const v4, 0x7f020361

    const v5, 0x7f0c00e5

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 293
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x899

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f02035d

    const v3, 0x7f02035f

    const v4, 0x7f02035e

    const v5, 0x7f0c00e6

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 296
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x91f

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f020219

    const v3, 0x7f02021b

    const v4, 0x7f02021a

    const v5, 0x7f0c009b

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 297
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x924

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f020213

    const v3, 0x7f020215

    const v4, 0x7f020214

    const v5, 0x7f0c009c

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 298
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x8fc

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f020240

    const v3, 0x7f020242

    const v4, 0x7f020241

    const v5, 0x7f0c009d

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 299
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x8fd

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f02025e

    const v3, 0x7f020260

    const v4, 0x7f02025f

    const v5, 0x7f0c009e

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 300
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x928

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f02025e

    const v3, 0x7f020260

    const v4, 0x7f02025f

    const v5, 0x7f0c009f

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 301
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x8fe

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f020240

    const v3, 0x7f020242

    const v4, 0x7f020241

    const v5, 0x7f0c00a0

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 302
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x8ff

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f02023d

    const v3, 0x7f02023f

    const v4, 0x7f02023e

    const v5, 0x7f0c00a1

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 303
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x920

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f020213

    const v3, 0x7f020215

    const v4, 0x7f020214

    const v5, 0x7f0c00a2

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 304
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x925

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f020255

    const v3, 0x7f020257

    const v4, 0x7f020256

    const v5, 0x7f0c00a3

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 305
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x900

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f02024f

    const v3, 0x7f020251

    const v4, 0x7f020250

    const v5, 0x7f0c00a4

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 306
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x901

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v3, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v8, 0x7f0c00a5

    move-object v4, p0

    move v5, v12

    move v6, v12

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 307
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x902

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f020249

    const v3, 0x7f02024b

    const v4, 0x7f02024a

    const v5, 0x7f0c00a6

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 308
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x92c

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f02025b

    const v3, 0x7f02025d

    const v4, 0x7f02025c

    const v5, 0x7f0c00a9

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 309
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x903

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v3, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v8, 0x7f0c00a7

    move-object v4, p0

    move v5, v12

    move v6, v12

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 310
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x904

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v3, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v8, 0x7f0c00a8

    move-object v4, p0

    move v5, v12

    move v6, v12

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 311
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x921

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f020255

    const v3, 0x7f020257

    const v4, 0x7f020256

    const v5, 0x7f0c00aa

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 312
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x905

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v3, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v8, 0x7f0c00ac

    move-object v4, p0

    move v5, v12

    move v6, v12

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 313
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x923

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f020249

    const v3, 0x7f02024b

    const v4, 0x7f02024a

    const v5, 0x7f0c00b1

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 314
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x926

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f02024c

    const v3, 0x7f02024e

    const v4, 0x7f02024d

    const v5, 0x7f0c00ab

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 315
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x929

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f020246

    const v3, 0x7f020248

    const v4, 0x7f020247

    const v5, 0x7f0c00ad

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 319
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x906

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v3, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v5, 0x7f020243

    const v6, 0x7f020243

    const v8, 0x7f0c00ae

    move-object v4, p0

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 321
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x92a

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f02023a

    const v3, 0x7f02023c

    const v4, 0x7f02023b

    const v5, 0x7f0c00b2

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 322
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x908

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f02023a

    const v3, 0x7f02023c

    const v4, 0x7f02023b

    const v5, 0x7f0c00b0

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 323
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x909

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f020234

    const v3, 0x7f020236

    const v4, 0x7f020235

    const v5, 0x7f0c00b3

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 324
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x90a

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f020225

    const v3, 0x7f020227

    const v4, 0x7f020226

    const v5, 0x7f0c00b4

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 325
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x90b

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f020225

    const v3, 0x7f020227

    const v4, 0x7f020226

    const v5, 0x7f0c00b5

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 326
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x90c

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v3, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v8, 0x7f0c00b6

    move-object v4, p0

    move v5, v12

    move v6, v12

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 327
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x927

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f020231

    const v3, 0x7f020233

    const v4, 0x7f020232

    const v5, 0x7f0c00b7

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 328
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x90d

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f02022e

    const v3, 0x7f020230

    const v4, 0x7f02022f

    const v5, 0x7f0c00b8

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 330
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x90f

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v3, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v8, 0x7f0c00bd

    move-object v4, p0

    move v5, v12

    move v6, v12

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 331
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x910

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v3, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v8, 0x7f0c00bb

    move-object v4, p0

    move v5, v12

    move v6, v12

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 332
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x911

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v3, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v8, 0x7f0c00be

    move-object v4, p0

    move v5, v12

    move v6, v12

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 333
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x913

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f020222

    const v3, 0x7f020224

    const v4, 0x7f020223

    const v5, 0x7f0c00ba

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 334
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x912

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f020216

    const v3, 0x7f020218

    const v4, 0x7f020217

    const v5, 0x7f0c00bf

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 336
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x915

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f02020d

    const v3, 0x7f02020f

    const v4, 0x7f02020e

    const v5, 0x7f0c00c1

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 337
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x916

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v3, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v8, 0x7f0c00c2

    move-object v4, p0

    move v5, v12

    move v6, v12

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 338
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x922

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f02021c

    const v3, 0x7f02021e

    const v4, 0x7f02021d

    const v5, 0x7f0c00c3

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 339
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x92b

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f02022e

    const v3, 0x7f020230

    const v4, 0x7f02022f

    const v5, 0x7f0c00c4

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 340
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x91c

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v3, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v8, 0x7f0c00c5

    move-object v4, p0

    move v5, v12

    move v6, v12

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 341
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x919

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f020207

    const v3, 0x7f020209

    const v4, 0x7f020208

    const v5, 0x7f0c00c8

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 343
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x917

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v3, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v8, 0x7f0c00c6

    move-object v4, p0

    move v5, v12

    move v6, v12

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 344
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x91a

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v3, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v8, 0x7f0c00ca

    move-object v4, p0

    move v5, v12

    move v6, v12

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 345
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x918

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f02020a

    const v3, 0x7f02020c

    const v4, 0x7f02020b

    const v5, 0x7f0c00c7

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 346
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x91b

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v3, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v8, 0x7f0c00cb

    move-object v4, p0

    move v5, v12

    move v6, v12

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 347
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x91d

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v3, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v8, 0x7f0c00cc

    move-object v4, p0

    move v5, v12

    move v6, v12

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 349
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x96a

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f020393

    const v3, 0x7f020395

    const v4, 0x7f020394

    const v5, 0x7f0c00d5

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 350
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x96d

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f020399

    const v3, 0x7f02039b

    const v4, 0x7f02039a

    const v5, 0x7f0c00d6

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 351
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x96c

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f020384

    const v3, 0x7f020386

    const v4, 0x7f020385

    const v5, 0x7f0c00cd

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 352
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x960

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f020384

    const v3, 0x7f020386

    const v4, 0x7f020385

    const v5, 0x7f0c00cd

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 354
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x962

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f020387

    const v3, 0x7f020389

    const v4, 0x7f020388

    const v5, 0x7f0c00ce

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 355
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x963

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f0201df

    const v3, 0x7f0201e1

    const v4, 0x7f0201e0

    const v5, 0x7f0c00cf

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 356
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x964

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v3, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v8, 0x7f0c00d0

    move-object v4, p0

    move v5, v12

    move v6, v12

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 357
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x965

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f0201dc

    const v3, 0x7f0201de

    const v4, 0x7f0201dd

    const v5, 0x7f0c00d1

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 358
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x966

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f020396

    const v3, 0x7f020398

    const v4, 0x7f020397

    const v5, 0x7f0c00d2

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 360
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x967

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v3, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v8, 0x7f0c00c9

    move-object v4, p0

    move v5, v12

    move v6, v12

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 361
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x968

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f020390

    const v3, 0x7f020392

    const v4, 0x7f020391

    const v5, 0x7f0c00d3

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 362
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x969

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f02038a

    const v3, 0x7f02038c

    const v4, 0x7f02038b

    const v5, 0x7f0c00d4

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 365
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0xc1c

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f0202e1

    const v3, 0x7f0202e3

    const v4, 0x7f0202e2

    const v5, 0x7f0c00e8

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 366
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0xc1d

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f0202e4

    const v3, 0x7f0202e6

    const v4, 0x7f0202e5

    const v5, 0x7f0c00e9

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 367
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x170e

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f0202e7

    const v3, 0x7f0202e9

    const v4, 0x7f0202e8

    const v5, 0x7f0c00eb

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 368
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x170f

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f0202d8

    const v3, 0x7f0202da

    const v4, 0x7f0202d9

    const v5, 0x7f0c00ec

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 369
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0xc20

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f0202e1

    const v3, 0x7f0202e3

    const v4, 0x7f0202e2

    const v5, 0x7f0c00ed

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 370
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0xc21

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f0202e4

    const v3, 0x7f0202e6

    const v4, 0x7f0202e5

    const v5, 0x7f0c00ea

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 371
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0xc22

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f0202d5

    const v3, 0x7f0202d7

    const v4, 0x7f0202d6

    const v5, 0x7f0c00ee

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 374
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0xc80

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v4, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const/high16 v9, 0x7f0c0000

    move-object v5, p0

    move v6, v12

    move v8, v7

    invoke-direct/range {v4 .. v9}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 375
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0xc81

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v4, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const/high16 v9, 0x7f0c0000

    move-object v5, p0

    move v6, v12

    move v8, v7

    invoke-direct/range {v4 .. v9}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 378
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0xdac

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v4, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    move-object v5, p0

    move v6, v12

    move v8, v7

    move v9, v13

    invoke-direct/range {v4 .. v9}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 379
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0xdad

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v4, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    move-object v5, p0

    move v6, v12

    move v8, v7

    move v9, v14

    invoke-direct/range {v4 .. v9}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 382
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0xe10

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v3, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v5, 0x7f02008d

    const v6, 0x7f02008e

    const v8, 0x7f0c015e

    move-object v4, p0

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 383
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0xe11

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v3, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v5, 0x7f02008b

    const v6, 0x7f02008c

    const v8, 0x7f0c0166

    move-object v4, p0

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 384
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0xe12

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f02007c

    const v3, 0x7f02007e

    const v4, 0x7f02007d

    const v5, 0x7f0c0166

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 385
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0xe13

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f02007c

    const v3, 0x7f02007e

    const v4, 0x7f02007d

    const v5, 0x7f0c0165

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 386
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0xe14

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f02007c

    const v3, 0x7f02007e

    const v4, 0x7f02007d

    const v5, 0x7f0c0164

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 387
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0xe15

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f02007c

    const v3, 0x7f02007e

    const v4, 0x7f02007d

    const v5, 0x7f0c0163

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 388
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0xe16

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f02007c

    const v3, 0x7f02007e

    const v4, 0x7f02007d

    const v5, 0x7f0c0162

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 389
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0xe17

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f02007c

    const v3, 0x7f02007e

    const v4, 0x7f02007d

    const v5, 0x7f0c0161

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 390
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0xe18

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f02007c

    const v3, 0x7f02007e

    const v4, 0x7f02007d

    const v5, 0x7f0c0160

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 391
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0xe19

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f02007c

    const v3, 0x7f02007e

    const v4, 0x7f02007d

    const v5, 0x7f0c015f

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 392
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0xe1a

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f02007c

    const v3, 0x7f02007e

    const v4, 0x7f02007d

    const v5, 0x7f0c015e

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 395
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x80

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f0204ba

    const v3, 0x7f0204bc

    const v4, 0x7f0204bb

    const v5, 0x7f0c02b6

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 396
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x81

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v3, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v5, 0x7f0204ae

    const v6, 0x7f0204b0

    const v8, 0x7f0c0176

    move-object v4, p0

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 397
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x82

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v3, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v5, 0x7f0204b1

    const v6, 0x7f0204b3

    const v8, 0x7f0c0177

    move-object v4, p0

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 398
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x83

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v3, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v5, 0x7f0204b4

    const v6, 0x7f0204b6

    const v8, 0x7f0c0178

    move-object v4, p0

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 399
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x84

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v3, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v5, 0x7f0204b7

    const v6, 0x7f0204b9

    const v8, 0x7f0c0179

    move-object v4, p0

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 402
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x19ca

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v3, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v5, 0x7f020027

    const v6, 0x7f020028

    const v8, 0x7f0c016b

    move-object v4, p0

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 403
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x19cb

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v3, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v5, 0x7f020029

    const v6, 0x7f02002a

    const v8, 0x7f0c016c

    move-object v4, p0

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 404
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x19cc

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v3, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v5, 0x7f020029

    const v6, 0x7f02002a

    const v8, 0x7f0c016d

    move-object v4, p0

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 405
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x19cd

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v3, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v5, 0x7f020029

    const v6, 0x7f02002a

    const v8, 0x7f0c016e

    move-object v4, p0

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 406
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x19ce

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v3, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v5, 0x7f020029

    const v6, 0x7f02002a

    const v8, 0x7f0c016f

    move-object v4, p0

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 407
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x19cf

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v3, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v5, 0x7f020029

    const v6, 0x7f02002a

    const v8, 0x7f0c0170

    move-object v4, p0

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 408
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x19d0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v3, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v5, 0x7f020029

    const v6, 0x7f02002a

    const v8, 0x7f0c0171

    move-object v4, p0

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 409
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x19d1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v3, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v5, 0x7f020029

    const v6, 0x7f02002a

    const v8, 0x7f0c0172

    move-object v4, p0

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 410
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x19d2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v3, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v5, 0x7f020029

    const v6, 0x7f02002a

    const v8, 0x7f0c0173

    move-object v4, p0

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 412
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x1c86

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v3, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v5, 0x7f02035c

    const v6, 0x7f02035c

    const v8, 0x7f0c016b

    move-object v4, p0

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 413
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x1c87

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v3, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v5, 0x7f02035c

    const v6, 0x7f02035c

    const v8, 0x7f0c016c

    move-object v4, p0

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 414
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x1c88

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v3, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v5, 0x7f02035c

    const v6, 0x7f02035c

    const v8, 0x7f0c016d

    move-object v4, p0

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 415
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x1c89

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v3, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v5, 0x7f02035c

    const v6, 0x7f02035c

    const v8, 0x7f0c016e

    move-object v4, p0

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x1c8a

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v3, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v5, 0x7f02035c

    const v6, 0x7f02035c

    const v8, 0x7f0c016f

    move-object v4, p0

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 417
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x1c8b

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v3, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v5, 0x7f02035c

    const v6, 0x7f02035c

    const v8, 0x7f0c0170

    move-object v4, p0

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 418
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x1c8c

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v3, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v5, 0x7f02035c

    const v6, 0x7f02035c

    const v8, 0x7f0c0171

    move-object v4, p0

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 419
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x1c8d

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v3, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v5, 0x7f02035c

    const v6, 0x7f02035c

    const v8, 0x7f0c0172

    move-object v4, p0

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 420
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x1c8e

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v3, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v5, 0x7f02035c

    const v6, 0x7f02035c

    const v8, 0x7f0c0173

    move-object v4, p0

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 422
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x1cea

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v3, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v8, 0x7f0c016b

    move-object v4, p0

    move v5, v10

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 423
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x1ceb

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v3, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v8, 0x7f0c016c

    move-object v4, p0

    move v5, v10

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 424
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x1cec

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v3, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v8, 0x7f0c016d

    move-object v4, p0

    move v5, v10

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 425
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x1ced

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v3, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v8, 0x7f0c016e

    move-object v4, p0

    move v5, v10

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 426
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x1cee

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v3, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v8, 0x7f0c016f

    move-object v4, p0

    move v5, v10

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 427
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x1cef

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v3, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v8, 0x7f0c0170

    move-object v4, p0

    move v5, v10

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 428
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x1cf0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v3, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v8, 0x7f0c0171

    move-object v4, p0

    move v5, v10

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 429
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x1cf1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v3, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v8, 0x7f0c0172

    move-object v4, p0

    move v5, v10

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 430
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x1cf2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v3, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v8, 0x7f0c0173

    move-object v4, p0

    move v5, v10

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 433
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0xe74

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v4, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v6, 0x7f0201e8

    const v9, 0x7f0c0159

    move-object v5, p0

    move v8, v7

    invoke-direct/range {v4 .. v9}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 434
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0xe75

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v4, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v6, 0x7f0201ea

    const v9, 0x7f0c0122

    const v10, 0x7f0c0296

    move-object v5, p0

    move v8, v7

    move v11, v7

    invoke-direct/range {v4 .. v11}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIIIII)V

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 435
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0xe76

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v4, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v6, 0x7f0201ec

    const v9, 0x7f0c015a

    move-object v5, p0

    move v8, v7

    invoke-direct/range {v4 .. v9}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 436
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0xe77

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v4, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v6, 0x7f0201ed

    const v9, 0x7f0c015b

    move-object v5, p0

    move v8, v7

    invoke-direct/range {v4 .. v9}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 437
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0xe78

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v4, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v6, 0x7f0201eb

    const v9, 0x7f0c015c

    move-object v5, p0

    move v8, v7

    invoke-direct/range {v4 .. v9}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 438
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0xe79

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v4, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v6, 0x7f0201e9

    const v9, 0x7f0c015d

    move-object v5, p0

    move v8, v7

    invoke-direct/range {v4 .. v9}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 441
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0xed8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v4, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    move-object v5, p0

    move v6, v12

    move v8, v7

    move v9, v13

    invoke-direct/range {v4 .. v9}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 442
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0xed9

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v4, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    move-object v5, p0

    move v6, v12

    move v8, v7

    move v9, v14

    invoke-direct/range {v4 .. v9}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 445
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0xf3c

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f020307

    const v3, 0x7f020309

    const v4, 0x7f020308

    move-object v1, p0

    move v5, v13

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 446
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0xf3d

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f020305

    const v3, 0x7f02030a

    const v4, 0x7f020306

    move-object v1, p0

    move v5, v14

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 449
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x1644

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f020110

    const v3, 0x7f020112

    const v4, 0x7f020111

    move-object v1, p0

    move v5, v13

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 450
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x1645

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f02010e

    const v3, 0x7f020113

    const v4, 0x7f02010f

    move-object v1, p0

    move v5, v14

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 453
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x1194

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f020046

    const v3, 0x7f020048

    const v4, 0x7f020047

    move-object v1, p0

    move v5, v13

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 454
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x1195

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f020034

    const v3, 0x7f020036

    const v4, 0x7f020035

    move-object v1, p0

    move v5, v14

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 457
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x125c

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f02039f

    const v3, 0x7f0203a1

    const v4, 0x7f0203a0

    move-object v1, p0

    move v5, v13

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 458
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x125d

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f02039c

    const v3, 0x7f02039e

    const v4, 0x7f02039d

    move-object v1, p0

    move v5, v14

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 461
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x12c0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v4, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v6, 0x7f020303

    const v9, 0x7f0c021f

    move-object v5, p0

    move v8, v7

    invoke-direct/range {v4 .. v9}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 462
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x12c1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v4, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v6, 0x7f020304

    const v9, 0x7f0c021e

    move-object v5, p0

    move v8, v7

    invoke-direct/range {v4 .. v9}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 465
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x1388

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f0203a8

    const v3, 0x7f0203aa

    const v4, 0x7f0203a9

    const v5, 0x7f0c01f5

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 466
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x1389

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f0203a5

    const v3, 0x7f0203a7

    const v4, 0x7f0203a6

    const v5, 0x7f0c01f6

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 467
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x138a

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f0203a2

    const v3, 0x7f0203a4

    const v4, 0x7f0203a3

    const v5, 0x7f0c01f7

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 470
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x1450

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v4, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v6, 0x7f0204d5

    const v9, 0x7f0c01f0

    move-object v5, p0

    move v8, v7

    invoke-direct/range {v4 .. v9}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 471
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x1451

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v4, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v6, 0x7f0204d9

    const v9, 0x7f0c01f1

    move-object v5, p0

    move v8, v7

    invoke-direct/range {v4 .. v9}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 474
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x1518

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v3, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v5, 0x7f020029

    const v6, 0x7f02002a

    const v8, 0x7f0c021b

    move-object v4, p0

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 475
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x1519

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v3, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v5, 0x7f020027

    const v6, 0x7f020028

    const v8, 0x7f0c021c

    move-object v4, p0

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 478
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x157d

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v3, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v5, 0x7f0204bd

    const v6, 0x7f0204c0

    const v8, 0x7f0c02b4

    move-object v4, p0

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 479
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x157e

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v3, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v5, 0x7f0204c1

    const v6, 0x7f0204c2

    const v8, 0x7f0c02b5

    move-object v4, p0

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 483
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x1580

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v3, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v5, 0x7f0204be

    const v6, 0x7f0204bf

    const v8, 0x7f0c02b6

    move-object v4, p0

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 486
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x16a8

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f0200ac

    const v3, 0x7f0200ae

    const v4, 0x7f0200ad

    move-object v1, p0

    move v5, v13

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 487
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x16a9

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f0200af

    const v3, 0x7f0200b1

    const v4, 0x7f0200b0

    move-object v1, p0

    move v5, v14

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 490
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x189c

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f0201e2

    const v3, 0x7f0201e4

    const v4, 0x7f0201e3

    move-object v1, p0

    move v5, v13

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 491
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x189d

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f0201e5

    const v3, 0x7f0201e7

    const v4, 0x7f0201e6

    move-object v1, p0

    move v5, v14

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 494
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x1770

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v3, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v5, 0x7f0202ee

    const v6, 0x7f0202ef

    const v8, 0x7f0c01b5

    move-object v4, p0

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 495
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x1771

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v3, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v5, 0x7f0202ec

    const v6, 0x7f0202ed

    const v8, 0x7f0c01b6

    move-object v4, p0

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 496
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x1772

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v3, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v5, 0x7f0202ea

    const v6, 0x7f0202eb

    const v8, 0x7f0c01b7

    move-object v4, p0

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 499
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x1773

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v3, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v5, 0x7f0202db

    const v6, 0x7f0202dc

    const v8, 0x7f0c01b2

    move-object v4, p0

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 500
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x1774

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v3, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v5, 0x7f0202dd

    const v6, 0x7f0202de

    const v8, 0x7f0c01b3

    move-object v4, p0

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 501
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x1775

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v3, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v5, 0x7f0202df

    const v6, 0x7f0202e0

    const v8, 0x7f0c01b4

    move-object v4, p0

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 504
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x15e0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f02037e

    const v3, 0x7f020380

    const v4, 0x7f02037f

    move-object v1, p0

    move v5, v13

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 505
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x15e1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f020381

    const v3, 0x7f020383

    const v4, 0x7f020382

    move-object v1, p0

    move v5, v14

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 508
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x1964

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f0203f2

    const v3, 0x7f0203f2

    const v4, 0x7f0203f2

    const v5, 0x7f0c01eb

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 509
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x1965

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v3, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v5, 0x7f0203f1

    const v6, 0x7f0203f1

    const v8, 0x7f0c01ec

    move-object v4, p0

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 512
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x1838

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f0200ac

    const v3, 0x7f0200ae

    const v4, 0x7f0200ad

    move-object v1, p0

    move v5, v13

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 513
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v1, 0x1839

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v3, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v5, 0x7f0200af

    const v6, 0x7f0200b1

    move-object v4, p0

    move v8, v14

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 516
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x17d7

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f020016

    const v3, 0x7f020018

    const v4, 0x7f020017

    move-object v1, p0

    move v5, v13

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 517
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x17d6

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f020019

    const v3, 0x7f02001b

    const v4, 0x7f02001a

    move-object v1, p0

    move v5, v14

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 520
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x1a90

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f020337

    const v3, 0x7f020339

    const v4, 0x7f020338

    move-object v1, p0

    move v5, v13

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 521
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x1a91

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f020334

    const v3, 0x7f020336

    const v4, 0x7f020335

    move-object v1, p0

    move v5, v14

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 524
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x1af4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f02027e

    const v3, 0x7f020280

    const v4, 0x7f02027f

    move-object v1, p0

    move v5, v13

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 525
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x1af5

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f02027c

    const v3, 0x7f020281

    const v4, 0x7f02027d

    move-object v1, p0

    move v5, v14

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 528
    iget-object v8, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x1c20

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f020477

    const v3, 0x7f020479

    const v4, 0x7f020478

    const v5, 0x7f0c0123

    const v6, 0x7f0c02ad

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIIIII)V

    invoke-virtual {v8, v9, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 532
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x51e

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f020070

    const v3, 0x7f020072

    const v4, 0x7f020071

    move-object v1, p0

    move v5, v13

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v7, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 533
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x51f

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f020073

    const v3, 0x7f020075

    const v4, 0x7f020074

    move-object v1, p0

    move v5, v14

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v7, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 536
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x262

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f02006a

    const v3, 0x7f02006c

    const v4, 0x7f02006b

    move-object v1, p0

    move v5, v13

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v7, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 537
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x263

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f02006d

    const v3, 0x7f02006f

    const v4, 0x7f02006e

    move-object v1, p0

    move v5, v14

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v7, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 538
    iget-object v6, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    const/16 v0, 0x264

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    const v2, 0x7f020067

    const v3, 0x7f020069

    const v4, 0x7f020068

    const v5, 0x7f0c00d8

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;-><init>(Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;IIII)V

    invoke-virtual {v6, v7, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 549
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    .line 565
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 566
    return-void
.end method

.method public get(I)Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;
    .locals 2
    .param p1, "commandId"    # I

    .prologue
    .line 552
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    return-object v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 556
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    return v0
.end method

.method public getResourceIDByIndex(I)Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 560
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap;->mResourceIDs:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 561
    .local v0, "list":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;>;"
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/camera/subview/SubViewResourceIDMap$SubResourceIDSet;

    return-object v1
.end method
