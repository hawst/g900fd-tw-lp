.class public Lcom/sec/android/app/camera/MenuDimController;
.super Ljava/lang/Object;
.source "MenuDimController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/camera/MenuDimController$DimArray;,
        Lcom/sec/android/app/camera/MenuDimController$SettingValueReference;
    }
.end annotation


# static fields
.field public static final AUTO_NIGHT_DETECTION:I = 0x21

.field public static final BEAUTY_LEVEL:I = 0x2e

.field public static final BRIGHTNESS:I = 0x6

.field public static final BURST_SETTINGS:I = 0x1e

.field public static final CAMCORDER_ANTISHAKE:I = 0x1b

.field public static final CAMCORDER_AUDIOZOOM:I = 0x25

.field public static final CAMCORDER_RESOLUTION:I = 0x18

.field public static final CAMCORDER_SELF_SWITCH:I = 0x16

.field public static final CAMERA_ANTI_SHAKE:I = 0x5

.field public static final CAMERA_MODE:I = 0x0

.field public static final CAMERA_QUALITY:I = 0x1d

.field public static final CAMERA_RESOLUTION:I = 0xf

.field public static final DUAL_CAPTURE_MODE:I = 0x23

.field public static final EFFECT:I = 0x9

.field public static final EXPOSURE_VALUE:I = 0xe

.field public static final EYEENLARGE_LEVEL:I = 0x33

.field public static final FACE_DETECTION:I = 0x20

.field public static final FLASH_MODE:I = 0xb

.field public static final FLIP:I = 0x1a

.field public static final FOCUS_MODE:I = 0xc

.field public static final FOCUS_SELECT_MODE:I = 0x2b

.field public static final GPS:I = 0x22

.field public static final GUIDELINE:I = 0x14

.field public static final HDR:I = 0x4

.field public static final HELP:I = 0x2a

.field public static final IMAGE_VIEWER:I = 0x15

.field public static final ISO:I = 0x8

.field public static final METERING:I = 0xd

.field public static final NUM_OF_DIM_BUTTONS:I = 0x35

.field public static final QUICK_ACCESS:I = 0x2c

.field public static final RECORDING:I = 0x2

.field public static final RECORDING_MODE:I = 0x17

.field public static final REMOTE_VIEWFINDER:I = 0x27

.field public static final REVIEW:I = 0x12

.field public static final SAVE_RICHTONE:I = 0x1c

.field public static final SCENE_MODE:I = 0x7

.field public static final SCREEN_FLASH:I = 0x34

.field public static final SELFIE_CONTINUOUS:I = 0x30

.field public static final SELFIE_MODE:I = 0x31

.field public static final SETTINGS:I = 0x29

.field public static final SHOOTING_MODE:I = 0x3

.field public static final SHUTTER:I = 0x1

.field public static final SHUTTER_SOUND:I = 0x19

.field public static final SIDE_TOUCH_MODE:I = 0x28

.field public static final SLIMFACE_LEVEL:I = 0x32

.field public static final STORAGE:I = 0x13

.field private static final TAG:Ljava/lang/String; = "MenuDimController"

.field public static final THUMBNAIL_LIST:I = 0x26

.field public static final TIMER:I = 0x11

.field public static final TOUCH_TO_CAPTURE:I = 0x24

.field public static final VIEWMODE:I = 0x2d

.field public static final VOICECOMMAND:I = 0x1f

.field public static final VOLUME_KEY:I = 0x2f

.field public static final WHITEBALANCE:I = 0x10

.field public static final ZOOM:I = 0xa


# instance fields
.field private mActivityContext:Lcom/sec/android/app/camera/Camera;

.field private mButtonList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/glview/TwGLView;",
            ">;"
        }
    .end annotation
.end field

.field private mCheckSceneModeSet:Z

.field private mCurrentDimArray:[Z

.field private mDimArrayList:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/sec/android/app/camera/MenuDimController$DimArray;",
            ">;"
        }
    .end annotation
.end field

.field private mPreviousFlashValue:I

.field private mPreviousMeteringValue:I

.field private mUserSettingValueList:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/sec/android/app/camera/MenuDimController$SettingValueReference;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/sec/android/app/camera/Camera;)V
    .locals 2
    .param p1, "activityContext"    # Lcom/sec/android/app/camera/Camera;

    .prologue
    const/4 v1, 0x0

    .line 113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 102
    const/16 v0, 0x35

    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/sec/android/app/camera/MenuDimController;->mCurrentDimArray:[Z

    .line 103
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/camera/MenuDimController;->mUserSettingValueList:Ljava/util/HashMap;

    .line 104
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/camera/MenuDimController;->mDimArrayList:Ljava/util/concurrent/ConcurrentHashMap;

    .line 105
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/camera/MenuDimController;->mButtonList:Ljava/util/ArrayList;

    .line 106
    iput-boolean v1, p0, Lcom/sec/android/app/camera/MenuDimController;->mCheckSceneModeSet:Z

    .line 107
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/camera/MenuDimController;->mPreviousFlashValue:I

    .line 108
    iput v1, p0, Lcom/sec/android/app/camera/MenuDimController;->mPreviousMeteringValue:I

    .line 114
    iput-object p1, p0, Lcom/sec/android/app/camera/MenuDimController;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    .line 115
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/camera/MenuDimController;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/MenuDimController;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/camera/MenuDimController;->mUserSettingValueList:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/camera/MenuDimController;)Lcom/sec/android/app/camera/Camera;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/MenuDimController;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/camera/MenuDimController;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/camera/MenuDimController;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/MenuDimController;

    .prologue
    .line 41
    iget v0, p0, Lcom/sec/android/app/camera/MenuDimController;->mPreviousFlashValue:I

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/camera/MenuDimController;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/MenuDimController;

    .prologue
    .line 41
    iget v0, p0, Lcom/sec/android/app/camera/MenuDimController;->mPreviousMeteringValue:I

    return v0
.end method

.method private merge([Z)V
    .locals 4
    .param p1, "array"    # [Z

    .prologue
    .line 2438
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v1, 0x35

    if-ge v0, v1, :cond_0

    .line 2439
    iget-object v1, p0, Lcom/sec/android/app/camera/MenuDimController;->mCurrentDimArray:[Z

    iget-object v2, p0, Lcom/sec/android/app/camera/MenuDimController;->mCurrentDimArray:[Z

    aget-boolean v2, v2, v0

    aget-boolean v3, p1, v0

    or-int/2addr v2, v3

    aput-boolean v2, v1, v0

    .line 2438
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2441
    :cond_0
    return-void
.end method


# virtual methods
.method public declared-synchronized addButton(Lcom/sec/android/glview/TwGLView;)V
    .locals 1
    .param p1, "button"    # Lcom/sec/android/glview/TwGLView;

    .prologue
    .line 684
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/camera/MenuDimController;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 685
    monitor-exit p0

    return-void

    .line 684
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized clear()V
    .locals 1

    .prologue
    .line 671
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/camera/MenuDimController;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 672
    iget-object v0, p0, Lcom/sec/android/app/camera/MenuDimController;->mDimArrayList:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 673
    monitor-exit p0

    return-void

    .line 671
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public clearLimitedResolutionSettingValues()V
    .locals 3

    .prologue
    .line 2592
    iget-object v1, p0, Lcom/sec/android/app/camera/MenuDimController;->mDimArrayList:Ljava/util/concurrent/ConcurrentHashMap;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/camera/MenuDimController$DimArray;

    .line 2593
    .local v0, "dimArray":Lcom/sec/android/app/camera/MenuDimController$DimArray;
    if-eqz v0, :cond_0

    .line 2594
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->getSavedSettingValue(I)I

    .line 2596
    :cond_0
    return-void
.end method

.method public getDim(I)Z
    .locals 1
    .param p1, "key"    # I

    .prologue
    .line 875
    const/4 v0, -0x1

    if-le p1, v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/camera/MenuDimController;->mCurrentDimArray:[Z

    array-length v0, v0

    if-ge p1, v0, :cond_0

    .line 876
    iget-object v0, p0, Lcom/sec/android/app/camera/MenuDimController;->mCurrentDimArray:[Z

    aget-boolean v0, v0, p1

    .line 879
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDimArray([Z)V
    .locals 3
    .param p1, "dimArray"    # [Z

    .prologue
    const/4 v2, 0x0

    .line 676
    iget-object v0, p0, Lcom/sec/android/app/camera/MenuDimController;->mCurrentDimArray:[Z

    const/16 v1, 0x35

    invoke-static {v0, v2, p1, v2, v1}, Ljava/lang/System;->arraycopy([ZI[ZII)V

    .line 677
    return-void
.end method

.method public getSavedUserSettingValues(I)I
    .locals 3
    .param p1, "menuId"    # I

    .prologue
    .line 892
    iget-object v1, p0, Lcom/sec/android/app/camera/MenuDimController;->mUserSettingValueList:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/camera/MenuDimController$SettingValueReference;

    .line 894
    .local v0, "dimReference":Lcom/sec/android/app/camera/MenuDimController$SettingValueReference;
    if-eqz v0, :cond_0

    .line 895
    invoke-virtual {v0}, Lcom/sec/android/app/camera/MenuDimController$SettingValueReference;->getSettingsValue()I

    move-result v1

    .line 897
    :goto_0
    return v1

    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public getkeyFromCommandId(I)I
    .locals 1
    .param p1, "commandId"    # I

    .prologue
    .line 692
    const/4 v0, -0x1

    .line 694
    .local v0, "key":I
    sparse-switch p1, :sswitch_data_0

    .line 856
    :goto_0
    return v0

    .line 696
    :sswitch_0
    const/4 v0, 0x0

    .line 697
    goto :goto_0

    .line 699
    :sswitch_1
    const/4 v0, 0x1

    .line 700
    goto :goto_0

    .line 702
    :sswitch_2
    const/4 v0, 0x2

    .line 703
    goto :goto_0

    .line 705
    :sswitch_3
    const/4 v0, 0x3

    .line 706
    goto :goto_0

    .line 708
    :sswitch_4
    const/4 v0, 0x7

    .line 709
    goto :goto_0

    .line 713
    :sswitch_5
    const/16 v0, 0xb

    .line 714
    goto :goto_0

    .line 716
    :sswitch_6
    const/16 v0, 0xf

    .line 717
    goto :goto_0

    .line 719
    :sswitch_7
    const/16 v0, 0xc

    .line 720
    goto :goto_0

    .line 722
    :sswitch_8
    const/16 v0, 0x11

    .line 723
    goto :goto_0

    .line 725
    :sswitch_9
    const/16 v0, 0xe

    .line 726
    goto :goto_0

    .line 728
    :sswitch_a
    const/16 v0, 0x9

    .line 729
    goto :goto_0

    .line 731
    :sswitch_b
    const/16 v0, 0x10

    .line 732
    goto :goto_0

    .line 734
    :sswitch_c
    const/16 v0, 0x8

    .line 735
    goto :goto_0

    .line 737
    :sswitch_d
    const/16 v0, 0xd

    .line 738
    goto :goto_0

    .line 741
    :sswitch_e
    const/4 v0, 0x4

    .line 742
    goto :goto_0

    .line 744
    :sswitch_f
    const/4 v0, 0x5

    .line 745
    goto :goto_0

    .line 747
    :sswitch_10
    const/16 v0, 0x12

    .line 748
    goto :goto_0

    .line 750
    :sswitch_11
    const/16 v0, 0xa

    .line 751
    goto :goto_0

    .line 753
    :sswitch_12
    const/16 v0, 0x14

    .line 754
    goto :goto_0

    .line 756
    :sswitch_13
    const/16 v0, 0x13

    .line 757
    goto :goto_0

    .line 759
    :sswitch_14
    const/16 v0, 0x15

    .line 760
    goto :goto_0

    .line 762
    :sswitch_15
    const/16 v0, 0x17

    .line 763
    goto :goto_0

    .line 765
    :sswitch_16
    const/16 v0, 0x18

    .line 766
    goto :goto_0

    .line 768
    :sswitch_17
    const/16 v0, 0x1b

    .line 769
    goto :goto_0

    .line 771
    :sswitch_18
    const/16 v0, 0x25

    .line 772
    goto :goto_0

    .line 774
    :sswitch_19
    const/16 v0, 0x1a

    .line 775
    goto :goto_0

    .line 777
    :sswitch_1a
    const/16 v0, 0x1d

    .line 778
    goto :goto_0

    .line 780
    :sswitch_1b
    const/16 v0, 0x1e

    .line 781
    goto :goto_0

    .line 783
    :sswitch_1c
    const/16 v0, 0x1f

    .line 784
    goto :goto_0

    .line 786
    :sswitch_1d
    const/16 v0, 0x19

    .line 787
    goto :goto_0

    .line 789
    :sswitch_1e
    const/16 v0, 0x1c

    .line 790
    goto :goto_0

    .line 792
    :sswitch_1f
    const/16 v0, 0x20

    .line 793
    goto :goto_0

    .line 795
    :sswitch_20
    const/16 v0, 0x24

    .line 796
    goto :goto_0

    .line 798
    :sswitch_21
    const/16 v0, 0x21

    .line 799
    goto :goto_0

    .line 801
    :sswitch_22
    const/16 v0, 0x22

    .line 802
    goto :goto_0

    .line 804
    :sswitch_23
    const/16 v0, 0x23

    .line 805
    goto :goto_0

    .line 807
    :sswitch_24
    const/16 v0, 0x26

    .line 808
    goto :goto_0

    .line 810
    :sswitch_25
    const/16 v0, 0x27

    .line 811
    goto :goto_0

    .line 813
    :sswitch_26
    const/16 v0, 0x28

    .line 814
    goto :goto_0

    .line 816
    :sswitch_27
    const/16 v0, 0x2c

    .line 817
    goto :goto_0

    .line 820
    :sswitch_28
    const/16 v0, 0x29

    .line 821
    goto :goto_0

    .line 823
    :sswitch_29
    const/16 v0, 0x2a

    .line 824
    goto :goto_0

    .line 826
    :sswitch_2a
    const/16 v0, 0x2b

    .line 827
    goto :goto_0

    .line 829
    :sswitch_2b
    const/16 v0, 0x2d

    .line 830
    goto :goto_0

    .line 832
    :sswitch_2c
    const/16 v0, 0x2e

    .line 833
    goto :goto_0

    .line 835
    :sswitch_2d
    const/16 v0, 0x32

    .line 836
    goto/16 :goto_0

    .line 838
    :sswitch_2e
    const/16 v0, 0x33

    .line 839
    goto/16 :goto_0

    .line 841
    :sswitch_2f
    const/16 v0, 0x30

    .line 842
    goto/16 :goto_0

    .line 844
    :sswitch_30
    const/16 v0, 0x31

    .line 845
    goto/16 :goto_0

    .line 847
    :sswitch_31
    const/16 v0, 0x2f

    .line 848
    goto/16 :goto_0

    .line 850
    :sswitch_32
    const/16 v0, 0x34

    .line 851
    goto/16 :goto_0

    .line 694
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_3
        0x2 -> :sswitch_4
        0x3 -> :sswitch_5
        0x4 -> :sswitch_6
        0x5 -> :sswitch_7
        0x6 -> :sswitch_8
        0x7 -> :sswitch_9
        0x8 -> :sswitch_a
        0x9 -> :sswitch_b
        0xa -> :sswitch_c
        0xb -> :sswitch_d
        0xc -> :sswitch_e
        0xd -> :sswitch_f
        0x10 -> :sswitch_1a
        0x11 -> :sswitch_10
        0x12 -> :sswitch_11
        0x13 -> :sswitch_12
        0x14 -> :sswitch_22
        0x15 -> :sswitch_1d
        0x16 -> :sswitch_13
        0x17 -> :sswitch_2b
        0x1b -> :sswitch_28
        0x1d -> :sswitch_14
        0x20 -> :sswitch_1
        0x22 -> :sswitch_1e
        0x24 -> :sswitch_0
        0x33 -> :sswitch_19
        0x3f -> :sswitch_1b
        0x41 -> :sswitch_2
        0x47 -> :sswitch_1c
        0x48 -> :sswitch_31
        0x54 -> :sswitch_30
        0x56 -> :sswitch_1f
        0x5c -> :sswitch_21
        0x5e -> :sswitch_23
        0x61 -> :sswitch_20
        0x62 -> :sswitch_2a
        0x6d -> :sswitch_2c
        0x6f -> :sswitch_29
        0x70 -> :sswitch_26
        0x71 -> :sswitch_24
        0x72 -> :sswitch_25
        0x78 -> :sswitch_27
        0x7a -> :sswitch_32
        0x7d -> :sswitch_2f
        0x85 -> :sswitch_2d
        0x86 -> :sswitch_2e
        0xbb8 -> :sswitch_15
        0xbb9 -> :sswitch_16
        0xbbf -> :sswitch_17
        0xbc3 -> :sswitch_e
        0xbc4 -> :sswitch_5
        0xbc7 -> :sswitch_18
        0x1c21 -> :sswitch_28
        0x2330 -> :sswitch_5
    .end sparse-switch
.end method

.method public processDim()V
    .locals 3

    .prologue
    .line 2425
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v2, 0x35

    if-ge v0, v2, :cond_0

    .line 2426
    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2}, Lcom/sec/android/app/camera/MenuDimController;->setDim(IZ)V

    .line 2425
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2429
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/camera/MenuDimController;->mDimArrayList:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 2430
    .local v1, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/app/camera/MenuDimController$DimArray;>;"
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2431
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/camera/MenuDimController$DimArray;

    invoke-virtual {v2}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->getArray()[Z

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/sec/android/app/camera/MenuDimController;->merge([Z)V

    goto :goto_1

    .line 2434
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/camera/MenuDimController;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v2}, Lcom/sec/android/app/camera/Camera;->handleDimButtons()V

    .line 2435
    return-void
.end method

.method public declared-synchronized refreshButtonDim(II)V
    .locals 12
    .param p1, "menuid"    # I
    .param p2, "modeid"    # I

    .prologue
    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    .line 925
    monitor-enter p0

    :try_start_0
    const-string v6, "MenuDimController"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "refreshButtonDim: menuid="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " modeid="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 929
    iget-object v6, p0, Lcom/sec/android/app/camera/MenuDimController;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v6}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v0

    .line 930
    .local v0, "cs":Lcom/sec/android/app/camera/CameraSettings;
    iget-object v6, p0, Lcom/sec/android/app/camera/MenuDimController;->mDimArrayList:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/camera/MenuDimController$DimArray;

    .line 932
    .local v1, "dimArray":Lcom/sec/android/app/camera/MenuDimController$DimArray;
    if-eqz v1, :cond_0

    .line 933
    const/4 v6, 0x0

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->restoreUserSettingValues(Z)V

    .line 934
    iget-object v6, p0, Lcom/sec/android/app/camera/MenuDimController;->mDimArrayList:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->getMenuId()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 935
    const-string v6, "MenuDimController"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "restoring user setting values. CameraResolutionChanged = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->getCameraResolutionChanged()Z

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " CamcorderResolutionChanged = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->getCamcorderResolutionChanged()Z

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 937
    :cond_0
    new-instance v1, Lcom/sec/android/app/camera/MenuDimController$DimArray;

    .end local v1    # "dimArray":Lcom/sec/android/app/camera/MenuDimController$DimArray;
    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/camera/MenuDimController$DimArray;-><init>(Lcom/sec/android/app/camera/MenuDimController;I)V

    .line 939
    .restart local v1    # "dimArray":Lcom/sec/android/app/camera/MenuDimController$DimArray;
    sparse-switch p1, :sswitch_data_0

    .line 2383
    :cond_1
    :goto_0
    iget-object v6, p0, Lcom/sec/android/app/camera/MenuDimController;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v6}, Lcom/sec/android/app/camera/Camera;->isConnectHeadSet()Z

    move-result v6

    if-eqz v6, :cond_2

    const/16 v6, 0xbc7

    if-ne p1, v6, :cond_2

    .line 2384
    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v7, 0x0

    const/16 v8, 0x25

    aput v8, v6, v7

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 2385
    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v7, 0x0

    const/16 v8, 0x25

    aput v8, v6, v7

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 2386
    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v7, 0x0

    const/16 v8, 0x25

    aput v8, v6, v7

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    .line 2389
    :cond_2
    iget-object v6, p0, Lcom/sec/android/app/camera/MenuDimController;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v6}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/camera/CameraSettings;->isTemperatureHighToRecord()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 2390
    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v7, 0x0

    const/4 v8, 0x2

    aput v8, v6, v7

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 2394
    :cond_3
    iget-object v6, p0, Lcom/sec/android/app/camera/MenuDimController;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-static {v6}, Lcom/sec/android/app/camera/Util;->isKNOXMode(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 2395
    const/16 v6, 0xa

    new-array v6, v6, [I

    fill-array-data v6, :array_0

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 2396
    const/16 v6, 0x9

    new-array v6, v6, [I

    fill-array-data v6, :array_1

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 2397
    const/16 v6, 0x8

    new-array v6, v6, [I

    fill-array-data v6, :array_2

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    .line 2400
    :cond_4
    iget-object v6, p0, Lcom/sec/android/app/camera/MenuDimController;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    const-string v7, "com.sec.android.gallery3d"

    invoke-static {v6, v7}, Lcom/sec/android/app/camera/Util;->isPkgEnabled(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_5

    .line 2401
    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v7, 0x0

    const/16 v8, 0x12

    aput v8, v6, v7

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 2402
    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v7, 0x0

    const/16 v8, 0x12

    aput v8, v6, v7

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 2403
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setReview(I)V

    .line 2406
    :cond_5
    iget-object v6, p0, Lcom/sec/android/app/camera/MenuDimController;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v6}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/camera/CameraSettings;->isSecureMode()Z

    move-result v6

    if-eqz v6, :cond_6

    .line 2410
    const/4 v6, 0x6

    new-array v6, v6, [I

    fill-array-data v6, :array_3

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 2411
    const/4 v6, 0x4

    new-array v6, v6, [I

    fill-array-data v6, :array_4

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 2412
    const/4 v6, 0x4

    new-array v6, v6, [I

    fill-array-data v6, :array_5

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    .line 2420
    :cond_6
    iget-object v6, p0, Lcom/sec/android/app/camera/MenuDimController;->mDimArrayList:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7, v1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2421
    invoke-virtual {p0}, Lcom/sec/android/app/camera/MenuDimController;->processDim()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2422
    monitor-exit p0

    return-void

    .line 942
    :sswitch_0
    :try_start_1
    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->isBackCamera()Z

    move-result v6

    if-eqz v6, :cond_7

    .line 943
    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v7, 0x0

    const/16 v8, 0x1a

    aput v8, v6, v7

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 944
    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v7, 0x0

    const/16 v8, 0x1a

    aput v8, v6, v7

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 945
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setSelfFlip(I)V

    .line 947
    :cond_7
    if-eqz p2, :cond_b

    .line 948
    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->isFrontCamera()Z

    move-result v6

    if-eqz v6, :cond_a

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->getShootingMode()I

    move-result v6

    const/4 v7, 0x7

    if-eq v6, v7, :cond_a

    .line 949
    const/16 v6, 0x12

    new-array v6, v6, [I

    fill-array-data v6, :array_6

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 951
    const/16 v6, 0x11

    new-array v6, v6, [I

    fill-array-data v6, :array_7

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 953
    const/16 v6, 0xf

    new-array v6, v6, [I

    fill-array-data v6, :array_8

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    .line 964
    :goto_1
    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->getCamcorderResolution()I

    move-result v6

    const/16 v7, 0x28

    if-eq v6, v7, :cond_8

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->getCamcorderResolution()I

    move-result v6

    const/16 v7, 0x15

    if-ne v6, v7, :cond_9

    .line 965
    :cond_8
    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v7, 0x0

    const/4 v8, 0x2

    aput v8, v6, v7

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 967
    :cond_9
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCamcorderAntishake(I)V

    .line 968
    iget-object v6, p0, Lcom/sec/android/app/camera/MenuDimController;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/sec/android/app/camera/MenuDimController;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v6}, Lcom/sec/android/app/camera/Camera;->getCameraBaseMenu()Lcom/sec/android/app/camera/glwidget/TwGLCameraBaseMenu;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 969
    iget-object v6, p0, Lcom/sec/android/app/camera/MenuDimController;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v6}, Lcom/sec/android/app/camera/Camera;->getCameraBaseMenu()Lcom/sec/android/app/camera/glwidget/TwGLCameraBaseMenu;

    move-result-object v6

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Lcom/sec/android/app/camera/glwidget/TwGLCameraBaseMenu;->setBeautyButtonDimmed(Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 925
    .end local v0    # "cs":Lcom/sec/android/app/camera/CameraSettings;
    .end local v1    # "dimArray":Lcom/sec/android/app/camera/MenuDimController$DimArray;
    :catchall_0
    move-exception v6

    monitor-exit p0

    throw v6

    .line 956
    .restart local v0    # "cs":Lcom/sec/android/app/camera/CameraSettings;
    .restart local v1    # "dimArray":Lcom/sec/android/app/camera/MenuDimController$DimArray;
    :cond_a
    const/16 v6, 0x11

    :try_start_2
    new-array v6, v6, [I

    fill-array-data v6, :array_9

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 958
    const/16 v6, 0x10

    new-array v6, v6, [I

    fill-array-data v6, :array_a

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 960
    const/16 v6, 0xe

    new-array v6, v6, [I

    fill-array-data v6, :array_b

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    goto :goto_1

    .line 972
    :cond_b
    iget-object v6, p0, Lcom/sec/android/app/camera/MenuDimController;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/sec/android/app/camera/MenuDimController;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v6}, Lcom/sec/android/app/camera/Camera;->getCameraBaseMenu()Lcom/sec/android/app/camera/glwidget/TwGLCameraBaseMenu;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 973
    iget-object v6, p0, Lcom/sec/android/app/camera/MenuDimController;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v6}, Lcom/sec/android/app/camera/Camera;->getCameraBaseMenu()Lcom/sec/android/app/camera/glwidget/TwGLCameraBaseMenu;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Lcom/sec/android/app/camera/glwidget/TwGLCameraBaseMenu;->setBeautyButtonDimmed(Z)V

    goto/16 :goto_0

    .line 990
    :sswitch_1
    if-nez p2, :cond_1

    .line 991
    const/4 v6, 0x2

    new-array v6, v6, [I

    fill-array-data v6, :array_c

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 992
    const/4 v6, 0x2

    new-array v6, v6, [I

    fill-array-data v6, :array_d

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 993
    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v7, 0x0

    const/16 v8, 0x1f

    aput v8, v6, v7

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    goto/16 :goto_0

    .line 997
    :sswitch_2
    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->isBackCamera()Z

    move-result v6

    if-eqz v6, :cond_c

    .line 998
    const/4 v6, 0x2

    new-array v6, v6, [I

    fill-array-data v6, :array_e

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 999
    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v7, 0x0

    const/16 v8, 0x1a

    aput v8, v6, v7

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 1000
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setSelfFlip(I)V

    .line 1002
    :cond_c
    iget-object v6, p0, Lcom/sec/android/app/camera/MenuDimController;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v6}, Lcom/sec/android/app/camera/Camera;->isMultiWindow()Z

    move-result v6

    if-eqz v6, :cond_d

    .line 1003
    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v7, 0x0

    const/16 v8, 0x27

    aput v8, v6, v7

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 1004
    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v7, 0x0

    const/16 v8, 0x27

    aput v8, v6, v7

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 1005
    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v7, 0x0

    const/16 v8, 0x27

    aput v8, v6, v7

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    .line 1014
    :cond_d
    packed-switch p2, :pswitch_data_0

    :pswitch_0
    goto/16 :goto_0

    .line 1727
    :pswitch_1
    const/4 v6, 0x2

    new-array v6, v6, [I

    fill-array-data v6, :array_f

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 1729
    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->isFrontCamera()Z

    move-result v6

    if-nez v6, :cond_e

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->isDualFrontCamera()Z

    move-result v6

    if-eqz v6, :cond_f

    .line 1730
    :cond_e
    const/4 v6, 0x2

    new-array v6, v6, [I

    fill-array-data v6, :array_10

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 1731
    const/4 v6, 0x2

    new-array v6, v6, [I

    fill-array-data v6, :array_11

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 1732
    const/4 v6, 0x2

    new-array v6, v6, [I

    fill-array-data v6, :array_12

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    .line 1735
    :cond_f
    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->isFrontCamera()Z

    move-result v6

    if-nez v6, :cond_10

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->isDualFrontCamera()Z

    move-result v6

    if-eqz v6, :cond_3e

    .line 1736
    :cond_10
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraFocusMode(I)V

    goto/16 :goto_0

    .line 1016
    :pswitch_2
    const/16 v6, 0xe

    new-array v6, v6, [I

    fill-array-data v6, :array_13

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 1018
    const/16 v6, 0xb

    new-array v6, v6, [I

    fill-array-data v6, :array_14

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 1019
    const/16 v6, 0x9

    new-array v6, v6, [I

    fill-array-data v6, :array_15

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    .line 1022
    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->getCameraFocusMode()I

    move-result v6

    if-ne v6, v11, :cond_1

    .line 1023
    const/4 v6, 0x1

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraFocusMode(I)V

    goto/16 :goto_0

    .line 1029
    :pswitch_3
    const/16 v6, 0x10

    new-array v6, v6, [I

    fill-array-data v6, :array_16

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 1031
    const/16 v6, 0xe

    new-array v6, v6, [I

    fill-array-data v6, :array_17

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 1033
    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->isFrontCamera()Z

    move-result v6

    if-eqz v6, :cond_11

    .line 1034
    const/16 v6, 0xb

    new-array v6, v6, [I

    fill-array-data v6, :array_18

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    .line 1035
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setTouchToCapture(I)V

    .line 1041
    :goto_2
    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v7, 0x0

    const/16 v8, 0x9

    aput v8, v6, v7

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 1055
    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->isFrontCamera()Z

    move-result v6

    if-nez v6, :cond_12

    .line 1060
    :goto_3
    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->isFrontCamera()Z

    move-result v6

    if-eqz v6, :cond_13

    .line 1061
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraFocusMode(I)V

    .line 1069
    :goto_4
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setFlashMode(I)V

    goto/16 :goto_0

    .line 1037
    :cond_11
    const/16 v6, 0xc

    new-array v6, v6, [I

    fill-array-data v6, :array_19

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    goto :goto_2

    .line 1056
    :cond_12
    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v7, 0x0

    const/16 v8, 0x20

    aput v8, v6, v7

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 1057
    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v7, 0x0

    const/16 v8, 0x20

    aput v8, v6, v7

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 1058
    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v7, 0x0

    const/16 v8, 0x20

    aput v8, v6, v7

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    goto :goto_3

    .line 1063
    :cond_13
    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->getFaceDetectionMode()I

    move-result v6

    if-ne v6, v9, :cond_14

    .line 1064
    const/4 v6, 0x3

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraFocusMode(I)V

    goto :goto_4

    .line 1066
    :cond_14
    const/4 v6, 0x1

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraFocusMode(I)V

    goto :goto_4

    .line 1078
    :pswitch_4
    const/16 v6, 0x13

    new-array v6, v6, [I

    fill-array-data v6, :array_1a

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 1080
    const/16 v6, 0x10

    new-array v6, v6, [I

    fill-array-data v6, :array_1b

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 1083
    const/16 v6, 0xc

    new-array v6, v6, [I

    fill-array-data v6, :array_1c

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    .line 1084
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraQuality(I)V

    .line 1086
    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->isFrontCamera()Z

    move-result v6

    if-eqz v6, :cond_15

    .line 1087
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraFocusMode(I)V

    .line 1096
    :goto_5
    const/4 v6, 0x1

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setFaceDetectionMode(I)V

    .line 1102
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setFlashMode(I)V

    goto/16 :goto_0

    .line 1090
    :cond_15
    const/4 v6, 0x3

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraFocusMode(I)V

    goto :goto_5

    .line 1106
    :pswitch_5
    const/16 v6, 0x1b

    new-array v6, v6, [I

    fill-array-data v6, :array_1d

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 1108
    const/16 v6, 0x18

    new-array v6, v6, [I

    fill-array-data v6, :array_1e

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 1116
    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->isFrontCamera()Z

    move-result v6

    if-eqz v6, :cond_16

    .line 1117
    const/16 v6, 0x13

    new-array v6, v6, [I

    fill-array-data v6, :array_1f

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    .line 1118
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setTouchToCapture(I)V

    .line 1122
    :goto_6
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraQuality(I)V

    .line 1124
    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->isFrontCamera()Z

    move-result v6

    if-eqz v6, :cond_17

    .line 1125
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraFocusMode(I)V

    .line 1135
    :goto_7
    const-string v6, "640x480"

    invoke-static {v6}, Lcom/sec/android/app/camera/CameraResolution;->getResolutionID(Ljava/lang/String;)I

    move-result v5

    .line 1136
    .local v5, "resolutionId":I
    invoke-virtual {v0, v5}, Lcom/sec/android/app/camera/CameraSettings;->setCameraResolution(I)Z

    .line 1138
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setFlashMode(I)V

    goto/16 :goto_0

    .line 1120
    .end local v5    # "resolutionId":I
    :cond_16
    const/16 v6, 0x14

    new-array v6, v6, [I

    fill-array-data v6, :array_20

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    goto :goto_6

    .line 1127
    :cond_17
    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->getFaceDetectionMode()I

    move-result v6

    if-ne v6, v9, :cond_18

    .line 1128
    const/4 v6, 0x3

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraFocusMode(I)V

    goto :goto_7

    .line 1130
    :cond_18
    const/4 v6, 0x1

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraFocusMode(I)V

    goto :goto_7

    .line 1141
    :pswitch_6
    const/16 v6, 0x13

    new-array v6, v6, [I

    fill-array-data v6, :array_21

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 1143
    const/16 v6, 0x10

    new-array v6, v6, [I

    fill-array-data v6, :array_22

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 1145
    const/16 v6, 0xc

    new-array v6, v6, [I

    fill-array-data v6, :array_23

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    .line 1146
    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->isFrontCamera()Z

    move-result v6

    if-eqz v6, :cond_19

    .line 1147
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraFocusMode(I)V

    .line 1156
    :goto_8
    const/4 v6, 0x1

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setFaceDetectionMode(I)V

    .line 1158
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setFlashMode(I)V

    .line 1162
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraQuality(I)V

    goto/16 :goto_0

    .line 1150
    :cond_19
    const/4 v6, 0x3

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraFocusMode(I)V

    goto :goto_8

    .line 1166
    :pswitch_7
    const/16 v6, 0x13

    new-array v6, v6, [I

    fill-array-data v6, :array_24

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 1168
    const/16 v6, 0x10

    new-array v6, v6, [I

    fill-array-data v6, :array_25

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 1170
    const/16 v6, 0xd

    new-array v6, v6, [I

    fill-array-data v6, :array_26

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    .line 1171
    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->isFrontCamera()Z

    move-result v6

    if-eqz v6, :cond_1a

    .line 1172
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraFocusMode(I)V

    .line 1176
    :goto_9
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setFlashMode(I)V

    .line 1177
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraQuality(I)V

    goto/16 :goto_0

    .line 1174
    :cond_1a
    const/4 v6, 0x1

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraFocusMode(I)V

    goto :goto_9

    .line 1180
    :pswitch_8
    const/16 v6, 0x16

    new-array v6, v6, [I

    fill-array-data v6, :array_27

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 1182
    const/16 v6, 0x13

    new-array v6, v6, [I

    fill-array-data v6, :array_28

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 1184
    const/16 v6, 0xe

    new-array v6, v6, [I

    fill-array-data v6, :array_29

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    .line 1186
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCamcorderAntishake(I)V

    .line 1188
    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->isFrontCamera()Z

    move-result v6

    if-eqz v6, :cond_1b

    .line 1189
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraFocusMode(I)V

    .line 1193
    :goto_a
    const-string v6, "800x450"

    invoke-static {v6}, Lcom/sec/android/app/camera/CameraResolution;->getResolutionID(Ljava/lang/String;)I

    move-result v5

    .line 1194
    .restart local v5    # "resolutionId":I
    invoke-virtual {v0, v5}, Lcom/sec/android/app/camera/CameraSettings;->setCameraResolution(I)Z

    .line 1195
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setFlashMode(I)V

    .line 1196
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraQuality(I)V

    goto/16 :goto_0

    .line 1191
    .end local v5    # "resolutionId":I
    :cond_1b
    const/4 v6, 0x1

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraFocusMode(I)V

    goto :goto_a

    .line 1199
    :pswitch_9
    const/16 v6, 0x14

    new-array v6, v6, [I

    fill-array-data v6, :array_2a

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 1201
    const/16 v6, 0x11

    new-array v6, v6, [I

    fill-array-data v6, :array_2b

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 1203
    const/16 v6, 0xe

    new-array v6, v6, [I

    fill-array-data v6, :array_2c

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    .line 1205
    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->isFrontCamera()Z

    move-result v6

    if-eqz v6, :cond_1c

    .line 1206
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraFocusMode(I)V

    .line 1210
    :goto_b
    const/4 v6, 0x2

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraExposureMeter(I)V

    .line 1215
    const-string v6, "3264x1836"

    invoke-static {v6}, Lcom/sec/android/app/camera/CameraResolution;->getResolutionID(Ljava/lang/String;)I

    move-result v4

    .line 1219
    .local v4, "res_id":I
    invoke-virtual {v0, v4}, Lcom/sec/android/app/camera/CameraSettings;->setCameraResolution(I)Z

    goto/16 :goto_0

    .line 1208
    .end local v4    # "res_id":I
    :cond_1c
    const/4 v6, 0x1

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraFocusMode(I)V

    goto :goto_b

    .line 1223
    :pswitch_a
    iget-object v6, p0, Lcom/sec/android/app/camera/MenuDimController;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v6}, Lcom/sec/android/app/camera/Camera;->isEasyModeSettingOn()Z

    move-result v6

    if-eqz v6, :cond_1d

    .line 1224
    const/16 v6, 0x13

    new-array v6, v6, [I

    fill-array-data v6, :array_2d

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 1226
    const/16 v6, 0x10

    new-array v6, v6, [I

    fill-array-data v6, :array_2e

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 1228
    const/16 v6, 0xc

    new-array v6, v6, [I

    fill-array-data v6, :array_2f

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    .line 1238
    :goto_c
    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->isFrontCamera()Z

    move-result v6

    if-eqz v6, :cond_1e

    .line 1239
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraFocusMode(I)V

    .line 1244
    :goto_d
    const/4 v6, 0x1

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setFaceDetectionMode(I)V

    .line 1246
    const/4 v6, 0x2

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraExposureMeter(I)V

    .line 1247
    const-string v6, "1920x1080"

    invoke-static {v6}, Lcom/sec/android/app/camera/CameraResolution;->getResolutionID(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraResolution(I)Z

    goto/16 :goto_0

    .line 1231
    :cond_1d
    const/16 v6, 0x15

    new-array v6, v6, [I

    fill-array-data v6, :array_30

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 1233
    const/16 v6, 0x12

    new-array v6, v6, [I

    fill-array-data v6, :array_31

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 1235
    const/16 v6, 0xe

    new-array v6, v6, [I

    fill-array-data v6, :array_32

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    goto :goto_c

    .line 1241
    :cond_1e
    const/4 v6, 0x1

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraFocusMode(I)V

    goto :goto_d

    .line 1250
    :pswitch_b
    const/16 v6, 0xf

    new-array v6, v6, [I

    fill-array-data v6, :array_33

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 1252
    const/16 v6, 0xc

    new-array v6, v6, [I

    fill-array-data v6, :array_34

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 1254
    const/16 v6, 0xb

    new-array v6, v6, [I

    fill-array-data v6, :array_35

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    .line 1255
    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->isFrontCamera()Z

    move-result v6

    if-eqz v6, :cond_1f

    .line 1256
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraFocusMode(I)V

    goto/16 :goto_0

    .line 1258
    :cond_1f
    const/4 v6, 0x1

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraFocusMode(I)V

    goto/16 :goto_0

    .line 1262
    :pswitch_c
    const/16 v6, 0xc

    new-array v6, v6, [I

    fill-array-data v6, :array_36

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 1263
    const/16 v6, 0x9

    new-array v6, v6, [I

    fill-array-data v6, :array_37

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 1264
    const/4 v6, 0x7

    new-array v6, v6, [I

    fill-array-data v6, :array_38

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    .line 1265
    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->isFrontCamera()Z

    move-result v6

    if-eqz v6, :cond_20

    .line 1266
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraFocusMode(I)V

    .line 1274
    :goto_e
    const/4 v6, 0x1

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setFaceDetectionMode(I)V

    goto/16 :goto_0

    .line 1269
    :cond_20
    const/4 v6, 0x3

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraFocusMode(I)V

    goto :goto_e

    .line 1277
    :pswitch_d
    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->isFrontCamera()Z

    move-result v6

    if-eqz v6, :cond_21

    .line 1278
    const/16 v6, 0xc

    new-array v6, v6, [I

    fill-array-data v6, :array_39

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 1280
    const/16 v6, 0x9

    new-array v6, v6, [I

    fill-array-data v6, :array_3a

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 1281
    const/4 v6, 0x7

    new-array v6, v6, [I

    fill-array-data v6, :array_3b

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    .line 1293
    :goto_f
    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->isFrontCamera()Z

    move-result v6

    if-eqz v6, :cond_22

    .line 1294
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraFocusMode(I)V

    .line 1303
    :goto_10
    const/4 v6, 0x1

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setFaceDetectionMode(I)V

    .line 1307
    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->getBeautyLevel()I

    move-result v6

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setBeautyLevel(I)V

    goto/16 :goto_0

    .line 1283
    :cond_21
    const/16 v6, 0xd

    new-array v6, v6, [I

    fill-array-data v6, :array_3c

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 1285
    const/16 v6, 0xa

    new-array v6, v6, [I

    fill-array-data v6, :array_3d

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 1286
    const/16 v6, 0x8

    new-array v6, v6, [I

    fill-array-data v6, :array_3e

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    goto :goto_f

    .line 1297
    :cond_22
    const/4 v6, 0x3

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraFocusMode(I)V

    goto :goto_10

    .line 1319
    :pswitch_e
    const/16 v6, 0x9

    new-array v6, v6, [I

    fill-array-data v6, :array_3f

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 1321
    const/4 v6, 0x7

    new-array v6, v6, [I

    fill-array-data v6, :array_40

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 1322
    const/4 v6, 0x5

    new-array v6, v6, [I

    fill-array-data v6, :array_41

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    .line 1331
    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->isFrontCamera()Z

    move-result v6

    if-eqz v6, :cond_23

    .line 1332
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraFocusMode(I)V

    .line 1341
    :goto_11
    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->isSingleEffect()Z

    move-result v6

    if-nez v6, :cond_24

    .line 1342
    const/4 v6, 0x1

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setFaceDetectionMode(I)V

    .line 1354
    :goto_12
    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->getBeautyLevel()I

    move-result v6

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setBeautyLevel(I)V

    .line 1358
    iget-object v6, p0, Lcom/sec/android/app/camera/MenuDimController;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/sec/android/app/camera/MenuDimController;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v6}, Lcom/sec/android/app/camera/Camera;->getCameraBaseMenu()Lcom/sec/android/app/camera/glwidget/TwGLCameraBaseMenu;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 1359
    iget-object v6, p0, Lcom/sec/android/app/camera/MenuDimController;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v6}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/camera/CameraSettings;->getAttachCamcorderMode()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1360
    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v7, 0x0

    const/16 v8, 0x2e

    aput v8, v6, v7

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 1361
    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v7, 0x0

    const/16 v8, 0x2e

    aput v8, v6, v7

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 1362
    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v7, 0x0

    const/16 v8, 0x2e

    aput v8, v6, v7

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    .line 1363
    iget-object v6, p0, Lcom/sec/android/app/camera/MenuDimController;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v6}, Lcom/sec/android/app/camera/Camera;->getCameraBaseMenu()Lcom/sec/android/app/camera/glwidget/TwGLCameraBaseMenu;

    move-result-object v6

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Lcom/sec/android/app/camera/glwidget/TwGLCameraBaseMenu;->setBeautyButtonDimmed(Z)V

    goto/16 :goto_0

    .line 1335
    :cond_23
    const/4 v6, 0x3

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraFocusMode(I)V

    goto :goto_11

    .line 1344
    :cond_24
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setFaceDetectionMode(I)V

    goto :goto_12

    .line 1368
    :pswitch_f
    const/16 v6, 0xf

    new-array v6, v6, [I

    fill-array-data v6, :array_42

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 1370
    const/16 v6, 0xc

    new-array v6, v6, [I

    fill-array-data v6, :array_43

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 1372
    const/16 v6, 0xb

    new-array v6, v6, [I

    fill-array-data v6, :array_44

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    .line 1373
    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->isFrontCamera()Z

    move-result v6

    if-eqz v6, :cond_25

    .line 1374
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraFocusMode(I)V

    goto/16 :goto_0

    .line 1376
    :cond_25
    const/4 v6, 0x1

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraFocusMode(I)V

    goto/16 :goto_0

    .line 1380
    :pswitch_10
    const/16 v6, 0x10

    new-array v6, v6, [I

    fill-array-data v6, :array_45

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 1382
    const/16 v6, 0xe

    new-array v6, v6, [I

    fill-array-data v6, :array_46

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 1384
    const/16 v6, 0xd

    new-array v6, v6, [I

    fill-array-data v6, :array_47

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    .line 1386
    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->isFrontCamera()Z

    move-result v6

    if-eqz v6, :cond_26

    .line 1387
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraFocusMode(I)V

    goto/16 :goto_0

    .line 1389
    :cond_26
    const/4 v6, 0x1

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraFocusMode(I)V

    goto/16 :goto_0

    .line 1418
    :pswitch_11
    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->isFrontCamera()Z

    move-result v6

    if-eqz v6, :cond_27

    .line 1419
    const/16 v6, 0x12

    new-array v6, v6, [I

    fill-array-data v6, :array_48

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 1421
    const/16 v6, 0xf

    new-array v6, v6, [I

    fill-array-data v6, :array_49

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 1423
    const/16 v6, 0xe

    new-array v6, v6, [I

    fill-array-data v6, :array_4a

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    .line 1431
    :goto_13
    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->isFrontCamera()Z

    move-result v6

    if-eqz v6, :cond_28

    .line 1432
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraFocusMode(I)V

    goto/16 :goto_0

    .line 1425
    :cond_27
    const/16 v6, 0xe

    new-array v6, v6, [I

    fill-array-data v6, :array_4b

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 1427
    const/16 v6, 0xb

    new-array v6, v6, [I

    fill-array-data v6, :array_4c

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 1428
    const/16 v6, 0xa

    new-array v6, v6, [I

    fill-array-data v6, :array_4d

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    goto :goto_13

    .line 1434
    :cond_28
    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->getFaceDetectionMode()I

    move-result v6

    if-ne v6, v9, :cond_29

    .line 1435
    const/4 v6, 0x3

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraFocusMode(I)V

    goto/16 :goto_0

    .line 1437
    :cond_29
    const/4 v6, 0x1

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraFocusMode(I)V

    goto/16 :goto_0

    .line 1460
    :pswitch_12
    const/16 v6, 0x11

    new-array v6, v6, [I

    fill-array-data v6, :array_4e

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 1462
    const/16 v6, 0xe

    new-array v6, v6, [I

    fill-array-data v6, :array_4f

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 1464
    const/16 v6, 0xb

    new-array v6, v6, [I

    fill-array-data v6, :array_50

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    .line 1466
    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->isFrontCamera()Z

    move-result v6

    if-eqz v6, :cond_2a

    .line 1467
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraFocusMode(I)V

    goto/16 :goto_0

    .line 1469
    :cond_2a
    const/4 v6, 0x1

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraFocusMode(I)V

    goto/16 :goto_0

    .line 1473
    :pswitch_13
    const/16 v6, 0x10

    new-array v6, v6, [I

    fill-array-data v6, :array_51

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 1475
    const/16 v6, 0xd

    new-array v6, v6, [I

    fill-array-data v6, :array_52

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 1477
    const/16 v6, 0xb

    new-array v6, v6, [I

    fill-array-data v6, :array_53

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    .line 1479
    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->isFrontCamera()Z

    move-result v6

    if-eqz v6, :cond_2b

    .line 1480
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraFocusMode(I)V

    goto/16 :goto_0

    .line 1482
    :cond_2b
    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->getFaceDetectionMode()I

    move-result v6

    if-ne v6, v9, :cond_2c

    .line 1483
    const/4 v6, 0x3

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraFocusMode(I)V

    goto/16 :goto_0

    .line 1485
    :cond_2c
    const/4 v6, 0x1

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraFocusMode(I)V

    goto/16 :goto_0

    .line 1501
    :pswitch_14
    const/16 v6, 0x11

    new-array v6, v6, [I

    fill-array-data v6, :array_54

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 1503
    const/16 v6, 0xe

    new-array v6, v6, [I

    fill-array-data v6, :array_55

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 1506
    const/16 v6, 0xb

    new-array v6, v6, [I

    fill-array-data v6, :array_56

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    .line 1507
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setFlashMode(I)V

    .line 1508
    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->isFrontCamera()Z

    move-result v6

    if-eqz v6, :cond_2d

    .line 1509
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraFocusMode(I)V

    goto/16 :goto_0

    .line 1511
    :cond_2d
    const/4 v6, 0x1

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraFocusMode(I)V

    goto/16 :goto_0

    .line 1515
    :pswitch_15
    const/16 v6, 0x15

    new-array v6, v6, [I

    fill-array-data v6, :array_57

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 1517
    const/16 v6, 0x12

    new-array v6, v6, [I

    fill-array-data v6, :array_58

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 1519
    const/16 v6, 0xe

    new-array v6, v6, [I

    fill-array-data v6, :array_59

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    .line 1520
    const-string v6, "1920x1080"

    invoke-static {v6}, Lcom/sec/android/app/camera/CameraResolution;->getResolutionID(Ljava/lang/String;)I

    move-result v5

    .line 1521
    .restart local v5    # "resolutionId":I
    invoke-virtual {v0, v5}, Lcom/sec/android/app/camera/CameraSettings;->setCameraResolution(I)Z

    .line 1522
    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->isFrontCamera()Z

    move-result v6

    if-eqz v6, :cond_2e

    .line 1523
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraFocusMode(I)V

    .line 1527
    :goto_14
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setFlashMode(I)V

    goto/16 :goto_0

    .line 1525
    :cond_2e
    const/4 v6, 0x1

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraFocusMode(I)V

    goto :goto_14

    .line 1530
    .end local v5    # "resolutionId":I
    :pswitch_16
    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->getSoundShotMode()I

    move-result v6

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setSoundShotMode(I)V

    .line 1539
    const/16 v6, 0xc

    new-array v6, v6, [I

    fill-array-data v6, :array_5a

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 1540
    const/16 v6, 0xa

    new-array v6, v6, [I

    fill-array-data v6, :array_5b

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 1541
    const/16 v6, 0xc

    const/4 v7, 0x1

    invoke-virtual {v1, v6, v7}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setChangedOnly(IZ)V

    .line 1543
    const/16 v6, 0x8

    new-array v6, v6, [I

    fill-array-data v6, :array_5c

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    .line 1566
    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->getSoundShotMode()I

    move-result v6

    if-nez v6, :cond_2f

    .line 1567
    const-string v6, "ro.csc.country_code"

    invoke-static {v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "JP"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2f

    .line 1568
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraShutterSound(I)V

    .line 1571
    :cond_2f
    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->isFrontCamera()Z

    move-result v6

    if-eqz v6, :cond_30

    .line 1572
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraFocusMode(I)V

    goto/16 :goto_0

    .line 1574
    :cond_30
    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->getFaceDetectionMode()I

    move-result v6

    if-ne v6, v9, :cond_31

    .line 1575
    const/4 v6, 0x3

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraFocusMode(I)V

    goto/16 :goto_0

    .line 1577
    :cond_31
    const/4 v6, 0x1

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraFocusMode(I)V

    goto/16 :goto_0

    .line 1586
    :pswitch_17
    const/16 v6, 0x18

    new-array v6, v6, [I

    fill-array-data v6, :array_5d

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 1588
    const/16 v6, 0x14

    new-array v6, v6, [I

    fill-array-data v6, :array_5e

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 1590
    const/16 v6, 0x12

    new-array v6, v6, [I

    fill-array-data v6, :array_5f

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    .line 1593
    sget-object v6, Lcom/sec/android/app/camera/Feature;->PANORAMA360_STILL_RESOLUTION:Ljava/lang/String;

    invoke-static {v6}, Lcom/sec/android/app/camera/CameraResolution;->getResolutionID(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraResolution(I)Z

    .line 1594
    const/4 v6, 0x1

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraFocusMode(I)V

    goto/16 :goto_0

    .line 1604
    :pswitch_18
    const/16 v6, 0x15

    new-array v6, v6, [I

    fill-array-data v6, :array_60

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 1606
    const/16 v6, 0x12

    new-array v6, v6, [I

    fill-array-data v6, :array_61

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 1608
    const/16 v6, 0xe

    new-array v6, v6, [I

    fill-array-data v6, :array_62

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    .line 1612
    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->isBackCamera()Z

    move-result v6

    if-eqz v6, :cond_32

    .line 1613
    const-string v6, "3264x1836"

    invoke-static {v6}, Lcom/sec/android/app/camera/CameraResolution;->getResolutionID(Ljava/lang/String;)I

    move-result v5

    .line 1617
    .restart local v5    # "resolutionId":I
    :goto_15
    invoke-virtual {v0, v5}, Lcom/sec/android/app/camera/CameraSettings;->setCameraResolution(I)Z

    .line 1618
    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->isFrontCamera()Z

    move-result v6

    if-eqz v6, :cond_33

    .line 1619
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraFocusMode(I)V

    .line 1623
    :goto_16
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setFlashMode(I)V

    goto/16 :goto_0

    .line 1615
    .end local v5    # "resolutionId":I
    :cond_32
    const-string v6, "1920x1080"

    invoke-static {v6}, Lcom/sec/android/app/camera/CameraResolution;->getResolutionID(Ljava/lang/String;)I

    move-result v5

    .restart local v5    # "resolutionId":I
    goto :goto_15

    .line 1621
    :cond_33
    const/4 v6, 0x1

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraFocusMode(I)V

    goto :goto_16

    .line 1634
    .end local v5    # "resolutionId":I
    :pswitch_19
    const/16 v6, 0x11

    new-array v6, v6, [I

    fill-array-data v6, :array_63

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 1636
    const/16 v6, 0xc

    new-array v6, v6, [I

    fill-array-data v6, :array_64

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 1638
    const/16 v6, 0xc

    new-array v6, v6, [I

    fill-array-data v6, :array_65

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    .line 1642
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCamcorderAntishake(I)V

    .line 1645
    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->updateCameraResolutionForDual()V

    .line 1650
    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->isBackCamera()Z

    move-result v6

    if-nez v6, :cond_34

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->isDualBackCamera()Z

    move-result v6

    if-eqz v6, :cond_36

    .line 1651
    :cond_34
    const/4 v6, 0x5

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraFocusMode(I)V

    .line 1662
    :goto_17
    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->isDualBackCamera()Z

    move-result v6

    if-eqz v6, :cond_37

    iget-object v6, p0, Lcom/sec/android/app/camera/MenuDimController;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v6}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/camera/CameraSettings;->getCamcorderResolution()I

    move-result v6

    const/16 v7, 0x28

    if-eq v6, v7, :cond_35

    iget-object v6, p0, Lcom/sec/android/app/camera/MenuDimController;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v6}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/camera/CameraSettings;->getCamcorderResolution()I

    move-result v6

    const/16 v7, 0x15

    if-ne v6, v7, :cond_37

    .line 1663
    :cond_35
    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->getDefaultCamcorderResolution()I

    move-result v6

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCamcorderResolution(I)Z

    goto/16 :goto_0

    .line 1653
    :cond_36
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraFocusMode(I)V

    goto :goto_17

    .line 1665
    :cond_37
    const/16 v6, 0xbb9

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->getCamcorderResolution()I

    move-result v7

    invoke-virtual {v0, v6, v7}, Lcom/sec/android/app/camera/CameraSettings;->notifyCameraSettingsChanged(II)V

    goto/16 :goto_0

    .line 1669
    :pswitch_1a
    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->isFrontCamera()Z

    move-result v6

    if-eqz v6, :cond_38

    .line 1670
    const/16 v6, 0xe

    new-array v6, v6, [I

    fill-array-data v6, :array_66

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 1672
    const/16 v6, 0xb

    new-array v6, v6, [I

    fill-array-data v6, :array_67

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 1674
    const/16 v6, 0xa

    new-array v6, v6, [I

    fill-array-data v6, :array_68

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    .line 1682
    :goto_18
    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->isFrontCamera()Z

    move-result v6

    if-eqz v6, :cond_39

    .line 1683
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraFocusMode(I)V

    goto/16 :goto_0

    .line 1676
    :cond_38
    const/16 v6, 0xe

    new-array v6, v6, [I

    fill-array-data v6, :array_69

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 1678
    const/16 v6, 0xb

    new-array v6, v6, [I

    fill-array-data v6, :array_6a

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 1679
    const/16 v6, 0xa

    new-array v6, v6, [I

    fill-array-data v6, :array_6b

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    goto :goto_18

    .line 1685
    :cond_39
    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->getFaceDetectionMode()I

    move-result v6

    if-ne v6, v9, :cond_3a

    .line 1686
    const/4 v6, 0x3

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraFocusMode(I)V

    goto/16 :goto_0

    .line 1688
    :cond_3a
    const/4 v6, 0x1

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraFocusMode(I)V

    goto/16 :goto_0

    .line 1693
    :pswitch_1b
    const/16 v6, 0x15

    new-array v6, v6, [I

    fill-array-data v6, :array_6c

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 1695
    const/16 v6, 0x11

    new-array v6, v6, [I

    fill-array-data v6, :array_6d

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 1696
    const/16 v6, 0xf

    new-array v6, v6, [I

    fill-array-data v6, :array_6e

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    .line 1697
    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->isFrontCamera()Z

    move-result v6

    if-eqz v6, :cond_3b

    .line 1698
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraFocusMode(I)V

    .line 1706
    :goto_19
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setSideTouchMode(I)V

    .line 1707
    const-string v6, "2048x1152"

    invoke-static {v6}, Lcom/sec/android/app/camera/CameraResolution;->getResolutionID(Ljava/lang/String;)I

    move-result v5

    .line 1708
    .restart local v5    # "resolutionId":I
    invoke-virtual {v0, v5}, Lcom/sec/android/app/camera/CameraSettings;->setCameraResolution(I)Z

    goto/16 :goto_0

    .line 1700
    .end local v5    # "resolutionId":I
    :cond_3b
    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->getFaceDetectionMode()I

    move-result v6

    if-ne v6, v9, :cond_3c

    .line 1701
    const/4 v6, 0x3

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraFocusMode(I)V

    goto :goto_19

    .line 1703
    :cond_3c
    const/4 v6, 0x1

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraFocusMode(I)V

    goto :goto_19

    .line 1711
    :pswitch_1c
    const/16 v6, 0x11

    new-array v6, v6, [I

    fill-array-data v6, :array_6f

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 1713
    const/16 v6, 0xe

    new-array v6, v6, [I

    fill-array-data v6, :array_70

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 1715
    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->isFrontCamera()Z

    move-result v6

    if-eqz v6, :cond_3d

    .line 1716
    const/16 v6, 0xb

    new-array v6, v6, [I

    fill-array-data v6, :array_71

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    .line 1717
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setTouchToCapture(I)V

    .line 1722
    :goto_1a
    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->getHelpMode()I

    move-result v6

    const/16 v7, 0xcb

    if-eq v6, v7, :cond_1

    .line 1723
    const/4 v6, 0x1

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setFaceDetectionMode(I)V

    goto/16 :goto_0

    .line 1719
    :cond_3d
    const/16 v6, 0xc

    new-array v6, v6, [I

    fill-array-data v6, :array_72

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    goto :goto_1a

    .line 1738
    :cond_3e
    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->getFaceDetectionMode()I

    move-result v6

    if-ne v6, v9, :cond_3f

    .line 1739
    const/4 v6, 0x3

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraFocusMode(I)V

    goto/16 :goto_0

    .line 1741
    :cond_3f
    const/4 v6, 0x1

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraFocusMode(I)V

    goto/16 :goto_0

    .line 1758
    :sswitch_3
    if-eqz p2, :cond_41

    .line 1760
    iget-boolean v6, p0, Lcom/sec/android/app/camera/MenuDimController;->mCheckSceneModeSet:Z

    if-nez v6, :cond_40

    .line 1761
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/sec/android/app/camera/MenuDimController;->mCheckSceneModeSet:Z

    .line 1762
    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->getFlashMode()I

    move-result v6

    iput v6, p0, Lcom/sec/android/app/camera/MenuDimController;->mPreviousFlashValue:I

    .line 1763
    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->getCameraExposureMeter()I

    move-result v6

    iput v6, p0, Lcom/sec/android/app/camera/MenuDimController;->mPreviousMeteringValue:I

    .line 1766
    :cond_40
    packed-switch p2, :pswitch_data_1

    .line 1872
    :goto_1b
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraAntiShake(I)V

    goto/16 :goto_0

    .line 1768
    :pswitch_1d
    const/16 v6, 0x8

    new-array v6, v6, [I

    fill-array-data v6, :array_73

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 1769
    const/16 v6, 0x8

    new-array v6, v6, [I

    fill-array-data v6, :array_74

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 1770
    const/4 v6, 0x6

    new-array v6, v6, [I

    fill-array-data v6, :array_75

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    .line 1772
    const/4 v6, 0x3

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraFocusMode(I)V

    .line 1777
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setFlashMode(I)V

    goto :goto_1b

    .line 1780
    :pswitch_1e
    const/16 v6, 0x9

    new-array v6, v6, [I

    fill-array-data v6, :array_76

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 1781
    const/16 v6, 0x9

    new-array v6, v6, [I

    fill-array-data v6, :array_77

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 1782
    const/4 v6, 0x5

    new-array v6, v6, [I

    fill-array-data v6, :array_78

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    .line 1783
    const/4 v6, 0x2

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraExposureMeter(I)V

    .line 1784
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setFlashMode(I)V

    goto :goto_1b

    .line 1787
    :pswitch_1f
    const/16 v6, 0x9

    new-array v6, v6, [I

    fill-array-data v6, :array_79

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 1788
    const/16 v6, 0x9

    new-array v6, v6, [I

    fill-array-data v6, :array_7a

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 1789
    const/4 v6, 0x6

    new-array v6, v6, [I

    fill-array-data v6, :array_7b

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    .line 1791
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setFlashMode(I)V

    goto :goto_1b

    .line 1794
    :pswitch_20
    const/16 v6, 0x8

    new-array v6, v6, [I

    fill-array-data v6, :array_7c

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 1795
    const/16 v6, 0x8

    new-array v6, v6, [I

    fill-array-data v6, :array_7d

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 1796
    const/4 v6, 0x6

    new-array v6, v6, [I

    fill-array-data v6, :array_7e

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    .line 1798
    const/4 v6, 0x3

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraISO(I)V

    .line 1799
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setFlashMode(I)V

    goto/16 :goto_1b

    .line 1802
    :pswitch_21
    const/16 v6, 0x9

    new-array v6, v6, [I

    fill-array-data v6, :array_7f

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 1803
    const/16 v6, 0x9

    new-array v6, v6, [I

    fill-array-data v6, :array_80

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 1804
    const/4 v6, 0x5

    new-array v6, v6, [I

    fill-array-data v6, :array_81

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    .line 1806
    const/4 v6, 0x1

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraISO(I)V

    .line 1807
    const/4 v6, 0x2

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setExposureValue(I)V

    .line 1808
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setFlashMode(I)V

    goto/16 :goto_1b

    .line 1811
    :pswitch_22
    const/16 v6, 0x9

    new-array v6, v6, [I

    fill-array-data v6, :array_82

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 1812
    const/16 v6, 0x9

    new-array v6, v6, [I

    fill-array-data v6, :array_83

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 1813
    const/4 v6, 0x6

    new-array v6, v6, [I

    fill-array-data v6, :array_84

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    .line 1814
    const/4 v6, 0x1

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setWhiteBalance(I)V

    .line 1815
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setFlashMode(I)V

    goto/16 :goto_1b

    .line 1818
    :pswitch_23
    const/16 v6, 0x9

    new-array v6, v6, [I

    fill-array-data v6, :array_85

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 1819
    const/16 v6, 0x9

    new-array v6, v6, [I

    fill-array-data v6, :array_86

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 1820
    const/4 v6, 0x6

    new-array v6, v6, [I

    fill-array-data v6, :array_87

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    .line 1821
    const/4 v6, 0x4

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setWhiteBalance(I)V

    .line 1822
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setFlashMode(I)V

    goto/16 :goto_1b

    .line 1825
    :pswitch_24
    const/16 v6, 0x9

    new-array v6, v6, [I

    fill-array-data v6, :array_88

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 1826
    const/16 v6, 0x9

    new-array v6, v6, [I

    fill-array-data v6, :array_89

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 1827
    const/4 v6, 0x6

    new-array v6, v6, [I

    fill-array-data v6, :array_8a

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    .line 1828
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setFlashMode(I)V

    goto/16 :goto_1b

    .line 1831
    :pswitch_25
    const/16 v6, 0x9

    new-array v6, v6, [I

    fill-array-data v6, :array_8b

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 1832
    const/16 v6, 0x9

    new-array v6, v6, [I

    fill-array-data v6, :array_8c

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 1833
    const/4 v6, 0x6

    new-array v6, v6, [I

    fill-array-data v6, :array_8d

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    .line 1835
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setFlashMode(I)V

    goto/16 :goto_1b

    .line 1838
    :pswitch_26
    const/16 v6, 0x9

    new-array v6, v6, [I

    fill-array-data v6, :array_8e

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 1839
    const/16 v6, 0x9

    new-array v6, v6, [I

    fill-array-data v6, :array_8f

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 1840
    const/4 v6, 0x6

    new-array v6, v6, [I

    fill-array-data v6, :array_90

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    .line 1841
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setFlashMode(I)V

    goto/16 :goto_1b

    .line 1844
    :pswitch_27
    const/16 v6, 0x8

    new-array v6, v6, [I

    fill-array-data v6, :array_91

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 1845
    const/16 v6, 0x8

    new-array v6, v6, [I

    fill-array-data v6, :array_92

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 1846
    const/4 v6, 0x6

    new-array v6, v6, [I

    fill-array-data v6, :array_93

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    .line 1850
    const/4 v6, 0x2

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraFocusMode(I)V

    .line 1851
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setFlashMode(I)V

    goto/16 :goto_1b

    .line 1854
    :pswitch_28
    const/16 v6, 0x9

    new-array v6, v6, [I

    fill-array-data v6, :array_94

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 1855
    const/16 v6, 0x9

    new-array v6, v6, [I

    fill-array-data v6, :array_95

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 1856
    const/4 v6, 0x6

    new-array v6, v6, [I

    fill-array-data v6, :array_96

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    .line 1857
    const/4 v6, 0x1

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setWhiteBalance(I)V

    .line 1858
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setFlashMode(I)V

    goto/16 :goto_1b

    .line 1861
    :pswitch_29
    const/16 v6, 0x8

    new-array v6, v6, [I

    fill-array-data v6, :array_97

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 1862
    const/16 v6, 0x8

    new-array v6, v6, [I

    fill-array-data v6, :array_98

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 1863
    const/4 v6, 0x5

    new-array v6, v6, [I

    fill-array-data v6, :array_99

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    .line 1865
    const/4 v6, 0x1

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setFlashMode(I)V

    goto/16 :goto_1b

    .line 1874
    :cond_41
    iget-boolean v6, p0, Lcom/sec/android/app/camera/MenuDimController;->mCheckSceneModeSet:Z

    if-eqz v6, :cond_1

    .line 1875
    iget v6, p0, Lcom/sec/android/app/camera/MenuDimController;->mPreviousFlashValue:I

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setFlashMode(I)V

    .line 1876
    iget v6, p0, Lcom/sec/android/app/camera/MenuDimController;->mPreviousMeteringValue:I

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraExposureMeter(I)V

    .line 1877
    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/sec/android/app/camera/MenuDimController;->mCheckSceneModeSet:Z

    goto/16 :goto_0

    .line 1882
    :sswitch_4
    if-eqz p2, :cond_1

    .line 1883
    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v7, 0x0

    const/4 v8, 0x5

    aput v8, v6, v7

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 1884
    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v7, 0x0

    const/4 v8, 0x5

    aput v8, v6, v7

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 1885
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraAntiShake(I)V

    goto/16 :goto_0

    .line 1896
    :sswitch_5
    if-ne p2, v9, :cond_42

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->isBackCamera()Z

    move-result v6

    if-eqz v6, :cond_42

    .line 1897
    const/4 v6, 0x5

    new-array v6, v6, [I

    fill-array-data v6, :array_9a

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 1898
    const/4 v6, 0x5

    new-array v6, v6, [I

    fill-array-data v6, :array_9b

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 1899
    const/4 v6, 0x4

    new-array v6, v6, [I

    fill-array-data v6, :array_9c

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    .line 1900
    const/4 v6, 0x2

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraExposureMeter(I)V

    .line 1905
    :cond_42
    if-nez p2, :cond_1

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->isBackCamera()Z

    move-result v6

    if-eqz v6, :cond_1

    goto/16 :goto_0

    .line 1913
    :sswitch_6
    if-ne p2, v9, :cond_1

    .line 1914
    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v7, 0x0

    const/16 v8, 0x8

    aput v8, v6, v7

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 1915
    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v7, 0x0

    const/16 v8, 0x8

    aput v8, v6, v7

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 1916
    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v7, 0x0

    const/16 v8, 0x8

    aput v8, v6, v7

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    goto/16 :goto_0

    .line 1920
    :sswitch_7
    if-eqz p2, :cond_43

    .line 1921
    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v7, 0x0

    const/16 v8, 0x1b

    aput v8, v6, v7

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 1922
    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v7, 0x0

    const/16 v8, 0x1b

    aput v8, v6, v7

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 1923
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCamcorderAntishake(I)V

    .line 1925
    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->getShootingMode()I

    move-result v6

    const/16 v7, 0x3c

    if-eq v6, v7, :cond_43

    .line 1926
    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v7, 0x0

    const/16 v8, 0x9

    aput v8, v6, v7

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 1927
    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v7, 0x0

    const/16 v8, 0x9

    aput v8, v6, v7

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 1928
    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v7, 0x0

    const/16 v8, 0x9

    aput v8, v6, v7

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    .line 1931
    :cond_43
    if-ne p2, v9, :cond_47

    .line 1932
    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v7, 0x0

    const/16 v8, 0x25

    aput v8, v6, v7

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 1933
    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v7, 0x0

    const/16 v8, 0x25

    aput v8, v6, v7

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 1934
    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v7, 0x0

    const/16 v8, 0x25

    aput v8, v6, v7

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    .line 1935
    const/4 v6, 0x1

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCamcorderAudioRecording(I)V

    .line 1936
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCamcorderAntishake(I)V

    .line 1937
    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->isBackCamera()Z

    move-result v6

    if-eqz v6, :cond_45

    .line 1938
    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v7, 0x0

    const/16 v8, 0x18

    aput v8, v6, v7

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 1939
    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v7, 0x0

    const/16 v8, 0x18

    aput v8, v6, v7

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 1941
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v6

    const-string v7, "CscFeature_Message_MmsModeCaptureVideoResolution"

    invoke-virtual {v6, v7}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_44

    .line 1942
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v6

    const-string v7, "CscFeature_Message_MmsModeCaptureVideoResolution"

    invoke-virtual {v6, v7}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/app/camera/CameraResolution;->getResolutionID(Ljava/lang/String;)I

    move-result v5

    .line 1946
    .restart local v5    # "resolutionId":I
    :goto_1c
    invoke-virtual {v0, v5}, Lcom/sec/android/app/camera/CameraSettings;->setCamcorderResolution(I)Z

    goto/16 :goto_0

    .line 1944
    .end local v5    # "resolutionId":I
    :cond_44
    const/16 v5, 0x13

    .restart local v5    # "resolutionId":I
    goto :goto_1c

    .line 1948
    .end local v5    # "resolutionId":I
    :cond_45
    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v7, 0x0

    const/16 v8, 0x18

    aput v8, v6, v7

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 1949
    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v7, 0x0

    const/16 v8, 0x18

    aput v8, v6, v7

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 1951
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v6

    const-string v7, "CscFeature_Message_MmsModeCaptureVideoResolution"

    invoke-virtual {v6, v7}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_46

    .line 1952
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v6

    const-string v7, "CscFeature_Message_MmsModeCaptureVideoResolution"

    invoke-virtual {v6, v7}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/app/camera/CameraResolution;->getResolutionID(Ljava/lang/String;)I

    move-result v5

    .line 1956
    .restart local v5    # "resolutionId":I
    :goto_1d
    invoke-virtual {v0, v5}, Lcom/sec/android/app/camera/CameraSettings;->setCamcorderResolution(I)Z

    goto/16 :goto_0

    .line 1954
    .end local v5    # "resolutionId":I
    :cond_46
    const/16 v5, 0x13

    .restart local v5    # "resolutionId":I
    goto :goto_1d

    .line 1958
    .end local v5    # "resolutionId":I
    :cond_47
    const/4 v6, 0x6

    if-ne p2, v6, :cond_48

    .line 1959
    const/4 v6, 0x1

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCamcorderAudioRecording(I)V

    goto/16 :goto_0

    .line 1960
    :cond_48
    if-nez p2, :cond_4a

    .line 1961
    const/4 v6, 0x1

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCamcorderAudioRecording(I)V

    .line 1962
    iget-object v6, p0, Lcom/sec/android/app/camera/MenuDimController;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v6}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/camera/CameraSettings;->isVideocallPresetSelected()Z

    move-result v6

    if-eqz v6, :cond_49

    .line 1963
    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v7, 0x0

    const/16 v8, 0x18

    aput v8, v6, v7

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 1964
    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v7, 0x0

    const/16 v8, 0x18

    aput v8, v6, v7

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 1965
    const/16 v6, 0x13

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCamcorderResolution(I)Z

    goto/16 :goto_0

    .line 1966
    :cond_49
    iget-object v6, p0, Lcom/sec/android/app/camera/MenuDimController;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v6}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/camera/CameraSettings;->isRCSMMSMode()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1967
    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v7, 0x0

    const/16 v8, 0x18

    aput v8, v6, v7

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 1968
    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v7, 0x0

    const/16 v8, 0x18

    aput v8, v6, v7

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 1969
    const/16 v6, 0x12

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCamcorderResolution(I)Z

    goto/16 :goto_0

    .line 1971
    :cond_4a
    if-ne p2, v10, :cond_4b

    .line 1972
    const/4 v6, 0x2

    new-array v6, v6, [I

    fill-array-data v6, :array_9d

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 1973
    const/4 v6, 0x2

    new-array v6, v6, [I

    fill-array-data v6, :array_9e

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 1974
    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v7, 0x0

    const/16 v8, 0x25

    aput v8, v6, v7

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    .line 1975
    const-string v6, "1280x720"

    invoke-static {v6}, Lcom/sec/android/app/camera/CameraResolution;->getResolutionID(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCamcorderResolution(I)Z

    .line 1976
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCamcorderAudioRecording(I)V

    goto/16 :goto_0

    .line 1977
    :cond_4b
    if-ne p2, v11, :cond_4c

    .line 1978
    const/4 v6, 0x2

    new-array v6, v6, [I

    fill-array-data v6, :array_9f

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 1979
    const/4 v6, 0x2

    new-array v6, v6, [I

    fill-array-data v6, :array_a0

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 1980
    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v7, 0x0

    const/16 v8, 0x25

    aput v8, v6, v7

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    .line 1981
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCamcorderAudioRecording(I)V

    .line 1982
    const/16 v6, 0xd

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCamcorderResolution(I)Z

    goto/16 :goto_0

    .line 1983
    :cond_4c
    const/4 v6, 0x4

    if-ne p2, v6, :cond_1

    .line 1984
    const/4 v6, 0x1

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCamcorderAudioRecording(I)V

    .line 1985
    const/4 v6, 0x2

    new-array v6, v6, [I

    fill-array-data v6, :array_a1

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 1986
    const/4 v6, 0x2

    new-array v6, v6, [I

    fill-array-data v6, :array_a2

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 1987
    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v7, 0x0

    const/16 v8, 0x25

    aput v8, v6, v7

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    .line 1988
    const/16 v6, 0x2a

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCamcorderResolution(I)Z

    goto/16 :goto_0

    .line 1993
    :sswitch_8
    iget-object v6, p0, Lcom/sec/android/app/camera/MenuDimController;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    const-string v7, "enterprise_policy"

    invoke-virtual {v6, v7}, Lcom/sec/android/app/camera/Camera;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/enterprise/EnterpriseDeviceManager;

    .line 1994
    .local v2, "edm":Landroid/app/enterprise/EnterpriseDeviceManager;
    invoke-static {}, Lcom/sec/android/app/camera/CheckMemory;->isStorageMounted()Z

    move-result v6

    if-eqz v6, :cond_4d

    iget-object v6, p0, Lcom/sec/android/app/camera/MenuDimController;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-static {v6}, Lcom/sec/android/app/camera/Util;->isKNOXMode(Landroid/content/Context;)Z

    move-result v6

    if-nez v6, :cond_4d

    invoke-virtual {v2}, Landroid/app/enterprise/EnterpriseDeviceManager;->getRestrictionPolicy()Landroid/app/enterprise/RestrictionPolicy;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/enterprise/RestrictionPolicy;->isSDCardWriteAllowed()Z

    move-result v6

    if-nez v6, :cond_1

    .line 1995
    :cond_4d
    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v7, 0x0

    const/16 v8, 0x13

    aput v8, v6, v7

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 1996
    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v7, 0x0

    const/16 v8, 0x13

    aput v8, v6, v7

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    goto/16 :goto_0

    .line 2000
    .end local v2    # "edm":Landroid/app/enterprise/EnterpriseDeviceManager;
    :sswitch_9
    if-ne p2, v9, :cond_50

    .line 2001
    iget-object v6, p0, Lcom/sec/android/app/camera/MenuDimController;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v6}, Lcom/sec/android/app/camera/Camera;->isFromSmusician()Z

    move-result v6

    if-eqz v6, :cond_4e

    .line 2002
    const/16 v6, 0xe

    new-array v6, v6, [I

    fill-array-data v6, :array_a3

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 2004
    const/16 v6, 0xd

    new-array v6, v6, [I

    fill-array-data v6, :array_a4

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 2006
    const/4 v6, 0x5

    new-array v6, v6, [I

    fill-array-data v6, :array_a5

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    .line 2007
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCamcorderAntishake(I)V

    .line 2008
    iget-object v6, p0, Lcom/sec/android/app/camera/MenuDimController;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v6}, Lcom/sec/android/app/camera/Camera;->isPanoramaFromSmusician()Z

    move-result v6

    if-nez v6, :cond_1

    .line 2009
    const/16 v6, 0x17

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraResolution(I)Z

    goto/16 :goto_0

    .line 2011
    :cond_4e
    const/16 v6, 0xa

    new-array v6, v6, [I

    fill-array-data v6, :array_a6

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 2012
    const/16 v6, 0x9

    new-array v6, v6, [I

    fill-array-data v6, :array_a7

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 2013
    const/4 v6, 0x5

    new-array v6, v6, [I

    fill-array-data v6, :array_a8

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    .line 2014
    iget-object v6, p0, Lcom/sec/android/app/camera/MenuDimController;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v6}, Lcom/sec/android/app/camera/Camera;->isFromGalleryForMagicshot()Z

    move-result v6

    if-eqz v6, :cond_4f

    .line 2015
    const/4 v6, 0x3

    new-array v6, v6, [I

    fill-array-data v6, :array_a9

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 2017
    :cond_4f
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCamcorderAntishake(I)V

    goto/16 :goto_0

    .line 2019
    :cond_50
    if-ne p2, v10, :cond_53

    .line 2020
    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->isVideocallPresetSelected()Z

    move-result v6

    if-eqz v6, :cond_51

    .line 2021
    const/16 v6, 0x14

    new-array v6, v6, [I

    fill-array-data v6, :array_aa

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 2023
    const/16 v6, 0x13

    new-array v6, v6, [I

    fill-array-data v6, :array_ab

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 2025
    const/16 v6, 0x8

    new-array v6, v6, [I

    fill-array-data v6, :array_ac

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    .line 2026
    const/16 v6, 0x13

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCamcorderResolution(I)Z

    .line 2042
    :goto_1e
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setSideTouchMode(I)V

    goto/16 :goto_0

    .line 2027
    :cond_51
    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->isRCSMMSMode()Z

    move-result v6

    if-eqz v6, :cond_52

    .line 2028
    const/16 v6, 0x14

    new-array v6, v6, [I

    fill-array-data v6, :array_ad

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 2030
    const/16 v6, 0x13

    new-array v6, v6, [I

    fill-array-data v6, :array_ae

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 2032
    const/16 v6, 0x8

    new-array v6, v6, [I

    fill-array-data v6, :array_af

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    .line 2033
    const/16 v6, 0x12

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCamcorderResolution(I)Z

    goto :goto_1e

    .line 2035
    :cond_52
    const/16 v6, 0x11

    new-array v6, v6, [I

    fill-array-data v6, :array_b0

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 2037
    const/16 v6, 0x11

    new-array v6, v6, [I

    fill-array-data v6, :array_b1

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 2039
    const/16 v6, 0xc

    new-array v6, v6, [I

    fill-array-data v6, :array_b2

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    goto :goto_1e

    .line 2043
    :cond_53
    if-ne p2, v11, :cond_55

    .line 2045
    const/16 v6, 0x14

    new-array v6, v6, [I

    fill-array-data v6, :array_b3

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 2051
    const/16 v6, 0x12

    new-array v6, v6, [I

    fill-array-data v6, :array_b4

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 2053
    const/16 v6, 0xc

    new-array v6, v6, [I

    fill-array-data v6, :array_b5

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    .line 2055
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCamcorderAntishake(I)V

    .line 2056
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setSideTouchMode(I)V

    .line 2057
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v6

    const-string v7, "CscFeature_Message_MmsModeCaptureVideoResolution"

    invoke-virtual {v6, v7}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_54

    .line 2058
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v6

    const-string v7, "CscFeature_Message_MmsModeCaptureVideoResolution"

    invoke-virtual {v6, v7}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/app/camera/CameraResolution;->getResolutionID(Ljava/lang/String;)I

    move-result v5

    .line 2062
    .restart local v5    # "resolutionId":I
    :goto_1f
    invoke-virtual {v0, v5}, Lcom/sec/android/app/camera/CameraSettings;->setCamcorderResolution(I)Z

    goto/16 :goto_0

    .line 2060
    .end local v5    # "resolutionId":I
    :cond_54
    const/16 v5, 0x13

    .restart local v5    # "resolutionId":I
    goto :goto_1f

    .line 2063
    .end local v5    # "resolutionId":I
    :cond_55
    const/4 v6, 0x4

    if-ne p2, v6, :cond_1

    .line 2065
    const/16 v6, 0x11

    new-array v6, v6, [I

    fill-array-data v6, :array_b6

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 2071
    const/16 v6, 0x10

    new-array v6, v6, [I

    fill-array-data v6, :array_b7

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 2073
    const/16 v6, 0xd

    new-array v6, v6, [I

    fill-array-data v6, :array_b8

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    .line 2075
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setSideTouchMode(I)V

    goto/16 :goto_0

    .line 2079
    :sswitch_a
    if-nez p2, :cond_1

    .line 2080
    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v7, 0x0

    const/16 v8, 0x15

    aput v8, v6, v7

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 2081
    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v7, 0x0

    const/16 v8, 0x15

    aput v8, v6, v7

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    goto/16 :goto_0

    .line 2102
    :sswitch_b
    const/16 v6, 0xd

    if-eq p2, v6, :cond_56

    const/16 v6, 0x1f

    if-eq p2, v6, :cond_56

    const/16 v6, 0xe

    if-eq p2, v6, :cond_56

    const/16 v6, 0x38

    if-eq p2, v6, :cond_56

    .line 2103
    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v7, 0x0

    const/16 v8, 0x1b

    aput v8, v6, v7

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 2104
    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v7, 0x0

    const/16 v8, 0x1b

    aput v8, v6, v7

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 2105
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCamcorderAntishake(I)V

    .line 2107
    :cond_56
    const/16 v6, 0x28

    if-eq p2, v6, :cond_57

    const/16 v6, 0x15

    if-ne p2, v6, :cond_1

    .line 2108
    :cond_57
    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->getRemoteViewfinderMode()I

    move-result v6

    if-ne v6, v9, :cond_1

    .line 2109
    iget-object v6, p0, Lcom/sec/android/app/camera/MenuDimController;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/sec/android/app/camera/MenuDimController;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v6}, Lcom/sec/android/app/camera/Camera;->getCameraBaseMenu()Lcom/sec/android/app/camera/glwidget/TwGLCameraBaseMenu;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 2110
    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v7, 0x0

    const/4 v8, 0x2

    aput v8, v6, v7

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    goto/16 :goto_0

    .line 2118
    :sswitch_c
    invoke-static {p2}, Lcom/sec/android/app/camera/CameraResolution;->isWideResolution(I)Z

    move-result v6

    if-nez v6, :cond_1

    .line 2119
    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v7, 0x0

    const/16 v8, 0x2d

    aput v8, v6, v7

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 2120
    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v7, 0x0

    const/16 v8, 0x2d

    aput v8, v6, v7

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    goto/16 :goto_0

    .line 2124
    :sswitch_d
    if-ne p2, v9, :cond_1

    .line 2125
    const/16 v6, 0x9

    new-array v6, v6, [I

    fill-array-data v6, :array_b9

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 2126
    const/4 v6, 0x3

    new-array v6, v6, [I

    fill-array-data v6, :array_ba

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 2127
    const/4 v6, 0x7

    new-array v6, v6, [I

    fill-array-data v6, :array_bb

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    .line 2128
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCamcorderAntishake(I)V

    .line 2136
    iget-object v6, p0, Lcom/sec/android/app/camera/MenuDimController;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v6}, Lcom/sec/android/app/camera/Camera;->getMenuResourceDepot()Lcom/sec/android/app/camera/MenuResourceDepot;

    move-result-object v3

    .line 2137
    .local v3, "menuResourceDepot":Lcom/sec/android/app/camera/MenuResourceDepot;
    const/16 v6, 0xbc1

    invoke-virtual {v3, v6}, Lcom/sec/android/app/camera/MenuResourceDepot;->getResource(I)Lcom/sec/android/app/camera/resourcedata/MenuResourceBase;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/camera/resourcedata/MenuResourceBase;->getNumberOfItems()I

    move-result v6

    if-gt v6, v9, :cond_58

    .line 2138
    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v7, 0x0

    const/16 v8, 0x18

    aput v8, v6, v7

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 2140
    :cond_58
    const/16 v6, 0x65

    invoke-virtual {v3, v6}, Lcom/sec/android/app/camera/MenuResourceDepot;->getResource(I)Lcom/sec/android/app/camera/resourcedata/MenuResourceBase;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/camera/resourcedata/MenuResourceBase;->getNumberOfItems()I

    move-result v6

    if-gt v6, v9, :cond_1

    .line 2141
    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v7, 0x0

    const/16 v8, 0xf

    aput v8, v6, v7

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    goto/16 :goto_0

    .line 2146
    .end local v3    # "menuResourceDepot":Lcom/sec/android/app/camera/MenuResourceDepot;
    :sswitch_e
    if-ne p2, v9, :cond_1

    .line 2147
    const/4 v6, 0x2

    new-array v6, v6, [I

    fill-array-data v6, :array_bc

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 2148
    const/4 v6, 0x2

    new-array v6, v6, [I

    fill-array-data v6, :array_bd

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 2149
    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v7, 0x0

    const/4 v8, 0x7

    aput v8, v6, v7

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    .line 2150
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setStorage(I)V

    goto/16 :goto_0

    .line 2154
    :sswitch_f
    if-ne p2, v9, :cond_59

    .line 2160
    const/4 v6, 0x2

    new-array v6, v6, [I

    fill-array-data v6, :array_be

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 2161
    const/4 v6, 0x2

    new-array v6, v6, [I

    fill-array-data v6, :array_bf

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 2162
    const/4 v6, 0x2

    new-array v6, v6, [I

    fill-array-data v6, :array_c0

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    goto/16 :goto_0

    .line 2165
    :cond_59
    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v7, 0x0

    const/16 v8, 0x34

    aput v8, v6, v7

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 2166
    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v7, 0x0

    const/16 v8, 0x34

    aput v8, v6, v7

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 2167
    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v7, 0x0

    const/16 v8, 0x34

    aput v8, v6, v7

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    goto/16 :goto_0

    .line 2171
    :sswitch_10
    if-ne p2, v9, :cond_1

    .line 2172
    const/4 v6, 0x6

    new-array v6, v6, [I

    fill-array-data v6, :array_c1

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 2173
    const/4 v6, 0x4

    new-array v6, v6, [I

    fill-array-data v6, :array_c2

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 2174
    const/4 v6, 0x4

    new-array v6, v6, [I

    fill-array-data v6, :array_c3

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    goto/16 :goto_0

    .line 2179
    :sswitch_11
    packed-switch p2, :pswitch_data_2

    goto/16 :goto_0

    .line 2181
    :pswitch_2a
    const/4 v6, 0x5

    new-array v6, v6, [I

    fill-array-data v6, :array_c4

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 2182
    const/4 v6, 0x4

    new-array v6, v6, [I

    fill-array-data v6, :array_c5

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    goto/16 :goto_0

    .line 2185
    :pswitch_2b
    const/4 v6, 0x4

    new-array v6, v6, [I

    fill-array-data v6, :array_c6

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 2186
    const/4 v6, 0x3

    new-array v6, v6, [I

    fill-array-data v6, :array_c7

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    goto/16 :goto_0

    .line 2189
    :pswitch_2c
    const/4 v6, 0x5

    new-array v6, v6, [I

    fill-array-data v6, :array_c8

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 2190
    const/4 v6, 0x4

    new-array v6, v6, [I

    fill-array-data v6, :array_c9

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 2191
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setAutoNightDetectionMode(I)V

    goto/16 :goto_0

    .line 2194
    :pswitch_2d
    const/4 v6, 0x5

    new-array v6, v6, [I

    fill-array-data v6, :array_ca

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 2195
    const/4 v6, 0x4

    new-array v6, v6, [I

    fill-array-data v6, :array_cb

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    goto/16 :goto_0

    .line 2202
    :sswitch_12
    packed-switch p2, :pswitch_data_3

    :pswitch_2e
    goto/16 :goto_0

    .line 2204
    :pswitch_2f
    const/4 v6, 0x4

    new-array v6, v6, [I

    fill-array-data v6, :array_cc

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 2205
    const/4 v6, 0x3

    new-array v6, v6, [I

    fill-array-data v6, :array_cd

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    goto/16 :goto_0

    .line 2208
    :pswitch_30
    const/4 v6, 0x5

    new-array v6, v6, [I

    fill-array-data v6, :array_ce

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 2209
    const/4 v6, 0x4

    new-array v6, v6, [I

    fill-array-data v6, :array_cf

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    goto/16 :goto_0

    .line 2216
    :sswitch_13
    packed-switch p2, :pswitch_data_4

    goto/16 :goto_0

    .line 2218
    :pswitch_31
    const/4 v6, 0x4

    new-array v6, v6, [I

    fill-array-data v6, :array_d0

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 2219
    const/4 v6, 0x3

    new-array v6, v6, [I

    fill-array-data v6, :array_d1

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    goto/16 :goto_0

    .line 2222
    :pswitch_32
    const/4 v6, 0x5

    new-array v6, v6, [I

    fill-array-data v6, :array_d2

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 2223
    const/4 v6, 0x4

    new-array v6, v6, [I

    fill-array-data v6, :array_d3

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    goto/16 :goto_0

    .line 2226
    :pswitch_33
    const/4 v6, 0x5

    new-array v6, v6, [I

    fill-array-data v6, :array_d4

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 2227
    const/4 v6, 0x4

    new-array v6, v6, [I

    fill-array-data v6, :array_d5

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    goto/16 :goto_0

    .line 2230
    :pswitch_34
    const/4 v6, 0x5

    new-array v6, v6, [I

    fill-array-data v6, :array_d6

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 2231
    const/4 v6, 0x4

    new-array v6, v6, [I

    fill-array-data v6, :array_d7

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    goto/16 :goto_0

    .line 2240
    :sswitch_14
    if-ne p2, v9, :cond_1

    .line 2241
    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v7, 0x0

    const/16 v8, 0xb

    aput v8, v6, v7

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 2242
    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v7, 0x0

    const/16 v8, 0xb

    aput v8, v6, v7

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 2243
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setFlashMode(I)V

    goto/16 :goto_0

    .line 2249
    :sswitch_15
    if-ne p2, v9, :cond_1

    .line 2250
    iget-object v6, p0, Lcom/sec/android/app/camera/MenuDimController;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v6}, Lcom/sec/android/app/camera/Camera;->isEnableDuringCall()Z

    move-result v6

    if-nez v6, :cond_5a

    iget-object v6, p0, Lcom/sec/android/app/camera/MenuDimController;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v6}, Lcom/sec/android/app/camera/Camera;->isSupportMirrorCall()Z

    move-result v6

    if-eqz v6, :cond_5b

    .line 2251
    :cond_5a
    const/4 v6, 0x3

    new-array v6, v6, [I

    fill-array-data v6, :array_d8

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 2252
    const/4 v6, 0x3

    new-array v6, v6, [I

    fill-array-data v6, :array_d9

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 2257
    :goto_20
    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v7, 0x0

    const/16 v8, 0x1f

    aput v8, v6, v7

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    .line 2258
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraShutterSound(I)V

    goto/16 :goto_0

    .line 2254
    :cond_5b
    const/4 v6, 0x7

    new-array v6, v6, [I

    fill-array-data v6, :array_da

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 2255
    const/4 v6, 0x6

    new-array v6, v6, [I

    fill-array-data v6, :array_db

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    goto :goto_20

    .line 2263
    :sswitch_16
    if-ne p2, v9, :cond_1

    .line 2264
    const/16 v6, 0x8

    new-array v6, v6, [I

    fill-array-data v6, :array_dc

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 2265
    const/16 v6, 0x8

    new-array v6, v6, [I

    fill-array-data v6, :array_dd

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 2266
    const/16 v6, 0x8

    new-array v6, v6, [I

    fill-array-data v6, :array_de

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    goto/16 :goto_0

    .line 2271
    :sswitch_17
    if-eqz p2, :cond_5c

    .line 2272
    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v7, 0x0

    const/16 v8, 0x1e

    aput v8, v6, v7

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 2273
    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v7, 0x0

    const/16 v8, 0x1e

    aput v8, v6, v7

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 2274
    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v7, 0x0

    const/16 v8, 0x1e

    aput v8, v6, v7

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    goto/16 :goto_0

    .line 2275
    :cond_5c
    if-nez p2, :cond_1

    goto/16 :goto_0

    .line 2283
    :sswitch_18
    if-nez p2, :cond_5d

    .line 2284
    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v7, 0x0

    const/16 v8, 0x13

    aput v8, v6, v7

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 2285
    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v7, 0x0

    const/16 v8, 0x13

    aput v8, v6, v7

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 2286
    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v7, 0x0

    const/16 v8, 0x13

    aput v8, v6, v7

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    .line 2291
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraQuality(I)V

    goto/16 :goto_0

    .line 2294
    :cond_5d
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraQuality(I)V

    goto/16 :goto_0

    .line 2298
    :sswitch_19
    if-nez p2, :cond_5e

    .line 2299
    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v7, 0x0

    const/16 v8, 0x13

    aput v8, v6, v7

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 2300
    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v7, 0x0

    const/16 v8, 0x13

    aput v8, v6, v7

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 2301
    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v7, 0x0

    const/16 v8, 0x13

    aput v8, v6, v7

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    .line 2306
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraQuality(I)V

    goto/16 :goto_0

    .line 2309
    :cond_5e
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraQuality(I)V

    goto/16 :goto_0

    .line 2313
    :sswitch_1a
    if-nez p2, :cond_5f

    .line 2317
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraQuality(I)V

    goto/16 :goto_0

    .line 2320
    :cond_5f
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraQuality(I)V

    goto/16 :goto_0

    .line 2324
    :sswitch_1b
    if-eq p2, v9, :cond_1

    .line 2325
    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v7, 0x0

    const/16 v8, 0x12

    aput v8, v6, v7

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 2326
    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v7, 0x0

    const/16 v8, 0x12

    aput v8, v6, v7

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 2327
    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v7, 0x0

    const/16 v8, 0x12

    aput v8, v6, v7

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    goto/16 :goto_0

    .line 2332
    :sswitch_1c
    const/16 v6, 0x33

    if-ne p2, v6, :cond_1

    .line 2333
    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v7, 0x0

    const/4 v8, 0x0

    aput v8, v6, v7

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    goto/16 :goto_0

    .line 2338
    :sswitch_1d
    if-ne p2, v9, :cond_1

    .line 2339
    iget-object v6, p0, Lcom/sec/android/app/camera/MenuDimController;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v6}, Lcom/sec/android/app/camera/Camera;->isEasyModeSettingOn()Z

    move-result v6

    if-eqz v6, :cond_60

    .line 2340
    const/16 v6, 0xa

    new-array v6, v6, [I

    fill-array-data v6, :array_df

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 2341
    const/16 v6, 0xa

    new-array v6, v6, [I

    fill-array-data v6, :array_e0

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 2342
    const/16 v6, 0xa

    new-array v6, v6, [I

    fill-array-data v6, :array_e1

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    goto/16 :goto_0

    .line 2345
    :cond_60
    const/16 v6, 0xb

    new-array v6, v6, [I

    fill-array-data v6, :array_e2

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 2346
    const/16 v6, 0xb

    new-array v6, v6, [I

    fill-array-data v6, :array_e3

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 2347
    const/16 v6, 0xb

    new-array v6, v6, [I

    fill-array-data v6, :array_e4

    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/MenuDimController;->setSettingDefault([I)V

    goto/16 :goto_0

    .line 2356
    :sswitch_1e
    if-nez p2, :cond_1

    .line 2357
    const/4 v6, 0x2

    new-array v6, v6, [I

    fill-array-data v6, :array_e5

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 2358
    const/4 v6, 0x2

    new-array v6, v6, [I

    fill-array-data v6, :array_e6

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->saveSettingMulti([I)V

    .line 2359
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setStorage(I)V

    .line 2363
    const/4 v6, 0x1

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraQuality(I)V

    goto/16 :goto_0

    .line 2368
    :sswitch_1f
    if-ne p2, v9, :cond_1

    .line 2369
    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v7, 0x0

    const/16 v8, 0x1f

    aput v8, v6, v7

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V

    .line 2370
    const/4 v6, 0x1

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraVoiceCommand(I)V

    goto/16 :goto_0

    .line 2374
    :sswitch_20
    if-ne p2, v9, :cond_1

    .line 2375
    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v7, 0x0

    const/16 v8, 0x2f

    aput v8, v6, v7

    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->setDimMulti([I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 939
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_9
        0x1 -> :sswitch_2
        0x2 -> :sswitch_3
        0x4 -> :sswitch_c
        0x6 -> :sswitch_17
        0x8 -> :sswitch_0
        0xa -> :sswitch_4
        0xc -> :sswitch_5
        0xd -> :sswitch_6
        0x16 -> :sswitch_8
        0x1d -> :sswitch_a
        0x24 -> :sswitch_d
        0x3f -> :sswitch_e
        0x41 -> :sswitch_1
        0x57 -> :sswitch_f
        0x59 -> :sswitch_16
        0x5a -> :sswitch_1c
        0x63 -> :sswitch_10
        0x77 -> :sswitch_18
        0x79 -> :sswitch_5
        0x7c -> :sswitch_1a
        0x7d -> :sswitch_1b
        0x7e -> :sswitch_19
        0xc9 -> :sswitch_11
        0xca -> :sswitch_12
        0xcb -> :sswitch_13
        0x12c -> :sswitch_14
        0x136 -> :sswitch_15
        0x137 -> :sswitch_1d
        0x139 -> :sswitch_1e
        0xbb8 -> :sswitch_7
        0xbb9 -> :sswitch_b
        0x1770 -> :sswitch_1f
        0x1771 -> :sswitch_20
    .end sparse-switch

    .line 2395
    :array_0
    .array-data 4
        0x3
        0x2b
        0x9
        0x1e
        0x1f
        0x22
        0x2a
        0x17
        0x27
        0x30
    .end array-data

    .line 2396
    :array_1
    .array-data 4
        0x3
        0x2b
        0x9
        0x1e
        0x1f
        0x22
        0x17
        0x27
        0x30
    .end array-data

    .line 2397
    :array_2
    .array-data 4
        0x2b
        0x9
        0x1e
        0x1f
        0x22
        0x17
        0x27
        0x30
    .end array-data

    .line 2410
    :array_3
    .array-data 4
        0x3
        0x27
        0x2b
        0x2a
        0x22
        0x30
    .end array-data

    .line 2411
    :array_4
    .array-data 4
        0x27
        0x2b
        0x22
        0x30
    .end array-data

    .line 2412
    :array_5
    .array-data 4
        0x27
        0x2b
        0x22
        0x30
    .end array-data

    .line 949
    :array_6
    .array-data 4
        0x4
        0x1e
        0xb
        0xc
        0x1b
        0x8
        0xd
        0x17
        0x10
        0xe
        0x20
        0x21
        0x34
        0x12
        0x2b
        0x30
        0x31
        0x2e
    .end array-data

    .line 951
    :array_7
    .array-data 4
        0x4
        0x1e
        0xb
        0xc
        0x1b
        0x8
        0xd
        0x10
        0xe
        0x20
        0x21
        0x34
        0x12
        0x2b
        0x30
        0x31
        0x2e
    .end array-data

    .line 953
    :array_8
    .array-data 4
        0x4
        0x1e
        0xb
        0x8
        0xd
        0x10
        0xe
        0x20
        0x21
        0x34
        0x12
        0x2b
        0x30
        0x31
        0x2e
    .end array-data

    .line 956
    :array_9
    .array-data 4
        0x4
        0x1e
        0xb
        0xc
        0x1b
        0x8
        0xd
        0x17
        0x10
        0xe
        0x20
        0x21
        0x34
        0x12
        0x2b
        0x30
        0x31
    .end array-data

    .line 958
    :array_a
    .array-data 4
        0x4
        0x1e
        0xb
        0xc
        0x1b
        0x8
        0xd
        0x10
        0xe
        0x20
        0x21
        0x34
        0x12
        0x2b
        0x30
        0x31
    .end array-data

    .line 960
    :array_b
    .array-data 4
        0x4
        0x1e
        0xb
        0x8
        0xd
        0x10
        0xe
        0x20
        0x21
        0x34
        0x12
        0x2b
        0x30
        0x31
    .end array-data

    .line 991
    :array_c
    .array-data 4
        0x2
        0x1f
    .end array-data

    .line 992
    :array_d
    .array-data 4
        0x2
        0x1f
    .end array-data

    .line 998
    :array_e
    .array-data 4
        0x1a
        0x2e
    .end array-data

    .line 1014
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_9
        :pswitch_b
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_d
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_f
        :pswitch_10
        :pswitch_0
        :pswitch_c
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_11
        :pswitch_4
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_15
        :pswitch_0
        :pswitch_0
        :pswitch_7
        :pswitch_16
        :pswitch_0
        :pswitch_17
        :pswitch_8
        :pswitch_12
        :pswitch_13
        :pswitch_0
        :pswitch_14
        :pswitch_0
        :pswitch_0
        :pswitch_1a
        :pswitch_18
        :pswitch_19
        :pswitch_1b
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_a
        :pswitch_0
        :pswitch_0
        :pswitch_e
        :pswitch_1c
        :pswitch_0
        :pswitch_5
    .end packed-switch

    .line 1727
    :array_f
    .array-data 4
        0x1c
        0x23
    .end array-data

    .line 1730
    :array_10
    .array-data 4
        0x4
        0x2b
    .end array-data

    .line 1731
    :array_11
    .array-data 4
        0x4
        0x2b
    .end array-data

    .line 1732
    :array_12
    .array-data 4
        0x4
        0x2b
    .end array-data

    .line 1016
    :array_13
    .array-data 4
        0x4
        0x9
        0x7
        0xb
        0xf
        0x8
        0x5
        0x1d
        0x1e
        0x20
        0x21
        0x1c
        0x23
        0x2b
    .end array-data

    .line 1018
    :array_14
    .array-data 4
        0x4
        0x7
        0xb
        0xf
        0x8
        0x5
        0x1d
        0x1e
        0x20
        0x21
        0x2b
    .end array-data

    .line 1019
    :array_15
    .array-data 4
        0x4
        0x7
        0xb
        0x8
        0x5
        0x1e
        0x20
        0x21
        0x2b
    .end array-data

    .line 1029
    :array_16
    .array-data 4
        0x7
        0x1f
        0xb
        0xc
        0x11
        0x1d
        0x1e
        0x21
        0x1c
        0x27
        0x23
        0x24
        0x13
        0x30
        0x31
        0x2e
    .end array-data

    .line 1031
    :array_17
    .array-data 4
        0x7
        0x1f
        0xb
        0xc
        0x11
        0x1d
        0x1e
        0x21
        0x27
        0x24
        0x13
        0x30
        0x31
        0x2e
    .end array-data

    .line 1034
    :array_18
    .array-data 4
        0x1d
        0x7
        0x1f
        0x1e
        0x21
        0x11
        0x27
        0x13
        0x30
        0x31
        0x2e
    .end array-data

    .line 1037
    :array_19
    .array-data 4
        0x1d
        0x7
        0x1f
        0x1e
        0x21
        0x11
        0x27
        0x24
        0x13
        0x30
        0x31
        0x2e
    .end array-data

    .line 1078
    :array_1a
    .array-data 4
        0x4
        0x9
        0x7
        0xb
        0x8
        0xc
        0x11
        0x10
        0xe
        0x5
        0x1d
        0x1e
        0x12
        0x20
        0x21
        0x1c
        0x23
        0x27
        0x2b
    .end array-data

    .line 1080
    :array_1b
    .array-data 4
        0x4
        0x7
        0xb
        0x8
        0xc
        0x11
        0x10
        0xe
        0x5
        0x1d
        0x1e
        0x12
        0x20
        0x21
        0x27
        0x2b
    .end array-data

    .line 1083
    :array_1c
    .array-data 4
        0x4
        0x7
        0x8
        0x5
        0x1e
        0x10
        0xe
        0x12
        0x21
        0x11
        0x27
        0x2b
    .end array-data

    .line 1106
    :array_1d
    .array-data 4
        0x4
        0x9
        0x7
        0x1f
        0xb
        0x8
        0xc
        0x11
        0x10
        0xe
        0xf
        0x5
        0x1d
        0x1e
        0x12
        0x20
        0x21
        0x22
        0x1c
        0x23
        0x27
        0x2b
        0x24
        0x1a
        0x30
        0x31
        0x2e
    .end array-data

    .line 1108
    :array_1e
    .array-data 4
        0x4
        0x7
        0x1f
        0xb
        0x8
        0xc
        0x11
        0x10
        0xe
        0xf
        0x5
        0x1d
        0x1e
        0x12
        0x20
        0x21
        0x22
        0x27
        0x2b
        0x24
        0x1a
        0x30
        0x31
        0x2e
    .end array-data

    .line 1117
    :array_1f
    .array-data 4
        0x4
        0x7
        0x1f
        0x8
        0x5
        0x1e
        0x10
        0xe
        0x12
        0x20
        0x21
        0x22
        0x11
        0x27
        0x2b
        0x1a
        0x30
        0x31
        0x2e
    .end array-data

    .line 1120
    :array_20
    .array-data 4
        0x4
        0x7
        0x1f
        0x8
        0x5
        0x1e
        0x10
        0xe
        0x12
        0x20
        0x21
        0x22
        0x11
        0x27
        0x2b
        0x24
        0x1a
        0x30
        0x31
        0x2e
    .end array-data

    .line 1141
    :array_21
    .array-data 4
        0x4
        0x9
        0x7
        0xb
        0x8
        0xc
        0x11
        0x10
        0xe
        0x5
        0x1d
        0x1e
        0x12
        0x20
        0x21
        0x1c
        0x23
        0x27
        0x2b
    .end array-data

    .line 1143
    :array_22
    .array-data 4
        0x4
        0x7
        0xb
        0x8
        0xc
        0x11
        0x10
        0xe
        0x5
        0x1d
        0x1e
        0x12
        0x20
        0x21
        0x27
        0x2b
    .end array-data

    .line 1145
    :array_23
    .array-data 4
        0x4
        0x7
        0x8
        0x5
        0x10
        0xe
        0x1e
        0x12
        0x21
        0x11
        0x27
        0x2b
    .end array-data

    .line 1166
    :array_24
    .array-data 4
        0x4
        0x9
        0x7
        0xb
        0x8
        0xc
        0x11
        0x10
        0xe
        0x5
        0x1d
        0x1e
        0x12
        0x20
        0x21
        0x1c
        0x23
        0x27
        0x2b
    .end array-data

    .line 1168
    :array_25
    .array-data 4
        0x4
        0x7
        0xb
        0x8
        0xc
        0x11
        0x10
        0xe
        0x5
        0x1d
        0x1e
        0x12
        0x20
        0x21
        0x27
        0x2b
    .end array-data

    .line 1170
    :array_26
    .array-data 4
        0x4
        0x7
        0x8
        0x5
        0x11
        0x10
        0xe
        0x1e
        0x12
        0x20
        0x21
        0x27
        0x2b
    .end array-data

    .line 1180
    :array_27
    .array-data 4
        0x4
        0x9
        0xa
        0x7
        0xb
        0x8
        0xc
        0x10
        0xe
        0x1b
        0x1c
        0x5
        0x1d
        0x1e
        0x12
        0xf
        0x20
        0x21
        0x22
        0x23
        0x27
        0x2b
    .end array-data

    .line 1182
    :array_28
    .array-data 4
        0x4
        0xa
        0x7
        0xb
        0x8
        0xc
        0x10
        0xe
        0x1b
        0x5
        0x1d
        0x1e
        0x12
        0xf
        0x20
        0x21
        0x22
        0x27
        0x2b
    .end array-data

    .line 1184
    :array_29
    .array-data 4
        0x4
        0xa
        0x7
        0x8
        0x10
        0xe
        0x5
        0x1e
        0x12
        0x20
        0x21
        0x22
        0x27
        0x2b
    .end array-data

    .line 1199
    :array_2a
    .array-data 4
        0x4
        0x9
        0xa
        0x7
        0xb
        0xe
        0xf
        0xc
        0x11
        0x1c
        0x10
        0x5
        0xd
        0x1e
        0x20
        0x21
        0x8
        0x23
        0x27
        0x2b
    .end array-data

    .line 1201
    :array_2b
    .array-data 4
        0x4
        0xa
        0x7
        0xb
        0xe
        0xf
        0xc
        0x11
        0x10
        0x5
        0xd
        0x1e
        0x20
        0x21
        0x8
        0x27
        0x2b
    .end array-data

    .line 1203
    :array_2c
    .array-data 4
        0x4
        0xa
        0x7
        0xb
        0xe
        0x11
        0x10
        0x5
        0x1e
        0x20
        0x21
        0x8
        0x27
        0x2b
    .end array-data

    .line 1224
    :array_2d
    .array-data 4
        0x4
        0x9
        0x7
        0xb
        0xe
        0xf
        0xc
        0x1c
        0x10
        0x5
        0xd
        0x20
        0x8
        0x23
        0x27
        0x2b
        0x30
        0x34
        0x2e
    .end array-data

    .line 1226
    :array_2e
    .array-data 4
        0x4
        0x7
        0xb
        0xe
        0xf
        0xc
        0x10
        0x5
        0xd
        0x20
        0x8
        0x27
        0x2b
        0x30
        0x34
        0x2e
    .end array-data

    .line 1228
    :array_2f
    .array-data 4
        0x4
        0x7
        0xb
        0xe
        0x10
        0x5
        0x20
        0x8
        0x27
        0x2b
        0x30
        0x34
    .end array-data

    .line 1231
    :array_30
    .array-data 4
        0x4
        0x9
        0x7
        0xb
        0xe
        0xf
        0xc
        0x1c
        0x10
        0x5
        0xd
        0x1e
        0x20
        0x21
        0x8
        0x23
        0x27
        0x2b
        0x30
        0x34
        0x2e
    .end array-data

    .line 1233
    :array_31
    .array-data 4
        0x4
        0x7
        0xb
        0xe
        0xf
        0xc
        0x10
        0x5
        0xd
        0x1e
        0x20
        0x21
        0x8
        0x27
        0x2b
        0x30
        0x34
        0x2e
    .end array-data

    .line 1235
    :array_32
    .array-data 4
        0x4
        0x7
        0xb
        0xe
        0x10
        0x5
        0x1e
        0x20
        0x21
        0x8
        0x27
        0x2b
        0x30
        0x34
    .end array-data

    .line 1250
    :array_33
    .array-data 4
        0x4
        0x9
        0x7
        0xb
        0xe
        0xc
        0x11
        0x10
        0x1e
        0x20
        0x21
        0x1c
        0x23
        0x27
        0x2b
    .end array-data

    .line 1252
    :array_34
    .array-data 4
        0x4
        0x7
        0xb
        0xe
        0xc
        0x11
        0x10
        0x1e
        0x20
        0x21
        0x27
        0x2b
    .end array-data

    .line 1254
    :array_35
    .array-data 4
        0x4
        0x7
        0xb
        0xe
        0x11
        0x10
        0x1e
        0x20
        0x21
        0x27
        0x2b
    .end array-data

    .line 1262
    :array_36
    .array-data 4
        0x4
        0x9
        0x7
        0xc
        0x10
        0x1e
        0x20
        0x21
        0x1c
        0x23
        0x27
        0x2b
    .end array-data

    .line 1263
    :array_37
    .array-data 4
        0x4
        0x7
        0xc
        0x10
        0x1e
        0x20
        0x21
        0x27
        0x2b
    .end array-data

    .line 1264
    :array_38
    .array-data 4
        0x4
        0x7
        0x10
        0x1e
        0x21
        0x27
        0x2b
    .end array-data

    .line 1278
    :array_39
    .array-data 4
        0x4
        0x9
        0x7
        0xb
        0xc
        0x10
        0x1e
        0x20
        0x1c
        0x23
        0x27
        0x2b
    .end array-data

    .line 1280
    :array_3a
    .array-data 4
        0x4
        0x7
        0xb
        0xc
        0x10
        0x1e
        0x20
        0x27
        0x2b
    .end array-data

    .line 1281
    :array_3b
    .array-data 4
        0x4
        0x7
        0xb
        0x10
        0x1e
        0x27
        0x2b
    .end array-data

    .line 1283
    :array_3c
    .array-data 4
        0x4
        0x9
        0x7
        0xb
        0xc
        0x10
        0x1e
        0x20
        0x21
        0x1c
        0x23
        0x27
        0x2b
    .end array-data

    .line 1285
    :array_3d
    .array-data 4
        0x4
        0x7
        0xb
        0xc
        0x10
        0x1e
        0x20
        0x21
        0x27
        0x2b
    .end array-data

    .line 1286
    :array_3e
    .array-data 4
        0x4
        0x7
        0xb
        0x10
        0x1e
        0x21
        0x27
        0x2b
    .end array-data

    .line 1319
    :array_3f
    .array-data 4
        0x4
        0x7
        0xb
        0xc
        0x10
        0x20
        0x1c
        0x23
        0x2b
    .end array-data

    .line 1321
    :array_40
    .array-data 4
        0x4
        0x7
        0xb
        0xc
        0x10
        0x20
        0x2b
    .end array-data

    .line 1322
    :array_41
    .array-data 4
        0x4
        0x7
        0xb
        0x10
        0x2b
    .end array-data

    .line 1368
    :array_42
    .array-data 4
        0x4
        0x9
        0x7
        0xb
        0xe
        0xc
        0x10
        0x5
        0x1e
        0x20
        0x21
        0x1c
        0x23
        0x27
        0x2b
    .end array-data

    .line 1370
    :array_43
    .array-data 4
        0x4
        0x7
        0xb
        0xe
        0xc
        0x10
        0x5
        0x1e
        0x20
        0x21
        0x27
        0x2b
    .end array-data

    .line 1372
    :array_44
    .array-data 4
        0x4
        0x7
        0xb
        0xe
        0x10
        0x5
        0x1e
        0x20
        0x21
        0x27
        0x2b
    .end array-data

    .line 1380
    :array_45
    .array-data 4
        0x4
        0x9
        0x7
        0xb
        0xe
        0xc
        0x10
        0x5
        0x8
        0xd
        0x1e
        0x20
        0x21
        0x23
        0x27
        0x2b
    .end array-data

    .line 1382
    :array_46
    .array-data 4
        0x4
        0x7
        0xb
        0xe
        0xc
        0x10
        0x5
        0x8
        0xd
        0x1e
        0x20
        0x21
        0x27
        0x2b
    .end array-data

    .line 1384
    :array_47
    .array-data 4
        0x4
        0x7
        0xb
        0xe
        0x10
        0x5
        0x1e
        0x8
        0xd
        0x20
        0x21
        0x27
        0x2b
    .end array-data

    .line 1419
    :array_48
    .array-data 4
        0x4
        0x9
        0x20
        0x7
        0xb
        0xc
        0x10
        0x5
        0x1e
        0x21
        0x1c
        0x8
        0x23
        0x27
        0x2b
        0x30
        0x31
        0x2e
    .end array-data

    .line 1421
    :array_49
    .array-data 4
        0x4
        0x20
        0x7
        0xb
        0xc
        0x10
        0x5
        0x1e
        0x21
        0x8
        0x27
        0x2b
        0x30
        0x31
        0x2e
    .end array-data

    .line 1423
    :array_4a
    .array-data 4
        0x4
        0x20
        0x7
        0xb
        0x10
        0x5
        0x1e
        0x21
        0x8
        0x27
        0x2b
        0x30
        0x31
        0x2e
    .end array-data

    .line 1425
    :array_4b
    .array-data 4
        0x4
        0x9
        0x7
        0xb
        0xc
        0x10
        0x5
        0x1e
        0x21
        0x1c
        0x8
        0x23
        0x27
        0x2b
    .end array-data

    .line 1427
    :array_4c
    .array-data 4
        0x4
        0x7
        0xb
        0xc
        0x10
        0x5
        0x1e
        0x21
        0x8
        0x27
        0x2b
    .end array-data

    .line 1428
    :array_4d
    .array-data 4
        0x4
        0x7
        0xb
        0x10
        0x5
        0x1e
        0x21
        0x8
        0x27
        0x2b
    .end array-data

    .line 1460
    :array_4e
    .array-data 4
        0x4
        0x9
        0x7
        0xb
        0xe
        0x10
        0x8
        0xd
        0xc
        0x5
        0x1e
        0x20
        0x21
        0x1c
        0x23
        0x27
        0x2b
    .end array-data

    .line 1462
    :array_4f
    .array-data 4
        0x4
        0x7
        0xb
        0xe
        0x10
        0x8
        0xd
        0xc
        0x5
        0x1e
        0x20
        0x21
        0x27
        0x2b
    .end array-data

    .line 1464
    :array_50
    .array-data 4
        0x4
        0xe
        0xb
        0x10
        0x8
        0xd
        0x1e
        0x20
        0x21
        0x27
        0x2b
    .end array-data

    .line 1473
    :array_51
    .array-data 4
        0x4
        0x9
        0x7
        0xb
        0xe
        0x10
        0x8
        0xd
        0xc
        0x5
        0x1e
        0x21
        0x1c
        0x23
        0x27
        0x2b
    .end array-data

    .line 1475
    :array_52
    .array-data 4
        0x4
        0x7
        0xb
        0xe
        0x10
        0x8
        0xd
        0xc
        0x5
        0x1e
        0x21
        0x27
        0x2b
    .end array-data

    .line 1477
    :array_53
    .array-data 4
        0x4
        0xe
        0xb
        0x10
        0x8
        0xd
        0x1e
        0x5
        0x21
        0x27
        0x2b
    .end array-data

    .line 1501
    :array_54
    .array-data 4
        0x4
        0x9
        0x7
        0xb
        0xe
        0x10
        0x8
        0xd
        0x5
        0xc
        0x1e
        0x20
        0x21
        0x1c
        0x23
        0x27
        0x2b
    .end array-data

    .line 1503
    :array_55
    .array-data 4
        0x4
        0x7
        0xb
        0xe
        0x10
        0x8
        0xd
        0x5
        0xc
        0x1e
        0x20
        0x21
        0x27
        0x2b
    .end array-data

    .line 1506
    :array_56
    .array-data 4
        0x4
        0xe
        0x10
        0x8
        0xd
        0x1e
        0x5
        0x20
        0x21
        0x27
        0x2b
    .end array-data

    .line 1515
    :array_57
    .array-data 4
        0x4
        0x9
        0xa
        0x7
        0xb
        0x8
        0xc
        0x11
        0x10
        0xe
        0x5
        0x1d
        0x1e
        0x12
        0xf
        0x20
        0x21
        0x1c
        0x23
        0x27
        0x2b
    .end array-data

    .line 1517
    :array_58
    .array-data 4
        0x4
        0xa
        0x7
        0xb
        0x8
        0xc
        0x11
        0x10
        0xe
        0x5
        0x1d
        0x1e
        0x12
        0xf
        0x20
        0x21
        0x27
        0x2b
    .end array-data

    .line 1519
    :array_59
    .array-data 4
        0x4
        0xa
        0x7
        0x8
        0x5
        0x11
        0x10
        0xe
        0x1e
        0x12
        0x20
        0x21
        0x27
        0x2b
    .end array-data

    .line 1539
    :array_5a
    .array-data 4
        0x4
        0x9
        0x19
        0x1f
        0x11
        0x1e
        0x21
        0x12
        0x1c
        0x23
        0x27
        0x2b
    .end array-data

    .line 1540
    :array_5b
    .array-data 4
        0x4
        0x19
        0x1f
        0x11
        0x1e
        0x21
        0x12
        0xc
        0x27
        0x2b
    .end array-data

    .line 1543
    :array_5c
    .array-data 4
        0x4
        0x1f
        0x11
        0x1e
        0x21
        0x12
        0x27
        0x2b
    .end array-data

    .line 1586
    :array_5d
    .array-data 4
        0x4
        0x9
        0xa
        0x7
        0xb
        0xe
        0xf
        0xc
        0x11
        0x1c
        0x12
        0x5
        0x8
        0xd
        0x1e
        0x20
        0x21
        0x14
        0x1f
        0x24
        0x23
        0x1
        0x27
        0x2b
    .end array-data

    .line 1588
    :array_5e
    .array-data 4
        0x4
        0xa
        0x7
        0xb
        0xe
        0xf
        0xc
        0x11
        0x12
        0x5
        0x8
        0xd
        0x1e
        0x20
        0x21
        0x14
        0x1f
        0x24
        0x27
        0x2b
    .end array-data

    .line 1590
    :array_5f
    .array-data 4
        0x4
        0xa
        0x7
        0xb
        0xe
        0x11
        0x12
        0x5
        0xd
        0x8
        0x1e
        0x20
        0x21
        0x14
        0x1f
        0x24
        0x27
        0x2b
    .end array-data

    .line 1604
    :array_60
    .array-data 4
        0x4
        0x9
        0xa
        0x7
        0xb
        0x8
        0xc
        0x11
        0x10
        0xe
        0xf
        0x5
        0x1d
        0x1e
        0x12
        0x20
        0x21
        0x1c
        0x23
        0x27
        0x2b
    .end array-data

    .line 1606
    :array_61
    .array-data 4
        0x4
        0xa
        0x7
        0xb
        0x8
        0xc
        0x11
        0x10
        0xe
        0xf
        0x5
        0x1d
        0x1e
        0x12
        0x20
        0x21
        0x27
        0x2b
    .end array-data

    .line 1608
    :array_62
    .array-data 4
        0x4
        0xa
        0x7
        0x8
        0x5
        0x11
        0x10
        0xe
        0x1e
        0x12
        0x20
        0x21
        0x27
        0x2b
    .end array-data

    .line 1634
    :array_63
    .array-data 4
        0x4
        0x9
        0x1e
        0xc
        0x5
        0x1b
        0x17
        0x1a
        0x20
        0x12
        0x21
        0x25
        0x1c
        0x27
        0x2b
        0x2e
        0x31
    .end array-data

    .line 1636
    :array_64
    .array-data 4
        0x4
        0x1e
        0x5
        0x1b
        0x1a
        0x20
        0x12
        0x21
        0x25
        0x27
        0x2b
        0x31
    .end array-data

    .line 1638
    :array_65
    .array-data 4
        0x4
        0x9
        0x1e
        0x5
        0x1a
        0x20
        0x12
        0x21
        0x25
        0x27
        0x2b
        0x31
    .end array-data

    .line 1670
    :array_66
    .array-data 4
        0x4
        0x9
        0x20
        0x7
        0xb
        0xc
        0x10
        0x5
        0x1e
        0x21
        0x1c
        0x8
        0x23
        0x27
    .end array-data

    .line 1672
    :array_67
    .array-data 4
        0x4
        0x20
        0x7
        0xb
        0xc
        0x10
        0x5
        0x1e
        0x21
        0x8
        0x27
    .end array-data

    .line 1674
    :array_68
    .array-data 4
        0x4
        0x20
        0x7
        0xb
        0x10
        0x5
        0x1e
        0x21
        0x8
        0x27
    .end array-data

    .line 1676
    :array_69
    .array-data 4
        0xa
        0x9
        0x20
        0x7
        0xb
        0xc
        0x10
        0x5
        0x1e
        0x21
        0x1c
        0x8
        0x23
        0x27
    .end array-data

    .line 1678
    :array_6a
    .array-data 4
        0xa
        0x20
        0x7
        0xb
        0xc
        0x10
        0x5
        0x1e
        0x21
        0x8
        0x27
    .end array-data

    .line 1679
    :array_6b
    .array-data 4
        0xa
        0x20
        0x7
        0xb
        0x10
        0x5
        0x1e
        0x21
        0x8
        0x27
    .end array-data

    .line 1693
    :array_6c
    .array-data 4
        0x4
        0x1
        0x1e
        0xc
        0x21
        0xf
        0x1c
        0x23
        0x27
        0x24
        0xa
        0x11
        0x9
        0x20
        0x28
        0x1f
        0x2b
        0xb
        0x8
        0xd
        0xe
    .end array-data

    .line 1695
    :array_6d
    .array-data 4
        0x4
        0x1e
        0xc
        0x21
        0xf
        0x27
        0x24
        0xa
        0x11
        0x20
        0x28
        0x1f
        0x2b
        0xb
        0x8
        0xd
        0xe
    .end array-data

    .line 1696
    :array_6e
    .array-data 4
        0x4
        0x1e
        0xc
        0x21
        0x27
        0x24
        0xa
        0x11
        0x20
        0x1f
        0x2b
        0xb
        0x8
        0xd
        0xe
    .end array-data

    .line 1711
    :array_6f
    .array-data 4
        0x4
        0x9
        0x7
        0xb
        0xc
        0x10
        0x1e
        0x20
        0x21
        0x1c
        0x23
        0x27
        0x2b
        0x11
        0xa
        0x24
        0x1f
    .end array-data

    .line 1713
    :array_70
    .array-data 4
        0x4
        0x7
        0xb
        0xc
        0x10
        0x1e
        0x20
        0x21
        0x27
        0x2b
        0x11
        0xa
        0x24
        0x1f
    .end array-data

    .line 1716
    :array_71
    .array-data 4
        0x4
        0x7
        0xb
        0x10
        0x1e
        0x21
        0x27
        0x2b
        0x11
        0xa
        0x1f
    .end array-data

    .line 1719
    :array_72
    .array-data 4
        0x4
        0x7
        0xb
        0x10
        0x1e
        0x21
        0x27
        0x2b
        0x24
        0x11
        0xa
        0x1f
    .end array-data

    .line 1766
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1d
        :pswitch_1e
        :pswitch_25
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_26
        :pswitch_27
        :pswitch_28
        :pswitch_29
    .end packed-switch

    .line 1768
    :array_73
    .array-data 4
        0x3
        0xe
        0x10
        0x8
        0xd
        0xc
        0x5
        0x1e
    .end array-data

    .line 1769
    :array_74
    .array-data 4
        0x3
        0xe
        0x10
        0x8
        0xd
        0xc
        0x5
        0x1e
    .end array-data

    .line 1770
    :array_75
    .array-data 4
        0xe
        0x10
        0x8
        0xd
        0xc
        0x1e
    .end array-data

    .line 1780
    :array_76
    .array-data 4
        0x3
        0xb
        0xe
        0x10
        0x8
        0xd
        0xc
        0x5
        0x1e
    .end array-data

    .line 1781
    :array_77
    .array-data 4
        0x3
        0xb
        0xe
        0x10
        0x8
        0xd
        0xc
        0x5
        0x1e
    .end array-data

    .line 1782
    :array_78
    .array-data 4
        0xe
        0x10
        0x8
        0xc
        0x1e
    .end array-data

    .line 1787
    :array_79
    .array-data 4
        0x3
        0xb
        0xe
        0x10
        0x8
        0xd
        0xc
        0x5
        0x1e
    .end array-data

    .line 1788
    :array_7a
    .array-data 4
        0x3
        0xb
        0xe
        0x10
        0x8
        0xd
        0xc
        0x5
        0x1e
    .end array-data

    .line 1789
    :array_7b
    .array-data 4
        0xe
        0x10
        0x8
        0xd
        0xc
        0x1e
    .end array-data

    .line 1794
    :array_7c
    .array-data 4
        0x3
        0xe
        0x10
        0x8
        0xd
        0xc
        0x5
        0x1e
    .end array-data

    .line 1795
    :array_7d
    .array-data 4
        0x3
        0xe
        0x10
        0x8
        0xd
        0xc
        0x5
        0x1e
    .end array-data

    .line 1796
    :array_7e
    .array-data 4
        0xe
        0x10
        0x8
        0xd
        0xc
        0x1e
    .end array-data

    .line 1802
    :array_7f
    .array-data 4
        0x3
        0xb
        0xe
        0x10
        0x8
        0xd
        0xc
        0x5
        0x1e
    .end array-data

    .line 1803
    :array_80
    .array-data 4
        0x3
        0xb
        0xe
        0x10
        0x8
        0xd
        0xc
        0x5
        0x1e
    .end array-data

    .line 1804
    :array_81
    .array-data 4
        0x10
        0x8
        0xd
        0xc
        0x1e
    .end array-data

    .line 1811
    :array_82
    .array-data 4
        0x3
        0xb
        0xe
        0x10
        0x8
        0xd
        0xc
        0x5
        0x1e
    .end array-data

    .line 1812
    :array_83
    .array-data 4
        0x3
        0xb
        0xe
        0x10
        0x8
        0xd
        0xc
        0x5
        0x1e
    .end array-data

    .line 1813
    :array_84
    .array-data 4
        0xe
        0x10
        0x8
        0xd
        0xc
        0x1e
    .end array-data

    .line 1818
    :array_85
    .array-data 4
        0x3
        0xb
        0xe
        0x10
        0x8
        0xd
        0xc
        0x5
        0x1e
    .end array-data

    .line 1819
    :array_86
    .array-data 4
        0x3
        0xb
        0xe
        0x10
        0x8
        0xd
        0xc
        0x5
        0x1e
    .end array-data

    .line 1820
    :array_87
    .array-data 4
        0xe
        0x10
        0x8
        0xd
        0xc
        0x1e
    .end array-data

    .line 1825
    :array_88
    .array-data 4
        0x3
        0xb
        0xe
        0x10
        0x8
        0xd
        0xc
        0x5
        0x1e
    .end array-data

    .line 1826
    :array_89
    .array-data 4
        0x3
        0xb
        0xe
        0x10
        0x8
        0xd
        0xc
        0x5
        0x1e
    .end array-data

    .line 1827
    :array_8a
    .array-data 4
        0xe
        0x10
        0x8
        0xd
        0xc
        0x1e
    .end array-data

    .line 1831
    :array_8b
    .array-data 4
        0x3
        0xb
        0xe
        0x10
        0x8
        0xd
        0xc
        0x5
        0x1e
    .end array-data

    .line 1832
    :array_8c
    .array-data 4
        0x3
        0xb
        0xe
        0x10
        0x8
        0xd
        0xc
        0x5
        0x1e
    .end array-data

    .line 1833
    :array_8d
    .array-data 4
        0xe
        0x10
        0x8
        0xd
        0xc
        0x1e
    .end array-data

    .line 1838
    :array_8e
    .array-data 4
        0x3
        0xb
        0xe
        0x10
        0x8
        0xd
        0xc
        0x5
        0x1e
    .end array-data

    .line 1839
    :array_8f
    .array-data 4
        0x3
        0xb
        0xe
        0x10
        0x8
        0xd
        0xc
        0x5
        0x1e
    .end array-data

    .line 1840
    :array_90
    .array-data 4
        0xe
        0x10
        0x8
        0xd
        0xc
        0x1e
    .end array-data

    .line 1844
    :array_91
    .array-data 4
        0x3
        0xe
        0x10
        0x8
        0xd
        0xc
        0x5
        0x1e
    .end array-data

    .line 1845
    :array_92
    .array-data 4
        0x3
        0xe
        0x10
        0x8
        0xd
        0xc
        0x5
        0x1e
    .end array-data

    .line 1846
    :array_93
    .array-data 4
        0xe
        0x10
        0x8
        0xd
        0xc
        0x1e
    .end array-data

    .line 1854
    :array_94
    .array-data 4
        0x3
        0xb
        0xe
        0x10
        0x8
        0xd
        0xc
        0x5
        0x1e
    .end array-data

    .line 1855
    :array_95
    .array-data 4
        0x3
        0xb
        0xe
        0x10
        0x8
        0xd
        0xc
        0x5
        0x1e
    .end array-data

    .line 1856
    :array_96
    .array-data 4
        0xe
        0x10
        0x8
        0xd
        0xc
        0x1e
    .end array-data

    .line 1861
    :array_97
    .array-data 4
        0x3
        0xe
        0x10
        0x8
        0xd
        0xc
        0x5
        0x1e
    .end array-data

    .line 1862
    :array_98
    .array-data 4
        0x3
        0xe
        0x10
        0x8
        0xd
        0xc
        0x5
        0x1e
    .end array-data

    .line 1863
    :array_99
    .array-data 4
        0xe
        0x10
        0x8
        0xc
        0x1e
    .end array-data

    .line 1897
    :array_9a
    .array-data 4
        0x8
        0x9
        0xe
        0xd
        0x1b
    .end array-data

    .line 1898
    :array_9b
    .array-data 4
        0x8
        0x9
        0xe
        0xd
        0x1b
    .end array-data

    .line 1899
    :array_9c
    .array-data 4
        0x8
        0x9
        0xe
        0x1b
    .end array-data

    .line 1972
    :array_9d
    .array-data 4
        0x18
        0x25
    .end array-data

    .line 1973
    :array_9e
    .array-data 4
        0x18
        0x25
    .end array-data

    .line 1978
    :array_9f
    .array-data 4
        0x25
        0x18
    .end array-data

    .line 1979
    :array_a0
    .array-data 4
        0x25
        0x18
    .end array-data

    .line 1985
    :array_a1
    .array-data 4
        0x18
        0x25
    .end array-data

    .line 1986
    :array_a2
    .array-data 4
        0x18
        0x25
    .end array-data

    .line 2002
    :array_a3
    .array-data 4
        0x3
        0x0
        0x2
        0x1e
        0x18
        0x17
        0x1b
        0x15
        0x12
        0x20
        0xf
        0x2b
        0x27
        0x30
    .end array-data

    .line 2004
    :array_a4
    .array-data 4
        0x3
        0x0
        0x2
        0x1e
        0x18
        0x17
        0x1b
        0x15
        0x12
        0x20
        0xf
        0x2b
        0x27
    .end array-data

    .line 2006
    :array_a5
    .array-data 4
        0x1e
        0x17
        0x15
        0x12
        0x2b
    .end array-data

    .line 2011
    :array_a6
    .array-data 4
        0x2
        0x1e
        0x18
        0x17
        0x1b
        0x15
        0x12
        0x2b
        0x27
        0x30
    .end array-data

    .line 2012
    :array_a7
    .array-data 4
        0x2
        0x1e
        0x18
        0x17
        0x1b
        0x15
        0x12
        0x2b
        0x27
    .end array-data

    .line 2013
    :array_a8
    .array-data 4
        0x1e
        0x17
        0x15
        0x12
        0x2b
    .end array-data

    .line 2015
    :array_a9
    .array-data 4
        0x3
        0x29
        0x0
    .end array-data

    .line 2021
    :array_aa
    .array-data 4
        0x3
        0x1
        0x1e
        0xf
        0x5
        0x18
        0x17
        0x1b
        0x15
        0x12
        0x20
        0x21
        0x34
        0x9
        0x24
        0x28
        0x2b
        0x30
        0x31
        0x27
    .end array-data

    .line 2023
    :array_ab
    .array-data 4
        0x3
        0x1
        0x1e
        0xf
        0x5
        0x18
        0x17
        0x1b
        0x15
        0x12
        0x20
        0x21
        0x34
        0x24
        0x28
        0x2b
        0x30
        0x31
        0x27
    .end array-data

    .line 2025
    :array_ac
    .array-data 4
        0x5
        0x20
        0x21
        0x34
        0x24
        0x2b
        0x30
        0x31
    .end array-data

    .line 2028
    :array_ad
    .array-data 4
        0x3
        0x1
        0x1e
        0xf
        0x5
        0x18
        0x17
        0x1b
        0x15
        0x12
        0x20
        0x21
        0x34
        0x9
        0x24
        0x28
        0x2b
        0x30
        0x31
        0x27
    .end array-data

    .line 2030
    :array_ae
    .array-data 4
        0x3
        0x1
        0x1e
        0xf
        0x5
        0x18
        0x17
        0x1b
        0x15
        0x12
        0x20
        0x21
        0x34
        0x24
        0x28
        0x2b
        0x30
        0x31
        0x27
    .end array-data

    .line 2032
    :array_af
    .array-data 4
        0x5
        0x20
        0x21
        0x34
        0x24
        0x2b
        0x30
        0x31
    .end array-data

    .line 2035
    :array_b0
    .array-data 4
        0x3
        0x1
        0x1e
        0xf
        0x5
        0x17
        0x15
        0x12
        0x20
        0x21
        0x34
        0x24
        0x28
        0x2b
        0x30
        0x31
        0x27
    .end array-data

    .line 2037
    :array_b1
    .array-data 4
        0x3
        0x1
        0x1e
        0xf
        0x5
        0x17
        0x15
        0x12
        0x20
        0x21
        0x34
        0x24
        0x28
        0x2b
        0x30
        0x31
        0x27
    .end array-data

    .line 2039
    :array_b2
    .array-data 4
        0x1e
        0x5
        0x17
        0x15
        0x12
        0x20
        0x21
        0x34
        0x24
        0x2b
        0x30
        0x31
    .end array-data

    .line 2045
    :array_b3
    .array-data 4
        0x3
        0x1
        0x1e
        0xf
        0x5
        0x18
        0x17
        0x1b
        0x15
        0x12
        0x20
        0x21
        0x34
        0x9
        0x24
        0x28
        0x2b
        0x30
        0x31
        0x27
    .end array-data

    .line 2051
    :array_b4
    .array-data 4
        0x3
        0x1
        0x1e
        0x5
        0x18
        0x17
        0x1b
        0x15
        0x12
        0x20
        0x21
        0x34
        0x24
        0x28
        0x2b
        0x30
        0x31
        0x27
    .end array-data

    .line 2053
    :array_b5
    .array-data 4
        0x1e
        0x5
        0x17
        0x15
        0x12
        0x20
        0x21
        0x34
        0x24
        0x2b
        0x30
        0x31
    .end array-data

    .line 2065
    :array_b6
    .array-data 4
        0x3
        0x1
        0x1e
        0xf
        0x5
        0x17
        0x15
        0x12
        0x20
        0x21
        0x34
        0x9
        0x24
        0x28
        0x2b
        0x30
        0x31
    .end array-data

    .line 2071
    :array_b7
    .array-data 4
        0x3
        0x1
        0x1e
        0xf
        0x5
        0x17
        0x15
        0x12
        0x20
        0x21
        0x34
        0x24
        0x28
        0x2b
        0x30
        0x31
    .end array-data

    .line 2073
    :array_b8
    .array-data 4
        0x1e
        0x5
        0x17
        0x15
        0x12
        0x1f
        0x20
        0x21
        0x34
        0x24
        0x2b
        0x30
        0x31
    .end array-data

    .line 2125
    :array_b9
    .array-data 4
        0xb
        0xc
        0x7
        0x1b
        0x8
        0xd
        0x25
        0x2b
        0x4
    .end array-data

    .line 2126
    :array_ba
    .array-data 4
        0x25
        0x2b
        0x4
    .end array-data

    .line 2127
    :array_bb
    .array-data 4
        0xb
        0x7
        0x8
        0xd
        0x25
        0x2b
        0x4
    .end array-data

    .line 2147
    :array_bc
    .array-data 4
        0x7
        0x13
    .end array-data

    .line 2148
    :array_bd
    .array-data 4
        0x7
        0x13
    .end array-data

    .line 2160
    :array_be
    .array-data 4
        0x5
        0x8
    .end array-data

    .line 2161
    :array_bf
    .array-data 4
        0x5
        0x8
    .end array-data

    .line 2162
    :array_c0
    .array-data 4
        0x5
        0x8
    .end array-data

    .line 2172
    :array_c1
    .array-data 4
        0x12
        0x1e
        0x1f
        0x3
        0x2b
        0x30
    .end array-data

    .line 2173
    :array_c2
    .array-data 4
        0x12
        0x1e
        0x1f
        0x30
    .end array-data

    .line 2174
    :array_c3
    .array-data 4
        0x12
        0x1e
        0x1f
        0x30
    .end array-data

    .line 2179
    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_2a
        :pswitch_2b
        :pswitch_2c
        :pswitch_2d
    .end packed-switch

    .line 2181
    :array_c4
    .array-data 4
        0x3
        0x2
        0x1
        0x0
        0x29
    .end array-data

    .line 2182
    :array_c5
    .array-data 4
        0x3
        0x2
        0x1
        0x0
    .end array-data

    .line 2185
    :array_c6
    .array-data 4
        0x3
        0x2
        0x0
        0x29
    .end array-data

    .line 2186
    :array_c7
    .array-data 4
        0x3
        0x2
        0x0
    .end array-data

    .line 2189
    :array_c8
    .array-data 4
        0x3
        0x2
        0x0
        0x29
        0x21
    .end array-data

    .line 2190
    :array_c9
    .array-data 4
        0x3
        0x2
        0x0
        0x21
    .end array-data

    .line 2194
    :array_ca
    .array-data 4
        0x3
        0x2
        0x1
        0x0
        0x29
    .end array-data

    .line 2195
    :array_cb
    .array-data 4
        0x3
        0x2
        0x1
        0x0
    .end array-data

    .line 2202
    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_2f
        :pswitch_2e
        :pswitch_2e
        :pswitch_30
    .end packed-switch

    .line 2204
    :array_cc
    .array-data 4
        0x3
        0x1
        0x0
        0x29
    .end array-data

    .line 2205
    :array_cd
    .array-data 4
        0x3
        0x1
        0x0
    .end array-data

    .line 2208
    :array_ce
    .array-data 4
        0x3
        0x2
        0x1
        0x0
        0x29
    .end array-data

    .line 2209
    :array_cf
    .array-data 4
        0x3
        0x2
        0x1
        0x0
    .end array-data

    .line 2216
    :pswitch_data_4
    .packed-switch 0x1
        :pswitch_31
        :pswitch_32
        :pswitch_33
        :pswitch_34
    .end packed-switch

    .line 2218
    :array_d0
    .array-data 4
        0x2
        0x1
        0x0
        0x29
    .end array-data

    .line 2219
    :array_d1
    .array-data 4
        0x2
        0x1
        0x0
    .end array-data

    .line 2222
    :array_d2
    .array-data 4
        0x3
        0x2
        0x1
        0x0
        0x29
    .end array-data

    .line 2223
    :array_d3
    .array-data 4
        0x3
        0x2
        0x1
        0x0
    .end array-data

    .line 2226
    :array_d4
    .array-data 4
        0x3
        0x2
        0x1
        0x0
        0x29
    .end array-data

    .line 2227
    :array_d5
    .array-data 4
        0x3
        0x2
        0x1
        0x0
    .end array-data

    .line 2230
    :array_d6
    .array-data 4
        0x3
        0x2
        0x1
        0x0
        0x29
    .end array-data

    .line 2231
    :array_d7
    .array-data 4
        0x3
        0x2
        0x1
        0x0
    .end array-data

    .line 2251
    :array_d8
    .array-data 4
        0x19
        0x2
        0x1f
    .end array-data

    .line 2252
    :array_d9
    .array-data 4
        0x19
        0x2
        0x1f
    .end array-data

    .line 2254
    :array_da
    .array-data 4
        0x19
        0x2
        0x1f
        0x1
        0x3
        0x0
        0x29
    .end array-data

    .line 2255
    :array_db
    .array-data 4
        0x19
        0x2
        0x1f
        0x1
        0x3
        0x0
    .end array-data

    .line 2264
    :array_dc
    .array-data 4
        0xa
        0x7
        0x9
        0xe
        0x11
        0x10
        0x1e
        0x21
    .end array-data

    .line 2265
    :array_dd
    .array-data 4
        0xa
        0x7
        0x9
        0xe
        0x11
        0x10
        0x1e
        0x21
    .end array-data

    .line 2266
    :array_de
    .array-data 4
        0xa
        0x7
        0x9
        0xe
        0x11
        0x10
        0x1e
        0x21
    .end array-data

    .line 2340
    :array_df
    .array-data 4
        0x1e
        0x20
        0x22
        0x14
        0x4
        0x8
        0xd
        0x12
        0x24
        0x1f
    .end array-data

    .line 2341
    :array_e0
    .array-data 4
        0x1e
        0x20
        0x22
        0x14
        0x4
        0x8
        0xd
        0x12
        0x24
        0x1f
    .end array-data

    .line 2342
    :array_e1
    .array-data 4
        0x1e
        0x20
        0x22
        0x14
        0x4
        0x8
        0xd
        0x12
        0x24
        0x1f
    .end array-data

    .line 2345
    :array_e2
    .array-data 4
        0x21
        0x1e
        0x20
        0x22
        0x14
        0x4
        0x8
        0xd
        0x12
        0x24
        0x1f
    .end array-data

    .line 2346
    :array_e3
    .array-data 4
        0x21
        0x1e
        0x20
        0x22
        0x14
        0x4
        0x8
        0xd
        0x12
        0x24
        0x1f
    .end array-data

    .line 2347
    :array_e4
    .array-data 4
        0x21
        0x1e
        0x20
        0x22
        0x14
        0x4
        0x8
        0xd
        0x12
        0x24
        0x1f
    .end array-data

    .line 2357
    :array_e5
    .array-data 4
        0x13
        0x1d
    .end array-data

    .line 2358
    :array_e6
    .array-data 4
        0x13
        0x1d
    .end array-data
.end method

.method public declared-synchronized removeButton(Lcom/sec/android/glview/TwGLView;)V
    .locals 1
    .param p1, "button"    # Lcom/sec/android/glview/TwGLView;

    .prologue
    .line 688
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/camera/MenuDimController;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 689
    monitor-exit p0

    return-void

    .line 688
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public resetDim()V
    .locals 2

    .prologue
    .line 860
    iget-object v1, p0, Lcom/sec/android/app/camera/MenuDimController;->mDimArrayList:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    .line 861
    iget-object v1, p0, Lcom/sec/android/app/camera/MenuDimController;->mUserSettingValueList:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    .line 863
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v1, 0x35

    if-ge v0, v1, :cond_0

    .line 864
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/camera/MenuDimController;->setDim(IZ)V

    .line 863
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 866
    :cond_0
    return-void
.end method

.method public restoreUserSettingValues()V
    .locals 3

    .prologue
    .line 883
    iget-object v1, p0, Lcom/sec/android/app/camera/MenuDimController;->mDimArrayList:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 884
    .local v0, "itor":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/app/camera/MenuDimController$DimArray;>;"
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 885
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/camera/MenuDimController$DimArray;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/app/camera/MenuDimController$DimArray;->restoreUserSettingValues(Z)V

    goto :goto_0

    .line 887
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/camera/MenuDimController;->mDimArrayList:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    .line 888
    return-void
.end method

.method declared-synchronized setDim(IZ)V
    .locals 1
    .param p1, "key"    # I
    .param p2, "dimmed"    # Z

    .prologue
    .line 869
    monitor-enter p0

    const/4 v0, -0x1

    if-le p1, v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/camera/MenuDimController;->mCurrentDimArray:[Z

    array-length v0, v0

    if-ge p1, v0, :cond_0

    .line 870
    iget-object v0, p0, Lcom/sec/android/app/camera/MenuDimController;->mCurrentDimArray:[Z

    aput-boolean p2, v0, p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 872
    :cond_0
    monitor-exit p0

    return-void

    .line 869
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setDimArray([Z)V
    .locals 3
    .param p1, "dimArray"    # [Z

    .prologue
    const/4 v2, 0x0

    .line 680
    iget-object v0, p0, Lcom/sec/android/app/camera/MenuDimController;->mCurrentDimArray:[Z

    const/16 v1, 0x35

    invoke-static {p1, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy([ZI[ZII)V

    .line 681
    return-void
.end method

.method public varargs setSettingDefault([I)V
    .locals 8
    .param p1, "indices"    # [I

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 2445
    iget-object v5, p0, Lcom/sec/android/app/camera/MenuDimController;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v5}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v1

    .line 2447
    .local v1, "cs":Lcom/sec/android/app/camera/CameraSettings;
    move-object v0, p1

    .local v0, "arr$":[I
    array-length v4, v0

    .local v4, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v4, :cond_3

    aget v3, v0, v2

    .line 2448
    .local v3, "index":I
    packed-switch v3, :pswitch_data_0

    .line 2447
    :goto_1
    :pswitch_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2450
    :pswitch_1
    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/CameraSettings;->setSceneMode(I)V

    goto :goto_1

    .line 2454
    :pswitch_2
    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/CameraSettings;->setFlashMode(I)V

    goto :goto_1

    .line 2458
    :pswitch_3
    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraISO(I)V

    goto :goto_1

    .line 2464
    :pswitch_4
    invoke-virtual {v1}, Lcom/sec/android/app/camera/CameraSettings;->getDefaultCameraFocusMode()I

    move-result v5

    invoke-virtual {v1, v5}, Lcom/sec/android/app/camera/CameraSettings;->setCameraFocusMode(I)V

    goto :goto_1

    .line 2467
    :pswitch_5
    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraExposureMeter(I)V

    goto :goto_1

    .line 2470
    :pswitch_6
    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraHDR(I)V

    goto :goto_1

    .line 2473
    :pswitch_7
    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/CameraSettings;->setExposureValue(I)V

    goto :goto_1

    .line 2476
    :pswitch_8
    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraEffect(I)V

    goto :goto_1

    .line 2479
    :pswitch_9
    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/CameraSettings;->setWhiteBalance(I)V

    goto :goto_1

    .line 2482
    :pswitch_a
    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/CameraSettings;->setTimer(I)V

    goto :goto_1

    .line 2485
    :pswitch_b
    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/CameraSettings;->setReview(I)V

    goto :goto_1

    .line 2488
    :pswitch_c
    invoke-virtual {v1}, Lcom/sec/android/app/camera/CameraSettings;->getAttachCamcorderMode()Z

    move-result v5

    if-ne v5, v7, :cond_2

    .line 2489
    invoke-virtual {v1}, Lcom/sec/android/app/camera/CameraSettings;->getAttachMMSMode()Z

    move-result v5

    if-ne v5, v7, :cond_0

    .line 2490
    invoke-virtual {v1, v7}, Lcom/sec/android/app/camera/CameraSettings;->setCamcorderRecordingMode(I)V

    goto :goto_1

    .line 2491
    :cond_0
    invoke-virtual {v1}, Lcom/sec/android/app/camera/CameraSettings;->getAttachEmailMode()Z

    move-result v5

    if-ne v5, v7, :cond_1

    .line 2492
    const/4 v5, 0x6

    invoke-virtual {v1, v5}, Lcom/sec/android/app/camera/CameraSettings;->setCamcorderRecordingMode(I)V

    goto :goto_1

    .line 2494
    :cond_1
    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCamcorderRecordingMode(I)V

    goto :goto_1

    .line 2497
    :cond_2
    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCamcorderRecordingMode(I)V

    goto :goto_1

    .line 2501
    :pswitch_d
    const-string v5, "1920x1080"

    invoke-static {v5}, Lcom/sec/android/app/camera/CameraResolution;->getResolutionID(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v1, v5}, Lcom/sec/android/app/camera/CameraSettings;->setCamcorderResolution(I)Z

    goto :goto_1

    .line 2504
    :pswitch_e
    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraAntiShake(I)V

    goto :goto_1

    .line 2507
    :pswitch_f
    invoke-virtual {v1}, Lcom/sec/android/app/camera/CameraSettings;->getDefaultCamcorderAntishake()I

    move-result v5

    invoke-virtual {v1, v5}, Lcom/sec/android/app/camera/CameraSettings;->setCamcorderAntishake(I)V

    goto :goto_1

    .line 2510
    :pswitch_10
    invoke-virtual {v1}, Lcom/sec/android/app/camera/CameraSettings;->getDefaultCamcorderAudioZoom()I

    move-result v5

    invoke-virtual {v1, v5}, Lcom/sec/android/app/camera/CameraSettings;->setCamcorderAudioZoom(I)V

    goto :goto_1

    .line 2513
    :pswitch_11
    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/CameraSettings;->setSelfFlip(I)V

    goto :goto_1

    .line 2516
    :pswitch_12
    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/CameraSettings;->setBurstMode(I)V

    goto :goto_1

    .line 2519
    :pswitch_13
    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraVoiceCommand(I)V

    goto :goto_1

    .line 2522
    :pswitch_14
    invoke-virtual {v1, v7}, Lcom/sec/android/app/camera/CameraSettings;->setSaveRichtone(I)V

    goto/16 :goto_1

    .line 2525
    :pswitch_15
    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/CameraSettings;->setFaceDetectionMode(I)V

    goto/16 :goto_1

    .line 2528
    :pswitch_16
    invoke-virtual {v1}, Lcom/sec/android/app/camera/CameraSettings;->getDefaultTouchToCapture()I

    move-result v5

    invoke-virtual {v1, v5}, Lcom/sec/android/app/camera/CameraSettings;->setTouchToCapture(I)V

    goto/16 :goto_1

    .line 2531
    :pswitch_17
    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/CameraSettings;->setAutoNightDetectionMode(I)V

    goto/16 :goto_1

    .line 2534
    :pswitch_18
    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/CameraSettings;->setShootingMode(I)V

    goto/16 :goto_1

    .line 2537
    :pswitch_19
    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/CameraSettings;->setGPS(I)V

    goto/16 :goto_1

    .line 2540
    :pswitch_1a
    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/CameraSettings;->setZoomValue(I)V

    goto/16 :goto_1

    .line 2543
    :pswitch_1b
    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/CameraSettings;->setGuideline(I)V

    goto/16 :goto_1

    .line 2546
    :pswitch_1c
    invoke-virtual {v1, v7}, Lcom/sec/android/app/camera/CameraSettings;->setCameraShutterSound(I)V

    goto/16 :goto_1

    .line 2549
    :pswitch_1d
    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/CameraSettings;->setCameraQuality(I)V

    goto/16 :goto_1

    .line 2552
    :pswitch_1e
    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/CameraSettings;->setStorage(I)V

    goto/16 :goto_1

    .line 2555
    :pswitch_1f
    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/CameraSettings;->setRemoteViewfinderMode(I)V

    goto/16 :goto_1

    .line 2558
    :pswitch_20
    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/CameraSettings;->setSideTouchMode(I)V

    goto/16 :goto_1

    .line 2561
    :pswitch_21
    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/CameraSettings;->setFocusSelectMode(I)V

    goto/16 :goto_1

    .line 2564
    :pswitch_22
    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/CameraSettings;->setViewMode(I)V

    goto/16 :goto_1

    .line 2567
    :pswitch_23
    invoke-virtual {v1, v7}, Lcom/sec/android/app/camera/CameraSettings;->setSelfieContinuousValue(I)V

    goto/16 :goto_1

    .line 2570
    :pswitch_24
    const/4 v5, 0x4

    invoke-virtual {v1, v5}, Lcom/sec/android/app/camera/CameraSettings;->setSelfieMode(I)V

    goto/16 :goto_1

    .line 2573
    :pswitch_25
    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/CameraSettings;->setBeautyLevel(I)V

    goto/16 :goto_1

    .line 2576
    :pswitch_26
    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/CameraSettings;->setSlimFaceLevel(I)V

    goto/16 :goto_1

    .line 2579
    :pswitch_27
    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/CameraSettings;->setEyeEnlargeLevel(I)V

    goto/16 :goto_1

    .line 2582
    :pswitch_28
    invoke-virtual {v1, v6}, Lcom/sec/android/app/camera/CameraSettings;->setScreenFlash(I)V

    goto/16 :goto_1

    .line 2589
    .end local v3    # "index":I
    :cond_3
    return-void

    .line 2448
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_18
        :pswitch_6
        :pswitch_e
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_8
        :pswitch_1a
        :pswitch_2
        :pswitch_4
        :pswitch_5
        :pswitch_7
        :pswitch_0
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_1e
        :pswitch_1b
        :pswitch_0
        :pswitch_0
        :pswitch_c
        :pswitch_d
        :pswitch_1c
        :pswitch_11
        :pswitch_f
        :pswitch_14
        :pswitch_1d
        :pswitch_12
        :pswitch_13
        :pswitch_15
        :pswitch_17
        :pswitch_19
        :pswitch_0
        :pswitch_16
        :pswitch_10
        :pswitch_0
        :pswitch_1f
        :pswitch_20
        :pswitch_0
        :pswitch_0
        :pswitch_21
        :pswitch_0
        :pswitch_22
        :pswitch_25
        :pswitch_0
        :pswitch_23
        :pswitch_24
        :pswitch_26
        :pswitch_27
        :pswitch_28
    .end packed-switch
.end method

.method public declared-synchronized synchronizeDim()V
    .locals 6

    .prologue
    .line 901
    monitor-enter p0

    :try_start_0
    const-string v4, "MenuDimController"

    const-string v5, "synchronizeDim"

    invoke-static {v4, v5}, Landroid/util/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 903
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/16 v4, 0x35

    if-ge v1, v4, :cond_3

    .line 904
    iget-object v4, p0, Lcom/sec/android/app/camera/MenuDimController;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/glview/TwGLView;

    .line 905
    .local v3, "view":Lcom/sec/android/glview/TwGLView;
    instance-of v4, v3, Lcom/sec/android/app/camera/glwidget/TwGLItem;

    if-eqz v4, :cond_1

    .line 906
    move-object v0, v3

    check-cast v0, Lcom/sec/android/app/camera/glwidget/TwGLItem;

    move-object v4, v0

    invoke-virtual {v4}, Lcom/sec/android/app/camera/glwidget/TwGLItem;->getCommandId()I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/sec/android/app/camera/MenuDimController;->getkeyFromCommandId(I)I

    move-result v4

    if-ne v4, v1, :cond_0

    .line 907
    iget-object v4, p0, Lcom/sec/android/app/camera/MenuDimController;->mCurrentDimArray:[Z

    aget-boolean v4, v4, v1

    invoke-virtual {v3}, Lcom/sec/android/glview/TwGLView;->isDim()Z

    move-result v5

    if-eq v4, v5, :cond_0

    .line 908
    iget-object v4, p0, Lcom/sec/android/app/camera/MenuDimController;->mCurrentDimArray:[Z

    aget-boolean v4, v4, v1

    invoke-virtual {v3, v4}, Lcom/sec/android/glview/TwGLView;->setDim(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 901
    .end local v1    # "i":I
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "view":Lcom/sec/android/glview/TwGLView;
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4

    .line 912
    .restart local v1    # "i":I
    .restart local v2    # "i$":Ljava/util/Iterator;
    .restart local v3    # "view":Lcom/sec/android/glview/TwGLView;
    :cond_1
    :try_start_1
    invoke-virtual {v3}, Lcom/sec/android/glview/TwGLView;->getTag()I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/sec/android/app/camera/MenuDimController;->getkeyFromCommandId(I)I

    move-result v4

    if-ne v4, v1, :cond_0

    .line 913
    iget-object v4, p0, Lcom/sec/android/app/camera/MenuDimController;->mCurrentDimArray:[Z

    aget-boolean v4, v4, v1

    invoke-virtual {v3}, Lcom/sec/android/glview/TwGLView;->isDim()Z

    move-result v5

    if-eq v4, v5, :cond_0

    .line 914
    iget-object v4, p0, Lcom/sec/android/app/camera/MenuDimController;->mCurrentDimArray:[Z

    aget-boolean v4, v4, v1

    invoke-virtual {v3, v4}, Lcom/sec/android/glview/TwGLView;->setDim(Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 903
    .end local v3    # "view":Lcom/sec/android/glview/TwGLView;
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 920
    .end local v2    # "i$":Ljava/util/Iterator;
    :cond_3
    monitor-exit p0

    return-void
.end method
