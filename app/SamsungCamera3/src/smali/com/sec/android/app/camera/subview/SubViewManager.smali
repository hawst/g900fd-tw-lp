.class public Lcom/sec/android/app/camera/subview/SubViewManager;
.super Ljava/lang/Object;
.source "SubViewManager.java"


# static fields
.field public static final DISPLAY_ORIENTATION_0:I = 0x0

.field public static final DISPLAY_ORIENTATION_180:I = 0x2

.field public static final DISPLAY_ORIENTATION_270:I = 0x1

.field public static final DISPLAY_ORIENTATION_90:I = 0x3

.field private static final MSG_UPDATE_LANDSCAPE_MENU:I = 0x1

.field private static final TAG:Ljava/lang/String; = "SubViewManager"

.field private static final UPDATE_LANDSCAPE_MENU_DELAY:I = 0xc8

.field private static mEasyMode:Z


# instance fields
.field private mActivityContext:Lcom/sec/android/app/camera/Camera;

.field private mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

.field private mDisplayOrientation:I

.field private mIsRequestRotateLayout:Z

.field private mLandscapeMode:Z

.field private mLastOrientation:I

.field protected mMainHandler:Landroid/os/Handler;

.field protected mOrientationListener:Landroid/view/OrientationEventListener;

.field protected mOrientationListener2:Landroid/view/OrientationEventListener;

.field private mRecordingMenu:Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;

.field protected mSubViewGroup:Landroid/view/ViewGroup;

.field private mTutorialCaptureMode:Lcom/sec/android/app/camera/subview/SubViewTutorialCaptureMode;

.field private mTutorialRecordingMode:Lcom/sec/android/app/camera/subview/SubViewTutorialRecordingMode;

.field private mTutorialSelectCameraMode:Lcom/sec/android/app/camera/subview/SubViewTutorialSelectCameraMode;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/camera/Camera;)V
    .locals 2
    .param p1, "activitycontext"    # Lcom/sec/android/app/camera/Camera;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mLastOrientation:I

    .line 49
    iput v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mDisplayOrientation:I

    .line 50
    iput-boolean v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mLandscapeMode:Z

    .line 51
    iput-boolean v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mIsRequestRotateLayout:Z

    .line 52
    iput-object v1, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mOrientationListener:Landroid/view/OrientationEventListener;

    .line 53
    iput-object v1, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mOrientationListener2:Landroid/view/OrientationEventListener;

    .line 67
    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewManager$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/camera/subview/SubViewManager$1;-><init>(Lcom/sec/android/app/camera/subview/SubViewManager;)V

    iput-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mMainHandler:Landroid/os/Handler;

    .line 82
    iput-object p1, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    .line 120
    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    invoke-direct {v0, p1, p0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;-><init>(Lcom/sec/android/app/camera/Camera;Lcom/sec/android/app/camera/subview/SubViewManager;)V

    iput-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    .line 128
    invoke-virtual {p0}, Lcom/sec/android/app/camera/subview/SubViewManager;->initialize()V

    .line 129
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/camera/subview/SubViewManager;)Lcom/sec/android/app/camera/subview/SubViewBaseMenu;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/subview/SubViewManager;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/camera/subview/SubViewManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/subview/SubViewManager;

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mLandscapeMode:Z

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/camera/subview/SubViewManager;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/subview/SubViewManager;

    .prologue
    .line 34
    iget v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mLastOrientation:I

    return v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/camera/subview/SubViewManager;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/camera/subview/SubViewManager;
    .param p1, "x1"    # I

    .prologue
    .line 34
    iput p1, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mLastOrientation:I

    return p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/camera/subview/SubViewManager;)Lcom/sec/android/app/camera/Camera;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/subview/SubViewManager;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/camera/subview/SubViewManager;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/subview/SubViewManager;

    .prologue
    .line 34
    iget v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mDisplayOrientation:I

    return v0
.end method

.method static synthetic access$402(Lcom/sec/android/app/camera/subview/SubViewManager;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/camera/subview/SubViewManager;
    .param p1, "x1"    # I

    .prologue
    .line 34
    iput p1, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mDisplayOrientation:I

    return p1
.end method


# virtual methods
.method public clear()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 226
    const-string v0, "SubViewManager"

    const-string v1, "clear"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 227
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mOrientationListener:Landroid/view/OrientationEventListener;

    if-eqz v0, :cond_0

    .line 228
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mOrientationListener:Landroid/view/OrientationEventListener;

    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->disable()V

    .line 229
    iput-object v2, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mOrientationListener:Landroid/view/OrientationEventListener;

    .line 231
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mOrientationListener2:Landroid/view/OrientationEventListener;

    if-eqz v0, :cond_1

    .line 232
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mOrientationListener2:Landroid/view/OrientationEventListener;

    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->disable()V

    .line 233
    iput-object v2, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mOrientationListener2:Landroid/view/OrientationEventListener;

    .line 235
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    if-eqz v0, :cond_2

    .line 236
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->clear()V

    .line 237
    iput-object v2, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    .line 239
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mRecordingMenu:Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;

    if-eqz v0, :cond_3

    .line 240
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mRecordingMenu:Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->clear()V

    .line 241
    iput-object v2, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mRecordingMenu:Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;

    .line 243
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mTutorialCaptureMode:Lcom/sec/android/app/camera/subview/SubViewTutorialCaptureMode;

    if-eqz v0, :cond_4

    .line 244
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mTutorialCaptureMode:Lcom/sec/android/app/camera/subview/SubViewTutorialCaptureMode;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewTutorialCaptureMode;->clear()V

    .line 245
    iput-object v2, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mTutorialCaptureMode:Lcom/sec/android/app/camera/subview/SubViewTutorialCaptureMode;

    .line 247
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mTutorialRecordingMode:Lcom/sec/android/app/camera/subview/SubViewTutorialRecordingMode;

    if-eqz v0, :cond_5

    .line 248
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mTutorialRecordingMode:Lcom/sec/android/app/camera/subview/SubViewTutorialRecordingMode;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewTutorialRecordingMode;->clear()V

    .line 249
    iput-object v2, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mTutorialRecordingMode:Lcom/sec/android/app/camera/subview/SubViewTutorialRecordingMode;

    .line 251
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mTutorialSelectCameraMode:Lcom/sec/android/app/camera/subview/SubViewTutorialSelectCameraMode;

    if-eqz v0, :cond_6

    .line 252
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mTutorialSelectCameraMode:Lcom/sec/android/app/camera/subview/SubViewTutorialSelectCameraMode;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewTutorialSelectCameraMode;->clear()V

    .line 253
    iput-object v2, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mTutorialSelectCameraMode:Lcom/sec/android/app/camera/subview/SubViewTutorialSelectCameraMode;

    .line 255
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mSubViewGroup:Landroid/view/ViewGroup;

    if-eqz v0, :cond_7

    .line 256
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mSubViewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 257
    iput-object v2, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mSubViewGroup:Landroid/view/ViewGroup;

    .line 259
    :cond_7
    return-void
.end method

.method public clearTutorialForHelpMode()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 285
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mTutorialCaptureMode:Lcom/sec/android/app/camera/subview/SubViewTutorialCaptureMode;

    if-eqz v0, :cond_0

    .line 286
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mTutorialCaptureMode:Lcom/sec/android/app/camera/subview/SubViewTutorialCaptureMode;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewTutorialCaptureMode;->onHide()V

    .line 287
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mTutorialCaptureMode:Lcom/sec/android/app/camera/subview/SubViewTutorialCaptureMode;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewTutorialCaptureMode;->clear()V

    .line 288
    iput-object v1, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mTutorialCaptureMode:Lcom/sec/android/app/camera/subview/SubViewTutorialCaptureMode;

    .line 290
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mTutorialRecordingMode:Lcom/sec/android/app/camera/subview/SubViewTutorialRecordingMode;

    if-eqz v0, :cond_1

    .line 291
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mTutorialRecordingMode:Lcom/sec/android/app/camera/subview/SubViewTutorialRecordingMode;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewTutorialRecordingMode;->onHide()V

    .line 292
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mTutorialRecordingMode:Lcom/sec/android/app/camera/subview/SubViewTutorialRecordingMode;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewTutorialRecordingMode;->clear()V

    .line 293
    iput-object v1, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mTutorialRecordingMode:Lcom/sec/android/app/camera/subview/SubViewTutorialRecordingMode;

    .line 295
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mTutorialSelectCameraMode:Lcom/sec/android/app/camera/subview/SubViewTutorialSelectCameraMode;

    if-eqz v0, :cond_2

    .line 296
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mTutorialSelectCameraMode:Lcom/sec/android/app/camera/subview/SubViewTutorialSelectCameraMode;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewTutorialSelectCameraMode;->onHide()V

    .line 297
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mTutorialSelectCameraMode:Lcom/sec/android/app/camera/subview/SubViewTutorialSelectCameraMode;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewTutorialSelectCameraMode;->clear()V

    .line 298
    iput-object v1, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mTutorialSelectCameraMode:Lcom/sec/android/app/camera/subview/SubViewTutorialSelectCameraMode;

    .line 300
    :cond_2
    return-void
.end method

.method public getCamcorderRecordingMenu()Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mRecordingMenu:Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;

    return-object v0
.end method

.method public getCameraBaseMenu()Lcom/sec/android/app/camera/subview/SubViewBaseMenu;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    return-object v0
.end method

.method public getCameraEditableSideBar()Lcom/sec/android/app/camera/subview/SubViewEditableSideBar;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    if-eqz v0, :cond_0

    .line 158
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->getEditableSideBar()Lcom/sec/android/app/camera/subview/SubViewEditableSideBar;

    move-result-object v0

    .line 160
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCameraEditableSideBar(I)Lcom/sec/android/app/camera/subview/SubViewEditableSideBar;
    .locals 1
    .param p1, "command"    # I

    .prologue
    .line 164
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    if-eqz v0, :cond_0

    .line 165
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->getEditableSideBar(I)Lcom/sec/android/app/camera/subview/SubViewEditableSideBar;

    move-result-object v0

    .line 167
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDisplayOrientation()I
    .locals 1

    .prologue
    .line 142
    iget v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mDisplayOrientation:I

    return v0
.end method

.method public getSubViewGroup()Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mSubViewGroup:Landroid/view/ViewGroup;

    return-object v0
.end method

.method public getTutorialCaptureMode()Lcom/sec/android/app/camera/subview/SubViewTutorialCaptureMode;
    .locals 1

    .prologue
    .line 628
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mTutorialCaptureMode:Lcom/sec/android/app/camera/subview/SubViewTutorialCaptureMode;

    return-object v0
.end method

.method public getTutorialRecordingMode()Lcom/sec/android/app/camera/subview/SubViewTutorialRecordingMode;
    .locals 1

    .prologue
    .line 632
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mTutorialRecordingMode:Lcom/sec/android/app/camera/subview/SubViewTutorialRecordingMode;

    return-object v0
.end method

.method public getTutorialSelectCameraMode()Lcom/sec/android/app/camera/subview/SubViewTutorialSelectCameraMode;
    .locals 1

    .prologue
    .line 636
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mTutorialSelectCameraMode:Lcom/sec/android/app/camera/subview/SubViewTutorialSelectCameraMode;

    return-object v0
.end method

.method public handleDimButtons()V
    .locals 2

    .prologue
    .line 510
    const-string v0, "SubViewManager"

    const-string v1, "handleDimButtons"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 511
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->isBaseMenuLoadingComplete()Z

    move-result v0

    if-nez v0, :cond_1

    .line 512
    const-string v0, "SubViewManager"

    const-string v1, "return handleDimButtons because basemenu is still loading"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 518
    :cond_0
    :goto_0
    return-void

    .line 515
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    if-eqz v0, :cond_0

    .line 516
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->handleDimButtons()V

    goto :goto_0
.end method

.method public hideBaseMenu()V
    .locals 1

    .prologue
    .line 558
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    if-eqz v0, :cond_0

    .line 559
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->hideBaseMenu()V

    .line 561
    :cond_0
    return-void
.end method

.method public hideBaseMenuForBestShot()V
    .locals 1

    .prologue
    .line 564
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    if-eqz v0, :cond_0

    .line 565
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->hideBaseMenuForBestShot()V

    .line 567
    :cond_0
    return-void
.end method

.method public hideBaseMenuForEditQuickSettings()V
    .locals 1

    .prologue
    .line 576
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    if-eqz v0, :cond_0

    .line 577
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->hideBaseMenuForEditQuickSettings()V

    .line 579
    :cond_0
    return-void
.end method

.method public hideBaseMenuForShootingMode()V
    .locals 1

    .prologue
    .line 570
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    if-eqz v0, :cond_0

    .line 571
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->hideBaseMenuForShootingMode()V

    .line 573
    :cond_0
    return-void
.end method

.method public hideCameraBaseIndicator()V
    .locals 1

    .prologue
    .line 666
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->getBaseIndicators()Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 667
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->getBaseIndicators()Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->hideCameraBaseIndicator()V

    .line 669
    :cond_0
    return-void
.end method

.method public hideRecordingMenu()V
    .locals 1

    .prologue
    .line 602
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mRecordingMenu:Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;

    if-eqz v0, :cond_0

    .line 603
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mRecordingMenu:Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->onHide()V

    .line 605
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    if-eqz v0, :cond_1

    .line 606
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->showBaseMenu()V

    .line 607
    iget-boolean v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mLandscapeMode:Z

    if-eqz v0, :cond_1

    .line 608
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->translateMenu2()V

    .line 611
    :cond_1
    return-void
.end method

.method public initialize()V
    .locals 2

    .prologue
    .line 132
    const-string v0, "SubViewManager"

    const-string v1, "initialize"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    invoke-virtual {p0}, Lcom/sec/android/app/camera/subview/SubViewManager;->setOrientationListener()V

    .line 134
    invoke-virtual {p0}, Lcom/sec/android/app/camera/subview/SubViewManager;->setOrientationListener2()V

    .line 135
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    if-eqz v0, :cond_0

    .line 136
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mDisplayOrientation:I

    .line 137
    :cond_0
    return-void
.end method

.method public isEasyMode()Z
    .locals 1

    .prologue
    .line 139
    sget-boolean v0, Lcom/sec/android/app/camera/subview/SubViewManager;->mEasyMode:Z

    return v0
.end method

.method public isShutterPressed()Z
    .locals 1

    .prologue
    .line 528
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    if-eqz v0, :cond_0

    .line 529
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->isShutterPressed()Z

    move-result v0

    .line 531
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onBaseMenuLoadingComplete()V
    .locals 2

    .prologue
    .line 362
    const-string v0, "SubViewManager"

    const-string v1, "onBaseMenuLoadingComplete"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 364
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    if-nez v0, :cond_1

    .line 365
    const-string v0, "SubViewManager"

    const-string v1, "mActivityContext is null"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 393
    :cond_0
    :goto_0
    return-void

    .line 369
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getEngine()Lcom/sec/android/app/camera/CommonEngine;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CommonEngine;->isRecording()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getEngine()Lcom/sec/android/app/camera/CommonEngine;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CommonEngine;->isRecordingPause()Z

    move-result v0

    if-nez v0, :cond_2

    .line 370
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    if-eqz v0, :cond_2

    .line 371
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->onShow()V

    .line 374
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->isMultiWindow()Z

    move-result v0

    if-nez v0, :cond_3

    .line 375
    invoke-virtual {p0}, Lcom/sec/android/app/camera/subview/SubViewManager;->rotateLayout()V

    .line 382
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/camera/subview/SubViewManager;->handleDimButtons()V

    .line 383
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->getEditableSideBar()Lcom/sec/android/app/camera/subview/SubViewEditableSideBar;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 384
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->getEditableSideBar()Lcom/sec/android/app/camera/subview/SubViewEditableSideBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewEditableSideBar;->refreshShortcutMenu()V

    .line 385
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->isContextMenuOpened()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getEngine()Lcom/sec/android/app/camera/CommonEngine;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CommonEngine;->isRecording()Z

    move-result v0

    if-nez v0, :cond_4

    .line 390
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mRecordingMenu:Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;

    if-nez v0, :cond_0

    .line 391
    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;

    iget-object v1, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-direct {v0, v1, p0}, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;-><init>(Lcom/sec/android/app/camera/Camera;Lcom/sec/android/app/camera/subview/SubViewManager;)V

    iput-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mRecordingMenu:Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;

    goto :goto_0
.end method

.method public onCameraSettingsChanged(II)V
    .locals 1
    .param p1, "menuid"    # I
    .param p2, "modeid"    # I

    .prologue
    .line 614
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    if-eqz v0, :cond_0

    .line 615
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->onCameraSettingsChanged(II)V

    .line 617
    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 171
    const-string v0, "SubViewManager"

    const-string v1, "onPause"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 172
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->isMultiWindow()Z

    move-result v0

    if-nez v0, :cond_0

    .line 173
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->enableNotificationTicker()V

    .line 175
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    if-eqz v0, :cond_1

    .line 176
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->onHide()V

    .line 178
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mRecordingMenu:Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;

    if-eqz v0, :cond_2

    .line 179
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mRecordingMenu:Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->onHide()V

    .line 181
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->getHelpMode()I

    move-result v0

    const/16 v1, 0xc8

    if-eq v0, v1, :cond_5

    .line 182
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mTutorialCaptureMode:Lcom/sec/android/app/camera/subview/SubViewTutorialCaptureMode;

    if-eqz v0, :cond_3

    .line 183
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mTutorialCaptureMode:Lcom/sec/android/app/camera/subview/SubViewTutorialCaptureMode;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewTutorialCaptureMode;->onHide()V

    .line 185
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mTutorialRecordingMode:Lcom/sec/android/app/camera/subview/SubViewTutorialRecordingMode;

    if-eqz v0, :cond_4

    .line 186
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mTutorialRecordingMode:Lcom/sec/android/app/camera/subview/SubViewTutorialRecordingMode;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewTutorialRecordingMode;->onHide()V

    .line 188
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mTutorialSelectCameraMode:Lcom/sec/android/app/camera/subview/SubViewTutorialSelectCameraMode;

    if-eqz v0, :cond_5

    .line 189
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mTutorialSelectCameraMode:Lcom/sec/android/app/camera/subview/SubViewTutorialSelectCameraMode;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewTutorialSelectCameraMode;->onHide()V

    .line 192
    :cond_5
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 195
    const-string v0, "SubViewManager"

    const-string v1, "onResume"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 196
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mRecordingMenu:Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;

    if-eqz v0, :cond_0

    .line 197
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mRecordingMenu:Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->onHide()V

    .line 199
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->getHelpMode()I

    move-result v0

    const/16 v1, 0xc8

    if-eq v0, v1, :cond_6

    .line 201
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mTutorialCaptureMode:Lcom/sec/android/app/camera/subview/SubViewTutorialCaptureMode;

    if-nez v0, :cond_1

    .line 202
    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewTutorialCaptureMode;

    iget-object v1, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-direct {v0, v1, p0}, Lcom/sec/android/app/camera/subview/SubViewTutorialCaptureMode;-><init>(Lcom/sec/android/app/camera/Camera;Lcom/sec/android/app/camera/subview/SubViewManager;)V

    iput-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mTutorialCaptureMode:Lcom/sec/android/app/camera/subview/SubViewTutorialCaptureMode;

    .line 204
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mTutorialRecordingMode:Lcom/sec/android/app/camera/subview/SubViewTutorialRecordingMode;

    if-nez v0, :cond_2

    .line 205
    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewTutorialRecordingMode;

    iget-object v1, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-direct {v0, v1, p0}, Lcom/sec/android/app/camera/subview/SubViewTutorialRecordingMode;-><init>(Lcom/sec/android/app/camera/Camera;Lcom/sec/android/app/camera/subview/SubViewManager;)V

    iput-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mTutorialRecordingMode:Lcom/sec/android/app/camera/subview/SubViewTutorialRecordingMode;

    .line 207
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mTutorialSelectCameraMode:Lcom/sec/android/app/camera/subview/SubViewTutorialSelectCameraMode;

    if-nez v0, :cond_3

    .line 208
    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewTutorialSelectCameraMode;

    iget-object v1, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-direct {v0, v1, p0}, Lcom/sec/android/app/camera/subview/SubViewTutorialSelectCameraMode;-><init>(Lcom/sec/android/app/camera/Camera;Lcom/sec/android/app/camera/subview/SubViewManager;)V

    iput-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mTutorialSelectCameraMode:Lcom/sec/android/app/camera/subview/SubViewTutorialSelectCameraMode;

    .line 210
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mTutorialCaptureMode:Lcom/sec/android/app/camera/subview/SubViewTutorialCaptureMode;

    if-eqz v0, :cond_4

    .line 211
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mTutorialCaptureMode:Lcom/sec/android/app/camera/subview/SubViewTutorialCaptureMode;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewTutorialCaptureMode;->onShow()V

    .line 213
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mTutorialRecordingMode:Lcom/sec/android/app/camera/subview/SubViewTutorialRecordingMode;

    if-eqz v0, :cond_5

    .line 214
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mTutorialRecordingMode:Lcom/sec/android/app/camera/subview/SubViewTutorialRecordingMode;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewTutorialRecordingMode;->onShow()V

    .line 216
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mTutorialSelectCameraMode:Lcom/sec/android/app/camera/subview/SubViewTutorialSelectCameraMode;

    if-eqz v0, :cond_6

    .line 217
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mTutorialSelectCameraMode:Lcom/sec/android/app/camera/subview/SubViewTutorialSelectCameraMode;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewTutorialSelectCameraMode;->onShow()V

    .line 220
    :cond_6
    invoke-virtual {p0}, Lcom/sec/android/app/camera/subview/SubViewManager;->handleDimButtons()V

    .line 221
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->isMultiWindow()Z

    move-result v0

    if-nez v0, :cond_7

    .line 222
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->disableNotificationTicker()V

    .line 223
    :cond_7
    return-void
.end method

.method public onTutorialMode()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 640
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->getHelpMode()I

    move-result v0

    const/16 v1, 0xca

    if-ne v0, v1, :cond_0

    .line 641
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    iget-object v0, v0, Lcom/sec/android/app/camera/Camera;->mTutorialRecordingMode:Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;

    if-eqz v0, :cond_0

    .line 642
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    iget-object v0, v0, Lcom/sec/android/app/camera/Camera;->mTutorialRecordingMode:Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;

    iget v0, v0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mTutorialStep:I

    if-ne v0, v2, :cond_0

    .line 643
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mTutorialRecordingMode:Lcom/sec/android/app/camera/subview/SubViewTutorialRecordingMode;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewTutorialRecordingMode;->setStepRecordingStart()V

    .line 647
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->getHelpMode()I

    move-result v0

    const/16 v1, 0xcb

    if-ne v0, v1, :cond_1

    .line 648
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    iget-object v0, v0, Lcom/sec/android/app/camera/Camera;->mTutorialSelectCameraMode:Lcom/sec/android/app/camera/glwidget/TwGLTutorialSelectCameraMode;

    if-eqz v0, :cond_1

    .line 649
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    iget-object v0, v0, Lcom/sec/android/app/camera/Camera;->mTutorialSelectCameraMode:Lcom/sec/android/app/camera/glwidget/TwGLTutorialSelectCameraMode;

    iget v0, v0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialSelectCameraMode;->mTutorialStep:I

    if-ne v0, v2, :cond_1

    .line 650
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mTutorialSelectCameraMode:Lcom/sec/android/app/camera/subview/SubViewTutorialSelectCameraMode;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewTutorialSelectCameraMode;->setStepSelectMode()V

    .line 654
    :cond_1
    return-void
.end method

.method public refreshShortcutMenu()V
    .locals 1

    .prologue
    .line 582
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    if-eqz v0, :cond_0

    .line 583
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->refreshShortcutMenu()V

    .line 585
    :cond_0
    return-void
.end method

.method public resetCameraSideBar()V
    .locals 2

    .prologue
    .line 620
    const-string v0, "SubViewManager"

    const-string v1, "resetCameraSideBar"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 621
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    if-eqz v0, :cond_0

    .line 622
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->resetCameraSideBar()V

    .line 624
    :cond_0
    return-void
.end method

.method public rotateLayout()V
    .locals 3

    .prologue
    .line 433
    const-string v0, "SubViewManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "rotateLayout : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mDisplayOrientation:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 507
    return-void
.end method

.method public rotateMenu()V
    .locals 2

    .prologue
    .line 419
    const-string v0, "SubViewManager"

    const-string v1, "rotateMenu"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 420
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mOrientationListener:Landroid/view/OrientationEventListener;

    if-eqz v0, :cond_0

    .line 421
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mOrientationListener:Landroid/view/OrientationEventListener;

    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->disable()V

    .line 423
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    if-eqz v0, :cond_1

    .line 424
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    iget v1, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mLastOrientation:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->setLastOrientation(I)V

    .line 425
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->rotateMenu()V

    .line 427
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mOrientationListener:Landroid/view/OrientationEventListener;

    if-eqz v0, :cond_2

    .line 428
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mOrientationListener:Landroid/view/OrientationEventListener;

    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->enable()V

    .line 430
    :cond_2
    return-void
.end method

.method public setBatteryLevel(IZ)V
    .locals 1
    .param p1, "level"    # I
    .param p2, "isCharging"    # Z

    .prologue
    .line 672
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->getBaseIndicators()Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 673
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->getBaseIndicators()Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->setBatteryLevel(IZ)V

    .line 675
    :cond_0
    return-void
.end method

.method public setButtonsDimControlForOverlayDialog(Z)V
    .locals 2
    .param p1, "bFocus"    # Z

    .prologue
    .line 521
    const-string v0, "SubViewManager"

    const-string v1, "setButtonsDimControlForOverlayDialog"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 523
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    if-eqz v0, :cond_0

    .line 524
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->handleDimButtons(Z)V

    .line 526
    :cond_0
    return-void
.end method

.method public setFlashIndicator(I)V
    .locals 1
    .param p1, "mode"    # I

    .prologue
    .line 690
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->getBaseIndicators()Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 691
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->getBaseIndicators()Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->setFlashIndicator(I)V

    .line 693
    :cond_0
    return-void
.end method

.method public setGPSIndicator(I)V
    .locals 1
    .param p1, "mode"    # I

    .prologue
    .line 708
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->getBaseIndicators()Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 709
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->getBaseIndicators()Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->setGPSIndicator(I)V

    .line 711
    :cond_0
    return-void
.end method

.method public setLandscapeMode(Z)V
    .locals 3
    .param p1, "landscapemode"    # Z

    .prologue
    .line 396
    const-string v0, "SubViewManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setLandscapeMode : landscapemode="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 398
    iput-boolean p1, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mLandscapeMode:Z

    .line 399
    if-eqz p1, :cond_3

    .line 400
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    if-eqz v0, :cond_0

    .line 401
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->setLandscapeMode(Z)V

    .line 403
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mOrientationListener:Landroid/view/OrientationEventListener;

    if-eqz v0, :cond_1

    .line 404
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mOrientationListener:Landroid/view/OrientationEventListener;

    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->enable()V

    .line 413
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mRecordingMenu:Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;

    if-eqz v0, :cond_2

    .line 414
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mRecordingMenu:Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->setLandscapeMode(Z)V

    .line 416
    :cond_2
    return-void

    .line 407
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mOrientationListener:Landroid/view/OrientationEventListener;

    if-eqz v0, :cond_1

    .line 408
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mOrientationListener:Landroid/view/OrientationEventListener;

    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->disable()V

    goto :goto_0
.end method

.method public setLowlightIndicator(I)V
    .locals 1
    .param p1, "type"    # I

    .prologue
    .line 696
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->getBaseIndicators()Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 697
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->getBaseIndicators()Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->setLowlightIndicator(I)V

    .line 699
    :cond_0
    return-void
.end method

.method protected setOrientationListener()V
    .locals 2

    .prologue
    .line 303
    const-string v0, "SubViewManager"

    const-string v1, "setOrientationListener"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 305
    sget-boolean v0, Lcom/sec/android/app/camera/subview/SubViewManager;->mEasyMode:Z

    if-eqz v0, :cond_0

    .line 306
    const-string v0, "SubViewManager"

    const-string v1, "Don\'t rotate recording menu"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 325
    :goto_0
    return-void

    .line 309
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mOrientationListener:Landroid/view/OrientationEventListener;

    if-nez v0, :cond_1

    .line 310
    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewManager$2;

    iget-object v1, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/camera/subview/SubViewManager$2;-><init>(Lcom/sec/android/app/camera/subview/SubViewManager;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mOrientationListener:Landroid/view/OrientationEventListener;

    .line 324
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mOrientationListener:Landroid/view/OrientationEventListener;

    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->disable()V

    goto :goto_0
.end method

.method protected setOrientationListener2()V
    .locals 2

    .prologue
    .line 328
    const-string v0, "SubViewManager"

    const-string v1, "setOrientationListener2"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 330
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mOrientationListener2:Landroid/view/OrientationEventListener;

    if-nez v0, :cond_0

    .line 331
    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewManager$3;

    iget-object v1, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/Camera;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/camera/subview/SubViewManager$3;-><init>(Lcom/sec/android/app/camera/subview/SubViewManager;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mOrientationListener2:Landroid/view/OrientationEventListener;

    .line 358
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mOrientationListener2:Landroid/view/OrientationEventListener;

    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->enable()V

    .line 359
    return-void
.end method

.method public setRecordingModeIndicator(I)V
    .locals 1
    .param p1, "mode"    # I

    .prologue
    .line 714
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->getBaseIndicators()Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 715
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->getBaseIndicators()Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->setRecordingModeIndicator(I)V

    .line 717
    :cond_0
    return-void
.end method

.method public setRemainCountIndicator(I)V
    .locals 1
    .param p1, "count"    # I

    .prologue
    .line 678
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->getBaseIndicators()Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 679
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->getBaseIndicators()Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->setRemainCountIndicator(I)V

    .line 681
    :cond_0
    return-void
.end method

.method public setShutterBtnPressed(Z)V
    .locals 1
    .param p1, "pressed"    # Z

    .prologue
    .line 535
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    if-eqz v0, :cond_0

    .line 536
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->setShutterBtnPressed(Z)V

    .line 538
    :cond_0
    return-void
.end method

.method public setShutterButtonDimmed(Z)V
    .locals 1
    .param p1, "dim"    # Z

    .prologue
    .line 540
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    if-eqz v0, :cond_0

    .line 541
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->setShutterButtonDimmed(Z)V

    .line 543
    :cond_0
    return-void
.end method

.method public setStorageIndicator(I)V
    .locals 1
    .param p1, "mode"    # I

    .prologue
    .line 702
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->getBaseIndicators()Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 703
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->getBaseIndicators()Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->setStorageIndicator(I)V

    .line 705
    :cond_0
    return-void
.end method

.method public setTimerIndicator(I)V
    .locals 1
    .param p1, "mode"    # I

    .prologue
    .line 684
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->getBaseIndicators()Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 685
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->getBaseIndicators()Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->setTimerIndicator(I)V

    .line 687
    :cond_0
    return-void
.end method

.method public setTutorialForHelpMode(I)V
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 262
    packed-switch p1, :pswitch_data_0

    .line 282
    :cond_0
    :goto_0
    return-void

    .line 264
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mTutorialCaptureMode:Lcom/sec/android/app/camera/subview/SubViewTutorialCaptureMode;

    if-nez v0, :cond_0

    .line 265
    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewTutorialCaptureMode;

    iget-object v1, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-direct {v0, v1, p0}, Lcom/sec/android/app/camera/subview/SubViewTutorialCaptureMode;-><init>(Lcom/sec/android/app/camera/Camera;Lcom/sec/android/app/camera/subview/SubViewManager;)V

    iput-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mTutorialCaptureMode:Lcom/sec/android/app/camera/subview/SubViewTutorialCaptureMode;

    .line 266
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mTutorialCaptureMode:Lcom/sec/android/app/camera/subview/SubViewTutorialCaptureMode;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewTutorialCaptureMode;->onShow()V

    goto :goto_0

    .line 270
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mTutorialRecordingMode:Lcom/sec/android/app/camera/subview/SubViewTutorialRecordingMode;

    if-nez v0, :cond_0

    .line 271
    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewTutorialRecordingMode;

    iget-object v1, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-direct {v0, v1, p0}, Lcom/sec/android/app/camera/subview/SubViewTutorialRecordingMode;-><init>(Lcom/sec/android/app/camera/Camera;Lcom/sec/android/app/camera/subview/SubViewManager;)V

    iput-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mTutorialRecordingMode:Lcom/sec/android/app/camera/subview/SubViewTutorialRecordingMode;

    .line 272
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mTutorialRecordingMode:Lcom/sec/android/app/camera/subview/SubViewTutorialRecordingMode;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewTutorialRecordingMode;->onShow()V

    goto :goto_0

    .line 276
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mTutorialSelectCameraMode:Lcom/sec/android/app/camera/subview/SubViewTutorialSelectCameraMode;

    if-nez v0, :cond_0

    .line 277
    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewTutorialSelectCameraMode;

    iget-object v1, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-direct {v0, v1, p0}, Lcom/sec/android/app/camera/subview/SubViewTutorialSelectCameraMode;-><init>(Lcom/sec/android/app/camera/Camera;Lcom/sec/android/app/camera/subview/SubViewManager;)V

    iput-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mTutorialSelectCameraMode:Lcom/sec/android/app/camera/subview/SubViewTutorialSelectCameraMode;

    .line 278
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mTutorialSelectCameraMode:Lcom/sec/android/app/camera/subview/SubViewTutorialSelectCameraMode;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewTutorialSelectCameraMode;->onShow()V

    goto :goto_0

    .line 262
    nop

    :pswitch_data_0
    .packed-switch 0xc9
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setVoiceStatus(I)V
    .locals 1
    .param p1, "status"    # I

    .prologue
    .line 726
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->getBaseIndicators()Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 727
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->getBaseIndicators()Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->setVoiceStatus(I)V

    .line 729
    :cond_0
    return-void
.end method

.method public showBaseMenu()V
    .locals 1

    .prologue
    .line 546
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    if-eqz v0, :cond_0

    .line 547
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->showBaseMenu()V

    .line 549
    :cond_0
    return-void
.end method

.method public showBaseMenuForEditQuickSettings()V
    .locals 1

    .prologue
    .line 552
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    if-eqz v0, :cond_0

    .line 553
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->showBaseMenuForEditQuickSettings()V

    .line 555
    :cond_0
    return-void
.end method

.method public showCameraBaseIndicator()V
    .locals 1

    .prologue
    .line 660
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->getBaseIndicators()Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 661
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->getBaseIndicators()Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->showCameraBaseIndicator()V

    .line 663
    :cond_0
    return-void
.end method

.method public showRecordingMenu()V
    .locals 2

    .prologue
    .line 588
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    if-eqz v0, :cond_0

    .line 589
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->hideBaseMenuForRecording()V

    .line 592
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mRecordingMenu:Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;

    if-nez v0, :cond_1

    .line 593
    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;

    iget-object v1, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-direct {v0, v1, p0}, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;-><init>(Lcom/sec/android/app/camera/Camera;Lcom/sec/android/app/camera/subview/SubViewManager;)V

    iput-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mRecordingMenu:Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;

    .line 595
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mRecordingMenu:Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;

    if-eqz v0, :cond_2

    .line 596
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mRecordingMenu:Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->onShow()V

    .line 597
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mRecordingMenu:Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->handleDimButtons()V

    .line 599
    :cond_2
    return-void
.end method

.method public showVoiceInputIndicator(I)V
    .locals 1
    .param p1, "visible"    # I

    .prologue
    .line 720
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->getBaseIndicators()Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 721
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewManager;->mBaseMenu:Lcom/sec/android/app/camera/subview/SubViewBaseMenu;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewBaseMenu;->getBaseIndicators()Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/camera/subview/SubViewBaseIndicators;->showVoiceInputIndicator(I)V

    .line 723
    :cond_0
    return-void
.end method
