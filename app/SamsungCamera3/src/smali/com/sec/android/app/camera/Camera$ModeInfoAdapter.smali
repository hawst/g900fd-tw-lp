.class Lcom/sec/android/app/camera/Camera$ModeInfoAdapter;
.super Landroid/widget/BaseAdapter;
.source "Camera.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/camera/Camera;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ModeInfoAdapter"
.end annotation


# instance fields
.field mActivityContext:Lcom/sec/android/app/camera/Camera;

.field mData:Lcom/sec/android/app/camera/resourcedata/AllCameraShootingModeResourceData;

.field final synthetic this$0:Lcom/sec/android/app/camera/Camera;


# direct methods
.method constructor <init>(Lcom/sec/android/app/camera/Camera;Lcom/sec/android/app/camera/Camera;Lcom/sec/android/app/camera/resourcedata/AllCameraShootingModeResourceData;)V
    .locals 0
    .param p2, "activityContext"    # Lcom/sec/android/app/camera/Camera;
    .param p3, "data"    # Lcom/sec/android/app/camera/resourcedata/AllCameraShootingModeResourceData;

    .prologue
    .line 20079
    iput-object p1, p0, Lcom/sec/android/app/camera/Camera$ModeInfoAdapter;->this$0:Lcom/sec/android/app/camera/Camera;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 20080
    iput-object p2, p0, Lcom/sec/android/app/camera/Camera$ModeInfoAdapter;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    .line 20081
    iput-object p3, p0, Lcom/sec/android/app/camera/Camera$ModeInfoAdapter;->mData:Lcom/sec/android/app/camera/resourcedata/AllCameraShootingModeResourceData;

    .line 20082
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 2

    .prologue
    .line 20086
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera$ModeInfoAdapter;->mData:Lcom/sec/android/app/camera/resourcedata/AllCameraShootingModeResourceData;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/resourcedata/AllCameraShootingModeResourceData;->getNumberOfItems()I

    move-result v0

    .line 20087
    .local v0, "size":I
    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "arg0"    # I

    .prologue
    .line 20145
    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "arg0"    # I

    .prologue
    .line 20150
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 15
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 20092
    if-nez p2, :cond_0

    .line 20093
    iget-object v12, p0, Lcom/sec/android/app/camera/Camera$ModeInfoAdapter;->this$0:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v12}, Lcom/sec/android/app/camera/Camera;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v12

    const v13, 0x7f040010

    const/4 v14, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v12, v13, v0, v14}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 20096
    :cond_0
    const v12, 0x7f0e002d

    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    .line 20097
    .local v5, "image":Landroid/widget/ImageView;
    const v12, 0x7f0e002e

    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    .line 20098
    .local v11, "titleView":Landroid/widget/TextView;
    const v12, 0x7f0e002f

    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 20100
    .local v3, "descView":Landroid/widget/TextView;
    const-string v10, ""

    .line 20101
    .local v10, "title":Ljava/lang/String;
    const-string v4, ""

    .line 20102
    .local v4, "description":Ljava/lang/String;
    iget-object v12, p0, Lcom/sec/android/app/camera/Camera$ModeInfoAdapter;->mData:Lcom/sec/android/app/camera/resourcedata/AllCameraShootingModeResourceData;

    move/from16 v0, p1

    invoke-virtual {v12, v0}, Lcom/sec/android/app/camera/resourcedata/AllCameraShootingModeResourceData;->getCommandId(I)I

    move-result v2

    .line 20103
    .local v2, "commandId":I
    invoke-static {v2}, Lcom/sec/android/app/camera/PlugInShootingModesLoader;->isShootingModeExternal(I)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 20104
    const/4 v12, 0x0

    invoke-static {v2, v12}, Lcom/sec/android/app/camera/PlugInShootingModesStorage;->getAllMode(ILjava/lang/String;)Lcom/sec/android/app/camera/PlugInShootingModesStorage$PlugInShootingMode;

    move-result-object v6

    .line 20105
    .local v6, "mode":Lcom/sec/android/app/camera/PlugInShootingModesStorage$PlugInShootingMode;
    if-eqz v6, :cond_1

    .line 20106
    iget-object v12, v6, Lcom/sec/android/app/camera/PlugInShootingModesStorage$PlugInShootingMode;->modeTitle:Ljava/lang/String;

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 20107
    iget-object v12, v6, Lcom/sec/android/app/camera/PlugInShootingModesStorage$PlugInShootingMode;->modeDesc:Ljava/lang/String;

    invoke-virtual {v3, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 20109
    iget-object v12, p0, Lcom/sec/android/app/camera/Camera$ModeInfoAdapter;->this$0:Lcom/sec/android/app/camera/Camera;

    iget-object v12, v12, Lcom/sec/android/app/camera/Camera;->mGLContext:Lcom/sec/android/glview/TwGLContext;

    invoke-virtual {v12}, Lcom/sec/android/glview/TwGLContext;->getApplicationContext()Landroid/content/Context;

    move-result-object v12

    iget-object v13, v6, Lcom/sec/android/app/camera/PlugInShootingModesStorage$PlugInShootingMode;->packageName:Ljava/lang/String;

    invoke-static {v12, v13}, Lcom/sec/android/app/camera/PlugInShootingModesStorage;->getResources(Landroid/content/Context;Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v12

    iget v13, v6, Lcom/sec/android/app/camera/PlugInShootingModesStorage$PlugInShootingMode;->iconId:I

    invoke-static {v12, v13}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 20111
    .local v1, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v1, :cond_1

    .line 20112
    invoke-virtual {v5, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 20138
    .end local v1    # "bitmap":Landroid/graphics/Bitmap;
    .end local v6    # "mode":Lcom/sec/android/app/camera/PlugInShootingModesStorage$PlugInShootingMode;
    :cond_1
    :goto_0
    return-object p2

    .line 20116
    :cond_2
    iget-object v12, p0, Lcom/sec/android/app/camera/Camera$ModeInfoAdapter;->this$0:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v12}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v12

    invoke-virtual {v12, v2}, Lcom/sec/android/app/camera/CameraSettings;->getShootingModeByCommandId(I)I

    move-result v9

    .line 20117
    .local v9, "shootingMode":I
    iget-object v12, p0, Lcom/sec/android/app/camera/Camera$ModeInfoAdapter;->this$0:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v12}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v12

    invoke-virtual {v12}, Lcom/sec/android/app/camera/CameraSettings;->getShootingMode()I

    move-result v12

    const/16 v13, 0x30

    if-ne v12, v13, :cond_3

    .line 20118
    iget-object v12, p0, Lcom/sec/android/app/camera/Camera$ModeInfoAdapter;->this$0:Lcom/sec/android/app/camera/Camera;

    const v13, 0x7f0c0041

    invoke-virtual {v12, v13}, Lcom/sec/android/app/camera/Camera;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 20124
    :goto_1
    iget-object v12, p0, Lcom/sec/android/app/camera/Camera$ModeInfoAdapter;->this$0:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v12, v9}, Lcom/sec/android/app/camera/Camera;->getShootingModeDescription(I)Ljava/lang/String;

    move-result-object v4

    .line 20125
    invoke-virtual {v11, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 20126
    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 20128
    const/4 v12, 0x0

    invoke-static {v2, v12}, Lcom/sec/android/app/camera/PlugInShootingModesStorage;->getAllMode(ILjava/lang/String;)Lcom/sec/android/app/camera/PlugInShootingModesStorage$PlugInShootingMode;

    move-result-object v8

    .line 20129
    .local v8, "preMode":Lcom/sec/android/app/camera/PlugInShootingModesStorage$PlugInShootingMode;
    if-eqz v8, :cond_1

    .line 20130
    iget-object v12, p0, Lcom/sec/android/app/camera/Camera$ModeInfoAdapter;->this$0:Lcom/sec/android/app/camera/Camera;

    iget-object v12, v12, Lcom/sec/android/app/camera/Camera;->mGLContext:Lcom/sec/android/glview/TwGLContext;

    invoke-virtual {v12}, Lcom/sec/android/glview/TwGLContext;->getApplicationContext()Landroid/content/Context;

    move-result-object v12

    iget-object v13, v8, Lcom/sec/android/app/camera/PlugInShootingModesStorage$PlugInShootingMode;->packageName:Ljava/lang/String;

    invoke-static {v12, v13}, Lcom/sec/android/app/camera/PlugInShootingModesStorage;->getResources(Landroid/content/Context;Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v12

    iget v13, v8, Lcom/sec/android/app/camera/PlugInShootingModesStorage$PlugInShootingMode;->iconId:I

    invoke-static {v12, v13}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 20132
    .local v7, "preBitmap":Landroid/graphics/Bitmap;
    if-eqz v7, :cond_1

    .line 20133
    invoke-virtual {v5, v7}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 20122
    .end local v7    # "preBitmap":Landroid/graphics/Bitmap;
    .end local v8    # "preMode":Lcom/sec/android/app/camera/PlugInShootingModesStorage$PlugInShootingMode;
    :cond_3
    iget-object v12, p0, Lcom/sec/android/app/camera/Camera$ModeInfoAdapter;->this$0:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v12}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v12

    invoke-virtual {v12, v9}, Lcom/sec/android/app/camera/CameraSettings;->getShootingModeResourceString(I)Ljava/lang/String;

    move-result-object v10

    goto :goto_1
.end method
