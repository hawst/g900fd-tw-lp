.class public Lcom/sec/android/app/camera/panorama360/Panorama360Callback;
.super Ljava/lang/Object;
.source "Panorama360Callback.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;
.implements Lcom/sec/android/app/camera/CommonEngine$CommonEnginePreviewCallback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/camera/panorama360/Panorama360Callback$ImageClassifyThread;,
        Lcom/sec/android/app/camera/panorama360/Panorama360Callback$ReleaseMemory;
    }
.end annotation


# static fields
.field private static final AppSensorFusionMode:I = 0x1

.field private static final BOUNDARY_APS_FOR_CALC_OFFSET:D

.field private static final BOUNDARY_APS_FOR_EXTRA_TIME:D

.field private static final CENTER_GUIDE_ANIMATION_PHASE1:I = 0x280

.field private static final CENTER_GUIDE_ANIMATION_PHASE2:I = 0x49c

.field private static final CENTER_GUIDE_ANIMATION_PHASE3:I = 0x71c

.field private static final CENTER_GUIDE_ANIMATION_PHASE4:I = 0x938

.field private static final CENTER_GUIDE_ANIMATION_PHASE5:I = 0xbb8

.field private static final CENTER_GUIDE_ANIMATION_PROGRESS1:I = 0x2aaa

.field private static final CENTER_GUIDE_ANIMATION_PROGRESS2:I = 0x5555

.field private static final CENTER_GUIDE_ANIMATION_PROGRESS3:I = 0x8000

.field private static final InitialSensorFusionMode:I = 0x2

.field private static final LIMITS_OVER_INTERVAL:F = 1.0E8f

.field private static final LOG_TAG:Ljava/lang/String; = "Panorama360Callback"

.field private static final NS2MS:F = 1.0E-6f

.field private static final NS2S:F = 1.0E-9f

.field private static final PANORAMA_STATE_INITIALIZED:I = 0x1

.field private static final PANORAMA_STATE_PROCESS:I = 0x2

.field private static final PANORAMA_STATE_STOP:I = 0x3

.field private static final PANORAMA_STATE_UNINITIALIZED:I = 0x0

.field private static final SENSOR_CORRECTION_EXTRA_TIME:I = 0x3e8

.field private static final SENSOR_CORRECTION_TIME_BEFORE_HAND:I = 0x2710

.field private static final SENSOR_CORRECTION_TIME_EVERYTIME:I = 0xbb8


# instance fields
.field private final RES_ID_PANORAMA_GUIDE_IMAGE:[[I

.field private doImageClassify:Z

.field isSetOffset:Z

.field private mACMatrix:[D

.field private mAccelerometer:Landroid/hardware/Sensor;

.field private mActivityContext:Lcom/sec/android/app/camera/Camera;

.field private mAllGuideTaken:Z

.field private mCameraHeight:I

.field private mCameraOrientation:I

.field private mCameraWidth:I

.field private mCurGyroscopeAngleIndex:I

.field private mCurGyroscopeAngleValue:[D

.field private mCurGyroscopeAngleValueHist:[[D

.field private mCurSensorIndex:[I

.field private mCurrentSaveStorage:I

.field private mDateTaken:[J

.field private mFinishFlg:Z

.field private mFinishShootingAsync:Lcom/sec/android/app/camera/panorama360/FinishShootingAsync;

.field private mGuideImage:[Landroid/graphics/Bitmap;

.field private mGyroMatrix:[D

.field private mGyroscope:Landroid/hardware/Sensor;

.field private mGyroscopeValueNumPerFrame:I

.field private mHandler:Landroid/os/Handler;

.field private mImageClassifyBmp:Landroid/graphics/Bitmap;

.field private mImageClassifyResult:D

.field private mImageClassifyThread:Lcom/sec/android/app/camera/panorama360/Panorama360Callback$ImageClassifyThread;

.field private mIsCalibrationEnabled:Z

.field private mIsEnoughSpace:Z

.field private mMagneticField:Landroid/hardware/Sensor;

.field private mMaxHeapSize:[I

.field private mMenuResourceDepot:Lcom/sec/android/app/camera/MenuResourceDepot;

.field private mMorphoImageStitcher:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;

.field private mOffsetMode:I

.field public mPanorama360View:Lcom/sec/android/app/camera/panorama360/Panorama360View;

.field private mPanoramaInitParam:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$PanoramaInitParam;

.field private mPanoramaPreviewCount:I

.field public mPanoramaProcessCount:I

.field private mPanoramaState:I

.field private mPreviousCenterGuideProgress:I

.field private mProgressPopup:Lcom/sec/android/app/camera/glwidget/TwGLProgressingPopup;

.field private mRVMatrix:[D

.field private mRotationVector:Landroid/hardware/Sensor;

.field public mSensorFusion:Lcom/sec/android/app/camera/panorama360/SensorFusion;

.field private mSensorLockObj:Ljava/lang/Object;

.field private mSensorManager:Landroid/hardware/SensorManager;

.field private mShootingNum:I

.field private mStatus:[I

.field private mTotalGyroscopeValue:[D

.field private mTotalGyroscopeValueNum:I

.field private mUseImage:[I

.field private mWaitTime:I

.field private moveToShooting:Z

.field private pre_frame_time:J

.field private prev_timestamp:J

.field private registered_accelerometer:Z

.field private registered_gyroscope:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 55
    const-wide/high16 v0, 0x4014000000000000L    # 5.0

    invoke-static {v0, v1}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v0

    sput-wide v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->BOUNDARY_APS_FOR_EXTRA_TIME:D

    .line 56
    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    invoke-static {v0, v1}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v0

    sput-wide v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->BOUNDARY_APS_FOR_CALC_OFFSET:D

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/camera/Camera;Landroid/os/Handler;)V
    .locals 12
    .param p1, "contextActivity"    # Lcom/sec/android/app/camera/Camera;
    .param p2, "handler"    # Landroid/os/Handler;

    .prologue
    .line 158
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanoramaState:I

    .line 67
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mAllGuideTaken:Z

    .line 75
    const/16 v0, 0x10

    new-array v0, v0, [[I

    const/4 v1, 0x0

    const/4 v2, 0x2

    new-array v2, v2, [I

    fill-array-data v2, :array_0

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const/4 v2, 0x2

    new-array v2, v2, [I

    fill-array-data v2, :array_1

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const/4 v2, 0x2

    new-array v2, v2, [I

    fill-array-data v2, :array_2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const/4 v2, 0x2

    new-array v2, v2, [I

    fill-array-data v2, :array_3

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const/4 v2, 0x2

    new-array v2, v2, [I

    fill-array-data v2, :array_4

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const/4 v2, 0x2

    new-array v2, v2, [I

    fill-array-data v2, :array_5

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const/4 v2, 0x2

    new-array v2, v2, [I

    fill-array-data v2, :array_6

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const/4 v2, 0x2

    new-array v2, v2, [I

    fill-array-data v2, :array_7

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const/4 v2, 0x2

    new-array v2, v2, [I

    fill-array-data v2, :array_8

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const/4 v2, 0x2

    new-array v2, v2, [I

    fill-array-data v2, :array_9

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const/4 v2, 0x2

    new-array v2, v2, [I

    fill-array-data v2, :array_a

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const/4 v2, 0x2

    new-array v2, v2, [I

    fill-array-data v2, :array_b

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const/4 v2, 0x2

    new-array v2, v2, [I

    fill-array-data v2, :array_c

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const/4 v2, 0x2

    new-array v2, v2, [I

    fill-array-data v2, :array_d

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const/4 v2, 0x2

    new-array v2, v2, [I

    fill-array-data v2, :array_e

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const/4 v2, 0x2

    new-array v2, v2, [I

    fill-array-data v2, :array_f

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->RES_ID_PANORAMA_GUIDE_IMAGE:[[I

    .line 92
    const/4 v0, 0x1

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mMaxHeapSize:[I

    .line 93
    const/4 v0, 0x1

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mStatus:[I

    .line 98
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mFinishFlg:Z

    .line 99
    const/4 v0, 0x1

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mUseImage:[I

    .line 117
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mGyroscope:Landroid/hardware/Sensor;

    .line 118
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mAccelerometer:Landroid/hardware/Sensor;

    .line 119
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mMagneticField:Landroid/hardware/Sensor;

    .line 120
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mRotationVector:Landroid/hardware/Sensor;

    .line 122
    const/4 v0, 0x3

    new-array v0, v0, [D

    iput-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mTotalGyroscopeValue:[D

    .line 124
    const/4 v0, 0x3

    new-array v0, v0, [D

    iput-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mCurGyroscopeAngleValue:[D

    .line 125
    const/16 v0, 0xa

    const/4 v1, 0x3

    filled-new-array {v0, v1}, [I

    move-result-object v0

    sget-object v1, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[D

    iput-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mCurGyroscopeAngleValueHist:[[D

    .line 130
    const/16 v0, 0x9

    new-array v0, v0, [D

    iput-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mGyroMatrix:[D

    .line 131
    const/16 v0, 0x9

    new-array v0, v0, [D

    iput-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mRVMatrix:[D

    .line 132
    const/16 v0, 0x9

    new-array v0, v0, [D

    iput-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mACMatrix:[D

    .line 139
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mMorphoImageStitcher:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;

    .line 140
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mSensorFusion:Lcom/sec/android/app/camera/panorama360/SensorFusion;

    .line 143
    const/4 v0, 0x2

    new-array v0, v0, [J

    iput-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mDateTaken:[J

    .line 145
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mSensorLockObj:Ljava/lang/Object;

    .line 146
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->pre_frame_time:J

    .line 148
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->moveToShooting:Z

    .line 150
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mImageClassifyThread:Lcom/sec/android/app/camera/panorama360/Panorama360Callback$ImageClassifyThread;

    .line 154
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->isSetOffset:Z

    .line 363
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mIsCalibrationEnabled:Z

    .line 970
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->registered_gyroscope:Z

    .line 971
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->registered_accelerometer:Z

    .line 1036
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPreviousCenterGuideProgress:I

    .line 1053
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->prev_timestamp:J

    .line 1277
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mFinishShootingAsync:Lcom/sec/android/app/camera/panorama360/FinishShootingAsync;

    .line 1388
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mIsEnoughSpace:Z

    .line 1390
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mCurrentSaveStorage:I

    .line 159
    const-string v0, "Panorama360Callback"

    const-string v1, "pc Panorama360Callback "

    invoke-static {v0, v1}, Lcom/sec/android/app/camera/panorama360/LogFilter;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    iput-object p1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    .line 161
    iput-object p2, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mHandler:Landroid/os/Handler;

    .line 163
    const-string v0, "1440x1080"

    invoke-static {v0}, Lcom/sec/android/app/camera/CameraResolution;->getResolutionID(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Lcom/sec/android/app/camera/CameraResolution;->getIntWidth(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mCameraWidth:I

    .line 164
    const-string v0, "1440x1080"

    invoke-static {v0}, Lcom/sec/android/app/camera/CameraResolution;->getResolutionID(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Lcom/sec/android/app/camera/CameraResolution;->getIntHeight(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mCameraHeight:I

    .line 166
    invoke-static {}, Lcom/sec/android/app/camera/panorama360/Panorama360DebugUtils;->initialize()V

    .line 168
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    if-eqz v0, :cond_4

    .line 169
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getMenuResourceDepot()Lcom/sec/android/app/camera/MenuResourceDepot;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mMenuResourceDepot:Lcom/sec/android/app/camera/MenuResourceDepot;

    .line 170
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getEngine()Lcom/sec/android/app/camera/CommonEngine;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CommonEngine;->checkSaveDirectory()V

    .line 172
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    const-string v1, "sensor"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/camera/Camera;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mSensorManager:Landroid/hardware/SensorManager;

    .line 173
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mSensorManager:Landroid/hardware/SensorManager;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getSensorList(I)Ljava/util/List;

    move-result-object v11

    .line 174
    .local v11, "sensors":Ljava/util/List;, "Ljava/util/List<Landroid/hardware/Sensor;>;"
    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/hardware/Sensor;

    .line 175
    .local v10, "sensor":Landroid/hardware/Sensor;
    invoke-virtual {v10}, Landroid/hardware/Sensor;->getType()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 176
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mSensorManager:Landroid/hardware/SensorManager;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mGyroscope:Landroid/hardware/Sensor;

    .line 179
    :cond_1
    invoke-virtual {v10}, Landroid/hardware/Sensor;->getType()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 180
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mSensorManager:Landroid/hardware/SensorManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mAccelerometer:Landroid/hardware/Sensor;

    .line 182
    :cond_2
    invoke-virtual {v10}, Landroid/hardware/Sensor;->getType()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 183
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mSensorManager:Landroid/hardware/SensorManager;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mMagneticField:Landroid/hardware/Sensor;

    .line 185
    :cond_3
    invoke-virtual {v10}, Landroid/hardware/Sensor;->getType()I

    move-result v0

    const/16 v1, 0xb

    if-ne v0, v1, :cond_0

    .line 186
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mSensorManager:Landroid/hardware/SensorManager;

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mRotationVector:Landroid/hardware/Sensor;

    goto :goto_0

    .line 191
    .end local v8    # "i$":Ljava/util/Iterator;
    .end local v10    # "sensor":Landroid/hardware/Sensor;
    .end local v11    # "sensors":Ljava/util/List;, "Ljava/util/List<Landroid/hardware/Sensor;>;"
    :cond_4
    const/4 v0, 0x4

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mCurSensorIndex:[I

    .line 194
    new-instance v0, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;

    invoke-direct {v0}, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mMorphoImageStitcher:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;

    .line 195
    new-instance v0, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$PanoramaInitParam;

    invoke-direct {v0}, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$PanoramaInitParam;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanoramaInitParam:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$PanoramaInitParam;

    .line 197
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mOffsetMode:I

    .line 198
    new-instance v0, Lcom/sec/android/app/camera/panorama360/SensorFusion;

    invoke-static {}, Lcom/sec/android/app/camera/panorama360/Panorama360DebugUtils;->isInDebugging()Z

    move-result v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/camera/panorama360/SensorFusion;-><init>(Z)V

    iput-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mSensorFusion:Lcom/sec/android/app/camera/panorama360/SensorFusion;

    .line 199
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mSensorFusion:Lcom/sec/android/app/camera/panorama360/SensorFusion;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/camera/panorama360/SensorFusion;->setMode(I)V

    .line 200
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mSensorFusion:Lcom/sec/android/app/camera/panorama360/SensorFusion;

    iget v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mOffsetMode:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/camera/panorama360/SensorFusion;->setOffsetMode(I)V

    .line 201
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sec/android/app/camera/Util;->getCameraOrientation(I)I

    move-result v6

    .line 203
    .local v6, "camera_orientation":I
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mSensorFusion:Lcom/sec/android/app/camera/panorama360/SensorFusion;

    invoke-virtual {v0, v6}, Lcom/sec/android/app/camera/panorama360/SensorFusion;->setRotation(I)V

    .line 204
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mSensorFusion:Lcom/sec/android/app/camera/panorama360/SensorFusion;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/camera/panorama360/SensorFusion;->setAppState(I)V

    .line 206
    iget v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mCameraWidth:I

    iget v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mCameraHeight:I

    mul-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x3

    div-int/lit8 v5, v0, 0x2

    .line 207
    .local v5, "buff_size":I
    new-instance v0, Lcom/sec/android/app/camera/panorama360/Panorama360View;

    invoke-virtual {p1}, Lcom/sec/android/app/camera/Camera;->getGLContext()Lcom/sec/android/glview/TwGLContext;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object v4, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/panorama360/Panorama360View;-><init>(Lcom/sec/android/glview/TwGLContext;FFLcom/sec/android/app/camera/panorama360/Panorama360Callback;I)V

    iput-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanorama360View:Lcom/sec/android/app/camera/panorama360/Panorama360View;

    .line 208
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mSensorFusion:Lcom/sec/android/app/camera/panorama360/SensorFusion;

    iget-object v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanorama360View:Lcom/sec/android/app/camera/panorama360/Panorama360View;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/panorama360/Panorama360View;->pvGetOrientation()I

    move-result v1

    mul-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/camera/panorama360/SensorFusion;->setInitialOrientation(I)V

    .line 210
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanorama360View:Lcom/sec/android/app/camera/panorama360/Panorama360View;

    new-instance v1, Lcom/sec/android/app/camera/panorama360/Panorama360Callback$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/camera/panorama360/Panorama360Callback$1;-><init>(Lcom/sec/android/app/camera/panorama360/Panorama360Callback;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/camera/panorama360/Panorama360View;->pvSetPanorama360ViewEventListener(Lcom/sec/android/app/camera/panorama360/Panorama360View$Panorama360ViewEventListener;)V

    .line 237
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->RES_ID_PANORAMA_GUIDE_IMAGE:[[I

    array-length v0, v0

    new-array v0, v0, [Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mGuideImage:[Landroid/graphics/Bitmap;

    .line 238
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->RES_ID_PANORAMA_GUIDE_IMAGE:[[I

    array-length v0, v0

    if-ge v7, v0, :cond_5

    .line 239
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->RES_ID_PANORAMA_GUIDE_IMAGE:[[I

    aget-object v0, v0, v7

    const/4 v1, 0x1

    aget v9, v0, v1

    .line 240
    .local v9, "res_id":I
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mGuideImage:[Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/Camera;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1, v9}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    aput-object v1, v0, v7

    .line 238
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 242
    .end local v9    # "res_id":I
    :cond_5
    invoke-direct {p0}, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->pcUpdateScreenRotation()V

    .line 244
    invoke-direct {p0}, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->checkAvailableStorage()V

    .line 245
    return-void

    .line 75
    nop

    :array_0
    .array-data 4
        0xc
        0x7f020488
    .end array-data

    :array_1
    .array-data 4
        0xd
        0x7f020486
    .end array-data

    :array_2
    .array-data 4
        0xe
        0x7f020487
    .end array-data

    :array_3
    .array-data 4
        0xf
        0x7f020484
    .end array-data

    :array_4
    .array-data 4
        0x10
        0x7f020485
    .end array-data

    :array_5
    .array-data 4
        0x11
        0x7f020482
    .end array-data

    :array_6
    .array-data 4
        0x12
        0x7f020489
    .end array-data

    :array_7
    .array-data 4
        0x13
        0x7f020483
    .end array-data

    :array_8
    .array-data 4
        0x2
        0x7f02047c
    .end array-data

    :array_9
    .array-data 4
        0x3
        0x7f020480
    .end array-data

    :array_a
    .array-data 4
        0x4
        0x7f02047e
    .end array-data

    :array_b
    .array-data 4
        0x5
        0x7f02047a
    .end array-data

    :array_c
    .array-data 4
        0x6
        0x7f02047d
    .end array-data

    :array_d
    .array-data 4
        0x7
        0x7f020481
    .end array-data

    :array_e
    .array-data 4
        0x8
        0x7f02047f
    .end array-data

    :array_f
    .array-data 4
        0x9
        0x7f02047b
    .end array-data
.end method

.method static synthetic access$000(Lcom/sec/android/app/camera/panorama360/Panorama360Callback;)[I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/panorama360/Panorama360Callback;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mStatus:[I

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/camera/panorama360/Panorama360Callback;)Lcom/sec/android/app/camera/Camera;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/panorama360/Panorama360Callback;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/camera/panorama360/Panorama360Callback;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/panorama360/Panorama360Callback;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/camera/panorama360/Panorama360Callback;)D
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/camera/panorama360/Panorama360Callback;

    .prologue
    .line 47
    iget-wide v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mImageClassifyResult:D

    return-wide v0
.end method

.method static synthetic access$302(Lcom/sec/android/app/camera/panorama360/Panorama360Callback;D)D
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/panorama360/Panorama360Callback;
    .param p1, "x1"    # D

    .prologue
    .line 47
    iput-wide p1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mImageClassifyResult:D

    return-wide p1
.end method

.method private checkAvailableStorage()V
    .locals 6

    .prologue
    .line 1397
    iget-object v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/camera/CameraSettings;->getStorage()I

    move-result v0

    .line 1398
    .local v0, "storage":I
    const-string v1, "Panorama360Callback"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "pc checkAvailableStorage : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/camera/panorama360/LogFilter;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1399
    iget v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mCurrentSaveStorage:I

    if-eq v1, v0, :cond_0

    .line 1400
    iget-object v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/Camera;->getAvailableStorage()J

    move-result-wide v2

    const-wide/32 v4, 0x1400000

    cmp-long v1, v2, v4

    if-gez v1, :cond_1

    .line 1401
    const-string v1, "Panorama360Callback"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Available storage size is not enough. -> "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v3}, Lcom/sec/android/app/camera/Camera;->getAvailableStorage()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/camera/panorama360/LogFilter;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 1402
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mIsEnoughSpace:Z

    .line 1406
    :goto_0
    iput v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mCurrentSaveStorage:I

    .line 1408
    :cond_0
    return-void

    .line 1404
    :cond_1
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mIsEnoughSpace:Z

    goto :goto_0
.end method

.method private checkCalibrationEnabled(Lcom/sec/android/app/camera/panorama360/Panorama360View$ResultInfo;)Z
    .locals 4
    .param p1, "result"    # Lcom/sec/android/app/camera/panorama360/Panorama360View$ResultInfo;

    .prologue
    .line 366
    const/4 v0, 0x0

    .line 367
    .local v0, "isCalibrationEnabled":Z
    iget-object v2, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mSensorLockObj:Ljava/lang/Object;

    monitor-enter v2

    .line 368
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/Camera;->isResetDialogActive()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/Camera;->isShootingDialogShowing()Z

    move-result v1

    if-nez v1, :cond_0

    iget v1, p1, Lcom/sec/android/app/camera/panorama360/Panorama360View$ResultInfo;->mIsStootable:I

    const/4 v3, 0x1

    if-ne v1, v3, :cond_0

    .line 369
    const/4 v0, 0x1

    .line 370
    :cond_0
    iget-boolean v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mIsCalibrationEnabled:Z

    if-eq v0, v1, :cond_1

    .line 371
    if-eqz v0, :cond_2

    .line 372
    iget-object v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mHandler:Landroid/os/Handler;

    const/16 v3, 0x103

    invoke-virtual {v1, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 375
    :goto_0
    iput-boolean v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mIsCalibrationEnabled:Z

    .line 377
    :cond_1
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 378
    const-string v1, "Panorama360Callback"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "checkCalibrationEnabled : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/camera/panorama360/LogFilter;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 379
    return v0

    .line 374
    :cond_2
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mHandler:Landroid/os/Handler;

    const/16 v3, 0x104

    invoke-virtual {v1, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 377
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method private executeFinishShootingAsync(IZ)V
    .locals 3
    .param p1, "finishtype"    # I
    .param p2, "saveimage"    # Z

    .prologue
    .line 1287
    const-string v0, "Panorama360Callback"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "pc executeFinishShootingAsync : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/camera/panorama360/LogFilter;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1288
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mFinishShootingAsync:Lcom/sec/android/app/camera/panorama360/FinishShootingAsync;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mFinishShootingAsync:Lcom/sec/android/app/camera/panorama360/FinishShootingAsync;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/panorama360/FinishShootingAsync;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-eq v0, v1, :cond_1

    .line 1289
    :cond_0
    new-instance v0, Lcom/sec/android/app/camera/panorama360/FinishShootingAsync;

    invoke-direct {v0, p0, p1, p2}, Lcom/sec/android/app/camera/panorama360/FinishShootingAsync;-><init>(Lcom/sec/android/app/camera/panorama360/Panorama360Callback;IZ)V

    iput-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mFinishShootingAsync:Lcom/sec/android/app/camera/panorama360/FinishShootingAsync;

    .line 1290
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mFinishShootingAsync:Lcom/sec/android/app/camera/panorama360/FinishShootingAsync;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/camera/panorama360/FinishShootingAsync;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 1292
    :cond_1
    return-void
.end method

.method private getInstanceProgressingPopup()Lcom/sec/android/app/camera/glwidget/TwGLProgressingPopup;
    .locals 7

    .prologue
    const/16 v2, 0xbea

    .line 1212
    const-string v1, "Panorama360Callback"

    const-string v3, "pc getInstanceProgressingPopup"

    invoke-static {v1, v3}, Lcom/sec/android/app/camera/panorama360/LogFilter;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1213
    iget-object v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mMenuResourceDepot:Lcom/sec/android/app/camera/MenuResourceDepot;

    iget-object v1, v1, Lcom/sec/android/app/camera/MenuResourceDepot;->mMenus:Ljava/util/HashMap;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/camera/glwidget/TwGLProgressingPopup;

    .line 1214
    .local v0, "progressPopup":Lcom/sec/android/app/camera/glwidget/TwGLProgressingPopup;
    if-nez v0, :cond_0

    .line 1215
    iget-object v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    if-nez v1, :cond_1

    .line 1216
    const-string v1, "Panorama360Callback"

    const-string v2, "mActivityContext is NULL!"

    invoke-static {v1, v2}, Lcom/sec/android/app/camera/panorama360/LogFilter;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 1223
    :cond_0
    :goto_0
    return-object v0

    .line 1218
    :cond_1
    new-instance v0, Lcom/sec/android/app/camera/glwidget/TwGLProgressingPopup;

    .end local v0    # "progressPopup":Lcom/sec/android/app/camera/glwidget/TwGLProgressingPopup;
    iget-object v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    iget-object v3, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v3}, Lcom/sec/android/app/camera/Camera;->getMenuRoot()Lcom/sec/android/glview/TwGLViewGroup;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mMenuResourceDepot:Lcom/sec/android/app/camera/MenuResourceDepot;

    iget-object v5, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    const v6, 0x7f0c000f

    invoke-virtual {v5, v6}, Lcom/sec/android/app/camera/Camera;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/glwidget/TwGLProgressingPopup;-><init>(Lcom/sec/android/app/camera/Camera;ILcom/sec/android/glview/TwGLViewGroup;Lcom/sec/android/app/camera/MenuResourceDepot;Ljava/lang/String;)V

    .line 1220
    .restart local v0    # "progressPopup":Lcom/sec/android/app/camera/glwidget/TwGLProgressingPopup;
    iget-object v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mMenuResourceDepot:Lcom/sec/android/app/camera/MenuResourceDepot;

    iget-object v1, v1, Lcom/sec/android/app/camera/MenuResourceDepot;->mMenus:Ljava/util/HashMap;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private pcCheckAngle([D[DDJ)Z
    .locals 9
    .param p1, "cur_angle"    # [D
    .param p2, "base_angle"    # [D
    .param p3, "boundary_angle"    # D
    .param p5, "diff_time"    # J

    .prologue
    .line 883
    const/4 v3, 0x1

    .line 884
    .local v3, "ret":Z
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v4, p2

    if-ge v2, v4, :cond_1

    .line 885
    aget-wide v4, p2, v2

    aget-wide v6, p1, v2

    sub-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    .line 886
    .local v0, "diff_angle":D
    cmpl-double v4, v0, p3

    if-lez v4, :cond_0

    .line 887
    const/4 v3, 0x0

    .line 884
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 890
    .end local v0    # "diff_angle":D
    :cond_1
    const-string v4, "Panorama360Callback"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "pc pcCheckAngle : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/camera/panorama360/LogFilter;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 891
    return v3
.end method

.method private pcIsStopPanoramaShooting(I)Z
    .locals 4
    .param p1, "attach_status"    # I

    .prologue
    .line 780
    const-string v1, "Panorama360Callback"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "pc pcIsStopPanoramaShooting : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/camera/panorama360/LogFilter;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 781
    const/4 v0, 0x0

    .line 782
    .local v0, "is_stop_shooting":Z
    packed-switch p1, :pswitch_data_0

    .line 802
    :goto_0
    :pswitch_0
    return v0

    .line 788
    :pswitch_1
    const/4 v0, 0x0

    .line 789
    goto :goto_0

    .line 796
    :pswitch_2
    const-string v1, "Panorama360Callback"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "pc pcIsStopPanoramaShooting->stop attach_status "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/camera/panorama360/LogFilter;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 797
    const/4 v0, 0x1

    .line 798
    goto :goto_0

    .line 782
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method private pcIsUseSensor()Z
    .locals 4

    .prologue
    .line 895
    const/4 v0, 0x0

    .line 896
    .local v0, "result":Z
    iget-object v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mGyroscope:Landroid/hardware/Sensor;

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mAccelerometer:Landroid/hardware/Sensor;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mMagneticField:Landroid/hardware/Sensor;

    if-nez v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mRotationVector:Landroid/hardware/Sensor;

    if-eqz v1, :cond_2

    .line 897
    :cond_1
    const/4 v0, 0x1

    .line 899
    :cond_2
    const-string v1, "Panorama360Callback"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "pc pcIsUseSensor : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/camera/panorama360/LogFilter;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 900
    return v0
.end method

.method private pcMoveToNextStateByAttachStatus(I)V
    .locals 5
    .param p1, "attach_status"    # I

    .prologue
    const/16 v4, 0x108

    const/4 v3, 0x1

    .line 806
    const-string v0, "Panorama360Callback"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "pc pcIsStopPanoramaShooting : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/camera/panorama360/LogFilter;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 807
    packed-switch p1, :pswitch_data_0

    .line 812
    :pswitch_0
    iget-boolean v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->moveToShooting:Z

    if-eqz v0, :cond_0

    .line 813
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x107

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 849
    :cond_0
    :goto_0
    :pswitch_1
    return-void

    .line 819
    :pswitch_2
    iget-boolean v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->moveToShooting:Z

    if-eqz v0, :cond_0

    .line 821
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x106

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 833
    :pswitch_3
    const-string v0, "Panorama360Callback"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "pc pcMoveToNextStateByAttachStatus ERROR "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/camera/panorama360/LogFilter;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 836
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 837
    invoke-virtual {p0, v3, v3}, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->pcRestartShooting(ZZ)V

    goto :goto_0

    .line 841
    :pswitch_4
    const-string v0, "Panorama360Callback"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "pc pcMoveToNextStateByAttachStatus STATUS_GUIDE_ENDED "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/camera/panorama360/LogFilter;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 842
    iput-boolean v3, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mAllGuideTaken:Z

    .line 845
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 846
    invoke-virtual {p0, v3, v3}, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->pcRestartShooting(ZZ)V

    goto :goto_0

    .line 807
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_3
        :pswitch_3
        :pswitch_1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method private pcPanoramaPreview([B)V
    .locals 30
    .param p1, "cameraOutputRaw"    # [B

    .prologue
    .line 606
    const-string v4, "Panorama360Callback"

    const-string v5, "pc pcPanoramaPreview"

    invoke-static {v4, v5}, Lcom/sec/android/app/camera/panorama360/LogFilter;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 608
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v24

    .line 609
    .local v24, "now_time":J
    const-wide/16 v16, 0x0

    .line 610
    .local v16, "frame_interval":J
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->pre_frame_time:J

    const-wide/16 v10, 0x0

    cmp-long v4, v4, v10

    if-eqz v4, :cond_0

    .line 611
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->pre_frame_time:J

    sub-long v16, v24, v4

    .line 613
    :cond_0
    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->pre_frame_time:J

    .line 615
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanoramaPreviewCount:I

    if-nez v4, :cond_3

    const/16 v20, 0x1

    .line 616
    .local v20, "is_first":Z
    :goto_0
    if-eqz v20, :cond_1

    .line 617
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mStatus:[I

    const/4 v5, 0x0

    const/4 v6, 0x0

    aput v6, v4, v5

    .line 619
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mSensorLockObj:Ljava/lang/Object;

    move-object/from16 v27, v0

    monitor-enter v27

    .line 620
    const/4 v7, 0x0

    .line 621
    .local v7, "g_mat":[D
    const/4 v8, 0x0

    .line 622
    .local v8, "rv_mat":[D
    const/4 v9, 0x0

    .line 623
    .local v9, "ac_mat":[D
    :try_start_0
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->pcIsUseSensor()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 624
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mSensorFusion:Lcom/sec/android/app/camera/panorama360/SensorFusion;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mGyroMatrix:[D

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mRVMatrix:[D

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mACMatrix:[D

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mCurSensorIndex:[I

    invoke-virtual {v4, v5, v6, v10, v11}, Lcom/sec/android/app/camera/panorama360/SensorFusion;->getSensorMatrix([D[D[D[I)I

    .line 625
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mGyroMatrix:[D

    .line 626
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mRVMatrix:[D

    .line 627
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mACMatrix:[D

    .line 628
    invoke-static {}, Lcom/sec/android/app/camera/panorama360/Panorama360DebugUtils;->isInSensorDebugging()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 629
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mCurSensorIndex:[I

    const/4 v5, 0x0

    aget v4, v4, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mCurSensorIndex:[I

    const/4 v6, 0x1

    aget v5, v5, v6

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanoramaProcessCount:I

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/camera/panorama360/Panorama360DebugUtils;->addSensorInfo(III)V

    .line 632
    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanorama360View:Lcom/sec/android/app/camera/panorama360/Panorama360View;

    const/4 v6, 0x0

    const/4 v10, -0x1

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object/from16 v5, p1

    invoke-virtual/range {v4 .. v12}, Lcom/sec/android/app/camera/panorama360/Panorama360View;->pvSetRenderInfo([BLjava/util/ArrayList;[D[D[DIILjava/lang/String;)V

    .line 634
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanorama360View:Lcom/sec/android/app/camera/panorama360/Panorama360View;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/sec/android/app/camera/panorama360/Panorama360View;->pvSetDrawBgBlankMode(I)V

    .line 636
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mIsCalibrationEnabled:Z

    if-eqz v4, :cond_e

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mWaitTime:I

    const/16 v5, 0xbb8

    if-ge v4, v5, :cond_e

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mWaitTime:I

    if-ltz v4, :cond_e

    .line 637
    if-eqz v7, :cond_e

    .line 639
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mWaitTime:I

    int-to-double v4, v4

    const-wide v10, 0x4092c00000000000L    # 1200.0

    cmpg-double v4, v4, v10

    if-gez v4, :cond_4

    .line 640
    sget-wide v14, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->BOUNDARY_APS_FOR_EXTRA_TIME:D

    .line 644
    .local v14, "boundary_angle":D
    :goto_1
    const/16 v21, 0x1

    .line 646
    .local v21, "is_stop":Z
    const/4 v4, 0x3

    new-array v13, v4, [D

    .line 647
    .local v13, "base_angle":[D
    const/4 v4, 0x3

    new-array v12, v4, [D

    .line 648
    .local v12, "cur_angle_ave":[D
    const-wide/16 v4, 0x0

    cmp-long v4, v16, v4

    if-lez v4, :cond_a

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mGyroscopeValueNumPerFrame:I

    if-lez v4, :cond_a

    .line 649
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mCurGyroscopeAngleIndex:I

    add-int/lit8 v4, v4, 0x1

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mCurGyroscopeAngleValueHist:[[D

    array-length v5, v5

    if-ge v4, v5, :cond_5

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mCurGyroscopeAngleIndex:I

    add-int/lit8 v26, v4, 0x1

    .line 650
    .local v26, "use_hist_num":I
    :goto_2
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mCurGyroscopeAngleIndex:I

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mCurGyroscopeAngleValueHist:[[D

    array-length v5, v5

    rem-int v18, v4, v5

    .line 652
    .local v18, "hist_ix":I
    const/16 v19, 0x0

    .local v19, "i":I
    :goto_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mCurGyroscopeAngleValue:[D

    array-length v4, v4

    move/from16 v0, v19

    if-ge v0, v4, :cond_6

    .line 653
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mCurGyroscopeAngleValueHist:[[D

    aget-object v4, v4, v18

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mCurGyroscopeAngleValue:[D

    aget-wide v10, v5, v19

    const-wide v28, 0x41cdcd6500000000L    # 1.0E9

    mul-double v10, v10, v28

    move-wide/from16 v0, v16

    long-to-double v0, v0

    move-wide/from16 v28, v0

    div-double v10, v10, v28

    aput-wide v10, v4, v19

    .line 652
    add-int/lit8 v19, v19, 0x1

    goto :goto_3

    .line 615
    .end local v7    # "g_mat":[D
    .end local v8    # "rv_mat":[D
    .end local v9    # "ac_mat":[D
    .end local v12    # "cur_angle_ave":[D
    .end local v13    # "base_angle":[D
    .end local v14    # "boundary_angle":D
    .end local v18    # "hist_ix":I
    .end local v19    # "i":I
    .end local v20    # "is_first":Z
    .end local v21    # "is_stop":Z
    .end local v26    # "use_hist_num":I
    :cond_3
    const/16 v20, 0x0

    goto/16 :goto_0

    .line 642
    .restart local v7    # "g_mat":[D
    .restart local v8    # "rv_mat":[D
    .restart local v9    # "ac_mat":[D
    .restart local v20    # "is_first":Z
    :cond_4
    sget-wide v14, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->BOUNDARY_APS_FOR_CALC_OFFSET:D

    .restart local v14    # "boundary_angle":D
    goto :goto_1

    .line 649
    .restart local v12    # "cur_angle_ave":[D
    .restart local v13    # "base_angle":[D
    .restart local v21    # "is_stop":Z
    :cond_5
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mCurGyroscopeAngleValueHist:[[D

    array-length v0, v4

    move/from16 v26, v0

    goto :goto_2

    .line 656
    .restart local v18    # "hist_ix":I
    .restart local v19    # "i":I
    .restart local v26    # "use_hist_num":I
    :cond_6
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mTotalGyroscopeValueNum:I

    if-lez v4, :cond_9

    .line 657
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mTotalGyroscopeValue:[D

    const/4 v6, 0x0

    aget-wide v10, v5, v6

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mTotalGyroscopeValueNum:I

    int-to-double v0, v5

    move-wide/from16 v28, v0

    div-double v10, v10, v28

    aput-wide v10, v13, v4

    .line 658
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mTotalGyroscopeValue:[D

    const/4 v6, 0x1

    aget-wide v10, v5, v6

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mTotalGyroscopeValueNum:I

    int-to-double v0, v5

    move-wide/from16 v28, v0

    div-double v10, v10, v28

    aput-wide v10, v13, v4

    .line 659
    const/4 v4, 0x2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mTotalGyroscopeValue:[D

    const/4 v6, 0x2

    aget-wide v10, v5, v6

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mTotalGyroscopeValueNum:I

    int-to-double v0, v5

    move-wide/from16 v28, v0

    div-double v10, v10, v28

    aput-wide v10, v13, v4

    .line 661
    const/16 v19, 0x0

    :goto_4
    move/from16 v0, v19

    move/from16 v1, v26

    if-ge v0, v1, :cond_8

    .line 662
    const/16 v22, 0x0

    .local v22, "j":I
    :goto_5
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mCurGyroscopeAngleValueHist:[[D

    aget-object v4, v4, v19

    array-length v4, v4

    move/from16 v0, v22

    if-ge v0, v4, :cond_7

    .line 663
    aget-wide v4, v12, v22

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mCurGyroscopeAngleValueHist:[[D

    aget-object v6, v6, v19

    aget-wide v10, v6, v22

    add-double/2addr v4, v10

    aput-wide v4, v12, v22

    .line 662
    add-int/lit8 v22, v22, 0x1

    goto :goto_5

    .line 661
    :cond_7
    add-int/lit8 v19, v19, 0x1

    goto :goto_4

    .line 666
    .end local v22    # "j":I
    :cond_8
    const/4 v4, 0x0

    aget-wide v10, v12, v4

    move/from16 v0, v26

    int-to-double v0, v0

    move-wide/from16 v28, v0

    div-double v10, v10, v28

    aput-wide v10, v12, v4

    .line 667
    const/4 v4, 0x1

    aget-wide v10, v12, v4

    move/from16 v0, v26

    int-to-double v0, v0

    move-wide/from16 v28, v0

    div-double v10, v10, v28

    aput-wide v10, v12, v4

    .line 668
    const/4 v4, 0x2

    aget-wide v10, v12, v4

    move/from16 v0, v26

    int-to-double v0, v0

    move-wide/from16 v28, v0

    div-double v10, v10, v28

    aput-wide v10, v12, v4

    move-object/from16 v11, p0

    .line 669
    invoke-direct/range {v11 .. v17}, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->pcCheckAngle([D[DDJ)Z

    move-result v21

    .line 671
    :cond_9
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mTotalGyroscopeValue:[D

    const/4 v5, 0x0

    aget-wide v10, v4, v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mCurGyroscopeAngleValueHist:[[D

    aget-object v6, v6, v18

    const/16 v28, 0x0

    aget-wide v28, v6, v28

    add-double v10, v10, v28

    aput-wide v10, v4, v5

    .line 672
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mTotalGyroscopeValue:[D

    const/4 v5, 0x1

    aget-wide v10, v4, v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mCurGyroscopeAngleValueHist:[[D

    aget-object v6, v6, v18

    const/16 v28, 0x1

    aget-wide v28, v6, v28

    add-double v10, v10, v28

    aput-wide v10, v4, v5

    .line 673
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mTotalGyroscopeValue:[D

    const/4 v5, 0x2

    aget-wide v10, v4, v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mCurGyroscopeAngleValueHist:[[D

    aget-object v6, v6, v18

    const/16 v28, 0x2

    aget-wide v28, v6, v28

    add-double v10, v10, v28

    aput-wide v10, v4, v5

    .line 674
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mTotalGyroscopeValueNum:I

    add-int/lit8 v4, v4, 0x1

    move-object/from16 v0, p0

    iput v4, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mTotalGyroscopeValueNum:I

    .line 675
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mCurGyroscopeAngleValue:[D

    const/4 v5, 0x0

    const-wide/16 v10, 0x0

    aput-wide v10, v4, v5

    .line 676
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mCurGyroscopeAngleValue:[D

    const/4 v5, 0x1

    const-wide/16 v10, 0x0

    aput-wide v10, v4, v5

    .line 677
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mCurGyroscopeAngleValue:[D

    const/4 v5, 0x2

    const-wide/16 v10, 0x0

    aput-wide v10, v4, v5

    .line 678
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mCurGyroscopeAngleIndex:I

    add-int/lit8 v4, v4, 0x1

    move-object/from16 v0, p0

    iput v4, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mCurGyroscopeAngleIndex:I

    .line 680
    .end local v18    # "hist_ix":I
    .end local v19    # "i":I
    .end local v26    # "use_hist_num":I
    :cond_a
    if-nez v21, :cond_b

    .line 681
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->pcResetShootingWaitProcess()V

    .line 683
    :cond_b
    if-eqz v21, :cond_e

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mIsCalibrationEnabled:Z

    if-eqz v4, :cond_e

    .line 684
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mImageClassifyBmp:Landroid/graphics/Bitmap;

    if-nez v4, :cond_c

    .line 685
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mCameraWidth:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mCameraHeight:I

    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v4, v5, v6}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mImageClassifyBmp:Landroid/graphics/Bitmap;

    .line 687
    :cond_c
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->doImageClassify:Z

    if-eqz v4, :cond_e

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mImageClassifyThread:Lcom/sec/android/app/camera/panorama360/Panorama360Callback$ImageClassifyThread;

    if-eqz v4, :cond_d

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mImageClassifyThread:Lcom/sec/android/app/camera/panorama360/Panorama360Callback$ImageClassifyThread;

    invoke-virtual {v4}, Lcom/sec/android/app/camera/panorama360/Panorama360Callback$ImageClassifyThread;->getState()Ljava/lang/Thread$State;

    move-result-object v4

    sget-object v5, Ljava/lang/Thread$State;->TERMINATED:Ljava/lang/Thread$State;

    if-ne v4, v5, :cond_e

    .line 689
    :cond_d
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->doImageClassify:Z

    .line 690
    new-instance v4, Lcom/sec/android/app/camera/panorama360/Panorama360Callback$ImageClassifyThread;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mImageClassifyBmp:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mMorphoImageStitcher:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v5, v6}, Lcom/sec/android/app/camera/panorama360/Panorama360Callback$ImageClassifyThread;-><init>(Lcom/sec/android/app/camera/panorama360/Panorama360Callback;Landroid/graphics/Bitmap;Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mImageClassifyThread:Lcom/sec/android/app/camera/panorama360/Panorama360Callback$ImageClassifyThread;

    .line 691
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mImageClassifyThread:Lcom/sec/android/app/camera/panorama360/Panorama360Callback$ImageClassifyThread;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mCameraWidth:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mCameraHeight:I

    const-string v10, "YVU420_SEMIPLANAR"

    move-object/from16 v0, p1

    invoke-virtual {v4, v0, v5, v6, v10}, Lcom/sec/android/app/camera/panorama360/Panorama360Callback$ImageClassifyThread;->setInputData([BIILjava/lang/String;)V

    .line 692
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mImageClassifyThread:Lcom/sec/android/app/camera/panorama360/Panorama360Callback$ImageClassifyThread;

    invoke-virtual {v4}, Lcom/sec/android/app/camera/panorama360/Panorama360Callback$ImageClassifyThread;->start()V

    .line 697
    .end local v12    # "cur_angle_ave":[D
    .end local v13    # "base_angle":[D
    .end local v14    # "boundary_angle":D
    .end local v21    # "is_stop":Z
    :cond_e
    monitor-exit v27
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 699
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->moveToShooting:Z

    if-eqz v4, :cond_11

    .line 701
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mImageClassifyResult:D

    const-wide/16 v10, 0x0

    cmpl-double v4, v4, v10

    if-nez v4, :cond_12

    .line 702
    const-string v4, "Panorama360Callback"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "pc pcPanoramaPreview SCENE_OUTDOOR "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mImageClassifyResult:D

    invoke-virtual {v5, v10, v11}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/camera/panorama360/LogFilter;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 703
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mMorphoImageStitcher:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;->setScene(I)I

    move-result v23

    .line 704
    .local v23, "ret":I
    if-eqz v23, :cond_f

    .line 705
    const-string v4, "Panorama360Callback"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mMorphoImageStitcher.setScene error ret:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v23

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/camera/panorama360/LogFilter;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 707
    :cond_f
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mMorphoImageStitcher:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;

    const/4 v5, 0x5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;->setGuideType(I)I

    move-result v23

    .line 708
    if-eqz v23, :cond_10

    .line 709
    const-string v4, "Panorama360Callback"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mMorphoImageStitcher.setGuideType error ret:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v23

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/camera/panorama360/LogFilter;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 722
    :cond_10
    :goto_6
    const/4 v4, 0x2

    move-object/from16 v0, p0

    iput v4, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanoramaState:I

    .line 723
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v4}, Lcom/sec/android/app/camera/Camera;->pauseAudioPlayback()V

    .line 725
    .end local v23    # "ret":I
    :cond_11
    return-void

    .line 697
    :catchall_0
    move-exception v4

    :try_start_1
    monitor-exit v27
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4

    .line 712
    :cond_12
    const-string v4, "Panorama360Callback"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "pc pcPanoramaPreview SCENE_INDOOR "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mImageClassifyResult:D

    invoke-virtual {v5, v10, v11}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 713
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mMorphoImageStitcher:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;->setScene(I)I

    move-result v23

    .line 714
    .restart local v23    # "ret":I
    if-eqz v23, :cond_13

    .line 715
    const-string v4, "Panorama360Callback"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mMorphoImageStitcher.setScene error ret:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v23

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/camera/panorama360/LogFilter;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 717
    :cond_13
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mMorphoImageStitcher:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;

    const/4 v5, 0x4

    invoke-virtual {v4, v5}, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;->setGuideType(I)I

    move-result v23

    .line 718
    if-eqz v23, :cond_10

    .line 719
    const-string v4, "Panorama360Callback"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mMorphoImageStitcher.setGuideType error ret:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v23

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/camera/panorama360/LogFilter;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_6
.end method

.method private pcPanoramaProcess([B)V
    .locals 14
    .param p1, "cameraOutputRaw"    # [B

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 729
    iget v2, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanoramaProcessCount:I

    if-nez v2, :cond_6

    move v10, v0

    .line 730
    .local v10, "is_first":Z
    :goto_0
    const-string v2, "Panorama360Callback"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "pc pcPanoramaProcess start >>>>>> : "

    invoke-virtual {v7, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v2, v7}, Lcom/sec/android/app/camera/panorama360/LogFilter;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 731
    if-eqz v10, :cond_1

    .line 732
    iget-object v2, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanorama360View:Lcom/sec/android/app/camera/panorama360/Panorama360View;

    invoke-virtual {v2, v0}, Lcom/sec/android/app/camera/panorama360/Panorama360View;->pvSetDrawBgBlankMode(I)V

    .line 733
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mMorphoImageStitcher:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;->end()I

    move-result v11

    .line 734
    .local v11, "ret":I
    if-eqz v11, :cond_0

    .line 735
    const-string v0, "Panorama360Callback"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "mMorphoImageStitcher.start error ret:"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/android/app/camera/panorama360/LogFilter;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 737
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mMorphoImageStitcher:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;->start(I)I

    move-result v11

    .line 738
    if-eqz v11, :cond_1

    .line 739
    const-string v0, "Panorama360Callback"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mMorphoImageStitcher.start error ret:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/camera/panorama360/LogFilter;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 743
    .end local v11    # "ret":I
    :cond_1
    const/4 v6, 0x0

    .line 744
    .local v6, "use_image":I
    iget-object v12, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mSensorLockObj:Ljava/lang/Object;

    monitor-enter v12

    .line 745
    const/4 v8, 0x0

    .line 746
    .local v8, "path":Ljava/lang/String;
    const/4 v3, 0x0

    .line 747
    .local v3, "g_mat":[D
    const/4 v4, 0x0

    .line 748
    .local v4, "rv_mat":[D
    const/4 v5, 0x0

    .line 749
    .local v5, "ac_mat":[D
    :try_start_0
    invoke-direct {p0}, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->pcIsUseSensor()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 750
    if-eqz v10, :cond_3

    .line 751
    invoke-static {}, Lcom/sec/android/app/camera/panorama360/Panorama360DebugUtils;->isInSensorDebugging()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 752
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mDateTaken:[J

    const/4 v1, 0x0

    aget-wide v0, v0, v1

    invoke-static {v0, v1}, Lcom/sec/android/app/camera/panorama360/Panorama360DebugUtils;->getPreviewPath(J)Ljava/lang/String;

    move-result-object v9

    .line 753
    .local v9, "dir_path":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mSensorFusion:Lcom/sec/android/app/camera/panorama360/SensorFusion;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/panorama360/SensorFusion;->getStockData()[Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v9, v0}, Lcom/sec/android/app/camera/panorama360/Panorama360DebugUtils;->startSavePreviewSDThread(Ljava/lang/String;[Ljava/util/ArrayList;)V

    .line 755
    .end local v9    # "dir_path":Ljava/lang/String;
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mSensorFusion:Lcom/sec/android/app/camera/panorama360/SensorFusion;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/camera/panorama360/SensorFusion;->setMode(I)V

    .line 758
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mSensorFusion:Lcom/sec/android/app/camera/panorama360/SensorFusion;

    iget-object v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mGyroMatrix:[D

    iget-object v2, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mRVMatrix:[D

    iget-object v7, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mACMatrix:[D

    iget-object v13, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mCurSensorIndex:[I

    invoke-virtual {v0, v1, v2, v7, v13}, Lcom/sec/android/app/camera/panorama360/SensorFusion;->getSensorMatrix([D[D[D[I)I

    .line 759
    invoke-static {}, Lcom/sec/android/app/camera/panorama360/Panorama360DebugUtils;->isInSensorDebugging()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 760
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mCurSensorIndex:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    iget-object v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mCurSensorIndex:[I

    const/4 v2, 0x1

    aget v1, v1, v2

    iget v2, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanoramaProcessCount:I

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/camera/panorama360/Panorama360DebugUtils;->addSensorInfo(III)V

    .line 761
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mDateTaken:[J

    const/4 v1, 0x0

    aget-wide v0, v0, v1

    invoke-static {v0, v1}, Lcom/sec/android/app/camera/panorama360/Panorama360DebugUtils;->getFilePathFromSensorInfo(J)Ljava/lang/String;

    move-result-object v8

    .line 763
    :cond_4
    iget-object v3, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mGyroMatrix:[D

    .line 764
    iget-object v4, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mRVMatrix:[D

    .line 765
    iget-object v5, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mACMatrix:[D

    .line 767
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mStatus:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    invoke-direct {p0, v0}, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->pcIsStopPanoramaShooting(I)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 768
    monitor-exit v12

    .line 777
    :goto_1
    return-void

    .end local v3    # "g_mat":[D
    .end local v4    # "rv_mat":[D
    .end local v5    # "ac_mat":[D
    .end local v6    # "use_image":I
    .end local v8    # "path":Ljava/lang/String;
    .end local v10    # "is_first":Z
    :cond_6
    move v10, v1

    .line 729
    goto/16 :goto_0

    .line 770
    .restart local v3    # "g_mat":[D
    .restart local v4    # "rv_mat":[D
    .restart local v5    # "ac_mat":[D
    .restart local v6    # "use_image":I
    .restart local v8    # "path":Ljava/lang/String;
    .restart local v10    # "is_first":Z
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanorama360View:Lcom/sec/android/app/camera/panorama360/Panorama360View;

    const/4 v2, 0x0

    iget v7, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanoramaProcessCount:I

    move-object v1, p1

    invoke-virtual/range {v0 .. v8}, Lcom/sec/android/app/camera/panorama360/Panorama360View;->pvSetRenderInfo([BLjava/util/ArrayList;[D[D[DIILjava/lang/String;)V

    .line 772
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanorama360View:Lcom/sec/android/app/camera/panorama360/Panorama360View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/camera/panorama360/Panorama360View;->pvSetDrawBgBlankMode(I)V

    .line 774
    monitor-exit v12
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 776
    const-string v0, "Panorama360Callback"

    const-string v1, "<<<<<< pc pcPanoramaProcess end"

    invoke-static {v0, v1}, Lcom/sec/android/app/camera/panorama360/LogFilter;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 774
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v12
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private pcRestartToStartupPreview(Z)V
    .locals 3
    .param p1, "save_image"    # Z

    .prologue
    .line 852
    const-string v0, "Panorama360Callback"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "pc pcRestartToStartupPreview save_image: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/camera/panorama360/LogFilter;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 853
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->executeFinishShootingAsync(IZ)V

    .line 854
    return-void
.end method

.method private pcUpdateScreenRotation()V
    .locals 7

    .prologue
    .line 434
    new-instance v1, Landroid/hardware/Camera$CameraInfo;

    invoke-direct {v1}, Landroid/hardware/Camera$CameraInfo;-><init>()V

    .line 435
    .local v1, "info":Landroid/hardware/Camera$CameraInfo;
    const/4 v4, 0x0

    invoke-static {v4, v1}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    .line 436
    const-string v4, "Panorama360Callback"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "pc pcUpdateScreenRotation old: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mCameraOrientation:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " new: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v1, Landroid/hardware/Camera$CameraInfo;->orientation:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/camera/panorama360/LogFilter;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 437
    iget v4, v1, Landroid/hardware/Camera$CameraInfo;->orientation:I

    iput v4, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mCameraOrientation:I

    .line 439
    iget-object v4, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mSensorFusion:Lcom/sec/android/app/camera/panorama360/SensorFusion;

    if-eqz v4, :cond_0

    .line 440
    const/4 v3, 0x1

    .line 441
    .local v3, "rotation":I
    iget v4, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mCameraOrientation:I

    sparse-switch v4, :sswitch_data_0

    .line 455
    :goto_0
    iget-object v4, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mSensorFusion:Lcom/sec/android/app/camera/panorama360/SensorFusion;

    invoke-virtual {v4, v3}, Lcom/sec/android/app/camera/panorama360/SensorFusion;->setRotation(I)V

    .line 457
    .end local v3    # "rotation":I
    :cond_0
    const/4 v0, 0x0

    .line 458
    .local v0, "degrees":I
    iget-object v4, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v4}, Lcom/sec/android/app/camera/Camera;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v4

    invoke-interface {v4}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/Display;->getRotation()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 472
    :goto_1
    iget v4, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mCameraOrientation:I

    sub-int v4, v0, v4

    add-int/lit16 v4, v4, 0x168

    rem-int/lit16 v2, v4, 0x168

    .line 473
    .local v2, "preview_rotation":I
    const-string v4, "Panorama360Callback"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "camera:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mCameraOrientation:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " disp:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " preview:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/camera/panorama360/LogFilter;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 474
    iget-object v4, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanorama360View:Lcom/sec/android/app/camera/panorama360/Panorama360View;

    invoke-virtual {v4, v2}, Lcom/sec/android/app/camera/panorama360/Panorama360View;->pvSetPreviewRotation(I)V

    .line 475
    return-void

    .line 443
    .end local v0    # "degrees":I
    .end local v2    # "preview_rotation":I
    .restart local v3    # "rotation":I
    :sswitch_0
    const/4 v3, 0x0

    .line 444
    goto :goto_0

    .line 446
    :sswitch_1
    const/4 v3, 0x1

    .line 447
    goto :goto_0

    .line 449
    :sswitch_2
    const/4 v3, 0x2

    .line 450
    goto :goto_0

    .line 452
    :sswitch_3
    const/4 v3, 0x3

    goto :goto_0

    .line 460
    .end local v3    # "rotation":I
    .restart local v0    # "degrees":I
    :pswitch_0
    const/4 v0, 0x0

    .line 461
    goto :goto_1

    .line 463
    :pswitch_1
    const/16 v0, 0x5a

    .line 464
    goto :goto_1

    .line 466
    :pswitch_2
    const/16 v0, 0xb4

    .line 467
    goto :goto_1

    .line 469
    :pswitch_3
    const/16 v0, 0x10e

    goto :goto_1

    .line 441
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x5a -> :sswitch_1
        0xb4 -> :sswitch_2
        0x10e -> :sswitch_3
    .end sparse-switch

    .line 458
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private setCenterGuideAnimationProgress(I)V
    .locals 3
    .param p1, "waittime"    # I

    .prologue
    .line 1043
    invoke-direct {p0, p1}, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->toCenterGuideAnimationProgress(I)I

    move-result v0

    .line 1044
    .local v0, "progress":I
    iget v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPreviousCenterGuideProgress:I

    if-eq v1, v0, :cond_0

    .line 1045
    iget-object v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanorama360View:Lcom/sec/android/app/camera/panorama360/Panorama360View;

    const/4 v2, 0x1

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/camera/panorama360/Panorama360View;->pvSetAnimationInfo(II)V

    .line 1046
    iput v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPreviousCenterGuideProgress:I

    .line 1048
    :cond_0
    return-void
.end method

.method private toCenterGuideAnimationProgress(I)I
    .locals 10
    .param p1, "waittime"    # I

    .prologue
    const/16 v5, 0x71c

    const/16 v4, 0x49c

    const/16 v3, 0x280

    const-wide/high16 v8, 0x4084000000000000L    # 640.0

    const-wide v6, 0x405a200000000000L    # 104.5

    .line 1019
    if-gt p1, v3, :cond_0

    .line 1020
    iget v3, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mWaitTime:I

    int-to-double v4, v3

    mul-double/2addr v4, v6

    div-double v0, v4, v8

    .line 1021
    .local v0, "d_progress":D
    mul-double v4, v0, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->round(D)J

    move-result-wide v4

    long-to-int v2, v4

    .line 1033
    .end local v0    # "d_progress":D
    .local v2, "progress":I
    :goto_0
    return v2

    .line 1022
    .end local v2    # "progress":I
    :cond_0
    if-ge v3, p1, :cond_1

    if-gt p1, v4, :cond_1

    .line 1023
    const/16 v2, 0x2aaa

    .restart local v2    # "progress":I
    goto :goto_0

    .line 1024
    .end local v2    # "progress":I
    :cond_1
    if-ge v4, p1, :cond_2

    if-gt p1, v5, :cond_2

    .line 1025
    iget v3, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mWaitTime:I

    add-int/lit16 v3, v3, -0x49c

    int-to-double v4, v3

    mul-double/2addr v4, v6

    div-double v0, v4, v8

    .line 1026
    .restart local v0    # "d_progress":D
    mul-double v4, v0, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->round(D)J

    move-result-wide v4

    long-to-int v3, v4

    add-int/lit16 v2, v3, 0x2aaa

    .restart local v2    # "progress":I
    goto :goto_0

    .line 1027
    .end local v0    # "d_progress":D
    .end local v2    # "progress":I
    :cond_2
    if-ge v5, p1, :cond_3

    const/16 v3, 0x938

    if-gt p1, v3, :cond_3

    .line 1028
    const/16 v2, 0x5555

    .restart local v2    # "progress":I
    goto :goto_0

    .line 1030
    .end local v2    # "progress":I
    :cond_3
    iget v3, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mWaitTime:I

    add-int/lit16 v3, v3, -0x938

    int-to-double v4, v3

    mul-double/2addr v4, v6

    div-double v0, v4, v8

    .line 1031
    .restart local v0    # "d_progress":D
    mul-double v4, v0, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->round(D)J

    move-result-wide v4

    long-to-int v3, v4

    add-int/lit16 v2, v3, 0x5555

    .restart local v2    # "progress":I
    goto :goto_0
.end method


# virtual methods
.method public attach(Lcom/sec/android/app/camera/panorama360/Panorama360View$RenderInfo;Lcom/sec/android/app/camera/panorama360/Panorama360View$ResultInfo;[I[I)I
    .locals 7
    .param p1, "info"    # Lcom/sec/android/app/camera/panorama360/Panorama360View$RenderInfo;
    .param p2, "result"    # Lcom/sec/android/app/camera/panorama360/Panorama360View$ResultInfo;
    .param p3, "attachStatus"    # [I
    .param p4, "isShootable"    # [I

    .prologue
    .line 1180
    const-string v4, "Panorama360Callback"

    const-string v5, "attach"

    invoke-static {v4, v5}, Lcom/sec/android/app/camera/panorama360/LogFilter;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1182
    const/4 v4, 0x1

    new-array v0, v4, [I

    .line 1184
    .local v0, "image_id":[I
    invoke-virtual {p0, p1}, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->setAngleMatrix(Lcom/sec/android/app/camera/panorama360/Panorama360View$RenderInfo;)V

    .line 1186
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 1188
    .local v2, "s_time":J
    iget-object v4, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mMorphoImageStitcher:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;

    iget-object v5, p1, Lcom/sec/android/app/camera/panorama360/Panorama360View$RenderInfo;->image:[B

    iget v6, p1, Lcom/sec/android/app/camera/panorama360/Panorama360View$RenderInfo;->use_image:I

    invoke-virtual {v4, v5, v6, v0, p3}, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;->attach([BI[I[I)I

    move-result v1

    .line 1190
    .local v1, "ret":I
    const/4 v4, 0x0

    aget v4, v0, v4

    iput v4, p2, Lcom/sec/android/app/camera/panorama360/Panorama360View$ResultInfo;->mImageID:I

    .line 1191
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long/2addr v4, v2

    iput-wide v4, p2, Lcom/sec/android/app/camera/panorama360/Panorama360View$ResultInfo;->mAttachTime:J

    .line 1192
    iget v4, p2, Lcom/sec/android/app/camera/panorama360/Panorama360View$ResultInfo;->mImageID:I

    if-lez v4, :cond_0

    .line 1193
    iget v4, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mShootingNum:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mShootingNum:I

    .line 1194
    const-string v4, "Panorama360Callback"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "pvRenderPreview mShootingNum "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mShootingNum:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/camera/panorama360/LogFilter;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1197
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mMorphoImageStitcher:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;

    invoke-virtual {v4, p4}, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;->getIsShootable([I)I

    move-result v1

    .line 1198
    return v1
.end method

.method public clearStockData()V
    .locals 3

    .prologue
    .line 1476
    const-string v1, "Panorama360Callback"

    const-string v2, "pc clearStockData"

    invoke-static {v1, v2}, Lcom/sec/android/app/camera/panorama360/LogFilter;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1477
    iget-object v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mSensorFusion:Lcom/sec/android/app/camera/panorama360/SensorFusion;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/panorama360/SensorFusion;->clearStockData()V

    .line 1478
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mCurSensorIndex:[I

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 1479
    iget-object v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mCurSensorIndex:[I

    const/4 v2, 0x0

    aput v2, v1, v0

    .line 1478
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1481
    :cond_0
    return-void
.end method

.method public createExifData(JLandroid/graphics/Rect;Landroid/location/Location;)Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$ExifData;
    .locals 5
    .param p1, "date"    # J
    .param p3, "rect"    # Landroid/graphics/Rect;
    .param p4, "location"    # Landroid/location/Location;

    .prologue
    .line 911
    const-string v1, "Panorama360Callback"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "pc createExifData : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p3}, Landroid/graphics/Rect;->width()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "x"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p3}, Landroid/graphics/Rect;->hashCode()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/camera/panorama360/LogFilter;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 912
    new-instance v0, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$ExifData;

    iget-object v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/Camera;->getEngine()Lcom/sec/android/app/camera/CommonEngine;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/camera/CommonEngine;->getUniqueID()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$ExifData;-><init>(Ljava/lang/String;)V

    .line 913
    .local v0, "exifData":Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$ExifData;
    iget-object v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/Camera;->getEngine()Lcom/sec/android/app/camera/CommonEngine;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/camera/CommonEngine;->getMaxaperture()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$ExifData;->setMaxApertureValue([I)V

    .line 914
    iget-object v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/Camera;->getEngine()Lcom/sec/android/app/camera/CommonEngine;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/camera/CommonEngine;->getFocalLength()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$ExifData;->setFocalLength([I)V

    .line 915
    invoke-virtual {v0, p3}, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$ExifData;->setImageSize(Landroid/graphics/Rect;)V

    .line 916
    const-string v1, "SAMSUNG"

    iput-object v1, v0, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$ExifData;->Maker:Ljava/lang/String;

    .line 917
    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    iput-object v1, v0, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$ExifData;->Model:Ljava/lang/String;

    .line 918
    sget-object v1, Landroid/os/Build;->BOOTLOADER:Ljava/lang/String;

    iput-object v1, v0, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$ExifData;->Software:Ljava/lang/String;

    .line 919
    const/4 v1, 0x4

    new-array v1, v1, [B

    fill-array-data v1, :array_0

    iput-object v1, v0, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$ExifData;->ExifVersion:[B

    .line 920
    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$ExifData;->setDateTime(J)V

    .line 921
    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$ExifData;->setDateTimeOriginal(J)V

    .line 922
    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$ExifData;->setDateTimeDigitized(J)V

    .line 923
    const/4 v1, 0x1

    iput v1, v0, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$ExifData;->ColorSpace:I

    .line 924
    invoke-virtual {v0, p4}, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$ExifData;->setLocation(Landroid/location/Location;)V

    .line 925
    return-object v0

    .line 919
    :array_0
    .array-data 1
        0x30t
        0x32t
        0x32t
        0x30t
    .end array-data
.end method

.method public getSaveFileName()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1385
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mDateTaken:[J

    const/4 v2, 0x0

    aget-wide v2, v1, v2

    invoke-static {v2, v3}, Lcom/sec/android/app/camera/ImageSavingUtils;->createName(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".jpg"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSavePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1380
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getEngine()Lcom/sec/android/app/camera/CommonEngine;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CommonEngine;->checkSaveDirectory()V

    .line 1381
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->getStorage()I

    move-result v0

    invoke-static {v0}, Lcom/sec/android/app/camera/ImageSavingUtils;->getImageSavingDir(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getShootingNum()I
    .locals 1

    .prologue
    .line 1176
    iget v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mShootingNum:I

    return v0
.end method

.method public isCalibrationLastPhase()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 1245
    iget-boolean v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mIsCalibrationEnabled:Z

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanoramaState:I

    if-ne v1, v0, :cond_0

    iget v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mWaitTime:I

    const/16 v2, 0x938

    if-lt v1, v2, :cond_0

    iget v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mWaitTime:I

    const/16 v2, 0xbb8

    if-gt v1, v2, :cond_0

    .line 1249
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isEnoughSpace()Z
    .locals 1

    .prologue
    .line 1393
    iget-boolean v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mIsEnoughSpace:Z

    return v0
.end method

.method public isInProcessState()Z
    .locals 2

    .prologue
    .line 70
    iget v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanoramaState:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 71
    const/4 v0, 0x1

    .line 72
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isInRunningFinishShootingAsync()Z
    .locals 2

    .prologue
    .line 1295
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mFinishShootingAsync:Lcom/sec/android/app/camera/panorama360/FinishShootingAsync;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mFinishShootingAsync:Lcom/sec/android/app/camera/panorama360/FinishShootingAsync;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/panorama360/FinishShootingAsync;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_0

    .line 1296
    const/4 v0, 0x1

    .line 1298
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "arg0"    # Landroid/hardware/Sensor;
    .param p2, "arg1"    # I

    .prologue
    .line 1000
    return-void
.end method

.method public onPreviewFrame([BLcom/sec/android/seccamera/SecCamera;)V
    .locals 5
    .param p1, "data"    # [B
    .param p2, "sec_camera"    # Lcom/sec/android/seccamera/SecCamera;

    .prologue
    .line 479
    const-string v1, "Panorama360Callback"

    const-string v2, "pc onPreviewFrame start"

    invoke-static {v1, v2}, Lcom/sec/android/app/camera/panorama360/LogFilter;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 480
    iget-boolean v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mFinishFlg:Z

    if-eqz v1, :cond_0

    .line 514
    :goto_0
    return-void

    .line 486
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mSensorLockObj:Ljava/lang/Object;

    monitor-enter v2

    .line 487
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mAccelerometer:Landroid/hardware/Sensor;

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->registered_accelerometer:Z

    if-nez v1, :cond_1

    .line 488
    iget-object v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v3, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mAccelerometer:Landroid/hardware/Sensor;

    const/4 v4, 0x0

    invoke-virtual {v1, p0, v3, v4}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->registered_accelerometer:Z

    .line 490
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mGyroscope:Landroid/hardware/Sensor;

    if-eqz v1, :cond_2

    iget-boolean v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->registered_gyroscope:Z

    if-nez v1, :cond_2

    .line 491
    iget-object v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v3, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mGyroscope:Landroid/hardware/Sensor;

    const/4 v4, 0x0

    invoke-virtual {v1, p0, v3, v4}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->registered_gyroscope:Z

    .line 493
    :cond_2
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 495
    iget v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanoramaState:I

    .line 496
    .local v0, "type":I
    iget v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanoramaState:I

    packed-switch v1, :pswitch_data_0

    .line 512
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/Camera;->getGLContext()Lcom/sec/android/glview/TwGLContext;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/glview/TwGLContext;->setDirty(Z)V

    .line 513
    const-string v1, "Panorama360Callback"

    const-string v2, "pc onPreviewFrame end"

    invoke-static {v1, v2}, Lcom/sec/android/app/camera/panorama360/LogFilter;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 493
    .end local v0    # "type":I
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    .line 498
    .restart local v0    # "type":I
    :pswitch_0
    const-string v1, "Panorama360Callback"

    const-string v2, "pc onPreviewFrame PANORAMA_STATE_UNINITIALIZED"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 499
    invoke-virtual {p0}, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->pcPanoramaInitialize()V

    goto :goto_1

    .line 502
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->pcPanoramaPreview([B)V

    .line 503
    iget v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanoramaPreviewCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanoramaPreviewCount:I

    goto :goto_1

    .line 506
    :pswitch_2
    invoke-direct {p0, p1}, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->pcPanoramaProcess([B)V

    .line 507
    iget v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanoramaProcessCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanoramaProcessCount:I

    goto :goto_1

    .line 496
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 13
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    const v12, 0x3089705f    # 1.0E-9f

    const/4 v7, 0x1

    .line 1057
    iget-object v5, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mSensorLockObj:Ljava/lang/Object;

    monitor-enter v5

    .line 1058
    :try_start_0
    const-string v4, "Panorama360Callback"

    const-string v6, "onSensorChanged start >>>>>>"

    invoke-static {v4, v6}, Lcom/sec/android/app/camera/panorama360/LogFilter;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1059
    iget-object v4, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    iget-object v6, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mGyroscope:Landroid/hardware/Sensor;

    if-ne v4, v6, :cond_3

    .line 1060
    iget v4, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanoramaState:I

    if-ne v4, v7, :cond_3

    .line 1061
    iget-boolean v4, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mIsCalibrationEnabled:Z

    if-eqz v4, :cond_5

    .line 1062
    iget v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mWaitTime:I

    .line 1063
    .local v1, "pre_time":I
    iget-wide v6, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->prev_timestamp:J

    const-wide/16 v8, 0x0

    cmp-long v4, v6, v8

    if-eqz v4, :cond_1

    .line 1064
    iget-wide v6, p1, Landroid/hardware/SensorEvent;->timestamp:J

    iget-wide v8, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->prev_timestamp:J

    sub-long/2addr v6, v8

    const-wide/16 v8, 0x1

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    .line 1065
    .local v2, "ns":J
    long-to-float v4, v2

    const v6, 0x4cbebc20    # 1.0E8f

    cmpl-float v4, v4, v6

    if-lez v4, :cond_0

    const-wide/16 v2, 0x1

    .line 1066
    :cond_0
    iget v4, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mWaitTime:I

    int-to-float v4, v4

    long-to-float v6, v2

    const v7, 0x358637bd    # 1.0E-6f

    mul-float/2addr v6, v7

    add-float/2addr v4, v6

    float-to-int v4, v4

    iput v4, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mWaitTime:I

    .line 1068
    iget-object v4, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mCurGyroscopeAngleValue:[D

    const/4 v6, 0x0

    aget-wide v8, v4, v6

    iget-object v7, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v10, 0x0

    aget v7, v7, v10

    long-to-float v10, v2

    mul-float/2addr v7, v10

    mul-float/2addr v7, v12

    float-to-double v10, v7

    add-double/2addr v8, v10

    aput-wide v8, v4, v6

    .line 1069
    iget-object v4, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mCurGyroscopeAngleValue:[D

    const/4 v6, 0x1

    aget-wide v8, v4, v6

    iget-object v7, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v10, 0x1

    aget v7, v7, v10

    long-to-float v10, v2

    mul-float/2addr v7, v10

    mul-float/2addr v7, v12

    float-to-double v10, v7

    add-double/2addr v8, v10

    aput-wide v8, v4, v6

    .line 1070
    iget-object v4, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mCurGyroscopeAngleValue:[D

    const/4 v6, 0x2

    aget-wide v8, v4, v6

    iget-object v7, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v10, 0x2

    aget v7, v7, v10

    long-to-float v10, v2

    mul-float/2addr v7, v10

    mul-float/2addr v7, v12

    float-to-double v10, v7

    add-double/2addr v8, v10

    aput-wide v8, v4, v6

    .line 1071
    iget v4, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mGyroscopeValueNumPerFrame:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mGyroscopeValueNumPerFrame:I

    .line 1073
    const-string v4, "Panorama360Callback"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onSensorChanged mWaitTime : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mWaitTime:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/sec/android/app/camera/panorama360/LogFilter;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1074
    iget v4, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mWaitTime:I

    invoke-direct {p0, v4}, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->setCenterGuideAnimationProgress(I)V

    .line 1076
    .end local v2    # "ns":J
    :cond_1
    iget v4, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mWaitTime:I

    const/16 v6, 0xbb8

    if-lt v4, v6, :cond_4

    .line 1077
    iget-object v4, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mSensorFusion:Lcom/sec/android/app/camera/panorama360/SensorFusion;

    const/4 v6, 0x1

    invoke-virtual {v4, v6}, Lcom/sec/android/app/camera/panorama360/SensorFusion;->setAppState(I)V

    .line 1078
    iget-object v4, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanorama360View:Lcom/sec/android/app/camera/panorama360/Panorama360View;

    const/4 v6, 0x2

    const/4 v7, -0x1

    invoke-virtual {v4, v6, v7}, Lcom/sec/android/app/camera/panorama360/Panorama360View;->pvSetAnimationInfo(II)V

    .line 1079
    iget-object v4, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v4}, Lcom/sec/android/app/camera/Camera;->getEngine()Lcom/sec/android/app/camera/CommonEngine;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 1080
    invoke-virtual {p0}, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->pcSensorCalculationCompleted()V

    .line 1099
    :cond_2
    :goto_0
    iget-wide v6, p1, Landroid/hardware/SensorEvent;->timestamp:J

    iput-wide v6, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->prev_timestamp:J

    .line 1105
    .end local v1    # "pre_time":I
    :cond_3
    :goto_1
    const-string v4, "Panorama360Callback"

    const-string v6, "<<<<<< onSensorChanged end"

    invoke-static {v4, v6}, Lcom/sec/android/app/camera/panorama360/LogFilter;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1106
    monitor-exit v5

    .line 1107
    return-void

    .line 1082
    .restart local v1    # "pre_time":I
    :cond_4
    iget v4, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mWaitTime:I

    const/16 v6, 0x3e8

    if-le v4, v6, :cond_2

    .line 1083
    const/16 v4, 0x3e8

    if-gt v1, v4, :cond_2

    .line 1084
    iget-object v4, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mTotalGyroscopeValue:[D

    const/4 v6, 0x0

    const-wide/16 v8, 0x0

    aput-wide v8, v4, v6

    .line 1085
    iget-object v4, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mTotalGyroscopeValue:[D

    const/4 v6, 0x1

    const-wide/16 v8, 0x0

    aput-wide v8, v4, v6

    .line 1086
    iget-object v4, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mTotalGyroscopeValue:[D

    const/4 v6, 0x2

    const-wide/16 v8, 0x0

    aput-wide v8, v4, v6

    .line 1087
    const/4 v4, 0x0

    iput v4, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mTotalGyroscopeValueNum:I

    .line 1088
    iget-object v4, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mSensorFusion:Lcom/sec/android/app/camera/panorama360/SensorFusion;

    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Lcom/sec/android/app/camera/panorama360/SensorFusion;->setAppState(I)V

    .line 1089
    iget-object v4, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v4}, Lcom/sec/android/app/camera/Camera;->getEngine()Lcom/sec/android/app/camera/CommonEngine;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 1090
    const-string v4, "Panorama360Callback"

    const-string v6, "pc onSensorChanged 1sec FOCUSMODE_AF"

    invoke-static {v4, v6}, Lcom/sec/android/app/camera/panorama360/LogFilter;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1092
    :try_start_1
    iget-object v4, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v4}, Lcom/sec/android/app/camera/Camera;->getEngine()Lcom/sec/android/app/camera/CommonEngine;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/camera/CommonEngine;->doAutoFocusAsync()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1093
    :catch_0
    move-exception v0

    .line 1094
    .local v0, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v4, "Panorama360Callback"

    const-string v6, "pc AutoFocus failed"

    invoke-static {v4, v6}, Lcom/sec/android/app/camera/panorama360/LogFilter;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1106
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "pre_time":I
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v4

    .line 1101
    :cond_5
    const/4 v4, 0x0

    :try_start_3
    invoke-direct {p0, v4}, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->setCenterGuideAnimationProgress(I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method public pause()V
    .locals 2

    .prologue
    .line 1259
    const-string v0, "Panorama360Callback"

    const-string v1, "pc pause"

    invoke-static {v0, v1}, Lcom/sec/android/app/camera/panorama360/LogFilter;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1260
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanoramaState:I

    .line 1261
    invoke-virtual {p0}, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->unregisterSensorManager()V

    .line 1262
    return-void
.end method

.method public pcFinishRenderPreview(Lcom/sec/android/app/camera/panorama360/Panorama360View$ResultInfo;)V
    .locals 3
    .param p1, "result"    # Lcom/sec/android/app/camera/panorama360/Panorama360View$ResultInfo;

    .prologue
    .line 350
    const-string v0, "Panorama360Callback"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "pcFinishRenderPreview : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanoramaState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/camera/panorama360/LogFilter;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 351
    iget v0, p1, Lcom/sec/android/app/camera/panorama360/Panorama360View$ResultInfo;->mAttachStatus:I

    invoke-direct {p0, v0}, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->pcMoveToNextStateByAttachStatus(I)V

    .line 353
    iget v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanoramaState:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 354
    invoke-direct {p0, p1}, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->checkCalibrationEnabled(Lcom/sec/android/app/camera/panorama360/Panorama360View$ResultInfo;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 355
    invoke-virtual {p0}, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->pcResetShootingWaitProcess()V

    .line 361
    :cond_0
    :goto_0
    return-void

    .line 356
    :cond_1
    iget v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanoramaState:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 357
    iget v0, p1, Lcom/sec/android/app/camera/panorama360/Panorama360View$ResultInfo;->mImageID:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mShootingNum:I

    if-lez v0, :cond_0

    .line 358
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x102

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method

.method public pcOnBackProc()Z
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 328
    const/4 v0, 0x0

    .line 329
    .local v0, "finishPanorama":Z
    const-string v1, "Panorama360Callback"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "pc pcOnBackProc mPanoramaState: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanoramaState:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/camera/panorama360/LogFilter;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 330
    iget v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanoramaState:I

    packed-switch v1, :pswitch_data_0

    .line 346
    :goto_0
    return v0

    .line 332
    :pswitch_0
    invoke-virtual {p0, v5, v5}, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->pcRestartShooting(ZZ)V

    .line 333
    const-string v1, "Panorama360Callback"

    const-string v2, "pcOnBackProc,assertion error!!!!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 336
    :pswitch_1
    invoke-virtual {p0, v5, v4, v4}, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->pcStopShooting(ZZZ)V

    .line 337
    iget-object v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanorama360View:Lcom/sec/android/app/camera/panorama360/Panorama360View;

    invoke-virtual {v1, v4}, Lcom/sec/android/app/camera/panorama360/Panorama360View;->pvSetRenderEnable(Z)V

    .line 339
    invoke-virtual {p0}, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->pcRelease()V

    .line 340
    const/4 v0, 0x1

    .line 341
    goto :goto_0

    .line 330
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public pcPanoramaInitialize()V
    .locals 10

    .prologue
    .line 517
    const-string v0, "Panorama360Callback"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "pc pcPanoramaInitialize previous state : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanoramaState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/camera/panorama360/LogFilter;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 518
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanoramaState:I

    .line 521
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanoramaInitParam:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$PanoramaInitParam;

    const/4 v1, 0x0

    iput v1, v0, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$PanoramaInitParam;->mode:I

    .line 522
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanoramaInitParam:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$PanoramaInitParam;

    const/4 v1, 0x1

    iput v1, v0, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$PanoramaInitParam;->render_mode:I

    .line 524
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanoramaInitParam:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$PanoramaInitParam;

    invoke-static {}, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;->getAngleOfViewDegree()D

    move-result-wide v2

    iput-wide v2, v0, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$PanoramaInitParam;->input_angle_of_view_degree:D

    .line 525
    const-string v0, "Panorama360Callback"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "input_angle_of_view_degree : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanoramaInitParam:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$PanoramaInitParam;

    iget-wide v2, v2, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$PanoramaInitParam;->input_angle_of_view_degree:D

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/camera/panorama360/LogFilter;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    .line 530
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanoramaInitParam:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$PanoramaInitParam;

    iget v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mCameraWidth:I

    iput v1, v0, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$PanoramaInitParam;->input_width:I

    .line 531
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanoramaInitParam:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$PanoramaInitParam;

    iget v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mCameraHeight:I

    iput v1, v0, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$PanoramaInitParam;->input_height:I

    .line 532
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanoramaInitParam:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$PanoramaInitParam;

    const/4 v1, 0x0

    iput v1, v0, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$PanoramaInitParam;->use_still_capture:I

    .line 533
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanoramaInitParam:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$PanoramaInitParam;

    const/4 v1, 0x0

    iput v1, v0, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$PanoramaInitParam;->still_width:I

    .line 534
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanoramaInitParam:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$PanoramaInitParam;

    const/4 v1, 0x0

    iput v1, v0, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$PanoramaInitParam;->still_height:I

    .line 535
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanoramaInitParam:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$PanoramaInitParam;

    iget-object v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanoramaInitParam:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$PanoramaInitParam;

    iget-wide v2, v1, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$PanoramaInitParam;->input_angle_of_view_degree:D

    iput-wide v2, v0, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$PanoramaInitParam;->still_angle_of_view_degree:D

    .line 536
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanoramaInitParam:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$PanoramaInitParam;

    const-string v1, "YVU420_SEMIPLANAR"

    iput-object v1, v0, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$PanoramaInitParam;->format:Ljava/lang/String;

    .line 537
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanoramaInitParam:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$PanoramaInitParam;

    const/4 v1, 0x1

    iput v1, v0, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$PanoramaInitParam;->alpha_blending_image_frame:I

    .line 538
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanoramaInitParam:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$PanoramaInitParam;

    const/4 v1, 0x0

    iput v1, v0, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$PanoramaInitParam;->gradually_disp_guide_frame:I

    .line 539
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanoramaInitParam:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$PanoramaInitParam;

    const/4 v1, 0x1

    iput v1, v0, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$PanoramaInitParam;->fix_current_image:I

    .line 540
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanoramaInitParam:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$PanoramaInitParam;

    const/4 v1, 0x1

    iput v1, v0, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$PanoramaInitParam;->disp_current_image:I

    .line 541
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanoramaInitParam:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$PanoramaInitParam;

    const/4 v1, 0x1

    iput v1, v0, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$PanoramaInitParam;->blink_preview_mode:I

    .line 542
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanoramaInitParam:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$PanoramaInitParam;

    const/4 v1, 0x1

    iput v1, v0, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$PanoramaInitParam;->version:I

    .line 543
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanoramaInitParam:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$PanoramaInitParam;

    iget-object v0, v0, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$PanoramaInitParam;->registered_frame_color:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$FrameColor;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$FrameColor;->set(FFFFF)V

    .line 544
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanoramaInitParam:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$PanoramaInitParam;

    iget-object v0, v0, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$PanoramaInitParam;->preview_frame_color:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$FrameColor;

    const/high16 v1, 0x3f800000    # 1.0f

    const/high16 v2, 0x3f800000    # 1.0f

    const/high16 v3, 0x3f800000    # 1.0f

    const/high16 v4, 0x3f800000    # 1.0f

    const/high16 v5, 0x40000000    # 2.0f

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$FrameColor;->set(FFFFF)V

    .line 545
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanoramaInitParam:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$PanoramaInitParam;

    iget-object v0, v0, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$PanoramaInitParam;->state_warning_toofar_frame_color:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$FrameColor;

    const/high16 v1, 0x3f800000    # 1.0f

    const/high16 v2, 0x3f800000    # 1.0f

    const/high16 v3, 0x3f800000    # 1.0f

    const/high16 v4, 0x3f800000    # 1.0f

    const/high16 v5, 0x40000000    # 2.0f

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$FrameColor;->set(FFFFF)V

    .line 546
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanoramaInitParam:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$PanoramaInitParam;

    iget-object v0, v0, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$PanoramaInitParam;->state_warning_rotated_frame_color:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$FrameColor;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    const/high16 v5, 0x40000000    # 2.0f

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$FrameColor;->set(FFFFF)V

    .line 547
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanoramaInitParam:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$PanoramaInitParam;

    iget-object v0, v0, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$PanoramaInitParam;->guide_frame_color:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$FrameColor;

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    const/high16 v3, 0x3f800000    # 1.0f

    const/high16 v4, 0x3f800000    # 1.0f

    const/high16 v5, 0x40400000    # 3.0f

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$FrameColor;->set(FFFFF)V

    .line 548
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanoramaInitParam:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$PanoramaInitParam;

    const/16 v1, 0x74

    iput v1, v0, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$PanoramaInitParam;->angle_fov:I

    .line 550
    const-string v0, "Panorama360Callback"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "pc pcPanoramaInitialize angle of view degree : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanoramaInitParam:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$PanoramaInitParam;

    iget-wide v2, v2, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$PanoramaInitParam;->input_angle_of_view_degree:D

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/camera/panorama360/LogFilter;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 551
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mMorphoImageStitcher:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;

    iget-object v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanoramaInitParam:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$PanoramaInitParam;

    iget-object v2, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mMaxHeapSize:[I

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;->initialize(Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$PanoramaInitParam;[I)I

    move-result v9

    .line 552
    .local v9, "ret":I
    if-eqz v9, :cond_0

    .line 553
    const-string v0, "Panorama360Callback"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mMorphoImageStitcher.initialize error ret:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/camera/panorama360/LogFilter;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 556
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mMorphoImageStitcher:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;

    sget v1, Lcom/sec/android/app/camera/panorama360/Panorama360Property;->ProjectionType:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;->setProjectionType(I)I

    move-result v9

    .line 557
    if-eqz v9, :cond_1

    .line 558
    const-string v0, "Panorama360Callback"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mMorphoImageStitcher.setProjectionType error ret:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/camera/panorama360/LogFilter;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 561
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mMorphoImageStitcher:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;

    sget v1, Lcom/sec/android/app/camera/panorama360/Panorama360Property;->MotionlessThres:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;->setMotionlessThreshold(I)I

    move-result v9

    .line 562
    if-eqz v9, :cond_2

    .line 563
    const-string v0, "Panorama360Callback"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mMorphoImageStitcher.setMotionlessThreshold error ret:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/camera/panorama360/LogFilter;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 566
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mMorphoImageStitcher:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;

    sget v1, Lcom/sec/android/app/camera/panorama360/Panorama360Property;->UseThres:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;->setUseThreshold(I)I

    move-result v9

    .line 567
    if-eqz v9, :cond_3

    .line 568
    const-string v0, "Panorama360Callback"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mMorphoImageStitcher.setUseThreshold error ret:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/camera/panorama360/LogFilter;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 570
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mMorphoImageStitcher:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;

    const/4 v2, 0x0

    sget-boolean v0, Lcom/sec/android/app/camera/panorama360/Panorama360Property;->UseSensorForAWF:Z

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;->setUseSensorAssist(II)I

    move-result v9

    .line 571
    if-eqz v9, :cond_4

    .line 572
    const-string v0, "Panorama360Callback"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mMorphoImageStitcher.setUseSensorAssist error ret:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/camera/panorama360/LogFilter;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 574
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mMorphoImageStitcher:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;

    const/4 v2, 0x1

    sget-boolean v0, Lcom/sec/android/app/camera/panorama360/Panorama360Property;->UseSensorForGA:Z

    if-eqz v0, :cond_9

    const/4 v0, 0x1

    :goto_2
    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;->setUseSensorAssist(II)I

    move-result v9

    .line 575
    if-eqz v9, :cond_5

    .line 576
    const-string v0, "Panorama360Callback"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mMorphoImageStitcher.setUseSensorAssist error ret:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/camera/panorama360/LogFilter;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 578
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mMorphoImageStitcher:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;

    sget v1, Lcom/sec/android/app/camera/panorama360/Panorama360Property;->UseSensorThres:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;->setUseSensorThreshold(I)I

    move-result v9

    .line 579
    if-eqz v9, :cond_6

    .line 580
    const-string v0, "Panorama360Callback"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mMorphoImageStitcher.setUseSensorThreshold error ret:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/camera/panorama360/LogFilter;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 582
    :cond_6
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_3
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->RES_ID_PANORAMA_GUIDE_IMAGE:[[I

    array-length v0, v0

    if-ge v8, v0, :cond_a

    .line 583
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mGuideImage:[Landroid/graphics/Bitmap;

    aget-object v0, v0, v8

    if-eqz v0, :cond_7

    .line 584
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->RES_ID_PANORAMA_GUIDE_IMAGE:[[I

    aget-object v0, v0, v8

    const/4 v1, 0x0

    aget v7, v0, v1

    .line 585
    .local v7, "guide_image_type":I
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mMorphoImageStitcher:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;

    iget-object v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mGuideImage:[Landroid/graphics/Bitmap;

    aget-object v1, v1, v8

    invoke-virtual {v0, v7, v1}, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;->setGuideImage(ILandroid/graphics/Bitmap;)I

    .line 586
    if-eqz v9, :cond_7

    .line 587
    const-string v0, "Panorama360Callback"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mMorphoImageStitcher.setGuideImage error ret:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/camera/panorama360/LogFilter;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 582
    .end local v7    # "guide_image_type":I
    :cond_7
    add-int/lit8 v8, v8, 0x1

    goto :goto_3

    .line 526
    .end local v8    # "i":I
    .end local v9    # "ret":I
    :catch_0
    move-exception v6

    .line 527
    .local v6, "e":Ljava/lang/UnsatisfiedLinkError;
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanoramaInitParam:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$PanoramaInitParam;

    sget-wide v2, Lcom/sec/android/app/camera/panorama360/Panorama360Property;->AngleOfView:D

    iput-wide v2, v0, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$PanoramaInitParam;->input_angle_of_view_degree:D

    goto/16 :goto_0

    .line 570
    .end local v6    # "e":Ljava/lang/UnsatisfiedLinkError;
    .restart local v9    # "ret":I
    :cond_8
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 574
    :cond_9
    const/4 v0, 0x0

    goto/16 :goto_2

    .line 597
    .restart local v8    # "i":I
    :cond_a
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mMorphoImageStitcher:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;->start(I)I

    move-result v9

    .line 598
    if-eqz v9, :cond_b

    .line 599
    const-string v0, "Panorama360Callback"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mMorphoImageStitcher.start error ret:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/camera/panorama360/LogFilter;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 602
    :cond_b
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mUseImage:[I

    const/4 v1, 0x0

    const/4 v2, 0x0

    aput v2, v0, v1

    .line 603
    return-void
.end method

.method public pcPrepareShooting(Z)V
    .locals 5
    .param p1, "sensor_init"    # Z

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 974
    const-string v0, "Panorama360Callback"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "pc pcPrepareShooting sensor_init: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/camera/panorama360/LogFilter;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 976
    if-ne p1, v4, :cond_1

    .line 977
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mGyroscope:Landroid/hardware/Sensor;

    if-eqz v0, :cond_0

    .line 978
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mGyroscope:Landroid/hardware/Sensor;

    invoke-virtual {v0, p0, v1, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->registered_gyroscope:Z

    .line 979
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mSensorFusion:Lcom/sec/android/app/camera/panorama360/SensorFusion;

    iget-object v2, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mGyroscope:Landroid/hardware/Sensor;

    invoke-virtual {v0, v1, v2, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->registered_gyroscope:Z

    .line 981
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mAccelerometer:Landroid/hardware/Sensor;

    if-eqz v0, :cond_1

    .line 982
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mAccelerometer:Landroid/hardware/Sensor;

    invoke-virtual {v0, p0, v1, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->registered_accelerometer:Z

    .line 983
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mSensorFusion:Lcom/sec/android/app/camera/panorama360/SensorFusion;

    iget-object v2, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mAccelerometer:Landroid/hardware/Sensor;

    invoke-virtual {v0, v1, v2, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->registered_accelerometer:Z

    .line 987
    :cond_1
    iput v3, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanoramaPreviewCount:I

    .line 988
    iput v3, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanoramaProcessCount:I

    .line 989
    iput v3, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mWaitTime:I

    .line 990
    iput-boolean v3, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->moveToShooting:Z

    .line 991
    iput-boolean v3, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mFinishFlg:Z

    .line 992
    iput-boolean v3, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mIsCalibrationEnabled:Z

    .line 993
    iput-boolean v4, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->doImageClassify:Z

    .line 994
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mUseImage:[I

    aput v3, v0, v3

    .line 995
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x105

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 996
    return-void
.end method

.method public pcRelease()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 402
    const-string v1, "Panorama360Callback"

    const-string v2, "pc pcRelease "

    invoke-static {v1, v2}, Lcom/sec/android/app/camera/panorama360/LogFilter;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 404
    iget-object v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mMorphoImageStitcher:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;

    if-eqz v1, :cond_0

    .line 405
    iget-object v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    new-instance v2, Lcom/sec/android/app/camera/panorama360/Panorama360Callback$ReleaseMemory;

    iget-object v3, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mMorphoImageStitcher:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;

    invoke-direct {v2, v3}, Lcom/sec/android/app/camera/panorama360/Panorama360Callback$ReleaseMemory;-><init>(Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/camera/Camera;->setGLQueueEvent(Ljava/lang/Runnable;)V

    .line 406
    iput-object v4, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mMorphoImageStitcher:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;

    .line 409
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanorama360View:Lcom/sec/android/app/camera/panorama360/Panorama360View;

    if-eqz v1, :cond_1

    .line 410
    iget-object v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanorama360View:Lcom/sec/android/app/camera/panorama360/Panorama360View;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/panorama360/Panorama360View;->pvRelease()V

    .line 411
    iget-object v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanorama360View:Lcom/sec/android/app/camera/panorama360/Panorama360View;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/panorama360/Panorama360View;->clear()V

    .line 412
    iput-object v4, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanorama360View:Lcom/sec/android/app/camera/panorama360/Panorama360View;

    .line 414
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mGuideImage:[Landroid/graphics/Bitmap;

    if-eqz v1, :cond_3

    .line 415
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mGuideImage:[Landroid/graphics/Bitmap;

    array-length v1, v1

    if-ge v0, v1, :cond_3

    .line 416
    iget-object v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mGuideImage:[Landroid/graphics/Bitmap;

    aget-object v1, v1, v0

    if-eqz v1, :cond_2

    .line 417
    iget-object v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mGuideImage:[Landroid/graphics/Bitmap;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 418
    iget-object v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mGuideImage:[Landroid/graphics/Bitmap;

    aput-object v4, v1, v0

    .line 415
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 422
    .end local v0    # "i":I
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mImageClassifyBmp:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_4

    .line 423
    iget-object v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mImageClassifyBmp:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 424
    iput-object v4, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mImageClassifyBmp:Landroid/graphics/Bitmap;

    .line 426
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mSensorFusion:Lcom/sec/android/app/camera/panorama360/SensorFusion;

    if-eqz v1, :cond_5

    .line 427
    iget-object v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mSensorFusion:Lcom/sec/android/app/camera/panorama360/SensorFusion;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/panorama360/SensorFusion;->release()V

    .line 430
    :cond_5
    invoke-static {}, Lcom/sec/android/app/camera/panorama360/Panorama360DebugUtils;->finalized()V

    .line 431
    return-void
.end method

.method public pcResetShootingWaitProcess()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v4, 0x0

    .line 857
    const-string v2, "Panorama360Callback"

    const-string v3, "pc pcResetShootingWaitProcess "

    invoke-static {v2, v3}, Lcom/sec/android/app/camera/panorama360/LogFilter;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 858
    iput v4, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mWaitTime:I

    .line 859
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->doImageClassify:Z

    .line 860
    iget-object v2, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mGyroscope:Landroid/hardware/Sensor;

    if-eqz v2, :cond_0

    .line 864
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mSensorFusion:Lcom/sec/android/app/camera/panorama360/SensorFusion;

    invoke-virtual {v2}, Lcom/sec/android/app/camera/panorama360/SensorFusion;->resetOffsetValue()V

    .line 865
    iget-object v2, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mSensorFusion:Lcom/sec/android/app/camera/panorama360/SensorFusion;

    invoke-virtual {v2}, Lcom/sec/android/app/camera/panorama360/SensorFusion;->clearStockData()V

    .line 867
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mCurGyroscopeAngleValue:[D

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 868
    iget-object v2, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mTotalGyroscopeValue:[D

    aput-wide v6, v2, v0

    .line 869
    iget-object v2, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mCurGyroscopeAngleValue:[D

    aput-wide v6, v2, v0

    .line 867
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 871
    :cond_1
    const/4 v0, 0x0

    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mCurGyroscopeAngleValueHist:[[D

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 872
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_2
    iget-object v2, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mCurGyroscopeAngleValueHist:[[D

    aget-object v2, v2, v0

    array-length v2, v2

    if-ge v1, v2, :cond_2

    .line 873
    iget-object v2, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mCurGyroscopeAngleValueHist:[[D

    aget-object v2, v2, v0

    aput-wide v6, v2, v1

    .line 872
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 871
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 876
    .end local v1    # "j":I
    :cond_3
    iput v4, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mTotalGyroscopeValueNum:I

    .line 877
    iput v4, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mGyroscopeValueNumPerFrame:I

    .line 878
    iput v4, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mCurGyroscopeAngleIndex:I

    .line 879
    return-void
.end method

.method public pcRestartShooting(ZZ)V
    .locals 4
    .param p1, "save_image"    # Z
    .param p2, "do_in_background"    # Z

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 265
    const-string v0, "Panorama360Callback"

    const-string v1, "pc pcRestartShooting "

    invoke-static {v0, v1}, Lcom/sec/android/app/camera/panorama360/LogFilter;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    invoke-direct {p0}, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->checkAvailableStorage()V

    .line 268
    iget v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanoramaState:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 269
    const/4 p1, 0x0

    .line 271
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanorama360View:Lcom/sec/android/app/camera/panorama360/Panorama360View;

    invoke-virtual {v0, v3, v2}, Lcom/sec/android/app/camera/panorama360/Panorama360View;->pvSetAnimationInfo(II)V

    .line 272
    if-eqz p2, :cond_1

    .line 273
    invoke-direct {p0, p1}, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->pcRestartToStartupPreview(Z)V

    .line 283
    :goto_0
    return-void

    .line 275
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanorama360View:Lcom/sec/android/app/camera/panorama360/Panorama360View;

    iget-object v1, v0, Lcom/sec/android/app/camera/panorama360/Panorama360View;->mSyncObj:Ljava/lang/Object;

    monitor-enter v1

    .line 276
    const/4 v0, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    :try_start_0
    invoke-virtual {p0, v0, v2, v3}, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->pcStopShooting(ZZZ)V

    .line 277
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getEngine()Lcom/sec/android/app/camera/CommonEngine;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CommonEngine;->enablePreviewCallbackManagerPanorama360()V

    .line 278
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getEngine()Lcom/sec/android/app/camera/CommonEngine;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/camera/CommonEngine;->setPreviewCallback(Lcom/sec/android/app/camera/CommonEngine$CommonEnginePreviewCallback;)V

    .line 279
    invoke-virtual {p0}, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->pcStartShooting()V

    .line 280
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanorama360View:Lcom/sec/android/app/camera/panorama360/Panorama360View;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/sec/android/app/camera/panorama360/Panorama360View;->pvSetDrawBgBlankMode(I)V

    .line 281
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public pcSensorCalculationCompleted()V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 250
    const-string v0, "Panorama360Callback"

    const-string v1, "pc pcSensorCalculationCompleted "

    invoke-static {v0, v1}, Lcom/sec/android/app/camera/panorama360/LogFilter;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 252
    iput-boolean v4, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mAllGuideTaken:Z

    .line 254
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x100

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 256
    iget v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanoramaState:I

    if-eq v0, v2, :cond_0

    .line 262
    :goto_0
    return-void

    .line 259
    :cond_0
    iput-boolean v2, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->moveToShooting:Z

    .line 260
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mDateTaken:[J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    aput-wide v2, v0, v4

    .line 261
    iput v4, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mShootingNum:I

    goto :goto_0
.end method

.method public pcStartShooting()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 286
    const-string v1, "Panorama360Callback"

    const-string v2, "pc pcStartShooting "

    invoke-static {v1, v2}, Lcom/sec/android/app/camera/panorama360/LogFilter;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 287
    iget-object v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    if-eqz v1, :cond_0

    .line 288
    iget-object v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/Camera;->resumeAudioPlayback()V

    .line 290
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/Camera;->getCameraBaseMenu()Lcom/sec/android/app/camera/glwidget/TwGLCameraBaseMenu;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 291
    iget-object v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/Camera;->getCameraBaseMenu()Lcom/sec/android/app/camera/glwidget/TwGLCameraBaseMenu;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/camera/glwidget/TwGLCameraBaseMenu;->showBaseMenu()V

    .line 294
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x105

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 296
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->pcPrepareShooting(Z)V

    .line 297
    iget-object v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mMorphoImageStitcher:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;

    invoke-virtual {v1, v4}, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;->start(I)I

    move-result v0

    .line 298
    .local v0, "ret":I
    if-eqz v0, :cond_2

    .line 299
    const-string v1, "Panorama360Callback"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mMorphoImageStitcher.start(1) error int panorama_restart_button onclicked ret:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/camera/panorama360/LogFilter;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    :cond_2
    iput v4, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanoramaState:I

    .line 302
    iget-object v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mSensorFusion:Lcom/sec/android/app/camera/panorama360/SensorFusion;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/camera/panorama360/SensorFusion;->setMode(I)V

    .line 303
    return-void
.end method

.method public pcStopShooting(ZZZ)V
    .locals 6
    .param p1, "unregist_sensor"    # Z
    .param p2, "save_image"    # Z
    .param p3, "do_in_background"    # Z

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 306
    const-string v0, "Panorama360Callback"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "pc pcStopShooting unregist_sensor: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " save_image: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " do_in_background: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/camera/panorama360/LogFilter;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 307
    if-ne p1, v4, :cond_0

    .line 308
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mSensorManager:Landroid/hardware/SensorManager;

    invoke-virtual {v0, p0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 309
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mSensorFusion:Lcom/sec/android/app/camera/panorama360/SensorFusion;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 311
    :cond_0
    if-eqz p2, :cond_1

    .line 325
    :goto_0
    return-void

    .line 314
    :cond_1
    if-eqz p3, :cond_2

    .line 315
    invoke-direct {p0, v5, v3}, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->executeFinishShootingAsync(IZ)V

    goto :goto_0

    .line 317
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 318
    iput-boolean v4, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mFinishFlg:Z

    .line 319
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanoramaState:I

    .line 320
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mMorphoImageStitcher:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;->releaseRegisteredImage()I

    .line 321
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanorama360View:Lcom/sec/android/app/camera/panorama360/Panorama360View;

    invoke-virtual {v0, v5}, Lcom/sec/android/app/camera/panorama360/Panorama360View;->pvSetDrawBgBlankMode(I)V

    .line 322
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mMorphoImageStitcher:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;->end()I

    goto :goto_0
.end method

.method public postFinishShooting(I)V
    .locals 3
    .param p1, "finishtype"    # I

    .prologue
    .line 1335
    const-string v0, "Panorama360Callback"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "postFinishShooting : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mProgressPopup:Lcom/sec/android/app/camera/glwidget/TwGLProgressingPopup;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/camera/panorama360/LogFilter;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1336
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mProgressPopup:Lcom/sec/android/app/camera/glwidget/TwGLProgressingPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/glwidget/TwGLProgressingPopup;->reset()V

    .line 1337
    packed-switch p1, :pswitch_data_0

    .line 1348
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanorama360View:Lcom/sec/android/app/camera/panorama360/Panorama360View;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/camera/panorama360/Panorama360View;->pvSetDrawBgBlankMode(I)V

    .line 1349
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getEngine()Lcom/sec/android/app/camera/CommonEngine;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CommonEngine;->enablePreviewCallbackManagerPanorama360()V

    .line 1350
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getEngine()Lcom/sec/android/app/camera/CommonEngine;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/camera/CommonEngine;->setPreviewCallback(Lcom/sec/android/app/camera/CommonEngine$CommonEnginePreviewCallback;)V

    .line 1351
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x109

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1352
    return-void

    .line 1339
    :pswitch_0
    const-string v0, "Panorama360Callback"

    const-string v1, "pc onPostExecute FT_RESTART"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1340
    invoke-virtual {p0}, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->pcStartShooting()V

    goto :goto_0

    .line 1343
    :pswitch_1
    const-string v0, "Panorama360Callback"

    const-string v1, "pc onPostExecute FT_CANCEL"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1344
    invoke-virtual {p0}, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->pcStartShooting()V

    .line 1345
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanoramaState:I

    goto :goto_0

    .line 1337
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public preFinishShooting()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1314
    const-string v0, "Panorama360Callback"

    const-string v1, "pc preFinishShooting"

    invoke-static {v0, v1}, Lcom/sec/android/app/camera/panorama360/LogFilter;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1315
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 1316
    iput-boolean v5, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mFinishFlg:Z

    .line 1317
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanorama360View:Lcom/sec/android/app/camera/panorama360/Panorama360View;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/panorama360/Panorama360View;->pvFinishShooting()V

    .line 1318
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanoramaState:I

    .line 1319
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mDateTaken:[J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    aput-wide v2, v0, v5

    .line 1321
    invoke-direct {p0}, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->getInstanceProgressingPopup()Lcom/sec/android/app/camera/glwidget/TwGLProgressingPopup;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mProgressPopup:Lcom/sec/android/app/camera/glwidget/TwGLProgressingPopup;

    .line 1323
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x101

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1325
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mProgressPopup:Lcom/sec/android/app/camera/glwidget/TwGLProgressingPopup;

    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, Lcom/sec/android/app/camera/glwidget/TwGLProgressingPopup;->setPostCaptureProgressMax(I)V

    .line 1326
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mProgressPopup:Lcom/sec/android/app/camera/glwidget/TwGLProgressingPopup;

    invoke-virtual {v0, v4}, Lcom/sec/android/app/camera/glwidget/TwGLProgressingPopup;->setPostCaptureProgress(I)V

    .line 1327
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mProgressPopup:Lcom/sec/android/app/camera/glwidget/TwGLProgressingPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/glwidget/TwGLProgressingPopup;->showPostCaptureLayout()V

    .line 1329
    invoke-static {}, Lcom/sec/android/app/camera/panorama360/Panorama360DebugUtils;->isInSensorDebugging()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1330
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mDateTaken:[J

    aget-wide v0, v0, v4

    iget-object v2, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mSensorFusion:Lcom/sec/android/app/camera/panorama360/SensorFusion;

    invoke-virtual {v2}, Lcom/sec/android/app/camera/panorama360/SensorFusion;->getStockData()[Ljava/util/ArrayList;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/camera/panorama360/Panorama360DebugUtils;->startSaveShootingSDThread(J[Ljava/util/ArrayList;)V

    .line 1332
    :cond_0
    return-void
.end method

.method public progressPopupRefresh()V
    .locals 2

    .prologue
    .line 1227
    const-string v0, "Panorama360Callback"

    const-string v1, "pc progressPopupRefresh"

    invoke-static {v0, v1}, Lcom/sec/android/app/camera/panorama360/LogFilter;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1228
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mFinishShootingAsync:Lcom/sec/android/app/camera/panorama360/FinishShootingAsync;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mFinishShootingAsync:Lcom/sec/android/app/camera/panorama360/FinishShootingAsync;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/panorama360/FinishShootingAsync;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_0

    .line 1229
    invoke-direct {p0}, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->getInstanceProgressingPopup()Lcom/sec/android/app/camera/glwidget/TwGLProgressingPopup;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mProgressPopup:Lcom/sec/android/app/camera/glwidget/TwGLProgressingPopup;

    .line 1230
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mProgressPopup:Lcom/sec/android/app/camera/glwidget/TwGLProgressingPopup;

    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, Lcom/sec/android/app/camera/glwidget/TwGLProgressingPopup;->setPostCaptureProgressMax(I)V

    .line 1231
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mProgressPopup:Lcom/sec/android/app/camera/glwidget/TwGLProgressingPopup;

    iget-object v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mFinishShootingAsync:Lcom/sec/android/app/camera/panorama360/FinishShootingAsync;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/panorama360/FinishShootingAsync;->getProgress()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/camera/glwidget/TwGLProgressingPopup;->setPostCaptureProgress(I)V

    .line 1232
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mProgressPopup:Lcom/sec/android/app/camera/glwidget/TwGLProgressingPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/glwidget/TwGLProgressingPopup;->showPostCaptureLayout()V

    .line 1234
    :cond_0
    return-void
.end method

.method public progressUpdate(I)V
    .locals 3
    .param p1, "progress"    # I

    .prologue
    .line 1237
    const-string v0, "Panorama360Callback"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "pc progressUpdate : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/camera/panorama360/LogFilter;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1238
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mProgressPopup:Lcom/sec/android/app/camera/glwidget/TwGLProgressingPopup;

    if-eqz v0, :cond_0

    .line 1239
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mProgressPopup:Lcom/sec/android/app/camera/glwidget/TwGLProgressingPopup;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/camera/glwidget/TwGLProgressingPopup;->setPostCaptureProgress(I)V

    .line 1240
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mProgressPopup:Lcom/sec/android/app/camera/glwidget/TwGLProgressingPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/glwidget/TwGLProgressingPopup;->showPostCaptureLayout()V

    .line 1242
    :cond_0
    return-void
.end method

.method public releasePreview()V
    .locals 2

    .prologue
    .line 1417
    const-string v0, "Panorama360Callback"

    const-string v1, "pc releasePreview"

    invoke-static {v0, v1}, Lcom/sec/android/app/camera/panorama360/LogFilter;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1418
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    if-eqz v0, :cond_0

    .line 1419
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getEngine()Lcom/sec/android/app/camera/CommonEngine;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CommonEngine;->disablePreviewCallbackManager()V

    .line 1420
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getEngine()Lcom/sec/android/app/camera/CommonEngine;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/camera/CommonEngine;->setPreviewCallback(Lcom/sec/android/app/camera/CommonEngine$CommonEnginePreviewCallback;)V

    .line 1422
    :cond_0
    return-void
.end method

.method public releaseRegisteredImages()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1355
    const-string v2, "Panorama360Callback"

    const-string v3, "pc releaseRegisteredImages"

    invoke-static {v2, v3}, Lcom/sec/android/app/camera/panorama360/LogFilter;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1356
    invoke-static {}, Lcom/sec/android/app/camera/panorama360/Panorama360DebugUtils;->isInImageDebugging()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1357
    iget-object v2, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mDateTaken:[J

    aget-wide v2, v2, v4

    invoke-static {v2, v3}, Lcom/sec/android/app/camera/panorama360/Panorama360DebugUtils;->createDebugImageDirectory(J)Ljava/lang/String;

    move-result-object v0

    .line 1358
    .local v0, "dir":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/camera/panorama360/Panorama360DebugUtils;->getSaveImagePathList()[Ljava/lang/Object;

    move-result-object v1

    .line 1359
    .local v1, "save_path_list":[Ljava/lang/Object;
    iget-object v2, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mMorphoImageStitcher:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;

    invoke-virtual {v2, v4, v1}, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;->saveRegisteredImage(Z[Ljava/lang/Object;)I

    .line 1360
    const-string v2, "Panorama360Callback"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "pc FinishShootingAsync saveRegisteredImage : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/camera/panorama360/LogFilter;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1362
    .end local v0    # "dir":Ljava/lang/String;
    .end local v1    # "save_path_list":[Ljava/lang/Object;
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mMorphoImageStitcher:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;

    invoke-virtual {v2}, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;->releaseRegisteredImage()I

    .line 1363
    return-void
.end method

.method public renderBgOnly()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1141
    const-string v0, "Panorama360Callback"

    const-string v1, "renderBgOnly"

    invoke-static {v0, v1}, Lcom/sec/android/app/camera/panorama360/LogFilter;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1143
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanorama360View:Lcom/sec/android/app/camera/panorama360/Panorama360View;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/camera/panorama360/Panorama360View;->pvSetDrawBgBlankMode(I)V

    .line 1144
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanorama360View:Lcom/sec/android/app/camera/panorama360/Panorama360View;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/camera/panorama360/Panorama360View;->pvSetAnimationInfo(II)V

    .line 1145
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanorama360View:Lcom/sec/android/app/camera/panorama360/Panorama360View;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/camera/panorama360/Panorama360View;->pvSetAnimationEnable(Z)V

    .line 1146
    invoke-virtual {p0}, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->pcResetShootingWaitProcess()V

    .line 1147
    invoke-virtual {p0}, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->unregisterSensorManager()V

    .line 1148
    return-void
.end method

.method public renderEmptyPreview(II)V
    .locals 7
    .param p1, "rotation"    # I
    .param p2, "device_rotation"    # I

    .prologue
    const/4 v1, 0x0

    .line 1151
    const-string v0, "Panorama360Callback"

    const-string v2, "renderEmptyPreview"

    invoke-static {v0, v2}, Lcom/sec/android/app/camera/panorama360/LogFilter;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1152
    const/16 v0, 0x9

    new-array v6, v0, [D

    .line 1153
    .local v6, "screen_rot":[D
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mSensorFusion:Lcom/sec/android/app/camera/panorama360/SensorFusion;

    invoke-virtual {v0, v6, v1, v1, v1}, Lcom/sec/android/app/camera/panorama360/SensorFusion;->getSensorMatrix([D[D[D[I)I

    .line 1154
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mMorphoImageStitcher:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;

    sget v2, Lcom/sec/android/app/camera/panorama360/Panorama360Property;->GyroscopeType:I

    invoke-virtual {v0, v6, v2}, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;->setAngleMatrix([DI)I

    .line 1155
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mMorphoImageStitcher:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;

    const/4 v2, 0x0

    sget v3, Lcom/sec/android/app/camera/panorama360/Panorama360Property;->DiplayType:I

    move v4, p1

    move v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;->renderPreview([BIIII)I

    .line 1156
    return-void
.end method

.method public renderPostView()V
    .locals 9

    .prologue
    const-wide/16 v2, 0x0

    .line 1159
    const-string v0, "Panorama360Callback"

    const-string v1, "renderPostView"

    invoke-static {v0, v1}, Lcom/sec/android/app/camera/panorama360/LogFilter;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1160
    iget-object v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mMorphoImageStitcher:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    sget v8, Lcom/sec/android/app/camera/panorama360/Panorama360Property;->DiplayType:I

    move-wide v4, v2

    invoke-virtual/range {v1 .. v8}, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;->renderPostview(DDDI)I

    .line 1161
    return-void
.end method

.method public renderPreview([BIII)I
    .locals 6
    .param p1, "input_img"    # [B
    .param p2, "image_id"    # I
    .param p3, "rotation"    # I
    .param p4, "device_rotation"    # I

    .prologue
    .line 1169
    const-string v0, "Panorama360Callback"

    const-string v1, "renderPreview"

    invoke-static {v0, v1}, Lcom/sec/android/app/camera/panorama360/LogFilter;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1170
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mMorphoImageStitcher:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;

    sget v3, Lcom/sec/android/app/camera/panorama360/Panorama360Property;->DiplayType:I

    move-object v1, p1

    move v2, p2

    move v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;->renderPreview([BIIII)I

    move-result v0

    return v0
.end method

.method public renderPreviewWithAnimation([BIIIII)I
    .locals 8
    .param p1, "input_img"    # [B
    .param p2, "image_id"    # I
    .param p3, "rotation"    # I
    .param p4, "animation_type"    # I
    .param p5, "animation_progress"    # I
    .param p6, "device_rotate"    # I

    .prologue
    .line 1164
    const-string v0, "Panorama360Callback"

    const-string v1, "renderPreviewWithAnimation"

    invoke-static {v0, v1}, Lcom/sec/android/app/camera/panorama360/LogFilter;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1165
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mMorphoImageStitcher:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;

    sget v3, Lcom/sec/android/app/camera/panorama360/Panorama360Property;->DiplayType:I

    move-object v1, p1

    move v2, p2

    move v4, p3

    move v5, p4

    move v6, p5

    move v7, p6

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;->renderPreviewWithAnimation([BIIIIII)I

    move-result v0

    return v0
.end method

.method public resetGL()V
    .locals 2

    .prologue
    .line 1202
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mMorphoImageStitcher:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;->resetGL(I)I

    .line 1203
    return-void
.end method

.method public restartInactivityTimer()V
    .locals 2

    .prologue
    .line 1411
    const-string v0, "Panorama360Callback"

    const-string v1, "pc restartInactivityTimer"

    invoke-static {v0, v1}, Lcom/sec/android/app/camera/panorama360/LogFilter;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1412
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    if-eqz v0, :cond_0

    .line 1413
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->restartInactivityTimer()V

    .line 1414
    :cond_0
    return-void
.end method

.method public resumeFinishShooting()V
    .locals 2

    .prologue
    .line 1302
    const-string v0, "Panorama360Callback"

    const-string v1, "pc resumeFinishShooting"

    invoke-static {v0, v1}, Lcom/sec/android/app/camera/panorama360/LogFilter;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1303
    invoke-direct {p0}, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->getInstanceProgressingPopup()Lcom/sec/android/app/camera/glwidget/TwGLProgressingPopup;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mProgressPopup:Lcom/sec/android/app/camera/glwidget/TwGLProgressingPopup;

    .line 1305
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanorama360View:Lcom/sec/android/app/camera/panorama360/Panorama360View;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/panorama360/Panorama360View;->pvFinishShooting()V

    .line 1307
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x101

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1309
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mProgressPopup:Lcom/sec/android/app/camera/glwidget/TwGLProgressingPopup;

    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, Lcom/sec/android/app/camera/glwidget/TwGLProgressingPopup;->setPostCaptureProgressMax(I)V

    .line 1310
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mProgressPopup:Lcom/sec/android/app/camera/glwidget/TwGLProgressingPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/glwidget/TwGLProgressingPopup;->showPostCaptureLayout()V

    .line 1311
    return-void
.end method

.method public saveOutputImage(Landroid/graphics/Rect;I)I
    .locals 36
    .param p1, "rect"    # Landroid/graphics/Rect;
    .param p2, "orientation"    # I

    .prologue
    .line 929
    const-string v4, "Panorama360Callback"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "pc saveOutputImage : "

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, p2

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v10, ", "

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    move-result v10

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v10, "x"

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->hashCode()I

    move-result v10

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/sec/android/app/camera/panorama360/LogFilter;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 930
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->getSavePath()Ljava/lang/String;

    move-result-object v32

    .line 931
    .local v32, "directory":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->getSaveFileName()Ljava/lang/String;

    move-result-object v34

    .line 932
    .local v34, "filename":Ljava/lang/String;
    const/4 v4, 0x1

    new-array v7, v4, [I

    .line 934
    .local v7, "output_size":[I
    const-string v4, "Panorama360Callback"

    const-string v6, "pc saveOutputImage"

    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 935
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mDateTaken:[J

    const/4 v6, 0x0

    aget-wide v12, v4, v6

    invoke-static {v12, v13}, Lcom/sec/android/app/camera/panorama360/core/MediaProviderUtils;->createDateStringForAppSeg(J)Ljava/lang/String;

    move-result-object v8

    .line 936
    .local v8, "first_date":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mDateTaken:[J

    const/4 v6, 0x1

    aget-wide v12, v4, v6

    invoke-static {v12, v13}, Lcom/sec/android/app/camera/panorama360/core/MediaProviderUtils;->createDateStringForAppSeg(J)Ljava/lang/String;

    move-result-object v9

    .line 937
    .local v9, "last_date":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v32

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v6, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v34

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 938
    .local v5, "path":Ljava/lang/String;
    const-wide/16 v21, 0x0

    .line 939
    .local v21, "cityId":J
    const/16 v23, 0x0

    .line 941
    .local v23, "weatherId":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v4}, Lcom/sec/android/app/camera/Camera;->getEngine()Lcom/sec/android/app/camera/CommonEngine;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/camera/CommonEngine;->getGpsLocation()Landroid/location/Location;

    move-result-object v18

    .line 942
    .local v18, "location":Landroid/location/Location;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mDateTaken:[J

    const/4 v6, 0x1

    aget-wide v12, v4, v6

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v18

    invoke-virtual {v0, v12, v13, v1, v2}, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->createExifData(JLandroid/graphics/Rect;Landroid/location/Location;)Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$ExifData;

    move-result-object v11

    .line 943
    .local v11, "exifData":Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$ExifData;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mMorphoImageStitcher:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;

    const/4 v10, 0x1

    move/from16 v6, p2

    invoke-virtual/range {v4 .. v11}, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;->saveCreatedOutputImage(Ljava/lang/String;I[ILjava/lang/String;Ljava/lang/String;ZLcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher$ExifData;)I

    move-result v35

    .line 945
    .local v35, "ret":I
    if-eqz v35, :cond_0

    .line 946
    const-string v4, "Panorama360Callback"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "mMorphoImageStitcher.saveOutputJpeg error ret:"

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v35

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/sec/android/app/camera/panorama360/LogFilter;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 966
    :goto_0
    return v35

    .line 950
    :cond_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v4}, Lcom/sec/android/app/camera/Camera;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v12

    const-string v14, "image/jpeg"

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mDateTaken:[J

    const/4 v6, 0x0

    aget-wide v16, v4, v6

    move-object/from16 v0, p1

    iget v4, v0, Landroid/graphics/Rect;->right:I

    move-object/from16 v0, p1

    iget v6, v0, Landroid/graphics/Rect;->left:I

    sub-int v19, v4, v6

    move-object/from16 v0, p1

    iget v4, v0, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v0, p1

    iget v6, v0, Landroid/graphics/Rect;->top:I

    sub-int v20, v4, v6

    move-object v13, v5

    invoke-static/range {v12 .. v23}, Lcom/sec/android/app/camera/panorama360/core/MediaProviderUtils;->addImageExternal(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;IJLandroid/location/Location;IIJI)Landroid/net/Uri;

    move-result-object v28

    .line 952
    .local v28, "uri":Landroid/net/Uri;
    if-eqz v28, :cond_1

    .line 953
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    move-object/from16 v0, v28

    invoke-virtual {v4, v0}, Lcom/sec/android/app/camera/Camera;->setLastContentUri(Landroid/net/Uri;)V

    .line 955
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v4}, Lcom/sec/android/app/camera/Camera;->getPackageName()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mCameraWidth:I

    move/from16 v26, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mCameraHeight:I

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v4}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/camera/CameraSettings;->getShootingModeType()I

    move-result v29

    const/16 v30, 0x25

    const/16 v31, 0x0

    move/from16 v25, p2

    invoke-static/range {v24 .. v31}, Lcom/sec/android/app/camera/ContextProviderUtils;->getTakePhotoSet(Ljava/lang/String;IIILandroid/net/Uri;III)Landroid/os/Bundle;

    move-result-object v33

    .line 957
    .local v33, "extras":Landroid/os/Bundle;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    move-object/from16 v0, v28

    move-object/from16 v1, v33

    invoke-static {v4, v0, v1}, Lcom/sec/android/app/camera/Util;->broadcastNewPicture(Landroid/content/Context;Landroid/net/Uri;Landroid/os/Bundle;)V

    .line 963
    .end local v33    # "extras":Landroid/os/Bundle;
    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v4}, Lcom/sec/android/app/camera/Camera;->updateThumbnailButton()V

    .line 964
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v4}, Lcom/sec/android/app/camera/Camera;->showThumbnailButton()V

    goto :goto_0
.end method

.method public setAngleMatrix(Lcom/sec/android/app/camera/panorama360/Panorama360View$RenderInfo;)V
    .locals 3
    .param p1, "info"    # Lcom/sec/android/app/camera/panorama360/Panorama360View$RenderInfo;

    .prologue
    .line 1110
    const-string v0, "Panorama360Callback"

    const-string v1, "setAngleMatrix"

    invoke-static {v0, v1}, Lcom/sec/android/app/camera/panorama360/LogFilter;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1111
    iget-boolean v0, p1, Lcom/sec/android/app/camera/panorama360/Panorama360View$RenderInfo;->use_gr_mat:Z

    if-eqz v0, :cond_0

    .line 1112
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mMorphoImageStitcher:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;

    iget-object v1, p1, Lcom/sec/android/app/camera/panorama360/Panorama360View$RenderInfo;->gr_mat:[D

    sget v2, Lcom/sec/android/app/camera/panorama360/Panorama360Property;->GyroscopeType:I

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;->setAngleMatrix([DI)I

    .line 1114
    :cond_0
    iget-boolean v0, p1, Lcom/sec/android/app/camera/panorama360/Panorama360View$RenderInfo;->use_rv_mat:Z

    if-eqz v0, :cond_1

    .line 1115
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mMorphoImageStitcher:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;

    iget-object v1, p1, Lcom/sec/android/app/camera/panorama360/Panorama360View$RenderInfo;->rv_mat:[D

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;->setAngleMatrix([DI)I

    .line 1117
    :cond_1
    iget-boolean v0, p1, Lcom/sec/android/app/camera/panorama360/Panorama360View$RenderInfo;->use_ac_mat:Z

    if-eqz v0, :cond_2

    .line 1118
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mMorphoImageStitcher:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;

    iget-object v1, p1, Lcom/sec/android/app/camera/panorama360/Panorama360View$RenderInfo;->ac_mat:[D

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;->setAngleMatrix([DI)I

    .line 1120
    :cond_2
    return-void
.end method

.method public setDrawBgBlankMode(I)V
    .locals 1
    .param p1, "mode"    # I

    .prologue
    .line 1206
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mMorphoImageStitcher:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;->setDrawBgBlankMode(I)I

    .line 1207
    return-void
.end method

.method public startRender()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1123
    const-string v0, "Panorama360Callback"

    const-string v1, "startRender"

    invoke-static {v0, v1}, Lcom/sec/android/app/camera/panorama360/LogFilter;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1124
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getEngine()Lcom/sec/android/app/camera/CommonEngine;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/camera/CommonEngine;->setPreviewCallback(Lcom/sec/android/app/camera/CommonEngine$CommonEnginePreviewCallback;)V

    .line 1125
    iget v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanoramaState:I

    if-nez v0, :cond_0

    .line 1126
    const-string v0, "Panorama360Callback"

    const-string v1, "onShow PANORAMA_STATE_UNINITIALIZED call mPanorama360Callback.pcPanoramaInitialize()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1127
    invoke-virtual {p0}, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->pcPanoramaInitialize()V

    .line 1128
    const-string v0, "Panorama360Callback"

    const-string v1, "onShow PANORAMA_STATE_UNINITIALIZED back from mPanorama360Callback.pcPanoramaInitialize()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1129
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanorama360View:Lcom/sec/android/app/camera/panorama360/Panorama360View;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/camera/panorama360/Panorama360View;->pvSetDrawBgBlankMode(I)V

    .line 1130
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanorama360View:Lcom/sec/android/app/camera/panorama360/Panorama360View;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/camera/panorama360/Panorama360View;->pvSetAnimationEnable(Z)V

    .line 1131
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanorama360View:Lcom/sec/android/app/camera/panorama360/Panorama360View;

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/camera/panorama360/Panorama360View;->pvSetAnimationInfo(II)V

    .line 1132
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanorama360View:Lcom/sec/android/app/camera/panorama360/Panorama360View;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/camera/panorama360/Panorama360View;->pvSetRenderEnable(Z)V

    .line 1138
    :goto_0
    return-void

    .line 1134
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanorama360View:Lcom/sec/android/app/camera/panorama360/Panorama360View;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/camera/panorama360/Panorama360View;->pvSetDrawBgBlankMode(I)V

    .line 1135
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanorama360View:Lcom/sec/android/app/camera/panorama360/Panorama360View;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/camera/panorama360/Panorama360View;->pvSetAnimationEnable(Z)V

    .line 1136
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanorama360View:Lcom/sec/android/app/camera/panorama360/Panorama360View;

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/camera/panorama360/Panorama360View;->pvSetAnimationInfo(II)V

    goto :goto_0
.end method

.method public stitcherCreateOutputImage(Landroid/graphics/Rect;Ljava/lang/Object;[I)I
    .locals 6
    .param p1, "rect"    # Landroid/graphics/Rect;
    .param p2, "listener"    # Ljava/lang/Object;
    .param p3, "crect"    # [I

    .prologue
    .line 1375
    const-string v0, "Panorama360Callback"

    const-string v1, "pc stitcherCreateOutputImage"

    invoke-static {v0, v1}, Lcom/sec/android/app/camera/panorama360/LogFilter;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1376
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mMorphoImageStitcher:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;

    sget v4, Lcom/sec/android/app/camera/panorama360/Panorama360Property;->OutputType:I

    iget-boolean v5, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mAllGuideTaken:Z

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;->createOutputImage(Landroid/graphics/Rect;Ljava/lang/Object;[IIZ)I

    move-result v0

    return v0
.end method

.method public stitcherEnd()I
    .locals 2

    .prologue
    .line 1366
    const-string v0, "Panorama360Callback"

    const-string v1, "pc stitcherEnd"

    invoke-static {v0, v1}, Lcom/sec/android/app/camera/panorama360/LogFilter;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1367
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mMorphoImageStitcher:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;->end()I

    move-result v0

    return v0
.end method

.method public stitcherGetBound(Landroid/graphics/Rect;)I
    .locals 1
    .param p1, "rect"    # Landroid/graphics/Rect;

    .prologue
    .line 1371
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mMorphoImageStitcher:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;->getBoundingRect(Landroid/graphics/Rect;)I

    move-result v0

    return v0
.end method

.method public stop()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1265
    const-string v0, "Panorama360Callback"

    const-string v1, "pc stop"

    invoke-static {v0, v1}, Lcom/sec/android/app/camera/panorama360/LogFilter;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1266
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v2, v2}, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->pcStopShooting(ZZZ)V

    .line 1267
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mPanorama360View:Lcom/sec/android/app/camera/panorama360/Panorama360View;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/camera/panorama360/Panorama360View;->pvSetRenderEnable(Z)V

    .line 1269
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mMorphoImageStitcher:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;

    if-eqz v0, :cond_0

    .line 1270
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    new-instance v1, Lcom/sec/android/app/camera/panorama360/Panorama360Callback$ReleaseMemory;

    iget-object v2, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mMorphoImageStitcher:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;

    invoke-direct {v1, v2}, Lcom/sec/android/app/camera/panorama360/Panorama360Callback$ReleaseMemory;-><init>(Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/camera/Camera;->setGLQueueEvent(Ljava/lang/Runnable;)V

    .line 1271
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mMorphoImageStitcher:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;

    .line 1274
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->pcRelease()V

    .line 1275
    return-void
.end method

.method public stopFinishShootingAsync()V
    .locals 2

    .prologue
    .line 1280
    const-string v0, "Panorama360Callback"

    const-string v1, "pc stopFinishShootingAsync"

    invoke-static {v0, v1}, Lcom/sec/android/app/camera/panorama360/LogFilter;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1281
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mFinishShootingAsync:Lcom/sec/android/app/camera/panorama360/FinishShootingAsync;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mFinishShootingAsync:Lcom/sec/android/app/camera/panorama360/FinishShootingAsync;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/panorama360/FinishShootingAsync;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_0

    .line 1282
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mFinishShootingAsync:Lcom/sec/android/app/camera/panorama360/FinishShootingAsync;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/camera/panorama360/FinishShootingAsync;->cancel(Z)Z

    .line 1284
    :cond_0
    return-void
.end method

.method public undo()V
    .locals 2

    .prologue
    .line 1253
    const-string v0, "Panorama360Callback"

    const-string v1, "pc undo"

    invoke-static {v0, v1}, Lcom/sec/android/app/camera/panorama360/LogFilter;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1254
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mMorphoImageStitcher:Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/panorama360/core/MorphoImageStitcher;->undo()I

    .line 1255
    invoke-static {}, Lcom/sec/android/app/camera/panorama360/Panorama360DebugUtils;->undo()V

    .line 1256
    return-void
.end method

.method public unregisterSensorManager()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 111
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mSensorManager:Landroid/hardware/SensorManager;

    if-eqz v0, :cond_0

    .line 112
    iget-object v0, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->mSensorManager:Landroid/hardware/SensorManager;

    invoke-virtual {v0, p0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 113
    :cond_0
    iput-boolean v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->registered_accelerometer:Z

    .line 114
    iput-boolean v1, p0, Lcom/sec/android/app/camera/panorama360/Panorama360Callback;->registered_gyroscope:Z

    .line 115
    return-void
.end method
