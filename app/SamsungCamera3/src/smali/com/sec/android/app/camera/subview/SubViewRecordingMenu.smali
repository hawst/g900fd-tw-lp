.class public Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;
.super Lcom/sec/android/app/camera/subview/SubViewBase;
.source "SubViewRecordingMenu.java"


# static fields
.field private static final BENDEDUI_SHUTTER_BUTTON_MOVING_THRESHOLD:F = 100.0f

.field private static final DP_TO_PIXEL:I = 0x4

.field private static final PADDING:I = 0x50

.field private static final PAUSE_BUTTON_WIDTH:I = 0xa0

.field private static final POS_LANDSCAPE:[Landroid/graphics/PointF;

.field private static final POS_LANDSCAPE_270:[Landroid/graphics/PointF;

.field private static final POS_LANDSCAPE_90:[Landroid/graphics/PointF;

.field private static final POS_PORTRAIT:[Landroid/graphics/PointF;

.field private static final POS_PORTRAIT_0:[Landroid/graphics/PointF;

.field private static final POS_PORTRAIT_180:[Landroid/graphics/PointF;

.field private static final SHUTTER_BUTTON_WIDTH:I = 0x1ec

.field private static final STOP_BUTTON_WIDTH:I = 0xa0

.field private static final TAG:Ljava/lang/String; = "SubViewRecordingMenu"


# instance fields
.field private mCAFButton:Landroid/widget/ImageView;

.field private mDualButton:Landroid/widget/ImageView;

.field private mInitTouchPoint:Landroid/graphics/PointF;

.field protected mOrientationListener:Landroid/view/OrientationEventListener;

.field private mPauseButton:Landroid/widget/ImageView;

.field private mRecordingDualMode:Z

.field private mRecordingMenu:Landroid/view/ViewGroup;

.field private mShutterButton:Landroid/widget/ImageView;

.field private mShutterButtonBg:Landroid/widget/ImageView;

.field private mShutterLayout:Landroid/view/ViewGroup;

.field private mStopButton:Landroid/widget/ImageView;

.field mSubViewOnTouchListener:Landroid/view/View$OnTouchListener;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    const v5, 0x44bc8000    # 1508.0f

    const/4 v4, 0x0

    .line 55
    const/4 v0, 0x4

    new-array v0, v0, [Landroid/graphics/PointF;

    new-instance v1, Landroid/graphics/PointF;

    const/high16 v2, 0x44960000    # 1200.0f

    invoke-direct {v1, v2, v4}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v1, v0, v6

    new-instance v1, Landroid/graphics/PointF;

    invoke-direct {v1, v5, v4}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v1, v0, v7

    new-instance v1, Landroid/graphics/PointF;

    const/high16 v2, 0x44960000    # 1200.0f

    invoke-direct {v1, v2, v4}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v1, v0, v8

    const/4 v1, 0x3

    new-instance v2, Landroid/graphics/PointF;

    invoke-direct {v2, v5, v4}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->POS_LANDSCAPE:[Landroid/graphics/PointF;

    .line 58
    const/4 v0, 0x4

    new-array v0, v0, [Landroid/graphics/PointF;

    new-instance v1, Landroid/graphics/PointF;

    const/high16 v2, 0x44960000    # 1200.0f

    invoke-direct {v1, v4, v2}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v1, v0, v6

    new-instance v1, Landroid/graphics/PointF;

    invoke-direct {v1, v4, v5}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v1, v0, v7

    new-instance v1, Landroid/graphics/PointF;

    const/high16 v2, 0x44960000    # 1200.0f

    invoke-direct {v1, v4, v2}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v1, v0, v8

    const/4 v1, 0x3

    new-instance v2, Landroid/graphics/PointF;

    invoke-direct {v2, v4, v5}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->POS_PORTRAIT:[Landroid/graphics/PointF;

    .line 62
    const/4 v0, 0x4

    new-array v0, v0, [Landroid/graphics/PointF;

    new-instance v1, Landroid/graphics/PointF;

    const/high16 v2, 0x43f00000    # 480.0f

    invoke-direct {v1, v2, v4}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v1, v0, v6

    new-instance v1, Landroid/graphics/PointF;

    const/high16 v2, 0x42a00000    # 80.0f

    invoke-direct {v1, v2, v4}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v1, v0, v7

    new-instance v1, Landroid/graphics/PointF;

    const/high16 v2, 0x43f00000    # 480.0f

    invoke-direct {v1, v2, v4}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v1, v0, v8

    const/4 v1, 0x3

    new-instance v2, Landroid/graphics/PointF;

    const/high16 v3, 0x42a00000    # 80.0f

    invoke-direct {v2, v3, v4}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->POS_LANDSCAPE_90:[Landroid/graphics/PointF;

    .line 65
    const/4 v0, 0x4

    new-array v0, v0, [Landroid/graphics/PointF;

    new-instance v1, Landroid/graphics/PointF;

    const/high16 v2, 0x448c0000    # 1120.0f

    invoke-direct {v1, v2, v4}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v1, v0, v6

    new-instance v1, Landroid/graphics/PointF;

    invoke-direct {v1, v5, v4}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v1, v0, v7

    new-instance v1, Landroid/graphics/PointF;

    const/high16 v2, 0x448c0000    # 1120.0f

    invoke-direct {v1, v2, v4}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v1, v0, v8

    const/4 v1, 0x3

    new-instance v2, Landroid/graphics/PointF;

    invoke-direct {v2, v5, v4}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->POS_LANDSCAPE_270:[Landroid/graphics/PointF;

    .line 68
    const/4 v0, 0x4

    new-array v0, v0, [Landroid/graphics/PointF;

    new-instance v1, Landroid/graphics/PointF;

    const/high16 v2, 0x43ea0000    # 468.0f

    invoke-direct {v1, v4, v2}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v1, v0, v6

    new-instance v1, Landroid/graphics/PointF;

    const/high16 v2, 0x42a00000    # 80.0f

    invoke-direct {v1, v4, v2}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v1, v0, v7

    new-instance v1, Landroid/graphics/PointF;

    const/high16 v2, 0x43ea0000    # 468.0f

    invoke-direct {v1, v4, v2}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v1, v0, v8

    const/4 v1, 0x3

    new-instance v2, Landroid/graphics/PointF;

    const/high16 v3, 0x42a00000    # 80.0f

    invoke-direct {v2, v4, v3}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->POS_PORTRAIT_180:[Landroid/graphics/PointF;

    .line 71
    const/4 v0, 0x4

    new-array v0, v0, [Landroid/graphics/PointF;

    new-instance v1, Landroid/graphics/PointF;

    const/high16 v2, 0x44820000    # 1040.0f

    invoke-direct {v1, v4, v2}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v1, v0, v6

    new-instance v1, Landroid/graphics/PointF;

    const v2, 0x44b28000    # 1428.0f

    invoke-direct {v1, v4, v2}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v1, v0, v7

    new-instance v1, Landroid/graphics/PointF;

    const/high16 v2, 0x44820000    # 1040.0f

    invoke-direct {v1, v4, v2}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v1, v0, v8

    const/4 v1, 0x3

    new-instance v2, Landroid/graphics/PointF;

    const v3, 0x44b28000    # 1428.0f

    invoke-direct {v2, v4, v3}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->POS_PORTRAIT_0:[Landroid/graphics/PointF;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/camera/Camera;Lcom/sec/android/app/camera/subview/SubViewManager;)V
    .locals 1
    .param p1, "activitycontext"    # Lcom/sec/android/app/camera/Camera;
    .param p2, "subviewmanager"    # Lcom/sec/android/app/camera/subview/SubViewManager;

    .prologue
    .line 87
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/camera/subview/SubViewBase;-><init>(Lcom/sec/android/app/camera/Camera;Lcom/sec/android/app/camera/subview/SubViewManager;)V

    .line 81
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mOrientationListener:Landroid/view/OrientationEventListener;

    .line 83
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mInitTouchPoint:Landroid/graphics/PointF;

    .line 176
    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu$1;-><init>(Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;)V

    iput-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mSubViewOnTouchListener:Landroid/view/View$OnTouchListener;

    .line 89
    invoke-virtual {p0}, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->setOrientationListener()V

    .line 90
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mOrientationListener:Landroid/view/OrientationEventListener;

    if-eqz v0, :cond_0

    .line 91
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mOrientationListener:Landroid/view/OrientationEventListener;

    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->enable()V

    .line 94
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->initialize()V

    .line 95
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;)Landroid/graphics/PointF;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mInitTouchPoint:Landroid/graphics/PointF;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mStopButton:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mPauseButton:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mCAFButton:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mDualButton:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mRecordingDualMode:Z

    return v0
.end method

.method static synthetic access$502(Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;
    .param p1, "x1"    # Z

    .prologue
    .line 40
    iput-boolean p1, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mRecordingDualMode:Z

    return p1
.end method

.method static synthetic access$600(Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;)Landroid/view/ViewGroup;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mRecordingMenu:Landroid/view/ViewGroup;

    return-object v0
.end method

.method private isPauseButtonEnabled()Z
    .locals 2

    .prologue
    .line 632
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->isRecordingSpeedControlEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->getShootingMode()I

    move-result v0

    const/16 v1, 0x26

    if-ne v0, v1, :cond_1

    .line 634
    :cond_0
    const/4 v0, 0x0

    .line 636
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private isShutterButtonEnabled()Z
    .locals 2

    .prologue
    .line 621
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->getShootingMode()I

    move-result v0

    const/16 v1, 0x2f

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->isSingleEffect()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->getCamcorderResolution()I

    move-result v0

    const/16 v1, 0x28

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->getCamcorderResolution()I

    move-result v0

    const/16 v1, 0x15

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->getCamcorderRecordingMode()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->getShootingMode()I

    move-result v0

    const/16 v1, 0x26

    if-ne v0, v1, :cond_1

    .line 626
    :cond_0
    const/4 v0, 0x0

    .line 628
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private translateMenu()V
    .locals 11

    .prologue
    const/16 v10, 0x23c

    const/16 v9, 0x1e0

    const/16 v8, 0xf0

    const/16 v7, 0xb4

    const/4 v6, 0x0

    .line 438
    const-string v3, "SubViewRecordingMenu"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "translateMenu : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mOrientationMainLCD:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mLastOrientation:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 439
    iget-boolean v3, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mEasyMode:Z

    if-eqz v3, :cond_1

    .line 532
    :cond_0
    :goto_0
    return-void

    .line 442
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mShutterLayout:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 443
    .local v1, "pShutterLayout":Landroid/view/ViewGroup$MarginLayoutParams;
    iget-object v3, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mStopButton:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 444
    .local v2, "pStopLayout":Landroid/view/ViewGroup$MarginLayoutParams;
    iget-object v3, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mPauseButton:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 447
    .local v0, "pPauseLayout":Landroid/view/ViewGroup$MarginLayoutParams;
    iget v3, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mOrientationMainLCD:I

    const/16 v4, 0x5a

    if-eq v3, v4, :cond_2

    iget v3, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mOrientationMainLCD:I

    const/16 v4, 0x10e

    if-ne v3, v4, :cond_8

    .line 448
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mRecordingMenu:Landroid/view/ViewGroup;

    if-eqz v3, :cond_0

    .line 449
    iget v3, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mOrientationMainLCD:I

    const/16 v4, 0x5a

    if-ne v3, v4, :cond_5

    .line 450
    iget-object v3, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mRecordingMenu:Landroid/view/ViewGroup;

    sget-object v4, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->POS_LANDSCAPE_90:[Landroid/graphics/PointF;

    iget v5, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mLastOrientation:I

    div-int/lit8 v5, v5, 0x5a

    aget-object v4, v4, v5

    iget v4, v4, Landroid/graphics/PointF;->x:F

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->setX(F)V

    .line 452
    iget v3, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mLastOrientation:I

    if-eqz v3, :cond_3

    iget v3, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mLastOrientation:I

    if-ne v3, v7, :cond_4

    .line 454
    :cond_3
    invoke-virtual {v0, v6, v6, v6, v6}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 455
    invoke-virtual {v2, v8, v6, v6, v6}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 456
    invoke-virtual {v1, v9, v6, v6, v6}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 482
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mShutterLayout:Landroid/view/ViewGroup;

    invoke-virtual {v3, v1}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 483
    iget-object v3, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mPauseButton:Landroid/widget/ImageView;

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 484
    iget-object v3, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mStopButton:Landroid/widget/ImageView;

    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 486
    iget-object v3, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mRecordingMenu:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->invalidate()V

    goto :goto_0

    .line 460
    :cond_4
    invoke-virtual {v1, v6, v6, v6, v6}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 461
    invoke-virtual {v2, v10, v6, v6, v6}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 462
    const/16 v3, 0x32c

    invoke-virtual {v0, v3, v6, v6, v6}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    goto :goto_1

    .line 465
    :cond_5
    iget-object v3, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mRecordingMenu:Landroid/view/ViewGroup;

    sget-object v4, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->POS_LANDSCAPE_270:[Landroid/graphics/PointF;

    iget v5, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mLastOrientation:I

    div-int/lit8 v5, v5, 0x5a

    aget-object v4, v4, v5

    iget v4, v4, Landroid/graphics/PointF;->x:F

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->setX(F)V

    .line 467
    iget v3, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mLastOrientation:I

    if-eqz v3, :cond_6

    iget v3, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mLastOrientation:I

    if-ne v3, v7, :cond_7

    .line 469
    :cond_6
    invoke-virtual {v1, v6, v6, v6, v6}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 470
    invoke-virtual {v2, v10, v6, v6, v6}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 471
    const/16 v3, 0x32c

    invoke-virtual {v0, v3, v6, v6, v6}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    goto :goto_1

    .line 475
    :cond_7
    invoke-virtual {v0, v6, v6, v6, v6}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 476
    invoke-virtual {v2, v8, v6, v6, v6}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 477
    invoke-virtual {v1, v9, v6, v6, v6}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    goto :goto_1

    .line 489
    :cond_8
    iget-object v3, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mRecordingMenu:Landroid/view/ViewGroup;

    if-eqz v3, :cond_0

    .line 490
    iget v3, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mOrientationMainLCD:I

    if-nez v3, :cond_b

    .line 491
    iget-object v3, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mRecordingMenu:Landroid/view/ViewGroup;

    sget-object v4, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->POS_PORTRAIT_0:[Landroid/graphics/PointF;

    iget v5, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mLastOrientation:I

    div-int/lit8 v5, v5, 0x5a

    aget-object v4, v4, v5

    iget v4, v4, Landroid/graphics/PointF;->y:F

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->setY(F)V

    .line 493
    iget v3, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mLastOrientation:I

    if-eqz v3, :cond_9

    iget v3, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mLastOrientation:I

    if-ne v3, v7, :cond_a

    .line 495
    :cond_9
    invoke-virtual {v1, v6, v6, v6, v6}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 496
    invoke-virtual {v2, v6, v10, v6, v6}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 497
    const/16 v3, 0x32c

    invoke-virtual {v0, v6, v3, v6, v6}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 523
    :goto_2
    iget-object v3, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mShutterLayout:Landroid/view/ViewGroup;

    invoke-virtual {v3, v1}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 524
    iget-object v3, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mStopButton:Landroid/widget/ImageView;

    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 525
    iget-object v3, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mPauseButton:Landroid/widget/ImageView;

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 527
    iget-object v3, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mRecordingMenu:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->invalidate()V

    goto/16 :goto_0

    .line 501
    :cond_a
    invoke-virtual {v1, v6, v9, v6, v6}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 502
    invoke-virtual {v2, v6, v8, v6, v6}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 503
    invoke-virtual {v0, v6, v6, v6, v6}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    goto :goto_2

    .line 507
    :cond_b
    iget-object v3, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mRecordingMenu:Landroid/view/ViewGroup;

    sget-object v4, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->POS_PORTRAIT_180:[Landroid/graphics/PointF;

    iget v5, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mLastOrientation:I

    div-int/lit8 v5, v5, 0x5a

    aget-object v4, v4, v5

    iget v4, v4, Landroid/graphics/PointF;->y:F

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->setY(F)V

    .line 509
    iget v3, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mLastOrientation:I

    if-eqz v3, :cond_c

    iget v3, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mLastOrientation:I

    if-ne v3, v7, :cond_d

    .line 511
    :cond_c
    invoke-virtual {v0, v6, v6, v6, v6}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 512
    invoke-virtual {v2, v6, v8, v6, v6}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 513
    invoke-virtual {v1, v6, v9, v6, v6}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    goto :goto_2

    .line 517
    :cond_d
    const/16 v3, 0x32c

    invoke-virtual {v0, v6, v3, v6, v6}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 518
    invoke-virtual {v2, v6, v10, v6, v6}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 519
    invoke-virtual {v1, v6, v6, v6, v6}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    goto :goto_2
.end method


# virtual methods
.method public clear()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 140
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mOrientationListener:Landroid/view/OrientationEventListener;

    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mOrientationListener:Landroid/view/OrientationEventListener;

    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->disable()V

    .line 142
    iput-object v1, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mOrientationListener:Landroid/view/OrientationEventListener;

    .line 144
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mRecordingMenu:Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    .line 145
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mRecordingMenu:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 147
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mShutterLayout:Landroid/view/ViewGroup;

    if-eqz v0, :cond_2

    .line 148
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mShutterLayout:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 149
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mShutterLayout:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 151
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mShutterButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_3

    .line 152
    iput-object v1, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mShutterButton:Landroid/widget/ImageView;

    .line 154
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mShutterButtonBg:Landroid/widget/ImageView;

    if-eqz v0, :cond_4

    .line 155
    iput-object v1, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mShutterButtonBg:Landroid/widget/ImageView;

    .line 157
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mStopButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_5

    .line 158
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mStopButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 159
    iput-object v1, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mStopButton:Landroid/widget/ImageView;

    .line 161
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mPauseButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_6

    .line 162
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mPauseButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 163
    iput-object v1, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mPauseButton:Landroid/widget/ImageView;

    .line 165
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mCAFButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_7

    .line 166
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mCAFButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 167
    iput-object v1, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mCAFButton:Landroid/widget/ImageView;

    .line 169
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mDualButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_8

    .line 170
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mDualButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 171
    iput-object v1, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mDualButton:Landroid/widget/ImageView;

    .line 173
    :cond_8
    invoke-super {p0}, Lcom/sec/android/app/camera/subview/SubViewBase;->clear()V

    .line 174
    return-void
.end method

.method public handleDimButtons()V
    .locals 2

    .prologue
    .line 535
    const-string v0, "SubViewRecordingMenu"

    const-string v1, "handleDimButtons"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 537
    invoke-direct {p0}, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->isShutterButtonEnabled()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->setShutterButtonEnabled(Z)Z

    .line 538
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mPauseButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 539
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mPauseButton:Landroid/widget/ImageView;

    invoke-direct {p0}, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->isPauseButtonEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 541
    :cond_0
    return-void
.end method

.method public initLayout()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 103
    invoke-super {p0}, Lcom/sec/android/app/camera/subview/SubViewBase;->initLayout()V

    .line 104
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mBaseLayout:Landroid/view/ViewGroup;

    const v1, 0x7f0e0047

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mRecordingMenu:Landroid/view/ViewGroup;

    .line 105
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mRecordingMenu:Landroid/view/ViewGroup;

    if-eqz v0, :cond_4

    .line 106
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mRecordingMenu:Landroid/view/ViewGroup;

    const v1, 0x7f0e0048

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mShutterLayout:Landroid/view/ViewGroup;

    .line 107
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mShutterLayout:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 108
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mShutterLayout:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mSubViewOnTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 109
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mShutterLayout:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setSoundEffectsEnabled(Z)V

    .line 110
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mRecordingMenu:Landroid/view/ViewGroup;

    const v1, 0x7f0e004a

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mShutterButton:Landroid/widget/ImageView;

    .line 111
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mRecordingMenu:Landroid/view/ViewGroup;

    const v1, 0x7f0e0049

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mShutterButtonBg:Landroid/widget/ImageView;

    .line 113
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mRecordingMenu:Landroid/view/ViewGroup;

    const v1, 0x7f0e004b

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mStopButton:Landroid/widget/ImageView;

    .line 114
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mStopButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 115
    iget-boolean v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mEasyMode:Z

    if-eqz v0, :cond_5

    .line 116
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mStopButton:Landroid/widget/ImageView;

    const v1, 0x7f020275

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 120
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mStopButton:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mSubViewOnTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 121
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mStopButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setSoundEffectsEnabled(Z)V

    .line 123
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mRecordingMenu:Landroid/view/ViewGroup;

    const v1, 0x7f0e004c

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mPauseButton:Landroid/widget/ImageView;

    .line 124
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mPauseButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    .line 125
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mPauseButton:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mSubViewOnTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 126
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mPauseButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setSoundEffectsEnabled(Z)V

    .line 128
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mBaseLayout:Landroid/view/ViewGroup;

    const v1, 0x7f0e0041

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mCAFButton:Landroid/widget/ImageView;

    .line 129
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mCAFButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_3

    .line 130
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mCAFButton:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mSubViewOnTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 132
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mBaseLayout:Landroid/view/ViewGroup;

    const v1, 0x7f0e0042

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mDualButton:Landroid/widget/ImageView;

    .line 133
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mDualButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_4

    .line 134
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mDualButton:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mSubViewOnTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 137
    :cond_4
    return-void

    .line 118
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mStopButton:Landroid/widget/ImageView;

    const v1, 0x7f020274

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method public initialize()V
    .locals 2

    .prologue
    .line 98
    const-string v0, "SubViewRecordingMenu"

    const-string v1, "initialize"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    invoke-virtual {p0}, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->initLayout()V

    .line 100
    return-void
.end method

.method public isPressedShutterButton()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 677
    iget-object v1, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mShutterButton:Landroid/widget/ImageView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mShutterButtonBg:Landroid/widget/ImageView;

    if-nez v1, :cond_2

    .line 678
    :cond_0
    const-string v1, "SubViewRecordingMenu"

    const-string v2, "ShutterButton is null"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 685
    :cond_1
    :goto_0
    return v0

    .line 682
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mShutterButton:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->isPressed()Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mShutterButtonBg:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->isPressed()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 683
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onHide()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 582
    const-string v0, "SubViewRecordingMenu"

    const-string v1, "onHide"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 583
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mOrientationListener:Landroid/view/OrientationEventListener;

    if-eqz v0, :cond_0

    .line 584
    const-string v0, "SubViewRecordingMenu"

    const-string v1, "orientation listener disabled"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 585
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mOrientationListener:Landroid/view/OrientationEventListener;

    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->disable()V

    .line 587
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mRecordingMenu:Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    .line 588
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mRecordingMenu:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 590
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mCAFButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    .line 591
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mCAFButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 593
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mDualButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_3

    .line 594
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mDualButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 596
    :cond_3
    invoke-super {p0}, Lcom/sec/android/app/camera/subview/SubViewBase;->onHide()V

    .line 597
    return-void
.end method

.method public onShow()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 544
    const-string v0, "SubViewRecordingMenu"

    const-string v1, "onShow"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 546
    invoke-virtual {p0}, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->setFixedOrientation()V

    .line 547
    invoke-virtual {p0}, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->getFixedOrientation()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->setLastOrientation(I)V

    .line 549
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->getShootingMode()I

    move-result v0

    const/16 v1, 0x2f

    if-ne v0, v1, :cond_5

    .line 550
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mCAFButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 551
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mCAFButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 553
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mDualButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 554
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mDualButton:Landroid/widget/ImageView;

    const v1, 0x7f02026e

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 555
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mRecordingDualMode:Z

    .line 556
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mDualButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 567
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mPauseButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    .line 568
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mPauseButton:Landroid/widget/ImageView;

    const v1, 0x7f02026b

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 570
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mRecordingMenu:Landroid/view/ViewGroup;

    if-eqz v0, :cond_3

    .line 571
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mRecordingMenu:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 573
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mOrientationListener:Landroid/view/OrientationEventListener;

    if-eqz v0, :cond_4

    .line 574
    const-string v0, "SubViewRecordingMenu"

    const-string v1, "orientation listener enabled"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 575
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mOrientationListener:Landroid/view/OrientationEventListener;

    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->enable()V

    .line 577
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->rotateImageButtons()V

    .line 578
    invoke-super {p0}, Lcom/sec/android/app/camera/subview/SubViewBase;->onShow()V

    .line 579
    return-void

    .line 559
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mCAFButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_6

    .line 560
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mCAFButton:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 562
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mDualButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 563
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mDualButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 564
    iput-boolean v2, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mRecordingDualMode:Z

    goto :goto_0
.end method

.method public rotateImageButtons()V
    .locals 1

    .prologue
    .line 398
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mPauseButton:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->rotateImages(Landroid/widget/ImageView;)V

    .line 399
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mShutterButton:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->rotateImages(Landroid/widget/ImageView;)V

    .line 400
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mCAFButton:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->rotateImages(Landroid/widget/ImageView;)V

    .line 401
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mDualButton:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->rotateImages(Landroid/widget/ImageView;)V

    .line 402
    return-void
.end method

.method public rotateLayout()V
    .locals 2

    .prologue
    .line 394
    const-string v0, "SubViewRecordingMenu"

    const-string v1, "rotateLayout"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 395
    invoke-virtual {p0}, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->initLayout()V

    .line 396
    return-void
.end method

.method public rotateMenu()V
    .locals 3

    .prologue
    .line 405
    const-string v0, "SubViewRecordingMenu"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "rotateMenu "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mLastOrientation:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mOrientationMainLCD:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 407
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mOrientationListener:Landroid/view/OrientationEventListener;

    if-eqz v0, :cond_0

    .line 408
    const-string v0, "SubViewRecordingMenu"

    const-string v1, "orientation listener disabled"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 409
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mOrientationListener:Landroid/view/OrientationEventListener;

    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->disable()V

    .line 411
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mRecordingMenu:Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    .line 412
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mRecordingMenu:Landroid/view/ViewGroup;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 415
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->translateMenu()V

    .line 417
    iget-boolean v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mEasyMode:Z

    if-nez v0, :cond_2

    .line 418
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mPauseButton:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->rotateImages(Landroid/widget/ImageView;)V

    .line 419
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mShutterButton:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->rotateImages(Landroid/widget/ImageView;)V

    .line 420
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mCAFButton:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->rotateImages(Landroid/widget/ImageView;)V

    .line 421
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mDualButton:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->rotateImages(Landroid/widget/ImageView;)V

    .line 424
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mRecordingMenu:Landroid/view/ViewGroup;

    if-eqz v0, :cond_3

    .line 425
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mRecordingMenu:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 427
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->isHelpMode()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mSubViewManager:Lcom/sec/android/app/camera/subview/SubViewManager;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewManager;->getTutorialRecordingMode()Lcom/sec/android/app/camera/subview/SubViewTutorialRecordingMode;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 428
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mSubViewManager:Lcom/sec/android/app/camera/subview/SubViewManager;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewManager;->getTutorialRecordingMode()Lcom/sec/android/app/camera/subview/SubViewTutorialRecordingMode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewTutorialRecordingMode;->rotateLayout()V

    .line 431
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mOrientationListener:Landroid/view/OrientationEventListener;

    if-eqz v0, :cond_5

    .line 432
    const-string v0, "SubViewRecordingMenu"

    const-string v1, "orientation listener enabled"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 433
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mOrientationListener:Landroid/view/OrientationEventListener;

    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->enable()V

    .line 435
    :cond_5
    return-void
.end method

.method public setCAFButtonVisibility(I)V
    .locals 3
    .param p1, "visibility"    # I

    .prologue
    .line 600
    const-string v0, "SubViewRecordingMenu"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setCAFButtonVisibility "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 601
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mCAFButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 602
    iget-object v1, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mCAFButton:Landroid/widget/ImageView;

    if-nez p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 604
    :cond_0
    return-void

    .line 602
    :cond_1
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public setDualButtonVisibility(I)V
    .locals 4
    .param p1, "visibility"    # I

    .prologue
    const/16 v0, 0x8

    .line 607
    const-string v1, "SubViewRecordingMenu"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setDualButtonVisibility "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 608
    iget-object v1, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mDualButton:Landroid/widget/ImageView;

    if-eqz v1, :cond_1

    .line 609
    iget-object v1, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mCAFButton:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getVisibility()I

    move-result v1

    if-eq v1, v0, :cond_2

    .line 610
    iget-object v1, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mCAFButton:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 614
    :goto_0
    if-nez p1, :cond_0

    const/4 v0, 0x0

    .line 615
    .local v0, "visible":I
    :cond_0
    const-string v1, "SubViewRecordingMenu"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setDualButtonVisibility "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 616
    iget-object v1, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mDualButton:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 618
    .end local v0    # "visible":I
    :cond_1
    return-void

    .line 612
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mCAFButton:Landroid/widget/ImageView;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method protected setOrientationListener()V
    .locals 2

    .prologue
    .line 362
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mOrientationListener:Landroid/view/OrientationEventListener;

    if-nez v0, :cond_0

    .line 363
    new-instance v0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu$2;

    iget-object v1, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu$2;-><init>(Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mOrientationListener:Landroid/view/OrientationEventListener;

    .line 390
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mOrientationListener:Landroid/view/OrientationEventListener;

    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->disable()V

    .line 391
    return-void
.end method

.method public setShutterButtonEnabled(Z)Z
    .locals 2
    .param p1, "bEnable"    # Z

    .prologue
    .line 656
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mShutterButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mShutterButtonBg:Landroid/widget/ImageView;

    if-nez v0, :cond_1

    .line 657
    :cond_0
    const-string v0, "SubViewRecordingMenu"

    const-string v1, "ShutterButton is null"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 658
    const/4 v0, 0x0

    .line 663
    :goto_0
    return v0

    .line 660
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mShutterButton:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 661
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mShutterButtonBg:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 662
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mShutterLayout:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->setEnabled(Z)V

    .line 663
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setShutterButtonPressed(Z)Z
    .locals 2
    .param p1, "bPress"    # Z

    .prologue
    .line 667
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mShutterButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mShutterButtonBg:Landroid/widget/ImageView;

    if-nez v0, :cond_1

    .line 668
    :cond_0
    const-string v0, "SubViewRecordingMenu"

    const-string v1, "ShutterButton is null"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 669
    const/4 v0, 0x0

    .line 673
    :goto_0
    return v0

    .line 671
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mShutterButton:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setPressed(Z)V

    .line 672
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mShutterButtonBg:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setPressed(Z)V

    .line 673
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setStepTutorialMode(I)V
    .locals 3
    .param p1, "mode"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 640
    const/4 v0, 0x2

    if-ne p1, v0, :cond_3

    .line 641
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mPauseButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 642
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mPauseButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 643
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mStopButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 644
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mStopButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 645
    :cond_1
    invoke-virtual {p0, v2}, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->setShutterButtonEnabled(Z)Z

    .line 653
    :cond_2
    :goto_0
    return-void

    .line 646
    :cond_3
    const/4 v0, 0x3

    if-ne p1, v0, :cond_2

    .line 647
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mPauseButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_4

    .line 648
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mPauseButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 649
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mStopButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_5

    .line 650
    iget-object v0, p0, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->mStopButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 651
    :cond_5
    invoke-virtual {p0, v1}, Lcom/sec/android/app/camera/subview/SubViewRecordingMenu;->setShutterButtonEnabled(Z)Z

    goto :goto_0
.end method
