.class public Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;
.super Lcom/sec/android/glview/TwGLViewGroup;
.source "TwGLTutorialRecordingMode.java"


# static fields
.field private static final CAMCORDER_BUTTON_X:F

.field private static final CAMCORDER_BUTTON_Y:F

.field private static final EASY_HELP_FOCUS_PADDING_Y:F

.field private static final EASY_RECORDING_STOP_BUTTON_Y:F

.field private static final FONT_COLOR:I

.field private static final FONT_SIZE:I

.field private static final HELP_POPUP_HEIGHT:F

.field private static final HELP_POPUP_PADDING:[F

.field private static final HELP_POPUP_PICKER_EXTRA_PADDING_2:F

.field private static final HELP_POPUP_PICKER_OFFSET:F

.field private static final HELP_POPUP_PICKER_PADDING:F

.field private static final HELP_POPUP_WIDTH:[F

.field private static final HELP_TEXT_HEIGHT:F

.field private static final HELP_TEXT_HEIGHT_MARGIN:F

.field private static final HELP_TEXT_WIDTH:[F

.field private static final HELP_TEXT_WIDTH_MARGIN:F

.field private static final RECORDING_BUTTON_GROUP_X:F

.field private static final RECORDING_SNAPSHOT_BUTTON_Y:F

.field private static final RECORDING_SNAPSHOT_MARGIN_Y:F

.field private static final RECORDING_STOP_BUTTON_Y:F

.field private static final RIGHT_SIDE_BUTTON_HEIGHT:F

.field private static final RIGHT_SIDE_BUTTON_WIDTH:F

.field private static final RIGHT_SIDE_MENU_X:F

.field private static final SCREEN_HEIGHT:I

.field private static final SCREEN_WIDTH:I

.field public static final STEP_COMPLETE:I = 0x4

.field public static final STEP_RECORDING_START:I = 0x1

.field public static final STEP_RECORDING_STOP:I = 0x3

.field public static final STEP_SNAP_SHOT:I = 0x2

.field private static final SUBVIEW_HELP_FOCUS_HEIGHT:I = 0xa0

.field private static final SUBVIEW_HELP_FOCUS_WIDTH:I = 0xa0

.field protected static final TAG:Ljava/lang/String; = "TwGLTutorialRecordingMode"

.field private static mOrientation:I


# instance fields
.field public blinkAnimation:Landroid/view/animation/Animation;

.field private mActivityContext:Lcom/sec/android/app/camera/Camera;

.field private mHelpFocus:Lcom/sec/android/glview/TwGLImage;

.field private mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

.field private mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

.field private mHelpText:Lcom/sec/android/glview/TwGLText;

.field private mHelpViewGroup:Lcom/sec/android/glview/TwGLViewGroup;

.field private final mShowAlphaViewAnimation:Landroid/view/animation/AlphaAnimation;

.field private mShowAnimation:Landroid/view/animation/Animation;

.field private final mShowAnimationSet:Landroid/view/animation/AnimationSet;

.field private final mShowScaleAnimation:Landroid/view/animation/ScaleAnimation;

.field public mTutorialStep:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/high16 v5, 0x40000000    # 2.0f

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 28
    const v0, 0x7f0a0006

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->SCREEN_WIDTH:I

    .line 29
    const v0, 0x7f0a0007

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->SCREEN_HEIGHT:I

    .line 31
    const v0, 0x7f0a01f4

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_WIDTH_MARGIN:F

    .line 32
    const v0, 0x7f0a01f5

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_HEIGHT_MARGIN:F

    .line 33
    new-array v0, v6, [F

    const v1, 0x7f0a01f6

    invoke-static {v1}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v1

    aput v1, v0, v3

    const v1, 0x7f0a01f7

    invoke-static {v1}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v1

    aput v1, v0, v4

    sput-object v0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_WIDTH:[F

    .line 34
    const v0, 0x7f0a01f8

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_HEIGHT:F

    .line 36
    new-array v0, v6, [F

    sget-object v1, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_WIDTH:[F

    aget v1, v1, v3

    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_WIDTH_MARGIN:F

    mul-float/2addr v2, v5

    add-float/2addr v1, v2

    aput v1, v0, v3

    sget-object v1, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_WIDTH:[F

    aget v1, v1, v4

    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_WIDTH_MARGIN:F

    mul-float/2addr v2, v5

    add-float/2addr v1, v2

    aput v1, v0, v4

    sput-object v0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_WIDTH:[F

    .line 37
    sget v0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_HEIGHT:F

    sget v1, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_HEIGHT_MARGIN:F

    mul-float/2addr v1, v5

    add-float/2addr v0, v1

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_HEIGHT:F

    .line 39
    const v0, 0x7f0a01e9

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_PICKER_PADDING:F

    .line 40
    const v0, 0x7f0a01eb

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_PICKER_EXTRA_PADDING_2:F

    .line 41
    const v0, 0x7f0a01fa

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_PICKER_OFFSET:F

    .line 42
    const/4 v0, 0x4

    new-array v0, v0, [F

    const v1, 0x7f0a01ed

    invoke-static {v1}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v1

    aput v1, v0, v3

    const v1, 0x7f0a01ee

    invoke-static {v1}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v1

    aput v1, v0, v4

    const v1, 0x7f0a01ec

    invoke-static {v1}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v1

    aput v1, v0, v6

    const/4 v1, 0x3

    const v2, 0x7f0a01ef

    invoke-static {v2}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v2

    aput v2, v0, v1

    sput-object v0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_PADDING:[F

    .line 45
    sget v0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->SCREEN_WIDTH:I

    int-to-float v0, v0

    const v1, 0x7f0a0015

    invoke-static {v1}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v1

    sub-float/2addr v0, v1

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->RIGHT_SIDE_MENU_X:F

    .line 46
    const v0, 0x7f0a0020

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->RIGHT_SIDE_BUTTON_WIDTH:F

    .line 47
    const v0, 0x7f0a0021

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->RIGHT_SIDE_BUTTON_HEIGHT:F

    .line 48
    const v0, 0x7f0a002b

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->CAMCORDER_BUTTON_X:F

    .line 49
    const v0, 0x7f0a002c

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->CAMCORDER_BUTTON_Y:F

    .line 50
    sget v0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->RIGHT_SIDE_MENU_X:F

    const v1, 0x7f0a00ab

    invoke-static {v1}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v1

    add-float/2addr v0, v1

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->RECORDING_BUTTON_GROUP_X:F

    .line 51
    const v0, 0x7f0a00aa

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->RECORDING_SNAPSHOT_BUTTON_Y:F

    .line 52
    const v0, 0x7f0a00a9

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->RECORDING_SNAPSHOT_MARGIN_Y:F

    .line 53
    const v0, 0x7f0a00ae

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->RECORDING_STOP_BUTTON_Y:F

    .line 55
    const v0, 0x7f0a0023

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->EASY_RECORDING_STOP_BUTTON_Y:F

    .line 56
    const v0, 0x7f0a0014

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->EASY_HELP_FOCUS_PADDING_Y:F

    .line 57
    const v0, 0x7f0b004c

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getInteger(I)I

    move-result v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->FONT_SIZE:I

    .line 58
    const v0, 0x7f090008

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getColor(I)I

    move-result v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->FONT_COLOR:I

    .line 63
    const/4 v0, -0x1

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mOrientation:I

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/camera/Camera;FFFF)V
    .locals 10
    .param p1, "activityContext"    # Lcom/sec/android/app/camera/Camera;
    .param p2, "left"    # F
    .param p3, "top"    # F
    .param p4, "width"    # F
    .param p5, "height"    # F

    .prologue
    .line 85
    invoke-virtual {p1}, Lcom/sec/android/app/camera/Camera;->getGLContext()Lcom/sec/android/glview/TwGLContext;

    move-result-object v1

    move-object v0, p0

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/glview/TwGLViewGroup;-><init>(Lcom/sec/android/glview/TwGLContext;FFFF)V

    .line 80
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mShowAlphaViewAnimation:Landroid/view/animation/AlphaAnimation;

    .line 81
    new-instance v0, Landroid/view/animation/ScaleAnimation;

    const v1, 0x3f19999a    # 0.6f

    const/high16 v2, 0x3f800000    # 1.0f

    const v3, 0x3f19999a    # 0.6f

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v5, 0x1

    const/high16 v6, 0x3f000000    # 0.5f

    const/4 v7, 0x1

    const/high16 v8, 0x3f000000    # 0.5f

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    iput-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mShowScaleAnimation:Landroid/view/animation/ScaleAnimation;

    .line 82
    new-instance v0, Landroid/view/animation/AnimationSet;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    iput-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mShowAnimationSet:Landroid/view/animation/AnimationSet;

    .line 87
    iput-object p1, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    .line 89
    new-instance v0, Lcom/sec/android/glview/TwGLViewGroup;

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/Camera;->getGLContext()Lcom/sec/android/glview/TwGLContext;

    move-result-object v1

    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->SCREEN_WIDTH:I

    int-to-float v2, v2

    neg-float v2, v2

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->SCREEN_HEIGHT:I

    int-to-float v3, v3

    neg-float v3, v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->SCREEN_WIDTH:I

    int-to-float v4, v4

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->SCREEN_HEIGHT:I

    int-to-float v5, v5

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/glview/TwGLViewGroup;-><init>(Lcom/sec/android/glview/TwGLContext;FFFF)V

    iput-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpViewGroup:Lcom/sec/android/glview/TwGLViewGroup;

    .line 91
    new-instance v0, Lcom/sec/android/glview/TwGLText;

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/Camera;->getGLContext()Lcom/sec/android/glview/TwGLContext;

    move-result-object v1

    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_WIDTH_MARGIN:F

    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_HEIGHT_MARGIN:F

    sget-object v4, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_WIDTH:[F

    const/4 v5, 0x0

    aget v4, v4, v5

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_HEIGHT:F

    const-string v6, ""

    sget v7, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->FONT_SIZE:I

    int-to-float v7, v7

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->FONT_COLOR:I

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/sec/android/glview/TwGLText;-><init>(Lcom/sec/android/glview/TwGLContext;FFFFLjava/lang/String;FIZ)V

    iput-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpText:Lcom/sec/android/glview/TwGLText;

    .line 92
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpText:Lcom/sec/android/glview/TwGLText;

    const/4 v1, 0x1

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/glview/TwGLText;->setAlign(II)V

    .line 94
    new-instance v0, Lcom/sec/android/glview/TwGLViewGroup;

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/Camera;->getGLContext()Lcom/sec/android/glview/TwGLContext;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    sget-object v4, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_WIDTH:[F

    const/4 v5, 0x0

    aget v4, v4, v5

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_HEIGHT:F

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/glview/TwGLViewGroup;-><init>(Lcom/sec/android/glview/TwGLContext;FFFF)V

    iput-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    .line 95
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    const v1, 0x7f020422

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLViewGroup;->setNinePatchBackground(I)Z

    .line 97
    new-instance v0, Lcom/sec/android/glview/TwGLImage;

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/Camera;->getGLContext()Lcom/sec/android/glview/TwGLContext;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const v4, 0x7f020423

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/glview/TwGLImage;-><init>(Lcom/sec/android/glview/TwGLContext;FFI)V

    iput-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    .line 99
    const v0, 0x7f050002

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->blinkAnimation:Landroid/view/animation/Animation;

    .line 100
    new-instance v0, Lcom/sec/android/glview/TwGLImage;

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/Camera;->getGLContext()Lcom/sec/android/glview/TwGLContext;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const v4, 0x7f020426

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/glview/TwGLImage;-><init>(Lcom/sec/android/glview/TwGLContext;FFI)V

    iput-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpFocus:Lcom/sec/android/glview/TwGLImage;

    .line 101
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpFocus:Lcom/sec/android/glview/TwGLImage;

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->blinkAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLImage;->setAnimation(Landroid/view/animation/Animation;)V

    .line 103
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mShowScaleAnimation:Landroid/view/animation/ScaleAnimation;

    new-instance v1, Landroid/view/animation/OvershootInterpolator;

    invoke-direct {v1}, Landroid/view/animation/OvershootInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/ScaleAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 105
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mShowAnimationSet:Landroid/view/animation/AnimationSet;

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mShowAlphaViewAnimation:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v0, v1}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 106
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mShowAnimationSet:Landroid/view/animation/AnimationSet;

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mShowScaleAnimation:Landroid/view/animation/ScaleAnimation;

    invoke-virtual {v0, v1}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 107
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mShowAlphaViewAnimation:Landroid/view/animation/AlphaAnimation;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 108
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mShowScaleAnimation:Landroid/view/animation/ScaleAnimation;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/ScaleAnimation;->setDuration(J)V

    .line 110
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mShowAnimationSet:Landroid/view/animation/AnimationSet;

    iput-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mShowAnimation:Landroid/view/animation/Animation;

    .line 112
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpText:Lcom/sec/android/glview/TwGLText;

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLViewGroup;->addView(Lcom/sec/android/glview/TwGLView;)V

    .line 114
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpViewGroup:Lcom/sec/android/glview/TwGLViewGroup;

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLViewGroup;->addView(Lcom/sec/android/glview/TwGLView;)V

    .line 115
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpViewGroup:Lcom/sec/android/glview/TwGLViewGroup;

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLViewGroup;->addView(Lcom/sec/android/glview/TwGLView;)V

    .line 116
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpViewGroup:Lcom/sec/android/glview/TwGLViewGroup;

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpFocus:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLViewGroup;->addView(Lcom/sec/android/glview/TwGLView;)V

    .line 118
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpViewGroup:Lcom/sec/android/glview/TwGLViewGroup;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLViewGroup;->setVisibility(I)V

    .line 119
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpViewGroup:Lcom/sec/android/glview/TwGLViewGroup;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLViewGroup;->setBypassTouch(Z)V

    .line 121
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpViewGroup:Lcom/sec/android/glview/TwGLViewGroup;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->addView(Lcom/sec/android/glview/TwGLView;)V

    .line 123
    invoke-static {}, Lcom/sec/android/glview/TwGLContext;->getLastOrientation()I

    move-result v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mOrientation:I

    .line 124
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->setStepRecordingStart()V

    .line 125
    return-void
.end method


# virtual methods
.method public declared-synchronized contains(FF)Z
    .locals 1
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 675
    monitor-enter p0

    const/4 v0, 0x1

    monitor-exit p0

    return v0
.end method

.method public convertOrientationForSubView(I)I
    .locals 1
    .param p1, "lastOrientation"    # I

    .prologue
    .line 803
    const/4 v0, -0x1

    .line 804
    .local v0, "orientation":I
    sparse-switch p1, :sswitch_data_0

    .line 820
    :goto_0
    return v0

    .line 806
    :sswitch_0
    const/4 v0, 0x0

    .line 807
    goto :goto_0

    .line 809
    :sswitch_1
    const/4 v0, 0x1

    .line 810
    goto :goto_0

    .line 812
    :sswitch_2
    const/4 v0, 0x2

    .line 813
    goto :goto_0

    .line 815
    :sswitch_3
    const/4 v0, 0x3

    .line 816
    goto :goto_0

    .line 804
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_3
        0x5a -> :sswitch_2
        0xb4 -> :sswitch_1
        0x10e -> :sswitch_0
    .end sparse-switch
.end method

.method public declared-synchronized findViewByCoordinate(FF)Lcom/sec/android/glview/TwGLView;
    .locals 1
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 680
    monitor-enter p0

    const/4 v0, 0x0

    monitor-exit p0

    return-object v0
.end method

.method public hideHelpTutorial()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 128
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpViewGroup:Lcom/sec/android/glview/TwGLViewGroup;

    if-eqz v0, :cond_0

    .line 129
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpViewGroup:Lcom/sec/android/glview/TwGLViewGroup;

    invoke-virtual {v0}, Lcom/sec/android/glview/TwGLViewGroup;->clear()V

    .line 130
    iput-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpViewGroup:Lcom/sec/android/glview/TwGLViewGroup;

    .line 133
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    if-eqz v0, :cond_1

    .line 134
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    invoke-virtual {v0}, Lcom/sec/android/glview/TwGLViewGroup;->clear()V

    .line 135
    iput-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    .line 138
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpText:Lcom/sec/android/glview/TwGLText;

    if-eqz v0, :cond_2

    .line 139
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpText:Lcom/sec/android/glview/TwGLText;

    invoke-virtual {v0}, Lcom/sec/android/glview/TwGLText;->clear()V

    .line 140
    iput-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpText:Lcom/sec/android/glview/TwGLText;

    .line 143
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    if-eqz v0, :cond_3

    .line 144
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v0}, Lcom/sec/android/glview/TwGLImage;->clear()V

    .line 145
    iput-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    .line 148
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpFocus:Lcom/sec/android/glview/TwGLImage;

    if-eqz v0, :cond_4

    .line 149
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpFocus:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v0}, Lcom/sec/android/glview/TwGLImage;->clear()V

    .line 150
    iput-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpFocus:Lcom/sec/android/glview/TwGLImage;

    .line 152
    :cond_4
    return-void
.end method

.method public onHelpTts()V
    .locals 4

    .prologue
    .line 797
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpText:Lcom/sec/android/glview/TwGLText;

    invoke-virtual {v0}, Lcom/sec/android/glview/TwGLText;->getText()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->getContext()Lcom/sec/android/glview/TwGLContext;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->getContext()Lcom/sec/android/glview/TwGLContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/glview/TwGLContext;->isScreenReaderActive()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->getContext()Lcom/sec/android/glview/TwGLContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/glview/TwGLContext;->isTalkbackPaused()Z

    move-result v0

    if-nez v0, :cond_0

    .line 798
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->getContext()Lcom/sec/android/glview/TwGLContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/glview/TwGLContext;->getTts()Landroid/speech/tts/TextToSpeech;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpText:Lcom/sec/android/glview/TwGLText;

    invoke-virtual {v1}, Lcom/sec/android/glview/TwGLText;->getText()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/speech/tts/TextToSpeech;->speak(Ljava/lang/String;ILjava/util/HashMap;)I

    .line 800
    :cond_0
    return-void
.end method

.method public onMoveHelpFocusPicker()V
    .locals 0

    .prologue
    .line 795
    return-void
.end method

.method public onMoveHelpFocusPicker(I)V
    .locals 0
    .param p1, "lastOrientation"    # I

    .prologue
    .line 776
    return-void
.end method

.method public onOrientationChanged(I)V
    .locals 1
    .param p1, "orientation"    # I

    .prologue
    .line 738
    sget v0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mOrientation:I

    if-eq v0, p1, :cond_0

    .line 739
    sput p1, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mOrientation:I

    .line 754
    :cond_0
    invoke-virtual {p0, p1}, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->setSizeForOrientation(I)V

    .line 755
    invoke-super {p0, p1}, Lcom/sec/android/glview/TwGLViewGroup;->onOrientationChanged(I)V

    .line 756
    return-void
.end method

.method public refreshHelpPopupPicker()V
    .locals 8

    .prologue
    const/4 v3, 0x1

    const v7, 0x7f020421

    const v6, 0x7f020425

    const/high16 v4, 0x43200000    # 160.0f

    const/high16 v5, 0x40000000    # 2.0f

    .line 629
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/Camera;->getSubViewManager()Lcom/sec/android/app/camera/subview/SubViewManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/camera/subview/SubViewManager;->getTutorialRecordingMode()Lcom/sec/android/app/camera/subview/SubViewTutorialRecordingMode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/camera/subview/SubViewTutorialRecordingMode;->getFocusPosition()Landroid/graphics/Point;

    move-result-object v0

    .line 630
    .local v0, "helpFocusPos":Landroid/graphics/Point;
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/Camera;->isMultiWindow()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 631
    sget v1, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mOrientation:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    .line 632
    invoke-static {}, Lcom/sec/android/glview/TwGLContext;->getLastOrientation()I

    move-result v1

    sput v1, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mOrientation:I

    .line 634
    :cond_0
    sget v1, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mOrientation:I

    packed-switch v1, :pswitch_data_0

    .line 671
    :goto_0
    return-void

    .line 636
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/Camera;->getEngine()Lcom/sec/android/app/camera/CommonEngine;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/camera/CommonEngine;->isRecording()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/Camera;->getRequestedOrientation()I

    move-result v1

    if-ne v1, v3, :cond_1

    .line 637
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    iget v2, v0, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v3}, Lcom/sec/android/glview/TwGLImage;->getWidth()F

    move-result v3

    sub-float v3, v4, v3

    div-float/2addr v3, v5

    add-float/2addr v2, v3

    iget v3, v0, Landroid/graphics/Point;->y:I

    int-to-float v3, v3

    iget-object v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v4}, Lcom/sec/android/glview/TwGLImage;->getHeight()F

    move-result v4

    div-float/2addr v4, v5

    add-float/2addr v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/glview/TwGLImage;->translateAbsolute(FF)V

    .line 638
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v1, v6}, Lcom/sec/android/glview/TwGLImage;->setImageResources(I)V

    goto :goto_0

    .line 640
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    iget v2, v0, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v3}, Lcom/sec/android/glview/TwGLImage;->getWidth()F

    move-result v3

    sub-float v3, v4, v3

    div-float/2addr v3, v5

    add-float/2addr v2, v3

    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->SCREEN_HEIGHT:I

    add-int/lit16 v3, v3, -0xa0

    iget v4, v0, Landroid/graphics/Point;->y:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    iget-object v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v4}, Lcom/sec/android/glview/TwGLImage;->getHeight()F

    move-result v4

    div-float/2addr v4, v5

    add-float/2addr v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/glview/TwGLImage;->translateAbsolute(FF)V

    .line 641
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v1, v7}, Lcom/sec/android/glview/TwGLImage;->setImageResources(I)V

    goto :goto_0

    .line 645
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->SCREEN_WIDTH:I

    iget v3, v0, Landroid/graphics/Point;->x:I

    sub-int/2addr v2, v3

    add-int/lit16 v2, v2, -0xa0

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v3}, Lcom/sec/android/glview/TwGLImage;->getWidth()F

    move-result v3

    sub-float v3, v4, v3

    div-float/2addr v3, v5

    add-float/2addr v2, v3

    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->SCREEN_HEIGHT:I

    add-int/lit16 v3, v3, -0xa0

    iget v4, v0, Landroid/graphics/Point;->y:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    iget-object v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v4}, Lcom/sec/android/glview/TwGLImage;->getHeight()F

    move-result v4

    div-float/2addr v4, v5

    add-float/2addr v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/glview/TwGLImage;->translateAbsolute(FF)V

    .line 646
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v1, v7}, Lcom/sec/android/glview/TwGLImage;->setImageResources(I)V

    goto/16 :goto_0

    .line 649
    :pswitch_2
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->SCREEN_WIDTH:I

    iget v3, v0, Landroid/graphics/Point;->x:I

    sub-int/2addr v2, v3

    add-int/lit16 v2, v2, -0xa0

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v3}, Lcom/sec/android/glview/TwGLImage;->getWidth()F

    move-result v3

    sub-float v3, v4, v3

    div-float/2addr v3, v5

    add-float/2addr v2, v3

    iget v3, v0, Landroid/graphics/Point;->y:I

    int-to-float v3, v3

    iget-object v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v4}, Lcom/sec/android/glview/TwGLImage;->getHeight()F

    move-result v4

    div-float/2addr v4, v5

    add-float/2addr v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/glview/TwGLImage;->translateAbsolute(FF)V

    .line 650
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v1, v6}, Lcom/sec/android/glview/TwGLImage;->setImageResources(I)V

    goto/16 :goto_0

    .line 653
    :pswitch_3
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/Camera;->getEngine()Lcom/sec/android/app/camera/CommonEngine;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/camera/CommonEngine;->isRecording()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/Camera;->getRequestedOrientation()I

    move-result v1

    if-nez v1, :cond_2

    .line 654
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    iget v2, v0, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v3}, Lcom/sec/android/glview/TwGLImage;->getWidth()F

    move-result v3

    sub-float v3, v4, v3

    div-float/2addr v3, v5

    add-float/2addr v2, v3

    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->SCREEN_HEIGHT:I

    add-int/lit16 v3, v3, -0xa0

    iget v4, v0, Landroid/graphics/Point;->y:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    iget-object v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v4}, Lcom/sec/android/glview/TwGLImage;->getHeight()F

    move-result v4

    div-float/2addr v4, v5

    add-float/2addr v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/glview/TwGLImage;->translateAbsolute(FF)V

    .line 655
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v1, v7}, Lcom/sec/android/glview/TwGLImage;->setImageResources(I)V

    goto/16 :goto_0

    .line 657
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    iget v2, v0, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v3}, Lcom/sec/android/glview/TwGLImage;->getWidth()F

    move-result v3

    sub-float v3, v4, v3

    div-float/2addr v3, v5

    add-float/2addr v2, v3

    iget v3, v0, Landroid/graphics/Point;->y:I

    int-to-float v3, v3

    iget-object v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v4}, Lcom/sec/android/glview/TwGLImage;->getHeight()F

    move-result v4

    div-float/2addr v4, v5

    add-float/2addr v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/glview/TwGLImage;->translateAbsolute(FF)V

    .line 658
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v1, v6}, Lcom/sec/android/glview/TwGLImage;->setImageResources(I)V

    goto/16 :goto_0

    .line 665
    :cond_3
    sget v1, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mOrientation:I

    if-eq v1, v3, :cond_4

    sget v1, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mOrientation:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_5

    .line 666
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->SCREEN_WIDTH:I

    iget v3, v0, Landroid/graphics/Point;->x:I

    sub-int/2addr v2, v3

    add-int/lit16 v2, v2, -0xa0

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v3}, Lcom/sec/android/glview/TwGLImage;->getWidth()F

    move-result v3

    sub-float v3, v4, v3

    div-float/2addr v3, v5

    add-float/2addr v2, v3

    iget v3, v0, Landroid/graphics/Point;->y:I

    int-to-float v3, v3

    iget-object v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v4}, Lcom/sec/android/glview/TwGLImage;->getHeight()F

    move-result v4

    div-float/2addr v4, v5

    add-float/2addr v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/glview/TwGLImage;->translateAbsolute(FF)V

    .line 669
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v1, v6}, Lcom/sec/android/glview/TwGLImage;->setImageResources(I)V

    goto/16 :goto_0

    .line 668
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    iget v2, v0, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v3}, Lcom/sec/android/glview/TwGLImage;->getWidth()F

    move-result v3

    sub-float v3, v4, v3

    div-float/2addr v3, v5

    add-float/2addr v2, v3

    iget v3, v0, Landroid/graphics/Point;->y:I

    int-to-float v3, v3

    iget-object v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v4}, Lcom/sec/android/glview/TwGLImage;->getHeight()F

    move-result v4

    div-float/2addr v4, v5

    add-float/2addr v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/glview/TwGLImage;->translateAbsolute(FF)V

    goto :goto_1

    .line 634
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public setHelpText(I)V
    .locals 12
    .param p1, "step"    # I

    .prologue
    .line 159
    const/4 v0, 0x0

    .line 160
    .local v0, "helptext":Ljava/lang/String;
    const/4 v4, 0x0

    .line 162
    .local v4, "rows":I
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    .line 163
    .local v3, "paint":Landroid/graphics/Paint;
    const/4 v1, 0x0

    .line 164
    .local v1, "mTextSize":F
    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->FONT_SIZE:I

    int-to-float v5, v5

    invoke-virtual {v3, v5}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 165
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpText:Lcom/sec/android/glview/TwGLText;

    invoke-static {}, Lcom/sec/android/app/camera/Util;->getRobotoLightFont()Landroid/graphics/Typeface;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/android/glview/TwGLText;->setTextFont(Landroid/graphics/Typeface;)V

    .line 166
    invoke-static {}, Lcom/sec/android/glview/TwGLContext;->getLastOrientation()I

    move-result v2

    .line 167
    .local v2, "orientation":I
    packed-switch p1, :pswitch_data_0

    .line 498
    :goto_0
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpText:Lcom/sec/android/glview/TwGLText;

    invoke-virtual {v5, v0}, Lcom/sec/android/glview/TwGLText;->setText(Ljava/lang/String;)V

    .line 499
    return-void

    .line 169
    :pswitch_0
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    const v6, 0x7f0c0225

    invoke-virtual {v5, v6}, Lcom/sec/android/app/camera/Camera;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 170
    invoke-virtual {v3, v0}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v1

    .line 172
    invoke-static {}, Lcom/sec/android/glview/TwGLContext;->getLastOrientation()I

    move-result v5

    packed-switch v5, :pswitch_data_1

    .line 216
    :goto_1
    sget-object v5, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_WIDTH:[F

    const/4 v6, 0x0

    aget v5, v5, v6

    sget v6, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->FONT_SIZE:I

    int-to-float v6, v6

    const/4 v7, 0x0

    invoke-static {v5, v0, v6, v7}, Lcom/sec/android/glview/TwGLText;->measureRows(FLjava/lang/String;FLandroid/graphics/Typeface;)I

    move-result v4

    .line 218
    const/4 v5, 0x1

    if-ne v4, v5, :cond_2

    .line 219
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v7}, Lcom/sec/android/glview/TwGLImage;->getTranslateX()F

    move-result v7

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_PICKER_PADDING:F

    add-float/2addr v7, v8

    sget-object v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_PADDING:[F

    const/4 v9, 0x0

    aget v8, v8, v9

    add-float/2addr v7, v8

    const/high16 v8, 0x40000000    # 2.0f

    sget v9, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_WIDTH_MARGIN:F

    mul-float/2addr v8, v9

    add-float/2addr v8, v1

    sub-float/2addr v7, v8

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_PICKER_OFFSET:F

    sub-float/2addr v7, v8

    iget-object v8, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v8}, Lcom/sec/android/glview/TwGLImage;->getTranslateY()F

    move-result v8

    sget v9, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_HEIGHT:F

    add-int/lit8 v10, v4, -0x1

    int-to-float v10, v10

    sget v11, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_HEIGHT:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget-object v10, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v10}, Lcom/sec/android/glview/TwGLImage;->getHeight()F

    move-result v10

    sub-float/2addr v9, v10

    const/high16 v10, 0x40000000    # 2.0f

    div-float/2addr v9, v10

    sub-float/2addr v8, v9

    invoke-virtual {v5, v6, v7, v8}, Lcom/sec/android/glview/TwGLViewGroup;->setLeftTop(IFF)V

    .line 221
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    const/4 v6, 0x2

    iget-object v7, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v7}, Lcom/sec/android/glview/TwGLImage;->getTranslateX()F

    move-result v7

    const/high16 v8, 0x40400000    # 3.0f

    sget v9, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_PICKER_PADDING:F

    mul-float/2addr v8, v9

    const/high16 v9, 0x40000000    # 2.0f

    div-float/2addr v8, v9

    add-float/2addr v7, v8

    sget-object v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_PADDING:[F

    const/4 v9, 0x0

    aget v8, v8, v9

    add-float/2addr v7, v8

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_PICKER_OFFSET:F

    sub-float/2addr v7, v8

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_PICKER_EXTRA_PADDING_2:F

    sub-float/2addr v7, v8

    iget-object v8, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v8}, Lcom/sec/android/glview/TwGLImage;->getTranslateY()F

    move-result v8

    sget v9, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_HEIGHT:F

    add-int/lit8 v10, v4, -0x1

    int-to-float v10, v10

    sget v11, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_HEIGHT:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget-object v10, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v10}, Lcom/sec/android/glview/TwGLImage;->getHeight()F

    move-result v10

    add-float/2addr v9, v10

    const/high16 v10, 0x40000000    # 2.0f

    div-float/2addr v9, v10

    add-float/2addr v8, v9

    invoke-virtual {v5, v6, v7, v8}, Lcom/sec/android/glview/TwGLViewGroup;->setLeftTop(IFF)V

    .line 230
    :goto_2
    sget-object v5, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_WIDTH:[F

    const/4 v6, 0x1

    aget v5, v5, v6

    sget v6, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->FONT_SIZE:I

    int-to-float v6, v6

    const/4 v7, 0x0

    invoke-static {v5, v0, v6, v7}, Lcom/sec/android/glview/TwGLText;->measureRows(FLjava/lang/String;FLandroid/graphics/Typeface;)I

    move-result v4

    .line 231
    const/4 v5, 0x1

    if-ne v4, v5, :cond_3

    .line 232
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    const/4 v6, 0x1

    iget-object v7, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v7}, Lcom/sec/android/glview/TwGLImage;->getTranslateX()F

    move-result v7

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_PICKER_PADDING:F

    const/high16 v9, 0x40000000    # 2.0f

    div-float/2addr v8, v9

    sub-float/2addr v7, v8

    sget-object v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_PADDING:[F

    const/4 v9, 0x2

    aget v8, v8, v9

    add-float/2addr v7, v8

    const/high16 v8, 0x40000000    # 2.0f

    sget v9, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_PICKER_OFFSET:F

    mul-float/2addr v8, v9

    add-float/2addr v7, v8

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_PICKER_EXTRA_PADDING_2:F

    add-float/2addr v7, v8

    iget-object v8, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v8}, Lcom/sec/android/glview/TwGLImage;->getTranslateY()F

    move-result v8

    const/high16 v9, 0x40400000    # 3.0f

    sget v10, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_WIDTH_MARGIN:F

    mul-float/2addr v9, v10

    sub-float/2addr v8, v9

    invoke-virtual {v5, v6, v7, v8}, Lcom/sec/android/glview/TwGLViewGroup;->setLeftTop(IFF)V

    .line 234
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    const/4 v6, 0x3

    iget-object v7, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v7}, Lcom/sec/android/glview/TwGLImage;->getTranslateX()F

    move-result v7

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_HEIGHT:F

    add-int/lit8 v9, v4, -0x1

    int-to-float v9, v9

    sget v10, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_HEIGHT:F

    mul-float/2addr v9, v10

    add-float/2addr v8, v9

    sub-float/2addr v7, v8

    sget-object v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_PADDING:[F

    const/4 v9, 0x3

    aget v8, v8, v9

    add-float/2addr v7, v8

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_PICKER_PADDING:F

    add-float/2addr v7, v8

    iget-object v8, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v8}, Lcom/sec/android/glview/TwGLImage;->getTranslateY()F

    move-result v8

    add-float/2addr v8, v1

    sget v9, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_WIDTH_MARGIN:F

    sub-float/2addr v8, v9

    invoke-virtual {v5, v6, v7, v8}, Lcom/sec/android/glview/TwGLViewGroup;->setLeftTop(IFF)V

    .line 244
    :goto_3
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    invoke-static {}, Lcom/sec/android/glview/TwGLContext;->getLastOrientation()I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/sec/android/glview/TwGLViewGroup;->setOrientation(I)V

    .line 245
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    iget-object v6, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mShowAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v5, v6}, Lcom/sec/android/glview/TwGLViewGroup;->setAnimation(Landroid/view/animation/Animation;)V

    .line 246
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    iget-object v6, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mShowAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v5, v6}, Lcom/sec/android/glview/TwGLImage;->setAnimation(Landroid/view/animation/Animation;)V

    .line 247
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lcom/sec/android/glview/TwGLViewGroup;->updateLayout(Z)V

    goto/16 :goto_0

    .line 175
    :pswitch_1
    sget-object v5, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_WIDTH:[F

    const/4 v6, 0x0

    aget v5, v5, v6

    sget v6, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->FONT_SIZE:I

    int-to-float v6, v6

    const/4 v7, 0x0

    invoke-static {v5, v0, v6, v7}, Lcom/sec/android/glview/TwGLText;->measureRows(FLjava/lang/String;FLandroid/graphics/Typeface;)I

    move-result v4

    .line 176
    const/4 v5, 0x1

    if-ne v4, v5, :cond_0

    .line 177
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpText:Lcom/sec/android/glview/TwGLText;

    sget v6, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_WIDTH_MARGIN:F

    const/high16 v7, 0x40000000    # 2.0f

    mul-float/2addr v6, v7

    add-float/2addr v6, v1

    int-to-float v7, v4

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_HEIGHT:F

    mul-float/2addr v7, v8

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/glview/TwGLText;->setSize(FF)V

    .line 178
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    sget v6, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_WIDTH_MARGIN:F

    const/high16 v7, 0x40000000    # 2.0f

    mul-float/2addr v6, v7

    add-float/2addr v6, v1

    sget v7, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_HEIGHT:F

    add-int/lit8 v8, v4, -0x1

    int-to-float v8, v8

    sget v9, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_HEIGHT:F

    mul-float/2addr v8, v9

    add-float/2addr v7, v8

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/glview/TwGLViewGroup;->setSize(FF)V

    goto/16 :goto_1

    .line 180
    :cond_0
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpText:Lcom/sec/android/glview/TwGLText;

    sget-object v6, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_WIDTH:[F

    const/4 v7, 0x0

    aget v6, v6, v7

    int-to-float v7, v4

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_HEIGHT:F

    mul-float/2addr v7, v8

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/glview/TwGLText;->setSize(FF)V

    .line 181
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    sget-object v6, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_WIDTH:[F

    const/4 v7, 0x0

    aget v6, v6, v7

    sget v7, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_HEIGHT:F

    add-int/lit8 v8, v4, -0x1

    int-to-float v8, v8

    sget v9, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_HEIGHT:F

    mul-float/2addr v8, v9

    add-float/2addr v7, v8

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/glview/TwGLViewGroup;->setSize(FF)V

    goto/16 :goto_1

    .line 186
    :pswitch_2
    sget-object v5, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_WIDTH:[F

    const/4 v6, 0x1

    aget v5, v5, v6

    sget v6, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->FONT_SIZE:I

    int-to-float v6, v6

    const/4 v7, 0x0

    invoke-static {v5, v0, v6, v7}, Lcom/sec/android/glview/TwGLText;->measureRows(FLjava/lang/String;FLandroid/graphics/Typeface;)I

    move-result v4

    .line 187
    const/4 v5, 0x1

    if-ne v4, v5, :cond_1

    .line 188
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpText:Lcom/sec/android/glview/TwGLText;

    sget v6, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_WIDTH_MARGIN:F

    const/high16 v7, 0x40000000    # 2.0f

    mul-float/2addr v6, v7

    add-float/2addr v6, v1

    int-to-float v7, v4

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_HEIGHT:F

    mul-float/2addr v7, v8

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/glview/TwGLText;->setSize(FF)V

    .line 189
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    sget v6, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_WIDTH_MARGIN:F

    const/high16 v7, 0x40000000    # 2.0f

    mul-float/2addr v6, v7

    add-float/2addr v6, v1

    sget v7, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_HEIGHT:F

    add-int/lit8 v8, v4, -0x1

    int-to-float v8, v8

    sget v9, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_HEIGHT:F

    mul-float/2addr v8, v9

    add-float/2addr v7, v8

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/glview/TwGLViewGroup;->setSize(FF)V

    goto/16 :goto_1

    .line 191
    :cond_1
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpText:Lcom/sec/android/glview/TwGLText;

    sget-object v6, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_WIDTH:[F

    const/4 v7, 0x1

    aget v6, v6, v7

    int-to-float v7, v4

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_HEIGHT:F

    mul-float/2addr v7, v8

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/glview/TwGLText;->setSize(FF)V

    .line 192
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    sget-object v6, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_WIDTH:[F

    const/4 v7, 0x1

    aget v6, v6, v7

    sget v7, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_HEIGHT:F

    add-int/lit8 v8, v4, -0x1

    int-to-float v8, v8

    sget v9, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_HEIGHT:F

    mul-float/2addr v8, v9

    add-float/2addr v7, v8

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/glview/TwGLViewGroup;->setSize(FF)V

    goto/16 :goto_1

    .line 224
    :cond_2
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v7}, Lcom/sec/android/glview/TwGLImage;->getTranslateX()F

    move-result v7

    sget-object v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_WIDTH:[F

    const/4 v9, 0x0

    aget v8, v8, v9

    sub-float/2addr v7, v8

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_PICKER_PADDING:F

    add-float/2addr v7, v8

    sget-object v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_PADDING:[F

    const/4 v9, 0x0

    aget v8, v8, v9

    add-float/2addr v7, v8

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_PICKER_OFFSET:F

    sub-float/2addr v7, v8

    iget-object v8, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v8}, Lcom/sec/android/glview/TwGLImage;->getTranslateY()F

    move-result v8

    sget v9, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_HEIGHT:F

    add-int/lit8 v10, v4, -0x1

    int-to-float v10, v10

    sget v11, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_HEIGHT:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget-object v10, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v10}, Lcom/sec/android/glview/TwGLImage;->getHeight()F

    move-result v10

    sub-float/2addr v9, v10

    const/high16 v10, 0x40000000    # 2.0f

    div-float/2addr v9, v10

    sub-float/2addr v8, v9

    invoke-virtual {v5, v6, v7, v8}, Lcom/sec/android/glview/TwGLViewGroup;->setLeftTop(IFF)V

    .line 226
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    const/4 v6, 0x2

    iget-object v7, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v7}, Lcom/sec/android/glview/TwGLImage;->getTranslateX()F

    move-result v7

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_PICKER_PADDING:F

    add-float/2addr v7, v8

    sget-object v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_PADDING:[F

    const/4 v9, 0x2

    aget v8, v8, v9

    add-float/2addr v7, v8

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_PICKER_OFFSET:F

    sub-float/2addr v7, v8

    iget-object v8, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v8}, Lcom/sec/android/glview/TwGLImage;->getTranslateY()F

    move-result v8

    sget v9, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_HEIGHT:F

    add-int/lit8 v10, v4, -0x1

    int-to-float v10, v10

    sget v11, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_HEIGHT:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget-object v10, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v10}, Lcom/sec/android/glview/TwGLImage;->getHeight()F

    move-result v10

    add-float/2addr v9, v10

    const/high16 v10, 0x40000000    # 2.0f

    div-float/2addr v9, v10

    add-float/2addr v8, v9

    invoke-virtual {v5, v6, v7, v8}, Lcom/sec/android/glview/TwGLViewGroup;->setLeftTop(IFF)V

    goto/16 :goto_2

    .line 237
    :cond_3
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    const/4 v6, 0x1

    iget-object v7, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v7}, Lcom/sec/android/glview/TwGLImage;->getTranslateX()F

    move-result v7

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_PICKER_PADDING:F

    add-float/2addr v7, v8

    sget-object v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_PADDING:[F

    const/4 v9, 0x1

    aget v8, v8, v9

    add-float/2addr v7, v8

    const/high16 v8, 0x40000000    # 2.0f

    sget v9, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_PICKER_OFFSET:F

    mul-float/2addr v8, v9

    add-float/2addr v7, v8

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->SCREEN_HEIGHT:I

    int-to-float v8, v8

    sget-object v9, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_WIDTH:[F

    const/4 v10, 0x1

    aget v9, v9, v10

    sub-float/2addr v8, v9

    const/high16 v9, 0x40000000    # 2.0f

    div-float/2addr v8, v9

    sget v9, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->SCREEN_HEIGHT:I

    int-to-float v9, v9

    const/high16 v10, 0x40000000    # 2.0f

    div-float/2addr v9, v10

    add-float/2addr v8, v9

    invoke-virtual {v5, v6, v7, v8}, Lcom/sec/android/glview/TwGLViewGroup;->setLeftTop(IFF)V

    .line 239
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    const/4 v6, 0x3

    iget-object v7, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v7}, Lcom/sec/android/glview/TwGLImage;->getTranslateX()F

    move-result v7

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_HEIGHT:F

    add-int/lit8 v9, v4, -0x1

    int-to-float v9, v9

    sget v10, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_HEIGHT:F

    mul-float/2addr v9, v10

    add-float/2addr v8, v9

    sub-float/2addr v7, v8

    sget-object v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_PADDING:[F

    const/4 v9, 0x3

    aget v8, v8, v9

    add-float/2addr v7, v8

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_PICKER_PADDING:F

    add-float/2addr v7, v8

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->SCREEN_HEIGHT:I

    int-to-float v8, v8

    sget v9, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->SCREEN_HEIGHT:I

    int-to-float v9, v9

    sget-object v10, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_WIDTH:[F

    const/4 v11, 0x1

    aget v10, v10, v11

    sub-float/2addr v9, v10

    const/high16 v10, 0x40000000    # 2.0f

    div-float/2addr v9, v10

    sub-float/2addr v8, v9

    sget v9, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->SCREEN_HEIGHT:I

    div-int/lit8 v9, v9, 0x2

    int-to-float v9, v9

    add-float/2addr v8, v9

    invoke-virtual {v5, v6, v7, v8}, Lcom/sec/android/glview/TwGLViewGroup;->setLeftTop(IFF)V

    goto/16 :goto_3

    .line 255
    :pswitch_3
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    const v6, 0x7f0c0226

    invoke-virtual {v5, v6}, Lcom/sec/android/app/camera/Camera;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 256
    invoke-virtual {v3, v0}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v1

    .line 258
    packed-switch v2, :pswitch_data_2

    .line 321
    :goto_4
    sget-object v5, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_WIDTH:[F

    const/4 v6, 0x0

    aget v5, v5, v6

    sget v6, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->FONT_SIZE:I

    int-to-float v6, v6

    const/4 v7, 0x0

    invoke-static {v5, v0, v6, v7}, Lcom/sec/android/glview/TwGLText;->measureRows(FLjava/lang/String;FLandroid/graphics/Typeface;)I

    move-result v4

    .line 322
    const/4 v5, 0x1

    if-ne v4, v5, :cond_6

    .line 323
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v7}, Lcom/sec/android/glview/TwGLImage;->getTranslateX()F

    move-result v7

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_PICKER_PADDING:F

    add-float/2addr v7, v8

    sget-object v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_PADDING:[F

    const/4 v9, 0x0

    aget v8, v8, v9

    add-float/2addr v7, v8

    const/high16 v8, 0x40000000    # 2.0f

    sget v9, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_WIDTH_MARGIN:F

    mul-float/2addr v8, v9

    add-float/2addr v8, v1

    sub-float/2addr v7, v8

    iget-object v8, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v8}, Lcom/sec/android/glview/TwGLImage;->getTranslateY()F

    move-result v8

    sget v9, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_HEIGHT:F

    add-int/lit8 v10, v4, -0x1

    int-to-float v10, v10

    sget v11, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_HEIGHT:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget-object v10, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v10}, Lcom/sec/android/glview/TwGLImage;->getHeight()F

    move-result v10

    sub-float/2addr v9, v10

    const/high16 v10, 0x40000000    # 2.0f

    div-float/2addr v9, v10

    sub-float/2addr v8, v9

    invoke-virtual {v5, v6, v7, v8}, Lcom/sec/android/glview/TwGLViewGroup;->setLeftTop(IFF)V

    .line 325
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    const/4 v6, 0x2

    iget-object v7, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v7}, Lcom/sec/android/glview/TwGLImage;->getTranslateX()F

    move-result v7

    const/high16 v8, 0x40400000    # 3.0f

    sget v9, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_PICKER_PADDING:F

    mul-float/2addr v8, v9

    const/high16 v9, 0x40000000    # 2.0f

    div-float/2addr v8, v9

    add-float/2addr v7, v8

    sget-object v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_PADDING:[F

    const/4 v9, 0x0

    aget v8, v8, v9

    add-float/2addr v7, v8

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_PICKER_EXTRA_PADDING_2:F

    sub-float/2addr v7, v8

    iget-object v8, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v8}, Lcom/sec/android/glview/TwGLImage;->getTranslateY()F

    move-result v8

    sget v9, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_HEIGHT:F

    add-int/lit8 v10, v4, -0x1

    int-to-float v10, v10

    sget v11, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_HEIGHT:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget-object v10, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v10}, Lcom/sec/android/glview/TwGLImage;->getHeight()F

    move-result v10

    add-float/2addr v9, v10

    const/high16 v10, 0x40000000    # 2.0f

    div-float/2addr v9, v10

    add-float/2addr v8, v9

    invoke-virtual {v5, v6, v7, v8}, Lcom/sec/android/glview/TwGLViewGroup;->setLeftTop(IFF)V

    .line 334
    :goto_5
    sget-object v5, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_WIDTH:[F

    const/4 v6, 0x1

    aget v5, v5, v6

    sget v6, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->FONT_SIZE:I

    int-to-float v6, v6

    const/4 v7, 0x0

    invoke-static {v5, v0, v6, v7}, Lcom/sec/android/glview/TwGLText;->measureRows(FLjava/lang/String;FLandroid/graphics/Typeface;)I

    move-result v4

    .line 335
    const/4 v5, 0x1

    if-ne v4, v5, :cond_7

    .line 336
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    const/4 v6, 0x1

    iget-object v7, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v7}, Lcom/sec/android/glview/TwGLImage;->getTranslateX()F

    move-result v7

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_PICKER_PADDING:F

    const/high16 v9, 0x40000000    # 2.0f

    div-float/2addr v8, v9

    sub-float/2addr v7, v8

    sget-object v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_PADDING:[F

    const/4 v9, 0x2

    aget v8, v8, v9

    add-float/2addr v7, v8

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_PICKER_EXTRA_PADDING_2:F

    add-float/2addr v7, v8

    iget-object v8, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v8}, Lcom/sec/android/glview/TwGLImage;->getTranslateY()F

    move-result v8

    sub-float/2addr v8, v1

    const/high16 v9, 0x40000000    # 2.0f

    sget v10, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_WIDTH_MARGIN:F

    mul-float/2addr v9, v10

    add-float/2addr v8, v9

    invoke-virtual {v5, v6, v7, v8}, Lcom/sec/android/glview/TwGLViewGroup;->setLeftTop(IFF)V

    .line 338
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    const/4 v6, 0x3

    iget-object v7, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v7}, Lcom/sec/android/glview/TwGLImage;->getTranslateX()F

    move-result v7

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_HEIGHT:F

    add-int/lit8 v9, v4, -0x1

    int-to-float v9, v9

    sget v10, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_HEIGHT:F

    mul-float/2addr v9, v10

    add-float/2addr v8, v9

    sub-float/2addr v7, v8

    sget-object v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_PADDING:[F

    const/4 v9, 0x3

    aget v8, v8, v9

    add-float/2addr v7, v8

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_PICKER_PADDING:F

    add-float/2addr v7, v8

    iget-object v8, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v8}, Lcom/sec/android/glview/TwGLImage;->getTranslateY()F

    move-result v8

    const/high16 v9, 0x40800000    # 4.0f

    sget v10, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_WIDTH_MARGIN:F

    mul-float/2addr v9, v10

    add-float/2addr v8, v9

    invoke-virtual {v5, v6, v7, v8}, Lcom/sec/android/glview/TwGLViewGroup;->setLeftTop(IFF)V

    .line 348
    :goto_6
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    invoke-virtual {v5, v2}, Lcom/sec/android/glview/TwGLViewGroup;->setOrientation(I)V

    .line 349
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    iget-object v6, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mShowAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v5, v6}, Lcom/sec/android/glview/TwGLViewGroup;->setAnimation(Landroid/view/animation/Animation;)V

    .line 350
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    iget-object v6, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mShowAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v5, v6}, Lcom/sec/android/glview/TwGLImage;->setAnimation(Landroid/view/animation/Animation;)V

    .line 351
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lcom/sec/android/glview/TwGLViewGroup;->updateLayout(Z)V

    goto/16 :goto_0

    .line 261
    :pswitch_4
    sget-object v5, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_WIDTH:[F

    const/4 v6, 0x0

    aget v5, v5, v6

    sget v6, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->FONT_SIZE:I

    int-to-float v6, v6

    const/4 v7, 0x0

    invoke-static {v5, v0, v6, v7}, Lcom/sec/android/glview/TwGLText;->measureRows(FLjava/lang/String;FLandroid/graphics/Typeface;)I

    move-result v4

    .line 262
    const/4 v5, 0x1

    if-ne v4, v5, :cond_4

    .line 263
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpText:Lcom/sec/android/glview/TwGLText;

    sget v6, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_WIDTH_MARGIN:F

    const/high16 v7, 0x40000000    # 2.0f

    mul-float/2addr v6, v7

    add-float/2addr v6, v1

    int-to-float v7, v4

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_HEIGHT:F

    mul-float/2addr v7, v8

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/glview/TwGLText;->setSize(FF)V

    .line 264
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    sget v6, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_WIDTH_MARGIN:F

    const/high16 v7, 0x40000000    # 2.0f

    mul-float/2addr v6, v7

    add-float/2addr v6, v1

    sget v7, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_HEIGHT:F

    add-int/lit8 v8, v4, -0x1

    int-to-float v8, v8

    sget v9, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_HEIGHT:F

    mul-float/2addr v8, v9

    add-float/2addr v7, v8

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/glview/TwGLViewGroup;->setSize(FF)V

    goto/16 :goto_4

    .line 266
    :cond_4
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpText:Lcom/sec/android/glview/TwGLText;

    sget-object v6, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_WIDTH:[F

    const/4 v7, 0x0

    aget v6, v6, v7

    int-to-float v7, v4

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_HEIGHT:F

    mul-float/2addr v7, v8

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/glview/TwGLText;->setSize(FF)V

    .line 267
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    sget-object v6, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_WIDTH:[F

    const/4 v7, 0x0

    aget v6, v6, v7

    sget v7, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_HEIGHT:F

    add-int/lit8 v8, v4, -0x1

    int-to-float v8, v8

    sget v9, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_HEIGHT:F

    mul-float/2addr v8, v9

    add-float/2addr v7, v8

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/glview/TwGLViewGroup;->setSize(FF)V

    goto/16 :goto_4

    .line 272
    :pswitch_5
    sget-object v5, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_WIDTH:[F

    const/4 v6, 0x1

    aget v5, v5, v6

    sget v6, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->FONT_SIZE:I

    int-to-float v6, v6

    const/4 v7, 0x0

    invoke-static {v5, v0, v6, v7}, Lcom/sec/android/glview/TwGLText;->measureRows(FLjava/lang/String;FLandroid/graphics/Typeface;)I

    move-result v4

    .line 273
    const/4 v5, 0x1

    if-ne v4, v5, :cond_5

    .line 274
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpText:Lcom/sec/android/glview/TwGLText;

    sget v6, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_WIDTH_MARGIN:F

    const/high16 v7, 0x40000000    # 2.0f

    mul-float/2addr v6, v7

    add-float/2addr v6, v1

    int-to-float v7, v4

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_HEIGHT:F

    mul-float/2addr v7, v8

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/glview/TwGLText;->setSize(FF)V

    .line 275
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    sget v6, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_WIDTH_MARGIN:F

    const/high16 v7, 0x40000000    # 2.0f

    mul-float/2addr v6, v7

    add-float/2addr v6, v1

    sget v7, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_HEIGHT:F

    add-int/lit8 v8, v4, -0x1

    int-to-float v8, v8

    sget v9, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_HEIGHT:F

    mul-float/2addr v8, v9

    add-float/2addr v7, v8

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/glview/TwGLViewGroup;->setSize(FF)V

    goto/16 :goto_4

    .line 277
    :cond_5
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpText:Lcom/sec/android/glview/TwGLText;

    sget-object v6, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_WIDTH:[F

    const/4 v7, 0x1

    aget v6, v6, v7

    int-to-float v7, v4

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_HEIGHT:F

    mul-float/2addr v7, v8

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/glview/TwGLText;->setSize(FF)V

    .line 278
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    sget-object v6, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_WIDTH:[F

    const/4 v7, 0x1

    aget v6, v6, v7

    sget v7, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_HEIGHT:F

    add-int/lit8 v8, v4, -0x1

    int-to-float v8, v8

    sget v9, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_HEIGHT:F

    mul-float/2addr v8, v9

    add-float/2addr v7, v8

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/glview/TwGLViewGroup;->setSize(FF)V

    goto/16 :goto_4

    .line 328
    :cond_6
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v7}, Lcom/sec/android/glview/TwGLImage;->getTranslateX()F

    move-result v7

    sget-object v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_WIDTH:[F

    const/4 v9, 0x0

    aget v8, v8, v9

    sub-float/2addr v7, v8

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_PICKER_PADDING:F

    add-float/2addr v7, v8

    sget-object v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_PADDING:[F

    const/4 v9, 0x0

    aget v8, v8, v9

    add-float/2addr v7, v8

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_PICKER_EXTRA_PADDING_2:F

    const/high16 v9, 0x40000000    # 2.0f

    div-float/2addr v8, v9

    add-float/2addr v7, v8

    iget-object v8, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v8}, Lcom/sec/android/glview/TwGLImage;->getTranslateY()F

    move-result v8

    sget v9, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_HEIGHT:F

    add-int/lit8 v10, v4, -0x1

    int-to-float v10, v10

    sget v11, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_HEIGHT:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget-object v10, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v10}, Lcom/sec/android/glview/TwGLImage;->getHeight()F

    move-result v10

    sub-float/2addr v9, v10

    const/high16 v10, 0x40000000    # 2.0f

    div-float/2addr v9, v10

    sub-float/2addr v8, v9

    invoke-virtual {v5, v6, v7, v8}, Lcom/sec/android/glview/TwGLViewGroup;->setLeftTop(IFF)V

    .line 330
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    const/4 v6, 0x2

    iget-object v7, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v7}, Lcom/sec/android/glview/TwGLImage;->getTranslateX()F

    move-result v7

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_PICKER_PADDING:F

    add-float/2addr v7, v8

    sget-object v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_PADDING:[F

    const/4 v9, 0x2

    aget v8, v8, v9

    add-float/2addr v7, v8

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_PICKER_EXTRA_PADDING_2:F

    sub-float/2addr v7, v8

    iget-object v8, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v8}, Lcom/sec/android/glview/TwGLImage;->getTranslateY()F

    move-result v8

    sget v9, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_HEIGHT:F

    add-int/lit8 v10, v4, -0x1

    int-to-float v10, v10

    sget v11, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_HEIGHT:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget-object v10, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v10}, Lcom/sec/android/glview/TwGLImage;->getHeight()F

    move-result v10

    add-float/2addr v9, v10

    const/high16 v10, 0x40000000    # 2.0f

    div-float/2addr v9, v10

    add-float/2addr v8, v9

    invoke-virtual {v5, v6, v7, v8}, Lcom/sec/android/glview/TwGLViewGroup;->setLeftTop(IFF)V

    goto/16 :goto_5

    .line 341
    :cond_7
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    const/4 v6, 0x1

    iget-object v7, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v7}, Lcom/sec/android/glview/TwGLImage;->getTranslateX()F

    move-result v7

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_PICKER_PADDING:F

    add-float/2addr v7, v8

    sget-object v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_PADDING:[F

    const/4 v9, 0x1

    aget v8, v8, v9

    add-float/2addr v7, v8

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->SCREEN_HEIGHT:I

    int-to-float v8, v8

    sget-object v9, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_WIDTH:[F

    const/4 v10, 0x1

    aget v9, v9, v10

    sub-float/2addr v8, v9

    const/high16 v9, 0x40000000    # 2.0f

    div-float/2addr v8, v9

    sget v9, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->SCREEN_HEIGHT:I

    div-int/lit8 v9, v9, 0x2

    int-to-float v9, v9

    add-float/2addr v8, v9

    invoke-virtual {v5, v6, v7, v8}, Lcom/sec/android/glview/TwGLViewGroup;->setLeftTop(IFF)V

    .line 343
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    const/4 v6, 0x3

    iget-object v7, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v7}, Lcom/sec/android/glview/TwGLImage;->getTranslateX()F

    move-result v7

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_HEIGHT:F

    add-int/lit8 v9, v4, -0x1

    int-to-float v9, v9

    sget v10, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_HEIGHT:F

    mul-float/2addr v9, v10

    add-float/2addr v8, v9

    sub-float/2addr v7, v8

    sget-object v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_PADDING:[F

    const/4 v9, 0x3

    aget v8, v8, v9

    add-float/2addr v7, v8

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_PICKER_PADDING:F

    add-float/2addr v7, v8

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->SCREEN_HEIGHT:I

    int-to-float v8, v8

    sget v9, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->SCREEN_HEIGHT:I

    int-to-float v9, v9

    sget-object v10, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_WIDTH:[F

    const/4 v11, 0x1

    aget v10, v10, v11

    sub-float/2addr v9, v10

    const/high16 v10, 0x40000000    # 2.0f

    div-float/2addr v9, v10

    sub-float/2addr v8, v9

    sget v9, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->SCREEN_HEIGHT:I

    div-int/lit8 v9, v9, 0x2

    int-to-float v9, v9

    add-float/2addr v8, v9

    invoke-virtual {v5, v6, v7, v8}, Lcom/sec/android/glview/TwGLViewGroup;->setLeftTop(IFF)V

    goto/16 :goto_6

    .line 359
    :pswitch_6
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    const v6, 0x7f0c0227

    invoke-virtual {v5, v6}, Lcom/sec/android/app/camera/Camera;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 360
    invoke-virtual {v3, v0}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v1

    .line 362
    invoke-static {}, Lcom/sec/android/glview/TwGLContext;->getLastOrientation()I

    move-result v5

    packed-switch v5, :pswitch_data_3

    .line 413
    :goto_7
    sget-object v5, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_WIDTH:[F

    const/4 v6, 0x0

    aget v5, v5, v6

    sget v6, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->FONT_SIZE:I

    int-to-float v6, v6

    const/4 v7, 0x0

    invoke-static {v5, v0, v6, v7}, Lcom/sec/android/glview/TwGLText;->measureRows(FLjava/lang/String;FLandroid/graphics/Typeface;)I

    move-result v4

    .line 414
    const/4 v5, 0x1

    if-ne v4, v5, :cond_a

    .line 415
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v7}, Lcom/sec/android/glview/TwGLImage;->getTranslateX()F

    move-result v7

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_PICKER_PADDING:F

    add-float/2addr v7, v8

    sget-object v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_PADDING:[F

    const/4 v9, 0x0

    aget v8, v8, v9

    add-float/2addr v7, v8

    const/high16 v8, 0x40000000    # 2.0f

    sget v9, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_WIDTH_MARGIN:F

    mul-float/2addr v8, v9

    add-float/2addr v8, v1

    sub-float/2addr v7, v8

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_PICKER_OFFSET:F

    sub-float/2addr v7, v8

    iget-object v8, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v8}, Lcom/sec/android/glview/TwGLImage;->getTranslateY()F

    move-result v8

    sget v9, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_HEIGHT:F

    add-int/lit8 v10, v4, -0x1

    int-to-float v10, v10

    sget v11, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_HEIGHT:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget-object v10, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v10}, Lcom/sec/android/glview/TwGLImage;->getHeight()F

    move-result v10

    sub-float/2addr v9, v10

    const/high16 v10, 0x40000000    # 2.0f

    div-float/2addr v9, v10

    sub-float/2addr v8, v9

    invoke-virtual {v5, v6, v7, v8}, Lcom/sec/android/glview/TwGLViewGroup;->setLeftTop(IFF)V

    .line 417
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    const/4 v6, 0x2

    iget-object v7, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v7}, Lcom/sec/android/glview/TwGLImage;->getTranslateX()F

    move-result v7

    const/high16 v8, 0x40400000    # 3.0f

    sget v9, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_PICKER_PADDING:F

    mul-float/2addr v8, v9

    const/high16 v9, 0x40000000    # 2.0f

    div-float/2addr v8, v9

    add-float/2addr v7, v8

    sget-object v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_PADDING:[F

    const/4 v9, 0x0

    aget v8, v8, v9

    add-float/2addr v7, v8

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_PICKER_OFFSET:F

    sub-float/2addr v7, v8

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_PICKER_EXTRA_PADDING_2:F

    sub-float/2addr v7, v8

    iget-object v8, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v8}, Lcom/sec/android/glview/TwGLImage;->getTranslateY()F

    move-result v8

    sget v9, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_HEIGHT:F

    add-int/lit8 v10, v4, -0x1

    int-to-float v10, v10

    sget v11, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_HEIGHT:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget-object v10, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v10}, Lcom/sec/android/glview/TwGLImage;->getHeight()F

    move-result v10

    add-float/2addr v9, v10

    const/high16 v10, 0x40000000    # 2.0f

    div-float/2addr v9, v10

    add-float/2addr v8, v9

    invoke-virtual {v5, v6, v7, v8}, Lcom/sec/android/glview/TwGLViewGroup;->setLeftTop(IFF)V

    .line 426
    :goto_8
    sget-object v5, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_WIDTH:[F

    const/4 v6, 0x1

    aget v5, v5, v6

    sget v6, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->FONT_SIZE:I

    int-to-float v6, v6

    const/4 v7, 0x0

    invoke-static {v5, v0, v6, v7}, Lcom/sec/android/glview/TwGLText;->measureRows(FLjava/lang/String;FLandroid/graphics/Typeface;)I

    move-result v4

    .line 427
    const/4 v5, 0x1

    if-ne v4, v5, :cond_c

    .line 428
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v5}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/camera/CameraSettings;->isEasyMode()Z

    move-result v5

    if-nez v5, :cond_b

    .line 429
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    const/4 v6, 0x1

    iget-object v7, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v7}, Lcom/sec/android/glview/TwGLImage;->getTranslateX()F

    move-result v7

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_PICKER_PADDING:F

    const/high16 v9, 0x40000000    # 2.0f

    div-float/2addr v8, v9

    sub-float/2addr v7, v8

    sget-object v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_PADDING:[F

    const/4 v9, 0x2

    aget v8, v8, v9

    add-float/2addr v7, v8

    const/high16 v8, 0x40000000    # 2.0f

    sget v9, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_PICKER_OFFSET:F

    mul-float/2addr v8, v9

    add-float/2addr v7, v8

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_PICKER_EXTRA_PADDING_2:F

    add-float/2addr v7, v8

    iget-object v8, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v8}, Lcom/sec/android/glview/TwGLImage;->getTranslateY()F

    move-result v8

    const/high16 v9, 0x40400000    # 3.0f

    sget v10, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_WIDTH_MARGIN:F

    mul-float/2addr v9, v10

    sub-float/2addr v8, v9

    invoke-virtual {v5, v6, v7, v8}, Lcom/sec/android/glview/TwGLViewGroup;->setLeftTop(IFF)V

    .line 431
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    const/4 v6, 0x3

    iget-object v7, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v7}, Lcom/sec/android/glview/TwGLImage;->getTranslateX()F

    move-result v7

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_HEIGHT:F

    add-int/lit8 v9, v4, -0x1

    int-to-float v9, v9

    sget v10, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_HEIGHT:F

    mul-float/2addr v9, v10

    add-float/2addr v8, v9

    sub-float/2addr v7, v8

    sget-object v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_PADDING:[F

    const/4 v9, 0x3

    aget v8, v8, v9

    add-float/2addr v7, v8

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_PICKER_PADDING:F

    add-float/2addr v7, v8

    iget-object v8, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v8}, Lcom/sec/android/glview/TwGLImage;->getTranslateY()F

    move-result v8

    add-float/2addr v8, v1

    sget v9, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_WIDTH_MARGIN:F

    sub-float/2addr v8, v9

    invoke-virtual {v5, v6, v7, v8}, Lcom/sec/android/glview/TwGLViewGroup;->setLeftTop(IFF)V

    .line 446
    :goto_9
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    invoke-virtual {v5, v2}, Lcom/sec/android/glview/TwGLViewGroup;->setOrientation(I)V

    .line 447
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    iget-object v6, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mShowAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v5, v6}, Lcom/sec/android/glview/TwGLViewGroup;->setAnimation(Landroid/view/animation/Animation;)V

    .line 448
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    iget-object v6, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mShowAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v5, v6}, Lcom/sec/android/glview/TwGLImage;->setAnimation(Landroid/view/animation/Animation;)V

    .line 449
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lcom/sec/android/glview/TwGLViewGroup;->updateLayout(Z)V

    goto/16 :goto_0

    .line 365
    :pswitch_7
    sget-object v5, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_WIDTH:[F

    const/4 v6, 0x0

    aget v5, v5, v6

    sget v6, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->FONT_SIZE:I

    int-to-float v6, v6

    const/4 v7, 0x0

    invoke-static {v5, v0, v6, v7}, Lcom/sec/android/glview/TwGLText;->measureRows(FLjava/lang/String;FLandroid/graphics/Typeface;)I

    move-result v4

    .line 366
    const/4 v5, 0x1

    if-ne v4, v5, :cond_8

    .line 367
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpText:Lcom/sec/android/glview/TwGLText;

    sget v6, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_WIDTH_MARGIN:F

    const/high16 v7, 0x40000000    # 2.0f

    mul-float/2addr v6, v7

    add-float/2addr v6, v1

    int-to-float v7, v4

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_HEIGHT:F

    mul-float/2addr v7, v8

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/glview/TwGLText;->setSize(FF)V

    .line 368
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    sget v6, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_WIDTH_MARGIN:F

    const/high16 v7, 0x40000000    # 2.0f

    mul-float/2addr v6, v7

    add-float/2addr v6, v1

    sget v7, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_HEIGHT:F

    add-int/lit8 v8, v4, -0x1

    int-to-float v8, v8

    sget v9, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_HEIGHT:F

    mul-float/2addr v8, v9

    add-float/2addr v7, v8

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/glview/TwGLViewGroup;->setSize(FF)V

    goto/16 :goto_7

    .line 370
    :cond_8
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpText:Lcom/sec/android/glview/TwGLText;

    sget-object v6, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_WIDTH:[F

    const/4 v7, 0x0

    aget v6, v6, v7

    int-to-float v7, v4

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_HEIGHT:F

    mul-float/2addr v7, v8

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/glview/TwGLText;->setSize(FF)V

    .line 371
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    sget-object v6, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_WIDTH:[F

    const/4 v7, 0x0

    aget v6, v6, v7

    sget v7, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_HEIGHT:F

    add-int/lit8 v8, v4, -0x1

    int-to-float v8, v8

    sget v9, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_HEIGHT:F

    mul-float/2addr v8, v9

    add-float/2addr v7, v8

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/glview/TwGLViewGroup;->setSize(FF)V

    goto/16 :goto_7

    .line 376
    :pswitch_8
    sget-object v5, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_WIDTH:[F

    const/4 v6, 0x1

    aget v5, v5, v6

    sget v6, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->FONT_SIZE:I

    int-to-float v6, v6

    const/4 v7, 0x0

    invoke-static {v5, v0, v6, v7}, Lcom/sec/android/glview/TwGLText;->measureRows(FLjava/lang/String;FLandroid/graphics/Typeface;)I

    move-result v4

    .line 377
    const/4 v5, 0x1

    if-ne v4, v5, :cond_9

    .line 378
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpText:Lcom/sec/android/glview/TwGLText;

    sget v6, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_WIDTH_MARGIN:F

    const/high16 v7, 0x40000000    # 2.0f

    mul-float/2addr v6, v7

    add-float/2addr v6, v1

    int-to-float v7, v4

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_HEIGHT:F

    mul-float/2addr v7, v8

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/glview/TwGLText;->setSize(FF)V

    .line 379
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    sget v6, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_WIDTH_MARGIN:F

    const/high16 v7, 0x40000000    # 2.0f

    mul-float/2addr v6, v7

    add-float/2addr v6, v1

    sget v7, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_HEIGHT:F

    add-int/lit8 v8, v4, -0x1

    int-to-float v8, v8

    sget v9, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_HEIGHT:F

    mul-float/2addr v8, v9

    add-float/2addr v7, v8

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/glview/TwGLViewGroup;->setSize(FF)V

    goto/16 :goto_7

    .line 381
    :cond_9
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpText:Lcom/sec/android/glview/TwGLText;

    sget-object v6, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_WIDTH:[F

    const/4 v7, 0x1

    aget v6, v6, v7

    int-to-float v7, v4

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_HEIGHT:F

    mul-float/2addr v7, v8

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/glview/TwGLText;->setSize(FF)V

    .line 382
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    sget-object v6, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_WIDTH:[F

    const/4 v7, 0x1

    aget v6, v6, v7

    sget v7, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_HEIGHT:F

    add-int/lit8 v8, v4, -0x1

    int-to-float v8, v8

    sget v9, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_HEIGHT:F

    mul-float/2addr v8, v9

    add-float/2addr v7, v8

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/glview/TwGLViewGroup;->setSize(FF)V

    goto/16 :goto_7

    .line 420
    :cond_a
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v7}, Lcom/sec/android/glview/TwGLImage;->getTranslateX()F

    move-result v7

    sget-object v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_WIDTH:[F

    const/4 v9, 0x0

    aget v8, v8, v9

    sub-float/2addr v7, v8

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_PICKER_PADDING:F

    add-float/2addr v7, v8

    sget-object v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_PADDING:[F

    const/4 v9, 0x0

    aget v8, v8, v9

    add-float/2addr v7, v8

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_PICKER_OFFSET:F

    sub-float/2addr v7, v8

    iget-object v8, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v8}, Lcom/sec/android/glview/TwGLImage;->getTranslateY()F

    move-result v8

    sget v9, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_HEIGHT:F

    add-int/lit8 v10, v4, -0x1

    int-to-float v10, v10

    sget v11, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_HEIGHT:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget-object v10, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v10}, Lcom/sec/android/glview/TwGLImage;->getHeight()F

    move-result v10

    sub-float/2addr v9, v10

    const/high16 v10, 0x40000000    # 2.0f

    div-float/2addr v9, v10

    sub-float/2addr v8, v9

    invoke-virtual {v5, v6, v7, v8}, Lcom/sec/android/glview/TwGLViewGroup;->setLeftTop(IFF)V

    .line 422
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    const/4 v6, 0x2

    iget-object v7, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v7}, Lcom/sec/android/glview/TwGLImage;->getTranslateX()F

    move-result v7

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_PICKER_PADDING:F

    add-float/2addr v7, v8

    sget-object v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_PADDING:[F

    const/4 v9, 0x2

    aget v8, v8, v9

    add-float/2addr v7, v8

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_PICKER_OFFSET:F

    sub-float/2addr v7, v8

    iget-object v8, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v8}, Lcom/sec/android/glview/TwGLImage;->getTranslateY()F

    move-result v8

    sget v9, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_HEIGHT:F

    add-int/lit8 v10, v4, -0x1

    int-to-float v10, v10

    sget v11, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_HEIGHT:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget-object v10, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v10}, Lcom/sec/android/glview/TwGLImage;->getHeight()F

    move-result v10

    add-float/2addr v9, v10

    const/high16 v10, 0x40000000    # 2.0f

    div-float/2addr v9, v10

    add-float/2addr v8, v9

    invoke-virtual {v5, v6, v7, v8}, Lcom/sec/android/glview/TwGLViewGroup;->setLeftTop(IFF)V

    goto/16 :goto_8

    .line 434
    :cond_b
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    const/4 v6, 0x1

    iget-object v7, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v7}, Lcom/sec/android/glview/TwGLImage;->getTranslateX()F

    move-result v7

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_PICKER_PADDING:F

    const/high16 v9, 0x40000000    # 2.0f

    div-float/2addr v8, v9

    sub-float/2addr v7, v8

    sget-object v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_PADDING:[F

    const/4 v9, 0x2

    aget v8, v8, v9

    add-float/2addr v7, v8

    const/high16 v8, 0x40000000    # 2.0f

    sget v9, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_PICKER_OFFSET:F

    mul-float/2addr v8, v9

    add-float/2addr v7, v8

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_PICKER_EXTRA_PADDING_2:F

    add-float/2addr v7, v8

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->SCREEN_HEIGHT:I

    int-to-float v8, v8

    sget v9, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_WIDTH_MARGIN:F

    const/high16 v10, 0x40000000    # 2.0f

    mul-float/2addr v9, v10

    add-float/2addr v9, v1

    sub-float/2addr v8, v9

    const/high16 v9, 0x40000000    # 2.0f

    div-float/2addr v8, v9

    sget v9, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->SCREEN_HEIGHT:I

    div-int/lit8 v9, v9, 0x2

    int-to-float v9, v9

    add-float/2addr v8, v9

    invoke-virtual {v5, v6, v7, v8}, Lcom/sec/android/glview/TwGLViewGroup;->setLeftTop(IFF)V

    .line 436
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    const/4 v6, 0x3

    iget-object v7, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v7}, Lcom/sec/android/glview/TwGLImage;->getTranslateX()F

    move-result v7

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_HEIGHT:F

    add-int/lit8 v9, v4, -0x1

    int-to-float v9, v9

    sget v10, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_HEIGHT:F

    mul-float/2addr v9, v10

    add-float/2addr v8, v9

    sub-float/2addr v7, v8

    sget-object v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_PADDING:[F

    const/4 v9, 0x3

    aget v8, v8, v9

    add-float/2addr v7, v8

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_PICKER_PADDING:F

    add-float/2addr v7, v8

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->SCREEN_HEIGHT:I

    int-to-float v8, v8

    sget v9, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->SCREEN_HEIGHT:I

    int-to-float v9, v9

    sget v10, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_WIDTH_MARGIN:F

    const/high16 v11, 0x40000000    # 2.0f

    mul-float/2addr v10, v11

    add-float/2addr v10, v1

    sub-float/2addr v9, v10

    const/high16 v10, 0x40000000    # 2.0f

    div-float/2addr v9, v10

    sub-float/2addr v8, v9

    sget v9, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->SCREEN_HEIGHT:I

    div-int/lit8 v9, v9, 0x2

    int-to-float v9, v9

    add-float/2addr v8, v9

    invoke-virtual {v5, v6, v7, v8}, Lcom/sec/android/glview/TwGLViewGroup;->setLeftTop(IFF)V

    goto/16 :goto_9

    .line 440
    :cond_c
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    const/4 v6, 0x1

    iget-object v7, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v7}, Lcom/sec/android/glview/TwGLImage;->getTranslateX()F

    move-result v7

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_PICKER_PADDING:F

    add-float/2addr v7, v8

    sget-object v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_PADDING:[F

    const/4 v9, 0x1

    aget v8, v8, v9

    add-float/2addr v7, v8

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->SCREEN_HEIGHT:I

    int-to-float v8, v8

    sget-object v9, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_WIDTH:[F

    const/4 v10, 0x1

    aget v9, v9, v10

    sub-float/2addr v8, v9

    const/high16 v9, 0x40000000    # 2.0f

    div-float/2addr v8, v9

    sget v9, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->SCREEN_HEIGHT:I

    div-int/lit8 v9, v9, 0x2

    int-to-float v9, v9

    add-float/2addr v8, v9

    invoke-virtual {v5, v6, v7, v8}, Lcom/sec/android/glview/TwGLViewGroup;->setLeftTop(IFF)V

    .line 442
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    const/4 v6, 0x3

    iget-object v7, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v7}, Lcom/sec/android/glview/TwGLImage;->getTranslateX()F

    move-result v7

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_HEIGHT:F

    add-int/lit8 v9, v4, -0x1

    int-to-float v9, v9

    sget v10, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_HEIGHT:F

    mul-float/2addr v9, v10

    add-float/2addr v8, v9

    sub-float/2addr v7, v8

    sget-object v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_PADDING:[F

    const/4 v9, 0x3

    aget v8, v8, v9

    add-float/2addr v7, v8

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_PICKER_PADDING:F

    add-float/2addr v7, v8

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->SCREEN_HEIGHT:I

    int-to-float v8, v8

    sget v9, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->SCREEN_HEIGHT:I

    int-to-float v9, v9

    sget-object v10, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_WIDTH:[F

    const/4 v11, 0x1

    aget v10, v10, v11

    sub-float/2addr v9, v10

    const/high16 v10, 0x40000000    # 2.0f

    div-float/2addr v9, v10

    sub-float/2addr v8, v9

    sget v9, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->SCREEN_HEIGHT:I

    div-int/lit8 v9, v9, 0x2

    int-to-float v9, v9

    add-float/2addr v8, v9

    invoke-virtual {v5, v6, v7, v8}, Lcom/sec/android/glview/TwGLViewGroup;->setLeftTop(IFF)V

    goto/16 :goto_9

    .line 452
    :pswitch_9
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    const v6, 0x7f0c022f

    invoke-virtual {v5, v6}, Lcom/sec/android/app/camera/Camera;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 453
    invoke-virtual {v3, v0}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v1

    .line 455
    invoke-static {}, Lcom/sec/android/glview/TwGLContext;->getLastOrientation()I

    move-result v5

    packed-switch v5, :pswitch_data_4

    .line 483
    :goto_a
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    const/4 v6, 0x0

    sget v7, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->SCREEN_WIDTH:I

    int-to-float v7, v7

    iget-object v8, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    invoke-virtual {v8}, Lcom/sec/android/glview/TwGLViewGroup;->getWidth()F

    move-result v8

    const/high16 v9, 0x40000000    # 2.0f

    div-float/2addr v8, v9

    sub-float/2addr v7, v8

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->SCREEN_HEIGHT:I

    sget v9, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->SCREEN_HEIGHT:I

    div-int/lit8 v9, v9, 0x2

    add-int/2addr v8, v9

    int-to-float v8, v8

    iget-object v9, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    invoke-virtual {v9}, Lcom/sec/android/glview/TwGLViewGroup;->getHeight()F

    move-result v9

    sub-float/2addr v8, v9

    invoke-virtual {v5, v6, v7, v8}, Lcom/sec/android/glview/TwGLViewGroup;->setLeftTop(IFF)V

    .line 484
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    const/4 v6, 0x2

    sget v7, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->SCREEN_WIDTH:I

    int-to-float v7, v7

    iget-object v8, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    invoke-virtual {v8}, Lcom/sec/android/glview/TwGLViewGroup;->getWidth()F

    move-result v8

    const/high16 v9, 0x40000000    # 2.0f

    div-float/2addr v8, v9

    add-float/2addr v7, v8

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->SCREEN_HEIGHT:I

    sget v9, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->SCREEN_HEIGHT:I

    div-int/lit8 v9, v9, 0x2

    sub-int/2addr v8, v9

    int-to-float v8, v8

    iget-object v9, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    invoke-virtual {v9}, Lcom/sec/android/glview/TwGLViewGroup;->getHeight()F

    move-result v9

    add-float/2addr v8, v9

    invoke-virtual {v5, v6, v7, v8}, Lcom/sec/android/glview/TwGLViewGroup;->setLeftTop(IFF)V

    .line 486
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    const/4 v6, 0x1

    iget-object v7, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    invoke-virtual {v7}, Lcom/sec/android/glview/TwGLViewGroup;->getHeight()F

    move-result v7

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->SCREEN_WIDTH:I

    div-int/lit8 v8, v8, 0x2

    int-to-float v8, v8

    add-float/2addr v7, v8

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->SCREEN_HEIGHT:I

    int-to-float v8, v8

    iget-object v9, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    invoke-virtual {v9}, Lcom/sec/android/glview/TwGLViewGroup;->getWidth()F

    move-result v9

    sub-float/2addr v8, v9

    const/high16 v9, 0x40000000    # 2.0f

    div-float/2addr v8, v9

    sget v9, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->SCREEN_HEIGHT:I

    div-int/lit8 v9, v9, 0x2

    int-to-float v9, v9

    add-float/2addr v8, v9

    invoke-virtual {v5, v6, v7, v8}, Lcom/sec/android/glview/TwGLViewGroup;->setLeftTop(IFF)V

    .line 487
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    const/4 v6, 0x3

    sget v7, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->SCREEN_WIDTH:I

    int-to-float v7, v7

    iget-object v8, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    invoke-virtual {v8}, Lcom/sec/android/glview/TwGLViewGroup;->getHeight()F

    move-result v8

    sub-float/2addr v7, v8

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->SCREEN_WIDTH:I

    int-to-float v8, v8

    sget v9, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->CAMCORDER_BUTTON_X:F

    sget v10, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->RIGHT_SIDE_BUTTON_WIDTH:F

    iget-object v11, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpFocus:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v11}, Lcom/sec/android/glview/TwGLImage;->getWidth()F

    move-result v11

    sub-float/2addr v10, v11

    const/high16 v11, 0x40000000    # 2.0f

    div-float/2addr v10, v11

    add-float/2addr v9, v10

    sub-float/2addr v8, v9

    sub-float/2addr v7, v8

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->SCREEN_WIDTH:I

    div-int/lit8 v8, v8, 0x2

    int-to-float v8, v8

    add-float/2addr v7, v8

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->SCREEN_HEIGHT:I

    int-to-float v8, v8

    sget v9, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->SCREEN_HEIGHT:I

    int-to-float v9, v9

    iget-object v10, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    invoke-virtual {v10}, Lcom/sec/android/glview/TwGLViewGroup;->getWidth()F

    move-result v10

    sub-float/2addr v9, v10

    const/high16 v10, 0x40000000    # 2.0f

    div-float/2addr v9, v10

    sub-float/2addr v8, v9

    sget v9, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->SCREEN_HEIGHT:I

    div-int/lit8 v9, v9, 0x2

    int-to-float v9, v9

    add-float/2addr v8, v9

    invoke-virtual {v5, v6, v7, v8}, Lcom/sec/android/glview/TwGLViewGroup;->setLeftTop(IFF)V

    .line 490
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    invoke-static {}, Lcom/sec/android/glview/TwGLContext;->getLastOrientation()I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/sec/android/glview/TwGLViewGroup;->setOrientation(I)V

    .line 491
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    iget-object v6, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mShowAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v5, v6}, Lcom/sec/android/glview/TwGLViewGroup;->setAnimation(Landroid/view/animation/Animation;)V

    .line 492
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lcom/sec/android/glview/TwGLViewGroup;->updateLayout(Z)V

    goto/16 :goto_0

    .line 458
    :pswitch_a
    sget-object v5, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_WIDTH:[F

    const/4 v6, 0x0

    aget v5, v5, v6

    sget v6, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->FONT_SIZE:I

    int-to-float v6, v6

    const/4 v7, 0x0

    invoke-static {v5, v0, v6, v7}, Lcom/sec/android/glview/TwGLText;->measureRows(FLjava/lang/String;FLandroid/graphics/Typeface;)I

    move-result v4

    .line 459
    const/4 v5, 0x1

    if-ne v4, v5, :cond_d

    .line 460
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpText:Lcom/sec/android/glview/TwGLText;

    sget v6, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_WIDTH_MARGIN:F

    const/high16 v7, 0x40000000    # 2.0f

    mul-float/2addr v6, v7

    add-float/2addr v6, v1

    sget v7, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_HEIGHT:F

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/glview/TwGLText;->setSize(FF)V

    .line 461
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    sget v6, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_WIDTH_MARGIN:F

    const/high16 v7, 0x40000000    # 2.0f

    mul-float/2addr v6, v7

    add-float/2addr v6, v1

    sget v7, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_HEIGHT:F

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/glview/TwGLViewGroup;->setSize(FF)V

    goto/16 :goto_a

    .line 463
    :cond_d
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpText:Lcom/sec/android/glview/TwGLText;

    sget-object v6, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_WIDTH:[F

    const/4 v7, 0x0

    aget v6, v6, v7

    int-to-float v7, v4

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_HEIGHT:F

    mul-float/2addr v7, v8

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/glview/TwGLText;->setSize(FF)V

    .line 464
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    sget-object v6, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_WIDTH:[F

    const/4 v7, 0x0

    aget v6, v6, v7

    sget v7, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_HEIGHT:F

    add-int/lit8 v8, v4, -0x1

    int-to-float v8, v8

    sget v9, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_HEIGHT:F

    mul-float/2addr v8, v9

    add-float/2addr v7, v8

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/glview/TwGLViewGroup;->setSize(FF)V

    goto/16 :goto_a

    .line 470
    :pswitch_b
    sget-object v5, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_WIDTH:[F

    const/4 v6, 0x1

    aget v5, v5, v6

    sget v6, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->FONT_SIZE:I

    int-to-float v6, v6

    const/4 v7, 0x0

    invoke-static {v5, v0, v6, v7}, Lcom/sec/android/glview/TwGLText;->measureRows(FLjava/lang/String;FLandroid/graphics/Typeface;)I

    move-result v4

    .line 471
    const/4 v5, 0x1

    if-ne v4, v5, :cond_e

    .line 472
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpText:Lcom/sec/android/glview/TwGLText;

    sget v6, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_WIDTH_MARGIN:F

    const/high16 v7, 0x40000000    # 2.0f

    mul-float/2addr v6, v7

    add-float/2addr v6, v1

    sget v7, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_HEIGHT:F

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/glview/TwGLText;->setSize(FF)V

    .line 473
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    sget v6, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_WIDTH_MARGIN:F

    const/high16 v7, 0x40000000    # 2.0f

    mul-float/2addr v6, v7

    add-float/2addr v6, v1

    sget v7, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_HEIGHT:F

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/glview/TwGLViewGroup;->setSize(FF)V

    goto/16 :goto_a

    .line 475
    :cond_e
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpText:Lcom/sec/android/glview/TwGLText;

    sget-object v6, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_WIDTH:[F

    const/4 v7, 0x1

    aget v6, v6, v7

    int-to-float v7, v4

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_HEIGHT:F

    mul-float/2addr v7, v8

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/glview/TwGLText;->setSize(FF)V

    .line 476
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    sget-object v6, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_WIDTH:[F

    const/4 v7, 0x1

    aget v6, v6, v7

    sget v7, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_HEIGHT:F

    add-int/lit8 v8, v4, -0x1

    int-to-float v8, v8

    sget v9, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_HEIGHT:F

    mul-float/2addr v8, v9

    add-float/2addr v7, v8

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/glview/TwGLViewGroup;->setSize(FF)V

    goto/16 :goto_a

    .line 167
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_3
        :pswitch_6
        :pswitch_9
    .end packed-switch

    .line 172
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 258
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
        :pswitch_4
        :pswitch_5
    .end packed-switch

    .line 362
    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_7
        :pswitch_8
        :pswitch_7
        :pswitch_8
    .end packed-switch

    .line 455
    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_a
        :pswitch_b
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method public setSizeForOrientation(I)V
    .locals 11
    .param p1, "orientation"    # I

    .prologue
    const/4 v6, 0x0

    const/4 v10, 0x0

    const/4 v9, 0x4

    const/high16 v8, 0x40000000    # 2.0f

    const/4 v7, 0x1

    .line 684
    const/4 v2, 0x0

    .line 685
    .local v2, "rows":I
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 686
    .local v1, "paint":Landroid/graphics/Paint;
    const/4 v0, 0x0

    .line 687
    .local v0, "mTextSize":F
    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->FONT_SIZE:I

    int-to-float v3, v3

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 689
    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpText:Lcom/sec/android/glview/TwGLText;

    if-eqz v3, :cond_0

    .line 690
    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpText:Lcom/sec/android/glview/TwGLText;

    invoke-virtual {v3}, Lcom/sec/android/glview/TwGLText;->getText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v0

    .line 693
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 735
    :cond_1
    :goto_0
    return-void

    .line 696
    :pswitch_0
    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpText:Lcom/sec/android/glview/TwGLText;

    if-eqz v3, :cond_2

    iget v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mTutorialStep:I

    if-eq v3, v9, :cond_2

    .line 697
    sget-object v3, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_WIDTH:[F

    aget v3, v3, v10

    iget-object v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpText:Lcom/sec/android/glview/TwGLText;

    invoke-virtual {v4}, Lcom/sec/android/glview/TwGLText;->getText()Ljava/lang/String;

    move-result-object v4

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->FONT_SIZE:I

    int-to-float v5, v5

    invoke-static {v3, v4, v5, v6}, Lcom/sec/android/glview/TwGLText;->measureRows(FLjava/lang/String;FLandroid/graphics/Typeface;)I

    move-result v2

    .line 698
    if-ne v2, v7, :cond_4

    .line 699
    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpText:Lcom/sec/android/glview/TwGLText;

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_WIDTH_MARGIN:F

    mul-float/2addr v4, v8

    add-float/2addr v4, v0

    int-to-float v5, v2

    sget v6, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_HEIGHT:F

    mul-float/2addr v5, v6

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/glview/TwGLText;->setSize(FF)V

    .line 703
    :cond_2
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    if-eqz v3, :cond_1

    .line 704
    iget v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mTutorialStep:I

    if-eq v3, v9, :cond_3

    .line 705
    if-ne v2, v7, :cond_5

    .line 706
    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_WIDTH_MARGIN:F

    mul-float/2addr v4, v8

    add-float/2addr v4, v0

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_HEIGHT:F

    add-int/lit8 v6, v2, -0x1

    int-to-float v6, v6

    sget v7, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_HEIGHT:F

    mul-float/2addr v6, v7

    add-float/2addr v5, v6

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/glview/TwGLViewGroup;->setSize(FF)V

    .line 710
    :cond_3
    :goto_2
    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    invoke-virtual {v3, p1}, Lcom/sec/android/glview/TwGLViewGroup;->setOrientation(I)V

    goto :goto_0

    .line 701
    :cond_4
    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpText:Lcom/sec/android/glview/TwGLText;

    sget-object v4, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_WIDTH:[F

    aget v4, v4, v10

    int-to-float v5, v2

    sget v6, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_HEIGHT:F

    mul-float/2addr v5, v6

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/glview/TwGLText;->setSize(FF)V

    goto :goto_1

    .line 708
    :cond_5
    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    sget-object v4, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_WIDTH:[F

    aget v4, v4, v10

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_HEIGHT:F

    add-int/lit8 v6, v2, -0x1

    int-to-float v6, v6

    sget v7, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_HEIGHT:F

    mul-float/2addr v6, v7

    add-float/2addr v5, v6

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/glview/TwGLViewGroup;->setSize(FF)V

    goto :goto_2

    .line 715
    :pswitch_1
    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpText:Lcom/sec/android/glview/TwGLText;

    if-eqz v3, :cond_6

    iget v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mTutorialStep:I

    if-eq v3, v9, :cond_6

    .line 716
    sget-object v3, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_WIDTH:[F

    aget v3, v3, v7

    iget-object v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpText:Lcom/sec/android/glview/TwGLText;

    invoke-virtual {v4}, Lcom/sec/android/glview/TwGLText;->getText()Ljava/lang/String;

    move-result-object v4

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->FONT_SIZE:I

    int-to-float v5, v5

    invoke-static {v3, v4, v5, v6}, Lcom/sec/android/glview/TwGLText;->measureRows(FLjava/lang/String;FLandroid/graphics/Typeface;)I

    move-result v2

    .line 717
    if-ne v2, v7, :cond_8

    .line 718
    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpText:Lcom/sec/android/glview/TwGLText;

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_WIDTH_MARGIN:F

    mul-float/2addr v4, v8

    add-float/2addr v4, v0

    int-to-float v5, v2

    sget v6, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_HEIGHT:F

    mul-float/2addr v5, v6

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/glview/TwGLText;->setSize(FF)V

    .line 722
    :cond_6
    :goto_3
    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    if-eqz v3, :cond_1

    .line 723
    iget v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mTutorialStep:I

    if-eq v3, v9, :cond_7

    .line 724
    if-ne v2, v7, :cond_9

    .line 725
    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_WIDTH_MARGIN:F

    mul-float/2addr v4, v8

    add-float/2addr v4, v0

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_HEIGHT:F

    add-int/lit8 v6, v2, -0x1

    int-to-float v6, v6

    sget v7, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_HEIGHT:F

    mul-float/2addr v6, v7

    add-float/2addr v5, v6

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/glview/TwGLViewGroup;->setSize(FF)V

    .line 729
    :cond_7
    :goto_4
    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    invoke-virtual {v3, p1}, Lcom/sec/android/glview/TwGLViewGroup;->setOrientation(I)V

    goto/16 :goto_0

    .line 720
    :cond_8
    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpText:Lcom/sec/android/glview/TwGLText;

    sget-object v4, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_WIDTH:[F

    aget v4, v4, v7

    int-to-float v5, v2

    sget v6, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_HEIGHT:F

    mul-float/2addr v5, v6

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/glview/TwGLText;->setSize(FF)V

    goto :goto_3

    .line 727
    :cond_9
    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    sget-object v4, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_WIDTH:[F

    aget v4, v4, v7

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_POPUP_HEIGHT:F

    add-int/lit8 v6, v2, -0x1

    int-to-float v6, v6

    sget v7, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->HELP_TEXT_HEIGHT:F

    mul-float/2addr v6, v7

    add-float/2addr v5, v6

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/glview/TwGLViewGroup;->setSize(FF)V

    goto :goto_4

    .line 693
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setStepComplete()V
    .locals 3

    .prologue
    const/4 v1, 0x4

    .line 616
    iput v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mTutorialStep:I

    .line 617
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpFocus:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 618
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 619
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mTutorialStep:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->setHelpText(I)V

    .line 620
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpViewGroup:Lcom/sec/android/glview/TwGLViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLViewGroup;->setVisibility(I)V

    .line 621
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    invoke-virtual {v0}, Lcom/sec/android/glview/TwGLViewGroup;->startAnimation()V

    .line 622
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getSubViewManager()Lcom/sec/android/app/camera/subview/SubViewManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 623
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getSubViewManager()Lcom/sec/android/app/camera/subview/SubViewManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewManager;->getTutorialRecordingMode()Lcom/sec/android/app/camera/subview/SubViewTutorialRecordingMode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewTutorialRecordingMode;->setStepComplete()V

    .line 625
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getMenuDimController()Lcom/sec/android/app/camera/MenuDimController;

    move-result-object v0

    const/16 v1, 0xca

    iget v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mTutorialStep:I

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/camera/MenuDimController;->refreshButtonDim(II)V

    .line 626
    return-void
.end method

.method public setStepHide()V
    .locals 2

    .prologue
    .line 155
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpViewGroup:Lcom/sec/android/glview/TwGLViewGroup;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLViewGroup;->setVisibility(I)V

    .line 156
    return-void
.end method

.method public setStepRecordingStart()V
    .locals 6

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    .line 502
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mTutorialStep:I

    .line 503
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpFocus:Lcom/sec/android/glview/TwGLImage;

    sget v1, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->CAMCORDER_BUTTON_X:F

    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->RIGHT_SIDE_BUTTON_WIDTH:F

    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpFocus:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v3}, Lcom/sec/android/glview/TwGLImage;->getWidth()F

    move-result v3

    sub-float/2addr v2, v3

    div-float/2addr v2, v5

    add-float/2addr v1, v2

    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->CAMCORDER_BUTTON_Y:F

    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->RIGHT_SIDE_BUTTON_HEIGHT:F

    iget-object v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpFocus:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v4}, Lcom/sec/android/glview/TwGLImage;->getHeight()F

    move-result v4

    sub-float/2addr v3, v4

    div-float/2addr v3, v5

    add-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/glview/TwGLImage;->translateAbsolute(FF)V

    .line 505
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpFocus:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v1}, Lcom/sec/android/glview/TwGLImage;->getTranslateX()F

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v2}, Lcom/sec/android/glview/TwGLImage;->getWidth()F

    move-result v2

    sub-float/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpFocus:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v2}, Lcom/sec/android/glview/TwGLImage;->getTranslateY()F

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpFocus:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v3}, Lcom/sec/android/glview/TwGLImage;->getHeight()F

    move-result v3

    iget-object v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v4}, Lcom/sec/android/glview/TwGLImage;->getHeight()F

    move-result v4

    sub-float/2addr v3, v4

    div-float/2addr v3, v5

    add-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/glview/TwGLImage;->translateAbsolute(FF)V

    .line 507
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    sget v1, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->SCREEN_WIDTH:I

    int-to-float v1, v1

    neg-float v1, v1

    div-float/2addr v1, v5

    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->SCREEN_HEIGHT:I

    int-to-float v2, v2

    neg-float v2, v2

    div-float/2addr v2, v5

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/glview/TwGLViewGroup;->translateAbsolute(FF)V

    .line 508
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpViewGroup:Lcom/sec/android/glview/TwGLViewGroup;

    sget v1, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->SCREEN_WIDTH:I

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->SCREEN_HEIGHT:I

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/glview/TwGLViewGroup;->translate(FF)V

    .line 509
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mTutorialStep:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->setHelpText(I)V

    .line 510
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpViewGroup:Lcom/sec/android/glview/TwGLViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLViewGroup;->setVisibility(I)V

    .line 511
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    invoke-virtual {v0}, Lcom/sec/android/glview/TwGLViewGroup;->startAnimation()V

    .line 512
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v0}, Lcom/sec/android/glview/TwGLImage;->startAnimation()V

    .line 513
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getSubViewManager()Lcom/sec/android/app/camera/subview/SubViewManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 514
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpFocus:Lcom/sec/android/glview/TwGLImage;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 515
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getSubViewManager()Lcom/sec/android/app/camera/subview/SubViewManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewManager;->getTutorialRecordingMode()Lcom/sec/android/app/camera/subview/SubViewTutorialRecordingMode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewTutorialRecordingMode;->setStepRecordingStart()V

    .line 516
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->refreshHelpPopupPicker()V

    .line 517
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mTutorialStep:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->setHelpText(I)V

    .line 521
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getMenuDimController()Lcom/sec/android/app/camera/MenuDimController;

    move-result-object v0

    const/16 v1, 0xca

    iget v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mTutorialStep:I

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/camera/MenuDimController;->refreshButtonDim(II)V

    .line 540
    return-void

    .line 519
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpFocus:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v0}, Lcom/sec/android/glview/TwGLImage;->startAnimation()V

    goto :goto_0
.end method

.method public setStepRecordingStop()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/high16 v5, 0x40000000    # 2.0f

    .line 576
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mTutorialStep:I

    .line 577
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpViewGroup:Lcom/sec/android/glview/TwGLViewGroup;

    invoke-virtual {v0, v1, v1}, Lcom/sec/android/glview/TwGLViewGroup;->translateAbsolute(FF)V

    .line 585
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->isEasyMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 586
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpFocus:Lcom/sec/android/glview/TwGLImage;

    sget v1, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->RECORDING_BUTTON_GROUP_X:F

    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->RIGHT_SIDE_BUTTON_WIDTH:F

    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpFocus:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v3}, Lcom/sec/android/glview/TwGLImage;->getWidth()F

    move-result v3

    sub-float/2addr v2, v3

    div-float/2addr v2, v5

    add-float/2addr v1, v2

    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->EASY_RECORDING_STOP_BUTTON_Y:F

    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->RIGHT_SIDE_BUTTON_HEIGHT:F

    iget-object v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpFocus:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v4}, Lcom/sec/android/glview/TwGLImage;->getHeight()F

    move-result v4

    sub-float/2addr v3, v4

    div-float/2addr v3, v5

    add-float/2addr v2, v3

    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->EASY_HELP_FOCUS_PADDING_Y:F

    add-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/glview/TwGLImage;->translateAbsolute(FF)V

    .line 588
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpFocus:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v1}, Lcom/sec/android/glview/TwGLImage;->getTranslateX()F

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v2}, Lcom/sec/android/glview/TwGLImage;->getWidth()F

    move-result v2

    sub-float/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpFocus:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v2}, Lcom/sec/android/glview/TwGLImage;->getTranslateY()F

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpFocus:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v3}, Lcom/sec/android/glview/TwGLImage;->getHeight()F

    move-result v3

    iget-object v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v4}, Lcom/sec/android/glview/TwGLImage;->getHeight()F

    move-result v4

    sub-float/2addr v3, v4

    div-float/2addr v3, v5

    add-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/glview/TwGLImage;->translateAbsolute(FF)V

    .line 597
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpViewGroup:Lcom/sec/android/glview/TwGLViewGroup;

    sget v1, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->SCREEN_WIDTH:I

    int-to-float v1, v1

    div-float/2addr v1, v5

    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->SCREEN_HEIGHT:I

    int-to-float v2, v2

    div-float/2addr v2, v5

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/glview/TwGLViewGroup;->translateAbsolute(FF)V

    .line 598
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mTutorialStep:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->setHelpText(I)V

    .line 599
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpViewGroup:Lcom/sec/android/glview/TwGLViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLViewGroup;->setVisibility(I)V

    .line 600
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    invoke-virtual {v0}, Lcom/sec/android/glview/TwGLViewGroup;->startAnimation()V

    .line 601
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v0}, Lcom/sec/android/glview/TwGLImage;->startAnimation()V

    .line 602
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getSubViewManager()Lcom/sec/android/app/camera/subview/SubViewManager;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 603
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpFocus:Lcom/sec/android/glview/TwGLImage;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 604
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getSubViewManager()Lcom/sec/android/app/camera/subview/SubViewManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewManager;->getTutorialRecordingMode()Lcom/sec/android/app/camera/subview/SubViewTutorialRecordingMode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewTutorialRecordingMode;->setStepRecordingStop()V

    .line 605
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->refreshHelpPopupPicker()V

    .line 606
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mTutorialStep:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->setHelpText(I)V

    .line 613
    :goto_1
    return-void

    .line 591
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpFocus:Lcom/sec/android/glview/TwGLImage;

    sget v1, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->RECORDING_BUTTON_GROUP_X:F

    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->RIGHT_SIDE_BUTTON_WIDTH:F

    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpFocus:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v3}, Lcom/sec/android/glview/TwGLImage;->getWidth()F

    move-result v3

    sub-float/2addr v2, v3

    div-float/2addr v2, v5

    add-float/2addr v1, v2

    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->RECORDING_STOP_BUTTON_Y:F

    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->RIGHT_SIDE_BUTTON_HEIGHT:F

    iget-object v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpFocus:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v4}, Lcom/sec/android/glview/TwGLImage;->getHeight()F

    move-result v4

    sub-float/2addr v3, v4

    div-float/2addr v3, v5

    add-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/glview/TwGLImage;->translateAbsolute(FF)V

    .line 593
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpFocus:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v1}, Lcom/sec/android/glview/TwGLImage;->getTranslateX()F

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v2}, Lcom/sec/android/glview/TwGLImage;->getWidth()F

    move-result v2

    sub-float/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpFocus:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v2}, Lcom/sec/android/glview/TwGLImage;->getTranslateY()F

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpFocus:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v3}, Lcom/sec/android/glview/TwGLImage;->getHeight()F

    move-result v3

    iget-object v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v4}, Lcom/sec/android/glview/TwGLImage;->getHeight()F

    move-result v4

    sub-float/2addr v3, v4

    div-float/2addr v3, v5

    add-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/glview/TwGLImage;->translateAbsolute(FF)V

    goto/16 :goto_0

    .line 608
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpFocus:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v0}, Lcom/sec/android/glview/TwGLImage;->startAnimation()V

    goto :goto_1
.end method

.method public setStepSnapShot()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/high16 v5, 0x40000000    # 2.0f

    .line 543
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mTutorialStep:I

    .line 545
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpViewGroup:Lcom/sec/android/glview/TwGLViewGroup;

    invoke-virtual {v0, v1, v1}, Lcom/sec/android/glview/TwGLViewGroup;->translateAbsolute(FF)V

    .line 546
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->isEasyMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 547
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->setStepRecordingStop()V

    .line 573
    :goto_0
    return-void

    .line 550
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpFocus:Lcom/sec/android/glview/TwGLImage;

    sget v1, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->RECORDING_BUTTON_GROUP_X:F

    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->RIGHT_SIDE_BUTTON_WIDTH:F

    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpFocus:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v3}, Lcom/sec/android/glview/TwGLImage;->getWidth()F

    move-result v3

    sub-float/2addr v2, v3

    div-float/2addr v2, v5

    add-float/2addr v1, v2

    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->RECORDING_SNAPSHOT_BUTTON_Y:F

    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->RIGHT_SIDE_BUTTON_HEIGHT:F

    iget-object v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpFocus:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v4}, Lcom/sec/android/glview/TwGLImage;->getHeight()F

    move-result v4

    sub-float/2addr v3, v4

    div-float/2addr v3, v5

    add-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/glview/TwGLImage;->translateAbsolute(FF)V

    .line 552
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpFocus:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v1}, Lcom/sec/android/glview/TwGLImage;->getTranslateX()F

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v2}, Lcom/sec/android/glview/TwGLImage;->getWidth()F

    move-result v2

    sub-float/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpFocus:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v2}, Lcom/sec/android/glview/TwGLImage;->getTranslateY()F

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpFocus:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v3}, Lcom/sec/android/glview/TwGLImage;->getHeight()F

    move-result v3

    iget-object v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v4}, Lcom/sec/android/glview/TwGLImage;->getHeight()F

    move-result v4

    sub-float/2addr v3, v4

    div-float/2addr v3, v5

    add-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/glview/TwGLImage;->translateAbsolute(FF)V

    .line 554
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpViewGroup:Lcom/sec/android/glview/TwGLViewGroup;

    sget v1, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->SCREEN_WIDTH:I

    int-to-float v1, v1

    div-float/2addr v1, v5

    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->SCREEN_HEIGHT:I

    int-to-float v2, v2

    div-float/2addr v2, v5

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/glview/TwGLViewGroup;->translateAbsolute(FF)V

    .line 555
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mTutorialStep:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->setHelpText(I)V

    .line 556
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpViewGroup:Lcom/sec/android/glview/TwGLViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLViewGroup;->setVisibility(I)V

    .line 557
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopup:Lcom/sec/android/glview/TwGLViewGroup;

    invoke-virtual {v0}, Lcom/sec/android/glview/TwGLViewGroup;->startAnimation()V

    .line 558
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpPopupPicker:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v0}, Lcom/sec/android/glview/TwGLImage;->startAnimation()V

    .line 559
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getSubViewManager()Lcom/sec/android/app/camera/subview/SubViewManager;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 560
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpFocus:Lcom/sec/android/glview/TwGLImage;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 561
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getSubViewManager()Lcom/sec/android/app/camera/subview/SubViewManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewManager;->getTutorialRecordingMode()Lcom/sec/android/app/camera/subview/SubViewTutorialRecordingMode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/subview/SubViewTutorialRecordingMode;->setStepSnapShot()V

    .line 562
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->refreshHelpPopupPicker()V

    .line 563
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mTutorialStep:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->setHelpText(I)V

    goto/16 :goto_0

    .line 565
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLTutorialRecordingMode;->mHelpFocus:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v0}, Lcom/sec/android/glview/TwGLImage;->startAnimation()V

    goto/16 :goto_0
.end method
