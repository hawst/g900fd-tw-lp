.class public Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;
.super Lcom/sec/android/app/camera/MenuBase;
.source "TwGLSelfieAlarmMenu.java"

# interfaces
.implements Lcom/sec/android/app/camera/CameraSettings$OnCameraSettingsChangedObserver;
.implements Lcom/sec/android/glview/TwGLView$OnOrientationChangedListener;
.implements Lcom/sec/android/glview/TwGLView$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu$ScannerClient;,
        Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu$OnBurstCaptureCancelListener;,
        Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu$MyHandler;
    }
.end annotation


# static fields
.field private static BESTFACE_MAX_IMAGE_COUNT:I = 0x0

.field private static BEST_MAX_IMAGE_COUNT:I = 0x0

.field private static BURST_MAX_IMAGE_COUNT:I = 0x0

.field private static final CAPTURE_PROGRESSBAR_BOTTOM_PADDING_LANDSCAPE:I

.field private static final CAPTURE_PROGRESSBAR_BOTTOM_PADDING_PORTRAIT:I

.field private static final CAPTURE_PROGRESSBAR_STEP_GAP:I

.field private static final CAPTURE_PROGRESSBAR_STEP_WIDTH:I

.field private static final CAPTURE_PROGRESSBAR_TEXT_BOTTOM_PADDING:I

.field private static final CAPTURE_PROGRESSBAR_TEXT_HEIGHT:I

.field private static final CAPTURE_PROGRESSBAR_TEXT_SIZE:F

.field private static final DEFAULT_BURST_MAX_IMAGE_COUNT:I = 0x14

.field private static final DEFAULT_SELFIE_GUIDE_MOVING_THRESHOLD:F = 5.0f

.field private static final FONT_SIZE:I

.field private static GIFMAKER_MAX_IMAGE_COUNT:I = 0x0

.field private static final HELP_TEXT_BOTTOM_MARGIN:F

.field private static final HELP_TEXT_HEIGHT:F

.field private static final HELP_TEXT_HEIGHT_VERTICAL:F

.field private static final HELP_TEXT_POS_X:F

.field private static final HELP_TEXT_POS_Y:F

.field private static final HELP_TEXT_SIDE_MARGIN:F

.field private static final HELP_TEXT_SIZE:F

.field private static final HELP_TEXT_WIDTH:F

.field private static MAGIC_MAX_IMAGE_COUNT:I = 0x0

.field private static MAX_FACE_COUNT:I = 0x0

.field protected static final MESSAGE_TIMEOUT_CANCEL:I = 0x1

.field private static final MOVING:I = 0x2

.field private static final NONE:I = 0x0

.field protected static final POST_PROGRESS_DURATION:I = 0x64

.field protected static final POST_PROGRESS_UPDATE:I = 0x2

.field public static final PROCESS_TIMER_TIMEOUT:I = 0x4e20

.field private static final RESIZING:I = 0x1

.field private static final SCREEN_HEIGHT:I

.field private static final SCREEN_WIDTH:I

.field private static final SELFIE_FRAME_MARGIN:I

.field private static final SELFIE_GUIDE_HEIGHT:F

.field private static final SELFIE_GUIDE_HEIGHT_MAX:I = 0x3

.field private static final SELFIE_GUIDE_HEIGHT_MIN:I = 0x1

.field private static final SELFIE_GUIDE_POS_LANDSCAPE_X:F

.field private static final SELFIE_GUIDE_POS_PORTRAIT_X:F

.field private static final SELFIE_GUIDE_POS_Y:F

.field private static final SELFIE_GUIDE_WIDTH:F

.field private static final SELFIE_GUIDE_WIDTH_MAX:I = 0x2

.field private static final SELFIE_GUIDE_WIDTH_MIN:I = 0x0

.field private static SMARTSELFIE_MAX_IMAGE_COUNT:I = 0x0

.field protected static final TAG:Ljava/lang/String; = "TwGLSelfieAlarmMenu"

.field private static final TOUCH_AF_BOTTOM_BOUNDARY:I

.field private static final TOUCH_AF_BOUNDARY:I

.field private static final TOUCH_VERTEX_AREA_BOUNDARY:I

.field private static final TOUCH_VERTEX_LB:I = 0x2

.field private static final TOUCH_VERTEX_LT:I = 0x1

.field private static final TOUCH_VERTEX_NON:I = 0x0

.field private static final TOUCH_VERTEX_PRESS_BOUNDARY:I

.field private static final TOUCH_VERTEX_RB:I = 0x4

.field private static final TOUCH_VERTEX_RT:I = 0x3

.field private static mScannerClient:Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu$ScannerClient;


# instance fields
.field private final RES_ID_SELFIE_GUIDE_FRAME_IMAGE:[I

.field private mAmountDeltaX:F

.field private mAmountDeltaY:F

.field private final mBurstMsgHandler:Landroid/os/Handler;

.field private mCaptureCount:I

.field private mCapturedFileList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mFaceDetectionRect:Landroid/graphics/Rect;

.field private mIsBurstCapturing:Z

.field private mListener:Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu$OnBurstCaptureCancelListener;

.field private mMaxStep:I

.field private mMovePoint:Landroid/graphics/PointF;

.field private mNeedToResizeGuide:Z

.field private mOrientation:I

.field private mPreviewRect:Landroid/graphics/Rect;

.field private mProgressBarHeight:I

.field private mProgressBarWidth:I

.field private mScreenHeight:I

.field private mScreenWidth:I

.field private mSelectedSelfieGuide:I

.field private mSelfieAlarmMode:Z

.field private mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

.field private mSelfieGuideMode:I

.field private mStoreCount:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const v5, 0x7f0a0047

    const v4, 0x7f0a0007

    const v3, 0x7f0a0006

    const/high16 v2, 0x40000000    # 2.0f

    .line 55
    const/4 v0, 0x5

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->BESTFACE_MAX_IMAGE_COUNT:I

    .line 56
    const/16 v0, 0x8

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->BEST_MAX_IMAGE_COUNT:I

    .line 57
    const/4 v0, 0x3

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->SMARTSELFIE_MAX_IMAGE_COUNT:I

    .line 58
    const-string v0, "30"

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->BURST_MAX_IMAGE_COUNT:I

    .line 59
    const/16 v0, 0x20

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->MAGIC_MAX_IMAGE_COUNT:I

    .line 60
    const/16 v0, 0xa

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->MAX_FACE_COUNT:I

    .line 61
    const/16 v0, 0x14

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->GIFMAKER_MAX_IMAGE_COUNT:I

    .line 64
    const v0, 0x7f0a02b0

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->CAPTURE_PROGRESSBAR_BOTTOM_PADDING_LANDSCAPE:I

    .line 65
    const v0, 0x7f0a02b1

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->CAPTURE_PROGRESSBAR_BOTTOM_PADDING_PORTRAIT:I

    .line 66
    const v0, 0x7f0a02b2

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->CAPTURE_PROGRESSBAR_STEP_WIDTH:I

    .line 67
    const v0, 0x7f0a02b3

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->CAPTURE_PROGRESSBAR_STEP_GAP:I

    .line 68
    const v0, 0x7f0a02b4

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->CAPTURE_PROGRESSBAR_TEXT_HEIGHT:I

    .line 69
    const v0, 0x7f0a02b5

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->CAPTURE_PROGRESSBAR_TEXT_BOTTOM_PADDING:I

    .line 70
    const v0, 0x7f0b0050

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getInteger(I)I

    move-result v0

    int-to-float v0, v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->CAPTURE_PROGRESSBAR_TEXT_SIZE:F

    .line 72
    const v0, 0x7f0a007c

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->HELP_TEXT_WIDTH:F

    .line 73
    const v0, 0x7f0a007e

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->HELP_TEXT_HEIGHT:F

    .line 74
    const v0, 0x7f0a007f

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->HELP_TEXT_HEIGHT_VERTICAL:F

    .line 75
    const v0, 0x7f0a0080

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->HELP_TEXT_SIDE_MARGIN:F

    .line 76
    const v0, 0x7f0a0084

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->HELP_TEXT_BOTTOM_MARGIN:F

    .line 77
    invoke-static {v3}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    sget v1, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->HELP_TEXT_WIDTH:F

    sub-float/2addr v0, v1

    div-float/2addr v0, v2

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->HELP_TEXT_POS_X:F

    .line 78
    invoke-static {v4}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    sget v1, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->HELP_TEXT_HEIGHT:F

    sub-float/2addr v0, v1

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->HELP_TEXT_POS_Y:F

    .line 79
    const v0, 0x7f0b0051

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getInteger(I)I

    move-result v0

    int-to-float v0, v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->HELP_TEXT_SIZE:F

    .line 81
    invoke-static {v3}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->SCREEN_WIDTH:I

    .line 82
    invoke-static {v4}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->SCREEN_HEIGHT:I

    .line 114
    const v0, 0x7f0a0045

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->TOUCH_VERTEX_AREA_BOUNDARY:I

    .line 115
    const v0, 0x7f0a0046

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->TOUCH_VERTEX_PRESS_BOUNDARY:I

    .line 117
    const v0, 0x7f0a0048

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->TOUCH_AF_BOUNDARY:I

    .line 118
    const v0, 0x7f0a0049

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    invoke-static {v5}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sub-int/2addr v0, v1

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->TOUCH_AF_BOTTOM_BOUNDARY:I

    .line 119
    invoke-static {v5}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->SELFIE_FRAME_MARGIN:I

    .line 137
    const v0, 0x7f0a0043

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->SELFIE_GUIDE_WIDTH:F

    .line 138
    const v0, 0x7f0a0044

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->SELFIE_GUIDE_HEIGHT:F

    .line 139
    sget v0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->SCREEN_WIDTH:I

    int-to-float v0, v0

    sget v1, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->SELFIE_GUIDE_WIDTH:F

    sub-float/2addr v0, v1

    div-float/2addr v0, v2

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->SELFIE_GUIDE_POS_LANDSCAPE_X:F

    .line 140
    const v0, 0x7f0a0041

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->SELFIE_GUIDE_POS_PORTRAIT_X:F

    .line 141
    sget v0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->SCREEN_HEIGHT:I

    int-to-float v0, v0

    sget v1, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->SELFIE_GUIDE_HEIGHT:F

    sub-float/2addr v0, v1

    div-float/2addr v0, v2

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->SELFIE_GUIDE_POS_Y:F

    .line 144
    const v0, 0x7f0b0030

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getInteger(I)I

    move-result v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->FONT_SIZE:I

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/camera/Camera;ILcom/sec/android/glview/TwGLViewGroup;Lcom/sec/android/app/camera/MenuResourceDepot;I)V
    .locals 8
    .param p1, "activityContext"    # Lcom/sec/android/app/camera/Camera;
    .param p2, "viewId"    # I
    .param p3, "glParentView"    # Lcom/sec/android/glview/TwGLViewGroup;
    .param p4, "menuResourceDepot"    # Lcom/sec/android/app/camera/MenuResourceDepot;
    .param p5, "modeId"    # I

    .prologue
    .line 198
    const/4 v5, 0x6

    const/4 v6, 0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/camera/MenuBase;-><init>(Lcom/sec/android/app/camera/Camera;ILcom/sec/android/glview/TwGLViewGroup;Lcom/sec/android/app/camera/MenuResourceDepot;IZ)V

    .line 86
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mCaptureCount:I

    .line 87
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mStoreCount:I

    .line 88
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mOrientation:I

    .line 94
    new-instance v0, Landroid/graphics/Rect;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mFaceDetectionRect:Landroid/graphics/Rect;

    .line 97
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mMovePoint:Landroid/graphics/PointF;

    .line 99
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mPreviewRect:Landroid/graphics/Rect;

    .line 103
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideMode:I

    .line 104
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mAmountDeltaX:F

    .line 105
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mAmountDeltaY:F

    .line 112
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelectedSelfieGuide:I

    .line 125
    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->RES_ID_SELFIE_GUIDE_FRAME_IMAGE:[I

    .line 133
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mCapturedFileList:Ljava/util/ArrayList;

    .line 153
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mMaxStep:I

    .line 154
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mProgressBarWidth:I

    .line 155
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mProgressBarHeight:I

    .line 156
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mScreenWidth:I

    .line 157
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mScreenHeight:I

    .line 159
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieAlarmMode:Z

    .line 160
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mNeedToResizeGuide:Z

    .line 200
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->setCaptureEnabled(Z)V

    .line 202
    const/16 v0, 0x19

    if-eq p5, v0, :cond_0

    const/16 v0, 0x22

    if-ne p5, v0, :cond_1

    .line 203
    :cond_0
    sget v0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->BESTFACE_MAX_IMAGE_COUNT:I

    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mMaxStep:I

    .line 215
    :goto_0
    invoke-virtual {p1}, Lcom/sec/android/app/camera/Camera;->getGLContext()Lcom/sec/android/glview/TwGLContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/glview/TwGLContext;->getScreenWidth()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mScreenWidth:I

    .line 216
    invoke-virtual {p1}, Lcom/sec/android/app/camera/Camera;->getGLContext()Lcom/sec/android/glview/TwGLContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/glview/TwGLContext;->getScreenHeight()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mScreenHeight:I

    .line 217
    sget v0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->CAPTURE_PROGRESSBAR_STEP_WIDTH:I

    sget v1, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->CAPTURE_PROGRESSBAR_STEP_GAP:I

    add-int/2addr v0, v1

    iget v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mMaxStep:I

    mul-int/2addr v0, v1

    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mProgressBarWidth:I

    .line 218
    sget v0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->CAPTURE_PROGRESSBAR_TEXT_HEIGHT:I

    sget v1, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->CAPTURE_PROGRESSBAR_TEXT_BOTTOM_PADDING:I

    add-int/2addr v0, v1

    sget v1, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->CAPTURE_PROGRESSBAR_STEP_WIDTH:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mProgressBarHeight:I

    .line 220
    new-instance v0, Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {p1}, Lcom/sec/android/app/camera/Camera;->getGLContext()Lcom/sec/android/glview/TwGLContext;

    move-result-object v1

    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->SELFIE_GUIDE_POS_LANDSCAPE_X:F

    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->SELFIE_GUIDE_POS_Y:F

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->SELFIE_GUIDE_WIDTH:F

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->SELFIE_GUIDE_HEIGHT:F

    const v6, 0x7f020061

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/glview/TwGLNinePatch;-><init>(Lcom/sec/android/glview/TwGLContext;FFFFI)V

    iput-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    .line 223
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v0, p0}, Lcom/sec/android/glview/TwGLNinePatch;->setOnTouchListener(Lcom/sec/android/glview/TwGLView$OnTouchListener;)V

    .line 224
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v0, p0}, Lcom/sec/android/glview/TwGLNinePatch;->setOnOrientationChangedListener(Lcom/sec/android/glview/TwGLView$OnOrientationChangedListener;)V

    .line 225
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLNinePatch;->setClickable(Z)V

    .line 226
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/Camera;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c00fa

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLNinePatch;->setTitle(Ljava/lang/String;)V

    .line 227
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLNinePatch;->setVisibility(I)V

    .line 229
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {p3, v0}, Lcom/sec/android/glview/TwGLViewGroup;->addView(Lcom/sec/android/glview/TwGLView;)V

    .line 231
    invoke-direct {p0}, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->init()V

    .line 233
    new-instance v0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu$ScannerClient;

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-direct {v0, v1}, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu$ScannerClient;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mScannerClient:Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu$ScannerClient;

    .line 234
    new-instance v7, Landroid/os/HandlerThread;

    const-string v0, "TwGLBurstMenu"

    invoke-direct {v7, v0}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 235
    .local v7, "ht":Landroid/os/HandlerThread;
    invoke-virtual {v7}, Landroid/os/HandlerThread;->start()V

    .line 236
    new-instance v0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu$MyHandler;

    invoke-virtual {v7}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu$MyHandler;-><init>(Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mBurstMsgHandler:Landroid/os/Handler;

    .line 237
    return-void

    .line 204
    .end local v7    # "ht":Landroid/os/HandlerThread;
    :cond_1
    const/16 v0, 0x18

    if-ne p5, v0, :cond_2

    .line 205
    sget v0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->BEST_MAX_IMAGE_COUNT:I

    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mMaxStep:I

    goto/16 :goto_0

    .line 206
    :cond_2
    const/16 v0, 0x2e

    if-ne p5, v0, :cond_3

    .line 207
    sget v0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->MAGIC_MAX_IMAGE_COUNT:I

    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mMaxStep:I

    goto/16 :goto_0

    .line 208
    :cond_3
    const/16 v0, 0x38

    if-ne p5, v0, :cond_4

    .line 209
    sget v0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->SMARTSELFIE_MAX_IMAGE_COUNT:I

    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mMaxStep:I

    goto/16 :goto_0

    .line 210
    :cond_4
    const/16 v0, 0x3a

    if-ne p5, v0, :cond_5

    .line 211
    sget v0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->GIFMAKER_MAX_IMAGE_COUNT:I

    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mMaxStep:I

    goto/16 :goto_0

    .line 213
    :cond_5
    sget v0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->BURST_MAX_IMAGE_COUNT:I

    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mMaxStep:I

    goto/16 :goto_0

    .line 125
    nop

    :array_0
    .array-data 4
        0x7f020061
        0x7f020064
        0x7f020063
        0x7f020066
        0x7f020065
    .end array-data
.end method

.method static synthetic access$000(Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;)Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu$OnBurstCaptureCancelListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mListener:Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu$OnBurstCaptureCancelListener;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;

    .prologue
    .line 52
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mStoreCount:I

    return v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;
    .param p1, "x1"    # I

    .prologue
    .line 52
    iput p1, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mStoreCount:I

    return p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;

    .prologue
    .line 52
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mCaptureCount:I

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mBurstMsgHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private checkShotROIState([II)I
    .locals 6
    .param p1, "roistate"    # [I
    .param p2, "facecount"    # I

    .prologue
    const/4 v1, 0x4

    const/4 v3, 0x3

    const/4 v2, 0x2

    const/4 v4, 0x1

    .line 932
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p2, :cond_1

    .line 933
    aget v5, p1, v0

    if-ne v5, v1, :cond_0

    .line 948
    :goto_1
    return v1

    .line 932
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 936
    :cond_1
    const/4 v0, 0x0

    :goto_2
    if-ge v0, p2, :cond_3

    .line 937
    aget v1, p1, v0

    if-ne v1, v2, :cond_2

    move v1, v2

    .line 938
    goto :goto_1

    .line 936
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 940
    :cond_3
    const/4 v0, 0x0

    :goto_3
    if-ge v0, p2, :cond_5

    .line 941
    aget v1, p1, v0

    if-ne v1, v3, :cond_4

    move v1, v3

    .line 942
    goto :goto_1

    .line 940
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 944
    :cond_5
    const/4 v0, 0x0

    :goto_4
    if-ge v0, p2, :cond_7

    .line 945
    aget v1, p1, v0

    if-ne v1, v4, :cond_6

    move v1, v4

    .line 946
    goto :goto_1

    .line 944
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 948
    :cond_7
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private getSelfieGuideBoxResizeRange(Landroid/graphics/Rect;I)F
    .locals 3
    .param p1, "previewRect"    # Landroid/graphics/Rect;
    .param p2, "type"    # I

    .prologue
    const v2, 0x3f666666    # 0.9f

    const v1, 0x3ecccccd    # 0.4f

    .line 673
    if-eqz p1, :cond_0

    .line 674
    packed-switch p2, :pswitch_data_0

    .line 686
    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 676
    :pswitch_0
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, v1

    goto :goto_0

    .line 678
    :pswitch_1
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, v1

    goto :goto_0

    .line 680
    :pswitch_2
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, v2

    goto :goto_0

    .line 682
    :pswitch_3
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, v2

    goto :goto_0

    .line 674
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private init()V
    .locals 2

    .prologue
    .line 240
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->getShootingMode()I

    move-result v0

    const/16 v1, 0x38

    if-ne v0, v1, :cond_0

    .line 241
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/camera/CameraSettings;->registerCameraSettingsChangedObserver(Lcom/sec/android/app/camera/CameraSettings$OnCameraSettingsChangedObserver;)V

    .line 242
    :cond_0
    return-void
.end method

.method private resetFaceDetectionRect()V
    .locals 3

    .prologue
    .line 922
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mFaceDetectionRect:Landroid/graphics/Rect;

    if-eqz v0, :cond_0

    .line 923
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mFaceDetectionRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 926
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    if-eqz v0, :cond_1

    .line 927
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/camera/Camera;->onSelfieAlarmGuideStateChanged(IZ)V

    .line 929
    :cond_1
    return-void
.end method

.method private declared-synchronized resizeSelfieGuide(IFFFF)V
    .locals 8
    .param p1, "vertexTouchMode"    # I
    .param p2, "deltaX"    # F
    .param p3, "deltaY"    # F
    .param p4, "amountSizeX"    # F
    .param p5, "amountSizeY"    # F

    .prologue
    const/4 v6, 0x0

    .line 720
    monitor-enter p0

    :try_start_0
    iget-object v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mPreviewRect:Landroid/graphics/Rect;

    const/4 v5, 0x0

    invoke-direct {p0, v4, v5}, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->getSelfieGuideBoxResizeRange(Landroid/graphics/Rect;I)F

    move-result v3

    .line 721
    .local v3, "MIN_ROIWIDTH":F
    iget-object v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mPreviewRect:Landroid/graphics/Rect;

    const/4 v5, 0x1

    invoke-direct {p0, v4, v5}, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->getSelfieGuideBoxResizeRange(Landroid/graphics/Rect;I)F

    move-result v2

    .line 722
    .local v2, "MIN_ROIHEIGHT":F
    iget-object v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mPreviewRect:Landroid/graphics/Rect;

    const/4 v5, 0x2

    invoke-direct {p0, v4, v5}, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->getSelfieGuideBoxResizeRange(Landroid/graphics/Rect;I)F

    move-result v1

    .line 723
    .local v1, "MAX_ROIWIDTH":F
    iget-object v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mPreviewRect:Landroid/graphics/Rect;

    const/4 v5, 0x3

    invoke-direct {p0, v4, v5}, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->getSelfieGuideBoxResizeRange(Landroid/graphics/Rect;I)F

    move-result v0

    .line 725
    .local v0, "MAX_ROIHEIGHT":F
    packed-switch p1, :pswitch_data_0

    .line 823
    :goto_0
    iget-object v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->SELFIE_GUIDE_WIDTH:F

    iget v6, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mAmountDeltaX:F

    add-float/2addr v5, v6

    sget v6, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->SELFIE_GUIDE_HEIGHT:F

    iget v7, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mAmountDeltaY:F

    add-float/2addr v6, v7

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/glview/TwGLNinePatch;->setSize(FF)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 824
    monitor-exit p0

    return-void

    .line 727
    :pswitch_0
    :try_start_1
    iget-object v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v4}, Lcom/sec/android/glview/TwGLNinePatch;->getWidth()F

    move-result v4

    sub-float/2addr v4, p2

    cmpg-float v4, v4, v3

    if-gtz v4, :cond_2

    .line 728
    cmpl-float v4, p2, v6

    if-ltz v4, :cond_0

    .line 729
    iget-object v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v4}, Lcom/sec/android/glview/TwGLNinePatch;->getWidth()F

    move-result v4

    sub-float p2, v4, v3

    .line 737
    :cond_0
    :goto_1
    iget-object v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v4}, Lcom/sec/android/glview/TwGLNinePatch;->getHeight()F

    move-result v4

    sub-float/2addr v4, p3

    cmpg-float v4, v4, v2

    if-gtz v4, :cond_3

    .line 738
    cmpl-float v4, p3, v6

    if-ltz v4, :cond_1

    .line 739
    iget-object v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v4}, Lcom/sec/android/glview/TwGLNinePatch;->getHeight()F

    move-result v4

    sub-float p3, v4, v2

    .line 747
    :cond_1
    :goto_2
    iget v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mAmountDeltaX:F

    sub-float/2addr v4, p2

    iput v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mAmountDeltaX:F

    .line 748
    iget v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mAmountDeltaY:F

    sub-float/2addr v4, p3

    iput v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mAmountDeltaY:F

    .line 749
    iget-object v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    const/4 v5, 0x0

    invoke-virtual {v4, p2, p3, v5}, Lcom/sec/android/glview/TwGLNinePatch;->translate(FFZ)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 720
    .end local v0    # "MAX_ROIHEIGHT":F
    .end local v1    # "MAX_ROIWIDTH":F
    .end local v2    # "MIN_ROIHEIGHT":F
    .end local v3    # "MIN_ROIWIDTH":F
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4

    .line 731
    .restart local v0    # "MAX_ROIHEIGHT":F
    .restart local v1    # "MAX_ROIWIDTH":F
    .restart local v2    # "MIN_ROIHEIGHT":F
    .restart local v3    # "MIN_ROIWIDTH":F
    :cond_2
    :try_start_2
    iget-object v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v4}, Lcom/sec/android/glview/TwGLNinePatch;->getWidth()F

    move-result v4

    sub-float/2addr v4, p2

    cmpl-float v4, v4, v1

    if-ltz v4, :cond_0

    .line 732
    cmpg-float v4, p2, v6

    if-gtz v4, :cond_0

    .line 733
    iget-object v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v4}, Lcom/sec/android/glview/TwGLNinePatch;->getWidth()F

    move-result v4

    sub-float p2, v4, v1

    goto :goto_1

    .line 741
    :cond_3
    iget-object v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v4}, Lcom/sec/android/glview/TwGLNinePatch;->getHeight()F

    move-result v4

    sub-float/2addr v4, p3

    cmpl-float v4, v4, v0

    if-ltz v4, :cond_1

    .line 742
    cmpg-float v4, p3, v6

    if-gtz v4, :cond_1

    .line 743
    iget-object v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v4}, Lcom/sec/android/glview/TwGLNinePatch;->getHeight()F

    move-result v4

    sub-float p3, v4, v0

    goto :goto_2

    .line 752
    :pswitch_1
    iget-object v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v4}, Lcom/sec/android/glview/TwGLNinePatch;->getWidth()F

    move-result v4

    sub-float/2addr v4, p2

    cmpg-float v4, v4, v3

    if-gtz v4, :cond_6

    .line 753
    cmpl-float v4, p2, v6

    if-ltz v4, :cond_4

    .line 754
    iget-object v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v4}, Lcom/sec/android/glview/TwGLNinePatch;->getWidth()F

    move-result v4

    float-to-int v4, v4

    int-to-float v4, v4

    sub-float p2, v4, v3

    .line 761
    :cond_4
    :goto_3
    iget-object v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v4}, Lcom/sec/android/glview/TwGLNinePatch;->getHeight()F

    move-result v4

    add-float/2addr v4, p3

    cmpg-float v4, v4, v2

    if-gtz v4, :cond_7

    .line 762
    cmpg-float v4, p3, v6

    if-gtz v4, :cond_5

    .line 763
    iget-object v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v4}, Lcom/sec/android/glview/TwGLNinePatch;->getHeight()F

    move-result v4

    sub-float p3, v2, v4

    .line 771
    :cond_5
    :goto_4
    iget v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mAmountDeltaX:F

    sub-float/2addr v4, p2

    iput v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mAmountDeltaX:F

    .line 772
    iget v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mAmountDeltaY:F

    add-float/2addr v4, p3

    iput v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mAmountDeltaY:F

    .line 773
    iget-object v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v4, p2, v5, v6}, Lcom/sec/android/glview/TwGLNinePatch;->translate(FFZ)V

    goto/16 :goto_0

    .line 756
    :cond_6
    iget-object v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v4}, Lcom/sec/android/glview/TwGLNinePatch;->getWidth()F

    move-result v4

    sub-float/2addr v4, p2

    cmpl-float v4, v4, v1

    if-ltz v4, :cond_4

    .line 757
    cmpg-float v4, p2, v6

    if-gtz v4, :cond_4

    .line 758
    iget-object v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v4}, Lcom/sec/android/glview/TwGLNinePatch;->getWidth()F

    move-result v4

    sub-float p2, v4, v1

    goto :goto_3

    .line 765
    :cond_7
    iget-object v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v4}, Lcom/sec/android/glview/TwGLNinePatch;->getHeight()F

    move-result v4

    add-float/2addr v4, p3

    cmpl-float v4, v4, v0

    if-ltz v4, :cond_5

    .line 766
    cmpl-float v4, p3, v6

    if-ltz v4, :cond_5

    .line 767
    iget-object v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v4}, Lcom/sec/android/glview/TwGLNinePatch;->getHeight()F

    move-result v4

    sub-float p3, v0, v4

    goto :goto_4

    .line 776
    :pswitch_2
    iget-object v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v4}, Lcom/sec/android/glview/TwGLNinePatch;->getWidth()F

    move-result v4

    add-float/2addr v4, p2

    cmpg-float v4, v4, v3

    if-gtz v4, :cond_a

    .line 777
    cmpg-float v4, p2, v6

    if-gtz v4, :cond_8

    .line 778
    iget-object v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v4}, Lcom/sec/android/glview/TwGLNinePatch;->getWidth()F

    move-result v4

    sub-float p2, v3, v4

    .line 785
    :cond_8
    :goto_5
    iget-object v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v4}, Lcom/sec/android/glview/TwGLNinePatch;->getHeight()F

    move-result v4

    sub-float/2addr v4, p3

    cmpg-float v4, v4, v2

    if-gtz v4, :cond_b

    .line 786
    cmpl-float v4, p3, v6

    if-ltz v4, :cond_9

    .line 787
    iget-object v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v4}, Lcom/sec/android/glview/TwGLNinePatch;->getHeight()F

    move-result v4

    sub-float p3, v4, v2

    .line 794
    :cond_9
    :goto_6
    iget v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mAmountDeltaX:F

    add-float/2addr v4, p2

    iput v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mAmountDeltaX:F

    .line 795
    iget v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mAmountDeltaY:F

    sub-float/2addr v4, p3

    iput v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mAmountDeltaY:F

    .line 796
    iget-object v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v4, v5, p3, v6}, Lcom/sec/android/glview/TwGLNinePatch;->translate(FFZ)V

    goto/16 :goto_0

    .line 780
    :cond_a
    iget-object v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v4}, Lcom/sec/android/glview/TwGLNinePatch;->getWidth()F

    move-result v4

    add-float/2addr v4, p2

    cmpl-float v4, v4, v1

    if-ltz v4, :cond_8

    .line 781
    cmpl-float v4, p2, v6

    if-ltz v4, :cond_8

    .line 782
    iget-object v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v4}, Lcom/sec/android/glview/TwGLNinePatch;->getWidth()F

    move-result v4

    sub-float p2, v1, v4

    goto :goto_5

    .line 789
    :cond_b
    iget-object v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v4}, Lcom/sec/android/glview/TwGLNinePatch;->getHeight()F

    move-result v4

    sub-float/2addr v4, p3

    cmpl-float v4, v4, v0

    if-ltz v4, :cond_9

    .line 790
    cmpg-float v4, p3, v6

    if-gtz v4, :cond_9

    .line 791
    iget-object v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v4}, Lcom/sec/android/glview/TwGLNinePatch;->getHeight()F

    move-result v4

    sub-float p3, v4, v0

    goto :goto_6

    .line 799
    :pswitch_3
    iget-object v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v4}, Lcom/sec/android/glview/TwGLNinePatch;->getWidth()F

    move-result v4

    add-float/2addr v4, p2

    cmpg-float v4, v4, v3

    if-gtz v4, :cond_e

    .line 800
    cmpg-float v4, p2, v6

    if-gtz v4, :cond_c

    .line 801
    iget-object v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v4}, Lcom/sec/android/glview/TwGLNinePatch;->getWidth()F

    move-result v4

    sub-float p2, v3, v4

    .line 808
    :cond_c
    :goto_7
    iget-object v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v4}, Lcom/sec/android/glview/TwGLNinePatch;->getHeight()F

    move-result v4

    add-float/2addr v4, p3

    cmpg-float v4, v4, v2

    if-gtz v4, :cond_f

    .line 809
    cmpg-float v4, p3, v6

    if-gtz v4, :cond_d

    .line 810
    iget-object v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v4}, Lcom/sec/android/glview/TwGLNinePatch;->getHeight()F

    move-result v4

    sub-float p3, v2, v4

    .line 817
    :cond_d
    :goto_8
    iget v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mAmountDeltaX:F

    add-float/2addr v4, p2

    float-to-int v4, v4

    int-to-float v4, v4

    iput v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mAmountDeltaX:F

    .line 818
    iget v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mAmountDeltaY:F

    add-float/2addr v4, p3

    float-to-int v4, v4

    int-to-float v4, v4

    iput v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mAmountDeltaY:F

    goto/16 :goto_0

    .line 803
    :cond_e
    iget-object v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v4}, Lcom/sec/android/glview/TwGLNinePatch;->getWidth()F

    move-result v4

    add-float/2addr v4, p2

    cmpl-float v4, v4, v1

    if-ltz v4, :cond_c

    .line 804
    cmpl-float v4, p2, v6

    if-ltz v4, :cond_c

    .line 805
    iget-object v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v4}, Lcom/sec/android/glview/TwGLNinePatch;->getWidth()F

    move-result v4

    sub-float p2, v1, v4

    goto :goto_7

    .line 812
    :cond_f
    iget-object v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v4}, Lcom/sec/android/glview/TwGLNinePatch;->getHeight()F

    move-result v4

    add-float/2addr v4, p3

    cmpl-float v4, v4, v0

    if-ltz v4, :cond_d

    .line 813
    cmpl-float v4, p3, v6

    if-ltz v4, :cond_d

    .line 814
    iget-object v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v4}, Lcom/sec/android/glview/TwGLNinePatch;->getHeight()F
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v4

    sub-float p3, v0, v4

    goto :goto_8

    .line 725
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private setSelectedSelfieGuide(FF)V
    .locals 10
    .param p1, "touchX"    # F
    .param p2, "touchY"    # F

    .prologue
    const/4 v9, 0x0

    .line 540
    iget-object v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v4}, Lcom/sec/android/glview/TwGLNinePatch;->getCurrentLeft()F

    move-result v1

    .line 541
    .local v1, "curGuideLeft":F
    iget-object v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v4}, Lcom/sec/android/glview/TwGLNinePatch;->getCurrentRight()F

    move-result v2

    .line 542
    .local v2, "curGuideRight":F
    iget-object v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v4}, Lcom/sec/android/glview/TwGLNinePatch;->getCurrentTop()F

    move-result v3

    .line 543
    .local v3, "curGuideTop":F
    iget-object v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v4}, Lcom/sec/android/glview/TwGLNinePatch;->getCurrentBottom()F

    move-result v0

    .line 545
    .local v0, "curGuideBottom":F
    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->TOUCH_VERTEX_AREA_BOUNDARY:I

    int-to-float v4, v4

    sub-float v4, v1, v4

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->TOUCH_VERTEX_PRESS_BOUNDARY:I

    int-to-float v5, v5

    add-float/2addr v4, v5

    cmpg-float v4, v4, p1

    if-gez v4, :cond_1

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->TOUCH_VERTEX_AREA_BOUNDARY:I

    int-to-float v4, v4

    add-float/2addr v4, v1

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->TOUCH_VERTEX_PRESS_BOUNDARY:I

    int-to-float v5, v5

    add-float/2addr v4, v5

    cmpl-float v4, v4, p1

    if-lez v4, :cond_1

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->TOUCH_VERTEX_AREA_BOUNDARY:I

    int-to-float v4, v4

    sub-float v4, v3, v4

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->TOUCH_VERTEX_PRESS_BOUNDARY:I

    int-to-float v5, v5

    add-float/2addr v4, v5

    cmpg-float v4, v4, p2

    if-gez v4, :cond_1

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->TOUCH_VERTEX_AREA_BOUNDARY:I

    int-to-float v4, v4

    add-float/2addr v4, v3

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->TOUCH_VERTEX_PRESS_BOUNDARY:I

    int-to-float v5, v5

    add-float/2addr v4, v5

    cmpl-float v4, v4, p2

    if-lez v4, :cond_1

    .line 549
    const/4 v4, 0x1

    iput v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelectedSelfieGuide:I

    .line 568
    :goto_0
    iget v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelectedSelfieGuide:I

    if-eqz v4, :cond_0

    .line 569
    iget-object v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->RES_ID_SELFIE_GUIDE_FRAME_IMAGE:[I

    iget v6, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelectedSelfieGuide:I

    aget v5, v5, v6

    sget v6, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->SELFIE_GUIDE_WIDTH:F

    iget v7, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mAmountDeltaX:F

    add-float/2addr v6, v7

    sget v7, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->SELFIE_GUIDE_HEIGHT:F

    iget v8, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mAmountDeltaY:F

    add-float/2addr v7, v8

    invoke-virtual {v4, v5, v6, v7, v9}, Lcom/sec/android/glview/TwGLNinePatch;->setImage(IFFZ)V

    .line 572
    :cond_0
    return-void

    .line 550
    :cond_1
    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->TOUCH_VERTEX_AREA_BOUNDARY:I

    int-to-float v4, v4

    sub-float v4, v2, v4

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->TOUCH_VERTEX_PRESS_BOUNDARY:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    cmpg-float v4, v4, p1

    if-gez v4, :cond_2

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->TOUCH_VERTEX_AREA_BOUNDARY:I

    int-to-float v4, v4

    add-float/2addr v4, v2

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->TOUCH_VERTEX_PRESS_BOUNDARY:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    cmpl-float v4, v4, p1

    if-lez v4, :cond_2

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->TOUCH_VERTEX_AREA_BOUNDARY:I

    int-to-float v4, v4

    sub-float v4, v3, v4

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->TOUCH_VERTEX_PRESS_BOUNDARY:I

    int-to-float v5, v5

    add-float/2addr v4, v5

    cmpg-float v4, v4, p2

    if-gez v4, :cond_2

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->TOUCH_VERTEX_AREA_BOUNDARY:I

    int-to-float v4, v4

    add-float/2addr v4, v3

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->TOUCH_VERTEX_PRESS_BOUNDARY:I

    int-to-float v5, v5

    add-float/2addr v4, v5

    cmpl-float v4, v4, p2

    if-lez v4, :cond_2

    .line 554
    const/4 v4, 0x3

    iput v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelectedSelfieGuide:I

    goto :goto_0

    .line 555
    :cond_2
    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->TOUCH_VERTEX_AREA_BOUNDARY:I

    int-to-float v4, v4

    sub-float v4, v1, v4

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->TOUCH_VERTEX_PRESS_BOUNDARY:I

    int-to-float v5, v5

    add-float/2addr v4, v5

    cmpg-float v4, v4, p1

    if-gez v4, :cond_3

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->TOUCH_VERTEX_AREA_BOUNDARY:I

    int-to-float v4, v4

    add-float/2addr v4, v1

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->TOUCH_VERTEX_PRESS_BOUNDARY:I

    int-to-float v5, v5

    add-float/2addr v4, v5

    cmpl-float v4, v4, p1

    if-lez v4, :cond_3

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->TOUCH_VERTEX_AREA_BOUNDARY:I

    int-to-float v4, v4

    sub-float v4, v0, v4

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->TOUCH_VERTEX_PRESS_BOUNDARY:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    cmpg-float v4, v4, p2

    if-gez v4, :cond_3

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->TOUCH_VERTEX_AREA_BOUNDARY:I

    int-to-float v4, v4

    add-float/2addr v4, v0

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->TOUCH_VERTEX_PRESS_BOUNDARY:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    cmpl-float v4, v4, p2

    if-lez v4, :cond_3

    .line 559
    const/4 v4, 0x2

    iput v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelectedSelfieGuide:I

    goto/16 :goto_0

    .line 560
    :cond_3
    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->TOUCH_VERTEX_AREA_BOUNDARY:I

    int-to-float v4, v4

    sub-float v4, v2, v4

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->TOUCH_VERTEX_PRESS_BOUNDARY:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    cmpg-float v4, v4, p1

    if-gez v4, :cond_4

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->TOUCH_VERTEX_AREA_BOUNDARY:I

    int-to-float v4, v4

    add-float/2addr v4, v2

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->TOUCH_VERTEX_PRESS_BOUNDARY:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    cmpl-float v4, v4, p1

    if-lez v4, :cond_4

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->TOUCH_VERTEX_AREA_BOUNDARY:I

    int-to-float v4, v4

    sub-float v4, v0, v4

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->TOUCH_VERTEX_PRESS_BOUNDARY:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    cmpg-float v4, v4, p2

    if-gez v4, :cond_4

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->TOUCH_VERTEX_AREA_BOUNDARY:I

    int-to-float v4, v4

    add-float/2addr v4, v0

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->TOUCH_VERTEX_PRESS_BOUNDARY:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    cmpl-float v4, v4, p2

    if-lez v4, :cond_4

    .line 564
    const/4 v4, 0x4

    iput v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelectedSelfieGuide:I

    goto/16 :goto_0

    .line 566
    :cond_4
    iput v9, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelectedSelfieGuide:I

    goto/16 :goto_0
.end method


# virtual methods
.method public SelfieProgressLayout()V
    .locals 0

    .prologue
    .line 405
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->hideSelfieGuideFrame()V

    .line 406
    return-void
.end method

.method public clear()V
    .locals 0

    .prologue
    .line 251
    invoke-super {p0}, Lcom/sec/android/app/camera/MenuBase;->clear()V

    .line 252
    return-void
.end method

.method public clearBurstProgressBar()V
    .locals 2

    .prologue
    .line 415
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    if-eqz v0, :cond_0

    .line 416
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/camera/CameraSettings;->unregisterCameraSettingsChangedObserver(Lcom/sec/android/app/camera/CameraSettings$OnCameraSettingsChangedObserver;)V

    .line 418
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    if-eqz v0, :cond_1

    .line 419
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v0}, Lcom/sec/android/glview/TwGLNinePatch;->clear()V

    .line 420
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mGLParentView:Lcom/sec/android/glview/TwGLViewGroup;

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLViewGroup;->removeView(Lcom/sec/android/glview/TwGLView;)V

    .line 421
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    .line 423
    :cond_1
    return-void
.end method

.method public deNormalize(FI)F
    .locals 2
    .param p1, "value"    # F
    .param p2, "size"    # I

    .prologue
    .line 838
    const/high16 v0, 0x447a0000    # 1000.0f

    add-float/2addr v0, p1

    const/high16 v1, 0x44fa0000    # 2000.0f

    div-float/2addr v0, v1

    int-to-float v1, p2

    mul-float/2addr v0, v1

    return v0
.end method

.method public getCaptureProgressIncreased()I
    .locals 1

    .prologue
    .line 358
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mCaptureCount:I

    return v0
.end method

.method public getSelfieGuideFrame()Lcom/sec/android/glview/TwGLNinePatch;
    .locals 1

    .prologue
    .line 834
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    return-object v0
.end method

.method public getStoreCount()I
    .locals 1

    .prologue
    .line 469
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mStoreCount:I

    return v0
.end method

.method public hideCaptureLayout()V
    .locals 2

    .prologue
    .line 391
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->getShootingMode()I

    move-result v0

    const/16 v1, 0x38

    if-eq v0, v1, :cond_0

    .line 392
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->hideSelfieGuideFrame()V

    .line 394
    :cond_0
    return-void
.end method

.method public hidePostCaptureLayout()V
    .locals 1

    .prologue
    .line 373
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getProgressingPopup()Lcom/sec/android/app/camera/glwidget/TwGLProgressingPopup;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 374
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getProgressingPopup()Lcom/sec/android/app/camera/glwidget/TwGLProgressingPopup;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/glwidget/TwGLProgressingPopup;->hidePostCaptureLayout()V

    .line 376
    :cond_0
    return-void
.end method

.method public hideSelfieGuideFrame()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 506
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    if-eqz v0, :cond_0

    .line 507
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLNinePatch;->setVisibility(I)V

    .line 510
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    if-eqz v0, :cond_1

    .line 511
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0, v2, v2}, Lcom/sec/android/app/camera/Camera;->onSelfieAlarmGuideStateChanged(IZ)V

    .line 513
    :cond_1
    return-void
.end method

.method public isBurstCapturing()Z
    .locals 1

    .prologue
    .line 461
    iget-boolean v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mIsBurstCapturing:Z

    return v0
.end method

.method public isNeedInternalStorage()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 528
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/camera/CameraSettings;->getShootingMode()I

    move-result v1

    const/16 v2, 0x19

    if-eq v1, v2, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/camera/CameraSettings;->getShootingMode()I

    move-result v1

    const/16 v2, 0x22

    if-eq v1, v2, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/camera/CameraSettings;->getShootingMode()I

    move-result v1

    const/16 v2, 0x38

    if-eq v1, v2, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/camera/CameraSettings;->getShootingMode()I

    move-result v1

    const/16 v2, 0x3a

    if-eq v1, v2, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/camera/CameraSettings;->getShootingMode()I

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/camera/CameraSettings;->getBurstMode()I

    move-result v1

    if-eq v1, v0, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/camera/CameraSettings;->getShootingMode()I

    move-result v1

    const/16 v2, 0x37

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/camera/CameraSettings;->getBurstMode()I

    move-result v1

    if-ne v1, v0, :cond_2

    .line 536
    :cond_1
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPostCaptureLayoutVisible()Z
    .locals 1

    .prologue
    .line 387
    const/4 v0, 0x1

    return v0
.end method

.method public isSelfieAlarmFDDetecting()Z
    .locals 1

    .prologue
    .line 827
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v0}, Lcom/sec/android/glview/TwGLNinePatch;->isVisible()I

    move-result v0

    if-nez v0, :cond_0

    .line 828
    const/4 v0, 0x1

    .line 830
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSelfieAlarmMode()Z
    .locals 1

    .prologue
    .line 465
    iget-boolean v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieAlarmMode:Z

    return v0
.end method

.method public needToArrangeSelfieAlarmGuideRect()V
    .locals 2

    .prologue
    .line 351
    const-string v0, "TwGLSelfieAlarmMenu"

    const-string v1, "needToArrangeSelfieAlarmGuideRect..."

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 352
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getEngine()Lcom/sec/android/app/camera/CommonEngine;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CommonEngine;->getSurfaceView()Lcom/sec/android/app/camera/PreviewFrameLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/PreviewFrameLayout;->getPreviewRect()Landroid/graphics/Rect;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mPreviewRect:Landroid/graphics/Rect;

    .line 353
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->reset()V

    .line 354
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->showCaptureLayout()V

    .line 355
    return-void
.end method

.method public onBack()V
    .locals 1

    .prologue
    .line 255
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mCaptureCount:I

    if-lez v0, :cond_0

    .line 256
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mListener:Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu$OnBurstCaptureCancelListener;

    invoke-interface {v0}, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu$OnBurstCaptureCancelListener;->onBurstCaptureCancelled()V

    .line 257
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->reset()V

    .line 261
    :goto_0
    return-void

    .line 259
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->processBack()V

    goto :goto_0
.end method

.method public onCameraSettingsChanged(II)V
    .locals 10
    .param p1, "menuid"    # I
    .param p2, "modeid"    # I

    .prologue
    const/4 v9, 0x0

    .line 953
    const-string v5, "TwGLSelfieAlarmMenu"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onCameraSettingsChanged menuid="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " modeid="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 954
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v5}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/camera/CameraSettings;->getShootingMode()I

    move-result v5

    const/16 v6, 0x38

    if-ne v5, v6, :cond_0

    .line 955
    packed-switch p1, :pswitch_data_0

    .line 1002
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 958
    :pswitch_1
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v5}, Lcom/sec/android/app/camera/Camera;->getEngine()Lcom/sec/android/app/camera/CommonEngine;

    move-result-object v5

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v5}, Lcom/sec/android/app/camera/Camera;->getEngine()Lcom/sec/android/app/camera/CommonEngine;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/camera/CommonEngine;->getSurfaceView()Lcom/sec/android/app/camera/PreviewFrameLayout;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 959
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v5}, Lcom/sec/android/app/camera/Camera;->getEngine()Lcom/sec/android/app/camera/CommonEngine;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/camera/CommonEngine;->getSurfaceView()Lcom/sec/android/app/camera/PreviewFrameLayout;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/camera/PreviewFrameLayout;->getPreviewRect()Landroid/graphics/Rect;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mPreviewRect:Landroid/graphics/Rect;

    .line 962
    :cond_1
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    if-eqz v5, :cond_0

    .line 963
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mPreviewRect:Landroid/graphics/Rect;

    const/4 v6, 0x0

    invoke-direct {p0, v5, v6}, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->getSelfieGuideBoxResizeRange(Landroid/graphics/Rect;I)F

    move-result v1

    .line 964
    .local v1, "MIN_GUIDEWIDTH":F
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mPreviewRect:Landroid/graphics/Rect;

    const/4 v6, 0x2

    invoke-direct {p0, v5, v6}, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->getSelfieGuideBoxResizeRange(Landroid/graphics/Rect;I)F

    move-result v0

    .line 965
    .local v0, "MAX_GUIDEWIDTH":F
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v5}, Lcom/sec/android/glview/TwGLNinePatch;->getWidth()F

    move-result v4

    .line 966
    .local v4, "width":F
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v5}, Lcom/sec/android/glview/TwGLNinePatch;->getHeight()F

    move-result v2

    .line 967
    .local v2, "height":F
    const/4 v3, 0x0

    .line 970
    .local v3, "isResize":Z
    cmpg-float v5, v4, v1

    if-gez v5, :cond_2

    .line 971
    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->SELFIE_GUIDE_WIDTH:F

    sub-float v5, v1, v5

    iput v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mAmountDeltaX:F

    .line 972
    move v4, v1

    .line 973
    const/4 v3, 0x1

    .line 977
    :cond_2
    cmpl-float v5, v4, v0

    if-lez v5, :cond_3

    .line 978
    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->SELFIE_GUIDE_WIDTH:F

    sub-float v5, v0, v5

    iput v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mAmountDeltaX:F

    .line 979
    move v4, v0

    .line 980
    const/4 v3, 0x1

    .line 983
    :cond_3
    if-eqz v3, :cond_4

    .line 984
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v5, v4, v2}, Lcom/sec/android/glview/TwGLNinePatch;->setSize(FF)V

    .line 987
    :cond_4
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v5}, Lcom/sec/android/glview/TwGLNinePatch;->getCurrentLeft()F

    move-result v5

    iget-object v6, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mPreviewRect:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->left:I

    sget v7, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->TOUCH_AF_BOUNDARY:I

    add-int/2addr v6, v7

    int-to-float v6, v6

    cmpg-float v5, v5, v6

    if-gtz v5, :cond_6

    .line 988
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    iget-object v6, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mPreviewRect:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->left:I

    sget v7, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->TOUCH_AF_BOUNDARY:I

    add-int/2addr v6, v7

    int-to-float v6, v6

    iget-object v7, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v7}, Lcom/sec/android/glview/TwGLNinePatch;->getCurrentLeft()F

    move-result v7

    sub-float/2addr v6, v7

    invoke-virtual {v5, v6, v9}, Lcom/sec/android/glview/TwGLNinePatch;->translate(FF)V

    .line 993
    :cond_5
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->setCurrentGuideFramePosition()V

    goto/16 :goto_0

    .line 989
    :cond_6
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v5}, Lcom/sec/android/glview/TwGLNinePatch;->getCurrentRight()F

    move-result v5

    iget-object v6, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mPreviewRect:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->right:I

    sget v7, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->TOUCH_AF_BOUNDARY:I

    sub-int/2addr v6, v7

    int-to-float v6, v6

    cmpl-float v5, v5, v6

    if-ltz v5, :cond_5

    .line 990
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    iget-object v6, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v6}, Lcom/sec/android/glview/TwGLNinePatch;->getCurrentRight()F

    move-result v6

    iget-object v7, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mPreviewRect:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->right:I

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->TOUCH_AF_BOUNDARY:I

    sub-int/2addr v7, v8

    int-to-float v7, v7

    sub-float/2addr v6, v7

    neg-float v6, v6

    invoke-virtual {v5, v6, v9}, Lcom/sec/android/glview/TwGLNinePatch;->translate(FF)V

    goto :goto_1

    .line 997
    .end local v0    # "MAX_GUIDEWIDTH":F
    .end local v1    # "MIN_GUIDEWIDTH":F
    .end local v2    # "height":F
    .end local v3    # "isResize":Z
    .end local v4    # "width":F
    :pswitch_2
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->setCurrentGuideFramePosition()V

    goto/16 :goto_0

    .line 955
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onHide()V
    .locals 2

    .prologue
    .line 278
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->getShootingMode()I

    move-result v0

    const/16 v1, 0x38

    if-ne v0, v1, :cond_0

    .line 279
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->hideSelfieGuideFrame()V

    .line 281
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->hideCaptureLayout()V

    .line 282
    return-void
.end method

.method public onOrientationChanged(I)V
    .locals 0
    .param p1, "orientation"    # I

    .prologue
    .line 478
    iput p1, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mOrientation:I

    .line 479
    return-void
.end method

.method public onPause()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 437
    const-string v0, "TwGLSelfieAlarmMenu"

    const-string v1, "onPause"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 438
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->getCaptureProgressIncreased()I

    move-result v0

    if-gtz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->isBurstCapturing()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 439
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mIsBurstCapturing:Z

    if-eqz v0, :cond_1

    .line 440
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->onBurstCaptureCancelled()V

    .line 442
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->getShootingMode()I

    move-result v0

    const/16 v1, 0x18

    if-eq v0, v1, :cond_2

    .line 443
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->onHide()V

    .line 445
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->reset()V

    .line 446
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->resetPostCaptureProgress()V

    .line 453
    :cond_3
    :goto_0
    return-void

    .line 448
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->getShootingMode()I

    move-result v0

    const/16 v1, 0x38

    if-ne v0, v1, :cond_3

    .line 449
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0, v2, v2}, Lcom/sec/android/app/camera/Camera;->onSelfieAlarmGuideStateChanged(IZ)V

    .line 450
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->stopSelfieAlarmGuideStateSound()V

    goto :goto_0
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 427
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    if-eqz v0, :cond_0

    .line 428
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->getShootingMode()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->getShootingMode()I

    move-result v0

    const/16 v1, 0x11

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->getShootingMode()I

    move-result v0

    const/16 v1, 0x3a

    if-eq v0, v1, :cond_0

    .line 430
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->showCaptureLayout()V

    .line 433
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/camera/MenuBase;->onResume()V

    .line 434
    return-void
.end method

.method public onShow()V
    .locals 2

    .prologue
    .line 264
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->getShootingMode()I

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->getShootingMode()I

    move-result v0

    const/16 v1, 0x11

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->getShootingMode()I

    move-result v0

    const/16 v1, 0x3a

    if-eq v0, v1, :cond_1

    .line 266
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->getShootingMode()I

    move-result v0

    const/16 v1, 0x38

    if-ne v0, v1, :cond_0

    .line 267
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getEngine()Lcom/sec/android/app/camera/CommonEngine;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CommonEngine;->getSurfaceView()Lcom/sec/android/app/camera/PreviewFrameLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/PreviewFrameLayout;->getPreviewRect()Landroid/graphics/Rect;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mPreviewRect:Landroid/graphics/Rect;

    .line 270
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mNeedToResizeGuide:Z

    if-nez v0, :cond_1

    .line 271
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->reset()V

    .line 272
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->showCaptureLayout()V

    .line 275
    :cond_1
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 456
    const-string v0, "TwGLSelfieAlarmMenu"

    const-string v1, "onStop"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 457
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->stopCancelTimer()V

    .line 458
    return-void
.end method

.method public onTouch(Lcom/sec/android/glview/TwGLView;Landroid/view/MotionEvent;)Z
    .locals 10
    .param p1, "view"    # Lcom/sec/android/glview/TwGLView;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v8, 0x3

    const/high16 v4, 0x40a00000    # 5.0f

    const/4 v5, 0x2

    const/4 v0, 0x0

    const/4 v7, 0x1

    .line 576
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    if-nez v1, :cond_0

    .line 668
    :goto_0
    return v0

    .line 579
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    if-ne p1, v1, :cond_13

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/Camera;->getEngine()Lcom/sec/android/app/camera/CommonEngine;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/camera/CommonEngine;->isRecording()Z

    move-result v1

    if-nez v1, :cond_13

    .line 580
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :cond_1
    :goto_1
    move v0, v7

    .line 663
    goto :goto_0

    .line 582
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getCameraBaseIndicator()Lcom/sec/android/app/camera/glwidget/TwGLCameraBaseIndicators;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/glwidget/TwGLCameraBaseIndicators;->hideHelpText()V

    .line 584
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mMovePoint:Landroid/graphics/PointF;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    invoke-virtual {v0, v1, v4}, Landroid/graphics/PointF;->set(FF)V

    .line 585
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->setSelectedSelfieGuide(FF)V

    .line 591
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelectedSelfieGuide:I

    if-eqz v0, :cond_2

    .line 592
    iput v7, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideMode:I

    .line 593
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    const-string v1, "vibrator"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/camera/Camera;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/os/Vibrator;

    .line 594
    .local v6, "vibrator":Landroid/os/Vibrator;
    const-wide/16 v0, 0x32

    invoke-virtual {v6, v0, v1}, Landroid/os/Vibrator;->vibrate(J)V

    goto :goto_1

    .line 596
    .end local v6    # "vibrator":Landroid/os/Vibrator;
    :cond_2
    iput v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideMode:I

    goto :goto_1

    .line 601
    :pswitch_1
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideMode:I

    if-eqz v0, :cond_1

    .line 605
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mMovePoint:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    sub-float v2, v0, v1

    .line 606
    .local v2, "deltaX":F
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mMovePoint:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    sub-float v3, v0, v1

    .line 610
    .local v3, "deltaY":F
    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpg-float v0, v0, v4

    if-gez v0, :cond_3

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpg-float v0, v0, v4

    if-ltz v0, :cond_1

    .line 614
    :cond_3
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideMode:I

    if-ne v0, v5, :cond_9

    .line 615
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v0}, Lcom/sec/android/glview/TwGLNinePatch;->getCurrentLeft()F

    move-result v0

    add-float/2addr v0, v2

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mPreviewRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->TOUCH_AF_BOUNDARY:I

    add-int/2addr v1, v4

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_4

    .line 616
    const/4 v2, 0x0

    .line 618
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v0}, Lcom/sec/android/glview/TwGLNinePatch;->getCurrentRight()F

    move-result v0

    add-float/2addr v0, v2

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mPreviewRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->TOUCH_AF_BOUNDARY:I

    sub-int/2addr v1, v4

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_5

    .line 619
    const/4 v2, 0x0

    .line 621
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v0}, Lcom/sec/android/glview/TwGLNinePatch;->getCurrentTop()F

    move-result v0

    add-float/2addr v0, v3

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mPreviewRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->TOUCH_AF_BOUNDARY:I

    add-int/2addr v1, v4

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_6

    .line 622
    const/4 v3, 0x0

    .line 624
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v0}, Lcom/sec/android/glview/TwGLNinePatch;->getCurrentBottom()F

    move-result v0

    add-float/2addr v0, v3

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mPreviewRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->TOUCH_AF_BOTTOM_BOUNDARY:I

    sub-int/2addr v1, v4

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_7

    .line 625
    const/4 v3, 0x0

    .line 628
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/glview/TwGLNinePatch;->translate(FF)V

    .line 649
    :cond_8
    :goto_2
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mMovePoint:Landroid/graphics/PointF;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    invoke-virtual {v0, v1, v4}, Landroid/graphics/PointF;->set(FF)V

    goto/16 :goto_1

    .line 629
    :cond_9
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideMode:I

    if-ne v0, v7, :cond_8

    .line 630
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v0}, Lcom/sec/android/glview/TwGLNinePatch;->getCurrentLeft()F

    move-result v0

    add-float/2addr v0, v2

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mPreviewRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->TOUCH_AF_BOUNDARY:I

    add-int/2addr v1, v4

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_b

    .line 631
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelectedSelfieGuide:I

    if-eq v7, v0, :cond_a

    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelectedSelfieGuide:I

    if-ne v5, v0, :cond_b

    .line 632
    :cond_a
    const/4 v2, 0x0

    .line 634
    :cond_b
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v0}, Lcom/sec/android/glview/TwGLNinePatch;->getCurrentRight()F

    move-result v0

    add-float/2addr v0, v2

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mPreviewRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->TOUCH_AF_BOUNDARY:I

    sub-int/2addr v1, v4

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_d

    .line 635
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelectedSelfieGuide:I

    if-eq v8, v0, :cond_c

    const/4 v0, 0x4

    iget v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelectedSelfieGuide:I

    if-ne v0, v1, :cond_d

    .line 636
    :cond_c
    const/4 v2, 0x0

    .line 638
    :cond_d
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v0}, Lcom/sec/android/glview/TwGLNinePatch;->getCurrentTop()F

    move-result v0

    add-float/2addr v0, v3

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mPreviewRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->TOUCH_AF_BOUNDARY:I

    add-int/2addr v1, v4

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_f

    .line 639
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelectedSelfieGuide:I

    if-eq v7, v0, :cond_e

    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelectedSelfieGuide:I

    if-ne v8, v0, :cond_f

    .line 640
    :cond_e
    const/4 v3, 0x0

    .line 642
    :cond_f
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v0}, Lcom/sec/android/glview/TwGLNinePatch;->getCurrentBottom()F

    move-result v0

    add-float/2addr v0, v3

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mPreviewRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->TOUCH_AF_BOTTOM_BOUNDARY:I

    sub-int/2addr v1, v4

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_11

    .line 643
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelectedSelfieGuide:I

    if-eq v5, v0, :cond_10

    const/4 v0, 0x4

    iget v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelectedSelfieGuide:I

    if-ne v0, v1, :cond_11

    .line 644
    :cond_10
    const/4 v3, 0x0

    .line 646
    :cond_11
    const-string v0, "TwGLSelfieAlarmMenu"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[SelfShot][BONG]onTouch ACTION_MOVE / RESIZING: mSelectedSelfieGuide : "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelectedSelfieGuide:I

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ", DeltaX : "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mAmountDeltaX:F

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ", DeltaY : "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mAmountDeltaY:F

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 647
    iget v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelectedSelfieGuide:I

    iget v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mAmountDeltaX:F

    iget v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mAmountDeltaY:F

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->resizeSelfieGuide(IFFFF)V

    goto/16 :goto_2

    .line 653
    .end local v2    # "deltaX":F
    .end local v3    # "deltaY":F
    :pswitch_2
    iget v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideMode:I

    if-ne v1, v7, :cond_12

    .line 654
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    iget-object v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->RES_ID_SELFIE_GUIDE_FRAME_IMAGE:[I

    aget v4, v4, v0

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->SELFIE_GUIDE_WIDTH:F

    iget v8, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mAmountDeltaX:F

    add-float/2addr v5, v8

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->SELFIE_GUIDE_HEIGHT:F

    iget v9, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mAmountDeltaY:F

    add-float/2addr v8, v9

    invoke-virtual {v1, v4, v5, v8, v7}, Lcom/sec/android/glview/TwGLNinePatch;->setImage(IFFZ)V

    .line 657
    :cond_12
    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideMode:I

    .line 658
    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelectedSelfieGuide:I

    .line 660
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->setCurrentGuideFramePosition()V

    goto/16 :goto_1

    .line 665
    :cond_13
    const-string v1, "TwGLSelfieAlarmMenu"

    const-string v4, "[SelfShot]view is not self shot frame view"

    invoke-static {v1, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 580
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public reset()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 285
    const-string v0, "TwGLSelfieAlarmMenu"

    const-string v1, "Burstshot restart"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 286
    iput-boolean v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mIsBurstCapturing:Z

    .line 287
    iput v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mCaptureCount:I

    .line 288
    iput v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mStoreCount:I

    .line 289
    iput v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideMode:I

    .line 290
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->setTouchHandled(Z)V

    .line 291
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->stopCancelTimer()V

    .line 292
    iput-boolean v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mNeedToResizeGuide:Z

    .line 294
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mCapturedFileList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mCapturedFileList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 295
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mCapturedFileList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 296
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mCapturedFileList:Ljava/util/ArrayList;

    .line 298
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->getShootingMode()I

    move-result v0

    const/16 v1, 0x38

    if-ne v0, v1, :cond_1

    .line 299
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0, v2, v2}, Lcom/sec/android/app/camera/Camera;->onSelfieAlarmGuideStateChanged(IZ)V

    .line 301
    :cond_1
    return-void
.end method

.method public resetPostCaptureProgress()V
    .locals 3

    .prologue
    .line 304
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    if-nez v1, :cond_1

    .line 328
    :cond_0
    :goto_0
    return-void

    .line 308
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/camera/CameraSettings;->getShootingMode()I

    move-result v0

    .line 310
    .local v0, "modeid":I
    const/16 v1, 0x19

    if-eq v0, v1, :cond_2

    const/16 v1, 0x22

    if-ne v0, v1, :cond_3

    .line 311
    :cond_2
    sget v1, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->BESTFACE_MAX_IMAGE_COUNT:I

    iput v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mMaxStep:I

    .line 324
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/Camera;->getProgressingPopup()Lcom/sec/android/app/camera/glwidget/TwGLProgressingPopup;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 325
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/Camera;->getProgressingPopup()Lcom/sec/android/app/camera/glwidget/TwGLProgressingPopup;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/camera/glwidget/TwGLProgressingPopup;->resetAcquisitionProgress()V

    .line 326
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/Camera;->getProgressingPopup()Lcom/sec/android/app/camera/glwidget/TwGLProgressingPopup;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mMaxStep:I

    invoke-virtual {v1, v2}, Lcom/sec/android/app/camera/glwidget/TwGLProgressingPopup;->setPostCaptureProgressMax(I)V

    goto :goto_0

    .line 312
    :cond_3
    const/16 v1, 0x18

    if-ne v0, v1, :cond_4

    .line 313
    sget v1, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->BEST_MAX_IMAGE_COUNT:I

    iput v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mMaxStep:I

    goto :goto_1

    .line 314
    :cond_4
    const/16 v1, 0x38

    if-ne v0, v1, :cond_5

    .line 315
    sget v1, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->SMARTSELFIE_MAX_IMAGE_COUNT:I

    iput v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mMaxStep:I

    goto :goto_1

    .line 316
    :cond_5
    const/16 v1, 0x2e

    if-ne v0, v1, :cond_6

    .line 317
    sget v1, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->MAGIC_MAX_IMAGE_COUNT:I

    iput v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mMaxStep:I

    goto :goto_1

    .line 318
    :cond_6
    const/16 v1, 0x3a

    if-ne v0, v1, :cond_7

    .line 319
    sget v1, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->GIFMAKER_MAX_IMAGE_COUNT:I

    iput v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mMaxStep:I

    goto :goto_1

    .line 321
    :cond_7
    sget v1, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->BURST_MAX_IMAGE_COUNT:I

    iput v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mMaxStep:I

    goto :goto_1
.end method

.method public resetSelfieGuideFrame()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x0

    .line 516
    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideMode:I

    .line 517
    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelectedSelfieGuide:I

    .line 518
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    if-eqz v0, :cond_0

    .line 519
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLNinePatch;->setVisibility(I)V

    .line 520
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v0}, Lcom/sec/android/glview/TwGLNinePatch;->resetTranslate()V

    .line 521
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    sget v1, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->SELFIE_GUIDE_WIDTH:F

    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->SELFIE_GUIDE_HEIGHT:F

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/glview/TwGLNinePatch;->setSize(FF)V

    .line 522
    iput v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mAmountDeltaX:F

    .line 523
    iput v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mAmountDeltaY:F

    .line 525
    :cond_0
    return-void
.end method

.method public restartCancelTimer()V
    .locals 2

    .prologue
    .line 338
    const-string v0, "TwGLSelfieAlarmMenu"

    const-string v1, "call restartCancelTimer..."

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 339
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->stopCancelTimer()V

    .line 340
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->startCancelTimer()V

    .line 341
    return-void
.end method

.method public declared-synchronized selfieAlarmFDRectChanged([Lcom/sec/android/seccamera/SecCamera$Face;)V
    .locals 16
    .param p1, "faces"    # [Lcom/sec/android/seccamera/SecCamera$Face;

    .prologue
    .line 842
    monitor-enter p0

    const/16 v14, 0xa

    :try_start_0
    new-array v12, v14, [I

    .line 843
    .local v12, "selfShotROIState":[I
    const/4 v13, 0x0

    .line 845
    .local v13, "selfShotROIStateResult":I
    if-eqz p1, :cond_d

    move-object/from16 v0, p1

    array-length v14, v0

    if-lez v14, :cond_d

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->isSelfieAlarmFDDetecting()Z

    move-result v14

    if-eqz v14, :cond_d

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v14}, Lcom/sec/android/app/camera/Camera;->getWindowFocused()Z

    move-result v14

    if-eqz v14, :cond_d

    .line 846
    new-instance v8, Landroid/graphics/RectF;

    invoke-direct {v8}, Landroid/graphics/RectF;-><init>()V

    .line 848
    .local v8, "faceFrameRect":Landroid/graphics/RectF;
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_0
    move-object/from16 v0, p1

    array-length v14, v0

    if-ge v11, v14, :cond_c

    .line 849
    aget-object v14, p1, v11

    iget-object v14, v14, Lcom/sec/android/seccamera/SecCamera$Face;->rect:Landroid/graphics/Rect;

    invoke-virtual {v8, v14}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 850
    iget v14, v8, Landroid/graphics/RectF;->left:F

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mPreviewRect:Landroid/graphics/Rect;

    invoke-virtual {v15}, Landroid/graphics/Rect;->width()I

    move-result v15

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v15}, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->deNormalize(FI)F

    move-result v14

    iput v14, v8, Landroid/graphics/RectF;->left:F

    .line 851
    iget v14, v8, Landroid/graphics/RectF;->right:F

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mPreviewRect:Landroid/graphics/Rect;

    invoke-virtual {v15}, Landroid/graphics/Rect;->width()I

    move-result v15

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v15}, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->deNormalize(FI)F

    move-result v14

    iput v14, v8, Landroid/graphics/RectF;->right:F

    .line 852
    iget v14, v8, Landroid/graphics/RectF;->top:F

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mPreviewRect:Landroid/graphics/Rect;

    invoke-virtual {v15}, Landroid/graphics/Rect;->height()I

    move-result v15

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v15}, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->deNormalize(FI)F

    move-result v14

    iput v14, v8, Landroid/graphics/RectF;->top:F

    .line 853
    iget v14, v8, Landroid/graphics/RectF;->bottom:F

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mPreviewRect:Landroid/graphics/Rect;

    invoke-virtual {v15}, Landroid/graphics/Rect;->height()I

    move-result v15

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v15}, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->deNormalize(FI)F

    move-result v14

    iput v14, v8, Landroid/graphics/RectF;->bottom:F

    .line 855
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mPreviewRect:Landroid/graphics/Rect;

    iget v14, v14, Landroid/graphics/Rect;->left:I

    if-eqz v14, :cond_0

    .line 856
    iget v14, v8, Landroid/graphics/RectF;->left:F

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mPreviewRect:Landroid/graphics/Rect;

    iget v15, v15, Landroid/graphics/Rect;->left:I

    int-to-float v15, v15

    add-float/2addr v14, v15

    iput v14, v8, Landroid/graphics/RectF;->left:F

    .line 857
    iget v14, v8, Landroid/graphics/RectF;->top:F

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mPreviewRect:Landroid/graphics/Rect;

    iget v15, v15, Landroid/graphics/Rect;->top:I

    int-to-float v15, v15

    add-float/2addr v14, v15

    iput v14, v8, Landroid/graphics/RectF;->top:F

    .line 858
    iget v14, v8, Landroid/graphics/RectF;->right:F

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mPreviewRect:Landroid/graphics/Rect;

    iget v15, v15, Landroid/graphics/Rect;->left:I

    int-to-float v15, v15

    add-float/2addr v14, v15

    iput v14, v8, Landroid/graphics/RectF;->right:F

    .line 859
    iget v14, v8, Landroid/graphics/RectF;->bottom:F

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mPreviewRect:Landroid/graphics/Rect;

    iget v15, v15, Landroid/graphics/Rect;->top:I

    int-to-float v15, v15

    add-float/2addr v14, v15

    iput v14, v8, Landroid/graphics/RectF;->bottom:F

    .line 864
    :cond_0
    iget v14, v8, Landroid/graphics/RectF;->left:F

    const/4 v15, 0x0

    cmpg-float v14, v14, v15

    if-gtz v14, :cond_1

    iget v14, v8, Landroid/graphics/RectF;->top:F

    const/4 v15, 0x0

    cmpg-float v14, v14, v15

    if-lez v14, :cond_2

    :cond_1
    invoke-virtual {v8}, Landroid/graphics/RectF;->width()F

    move-result v14

    const/4 v15, 0x0

    cmpg-float v14, v14, v15

    if-gtz v14, :cond_3

    invoke-virtual {v8}, Landroid/graphics/RectF;->height()F

    move-result v14

    const/4 v15, 0x0

    cmpg-float v14, v14, v15

    if-gtz v14, :cond_3

    .line 866
    :cond_2
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->resetFaceDetectionRect()V

    .line 867
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    const/4 v15, 0x1

    invoke-virtual {v14, v13, v15}, Lcom/sec/android/app/camera/Camera;->onSelfieAlarmGuideStateChanged(IZ)V

    .line 868
    const-string v14, "TwGLSelfieAlarmMenu"

    const-string v15, "selfieAlarmFDRectChanged resetFaceDetectionRect return"

    invoke-static {v14, v15}, Landroid/util/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 919
    .end local v8    # "faceFrameRect":Landroid/graphics/RectF;
    .end local v11    # "i":I
    :goto_1
    monitor-exit p0

    return-void

    .line 873
    .restart local v8    # "faceFrameRect":Landroid/graphics/RectF;
    .restart local v11    # "i":I
    :cond_3
    :try_start_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v14}, Lcom/sec/android/glview/TwGLNinePatch;->getWidth()F

    move-result v14

    const v15, 0x3e4ccccd    # 0.2f

    mul-float/2addr v14, v15

    const/high16 v15, 0x40000000    # 2.0f

    div-float v10, v14, v15

    .line 874
    .local v10, "hiddenGuideWidth":F
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v14}, Lcom/sec/android/glview/TwGLNinePatch;->getHeight()F

    move-result v14

    const v15, 0x3e4ccccd    # 0.2f

    mul-float/2addr v14, v15

    const/high16 v15, 0x40000000    # 2.0f

    div-float v9, v14, v15

    .line 876
    .local v9, "hiddenGuideHeight":F
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v14}, Lcom/sec/android/glview/TwGLNinePatch;->getCurrentLeft()F

    move-result v14

    sget v15, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->SELFIE_FRAME_MARGIN:I

    int-to-float v15, v15

    add-float/2addr v14, v15

    sub-float v5, v14, v10

    .line 877
    .local v5, "curGuideLeft":F
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v14}, Lcom/sec/android/glview/TwGLNinePatch;->getCurrentRight()F

    move-result v14

    sget v15, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->SELFIE_FRAME_MARGIN:I

    int-to-float v15, v15

    sub-float/2addr v14, v15

    add-float v6, v14, v10

    .line 878
    .local v6, "curGuideRight":F
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v14}, Lcom/sec/android/glview/TwGLNinePatch;->getCurrentTop()F

    move-result v14

    sget v15, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->SELFIE_FRAME_MARGIN:I

    int-to-float v15, v15

    add-float/2addr v14, v15

    sub-float v7, v14, v9

    .line 879
    .local v7, "curGuideTop":F
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v14}, Lcom/sec/android/glview/TwGLNinePatch;->getCurrentBottom()F

    move-result v14

    sget v15, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->SELFIE_FRAME_MARGIN:I

    int-to-float v15, v15

    sub-float/2addr v14, v15

    add-float v4, v14, v9

    .line 880
    .local v4, "curGuideBottom":F
    sget v14, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->SCREEN_HEIGHT:I

    int-to-float v14, v14

    const v15, 0x3f4ccccd    # 0.8f

    mul-float/2addr v14, v15

    cmpl-float v14, v4, v14

    if-ltz v14, :cond_4

    .line 881
    sget v14, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->SCREEN_HEIGHT:I

    int-to-float v14, v14

    const v15, 0x3f4ccccd    # 0.8f

    mul-float v4, v14, v15

    .line 884
    :cond_4
    const/high16 v3, 0x40a00000    # 5.0f

    .line 885
    .local v3, "boundary_gap":F
    invoke-virtual {v8}, Landroid/graphics/RectF;->centerX()F

    move-result v1

    .line 886
    .local v1, "FDCenX":F
    invoke-virtual {v8}, Landroid/graphics/RectF;->centerY()F

    move-result v2

    .line 889
    .local v2, "FDCenY":F
    iget v14, v8, Landroid/graphics/RectF;->left:F

    cmpg-float v14, v5, v14

    if-gtz v14, :cond_5

    iget v14, v8, Landroid/graphics/RectF;->right:F

    cmpl-float v14, v6, v14

    if-ltz v14, :cond_5

    iget v14, v8, Landroid/graphics/RectF;->top:F

    cmpg-float v14, v7, v14

    if-gtz v14, :cond_5

    iget v14, v8, Landroid/graphics/RectF;->bottom:F

    cmpl-float v14, v4, v14

    if-ltz v14, :cond_5

    .line 891
    const/4 v14, 0x4

    aput v14, v12, v11

    .line 907
    :goto_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mFaceDetectionRect:Landroid/graphics/Rect;

    iget v15, v8, Landroid/graphics/RectF;->left:F

    float-to-int v15, v15

    iput v15, v14, Landroid/graphics/Rect;->left:I

    .line 908
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mFaceDetectionRect:Landroid/graphics/Rect;

    iget v15, v8, Landroid/graphics/RectF;->top:F

    float-to-int v15, v15

    iput v15, v14, Landroid/graphics/Rect;->top:I

    .line 909
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mFaceDetectionRect:Landroid/graphics/Rect;

    iget v15, v8, Landroid/graphics/RectF;->right:F

    float-to-int v15, v15

    iput v15, v14, Landroid/graphics/Rect;->right:I

    .line 910
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mFaceDetectionRect:Landroid/graphics/Rect;

    iget v15, v8, Landroid/graphics/RectF;->bottom:F

    float-to-int v15, v15

    iput v15, v14, Landroid/graphics/Rect;->bottom:I

    .line 848
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_0

    .line 892
    :cond_5
    cmpg-float v14, v5, v1

    if-gtz v14, :cond_6

    cmpl-float v14, v6, v1

    if-ltz v14, :cond_6

    cmpg-float v14, v7, v2

    if-gtz v14, :cond_6

    cmpl-float v14, v4, v2

    if-ltz v14, :cond_6

    .line 894
    const/4 v14, 0x3

    aput v14, v12, v11
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 842
    .end local v1    # "FDCenX":F
    .end local v2    # "FDCenY":F
    .end local v3    # "boundary_gap":F
    .end local v4    # "curGuideBottom":F
    .end local v5    # "curGuideLeft":F
    .end local v6    # "curGuideRight":F
    .end local v7    # "curGuideTop":F
    .end local v8    # "faceFrameRect":Landroid/graphics/RectF;
    .end local v9    # "hiddenGuideHeight":F
    .end local v10    # "hiddenGuideWidth":F
    .end local v11    # "i":I
    .end local v12    # "selfShotROIState":[I
    .end local v13    # "selfShotROIStateResult":I
    :catchall_0
    move-exception v14

    monitor-exit p0

    throw v14

    .line 895
    .restart local v1    # "FDCenX":F
    .restart local v2    # "FDCenY":F
    .restart local v3    # "boundary_gap":F
    .restart local v4    # "curGuideBottom":F
    .restart local v5    # "curGuideLeft":F
    .restart local v6    # "curGuideRight":F
    .restart local v7    # "curGuideTop":F
    .restart local v8    # "faceFrameRect":Landroid/graphics/RectF;
    .restart local v9    # "hiddenGuideHeight":F
    .restart local v10    # "hiddenGuideWidth":F
    .restart local v11    # "i":I
    .restart local v12    # "selfShotROIState":[I
    .restart local v13    # "selfShotROIStateResult":I
    :cond_6
    :try_start_2
    invoke-virtual {v8}, Landroid/graphics/RectF;->width()F

    move-result v14

    const/high16 v15, 0x40000000    # 2.0f

    div-float/2addr v14, v15

    sub-float v14, v5, v14

    add-float/2addr v14, v3

    cmpg-float v14, v14, v1

    if-gtz v14, :cond_7

    invoke-virtual {v8}, Landroid/graphics/RectF;->width()F

    move-result v14

    const/high16 v15, 0x40000000    # 2.0f

    div-float/2addr v14, v15

    add-float/2addr v14, v6

    sub-float/2addr v14, v3

    cmpl-float v14, v14, v1

    if-ltz v14, :cond_7

    invoke-virtual {v8}, Landroid/graphics/RectF;->height()F

    move-result v14

    const/high16 v15, 0x40000000    # 2.0f

    div-float/2addr v14, v15

    sub-float v14, v7, v14

    add-float/2addr v14, v3

    cmpg-float v14, v14, v2

    if-gtz v14, :cond_7

    invoke-virtual {v8}, Landroid/graphics/RectF;->height()F

    move-result v14

    const/high16 v15, 0x40000000    # 2.0f

    div-float/2addr v14, v15

    add-float/2addr v14, v4

    sub-float/2addr v14, v3

    cmpl-float v14, v14, v2

    if-ltz v14, :cond_7

    .line 897
    const/4 v14, 0x2

    aput v14, v12, v11

    goto :goto_2

    .line 898
    :cond_7
    iget v14, v8, Landroid/graphics/RectF;->right:F

    add-float/2addr v14, v3

    cmpl-float v14, v5, v14

    if-gtz v14, :cond_8

    add-float v14, v6, v3

    iget v15, v8, Landroid/graphics/RectF;->left:F

    cmpg-float v14, v14, v15

    if-ltz v14, :cond_8

    iget v14, v8, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v14, v3

    cmpl-float v14, v7, v14

    if-gtz v14, :cond_8

    add-float v14, v4, v3

    iget v15, v8, Landroid/graphics/RectF;->top:F

    cmpg-float v14, v14, v15

    if-gez v14, :cond_9

    .line 900
    :cond_8
    const/4 v14, 0x0

    aput v14, v12, v11

    goto/16 :goto_2

    .line 901
    :cond_9
    iget v14, v8, Landroid/graphics/RectF;->right:F

    add-float/2addr v14, v3

    cmpg-float v14, v5, v14

    if-lez v14, :cond_a

    iget v14, v8, Landroid/graphics/RectF;->left:F

    add-float/2addr v14, v3

    cmpg-float v14, v6, v14

    if-lez v14, :cond_a

    iget v14, v8, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v14, v3

    cmpg-float v14, v7, v14

    if-lez v14, :cond_a

    iget v14, v8, Landroid/graphics/RectF;->top:F

    add-float/2addr v14, v3

    cmpg-float v14, v4, v14

    if-gtz v14, :cond_b

    .line 903
    :cond_a
    const/4 v14, 0x1

    aput v14, v12, v11

    goto/16 :goto_2

    .line 905
    :cond_b
    const-string v14, "TwGLSelfieAlarmMenu"

    const-string v15, "Boundary line check error"

    invoke-static {v14, v15}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 912
    .end local v1    # "FDCenX":F
    .end local v2    # "FDCenY":F
    .end local v3    # "boundary_gap":F
    .end local v4    # "curGuideBottom":F
    .end local v5    # "curGuideLeft":F
    .end local v6    # "curGuideRight":F
    .end local v7    # "curGuideTop":F
    .end local v9    # "hiddenGuideHeight":F
    .end local v10    # "hiddenGuideWidth":F
    :cond_c
    move-object/from16 v0, p1

    array-length v14, v0

    move-object/from16 v0, p0

    invoke-direct {v0, v12, v14}, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->checkShotROIState([II)I

    move-result v13

    .line 913
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    const/4 v15, 0x0

    invoke-virtual {v14, v13, v15}, Lcom/sec/android/app/camera/Camera;->onSelfieAlarmGuideStateChanged(IZ)V

    goto/16 :goto_1

    .line 915
    .end local v8    # "faceFrameRect":Landroid/graphics/RectF;
    .end local v11    # "i":I
    :cond_d
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->resetFaceDetectionRect()V

    .line 916
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    const/4 v15, 0x1

    invoke-virtual {v14, v13, v15}, Lcom/sec/android/app/camera/Camera;->onSelfieAlarmGuideStateChanged(IZ)V

    .line 917
    const-string v14, "TwGLSelfieAlarmMenu"

    const-string v15, "selfieAlarmFDRectChanged resetFaceDetectionRect"

    invoke-static {v14, v15}, Landroid/util/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_1
.end method

.method public setBurstCapturing(Z)V
    .locals 1
    .param p1, "capture"    # Z

    .prologue
    .line 472
    iput-boolean p1, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mIsBurstCapturing:Z

    .line 473
    iget-boolean v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mIsBurstCapturing:Z

    if-eqz v0, :cond_0

    .line 474
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->setTouchHandled(Z)V

    .line 475
    :cond_0
    return-void
.end method

.method public setCurrentGuideFramePosition()V
    .locals 7

    .prologue
    .line 691
    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v3}, Lcom/sec/android/app/camera/Camera;->getEngine()Lcom/sec/android/app/camera/CommonEngine;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    if-nez v3, :cond_2

    .line 692
    :cond_0
    const-string v3, "TwGLSelfieAlarmMenu"

    const-string v4, "setCurrentGuideFramePosition obj is null. so return..."

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 716
    :cond_1
    :goto_0
    return-void

    .line 696
    :cond_2
    new-instance v2, Landroid/graphics/RectF;

    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v3}, Lcom/sec/android/glview/TwGLNinePatch;->getCurrentLeft()F

    move-result v3

    iget-object v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v4}, Lcom/sec/android/glview/TwGLNinePatch;->getCurrentTop()F

    move-result v4

    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v5}, Lcom/sec/android/glview/TwGLNinePatch;->getCurrentRight()F

    move-result v5

    iget-object v6, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v6}, Lcom/sec/android/glview/TwGLNinePatch;->getCurrentBottom()F

    move-result v6

    invoke-direct {v2, v3, v4, v5, v6}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 698
    .local v2, "roiRect":Landroid/graphics/RectF;
    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mPreviewRect:Landroid/graphics/Rect;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mPreviewRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    if-eqz v3, :cond_1

    .line 699
    iget v3, v2, Landroid/graphics/RectF;->left:F

    iget-object v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mPreviewRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->left:F

    .line 700
    iget v3, v2, Landroid/graphics/RectF;->right:F

    iget-object v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mPreviewRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->right:F

    .line 701
    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v3}, Lcom/sec/android/app/camera/Camera;->getEngine()Lcom/sec/android/app/camera/CommonEngine;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/camera/CommonEngine;->getPreviewWidth()I

    move-result v1

    .line 702
    .local v1, "realPreviewWith":I
    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v3}, Lcom/sec/android/app/camera/Camera;->getEngine()Lcom/sec/android/app/camera/CommonEngine;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/camera/CommonEngine;->getPreviewHeight()I

    move-result v0

    .line 706
    .local v0, "realPreviewHeight":I
    iget v3, v2, Landroid/graphics/RectF;->bottom:F

    int-to-float v4, v0

    cmpl-float v3, v3, v4

    if-lez v3, :cond_3

    .line 707
    int-to-float v3, v0

    iput v3, v2, Landroid/graphics/RectF;->bottom:F

    .line 709
    :cond_3
    iget v3, v2, Landroid/graphics/RectF;->right:F

    int-to-float v4, v1

    cmpl-float v3, v3, v4

    if-lez v3, :cond_1

    .line 710
    int-to-float v3, v1

    iput v3, v2, Landroid/graphics/RectF;->right:F

    goto :goto_0
.end method

.method public setNeedToResize(Z)V
    .locals 3
    .param p1, "resize"    # Z

    .prologue
    .line 482
    const-string v0, "TwGLSelfieAlarmMenu"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setNeedToResize : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 483
    iput-boolean p1, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mNeedToResizeGuide:Z

    .line 484
    return-void
.end method

.method public setOnBurstCaptureCancelledListener(Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu$OnBurstCaptureCancelListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu$OnBurstCaptureCancelListener;

    .prologue
    .line 410
    iput-object p1, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mListener:Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu$OnBurstCaptureCancelListener;

    .line 411
    return-void
.end method

.method public setPostCaptureProgress(I)V
    .locals 3
    .param p1, "progress"    # I

    .prologue
    .line 362
    const-string v0, "TwGLSelfieAlarmMenu"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setPostCaptureProgress"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 364
    return-void
.end method

.method public setPostCaptureProgressMax(I)V
    .locals 1
    .param p1, "max"    # I

    .prologue
    .line 367
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getProgressingPopup()Lcom/sec/android/app/camera/glwidget/TwGLProgressingPopup;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 368
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getProgressingPopup()Lcom/sec/android/app/camera/glwidget/TwGLProgressingPopup;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/camera/glwidget/TwGLProgressingPopup;->setPostCaptureProgressMax(I)V

    .line 370
    :cond_0
    return-void
.end method

.method public setSelfieAlarmMode(Z)V
    .locals 0
    .param p1, "smartSelfieMode"    # Z

    .prologue
    .line 487
    iput-boolean p1, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieAlarmMode:Z

    .line 488
    return-void
.end method

.method public showCaptureLayout()V
    .locals 2

    .prologue
    .line 397
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->getShootingMode()I

    move-result v0

    const/16 v1, 0x38

    if-ne v0, v1, :cond_0

    .line 398
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->showSelfieGuideFrame()V

    .line 400
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->hidePostCaptureLayout()V

    .line 401
    return-void
.end method

.method public showPostCaptureLayout()V
    .locals 1

    .prologue
    .line 379
    iget-boolean v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieAlarmMode:Z

    if-eqz v0, :cond_0

    .line 380
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getProgressingPopup()Lcom/sec/android/app/camera/glwidget/TwGLProgressingPopup;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 381
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getProgressingPopup()Lcom/sec/android/app/camera/glwidget/TwGLProgressingPopup;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/glwidget/TwGLProgressingPopup;->hidePostCaptureLayout()V

    .line 383
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->hideCaptureLayout()V

    .line 384
    return-void
.end method

.method public showSelfieGuideFrame()V
    .locals 3

    .prologue
    .line 491
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/Camera;->isContextMenuOpened()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/Camera;->isListMenuOpened()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/Camera;->isThumbnailListMenuOpened()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 492
    :cond_0
    const-string v1, "TwGLSelfieAlarmMenu"

    const-string v2, "Menu opened, return showselfieGuideFrame"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 503
    :cond_1
    :goto_0
    return-void

    .line 495
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v1}, Lcom/sec/android/glview/TwGLNinePatch;->getVisibility()I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_1

    .line 496
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mPreviewRect:Landroid/graphics/Rect;

    const/4 v2, 0x2

    invoke-direct {p0, v1, v2}, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->getSelfieGuideBoxResizeRange(Landroid/graphics/Rect;I)F

    move-result v0

    .line 497
    .local v0, "MAX_ROIWIDTH":F
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v1}, Lcom/sec/android/glview/TwGLNinePatch;->getWidth()F

    move-result v1

    cmpl-float v1, v1, v0

    if-lez v1, :cond_3

    .line 498
    sget v1, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->SELFIE_GUIDE_WIDTH:F

    sub-float v1, v0, v1

    iput v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mAmountDeltaX:F

    .line 499
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v2}, Lcom/sec/android/glview/TwGLNinePatch;->getHeight()F

    move-result v2

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/glview/TwGLNinePatch;->setSize(FF)V

    .line 501
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mSelfieGuideFrame:Lcom/sec/android/glview/TwGLNinePatch;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/glview/TwGLNinePatch;->setVisibility(I)V

    goto :goto_0
.end method

.method public startCancelTimer()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 331
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mBurstMsgHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 332
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mBurstMsgHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 334
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mBurstMsgHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x4e20

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 335
    return-void
.end method

.method public stopCancelTimer()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 344
    const-string v0, "TwGLSelfieAlarmMenu"

    const-string v1, "stopCancelTimer..."

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 345
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mBurstMsgHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 346
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLSelfieAlarmMenu;->mBurstMsgHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 348
    :cond_0
    return-void
.end method
