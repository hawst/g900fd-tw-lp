.class public Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;
.super Lcom/sec/android/app/camera/MenuBase;
.source "TwGLUltraWideShotMenu.java"

# interfaces
.implements Lcom/sec/android/glview/TwGLView$OnOrientationChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu$OnUltraWideShotCaptureCancelListener;
    }
.end annotation


# static fields
.field private static final ANIMATION_TOP_MARGIN_LEFT:F

.field private static final ANIMATION_TOP_MARGIN_TOP:F

.field private static final ARROW_LANDSCAPE_OVERLAY:F

.field private static final ARROW_MARGIN:F

.field private static final BACKGROUND_RECT_PORTRAIT_X:[F

.field private static final BACKGROUND_RECT_PORTRAIT_Y:[F

.field private static final CAPTURE_ANGLE_120:I = 0x78

.field private static final CAPTURE_ANGLE_150:I = 0x96

.field private static final CAPTURE_ANGLE_BUTTON_PORT_MARGIN:F

.field private static final CAPTURE_ANGLE_BUTTON_POS_Y:F

.field private static final CAPTURE_ANGLE_MARGIN:F

.field private static final DIRECTION_INDICATOR_POS_X:[F

.field private static final DIRECTION_INDICATOR_POS_Y:[F

.field private static final FOCUS_RECT_LANDSCAPE_X:F

.field private static final FOCUS_RECT_LANDSCAPE_Y:F

.field private static final FOCUS_RECT_PORTRAIT_X:F

.field private static FOCUS_RECT_PORTRAIT_Y:F = 0.0f

.field public static final GUIDETEXT_LEFT:I = 0x1

.field public static final GUIDETEXT_NONE:I = 0x3

.field public static final GUIDETEXT_RIGHT:I = 0x2

.field public static final GUIDETEXT_START:I = 0x0

.field public static final GUIDE_ALL:I = 0x0

.field public static final GUIDE_LEFT:I = 0x1

.field public static final GUIDE_RIGHT:I = 0x2

.field private static final GUIDE_TEXT_HEIGHT:F

.field private static GUIDE_TEXT_PORTRAIT_HEIGHT:F = 0.0f

.field private static final GUIDE_TEXT_PORTRAIT_X:[F

.field private static final GUIDE_TEXT_PORTRAIT_Y:[F

.field private static final GUIDE_TEXT_POS_X:F

.field private static final GUIDE_TEXT_SET_SHADOW:Z

.field private static final GUIDE_TEXT_SHADOW_OFFSET:I

.field private static final GUIDE_TEXT_SHADOW_POS_Y:I

.field private static final GUIDE_TEXT_SIZE:F

.field private static final GUIDE_TEXT_STROKE_COLOR:I

.field private static final GUIDE_TEXT_STROKE_WIDTH:I

.field private static final GUIDE_TEXT_WIDTH:F

.field private static final LANDSCAPE:I = 0x0

.field private static LANDSCAPE_CAPTURE_COUNT:I = 0x0

.field private static final LIVEPREVIEW_LANDSCAPE_HEIGHT:F

.field private static final LIVEPREVIEW_LANDSCAPE_WIDTH:F

.field private static final LIVEPREVIEW_LANDSCAPE_X:F

.field private static final LIVEPREVIEW_LANDSCAPE_Y:F

.field private static LIVEPREVIEW_PORTRAIT_HEIGHT:F = 0.0f

.field private static final LIVEPREVIEW_PORTRAIT_WIDTH:F

.field private static final LIVEPREVIEW_PORTRAIT_X:F

.field private static final LIVEPREVIEW_PORTRAIT_Y:F

.field private static final LIVEPREVIEW_RATIO:F

.field private static final LIVEPREVIEW_RECT_THICKNESS:F

.field private static final LIVEPREVIEW_SINGLE_HEIGHT:F

.field private static final LIVEPREVIEW_SINGLE_PORTRAIT_HEIGHT:F

.field private static final LIVEPREVIEW_SINGLE_PORTRAIT_WIDTH:F

.field private static final LIVEPREVIEW_SINGLE_PORTRAIT_X:F = 0.0f

.field private static LIVEPREVIEW_SINGLE_PORTRAIT_Y:F = 0.0f

.field private static final LIVEPREVIEW_SINGLE_WIDTH:F

.field private static final LIVEPREVIEW_SINGLE_X:F

.field private static final LIVEPREVIEW_SINGLE_Y:F = 0.0f

.field private static final MESSAGE_PLAY_HAPTIC:I = 0x9

.field private static final MESSAGE_SHOW_LIVEPREVIEW:I = 0xa

.field private static final MESSAGE_START_ANIMATION:I = 0x6

.field protected static final MESSAGE_TIMEOUT_CANCEL:I = 0x1

.field private static final MESSAGE_TIMEOUT_GUIDETEXT:I = 0x5

.field private static final MESSAGE_TIMEOUT_SKIP_CAPTURE:I = 0x7

.field private static final MESSAGE_TIMEOUT_SKIP_CHECK_WARNING:I = 0x8

.field private static final MESSAGE_ULTRA_WIDE_SHOT_WARNING_HIGH:I = 0x3

.field private static final MESSAGE_ULTRA_WIDE_SHOT_WARNING_LOW:I = 0x2

.field private static final NEXT_CAPTURE_FOCUS_LINE_WIDTH:F

.field private static final NEXT_CAPTURE_FOCUS_PORTRAIT_X:F

.field private static OFFSET_DISTANCE_HORIZONTAL:I = 0x0

.field private static OFFSET_DISTANCE_VERTICAL:I = 0x0

.field private static final PORTRAIT:I = 0x1

.field private static PORTRATE_CAPTURE_COUNT:I = 0x0

.field private static final PREVIEW_EASYMODE_GUIDE_PORTRAIT_MARGIN:F

.field private static final PREVIEW_EASYMODE_PORTRAIT_MARGIN:F

.field private static final PREVIEW_LEFT_MARGIN:F

.field private static final PREVIEW_PORTRAIT_MARGIN:F

.field private static final PREVIEW_TOP_MARGIN:F

.field public static final PROCESS_TIMER_TIMEOUT:I = 0x4e20

.field protected static SCREEN_HEIGHT:I = 0x0

.field private static final SCREEN_RATIO:F

.field protected static SCREEN_WIDTH:I = 0x0

.field private static final STOP_BUTTON_POS_X:I

.field private static final STOP_BUTTON_POS_Y:I

.field protected static final TAG:Ljava/lang/String; = "TwGLUltraWideShotMenu"

.field public static final WARNINGTEXT_MOVE_SLOWLY:I = 0x4

.field public static final WARNINGTEXT_WARNING_DOWN:I = 0x1

.field public static final WARNINGTEXT_WARNING_LEFT:I = 0x2

.field public static final WARNINGTEXT_WARNING_RIGHT:I = 0x3

.field public static final WARNINGTEXT_WARNING_UP:I

.field private static WARNING_DOWN:I

.field private static WARNING_LEFT:I

.field private static WARNING_LEVEL_HIGH:I

.field private static WARNING_LEVEL_LOW:I

.field private static WARNING_LEVEL_NONE:I

.field private static WARNING_LEVEL_STOP:I

.field private static WARNING_NONE:I

.field private static WARNING_RIGHT:I

.field private static WARNING_UP:I


# instance fields
.field private mAnimationDirection:Z

.field private mBackgroundRect:Lcom/sec/android/glview/TwGLNinePatch;

.field private mBackgroundRectGroup:Lcom/sec/android/glview/TwGLViewGroup;

.field private mBlinkAnimation:Landroid/view/animation/AlphaAnimation;

.field private mCaptureAngle:I

.field private mCaptureAngleButton:Lcom/sec/android/glview/TwGLButton;

.field private mCaptureCount:I

.field private mCaptureError:Z

.field private mCaptureFocusRect:Lcom/sec/android/glview/TwGLNinePatch;

.field private mCaptureStopButton:Lcom/sec/android/glview/TwGLButton;

.field private mCenterGuideImage:[Lcom/sec/android/glview/TwGLImage;

.field private mChanged:Z

.field private mCurrentDirection:I

.field private mCurrentOrientation:I

.field private mDetectedDirection:I

.field private mEncodingProgress:Z

.field private mFocusRectCenterX:F

.field private mFocusRectCenterY:F

.field private mFocusRectLeft:F

.field private mFocusRectTop:F

.field private mGuideArrow:[Lcom/sec/android/glview/TwGLImage;

.field private mGuideBlinkingAnimation:Landroid/view/animation/Animation;

.field private mGuideDirection:I

.field private mGuideOrient:I

.field private mGuideRect:[Lcom/sec/android/glview/TwGLImage;

.field private mGuideShow:Z

.field private mGuideState:I

.field private mGuideText:Lcom/sec/android/glview/TwGLText;

.field private mHeight:F

.field private mIsStartCapture:Z

.field private mIsUltraWideShotCapturing:Z

.field private mLandscapeOffset:D

.field private mLeftBottomX:F

.field private mLeftBottomY:F

.field private mLeftGuideImage:[Lcom/sec/android/glview/TwGLImage;

.field private mLeftTopX:F

.field private mLeftTopY:F

.field private mListener:Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu$OnUltraWideShotCaptureCancelListener;

.field private mLivePreview:Lcom/sec/android/glview/TwGLBitmapTexture;

.field private mLivePreviewGroup:Lcom/sec/android/glview/TwGLViewGroup;

.field private mMovingDetection:Z

.field private mNextCaptureFocusRect:Lcom/sec/android/glview/TwGLImage;

.field private mNextCaptureFocusRectCenterX:F

.field private mNextCaptureFocusRectCenterY:F

.field private mNextCaptureFocusRectLeft:F

.field private mNextCaptureFocusRectPort:Lcom/sec/android/glview/TwGLImage;

.field private mNextCaptureFocusRectTop:F

.field private mNextCaptureFocusStep:I

.field private mPlayHaptic:Z

.field private mPostProgress:I

.field private mPotraitOffset:D

.field private mPrevFocusRectCenterX:F

.field private mPrevFocusRectCenterY:F

.field private mPrevThumbnailHeight:F

.field private mPrevThumbnailWidth:F

.field private mPreviewFocusRect:Lcom/sec/android/glview/TwGLNinePatch;

.field private mPreviewThumbnailHeight:F

.field private mPreviewThumbnailLeft:F

.field private mPreviewThumbnailTop:F

.field private mPreviewThumbnailWidth:F

.field private mRightBottomX:F

.field private mRightBottomY:F

.field private mRightGuideImage:[Lcom/sec/android/glview/TwGLImage;

.field private mRightTopX:F

.field private mRightTopY:F

.field private mSkipCapture:Z

.field private mSkipCheckWarning:Z

.field private mSkipFrame:I

.field private mStartCheckWarning:Z

.field private mTrapezoidCaptureFocusRect:Lcom/sec/android/app/camera/glwidget/TwGLTrapezoid;

.field private mTwoBlinkAnimation:Landroid/view/animation/AlphaAnimation;

.field protected mUltraWideShotMsgHandler:Landroid/os/Handler;

.field private mUltraWideShotWarning:Z

.field private mWarningArrow:[Lcom/sec/android/glview/TwGLImage;

.field private mWarningBox:Lcom/sec/android/glview/TwGLImage;

.field private mWarningText:Lcom/sec/android/glview/TwGLText;

.field private mWidth:F


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 59
    const v0, 0x7f0a0006

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->SCREEN_WIDTH:I

    .line 60
    const v0, 0x7f0a0007

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->SCREEN_HEIGHT:I

    .line 61
    sget v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->SCREEN_WIDTH:I

    int-to-float v0, v0

    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->SCREEN_HEIGHT:I

    int-to-float v3, v3

    div-float/2addr v0, v3

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->SCREEN_RATIO:F

    .line 63
    const v0, 0x7f0a03dc

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->GUIDE_TEXT_WIDTH:F

    .line 64
    const v0, 0x7f0a03dd

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->GUIDE_TEXT_HEIGHT:F

    .line 65
    const v0, 0x7f0a03de

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->GUIDE_TEXT_POS_X:F

    .line 66
    const v0, 0x7f0a03e0

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->GUIDE_TEXT_PORTRAIT_HEIGHT:F

    .line 67
    new-array v0, v4, [F

    const v3, 0x7f0a03e1

    invoke-static {v3}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v3

    aput v3, v0, v2

    const v3, 0x7f0a03e3

    invoke-static {v3}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v3

    aput v3, v0, v1

    sput-object v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->GUIDE_TEXT_PORTRAIT_X:[F

    .line 68
    new-array v0, v4, [F

    const v3, 0x7f0a03e2

    invoke-static {v3}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v3

    aput v3, v0, v2

    const v3, 0x7f0a03e4

    invoke-static {v3}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v3

    aput v3, v0, v1

    sput-object v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->GUIDE_TEXT_PORTRAIT_Y:[F

    .line 69
    const v0, 0x7f0b0051

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getInteger(I)I

    move-result v0

    int-to-float v0, v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->GUIDE_TEXT_SIZE:F

    .line 70
    const v0, 0x7f0a04cf

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->GUIDE_TEXT_SHADOW_POS_Y:I

    .line 71
    const v0, 0x7f0b0029

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getInteger(I)I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->GUIDE_TEXT_SET_SHADOW:Z

    .line 72
    const v0, 0x7f0b001c

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getInteger(I)I

    move-result v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->GUIDE_TEXT_SHADOW_OFFSET:I

    .line 73
    const v0, 0x7f0b0028

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getInteger(I)I

    move-result v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->GUIDE_TEXT_STROKE_WIDTH:I

    .line 74
    const/high16 v0, 0x7f090000

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getColor(I)I

    move-result v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->GUIDE_TEXT_STROKE_COLOR:I

    .line 76
    const v0, 0x7f0a03f3

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->ARROW_MARGIN:F

    .line 77
    const v0, 0x7f0a03f2

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->ARROW_LANDSCAPE_OVERLAY:F

    .line 79
    const v0, 0x7f0a03d3

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_LANDSCAPE_WIDTH:F

    .line 80
    const v0, 0x7f0a03d4

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_LANDSCAPE_HEIGHT:F

    .line 81
    const v0, 0x7f0a03d7

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_LANDSCAPE_X:F

    .line 82
    const v0, 0x7f0a03d8

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_LANDSCAPE_Y:F

    .line 83
    const v0, 0x7f0a03d5

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_PORTRAIT_WIDTH:F

    .line 84
    const v0, 0x7f0a03d6

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_PORTRAIT_HEIGHT:F

    .line 85
    const v0, 0x7f0a03d9

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_PORTRAIT_X:F

    .line 86
    const v0, 0x7f0a03da

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_PORTRAIT_Y:F

    .line 88
    sget v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_LANDSCAPE_HEIGHT:F

    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->SCREEN_RATIO:F

    mul-float/2addr v0, v3

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_SINGLE_WIDTH:F

    .line 89
    sget v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_LANDSCAPE_HEIGHT:F

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_SINGLE_HEIGHT:F

    .line 90
    sget v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_LANDSCAPE_WIDTH:F

    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_SINGLE_WIDTH:F

    sub-float/2addr v0, v3

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v0, v3

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_SINGLE_X:F

    .line 92
    sget v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_PORTRAIT_WIDTH:F

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_SINGLE_PORTRAIT_WIDTH:F

    .line 93
    sget v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_PORTRAIT_WIDTH:F

    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->SCREEN_RATIO:F

    div-float/2addr v0, v3

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_SINGLE_PORTRAIT_HEIGHT:F

    .line 95
    sget v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_PORTRAIT_HEIGHT:F

    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_SINGLE_PORTRAIT_HEIGHT:F

    sub-float/2addr v0, v3

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v0, v3

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_SINGLE_PORTRAIT_Y:F

    .line 96
    const v0, 0x7f0a03db

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_RECT_THICKNESS:F

    .line 97
    sget v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->SCREEN_WIDTH:I

    int-to-float v0, v0

    const/high16 v3, 0x44f00000    # 1920.0f

    div-float/2addr v0, v3

    const v3, 0x3fb6a7f0    # 1.427f

    mul-float/2addr v0, v3

    const v3, 0x3f9d70a4    # 1.23f

    mul-float/2addr v0, v3

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_RATIO:F

    .line 99
    const v0, 0x7f0a03fd

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->NEXT_CAPTURE_FOCUS_LINE_WIDTH:F

    .line 100
    sget v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_PORTRAIT_X:F

    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->NEXT_CAPTURE_FOCUS_LINE_WIDTH:F

    sub-float/2addr v0, v3

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->NEXT_CAPTURE_FOCUS_PORTRAIT_X:F

    .line 102
    new-array v0, v4, [F

    const v3, 0x7f0a03e7

    invoke-static {v3}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v3

    aput v3, v0, v2

    const v3, 0x7f0a03e9

    invoke-static {v3}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v3

    aput v3, v0, v1

    sput-object v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->BACKGROUND_RECT_PORTRAIT_X:[F

    .line 103
    new-array v0, v4, [F

    const v3, 0x7f0a03e8

    invoke-static {v3}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v3

    aput v3, v0, v2

    const v3, 0x7f0a03ea

    invoke-static {v3}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v3

    aput v3, v0, v1

    sput-object v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->BACKGROUND_RECT_PORTRAIT_Y:[F

    .line 105
    sget v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_LANDSCAPE_X:F

    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_SINGLE_X:F

    add-float/2addr v0, v3

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->FOCUS_RECT_LANDSCAPE_X:F

    .line 106
    sget v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_LANDSCAPE_Y:F

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->FOCUS_RECT_LANDSCAPE_Y:F

    .line 107
    sget v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_PORTRAIT_X:F

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->FOCUS_RECT_PORTRAIT_X:F

    .line 108
    sget v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_PORTRAIT_Y:F

    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_SINGLE_PORTRAIT_Y:F

    add-float/2addr v0, v3

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->FOCUS_RECT_PORTRAIT_Y:F

    .line 110
    const v0, 0x7f0a03eb

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->PREVIEW_TOP_MARGIN:F

    .line 111
    const v0, 0x7f0a03ec

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->PREVIEW_LEFT_MARGIN:F

    .line 112
    const v0, 0x7f0a03ed

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->PREVIEW_PORTRAIT_MARGIN:F

    .line 113
    const v0, 0x7f0a03ee

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->PREVIEW_EASYMODE_PORTRAIT_MARGIN:F

    .line 114
    const v0, 0x7f0a03ef

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->PREVIEW_EASYMODE_GUIDE_PORTRAIT_MARGIN:F

    .line 116
    const v0, 0x7f0a03f0

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->ANIMATION_TOP_MARGIN_TOP:F

    .line 117
    const v0, 0x7f0a03f1

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->ANIMATION_TOP_MARGIN_LEFT:F

    .line 118
    new-array v0, v6, [F

    const v3, 0x7f0a03fe

    invoke-static {v3}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v3

    aput v3, v0, v2

    const v3, 0x7f0a03ff

    invoke-static {v3}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v3

    aput v3, v0, v1

    const v3, 0x7f0a0400

    invoke-static {v3}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v3

    aput v3, v0, v4

    const v3, 0x7f0a0401

    invoke-static {v3}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v3

    aput v3, v0, v5

    sput-object v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->DIRECTION_INDICATOR_POS_X:[F

    .line 120
    new-array v0, v6, [F

    const v3, 0x7f0a0402

    invoke-static {v3}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v3

    aput v3, v0, v2

    const v3, 0x7f0a0403

    invoke-static {v3}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v3

    aput v3, v0, v1

    const v3, 0x7f0a0404

    invoke-static {v3}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v3

    aput v3, v0, v4

    const v3, 0x7f0a0405

    invoke-static {v3}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v3

    aput v3, v0, v5

    sput-object v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->DIRECTION_INDICATOR_POS_Y:[F

    .line 123
    const v0, 0x7f0a03fb

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->CAPTURE_ANGLE_BUTTON_POS_Y:F

    .line 124
    const v0, 0x7f0a03fa

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->CAPTURE_ANGLE_MARGIN:F

    .line 125
    const v0, 0x7f0a03fc

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->CAPTURE_ANGLE_BUTTON_PORT_MARGIN:F

    .line 165
    const/4 v0, 0x5

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->PORTRATE_CAPTURE_COUNT:I

    .line 166
    sput v5, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LANDSCAPE_CAPTURE_COUNT:I

    .line 168
    sput v2, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->WARNING_LEVEL_NONE:I

    .line 169
    sput v1, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->WARNING_LEVEL_LOW:I

    .line 170
    sput v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->WARNING_LEVEL_HIGH:I

    .line 171
    sput v5, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->WARNING_LEVEL_STOP:I

    .line 173
    sput v2, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->WARNING_UP:I

    .line 174
    sput v1, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->WARNING_DOWN:I

    .line 175
    sput v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->WARNING_LEFT:I

    .line 176
    sput v5, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->WARNING_RIGHT:I

    .line 177
    sput v6, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->WARNING_NONE:I

    .line 179
    const v0, 0x7f0b0040

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getInteger(I)I

    move-result v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->OFFSET_DISTANCE_VERTICAL:I

    .line 180
    const v0, 0x7f0b0041

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getInteger(I)I

    move-result v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->OFFSET_DISTANCE_HORIZONTAL:I

    .line 192
    const v0, 0x7f0a0022

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->STOP_BUTTON_POS_X:I

    .line 193
    const v0, 0x7f0a0023

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->STOP_BUTTON_POS_Y:I

    return-void

    :cond_0
    move v0, v2

    .line 71
    goto/16 :goto_0
.end method

.method public constructor <init>(Lcom/sec/android/app/camera/Camera;ILcom/sec/android/glview/TwGLViewGroup;Lcom/sec/android/app/camera/MenuResourceDepot;)V
    .locals 9
    .param p1, "activityContext"    # Lcom/sec/android/app/camera/Camera;
    .param p2, "viewId"    # I
    .param p3, "glParentView"    # Lcom/sec/android/glview/TwGLViewGroup;
    .param p4, "menuResourceDepot"    # Lcom/sec/android/app/camera/MenuResourceDepot;

    .prologue
    .line 361
    const/4 v5, 0x6

    const/4 v6, 0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/camera/MenuBase;-><init>(Lcom/sec/android/app/camera/Camera;ILcom/sec/android/glview/TwGLViewGroup;Lcom/sec/android/app/camera/MenuResourceDepot;IZ)V

    .line 56
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureCount:I

    .line 57
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mSkipFrame:I

    .line 128
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mIsStartCapture:Z

    .line 129
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mSkipCapture:Z

    .line 130
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureError:Z

    .line 131
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mSkipCheckWarning:Z

    .line 133
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mUltraWideShotWarning:Z

    .line 134
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPlayHaptic:Z

    .line 149
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPrevFocusRectCenterX:F

    .line 150
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPrevFocusRectCenterY:F

    .line 152
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLeftTopX:F

    .line 153
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLeftTopY:F

    .line 154
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mRightTopX:F

    .line 155
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mRightTopY:F

    .line 156
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mRightBottomX:F

    .line 157
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mRightBottomY:F

    .line 158
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLeftBottomX:F

    .line 159
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLeftBottomY:F

    .line 160
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWidth:F

    .line 161
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mHeight:F

    .line 163
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mStartCheckWarning:Z

    .line 182
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sec/android/glview/TwGLImage;

    iput-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningArrow:[Lcom/sec/android/glview/TwGLImage;

    .line 183
    const/16 v0, 0xe

    new-array v0, v0, [Lcom/sec/android/glview/TwGLImage;

    iput-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideArrow:[Lcom/sec/android/glview/TwGLImage;

    .line 184
    const/16 v0, 0xe

    new-array v0, v0, [Lcom/sec/android/glview/TwGLImage;

    iput-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideRect:[Lcom/sec/android/glview/TwGLImage;

    .line 185
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningBox:Lcom/sec/android/glview/TwGLImage;

    .line 186
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mAnimationDirection:Z

    .line 189
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mChanged:Z

    .line 209
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/android/glview/TwGLImage;

    iput-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLeftGuideImage:[Lcom/sec/android/glview/TwGLImage;

    .line 210
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/android/glview/TwGLImage;

    iput-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCenterGuideImage:[Lcom/sec/android/glview/TwGLImage;

    .line 211
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/android/glview/TwGLImage;

    iput-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mRightGuideImage:[Lcom/sec/android/glview/TwGLImage;

    .line 212
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideBlinkingAnimation:Landroid/view/animation/Animation;

    .line 213
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideShow:Z

    .line 220
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideOrient:I

    .line 221
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideDirection:I

    .line 227
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideState:I

    .line 235
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mBlinkAnimation:Landroid/view/animation/AlphaAnimation;

    .line 236
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mTwoBlinkAnimation:Landroid/view/animation/AlphaAnimation;

    .line 240
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLivePreviewGroup:Lcom/sec/android/glview/TwGLViewGroup;

    .line 241
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPreviewFocusRect:Lcom/sec/android/glview/TwGLNinePatch;

    .line 242
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mBackgroundRectGroup:Lcom/sec/android/glview/TwGLViewGroup;

    .line 243
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLivePreview:Lcom/sec/android/glview/TwGLBitmapTexture;

    .line 244
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mBackgroundRect:Lcom/sec/android/glview/TwGLNinePatch;

    .line 246
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureFocusRect:Lcom/sec/android/glview/TwGLNinePatch;

    .line 247
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mTrapezoidCaptureFocusRect:Lcom/sec/android/app/camera/glwidget/TwGLTrapezoid;

    .line 248
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mNextCaptureFocusRect:Lcom/sec/android/glview/TwGLImage;

    .line 249
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mNextCaptureFocusRectPort:Lcom/sec/android/glview/TwGLImage;

    .line 253
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mNextCaptureFocusRectCenterX:F

    .line 254
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mNextCaptureFocusRectCenterY:F

    .line 256
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mNextCaptureFocusStep:I

    .line 257
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mDetectedDirection:I

    .line 258
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCurrentDirection:I

    .line 262
    const/16 v0, 0x78

    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureAngle:I

    .line 265
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mEncodingProgress:Z

    .line 267
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mMovingDetection:Z

    .line 269
    new-instance v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu$1;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu$1;-><init>(Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mUltraWideShotMsgHandler:Landroid/os/Handler;

    .line 363
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->setCaptureEnabled(Z)V

    .line 364
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->setTouchHandled(Z)V

    .line 366
    invoke-static {}, Lcom/sec/android/glview/TwGLContext;->getLastOrientation()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCurrentOrientation:I

    .line 368
    new-instance v0, Lcom/sec/android/glview/TwGLViewGroup;

    invoke-virtual {p1}, Lcom/sec/android/app/camera/Camera;->getGLContext()Lcom/sec/android/glview/TwGLContext;

    move-result-object v1

    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_LANDSCAPE_X:F

    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_LANDSCAPE_Y:F

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_LANDSCAPE_WIDTH:F

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_LANDSCAPE_HEIGHT:F

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/glview/TwGLViewGroup;-><init>(Lcom/sec/android/glview/TwGLContext;FFFF)V

    iput-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLivePreviewGroup:Lcom/sec/android/glview/TwGLViewGroup;

    .line 369
    new-instance v0, Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {p1}, Lcom/sec/android/app/camera/Camera;->getGLContext()Lcom/sec/android/glview/TwGLContext;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const v4, 0x7f020200

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/glview/TwGLNinePatch;-><init>(Lcom/sec/android/glview/TwGLContext;FFI)V

    iput-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPreviewFocusRect:Lcom/sec/android/glview/TwGLNinePatch;

    .line 370
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLivePreviewGroup:Lcom/sec/android/glview/TwGLViewGroup;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLViewGroup;->setVisibility(I)V

    .line 372
    new-instance v0, Lcom/sec/android/glview/TwGLViewGroup;

    invoke-virtual {p1}, Lcom/sec/android/app/camera/Camera;->getGLContext()Lcom/sec/android/glview/TwGLContext;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_LANDSCAPE_WIDTH:F

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_LANDSCAPE_HEIGHT:F

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/glview/TwGLViewGroup;-><init>(Lcom/sec/android/glview/TwGLContext;FFFF)V

    iput-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mBackgroundRectGroup:Lcom/sec/android/glview/TwGLViewGroup;

    .line 373
    new-instance v0, Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {p1}, Lcom/sec/android/app/camera/Camera;->getGLContext()Lcom/sec/android/glview/TwGLContext;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const v4, 0x7f0201fe

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/glview/TwGLNinePatch;-><init>(Lcom/sec/android/glview/TwGLContext;FFI)V

    iput-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mBackgroundRect:Lcom/sec/android/glview/TwGLNinePatch;

    .line 374
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mBackgroundRectGroup:Lcom/sec/android/glview/TwGLViewGroup;

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mBackgroundRect:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLViewGroup;->addView(Lcom/sec/android/glview/TwGLView;)V

    .line 375
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mBackgroundRectGroup:Lcom/sec/android/glview/TwGLViewGroup;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLViewGroup;->setCenterPivot(Z)V

    .line 376
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mBackgroundRectGroup:Lcom/sec/android/glview/TwGLViewGroup;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLViewGroup;->setRotatable(Z)V

    .line 377
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mBackgroundRectGroup:Lcom/sec/android/glview/TwGLViewGroup;

    invoke-virtual {v0, p0}, Lcom/sec/android/glview/TwGLViewGroup;->setOnOrientationChangedListener(Lcom/sec/android/glview/TwGLView$OnOrientationChangedListener;)V

    .line 378
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mBackgroundRectGroup:Lcom/sec/android/glview/TwGLViewGroup;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLViewGroup;->setVisibility(I)V

    .line 380
    new-instance v0, Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {p1}, Lcom/sec/android/app/camera/Camera;->getGLContext()Lcom/sec/android/glview/TwGLContext;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_SINGLE_WIDTH:F

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_SINGLE_HEIGHT:F

    const v6, 0x7f020200

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/glview/TwGLNinePatch;-><init>(Lcom/sec/android/glview/TwGLContext;FFFFI)V

    iput-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureFocusRect:Lcom/sec/android/glview/TwGLNinePatch;

    .line 381
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureFocusRect:Lcom/sec/android/glview/TwGLNinePatch;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLNinePatch;->setVisibility(I)V

    .line 382
    new-instance v0, Lcom/sec/android/app/camera/glwidget/TwGLTrapezoid;

    invoke-virtual {p1}, Lcom/sec/android/app/camera/Camera;->getGLContext()Lcom/sec/android/glview/TwGLContext;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_SINGLE_WIDTH:F

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_SINGLE_HEIGHT:F

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camera/glwidget/TwGLTrapezoid;-><init>(Lcom/sec/android/glview/TwGLContext;FFFF)V

    iput-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mTrapezoidCaptureFocusRect:Lcom/sec/android/app/camera/glwidget/TwGLTrapezoid;

    .line 383
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mTrapezoidCaptureFocusRect:Lcom/sec/android/app/camera/glwidget/TwGLTrapezoid;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/sec/android/app/camera/glwidget/TwGLTrapezoid;->setVisibility(I)V

    .line 385
    new-instance v0, Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {p1}, Lcom/sec/android/app/camera/Camera;->getGLContext()Lcom/sec/android/glview/TwGLContext;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const v4, 0x7f0203ca

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/glview/TwGLImage;-><init>(Lcom/sec/android/glview/TwGLContext;FFI)V

    iput-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mNextCaptureFocusRect:Lcom/sec/android/glview/TwGLImage;

    .line 386
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mNextCaptureFocusRect:Lcom/sec/android/glview/TwGLImage;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 387
    new-instance v0, Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {p1}, Lcom/sec/android/app/camera/Camera;->getGLContext()Lcom/sec/android/glview/TwGLContext;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const v4, 0x7f0203cb

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/glview/TwGLImage;-><init>(Lcom/sec/android/glview/TwGLContext;FFI)V

    iput-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mNextCaptureFocusRectPort:Lcom/sec/android/glview/TwGLImage;

    .line 388
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mNextCaptureFocusRectPort:Lcom/sec/android/glview/TwGLImage;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 389
    new-instance v0, Lcom/sec/android/glview/TwGLText;

    invoke-virtual {p1}, Lcom/sec/android/app/camera/Camera;->getGLContext()Lcom/sec/android/glview/TwGLContext;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->GUIDE_TEXT_WIDTH:F

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->GUIDE_TEXT_HEIGHT:F

    const-string v6, ""

    sget v7, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->GUIDE_TEXT_SIZE:F

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/glview/TwGLText;-><init>(Lcom/sec/android/glview/TwGLContext;FFFFLjava/lang/String;F)V

    iput-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideText:Lcom/sec/android/glview/TwGLText;

    .line 390
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideText:Lcom/sec/android/glview/TwGLText;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLText;->setRotatable(Z)V

    .line 391
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideText:Lcom/sec/android/glview/TwGLText;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLText;->setCenterPivot(Z)V

    .line 392
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideText:Lcom/sec/android/glview/TwGLText;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLText;->setVisibility(I)V

    .line 393
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideText:Lcom/sec/android/glview/TwGLText;

    invoke-virtual {p3, v0}, Lcom/sec/android/glview/TwGLViewGroup;->addView(Lcom/sec/android/glview/TwGLView;)V

    .line 395
    new-instance v0, Lcom/sec/android/glview/TwGLText;

    invoke-virtual {p1}, Lcom/sec/android/app/camera/Camera;->getGLContext()Lcom/sec/android/glview/TwGLContext;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->GUIDE_TEXT_WIDTH:F

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->GUIDE_TEXT_HEIGHT:F

    const-string v6, ""

    sget v7, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->GUIDE_TEXT_SIZE:F

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/glview/TwGLText;-><init>(Lcom/sec/android/glview/TwGLContext;FFFFLjava/lang/String;F)V

    iput-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningText:Lcom/sec/android/glview/TwGLText;

    .line 396
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningText:Lcom/sec/android/glview/TwGLText;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLText;->setRotatable(Z)V

    .line 397
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningText:Lcom/sec/android/glview/TwGLText;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLText;->setCenterPivot(Z)V

    .line 398
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningText:Lcom/sec/android/glview/TwGLText;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLText;->setVisibility(I)V

    .line 399
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningText:Lcom/sec/android/glview/TwGLText;

    invoke-virtual {p3, v0}, Lcom/sec/android/glview/TwGLViewGroup;->addView(Lcom/sec/android/glview/TwGLView;)V

    .line 401
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningArrow:[Lcom/sec/android/glview/TwGLImage;

    const/4 v1, 0x0

    new-instance v2, Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {p1}, Lcom/sec/android/app/camera/Camera;->getGLContext()Lcom/sec/android/glview/TwGLContext;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const v6, 0x7f020540

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/sec/android/glview/TwGLImage;-><init>(Lcom/sec/android/glview/TwGLContext;FFI)V

    aput-object v2, v0, v1

    .line 402
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningArrow:[Lcom/sec/android/glview/TwGLImage;

    const/4 v1, 0x1

    new-instance v2, Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {p1}, Lcom/sec/android/app/camera/Camera;->getGLContext()Lcom/sec/android/glview/TwGLContext;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const v6, 0x7f02053d

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/sec/android/glview/TwGLImage;-><init>(Lcom/sec/android/glview/TwGLContext;FFI)V

    aput-object v2, v0, v1

    .line 403
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningArrow:[Lcom/sec/android/glview/TwGLImage;

    const/4 v1, 0x2

    new-instance v2, Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {p1}, Lcom/sec/android/app/camera/Camera;->getGLContext()Lcom/sec/android/glview/TwGLContext;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const v6, 0x7f02053e

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/sec/android/glview/TwGLImage;-><init>(Lcom/sec/android/glview/TwGLContext;FFI)V

    aput-object v2, v0, v1

    .line 404
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningArrow:[Lcom/sec/android/glview/TwGLImage;

    const/4 v1, 0x3

    new-instance v2, Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {p1}, Lcom/sec/android/app/camera/Camera;->getGLContext()Lcom/sec/android/glview/TwGLContext;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const v6, 0x7f02053f

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/sec/android/glview/TwGLImage;-><init>(Lcom/sec/android/glview/TwGLContext;FFI)V

    aput-object v2, v0, v1

    .line 405
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideArrow:[Lcom/sec/android/glview/TwGLImage;

    const/4 v1, 0x0

    new-instance v2, Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {p1}, Lcom/sec/android/app/camera/Camera;->getGLContext()Lcom/sec/android/glview/TwGLContext;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const v6, 0x7f0203c0

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/sec/android/glview/TwGLImage;-><init>(Lcom/sec/android/glview/TwGLContext;FFI)V

    aput-object v2, v0, v1

    .line 406
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideArrow:[Lcom/sec/android/glview/TwGLImage;

    const/4 v1, 0x1

    new-instance v2, Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {p1}, Lcom/sec/android/app/camera/Camera;->getGLContext()Lcom/sec/android/glview/TwGLContext;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const v6, 0x7f0203be

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/sec/android/glview/TwGLImage;-><init>(Lcom/sec/android/glview/TwGLContext;FFI)V

    aput-object v2, v0, v1

    .line 407
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideArrow:[Lcom/sec/android/glview/TwGLImage;

    const/4 v1, 0x2

    new-instance v2, Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {p1}, Lcom/sec/android/app/camera/Camera;->getGLContext()Lcom/sec/android/glview/TwGLContext;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const v6, 0x7f0203bc

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/sec/android/glview/TwGLImage;-><init>(Lcom/sec/android/glview/TwGLContext;FFI)V

    aput-object v2, v0, v1

    .line 408
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideArrow:[Lcom/sec/android/glview/TwGLImage;

    const/4 v1, 0x3

    new-instance v2, Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {p1}, Lcom/sec/android/app/camera/Camera;->getGLContext()Lcom/sec/android/glview/TwGLContext;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const v6, 0x7f0203cc

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/sec/android/glview/TwGLImage;-><init>(Lcom/sec/android/glview/TwGLContext;FFI)V

    aput-object v2, v0, v1

    .line 409
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideArrow:[Lcom/sec/android/glview/TwGLImage;

    const/4 v1, 0x4

    new-instance v2, Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {p1}, Lcom/sec/android/app/camera/Camera;->getGLContext()Lcom/sec/android/glview/TwGLContext;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const v6, 0x7f0203c3

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/sec/android/glview/TwGLImage;-><init>(Lcom/sec/android/glview/TwGLContext;FFI)V

    aput-object v2, v0, v1

    .line 410
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideArrow:[Lcom/sec/android/glview/TwGLImage;

    const/4 v1, 0x5

    new-instance v2, Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {p1}, Lcom/sec/android/app/camera/Camera;->getGLContext()Lcom/sec/android/glview/TwGLContext;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const v6, 0x7f0203c5

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/sec/android/glview/TwGLImage;-><init>(Lcom/sec/android/glview/TwGLContext;FFI)V

    aput-object v2, v0, v1

    .line 411
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideArrow:[Lcom/sec/android/glview/TwGLImage;

    const/4 v1, 0x6

    new-instance v2, Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {p1}, Lcom/sec/android/app/camera/Camera;->getGLContext()Lcom/sec/android/glview/TwGLContext;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const v6, 0x7f0203c7

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/sec/android/glview/TwGLImage;-><init>(Lcom/sec/android/glview/TwGLContext;FFI)V

    aput-object v2, v0, v1

    .line 412
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideArrow:[Lcom/sec/android/glview/TwGLImage;

    const/4 v1, 0x7

    new-instance v2, Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {p1}, Lcom/sec/android/app/camera/Camera;->getGLContext()Lcom/sec/android/glview/TwGLContext;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const v6, 0x7f0203c1

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/sec/android/glview/TwGLImage;-><init>(Lcom/sec/android/glview/TwGLContext;FFI)V

    aput-object v2, v0, v1

    .line 413
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideArrow:[Lcom/sec/android/glview/TwGLImage;

    const/16 v1, 0x8

    new-instance v2, Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {p1}, Lcom/sec/android/app/camera/Camera;->getGLContext()Lcom/sec/android/glview/TwGLContext;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const v6, 0x7f0203bf

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/sec/android/glview/TwGLImage;-><init>(Lcom/sec/android/glview/TwGLContext;FFI)V

    aput-object v2, v0, v1

    .line 414
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideArrow:[Lcom/sec/android/glview/TwGLImage;

    const/16 v1, 0x9

    new-instance v2, Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {p1}, Lcom/sec/android/app/camera/Camera;->getGLContext()Lcom/sec/android/glview/TwGLContext;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const v6, 0x7f0203bd

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/sec/android/glview/TwGLImage;-><init>(Lcom/sec/android/glview/TwGLContext;FFI)V

    aput-object v2, v0, v1

    .line 415
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideArrow:[Lcom/sec/android/glview/TwGLImage;

    const/16 v1, 0xa

    new-instance v2, Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {p1}, Lcom/sec/android/app/camera/Camera;->getGLContext()Lcom/sec/android/glview/TwGLContext;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const v6, 0x7f0203cd

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/sec/android/glview/TwGLImage;-><init>(Lcom/sec/android/glview/TwGLContext;FFI)V

    aput-object v2, v0, v1

    .line 416
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideArrow:[Lcom/sec/android/glview/TwGLImage;

    const/16 v1, 0xb

    new-instance v2, Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {p1}, Lcom/sec/android/app/camera/Camera;->getGLContext()Lcom/sec/android/glview/TwGLContext;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const v6, 0x7f0203c4

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/sec/android/glview/TwGLImage;-><init>(Lcom/sec/android/glview/TwGLContext;FFI)V

    aput-object v2, v0, v1

    .line 417
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideArrow:[Lcom/sec/android/glview/TwGLImage;

    const/16 v1, 0xc

    new-instance v2, Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {p1}, Lcom/sec/android/app/camera/Camera;->getGLContext()Lcom/sec/android/glview/TwGLContext;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const v6, 0x7f0203c6

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/sec/android/glview/TwGLImage;-><init>(Lcom/sec/android/glview/TwGLContext;FFI)V

    aput-object v2, v0, v1

    .line 418
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideArrow:[Lcom/sec/android/glview/TwGLImage;

    const/16 v1, 0xd

    new-instance v2, Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {p1}, Lcom/sec/android/app/camera/Camera;->getGLContext()Lcom/sec/android/glview/TwGLContext;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const v6, 0x7f0203c8

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/sec/android/glview/TwGLImage;-><init>(Lcom/sec/android/glview/TwGLContext;FFI)V

    aput-object v2, v0, v1

    .line 419
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideRect:[Lcom/sec/android/glview/TwGLImage;

    const/4 v1, 0x0

    new-instance v2, Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {p1}, Lcom/sec/android/app/camera/Camera;->getGLContext()Lcom/sec/android/glview/TwGLContext;

    move-result-object v3

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->FOCUS_RECT_LANDSCAPE_X:F

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->ANIMATION_TOP_MARGIN_LEFT:F

    sub-float/2addr v4, v5

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->FOCUS_RECT_LANDSCAPE_Y:F

    sget v6, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->ANIMATION_TOP_MARGIN_TOP:F

    sub-float/2addr v5, v6

    const v6, 0x7f0200d8

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/sec/android/glview/TwGLImage;-><init>(Lcom/sec/android/glview/TwGLContext;FFI)V

    aput-object v2, v0, v1

    .line 420
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideRect:[Lcom/sec/android/glview/TwGLImage;

    const/4 v1, 0x1

    new-instance v2, Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {p1}, Lcom/sec/android/app/camera/Camera;->getGLContext()Lcom/sec/android/glview/TwGLContext;

    move-result-object v3

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->FOCUS_RECT_LANDSCAPE_X:F

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->ANIMATION_TOP_MARGIN_LEFT:F

    sub-float/2addr v4, v5

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->FOCUS_RECT_LANDSCAPE_Y:F

    sget v6, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->ANIMATION_TOP_MARGIN_TOP:F

    sub-float/2addr v5, v6

    const v6, 0x7f0200d6

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/sec/android/glview/TwGLImage;-><init>(Lcom/sec/android/glview/TwGLContext;FFI)V

    aput-object v2, v0, v1

    .line 421
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideRect:[Lcom/sec/android/glview/TwGLImage;

    const/4 v1, 0x2

    new-instance v2, Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {p1}, Lcom/sec/android/app/camera/Camera;->getGLContext()Lcom/sec/android/glview/TwGLContext;

    move-result-object v3

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->FOCUS_RECT_LANDSCAPE_X:F

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->ANIMATION_TOP_MARGIN_LEFT:F

    sub-float/2addr v4, v5

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->FOCUS_RECT_LANDSCAPE_Y:F

    sget v6, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->ANIMATION_TOP_MARGIN_TOP:F

    sub-float/2addr v5, v6

    const v6, 0x7f0200d4

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/sec/android/glview/TwGLImage;-><init>(Lcom/sec/android/glview/TwGLContext;FFI)V

    aput-object v2, v0, v1

    .line 422
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideRect:[Lcom/sec/android/glview/TwGLImage;

    const/4 v1, 0x3

    new-instance v2, Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {p1}, Lcom/sec/android/app/camera/Camera;->getGLContext()Lcom/sec/android/glview/TwGLContext;

    move-result-object v3

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->FOCUS_RECT_LANDSCAPE_X:F

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->ANIMATION_TOP_MARGIN_LEFT:F

    sub-float/2addr v4, v5

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->FOCUS_RECT_LANDSCAPE_Y:F

    sget v6, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->ANIMATION_TOP_MARGIN_TOP:F

    sub-float/2addr v5, v6

    const v6, 0x7f0200d2

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/sec/android/glview/TwGLImage;-><init>(Lcom/sec/android/glview/TwGLContext;FFI)V

    aput-object v2, v0, v1

    .line 423
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideRect:[Lcom/sec/android/glview/TwGLImage;

    const/4 v1, 0x4

    new-instance v2, Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {p1}, Lcom/sec/android/app/camera/Camera;->getGLContext()Lcom/sec/android/glview/TwGLContext;

    move-result-object v3

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->FOCUS_RECT_LANDSCAPE_X:F

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->ANIMATION_TOP_MARGIN_LEFT:F

    sub-float/2addr v4, v5

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->FOCUS_RECT_LANDSCAPE_Y:F

    sget v6, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->ANIMATION_TOP_MARGIN_TOP:F

    sub-float/2addr v5, v6

    const v6, 0x7f0200da

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/sec/android/glview/TwGLImage;-><init>(Lcom/sec/android/glview/TwGLContext;FFI)V

    aput-object v2, v0, v1

    .line 424
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideRect:[Lcom/sec/android/glview/TwGLImage;

    const/4 v1, 0x5

    new-instance v2, Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {p1}, Lcom/sec/android/app/camera/Camera;->getGLContext()Lcom/sec/android/glview/TwGLContext;

    move-result-object v3

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->FOCUS_RECT_LANDSCAPE_X:F

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->ANIMATION_TOP_MARGIN_LEFT:F

    sub-float/2addr v4, v5

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->FOCUS_RECT_LANDSCAPE_Y:F

    sget v6, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->ANIMATION_TOP_MARGIN_TOP:F

    sub-float/2addr v5, v6

    const v6, 0x7f0200dc

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/sec/android/glview/TwGLImage;-><init>(Lcom/sec/android/glview/TwGLContext;FFI)V

    aput-object v2, v0, v1

    .line 425
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideRect:[Lcom/sec/android/glview/TwGLImage;

    const/4 v1, 0x6

    new-instance v2, Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {p1}, Lcom/sec/android/app/camera/Camera;->getGLContext()Lcom/sec/android/glview/TwGLContext;

    move-result-object v3

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->FOCUS_RECT_LANDSCAPE_X:F

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->ANIMATION_TOP_MARGIN_LEFT:F

    sub-float/2addr v4, v5

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->FOCUS_RECT_LANDSCAPE_Y:F

    sget v6, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->ANIMATION_TOP_MARGIN_TOP:F

    sub-float/2addr v5, v6

    const v6, 0x7f0200de

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/sec/android/glview/TwGLImage;-><init>(Lcom/sec/android/glview/TwGLContext;FFI)V

    aput-object v2, v0, v1

    .line 433
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideRect:[Lcom/sec/android/glview/TwGLImage;

    const/4 v1, 0x7

    new-instance v2, Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {p1}, Lcom/sec/android/app/camera/Camera;->getGLContext()Lcom/sec/android/glview/TwGLContext;

    move-result-object v3

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->FOCUS_RECT_PORTRAIT_X:F

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->ANIMATION_TOP_MARGIN_LEFT:F

    sub-float/2addr v4, v5

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_PORTRAIT_Y:F

    const v6, 0x7f0a03d6

    invoke-static {v6}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v6

    sget v7, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_SINGLE_PORTRAIT_HEIGHT:F

    sub-float/2addr v6, v7

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    add-float/2addr v5, v6

    sget v6, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->ANIMATION_TOP_MARGIN_TOP:F

    sub-float/2addr v5, v6

    const v6, 0x7f0200d9

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/sec/android/glview/TwGLImage;-><init>(Lcom/sec/android/glview/TwGLContext;FFI)V

    aput-object v2, v0, v1

    .line 434
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideRect:[Lcom/sec/android/glview/TwGLImage;

    const/16 v1, 0x8

    new-instance v2, Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {p1}, Lcom/sec/android/app/camera/Camera;->getGLContext()Lcom/sec/android/glview/TwGLContext;

    move-result-object v3

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->FOCUS_RECT_PORTRAIT_X:F

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->ANIMATION_TOP_MARGIN_LEFT:F

    sub-float/2addr v4, v5

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_PORTRAIT_Y:F

    const v6, 0x7f0a03d6

    invoke-static {v6}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v6

    sget v7, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_SINGLE_PORTRAIT_HEIGHT:F

    sub-float/2addr v6, v7

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    add-float/2addr v5, v6

    sget v6, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->ANIMATION_TOP_MARGIN_TOP:F

    sub-float/2addr v5, v6

    const v6, 0x7f0200d7

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/sec/android/glview/TwGLImage;-><init>(Lcom/sec/android/glview/TwGLContext;FFI)V

    aput-object v2, v0, v1

    .line 435
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideRect:[Lcom/sec/android/glview/TwGLImage;

    const/16 v1, 0x9

    new-instance v2, Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {p1}, Lcom/sec/android/app/camera/Camera;->getGLContext()Lcom/sec/android/glview/TwGLContext;

    move-result-object v3

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->FOCUS_RECT_PORTRAIT_X:F

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->ANIMATION_TOP_MARGIN_LEFT:F

    sub-float/2addr v4, v5

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_PORTRAIT_Y:F

    const v6, 0x7f0a03d6

    invoke-static {v6}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v6

    sget v7, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_SINGLE_PORTRAIT_HEIGHT:F

    sub-float/2addr v6, v7

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    add-float/2addr v5, v6

    sget v6, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->ANIMATION_TOP_MARGIN_TOP:F

    sub-float/2addr v5, v6

    const v6, 0x7f0200d5

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/sec/android/glview/TwGLImage;-><init>(Lcom/sec/android/glview/TwGLContext;FFI)V

    aput-object v2, v0, v1

    .line 436
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideRect:[Lcom/sec/android/glview/TwGLImage;

    const/16 v1, 0xa

    new-instance v2, Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {p1}, Lcom/sec/android/app/camera/Camera;->getGLContext()Lcom/sec/android/glview/TwGLContext;

    move-result-object v3

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->FOCUS_RECT_PORTRAIT_X:F

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->ANIMATION_TOP_MARGIN_LEFT:F

    sub-float/2addr v4, v5

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_PORTRAIT_Y:F

    const v6, 0x7f0a03d6

    invoke-static {v6}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v6

    sget v7, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_SINGLE_PORTRAIT_HEIGHT:F

    sub-float/2addr v6, v7

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    add-float/2addr v5, v6

    sget v6, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->ANIMATION_TOP_MARGIN_TOP:F

    sub-float/2addr v5, v6

    const v6, 0x7f0200d3

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/sec/android/glview/TwGLImage;-><init>(Lcom/sec/android/glview/TwGLContext;FFI)V

    aput-object v2, v0, v1

    .line 437
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideRect:[Lcom/sec/android/glview/TwGLImage;

    const/16 v1, 0xb

    new-instance v2, Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {p1}, Lcom/sec/android/app/camera/Camera;->getGLContext()Lcom/sec/android/glview/TwGLContext;

    move-result-object v3

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->FOCUS_RECT_PORTRAIT_X:F

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->ANIMATION_TOP_MARGIN_LEFT:F

    sub-float/2addr v4, v5

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_PORTRAIT_Y:F

    const v6, 0x7f0a03d6

    invoke-static {v6}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v6

    sget v7, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_SINGLE_PORTRAIT_HEIGHT:F

    sub-float/2addr v6, v7

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    add-float/2addr v5, v6

    sget v6, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->ANIMATION_TOP_MARGIN_TOP:F

    sub-float/2addr v5, v6

    const v6, 0x7f0200db

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/sec/android/glview/TwGLImage;-><init>(Lcom/sec/android/glview/TwGLContext;FFI)V

    aput-object v2, v0, v1

    .line 438
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideRect:[Lcom/sec/android/glview/TwGLImage;

    const/16 v1, 0xc

    new-instance v2, Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {p1}, Lcom/sec/android/app/camera/Camera;->getGLContext()Lcom/sec/android/glview/TwGLContext;

    move-result-object v3

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->FOCUS_RECT_PORTRAIT_X:F

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->ANIMATION_TOP_MARGIN_LEFT:F

    sub-float/2addr v4, v5

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_PORTRAIT_Y:F

    const v6, 0x7f0a03d6

    invoke-static {v6}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v6

    sget v7, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_SINGLE_PORTRAIT_HEIGHT:F

    sub-float/2addr v6, v7

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    add-float/2addr v5, v6

    sget v6, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->ANIMATION_TOP_MARGIN_TOP:F

    sub-float/2addr v5, v6

    const v6, 0x7f0200dd

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/sec/android/glview/TwGLImage;-><init>(Lcom/sec/android/glview/TwGLContext;FFI)V

    aput-object v2, v0, v1

    .line 439
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideRect:[Lcom/sec/android/glview/TwGLImage;

    const/16 v1, 0xd

    new-instance v2, Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {p1}, Lcom/sec/android/app/camera/Camera;->getGLContext()Lcom/sec/android/glview/TwGLContext;

    move-result-object v3

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->FOCUS_RECT_PORTRAIT_X:F

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->ANIMATION_TOP_MARGIN_LEFT:F

    sub-float/2addr v4, v5

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_PORTRAIT_Y:F

    const v6, 0x7f0a03d6

    invoke-static {v6}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v6

    sget v7, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_SINGLE_PORTRAIT_HEIGHT:F

    sub-float/2addr v6, v7

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    add-float/2addr v5, v6

    sget v6, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->ANIMATION_TOP_MARGIN_TOP:F

    sub-float/2addr v5, v6

    const v6, 0x7f0200df

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/sec/android/glview/TwGLImage;-><init>(Lcom/sec/android/glview/TwGLContext;FFI)V

    aput-object v2, v0, v1

    .line 441
    new-instance v0, Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {p1}, Lcom/sec/android/app/camera/Camera;->getGLContext()Lcom/sec/android/glview/TwGLContext;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const v4, 0x7f020205

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/glview/TwGLImage;-><init>(Lcom/sec/android/glview/TwGLContext;FFI)V

    iput-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningBox:Lcom/sec/android/glview/TwGLImage;

    .line 442
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningBox:Lcom/sec/android/glview/TwGLImage;

    sget v1, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_SINGLE_WIDTH:F

    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_SINGLE_HEIGHT:F

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/glview/TwGLImage;->setSize(FF)V

    .line 443
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningBox:Lcom/sec/android/glview/TwGLImage;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 445
    new-instance v0, Lcom/sec/android/glview/TwGLButton;

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/Camera;->getGLContext()Lcom/sec/android/glview/TwGLContext;

    move-result-object v1

    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->STOP_BUTTON_POS_X:I

    int-to-float v2, v2

    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->STOP_BUTTON_POS_Y:I

    int-to-float v3, v3

    const v4, 0x7f0202c9

    const v5, 0x7f0202cb

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/glview/TwGLButton;-><init>(Lcom/sec/android/glview/TwGLContext;FFIIII)V

    iput-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureStopButton:Lcom/sec/android/glview/TwGLButton;

    .line 446
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureStopButton:Lcom/sec/android/glview/TwGLButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLButton;->setMute(Z)V

    .line 447
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureStopButton:Lcom/sec/android/glview/TwGLButton;

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/Camera;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0185

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLButton;->setTitle(Ljava/lang/String;)V

    .line 448
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureStopButton:Lcom/sec/android/glview/TwGLButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLButton;->setRotatable(Z)V

    .line 449
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureStopButton:Lcom/sec/android/glview/TwGLButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLButton;->setRotateAnimation(Z)V

    .line 450
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureStopButton:Lcom/sec/android/glview/TwGLButton;

    invoke-virtual {v0, p0}, Lcom/sec/android/glview/TwGLButton;->setOnClickListener(Lcom/sec/android/glview/TwGLView$OnClickListener;)V

    .line 451
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureStopButton:Lcom/sec/android/glview/TwGLButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLButton;->setFocusable(Z)V

    .line 452
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureStopButton:Lcom/sec/android/glview/TwGLButton;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLButton;->setVisibility(I)V

    .line 476
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/high16 v1, 0x3f800000    # 1.0f

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideBlinkingAnimation:Landroid/view/animation/Animation;

    .line 477
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideBlinkingAnimation:Landroid/view/animation/Animation;

    const-wide/16 v2, 0x2bc

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 478
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideBlinkingAnimation:Landroid/view/animation/Animation;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setRepeatMode(I)V

    .line 500
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureStopButton:Lcom/sec/android/glview/TwGLButton;

    invoke-virtual {p3, v0}, Lcom/sec/android/glview/TwGLViewGroup;->addView(Lcom/sec/android/glview/TwGLView;)V

    .line 501
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mBackgroundRectGroup:Lcom/sec/android/glview/TwGLViewGroup;

    invoke-virtual {p3, v0}, Lcom/sec/android/glview/TwGLViewGroup;->addView(Lcom/sec/android/glview/TwGLView;)V

    .line 502
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLivePreviewGroup:Lcom/sec/android/glview/TwGLViewGroup;

    invoke-virtual {p3, v0}, Lcom/sec/android/glview/TwGLViewGroup;->addView(Lcom/sec/android/glview/TwGLView;)V

    .line 503
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureFocusRect:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {p3, v0}, Lcom/sec/android/glview/TwGLViewGroup;->addView(Lcom/sec/android/glview/TwGLView;)V

    .line 504
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mTrapezoidCaptureFocusRect:Lcom/sec/android/app/camera/glwidget/TwGLTrapezoid;

    invoke-virtual {p3, v0}, Lcom/sec/android/glview/TwGLViewGroup;->addView(Lcom/sec/android/glview/TwGLView;)V

    .line 513
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningBox:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {p3, v0}, Lcom/sec/android/glview/TwGLViewGroup;->addView(Lcom/sec/android/glview/TwGLView;)V

    .line 515
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    const/4 v0, 0x4

    if-ge v8, v0, :cond_0

    .line 516
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningArrow:[Lcom/sec/android/glview/TwGLImage;

    aget-object v0, v0, v8

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 517
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningArrow:[Lcom/sec/android/glview/TwGLImage;

    aget-object v0, v0, v8

    invoke-virtual {p3, v0}, Lcom/sec/android/glview/TwGLViewGroup;->addView(Lcom/sec/android/glview/TwGLView;)V

    .line 515
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 519
    :cond_0
    const/4 v8, 0x0

    :goto_1
    const/16 v0, 0xe

    if-ge v8, v0, :cond_1

    .line 520
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideArrow:[Lcom/sec/android/glview/TwGLImage;

    aget-object v0, v0, v8

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 521
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideArrow:[Lcom/sec/android/glview/TwGLImage;

    aget-object v0, v0, v8

    invoke-virtual {p3, v0}, Lcom/sec/android/glview/TwGLViewGroup;->addView(Lcom/sec/android/glview/TwGLView;)V

    .line 519
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 523
    :cond_1
    const/4 v8, 0x0

    :goto_2
    const/16 v0, 0xe

    if-ge v8, v0, :cond_2

    .line 524
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideRect:[Lcom/sec/android/glview/TwGLImage;

    aget-object v0, v0, v8

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 525
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideRect:[Lcom/sec/android/glview/TwGLImage;

    aget-object v0, v0, v8

    invoke-virtual {p3, v0}, Lcom/sec/android/glview/TwGLViewGroup;->addView(Lcom/sec/android/glview/TwGLView;)V

    .line 523
    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    .line 527
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->init()V

    .line 528
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;)Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu$OnUltraWideShotCaptureCancelListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mListener:Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu$OnUltraWideShotCaptureCancelListener;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;)Lcom/sec/android/app/camera/Camera;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;)Lcom/sec/android/app/camera/Camera;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;

    .prologue
    .line 53
    iget-boolean v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPlayHaptic:Z

    return v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;)Lcom/sec/android/app/camera/Camera;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;)Lcom/sec/android/app/camera/Camera;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;)[Lcom/sec/android/glview/TwGLImage;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCenterGuideImage:[Lcom/sec/android/glview/TwGLImage;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;

    .prologue
    .line 53
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideOrient:I

    return v0
.end method

.method static synthetic access$1600(Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;)[Lcom/sec/android/glview/TwGLImage;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLeftGuideImage:[Lcom/sec/android/glview/TwGLImage;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;)[Lcom/sec/android/glview/TwGLImage;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mRightGuideImage:[Lcom/sec/android/glview/TwGLImage;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;

    .prologue
    .line 53
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideDirection:I

    return v0
.end method

.method static synthetic access$1900(Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;)Landroid/view/animation/Animation;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideBlinkingAnimation:Landroid/view/animation/Animation;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->stopLivePreviewHaptic()V

    return-void
.end method

.method static synthetic access$2000(Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;)Lcom/sec/android/app/camera/Camera;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;)D
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;

    .prologue
    .line 53
    iget-wide v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPotraitOffset:D

    return-wide v0
.end method

.method static synthetic access$2200()I
    .locals 1

    .prologue
    .line 53
    sget v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->OFFSET_DISTANCE_VERTICAL:I

    return v0
.end method

.method static synthetic access$2300(Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;)D
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;

    .prologue
    .line 53
    iget-wide v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLandscapeOffset:D

    return-wide v0
.end method

.method static synthetic access$2400()I
    .locals 1

    .prologue
    .line 53
    sget v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->OFFSET_DISTANCE_HORIZONTAL:I

    return v0
.end method

.method static synthetic access$2500(Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;)Lcom/sec/android/app/camera/Camera;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;

    .prologue
    .line 53
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCurrentOrientation:I

    return v0
.end method

.method static synthetic access$2700(Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;)[Lcom/sec/android/glview/TwGLImage;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideArrow:[Lcom/sec/android/glview/TwGLImage;

    return-object v0
.end method

.method static synthetic access$2800(Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;

    .prologue
    .line 53
    iget-boolean v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mAnimationDirection:Z

    return v0
.end method

.method static synthetic access$2802(Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;
    .param p1, "x1"    # Z

    .prologue
    .line 53
    iput-boolean p1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mAnimationDirection:Z

    return p1
.end method

.method static synthetic access$2900(Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;)Landroid/view/animation/AlphaAnimation;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mTwoBlinkAnimation:Landroid/view/animation/AlphaAnimation;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;)Lcom/sec/android/app/camera/Camera;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;

    .prologue
    .line 53
    iget-boolean v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mUltraWideShotWarning:Z

    return v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;)Lcom/sec/android/app/camera/Camera;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;)Lcom/sec/android/app/camera/Camera;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;)Lcom/sec/android/app/camera/Camera;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;)Lcom/sec/android/app/camera/Camera;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;)Lcom/sec/android/app/camera/Camera;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    return-object v0
.end method

.method private decodeRgbaBitmap([B)Landroid/graphics/Bitmap;
    .locals 17
    .param p1, "data"    # [B

    .prologue
    .line 2891
    if-eqz p1, :cond_0

    move-object/from16 v0, p1

    array-length v14, v0

    const/16 v15, 0x10

    if-ge v14, v15, :cond_2

    .line 2892
    :cond_0
    const-string v14, "TwGLUltraWideShotMenu"

    const-string v15, "Util.decodeRgbaBitmap: Received null or invalid data"

    invoke-static {v14, v15}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 2893
    const/4 v2, 0x0

    .line 2985
    :cond_1
    :goto_0
    return-object v2

    .line 2897
    :cond_2
    const/4 v14, 0x0

    aget-byte v14, p1, v14

    const/16 v15, 0x52

    if-ne v14, v15, :cond_3

    const/4 v14, 0x1

    aget-byte v14, p1, v14

    const/16 v15, 0x47

    if-ne v14, v15, :cond_3

    const/4 v14, 0x2

    aget-byte v14, p1, v14

    const/16 v15, 0x42

    if-ne v14, v15, :cond_3

    const/4 v14, 0x3

    aget-byte v14, p1, v14

    const/16 v15, 0x41

    if-eq v14, v15, :cond_4

    .line 2898
    :cond_3
    const-string v14, "TwGLUltraWideShotMenu"

    const-string v15, "Util.decodeRgbaBitmap: Data is invalid (RGBA tag not found)"

    invoke-static {v14, v15}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 2899
    const/4 v2, 0x0

    goto :goto_0

    .line 2904
    :cond_4
    const/4 v14, 0x4

    :try_start_0
    move-object/from16 v0, p1

    invoke-static {v0, v14}, Lcom/sec/android/app/camera/Util;->byteArrayToInt([BI)I

    move-result v13

    .line 2905
    .local v13, "width":I
    const/16 v14, 0x8

    move-object/from16 v0, p1

    invoke-static {v0, v14}, Lcom/sec/android/app/camera/Util;->byteArrayToInt([BI)I
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v6

    .line 2906
    .local v6, "height":I
    const/4 v10, 0x0

    .line 2912
    .local v10, "rotation":I
    move-object/from16 v0, p1

    array-length v14, v0

    mul-int v15, v13, v6

    mul-int/lit8 v15, v15, 0x4

    add-int/lit8 v15, v15, 0x10

    if-ge v14, v15, :cond_5

    .line 2913
    const-string v14, "TwGLUltraWideShotMenu"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Util.decodeRgbaBitmap: The buffer is too small to contain a image of "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "x"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 2914
    const/4 v2, 0x0

    goto :goto_0

    .line 2907
    .end local v6    # "height":I
    .end local v10    # "rotation":I
    .end local v13    # "width":I
    :catch_0
    move-exception v5

    .line 2908
    .local v5, "ex":Ljava/lang/ArrayIndexOutOfBoundsException;
    const-string v14, "TwGLUltraWideShotMenu"

    const-string v15, "Util.decodeRgbaBitmap: Could not parse panorama bitmap header"

    invoke-static {v14, v15}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 2909
    const/4 v2, 0x0

    goto :goto_0

    .line 2919
    .end local v5    # "ex":Ljava/lang/ArrayIndexOutOfBoundsException;
    .restart local v6    # "height":I
    .restart local v10    # "rotation":I
    .restart local v13    # "width":I
    :cond_5
    :try_start_1
    sget-object v14, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v13, v6, v14}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v2

    .line 2927
    .local v2, "bmp":Landroid/graphics/Bitmap;
    move-object/from16 v0, p1

    array-length v14, v0

    add-int/lit8 v14, v14, -0x10

    invoke-static {v14}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 2928
    .local v1, "bbuf":Ljava/nio/ByteBuffer;
    const/16 v14, 0x10

    move-object/from16 v0, p1

    array-length v15, v0

    add-int/lit8 v15, v15, -0x10

    move-object/from16 v0, p1

    invoke-virtual {v1, v0, v14, v15}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    .line 2929
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 2930
    invoke-virtual {v2, v1}, Landroid/graphics/Bitmap;->copyPixelsFromBuffer(Ljava/nio/Buffer;)V

    .line 2931
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 2933
    move v9, v13

    .line 2934
    .local v9, "realWidth":I
    move v8, v6

    .line 2936
    .local v8, "realHeight":I
    if-eqz v10, :cond_1

    .line 2938
    const/4 v11, 0x0

    .line 2939
    .local v11, "transX":I
    const/4 v12, 0x0

    .line 2941
    .local v12, "transY":I
    :goto_1
    if-gez v10, :cond_6

    .line 2942
    add-int/lit16 v10, v10, 0x168

    goto :goto_1

    .line 2920
    .end local v1    # "bbuf":Ljava/nio/ByteBuffer;
    .end local v2    # "bmp":Landroid/graphics/Bitmap;
    .end local v8    # "realHeight":I
    .end local v9    # "realWidth":I
    .end local v11    # "transX":I
    .end local v12    # "transY":I
    :catch_1
    move-exception v7

    .line 2921
    .local v7, "oom":Ljava/lang/OutOfMemoryError;
    const-string v14, "TwGLUltraWideShotMenu"

    const-string v15, "Util.decodeRgbaBitmap: Out of memory [1]"

    invoke-static {v14, v15}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 2922
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2944
    .end local v7    # "oom":Ljava/lang/OutOfMemoryError;
    .restart local v1    # "bbuf":Ljava/nio/ByteBuffer;
    .restart local v2    # "bmp":Landroid/graphics/Bitmap;
    .restart local v8    # "realHeight":I
    .restart local v9    # "realWidth":I
    .restart local v11    # "transX":I
    .restart local v12    # "transY":I
    :cond_6
    rem-int/lit16 v14, v10, 0x168

    const/16 v15, 0x5a

    if-ne v14, v15, :cond_7

    .line 2945
    move v9, v6

    .line 2946
    move v8, v13

    .line 2947
    const/16 v10, -0x5a

    .line 2948
    const/4 v11, 0x0

    .line 2949
    move v12, v13

    .line 2968
    :goto_2
    :try_start_2
    sget-object v14, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v9, v8, v14}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    :try_end_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v4

    .line 2976
    .local v4, "cvBmp":Landroid/graphics/Bitmap;
    new-instance v3, Landroid/graphics/Canvas;

    invoke-direct {v3, v4}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 2977
    .local v3, "cv":Landroid/graphics/Canvas;
    invoke-virtual {v3}, Landroid/graphics/Canvas;->save()I

    .line 2978
    int-to-float v14, v11

    int-to-float v15, v12

    invoke-virtual {v3, v14, v15}, Landroid/graphics/Canvas;->translate(FF)V

    .line 2979
    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v3, v2, v14, v15, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 2980
    invoke-virtual {v3}, Landroid/graphics/Canvas;->restore()V

    .line 2982
    const/4 v2, 0x0

    .line 2983
    move-object v2, v4

    goto/16 :goto_0

    .line 2950
    .end local v3    # "cv":Landroid/graphics/Canvas;
    .end local v4    # "cvBmp":Landroid/graphics/Bitmap;
    :cond_7
    rem-int/lit16 v14, v10, 0x168

    const/16 v15, 0xb4

    if-ne v14, v15, :cond_8

    .line 2951
    const/16 v10, 0xb4

    .line 2952
    move v11, v13

    .line 2953
    move v12, v6

    goto :goto_2

    .line 2954
    :cond_8
    rem-int/lit16 v14, v10, 0x168

    const/16 v15, 0x10e

    if-ne v14, v15, :cond_9

    .line 2955
    move v9, v6

    .line 2956
    move v8, v13

    .line 2957
    const/16 v10, 0x5a

    .line 2958
    move v11, v6

    .line 2959
    const/4 v12, 0x0

    goto :goto_2

    .line 2961
    :cond_9
    const-string v14, "TwGLUltraWideShotMenu"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Util.decodeRgbaBitmap: Rotation flag is invalid ("

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "). Discarded thumbnail."

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 2962
    const/4 v2, 0x0

    .line 2963
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2969
    :catch_2
    move-exception v7

    .line 2970
    .restart local v7    # "oom":Ljava/lang/OutOfMemoryError;
    const-string v14, "TwGLUltraWideShotMenu"

    const-string v15, "Util.decodeRgbaBitmap: Out of memory [2]"

    invoke-static {v14, v15}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 2971
    const/4 v2, 0x0

    .line 2972
    const/4 v2, 0x0

    goto/16 :goto_0
.end method

.method private init()V
    .locals 8

    .prologue
    const v5, 0x7f090008

    const/4 v7, 0x2

    const v2, 0x40490fdb    # (float)Math.PI

    const/4 v3, 0x0

    const/4 v1, 0x1

    .line 531
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideText:Lcom/sec/android/glview/TwGLText;

    invoke-virtual {v0, v1, v7}, Lcom/sec/android/glview/TwGLText;->setAlign(II)V

    .line 532
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideText:Lcom/sec/android/glview/TwGLText;

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->GUIDE_TEXT_SHADOW_POS_Y:I

    int-to-float v4, v4

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/glview/TwGLText;->setShadowLayer(ZFFFI)V

    .line 533
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideText:Lcom/sec/android/glview/TwGLText;

    sget-boolean v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->GUIDE_TEXT_SET_SHADOW:Z

    invoke-virtual {v0, v4}, Lcom/sec/android/glview/TwGLText;->setShadowVisibility(Z)V

    .line 534
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideText:Lcom/sec/android/glview/TwGLText;

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->GUIDE_TEXT_SHADOW_OFFSET:I

    int-to-float v4, v4

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/glview/TwGLText;->setShadowOffset(FF)V

    .line 535
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->isEasyMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 536
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideText:Lcom/sec/android/glview/TwGLText;

    const v4, 0x7f0b0052

    invoke-static {v4}, Lcom/sec/android/glview/TwGLContext;->getInteger(I)I

    move-result v4

    invoke-virtual {v0, v4}, Lcom/sec/android/glview/TwGLText;->setFontSize(I)V

    .line 538
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideText:Lcom/sec/android/glview/TwGLText;

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->GUIDE_TEXT_STROKE_WIDTH:I

    int-to-float v4, v4

    sget v6, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->GUIDE_TEXT_STROKE_COLOR:I

    invoke-virtual {v0, v1, v4, v6}, Lcom/sec/android/glview/TwGLText;->setStroke(ZFI)V

    .line 540
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningText:Lcom/sec/android/glview/TwGLText;

    invoke-virtual {v0, v1, v7}, Lcom/sec/android/glview/TwGLText;->setAlign(II)V

    .line 541
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningText:Lcom/sec/android/glview/TwGLText;

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->GUIDE_TEXT_SHADOW_POS_Y:I

    int-to-float v4, v4

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/glview/TwGLText;->setShadowLayer(ZFFFI)V

    .line 542
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningText:Lcom/sec/android/glview/TwGLText;

    sget-boolean v2, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->GUIDE_TEXT_SET_SHADOW:Z

    invoke-virtual {v0, v2}, Lcom/sec/android/glview/TwGLText;->setShadowVisibility(Z)V

    .line 543
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningText:Lcom/sec/android/glview/TwGLText;

    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->GUIDE_TEXT_SHADOW_OFFSET:I

    int-to-float v2, v2

    invoke-virtual {v0, v3, v2}, Lcom/sec/android/glview/TwGLText;->setShadowOffset(FF)V

    .line 544
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->isEasyMode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 545
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningText:Lcom/sec/android/glview/TwGLText;

    const v2, 0x7f0b0052

    invoke-static {v2}, Lcom/sec/android/glview/TwGLContext;->getInteger(I)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/sec/android/glview/TwGLText;->setFontSize(I)V

    .line 547
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningText:Lcom/sec/android/glview/TwGLText;

    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->GUIDE_TEXT_STROKE_WIDTH:I

    int-to-float v2, v2

    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->GUIDE_TEXT_STROKE_COLOR:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/glview/TwGLText;->setStroke(ZFI)V

    .line 548
    return-void
.end method

.method private resetTrapezoid()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2989
    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLeftTopX:F

    .line 2990
    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLeftTopY:F

    .line 2991
    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mRightTopX:F

    .line 2992
    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mRightTopY:F

    .line 2993
    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mRightBottomX:F

    .line 2994
    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mRightBottomY:F

    .line 2995
    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLeftBottomX:F

    .line 2996
    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLeftBottomY:F

    .line 2997
    return-void
.end method

.method private startLivePreviewHaptic()V
    .locals 2

    .prologue
    .line 2777
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mUltraWideShotMsgHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 2778
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mUltraWideShotMsgHandler:Landroid/os/Handler;

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 2780
    :cond_0
    return-void
.end method

.method private stopLivePreviewHaptic()V
    .locals 2

    .prologue
    .line 2782
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mUltraWideShotMsgHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 2783
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mUltraWideShotMsgHandler:Landroid/os/Handler;

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 2785
    :cond_0
    return-void
.end method


# virtual methods
.method public MoveSlowly()V
    .locals 1

    .prologue
    .line 756
    iget-boolean v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mIsUltraWideShotCapturing:Z

    if-eqz v0, :cond_0

    .line 757
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->hideGuideText()V

    .line 758
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->showWarningText(I)V

    .line 760
    :cond_0
    return-void
.end method

.method public checkWarningDirection()I
    .locals 5

    .prologue
    const/16 v4, 0x96

    const/16 v3, 0x78

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 1782
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mDetectedDirection:I

    if-nez v0, :cond_0

    .line 1783
    sget v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->WARNING_NONE:I

    .line 1855
    :goto_0
    return v0

    .line 1785
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->getSkipCheckWarning()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1786
    sget v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->WARNING_NONE:I

    goto :goto_0

    .line 1789
    :cond_1
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCurrentOrientation:I

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCurrentOrientation:I

    if-ne v0, v2, :cond_e

    .line 1790
    :cond_2
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mDetectedDirection:I

    if-ne v0, v2, :cond_8

    .line 1791
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureAngle:I

    if-ne v0, v3, :cond_3

    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mNextCaptureFocusStep:I

    if-eq v0, v1, :cond_4

    :cond_3
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureAngle:I

    if-ne v0, v4, :cond_6

    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mNextCaptureFocusStep:I

    if-eq v0, v1, :cond_4

    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mNextCaptureFocusStep:I

    if-ne v0, v2, :cond_6

    .line 1793
    :cond_4
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mDetectedDirection:I

    iget v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCurrentDirection:I

    if-eq v0, v1, :cond_5

    .line 1794
    sget v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->WARNING_LEFT:I

    goto :goto_0

    .line 1795
    :cond_5
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mFocusRectCenterX:F

    iget v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mNextCaptureFocusRectCenterX:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1a

    .line 1796
    sget v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->WARNING_RIGHT:I

    goto :goto_0

    .line 1799
    :cond_6
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPrevFocusRectCenterX:F

    iget v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mFocusRectCenterX:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_7

    .line 1800
    sget v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->WARNING_RIGHT:I

    goto :goto_0

    .line 1801
    :cond_7
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mFocusRectCenterX:F

    iget v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mNextCaptureFocusRectCenterX:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1a

    .line 1802
    sget v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->WARNING_LEFT:I

    goto :goto_0

    .line 1806
    :cond_8
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureAngle:I

    if-ne v0, v3, :cond_9

    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mNextCaptureFocusStep:I

    if-eq v0, v1, :cond_a

    :cond_9
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureAngle:I

    if-ne v0, v4, :cond_c

    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mNextCaptureFocusStep:I

    if-eq v0, v1, :cond_a

    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mNextCaptureFocusStep:I

    if-ne v0, v2, :cond_c

    .line 1808
    :cond_a
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mDetectedDirection:I

    iget v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCurrentDirection:I

    if-eq v0, v1, :cond_b

    .line 1809
    sget v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->WARNING_RIGHT:I

    goto :goto_0

    .line 1810
    :cond_b
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mFocusRectCenterX:F

    iget v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mNextCaptureFocusRectCenterX:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1a

    .line 1811
    sget v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->WARNING_LEFT:I

    goto :goto_0

    .line 1814
    :cond_c
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPrevFocusRectCenterX:F

    iget v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mFocusRectCenterX:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_d

    .line 1815
    sget v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->WARNING_LEFT:I

    goto/16 :goto_0

    .line 1816
    :cond_d
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mFocusRectCenterX:F

    iget v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mNextCaptureFocusRectCenterX:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1a

    .line 1817
    sget v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->WARNING_RIGHT:I

    goto/16 :goto_0

    .line 1822
    :cond_e
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mDetectedDirection:I

    if-ne v0, v2, :cond_14

    .line 1823
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureAngle:I

    if-ne v0, v3, :cond_f

    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mNextCaptureFocusStep:I

    if-eq v0, v1, :cond_10

    :cond_f
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureAngle:I

    if-ne v0, v4, :cond_12

    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mNextCaptureFocusStep:I

    if-eq v0, v1, :cond_10

    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mNextCaptureFocusStep:I

    if-ne v0, v2, :cond_12

    .line 1825
    :cond_10
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mDetectedDirection:I

    iget v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCurrentDirection:I

    if-eq v0, v1, :cond_11

    .line 1826
    sget v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->WARNING_DOWN:I

    goto/16 :goto_0

    .line 1827
    :cond_11
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mFocusRectCenterY:F

    iget v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mNextCaptureFocusRectCenterY:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1a

    .line 1828
    sget v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->WARNING_UP:I

    goto/16 :goto_0

    .line 1831
    :cond_12
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPrevFocusRectCenterY:F

    iget v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mFocusRectCenterY:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_13

    .line 1832
    sget v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->WARNING_UP:I

    goto/16 :goto_0

    .line 1833
    :cond_13
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mFocusRectCenterY:F

    iget v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mNextCaptureFocusRectCenterY:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1a

    .line 1834
    sget v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->WARNING_DOWN:I

    goto/16 :goto_0

    .line 1838
    :cond_14
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureAngle:I

    if-ne v0, v3, :cond_15

    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mNextCaptureFocusStep:I

    if-eq v0, v1, :cond_16

    :cond_15
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureAngle:I

    if-ne v0, v4, :cond_18

    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mNextCaptureFocusStep:I

    if-eq v0, v1, :cond_16

    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mNextCaptureFocusStep:I

    if-ne v0, v2, :cond_18

    .line 1840
    :cond_16
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mDetectedDirection:I

    iget v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCurrentDirection:I

    if-eq v0, v1, :cond_17

    .line 1841
    sget v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->WARNING_UP:I

    goto/16 :goto_0

    .line 1842
    :cond_17
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mFocusRectCenterY:F

    iget v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mNextCaptureFocusRectCenterY:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1a

    .line 1843
    sget v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->WARNING_DOWN:I

    goto/16 :goto_0

    .line 1846
    :cond_18
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPrevFocusRectCenterY:F

    iget v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mFocusRectCenterY:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_19

    .line 1847
    sget v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->WARNING_DOWN:I

    goto/16 :goto_0

    .line 1848
    :cond_19
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mFocusRectCenterY:F

    iget v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mNextCaptureFocusRectCenterY:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1a

    .line 1849
    sget v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->WARNING_UP:I

    goto/16 :goto_0

    .line 1855
    :cond_1a
    sget v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->WARNING_NONE:I

    goto/16 :goto_0
.end method

.method public clear()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2393
    const-string v0, "TwGLUltraWideShotMenu"

    const-string v1, "clear"

    invoke-static {v0, v1}, Landroid/util/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 2395
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLivePreview:Lcom/sec/android/glview/TwGLBitmapTexture;

    if-eqz v0, :cond_0

    .line 2396
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLivePreview:Lcom/sec/android/glview/TwGLBitmapTexture;

    invoke-virtual {v0}, Lcom/sec/android/glview/TwGLBitmapTexture;->clear()V

    .line 2397
    iput-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLivePreview:Lcom/sec/android/glview/TwGLBitmapTexture;

    .line 2399
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPreviewFocusRect:Lcom/sec/android/glview/TwGLNinePatch;

    if-eqz v0, :cond_1

    .line 2400
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPreviewFocusRect:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v0}, Lcom/sec/android/glview/TwGLNinePatch;->clear()V

    .line 2401
    iput-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPreviewFocusRect:Lcom/sec/android/glview/TwGLNinePatch;

    .line 2403
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLivePreviewGroup:Lcom/sec/android/glview/TwGLViewGroup;

    if-eqz v0, :cond_2

    .line 2404
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLivePreviewGroup:Lcom/sec/android/glview/TwGLViewGroup;

    invoke-virtual {v0}, Lcom/sec/android/glview/TwGLViewGroup;->clear()V

    .line 2405
    iput-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLivePreviewGroup:Lcom/sec/android/glview/TwGLViewGroup;

    .line 2407
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mBackgroundRectGroup:Lcom/sec/android/glview/TwGLViewGroup;

    if-eqz v0, :cond_3

    .line 2408
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mBackgroundRectGroup:Lcom/sec/android/glview/TwGLViewGroup;

    invoke-virtual {v0}, Lcom/sec/android/glview/TwGLViewGroup;->clear()V

    .line 2409
    iput-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mBackgroundRectGroup:Lcom/sec/android/glview/TwGLViewGroup;

    .line 2411
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureFocusRect:Lcom/sec/android/glview/TwGLNinePatch;

    if-eqz v0, :cond_4

    .line 2412
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureFocusRect:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v0}, Lcom/sec/android/glview/TwGLNinePatch;->clear()V

    .line 2413
    iput-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureFocusRect:Lcom/sec/android/glview/TwGLNinePatch;

    .line 2416
    :cond_4
    invoke-super {p0}, Lcom/sec/android/app/camera/MenuBase;->clear()V

    .line 2417
    return-void
.end method

.method public clearUltraWideRect()V
    .locals 3

    .prologue
    const/4 v2, 0x4

    .line 2350
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLivePreviewGroup:Lcom/sec/android/glview/TwGLViewGroup;

    if-eqz v0, :cond_1

    .line 2351
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLivePreviewGroup:Lcom/sec/android/glview/TwGLViewGroup;

    invoke-virtual {v0, v2}, Lcom/sec/android/glview/TwGLViewGroup;->setVisibility(I)V

    .line 2352
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLivePreview:Lcom/sec/android/glview/TwGLBitmapTexture;

    if-eqz v0, :cond_0

    .line 2353
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLivePreviewGroup:Lcom/sec/android/glview/TwGLViewGroup;

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLivePreview:Lcom/sec/android/glview/TwGLBitmapTexture;

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLViewGroup;->removeView(Lcom/sec/android/glview/TwGLView;)V

    .line 2354
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLivePreview:Lcom/sec/android/glview/TwGLBitmapTexture;

    invoke-virtual {v0}, Lcom/sec/android/glview/TwGLBitmapTexture;->clear()V

    .line 2355
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLivePreview:Lcom/sec/android/glview/TwGLBitmapTexture;

    .line 2357
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPreviewFocusRect:Lcom/sec/android/glview/TwGLNinePatch;

    if-eqz v0, :cond_1

    .line 2358
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPreviewFocusRect:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v0, v2}, Lcom/sec/android/glview/TwGLNinePatch;->setVisibility(I)V

    .line 2361
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mBackgroundRectGroup:Lcom/sec/android/glview/TwGLViewGroup;

    if-eqz v0, :cond_2

    .line 2362
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mBackgroundRectGroup:Lcom/sec/android/glview/TwGLViewGroup;

    invoke-virtual {v0}, Lcom/sec/android/glview/TwGLViewGroup;->resetTranslate()V

    .line 2363
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mBackgroundRect:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v0}, Lcom/sec/android/glview/TwGLNinePatch;->resetTranslate()V

    .line 2364
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mBackgroundRectGroup:Lcom/sec/android/glview/TwGLViewGroup;

    invoke-virtual {v0, v2}, Lcom/sec/android/glview/TwGLViewGroup;->setVisibility(I)V

    .line 2366
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureFocusRect:Lcom/sec/android/glview/TwGLNinePatch;

    if-eqz v0, :cond_3

    .line 2367
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureFocusRect:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v0, v2}, Lcom/sec/android/glview/TwGLNinePatch;->setVisibility(I)V

    .line 2369
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideText:Lcom/sec/android/glview/TwGLText;

    if-eqz v0, :cond_4

    .line 2370
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideText:Lcom/sec/android/glview/TwGLText;

    invoke-virtual {v0, v2}, Lcom/sec/android/glview/TwGLText;->setVisibility(I)V

    .line 2372
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningText:Lcom/sec/android/glview/TwGLText;

    if-eqz v0, :cond_5

    .line 2373
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningText:Lcom/sec/android/glview/TwGLText;

    invoke-virtual {v0, v2}, Lcom/sec/android/glview/TwGLText;->setVisibility(I)V

    .line 2376
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mNextCaptureFocusRect:Lcom/sec/android/glview/TwGLImage;

    if-eqz v0, :cond_6

    .line 2377
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mNextCaptureFocusRect:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v0, v2}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 2380
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mNextCaptureFocusRectPort:Lcom/sec/android/glview/TwGLImage;

    if-eqz v0, :cond_7

    .line 2381
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mNextCaptureFocusRectPort:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v0, v2}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 2383
    :cond_7
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->hideWarning()V

    .line 2384
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->hideWarningArrow()V

    .line 2389
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mTrapezoidCaptureFocusRect:Lcom/sec/android/app/camera/glwidget/TwGLTrapezoid;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/camera/glwidget/TwGLTrapezoid;->setVisibility(I)V

    .line 2390
    return-void
.end method

.method public getCaptureAngle()I
    .locals 2

    .prologue
    .line 1090
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCurrentOrientation:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCurrentOrientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 1091
    :cond_0
    const/16 v0, 0x78

    .line 1093
    :goto_0
    return v0

    :cond_1
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureAngle:I

    goto :goto_0
.end method

.method public getCaptureError()Z
    .locals 1

    .prologue
    .line 951
    iget-boolean v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureError:Z

    return v0
.end method

.method public declared-synchronized getCaptureProgressIncreased()I
    .locals 1

    .prologue
    .line 1106
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureCount:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getFocusRectDistance()F
    .locals 3

    .prologue
    .line 2760
    const/4 v0, 0x0

    .line 2761
    .local v0, "distance":F
    iget v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCurrentOrientation:I

    packed-switch v1, :pswitch_data_0

    .line 2771
    :goto_0
    return v0

    .line 2764
    :pswitch_0
    iget v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPrevFocusRectCenterX:F

    iget v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mFocusRectCenterX:F

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v0

    .line 2765
    goto :goto_0

    .line 2768
    :pswitch_1
    iget v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPrevFocusRectCenterY:F

    iget v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mFocusRectCenterY:F

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v0

    goto :goto_0

    .line 2761
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getGuideDirection(I)I
    .locals 3
    .param p1, "step"    # I

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    .line 2459
    iget v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCurrentOrientation:I

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCurrentOrientation:I

    if-ne v2, v0, :cond_4

    .line 2460
    :cond_0
    iget v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mDetectedDirection:I

    if-ne v2, v0, :cond_3

    .line 2461
    if-ne p1, v1, :cond_2

    .line 2501
    :cond_1
    :goto_0
    return v0

    :cond_2
    move v0, v1

    .line 2464
    goto :goto_0

    .line 2465
    :cond_3
    iget v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mDetectedDirection:I

    if-ne v2, v1, :cond_5

    .line 2466
    if-ne p1, v1, :cond_1

    move v0, v1

    .line 2467
    goto :goto_0

    .line 2472
    :cond_4
    iget v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureAngle:I

    sparse-switch v2, :sswitch_data_0

    .line 2501
    :cond_5
    const/4 v0, -0x1

    goto :goto_0

    .line 2474
    :sswitch_0
    iget v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mDetectedDirection:I

    if-ne v2, v0, :cond_6

    .line 2475
    if-eq p1, v1, :cond_1

    move v0, v1

    .line 2478
    goto :goto_0

    .line 2479
    :cond_6
    iget v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mDetectedDirection:I

    if-ne v2, v1, :cond_5

    .line 2480
    if-ne p1, v1, :cond_1

    move v0, v1

    .line 2481
    goto :goto_0

    .line 2487
    :sswitch_1
    iget v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mDetectedDirection:I

    if-ne v2, v0, :cond_7

    .line 2488
    if-eq p1, v1, :cond_1

    if-eq p1, v0, :cond_1

    move v0, v1

    .line 2491
    goto :goto_0

    .line 2492
    :cond_7
    iget v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mDetectedDirection:I

    if-ne v2, v1, :cond_5

    .line 2493
    if-eq p1, v1, :cond_8

    if-ne p1, v0, :cond_1

    :cond_8
    move v0, v1

    .line 2494
    goto :goto_0

    .line 2472
    :sswitch_data_0
    .sparse-switch
        0x78 -> :sswitch_0
        0x96 -> :sswitch_1
    .end sparse-switch
.end method

.method public getLivePreviewHapticLevel()I
    .locals 6

    .prologue
    const/16 v3, 0x8

    .line 2794
    const/4 v0, 0x4

    .line 2795
    .local v0, "LEVEL_NUM":I
    const/4 v2, 0x0

    .line 2796
    .local v2, "center":F
    const/4 v1, 0x0

    .line 2797
    .local v1, "area":F
    iget v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCurrentOrientation:I

    if-eqz v4, :cond_0

    iget v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCurrentOrientation:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_1

    .line 2798
    :cond_0
    iget v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mFocusRectCenterX:F

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v2

    .line 2799
    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_LANDSCAPE_WIDTH:F

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_SINGLE_WIDTH:F

    sub-float/2addr v4, v5

    int-to-float v5, v3

    div-float v1, v4, v5

    .line 2805
    :goto_0
    cmpg-float v4, v2, v1

    if-gez v4, :cond_2

    .line 2812
    :goto_1
    return v3

    .line 2801
    :cond_1
    iget v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mFocusRectCenterY:F

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v2

    .line 2802
    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_PORTRAIT_HEIGHT:F

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_SINGLE_PORTRAIT_HEIGHT:F

    sub-float/2addr v4, v5

    int-to-float v5, v3

    div-float v1, v4, v5

    goto :goto_0

    .line 2807
    :cond_2
    const/high16 v3, 0x40000000    # 2.0f

    mul-float/2addr v3, v1

    cmpg-float v3, v2, v3

    if-gez v3, :cond_3

    .line 2808
    const/16 v3, 0x9

    goto :goto_1

    .line 2809
    :cond_3
    const/high16 v3, 0x40400000    # 3.0f

    mul-float/2addr v3, v1

    cmpg-float v3, v2, v3

    if-gez v3, :cond_4

    .line 2810
    const/16 v3, 0xa

    goto :goto_1

    .line 2812
    :cond_4
    const/16 v3, 0xb

    goto :goto_1
.end method

.method public getNextFocusRectDistance()F
    .locals 2

    .prologue
    .line 1859
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCurrentOrientation:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCurrentOrientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 1860
    :cond_0
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mNextCaptureFocusRectCenterX:F

    iget v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mFocusRectCenterX:F

    sub-float/2addr v0, v1

    .line 1862
    :goto_0
    return v0

    :cond_1
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mNextCaptureFocusRectCenterY:F

    iget v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mFocusRectCenterY:F

    sub-float/2addr v0, v1

    goto :goto_0
.end method

.method public getPreviewThumbnailSize(II)V
    .locals 2
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 2432
    int-to-float v0, p1

    sget v1, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_RATIO:F

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPreviewThumbnailWidth:F

    .line 2433
    int-to-float v0, p2

    sget v1, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_RATIO:F

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPreviewThumbnailHeight:F

    .line 2434
    return-void
.end method

.method public getSkipCapture()Z
    .locals 1

    .prologue
    .line 959
    iget-boolean v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mSkipCapture:Z

    return v0
.end method

.method public getSkipCheckWarning()Z
    .locals 1

    .prologue
    .line 972
    iget-boolean v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mSkipCheckWarning:Z

    return v0
.end method

.method public hideCaptureAngleButton()V
    .locals 0

    .prologue
    .line 1074
    return-void
.end method

.method public hideGuideRect()V
    .locals 3

    .prologue
    .line 2043
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v1, 0xe

    if-ge v0, v1, :cond_0

    .line 2044
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideRect:[Lcom/sec/android/glview/TwGLImage;

    aget-object v1, v1, v0

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 2043
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2046
    :cond_0
    return-void
.end method

.method public hideGuideText()V
    .locals 2

    .prologue
    .line 579
    const-string v0, "TwGLUltraWideShotMenu"

    const-string v1, "hideGuideText"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 580
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideText:Lcom/sec/android/glview/TwGLText;

    if-eqz v0, :cond_0

    .line 581
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideText:Lcom/sec/android/glview/TwGLText;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLText;->setVisibility(I)V

    .line 582
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mUltraWideShotMsgHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 583
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mUltraWideShotMsgHandler:Landroid/os/Handler;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 586
    :cond_0
    return-void
.end method

.method public hideStopButton()V
    .locals 2

    .prologue
    .line 1025
    const-string v0, "TwGLUltraWideShotMenu"

    const-string v1, "hideStopButton"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1026
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureStopButton:Lcom/sec/android/glview/TwGLButton;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLButton;->setVisibility(I)V

    .line 1027
    return-void
.end method

.method public hideWarning()V
    .locals 2

    .prologue
    .line 1991
    const-string v0, "TwGLUltraWideShotMenu"

    const-string v1, "hideWarning"

    invoke-static {v0, v1}, Landroid/util/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 1992
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningBox:Lcom/sec/android/glview/TwGLImage;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 1993
    return-void
.end method

.method public hideWarningArrow()V
    .locals 5

    .prologue
    const/16 v4, 0xe

    const/4 v3, 0x4

    .line 1995
    const-string v1, "TwGLUltraWideShotMenu"

    const-string v2, "hideWarningArrow"

    invoke-static {v1, v2}, Landroid/util/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 1996
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v3, :cond_0

    .line 1997
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningArrow:[Lcom/sec/android/glview/TwGLImage;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/sec/android/glview/TwGLImage;->resetTranslate()V

    .line 1998
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningArrow:[Lcom/sec/android/glview/TwGLImage;

    aget-object v1, v1, v0

    invoke-virtual {v1, v3}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 1996
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2000
    :cond_0
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v4, :cond_1

    .line 2001
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideArrow:[Lcom/sec/android/glview/TwGLImage;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/sec/android/glview/TwGLImage;->resetTranslate()V

    .line 2002
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideArrow:[Lcom/sec/android/glview/TwGLImage;

    aget-object v1, v1, v0

    invoke-virtual {v1, v3}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 2000
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2004
    :cond_1
    const/4 v0, 0x0

    :goto_2
    if-ge v0, v4, :cond_2

    .line 2005
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideRect:[Lcom/sec/android/glview/TwGLImage;

    aget-object v1, v1, v0

    invoke-virtual {v1, v3}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 2004
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 2007
    :cond_2
    return-void
.end method

.method public hideWarningText()V
    .locals 2

    .prologue
    .line 622
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningText:Lcom/sec/android/glview/TwGLText;

    if-eqz v0, :cond_0

    .line 623
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningText:Lcom/sec/android/glview/TwGLText;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLText;->setVisibility(I)V

    .line 625
    :cond_0
    return-void
.end method

.method public isEasyMode()Z
    .locals 1

    .prologue
    .line 1226
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    if-eqz v0, :cond_0

    .line 1227
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->isEasyMode()Z

    move-result v0

    .line 1229
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isMaxPositionReached(FF)Z
    .locals 5
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    const/4 v3, 0x2

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1888
    iget v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCurrentOrientation:I

    packed-switch v2, :pswitch_data_0

    .line 1936
    :cond_0
    iput-boolean v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideShow:Z

    move v0, v1

    .line 1937
    :goto_0
    return v0

    .line 1891
    :pswitch_0
    iget v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mDetectedDirection:I

    if-ne v2, v3, :cond_2

    .line 1892
    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_LANDSCAPE_X:F

    cmpg-float v2, p1, v2

    if-gtz v2, :cond_0

    .line 1893
    iget-boolean v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideShow:Z

    if-nez v2, :cond_1

    .line 1894
    iput-boolean v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideShow:Z

    goto :goto_0

    :cond_1
    move v0, v1

    .line 1897
    goto :goto_0

    .line 1900
    :cond_2
    iget v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mDetectedDirection:I

    if-ne v2, v0, :cond_0

    .line 1901
    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_SINGLE_WIDTH:F

    add-float/2addr v2, p1

    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_LANDSCAPE_X:F

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_LANDSCAPE_WIDTH:F

    add-float/2addr v3, v4

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_0

    .line 1902
    iget-boolean v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideShow:Z

    if-nez v2, :cond_3

    .line 1903
    iput-boolean v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideShow:Z

    goto :goto_0

    :cond_3
    move v0, v1

    .line 1906
    goto :goto_0

    .line 1913
    :pswitch_1
    iget v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mDetectedDirection:I

    if-ne v2, v3, :cond_5

    .line 1914
    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_SINGLE_PORTRAIT_HEIGHT:F

    add-float/2addr v2, p2

    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_PORTRAIT_Y:F

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_PORTRAIT_HEIGHT:F

    add-float/2addr v3, v4

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_0

    .line 1915
    iget-boolean v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideShow:Z

    if-nez v2, :cond_4

    .line 1916
    iput-boolean v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideShow:Z

    goto :goto_0

    :cond_4
    move v0, v1

    .line 1919
    goto :goto_0

    .line 1922
    :cond_5
    iget v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mDetectedDirection:I

    if-ne v2, v0, :cond_0

    .line 1923
    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_PORTRAIT_Y:F

    cmpg-float v2, p2, v2

    if-gtz v2, :cond_0

    .line 1924
    iget-boolean v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideShow:Z

    if-nez v2, :cond_6

    .line 1925
    iput-boolean v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideShow:Z

    goto :goto_0

    :cond_6
    move v0, v1

    .line 1928
    goto :goto_0

    .line 1888
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public isMenuOpened()Z
    .locals 1

    .prologue
    .line 639
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    if-eqz v0, :cond_2

    .line 640
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->isContextMenuOpened()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->isListMenuOpened()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getCameraBaseMenu()Lcom/sec/android/app/camera/glwidget/TwGLCameraBaseMenu;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getCameraBaseMenu()Lcom/sec/android/app/camera/glwidget/TwGLCameraBaseMenu;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/glwidget/TwGLCameraBaseMenu;->isModeMenuOpened()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getEngine()Lcom/sec/android/app/camera/CommonEngine;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CommonEngine;->isRecording()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 644
    :cond_1
    const/4 v0, 0x1

    .line 647
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isNextFocusRectBoundary()Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 1867
    const/high16 v0, 0x40000000    # 2.0f

    .line 1868
    .local v0, "BOUNDARY":F
    const/4 v1, 0x0

    .line 1869
    .local v1, "gap":F
    iget v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCurrentOrientation:I

    packed-switch v3, :pswitch_data_0

    .line 1885
    :cond_0
    const/4 v2, 0x0

    :goto_0
    return v2

    .line 1872
    :pswitch_0
    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureFocusRect:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v3}, Lcom/sec/android/glview/TwGLNinePatch;->getCurrentLeft()F

    move-result v3

    iget-object v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mNextCaptureFocusRect:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v4}, Lcom/sec/android/glview/TwGLImage;->getCurrentLeft()F

    move-result v4

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v1

    .line 1873
    cmpg-float v3, v1, v0

    if-gtz v3, :cond_0

    goto :goto_0

    .line 1879
    :pswitch_1
    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureFocusRect:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v3}, Lcom/sec/android/glview/TwGLNinePatch;->getCurrentTop()F

    move-result v3

    iget-object v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mNextCaptureFocusRectPort:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v4}, Lcom/sec/android/glview/TwGLImage;->getCurrentTop()F

    move-result v4

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v1

    .line 1880
    cmpg-float v3, v1, v0

    if-gtz v3, :cond_0

    goto :goto_0

    .line 1869
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public declared-synchronized isReadyToCapture()Z
    .locals 1

    .prologue
    .line 351
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLivePreview:Lcom/sec/android/glview/TwGLBitmapTexture;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public isRectGuideVisible()Z
    .locals 2

    .prologue
    .line 2787
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v1, 0xe

    if-ge v0, v1, :cond_1

    .line 2788
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideRect:[Lcom/sec/android/glview/TwGLImage;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/sec/android/glview/TwGLImage;->isVisible()I

    move-result v1

    if-nez v1, :cond_0

    .line 2789
    const/4 v1, 0x1

    .line 2791
    :goto_1
    return v1

    .line 2787
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2791
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public isUltraWideShotCapturing()Z
    .locals 1

    .prologue
    .line 1233
    iget-boolean v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mIsUltraWideShotCapturing:Z

    return v0
.end method

.method public onBack()V
    .locals 2

    .prologue
    .line 767
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->setCaptureError(Z)V

    .line 768
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->getCaptureProgressIncreased()I

    move-result v0

    if-lez v0, :cond_0

    .line 769
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->onUltraWideShotCaptureStopped()V

    .line 779
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/camera/Camera;->setWinkDetected(Z)V

    .line 780
    return-void

    .line 770
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getEngine()Lcom/sec/android/app/camera/CommonEngine;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CommonEngine;->isUltraWideShotCapturing()Z

    move-result v0

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureCount:I

    if-nez v0, :cond_2

    .line 771
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getEngine()Lcom/sec/android/app/camera/CommonEngine;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CommonEngine;->isTimerCounting()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 772
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getEngine()Lcom/sec/android/app/camera/CommonEngine;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CommonEngine;->doCancelShutterTimer()V

    .line 775
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->reset()V

    goto :goto_0

    .line 774
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->onUltraWideShotCaptureCancelled()V

    goto :goto_1

    .line 777
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->processBack()V

    goto :goto_0
.end method

.method public onClick(Lcom/sec/android/glview/TwGLView;)Z
    .locals 2
    .param p1, "v"    # Lcom/sec/android/glview/TwGLView;

    .prologue
    const/4 v0, 0x1

    .line 1121
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureStopButton:Lcom/sec/android/glview/TwGLButton;

    if-ne p1, v1, :cond_1

    .line 1122
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/Camera;->getEngine()Lcom/sec/android/app/camera/CommonEngine;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/Camera;->getEngine()Lcom/sec/android/app/camera/CommonEngine;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/camera/CommonEngine;->isRecorderStarting()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1123
    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->setCaptureError(Z)V

    .line 1124
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/Camera;->getEngine()Lcom/sec/android/app/camera/CommonEngine;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/camera/CommonEngine;->doStopUltraWideShotSync()V

    .line 1126
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->hideStopButton()V

    .line 1138
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onHide()V
    .locals 2

    .prologue
    .line 928
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getEngine()Lcom/sec/android/app/camera/CommonEngine;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CommonEngine;->getRequestQueue()Lcom/sec/android/app/camera/CeRequestQueue;

    move-result-object v0

    const/16 v1, 0x66

    invoke-virtual {v0, v1}, Lcom/sec/android/app/camera/CeRequestQueue;->searchRequest(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 929
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->hideCropArea()V

    .line 931
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->clearUltraWideRect()V

    .line 932
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->stopUltraWideShotSound()V

    .line 933
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->setPreviewThumbnailSizeToDefault()V

    .line 937
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 1146
    const/4 v0, 0x4

    if-eq p1, v0, :cond_0

    const/16 v0, 0x1b

    if-ne p1, v0, :cond_1

    .line 1147
    :cond_0
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPostProgress:I

    if-lez v0, :cond_1

    .line 1148
    const-string v0, "TwGLUltraWideShotMenu"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onKeyDown - it is stitching ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPostProgress:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1149
    const/4 v0, 0x1

    .line 1153
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 5
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/16 v2, 0x1b

    const/4 v4, 0x6

    const/4 v0, 0x1

    .line 1157
    const/4 v1, 0x4

    if-eq p1, v1, :cond_0

    if-ne p1, v2, :cond_6

    .line 1158
    :cond_0
    if-ne p1, v2, :cond_5

    .line 1159
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    if-eqz v1, :cond_5

    .line 1160
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/Camera;->getEngine()Lcom/sec/android/app/camera/CommonEngine;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/Camera;->getEngine()Lcom/sec/android/app/camera/CommonEngine;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/camera/CommonEngine;->isPreviewStarted()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1161
    const-string v1, "TwGLUltraWideShotMenu"

    const-string v2, "onKeyUp - Preview is not started yet"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 1204
    :cond_1
    :goto_0
    return v0

    .line 1164
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/Camera;->getEngine()Lcom/sec/android/app/camera/CommonEngine;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/Camera;->getEngine()Lcom/sec/android/app/camera/CommonEngine;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/camera/CommonEngine;->isUltraWideShotCapturing()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1165
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->onBack()V

    .line 1166
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->showPreviewGroup()V

    goto :goto_0

    .line 1168
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/Camera;->IsWideSelfieLowLightDetected()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1169
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    const/16 v2, 0x27

    invoke-virtual {v1, v2}, Lcom/sec/android/app/camera/Camera;->showUltraWideShotToastPopup(I)V

    goto :goto_0

    .line 1175
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/camera/CameraSettings;->getTimer()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 1197
    :cond_5
    :goto_1
    :pswitch_0
    iget v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPostProgress:I

    if-gtz v1, :cond_1

    .line 1200
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getGLContext()Lcom/sec/android/glview/TwGLContext;

    move-result-object v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getGLContext()Lcom/sec/android/glview/TwGLContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/glview/TwGLContext;->isTouchExplorationEnabled()Z

    move-result v0

    if-nez v0, :cond_6

    .line 1201
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureStopButton:Lcom/sec/android/glview/TwGLButton;

    invoke-virtual {v0}, Lcom/sec/android/glview/TwGLButton;->requestFocus()Z

    .line 1204
    :cond_6
    const/4 v0, 0x0

    goto :goto_0

    .line 1182
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mUltraWideShotMsgHandler:Landroid/os/Handler;

    const-wide/16 v2, 0xbb8

    invoke-virtual {v1, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_1

    .line 1185
    :pswitch_2
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mUltraWideShotMsgHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x1770

    invoke-virtual {v1, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_1

    .line 1188
    :pswitch_3
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mUltraWideShotMsgHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x2af8

    invoke-virtual {v1, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_1

    .line 1175
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public declared-synchronized onLivePreviewData([B)V
    .locals 14
    .param p1, "data"    # [B

    .prologue
    const/4 v3, 0x2

    const/4 v1, 0x3

    const/4 v2, 0x1

    .line 2621
    monitor-enter p0

    const/4 v6, 0x0

    .line 2622
    .local v6, "bmp":Landroid/graphics/Bitmap;
    :try_start_0
    invoke-direct {p0, p1}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->decodeRgbaBitmap([B)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 2623
    if-nez v6, :cond_1

    .line 2624
    const-string v0, "TwGLUltraWideShotMenu"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unable to RGBA Data Creation Failed. mCaptureCount:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2757
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 2628
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    iget-boolean v0, v0, Lcom/sec/android/app/camera/Camera;->mSkipFrameOnUltraWideShot:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mSkipFrame:I

    if-ge v0, v1, :cond_2

    .line 2629
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->clearUltraWideRect()V

    .line 2630
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mSkipFrame:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mSkipFrame:I

    .line 2631
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mSkipFrame:I

    if-ne v0, v1, :cond_0

    .line 2632
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->showPreviewGroup()V

    .line 2633
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/sec/android/app/camera/Camera;->mSkipFrameOnUltraWideShot:Z

    .line 2634
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mSkipFrame:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2621
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2639
    :cond_2
    :try_start_2
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureCount:I

    if-eq v0, v2, :cond_3

    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureCount:I

    if-nez v0, :cond_5

    .line 2640
    :cond_3
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-static {v6, v0, v1}, Lcom/sec/android/app/camera/Util;->rotateAndMirror(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 2641
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mStartCheckWarning:Z

    .line 2651
    :cond_4
    :goto_1
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPreviewThumbnailWidth:F

    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPrevThumbnailWidth:F

    .line 2652
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPreviewThumbnailHeight:F

    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPrevThumbnailHeight:F

    .line 2653
    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->getPreviewThumbnailSize(II)V

    .line 2655
    const/4 v8, 0x0

    .local v8, "left":F
    const/4 v10, 0x0

    .local v10, "top":F
    const/4 v11, 0x0

    .local v11, "width":F
    const/4 v7, 0x0

    .line 2657
    .local v7, "height":F
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCurrentOrientation:I

    packed-switch v0, :pswitch_data_0

    .line 2705
    const-string v0, "TwGLUltraWideShotMenu"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onLivePreviewData : invalid orientation "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCurrentOrientation:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2643
    .end local v7    # "height":F
    .end local v8    # "left":F
    .end local v10    # "top":F
    .end local v11    # "width":F
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->isUltraWideShotCapturing()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2644
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCurrentOrientation:I

    if-eq v0, v2, :cond_6

    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCurrentOrientation:I

    if-eq v0, v1, :cond_6

    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getGLContext()Lcom/sec/android/glview/TwGLContext;

    invoke-static {}, Lcom/sec/android/glview/TwGLContext;->getLastOrientation()I

    move-result v0

    if-eq v0, v2, :cond_6

    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getGLContext()Lcom/sec/android/glview/TwGLContext;

    invoke-static {}, Lcom/sec/android/glview/TwGLContext;->getLastOrientation()I

    move-result v0

    if-ne v0, v1, :cond_4

    .line 2647
    :cond_6
    const/16 v0, 0xb4

    const/4 v1, 0x0

    invoke-static {v6, v0, v1}, Lcom/sec/android/app/camera/Util;->rotateAndMirror(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;

    move-result-object v6

    goto :goto_1

    .line 2660
    .restart local v7    # "height":F
    .restart local v8    # "left":F
    .restart local v10    # "top":F
    .restart local v11    # "width":F
    :pswitch_0
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureCount:I

    if-gt v0, v2, :cond_d

    .line 2661
    sget v11, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_SINGLE_WIDTH:F

    .line 2662
    sget v7, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_SINGLE_HEIGHT:F

    .line 2663
    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_SINGLE_X:F

    .line 2664
    const/4 v10, 0x0

    .line 2709
    :cond_7
    :goto_2
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLivePreviewGroup:Lcom/sec/android/glview/TwGLViewGroup;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLivePreviewGroup:Lcom/sec/android/glview/TwGLViewGroup;

    invoke-virtual {v0}, Lcom/sec/android/glview/TwGLViewGroup;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 2710
    iget-object v12, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLivePreviewGroup:Lcom/sec/android/glview/TwGLViewGroup;

    monitor-enter v12
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2711
    :try_start_3
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLivePreview:Lcom/sec/android/glview/TwGLBitmapTexture;

    if-eqz v0, :cond_8

    .line 2712
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLivePreviewGroup:Lcom/sec/android/glview/TwGLViewGroup;

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLivePreview:Lcom/sec/android/glview/TwGLBitmapTexture;

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLViewGroup;->removeView(Lcom/sec/android/glview/TwGLView;)V

    .line 2713
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLivePreview:Lcom/sec/android/glview/TwGLBitmapTexture;

    invoke-virtual {v0}, Lcom/sec/android/glview/TwGLBitmapTexture;->clear()V

    .line 2715
    :cond_8
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPreviewFocusRect:Lcom/sec/android/glview/TwGLNinePatch;

    if-eqz v0, :cond_9

    .line 2716
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLivePreviewGroup:Lcom/sec/android/glview/TwGLViewGroup;

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPreviewFocusRect:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLViewGroup;->removeView(Lcom/sec/android/glview/TwGLView;)V

    .line 2717
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPreviewFocusRect:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v0}, Lcom/sec/android/glview/TwGLNinePatch;->clear()V

    .line 2720
    :cond_9
    new-instance v0, Lcom/sec/android/glview/TwGLBitmapTexture;

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGLParentView:Lcom/sec/android/glview/TwGLViewGroup;

    invoke-virtual {v1}, Lcom/sec/android/glview/TwGLViewGroup;->getContext()Lcom/sec/android/glview/TwGLContext;

    move-result-object v1

    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_RECT_THICKNESS:F

    add-float/2addr v2, v8

    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_RECT_THICKNESS:F

    add-float/2addr v3, v10

    const/high16 v4, 0x40000000    # 2.0f

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_RECT_THICKNESS:F

    mul-float/2addr v4, v5

    sub-float v4, v11, v4

    const/high16 v5, 0x40000000    # 2.0f

    sget v13, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_RECT_THICKNESS:F

    mul-float/2addr v5, v13

    sub-float v5, v7, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/glview/TwGLBitmapTexture;-><init>(Lcom/sec/android/glview/TwGLContext;FFFFLandroid/graphics/Bitmap;)V

    iput-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLivePreview:Lcom/sec/android/glview/TwGLBitmapTexture;

    .line 2721
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLivePreviewGroup:Lcom/sec/android/glview/TwGLViewGroup;

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLivePreview:Lcom/sec/android/glview/TwGLBitmapTexture;

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLViewGroup;->addView(Lcom/sec/android/glview/TwGLView;)V

    .line 2723
    iget-boolean v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mIsStartCapture:Z

    if-eqz v0, :cond_12

    .line 2724
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPreviewFocusRect:Lcom/sec/android/glview/TwGLNinePatch;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPreviewFocusRect:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v0}, Lcom/sec/android/glview/TwGLNinePatch;->getVisibility()I

    move-result v0

    if-nez v0, :cond_a

    .line 2725
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPreviewFocusRect:Lcom/sec/android/glview/TwGLNinePatch;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLNinePatch;->setVisibility(I)V

    .line 2727
    :cond_a
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureFocusRect:Lcom/sec/android/glview/TwGLNinePatch;

    if-eqz v0, :cond_b

    iget-boolean v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mUltraWideShotWarning:Z

    if-nez v0, :cond_b

    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->isRectGuideVisible()Z

    move-result v0

    if-nez v0, :cond_b

    .line 2728
    iget-boolean v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mIsUltraWideShotCapturing:Z

    if-eqz v0, :cond_11

    .line 2729
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureFocusRect:Lcom/sec/android/glview/TwGLNinePatch;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLNinePatch;->setVisibility(I)V

    .line 2730
    const/16 v0, 0x8

    new-array v9, v0, [F

    const/4 v0, 0x0

    iget v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLeftTopX:F

    aput v1, v9, v0

    const/4 v0, 0x1

    iget v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLeftTopY:F

    aput v1, v9, v0

    const/4 v0, 0x2

    iget v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mRightTopX:F

    aput v1, v9, v0

    const/4 v0, 0x3

    iget v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mRightTopY:F

    aput v1, v9, v0

    const/4 v0, 0x4

    iget v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mRightBottomX:F

    aput v1, v9, v0

    const/4 v0, 0x5

    iget v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mRightBottomY:F

    aput v1, v9, v0

    const/4 v0, 0x6

    iget v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLeftBottomX:F

    aput v1, v9, v0

    const/4 v0, 0x7

    iget v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLeftBottomY:F

    aput v1, v9, v0

    .line 2731
    .local v9, "point":[F
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mTrapezoidCaptureFocusRect:Lcom/sec/android/app/camera/glwidget/TwGLTrapezoid;

    iget v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWidth:F

    iget v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mHeight:F

    invoke-virtual {v0, v1, v2, v9}, Lcom/sec/android/app/camera/glwidget/TwGLTrapezoid;->setRect(FF[F)V

    .line 2732
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mTrapezoidCaptureFocusRect:Lcom/sec/android/app/camera/glwidget/TwGLTrapezoid;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/camera/glwidget/TwGLTrapezoid;->setVisibility(I)V

    .line 2745
    .end local v9    # "point":[F
    :cond_b
    :goto_3
    iget-boolean v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mChanged:Z

    if-eqz v0, :cond_c

    .line 2746
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->refreshBackgroundRect()V

    .line 2748
    :cond_c
    monitor-exit v12

    goto/16 :goto_0

    :catchall_1
    move-exception v0

    monitor-exit v12
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    throw v0

    .line 2666
    :cond_d
    iget v11, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPreviewThumbnailWidth:F

    .line 2667
    sget v7, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_SINGLE_HEIGHT:F

    .line 2668
    iget v8, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPreviewThumbnailLeft:F

    .line 2669
    const/4 v10, 0x0

    .line 2670
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mDetectedDirection:I

    if-ne v0, v3, :cond_e

    .line 2671
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mFocusRectLeft:F

    sget v1, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_LANDSCAPE_X:F

    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_SINGLE_X:F

    add-float/2addr v1, v2

    cmpl-float v0, v0, v1

    if-lez v0, :cond_7

    .line 2672
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mStartCheckWarning:Z

    goto/16 :goto_2

    .line 2674
    :cond_e
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mDetectedDirection:I

    if-ne v0, v2, :cond_7

    .line 2675
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mFocusRectLeft:F

    sget v1, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_LANDSCAPE_X:F

    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_SINGLE_X:F

    add-float/2addr v1, v2

    cmpg-float v0, v0, v1

    if-gez v0, :cond_7

    .line 2676
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mStartCheckWarning:Z

    goto/16 :goto_2

    .line 2683
    :pswitch_1
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureCount:I

    if-gt v0, v2, :cond_f

    .line 2684
    sget v11, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_SINGLE_PORTRAIT_WIDTH:F

    .line 2685
    sget v7, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_SINGLE_PORTRAIT_HEIGHT:F

    .line 2686
    const/4 v8, 0x0

    .line 2687
    sget v10, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_SINGLE_PORTRAIT_Y:F

    goto/16 :goto_2

    .line 2689
    :cond_f
    sget v11, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_SINGLE_PORTRAIT_WIDTH:F

    .line 2690
    iget v7, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPreviewThumbnailHeight:F

    .line 2691
    const/4 v8, 0x0

    .line 2692
    iget v10, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPreviewThumbnailTop:F

    .line 2693
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mDetectedDirection:I

    if-ne v0, v3, :cond_10

    .line 2694
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mFocusRectTop:F

    sget v1, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_PORTRAIT_Y:F

    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_SINGLE_PORTRAIT_Y:F

    add-float/2addr v1, v2

    cmpg-float v0, v0, v1

    if-gez v0, :cond_7

    .line 2695
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mStartCheckWarning:Z

    goto/16 :goto_2

    .line 2697
    :cond_10
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mDetectedDirection:I

    if-ne v0, v2, :cond_7

    .line 2698
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mFocusRectTop:F

    sget v1, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_PORTRAIT_Y:F

    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_SINGLE_PORTRAIT_Y:F

    add-float/2addr v1, v2

    cmpl-float v0, v0, v1

    if-lez v0, :cond_7

    .line 2699
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mStartCheckWarning:Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_2

    .line 2734
    :cond_11
    :try_start_5
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureFocusRect:Lcom/sec/android/glview/TwGLNinePatch;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLNinePatch;->setVisibility(I)V

    goto :goto_3

    .line 2738
    :cond_12
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureFocusRect:Lcom/sec/android/glview/TwGLNinePatch;

    if-eqz v0, :cond_13

    .line 2739
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureFocusRect:Lcom/sec/android/glview/TwGLNinePatch;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLNinePatch;->setVisibility(I)V

    .line 2741
    :cond_13
    new-instance v0, Lcom/sec/android/glview/TwGLNinePatch;

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGLParentView:Lcom/sec/android/glview/TwGLViewGroup;

    invoke-virtual {v1}, Lcom/sec/android/glview/TwGLViewGroup;->getContext()Lcom/sec/android/glview/TwGLContext;

    move-result-object v1

    const v2, 0x7f020200

    invoke-direct {v0, v1, v8, v10, v2}, Lcom/sec/android/glview/TwGLNinePatch;-><init>(Lcom/sec/android/glview/TwGLContext;FFI)V

    iput-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPreviewFocusRect:Lcom/sec/android/glview/TwGLNinePatch;

    .line 2742
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPreviewFocusRect:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v0, v11, v7}, Lcom/sec/android/glview/TwGLNinePatch;->setSize(FF)V

    .line 2743
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLivePreviewGroup:Lcom/sec/android/glview/TwGLViewGroup;

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPreviewFocusRect:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLViewGroup;->addView(Lcom/sec/android/glview/TwGLView;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto/16 :goto_3

    .line 2657
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onOrientationChanged(I)V
    .locals 6
    .param p1, "orientation"    # I

    .prologue
    const/4 v5, 0x7

    const/4 v3, 0x1

    const/16 v4, 0xa

    .line 2818
    const-string v0, "TwGLUltraWideShotMenu"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onOrientationChanged - mCurrentOrientation : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCurrentOrientation:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", orientation:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 2820
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->getShootingMode()I

    move-result v0

    const/16 v1, 0x34

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->isContextMenuOpened()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getEngine()Lcom/sec/android/app/camera/CommonEngine;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CommonEngine;->isRecording()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2822
    :cond_0
    const-string v0, "TwGLUltraWideShotMenu"

    const-string v1, "onOrientationChanged - Already mActivityContext is null or This is not a WideSelfie mode"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 2823
    iput p1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCurrentOrientation:I

    .line 2882
    :cond_1
    :goto_0
    return-void

    .line 2830
    :cond_2
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCurrentOrientation:I

    if-eq v0, p1, :cond_3

    .line 2831
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->clearUltraWideRect()V

    .line 2833
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->isUltraWideShotCapturing()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2834
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mDetectedDirection:I

    if-nez v0, :cond_6

    .line 2835
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCurrentOrientation:I

    if-eq v0, p1, :cond_5

    .line 2836
    const-string v0, "TwGLUltraWideShotMenu"

    const-string v1, "onOrientationChanged Stop case - orientation changed"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 2837
    iput p1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCurrentOrientation:I

    .line 2838
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->reset()V

    .line 2839
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mUltraWideShotWarning:Z

    .line 2840
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getEngine()Lcom/sec/android/app/camera/CommonEngine;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CommonEngine;->isTimerCounting()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2841
    const-string v0, "TwGLUltraWideShotMenu"

    const-string v1, "onOrientationChanged isTimerCounting return"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 2842
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->refreshLivePreviewPosition()V

    .line 2843
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->finishTimerCount()V

    goto :goto_0

    .line 2846
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->onUltraWideShotCaptureCancelled()V

    .line 2847
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/camera/Camera;->showUltraWideShotToastPopup(I)V

    .line 2848
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mUltraWideShotMsgHandler:Landroid/os/Handler;

    if-eqz v0, :cond_5

    .line 2849
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mUltraWideShotMsgHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 2850
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mUltraWideShotMsgHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 2870
    :cond_5
    :goto_1
    iput p1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCurrentOrientation:I

    .line 2877
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->refreshLivePreviewPosition()V

    .line 2879
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getEngine()Lcom/sec/android/app/camera/CommonEngine;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CommonEngine;->isRecording()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2880
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    iget v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCurrentOrientation:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/camera/Camera;->showCropArea(I)V

    goto :goto_0

    .line 2854
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getEngine()Lcom/sec/android/app/camera/CommonEngine;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CommonEngine;->isUltraWideShotStopping()Z

    move-result v0

    if-nez v0, :cond_7

    .line 2855
    invoke-virtual {p0, v3}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->setCaptureError(Z)V

    .line 2856
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getEngine()Lcom/sec/android/app/camera/CommonEngine;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CommonEngine;->scheduleStopUltraWideShot()V

    .line 2858
    :cond_7
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->hideStopButton()V

    goto :goto_1

    .line 2861
    :cond_8
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mUltraWideShotMsgHandler:Landroid/os/Handler;

    if-eqz v0, :cond_5

    .line 2862
    invoke-virtual {p0, v3}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->setSkipCapture(Z)V

    .line 2863
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mUltraWideShotMsgHandler:Landroid/os/Handler;

    invoke-virtual {v0, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 2864
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mUltraWideShotMsgHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x5dc

    invoke-virtual {v0, v5, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 2865
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mUltraWideShotMsgHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 2866
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mUltraWideShotMsgHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_1
.end method

.method public onPause()V
    .locals 3

    .prologue
    .line 1208
    const-string v0, "TwGLUltraWideShotMenu"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onPause - capture count : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->getCaptureProgressIncreased()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1209
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->getCaptureProgressIncreased()I

    move-result v0

    if-lez v0, :cond_0

    .line 1210
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->onUltraWideShotCaptureCancelled()V

    .line 1212
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->reset()V

    .line 1213
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 763
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->showPreviewGroup()V

    .line 764
    return-void
.end method

.method public onShow()V
    .locals 2

    .prologue
    .line 914
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getEngine()Lcom/sec/android/app/camera/CommonEngine;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CommonEngine;->getRequestQueue()Lcom/sec/android/app/camera/CeRequestQueue;

    move-result-object v0

    const/16 v1, 0x66

    invoke-virtual {v0, v1}, Lcom/sec/android/app/camera/CeRequestQueue;->searchRequest(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 925
    :cond_0
    :goto_0
    return-void

    .line 918
    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mIsStartCapture:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureCount:I

    if-gtz v0, :cond_0

    .line 921
    :cond_2
    iget-boolean v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mEncodingProgress:Z

    if-nez v0, :cond_0

    .line 922
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    iget v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCurrentOrientation:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/camera/Camera;->showCropArea(I)V

    .line 923
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->showPreviewGroup()V

    goto :goto_0
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 1216
    const-string v0, "TwGLUltraWideShotMenu"

    const-string v1, "onStop"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1217
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->stopCancelTimer()V

    .line 1218
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->hideStopButton()V

    .line 1219
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLivePreview:Lcom/sec/android/glview/TwGLBitmapTexture;

    if-eqz v0, :cond_0

    .line 1220
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLivePreviewGroup:Lcom/sec/android/glview/TwGLViewGroup;

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLivePreview:Lcom/sec/android/glview/TwGLBitmapTexture;

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLViewGroup;->removeView(Lcom/sec/android/glview/TwGLView;)V

    .line 1221
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLivePreview:Lcom/sec/android/glview/TwGLBitmapTexture;

    invoke-virtual {v0}, Lcom/sec/android/glview/TwGLBitmapTexture;->clear()V

    .line 1223
    :cond_0
    return-void
.end method

.method public onUltraWideDirectionChanged(I)V
    .locals 7
    .param p1, "direction"    # I

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    .line 1237
    const-string v2, "TwGLUltraWideShotMenu"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onUltraWideDirectionChanged: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1238
    iput p1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCurrentDirection:I

    .line 1240
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLivePreview:Lcom/sec/android/glview/TwGLBitmapTexture;

    if-nez v2, :cond_1

    .line 1302
    :cond_0
    :goto_0
    return-void

    .line 1242
    :cond_1
    iget-boolean v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mChanged:Z

    if-nez v2, :cond_2

    .line 1243
    iget v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mDetectedDirection:I

    if-eqz v2, :cond_2

    iget v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mDetectedDirection:I

    if-eq v2, p1, :cond_2

    .line 1244
    iput-boolean v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mChanged:Z

    .line 1247
    invoke-virtual {p0, p1}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->showGuideArrow(I)V

    .line 1252
    :cond_2
    if-eqz p1, :cond_0

    .line 1253
    iget v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mDetectedDirection:I

    if-nez v2, :cond_0

    .line 1254
    const/4 v1, 0x0

    .local v1, "width":F
    const/4 v0, 0x0

    .line 1255
    .local v0, "height":F
    iget v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCurrentOrientation:I

    packed-switch v2, :pswitch_data_0

    .line 1267
    :goto_1
    iget v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mDetectedDirection:I

    if-eq v2, p1, :cond_4

    .line 1268
    iput p1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mDetectedDirection:I

    .line 1273
    iget v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mDetectedDirection:I

    invoke-virtual {p0, v2}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->showGuideArrow(I)V

    .line 1275
    iget v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCurrentOrientation:I

    if-eqz v2, :cond_3

    iget v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCurrentOrientation:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_6

    .line 1276
    :cond_3
    iget v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mDetectedDirection:I

    if-ne v2, v6, :cond_5

    .line 1277
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->hideWarningText()V

    .line 1278
    invoke-virtual {p0, v5}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->showGuideText(I)V

    .line 1294
    :cond_4
    :goto_2
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningBox:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v2, v1, v0}, Lcom/sec/android/glview/TwGLImage;->setSize(FF)V

    .line 1295
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureFocusRect:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v2, v1, v0}, Lcom/sec/android/glview/TwGLNinePatch;->setSize(FF)V

    .line 1296
    const-string v2, "TwGLUltraWideShotMenu"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onUltraWideDirectionChanged: mDetectedDirection: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mDetectedDirection:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1298
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->showStopButton()V

    goto :goto_0

    .line 1258
    :pswitch_0
    sget v1, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_SINGLE_WIDTH:F

    .line 1259
    sget v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_SINGLE_HEIGHT:F

    .line 1260
    goto :goto_1

    .line 1263
    :pswitch_1
    sget v1, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_SINGLE_PORTRAIT_WIDTH:F

    .line 1264
    sget v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_SINGLE_PORTRAIT_HEIGHT:F

    goto :goto_1

    .line 1279
    :cond_5
    iget v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mDetectedDirection:I

    if-ne v2, v5, :cond_4

    .line 1280
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->hideWarningText()V

    .line 1281
    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->showGuideText(I)V

    goto :goto_2

    .line 1285
    :cond_6
    iget v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mDetectedDirection:I

    if-ne v2, v6, :cond_7

    .line 1286
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->hideWarningText()V

    .line 1287
    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->showGuideText(I)V

    goto :goto_2

    .line 1288
    :cond_7
    iget v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mDetectedDirection:I

    if-ne v2, v5, :cond_4

    .line 1289
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->hideWarningText()V

    .line 1290
    invoke-virtual {p0, v5}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->showGuideText(I)V

    goto :goto_2

    .line 1255
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onUltraWideRectChanged([B)V
    .locals 42
    .param p1, "data"    # [B

    .prologue
    .line 1455
    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v4}, Lcom/sec/android/app/camera/Util;->byteArrayToInt([BI)I

    move-result v17

    .line 1456
    .local v17, "nLeft":I
    const/4 v4, 0x4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, Lcom/sec/android/app/camera/Util;->byteArrayToInt([BI)I

    move-result v29

    .line 1457
    .local v29, "nTop":I
    const/16 v4, 0x8

    move-object/from16 v0, p1

    invoke-static {v0, v4}, Lcom/sec/android/app/camera/Util;->byteArrayToInt([BI)I

    move-result v24

    .line 1458
    .local v24, "nRight":I
    const/16 v4, 0xc

    move-object/from16 v0, p1

    invoke-static {v0, v4}, Lcom/sec/android/app/camera/Util;->byteArrayToInt([BI)I

    move-result v16

    .line 1459
    .local v16, "nBottom":I
    const/16 v4, 0x10

    move-object/from16 v0, p1

    invoke-static {v0, v4}, Lcom/sec/android/app/camera/Util;->byteArrayToInt([BI)I

    move-result v22

    .line 1460
    .local v22, "nOffsetX":I
    const/16 v4, 0x14

    move-object/from16 v0, p1

    invoke-static {v0, v4}, Lcom/sec/android/app/camera/Util;->byteArrayToInt([BI)I

    move-result v23

    .line 1461
    .local v23, "nOffsetY":I
    const/16 v4, 0x18

    move-object/from16 v0, p1

    invoke-static {v0, v4}, Lcom/sec/android/app/camera/Util;->byteArrayToInt([BI)I

    move-result v20

    .line 1462
    .local v20, "nLeftTopX":I
    const/16 v4, 0x1c

    move-object/from16 v0, p1

    invoke-static {v0, v4}, Lcom/sec/android/app/camera/Util;->byteArrayToInt([BI)I

    move-result v21

    .line 1463
    .local v21, "nLeftTopY":I
    const/16 v4, 0x20

    move-object/from16 v0, p1

    invoke-static {v0, v4}, Lcom/sec/android/app/camera/Util;->byteArrayToInt([BI)I

    move-result v27

    .line 1464
    .local v27, "nRightTopX":I
    const/16 v4, 0x24

    move-object/from16 v0, p1

    invoke-static {v0, v4}, Lcom/sec/android/app/camera/Util;->byteArrayToInt([BI)I

    move-result v28

    .line 1465
    .local v28, "nRightTopY":I
    const/16 v4, 0x28

    move-object/from16 v0, p1

    invoke-static {v0, v4}, Lcom/sec/android/app/camera/Util;->byteArrayToInt([BI)I

    move-result v18

    .line 1466
    .local v18, "nLeftBottomX":I
    const/16 v4, 0x2c

    move-object/from16 v0, p1

    invoke-static {v0, v4}, Lcom/sec/android/app/camera/Util;->byteArrayToInt([BI)I

    move-result v19

    .line 1467
    .local v19, "nLeftBottomY":I
    const/16 v4, 0x30

    move-object/from16 v0, p1

    invoke-static {v0, v4}, Lcom/sec/android/app/camera/Util;->byteArrayToInt([BI)I

    move-result v25

    .line 1468
    .local v25, "nRightBottomX":I
    const/16 v4, 0x34

    move-object/from16 v0, p1

    invoke-static {v0, v4}, Lcom/sec/android/app/camera/Util;->byteArrayToInt([BI)I

    move-result v26

    .line 1469
    .local v26, "nRightBottomY":I
    const/16 v31, 0x0

    .line 1470
    .local v31, "rect_x":F
    const/16 v32, 0x0

    .line 1472
    .local v32, "rect_y":F
    const/4 v14, 0x0

    .line 1473
    .local v14, "arrow_x":F
    const/4 v15, 0x0

    .line 1475
    .local v15, "arrow_y":F
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v4}, Lcom/sec/android/app/camera/Camera;->getEngine()Lcom/sec/android/app/camera/CommonEngine;

    move-result-object v4

    if-nez v4, :cond_1

    .line 1779
    :cond_0
    :goto_0
    return-void

    .line 1478
    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLivePreviewGroup:Lcom/sec/android/glview/TwGLViewGroup;

    invoke-virtual {v4}, Lcom/sec/android/glview/TwGLViewGroup;->getVisibility()I

    move-result v4

    const/4 v5, 0x4

    if-eq v4, v5, :cond_0

    .line 1481
    move/from16 v0, v22

    int-to-float v4, v0

    move/from16 v0, v23

    int-to-float v5, v0

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v5}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->setFocusRectLeftTop(FF)V

    .line 1482
    move/from16 v0, v17

    int-to-float v4, v0

    move/from16 v0, v29

    int-to-float v5, v0

    move/from16 v0, v24

    int-to-float v6, v0

    move/from16 v0, v16

    int-to-float v7, v0

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v5, v6, v7}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->setPreviewThumbnailLeftTop(FFFF)V

    .line 1483
    move/from16 v0, v20

    int-to-float v5, v0

    move/from16 v0, v21

    int-to-float v6, v0

    move/from16 v0, v27

    int-to-float v7, v0

    move/from16 v0, v28

    int-to-float v8, v0

    move/from16 v0, v25

    int-to-float v9, v0

    move/from16 v0, v26

    int-to-float v10, v0

    move/from16 v0, v18

    int-to-float v11, v0

    move/from16 v0, v19

    int-to-float v12, v0

    move-object/from16 v4, p0

    invoke-virtual/range {v4 .. v12}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->setTrapezoidPosition(FFFFFFFF)V

    .line 1485
    const-string v4, "TwGLUltraWideShotMenu"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onUltraWideRectChanged: mDetectedDirection: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mDetectedDirection:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1486
    const-string v4, "TwGLUltraWideShotMenu"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onUltraWideRectChanged: mFocusRectCenterX :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mFocusRectCenterX:F

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " mFocusRectCenterY : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mFocusRectCenterY:F

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1487
    const-string v4, "TwGLUltraWideShotMenu"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onUltraWideRectChanged: mFocusRectLeft :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mFocusRectLeft:F

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " mFocusRectTop : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mFocusRectTop:F

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1489
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mFocusRectLeft:F

    move/from16 v31, v0

    .line 1490
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mFocusRectTop:F

    move/from16 v32, v0

    .line 1492
    sget v13, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->WARNING_LEVEL_NONE:I

    .line 1494
    .local v13, "WarningLevel":I
    invoke-static/range {v22 .. v22}, Ljava/lang/Math;->abs(I)I

    move-result v4

    int-to-double v0, v4

    move-wide/from16 v38, v0

    .line 1495
    .local v38, "xDistance":D
    invoke-static/range {v23 .. v23}, Ljava/lang/Math;->abs(I)I

    move-result v4

    int-to-double v0, v4

    move-wide/from16 v40, v0

    .line 1496
    .local v40, "yDistance":D
    move-wide/from16 v0, v40

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPotraitOffset:D

    .line 1497
    move-wide/from16 v0, v38

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLandscapeOffset:D

    .line 1498
    const/16 v36, 0x0

    .line 1499
    .local v36, "warningDistance":F
    sget v33, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->WARNING_NONE:I

    .line 1501
    .local v33, "warningDirection":I
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mMovingDetection:Z

    if-nez v4, :cond_3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCurrentOrientation:I

    if-eqz v4, :cond_2

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCurrentOrientation:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_3

    :cond_2
    const-wide/high16 v4, 0x4024000000000000L    # 10.0

    cmpl-double v4, v40, v4

    if-gtz v4, :cond_5

    :cond_3
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCurrentOrientation:I

    const/4 v5, 0x1

    if-eq v4, v5, :cond_4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCurrentOrientation:I

    const/4 v5, 0x3

    if-ne v4, v5, :cond_6

    :cond_4
    const-wide/high16 v4, 0x4024000000000000L    # 10.0

    cmpl-double v4, v38, v4

    if-lez v4, :cond_6

    .line 1504
    :cond_5
    const-string v4, "TwGLUltraWideShotMenu"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "xDistance : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, v38

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " , yDistance : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, v40

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1505
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->hideGuideRect()V

    .line 1506
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mMovingDetection:Z

    .line 1507
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mAnimationDirection:Z

    .line 1529
    :cond_6
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCurrentOrientation:I

    if-eqz v4, :cond_7

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCurrentOrientation:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_f

    .line 1530
    :cond_7
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mStartCheckWarning:Z

    if-eqz v4, :cond_c

    .line 1531
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mDetectedDirection:I

    const/4 v5, 0x1

    if-ne v4, v5, :cond_b

    .line 1532
    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_LANDSCAPE_X:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPreviewThumbnailLeft:F

    add-float/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mFocusRectLeft:F

    sub-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    float-to-double v0, v4

    move-wide/from16 v38, v0

    .line 1544
    :goto_1
    sget v36, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_SINGLE_HEIGHT:F

    .line 1562
    :goto_2
    invoke-static/range {v38 .. v41}, Ljava/lang/Math;->max(DD)D

    move-result-wide v34

    .line 1564
    .local v34, "totalDistance":D
    move/from16 v0, v36

    float-to-double v4, v0

    const-wide v6, 0x3fb999999999999aL    # 0.1

    mul-double/2addr v4, v6

    cmpg-double v4, v34, v4

    if-gez v4, :cond_14

    .line 1565
    sget v13, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->WARNING_LEVEL_NONE:I

    .line 1573
    :goto_3
    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->WARNING_LEVEL_NONE:I

    if-ne v13, v4, :cond_8

    .line 1574
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->restartCancelTimer()V

    .line 1580
    :cond_8
    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->prepareWarningArrow(I)V

    .line 1582
    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->WARNING_LEVEL_LOW:I

    if-lt v13, v4, :cond_1f

    .line 1618
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCurrentOrientation:I

    packed-switch v4, :pswitch_data_0

    .line 1700
    :cond_9
    :goto_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureFocusRect:Lcom/sec/android/glview/TwGLNinePatch;

    const/4 v5, 0x4

    invoke-virtual {v4, v5}, Lcom/sec/android/glview/TwGLNinePatch;->setVisibility(I)V

    .line 1702
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->isRectGuideVisible()Z

    move-result v4

    if-nez v4, :cond_a

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->getFocusRectDistance()F

    move-result v4

    const/high16 v5, 0x3f800000    # 1.0f

    cmpl-float v4, v4, v5

    if-lez v4, :cond_a

    .line 1703
    const-string v4, "TwGLUltraWideShotMenu"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onUltraWideRectChanged  Warning mLeftTopX : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLeftTopX:F

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", mLeftTopY = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLeftTopY:F

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", mRightTopX : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mRightTopX:F

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", mRightTopY : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mRightTopY:F

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", mRightBottomX : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mRightBottomX:F

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", mLeftBottomX : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLeftBottomX:F

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", mLeftBottomY : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLeftBottomX:F

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1704
    const-string v4, "TwGLUltraWideShotMenu"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onUltraWideRectChanged Warning mWidth : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWidth:F

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", mHeight : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mHeight:F

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1705
    const/16 v4, 0x8

    new-array v0, v4, [F

    move-object/from16 v30, v0

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLeftTopX:F

    aput v5, v30, v4

    const/4 v4, 0x1

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLeftTopY:F

    aput v5, v30, v4

    const/4 v4, 0x2

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mRightTopX:F

    aput v5, v30, v4

    const/4 v4, 0x3

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mRightTopY:F

    aput v5, v30, v4

    const/4 v4, 0x4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mRightBottomX:F

    aput v5, v30, v4

    const/4 v4, 0x5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mRightBottomY:F

    aput v5, v30, v4

    const/4 v4, 0x6

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLeftBottomX:F

    aput v5, v30, v4

    const/4 v4, 0x7

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLeftBottomY:F

    aput v5, v30, v4

    .line 1706
    .local v30, "point":[F
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mTrapezoidCaptureFocusRect:Lcom/sec/android/app/camera/glwidget/TwGLTrapezoid;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWidth:F

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mHeight:F

    move-object/from16 v0, v30

    invoke-virtual {v4, v5, v6, v0}, Lcom/sec/android/app/camera/glwidget/TwGLTrapezoid;->setRect(FF[F)V

    .line 1707
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mTrapezoidCaptureFocusRect:Lcom/sec/android/app/camera/glwidget/TwGLTrapezoid;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/sec/android/app/camera/glwidget/TwGLTrapezoid;->setVisibility(I)V

    .line 1715
    .end local v30    # "point":[F
    :cond_a
    const/4 v4, 0x3

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->showGuideArrow(I)V

    .line 1770
    :goto_5
    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->WARNING_LEVEL_STOP:I

    if-ne v13, v4, :cond_0

    .line 1771
    const-string v4, "TwGLUltraWideShotMenu"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onUltraWideRectChanged: Stop case - totalDistance:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, v34

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1772
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mUltraWideShotWarning:Z

    .line 1773
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->hideWarning()V

    .line 1774
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->hideWarningArrow()V

    .line 1775
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->stopUltraWideShotSound()V

    .line 1776
    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->setCaptureError(Z)V

    .line 1777
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v4}, Lcom/sec/android/app/camera/Camera;->getEngine()Lcom/sec/android/app/camera/CommonEngine;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/camera/CommonEngine;->scheduleStopUltraWideShot()V

    goto/16 :goto_0

    .line 1534
    .end local v34    # "totalDistance":D
    :cond_b
    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_LANDSCAPE_X:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPreviewThumbnailLeft:F

    add-float/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPreviewThumbnailWidth:F

    add-float/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mFocusRectLeft:F

    sget v6, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_SINGLE_WIDTH:F

    add-float/2addr v5, v6

    sub-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    float-to-double v0, v4

    move-wide/from16 v38, v0

    goto/16 :goto_1

    .line 1537
    :cond_c
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mDetectedDirection:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_d

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mFocusRectLeft:F

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mBackgroundRect:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v5}, Lcom/sec/android/glview/TwGLNinePatch;->getCurrentLeft()F

    move-result v5

    cmpg-float v4, v4, v5

    if-gez v4, :cond_d

    .line 1538
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mBackgroundRect:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v4}, Lcom/sec/android/glview/TwGLNinePatch;->getCurrentLeft()F

    move-result v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mFocusRectLeft:F

    sub-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    float-to-double v0, v4

    move-wide/from16 v38, v0

    goto/16 :goto_1

    .line 1539
    :cond_d
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mDetectedDirection:I

    const/4 v5, 0x1

    if-ne v4, v5, :cond_e

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mFocusRectLeft:F

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_SINGLE_WIDTH:F

    add-float/2addr v4, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mBackgroundRect:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v5}, Lcom/sec/android/glview/TwGLNinePatch;->getCurrentRight()F

    move-result v5

    cmpl-float v4, v4, v5

    if-lez v4, :cond_e

    .line 1540
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mBackgroundRect:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v4}, Lcom/sec/android/glview/TwGLNinePatch;->getCurrentRight()F

    move-result v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mFocusRectLeft:F

    sget v6, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_SINGLE_WIDTH:F

    add-float/2addr v5, v6

    sub-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    float-to-double v0, v4

    move-wide/from16 v38, v0

    goto/16 :goto_1

    .line 1542
    :cond_e
    const-wide/16 v38, 0x0

    goto/16 :goto_1

    .line 1546
    :cond_f
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mStartCheckWarning:Z

    if-eqz v4, :cond_11

    .line 1547
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mDetectedDirection:I

    const/4 v5, 0x1

    if-ne v4, v5, :cond_10

    .line 1548
    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_PORTRAIT_Y:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPreviewThumbnailTop:F

    add-float/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPreviewThumbnailHeight:F

    add-float/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mFocusRectTop:F

    sget v6, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_SINGLE_PORTRAIT_HEIGHT:F

    add-float/2addr v5, v6

    sub-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    float-to-double v0, v4

    move-wide/from16 v40, v0

    .line 1559
    :goto_6
    sget v36, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_SINGLE_PORTRAIT_WIDTH:F

    goto/16 :goto_2

    .line 1550
    :cond_10
    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_PORTRAIT_Y:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPreviewThumbnailTop:F

    add-float/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mFocusRectTop:F

    sub-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    float-to-double v0, v4

    move-wide/from16 v40, v0

    goto :goto_6

    .line 1552
    :cond_11
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mDetectedDirection:I

    const/4 v5, 0x1

    if-ne v4, v5, :cond_12

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mFocusRectTop:F

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_PORTRAIT_Y:F

    cmpg-float v4, v4, v5

    if-gez v4, :cond_12

    .line 1553
    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_PORTRAIT_Y:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mFocusRectTop:F

    sub-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    float-to-double v0, v4

    move-wide/from16 v40, v0

    goto :goto_6

    .line 1554
    :cond_12
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mDetectedDirection:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_13

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mFocusRectTop:F

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_SINGLE_PORTRAIT_HEIGHT:F

    add-float/2addr v4, v5

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_PORTRAIT_Y:F

    sget v6, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_PORTRAIT_HEIGHT:F

    add-float/2addr v5, v6

    cmpl-float v4, v4, v5

    if-lez v4, :cond_13

    .line 1555
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mFocusRectTop:F

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_SINGLE_PORTRAIT_HEIGHT:F

    add-float/2addr v4, v5

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_PORTRAIT_Y:F

    sget v6, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_PORTRAIT_HEIGHT:F

    add-float/2addr v5, v6

    sub-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    float-to-double v0, v4

    move-wide/from16 v40, v0

    goto :goto_6

    .line 1557
    :cond_13
    const-wide/16 v40, 0x0

    goto :goto_6

    .line 1566
    .restart local v34    # "totalDistance":D
    :cond_14
    move/from16 v0, v36

    float-to-double v4, v0

    const-wide v6, 0x3fd3333333333333L    # 0.3

    mul-double/2addr v4, v6

    cmpg-double v4, v34, v4

    if-gez v4, :cond_15

    .line 1567
    sget v13, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->WARNING_LEVEL_LOW:I

    goto/16 :goto_3

    .line 1568
    :cond_15
    move/from16 v0, v36

    float-to-double v4, v0

    const-wide v6, 0x3fe999999999999aL    # 0.8

    mul-double/2addr v4, v6

    cmpg-double v4, v34, v4

    if-gez v4, :cond_16

    .line 1569
    sget v13, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->WARNING_LEVEL_HIGH:I

    goto/16 :goto_3

    .line 1571
    :cond_16
    sget v13, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->WARNING_LEVEL_STOP:I

    goto/16 :goto_3

    .line 1620
    :pswitch_0
    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_LANDSCAPE_X:F

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_LANDSCAPE_WIDTH:F

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    add-float/2addr v4, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningArrow:[Lcom/sec/android/glview/TwGLImage;

    const/4 v6, 0x3

    aget-object v5, v5, v6

    invoke-virtual {v5}, Lcom/sec/android/glview/TwGLImage;->getWidth()F

    move-result v5

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    sub-float v14, v4, v5

    .line 1621
    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_LANDSCAPE_Y:F

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_LANDSCAPE_HEIGHT:F

    add-float/2addr v4, v5

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->ARROW_MARGIN:F

    add-float v15, v4, v5

    .line 1622
    cmpl-double v4, v38, v40

    if-lez v4, :cond_17

    .line 1623
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mDetectedDirection:I

    packed-switch v4, :pswitch_data_1

    goto/16 :goto_4

    .line 1628
    :pswitch_1
    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->WARNING_LEFT:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v15, v4}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->showWarningArrow(FFI)V

    goto/16 :goto_4

    .line 1625
    :pswitch_2
    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->WARNING_RIGHT:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v15, v4}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->showWarningArrow(FFI)V

    goto/16 :goto_4

    .line 1631
    :cond_17
    if-lez v23, :cond_18

    .line 1632
    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->WARNING_UP:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v15, v4}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->showWarningArrow(FFI)V

    goto/16 :goto_4

    .line 1633
    :cond_18
    if-gez v23, :cond_9

    .line 1634
    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->WARNING_DOWN:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v15, v4}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->showWarningArrow(FFI)V

    goto/16 :goto_4

    .line 1639
    :pswitch_3
    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_LANDSCAPE_X:F

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_LANDSCAPE_WIDTH:F

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    add-float/2addr v4, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningArrow:[Lcom/sec/android/glview/TwGLImage;

    const/4 v6, 0x2

    aget-object v5, v5, v6

    invoke-virtual {v5}, Lcom/sec/android/glview/TwGLImage;->getWidth()F

    move-result v5

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    sub-float v14, v4, v5

    .line 1640
    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_LANDSCAPE_Y:F

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_LANDSCAPE_HEIGHT:F

    add-float/2addr v4, v5

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->ARROW_MARGIN:F

    add-float v15, v4, v5

    .line 1641
    cmpl-double v4, v38, v40

    if-lez v4, :cond_19

    .line 1642
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mDetectedDirection:I

    packed-switch v4, :pswitch_data_2

    goto/16 :goto_4

    .line 1644
    :pswitch_4
    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->WARNING_LEFT:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v15, v4}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->showWarningArrow(FFI)V

    goto/16 :goto_4

    .line 1647
    :pswitch_5
    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->WARNING_RIGHT:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v15, v4}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->showWarningArrow(FFI)V

    goto/16 :goto_4

    .line 1652
    :cond_19
    if-lez v23, :cond_1a

    .line 1653
    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->WARNING_UP:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v15, v4}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->showWarningArrow(FFI)V

    goto/16 :goto_4

    .line 1654
    :cond_1a
    if-gez v23, :cond_9

    .line 1655
    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->WARNING_DOWN:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v15, v4}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->showWarningArrow(FFI)V

    goto/16 :goto_4

    .line 1659
    :pswitch_6
    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_PORTRAIT_X:F

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_SINGLE_PORTRAIT_WIDTH:F

    add-float/2addr v4, v5

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->ARROW_MARGIN:F

    add-float v14, v4, v5

    .line 1660
    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_PORTRAIT_Y:F

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_PORTRAIT_HEIGHT:F

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningArrow:[Lcom/sec/android/glview/TwGLImage;

    const/4 v7, 0x1

    aget-object v6, v6, v7

    invoke-virtual {v6}, Lcom/sec/android/glview/TwGLImage;->getHeight()F

    move-result v6

    sub-float/2addr v5, v6

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    add-float v15, v4, v5

    .line 1661
    cmpl-double v4, v40, v38

    if-lez v4, :cond_1b

    .line 1662
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mDetectedDirection:I

    packed-switch v4, :pswitch_data_3

    goto/16 :goto_4

    .line 1667
    :pswitch_7
    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->WARNING_DOWN:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v15, v4}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->showWarningArrow(FFI)V

    goto/16 :goto_4

    .line 1664
    :pswitch_8
    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->WARNING_UP:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v15, v4}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->showWarningArrow(FFI)V

    goto/16 :goto_4

    .line 1672
    :cond_1b
    if-gez v22, :cond_1c

    .line 1673
    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->WARNING_RIGHT:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v15, v4}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->showWarningArrow(FFI)V

    goto/16 :goto_4

    .line 1674
    :cond_1c
    if-lez v22, :cond_9

    .line 1675
    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->WARNING_LEFT:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v15, v4}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->showWarningArrow(FFI)V

    goto/16 :goto_4

    .line 1679
    :pswitch_9
    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_PORTRAIT_X:F

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_SINGLE_PORTRAIT_WIDTH:F

    add-float/2addr v4, v5

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->ARROW_MARGIN:F

    add-float v14, v4, v5

    .line 1680
    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_PORTRAIT_Y:F

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_PORTRAIT_HEIGHT:F

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningArrow:[Lcom/sec/android/glview/TwGLImage;

    const/4 v7, 0x1

    aget-object v6, v6, v7

    invoke-virtual {v6}, Lcom/sec/android/glview/TwGLImage;->getHeight()F

    move-result v6

    sub-float/2addr v5, v6

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    add-float v15, v4, v5

    .line 1681
    cmpl-double v4, v40, v38

    if-lez v4, :cond_1d

    .line 1682
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mDetectedDirection:I

    packed-switch v4, :pswitch_data_4

    goto/16 :goto_4

    .line 1687
    :pswitch_a
    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->WARNING_DOWN:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v15, v4}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->showWarningArrow(FFI)V

    goto/16 :goto_4

    .line 1684
    :pswitch_b
    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->WARNING_UP:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v15, v4}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->showWarningArrow(FFI)V

    goto/16 :goto_4

    .line 1692
    :cond_1d
    if-gez v22, :cond_1e

    .line 1693
    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->WARNING_RIGHT:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v15, v4}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->showWarningArrow(FFI)V

    goto/16 :goto_4

    .line 1694
    :cond_1e
    if-lez v22, :cond_9

    .line 1695
    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->WARNING_LEFT:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v15, v4}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->showWarningArrow(FFI)V

    goto/16 :goto_4

    .line 1718
    :cond_1f
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->isRectGuideVisible()Z

    move-result v4

    if-nez v4, :cond_20

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->getFocusRectDistance()F

    move-result v4

    const/high16 v5, 0x3f800000    # 1.0f

    cmpl-float v4, v4, v5

    if-lez v4, :cond_20

    .line 1719
    const-string v4, "TwGLUltraWideShotMenu"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onUltraWideRectChanged not Warning mLeftTopX : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLeftTopX:F

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", mLeftTopY = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLeftTopY:F

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", mRightTopX : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mRightTopX:F

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", mRightTopY : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mRightTopY:F

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", mRightBottomX : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mRightBottomX:F

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", mLeftBottomX : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLeftBottomX:F

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", mLeftBottomY : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLeftBottomX:F

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1720
    const-string v4, "TwGLUltraWideShotMenu"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onUltraWideRectChanged not Warning mWidth : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWidth:F

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", mHeight : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mHeight:F

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1721
    const/16 v4, 0x8

    new-array v0, v4, [F

    move-object/from16 v30, v0

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLeftTopX:F

    aput v5, v30, v4

    const/4 v4, 0x1

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLeftTopY:F

    aput v5, v30, v4

    const/4 v4, 0x2

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mRightTopX:F

    aput v5, v30, v4

    const/4 v4, 0x3

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mRightTopY:F

    aput v5, v30, v4

    const/4 v4, 0x4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mRightBottomX:F

    aput v5, v30, v4

    const/4 v4, 0x5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mRightBottomY:F

    aput v5, v30, v4

    const/4 v4, 0x6

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLeftBottomX:F

    aput v5, v30, v4

    const/4 v4, 0x7

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLeftBottomY:F

    aput v5, v30, v4

    .line 1722
    .restart local v30    # "point":[F
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mTrapezoidCaptureFocusRect:Lcom/sec/android/app/camera/glwidget/TwGLTrapezoid;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWidth:F

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mHeight:F

    move-object/from16 v0, v30

    invoke-virtual {v4, v5, v6, v0}, Lcom/sec/android/app/camera/glwidget/TwGLTrapezoid;->setRect(FF[F)V

    .line 1723
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mTrapezoidCaptureFocusRect:Lcom/sec/android/app/camera/glwidget/TwGLTrapezoid;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/sec/android/app/camera/glwidget/TwGLTrapezoid;->setVisibility(I)V

    .line 1738
    .end local v30    # "point":[F
    :cond_20
    move-object/from16 v0, p0

    move/from16 v1, v31

    move/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->isMaxPositionReached(FF)Z

    move-result v4

    if-eqz v4, :cond_23

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideShow:Z

    if-eqz v4, :cond_23

    .line 1742
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCurrentOrientation:I

    if-eqz v4, :cond_21

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCurrentOrientation:I

    const/4 v5, 0x3

    if-ne v4, v5, :cond_25

    .line 1743
    :cond_21
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mDetectedDirection:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_24

    .line 1744
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->hideWarningText()V

    .line 1745
    const/4 v4, 0x2

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->showGuideText(I)V

    .line 1760
    :cond_22
    :goto_7
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->stopLivePreviewHaptic()V

    .line 1761
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    sget v5, Lcom/sec/android/app/camera/Camera;->SHUTTER_SOUND_WIDESELFIE_BOUNDARY_LINE:I

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/app/camera/Camera;->playCameraSound(II)V

    .line 1762
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v4}, Lcom/sec/android/app/camera/Camera;->getGLContext()Lcom/sec/android/glview/TwGLContext;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/sec/android/glview/TwGLContext;->playHaptic(I)V

    .line 1764
    :cond_23
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCurrentDirection:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->showGuideArrow(I)V

    .line 1765
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->hideWarningText()V

    .line 1767
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->hideWarning()V

    goto/16 :goto_5

    .line 1746
    :cond_24
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mDetectedDirection:I

    const/4 v5, 0x1

    if-ne v4, v5, :cond_22

    .line 1747
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->hideWarningText()V

    .line 1748
    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->showGuideText(I)V

    goto :goto_7

    .line 1752
    :cond_25
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mDetectedDirection:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_26

    .line 1753
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->hideWarningText()V

    .line 1754
    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->showGuideText(I)V

    goto :goto_7

    .line 1755
    :cond_26
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mDetectedDirection:I

    const/4 v5, 0x1

    if-ne v4, v5, :cond_22

    .line 1756
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->hideWarningText()V

    .line 1757
    const/4 v4, 0x2

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->showGuideText(I)V

    goto :goto_7

    .line 1618
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_6
        :pswitch_3
        :pswitch_9
    .end packed-switch

    .line 1623
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 1642
    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
    .end packed-switch

    .line 1662
    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_7
        :pswitch_8
    .end packed-switch

    .line 1682
    :pswitch_data_4
    .packed-switch 0x1
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method public onUltraWideSelfieNextCapturePosition(II)V
    .locals 3
    .param p1, "posX"    # I
    .param p2, "posY"    # I

    .prologue
    .line 2437
    int-to-float v0, p1

    sget v1, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_RATIO:F

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mNextCaptureFocusRectCenterX:F

    .line 2438
    int-to-float v0, p2

    sget v1, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_RATIO:F

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mNextCaptureFocusRectCenterY:F

    .line 2439
    const-string v0, "TwGLUltraWideShotMenu"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onUltraWideSelfieNextCapturePosition : posX : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", posY : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 2440
    const-string v0, "TwGLUltraWideShotMenu"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onUltraWideSelfieNextCapturePosition : mNextCaptureFocusRectCenterX : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mNextCaptureFocusRectCenterX:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mNextCaptureFocusRectCenterY : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mNextCaptureFocusRectCenterY:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 2442
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCurrentOrientation:I

    packed-switch v0, :pswitch_data_0

    .line 2455
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->showNextCaptureFocusRect()V

    .line 2456
    return-void

    .line 2445
    :pswitch_0
    sget v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->FOCUS_RECT_LANDSCAPE_X:F

    sget v1, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->NEXT_CAPTURE_FOCUS_LINE_WIDTH:F

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mNextCaptureFocusRectCenterX:F

    add-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mNextCaptureFocusRectLeft:F

    .line 2446
    sget v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_LANDSCAPE_Y:F

    sget v1, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->NEXT_CAPTURE_FOCUS_LINE_WIDTH:F

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mNextCaptureFocusRectTop:F

    goto :goto_0

    .line 2450
    :pswitch_1
    sget v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_PORTRAIT_X:F

    sget v1, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->NEXT_CAPTURE_FOCUS_LINE_WIDTH:F

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mNextCaptureFocusRectLeft:F

    .line 2451
    sget v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->FOCUS_RECT_PORTRAIT_Y:F

    sget v1, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->NEXT_CAPTURE_FOCUS_LINE_WIDTH:F

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mNextCaptureFocusRectCenterY:F

    add-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mNextCaptureFocusRectTop:F

    goto :goto_0

    .line 2442
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onUltraWideSelfieSingleCaptureDone()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x1

    const/4 v3, 0x2

    .line 2543
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->isUltraWideShotCapturing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2592
    :goto_0
    return-void

    .line 2546
    :cond_0
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mNextCaptureFocusStep:I

    if-nez v0, :cond_1

    .line 2547
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->hideWarningText()V

    .line 2548
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->showGuideText(I)V

    .line 2551
    :cond_1
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mNextCaptureFocusStep:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mNextCaptureFocusStep:I

    .line 2552
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCurrentOrientation:I

    packed-switch v0, :pswitch_data_0

    .line 2564
    :cond_2
    :goto_1
    const-string v0, "TwGLUltraWideShotMenu"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onUltraWideSelfieSingleCaptureDone invisible next capture mDetectedDirection : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mDetectedDirection:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mNextCaptureFocusStep:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mNextCaptureFocusStep:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 2565
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mDetectedDirection:I

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mNextCaptureFocusStep:I

    if-ne v0, v4, :cond_4

    .line 2566
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->showStopButton()V

    goto :goto_0

    .line 2555
    :pswitch_0
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mNextCaptureFocusStep:I

    sget v1, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LANDSCAPE_CAPTURE_COUNT:I

    add-int/lit8 v1, v1, -0x1

    if-le v0, v1, :cond_2

    .line 2556
    sget v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LANDSCAPE_CAPTURE_COUNT:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mNextCaptureFocusStep:I

    goto :goto_1

    .line 2560
    :pswitch_1
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mNextCaptureFocusStep:I

    sget v1, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->PORTRATE_CAPTURE_COUNT:I

    add-int/lit8 v1, v1, -0x1

    if-le v0, v1, :cond_2

    .line 2561
    sget v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->PORTRATE_CAPTURE_COUNT:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mNextCaptureFocusStep:I

    goto :goto_1

    .line 2569
    :cond_4
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCurrentOrientation:I

    if-eqz v0, :cond_6

    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCurrentOrientation:I

    if-eq v0, v3, :cond_6

    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mNextCaptureFocusStep:I

    if-ne v0, v3, :cond_5

    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureAngle:I

    const/16 v1, 0x78

    if-eq v0, v1, :cond_6

    :cond_5
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mNextCaptureFocusStep:I

    if-ne v0, v5, :cond_8

    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureAngle:I

    const/16 v1, 0x96

    if-ne v0, v1, :cond_8

    .line 2572
    :cond_6
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCurrentOrientation:I

    if-eqz v0, :cond_7

    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCurrentOrientation:I

    if-ne v0, v5, :cond_a

    .line 2573
    :cond_7
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mDetectedDirection:I

    if-ne v0, v3, :cond_9

    .line 2574
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->hideWarningText()V

    .line 2575
    invoke-virtual {p0, v3}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->showGuideText(I)V

    .line 2591
    :cond_8
    :goto_2
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mNextCaptureFocusStep:I

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->getGuideDirection(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->showGuideArrow(I)V

    goto/16 :goto_0

    .line 2576
    :cond_9
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mDetectedDirection:I

    if-ne v0, v4, :cond_8

    .line 2577
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->hideWarningText()V

    .line 2578
    invoke-virtual {p0, v4}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->showGuideText(I)V

    goto :goto_2

    .line 2582
    :cond_a
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mDetectedDirection:I

    if-ne v0, v3, :cond_b

    .line 2583
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->hideWarningText()V

    .line 2584
    invoke-virtual {p0, v4}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->showGuideText(I)V

    goto :goto_2

    .line 2585
    :cond_b
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mDetectedDirection:I

    if-ne v0, v4, :cond_8

    .line 2586
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->hideWarningText()V

    .line 2587
    invoke-virtual {p0, v3}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->showGuideText(I)V

    goto :goto_2

    .line 2552
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onUltraWideSelfieSingleCaptureNew()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 2504
    const-string v0, "TwGLUltraWideShotMenu"

    const-string v1, "onUltraWideSelfieSingleCaptureNew"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 2505
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->isUltraWideShotCapturing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2540
    :cond_0
    :goto_0
    return-void

    .line 2509
    :cond_1
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mNextCaptureFocusStep:I

    if-nez v0, :cond_2

    .line 2510
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0, v4}, Lcom/sec/android/app/camera/Camera;->setUltraWideShotStartCapture(Z)V

    .line 2512
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->startBlinkShutterAnimation()V

    .line 2513
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    sget v1, Lcom/sec/android/app/camera/Camera;->SHUTTER_SOUND:I

    invoke-virtual {v0, v1, v5}, Lcom/sec/android/app/camera/Camera;->playCameraSound(II)V

    .line 2514
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getGLContext()Lcom/sec/android/glview/TwGLContext;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/sec/android/glview/TwGLContext;->playHaptic(I)V

    .line 2517
    :cond_2
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCurrentOrientation:I

    packed-switch v0, :pswitch_data_0

    .line 2533
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mUltraWideShotMsgHandler:Landroid/os/Handler;

    if-eqz v0, :cond_4

    .line 2534
    invoke-virtual {p0, v4}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->setSkipCheckWarning(Z)V

    .line 2535
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mUltraWideShotMsgHandler:Landroid/os/Handler;

    const/16 v1, 0x8

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 2537
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->startBlinkShutterAnimation()V

    .line 2538
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    sget v1, Lcom/sec/android/app/camera/Camera;->SHUTTER_SOUND:I

    invoke-virtual {v0, v1, v5}, Lcom/sec/android/app/camera/Camera;->playCameraSound(II)V

    .line 2539
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getGLContext()Lcom/sec/android/glview/TwGLContext;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/sec/android/glview/TwGLContext;->playHaptic(I)V

    goto :goto_0

    .line 2520
    :pswitch_0
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mNextCaptureFocusStep:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mNextCaptureFocusStep:I

    sget v1, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LANDSCAPE_CAPTURE_COUNT:I

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_3

    goto :goto_0

    .line 2525
    :pswitch_1
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mNextCaptureFocusStep:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureAngle:I

    const/16 v1, 0x96

    if-ne v0, v1, :cond_5

    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mNextCaptureFocusStep:I

    sget v1, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->PORTRATE_CAPTURE_COUNT:I

    add-int/lit8 v1, v1, -0x1

    if-eq v0, v1, :cond_0

    :cond_5
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureAngle:I

    const/16 v1, 0x78

    if-ne v0, v1, :cond_3

    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mNextCaptureFocusStep:I

    sget v1, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LANDSCAPE_CAPTURE_COUNT:I

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_3

    goto :goto_0

    .line 2517
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public prepareWarningArrow(I)V
    .locals 8
    .param p1, "warningLevel"    # I

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x3

    const/4 v5, 0x0

    const/4 v4, 0x2

    .line 1941
    sget v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->WARNING_LEVEL_LOW:I

    if-ne p1, v0, :cond_3

    .line 1942
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mUltraWideShotMsgHandler:Landroid/os/Handler;

    invoke-virtual {v0, v6}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1943
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mUltraWideShotMsgHandler:Landroid/os/Handler;

    invoke-virtual {v0, v6}, Landroid/os/Handler;->removeMessages(I)V

    .line 1944
    iput-boolean v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mUltraWideShotWarning:Z

    .line 1947
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mBlinkAnimation:Landroid/view/animation/AlphaAnimation;

    if-nez v0, :cond_1

    .line 1948
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mBlinkAnimation:Landroid/view/animation/AlphaAnimation;

    .line 1949
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mBlinkAnimation:Landroid/view/animation/AlphaAnimation;

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 1950
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mBlinkAnimation:Landroid/view/animation/AlphaAnimation;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setRepeatCount(I)V

    .line 1951
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mBlinkAnimation:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v0, v4}, Landroid/view/animation/AlphaAnimation;->setRepeatMode(I)V

    .line 1954
    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mUltraWideShotWarning:Z

    if-nez v0, :cond_2

    .line 1955
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mUltraWideShotMsgHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1956
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningArrow:[Lcom/sec/android/glview/TwGLImage;

    aget-object v0, v0, v5

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mBlinkAnimation:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLImage;->setAnimation(Landroid/view/animation/Animation;)V

    .line 1957
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningArrow:[Lcom/sec/android/glview/TwGLImage;

    aget-object v0, v0, v7

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mBlinkAnimation:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLImage;->setAnimation(Landroid/view/animation/Animation;)V

    .line 1958
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningArrow:[Lcom/sec/android/glview/TwGLImage;

    aget-object v0, v0, v4

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mBlinkAnimation:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLImage;->setAnimation(Landroid/view/animation/Animation;)V

    .line 1959
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningArrow:[Lcom/sec/android/glview/TwGLImage;

    aget-object v0, v0, v6

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mBlinkAnimation:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLImage;->setAnimation(Landroid/view/animation/Animation;)V

    .line 1960
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningArrow:[Lcom/sec/android/glview/TwGLImage;

    aget-object v0, v0, v5

    invoke-virtual {v0}, Lcom/sec/android/glview/TwGLImage;->startAnimation()V

    .line 1961
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningArrow:[Lcom/sec/android/glview/TwGLImage;

    aget-object v0, v0, v7

    invoke-virtual {v0}, Lcom/sec/android/glview/TwGLImage;->startAnimation()V

    .line 1962
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningArrow:[Lcom/sec/android/glview/TwGLImage;

    aget-object v0, v0, v4

    invoke-virtual {v0}, Lcom/sec/android/glview/TwGLImage;->startAnimation()V

    .line 1963
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningArrow:[Lcom/sec/android/glview/TwGLImage;

    aget-object v0, v0, v6

    invoke-virtual {v0}, Lcom/sec/android/glview/TwGLImage;->startAnimation()V

    .line 1964
    iput-boolean v7, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mUltraWideShotWarning:Z

    .line 1989
    :cond_2
    :goto_0
    return-void

    .line 1966
    :cond_3
    sget v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->WARNING_LEVEL_HIGH:I

    if-ne p1, v0, :cond_5

    .line 1967
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mUltraWideShotMsgHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1968
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mUltraWideShotMsgHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 1969
    iput-boolean v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mUltraWideShotWarning:Z

    .line 1972
    :cond_4
    iget-boolean v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mUltraWideShotWarning:Z

    if-nez v0, :cond_2

    .line 1973
    invoke-direct {p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->stopLivePreviewHaptic()V

    .line 1974
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mUltraWideShotMsgHandler:Landroid/os/Handler;

    invoke-virtual {v0, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1975
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningArrow:[Lcom/sec/android/glview/TwGLImage;

    aget-object v0, v0, v5

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mBlinkAnimation:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLImage;->setAnimation(Landroid/view/animation/Animation;)V

    .line 1976
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningArrow:[Lcom/sec/android/glview/TwGLImage;

    aget-object v0, v0, v7

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mBlinkAnimation:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLImage;->setAnimation(Landroid/view/animation/Animation;)V

    .line 1977
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningArrow:[Lcom/sec/android/glview/TwGLImage;

    aget-object v0, v0, v4

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mBlinkAnimation:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLImage;->setAnimation(Landroid/view/animation/Animation;)V

    .line 1978
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningArrow:[Lcom/sec/android/glview/TwGLImage;

    aget-object v0, v0, v6

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mBlinkAnimation:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLImage;->setAnimation(Landroid/view/animation/Animation;)V

    .line 1979
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningArrow:[Lcom/sec/android/glview/TwGLImage;

    aget-object v0, v0, v5

    invoke-virtual {v0}, Lcom/sec/android/glview/TwGLImage;->startAnimation()V

    .line 1980
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningArrow:[Lcom/sec/android/glview/TwGLImage;

    aget-object v0, v0, v7

    invoke-virtual {v0}, Lcom/sec/android/glview/TwGLImage;->startAnimation()V

    .line 1981
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningArrow:[Lcom/sec/android/glview/TwGLImage;

    aget-object v0, v0, v4

    invoke-virtual {v0}, Lcom/sec/android/glview/TwGLImage;->startAnimation()V

    .line 1982
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningArrow:[Lcom/sec/android/glview/TwGLImage;

    aget-object v0, v0, v6

    invoke-virtual {v0}, Lcom/sec/android/glview/TwGLImage;->startAnimation()V

    .line 1983
    iput-boolean v7, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mUltraWideShotWarning:Z

    goto :goto_0

    .line 1986
    :cond_5
    iput-boolean v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mUltraWideShotWarning:Z

    .line 1987
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->stopUltraWideShotSound()V

    goto :goto_0
.end method

.method public refreshBackgroundRect()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 1305
    const/4 v0, 0x0

    .line 1306
    .local v0, "mAmountDelta":F
    iget v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCurrentOrientation:I

    if-nez v1, :cond_2

    .line 1307
    iget v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPrevThumbnailWidth:F

    iget v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPreviewThumbnailWidth:F

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_0

    .line 1308
    iget v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mDetectedDirection:I

    if-ne v1, v3, :cond_1

    .line 1309
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mBackgroundRect:Lcom/sec/android/glview/TwGLNinePatch;

    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLivePreview:Lcom/sec/android/glview/TwGLBitmapTexture;

    invoke-virtual {v2}, Lcom/sec/android/glview/TwGLBitmapTexture;->getRight()F

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLivePreviewGroup:Lcom/sec/android/glview/TwGLViewGroup;

    invoke-virtual {v3}, Lcom/sec/android/glview/TwGLViewGroup;->getCurrentLeft()F

    move-result v3

    sub-float/2addr v2, v3

    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_RECT_THICKNESS:F

    add-float/2addr v2, v3

    invoke-virtual {v1, v2}, Lcom/sec/android/glview/TwGLNinePatch;->setWidth(F)V

    .line 1349
    :cond_0
    :goto_0
    return-void

    .line 1310
    :cond_1
    iget v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mDetectedDirection:I

    if-ne v1, v4, :cond_0

    .line 1311
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLivePreview:Lcom/sec/android/glview/TwGLBitmapTexture;

    invoke-virtual {v1}, Lcom/sec/android/glview/TwGLBitmapTexture;->getCurrentLeft()F

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLivePreviewGroup:Lcom/sec/android/glview/TwGLViewGroup;

    invoke-virtual {v2}, Lcom/sec/android/glview/TwGLViewGroup;->getCurrentLeft()F

    move-result v2

    sub-float/2addr v1, v2

    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_RECT_THICKNESS:F

    sub-float v0, v1, v2

    .line 1312
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mBackgroundRect:Lcom/sec/android/glview/TwGLNinePatch;

    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_LANDSCAPE_WIDTH:F

    sub-float/2addr v2, v0

    invoke-virtual {v1, v2}, Lcom/sec/android/glview/TwGLNinePatch;->setWidth(F)V

    .line 1313
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mBackgroundRect:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v1, v0, v5}, Lcom/sec/android/glview/TwGLNinePatch;->translateAbsolute(FF)V

    goto :goto_0

    .line 1316
    :cond_2
    iget v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCurrentOrientation:I

    if-ne v1, v4, :cond_4

    .line 1317
    iget v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPrevThumbnailWidth:F

    iget v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPreviewThumbnailWidth:F

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_0

    .line 1318
    iget v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mDetectedDirection:I

    if-ne v1, v3, :cond_3

    .line 1319
    sget v1, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_LANDSCAPE_WIDTH:F

    iget v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPreviewThumbnailLeft:F

    iget v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPreviewThumbnailWidth:F

    add-float/2addr v2, v3

    sub-float v0, v1, v2

    .line 1320
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mBackgroundRect:Lcom/sec/android/glview/TwGLNinePatch;

    iget v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPreviewThumbnailLeft:F

    iget v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPreviewThumbnailWidth:F

    add-float/2addr v2, v3

    invoke-virtual {v1, v2}, Lcom/sec/android/glview/TwGLNinePatch;->setWidth(F)V

    .line 1321
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mBackgroundRect:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v1, v0, v5}, Lcom/sec/android/glview/TwGLNinePatch;->translateAbsolute(FF)V

    goto :goto_0

    .line 1322
    :cond_3
    iget v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mDetectedDirection:I

    if-ne v1, v4, :cond_0

    .line 1323
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mBackgroundRect:Lcom/sec/android/glview/TwGLNinePatch;

    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLivePreviewGroup:Lcom/sec/android/glview/TwGLViewGroup;

    invoke-virtual {v2}, Lcom/sec/android/glview/TwGLViewGroup;->getCurrentRight()F

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLivePreview:Lcom/sec/android/glview/TwGLBitmapTexture;

    invoke-virtual {v3}, Lcom/sec/android/glview/TwGLBitmapTexture;->getCurrentLeft()F

    move-result v3

    sub-float/2addr v2, v3

    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_RECT_THICKNESS:F

    add-float/2addr v2, v3

    invoke-virtual {v1, v2}, Lcom/sec/android/glview/TwGLNinePatch;->setWidth(F)V

    goto :goto_0

    .line 1326
    :cond_4
    iget v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCurrentOrientation:I

    if-ne v1, v3, :cond_6

    .line 1327
    iget v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPrevThumbnailHeight:F

    iget v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPreviewThumbnailHeight:F

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_0

    .line 1328
    iget v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mDetectedDirection:I

    if-ne v1, v3, :cond_5

    .line 1329
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPreviewThumbnailTop:F

    .line 1330
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mBackgroundRect:Lcom/sec/android/glview/TwGLNinePatch;

    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_PORTRAIT_HEIGHT:F

    iget v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPreviewThumbnailTop:F

    sub-float/2addr v2, v3

    invoke-virtual {v1, v2}, Lcom/sec/android/glview/TwGLNinePatch;->setWidth(F)V

    .line 1331
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mBackgroundRect:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v1, v0, v5}, Lcom/sec/android/glview/TwGLNinePatch;->translateAbsolute(FF)V

    goto/16 :goto_0

    .line 1332
    :cond_5
    iget v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mDetectedDirection:I

    if-ne v1, v4, :cond_0

    .line 1333
    iget v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPreviewThumbnailTop:F

    iget v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPreviewThumbnailHeight:F

    add-float v0, v1, v2

    .line 1334
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mBackgroundRect:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v1, v0}, Lcom/sec/android/glview/TwGLNinePatch;->setWidth(F)V

    goto/16 :goto_0

    .line 1337
    :cond_6
    iget v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCurrentOrientation:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    .line 1338
    iget v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPrevThumbnailHeight:F

    iget v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPreviewThumbnailHeight:F

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_0

    .line 1339
    iget v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mDetectedDirection:I

    if-ne v1, v3, :cond_7

    .line 1340
    sget v1, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_PORTRAIT_HEIGHT:F

    iget v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPreviewThumbnailTop:F

    sub-float v0, v1, v2

    .line 1341
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mBackgroundRect:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v1, v0}, Lcom/sec/android/glview/TwGLNinePatch;->setWidth(F)V

    goto/16 :goto_0

    .line 1342
    :cond_7
    iget v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mDetectedDirection:I

    if-ne v1, v4, :cond_0

    .line 1343
    sget v1, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_PORTRAIT_HEIGHT:F

    iget v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPreviewThumbnailTop:F

    iget v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPreviewThumbnailHeight:F

    add-float/2addr v2, v3

    sub-float v0, v1, v2

    .line 1344
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mBackgroundRect:Lcom/sec/android/glview/TwGLNinePatch;

    iget v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPreviewThumbnailTop:F

    iget v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPreviewThumbnailHeight:F

    add-float/2addr v2, v3

    invoke-virtual {v1, v2}, Lcom/sec/android/glview/TwGLNinePatch;->setWidth(F)V

    .line 1345
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mBackgroundRect:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v1, v0, v5}, Lcom/sec/android/glview/TwGLNinePatch;->translateAbsolute(FF)V

    goto/16 :goto_0
.end method

.method public refreshLivePreviewPosition()V
    .locals 12

    .prologue
    const/16 v11, 0x78

    const/4 v10, 0x1

    const/4 v5, 0x0

    const/4 v9, 0x0

    .line 788
    iget-object v6, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLivePreviewGroup:Lcom/sec/android/glview/TwGLViewGroup;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mBackgroundRectGroup:Lcom/sec/android/glview/TwGLViewGroup;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureFocusRect:Lcom/sec/android/glview/TwGLNinePatch;

    if-nez v6, :cond_1

    .line 874
    :cond_0
    :goto_0
    return-void

    .line 791
    :cond_1
    const-string v6, "TwGLUltraWideShotMenu"

    const-string v7, "refreshLivePreviewPosition enter"

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 793
    iget-boolean v6, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mIsStartCapture:Z

    if-eqz v6, :cond_3

    .line 794
    iget-object v6, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureFocusRect:Lcom/sec/android/glview/TwGLNinePatch;

    if-eqz v6, :cond_2

    iget-boolean v6, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPlayHaptic:Z

    if-eqz v6, :cond_2

    .line 796
    const/16 v6, 0x8

    new-array v4, v6, [F

    iget v6, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLeftTopX:F

    aput v6, v4, v9

    iget v6, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLeftTopY:F

    aput v6, v4, v10

    const/4 v6, 0x2

    iget v7, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mRightTopX:F

    aput v7, v4, v6

    const/4 v6, 0x3

    iget v7, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mRightTopY:F

    aput v7, v4, v6

    const/4 v6, 0x4

    iget v7, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mRightBottomX:F

    aput v7, v4, v6

    const/4 v6, 0x5

    iget v7, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mRightBottomY:F

    aput v7, v4, v6

    const/4 v6, 0x6

    iget v7, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLeftBottomX:F

    aput v7, v4, v6

    const/4 v6, 0x7

    iget v7, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLeftBottomY:F

    aput v7, v4, v6

    .line 797
    .local v4, "point":[F
    iget-object v6, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mTrapezoidCaptureFocusRect:Lcom/sec/android/app/camera/glwidget/TwGLTrapezoid;

    iget v7, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWidth:F

    iget v8, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mHeight:F

    invoke-virtual {v6, v7, v8, v4}, Lcom/sec/android/app/camera/glwidget/TwGLTrapezoid;->setRect(FF[F)V

    .line 798
    iget-object v6, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mTrapezoidCaptureFocusRect:Lcom/sec/android/app/camera/glwidget/TwGLTrapezoid;

    invoke-virtual {v6, v9}, Lcom/sec/android/app/camera/glwidget/TwGLTrapezoid;->setVisibility(I)V

    .line 802
    .end local v4    # "point":[F
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->hideWarningText()V

    .line 803
    invoke-virtual {p0, v9}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->showGuideText(I)V

    .line 808
    :cond_3
    iget v6, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCurrentOrientation:I

    packed-switch v6, :pswitch_data_0

    goto :goto_0

    .line 811
    :pswitch_0
    iget-boolean v6, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mIsStartCapture:Z

    if-eqz v6, :cond_4

    move v3, v5

    .line 812
    .local v3, "TOP_MARGIN":F
    :goto_1
    iget-boolean v6, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mIsStartCapture:Z

    if-eqz v6, :cond_5

    move v1, v5

    .line 814
    .local v1, "LEFT_MARGIN":F
    :goto_2
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLivePreviewGroup:Lcom/sec/android/glview/TwGLViewGroup;

    sget v6, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_LANDSCAPE_WIDTH:F

    sget v7, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_LANDSCAPE_HEIGHT:F

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/glview/TwGLViewGroup;->setSize(FF)V

    .line 815
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLivePreviewGroup:Lcom/sec/android/glview/TwGLViewGroup;

    sget v6, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_LANDSCAPE_X:F

    add-float/2addr v6, v1

    sget v7, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_LANDSCAPE_Y:F

    add-float/2addr v7, v3

    invoke-virtual {v5, v9, v6, v7}, Lcom/sec/android/glview/TwGLViewGroup;->setLeftTop(IFF)V

    .line 816
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLivePreviewGroup:Lcom/sec/android/glview/TwGLViewGroup;

    invoke-virtual {v5, v9}, Lcom/sec/android/glview/TwGLViewGroup;->setOrientation(I)V

    .line 818
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mBackgroundRectGroup:Lcom/sec/android/glview/TwGLViewGroup;

    sget v6, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_LANDSCAPE_WIDTH:F

    sget v7, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_LANDSCAPE_HEIGHT:F

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/glview/TwGLViewGroup;->setSize(FF)V

    .line 819
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mBackgroundRectGroup:Lcom/sec/android/glview/TwGLViewGroup;

    sget v6, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_LANDSCAPE_X:F

    add-float/2addr v6, v1

    sget v7, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_LANDSCAPE_Y:F

    add-float/2addr v7, v3

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/glview/TwGLViewGroup;->translateAbsolute(FF)V

    .line 820
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mBackgroundRect:Lcom/sec/android/glview/TwGLNinePatch;

    sget v6, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_LANDSCAPE_WIDTH:F

    sget v7, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_LANDSCAPE_HEIGHT:F

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/glview/TwGLNinePatch;->setSize(FF)V

    .line 822
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureFocusRect:Lcom/sec/android/glview/TwGLNinePatch;

    sget v6, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_SINGLE_WIDTH:F

    sget v7, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_SINGLE_HEIGHT:F

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/glview/TwGLNinePatch;->setSize(FF)V

    .line 823
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureFocusRect:Lcom/sec/android/glview/TwGLNinePatch;

    sget v6, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->FOCUS_RECT_LANDSCAPE_X:F

    add-float/2addr v6, v1

    sget v7, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_LANDSCAPE_Y:F

    add-float/2addr v7, v3

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/glview/TwGLNinePatch;->translateAbsolute(FF)V

    .line 825
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideText:Lcom/sec/android/glview/TwGLText;

    sget v6, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->GUIDE_TEXT_WIDTH:F

    sget v7, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->GUIDE_TEXT_HEIGHT:F

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/glview/TwGLText;->setSize(FF)V

    .line 826
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideText:Lcom/sec/android/glview/TwGLText;

    sget v6, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->GUIDE_TEXT_POS_X:F

    sget v7, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_LANDSCAPE_Y:F

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/glview/TwGLText;->translateAbsolute(FF)V

    .line 828
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningText:Lcom/sec/android/glview/TwGLText;

    sget v6, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->GUIDE_TEXT_WIDTH:F

    sget v7, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->GUIDE_TEXT_HEIGHT:F

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/glview/TwGLText;->setSize(FF)V

    .line 829
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningText:Lcom/sec/android/glview/TwGLText;

    sget v6, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->GUIDE_TEXT_POS_X:F

    sget v7, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_LANDSCAPE_Y:F

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/glview/TwGLText;->translateAbsolute(FF)V

    goto/16 :goto_0

    .line 811
    .end local v1    # "LEFT_MARGIN":F
    .end local v3    # "TOP_MARGIN":F
    :cond_4
    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->PREVIEW_TOP_MARGIN:F

    goto :goto_1

    .line 812
    .restart local v3    # "TOP_MARGIN":F
    :cond_5
    sget v1, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->PREVIEW_LEFT_MARGIN:F

    goto :goto_2

    .line 833
    .end local v3    # "TOP_MARGIN":F
    :pswitch_1
    iget-boolean v6, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mIsStartCapture:Z

    if-eqz v6, :cond_6

    move v2, v5

    .line 834
    .local v2, "PORT_MARGIN":F
    :goto_3
    iget v6, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureAngle:I

    const/16 v7, 0x96

    if-ne v6, v7, :cond_8

    sget v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->CAPTURE_ANGLE_MARGIN:F

    .line 836
    .local v0, "ANGLE_MARGIN":F
    :goto_4
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->setPortraitModePosition()V

    .line 838
    iget-object v6, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLivePreviewGroup:Lcom/sec/android/glview/TwGLViewGroup;

    sget v7, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_PORTRAIT_WIDTH:F

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_PORTRAIT_HEIGHT:F

    invoke-virtual {v6, v7, v8}, Lcom/sec/android/glview/TwGLViewGroup;->setSize(FF)V

    .line 839
    iget-object v6, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLivePreviewGroup:Lcom/sec/android/glview/TwGLViewGroup;

    sget v7, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_PORTRAIT_X:F

    add-float/2addr v7, v2

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_PORTRAIT_Y:F

    invoke-virtual {v6, v9, v7, v8}, Lcom/sec/android/glview/TwGLViewGroup;->setLeftTop(IFF)V

    .line 840
    iget-object v6, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLivePreviewGroup:Lcom/sec/android/glview/TwGLViewGroup;

    invoke-virtual {v6, v9}, Lcom/sec/android/glview/TwGLViewGroup;->setOrientation(I)V

    .line 842
    iget-object v6, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mBackgroundRectGroup:Lcom/sec/android/glview/TwGLViewGroup;

    sget v7, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_PORTRAIT_HEIGHT:F

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_PORTRAIT_WIDTH:F

    invoke-virtual {v6, v7, v8}, Lcom/sec/android/glview/TwGLViewGroup;->setSize(FF)V

    .line 844
    iget v6, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureAngle:I

    if-ne v6, v11, :cond_9

    .line 845
    iget-object v6, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mBackgroundRectGroup:Lcom/sec/android/glview/TwGLViewGroup;

    sget-object v7, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->BACKGROUND_RECT_PORTRAIT_X:[F

    aget v7, v7, v9

    add-float/2addr v7, v2

    sget-object v8, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->BACKGROUND_RECT_PORTRAIT_Y:[F

    aget v8, v8, v9

    invoke-virtual {v6, v7, v8}, Lcom/sec/android/glview/TwGLViewGroup;->translateAbsolute(FF)V

    .line 849
    :goto_5
    iget-object v6, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mBackgroundRect:Lcom/sec/android/glview/TwGLNinePatch;

    sget v7, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_PORTRAIT_HEIGHT:F

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_PORTRAIT_WIDTH:F

    invoke-virtual {v6, v7, v8}, Lcom/sec/android/glview/TwGLNinePatch;->setSize(FF)V

    .line 851
    iget-object v6, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureFocusRect:Lcom/sec/android/glview/TwGLNinePatch;

    sget v7, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_PORTRAIT_WIDTH:F

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_SINGLE_PORTRAIT_HEIGHT:F

    invoke-virtual {v6, v7, v8}, Lcom/sec/android/glview/TwGLNinePatch;->setSize(FF)V

    .line 852
    iget-object v6, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureFocusRect:Lcom/sec/android/glview/TwGLNinePatch;

    sget v7, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->FOCUS_RECT_PORTRAIT_X:F

    add-float/2addr v7, v2

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->FOCUS_RECT_PORTRAIT_Y:F

    invoke-virtual {v6, v7, v8}, Lcom/sec/android/glview/TwGLNinePatch;->translateAbsolute(FF)V

    .line 854
    iget-object v6, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideText:Lcom/sec/android/glview/TwGLText;

    sget v7, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->GUIDE_TEXT_PORTRAIT_HEIGHT:F

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_PORTRAIT_WIDTH:F

    invoke-virtual {v6, v7, v8}, Lcom/sec/android/glview/TwGLText;->setSize(FF)V

    .line 855
    iget v6, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureAngle:I

    if-ne v6, v11, :cond_a

    .line 856
    iget-object v6, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideText:Lcom/sec/android/glview/TwGLText;

    sget-object v7, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->GUIDE_TEXT_PORTRAIT_X:[F

    aget v7, v7, v9

    sub-float v7, v5, v7

    sget-object v8, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->GUIDE_TEXT_PORTRAIT_Y:[F

    aget v8, v8, v9

    invoke-virtual {v6, v7, v8}, Lcom/sec/android/glview/TwGLText;->translateAbsolute(FF)V

    .line 860
    :goto_6
    iget-object v6, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningText:Lcom/sec/android/glview/TwGLText;

    sget v7, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->GUIDE_TEXT_PORTRAIT_HEIGHT:F

    sget v8, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_PORTRAIT_WIDTH:F

    invoke-virtual {v6, v7, v8}, Lcom/sec/android/glview/TwGLText;->setSize(FF)V

    .line 861
    iget v6, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureAngle:I

    if-ne v6, v11, :cond_b

    .line 862
    iget-object v6, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningText:Lcom/sec/android/glview/TwGLText;

    sget-object v7, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->GUIDE_TEXT_PORTRAIT_X:[F

    aget v7, v7, v9

    sub-float/2addr v5, v7

    sget-object v7, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->GUIDE_TEXT_PORTRAIT_Y:[F

    aget v7, v7, v9

    invoke-virtual {v6, v5, v7}, Lcom/sec/android/glview/TwGLText;->translateAbsolute(FF)V

    goto/16 :goto_0

    .line 833
    .end local v0    # "ANGLE_MARGIN":F
    .end local v2    # "PORT_MARGIN":F
    :cond_6
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->isEasyMode()Z

    move-result v6

    if-eqz v6, :cond_7

    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->PREVIEW_EASYMODE_PORTRAIT_MARGIN:F

    goto/16 :goto_3

    :cond_7
    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->PREVIEW_PORTRAIT_MARGIN:F

    goto/16 :goto_3

    .restart local v2    # "PORT_MARGIN":F
    :cond_8
    move v0, v5

    .line 834
    goto/16 :goto_4

    .line 847
    .restart local v0    # "ANGLE_MARGIN":F
    :cond_9
    iget-object v6, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mBackgroundRectGroup:Lcom/sec/android/glview/TwGLViewGroup;

    sget-object v7, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->BACKGROUND_RECT_PORTRAIT_X:[F

    aget v7, v7, v10

    sub-float v7, v5, v7

    add-float/2addr v7, v2

    sget-object v8, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->BACKGROUND_RECT_PORTRAIT_Y:[F

    aget v8, v8, v10

    invoke-virtual {v6, v7, v8}, Lcom/sec/android/glview/TwGLViewGroup;->translateAbsolute(FF)V

    goto :goto_5

    .line 858
    :cond_a
    iget-object v6, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideText:Lcom/sec/android/glview/TwGLText;

    sget-object v7, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->GUIDE_TEXT_PORTRAIT_X:[F

    aget v7, v7, v10

    sget-object v8, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->GUIDE_TEXT_PORTRAIT_Y:[F

    aget v8, v8, v10

    invoke-virtual {v6, v7, v8}, Lcom/sec/android/glview/TwGLText;->translateAbsolute(FF)V

    goto :goto_6

    .line 864
    :cond_b
    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningText:Lcom/sec/android/glview/TwGLText;

    sget-object v6, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->GUIDE_TEXT_PORTRAIT_X:[F

    aget v6, v6, v10

    sget-object v7, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->GUIDE_TEXT_PORTRAIT_Y:[F

    aget v7, v7, v10

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/glview/TwGLText;->translateAbsolute(FF)V

    goto/16 :goto_0

    .line 808
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public reset()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 975
    const-string v0, "TwGLUltraWideShotMenu"

    const-string v1, "reset"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 976
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    if-eqz v0, :cond_0

    .line 977
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getEngine()Lcom/sec/android/app/camera/CommonEngine;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CommonEngine;->resetUltraWideShotCapturing()V

    .line 978
    :cond_0
    iput-boolean v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mIsUltraWideShotCapturing:Z

    .line 979
    iput-boolean v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mIsStartCapture:Z

    .line 980
    iput-boolean v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mSkipCapture:Z

    .line 981
    iput-boolean v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mSkipCheckWarning:Z

    .line 982
    iput-boolean v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mChanged:Z

    .line 983
    iput v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPostProgress:I

    .line 984
    iput v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mDetectedDirection:I

    .line 985
    iput v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCurrentDirection:I

    .line 986
    iput v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mNextCaptureFocusStep:I

    .line 987
    iput v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPrevThumbnailWidth:F

    .line 988
    iput v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPrevThumbnailHeight:F

    .line 989
    iput-boolean v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mEncodingProgress:Z

    .line 990
    iput-boolean v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mMovingDetection:Z

    .line 992
    monitor-enter p0

    .line 993
    const/4 v0, 0x0

    :try_start_0
    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureCount:I

    .line 994
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 996
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->setTouchHandled(Z)V

    .line 997
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->clearUltraWideRect()V

    .line 998
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->stopCancelTimer()V

    .line 999
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->stopUltraWideShotSound()V

    .line 1003
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mUltraWideShotMsgHandler:Landroid/os/Handler;

    if-eqz v0, :cond_1

    .line 1004
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mUltraWideShotMsgHandler:Landroid/os/Handler;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1006
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->stopLivePreviewHaptic()V

    .line 1008
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    if-eqz v0, :cond_2

    .line 1009
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->showBaseMenu()V

    .line 1011
    :cond_2
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideState:I

    .line 1012
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->hideStopButton()V

    .line 1013
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->setPreviewThumbnailSizeToDefault()V

    .line 1016
    invoke-direct {p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->resetTrapezoid()V

    .line 1017
    return-void

    .line 994
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public resetFocusRect()V
    .locals 3

    .prologue
    .line 1351
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCurrentOrientation:I

    packed-switch v0, :pswitch_data_0

    .line 1365
    :goto_0
    return-void

    .line 1354
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureFocusRect:Lcom/sec/android/glview/TwGLNinePatch;

    sget v1, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_SINGLE_WIDTH:F

    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_SINGLE_HEIGHT:F

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/glview/TwGLNinePatch;->setSize(FF)V

    .line 1355
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureFocusRect:Lcom/sec/android/glview/TwGLNinePatch;

    sget v1, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->FOCUS_RECT_LANDSCAPE_X:F

    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->FOCUS_RECT_LANDSCAPE_Y:F

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/glview/TwGLNinePatch;->translateAbsolute(FF)V

    goto :goto_0

    .line 1359
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureFocusRect:Lcom/sec/android/glview/TwGLNinePatch;

    sget v1, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_SINGLE_PORTRAIT_WIDTH:F

    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_SINGLE_PORTRAIT_HEIGHT:F

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/glview/TwGLNinePatch;->setSize(FF)V

    .line 1360
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureFocusRect:Lcom/sec/android/glview/TwGLNinePatch;

    sget v1, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->FOCUS_RECT_PORTRAIT_X:F

    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->FOCUS_RECT_PORTRAIT_Y:F

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/glview/TwGLNinePatch;->translateAbsolute(FF)V

    goto :goto_0

    .line 1351
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public restartCancelTimer()V
    .locals 2

    .prologue
    .line 1037
    const-string v0, "TwGLUltraWideShotMenu"

    const-string v1, "call restartCancelTimer..."

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1038
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->stopCancelTimer()V

    .line 1039
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->startCancelTimer()V

    .line 1040
    return-void
.end method

.method public setAcquisitionProgress(I)V
    .locals 1
    .param p1, "nProgress"    # I

    .prologue
    .line 783
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->setTouchHandled(Z)V

    .line 784
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->hideBaseMenuForVirtualKey()V

    .line 785
    return-void
.end method

.method public setCaptureAngle(I)V
    .locals 0
    .param p1, "angle"    # I

    .prologue
    .line 1087
    return-void
.end method

.method public setCaptureError(Z)V
    .locals 0
    .param p1, "error"    # Z

    .prologue
    .line 948
    iput-boolean p1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureError:Z

    .line 949
    return-void
.end method

.method public declared-synchronized setCaptureProgressIncreased()V
    .locals 3

    .prologue
    .line 1098
    monitor-enter p0

    :try_start_0
    const-string v0, "TwGLUltraWideShotMenu"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setCaptureProgressIncreased mCaptureCount: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1099
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mIsUltraWideShotCapturing:Z

    .line 1101
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureCount:I

    .line 1102
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->setTouchHandled(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1103
    monitor-exit p0

    return-void

    .line 1098
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setDelayedSkipCapture()V
    .locals 5

    .prologue
    const/4 v4, 0x7

    .line 940
    const-string v0, "TwGLUltraWideShotMenu"

    const-string v1, "setDelayedSkipCapture"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 941
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mUltraWideShotMsgHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 942
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->setSkipCapture(Z)V

    .line 943
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mUltraWideShotMsgHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 944
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mUltraWideShotMsgHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 946
    :cond_0
    return-void
.end method

.method public setEncodingProgress(Z)V
    .locals 0
    .param p1, "bEncoding"    # Z

    .prologue
    .line 3000
    iput-boolean p1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mEncodingProgress:Z

    .line 3001
    return-void
.end method

.method public setFocusRectLeftTop(FF)V
    .locals 5
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    const/4 v3, 0x0

    .line 1379
    iget v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mFocusRectCenterX:F

    iput v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPrevFocusRectCenterX:F

    .line 1380
    iget v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mFocusRectCenterY:F

    iput v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPrevFocusRectCenterY:F

    .line 1381
    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_RATIO:F

    mul-float/2addr v4, p1

    iput v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mFocusRectCenterX:F

    .line 1382
    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_RATIO:F

    mul-float/2addr v4, p2

    iput v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mFocusRectCenterY:F

    .line 1384
    iget v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCurrentOrientation:I

    packed-switch v4, :pswitch_data_0

    .line 1399
    :goto_0
    return-void

    .line 1387
    :pswitch_0
    iget-boolean v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mIsStartCapture:Z

    if-eqz v4, :cond_0

    move v2, v3

    .line 1388
    .local v2, "TOP_MARGIN":F
    :goto_1
    iget-boolean v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mIsStartCapture:Z

    if-eqz v4, :cond_1

    move v0, v3

    .line 1389
    .local v0, "LEFT_MARGIN":F
    :goto_2
    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->FOCUS_RECT_LANDSCAPE_X:F

    iget v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mFocusRectCenterX:F

    add-float/2addr v3, v4

    add-float/2addr v3, v0

    iput v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mFocusRectLeft:F

    .line 1390
    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_LANDSCAPE_Y:F

    iget v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mFocusRectCenterY:F

    add-float/2addr v3, v4

    add-float/2addr v3, v2

    iput v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mFocusRectTop:F

    goto :goto_0

    .line 1387
    .end local v0    # "LEFT_MARGIN":F
    .end local v2    # "TOP_MARGIN":F
    :cond_0
    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->PREVIEW_TOP_MARGIN:F

    goto :goto_1

    .line 1388
    .restart local v2    # "TOP_MARGIN":F
    :cond_1
    sget v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->PREVIEW_LEFT_MARGIN:F

    goto :goto_2

    .line 1394
    .end local v2    # "TOP_MARGIN":F
    :pswitch_1
    iget-boolean v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mIsStartCapture:Z

    if-eqz v4, :cond_2

    move v1, v3

    .line 1395
    .local v1, "PORT_MARGIN":F
    :goto_3
    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_PORTRAIT_X:F

    iget v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mFocusRectCenterX:F

    add-float/2addr v3, v4

    add-float/2addr v3, v1

    iput v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mFocusRectLeft:F

    .line 1396
    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_PORTRAIT_Y:F

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_SINGLE_PORTRAIT_Y:F

    add-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mFocusRectCenterY:F

    add-float/2addr v3, v4

    iput v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mFocusRectTop:F

    goto :goto_0

    .line 1394
    .end local v1    # "PORT_MARGIN":F
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->isEasyMode()Z

    move-result v3

    if-eqz v3, :cond_3

    sget v1, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->PREVIEW_EASYMODE_PORTRAIT_MARGIN:F

    goto :goto_3

    :cond_3
    sget v1, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->PREVIEW_PORTRAIT_MARGIN:F

    goto :goto_3

    .line 1384
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setHapticEnabled(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 2774
    iput-boolean p1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPlayHaptic:Z

    .line 2775
    return-void
.end method

.method public setLowResolutionBitmap(Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1, "bmp"    # Landroid/graphics/Bitmap;

    .prologue
    .line 2423
    const-string v0, "TwGLUltraWideShotMenu"

    const-string v1, "Fancy progress bar is not implemented yet."

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 2424
    return-void
.end method

.method public setOnUltraWideShotCaptureCancelledListener(Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu$OnUltraWideShotCaptureCancelListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu$OnUltraWideShotCaptureCancelListener;

    .prologue
    .line 1142
    iput-object p1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mListener:Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu$OnUltraWideShotCaptureCancelListener;

    .line 1143
    return-void
.end method

.method public setPortraitModePosition()V
    .locals 3

    .prologue
    .line 877
    iget v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureAngle:I

    const/16 v2, 0x96

    if-ne v1, v2, :cond_0

    sget v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->CAPTURE_ANGLE_MARGIN:F

    .line 878
    .local v0, "ANGLE_MARGIN":F
    :goto_0
    const v1, 0x7f0a03d6

    invoke-static {v1}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v1

    add-float/2addr v1, v0

    sput v1, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_PORTRAIT_HEIGHT:F

    .line 879
    sget v1, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_PORTRAIT_HEIGHT:F

    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_SINGLE_PORTRAIT_HEIGHT:F

    sub-float/2addr v1, v2

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    sput v1, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_SINGLE_PORTRAIT_Y:F

    .line 880
    sget v1, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_PORTRAIT_Y:F

    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_SINGLE_PORTRAIT_Y:F

    add-float/2addr v1, v2

    sput v1, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->FOCUS_RECT_PORTRAIT_Y:F

    .line 881
    const v1, 0x7f0a03e0

    invoke-static {v1}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v1

    sub-float/2addr v1, v0

    sput v1, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->GUIDE_TEXT_PORTRAIT_HEIGHT:F

    .line 882
    return-void

    .line 877
    .end local v0    # "ANGLE_MARGIN":F
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized setPostCaptureProgress(I)V
    .locals 3
    .param p1, "progress"    # I

    .prologue
    .line 1110
    monitor-enter p0

    :try_start_0
    const-string v0, "TwGLUltraWideShotMenu"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setPostCaptureProgress :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1111
    iput p1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPostProgress:I

    .line 1112
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureStopButton:Lcom/sec/android/glview/TwGLButton;

    invoke-virtual {v0}, Lcom/sec/android/glview/TwGLButton;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 1113
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->hideStopButton()V

    .line 1115
    :cond_0
    if-nez p1, :cond_1

    .line 1116
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getGLContext()Lcom/sec/android/glview/TwGLContext;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLContext;->playHaptic(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1118
    :cond_1
    monitor-exit p0

    return-void

    .line 1110
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setPreviewThumbnailLeftTop(FFFF)V
    .locals 2
    .param p1, "left"    # F
    .param p2, "top"    # F
    .param p3, "right"    # F
    .param p4, "bottom"    # F

    .prologue
    const/4 v1, 0x0

    .line 1438
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCurrentOrientation:I

    packed-switch v0, :pswitch_data_0

    .line 1452
    :goto_0
    return-void

    .line 1441
    :pswitch_0
    sget v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_RATIO:F

    mul-float/2addr v0, p1

    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPreviewThumbnailLeft:F

    .line 1442
    iput v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPreviewThumbnailTop:F

    goto :goto_0

    .line 1446
    :pswitch_1
    iput v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPreviewThumbnailLeft:F

    .line 1447
    sget v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_PORTRAIT_HEIGHT:F

    sget v1, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_RATIO:F

    mul-float/2addr v1, p4

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPreviewThumbnailTop:F

    goto :goto_0

    .line 1438
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setPreviewThumbnailSizeToDefault()V
    .locals 1

    .prologue
    .line 2427
    sget v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_SINGLE_WIDTH:F

    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPreviewThumbnailWidth:F

    .line 2428
    sget v0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_SINGLE_HEIGHT:F

    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPreviewThumbnailHeight:F

    .line 2429
    return-void
.end method

.method public setSkipCapture(Z)V
    .locals 0
    .param p1, "isSkip"    # Z

    .prologue
    .line 955
    iput-boolean p1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mSkipCapture:Z

    .line 956
    return-void
.end method

.method public setSkipCheckWarning(Z)V
    .locals 0
    .param p1, "isSkip"    # Z

    .prologue
    .line 969
    iput-boolean p1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mSkipCheckWarning:Z

    .line 970
    return-void
.end method

.method public setStartCapture(Z)V
    .locals 2
    .param p1, "isStart"    # Z

    .prologue
    const/4 v1, 0x0

    .line 963
    iput-boolean p1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mIsStartCapture:Z

    .line 964
    iget-boolean v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mIsStartCapture:Z

    if-eqz v0, :cond_0

    .line 965
    invoke-virtual {p0, v1}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->setTouchHandled(Z)V

    .line 966
    :cond_0
    iput v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mNextCaptureFocusStep:I

    .line 967
    return-void
.end method

.method public setTrapezoidPosition(FFFFFFFF)V
    .locals 6
    .param p1, "leftTopX"    # F
    .param p2, "leftTopY"    # F
    .param p3, "rightTopX"    # F
    .param p4, "rightTopY"    # F
    .param p5, "rightBottomX"    # F
    .param p6, "rightBottomY"    # F
    .param p7, "leftBottomX"    # F
    .param p8, "leftBottomY"    # F

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    .line 1402
    iget v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCurrentOrientation:I

    packed-switch v2, :pswitch_data_0

    .line 1435
    :goto_0
    return-void

    .line 1405
    :pswitch_0
    const-string v2, "TwGLUltraWideShotMenu"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mFocusRectCenterX : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mFocusRectCenterX:F

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", mFocusRectCenterY : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mFocusRectCenterY:F

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1406
    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->FOCUS_RECT_LANDSCAPE_X:F

    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_SINGLE_WIDTH:F

    div-float/2addr v3, v5

    add-float v0, v2, v3

    .line 1407
    .local v0, "centerX":F
    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_LANDSCAPE_Y:F

    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_SINGLE_HEIGHT:F

    div-float/2addr v3, v5

    add-float v1, v2, v3

    .line 1408
    .local v1, "centerY":F
    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_RATIO:F

    mul-float/2addr v2, p1

    add-float/2addr v2, v0

    iput v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLeftTopX:F

    .line 1409
    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_RATIO:F

    mul-float/2addr v2, p2

    add-float/2addr v2, v1

    iput v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLeftTopY:F

    .line 1410
    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_RATIO:F

    mul-float/2addr v2, p3

    add-float/2addr v2, v0

    iput v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mRightTopX:F

    .line 1411
    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_RATIO:F

    mul-float/2addr v2, p4

    add-float/2addr v2, v1

    iput v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mRightTopY:F

    .line 1412
    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_RATIO:F

    mul-float/2addr v2, p5

    add-float/2addr v2, v0

    iput v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mRightBottomX:F

    .line 1413
    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_RATIO:F

    mul-float/2addr v2, p6

    add-float/2addr v2, v1

    iput v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mRightBottomY:F

    .line 1414
    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_RATIO:F

    mul-float/2addr v2, p7

    add-float/2addr v2, v0

    iput v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLeftBottomX:F

    .line 1415
    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_RATIO:F

    mul-float/2addr v2, p8

    add-float/2addr v2, v1

    iput v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLeftBottomY:F

    .line 1416
    iget v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLeftTopX:F

    iget v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mRightTopX:F

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLeftBottomX:F

    iget v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mRightBottomX:F

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    cmpl-float v2, v2, v3

    if-lez v2, :cond_0

    iget v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLeftTopX:F

    iget v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mRightTopX:F

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    :goto_1
    iput v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWidth:F

    .line 1417
    iget v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLeftTopY:F

    iget v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLeftBottomY:F

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mRightTopY:F

    iget v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mRightBottomY:F

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    cmpl-float v2, v2, v3

    if-lez v2, :cond_1

    iget v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLeftTopY:F

    iget v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLeftBottomY:F

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    :goto_2
    iput v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mHeight:F

    goto/16 :goto_0

    .line 1416
    :cond_0
    iget v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLeftBottomX:F

    iget v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mRightBottomX:F

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    goto :goto_1

    .line 1417
    :cond_1
    iget v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mRightTopY:F

    iget v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mRightBottomY:F

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    goto :goto_2

    .line 1421
    .end local v0    # "centerX":F
    .end local v1    # "centerY":F
    :pswitch_1
    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_PORTRAIT_X:F

    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_SINGLE_WIDTH:F

    div-float/2addr v3, v5

    add-float v0, v2, v3

    .line 1422
    .restart local v0    # "centerX":F
    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_LANDSCAPE_Y:F

    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_SINGLE_PORTRAIT_Y:F

    add-float/2addr v2, v3

    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_SINGLE_HEIGHT:F

    div-float/2addr v3, v5

    add-float/2addr v2, v3

    const/high16 v3, 0x41700000    # 15.0f

    add-float v1, v2, v3

    .line 1423
    .restart local v1    # "centerY":F
    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_RATIO:F

    mul-float/2addr v2, p1

    add-float/2addr v2, v0

    iput v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLeftTopX:F

    .line 1424
    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_RATIO:F

    mul-float/2addr v2, p2

    add-float/2addr v2, v1

    iput v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLeftTopY:F

    .line 1425
    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_RATIO:F

    mul-float/2addr v2, p3

    add-float/2addr v2, v0

    iput v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mRightTopX:F

    .line 1426
    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_RATIO:F

    mul-float/2addr v2, p4

    add-float/2addr v2, v1

    iput v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mRightTopY:F

    .line 1427
    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_RATIO:F

    mul-float/2addr v2, p5

    add-float/2addr v2, v0

    iput v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mRightBottomX:F

    .line 1428
    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_RATIO:F

    mul-float/2addr v2, p6

    add-float/2addr v2, v1

    iput v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mRightBottomY:F

    .line 1429
    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_RATIO:F

    mul-float/2addr v2, p7

    add-float/2addr v2, v0

    iput v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLeftBottomX:F

    .line 1430
    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_RATIO:F

    mul-float/2addr v2, p8

    add-float/2addr v2, v1

    iput v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLeftBottomY:F

    .line 1431
    iget v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLeftTopX:F

    iget v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mRightTopX:F

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLeftBottomX:F

    iget v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mRightBottomX:F

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    cmpl-float v2, v2, v3

    if-lez v2, :cond_2

    iget v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLeftTopX:F

    iget v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mRightTopX:F

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    :goto_3
    iput v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWidth:F

    .line 1432
    iget v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLeftTopY:F

    iget v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLeftBottomY:F

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mRightTopY:F

    iget v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mRightBottomY:F

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    cmpl-float v2, v2, v3

    if-lez v2, :cond_3

    iget v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLeftTopY:F

    iget v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLeftBottomY:F

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    :goto_4
    iput v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mHeight:F

    goto/16 :goto_0

    .line 1431
    :cond_2
    iget v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLeftBottomX:F

    iget v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mRightBottomX:F

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    goto :goto_3

    .line 1432
    :cond_3
    iget v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mRightTopY:F

    iget v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mRightBottomY:F

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    goto :goto_4

    .line 1402
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public showCaptureAngleButton()V
    .locals 0

    .prologue
    .line 1066
    return-void
.end method

.method public showGuideArrow(I)V
    .locals 11
    .param p1, "state"    # I

    .prologue
    const/high16 v10, 0x40000000    # 2.0f

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x4

    const/4 v6, 0x0

    .line 2049
    iget v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideState:I

    if-ne v3, p1, :cond_1

    .line 2347
    :cond_0
    :goto_0
    return-void

    .line 2052
    :cond_1
    iput p1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideState:I

    .line 2053
    const/4 v0, 0x0

    .line 2054
    .local v0, "arrow_x":F
    const/4 v1, 0x0

    .line 2055
    .local v1, "arrow_y":F
    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mBlinkAnimation:Landroid/view/animation/AlphaAnimation;

    if-nez v3, :cond_2

    .line 2056
    new-instance v3, Landroid/view/animation/AlphaAnimation;

    const/4 v4, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-direct {v3, v4, v5}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mBlinkAnimation:Landroid/view/animation/AlphaAnimation;

    .line 2057
    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mBlinkAnimation:Landroid/view/animation/AlphaAnimation;

    const-wide/16 v4, 0xfa

    invoke-virtual {v3, v4, v5}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 2058
    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mBlinkAnimation:Landroid/view/animation/AlphaAnimation;

    const/4 v4, -0x1

    invoke-virtual {v3, v4}, Landroid/view/animation/AlphaAnimation;->setRepeatCount(I)V

    .line 2059
    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mBlinkAnimation:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v3, v9}, Landroid/view/animation/AlphaAnimation;->setRepeatMode(I)V

    .line 2062
    :cond_2
    if-nez p1, :cond_3

    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mTwoBlinkAnimation:Landroid/view/animation/AlphaAnimation;

    if-nez v3, :cond_3

    .line 2063
    new-instance v3, Landroid/view/animation/AlphaAnimation;

    const/high16 v4, 0x3f800000    # 1.0f

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-direct {v3, v4, v5}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mTwoBlinkAnimation:Landroid/view/animation/AlphaAnimation;

    .line 2064
    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mTwoBlinkAnimation:Landroid/view/animation/AlphaAnimation;

    const-wide/16 v4, 0x46

    invoke-virtual {v3, v4, v5}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 2065
    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mTwoBlinkAnimation:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v3, v8}, Landroid/view/animation/AlphaAnimation;->setRepeatCount(I)V

    .line 2066
    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mTwoBlinkAnimation:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v3, v9}, Landroid/view/animation/AlphaAnimation;->setRepeatMode(I)V

    .line 2067
    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mTwoBlinkAnimation:Landroid/view/animation/AlphaAnimation;

    new-instance v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu$3;

    invoke-direct {v4, p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu$3;-><init>(Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;)V

    invoke-virtual {v3, v4}, Landroid/view/animation/AlphaAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 2240
    :cond_3
    iget v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCurrentOrientation:I

    packed-switch v3, :pswitch_data_0

    goto :goto_0

    .line 2243
    :pswitch_0
    if-ne p1, v8, :cond_4

    .line 2244
    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_LANDSCAPE_X:F

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_LANDSCAPE_WIDTH:F

    div-float/2addr v4, v10

    add-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningArrow:[Lcom/sec/android/glview/TwGLImage;

    const/4 v5, 0x3

    aget-object v4, v4, v5

    invoke-virtual {v4}, Lcom/sec/android/glview/TwGLImage;->getWidth()F

    move-result v4

    div-float/2addr v4, v10

    sub-float v0, v3, v4

    .line 2245
    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_LANDSCAPE_Y:F

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_LANDSCAPE_HEIGHT:F

    add-float/2addr v3, v4

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->ARROW_LANDSCAPE_OVERLAY:F

    sub-float v1, v3, v4

    .line 2246
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->hideWarningArrow()V

    .line 2248
    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningArrow:[Lcom/sec/android/glview/TwGLImage;

    const/4 v4, 0x3

    aget-object v3, v3, v4

    invoke-virtual {v3, v0, v1}, Lcom/sec/android/glview/TwGLImage;->translateAbsolute(FF)V

    .line 2250
    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningArrow:[Lcom/sec/android/glview/TwGLImage;

    aget-object v3, v3, v6

    invoke-virtual {v3, v7}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 2251
    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningArrow:[Lcom/sec/android/glview/TwGLImage;

    aget-object v3, v3, v8

    invoke-virtual {v3, v7}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 2252
    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningArrow:[Lcom/sec/android/glview/TwGLImage;

    aget-object v3, v3, v9

    invoke-virtual {v3, v7}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 2253
    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningArrow:[Lcom/sec/android/glview/TwGLImage;

    const/4 v4, 0x3

    aget-object v3, v3, v4

    invoke-virtual {v3, v6}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 2254
    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningArrow:[Lcom/sec/android/glview/TwGLImage;

    const/4 v4, 0x3

    aget-object v3, v3, v4

    iget-object v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mBlinkAnimation:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v3, v4}, Lcom/sec/android/glview/TwGLImage;->setAnimation(Landroid/view/animation/Animation;)V

    .line 2255
    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningArrow:[Lcom/sec/android/glview/TwGLImage;

    const/4 v4, 0x3

    aget-object v3, v3, v4

    invoke-virtual {v3}, Lcom/sec/android/glview/TwGLImage;->startAnimation()V

    goto/16 :goto_0

    .line 2257
    :cond_4
    if-ne p1, v9, :cond_5

    .line 2258
    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_LANDSCAPE_X:F

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_LANDSCAPE_WIDTH:F

    div-float/2addr v4, v10

    add-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningArrow:[Lcom/sec/android/glview/TwGLImage;

    const/4 v5, 0x3

    aget-object v4, v4, v5

    invoke-virtual {v4}, Lcom/sec/android/glview/TwGLImage;->getWidth()F

    move-result v4

    div-float/2addr v4, v10

    sub-float v0, v3, v4

    .line 2259
    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_LANDSCAPE_Y:F

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_LANDSCAPE_HEIGHT:F

    add-float/2addr v3, v4

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->ARROW_LANDSCAPE_OVERLAY:F

    sub-float v1, v3, v4

    .line 2260
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->hideWarningArrow()V

    .line 2262
    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningArrow:[Lcom/sec/android/glview/TwGLImage;

    aget-object v3, v3, v9

    invoke-virtual {v3, v0, v1}, Lcom/sec/android/glview/TwGLImage;->translateAbsolute(FF)V

    .line 2264
    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningArrow:[Lcom/sec/android/glview/TwGLImage;

    aget-object v3, v3, v6

    invoke-virtual {v3, v7}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 2265
    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningArrow:[Lcom/sec/android/glview/TwGLImage;

    aget-object v3, v3, v8

    invoke-virtual {v3, v7}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 2266
    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningArrow:[Lcom/sec/android/glview/TwGLImage;

    aget-object v3, v3, v9

    invoke-virtual {v3, v6}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 2267
    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningArrow:[Lcom/sec/android/glview/TwGLImage;

    const/4 v4, 0x3

    aget-object v3, v3, v4

    invoke-virtual {v3, v7}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 2268
    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningArrow:[Lcom/sec/android/glview/TwGLImage;

    aget-object v3, v3, v9

    iget-object v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mBlinkAnimation:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v3, v4}, Lcom/sec/android/glview/TwGLImage;->setAnimation(Landroid/view/animation/Animation;)V

    .line 2269
    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningArrow:[Lcom/sec/android/glview/TwGLImage;

    aget-object v3, v3, v9

    invoke-virtual {v3}, Lcom/sec/android/glview/TwGLImage;->startAnimation()V

    goto/16 :goto_0

    .line 2270
    :cond_5
    if-nez p1, :cond_0

    .line 2271
    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_LANDSCAPE_X:F

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_LANDSCAPE_WIDTH:F

    div-float/2addr v4, v10

    add-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideArrow:[Lcom/sec/android/glview/TwGLImage;

    const/4 v5, 0x3

    aget-object v4, v4, v5

    invoke-virtual {v4}, Lcom/sec/android/glview/TwGLImage;->getWidth()F

    move-result v4

    div-float/2addr v4, v10

    sub-float v0, v3, v4

    .line 2272
    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_LANDSCAPE_Y:F

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_LANDSCAPE_HEIGHT:F

    add-float/2addr v3, v4

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->ARROW_LANDSCAPE_OVERLAY:F

    sub-float v1, v3, v4

    .line 2273
    iput-boolean v6, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mAnimationDirection:Z

    .line 2274
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->hideWarningArrow()V

    .line 2275
    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPreviewFocusRect:Lcom/sec/android/glview/TwGLNinePatch;

    if-eqz v3, :cond_6

    .line 2276
    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPreviewFocusRect:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v3, v7}, Lcom/sec/android/glview/TwGLNinePatch;->setVisibility(I)V

    .line 2277
    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureFocusRect:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v3, v7}, Lcom/sec/android/glview/TwGLNinePatch;->setVisibility(I)V

    .line 2279
    :cond_6
    invoke-virtual {p0, v6}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->showGuideRect(I)V

    .line 2280
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    const/4 v3, 0x7

    if-ge v2, v3, :cond_7

    .line 2281
    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideArrow:[Lcom/sec/android/glview/TwGLImage;

    aget-object v3, v3, v2

    invoke-virtual {v3, v0, v1}, Lcom/sec/android/glview/TwGLImage;->translateAbsolute(FF)V

    .line 2280
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2283
    :cond_7
    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideArrow:[Lcom/sec/android/glview/TwGLImage;

    aget-object v3, v3, v6

    invoke-virtual {v3, v6}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 2285
    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideArrow:[Lcom/sec/android/glview/TwGLImage;

    aget-object v3, v3, v6

    iget-object v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mTwoBlinkAnimation:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v3, v4}, Lcom/sec/android/glview/TwGLImage;->setAnimation(Landroid/view/animation/Animation;)V

    .line 2286
    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideArrow:[Lcom/sec/android/glview/TwGLImage;

    aget-object v3, v3, v6

    invoke-virtual {v3}, Lcom/sec/android/glview/TwGLImage;->startAnimation()V

    goto/16 :goto_0

    .line 2292
    .end local v2    # "i":I
    :pswitch_1
    if-ne p1, v8, :cond_8

    .line 2293
    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_PORTRAIT_X:F

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_SINGLE_PORTRAIT_WIDTH:F

    add-float/2addr v3, v4

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->ARROW_MARGIN:F

    add-float v0, v3, v4

    .line 2294
    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_PORTRAIT_Y:F

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_PORTRAIT_HEIGHT:F

    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningArrow:[Lcom/sec/android/glview/TwGLImage;

    aget-object v5, v5, v8

    invoke-virtual {v5}, Lcom/sec/android/glview/TwGLImage;->getHeight()F

    move-result v5

    sub-float/2addr v4, v5

    div-float/2addr v4, v10

    add-float v1, v3, v4

    .line 2295
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->hideWarningArrow()V

    .line 2297
    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningArrow:[Lcom/sec/android/glview/TwGLImage;

    aget-object v3, v3, v6

    invoke-virtual {v3, v0, v1}, Lcom/sec/android/glview/TwGLImage;->translateAbsolute(FF)V

    .line 2299
    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningArrow:[Lcom/sec/android/glview/TwGLImage;

    aget-object v3, v3, v6

    invoke-virtual {v3, v6}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 2300
    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningArrow:[Lcom/sec/android/glview/TwGLImage;

    aget-object v3, v3, v8

    invoke-virtual {v3, v7}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 2301
    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningArrow:[Lcom/sec/android/glview/TwGLImage;

    aget-object v3, v3, v9

    invoke-virtual {v3, v7}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 2302
    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningArrow:[Lcom/sec/android/glview/TwGLImage;

    const/4 v4, 0x3

    aget-object v3, v3, v4

    invoke-virtual {v3, v7}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 2303
    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningArrow:[Lcom/sec/android/glview/TwGLImage;

    aget-object v3, v3, v6

    invoke-virtual {v3, v6}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 2304
    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningArrow:[Lcom/sec/android/glview/TwGLImage;

    aget-object v3, v3, v6

    iget-object v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mBlinkAnimation:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v3, v4}, Lcom/sec/android/glview/TwGLImage;->setAnimation(Landroid/view/animation/Animation;)V

    .line 2305
    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningArrow:[Lcom/sec/android/glview/TwGLImage;

    aget-object v3, v3, v6

    invoke-virtual {v3}, Lcom/sec/android/glview/TwGLImage;->startAnimation()V

    goto/16 :goto_0

    .line 2307
    :cond_8
    if-ne p1, v9, :cond_9

    .line 2308
    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_PORTRAIT_X:F

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_SINGLE_PORTRAIT_WIDTH:F

    add-float/2addr v3, v4

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->ARROW_MARGIN:F

    add-float v0, v3, v4

    .line 2309
    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_PORTRAIT_Y:F

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_PORTRAIT_HEIGHT:F

    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningArrow:[Lcom/sec/android/glview/TwGLImage;

    aget-object v5, v5, v8

    invoke-virtual {v5}, Lcom/sec/android/glview/TwGLImage;->getHeight()F

    move-result v5

    sub-float/2addr v4, v5

    div-float/2addr v4, v10

    add-float v1, v3, v4

    .line 2310
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->hideWarningArrow()V

    .line 2312
    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningArrow:[Lcom/sec/android/glview/TwGLImage;

    aget-object v3, v3, v8

    invoke-virtual {v3, v0, v1}, Lcom/sec/android/glview/TwGLImage;->translateAbsolute(FF)V

    .line 2314
    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningArrow:[Lcom/sec/android/glview/TwGLImage;

    aget-object v3, v3, v6

    invoke-virtual {v3, v7}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 2315
    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningArrow:[Lcom/sec/android/glview/TwGLImage;

    aget-object v3, v3, v8

    invoke-virtual {v3, v6}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 2316
    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningArrow:[Lcom/sec/android/glview/TwGLImage;

    aget-object v3, v3, v9

    invoke-virtual {v3, v7}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 2317
    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningArrow:[Lcom/sec/android/glview/TwGLImage;

    const/4 v4, 0x3

    aget-object v3, v3, v4

    invoke-virtual {v3, v7}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 2318
    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningArrow:[Lcom/sec/android/glview/TwGLImage;

    aget-object v3, v3, v8

    iget-object v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mBlinkAnimation:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v3, v4}, Lcom/sec/android/glview/TwGLImage;->setAnimation(Landroid/view/animation/Animation;)V

    .line 2319
    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningArrow:[Lcom/sec/android/glview/TwGLImage;

    aget-object v3, v3, v8

    invoke-virtual {v3}, Lcom/sec/android/glview/TwGLImage;->startAnimation()V

    goto/16 :goto_0

    .line 2320
    :cond_9
    if-nez p1, :cond_0

    .line 2321
    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_PORTRAIT_X:F

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_SINGLE_PORTRAIT_WIDTH:F

    add-float/2addr v3, v4

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->ARROW_MARGIN:F

    add-float v0, v3, v4

    .line 2322
    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_PORTRAIT_Y:F

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_PORTRAIT_HEIGHT:F

    iget-object v5, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningArrow:[Lcom/sec/android/glview/TwGLImage;

    aget-object v5, v5, v8

    invoke-virtual {v5}, Lcom/sec/android/glview/TwGLImage;->getHeight()F

    move-result v5

    sub-float/2addr v4, v5

    div-float/2addr v4, v10

    add-float v1, v3, v4

    .line 2323
    iput-boolean v6, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mAnimationDirection:Z

    .line 2324
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->hideWarningArrow()V

    .line 2325
    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPreviewFocusRect:Lcom/sec/android/glview/TwGLNinePatch;

    if-eqz v3, :cond_a

    .line 2326
    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPreviewFocusRect:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v3, v7}, Lcom/sec/android/glview/TwGLNinePatch;->setVisibility(I)V

    .line 2327
    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureFocusRect:Lcom/sec/android/glview/TwGLNinePatch;

    invoke-virtual {v3, v7}, Lcom/sec/android/glview/TwGLNinePatch;->setVisibility(I)V

    .line 2328
    const-string v3, "TwGLUltraWideShotMenu"

    const-string v4, "mCaptureFocusRect is INVISIBLE 1"

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 2330
    :cond_a
    const/4 v2, 0x7

    .restart local v2    # "i":I
    :goto_2
    const/16 v3, 0xe

    if-ge v2, v3, :cond_b

    .line 2331
    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideArrow:[Lcom/sec/android/glview/TwGLImage;

    aget-object v3, v3, v2

    invoke-virtual {v3, v0, v1}, Lcom/sec/android/glview/TwGLImage;->translateAbsolute(FF)V

    .line 2330
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 2333
    :cond_b
    const/4 v2, 0x7

    :goto_3
    const/16 v3, 0xe

    if-ge v2, v3, :cond_d

    .line 2334
    iget v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureAngle:I

    const/16 v4, 0x96

    if-ne v3, v4, :cond_c

    .line 2335
    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideRect:[Lcom/sec/android/glview/TwGLImage;

    aget-object v3, v3, v2

    const/4 v4, 0x0

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->CAPTURE_ANGLE_MARGIN:F

    div-float/2addr v5, v10

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/glview/TwGLImage;->translateAbsolute(FF)V

    .line 2333
    :goto_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 2337
    :cond_c
    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideRect:[Lcom/sec/android/glview/TwGLImage;

    aget-object v3, v3, v2

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/glview/TwGLImage;->translateAbsolute(FF)V

    goto :goto_4

    .line 2339
    :cond_d
    const/4 v3, 0x7

    invoke-virtual {p0, v3}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->showGuideRect(I)V

    .line 2340
    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideArrow:[Lcom/sec/android/glview/TwGLImage;

    const/4 v4, 0x7

    aget-object v3, v3, v4

    invoke-virtual {v3, v6}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 2341
    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideArrow:[Lcom/sec/android/glview/TwGLImage;

    const/4 v4, 0x7

    aget-object v3, v3, v4

    iget-object v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mTwoBlinkAnimation:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v3, v4}, Lcom/sec/android/glview/TwGLImage;->setAnimation(Landroid/view/animation/Animation;)V

    .line 2342
    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideArrow:[Lcom/sec/android/glview/TwGLImage;

    const/4 v4, 0x7

    aget-object v3, v3, v4

    invoke-virtual {v3}, Lcom/sec/android/glview/TwGLImage;->startAnimation()V

    goto/16 :goto_0

    .line 2240
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public showGuideRect(I)V
    .locals 3
    .param p1, "num"    # I

    .prologue
    .line 2033
    iget-boolean v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mMovingDetection:Z

    if-eqz v1, :cond_0

    .line 2040
    :goto_0
    return-void

    .line 2036
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    const/16 v1, 0xe

    if-ge v0, v1, :cond_1

    .line 2037
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideRect:[Lcom/sec/android/glview/TwGLImage;

    aget-object v1, v1, v0

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 2036
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2039
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideRect:[Lcom/sec/android/glview/TwGLImage;

    aget-object v1, v1, p1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    goto :goto_0
.end method

.method public showGuideText(I)V
    .locals 5
    .param p1, "guideType"    # I

    .prologue
    const/4 v4, 0x5

    const/4 v3, 0x0

    .line 551
    const-string v0, "TwGLUltraWideShotMenu"

    const-string v1, "showGuideText"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 552
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideText:Lcom/sec/android/glview/TwGLText;

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mPostProgress:I

    if-nez v0, :cond_1

    .line 553
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mUltraWideShotMsgHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 554
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mUltraWideShotMsgHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 556
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 572
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideText:Lcom/sec/android/glview/TwGLText;

    invoke-virtual {v0, v3}, Lcom/sec/android/glview/TwGLText;->setVisibility(I)V

    .line 573
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mUltraWideShotMsgHandler:Landroid/os/Handler;

    if-eqz v0, :cond_1

    .line 574
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mUltraWideShotMsgHandler:Landroid/os/Handler;

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 577
    :cond_1
    return-void

    .line 558
    :pswitch_0
    invoke-virtual {p0, v3}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->showGuideArrow(I)V

    .line 559
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideText:Lcom/sec/android/glview/TwGLText;

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    const v2, 0x7f0c01d8

    invoke-virtual {v1, v2}, Lcom/sec/android/app/camera/Camera;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLText;->setText(Ljava/lang/String;)V

    goto :goto_0

    .line 562
    :pswitch_1
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->showGuideArrow(I)V

    .line 563
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideText:Lcom/sec/android/glview/TwGLText;

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    const v2, 0x7f0c01d9

    invoke-virtual {v1, v2}, Lcom/sec/android/app/camera/Camera;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLText;->setText(Ljava/lang/String;)V

    goto :goto_0

    .line 566
    :pswitch_2
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->showGuideArrow(I)V

    .line 567
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideText:Lcom/sec/android/glview/TwGLText;

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    const v2, 0x7f0c01da

    invoke-virtual {v1, v2}, Lcom/sec/android/app/camera/Camera;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLText;->setText(Ljava/lang/String;)V

    goto :goto_0

    .line 556
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public showNextCaptureFocusRect()V
    .locals 5

    .prologue
    const/4 v3, 0x4

    const/4 v4, 0x0

    .line 2595
    const-string v0, "TwGLUltraWideShotMenu"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "showNextCaptureFocusRect left : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mNextCaptureFocusRectLeft:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " top : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mNextCaptureFocusRectTop:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 2596
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->isUltraWideShotCapturing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2618
    :goto_0
    return-void

    .line 2599
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mNextCaptureFocusRect:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v0}, Lcom/sec/android/glview/TwGLImage;->resetTranslate()V

    .line 2600
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mNextCaptureFocusRectPort:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v0}, Lcom/sec/android/glview/TwGLImage;->resetTranslate()V

    .line 2602
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCurrentOrientation:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 2605
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mNextCaptureFocusRectPort:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v0, v3}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 2606
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mNextCaptureFocusRect:Lcom/sec/android/glview/TwGLImage;

    iget v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mNextCaptureFocusRectLeft:F

    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->LIVEPREVIEW_LANDSCAPE_Y:F

    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->NEXT_CAPTURE_FOCUS_LINE_WIDTH:F

    sub-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/glview/TwGLImage;->translateAbsolute(FF)V

    .line 2607
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mNextCaptureFocusRect:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v0, v4}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    goto :goto_0

    .line 2611
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mNextCaptureFocusRect:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v0, v3}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 2612
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mNextCaptureFocusRectPort:Lcom/sec/android/glview/TwGLImage;

    sget v1, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->NEXT_CAPTURE_FOCUS_PORTRAIT_X:F

    iget v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mNextCaptureFocusRectTop:F

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/glview/TwGLImage;->translateAbsolute(FF)V

    .line 2613
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mNextCaptureFocusRectPort:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v0, v4}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    goto :goto_0

    .line 2602
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public showPreviewGroup()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 884
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->getShootingMode()I

    move-result v0

    const/16 v1, 0x34

    if-eq v0, v1, :cond_1

    .line 911
    :cond_0
    :goto_0
    return-void

    .line 886
    :cond_1
    const-string v0, "TwGLUltraWideShotMenu"

    const-string v1, "showPreviewGroup"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 888
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->isMenuOpened()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 889
    const-string v0, "TwGLUltraWideShotMenu"

    const-string v1, "showPreviewGroup, isMenuOpened"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 897
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->refreshLivePreviewPosition()V

    .line 898
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLivePreviewGroup:Lcom/sec/android/glview/TwGLViewGroup;

    if-eqz v0, :cond_3

    .line 899
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLivePreviewGroup:Lcom/sec/android/glview/TwGLViewGroup;

    invoke-virtual {v0, v2}, Lcom/sec/android/glview/TwGLViewGroup;->setVisibility(I)V

    .line 901
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mBackgroundRectGroup:Lcom/sec/android/glview/TwGLViewGroup;

    if-eqz v0, :cond_0

    .line 902
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mBackgroundRectGroup:Lcom/sec/android/glview/TwGLViewGroup;

    invoke-virtual {v0, v2}, Lcom/sec/android/glview/TwGLViewGroup;->setVisibility(I)V

    goto :goto_0
.end method

.method public showStopButton()V
    .locals 2

    .prologue
    .line 1020
    const-string v0, "TwGLUltraWideShotMenu"

    const-string v1, "showStopButton"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1021
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCaptureStopButton:Lcom/sec/android/glview/TwGLButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLButton;->setVisibility(I)V

    .line 1022
    return-void
.end method

.method public showWarningArrow(FFI)V
    .locals 4
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "direction"    # I

    .prologue
    const/16 v2, 0xe

    const/4 v3, 0x4

    .line 2010
    sget v1, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->WARNING_NONE:I

    if-ne p3, v1, :cond_0

    .line 2031
    :goto_0
    return-void

    .line 2013
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningArrow:[Lcom/sec/android/glview/TwGLImage;

    aget-object v1, v1, p3

    invoke-virtual {v1, p1, p2}, Lcom/sec/android/glview/TwGLImage;->translateAbsolute(FF)V

    .line 2014
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v2, :cond_1

    .line 2015
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideRect:[Lcom/sec/android/glview/TwGLImage;

    aget-object v1, v1, v0

    invoke-virtual {v1, v3}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 2014
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2017
    :cond_1
    const/4 v0, 0x0

    :goto_2
    if-ge v0, v2, :cond_2

    .line 2018
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideArrow:[Lcom/sec/android/glview/TwGLImage;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/sec/android/glview/TwGLImage;->resetTranslate()V

    .line 2019
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideArrow:[Lcom/sec/android/glview/TwGLImage;

    aget-object v1, v1, v0

    invoke-virtual {v1, v3}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 2017
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 2022
    :cond_2
    const/4 v0, 0x0

    :goto_3
    if-ge v0, v3, :cond_4

    .line 2023
    if-ne v0, p3, :cond_3

    .line 2024
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningArrow:[Lcom/sec/android/glview/TwGLImage;

    aget-object v1, v1, v0

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 2022
    :goto_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 2026
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningArrow:[Lcom/sec/android/glview/TwGLImage;

    aget-object v1, v1, v0

    invoke-virtual {v1, v3}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    goto :goto_4

    .line 2029
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->hideGuideText()V

    .line 2030
    invoke-virtual {p0, p3}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->showWarningText(I)V

    goto :goto_0
.end method

.method public showWarningText(I)V
    .locals 6
    .param p1, "warningType"    # I

    .prologue
    const v5, 0x7f0c01e1

    const v4, 0x7f0c01e0

    const v3, 0x7f0c01df

    const v2, 0x7f0c01de

    const/4 v1, 0x2

    .line 588
    packed-switch p1, :pswitch_data_0

    .line 619
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningText:Lcom/sec/android/glview/TwGLText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLText;->setVisibility(I)V

    .line 620
    return-void

    .line 590
    :pswitch_0
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCurrentOrientation:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCurrentOrientation:I

    if-ne v0, v1, :cond_1

    .line 591
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningText:Lcom/sec/android/glview/TwGLText;

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/camera/Camera;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLText;->setText(Ljava/lang/String;)V

    goto :goto_0

    .line 593
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningText:Lcom/sec/android/glview/TwGLText;

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v1, v5}, Lcom/sec/android/app/camera/Camera;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLText;->setText(Ljava/lang/String;)V

    goto :goto_0

    .line 596
    :pswitch_1
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCurrentOrientation:I

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCurrentOrientation:I

    if-ne v0, v1, :cond_3

    .line 597
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningText:Lcom/sec/android/glview/TwGLText;

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v1, v3}, Lcom/sec/android/app/camera/Camera;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLText;->setText(Ljava/lang/String;)V

    goto :goto_0

    .line 599
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningText:Lcom/sec/android/glview/TwGLText;

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v1, v4}, Lcom/sec/android/app/camera/Camera;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLText;->setText(Ljava/lang/String;)V

    goto :goto_0

    .line 602
    :pswitch_2
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCurrentOrientation:I

    if-eqz v0, :cond_4

    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCurrentOrientation:I

    if-ne v0, v1, :cond_5

    .line 603
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningText:Lcom/sec/android/glview/TwGLText;

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v1, v4}, Lcom/sec/android/app/camera/Camera;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLText;->setText(Ljava/lang/String;)V

    goto :goto_0

    .line 605
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningText:Lcom/sec/android/glview/TwGLText;

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/camera/Camera;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLText;->setText(Ljava/lang/String;)V

    goto :goto_0

    .line 608
    :pswitch_3
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCurrentOrientation:I

    if-eqz v0, :cond_6

    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCurrentOrientation:I

    if-ne v0, v1, :cond_7

    .line 609
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningText:Lcom/sec/android/glview/TwGLText;

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v1, v5}, Lcom/sec/android/app/camera/Camera;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLText;->setText(Ljava/lang/String;)V

    goto :goto_0

    .line 611
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningText:Lcom/sec/android/glview/TwGLText;

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v1, v3}, Lcom/sec/android/app/camera/Camera;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLText;->setText(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 614
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mWarningText:Lcom/sec/android/glview/TwGLText;

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    const v2, 0x7f0c01dd

    invoke-virtual {v1, v2}, Lcom/sec/android/app/camera/Camera;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLText;->setText(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 588
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public startCancelTimer()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 1030
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mUltraWideShotMsgHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1031
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mUltraWideShotMsgHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1033
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mUltraWideShotMsgHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x4e20

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 1034
    return-void
.end method

.method public startGuideAnimation(I)V
    .locals 8
    .param p1, "direction"    # I

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 650
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->getShootingMode()I

    move-result v0

    const/16 v1, 0x34

    if-eq v0, v1, :cond_1

    .line 753
    :cond_0
    :goto_0
    return-void

    .line 653
    :cond_1
    const-string v0, "TwGLUltraWideShotMenu"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "startGuideAnimation : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 655
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->isMenuOpened()Z

    move-result v0

    if-nez v0, :cond_0

    .line 658
    iget-boolean v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mIsStartCapture:Z

    if-eq v0, v4, :cond_0

    .line 661
    iput p1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideDirection:I

    .line 663
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCurrentOrientation:I

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCurrentOrientation:I

    if-ne v0, v6, :cond_7

    .line 664
    :cond_2
    iput v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideOrient:I

    .line 676
    :cond_3
    :goto_1
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideDirection:I

    if-nez v0, :cond_4

    .line 677
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCenterGuideImage:[Lcom/sec/android/glview/TwGLImage;

    iget v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideOrient:I

    aget-object v0, v0, v1

    invoke-virtual {v0, v3}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 678
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCenterGuideImage:[Lcom/sec/android/glview/TwGLImage;

    iget v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideOrient:I

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideBlinkingAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLImage;->setAnimation(Landroid/view/animation/Animation;)V

    .line 679
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCenterGuideImage:[Lcom/sec/android/glview/TwGLImage;

    iget v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideOrient:I

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/sec/android/glview/TwGLImage;->startAnimation()V

    .line 682
    :cond_4
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCurrentOrientation:I

    if-eqz v0, :cond_5

    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCurrentOrientation:I

    if-ne v0, v7, :cond_a

    .line 683
    :cond_5
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideDirection:I

    if-ne v0, v4, :cond_9

    .line 684
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLeftGuideImage:[Lcom/sec/android/glview/TwGLImage;

    iget v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideOrient:I

    aget-object v0, v0, v1

    invoke-virtual {v0, v3}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 685
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLeftGuideImage:[Lcom/sec/android/glview/TwGLImage;

    iget v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideOrient:I

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideBlinkingAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLImage;->setAnimation(Landroid/view/animation/Animation;)V

    .line 686
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLeftGuideImage:[Lcom/sec/android/glview/TwGLImage;

    iget v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideOrient:I

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/sec/android/glview/TwGLImage;->startAnimation()V

    .line 704
    :cond_6
    :goto_2
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideBlinkingAnimation:Landroid/view/animation/Animation;

    new-instance v1, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu$2;-><init>(Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    goto/16 :goto_0

    .line 665
    :cond_7
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCurrentOrientation:I

    if-eq v0, v4, :cond_8

    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCurrentOrientation:I

    if-ne v0, v7, :cond_0

    .line 666
    :cond_8
    iput v4, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideOrient:I

    .line 667
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->isEasyMode()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 668
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCenterGuideImage:[Lcom/sec/android/glview/TwGLImage;

    iget v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideOrient:I

    aget-object v0, v0, v1

    sget v1, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->PREVIEW_EASYMODE_GUIDE_PORTRAIT_MARGIN:F

    invoke-virtual {v0, v1, v5}, Lcom/sec/android/glview/TwGLImage;->translateAbsolute(FF)V

    .line 669
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLeftGuideImage:[Lcom/sec/android/glview/TwGLImage;

    iget v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideOrient:I

    aget-object v0, v0, v1

    sget v1, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->PREVIEW_EASYMODE_GUIDE_PORTRAIT_MARGIN:F

    invoke-virtual {v0, v1, v5}, Lcom/sec/android/glview/TwGLImage;->translateAbsolute(FF)V

    .line 670
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mRightGuideImage:[Lcom/sec/android/glview/TwGLImage;

    iget v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideOrient:I

    aget-object v0, v0, v1

    sget v1, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->PREVIEW_EASYMODE_GUIDE_PORTRAIT_MARGIN:F

    invoke-virtual {v0, v1, v5}, Lcom/sec/android/glview/TwGLImage;->translateAbsolute(FF)V

    goto/16 :goto_1

    .line 687
    :cond_9
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideDirection:I

    if-ne v0, v6, :cond_6

    .line 688
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mRightGuideImage:[Lcom/sec/android/glview/TwGLImage;

    iget v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideOrient:I

    aget-object v0, v0, v1

    invoke-virtual {v0, v3}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 689
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mRightGuideImage:[Lcom/sec/android/glview/TwGLImage;

    iget v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideOrient:I

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideBlinkingAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLImage;->setAnimation(Landroid/view/animation/Animation;)V

    .line 690
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mRightGuideImage:[Lcom/sec/android/glview/TwGLImage;

    iget v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideOrient:I

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/sec/android/glview/TwGLImage;->startAnimation()V

    goto :goto_2

    .line 693
    :cond_a
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideDirection:I

    if-ne v0, v4, :cond_b

    .line 694
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mRightGuideImage:[Lcom/sec/android/glview/TwGLImage;

    iget v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideOrient:I

    aget-object v0, v0, v1

    invoke-virtual {v0, v3}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 695
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mRightGuideImage:[Lcom/sec/android/glview/TwGLImage;

    iget v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideOrient:I

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideBlinkingAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLImage;->setAnimation(Landroid/view/animation/Animation;)V

    .line 696
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mRightGuideImage:[Lcom/sec/android/glview/TwGLImage;

    iget v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideOrient:I

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/sec/android/glview/TwGLImage;->startAnimation()V

    goto/16 :goto_2

    .line 697
    :cond_b
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideDirection:I

    if-ne v0, v6, :cond_6

    .line 698
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLeftGuideImage:[Lcom/sec/android/glview/TwGLImage;

    iget v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideOrient:I

    aget-object v0, v0, v1

    invoke-virtual {v0, v3}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 699
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLeftGuideImage:[Lcom/sec/android/glview/TwGLImage;

    iget v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideOrient:I

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideBlinkingAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLImage;->setAnimation(Landroid/view/animation/Animation;)V

    .line 700
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLeftGuideImage:[Lcom/sec/android/glview/TwGLImage;

    iget v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mGuideOrient:I

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/sec/android/glview/TwGLImage;->startAnimation()V

    goto/16 :goto_2
.end method

.method public stopCancelTimer()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1043
    const-string v0, "TwGLUltraWideShotMenu"

    const-string v1, "stopCancelTimer..."

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1044
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mUltraWideShotMsgHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mUltraWideShotMsgHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1045
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mUltraWideShotMsgHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 1047
    :cond_0
    return-void
.end method

.method public stopGuideAnimation()V
    .locals 4

    .prologue
    const/4 v3, 0x4

    .line 627
    const-string v1, "TwGLUltraWideShotMenu"

    const-string v2, "stopGuideAnimation"

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 628
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    .line 629
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLeftGuideImage:[Lcom/sec/android/glview/TwGLImage;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/sec/android/glview/TwGLImage;->cancelAnimation()V

    .line 630
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCenterGuideImage:[Lcom/sec/android/glview/TwGLImage;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/sec/android/glview/TwGLImage;->cancelAnimation()V

    .line 631
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mRightGuideImage:[Lcom/sec/android/glview/TwGLImage;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/sec/android/glview/TwGLImage;->cancelAnimation()V

    .line 632
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mLeftGuideImage:[Lcom/sec/android/glview/TwGLImage;

    aget-object v1, v1, v0

    invoke-virtual {v1, v3}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 633
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mCenterGuideImage:[Lcom/sec/android/glview/TwGLImage;

    aget-object v1, v1, v0

    invoke-virtual {v1, v3}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 634
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mRightGuideImage:[Lcom/sec/android/glview/TwGLImage;

    aget-object v1, v1, v0

    invoke-virtual {v1, v3}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 628
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 636
    :cond_0
    return-void
.end method

.method public stopUltraWideShotSound()V
    .locals 3

    .prologue
    const/4 v2, 0x3

    const/4 v1, 0x2

    .line 1368
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mUltraWideShotMsgHandler:Landroid/os/Handler;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mUltraWideShotMsgHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mUltraWideShotMsgHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1369
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mUltraWideShotMsgHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1370
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mUltraWideShotMsgHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 1373
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    if-eqz v0, :cond_2

    .line 1374
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLUltraWideShotMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    sget v1, Lcom/sec/android/app/camera/Camera;->SHUTTER_SOUND_PANORAMA_WARNING:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/camera/Camera;->stopCameraSound(I)V

    .line 1376
    :cond_2
    return-void
.end method
