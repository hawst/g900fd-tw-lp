.class Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;
.super Landroid/os/Handler;
.source "TwGLVirtualTourMenu.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;


# direct methods
.method constructor <init>(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)V
    .locals 0

    .prologue
    .line 266
    iput-object p1, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 10
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/16 v9, 0x30

    const/4 v8, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x4

    const/4 v7, 0x0

    .line 269
    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mHandlerFlagMutexObject:Ljava/lang/Object;
    invoke-static {}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$000()Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3

    .line 270
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$100(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/app/camera/Camera;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$100(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/app/camera/Camera;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v2

    if-nez v2, :cond_2

    .line 271
    :cond_0
    monitor-exit v3

    .line 693
    :cond_1
    :goto_0
    :sswitch_0
    return-void

    .line 272
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    invoke-virtual {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->isActive()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mHandlerActiveFlag:Z
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$200(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Z

    move-result v2

    if-nez v2, :cond_4

    :cond_3
    iget v2, p1, Landroid/os/Message;->what:I

    const/16 v4, 0x3e8

    if-eq v2, v4, :cond_4

    .line 273
    monitor-exit v3

    goto :goto_0

    .line 275
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    :cond_4
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 276
    iget v2, p1, Landroid/os/Message;->what:I

    sparse-switch v2, :sswitch_data_0

    goto :goto_0

    .line 278
    :sswitch_1
    const-string v2, "TwGLVirtualTourMenu"

    const-string v3, "Calibration started"

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 279
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mUndoButton:Lcom/sec/android/glview/TwGLButton;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$300(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLButton;

    move-result-object v2

    invoke-virtual {v2, v6}, Lcom/sec/android/glview/TwGLButton;->setDim(Z)V

    .line 280
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mDoneButton:Lcom/sec/android/glview/TwGLButton;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$400(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLButton;

    move-result-object v2

    invoke-virtual {v2, v6}, Lcom/sec/android/glview/TwGLButton;->setDim(Z)V

    .line 281
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$100(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/app/camera/Camera;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/camera/Camera;->getCameraBaseMenu()Lcom/sec/android/app/camera/glwidget/TwGLCameraBaseMenu;

    move-result-object v2

    if-eqz v2, :cond_5

    .line 282
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$100(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/app/camera/Camera;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/camera/Camera;->getCameraBaseMenu()Lcom/sec/android/app/camera/glwidget/TwGLCameraBaseMenu;

    move-result-object v2

    invoke-virtual {v2, v7}, Lcom/sec/android/app/camera/glwidget/TwGLCameraBaseMenu;->setEditableSideBarDraggable(Z)V

    .line 284
    :cond_5
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$100(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/app/camera/Camera;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/camera/CameraSettings;->getShootingMode()I

    move-result v2

    if-ne v2, v9, :cond_6

    .line 285
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$100(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/app/camera/Camera;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/camera/Camera;->getGLContext()Lcom/sec/android/glview/TwGLContext;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/sec/android/glview/TwGLContext;->playHaptic(I)V

    .line 286
    :cond_6
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mHelpText:Lcom/sec/android/glview/TwGLText;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$500(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLText;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;
    invoke-static {v3}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$100(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/app/camera/Camera;

    move-result-object v3

    const v4, 0x7f0c0280

    invoke-virtual {v3, v4}, Lcom/sec/android/app/camera/Camera;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/glview/TwGLText;->setText(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 290
    :sswitch_2
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mHelpText:Lcom/sec/android/glview/TwGLText;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$500(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLText;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;
    invoke-static {v3}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$100(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/app/camera/Camera;

    move-result-object v3

    const v4, 0x7f0c013c

    invoke-virtual {v3, v4}, Lcom/sec/android/app/camera/Camera;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/glview/TwGLText;->setText(Ljava/lang/String;)V

    .line 291
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # invokes: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->resetStabilizer()V
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$600(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)V

    goto/16 :goto_0

    .line 294
    :sswitch_3
    const-string v2, "TwGLVirtualTourMenu"

    const-string v3, "Calibration done"

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 295
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$100(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/app/camera/Camera;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/camera/Camera;->checkStorageLow()V

    .line 296
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$100(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/app/camera/Camera;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/camera/Camera;->getMemoryStatus()I

    move-result v2

    if-ne v2, v6, :cond_7

    .line 297
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$100(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/app/camera/Camera;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/camera/Camera;->mediaStorageDialog()V

    .line 298
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # invokes: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->onDone(Z)V
    invoke-static {v2, v7}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$700(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;Z)V

    goto/16 :goto_0

    .line 301
    :cond_7
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$100(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/app/camera/Camera;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/camera/Camera;->restartInactivityTimer()V

    .line 302
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # invokes: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->setProgress(I)V
    invoke-static {v2, v7}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$800(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;I)V

    .line 303
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # invokes: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->setStabilizerVisibleState(I)V
    invoke-static {v2, v7}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$900(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;I)V

    .line 304
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mGuideGroup:Lcom/sec/android/glview/TwGLViewGroup;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$1000(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLViewGroup;

    move-result-object v2

    invoke-virtual {v2, v7}, Lcom/sec/android/glview/TwGLViewGroup;->setVisibility(I)V

    .line 305
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mUndoButtonGroup:Lcom/sec/android/glview/TwGLViewGroup;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$1100(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLViewGroup;

    move-result-object v2

    invoke-virtual {v2, v7}, Lcom/sec/android/glview/TwGLViewGroup;->setVisibility(I)V

    .line 306
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mDoneButton:Lcom/sec/android/glview/TwGLButton;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$400(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLButton;

    move-result-object v2

    invoke-virtual {v2, v7}, Lcom/sec/android/glview/TwGLButton;->setVisibility(I)V

    .line 307
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mThumbnailOpenButton:Lcom/sec/android/glview/TwGLButton;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$1200(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLButton;

    move-result-object v2

    invoke-virtual {v2, v7}, Lcom/sec/android/glview/TwGLButton;->setVisibility(I)V

    .line 308
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$100(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/app/camera/Camera;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/camera/Camera;->hideBaseMenu()V

    .line 309
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$100(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/app/camera/Camera;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/camera/Camera;->getEngine()Lcom/sec/android/app/camera/CommonEngine;

    move-result-object v2

    if-eqz v2, :cond_8

    .line 310
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$100(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/app/camera/Camera;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/camera/Camera;->getEngine()Lcom/sec/android/app/camera/CommonEngine;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/camera/CommonEngine;->handleShutterReleaseEvent()V

    .line 311
    :cond_8
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mHelpText:Lcom/sec/android/glview/TwGLText;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$500(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLText;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;
    invoke-static {v3}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$100(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/app/camera/Camera;

    move-result-object v3

    const v4, 0x7f0c027f

    invoke-virtual {v3, v4}, Lcom/sec/android/app/camera/Camera;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/glview/TwGLText;->setText(Ljava/lang/String;)V

    .line 312
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # setter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mCapturing:Z
    invoke-static {v2, v6}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$1302(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;Z)Z

    .line 313
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mVirtualTourModeler:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$1400(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mCapturing:Z
    invoke-static {v3}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$1300(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->updateCapturingStatus(Z)V

    .line 314
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$100(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/app/camera/Camera;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/camera/CameraSettings;->getStorage()I

    move-result v2

    const-string v3, "/.3DTour"

    invoke-static {v2, v3}, Lcom/sec/android/app/camera/ImageSavingUtils;->getHiddenSavingDir(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 315
    .local v1, "tempDir":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "3DTour_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mCurrentPhotoCount:I
    invoke-static {v3}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$1500(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".jpg"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 316
    .local v0, "FileName":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mVirtualTourModeler:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$1400(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mPs32Direction:[I

    invoke-virtual {v2, v0, v3}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->onImageCaptured(Ljava/lang/String;[I)V

    .line 317
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # operator++ for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mCurrentPhotoCount:I
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$1508(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)I

    .line 318
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # invokes: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->setRemainCounter()V
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$1600(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)V

    .line 319
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mOrientation:I
    invoke-static {v3}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$1800(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)I

    move-result v3

    # setter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mOrientationForPicture:I
    invoke-static {v2, v3}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$1702(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;I)I

    goto/16 :goto_0

    .line 323
    .end local v0    # "FileName":Ljava/lang/String;
    .end local v1    # "tempDir":Ljava/lang/String;
    :sswitch_4
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mHelpText:Lcom/sec/android/glview/TwGLText;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$500(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLText;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;
    invoke-static {v3}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$100(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/app/camera/Camera;

    move-result-object v3

    const v4, 0x7f0c013c

    invoke-virtual {v3, v4}, Lcom/sec/android/app/camera/Camera;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/glview/TwGLText;->setText(Ljava/lang/String;)V

    .line 324
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$100(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/app/camera/Camera;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/camera/Camera;->getCameraBaseMenu()Lcom/sec/android/app/camera/glwidget/TwGLCameraBaseMenu;

    move-result-object v2

    if-eqz v2, :cond_9

    .line 325
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$100(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/app/camera/Camera;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/camera/Camera;->getCameraBaseMenu()Lcom/sec/android/app/camera/glwidget/TwGLCameraBaseMenu;

    move-result-object v2

    invoke-virtual {v2, v6}, Lcom/sec/android/app/camera/glwidget/TwGLCameraBaseMenu;->setEditableSideBarDraggable(Z)V

    .line 327
    :cond_9
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$100(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/app/camera/Camera;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/camera/CameraSettings;->getShootingMode()I

    move-result v2

    if-ne v2, v9, :cond_a

    .line 328
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$100(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/app/camera/Camera;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/camera/Camera;->getGLContext()Lcom/sec/android/glview/TwGLContext;

    move-result-object v2

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Lcom/sec/android/glview/TwGLContext;->playHaptic(I)V

    .line 329
    :cond_a
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # invokes: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->resetStabilizer()V
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$600(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)V

    goto/16 :goto_0

    .line 332
    :sswitch_5
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mCalibrationInProgressCount:I
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$1900(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)I

    move-result v2

    rem-int/lit8 v2, v2, 0xe

    const/16 v3, 0xa

    if-gt v2, v3, :cond_b

    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mCalibrationInProgressCount:I
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$1900(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)I

    move-result v2

    rem-int/lit8 v2, v2, 0xe

    if-eqz v2, :cond_b

    .line 334
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    const/16 v3, 0xc

    # += operator for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mProgressDegree:I
    invoke-static {v2, v3}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2012(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;I)I

    .line 335
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mProgressDegree:I
    invoke-static {v3}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2000(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)I

    move-result v3

    # invokes: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->setProgress(I)V
    invoke-static {v2, v3}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$800(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;I)V

    .line 337
    :cond_b
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # operator++ for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mCalibrationInProgressCount:I
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$1908(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)I

    goto/16 :goto_0

    .line 340
    :sswitch_6
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$100(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/app/camera/Camera;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/camera/Camera;->checkStorageLow()V

    .line 341
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$100(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/app/camera/Camera;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/camera/Camera;->getMemoryStatus()I

    move-result v2

    if-ne v2, v6, :cond_c

    .line 342
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$100(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/app/camera/Camera;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/camera/Camera;->mediaStorageDialog()V

    .line 343
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # invokes: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->onDone(Z)V
    invoke-static {v2, v6}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$700(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;Z)V

    goto/16 :goto_0

    .line 346
    :cond_c
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mCapturableProgressWheel:Lcom/sec/android/app/camera/glwidget/TwGLProgressWheel;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2100(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/app/camera/glwidget/TwGLProgressWheel;

    move-result-object v2

    const/16 v3, 0x168

    invoke-virtual {v2, v3}, Lcom/sec/android/app/camera/glwidget/TwGLProgressWheel;->setProgress(I)V

    .line 347
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$100(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/app/camera/Camera;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/camera/Camera;->restartInactivityTimer()V

    .line 348
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$100(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/app/camera/Camera;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/camera/Camera;->getEngine()Lcom/sec/android/app/camera/CommonEngine;

    move-result-object v2

    if-eqz v2, :cond_d

    .line 349
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$100(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/app/camera/Camera;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/camera/Camera;->getEngine()Lcom/sec/android/app/camera/CommonEngine;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/camera/CommonEngine;->handleShutterReleaseEvent()V

    .line 350
    :cond_d
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # setter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mCapturing:Z
    invoke-static {v2, v6}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$1302(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;Z)Z

    .line 351
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mVirtualTourModeler:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$1400(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mCapturing:Z
    invoke-static {v3}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$1300(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->updateCapturingStatus(Z)V

    .line 352
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$100(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/app/camera/Camera;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/camera/CameraSettings;->getStorage()I

    move-result v2

    const-string v3, "/.3DTour"

    invoke-static {v2, v3}, Lcom/sec/android/app/camera/ImageSavingUtils;->getHiddenSavingDir(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 353
    .restart local v1    # "tempDir":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "3DTour_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mCurrentPhotoCount:I
    invoke-static {v3}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$1500(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".jpg"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 354
    .restart local v0    # "FileName":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mVirtualTourModeler:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$1400(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mPs32Direction:[I

    invoke-virtual {v2, v0, v3}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->onImageCaptured(Ljava/lang/String;[I)V

    .line 355
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # operator++ for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mCurrentPhotoCount:I
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$1508(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)I

    .line 356
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # invokes: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->setRemainCounter()V
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$1600(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)V

    .line 357
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mCurrentPhotoCount:I
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$1500(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)I

    move-result v2

    if-lt v2, v8, :cond_e

    .line 358
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mUndoButton:Lcom/sec/android/glview/TwGLButton;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$300(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLButton;

    move-result-object v2

    invoke-virtual {v2, v7}, Lcom/sec/android/glview/TwGLButton;->setDim(Z)V

    .line 360
    :cond_e
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mDoneButton:Lcom/sec/android/glview/TwGLButton;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$400(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLButton;

    move-result-object v2

    invoke-virtual {v2, v7}, Lcom/sec/android/glview/TwGLButton;->setDim(Z)V

    .line 361
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2200(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Landroid/os/Handler;

    move-result-object v2

    new-instance v3, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1$1;

    invoke-direct {v3, p0}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1$1;-><init>(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;)V

    const-wide/16 v4, 0x1f4

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 370
    .end local v0    # "FileName":Ljava/lang/String;
    .end local v1    # "tempDir":Ljava/lang/String;
    :sswitch_7
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mLeftIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2300(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/glview/TwGLImage;->cancelAnimation()V

    .line 371
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mRightIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2400(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/glview/TwGLImage;->cancelAnimation()V

    .line 372
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mStraightIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2500(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/glview/TwGLImage;->cancelAnimation()V

    .line 373
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mStraightWarningIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2600(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/glview/TwGLImage;->cancelAnimation()V

    .line 374
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mLeftIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2300(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 375
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mRightIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2400(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 376
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mStraightIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2500(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    invoke-virtual {v2, v7}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 377
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mStraightWarningIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2600(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 378
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # invokes: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->setStabilizerVisibleState(I)V
    invoke-static {v2, v8}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$900(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;I)V

    .line 379
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mHelpText:Lcom/sec/android/glview/TwGLText;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$500(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLText;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;
    invoke-static {v3}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$100(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/app/camera/Camera;

    move-result-object v3

    const v4, 0x7f0c0277

    invoke-virtual {v3, v4}, Lcom/sec/android/app/camera/Camera;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/glview/TwGLText;->setText(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 383
    :sswitch_8
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mCurrentPhotoCount:I
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$1500(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)I

    move-result v2

    if-lez v2, :cond_f

    .line 384
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # invokes: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->onDone(Z)V
    invoke-static {v2, v6}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$700(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;Z)V

    .line 388
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$100(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/app/camera/Camera;

    move-result-object v2

    const v3, 0x7f0c027e

    invoke-static {v2, v3, v6}, Lcom/sec/android/app/camera/CameraToast;->makeText(Lcom/sec/android/app/camera/Camera;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 386
    :cond_f
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # invokes: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->onDone(Z)V
    invoke-static {v2, v7}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$700(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;Z)V

    goto :goto_1

    .line 392
    :sswitch_9
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mLeftIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2300(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/glview/TwGLImage;->cancelAnimation()V

    .line 393
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mRightIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2400(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/glview/TwGLImage;->cancelAnimation()V

    .line 394
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mStraightIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2500(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/glview/TwGLImage;->cancelAnimation()V

    .line 395
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mStraightWarningIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2600(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/glview/TwGLImage;->cancelAnimation()V

    .line 396
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mLeftIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2300(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    invoke-virtual {v2, v7}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 397
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mRightIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2400(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 398
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mStraightIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2500(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 399
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mStraightWarningIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2600(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 400
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # invokes: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->setStabilizerVisibleState(I)V
    invoke-static {v2, v8}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$900(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;I)V

    .line 401
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mHelpText:Lcom/sec/android/glview/TwGLText;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$500(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLText;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;
    invoke-static {v3}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$100(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/app/camera/Camera;

    move-result-object v3

    const v4, 0x7f0c0279

    invoke-virtual {v3, v4}, Lcom/sec/android/app/camera/Camera;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/glview/TwGLText;->setText(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 404
    :sswitch_a
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mLeftIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2300(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/glview/TwGLImage;->cancelAnimation()V

    .line 405
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mRightIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2400(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/glview/TwGLImage;->cancelAnimation()V

    .line 406
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mStraightIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2500(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/glview/TwGLImage;->cancelAnimation()V

    .line 407
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mStraightWarningIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2600(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/glview/TwGLImage;->cancelAnimation()V

    .line 408
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mLeftIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2300(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 409
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mRightIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2400(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    invoke-virtual {v2, v7}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 410
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mStraightIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2500(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 411
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mStraightWarningIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2600(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 412
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # invokes: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->setStabilizerVisibleState(I)V
    invoke-static {v2, v8}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$900(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;I)V

    .line 413
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mHelpText:Lcom/sec/android/glview/TwGLText;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$500(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLText;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;
    invoke-static {v3}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$100(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/app/camera/Camera;

    move-result-object v3

    const v4, 0x7f0c027b

    invoke-virtual {v3, v4}, Lcom/sec/android/app/camera/Camera;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/glview/TwGLText;->setText(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 417
    :sswitch_b
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mLeftIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2300(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/glview/TwGLImage;->cancelAnimation()V

    .line 418
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mRightIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2400(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/glview/TwGLImage;->cancelAnimation()V

    .line 419
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mStraightIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2500(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/glview/TwGLImage;->cancelAnimation()V

    .line 420
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mStraightWarningIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2600(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/glview/TwGLImage;->cancelAnimation()V

    .line 421
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mLeftIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2300(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    invoke-virtual {v2, v7}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 422
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mRightIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2400(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    invoke-virtual {v2, v7}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 423
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mStraightIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2500(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    invoke-virtual {v2, v7}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 424
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mStraightWarningIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2600(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 425
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mHelpText:Lcom/sec/android/glview/TwGLText;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$500(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLText;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;
    invoke-static {v3}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$100(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/app/camera/Camera;

    move-result-object v3

    const v4, 0x7f0c027f

    invoke-virtual {v3, v4}, Lcom/sec/android/app/camera/Camera;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/glview/TwGLText;->setText(Ljava/lang/String;)V

    .line 426
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # invokes: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->setStabilizerVisibleState(I)V
    invoke-static {v2, v7}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$900(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;I)V

    .line 427
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$100(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/app/camera/Camera;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/camera/CameraSettings;->getShootingMode()I

    move-result v2

    if-ne v2, v9, :cond_1

    .line 428
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$100(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/app/camera/Camera;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/camera/Camera;->getGLContext()Lcom/sec/android/glview/TwGLContext;

    move-result-object v2

    invoke-virtual {v2, v8}, Lcom/sec/android/glview/TwGLContext;->playHaptic(I)V

    goto/16 :goto_0

    .line 432
    :sswitch_c
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # invokes: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->setWarningArrowVisibleState(ZI)V
    invoke-static {v2, v7, v7}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2700(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;ZI)V

    .line 433
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mUndoButtonGroup:Lcom/sec/android/glview/TwGLViewGroup;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$1100(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLViewGroup;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/sec/android/glview/TwGLViewGroup;->setVisibility(I)V

    .line 434
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mDoneButton:Lcom/sec/android/glview/TwGLButton;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$400(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLButton;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/sec/android/glview/TwGLButton;->setVisibility(I)V

    .line 435
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mThumbnailOpenButton:Lcom/sec/android/glview/TwGLButton;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$1200(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLButton;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/sec/android/glview/TwGLButton;->setVisibility(I)V

    .line 436
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mLeftIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2300(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 437
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mRightIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2400(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 438
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mStraightIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2500(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 439
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mStraightWarningIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2600(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 440
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # invokes: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->setStabilizerVisibleState(I)V
    invoke-static {v2, v6}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$900(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;I)V

    .line 441
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mHelpText:Lcom/sec/android/glview/TwGLText;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$500(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLText;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;
    invoke-static {v3}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$100(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/app/camera/Camera;

    move-result-object v3

    const v4, 0x7f0c013c

    invoke-virtual {v3, v4}, Lcom/sec/android/app/camera/Camera;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/glview/TwGLText;->setText(Ljava/lang/String;)V

    .line 442
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mHelpText:Lcom/sec/android/glview/TwGLText;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$500(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLText;

    move-result-object v2

    invoke-virtual {v2, v7}, Lcom/sec/android/glview/TwGLText;->setVisibility(I)V

    .line 443
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$100(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/app/camera/Camera;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/camera/Camera;->showBaseMenu()V

    .line 444
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # invokes: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->resetStabilizer()V
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$600(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)V

    goto/16 :goto_0

    .line 463
    :sswitch_d
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # invokes: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->onDone(Z)V
    invoke-static {v2, v6}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$700(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;Z)V

    goto/16 :goto_0

    .line 511
    :sswitch_e
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mCurrentPhotoCount:I
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$1500(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)I

    move-result v2

    if-lez v2, :cond_10

    .line 512
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # invokes: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->onDone(Z)V
    invoke-static {v2, v6}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$700(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;Z)V

    .line 516
    :goto_2
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$100(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/app/camera/Camera;

    move-result-object v2

    const v3, 0x7f0c027c

    invoke-static {v2, v3, v6}, Lcom/sec/android/app/camera/CameraToast;->makeText(Lcom/sec/android/app/camera/Camera;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 514
    :cond_10
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # invokes: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->onDone(Z)V
    invoke-static {v2, v7}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$700(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;Z)V

    goto :goto_2

    .line 525
    :sswitch_f
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mLeftIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2300(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 526
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mRightIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2400(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 527
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mStraightIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2500(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    invoke-virtual {v2, v7}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 528
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mStraightWarningIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2600(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 529
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mHelpText:Lcom/sec/android/glview/TwGLText;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$500(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLText;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;
    invoke-static {v3}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$100(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/app/camera/Camera;

    move-result-object v3

    const v4, 0x7f0c0276

    invoke-virtual {v3, v4}, Lcom/sec/android/app/camera/Camera;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/glview/TwGLText;->setText(Ljava/lang/String;)V

    .line 530
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # invokes: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->setStabilizerVisibleState(I)V
    invoke-static {v2, v7}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$900(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;I)V

    .line 531
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mStraightIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2500(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mBlinkingAnimation:Landroid/view/animation/Animation;
    invoke-static {v3}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2800(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Landroid/view/animation/Animation;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/glview/TwGLImage;->setAnimation(Landroid/view/animation/Animation;)V

    .line 532
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mStraightIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2500(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/glview/TwGLImage;->startAnimation()V

    goto/16 :goto_0

    .line 535
    :sswitch_10
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mLeftIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2300(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    invoke-virtual {v2, v7, v7}, Lcom/sec/android/glview/TwGLImage;->setVisibility(IZ)V

    .line 536
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mRightIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2400(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    invoke-virtual {v2, v5, v7}, Lcom/sec/android/glview/TwGLImage;->setVisibility(IZ)V

    .line 537
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mStraightIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2500(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    invoke-virtual {v2, v5, v7}, Lcom/sec/android/glview/TwGLImage;->setVisibility(IZ)V

    .line 538
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mStraightWarningIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2600(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 539
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mHelpText:Lcom/sec/android/glview/TwGLText;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$500(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLText;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;
    invoke-static {v3}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$100(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/app/camera/Camera;

    move-result-object v3

    const v4, 0x7f0c0278

    invoke-virtual {v3, v4}, Lcom/sec/android/app/camera/Camera;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/glview/TwGLText;->setText(Ljava/lang/String;)V

    .line 540
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # invokes: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->setStabilizerVisibleState(IZ)V
    invoke-static {v2, v7, v7}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2900(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;IZ)V

    .line 541
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mLeftIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2300(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mBlinkingAnimation:Landroid/view/animation/Animation;
    invoke-static {v3}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2800(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Landroid/view/animation/Animation;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/glview/TwGLImage;->setAnimation(Landroid/view/animation/Animation;)V

    .line 542
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mLeftIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2300(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/glview/TwGLImage;->startAnimation()V

    goto/16 :goto_0

    .line 545
    :sswitch_11
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mLeftIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2300(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    invoke-virtual {v2, v5, v7}, Lcom/sec/android/glview/TwGLImage;->setVisibility(IZ)V

    .line 546
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mRightIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2400(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    invoke-virtual {v2, v7, v7}, Lcom/sec/android/glview/TwGLImage;->setVisibility(IZ)V

    .line 547
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mStraightIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2500(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    invoke-virtual {v2, v5, v7}, Lcom/sec/android/glview/TwGLImage;->setVisibility(IZ)V

    .line 548
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mStraightWarningIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2600(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 549
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mHelpText:Lcom/sec/android/glview/TwGLText;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$500(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLText;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;
    invoke-static {v3}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$100(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/app/camera/Camera;

    move-result-object v3

    const v4, 0x7f0c027a

    invoke-virtual {v3, v4}, Lcom/sec/android/app/camera/Camera;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/glview/TwGLText;->setText(Ljava/lang/String;)V

    .line 550
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # invokes: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->setStabilizerVisibleState(IZ)V
    invoke-static {v2, v7, v7}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2900(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;IZ)V

    .line 551
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mRightIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2400(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mBlinkingAnimation:Landroid/view/animation/Animation;
    invoke-static {v3}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2800(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Landroid/view/animation/Animation;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/glview/TwGLImage;->setAnimation(Landroid/view/animation/Animation;)V

    .line 552
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mRightIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2400(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/glview/TwGLImage;->startAnimation()V

    goto/16 :goto_0

    .line 559
    :sswitch_12
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mLeftIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2300(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/glview/TwGLImage;->cancelAnimation()V

    .line 560
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mStraightIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2500(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/glview/TwGLImage;->cancelAnimation()V

    .line 561
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mStraightWarningIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2600(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/glview/TwGLImage;->cancelAnimation()V

    .line 562
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mLeftIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2300(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 563
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mStraightIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2500(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 564
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mStraightWarningIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2600(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 565
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mHelpText:Lcom/sec/android/glview/TwGLText;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$500(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLText;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;
    invoke-static {v3}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$100(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/app/camera/Camera;

    move-result-object v3

    const v4, 0x7f0c027a

    invoke-virtual {v3, v4}, Lcom/sec/android/app/camera/Camera;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/glview/TwGLText;->setText(Ljava/lang/String;)V

    .line 566
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # invokes: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->setStabilizerVisibleState(IZ)V
    invoke-static {v2, v7, v7}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2900(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;IZ)V

    .line 567
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mRightIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2400(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    invoke-virtual {v2, v7}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 568
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mRightIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2400(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mBlinkingAnimation:Landroid/view/animation/Animation;
    invoke-static {v3}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2800(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Landroid/view/animation/Animation;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/glview/TwGLImage;->setAnimation(Landroid/view/animation/Animation;)V

    .line 569
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mRightIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2400(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/glview/TwGLImage;->startAnimation()V

    goto/16 :goto_0

    .line 572
    :sswitch_13
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mRightIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2400(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/glview/TwGLImage;->cancelAnimation()V

    .line 573
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mStraightIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2500(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/glview/TwGLImage;->cancelAnimation()V

    .line 574
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mStraightWarningIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2600(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/glview/TwGLImage;->cancelAnimation()V

    .line 575
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mRightIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2400(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 576
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mStraightIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2500(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 577
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mStraightWarningIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2600(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 578
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mHelpText:Lcom/sec/android/glview/TwGLText;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$500(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLText;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;
    invoke-static {v3}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$100(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/app/camera/Camera;

    move-result-object v3

    const v4, 0x7f0c0278

    invoke-virtual {v3, v4}, Lcom/sec/android/app/camera/Camera;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/glview/TwGLText;->setText(Ljava/lang/String;)V

    .line 579
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # invokes: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->setStabilizerVisibleState(IZ)V
    invoke-static {v2, v7, v7}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2900(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;IZ)V

    .line 580
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mLeftIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2300(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    invoke-virtual {v2, v7}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 581
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mLeftIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2300(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mBlinkingAnimation:Landroid/view/animation/Animation;
    invoke-static {v3}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2800(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Landroid/view/animation/Animation;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/glview/TwGLImage;->setAnimation(Landroid/view/animation/Animation;)V

    .line 582
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mLeftIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2300(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/glview/TwGLImage;->startAnimation()V

    goto/16 :goto_0

    .line 586
    :sswitch_14
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    iget v3, p1, Landroid/os/Message;->what:I

    # invokes: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->setWarningArrowVisibleState(ZI)V
    invoke-static {v2, v6, v3}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2700(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;ZI)V

    .line 587
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mGuideGroup:Lcom/sec/android/glview/TwGLViewGroup;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$1000(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLViewGroup;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/sec/android/glview/TwGLViewGroup;->setVisibility(I)V

    .line 588
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mHelpText:Lcom/sec/android/glview/TwGLText;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$500(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLText;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/sec/android/glview/TwGLText;->setVisibility(I)V

    goto/16 :goto_0

    .line 591
    :sswitch_15
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # invokes: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->setWarningArrowVisibleState(ZI)V
    invoke-static {v2, v7, v7}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2700(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;ZI)V

    .line 592
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mGuideGroup:Lcom/sec/android/glview/TwGLViewGroup;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$1000(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLViewGroup;

    move-result-object v2

    invoke-virtual {v2, v7}, Lcom/sec/android/glview/TwGLViewGroup;->setVisibility(I)V

    .line 593
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mThumbnailList:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourThumbnailList;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$3000(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourThumbnailList;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourThumbnailList;->getVisibility()I

    move-result v2

    if-ne v2, v5, :cond_1

    .line 594
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mHelpText:Lcom/sec/android/glview/TwGLText;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$500(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLText;

    move-result-object v2

    invoke-virtual {v2, v7}, Lcom/sec/android/glview/TwGLText;->setVisibility(I)V

    goto/16 :goto_0

    .line 599
    :sswitch_16
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # invokes: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->onDone(Z)V
    invoke-static {v2, v6}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$700(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;Z)V

    goto/16 :goto_0

    .line 602
    :sswitch_17
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # invokes: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->onDone(Z)V
    invoke-static {v2, v7}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$700(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;Z)V

    goto/16 :goto_0

    .line 605
    :sswitch_18
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # invokes: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->onDone(Z)V
    invoke-static {v2, v6}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$700(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;Z)V

    .line 607
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$100(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/app/camera/Camera;

    move-result-object v2

    const v3, 0x7f0c027c

    invoke-static {v2, v3, v6}, Lcom/sec/android/app/camera/CameraToast;->makeText(Lcom/sec/android/app/camera/Camera;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 610
    :sswitch_19
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    invoke-virtual {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->isActive()Z

    move-result v2

    if-eqz v2, :cond_11

    .line 611
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # invokes: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->restart()V
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$3100(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)V

    .line 613
    :cond_11
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$100(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/app/camera/Camera;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/camera/Camera;->getEngine()Lcom/sec/android/app/camera/CommonEngine;

    move-result-object v2

    if-eqz v2, :cond_12

    .line 614
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$100(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/app/camera/Camera;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/camera/Camera;->getEngine()Lcom/sec/android/app/camera/CommonEngine;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/camera/CommonEngine;->resetContinuousFileSequence()V

    .line 615
    :cond_12
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$100(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/app/camera/Camera;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/camera/Camera;->on3DTourComplete()V

    .line 616
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$100(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/app/camera/Camera;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/camera/Camera;->showThumbnailButton()V

    goto/16 :goto_0

    .line 619
    :sswitch_1a
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mCurrentPhotoCount:I
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$1500(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)I

    move-result v2

    if-lez v2, :cond_13

    .line 620
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # invokes: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->onDone(Z)V
    invoke-static {v2, v6}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$700(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;Z)V

    .line 624
    :goto_3
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$100(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/app/camera/Camera;

    move-result-object v2

    const v3, 0x7f0c0281

    invoke-static {v2, v3, v6}, Lcom/sec/android/app/camera/CameraToast;->makeText(Lcom/sec/android/app/camera/Camera;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 622
    :cond_13
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # invokes: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->onDone(Z)V
    invoke-static {v2, v7}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$700(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;Z)V

    goto :goto_3

    .line 641
    :sswitch_1b
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mRemainingStepCount:I
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$3200(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)I

    move-result v2

    if-lez v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mRemainingStepCount:I
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$3200(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)I

    move-result v2

    const/4 v3, 0x6

    if-ge v2, v3, :cond_1

    .line 642
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mLeftIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2300(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 643
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mRightIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2400(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 644
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mStraightIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2500(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 645
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mStraightWarningIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2600(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    invoke-virtual {v2, v7}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 646
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mHelpText:Lcom/sec/android/glview/TwGLText;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$500(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLText;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;
    invoke-static {v3}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$100(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/app/camera/Camera;

    move-result-object v3

    const v4, 0x7f0c027d

    new-array v5, v6, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mRemainingStepCount:I
    invoke-static {v6}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$3200(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/camera/Camera;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/glview/TwGLText;->setText(Ljava/lang/String;)V

    .line 647
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # invokes: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->setStabilizerVisibleState(I)V
    invoke-static {v2, v8}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$900(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;I)V

    .line 648
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mStraightWarningIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2600(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mBlinkingAnimation:Landroid/view/animation/Animation;
    invoke-static {v3}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2800(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Landroid/view/animation/Animation;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/glview/TwGLImage;->setAnimation(Landroid/view/animation/Animation;)V

    .line 649
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mStraightWarningIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2600(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/glview/TwGLImage;->startAnimation()V

    goto/16 :goto_0

    .line 665
    :sswitch_1c
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mLeftIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2300(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/glview/TwGLImage;->cancelAnimation()V

    .line 666
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mRightIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2400(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/glview/TwGLImage;->cancelAnimation()V

    .line 667
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mStraightIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2500(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/glview/TwGLImage;->cancelAnimation()V

    .line 668
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mStraightWarningIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2600(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/glview/TwGLImage;->cancelAnimation()V

    .line 669
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mLeftIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2300(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 670
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mRightIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2400(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 671
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mStraightIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2500(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    invoke-virtual {v2, v7}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 672
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mStraightWarningIndicator:Lcom/sec/android/glview/TwGLImage;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$2600(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLImage;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/sec/android/glview/TwGLImage;->setVisibility(I)V

    .line 673
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # invokes: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->setStabilizerVisibleState(I)V
    invoke-static {v2, v8}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$900(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;I)V

    .line 674
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mHelpText:Lcom/sec/android/glview/TwGLText;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$500(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/glview/TwGLText;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;
    invoke-static {v3}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$100(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/app/camera/Camera;

    move-result-object v3

    const v4, 0x7f0c0277

    invoke-virtual {v3, v4}, Lcom/sec/android/app/camera/Camera;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/glview/TwGLText;->setText(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 677
    :sswitch_1d
    const/16 v2, 0xcc

    invoke-virtual {p0, v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->removeMessages(I)V

    .line 678
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mPlayWarningArrowSound:Z
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$3300(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 679
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mActivityContext:Lcom/sec/android/app/camera/Camera;
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$100(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)Lcom/sec/android/app/camera/Camera;

    move-result-object v2

    sget v3, Lcom/sec/android/app/camera/Camera;->SHUTTER_SOUND_PANORAMA_WARNING:I

    invoke-virtual {v2, v3, v7}, Lcom/sec/android/app/camera/Camera;->playCameraSound(II)V

    .line 680
    const/16 v2, 0xcc

    const-wide/16 v4, 0x3e8

    invoke-virtual {p0, v2, v4, v5}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    .line 684
    :sswitch_1e
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # getter for: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->mCurrentPhotoCount:I
    invoke-static {v2}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$1500(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;)I

    move-result v2

    if-lez v2, :cond_14

    .line 685
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # invokes: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->onDone(Z)V
    invoke-static {v2, v6}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$700(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;Z)V

    goto/16 :goto_0

    .line 687
    :cond_14
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu$1;->this$0:Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;

    # invokes: Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->onDone(Z)V
    invoke-static {v2, v7}, Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;->access$700(Lcom/sec/android/app/camera/glwidget/TwGLVirtualTourMenu;Z)V

    goto/16 :goto_0

    .line 276
    nop

    :sswitch_data_0
    .sparse-switch
        0x5 -> :sswitch_1
        0x6 -> :sswitch_2
        0x7 -> :sswitch_3
        0x8 -> :sswitch_4
        0x9 -> :sswitch_5
        0xa -> :sswitch_6
        0xb -> :sswitch_7
        0xc -> :sswitch_8
        0xd -> :sswitch_9
        0xe -> :sswitch_a
        0xf -> :sswitch_b
        0x10 -> :sswitch_c
        0x11 -> :sswitch_0
        0x12 -> :sswitch_d
        0x13 -> :sswitch_0
        0x14 -> :sswitch_0
        0x15 -> :sswitch_0
        0x16 -> :sswitch_0
        0x17 -> :sswitch_e
        0x18 -> :sswitch_0
        0x19 -> :sswitch_f
        0x1a -> :sswitch_10
        0x1b -> :sswitch_11
        0x1c -> :sswitch_0
        0x1d -> :sswitch_0
        0x1e -> :sswitch_12
        0x1f -> :sswitch_13
        0x20 -> :sswitch_14
        0x21 -> :sswitch_14
        0x23 -> :sswitch_1a
        0x24 -> :sswitch_15
        0x25 -> :sswitch_0
        0x26 -> :sswitch_1c
        0x27 -> :sswitch_1e
        0xc8 -> :sswitch_16
        0xc9 -> :sswitch_17
        0xca -> :sswitch_18
        0xcb -> :sswitch_1b
        0xcc -> :sswitch_1d
        0x3e8 -> :sswitch_19
    .end sparse-switch
.end method
