.class public Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity;
.super Ljava/lang/Object;
.source "SDualScreenActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity$ScreenChangeListener;,
        Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity$DualScreen;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = true

.field public static final FLAG_COUPLED_TASK_CONTEXTUAL_MODE:I

.field public static final FLAG_COUPLED_TASK_EXPAND_MODE:I

.field private static final TAG:Ljava/lang/String;

.field private static mDualScreenAvailable:Z

.field private static mDualScreenAvailableChecked:Z


# instance fields
.field private mDualScreenManagerReflector:Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;

.field private mScreenChangeListener:Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity$ScreenChangeListener;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 29
    const-class v0, Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity;->TAG:Ljava/lang/String;

    .line 33
    sput-boolean v1, Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity;->mDualScreenAvailable:Z

    .line 34
    sput-boolean v1, Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity;->mDualScreenAvailableChecked:Z

    .line 63
    sget v0, Lcom/samsung/android/sdk/dualscreen/SDualScreenConstantsReflector$DualScreenLaunchParams;->FLAG_COUPLED_TASK_CONTEXTUAL_MODE:I

    sput v0, Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity;->FLAG_COUPLED_TASK_CONTEXTUAL_MODE:I

    .line 71
    sget v0, Lcom/samsung/android/sdk/dualscreen/SDualScreenConstantsReflector$DualScreenLaunchParams;->FLAG_COUPLED_TASK_EXPAND_MODE:I

    sput v0, Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity;->FLAG_COUPLED_TASK_EXPAND_MODE:I

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;)V
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    if-nez p1, :cond_0

    .line 79
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "activity is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 81
    :cond_0
    new-instance v0, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;

    invoke-direct {v0, p1}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity;->mDualScreenManagerReflector:Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;

    .line 82
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity;)Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity$ScreenChangeListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity;->mScreenChangeListener:Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity$ScreenChangeListener;

    return-object v0
.end method

.method public static getFocusedScreen()Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity$DualScreen;
    .locals 2

    .prologue
    .line 194
    sget-object v0, Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity;->TAG:Ljava/lang/String;

    const-string v1, "getFocusedScreen()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 195
    invoke-static {}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->getFocusedScreen()Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity$DualScreen;

    move-result-object v0

    return-object v0
.end method

.method private isDualScreenAvailable()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 248
    sget-boolean v0, Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity;->mDualScreenAvailableChecked:Z

    if-eqz v0, :cond_0

    .line 249
    sget-boolean v0, Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity;->mDualScreenAvailable:Z

    .line 254
    :goto_0
    return v0

    .line 251
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity;->mDualScreenManagerReflector:Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity;->mDualScreenManagerReflector:Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->initialized()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    sput-boolean v0, Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity;->mDualScreenAvailable:Z

    .line 253
    sput-boolean v1, Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity;->mDualScreenAvailableChecked:Z

    .line 254
    sget-boolean v0, Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity;->mDualScreenAvailable:Z

    goto :goto_0

    .line 251
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static makeIntent(Landroid/content/Context;Landroid/content/Intent;Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity$DualScreen;I)Landroid/content/Intent;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "targetScreen"    # Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity$DualScreen;
    .param p3, "flags"    # I

    .prologue
    .line 180
    sget-object v1, Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity;->TAG:Ljava/lang/String;

    const-string v2, "makeIntent()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 181
    move-object v0, p1

    .line 182
    .local v0, "ret":Landroid/content/Intent;
    invoke-static {p0, p1, p2, p3}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->makeIntent(Landroid/content/Context;Landroid/content/Intent;Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity$DualScreen;I)Landroid/content/Intent;

    move-result-object v0

    .line 183
    sget-object v1, Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "makeIntent() : ret="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    return-object v0
.end method


# virtual methods
.method public collapse()V
    .locals 4

    .prologue
    .line 126
    sget-object v1, Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity;->TAG:Ljava/lang/String;

    const-string v2, "collapse()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    invoke-direct {p0}, Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity;->isDualScreenAvailable()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 128
    iget-object v1, p0, Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity;->mDualScreenManagerReflector:Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;

    sget-object v2, Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity$DualScreen;->MAIN:Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity$DualScreen;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->moveToScreen(Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity$DualScreen;)Z

    move-result v0

    .line 129
    .local v0, "ret":Z
    sget-object v1, Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "collapse() ret="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    .end local v0    # "ret":Z
    :goto_0
    return-void

    .line 131
    :cond_0
    sget-object v1, Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity;->TAG:Ljava/lang/String;

    const-string v2, "DualScreenManager is not loaded"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public expand()V
    .locals 2

    .prologue
    .line 111
    sget-object v0, Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity;->TAG:Ljava/lang/String;

    const-string v1, "expand()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    invoke-direct {p0}, Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity;->isDualScreenAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 113
    iget-object v0, p0, Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity;->mDualScreenManagerReflector:Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;

    sget-object v1, Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity$DualScreen;->EXPANDED:Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity$DualScreen;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->moveToScreen(Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity$DualScreen;)Z

    .line 117
    :goto_0
    return-void

    .line 115
    :cond_0
    sget-object v0, Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity;->TAG:Ljava/lang/String;

    const-string v1, "DualScreenManager is not loaded"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getCoupledTaskInfo()Lcom/samsung/android/sdk/dualscreen/SDualScreenCoupledTaskInfo;
    .locals 2

    .prologue
    .line 204
    sget-object v0, Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity;->TAG:Ljava/lang/String;

    const-string v1, "getCoupledTaskInfo()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 205
    iget-object v0, p0, Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity;->mDualScreenManagerReflector:Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->getCoupledTaskInfo()Lcom/samsung/android/sdk/dualscreen/SDualScreenCoupledTaskInfo;

    move-result-object v0

    return-object v0
.end method

.method public getScreen()Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity$DualScreen;
    .locals 4

    .prologue
    .line 93
    sget-object v1, Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity;->TAG:Ljava/lang/String;

    const-string v2, "getScreen()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    invoke-direct {p0}, Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity;->isDualScreenAvailable()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 95
    iget-object v1, p0, Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity;->mDualScreenManagerReflector:Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->getScreen()Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity$DualScreen;

    move-result-object v0

    .line 96
    .local v0, "ret":Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity$DualScreen;
    sget-object v1, Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getScreen() ret="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    .end local v0    # "ret":Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity$DualScreen;
    :goto_0
    return-object v0

    .line 99
    :cond_0
    sget-object v1, Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity;->TAG:Ljava/lang/String;

    const-string v2, "DualScreenManager is not loaded"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    sget-object v0, Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity$DualScreen;->UNKNOWN:Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity$DualScreen;

    goto :goto_0
.end method

.method public setScreenChangeListener(Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity$ScreenChangeListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity$ScreenChangeListener;

    .prologue
    .line 216
    sget-object v1, Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity;->TAG:Ljava/lang/String;

    const-string v2, "setScreenChangeListener()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 217
    invoke-direct {p0}, Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity;->isDualScreenAvailable()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 218
    iput-object p1, p0, Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity;->mScreenChangeListener:Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity$ScreenChangeListener;

    .line 219
    iget-object v1, p0, Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity;->mScreenChangeListener:Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity$ScreenChangeListener;

    if-eqz v1, :cond_0

    .line 220
    new-instance v0, Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity$1;-><init>(Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity;)V

    .line 225
    .local v0, "screenChangeListener":Lcom/samsung/android/sdk/dualscreen/SDualScreenListener$ScreenChangeListener;
    iget-object v1, p0, Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity;->mDualScreenManagerReflector:Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->setScreenChangedListner(Lcom/samsung/android/sdk/dualscreen/SDualScreenListener$ScreenChangeListener;)V

    .line 231
    .end local v0    # "screenChangeListener":Lcom/samsung/android/sdk/dualscreen/SDualScreenListener$ScreenChangeListener;
    :cond_0
    :goto_0
    return-void

    .line 229
    :cond_1
    sget-object v1, Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity;->TAG:Ljava/lang/String;

    const-string v2, "DualScreenManager is not loaded"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public swapTopTask()V
    .locals 2

    .prologue
    .line 156
    sget-object v0, Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity;->TAG:Ljava/lang/String;

    const-string v1, "swapTopTask()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 157
    invoke-direct {p0}, Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity;->isDualScreenAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 158
    iget-object v0, p0, Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity;->mDualScreenManagerReflector:Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->swapTopTask()V

    .line 162
    :goto_0
    return-void

    .line 160
    :cond_0
    sget-object v0, Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity;->TAG:Ljava/lang/String;

    const-string v1, "DualScreenManager is not loaded"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public switchScreen()V
    .locals 2

    .prologue
    .line 140
    sget-object v0, Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity;->TAG:Ljava/lang/String;

    const-string v1, "switchScreen()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    invoke-direct {p0}, Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity;->isDualScreenAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 142
    iget-object v0, p0, Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity;->mDualScreenManagerReflector:Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->switchScreen()V

    .line 146
    :goto_0
    return-void

    .line 144
    :cond_0
    sget-object v0, Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity;->TAG:Ljava/lang/String;

    const-string v1, "DualScreenManager is not loaded"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
