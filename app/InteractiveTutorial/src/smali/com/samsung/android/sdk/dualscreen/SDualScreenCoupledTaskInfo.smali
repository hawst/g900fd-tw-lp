.class public Lcom/samsung/android/sdk/dualscreen/SDualScreenCoupledTaskInfo;
.super Ljava/lang/Object;
.source "SDualScreenCoupledTaskInfo.java"


# static fields
.field public static RUNNING_MODE_CONTEXTUAL:I = 0x0

.field public static RUNNING_MODE_EXPAND:I = 0x0

.field public static RUNNING_MODE_UNDEFINED:I = 0x0

.field public static final UNSPECIFIED_TASK_ID:I = -0x1


# instance fields
.field private mChildTaskId:I

.field private mCoupledMode:I

.field private mIsCoupled:Z

.field private mParentTaskId:I

.field private mTaskId:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x0

    sput v0, Lcom/samsung/android/sdk/dualscreen/SDualScreenCoupledTaskInfo;->RUNNING_MODE_UNDEFINED:I

    .line 34
    sget v0, Lcom/samsung/android/sdk/dualscreen/SDualScreenConstantsReflector$DualScreenLaunchParams;->FLAG_COUPLED_TASK_CONTEXTUAL_MODE:I

    sput v0, Lcom/samsung/android/sdk/dualscreen/SDualScreenCoupledTaskInfo;->RUNNING_MODE_CONTEXTUAL:I

    .line 35
    sget v0, Lcom/samsung/android/sdk/dualscreen/SDualScreenConstantsReflector$DualScreenLaunchParams;->FLAG_COUPLED_TASK_EXPAND_MODE:I

    sput v0, Lcom/samsung/android/sdk/dualscreen/SDualScreenCoupledTaskInfo;->RUNNING_MODE_EXPAND:I

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput v0, p0, Lcom/samsung/android/sdk/dualscreen/SDualScreenCoupledTaskInfo;->mParentTaskId:I

    .line 30
    iput v0, p0, Lcom/samsung/android/sdk/dualscreen/SDualScreenCoupledTaskInfo;->mChildTaskId:I

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/dualscreen/SDualScreenCoupledTaskInfo;->mIsCoupled:Z

    .line 39
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1, "taskId"    # I

    .prologue
    const/4 v0, -0x1

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput v0, p0, Lcom/samsung/android/sdk/dualscreen/SDualScreenCoupledTaskInfo;->mParentTaskId:I

    .line 30
    iput v0, p0, Lcom/samsung/android/sdk/dualscreen/SDualScreenCoupledTaskInfo;->mChildTaskId:I

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/dualscreen/SDualScreenCoupledTaskInfo;->mIsCoupled:Z

    .line 42
    iput p1, p0, Lcom/samsung/android/sdk/dualscreen/SDualScreenCoupledTaskInfo;->mTaskId:I

    .line 43
    return-void
.end method


# virtual methods
.method public getChildCoupledTaskId()I
    .locals 1

    .prologue
    .line 58
    iget v0, p0, Lcom/samsung/android/sdk/dualscreen/SDualScreenCoupledTaskInfo;->mChildTaskId:I

    return v0
.end method

.method public getCoupledTaskMode()I
    .locals 1

    .prologue
    .line 50
    iget v0, p0, Lcom/samsung/android/sdk/dualscreen/SDualScreenCoupledTaskInfo;->mCoupledMode:I

    return v0
.end method

.method public getParentCoupledTaskId()I
    .locals 1

    .prologue
    .line 54
    iget v0, p0, Lcom/samsung/android/sdk/dualscreen/SDualScreenCoupledTaskInfo;->mParentTaskId:I

    return v0
.end method

.method public getTaskId()I
    .locals 1

    .prologue
    .line 46
    iget v0, p0, Lcom/samsung/android/sdk/dualscreen/SDualScreenCoupledTaskInfo;->mTaskId:I

    return v0
.end method

.method public isCoupled()Z
    .locals 1

    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/samsung/android/sdk/dualscreen/SDualScreenCoupledTaskInfo;->mIsCoupled:Z

    return v0
.end method

.method setChildCoupledTaskId(I)V
    .locals 0
    .param p1, "taskId"    # I

    .prologue
    .line 77
    iput p1, p0, Lcom/samsung/android/sdk/dualscreen/SDualScreenCoupledTaskInfo;->mChildTaskId:I

    .line 78
    return-void
.end method

.method setCoupledState(Z)V
    .locals 0
    .param p1, "isCoupled"    # Z

    .prologue
    .line 81
    iput-boolean p1, p0, Lcom/samsung/android/sdk/dualscreen/SDualScreenCoupledTaskInfo;->mIsCoupled:Z

    .line 82
    return-void
.end method

.method setCoupledTaskMode(I)V
    .locals 0
    .param p1, "coupledTaskMode"    # I

    .prologue
    .line 69
    iput p1, p0, Lcom/samsung/android/sdk/dualscreen/SDualScreenCoupledTaskInfo;->mCoupledMode:I

    .line 70
    return-void
.end method

.method setParentCoupledTaskId(I)V
    .locals 0
    .param p1, "taskId"    # I

    .prologue
    .line 73
    iput p1, p0, Lcom/samsung/android/sdk/dualscreen/SDualScreenCoupledTaskInfo;->mParentTaskId:I

    .line 74
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 86
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 87
    .local v0, "b":Ljava/lang/StringBuilder;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-class v2, Lcom/samsung/android/sdk/dualscreen/SDualScreenCoupledTaskInfo;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " { "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 88
    const-string v1, "mTaskId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/sdk/dualscreen/SDualScreenCoupledTaskInfo;->mTaskId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 89
    const-string v1, "mCoupledMode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/sdk/dualscreen/SDualScreenCoupledTaskInfo;->mCoupledMode:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 90
    iget v1, p0, Lcom/samsung/android/sdk/dualscreen/SDualScreenCoupledTaskInfo;->mParentTaskId:I

    if-eq v1, v3, :cond_0

    .line 91
    const-string v1, "mParentTaskId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/sdk/dualscreen/SDualScreenCoupledTaskInfo;->mParentTaskId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 93
    :cond_0
    iget v1, p0, Lcom/samsung/android/sdk/dualscreen/SDualScreenCoupledTaskInfo;->mChildTaskId:I

    if-eq v1, v3, :cond_1

    .line 94
    const-string v1, "mChildTaskId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/sdk/dualscreen/SDualScreenCoupledTaskInfo;->mChildTaskId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 96
    :cond_1
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 97
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
